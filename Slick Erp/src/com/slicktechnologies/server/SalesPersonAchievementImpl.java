package com.slicktechnologies.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.SalesPersonAchievementService;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesperson.TargetInformation;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SalesPersonAchievementImpl extends RemoteServiceServlet implements SalesPersonAchievementService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4429991817224446732L;

	SimpleDateFormat format = new SimpleDateFormat("MMM-yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	  

	@Override
	public ArrayList<TargetInformation> getAchievementDetails(int employeeId,String financialYear, long companyId,String employeeeName) {
		
		List<SalesPersonTargets> targetlist = ofy().load().type(SalesPersonTargets.class).filter("employeeInfo.Id", employeeId).filter("financialYear", financialYear).filter("companyId", companyId).list();
		
		ArrayList<TargetInformation> salespersonAchievementlist = new ArrayList<TargetInformation>();
		
		FinancialCalender financialcalendar = ofy().load().type(FinancialCalender.class).filter("calendarName", financialYear).filter("companyId", companyId).first().now();
		logger.log(Level.SEVERE,"targetlist size"+targetlist.size());
		
		if(financialcalendar!=null){
			
		for(int i=0;i<targetlist.size();i++){
			logger.log(Level.SEVERE,"1111111");

//			Comparator<TargetInformation> contractEndDateComparator2 = new Comparator<TargetInformation>() {
//				public int compare(TargetInformation s1, TargetInformation s2) {
//				
//				Integer target1 = s1.getTarget();
//				Integer target2 = s2.getTarget();
//				
//				//ascending order
//				return target1.compareTo(target2);
//				}
//				};
//				Collections.sort(targetlist.get(i).getTargetInfoList(), contractEndDateComparator2);
				
				for(int j=0;j<targetlist.get(i).getTargetInfoList().size();j++){
					
					logger.log(Level.SEVERE,"222222222");
						
					if(targetlist.get(i).getTargetInfoList().get(j).getDuration().equals("Yearly")){
						double netamount =0;
						logger.log(Level.SEVERE,"333333333333");
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							logger.log(Level.SEVERE,"3.3======");
							logger.log(Level.SEVERE,"Product categoryy  === "+targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", financialcalendar.getStartDate()).filter("contractDate <=", financialcalendar.getEndDate()).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							 logger.log(Level.SEVERE,"contract list size ===="+contractlist.size());

							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
						  
						  logger.log(Level.SEVERE,"4444444444444444");
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("contractDate >=", financialcalendar.getStartDate()).filter("contractDate <=", financialcalendar.getEndDate()).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							 logger.log(Level.SEVERE,"contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(j).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(j).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(j).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(j).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(j).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(j).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						salespersonAchievementlist.add(targetinfo);
					}
					if(targetlist.get(i).getTargetInfoList().get(j).getDuration().equals("Half Yearly")){
						System.out.println(" inside half year");
						
						String month = targetlist.get(i).getTargetInfoList().get(j).getMonths();
						// here i am getting  half year starting month year and half year ending month year in string
						// so i am conveting into date
						String [] monthyear = month.split("To");
						
						System.out.println(" start motnh"+monthyear[0] );
						System.out.println(" end motnh"+monthyear[1] );

						
						Date halfyearstartdate = null;
						try {
							halfyearstartdate = format.parse(monthyear[0]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("Half year Start Date=="+halfyearstartdate);
						
						Date halfyearEnd = null;
						try {
							halfyearEnd = format.parse(monthyear[1]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					
						System.out.println("Half year Date=="+halfyearEnd);
						Date halfyearEndDate = DateUtility.getEndDateofMonth(halfyearEnd);
						System.out.println(" end Date of half year"+halfyearEndDate);
						
//						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
//						System.out.println("contract list size ===="+contractlist.size());
//						double netamount =0;
//						for(int p=0;p<contractlist.size();p++){
//							netamount +=contractlist.get(p).getNetpayable();
//						}
						
						double netamount =0;
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();

							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("contractDate >=",halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(j).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(j).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(j).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(j).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(j).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(j).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						
						salespersonAchievementlist.add(targetinfo);
					}
					
					if(targetlist.get(i).getTargetInfoList().get(j).getDuration().equals("Quaterely")){
						

						String month = targetlist.get(i).getTargetInfoList().get(j).getMonths();
						
						String [] monthyear = month.split("To");
						
						System.out.println(" start motnh"+monthyear[0] );
						System.out.println(" end motnh"+monthyear[1] );

						
						Date quaterelystartdate = null;
						try {
							quaterelystartdate = format.parse(monthyear[0]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("quaterly Start Date=="+quaterelystartdate);
						
						Date quaterelyDate = null;
						try {
							quaterelyDate = format.parse(monthyear[1]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					
						System.out.println("Quaterely Date=="+quaterelyDate);
						Date quterlyEndDate = DateUtility.getEndDateofMonth(quaterelyDate);
						System.out.println(" end Date of Quaterely Date"+quterlyEndDate);
						
//						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
//						System.out.println("contract list size ===="+contractlist.size());
//						double netamount =0;
//						for(int p=0;p<contractlist.size();p++){
//							netamount +=contractlist.get(p).getNetpayable();
//						}
						
						double netamount =0;
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", quaterelystartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("contractDate >=", quaterelystartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(j).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(j).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(j).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(j).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(j).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(j).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						
						salespersonAchievementlist.add(targetinfo);
					}
					
					if(targetlist.get(i).getTargetInfoList().get(j).getDuration().equals("Monthly")){
						System.out.println(" in mothly");
						

						Date monthDate = null;
						try {
							monthDate = format.parse(targetlist.get(i).getTargetInfoList().get(j).getMonths());
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("Monthly Start Date=="+monthDate);
						Date monthlyStartDate=monthDate;
						System.out.println(" monthly first Date ===="+monthlyStartDate);
						
						Date monthEndDate = DateUtility.getEndDateofMonth(monthDate);
						System.out.println(" end Date of Month Date"+monthEndDate);
						
//						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
//						System.out.println("contract list size ===="+contractlist.size());
//						double netamount =0;
//						for(int p=0;p<contractlist.size();p++){
//							netamount +=contractlist.get(p).getNetpayable();
//						}
						
						double netamount =0;
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeeName).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(j).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(j).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(j).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(j).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(j).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(j).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						
						salespersonAchievementlist.add(targetinfo);
					}

						

				}
			
		  }
		
	 }
		return salespersonAchievementlist;
	}

	@Override
	public ArrayList<TargetInformation> getProductCategoryDetails(String financialYear,String productCategory,long companyId) {
		
		List<SalesPersonTargets> targetlist = ofy().load().type(SalesPersonTargets.class).filter("targetInfoList.productCategory", productCategory).filter("financialYear", financialYear).filter("companyId", companyId).list();
		System.out.println("target list size ===="+targetlist.size());
		
		ArrayList<TargetInformation> salespersonAchievementlist = new ArrayList<TargetInformation>();
		
		FinancialCalender financialcalendar = ofy().load().type(FinancialCalender.class).filter("calendarName", financialYear).filter("companyId", companyId).first().now();
		
		if(financialcalendar!=null){
			
			for(int i=0;i<targetlist.size();i++){
			
				for(int j=0;j<targetlist.get(i).getEmployeeInfo().size();j++){
					System.out.println("full name ==="+targetlist.get(i).getEmployeeInfo().get(j).getFullName());
					String employeeName = targetlist.get(i).getEmployeeInfo().get(j).getFullName();

				for(int k=0;k<targetlist.get(i).getTargetInfoList().size();k++){
					System.out.println(" valueee ==="+targetlist.get(i).getTargetInfoList().get(k).getProductCategory());
					
					if(targetlist.get(i).getTargetInfoList().get(k).getProductCategory().equals(productCategory)){
						System.out.println(" 111111 **** ");
						if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Yearly")){
					
						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", financialcalendar.getStartDate()).filter("contractDate <=", financialcalendar.getEndDate()).filter("status", "Approved").filter("companyId", companyId).list();
						System.out.println("contract list size ===="+contractlist.size());
						double netamount =0;
						for(int p=0;p<contractlist.size();p++){
							netamount +=contractlist.get(p).getNetpayable();
						}
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(productCategory);
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						salespersonAchievementlist.add(targetinfo);
					}
					if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Half Yearly")){
						System.out.println(" inside half year");
						String month = targetlist.get(i).getTargetInfoList().get(k).getMonths();
						// here i am getting  half year starting month year and half year ending month year in string
						// so i am conveting into date
						String [] monthyear = month.split("To");
						
						System.out.println(" start motnh"+monthyear[0] );
						System.out.println(" end motnh"+monthyear[1] );

						
						Date halfyearstartdate = null;
						try {
							halfyearstartdate = format.parse(monthyear[0]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("Half year Start Date=="+halfyearstartdate);
						
						Date halfyearEnd = null;
						try {
							halfyearEnd = format.parse(monthyear[1]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					
						System.out.println("Half year Date=="+halfyearEnd);
						Date halfyearEndDate = DateUtility.getEndDateofMonth(halfyearEnd);
						System.out.println(" end Date of half year"+halfyearEndDate);
						
						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
						System.out.println("contract list size ===="+contractlist.size());
						double netamount =0;
						for(int p=0;p<contractlist.size();p++){
							netamount +=contractlist.get(p).getNetpayable();
						}
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(productCategory);
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						salespersonAchievementlist.add(targetinfo);
					}
					
					if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Quaterely")){
						

						String month = targetlist.get(i).getTargetInfoList().get(k).getMonths();
						
						String [] monthyear = month.split("To");
						
						System.out.println(" start motnh"+monthyear[0] );
						System.out.println(" end motnh"+monthyear[1] );

						
						Date quaterelystartdate = null;
						try {
							quaterelystartdate = format.parse(monthyear[0]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("quaterly Start Date=="+quaterelystartdate);
						
						Date quaterelyDate = null;
						try {
							quaterelyDate = format.parse(monthyear[1]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					
						System.out.println("Quaterely Date=="+quaterelyDate);
						Date quterlyEndDate = DateUtility.getEndDateofMonth(quaterelyDate);
						System.out.println(" end Date of Quaterely Date"+quterlyEndDate);
						
						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", quaterelystartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
						System.out.println("contract list size ===="+contractlist.size());
						double netamount =0;
						for(int p=0;p<contractlist.size();p++){
							netamount +=contractlist.get(p).getNetpayable();
						}
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(productCategory);
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						salespersonAchievementlist.add(targetinfo);
					}
					
					if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Monthly")){
						System.out.println(" in mothly");
						
						Date monthDate = null;
						try {
							monthDate = format.parse(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("Monthly Start Date=="+monthDate);
						Date monthlyStartDate=monthDate;
						System.out.println(" monthly first Date ===="+monthlyStartDate);
						
						Date monthEndDate = DateUtility.getEndDateofMonth(monthDate);
						System.out.println(" end Date of Month Date"+monthEndDate);
						
						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
						System.out.println("contract list size ===="+contractlist.size());
						double netamount =0;
						for(int p=0;p<contractlist.size();p++){
							netamount +=contractlist.get(p).getNetpayable();
						}
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(productCategory);
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						salespersonAchievementlist.add(targetinfo);
					}

					}

				}
				
				}
			
		    }
		}
		

		return salespersonAchievementlist;
	}

	@Override
	public ArrayList<TargetInformation> getemployeeRoleDetails(String financialYear, String employeeRole, long companyId) {

		List<SalesPersonTargets> targetlist = ofy().load().type(SalesPersonTargets.class).filter("employeeRole", employeeRole).filter("financialYear", financialYear).filter("companyId", companyId).list();
		System.out.println("target list size ===="+targetlist.size());
		
		ArrayList<TargetInformation> salespersonAchievementlist = new ArrayList<TargetInformation>();
		
		FinancialCalender financialcalendar = ofy().load().type(FinancialCalender.class).filter("calendarName", financialYear).filter("companyId", companyId).first().now();
		
		if(financialcalendar!=null){
			
			for(int i=0;i<targetlist.size();i++){
			
				for(int j=0;j<targetlist.get(i).getEmployeeInfo().size();j++){
					System.out.println("full name ==="+targetlist.get(i).getEmployeeInfo().get(j).getFullName());
					String employeeName = targetlist.get(i).getEmployeeInfo().get(j).getFullName();

				for(int k=0;k<targetlist.get(i).getTargetInfoList().size();k++){
					System.out.println(" valueee ==="+targetlist.get(i).getTargetInfoList().get(k).getProductCategory());
					
						if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Yearly")){
//							String productCategory = targetlist.get(i).getTargetInfoList().get(k).getProductCategory();
//							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", financialcalendar.getStartDate()).filter("contractDate <=", financialcalendar.getEndDate()).filter("status", "Approved").filter("companyId", companyId).list();
//							System.out.println("contract list size ===="+contractlist.size());
//							double netamount =0;
//							for(int p=0;p<contractlist.size();p++){
//								netamount +=contractlist.get(p).getNetpayable();
//							}
							
							double netamount =0;
							if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
								String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
								List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", financialcalendar.getStartDate()).filter("contractDate <=", financialcalendar.getEndDate()).filter("status", "Approved").filter("companyId", companyId).list();
								System.out.println("contract list size ===="+contractlist.size());
								
								for(int p=0;p<contractlist.size();p++){
									netamount +=contractlist.get(p).getNetpayable();
								}
							
						  }else{
								List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("contractDate >=", financialcalendar.getStartDate()).filter("contractDate <=", financialcalendar.getEndDate()).filter("status", "Approved").filter("companyId", companyId).list();
								System.out.println("contract list size ===="+contractlist.size());
								for(int p=0;p<contractlist.size();p++){
									netamount +=contractlist.get(p).getNetpayable();
								}
						  }
							
							TargetInformation targetinfo = new TargetInformation();
							targetinfo.setEmployeeName(employeeName);
							targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
							targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
							targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
							targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
							targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
							targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
							targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
							targetinfo.setAchievedTargetAmt(netamount);
							
							double incentiveAmt = 0;
							if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
								 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
								incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
							}
							targetinfo.setEmployeeIncentives(incentiveAmt);
							
							salespersonAchievementlist.add(targetinfo);
					}
					if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Half Yearly")){
						System.out.println(" inside half year");
//						String productCategory = targetlist.get(i).getTargetInfoList().get(k).getProductCategory();
						String month = targetlist.get(i).getTargetInfoList().get(k).getMonths();
						// here i am getting  half year starting month year and half year ending month year in string
						// so i am conveting into date
						String [] monthyear = month.split("To");
						
						System.out.println(" start motnh"+monthyear[0] );
						System.out.println(" end motnh"+monthyear[1] );

						
						Date halfyearstartdate = null;
						try {
							halfyearstartdate = format.parse(monthyear[0]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("Half year Start Date=="+halfyearstartdate);
						
						Date halfyearEnd = null;
						try {
							halfyearEnd = format.parse(monthyear[1]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					
						System.out.println("Half year Date=="+halfyearEnd);
						Date halfyearEndDate = DateUtility.getEndDateofMonth(halfyearEnd);
						System.out.println(" end Date of half year"+halfyearEndDate);
						
//						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
//						System.out.println("contract list size ===="+contractlist.size());
//						double netamount =0;
//						for(int p=0;p<contractlist.size();p++){
//							netamount +=contractlist.get(p).getNetpayable();
//						}
						
						double netamount =0;
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", halfyearEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						
						salespersonAchievementlist.add(targetinfo);
					}
					
					if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Quaterely")){
						

						String month = targetlist.get(i).getTargetInfoList().get(k).getMonths();
//						String productCategory = targetlist.get(i).getTargetInfoList().get(k).getProductCategory();

						
						String [] monthyear = month.split("To");
						
						System.out.println(" start motnh"+monthyear[0] );
						System.out.println(" end motnh"+monthyear[1] );

						
						Date quaterelystartdate = null;
						try {
							quaterelystartdate = format.parse(monthyear[0]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("quaterly Start Date=="+quaterelystartdate);
						
						Date quaterelyDate = null;
						try {
							quaterelyDate = format.parse(monthyear[1]);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
					
						System.out.println("Quaterely Date=="+quaterelyDate);
						Date quterlyEndDate = DateUtility.getEndDateofMonth(quaterelyDate);
						System.out.println(" end Date of Quaterely Date"+quterlyEndDate);
						
//						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", halfyearstartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
//						System.out.println("contract list size ===="+contractlist.size());
//						double netamount =0;
//						for(int p=0;p<contractlist.size();p++){
//							netamount +=contractlist.get(p).getNetpayable();
//						}
						
						double netamount =0;
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", quaterelystartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("contractDate >=", quaterelystartdate).filter("contractDate <=", quterlyEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						
						salespersonAchievementlist.add(targetinfo);
					}
					
					if(targetlist.get(i).getTargetInfoList().get(k).getDuration().equals("Monthly")){
						System.out.println(" in mothly");
//						String productCategory = targetlist.get(i).getTargetInfoList().get(k).getProductCategory();

						Date monthDate = null;
						try {
							monthDate = format.parse(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						System.out.println("Monthly Start Date=="+monthDate);
						Date monthlyStartDate=monthDate;
						System.out.println(" monthly first Date ===="+monthlyStartDate);
						
						Date monthEndDate = DateUtility.getEndDateofMonth(monthDate);
						System.out.println(" end Date of Month Date"+monthEndDate);
						
//						List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
//						System.out.println("contract list size ===="+contractlist.size());
//						double netamount =0;
//						for(int p=0;p<contractlist.size();p++){
//							netamount +=contractlist.get(p).getNetpayable();
//						}
						double netamount =0;
						if(!targetlist.get(i).getTargetInfoList().get(j).getProductCategory().equals("")){
							String productCategory = targetlist.get(i).getTargetInfoList().get(j).getProductCategory();
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("items.serviceProduct.productCategory", productCategory).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
						
					  }else{
							List<Contract> contractlist = ofy().load().type(Contract.class).filter("employee", employeeName).filter("contractDate >=", monthlyStartDate).filter("contractDate <=", monthEndDate).filter("status", "Approved").filter("companyId", companyId).list();
							System.out.println("contract list size ===="+contractlist.size());
							for(int p=0;p<contractlist.size();p++){
								netamount +=contractlist.get(p).getNetpayable();
							}
					  }
						
						TargetInformation targetinfo = new TargetInformation();
						targetinfo.setEmployeeName(employeeName);
						targetinfo.setProductCategory(targetlist.get(i).getTargetInfoList().get(j).getProductCategory());
						targetinfo.setFinancialYear(targetlist.get(i).getTargetInfoList().get(k).getFinancialYear());
						targetinfo.setDuration(targetlist.get(i).getTargetInfoList().get(k).getDuration());
						targetinfo.setMonths(targetlist.get(i).getTargetInfoList().get(k).getMonths());
						targetinfo.setTarget(targetlist.get(i).getTargetInfoList().get(k).getTarget());
						targetinfo.setPercatage(targetlist.get(i).getTargetInfoList().get(k).getPercatage());
						targetinfo.setFlatAmount(targetlist.get(i).getTargetInfoList().get(k).getFlatAmount());
						targetinfo.setAchievedTargetAmt(netamount);
						
						double incentiveAmt = 0;
						if(netamount>=targetlist.get(i).getTargetInfoList().get(j).getTarget()){
							 double incetiveperamt = targetlist.get(i).getTargetInfoList().get(j).getTarget() * targetlist.get(i).getTargetInfoList().get(j).getPercatage()/100;
							incentiveAmt = incetiveperamt+targetlist.get(i).getTargetInfoList().get(j).getFlatAmount();
						}
						targetinfo.setEmployeeIncentives(incentiveAmt);
						
						salespersonAchievementlist.add(targetinfo);
					}


				}
				
				}
			
		    }
		}
		
		return salespersonAchievementlist;
	}

}
