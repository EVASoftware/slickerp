package com.slicktechnologies.server.sessionmanagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.gargoylesoftware.htmlunit.javascript.host.Window;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.SessionService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.LoginServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.SessionManagement;
import com.slicktechnologies.shared.common.helperlayer.User;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SessionServiceImpl extends RemoteServiceServlet implements SessionService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1574881269479651452L;
	private static int timeout;
	public static ArrayList<SessionManagement> arrOfSession=new ArrayList<SessionManagement>();
	public static ArrayList<SessionManagement> cronSession=new ArrayList<SessionManagement>();
	static Logger cronLogger=Logger.getLogger("Cron Logger");
	Logger logger=Logger.getLogger("Session Logger");
//	long lastClickTime=getLastClick(null,0,false,0,null);
	Date lastClickDate;
	HttpSession session;
	HttpServletRequest request;
	
	
	@Override
	public String getUserSessionTimeout(String userName,  int sessionId, long companyId,String loggedInUser) {
		
		System.out.println("Time Out Calling");
		logger.log(Level.SEVERE,"--------Initiation for "+loggedInUser+"--------------------------");
		logger.log(Level.SEVERE,"Time Out Initiation On Login");
		request = this.getThreadLocalRequest();
		session = request.getSession(true);
		session.setAttribute("Username", userName);
		logger.log(Level.SEVERE,"session id server assigned id  "+	session.getId()+"-"+session.getAttribute(userName));
		timeout = getThreadLocalRequest().getSession().getMaxInactiveInterval() * 1000;
		logger.log(Level.SEVERE,"Time Out Value"+timeout);
		SessionManagement sessionData=new SessionManagement();
		sessionData.setUserName(userName.trim());
		sessionData.setSessionId(sessionId);
		sessionData.setLastAccessedTime(System.currentTimeMillis());
		sessionData.setCompanyId(companyId);
		sessionData.setSessionIdByServer(session.getId());
		sessionData.setEmployeeName(loggedInUser);
		arrOfSession.add(sessionData);
		
//		for(int n=0;n<arrOfSession.size();n++)
//		{
//			logger.log(Level.SEVERE,"Array Print"+arrOfSession.get(n).getUserName());
//			logger.log(Level.SEVERE,"Array Print"+arrOfSession.get(n).getSessionId());
//		}
		
//		System.out.println("SessionData"+arrOfSession.size());
		logger.log(Level.SEVERE,"Session Array"+arrOfSession.size());
		logger.log(Level.SEVERE,"-------------------------End Of Initiation--------------------------");
		
		return timeout+"-"+	session.getId();
		
	}
	
	@Override
	public Boolean isSessionAlive(String sessionIdbyserver , String user, int sessionCount, long companyId) {
		
//		System.out.println("Every 2 mintues"+sessionIdbyserver+lastClickTime);
//		System.out.println("Diff"+(System.currentTimeMillis() - lastClickTime));
//		logger.log(Level.SEVERE,"Session Alive On Server Side");
		boolean sessionFlag=true;
//		
//		if(arrOfSession.size()>0)
//		{
//			logger.log(Level.SEVERE,"Size Of Session =  "+arrOfSession.size());
//			System.out.println("Size > 0");
//			for(int h=0;h<arrOfSession.size();h++)
//			{
//				logger.log(Level.SEVERE,"Arr Company Id =  "+arrOfSession.get(h).getCompanyId()+"  =CID=  "+companyId);
//				logger.log(Level.SEVERE,"Arr Session Id by server"+arrOfSession.get(h).getSessionId()+"   == SID  "+sessionIdbyserver);
//				logger.log(Level.SEVERE,"Arr User Name"+arrOfSession.get(h).getUserName()+"   == UID  "+user);
//				System.out.println("Arr Company Id"+arrOfSession.get(h).getCompanyId()+"   == CID  "+companyId);
//				System.out.println("Arr Session Id"+arrOfSession.get(h).getSessionId()+"   == SID  "+sessionCount);
//				System.out.println("Arr User Name"+arrOfSession.get(h).getUserName()+"   == UID  "+user);
//
//				if(arrOfSession.get(h).getCompanyId()==companyId&&arrOfSession.get(h).getSessionId()==(sessionCount)&& arrOfSession.get(h).getUserName().trim().equals(user.trim()))
//				{
//					logger.log(Level.SEVERE,"Condition Passed");
//					logger.log(Level.SEVERE,"Last Click In loop"+arrOfSession.get(h).getLastAccessedTime());
//					logger.log(Level.SEVERE,"Current Time"+System.currentTimeMillis());
//					logger.log(Level.SEVERE,"Diff In Loop"+(System.currentTimeMillis()-arrOfSession.get(h).getLastAccessedTime()));
//					System.out.println("Condition Passed");
//					System.out.println("Diff In Loop"+(System.currentTimeMillis()-arrOfSession.get(h).getLastAccessedTime()));
//					
//					
//					
//					
//					if(System.currentTimeMillis()-arrOfSession.get(h).getLastAccessedTime()>timeout)
//					{
//						logger.log(Level.SEVERE,"Main If Else");
////						session.removeAttribute(user.trim());
//						
//						System.out.println("my attributes are removed "+session.getAttribute(user.trim()));
//						sessionFlag=false;
////						logoutOnServerSide(arrOfSession.get(h).getUserName(),arrOfSession.get(h).getCompanyId());
//					}
//					else if(!arrOfSession.get(h).getUserName().trim().equals(user))
//					{
//						logger.log(Level.SEVERE,"ELse If For Session NOt For That USer");
//						if(System.currentTimeMillis()-arrOfSession.get(h).getLastAccessedTime()>timeout)
//						{
//							session.invalidate();
//							logoutOnServerSide(arrOfSession.get(h).getUserName(),arrOfSession.get(h).getCompanyId());
//						}
//					}
////					else if(user.equals("CronCheck44")){
////						if(System.currentTimeMillis()-arrOfSession.get(h).getLastAccessedTime()>timeout)
////						{
////							session.invalidate();
////							logger.log(Level.SEVERE,"Cron Job Initiated");
////						
////							logoutOnServerSide(arrOfSession.get(h).getUserName(),arrOfSession.get(h).getCompanyId());
////						}
////					}
//					else{
//						return true;
//					}
//				}
//			}
//		}
		return sessionFlag;
	}
	
	
	
	@Override
	public Integer ping(String userName, int sessionId, boolean isPing,long companyId,String loggedInUser,String opration,int numberOfUser) {
		
		long cnt=0;
		logger.log(Level.SEVERE,"Server Side Ping with "+userName+"-"+sessionId+"-"+isPing+"-"+companyId+"-"+loggedInUser);
		logger.log(Level.SEVERE,"------opration Onclick Name---------"+opration); 
		logger.log(Level.SEVERE,"------Ping arrOfSession.size---------"+arrOfSession.size()); 
		logger.log(Level.SEVERE,"------number Of User---------"+numberOfUser); 
		
		boolean validUserFlag=false;
//		for(int x=0;x<arrOfSession.size();x++)
//		{
//			logger.log(Level.SEVERE,"Ping Received On ServerSide"+arrOfSession.get(x).getUserName()+"-"+arrOfSession.get(x).getSessionId()+"-"+arrOfSession.get(x).getCompanyId());
//			if(arrOfSession.get(x).getCompanyId()==companyId && arrOfSession.get(x).getSessionId()==sessionId && arrOfSession.get(x).getUserName().trim().equals(userName.trim()))
//			{
//				logger.log(Level.SEVERE,"Rohan in side condition");
//				validUserFlag=true;
		//   old code 
//				cnt = getLastClick(userName,sessionId,isPing,companyId,loggedInUser);
//				break;
				
//			}
//		}
		
				
		
		//  new code 
		
		List<LoggedIn> loginList = ofy().load().type(LoggedIn.class).filter("companyId", companyId).filter("status", "Active").list();  
		
		if(loginList.size() > numberOfUser){
			
			Comparator<LoggedIn> compare = new Comparator<LoggedIn>() {
				
				@Override
				public int compare(LoggedIn o1, LoggedIn o2) {
					Integer i1= o1.getCount();
					Integer i2= o2.getCount();
					
					return i1.compareTo(i2);
				}
			}; 
			Collections.sort(loginList, compare);
			
			for (int i = 0; i < loginList.size(); i++) {
				if(i+1 <= numberOfUser){
					
					if(userName.equals(loginList.get(i).getUserName()) && sessionId==loginList.get(i).getCount() 
							&&loggedInUser.equals(loginList.get(i).getEmployeeName())){
						
						cnt=0;
						validUserFlag=true;
						break;
					}
				}
				else
				{
					cnt= -1;
					validUserFlag=true;
					break;
				}
			}
			
			
		}
		else
		{
			for (int i = 0; i < loginList.size(); i++) {
				
				if(userName.equals(loginList.get(i).getUserName()) && sessionId==loginList.get(i).getCount() 
						&&loggedInUser.equals(loginList.get(i).getEmployeeName())){
					
					cnt=0;
					validUserFlag=true;
					break;
				}
			}		
		}
		logger.log(Level.SEVERE,"Counter Value"+cnt);
		
		
//		if(cnt == -2){
//			return -2;
//		}
//		else if(cnt == -1){
//			return -1;
//		}
//		else if(cnt > 0 )
//		{
//			return 1;
//		}
//		else{
//			return 0;
//		}
		if(!validUserFlag){
			return -20;
		}
		else{
		return (int) cnt;
		}
	}
	
	
	
	public long getLastClick(String userName,int sessionid,boolean isPing,long companyId,String loggedInUser)
	{
//		
		long cnt =0;
//		
////		boolean sessionFlag=false;
//		boolean forceFullyLogoutFlag=true;
//		
//		logger.log(Level.SEVERE,"Last Click due to ping"+lastClickTime+"- arrOfSession.size()"+arrOfSession.size());
//		logger.log(Level.SEVERE,"Number of users "+LoginServiceImpl.noOfUser);
//		System.out.println("Last Click"+lastClickTime);
//		System.out.println("Number of users "+LoginServiceImpl.noOfUser);
////		for(int j=0;j<arrOfSession.size();j++)
////		{
////			logger.log(Level.SEVERE,"---------------------Before------------------");
////			logger.log(Level.SEVERE,"Data===  "+arrOfSession.get(j).getLastAccessedTime());
////			logger.log(Level.SEVERE,"---------------------Before------------------");
////			System.out.println("---------------------Before------------------------");
////			System.out.println("Data"+arrOfSession.get(j).getLastAccessedTime());
////			System.out.println("---------------------Before------------------------");
////		}
//		
//		if(arrOfSession.size()>0)
//		{  
//			if(arrOfSession.size() > LoginServiceImpl.noOfUser)
//			{
//				Comparator<SessionManagement> compare = new Comparator<SessionManagement>() {
//					
//					@Override
//					public int compare(SessionManagement o1, SessionManagement o2) {
//						Integer i1= o1.getSessionId();
//						Integer i2=  o2.getSessionId();
//						
//						return i1.compareTo(i2);
//					}
//				}; 
//				Collections.sort(arrOfSession, compare);
//				
//				for (int i = 0; i < arrOfSession.size(); i++) {
//					
//					if(i+1 <= LoginServiceImpl.noOfUser){
//						
//						if(arrOfSession.get(i).getUserName().trim().equals(userName.trim()) && arrOfSession.get(i).getSessionId()==sessionid && isPing==true 
//								&& arrOfSession.get(i).getCompanyId()==companyId && arrOfSession.get(i).getEmployeeName().equalsIgnoreCase(loggedInUser))
//						{
////							if(lastClickTime - arrOfSession.get(i).getLastAccessedTime() < timeout)
////							{
////								arrOfSession.get(i).setLastAccessedTime(lastClickTime);
//								forceFullyLogoutFlag=false;
//								cnt=0;
//								break;
////								
////							}
////							else
////							{
//////								cnt = cnt+1;
////								forceFullyLogoutFlag=false;
////								sessionFlag = true;
////								break;
////							}
//						}
//						
//					}
//					else
//					{
//						forceFullyLogoutFlag=false;
//						cnt = -1;
//						break;
//					}
//				}
//				
//			}
//			else{
//			
////				lastClickTime=System.currentTimeMillis();
//				for(int k=0;k<arrOfSession.size();k++)
//				{
//					logger.log(Level.SEVERE,"Username ="+arrOfSession.get(k).getUserName()+"-"+userName.trim()+" Session Id="+arrOfSession.get(k).getSessionId()+"-"+sessionid +" Ping="+ isPing +" company id ="+arrOfSession.get(k).getCompanyId()+"-"+companyId);
//					if(arrOfSession.get(k).getUserName().trim().equals(userName.trim()) && arrOfSession.get(k).getSessionId()==sessionid && isPing==true 
//							&& arrOfSession.get(k).getCompanyId()==companyId && arrOfSession.get(k).getEmployeeName().equalsIgnoreCase(loggedInUser))
//					{
////						if(lastClickTime - arrOfSession.get(k).getLastAccessedTime() < timeout)
////						{
////							arrOfSession.get(k).setLastAccessedTime(lastClickTime);
//							cnt=0;
//							forceFullyLogoutFlag=false;
//							break;
////							
////						}
////						else
////						{
//////						cnt = cnt+1;
////							forceFullyLogoutFlag=false;
////							sessionFlag = true;
////							break;
////						}
//					}
//					else
//					{
////						cnt = cnt+1;
//					}
//				}
//			}
//		}
//		else
//		{
//			forceFullyLogoutFlag=false;
//			cnt = -2;
//		}
//		
////		for(int f=0;f<arrOfSession.size();f++)
////		{
////			System.out.println("--------------------------------After-----------------------");
////			System.out.println("Diff Oe"+arrOfSession.get(f).getLastAccessedTime());
////			System.out.println("--------------------------------After-----------------------");
////			logger.log(Level.SEVERE,"---------------------After------------------");
////			logger.log(Level.SEVERE,"Data===  "+arrOfSession.get(f).getLastAccessedTime());
////			logger.log(Level.SEVERE,"---------------------After------------------");
////		}
//		
//		logger.log(Level.SEVERE,"--------------------------------Ping Method Ends------------------------------");
//		
////		return cnt;
//		
//		
////		if(sessionFlag){
////			return -10;
////		}
////		else
//		if(forceFullyLogoutFlag){
//			return -20;
//		}
//		else{
			return cnt;
//		}
	}
	
	public static void logoutOnServerSide(String userId,long companyId)
	{
		cronLogger.log(Level.SEVERE,"LOGOUT ON SERVER SIDE AFTER CALLING");
		LoggedIn loggedin  = ofy().load().type(LoggedIn.class).filter("companyId", companyId).filter("userName", userId.trim()).filter("status", AppConstants.ACTIVE).first().now();

		if(loggedin!=null)
		{
			loggedin.setStatus(AppConstants.INACTIVE);
			loggedin.setLogoutDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			loggedin.setLogoutTime(DateUtility.getTimeWithTimeZone("IST", new Date()));
			loggedin.setRemark("Logged out on Session Timout at Server Side");
			ofy().save().entity(loggedin).now();
		
			for(int i=0;i<arrOfSession.size();i++)
			{
				if(arrOfSession.get(i).getUserName().trim().equals(userId.trim())&&arrOfSession.get(i).getCompanyId()==companyId)
				{
					arrOfSession.remove(i);
				}
			}
		}
	}
	
	public static void cronSessionHandling()
	{
//		cronSession=arrOfSession;
//		cronLogger.log(Level.SEVERE,"Size For Global Array"+arrOfSession.size());
//		for(int i=0;i<arrOfSession.size();i++)
//		{
//			if(System.currentTimeMillis()-arrOfSession.get(i).getLastAccessedTime()>timeout)
//			{
//				logoutOnServerSide(arrOfSession.get(i).getUserName().trim(), arrOfSession.get(i).getCompanyId());
//			}
//			if(System.currentTimeMillis()-arrOfSession.get(i).getLastAccessedTime()>timeout)
//			{
//				arrOfSession.remove(i);
//			}
//		}
	}



	@Override
	public Boolean getSessionArrayFromServer(User user) {
		
//		List<LoggedIn> loginlist = new ArrayList<LoggedIn>(); 
		LoggedIn login ;
		
		login = ofy().load().type(LoggedIn.class).filter("companyId", user.getCompanyId()).filter("employeeName",user.getEmployeeName()).filter("status","Active").first().now();
//		logger.log(Level.SEVERE,"login list size"+loginlist.size());
//		System.out.println("login list size"+loginlist.size());
		logger.log(Level.SEVERE,"arrOfSession size"+arrOfSession.size());
		
		int cnt=0;
//		if(loginlist.size()!=0){
		if(login!= null){
			if(arrOfSession.size()!=0) {
				for (int j = 0; j < arrOfSession.size() ; j ++) {
					if(login.getSessionId().equals(arrOfSession.get(j).getSessionId()) 
							&& login.getUserName().equalsIgnoreCase(arrOfSession.get(j).getUserName()))
								{	
							if(System.currentTimeMillis() - arrOfSession.get(j).getLastAccessedTime() > getThreadLocalRequest().getSession().getMaxInactiveInterval() * 1000)
							{
		//							LoggedIn login = new LoggedIn();
		//							login.setStatus("Inactive");
		//							login.setRemark("Forcefully logged out from server");
		//							ofy().save().entity(login);
								
									login.setStatus("Inactive");
									login.setRemark("Forcefully logged out from server");
									login.setLogoutDate(new Date());
									ofy().save().entity(login);
									
									
									arrOfSession.remove(j);
									
							}
							else
							{
									cnt = cnt+1;
							}
					}
					else
					{
						cnt = cnt+1;
					}
				}
			}
			else
			{
				login.setStatus("Inactive");
				login.setRemark("Forcefully logged out from server");
				login.setLogoutDate(new Date());
				ofy().save().entity(login);
			}
		}
		
		if(cnt > 0)
			return false;
		else
			return true;
		
	}

	@Override
	public Boolean getUserStatusInactiveAndRemoveSessionFromServer(ArrayList<LoggedIn> activeUsers,Long companyId) {
		
		logger.log(Level.SEVERE,"getUserStatusInactiveAndRemoveSessionFromServer"+arrOfSession.size());
		logger.log(Level.SEVERE,"get Active User size"+activeUsers.size());
		
		
//		for (int i = 0; i < activeUsers.size(); i++) {
//			int cnt=0;
//			if(arrOfSession.size() !=0)
//			{
//				for (int j = 0; j < arrOfSession.size(); j++) {
//					
//					logger.log(Level.SEVERE,"Session id-"+activeUsers.get(i).getSessionId()+"-"+arrOfSession.get(j).getSessionId()+"- User Name" +activeUsers.get(i).getUserName()+"-"
//					+arrOfSession.get(j).getUserName() +"- Employee Name"+activeUsers.get(i).getEmployeeName()+"-"+arrOfSession.get(j).getEmployeeName());
//				
//				
//					if(activeUsers.get(i).getCompanyId()== arrOfSession.get(j).getCompanyId()
//							&& activeUsers.get(i).getUserName().equalsIgnoreCase(arrOfSession.get(j).getUserName())
//							&& activeUsers.get(i).getEmployeeName().equalsIgnoreCase(arrOfSession.get(j).getEmployeeName()))
//						{	
//							
//							activeUsers.get(i).setStatus("Inactive");
//							activeUsers.get(i).setRemark("Forcefully logged out from server");
//							activeUsers.get(i).setLogoutDate(new Date());
//							ofy().save().entity(activeUsers.get(i));
//										
//							arrOfSession.remove(j);
//							logger.log(Level.SEVERE,"rohan inside if to remove user from array "+arrOfSession.size());
//							cnt=0;
//							break;
//							
//						}
//						else
//						{
//							cnt= cnt+1;
//						}
//					}
//				}
//				else
//				{
//					activeUsers.get(i).setStatus("Inactive");
//					activeUsers.get(i).setRemark("Forcefully logged out from server");
//					activeUsers.get(i).setLogoutDate(new Date());
//					ofy().save().entity(activeUsers.get(i));
//					
//				}
//				
//				if(cnt > 0)
//				{
//					activeUsers.get(i).setStatus("Inactive");
//					activeUsers.get(i).setRemark("Forcefully logged out from server");
//					activeUsers.get(i).setLogoutDate(new Date());
//					ofy().save().entity(activeUsers.get(i));
//					
//				}
//					
//			}
		
		
		//  new code 
		
		for (int i= 0; i < activeUsers.size(); i++) {
			activeUsers.get(i).setStatus("Inactive");
			activeUsers.get(i).setRemark("Forcefully logged out from server as new user login with same login credentials.");
			activeUsers.get(i).setLogoutDate(new Date());
			ofy().save().entity(activeUsers.get(i));
		}
		
		
		
//		if(arrOfSession.size()!=0) {
//			for (int j = 0; j < arrOfSession.size() ; j ++) {
//				logger.log(Level.SEVERE,"Session id-"+loginUser.getSessionId()+"-"+arrOfSession.get(j).getSessionId()+"- User Name" +loginUser.getUserName()+"-"+arrOfSession.get(j).getUserName());
//				if(loginUser.getSessionId().equals(arrOfSession.get(j).getSessionId())&& loginUser.getUserName().equalsIgnoreCase(arrOfSession.get(j).getUserName()))
//				{	
//					
//					loginUser.setStatus("Inactive");
//					loginUser.setRemark("Forcefully logged out from server");
//					loginUser.setLogoutDate(new Date());
//					ofy().save().entity(loginUser);
//								
//					arrOfSession.remove(j);
//					logger.log(Level.SEVERE,"rohan inside if to remove user from array "+arrOfSession.size());
//					return true;			
//				}
//				else
//				{
//					cnt= cnt+1;
//				}
//			}
//		}
//		else
//		{
//			loginUser.setStatus("Inactive");
//			loginUser.setRemark("Forcefully logged out from server");
//			loginUser.setLogoutDate(new Date());
//			ofy().save().entity(loginUser);
//			return true;
//		}
//		
//		if(cnt > 0)
//		{
//			loginUser.setStatus("Inactive");
//			loginUser.setRemark("Forcefully logged out from server");
//			loginUser.setLogoutDate(new Date());
//			ofy().save().entity(loginUser);
//			return true;
//		}
//		logger.log(Level.SEVERE,"getUserStatusInactiveAndRemoveSessionFromServer at End "+arrOfSession.size());
		
		return true;
	}

	
	
	
	@Override
	public void clearAllLoggedInUsers(ArrayList<LoggedIn> activeUsers, Long companyId) {
		
		System.out.println("in side server clearAllLoggedInUsers method");
		logger.log(Level.SEVERE,"clearAllLoggedInUsers");
		
		List<LoggedIn> loginlist = new ArrayList<LoggedIn>(); 
		
		loginlist = ofy().load().type(LoggedIn.class).filter("companyId", companyId).filter("status","Active").list();
		
		for (int i = 0; i < loginlist.size(); i++) {
			
			loginlist.get(i).setStatus("Inactive");
			loginlist.get(i).setRemark("Clear All Active user");
		}
		
		logger.log(Level.SEVERE,"clearAllLoggedInUsers  loginlist "+loginlist.size());
		ofy().save().entities(loginlist);
		
		logger.log(Level.SEVERE,"clearAllLoggedInUsers arrOfSession before delete"+arrOfSession.size());
		
		arrOfSession.clear();
		
		logger.log(Level.SEVERE,"clearAllLoggedInUsers arrOfSession after delete"+arrOfSession.size());
		
	}

	@Override
	public Boolean removeUserFromSessionArray(String userName,	String employeeName) {
		
		if(arrOfSession.size()!=0){
			
			for (int i = 0; i < arrOfSession.size(); i++) {
				
				if(arrOfSession.get(i).getUserName().equals(userName) 
						&& arrOfSession.get(i).getEmployeeName().equals(employeeName)){
					
					arrOfSession.remove(i);
				}
			}
		}
		return true;
	}

}
