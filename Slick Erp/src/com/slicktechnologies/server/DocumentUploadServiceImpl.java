package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.DocumentUploadService;
import com.slicktechnologies.client.views.datauploading.TransactionsUploadingForm;
import com.slicktechnologies.server.DataMigrationImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.SerialNumberMapping;
import com.slicktechnologies.shared.common.inventory.SerialNumberStockMaster;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.AssetMovementInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;

public class DocumentUploadServiceImpl extends RemoteServiceServlet implements DocumentUploadService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7624107156450842351L;
	Logger logger = Logger.getLogger("Name of logger");

	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	public static List<Branch> globalBranchlist = null;
	public static List<Employee> globalEmployeelist = null;
	public static List<ConfigCategory> globalConfig = null;
	public static List<City> globalCitylist = null;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	public static List<com.slicktechnologies.shared.common.helperlayer.Type> globalConfitype;
	public static List<ConfigCategory> globalConfigprcat;
	public static List<Config> globalConfiprgroup;
	public static List<ProductDetails> globalProductCode;
//	public static List<ItemProduct> globalProductCodenew;
	public static List<WareHouse> globalwarehouse;
	
	TransactionsUploadingForm form; 
	/****** Deepak Salve added for Excel Upload *****/
	static BlobKey blobkey;
//	public static BlobKey purchaseRequisitionUploadBlobkey;//Deepaks
	/**
	 * @author Vijay Chougule
	 * Des :- 18-10-2019 for NBHC CCPM Contract Upload
	 */
	
	@Override
	public ArrayList<String> readContractUploadExcelFile(String operationName,long companyId) {
		ArrayList<String> strvalidationlist = new ArrayList<String>();
		if(operationName.trim().equals("Contract Upload")){
//			if (DocumentUploadTaskQueue.contractUploadBlobKey == null) {
				ArrayList<String> exceldatalist = readExcelAndGetArraylist(DocumentUploadTaskQueue.contractUploadBlobKey);
				if (exceldatalist == null || exceldatalist.size() == 0) {
					strvalidationlist.add("Unable read excel data please try again!");
					return strvalidationlist;
				}
				if (exceldatalist.get(0).trim().equalsIgnoreCase("Customer Id")
						&& exceldatalist.get(1).trim().equalsIgnoreCase("Customer Name")
						&& exceldatalist.get(2).trim().equalsIgnoreCase("Reference No 1")//old label Client Code
						&& exceldatalist.get(3).trim().equalsIgnoreCase("Branch")
						&& exceldatalist.get(4).trim().equalsIgnoreCase("Sales Person")
						&& exceldatalist.get(5).trim().equalsIgnoreCase("Payment Method")
						&& exceldatalist.get(6).trim().equalsIgnoreCase("Approver Name")
						
						&& exceldatalist.get(7).trim().equalsIgnoreCase("Number Range") 
						&& exceldatalist.get(8).trim().equalsIgnoreCase("Payment Terms")
						
						&& exceldatalist.get(9).trim().equalsIgnoreCase("Contract Group")
						&& exceldatalist.get(10).trim().equalsIgnoreCase("Contract Category")
						&& exceldatalist.get(11).trim().equalsIgnoreCase("Contract Type")
						&& exceldatalist.get(12).trim().equalsIgnoreCase("Contract Date")
						&& exceldatalist.get(13).trim().equalsIgnoreCase("Start Date")
						&& exceldatalist.get(14).trim().equalsIgnoreCase("Contract Duration")
						
									
						&& exceldatalist.get(15).trim().equalsIgnoreCase("Is Rate Contract (Yes/No)")
						&& exceldatalist.get(16).trim().equalsIgnoreCase("Service Wise Billing (Yes/No)")
						&& exceldatalist.get(17).trim().equalsIgnoreCase("Consolidated (Yes/No)")
						&& exceldatalist.get(18).trim().equalsIgnoreCase("Contract Description")
						
						&& exceldatalist.get(19).trim().equalsIgnoreCase("Product Code")
						&& exceldatalist.get(20).trim().equalsIgnoreCase("Product Sr No")
						&& exceldatalist.get(21).trim().equalsIgnoreCase("No.Of. Services")
                 //Date : 20-05-2021 Added by Priyanka Req. Change Asset Qty to Qty By Rahul Tiwari.
						&& exceldatalist.get(22).trim().equalsIgnoreCase("Asset Qty")//Again changed label to Asset Qty Date:17-03-2023
						&& exceldatalist.get(23).trim().equalsIgnoreCase("Qty")//Changed from Unit to Qty as on contract screen it gets mapped to qty
						&& exceldatalist.get(24).trim().equalsIgnoreCase("UOM")
						&& exceldatalist.get(25).trim().equalsIgnoreCase("Price")
						&& exceldatalist.get(26).trim().equalsIgnoreCase("Customer Branch")
						
												
						&& exceldatalist.get(27).trim().equalsIgnoreCase("Branchwise Amount")
						&& exceldatalist.get(28).trim().equalsIgnoreCase("Tax 1")
						&& exceldatalist.get(29).trim().equalsIgnoreCase("Tax 2")
						&& exceldatalist.get(30).trim().equalsIgnoreCase("Premises")
						&& exceldatalist.get(31).trim().equalsIgnoreCase("Remark")
						&& exceldatalist.get(32).trim().equalsIgnoreCase("Model No.")
						&& exceldatalist.get(33).trim().equalsIgnoreCase("Serial No.")
						
						
						&& exceldatalist.get(34).trim().equalsIgnoreCase("Service Address Line 1")
						&& exceldatalist.get(35).trim().equalsIgnoreCase("Service Address Line 2")
						&& exceldatalist.get(36).trim().equalsIgnoreCase("City(Service Address)")
						) {
					
					int contractcount = exceldatalist.size()/37;//old 25
					if(contractcount>3000){
						strvalidationlist.add("Contract uploading excel sheet should not be more than 3000 records!");
						return strvalidationlist;
					}
					if(globalBranchlist==null){
						globalBranchlist = ofy().load().type(Branch.class).filter("companyId", companyId).list();
					}
					if(globalEmployeelist==null){
						globalEmployeelist = ofy().load().type(Employee.class).filter("companyId", companyId).list();
					}
					if(globalConfig==null){
						globalConfig = ofy().load().type(ConfigCategory.class).filter("companyId",companyId).filter("internalType", 25).list();
					}
					if(globalCitylist==null){
						globalCitylist = ofy().load().type(City.class).filter("companyId",companyId).list();
					}
					logger.log(Level.SEVERE, "Excel Read Successfully!");
					BlobKey blobkeyValue = DocumentUploadTaskQueue.contractUploadBlobKey;
					String strblobkey = blobkeyValue.getKeyString();
					logger.log(Level.SEVERE, "strblobkey"+strblobkey);
					
					DocumentUploadTaskQueue.contractUploadBlobKey = null;
					exceldatalist.add(strblobkey);
					
					return exceldatalist;
					
				} else {
					strvalidationlist.add("Invalid Excel Sheet");
					return strvalidationlist;
				}

//			} else {
//				strvalidationlist.add("Contract upload is in InProcess please try after few minutes!");
//				return strvalidationlist;
//			}
		}
		else{
			strvalidationlist.add("Invalid Operation");
			return strvalidationlist;
		}
	
	}

	/**
	 * Date 22-07-2019 Des :- Reading excel sheet and retuning aaraylist
	 * 
	 * @return
	 */
	public ArrayList<String> readExcelAndGetArraylist(BlobKey blobkey) {
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		ArrayList<String> exceldatalist = new ArrayList<String>();
		if(blobkey!=null)
		{
		try {
			logger.log(Level.SEVERE, "Reading excel ");
//			logger.log(Level.SEVERE, "Blobkey value" + blobkey);
//            if(blobkey==null){//Deepaks
//              form.showDialogMessage("Purchase Requisition Process in Under process kindly try after some Time");	
//            }
            logger.log(Level.SEVERE, "Blobkey value" + blobkey);
			long i;
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {

							// i = (long) cell.getNumericCellValue();
							// logger.log(Level.SEVERE, "Value : "+i);
							logger.log(Level.SEVERE, "Value : " + cell.getNumericCellValue());
							// exceldatalist.add(cell.getNumericCellValue() +
							// "");
							DecimalFormat decimalFormat = new DecimalFormat("#########.##");
							int value1 = 0, value2 = 0;
							String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
							if (array.length > 0) {
								// value1 = Integer.parseInt(array[0]);
							}
							if (array.length > 1) {
								value2 = Integer.parseInt(array[1]);
							}
							if (value2 == 0) {
								i = (long) cell.getNumericCellValue();
								exceldatalist.add(i + "");
							} else {
								exceldatalist.add(cell.getNumericCellValue() + "");
							}

						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:
					}
				}
			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist.size());

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	  }
		else{
		  try{
			  if(blobkey==null){
				   logger.log(Level.SEVERE, " Inside if Value is Null ");
				   exceldatalist.add("fail");
					 return exceldatalist;
			  }
		  }catch(Exception e){
			 exceldatalist.add("fail");
			 logger.log(Level.SEVERE, " Inside Exception Value is Null ");
			 return exceldatalist;
		  } 
	  }	
		return exceldatalist;
	}

	@Override
	public ArrayList<String> reactonValidateContractUpload(ArrayList<String> contractExcellist, long companyId) {
		logger.log(Level.SEVERE, "contractExcellist size"+contractExcellist.size());
		String blobkey = contractExcellist.get(contractExcellist.size()-1);
		logger.log(Level.SEVERE, "blobkey == "+blobkey);
		contractExcellist.remove(contractExcellist.size()-1);
		
		ArrayList<String> strvalidationlist = new ArrayList<String>();
		
		try {
		
		HashSet<String> hsprodCode=new HashSet<String>();
		HashSet<Integer> hsclientCode = new HashSet<Integer>();
		HashSet<String> hscustomerBranch = new HashSet<String>();

		if(contractExcellist.size()%37>0){
			strvalidationlist.add("Blank cells now allowed. Put \"NA\" instead of blank.");
			return strvalidationlist;
		}
		
		
		for(int i=37;i<contractExcellist.size();i+=37){//old 25
			hsclientCode.add(Integer.parseInt(contractExcellist.get(i)));
			hsprodCode.add(contractExcellist.get(i+19));//old 13
			hscustomerBranch.add(contractExcellist.get(i+26));//old 20
		}
		ArrayList<Integer> clientcodelist = new ArrayList<Integer>(hsclientCode);
		List<Customer> customerlist = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count IN", clientcodelist).list();
		
		ArrayList<String> productCodelist = new ArrayList<String>(hsprodCode);
		List<ServiceProduct> serviceproductlist = ofy().load().type(ServiceProduct.class)
											.filter("companyId", companyId).filter("productCode IN", productCodelist).list();
		
		ArrayList<String> strcustomerBrnach = new ArrayList<String>(hscustomerBranch);
		
		List<CustomerBranchDetails> globalCustomerBranchDetailsList = ofy().load().type(CustomerBranchDetails.class)
										.filter("companyId", companyId).filter("buisnessUnitName IN", strcustomerBrnach).list();
		logger.log(Level.SEVERE,"globalCustomerBranchDetailsList"+globalCustomerBranchDetailsList.size());
		
		ServerAppUtility serverapp = new ServerAppUtility();
		boolean complainServiceWithTurnAroundTime = serverapp.checkForProcessConfigurartionIsActiveOrNot("Contract", "COMPLAINSERVICEWITHTURNAROUNDTIME", companyId);
		
		ServerAppUtility serverutitlity = new ServerAppUtility();
		List<Config> contractgrouplist = serverutitlity.loadConfigsData(31, companyId); //contract group
		List<ConfigCategory> contractCategorylist = serverutitlity.loadConfigsCategoryData(4, companyId);		
		List<Type> contractCategoryTypelist = serverutitlity.loadCategoryTypeData(4, companyId);
		List<Config> numberRangeConfig = serverutitlity.loadConfigsData(91, companyId);
		List<TaxDetails> taxlist = ofy().load().type(TaxDetails.class).filter("taxChargeStatus", true).filter("companyId", companyId).list();
		List<Config> paymentMethodsList = serverutitlity.loadConfigsData(16, companyId);
		List<ConfigCategory> paymentTermslist = serverutitlity.loadConfigsCategoryData(25, companyId);		
		
		for(int i=37;i<contractExcellist.size();i+=37){//old 25
			System.out.println("i+37"+(i+37));
			if(contractExcellist.get(i).trim().equalsIgnoreCase("NA")){
				strvalidationlist.add("Customer Id code should not be NA!");
				return strvalidationlist;
			}
			if(!contractExcellist.get(i).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateNumericCloumn(contractExcellist.get(0).trim(), contractExcellist.get(i).trim());

				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			if(!contractExcellist.get(i).trim().equalsIgnoreCase("NA")){
				int customerId = Integer.parseInt(contractExcellist.get(i).trim());
				if(!validateCustomer(customerId,customerlist)){
					strvalidationlist.add("Customer does not exist in the system! Please check Customer Id!"+contractExcellist.get(i));
					return strvalidationlist;
				}
			}
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableContractUploadClientCodeMandatory", companyId)){
				if(contractExcellist.get(i+2).trim().equalsIgnoreCase("NA")){
					strvalidationlist.add("Client Code not be NA!");
					return strvalidationlist;
				}
			}
			
			if(contractExcellist.get(i+3).trim().equalsIgnoreCase("NA")){
				strvalidationlist.add("Branch should not be NA!");
				return strvalidationlist;
			}else{
				boolean branchflag = false;
				for(Branch branch : globalBranchlist){
					if(branch.getBusinessUnitName().trim().equals(contractExcellist.get(i+3).trim())){
						branchflag=true;
						break;
					}
				}
				if(branchflag==false){
					strvalidationlist.add("Branch does not exist in the system! Please check Branch! "+contractExcellist.get(i+3).trim());
					return strvalidationlist;
				}
			}
			if(!contractExcellist.get(i+4).trim().equalsIgnoreCase("NA")){			
				boolean employeeflag = false;
				for(Employee employee : globalEmployeelist){
					if(employee.getFullname().trim().equals(contractExcellist.get(i+4).trim())){
						employeeflag=true;
						break;
					}
				}
				if(employeeflag==false){
					strvalidationlist.add("Sales Person does not exist in the system! Please check Employee! "+contractExcellist.get(i+4).trim());
					return strvalidationlist;
				}
			}
			
			if(contractExcellist.get(i+5).trim().equalsIgnoreCase("NA")){
				strvalidationlist.add("Payment method should not be NA!");
				return strvalidationlist;
			}else{
				boolean flag = false;
				for(Config method : paymentMethodsList){
					if(method.getName().trim().equals(contractExcellist.get(i+5).trim())){
						flag=true;
						break;
					}
				}
				if(flag==false){
					strvalidationlist.add("Payment method "+contractExcellist.get(i+5).trim()+" does not exist in the system!");
					return strvalidationlist;
				}
			}
			
			if(contractExcellist.get(i+6).trim().equalsIgnoreCase("NA")){
				strvalidationlist.add("Approver Name should not be NA!");
				return strvalidationlist;
			}else{
				boolean employeeflag = false;
				for(Employee employee : globalEmployeelist){
					if(employee.getFullname().trim().equals(contractExcellist.get(i+6).trim())){
						employeeflag=true;
						break;
					}
				}
				if(employeeflag==false){
					strvalidationlist.add("Approver Name does not exist in the system! Please check Employee! "+contractExcellist.get(i+6).trim());
					return strvalidationlist;
				}
			}
			
			if(!contractExcellist.get(i+7).trim().equalsIgnoreCase("NA")){
				boolean flag = false;
				String error = serverutitlity.validateConfigsNames(numberRangeConfig, contractExcellist.get(i + 7).trim(), contractExcellist.get(7).trim());
				
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			if(!contractExcellist.get(i+8).trim().equalsIgnoreCase("NA")){
				boolean flag = false;
				String error = serverutitlity.validateCategoryName(paymentTermslist, contractExcellist.get(i + 8).trim(), contractExcellist.get(8).trim());
				
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
//			if(contractExcellist.get(i+9).trim().equalsIgnoreCase("NA")){//old 7
//				strvalidationlist.add("Contract Group should not be NA!");
//				return strvalidationlist;
//			}else 
			if (contractExcellist.get(i + 9).trim() != null && !contractExcellist.get(i + 9).trim().equalsIgnoreCase("NA")) {
				String error = serverutitlity.validateConfigsNames(contractgrouplist, contractExcellist.get(i + 9).trim(),contractExcellist.get(9).trim());
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
//			if(contractExcellist.get(i+10).trim().equalsIgnoreCase("NA")){//old 8
//				strvalidationlist.add("Contract Category should not be NA!");
//				return strvalidationlist;
//			}else 
			if (contractExcellist.get(i + 10).trim() != null && !contractExcellist.get(i + 10).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateCategoryName(contractCategorylist, contractExcellist.get(i + 10).trim(), contractExcellist.get(10).trim());
				
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
//			if(contractExcellist.get(i+11).trim().equalsIgnoreCase("NA")){//old 9
//				strvalidationlist.add("Contract Type should not be NA!");
//				return strvalidationlist;
//			}else 
			if (contractExcellist.get(i + 11).trim() != null && !contractExcellist.get(i + 11).trim().equalsIgnoreCase("NA")){
				String error =serverutitlity.validateCategoryTypeNames(contractCategoryTypelist, contractExcellist.get(i +10), contractExcellist.get(i + 11).trim(), contractExcellist.get(11).trim());
				
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			if(contractExcellist.get(i+12).trim().equalsIgnoreCase("NA")){//old 10
				strvalidationlist.add("Contract Date should not be NA!");
				return strvalidationlist;
			}
			else{
				try {
					Date date = dateFormat.parse(contractExcellist.get(i+12).trim());
				} catch (Exception e) {
					strvalidationlist.add("Contract date should be in 'dd/mm/yyyy' format only.");
					return strvalidationlist;
				}
			}
			if(contractExcellist.get(i+13).trim().equalsIgnoreCase("NA")){ //old 11
				strvalidationlist.add("Contract Start Date should not be NA!");
				return strvalidationlist;
			}
			else{
				try {
					Date date = dateFormat.parse(contractExcellist.get(i+13).trim());
				} catch (Exception e) {
					strvalidationlist.add("Contract start date should be in 'dd/mm/yyyy' format only.");
					return strvalidationlist;
				}
			}
			if(contractExcellist.get(i+14).trim().equalsIgnoreCase("NA")){ //old 12
				strvalidationlist.add("Contract Duration should not be NA!");
				return strvalidationlist;
			}
			if(contractExcellist.get(i+14).trim().equalsIgnoreCase("0")){
				strvalidationlist.add("Contract Duration should not be Zero!");
				return strvalidationlist;
			}
			
			
			if(!contractExcellist.get(i+15).trim().equalsIgnoreCase("YES")&&!contractExcellist.get(i+15).trim().equalsIgnoreCase("NO")){
				strvalidationlist.add("Only yes/no is allowed in Is Rate Contract (Yes/No) column");
				return strvalidationlist;
			}
			
			if(!contractExcellist.get(i+16).trim().equalsIgnoreCase("YES")&&!contractExcellist.get(i+16).trim().equalsIgnoreCase("NO")){
				strvalidationlist.add("Only yes/no is allowed in Service Wise Billing (Yes/No) column");
				return strvalidationlist;
			}
			
			if(!contractExcellist.get(i+17).trim().equalsIgnoreCase("YES")&&!contractExcellist.get(i+17).trim().equalsIgnoreCase("NO")){
				strvalidationlist.add("Only yes/no is allowed in Consolidated (Yes/No) column");
				return strvalidationlist;
			}
			
			if(contractExcellist.get(i+15).trim().equalsIgnoreCase("YES")&&contractExcellist.get(i+16).trim().equalsIgnoreCase("YES")){
				strvalidationlist.add("Contract type can either be Rate Contract or Service Wise Billing. Both cannot be set to yes.");
				return strvalidationlist;
			}
			
			if(contractExcellist.get(i+19).trim().equalsIgnoreCase("NA")){//old 13
				strvalidationlist.add("Product Code should not be NA!");
				return strvalidationlist;
			}
			else{
				SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId)
										.filter("productCode", contractExcellist.get(i+19).trim()).first().now();
				if(!validateServiceProduct(contractExcellist.get(i+19),serviceproductlist)){
					strvalidationlist.add("Product Code does not exist in the system! "+contractExcellist.get(i+19));
					return strvalidationlist;
				}
			}
			if(contractExcellist.get(i+20).trim().equalsIgnoreCase("NA")){//old 14
				strvalidationlist.add("Product Sr No should not be NA!");
				return strvalidationlist;
			}
			if(!contractExcellist.get(i+20).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateNumericCloumn(contractExcellist.get(20).trim(), contractExcellist.get(i+20).trim());

				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
			if(contractExcellist.get(i+21).trim().equalsIgnoreCase("NA")){//old 15
				strvalidationlist.add("No of services should not be NA!");
				return strvalidationlist;
			}
			if(!contractExcellist.get(i+21).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateNumericCloumn(contractExcellist.get(21).trim(), contractExcellist.get(i+21).trim());

				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
			//old 16
			if(complainServiceWithTurnAroundTime && contractExcellist.get(i+22).trim().equalsIgnoreCase("NA") || contractExcellist.get(i+22).trim().equalsIgnoreCase("0")){
				strvalidationlist.add("Asset Qty should not be NA or Zero!");
				return strvalidationlist;
			}
			
			if(!contractExcellist.get(i+23).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateNumericCloumn(contractExcellist.get(23).trim(), contractExcellist.get(i+23).trim());

				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
			if(contractExcellist.get(i+24).trim().equalsIgnoreCase("NA")){//old 18
				strvalidationlist.add("UOM should not be NA!");
				return strvalidationlist;
			}
			if(contractExcellist.get(i+25).trim().equalsIgnoreCase("NA")){//old 19
				strvalidationlist.add("Price should not be NA!");
				return strvalidationlist;
			}
			if(!contractExcellist.get(i+25).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateNumericCloumn(contractExcellist.get(25).trim(), contractExcellist.get(i+25).trim());

				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
			if(contractExcellist.get(i+26).trim().equalsIgnoreCase("NA")){//old 20
				strvalidationlist.add("Customer Branch should not be NA!");
				return strvalidationlist;
			}
			if(!validateCustomerBranch(contractExcellist.get(i+26),globalCustomerBranchDetailsList,contractExcellist.get(i))){
				/**
				 * @author Anil @since 06-04-2021
				 * If customer branch is not matching or exist in ERP then error was thrown of price column instead of 
				 * branch column
				 */
//				strvalidationlist.add("Customer Branch does not exist in the system please create first! "+contractExcellist.get(i+19));
				strvalidationlist.add("Customer Branch does not exist in the system please create first! "+contractExcellist.get(i+26));
				return strvalidationlist;
			}
			
			if(!contractExcellist.get(i+27).trim().equalsIgnoreCase("NA")){
				String error = serverutitlity.validateNumericCloumn(contractExcellist.get(27).trim(), contractExcellist.get(i+27).trim());

				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
			if(!contractExcellist.get(i+28).trim().equalsIgnoreCase("NA")){//old 9				
				String error =serverutitlity.validateTaxName(taxlist,contractExcellist.get(28).trim(), contractExcellist.get(i+28).trim());
				
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			if(!contractExcellist.get(i+29).trim().equalsIgnoreCase("NA")){//old 9				
				String error =serverutitlity.validateTaxName(taxlist,contractExcellist.get(29).trim(), contractExcellist.get(i+29).trim());
				
				if(!error.equals("")){
					strvalidationlist.add(error);
					return strvalidationlist;
				}
			}
			
			
			
			if(!contractExcellist.get(i+36).trim().equalsIgnoreCase("NA")){//old 23
				if(!validateCity(contractExcellist.get(i+36).trim())){
					strvalidationlist.add("City does not exist in the system! "+contractExcellist.get(i+36));
					return strvalidationlist;
				}
			}
			
			if(contractExcellist.get(i+8).trim().equalsIgnoreCase("NA")){//old 24
				strvalidationlist.add("Payment Terms should not be NA!");
				return strvalidationlist;
			}
			else{
				boolean paymentTermsFlag = false;
				for(ConfigCategory configcategory : globalConfig){
					if(configcategory.getCategoryName().equals(contractExcellist.get(i+8).trim())){
						paymentTermsFlag = true;
						break;
					}
				}
				if(paymentTermsFlag==false){
					strvalidationlist.add("Payment Terms does not exist in the system! "+contractExcellist.get(i+8));
					return strvalidationlist;
				}
			}
		
		}
		
		} catch (Exception e) {
			 e.getMessage();
			 strvalidationlist.add( e.getMessage());
				return strvalidationlist;
		}
		strvalidationlist.add("Success");
		strvalidationlist.add(blobkey);	
		return strvalidationlist;
		
//		try {
//			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
//			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", "Contract Upload").param("companyId", companyId+"").param("contractUploadblobkey", blobkey));
//			
//		} catch (Exception e) {
//			logger.log(Level.SEVERE,"Exception payment Date follow up update =="+e.getMessage());
//			strvalidationlist.add("Unbale to uplad data please try again.");
//			return strvalidationlist;
//		}
//		
//		strvalidationlist.add("Excel uploaded Sucessfully! Contract Upload Process Started!");
//		return strvalidationlist;
	
	}
	
	private boolean validateCity(String cityName) {
		for(City city : globalCitylist){
			if(city.getCityName().equals(cityName)){
				return true;
			}
		}
		return false;
	}

	private boolean validateCustomerBranch(String customerbranch, List<CustomerBranchDetails> globalCustomerBranchDetailsList, String customerid) {
		int customerId = Integer.parseInt(customerid);
		for(CustomerBranchDetails customerBranch : globalCustomerBranchDetailsList){
			if(customerBranch.getBusinessUnitName().trim().equals(customerbranch.trim()) &&
					customerBranch.getCinfo().getCount()==customerId){
				return true;
			}
		}
		return false;
	}

	private boolean validateServiceProduct(String productCode, List<ServiceProduct> serviceproductlist) {
		for(ServiceProduct product : serviceproductlist){
			if(productCode.equals(product.getProductCode())){
				return true;
			}
		}
		return false;
	}

	private boolean validateCustomer(int customerId, List<Customer> customerlist) {
		for(Customer customer : customerlist){
			if(customer.getCount()==customerId){
				return true;
			}
		}
		return false;
	}

	@Override
	public String reactonContractUpload(String blobkey, long companyId) {
		logger.log(Level.SEVERE, "Contract upload task queue calling");
		try {
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", "Contract Upload").param("companyId", companyId+"").param("contractUploadblobkey", blobkey));
		
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception =="+e.getMessage());
		}
		
		return "Contract Upload process started";
	}

	@Override
	public String sendApprovalRequest(Invoice inventity,String userName,int invoiceId) {
		logger.log(Level.SEVERE, "Approval Sending == invoiceId =="+invoiceId);
		
		try {
		Approvals approval = new Approvals();
		approval.setApproverName(inventity.getApproverName());
		approval.setBranchname(inventity.getBranch());
		approval.setBusinessprocessId(invoiceId);
		logger.log(Level.SEVERE, "Approval Sending 2");
		if(userName!=null){
		approval.setRequestedBy(userName);
		}
		logger.log(Level.SEVERE, "Approval Sending 3");
		approval.setPersonResponsible(inventity.getEmployee());
		if(inventity.getCreatedBy()!=null){
			approval.setDocumentCreatedBy(inventity.getCreatedBy());
		}
		logger.log(Level.SEVERE, "Approval Sending 4");
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype("Invoice Details");
		approval.setBpId(inventity.getPersonInfo().getCount()+"");
		approval.setBpName(inventity.getPersonInfo().getFullName());
		approval.setDocumentValidation(false);
		approval.setCompanyId(inventity.getCompanyId());
		logger.log(Level.SEVERE, "Approval Sending request");

		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(approval);
		logger.log(Level.SEVERE, "Auto Invoice Approval Sent successfully");
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
			return e.getMessage();
		}
		
		return "success";
	}

	/**
	 * Date 21-11-2019 by Vijay
	 * Des :- NBHC Inventory Management Lock seal GRN MMN
	 * Reading Excel File
	 */
	@Override
	public ArrayList<String> readexcelFile() {
		ArrayList<String> uploadexcelData = new ArrayList<String>();
		if(DataMigrationImpl.blobkey==null){
			uploadexcelData.add("Unable to read data please try again");
			return uploadexcelData;
		}
		ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);
		if (exceldatalist == null || exceldatalist.size() == 0) {
			uploadexcelData.add("Unable to read data please try again");
			return uploadexcelData;
		}
		
		return exceldatalist;
	}

	@Override
	public ArrayList<String> uploadGRN(long companyId,ArrayList<String> exceldatalist) {
		ArrayList<String> validationlist = new ArrayList<String>();
		if (exceldatalist.get(0).trim().equalsIgnoreCase("GRN Date")
			&& 	exceldatalist.get(1).trim().equalsIgnoreCase("Branch")
			&& exceldatalist.get(2).trim().equalsIgnoreCase("Approver Name")
			&& exceldatalist.get(3).trim().equalsIgnoreCase("Product Code")
			&& exceldatalist.get(4).trim().equalsIgnoreCase("Quantity")
			&& exceldatalist.get(5).trim().equalsIgnoreCase("StartSerialNumber")
			&& exceldatalist.get(6).trim().equalsIgnoreCase("EndSerialNumber")
			&& exceldatalist.get(7).trim().equalsIgnoreCase("Warehouse Name")
			&& exceldatalist.get(8).trim().equalsIgnoreCase("Storage Location")
			&& exceldatalist.get(9).trim().equalsIgnoreCase("Storage Bin")){
			return validationlist = reactonGRN(exceldatalist,companyId);
		}
		else{
			validationlist.add("Invalid Excel Sheet");
			return validationlist;
		}
				
	}

	private ArrayList<String> reactonGRN(ArrayList<String> exceldatalist, long companyId) {
		ArrayList<String> datalist = new ArrayList<String>();
		
		logger.log(Level.SEVERE, "GRN upload task queue calling");
		try {
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", "GRN").param("companyId", companyId+""));
		
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception =="+e.getMessage());
			datalist.add("Failed please try again");
			return datalist;
		}
		datalist.add("GRN Upload process started");
		return datalist;
	}

	@Override
	public String uploadMMN(long companyId, ArrayList<String> exceldatalist) {
		String strmsg = "";
		if (exceldatalist.get(0).trim().equalsIgnoreCase("MMN Date")
				&& 	exceldatalist.get(1).trim().equalsIgnoreCase("Branch")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Approver Name")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("Product Code")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("Quantity")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("StartSerialNumber")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("EndSerialNumber")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("TransferFrom Warehouse Name")
				&& exceldatalist.get(8).trim().equalsIgnoreCase("TransferFrom Storage Location")
				&& exceldatalist.get(9).trim().equalsIgnoreCase("TransferFrom Storage Bin")
				&& exceldatalist.get(10).trim().equalsIgnoreCase("TransferTo Warehouse Name")
				&& exceldatalist.get(11).trim().equalsIgnoreCase("TransferTo Storage Location")
				&& exceldatalist.get(12).trim().equalsIgnoreCase("TransferTo Storage Bin")){
				return strmsg = reactonMMN(exceldatalist,companyId);
			}
			else{
				return "Invalid Excel Sheet";
			}
	}

	private String reactonMMN(ArrayList<String> exceldatalist, long companyId) {
		logger.log(Level.SEVERE, "MMN upload task queue calling");
		try {
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName","MMN").param("companyId", companyId+""));
		
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception =="+e.getMessage());
			return "Failed please try again";
		}
		return "MMN Upload process started";

	}

	/**
	 * Date 27-11-2019
	 * Des :- NBHC Lock Seal IM Stock update directly
	 */
	@Override
	public String uploadLockSealStockUpdate(long companyId) {
			String msg = "";
		ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);
		if(exceldatalist.size()==0){
			return "Unable to read excel file Please try again!";
		}
		else{
			
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Product Code")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Quantity")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("StartSerialNumber")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("EndSerialNumber")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("TransferFrom Warehouse Name")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("TransferFrom Storage Location")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("TransferFrom Storage Bin")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("TransferTo Warehouse Name")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("TransferTo Storage Location")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("TransferTo Storage Bin")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("ISGRN") ){
					return msg = reactonLockSealStockUpdate(companyId);
				}
				else{
					return "Invalid Excel Sheet";
				}
			
			
		}
		

	}

	private String reactonLockSealStockUpdate(long companyId) {

		logger.log(Level.SEVERE, "Lock Seal Stock upload task queue calling");
		try {
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName","Lock Seal Stock Update").param("companyId", companyId+""));
		
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception =="+e.getMessage());
			return "Failed please try again";
		}
		return "Lock Seal Stock Upload process started";
	}

	@Override
	public String updateLockSealSerialNumberMaster(long companyId,String serialNumber) {
		SerialNumberStockMaster serialNumbermaster = ofy().load().type(SerialNumberStockMaster.class).filter("companyId", companyId)
												.filter("serialNumberRange", serialNumber).first().now();
		if(serialNumbermaster!=null){
			for(SerialNumberMapping srnumber : serialNumbermaster.getSerailNumberlist()){
				srnumber.setStatus(true);
			}
			serialNumbermaster.setStatus(true);
			ofy().save().entity(serialNumbermaster);
			logger.log(Level.SEVERE, "Serial Number Master Updated");
			return "Serial Number Master Updated";
		}
		return null;
	}

   	@Override
	public ArrayList<String> reactOnPurchaseRequisitionExcelRead(String entityName,Long companyId,String blobkeyURL) {
		// TODO Auto-generated method stub
		
		System.out.println(" Inside First Method "+entityName);
		
		ArrayList<String> strvalidationlist = new ArrayList<String>();
		
		if(entityName.trim().equalsIgnoreCase("Purchase Requisition Upload")){
			logger.log(Level.SEVERE," Excel Reading at 1st RPC blobkeyURL "+blobkeyURL);	
			BlobKey blobkeyObj=new BlobKey(blobkeyURL);
			ArrayList<String> exceldatalist = readExcelAndGetArraylist(blobkeyObj);
//			ArrayList<String> exceldatalist = readExcelAndGetArraylist(DocumentUploadTaskQueue.prequisitionUploadBlobKey);
				
			if(exceldatalist.size()!=0){
				logger.log(Level.SEVERE,"Excel List Size After Reading Excel"+exceldatalist.size());
			}
			
			if(exceldatalist.get(0).equals("fail")){
				strvalidationlist.add("PR upload is under process with another user please wait and try again");
				return strvalidationlist;
			}
			
			if (exceldatalist == null || exceldatalist.size() == 0) {
				strvalidationlist.add(" No Data Found in Excel Kindly Check ");
				return strvalidationlist;
			}
			
			if (exceldatalist.get(0).trim().equalsIgnoreCase("PR Title")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Expected Delivery Date")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("PR Group")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("PR Category")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("PR Type")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Branch")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Purchase Engineer")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Approver Name")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Description")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Description 2")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Product Code")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Price")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Quantity")
//					&& exceldatalist.get(13).trim().equalsIgnoreCase("Delivery Date")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Warehouse")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Header")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("Footer")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("Referance")) 
			{
				
				logger.log(Level.SEVERE, "Excel Read Successfully!");
				BlobKey blobkeyValue = DocumentUploadTaskQueue.prequisitionUploadBlobKey;
				String strblobkey=null;
//				try{
//				if(blobkeyValue!=null&&blobkeyValue.getKeyString()!=null){
//				 strblobkey = blobkeyValue.getKeyString();
//				}else{
//					strblobkey = blobkeyValue.getKeyString();
//				}
//				}catch(Exception e){
////					form.showDialogMessage(" Please try after some time ");
//					strvalidationlist.add("Please try after some time");
//				}
				strblobkey=blobkeyObj.getKeyString();
				logger.log(Level.SEVERE, "strblobkey"+strblobkey);
				logger.log(Level.SEVERE, " Excel Data List Size "+exceldatalist.size());
				DocumentUploadTaskQueue.prequisitionUploadBlobKey = null;
				exceldatalist.add(strblobkey);
				logger.log(Level.SEVERE, " Excel Data List Size "+exceldatalist.size());
				
				return exceldatalist;
				
			}else 
			{
				strvalidationlist.add(" Invalid Excel Kindly refer Excel Format ");
				return strvalidationlist;
			}
		  }else{
			  
			  strvalidationlist.add(" PR upload is under process with another user please wait and try again ");
			  return strvalidationlist;
		  }	
//		}
//		else{
//			System.out.println(" Invalid Transation Kindly Contact to EVA Software ");
//			strvalidationlist.add(" Invalid Transation Kindly Contact to EVA Software ");
//			return strvalidationlist;
//		}	
//		return strvalidationlist;
	}

   @Override
	public String reactOnPurchaseRequisitionExcelUpload(boolean masterPriceFlag,String blobkey,
			Long companyId) {
		String priceFlag="";
		// TODO Auto-generated method stub
		try {
			if(masterPriceFlag==true){
				priceFlag="true";
			}else{
				priceFlag="false";
			}
			System.out.println("Key String after Reading Data"+blobkey);
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", "Purchase Requisition Upload").param("companyId", companyId + "").param("prequisitionUploadBlobKey", blobkey).param("masterPriceFlag",priceFlag));
			logger.log(Level.SEVERE,"Blobkey Before Null Value "+blobkey);
			DocumentUploadTaskQueue.prequisitionUploadBlobKey=null;
//			blobkey=DocumentUploadTaskQueue.prequisitionUploadBlobKey;
			logger.log(Level.SEVERE,"Blobkey After Null Value "+blobkey);
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception payment Date follow up update =="+ e.getMessage());
			return "PR upload request uder process with another user kindly wait and try again later";
		}
		return " Purchase Requisition Upload Process Started ";
	}

   private String getPRGroup(List<Config> globalConfiprgroup2, String stringGroup) {
		// TODO Auto-generated method stub
		String group1="";
		for (Config group : globalConfiprgroup2) {
			if (group.getName().trim().equals(stringGroup)) {
				group1 = group.getName();
				return group1;
			}
		}
		return group1;
	}

	private void updateBlobKeyValueRequisition() {
		DocumentUploadTaskQueue.prequisitionUploadBlobKey = null;
	}
   
   private String getBranchName(List<Branch> globalBranchlist2, String stringBranch) {
		// TODO Auto-generated method stub
		String branchName="";
		logger.log(Level.SEVERE,"Branch Name into Method 1"+stringBranch);
		for (Branch branch : globalBranchlist2) {
			logger.log(Level.SEVERE,"Branch Name into Method 2 "+branch.getBusinessUnitName());
			if (branch.getBusinessUnitName().trim().equals(stringBranch)) {
				branchName = branch.getBusinessUnitName();
				logger.log(Level.SEVERE,"Branch Name into Method "+branchName);
				return branchName;
			}
		}
		logger.log(Level.SEVERE,"Branch Name into Method "+branchName);
		return branchName;
	}

	private String getPRType(List<Type> globalConfitype2, String stringType) {
		// TODO Auto-generated method stub
		String prtype="";
		for (com.slicktechnologies.shared.common.helperlayer.Type type : globalConfitype2) {
			if (type.getTypeName().trim().equals(stringType)) {
				prtype = type.getTypeName();
				return prtype;
			}
		}
		return prtype;
	}

	private String getPRCategory(List<ConfigCategory> globalConfigprcat2,
			String stringCategory) {
		// TODO Auto-generated method stub
		String category="";
		for (ConfigCategory employee : globalConfigprcat2) {
			if (employee.getCategoryName().trim().equals(stringCategory)) {
				category = employee.getCategoryName();
				return category;
			}
		}
		
		return category;
	}

   private String getWareHouseName(List<WareHouse> globalProductCode2,
			String trimWname) {
		// TODO Auto-generated method stub
		String wname="";
		logger.log(Level.SEVERE,"Warehouse Name inside Method 1"+trimWname);
		for (WareHouse warehouse : globalProductCode2) {
			logger.log(Level.SEVERE,"Warehouse Name inside Method 2"+warehouse.getBusinessUnitName());
			if (warehouse.getBusinessUnitName().trim().equals(trimWname)) {
				wname = warehouse.getBusinessUnitName();
				logger.log(Level.SEVERE,"Warehouse Name inside Method "+wname);
				return wname;
			}
		}
		logger.log(Level.SEVERE,"Warehouse Name inside Method "+wname);
		return wname;
	}
	private String getProduct_Code(List<SuperProduct> globalProductCode2,
			String trimNumber) {
		// TODO Auto-generated method stub
		String number="";
		logger.log(Level.SEVERE,"Product code inside method 1"+trimNumber);
		for (SuperProduct pcode : globalProductCode2) {
			logger.log(Level.SEVERE,"Product code inside method 2"+pcode.getProductCode());
			logger.log(Level.SEVERE,"Product Name inside method 2"+pcode.getProductName());
			if (pcode.getProductCode().trim().equalsIgnoreCase(trimNumber)) {
				number = pcode.getProductCode();
				logger.log(Level.SEVERE,"Product code inside method "+number);
				return number;
			}
		}
		System.out.println(" Product Code number  "+number);
		logger.log(Level.SEVERE,"Product code inside method after for "+number);
		return number;
	}
	private String getApprovalNameString(List<Employee> globalEmployeelist2,
			String approvalName) {
		// TODO Auto-generated method stub
		
		String name="";
		
		for (Employee employee : globalEmployeelist2) {
			if (employee.getFullname().trim().equals(approvalName)) {
				name = employee.getFullname();
				return name;
			}
		}
		
		return name;
	}

	private String getPurchaseEngg(List<Employee> globalEmployeelist2,
			String stringEngg) {
		// TODO Auto-generated method stub
		
		String engg="";
		
		for (Employee employee : globalEmployeelist2) {
			if (employee.getFullname().trim().equals(stringEngg)) {
				engg = employee.getFullname();
				return engg;
			}
		}
		return engg;
	}

    	@Override
	public ArrayList<String> reactOnPurchaseRequisitionExcelValidation(boolean masterPriceFlag,ArrayList<String> excelData, Long companyId) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		logger.log(Level.SEVERE," Inside PR Validation ");
		logger.log(Level.SEVERE,"Excel List Size Inside Validation "+excelData.size());
		System.out.println(" Excel Validation Start "+excelData.size());
		String blobkey = excelData.get(excelData.size()-1);
		System.out.println(" Blobkey 1 Value "+blobkey);
		logger.log(Level.SEVERE," Inside PR Validation Blobkey 1 Value "+blobkey);
		ArrayList<String> errorList=new ArrayList<String>();
		
		
		try {
			if (excelData.size() > 14000) {
				logger.log(Level.SEVERE," Inside PR Validation excelData.size() check "+excelData.size());
				errorList.add(" Purchase Requisition uploading excel sheet should not be more than 14000 records ");
				return errorList;
			}
		
			HashSet<String> product_code = new HashSet<String>();
			HashSet<String> warehouseName=new HashSet<String>();
//			// HashSet<Integer> hsclientCode = new HashSet<Integer>();
//           try{
			try{
			for (int i = 17; i < excelData.size(); i =i+17) {
				product_code.add(excelData.get(i + 10));
				logger.log(Level.SEVERE," Product Code Value at Validation Level "+excelData.get(i + 10));
				logger.log(Level.SEVERE," product_code "+product_code.size());
				warehouseName.add(excelData.get(i+13));				
				logger.log(Level.SEVERE," Warehouse Name Value at Validation Level "+excelData.get(i + 13));
				logger.log(Level.SEVERE," warehouseName "+warehouseName.size());
			}
			}catch(Exception e){
				logger.log(Level.SEVERE," Product Code and Warehouse Issue "+e);
			}
//			
			if(product_code.size()==0){
				errorList.add(" Product Code not Found ");
				return errorList;
			}
			if(warehouseName.size()==0){
				errorList.add(" Warehouse Not Found ");
				return errorList;
			}
			logger.log(Level.SEVERE," Product Code  size "+product_code.size());
			logger.log(Level.SEVERE," Warehouse Size "+warehouseName.size());
//			System.out.println(" Hashcode Values "+product_code.size());
			logger.log(Level.SEVERE," Hashcode Values "+product_code.size());
			ArrayList<String> productCode = new ArrayList<String>(product_code);
			ArrayList<String> wareHouseName=new ArrayList<String>(warehouseName);
			
			
			if (globalBranchlist == null) {
				
			 globalBranchlist = ofy().load().type(Branch.class).filter("companyId", companyId).list();
			 logger.log(Level.SEVERE," Inside PR Validation globalBranchlist check "+globalBranchlist.size());
			}
			if (globalEmployeelist == null) {
			 globalEmployeelist = ofy().load().type(Employee.class).filter("companyId", companyId).list();
			 logger.log(Level.SEVERE," Inside PR Validation globalEmployeelist check "+globalEmployeelist.size());
			}
			if (globalConfitype == null) {
			  globalConfitype = ofy().load().type(com.slicktechnologies.shared.common.helperlayer.Type.class).filter("companyId", companyId).filter("internalType", 17).list();
			  logger.log(Level.SEVERE," Inside PR Validation globalConfitype check "+globalConfitype.size());
			}
			if (globalConfigprcat == null) {
			  globalConfigprcat = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("internalType", 17).list();
			  logger.log(Level.SEVERE," Inside PR Validation globalConfigprcat check "+globalConfigprcat.size());
			}
			if (globalConfiprgroup == null) {
			globalConfiprgroup = ofy().load().type(Config.class).filter("companyId", companyId).filter("type", 55).list();
			logger.log(Level.SEVERE," Inside PR Validation globalConfiprgroup check "+globalConfiprgroup.size());
			}

			
			
			if(productCode.size()==0){
				errorList.add(" Product Code not Found ");
				return errorList;
			}
			if(wareHouseName.size()==0){
				errorList.add(" Warehouse Not Found ");
				return errorList;
			}
			
//			System.out.println(" Product Code List size "+productCode.size()+" Product code Value "+productCode.get(0));
			logger.log(Level.SEVERE," Product Code List size "+productCode.size()+" Product code Value "+productCode.get(0));
//			globalProductCodenew = new ArrayList<ItemProduct>();
			
			List<SuperProduct> globalProductCodenew =ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("productCode IN", productCode).list();	
			logger.log(Level.SEVERE," globalProductCodenew "+globalProductCodenew.size());
//			if(globalwarehouse==null){
			List<WareHouse>	globalwarehouse1=ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("buisnessUnitName IN", wareHouseName).list();
			logger.log(Level.SEVERE," globalwarehouse1 "+globalProductCodenew.size());
//			}
			
			logger.log(Level.SEVERE," Product Code List Size New "+globalProductCodenew.size());
			
			logger.log(Level.SEVERE," Product Code New List "+globalProductCodenew);
            
			try{
			for (int i = 17; i < excelData.size(); i =i+ 17) {
				logger.log(Level.SEVERE," Validation Start ");
				if (excelData.get(i).trim().equalsIgnoreCase("NA")) {
					System.out.println(" Error in Excel Data Reading ");
					updateBlobKeyValueRequisition();
					errorList.add(" PR Title should not be NA ");
					return errorList;
				}
				if (excelData.get(i + 1).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Expected Date should not be NA ");
					return errorList;
				}else{
					try{
//						System.out.println("excelData.get(i+1).trim() Value "+excelData.get(i+1).trim());
						Date date = dateFormat.parse(excelData.get(i+1).trim());  
						System.out.println(" Date format : "+date);
//					    System.out.println(" Invalid Date Format "+excelData.get(i+1)+" && Date "+date);  
					}catch(Exception e){
						int loc=i+1;
						System.out.println(" excelData.get(i+1).trim() Value "+excelData.get(i + 1));
						errorList.add(" Invalid Date Format : "+excelData.get(i + 1)+" Date should be dd/MM/yyyy format ");
						return errorList;
					};
				}
				
				if (excelData.get(i + 2).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add("PR Group should not be NA ");
					return errorList;
				} else {
					String group=getPRGroup(globalConfiprgroup,excelData.get(i + 2));
//					boolean prgroupflag = false;
//					for (Config group : globalConfiprgroup) {
//						if (group.getName().trim().equals(excelData.get(i + 2).trim())) {
//							prgroupflag = true;
//							break;
//						}
//					}
					if(group.equals("")){
						int loc=i+2;
						System.out.println(" PR Group Invalid : "+excelData.get(i + 2));
						errorList.add(" PR Group not found : "+excelData.get(i + 2));
						return errorList;
					}
				}

				if (excelData.get(i + 3).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" PR Category should not be NA ");
					return errorList;
				} else {
					String category=getPRCategory(globalConfigprcat,excelData.get(i + 3));
//					boolean prcatogoryflag = false;
//					for (ConfigCategory employee : globalConfigprcat) {
//						if (employee.getCategoryName().trim().equals(excelData.get(i + 3).trim())) {
//							prcatogoryflag = true;
//							break;
//						}
//					}
//					if(prcatogoryflag==false){
						int loc=i+3;
//						System.out.println("PR Category Invalid !!!"+excelData.get(i + 3)+" Excel Location "+loc);
//						errorList.add("PR Category Invalid !!!"+excelData.get(i + 3)+" Excel Location "+loc);
//						return errorList;
//					}
					if(category.equals("")){
						errorList.add(" PR Category not found : "+excelData.get(i + 3));
						return errorList;
					}
					
				}

				if (excelData.get(i + 4).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" PR Type should not be NA ");
					return errorList;
				} else {
					String prType=getPRType(globalConfitype,excelData.get(i+4));
//					boolean prtypeflag = false;
//					for (com.slicktechnologies.shared.common.helperlayer.Type type : globalConfitype) {
//						if (type.getTypeName().trim()
//								.equals(excelData.get(i + 4).trim())) {
//							prtypeflag = true;
//							break;
//						}
//					}
//					if(prtypeflag==false){
						int loc=i+4;
//						System.out.println("PR Type Invalid !!!"+excelData.get(i + 4)+" Excel Location "+loc);
//						errorList.add("PR Type Invalid !!!"+excelData.get(i + 4)+" Excel Location "+loc);
//						return errorList;
//					}
					
					if(prType.equals(""))
					{
						errorList.add(" PR Type not found : "+excelData.get(i + 4));
						return errorList;
					}
					
				}

				if (excelData.get(i + 5).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add("Branch should not be NA ");
					return errorList;
				} else {
					logger.log(Level.SEVERE,"Branch Name into Excel "+excelData.get(i+5));
					   String branch=getBranchName(globalBranchlist,excelData.get(i+5).trim());
//					boolean branchflag = false;
//					for (Branch branch : globalBranchlist) {
//						if (branch.getBusinessUnitName().trim().equals(excelData.get(i + 5).trim())) {
//							branchflag = true;
//							break;
//						}
//					}
//					if(!branchflag){
						int loc=i+5;
//						System.out.println("PR Branch Invalid !!!"+excelData.get(i + 5)+" Excel Location "+loc);
//						errorList.add("PR Branch Invalid !!!"+excelData.get(i + 5)+" Excel Location "+loc);
//						return errorList;
//					}
					if(branch.equals("")){
						errorList.add(" Branch not found : "+excelData.get(i + 5));
						return errorList;
					}	
						
				}

				if (excelData.get(i + 6).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Purchase Engineer should not be NA ");
					return errorList;
				} else {
					String engg=getPurchaseEngg(globalEmployeelist,excelData.get(i + 6).trim());
//					boolean employeeflag = false;
//					for (Employee employee : globalEmployeelist) {
//						if (employee.getFullname().trim().equals(excelData.get(i + 6).trim())) {
//							employeeflag = true;
//							break;
//						}
//					}
//					if(employeeflag==false){
						int loc=i+6;
//						System.out.println("Invalid Purchase Engineer !!!"+excelData.get(i + 6)+" Excel Location "+loc);
//						errorList.add("Invalid Purchase Engineer !!!"+excelData.get(i + 6)+" Excel Location "+loc);
//						return errorList;
//					}
					if(engg.equals("")){
						errorList.add(" Purchase Engineer not found : "+excelData.get(i + 6));
						return errorList;
					}	
				}

				if (excelData.get(i + 7).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Approval Name should not be NA ");
				} else {
					String approvalName=getApprovalNameString(globalEmployeelist,excelData.get(i + 7).trim());
//					boolean employeeflag = false;
//					for (Employee employee : globalEmployeelist) {
//						if (employee.getFullname().trim().equals(excelData.get(i + 7).trim())) {
//							employeeflag = true;
//							break;
//						}
//					}
//					if(employeeflag==false){
						int loc=i+7;
//						System.out.println("Invalid Approval Name !!!"+excelData.get(i + 7)+" Excel Location "+loc);
//						errorList.add("Invalid Approval Name !!!"+excelData.get(i + 7)+" Excel Location "+loc);
//						return errorList;
//					}
					if(approvalName.equals(""))
					{
						errorList.add(" Approval Name not found : "+excelData.get(i + 7));
						return errorList;
					}
				}
//				if (excelData.get(i + 8).trim().equalsIgnoreCase("NA")) {
//					updateBlobKeyValueRequisition();
//					errorList.add(" Description should not be NA ");
//					return errorList;
//				}
//				if (excelData.get(i + 9).trim().equalsIgnoreCase("NA")) {
//					updateBlobKeyValueRequisition();
//					errorList.add(" Description 2 should not be NA ");
//					return errorList;
//				}
				if (excelData.get(i + 10).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Product code should not be NA ");
					return errorList;
				} else {
					logger.log(Level.SEVERE,"Product code into Excel "+excelData.get(i + 10));
					String number=getProduct_Code(globalProductCodenew,excelData.get(i + 10).trim());
//					boolean productCodeflag = false;
//					for (ProductDetails pcode : globalProductCode) {
//						if (pcode.getProductCode().trim().equals(excelData.get(i + 10).trim())) {
//							productCodeflag = true;
//							break;
//						}
//					}
//					if(productCodeflag==false){
						int loc=i+10;
//						System.out.println("Invalid Product Code !!!"+excelData.get(i + 10)+" Excel Location "+loc);
//						errorList.add("Invalid Product Code !!!"+excelData.get(i + 10)+" Excel Location "+loc);
//						return errorList;
//					}
					if(number.equals("")){
						errorList.add(" Product Code not Found : "+excelData.get(i + 10));
						return errorList;
					}	
				}
				
				
				/***Deepak Added this code***/
				
				
				if (excelData.get(i + 11).trim().equalsIgnoreCase("NA")&&(masterPriceFlag==false)) {
					updateBlobKeyValueRequisition();
					errorList.add(" Product Price should not be NA ");
					return errorList;
				}else{
					if(!excelData.get(i + 11).trim().equalsIgnoreCase("NA")){
					if(excelData.get(i + 11).trim().equalsIgnoreCase("NA")&&(masterPriceFlag==false)){
						errorList.add(" Product Price should not be NA ");
					}else{
					try{
						int no=Integer.parseInt(excelData.get(i + 11).trim());
						if(no<0){
							errorList.add(" Number should not be Negative : "+excelData.get(i + 11));	
						}
//						System.out.println(" Number Format "+excelData.get(i + 11).trim());
					}catch(Exception e){
						int loc=i+11;
						errorList.add("Invalid Number Format : "+excelData.get(i + 11));
						return errorList;
					}	
				}
			  }		
			}	
//				else{
//					
//					try{
//						int no=Integer.parseInt(excelData.get(i + 11).trim());
//						if(no<0){
//							errorList.add(" Number should not be Negative : "+excelData.get(i + 11));	
//						}
////						System.out.println(" Number Format "+excelData.get(i + 11).trim());
//					}catch(Exception e){
//						int loc=i+11;
//						errorList.add("Invalid Number Format : "+excelData.get(i + 11));
//						return errorList;
//					}
//				}
				
				
				
				/***End***/
				
				
				
//				if (excelData.get(i + 11).trim().equalsIgnoreCase("NA")) {
//					updateBlobKeyValueRequisition();
//					errorList.add(" Product Price should not be NA ");
//					return errorList;
//				}else{
//					
//					try{
//						int no=Integer.parseInt(excelData.get(i + 11).trim());
//						if(no<0){
//							errorList.add(" Number should not be Negative : "+excelData.get(i + 11));	
//						}
////						System.out.println(" Number Format "+excelData.get(i + 11).trim());
//					}catch(Exception e){
//						int loc=i+11;
//						errorList.add("Invalid Number Format : "+excelData.get(i + 11));
//						return errorList;
//					}
//				}
				
				if (excelData.get(i + 12).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Product Quality should not be NA ");
					return errorList;
				}else{
					try{
						int no=Integer.parseInt(excelData.get(i + 12).trim());
						if(no<0){
							errorList.add(" Number should not be Negative : "+excelData.get(i + 12));	
						}
//						System.out.println(" Number Format "+excelData.get(i + 12).trim());
					}catch(Exception e){
						int loc=i+12;
						errorList.add("Invalid Number Format : "+excelData.get(i + 12));
						return errorList;
					}
				}
				
				
//				if (excelData.get(i + 13).trim().equalsIgnoreCase("NA")) {
//					updateBlobKeyValueRequisition();
//					errorList.add(" Delivary Date should not be NA ");
//					return errorList;
//				}else{
//				  try{  
//					Date date = dateFormat.parse(excelData.get(i+13).trim());  
////				    System.out.println(" Invalid Date Format "+excelData.get(i + 13)+" && Date "+date);
//				  }catch(Exception e){
//					int loc=i+13;
//					errorList.add(" Invalid Date Format : "+excelData.get(i + 13)+" Date should be dd/MM/yyyy format ");
//					return errorList;
//				  };
//				}	
				if (excelData.get(i + 13).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Warehouse Name should not be NA ");
					return errorList;
				} else {
					logger.log(Level.SEVERE,"Warehouse Name in Excel "+excelData.get(i + 13));
					String warehouseName1=getWareHouseName(globalwarehouse1,excelData.get(i + 13).trim());
//					boolean warehouseflag = false;
//					for (ProductDetails warehouse : globalProductCode) {
//						if (warehouse.getWarehouseName().trim().equals(excelData.get(i + 14).trim())) {
//							warehouseflag = true;
//							break;
//						}
//					}
//					if(warehouseflag==false){
						
//						System.out.println("Invalid Product Code !!!"+excelData.get(i + 14)+" Excel Location "+loc);
//						errorList.add("Invalid Product Code !!!"+excelData.get(i + 14)+" Excel Location "+loc);
//						return errorList;
//					}
					if(warehouseName1.equals("")){
						errorList.add(" Warehouse not found : "+excelData.get(i + 13));
						return errorList;
					}	
				}
//				if (excelData.get(i + 15).trim().equalsIgnoreCase("NA")) {
//					updateBlobKeyValueRequisition();
//					errorList.add(" Header should not be NA ");
//					return errorList;
//				}
//				if (excelData.get(i + 16).trim().equalsIgnoreCase("NA")) {
//					updateBlobKeyValueRequisition();
//					errorList.add(" Footer should not be NA ");
//					return errorList;
//				}
				if (excelData.get(i + 16).trim().equalsIgnoreCase("NA")) {
					updateBlobKeyValueRequisition();
					errorList.add(" Refrance Value should not be NA ");
					return errorList;
				}
				logger.log(Level.SEVERE," Validation End ");
			}
		}catch(Exception e){
			logger.log(Level.SEVERE," Excel Reading out of Bound Index "+e);
		}
//			System.out.println(" Excel Data Read UnCorrectly ");
			logger.log(Level.SEVERE,"  Excel Data Read UnCorrectly  ");
		}
		catch (Exception e) {
//			errorList.add(" Invalid Transation Please Contact to EVA Software Solutions"+" Error List Size "+errorList.size());
			errorList.add("Product Code and Warehouse not Found");
			
//			logger.log(Level.SEVERE,"  ErrorList Size  "+errorList.size());
//			System.out.println(" ErrorList Size "+errorList.size());
			e.getMessage();
			return errorList;
		}
	//	System.out.println(" Error List Size "+errorList.size());
		logger.log(Level.SEVERE,"  Error List Size  "+errorList.size());
		if(errorList!=null&&errorList.size()!=0){
		 return errorList;
		}else{
			logger.log(Level.SEVERE,"  Return value print Here  "+errorList);
			 System.out.println(" Return value print Here"+errorList);	
			 errorList.add("Success");
			 errorList.add(blobkey);
			 logger.log(Level.SEVERE,"  Return value value Here  "+errorList);;
			 return errorList;
		}
//		return errorList;
	}

    	/**
    	 * @author Vijay Chougule Date 10-07-2020
    	 * Des :- To Update inhouse fumigation services properly in fumigation report and if any services missing it also create the same
    	 */
    	
	@Override
	public String updateInhouseFumigationServices(long companyId) {

		String msg = "";
		ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);
		if (exceldatalist.size() == 0) {
			return "Unable to read excel file Please try again!";
		} else {
			if(exceldatalist.size()>1000){
				return "Excel sheet should not be more than 1000 records";
			}
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Service Id")) {
				return msg = reactonInhouseFumigationServiceUpdate(companyId);
			} else {
				return "Invalid Excel Sheet";
			}

		}

	}

		private String reactonInhouseFumigationServiceUpdate(long companyId) {

			logger.log(Level.SEVERE, "Inhouse services updation task queue calling");
			try {
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName","Update Fumigation Services").param("companyId", companyId+""));
			
			} catch (Exception e) {
				logger.log(Level.SEVERE,"Exception =="+e.getMessage());
				return "Failed please try again";
			}
			return "In-House Services Upload process started";
		}

		/**
	 	 * @author Vijay Date :- 13-10-2020
	 	 * Des :- Payment Upload
	 	 */
		@Override
		public ArrayList<CustomerPayment> validatePaymentUploader(long companyId) {

			ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);

			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size"+ exceldatalist.size());
			
			
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Invoice ID")
					&& exceldatalist.get(1).trim()
							.equalsIgnoreCase("Payment ID") //Removed Payment Date column as due date is already set and received date is going to be set from received date field
 					&& exceldatalist.get(2).trim()
							.equalsIgnoreCase("Payment Mode") //Added by Ashwini Patil Date:27-06-2023 for orion
					&& exceldatalist.get(3).trim()
							.equalsIgnoreCase("Payment Method")
					&& exceldatalist.get(4).trim()
							.equalsIgnoreCase("Net Receivable")
					&& exceldatalist.get(5).trim()
							.equalsIgnoreCase("Is TDS Applicable")
					&& exceldatalist.get(6).trim()
							.equalsIgnoreCase("TDS Percentage")
					&& exceldatalist.get(7).trim()
							.equalsIgnoreCase("TDS Amount") 
					&& exceldatalist.get(8).trim()
							.equalsIgnoreCase("Cheque No")
					&& exceldatalist.get(9).trim()
							.equalsIgnoreCase("Cheque Date")
					&& exceldatalist.get(10).trim()
							.equalsIgnoreCase("Cheque-Bank Name")
					&& exceldatalist.get(11).trim()
							.equalsIgnoreCase("Cheque-Bank Account No.")
					&& exceldatalist.get(12).trim()
							.equalsIgnoreCase("Reference No.")
					&& exceldatalist.get(13).trim()
							.equalsIgnoreCase("Payment Received Date"))	//old name NEFT-Transfer Date	
							
				{
				ArrayList<CustomerPayment> list = paymentUpdation(companyId, exceldatalist);
				return list;
			} else {
				return null;
			}	
			
		}

		private ArrayList<CustomerPayment> paymentUpdation(long companyId,ArrayList<String> exceldatalist) {
			
			ArrayList<CustomerPayment> customerpaymentlist = new ArrayList<CustomerPayment>();
			ArrayList<String> errorList = new ArrayList<String>();

			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			
			HashSet<String> hsInvoiceIdPaymentId = new HashSet<String>();
			HashSet<Integer> hsinvoiceId = new HashSet<Integer>();
			HashSet<Integer> hspaymentID = new HashSet<Integer>();
			
			ServerAppUtility serverapputility = new ServerAppUtility();
			
			List<CompanyPayment> companyPayments=ofy().load().type(CompanyPayment.class).filter("companyId", companyId).list();
			ArrayList<String> paymentModeList=new ArrayList<String>();
			if(companyPayments!=null&&companyPayments.size()>0){
				for(CompanyPayment cp:companyPayments){
					String mode=cp.getCount()+" / "+cp.getPaymentBankName();
					paymentModeList.add(mode);
					System.out.println("paymentmode "+mode+"added");
				}
			}

			if(exceldatalist.size()%14!=0) { //Ashwini Patil Date:26-05-2023
				errorList.add("File");
				errorList.add("Blank cells were found! Put NA instead of blank for the non-mandatory field and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.");
			}
			for(int i=14; i<exceldatalist.size();i+=14){
				String error="";
				error += serverapputility.validateNumericCloumn(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(1).trim(), exceldatalist.get(i+1).trim());
				if(!error.equals("") && !error.equals(" ")){
					errorList.add(exceldatalist.get(i+1));
					errorList.add(error);
				}
				try {
					hsInvoiceIdPaymentId.add(exceldatalist.get(i)+"-"+exceldatalist.get(i+1));
					hsinvoiceId.add(Integer.parseInt(exceldatalist.get(i).trim()));
					hspaymentID.add(Integer.parseInt(exceldatalist.get(i+1).trim()));
					
				} catch (Exception e) {
				}
				
			}
			
			for(String invoiceId : hsInvoiceIdPaymentId){
				int invoiceidCount = 0;
				for(int i=14; i<exceldatalist.size();i+=14){
					if(invoiceId.equals(exceldatalist.get(i).trim()+"-"+exceldatalist.get(i+1).trim())){
						invoiceidCount++;
					}
				}
				if(invoiceidCount>1){
					String[] invoiceid = invoiceId.split("-");
					errorList.add(invoiceid[1]+"");
					errorList.add("Duplicate Invoice id and Payment Id found in the excel.");
				}
			}
			
			if(errorList.size()>0) {
				CustomerPayment customerPayment = new CustomerPayment();
				customerPayment.setCount(-1);
				customerpaymentlist.add(customerPayment);
			}
			if(errorList!=null && errorList.size()>0){
				 CsvWriter.paymentuploaderErrorlist = errorList;
				 return customerpaymentlist;
			}
			
			ArrayList<Integer> arryinvoiceId = new ArrayList<Integer>(hsinvoiceId);
			ArrayList<Integer> arryPaymentId = new ArrayList<Integer>(hspaymentID);
			logger.log(Level.SEVERE, "arryinvoiceId size="+arryinvoiceId.size());
			logger.log(Level.SEVERE, "arryPaymentId size="+arryPaymentId.size());
			
			List<Invoice> invoicelist =null;
			List<CustomerPayment> paymentdetailslistByInvoiceId=null;
			if(arryinvoiceId.size()>0){
				invoicelist=ofy().load().type(Invoice.class).filter("companyId", companyId)
					.filter("count IN", arryinvoiceId).list();
			
				paymentdetailslistByInvoiceId= ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
					.filter("invoiceCount IN", arryinvoiceId).filter("status", "Created").list();
			}
			
			List<CustomerPayment> paymentdetailslist =null;
			
			if(arryPaymentId.size()>0){
				paymentdetailslist= ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
					.filter("count IN", arryPaymentId).list();
			
			}
			HashMap<Integer, CustomerPayment> invoicePaymentMap=new HashMap<Integer, CustomerPayment>();
			HashMap<Integer, Integer> invoicePaymentCount=new HashMap<Integer, Integer>();
			Map<Integer, List<CustomerPayment>> invoiceWisePaymentListMap =null;
			if(paymentdetailslistByInvoiceId!=null){
				logger.log(Level.SEVERE, "paymentdetailslistByInvoiceId size="+paymentdetailslistByInvoiceId.size());
				
				for(CustomerPayment cp:paymentdetailslistByInvoiceId){
					invoicePaymentMap.put(cp.getInvoiceCount(), cp);
					if(invoicePaymentCount.containsKey(cp.getInvoiceCount())){
						int count=invoicePaymentCount.get(cp.getInvoiceCount());
						invoicePaymentCount.put(cp.getInvoiceCount(), count+1);
					}else
						invoicePaymentCount.put(cp.getInvoiceCount(), 1);
					
				//	logger.log(Level.SEVERE, cp.getInvoiceCount()+" added payment to map pay id="+cp.getCount());
				}
				logger.log(Level.SEVERE, "invoicePaymentMap size= "+invoicePaymentMap.size());
				invoiceWisePaymentListMap = paymentdetailslistByInvoiceId.stream().collect(Collectors.groupingBy(CustomerPayment::getInvoiceCount,Collectors.toList()));

			}
			
			
			
			
			for(int i=14; i<exceldatalist.size();i+=14){
				String error = "";
				CustomerPayment customerPayment = new CustomerPayment();

				try {

				
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(1).trim(), exceldatalist.get(i+1).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(2).trim(), exceldatalist.get(i+2).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(3).trim(), exceldatalist.get(i+3).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(4).trim(), exceldatalist.get(i+4).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(5).trim(), exceldatalist.get(i+5).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(6).trim(), exceldatalist.get(i+6).trim());
//				error += serverapputility.validateSpecialCharComma(exceldatalist.get(7).trim(), exceldatalist.get(i+7).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(8).trim(), exceldatalist.get(i+8).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(9).trim(), exceldatalist.get(i+9).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(10).trim(), exceldatalist.get(i+10).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(11).trim(), exceldatalist.get(i+11).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(12).trim(), exceldatalist.get(i+12).trim());
				error += serverapputility.validateSpecialCharComma(exceldatalist.get(13).trim(), exceldatalist.get(i+13).trim());
				
				error += serverapputility.validateNumericCloumn(exceldatalist.get(4).trim(), exceldatalist.get(i+4).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(6).trim(), exceldatalist.get(i+6).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(7).trim(), exceldatalist.get(i+7).trim());
				error += serverapputility.validateNumericCloumn(exceldatalist.get(11).trim(), exceldatalist.get(i+11).trim());

				error += serverapputility.validateStringCloumn(exceldatalist.get(3).trim(), exceldatalist.get(i+3).trim());
				error += serverapputility.validateStringCloumn(exceldatalist.get(10).trim(), exceldatalist.get(i+10).trim());

				error += serverapputility.validateFormulla(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(1).trim(), exceldatalist.get(i+1).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(2).trim(), exceldatalist.get(i+2).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(3).trim(), exceldatalist.get(i+3).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(4).trim(), exceldatalist.get(i+4).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(5).trim(), exceldatalist.get(i+5).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(6).trim(), exceldatalist.get(i+6).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(7).trim(), exceldatalist.get(i+7).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(8).trim(), exceldatalist.get(i+8).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(9).trim(), exceldatalist.get(i+9).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(10).trim(), exceldatalist.get(i+10).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(11).trim(), exceldatalist.get(i+11).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(12).trim(), exceldatalist.get(i+12).trim());
				error += serverapputility.validateFormulla(exceldatalist.get(13).trim(), exceldatalist.get(i+13).trim());

				error += serverapputility.validateDate(exceldatalist.get(i+9).trim(), "MM/dd/yyyy", exceldatalist.get(9).trim());
				error += serverapputility.validateDate(exceldatalist.get(i+13).trim(), "MM/dd/yyyy", exceldatalist.get(13).trim());
			
				if(exceldatalist.get(i).trim().equalsIgnoreCase("NA") || exceldatalist.get(i).trim().equalsIgnoreCase("N A"))
					error += " Invoice Id is mandatory.";
				else{	
					boolean invoiceIdFlag = validateInvoiceId(invoicelist,Integer.parseInt(exceldatalist.get(i).trim()));
					if(invoiceIdFlag == false){
						error += " Invoice Id does not exist in the system.";
					}
					
				}
				
				if(paymentdetailslist!=null||invoicePaymentMap.size()>0){
				if(!exceldatalist.get(i+1).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+1).trim().equalsIgnoreCase("N A")){
					boolean paymentFlag = validatePaymentId(paymentdetailslist,Integer.parseInt(exceldatalist.get(i+1).trim()));
					if(paymentFlag == false){
						error += " Payment Id does not exist in the system.";
					}
					
					boolean paymentstatusFlag = validatePaymentStatus(paymentdetailslist,Integer.parseInt(exceldatalist.get(i+1).trim()));
					if(paymentstatusFlag == false){
						error += " Payment Id status closed in the system please check.";
					}
				}else if(invoiceWisePaymentListMap==null) {
					if(!invoiceWisePaymentListMap.containsKey(Integer.parseInt(exceldatalist.get(i).trim()))){
				
//					if(invoicePaymentCount.containsKey(Integer.parseInt(exceldatalist.get(i).trim()))){
//						if(invoicePaymentCount.get(Integer.parseInt(exceldatalist.get(i).trim()))>1)
//							error += " More than one open payment document found against invoice id "+exceldatalist.get(i)+". Please specify payment id to be closed.";
//						
//					}
//					else
						error += " No open payment document found against invoice id "+exceldatalist.get(i);
					
				}
				}
				
				//Ashwini Patil Date:6-12-2023
				//we are modifying program for pecopp. If payment id is missing then need to fetch open payment using invoice id
//				if(exceldatalist.get(i+1).trim().equalsIgnoreCase("NA")|| exceldatalist.get(i+1).trim().equalsIgnoreCase("N A")){
//					error += " Payment Id is mandatory.";
//				}
//				if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")|| !exceldatalist.get(i+2).trim().equalsIgnoreCase("N A")){
//					try {
//						Date paymentDate = format.parse(exceldatalist.get(i+2).trim());
//					} catch (Exception e) {
//						error += " Error in payment Date "+e.getMessage();
//					}
//				}
				if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")&&!exceldatalist.get(i+2).trim().equalsIgnoreCase("N A")){
					
					if(paymentModeList.size()>0){
						error += serverapputility.validatePaymentMode(paymentModeList, exceldatalist.get(i+2),exceldatalist.get(2));

					}else{
						error += " No payment modes defined in system.";
					}
				}
				if(exceldatalist.get(i+3).trim().equalsIgnoreCase("NA")|| exceldatalist.get(i+3).trim().equalsIgnoreCase("N A")){
					error += " Payment Method is mandatory.";
				}
				if(exceldatalist.get(i+4).trim().equalsIgnoreCase("NA")|| exceldatalist.get(i+4).trim().equalsIgnoreCase("N A")){
					error += " Net Receivable is mandatory.";
				}
				if(exceldatalist.get(i+4).trim().equalsIgnoreCase("0")){
					error += " Net Receivable should not be zero.";
				}
				
				if(!exceldatalist.get(i+4).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+4).trim().equalsIgnoreCase("N A")){
					double netreceivable = Double.parseDouble(exceldatalist.get(i+4).trim());//Integer.parseInt(exceldatalist.get(i+4).trim()); //Ashwini Patil Date:14-08-2023 orion faced issue in upload. they were having fractional net payable 
					CustomerPayment paymentEntity =null;
					if(!exceldatalist.get(i+1).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+1).trim().equalsIgnoreCase("N A"))
						paymentEntity= getPaymentDocument(paymentdetailslist,Integer.parseInt(exceldatalist.get(i+1).trim()));
					else {
						//if more than one open payment exist in system then sort payments by due date and then check for netreceivable amount.
						if(invoiceWisePaymentListMap!=null&&invoiceWisePaymentListMap.containsKey(Integer.parseInt(exceldatalist.get(i).trim()))) {
							logger.log(Level.SEVERE, "invoiceWisePaymentListMap size="+invoiceWisePaymentListMap.size()); 
							List<CustomerPayment> paymentList=invoiceWisePaymentListMap.get(Integer.parseInt(exceldatalist.get(i).trim()));
							Comparator<CustomerPayment> dueDate=new Comparator<CustomerPayment>() {
								@Override
								public int compare(CustomerPayment arg0, CustomerPayment arg1) {
									return arg0.getPaymentDate().compareTo(arg1.getPaymentDate());
								}
							};
							Collections.sort(paymentList, dueDate);
							logger.log(Level.SEVERE, "paymentList size="+paymentList.size());
							for(CustomerPayment cp:paymentList) {
								logger.log(Level.SEVERE, cp.getCount()+" cp.getPaymentAmt()="+cp.getPaymentAmt()+" netreceivable="+netreceivable);
								if(cp.getPaymentAmt()>=netreceivable) {
									paymentEntity=cp;
									break;
								}
							}
							if(paymentEntity==null)
								error += " Net Receivable should not be greater than gross amount.";
						}else
							paymentEntity=invoicePaymentMap.get(exceldatalist.get(i).trim());
					
					}
					if(paymentEntity!=null){
						logger.log(Level.SEVERE, "paymentEntity == "+paymentEntity.getCount());
						if(netreceivable > paymentEntity.getPaymentAmt() ){
							error += " Net Receivable should not be greater than gross amount.";
						}
					}
				}
				
				if(exceldatalist.get(i+5).trim().equalsIgnoreCase("NA")|| exceldatalist.get(i+5).trim().equalsIgnoreCase("N A")){
					error += " Is TDS Applicable is mandatory.";
				}
				if(exceldatalist.get(i+5).trim().equalsIgnoreCase("yes")){
					if((exceldatalist.get(i+6).trim().equalsIgnoreCase("NA") && exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")) && (exceldatalist.get(i+6).trim().equalsIgnoreCase("N A") && exceldatalist.get(i+7).trim().equalsIgnoreCase("N A"))){
						error += " Is TDS Applicable is yes then TDS Percentage or TDS Amount is mandatory.";
					}
					if((!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")) && (!exceldatalist.get(i+6).trim().equalsIgnoreCase("N A") && !exceldatalist.get(i+7).trim().equalsIgnoreCase("N A"))){
						error += " Please add either TDS Pecentage or TDS Amount.";
					}

				}
				
				if(exceldatalist.get(i+3).trim().equalsIgnoreCase("Cheque")){
					if(exceldatalist.get(i+8).trim().equalsIgnoreCase("NA") || exceldatalist.get(i+8).trim().equalsIgnoreCase("N A")){
						error += " Payment Method is cheque so cheque number is mandatory.";
					}
					if(exceldatalist.get(i+9).trim().equalsIgnoreCase("NA") || exceldatalist.get(i+9).trim().equalsIgnoreCase("N A")){
						error += " Payment Method is cheque so cheque date is mandatory.";
					}
				}
				if(exceldatalist.get(i+3).trim().equalsIgnoreCase("ECS")){
					if(exceldatalist.get(i+13).trim().equalsIgnoreCase("NA")||exceldatalist.get(i+13).trim().equalsIgnoreCase("N A")){
						error += " Payment Method is ECS so transfer date is mandatory.";
					}
					if(exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")||exceldatalist.get(i+10).trim().equalsIgnoreCase("N A")){
						error += " Payment Method is ECS so Bank Branch is mandatory.";
					}
				}

				
				}
				}catch (Exception e) {
					e.printStackTrace();
					error += "There is some error. Ask EVA support team to check the same.";
				}

				if(!error.equals("") && !error.equals(" ")){
					errorList.add(exceldatalist.get(i+1));
					errorList.add(error);
					customerPayment.setCount(-1);
					customerpaymentlist.add(customerPayment);
				}
				
			}
			
			if(errorList!=null && errorList.size()>0){
				 CsvWriter.paymentuploaderErrorlist = errorList;
				 
			}
			
			return customerpaymentlist;
		}

		private CustomerPayment getPaymentDocument(	List<CustomerPayment> paymentdetailslist, int paymentId) {
			for(CustomerPayment paymentEntity : paymentdetailslist){
				if(paymentEntity.getCount() == paymentId){
					return paymentEntity;
				}
			}
			return null;
		}

		private boolean validatePaymentStatus(List<CustomerPayment> paymentdetailslist, int paymentId) {
			for(CustomerPayment paymentEntity : paymentdetailslist){
				if(paymentEntity.getCount() == paymentId && paymentEntity.getStatus().equals(CustomerPayment.CREATED)){
					return true;
				}
			}
			return false;
		}

		private boolean validatePaymentId(List<CustomerPayment> paymentdetailslist, int paymentId) {
			for(CustomerPayment paymentEntity : paymentdetailslist){
				if(paymentEntity.getCount() == paymentId){
					return true;
				}
			}
			return false;
		}

		private boolean validateInvoiceId(List<Invoice> invoicelist, int invoiceId) {
			
			for(Invoice invoiceEntity : invoicelist){
				if(invoiceEntity.getCount() == invoiceId){
					return true;
				}
			}
			return false;
		}

		@Override
		public String uploadPaymentDetails(long comapnyId, String taskName) {

			logger.log(Level.SEVERE, "Payment upload updation task queue calling");
			
			

			ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);

			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
			try {
				
			int interval=0; //Ashwini Patil Date:8-03-2024 Delaying taskqueue by 7 seconds everytime as duplicate payment id's are getting created in orion's system.
			for(int i=14; i<exceldatalist.size();i+=14){
				JSONObject jsonobject = new JSONObject();
				
				jsonobject.put("invoiceId", exceldatalist.get(i).trim());
				jsonobject.put("paymentId", exceldatalist.get(i+1).trim());
//				if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+2).trim().equalsIgnoreCase("N A")){
//					jsonobject.put("paymentDate", exceldatalist.get(i+2).trim());
//				}
//				else{
//					jsonobject.put("paymentDate", "");
//				}
				if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+2).trim().equalsIgnoreCase("N A")){
					jsonobject.put("paymentMode", exceldatalist.get(i+2).trim());
				}
				if(!exceldatalist.get(i+3).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+3).trim().equalsIgnoreCase("N A")){
					jsonobject.put("paymentMethod", exceldatalist.get(i+3).trim());
				}
				else{
					jsonobject.put("paymentMethod", "");
				}
				if(!exceldatalist.get(i+4).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+4).trim().equalsIgnoreCase("N A")){
					jsonobject.put("netReceivable", exceldatalist.get(i+4).trim());
				}
				else{
					jsonobject.put("netReceivable", "");
				}
				
				if(!exceldatalist.get(i+5).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+5).trim().equalsIgnoreCase("N A")){
					jsonobject.put("is Tds Applicable", exceldatalist.get(i+5).trim());
				}
				else{
					jsonobject.put("is Tds Applicable", "");
				}
				
				if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+6).trim().equalsIgnoreCase("N A")){
					jsonobject.put("Tds Pecentage", exceldatalist.get(i+6).trim());
				}
				else{
					jsonobject.put("Tds Pecentage", "");
				}
				
				if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+7).trim().equalsIgnoreCase("N A")){
					jsonobject.put("Tds Amount", exceldatalist.get(i+7).trim());
				}
				else{
					jsonobject.put("Tds Amount", "");
				}
				
				if(!exceldatalist.get(i+8).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+8).trim().equalsIgnoreCase("N A")){
					jsonobject.put("cheque Number", exceldatalist.get(i+8).trim());
				}
				else{
					jsonobject.put("cheque Number", "");
				}
				
				if(!exceldatalist.get(i+9).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+9).trim().equalsIgnoreCase("N A")){
					jsonobject.put("cheque Date", exceldatalist.get(i+9).trim());
				}
				else{
					jsonobject.put("cheque Date", "");
				}
				
				if(!exceldatalist.get(i+10).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+10).trim().equalsIgnoreCase("N A")){
					jsonobject.put("cheque-Bank Name", exceldatalist.get(i+10).trim());
				}
				else{
					jsonobject.put("cheque-Bank Name", "");
				}
				
				if(!exceldatalist.get(i+11).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+11).trim().equalsIgnoreCase("N A")){
					jsonobject.put("cheque-Bank Account No", exceldatalist.get(i+11).trim());
				}
				else{
					jsonobject.put("cheque-Bank Account No", "");
				}
				
				if(!exceldatalist.get(i+12).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+12).trim().equalsIgnoreCase("N A")){
					jsonobject.put("reference No", exceldatalist.get(i+12).trim());
				}
				else{
					jsonobject.put("reference No", "");
				}
				
				if(!exceldatalist.get(i+13).trim().equalsIgnoreCase("NA") && !exceldatalist.get(i+13).trim().equalsIgnoreCase("N A")){
					jsonobject.put("Payment Received Date", exceldatalist.get(i+13).trim()); //old name NEFT-Transfer Date
				}
				else{
					jsonobject.put("Payment Received Date", "");
				}
				

				String jsonstring = jsonobject.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "Json"+jsonstring);
				
				try {
					
					Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
							.param("EntityName",taskName).param("companyId", comapnyId+"")
							.param("paymentUpdationData",jsonstring).etaMillis(System.currentTimeMillis()+interval));
					interval+=7000;
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Exception =="+e.getMessage());
						return "Failed please try again";
					}
				
			}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			return "Payment Upload Process Started";
		}

		@Override
		public ArrayList<CustomerPayment> validateTallyPaymentUploader(long companyId) {
			ServerAppUtility serverUtility = new ServerAppUtility();
			
			ArrayList<String> exceldatalist = serverUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey,null);
			if(exceldatalist!=null) {
				logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
				
				ArrayList<String> uploadfileHeader = new ArrayList<String>();
				uploadfileHeader.add("Sr No");
				uploadfileHeader.add("Voucher No.");
				uploadfileHeader.add("Date");
				uploadfileHeader.add("Account");
				uploadfileHeader.add("PartyName");
				uploadfileHeader.add("Agst Ref");
				uploadfileHeader.add("Agst Ref Amt");
				uploadfileHeader.add("TDS Date");
				uploadfileHeader.add("TDS Amt");
				uploadfileHeader.add("Narration");
				uploadfileHeader.add("Mode");
				uploadfileHeader.add("Instrument No.");
				uploadfileHeader.add("Instrument Date");
				uploadfileHeader.add("Bank");

				boolean validExcelfile = serverUtility.validateExcelSheet(exceldatalist,uploadfileHeader);
				if(validExcelfile) {
					ArrayList<CustomerPayment> list = ValidatetallyPaymentUploadFile(companyId, exceldatalist,serverUtility);
					return list;
				}
				else {
					return null;
				}
			}
			else {
				return null;
			}
		}

		private ArrayList<CustomerPayment> ValidatetallyPaymentUploadFile(long companyId, ArrayList<String> exceldatalist, ServerAppUtility serverUtility) {
			
			ArrayList<CustomerPayment> customerpaymentlist = new ArrayList<CustomerPayment>();
			ArrayList<String> errorList = new ArrayList<String>();

			SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			
			HashSet<Integer> hsinvoiceId = new HashSet<Integer>();
			
			for(int i=14; i<exceldatalist.size();i+=14){
				try {
					hsinvoiceId.add(Integer.parseInt(exceldatalist.get(i+5)));
				} catch (Exception e) {
					errorList.add(exceldatalist.get(i+5));
					errorList.add("Invalid Invoice Number! Column Name - Agst Ref must be only numeric values.");
				}
			}
			
			if(errorList.size()>0) {
				CustomerPayment customerPayment = new CustomerPayment();
				customerPayment.setCount(-1);
				customerpaymentlist.add(customerPayment);
			}
			
//			try {
//				
//			int exellistsize = exceldatalist.size();
//			double value = exellistsize/14;
//			logger.log(Level.SEVERE, " value "+value);
//			logger.log(Level.SEVERE, " exceldatalist.size() "+exceldatalist.size());
//			logger.log(Level.SEVERE, " exceldatalist.size() "+(exceldatalist.size() % 14));
//
//			String strvalue = value+"";
//			String stravaluearray[] = strvalue.split("\\.");
//			double decimalvalue = Double.parseDouble(stravaluearray[1]);
//			logger.log(Level.SEVERE, "decimalvalue"+decimalvalue);
//
//			if(decimalvalue>0) {
//				errorList.add("NA");
//				errorList.add("There are some blank value in some column. Blank not allowed please correct the excel data. ");
//				
//				CustomerPayment customerPayment = new CustomerPayment();
//				customerPayment.setCount(-1);
//				customerpaymentlist.add(customerPayment);
//				
//				CsvWriter.tallyPaymentUploaderErrorlist = errorList;
//				return customerpaymentlist;
//			}
//			} catch (Exception e) {
//			}
			
			
			ArrayList<Integer> arryinvoiceId = new ArrayList<Integer>(hsinvoiceId);
			
			List<Invoice> invoicelist = serverUtility.loadInvoices(companyId,arryinvoiceId);
			List<CustomerPayment> paymentdetailslist = serverUtility.loadCustomerPayment(companyId,arryinvoiceId,CustomerPayment.CREATED);
					
			List<Config> configEntity = serverUtility.loadConfigsData(16,companyId);
			
			for(int i=14; i<exceldatalist.size();i+=14){
			
				CustomerPayment customerPayment = new CustomerPayment();
				String error = "";

				try {
				
				
				
				
				try {
					
					boolean invoiceIdFlag = validateInvoiceId(invoicelist,Integer.parseInt(exceldatalist.get(i+5).trim()));
					if(invoiceIdFlag == false){
						error += " Invoice Id does not exist in the system.";
						customerPayment.setCount(-1);
					}
					
				} catch (Exception e) {
				}
				
				
				boolean paymentstatusFlag = validatePaymentDocumentStatus(paymentdetailslist,Integer.parseInt(exceldatalist.get(i+5).trim()));
				if(paymentstatusFlag == false){
					error += " No Open Payment Document found for Invoice Id -"+exceldatalist.get(i+5).trim();
					customerPayment.setCount(-1);
				}
				
				if(exceldatalist.get(i+5).trim().equalsIgnoreCase("NA")){
					error += " Agst Ref Column (Invoice Id) is mandatory.";
					customerPayment.setCount(-1);
				}
				
				if(exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")){
						error += " Date column payment Date is mandatory.";
						customerPayment.setCount(-1);
				}
				if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")){
					boolean flag = true;
					try {
						Date paymentDate = format.parse(exceldatalist.get(i+2).trim());
						if(paymentDate.after(new Date())) {
							error += " Date column(Payment Date) should not be future date. ";
							customerPayment.setCount(-1);
						}
					} catch (Exception e) {
						error += " Invalid Date format in Date Column. Date value should be DD-MMM-YYYY. ";
						customerPayment.setCount(-1);
						flag = false;
					}
//					if(serverUtility.isValidDate(exceldatalist.get(i+2).trim(), "DD-MMM-YYYY")) {
//						error += " Invalid Date in Date Column. ";
//						customerPayment.setCount(-1);
//					}
					if(flag) {
						if(!serverUtility.isDateValid(exceldatalist.get(i + 2).trim(),"dd-MMM-yyyy")) {
							error += exceldatalist.get(2).trim()+" Column Invalid date.";
							customerPayment.setCount(-1);
						}
					}
					
				}
				
				try {
					Invoice invoiceEntity = getInvoiceEntity(invoicelist,Integer.parseInt(exceldatalist.get(i+5).trim()));
					if(exceldatalist.get(i+4).trim().equalsIgnoreCase("NA")){
						error += " PartyName (Customer Name) Column is mandatory.";
						customerPayment.setCount(-1);
					}
					else {
						if(invoiceEntity!=null) {
							if(!invoiceEntity.getPersonInfo().getFullName().equals(exceldatalist.get(i+4).trim())) {
								error += " PartyName (Customer Name) Column does not match with the Agst Ref Column (Invoice Id) - "+exceldatalist.get(i+5).trim()+". ";
								customerPayment.setCount(-1);
							}
						}
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
				if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
					if(!serverUtility.validateNumricValue(exceldatalist.get(i+6).trim())) {
						error += " Please add numeric value in Agst Ref Amt Column.";
						customerPayment.setCount(-1);
					}
				}
				
				if(exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
					error += " Agst Ref Amt Column is mandatory.";
					customerPayment.setCount(-1);
				}
				if(exceldatalist.get(i+6).trim().equalsIgnoreCase("0")){
					error += " Agst Ref Amt Column should not be zero.";
					customerPayment.setCount(-1);
				}
				
				if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
					double netreceivable = Double.parseDouble(exceldatalist.get(i+6).trim());
					CustomerPayment paymentEntity = getPaymentDocumentOnInvoiceId(paymentdetailslist,Integer.parseInt(exceldatalist.get(i+5).trim()));
					if(paymentEntity!=null){
						if(netreceivable > paymentEntity.getPaymentAmt() ){
							error += " Agst Ref Amt Column should not be greater than gross amount.";
							customerPayment.setCount(-1);
						}
					}
				}
				
				if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
					try {
						Date TDSDate = format.parse(exceldatalist.get(i + 7).trim());
					} catch (Exception e) {
						error += " Invalid Date format in TDS Date Column. Date value should be DD-MMM-YYYY. ";
						customerPayment.setCount(-1);
					}
					if(!serverUtility.isDateValid(exceldatalist.get(i + 7).trim(),"dd-MMM-yyyy")) {
						error += exceldatalist.get(7).trim()+" Column Invalid date.";
						customerPayment.setCount(-1);

					}
				}
				
				if(!exceldatalist.get(i+8).trim().equalsIgnoreCase("NA")){
					if(!serverUtility.validateNumricValue(exceldatalist.get(i+8).trim())) {
						error += " Please add numeric value in TDS Amt Column.";
						customerPayment.setCount(-1);
					}
				}
				
				if(!exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")){
					if(!serverUtility.validateConfig(configEntity,exceldatalist.get(i+10).trim())) {
						error += " Mode Column (Payment Method) does not exist in the ERP Please define first in ERP - "+exceldatalist.get(i+10);
						customerPayment.setCount(-1);
					}
				}
				
				if(!exceldatalist.get(i+12).trim().equalsIgnoreCase("NA")){
					try {
						Date instrumentDate = format.parse(exceldatalist.get(i + 12).trim());
					} catch (Exception e) {
						error += " Invalid Date format in Instrument Date Column. Date value should be DD-MMM-YYYY. ";
						customerPayment.setCount(-1);
					}
					if(!serverUtility.isDateValid(exceldatalist.get(i + 12).trim(),"dd-MMM-yyyy")) {
						error += exceldatalist.get(12).trim()+" Column Invalid date.";
						customerPayment.setCount(-1);

					}
				}
				
				error += serverUtility.validateSpecialCharComma(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());
				error += serverUtility.validateSpecialCharComma(exceldatalist.get(1).trim(), exceldatalist.get(i+1).trim());
				error += serverUtility.validateSpecialCharComma(exceldatalist.get(5).trim(), exceldatalist.get(i +5).trim());
				error += serverUtility.validateSpecialCharComma(exceldatalist.get(6).trim(), exceldatalist.get(i +6).trim());
				error += serverUtility.validateSpecialCharComma(exceldatalist.get(8).trim(), exceldatalist.get(i +6).trim());

				error += serverUtility.validateNumericCloumn(exceldatalist.get(0).trim(), exceldatalist.get(i).trim());

				} catch (Exception e) {
					e.printStackTrace();
					error += "There may be some formula used in some column. Formula value column not allowed! OR May be hiden column is there please check ";
				}
			
				if(!error.equals("")) {
					customerPayment.setCount(-1);
					errorList.add(exceldatalist.get(i));
					errorList.add(error);
					customerpaymentlist.add(customerPayment);
					logger.log(Level.SEVERE, "Error " +error);
				}
				
			
			}
			logger.log(Level.SEVERE, "Error size" +errorList.size());
			logger.log(Level.SEVERE, "customerpaymentlist size" +customerpaymentlist.size());
			logger.log(Level.SEVERE, "customerpaymentlist " +customerpaymentlist);

			 CsvWriter.tallyPaymentUploaderErrorlist = errorList;

			return customerpaymentlist;
		
		}

		private CustomerPayment getPaymentDocumentOnInvoiceId(List<CustomerPayment> paymentdetailslist, int invoiceId) {

			for(CustomerPayment paymentEntity : paymentdetailslist){
				if(paymentEntity.getInvoiceCount() == invoiceId && paymentEntity.getStatus().equals(CustomerPayment.CREATED)){
					return paymentEntity;
				}
			}
			return null;
		
		}

		private Invoice getInvoiceEntity(List<Invoice> invoicelist, int invoiceId) {

			for(Invoice invoiceEntity : invoicelist){
				if(invoiceEntity.getCount() == invoiceId){
					return invoiceEntity;
				}
			}
			return null;
		
		}

		private boolean validatePaymentDocumentStatus(List<CustomerPayment> paymentdetailslist, int invoieId) {
			for(CustomerPayment paymentEntity : paymentdetailslist){
				if(paymentEntity.getInvoiceCount() == invoieId && paymentEntity.getStatus().equals(CustomerPayment.CREATED)){
					return true;
				}
			}
			return false;
		}

		@Override
		public String uploadTallyPaymentDetails(long comapnyId, String taskName) {


			logger.log(Level.SEVERE, "Payment upload updation task queue calling");
			
			SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdformat.setTimeZone(TimeZone.getTimeZone("IST"));
			

			ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);

			logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
			try {
				ServerAppUtility serverutility = new ServerAppUtility();
			
			for(int i=14; i<exceldatalist.size();i+=14){
				
				JSONObject jsonobject = new JSONObject();
				if(serverutility.validateNumricValue(exceldatalist.get(i+5).trim())) {
					
				jsonobject.put("invoiceId", exceldatalist.get(i+5).trim());
				jsonobject.put("paymentId", "");
				if(!exceldatalist.get(i+2).trim().equals("NA")){
					jsonobject.put("paymentDate", exceldatalist.get(i+2).trim());
				}
				else{
					jsonobject.put("paymentDate", "");
				}
				if(!exceldatalist.get(i+10).trim().equals("NA")){
					jsonobject.put("paymentMethod", exceldatalist.get(i+10).trim());
				}
				else{
					jsonobject.put("paymentMethod", "");
				}
				
				double invoiceAmt = Double.parseDouble(exceldatalist.get(i+6).trim());
				double tdsAmt =0;
				double netrecievable = invoiceAmt;
				if(!exceldatalist.get(i+8).trim().equals("NA")){
					tdsAmt = Double.parseDouble(exceldatalist.get(i+8).trim());
					netrecievable = invoiceAmt - tdsAmt;
				}
				if(!exceldatalist.get(i+6).trim().equals("NA")){
					jsonobject.put("netReceivable", netrecievable+"");
				}
				else{
					jsonobject.put("netReceivable", "");
				}
				
				if(!exceldatalist.get(i+8).trim().equals("NA")){
					jsonobject.put("is Tds Applicable", "yes");
				}
				else{
					jsonobject.put("is Tds Applicable", "");
				}
				
				jsonobject.put("Tds Pecentage", "");
				
				if(!exceldatalist.get(i+8).trim().equals("NA")){
					jsonobject.put("Tds Amount", exceldatalist.get(i+8).trim());
				}
				else{
					jsonobject.put("Tds Amount", "");
				}
				
				if(!exceldatalist.get(i+11).trim().equals("NA") && (exceldatalist.get(i+10).trim().contains("Cheque") ) ){
					jsonobject.put("cheque Number", exceldatalist.get(i+11).trim());
				}
				else{
					jsonobject.put("cheque Number", "");
				}
				
				if(!exceldatalist.get(i+11).trim().equals("NA") && !exceldatalist.get(i+10).trim().contains("Cheque") ){
					if(exceldatalist.get(i+10).trim().equalsIgnoreCase("NEFT") || exceldatalist.get(i+10).trim().equalsIgnoreCase("RTGS")
							|| exceldatalist.get(i+10).trim().equalsIgnoreCase("UPI")) {
						jsonobject.put("reference No", exceldatalist.get(i+11).trim());
						
					}
					else {
						jsonobject.put("reference No", "");
					}
				}
				else{
					jsonobject.put("reference No", "");
				}
				
				if(!exceldatalist.get(i+12).trim().equals("NA") && !exceldatalist.get(i+10).trim().equalsIgnoreCase("CASH")){
					if(exceldatalist.get(i+10).trim().equalsIgnoreCase("NEFT") || exceldatalist.get(i+10).trim().equalsIgnoreCase("RTGS")
							|| exceldatalist.get(i+10).trim().equalsIgnoreCase("UPI")) {
						jsonobject.put("cheque Date", "");
					}
					else {
						jsonobject.put("cheque Date", exceldatalist.get(i+12).trim());
					}
				}
				else{
					jsonobject.put("cheque Date", "");
				}
				
				if(!exceldatalist.get(i+13).trim().equals("NA") && !exceldatalist.get(i+10).trim().equalsIgnoreCase("CASH")){
					if(exceldatalist.get(i+10).trim().equalsIgnoreCase("NEFT") || exceldatalist.get(i+10).trim().equalsIgnoreCase("RTGS")
							|| exceldatalist.get(i+10).trim().equalsIgnoreCase("UPI")) {
						jsonobject.put("cheque-Bank Name", "");
					}
					else {
						jsonobject.put("cheque-Bank Name", exceldatalist.get(i+13).trim());
					}
				}
				else{
					jsonobject.put("cheque-Bank Name", "");
				}
				
				if(!exceldatalist.get(i+3).trim().equals("NA") && !exceldatalist.get(i+10).trim().equalsIgnoreCase("CASH")){
					if(exceldatalist.get(i+10).trim().equalsIgnoreCase("NEFT") || exceldatalist.get(i+10).trim().equalsIgnoreCase("RTGS")
							|| exceldatalist.get(i+10).trim().equalsIgnoreCase("UPI")) {
						jsonobject.put("cheque-Bank Account No", "");
					}
					else {
						jsonobject.put("cheque-Bank Account No", exceldatalist.get(i+3).trim());
					}
					
				}
				else{
					jsonobject.put("cheque-Bank Account No", "");
				}
				
				
				
				if(!exceldatalist.get(i+12).trim().equals("NA") && !exceldatalist.get(i+10).trim().contains("Cheque")
						  && !exceldatalist.get(i+10).trim().equalsIgnoreCase("CASH")){
					jsonobject.put("NEFT-Transfer Date", exceldatalist.get(i+12).trim());
				}
				else{
					jsonobject.put("NEFT-Transfer Date", "");
				}
				
				if(!exceldatalist.get(i+9).trim().equals("NA")){
					jsonobject.put("Description", exceldatalist.get(i+9).trim());
				}
				else{
					jsonobject.put("Description", "");
				}

				if(!exceldatalist.get(i+1).trim().equals("NA")){
					jsonobject.put("ref No", exceldatalist.get(i+1).trim());
				}
				else{
					jsonobject.put("ref No", "");
				}
				
				String jsonstring = jsonobject.toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "Json"+jsonstring);
				
				try {
					Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
							.param("EntityName",taskName).param("companyId", comapnyId+"")
							.param("paymentUpdationData",jsonstring) );
					
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Exception =="+e.getMessage());
						return "Failed please try again";
					}
			
			}
				
			}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
			return "Payment Upload Process Started";
		}

		@Override
		public ArrayList<Service> validateServiceCancellationUpload(long companyId,String loggedinuser) {
			
			ServerAppUtility serverUtility = new ServerAppUtility();
			
			ArrayList<String> exceldatalist = serverUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey,null);
			if(exceldatalist!=null) {
				logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
				
				ArrayList<String> uploadfileHeader = new ArrayList<String>();
				uploadfileHeader.add("Service ID");
				uploadfileHeader.add("Remark");

				boolean validExcelfile = serverUtility.validateExcelSheet(exceldatalist,uploadfileHeader);
				if(validExcelfile) {
					ArrayList<Service> list = ValidateServiceCancellationFile(companyId, exceldatalist,serverUtility,loggedinuser);
					return list;
				}
				else {
					return null;
				}
			}
			else {
				return null;
			}
		}

		private ArrayList<Service> ValidateServiceCancellationFile(	long companyId, ArrayList<String> exceldatalist,
				ServerAppUtility serverUtility, String loggedinuser) {
			
			ArrayList<Integer> serviceidlist = new ArrayList<Integer>();
			for(int i=2;i<exceldatalist.size();i+=2){
				try {
					serviceidlist.add(Integer.parseInt(exceldatalist.get(i).trim()));
				} catch (Exception e) {
				}
			}

			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceidlist).list();
			logger.log(Level.SEVERE, "servicelist size "+servicelist.size());
			
			ArrayList<Service> list = new ArrayList<Service>();
			ArrayList<String> errorList = new ArrayList<String>();

			
			
			
			int serviceidsize = exceldatalist.size()/2;
			logger.log(Level.SEVERE, "serviceidsize"+serviceidsize);
			if(serviceidsize>1001){
				Service service = new Service();
				service.setCount(-1);
				errorList.add(exceldatalist.get(2));
				errorList.add("Excel sheet should not be more than 1000 records!");
				list.add(service);
			}
			
			for(int i=2;i<exceldatalist.size();i+=2){
				Service service = new Service();
				String error = "";
				error += serverUtility.validateMandatory(exceldatalist.get(i).trim(), exceldatalist.get(0).trim());
				error += serverUtility.validateMandatory(exceldatalist.get(i+1).trim(), exceldatalist.get(1).trim());

					if(!serverUtility.validateNumricValue(exceldatalist.get(i).trim())) {
						error += " Please add numeric value in Service Id Column.";
						service.setCount(-1);
					}
					
					error += validateServiceId(exceldatalist.get(i).trim(),servicelist);
					
					if(!error.equals("")) {
						service.setCount(-1);
						errorList.add(exceldatalist.get(i));
						errorList.add(error);
						list.add(service);
						logger.log(Level.SEVERE, "Error " +error);
					}
			}
			
			for(Service serviceEntity : servicelist){
				if(!serviceEntity.getStatus().equals(Service.SERVICESTATUSSCHEDULE) && !serviceEntity.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
					Service service = new Service();
					service.setCount(-1);
					errorList.add(serviceEntity.getCount()+"");
					errorList.add("Service status must be either Scheduled or Rescheduled");
					list.add(service);
				}
			}
			
			if(errorList!=null && errorList.size()>0){
				 CsvWriter.serviceCancellationErrorlist = errorList;
			}
			else{
				String msg = callTaskQueueForServiceCancellationUpload(companyId,loggedinuser);
				 logger.log(Level.SEVERE, "msg "+msg);
			}
				
			return list;
		}

		private String validateServiceId(String strserviceId, List<Service> servicelist) {
			for(Service service : servicelist){
				try {
					int serviceId = Integer.parseInt(strserviceId);
					
					if(service.getCount()==serviceId){
						return "";
					}	
				} catch (Exception e) {
					return "Service id is not valid please check";
				}
				
			}
			return "Service id does not exist please check";
		}

		private String callTaskQueueForServiceCancellationUpload(long companyId, String loggedinuser) {
			
			logger.log(Level.SEVERE, "Service Cancellation Upload task queue calling");
			try {
				
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName","Service Cancellation Upload").param("companyId", companyId+"")
					.param("loggedinuser", loggedinuser));
			
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE,"Exception =="+e.getMessage());
				return "Failed please try again";
			}
			return "Service Cancellation Upload Process Started";
		}

		public void ServiceCancelationUpload(long companyId, ArrayList<String> exceldatalist,String loggedInUser) {

			ArrayList<Integer> serviceidlist = new ArrayList<Integer>();
			for(int i=2;i<exceldatalist.size();i+=2){
				serviceidlist.add(Integer.parseInt(exceldatalist.get(i).trim()));
			}

			List<Service> list = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceidlist).list();
			logger.log(Level.SEVERE, "list size "+list.size());

			for(Service serviceEntity : list){
				String cancelationRemark = getRemark(exceldatalist,serviceEntity.getCount());
				serviceEntity.setRemark(cancelationRemark);
				
				serviceEntity.setReasonForChange("Service ID ="+serviceEntity.getCount()+" "+"Service status ="+serviceEntity.getStatus().trim()+"\n"
						+"has been cancelled by "+loggedInUser+" with remark "+"\n"
						+"Remark ="+cancelationRemark+" "+"Cancellation Date ="+fmt.format(new Date()));
				serviceEntity.setStatus(Service.SERVICESTATUSCANCELLED);
				serviceEntity.setRecordSelect(false);
				serviceEntity.setCancellationDate(new Date());
			}
			logger.log(Level.SEVERE, "After updated data now saving the data into db");
			ofy().save().entities(list);
			logger.log(Level.SEVERE, "Done here");

		}

		private String getRemark(ArrayList<String> exceldatalist, int servicecId) {
			for(int i=2;i<exceldatalist.size();i+=2){
				if(Integer.parseInt(exceldatalist.get(i).trim())==servicecId){
					return exceldatalist.get(i+1).trim();
				}
			}

			return "";
		}
		
		
			@Override
		public String customerBranchUpload(long companyId) {
			

			ServerAppUtility serverUtility = new ServerAppUtility();
			
			ArrayList<String> exceldatalist = serverUtility.readExcelAndGetArraylist(DataMigrationImpl.blobkey,null);
			if(exceldatalist!=null) {
				logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
				
				ArrayList<String> uploadfileHeader = new ArrayList<String>();
				uploadfileHeader.add("*Customer ID");
				uploadfileHeader.add("*Branch Name");
				uploadfileHeader.add("Branch Email");
				uploadfileHeader.add("*Servicing Branch");
				uploadfileHeader.add("*Cell Phone No 1");
				uploadfileHeader.add("Branch GSTIN");
				uploadfileHeader.add("Point Of Contact Name");
				uploadfileHeader.add("Point Of Contact Landline");
				uploadfileHeader.add("Point Of Contact Cell");
				uploadfileHeader.add("Point Of Contact Email");
				uploadfileHeader.add("*Address Line 1");
				uploadfileHeader.add("Address Line 2");
				uploadfileHeader.add("Landmark");
				uploadfileHeader.add("Locality");
				uploadfileHeader.add("*City");
				uploadfileHeader.add("*State");
				uploadfileHeader.add("*Country");
				uploadfileHeader.add("Pin");
				uploadfileHeader.add("*Billing Address Line 1");
				uploadfileHeader.add("Billing Address Line 2");
				uploadfileHeader.add("Billing Address Landmark");
				uploadfileHeader.add("Billing Address Locality");
				uploadfileHeader.add("*Billing Address City");
				uploadfileHeader.add("*Billing Address State");
				uploadfileHeader.add("*Billing Address Country");
				uploadfileHeader.add("Billing Address Pin");
				uploadfileHeader.add("Area");
				uploadfileHeader.add("UOM");
				uploadfileHeader.add("Cost Center");
				uploadfileHeader.add("Tier");
				uploadfileHeader.add("TAT");


				boolean validExcelfile = serverUtility.validateExcelSheet(exceldatalist,uploadfileHeader);
				if(validExcelfile) {
					
					int sizeOfList =0;
					for (int i = 31; i < exceldatalist.size(); i += 31) {
						sizeOfList++;
					}
					if(exceldatalist.size()%31!=0) { //Ashwini Patil Date:12-1-2024
						return "Blank cells were found! Put NA instead of blank for the non-mandatory field, and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.";
					}
					if(sizeOfList > 1000){
						return "You can only upload 1000 branches in one excel file upload";
					}
					
					logger.log(Level.SEVERE, "Lock Seal Stock upload task queue calling");
					try {
					Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName","Customer Branch Upload").param("companyId", companyId+""));
					
					} catch (Exception e) {
						logger.log(Level.SEVERE,"Exception =="+e.getMessage());
						return "Failed please try again";
					}
					Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
					return "Customer branch upload process started,please check email for its status email sent on "+companyEntity.getPocEmail();
				}
				else {
					return "Excel upload file format headres are not matching please get latest excel file format from EVA Support team";
				}
			}
			else {
				return "Unable to read excel file please try again";
			}
		
		}

			@Override
			public Integer validateCustomerBranchStatusUpdation(long companyId,boolean cancelServicesFlag) {


				ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);

				logger.log(Level.SEVERE, "excel list after reading data exceldatalist size"+ exceldatalist.size());
				ArrayList<String> errorList = new ArrayList<String>();
				ServerAppUtility serverUtility = new ServerAppUtility();
				if(exceldatalist.size()%2!=0) { //Ashwini Patil Date:26-05-2023
					errorList.add("File");
					errorList.add("Blank cells were found! ");
				}
				if (exceldatalist.get(0).trim().equalsIgnoreCase("Customer Branch ID")
						&& exceldatalist.get(1).trim()
								.equalsIgnoreCase("Status"))
				{
					
					for(int i=2; i<exceldatalist.size();i+=2){
						String error="";
						error += serverUtility.validateMandatory(exceldatalist.get(i+0).trim(), exceldatalist.get(0).trim());
						error += serverUtility.validateMandatory(exceldatalist.get(i+1).trim(), exceldatalist.get(1).trim());

						if(exceldatalist.get(i+1)==null||(!exceldatalist.get(i+1).equalsIgnoreCase("Active")&&!exceldatalist.get(i+1).equalsIgnoreCase("Inactive")))
							error +=" Put status as Active or Inactive";
						if(!error.equals("") && !error.equals(" ")){
							errorList.add(exceldatalist.get(i+0));
							errorList.add(error);
						}
					}
					if(errorList!=null && errorList.size()>0){
						 CsvWriter.customerBranchuploaderErrorlist = errorList;
						 return -2;
					}
					return 1;
				} else {
					return -1;
				}	
				
			}

			@Override
			public String updateCustomerBranchStatus(long comapnyId,boolean cancelServicesFlag) {
				// TODO Auto-generated method stub


				logger.log(Level.SEVERE, "Customer branch updation task queue calling. cancelServicesFlag="+cancelServicesFlag);
				
				

				ArrayList<String> exceldatalist = readExcelAndGetArraylist(DataMigrationImpl.blobkey);

				logger.log(Level.SEVERE, "excel list after reading data exceldatalist size", exceldatalist.size());
				try {
					
				if(cancelServicesFlag){
					for(int i=2; i<exceldatalist.size();i+=2){
						JSONObject jsonobject = new JSONObject();
						
						jsonobject.put("custBranchId", exceldatalist.get(i).trim());
						jsonobject.put("status", exceldatalist.get(i+1).trim());

						String jsonstring = jsonobject.toString().replaceAll("\\\\", "");
						logger.log(Level.SEVERE, "Json"+jsonstring);
						
						try {
							logger.log(Level.SEVERE,"calling UpdateCustomerBranchStatus task queue");
							Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
									.param("EntityName","UpdateCustomerBranchStatus").param("companyId", comapnyId+"")
									.param("UpdationData",jsonstring).param("ServiceCancellationFlag","true") );
							
							} catch (Exception e) {
								logger.log(Level.SEVERE,"Exception =="+e.getMessage());
								return "Failed please try again";
							}
						
					}				
				}else{
					JSONArray jsonArray=new JSONArray();
					for(int i=2; i<exceldatalist.size();i+=2){
						JSONObject jsonobject = new JSONObject();
						
						jsonobject.put("custBranchId", exceldatalist.get(i).trim());
						jsonobject.put("status", exceldatalist.get(i+1).trim());

						jsonArray.put(jsonobject);
					}
					JSONObject finalJsonObj = new JSONObject();
					finalJsonObj.put("data", jsonArray);
					String jsonstring = finalJsonObj.toString().replaceAll("\\\\", "");
					logger.log(Level.SEVERE, "Json::"+jsonstring);
					try {
						Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue")
								.param("EntityName","UpdateCustomerBranchStatus").param("companyId", comapnyId+"")
								.param("UpdationData",jsonstring).param("ServiceCancellationFlag","false") );
						
					} catch (Exception e) {
							logger.log(Level.SEVERE,"Exception =="+e.getMessage());
							return "Failed please try again";
					}
					
				}

				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				return "Customer branch status updation process started.";
			
				
			}

			@Override
			public ArrayList<String> UpdateZohoCustomerId() {
				ArrayList<String> errorList=new ArrayList<String>();
				ArrayList<String> exceldatalist =  readExcelAndGetArraylist(DocumentUploadTaskQueue.contractUploadBlobKey);
				ServerAppUtility serverapputility = new ServerAppUtility();

				if (exceldatalist == null || exceldatalist.size() == 0) {
					errorList.add("File");
					errorList.add("Unable read excel data please try again!");	
					CsvWriter.customerWithContractErrorList = errorList;
					return errorList;
				}
				logger.log(Level.SEVERE, "columns are "+exceldatalist.get(0).trim()+","+exceldatalist.get(1).trim()+","+exceldatalist.get(2).trim());
				if (exceldatalist.get(0).trim().equalsIgnoreCase("Display Name")
						&& exceldatalist.get(1).trim().equalsIgnoreCase("Customer Sub Type")
						&& exceldatalist.get(2).trim().equalsIgnoreCase("Contact ID")
						) {
					logger.log(Level.SEVERE, "headers matched");
					int custcount = exceldatalist.size()/3;
					if(custcount>1001){
						errorList.add("File");
						errorList.add("Upload excel sheet should not be more than 1000 records!");
						CsvWriter.customerWithContractErrorList = errorList;
						return errorList;
					}
					if(exceldatalist.size()%3!=0){
						errorList.add("File");
						errorList.add("Blank cells were found! Put NA instead of blank for the non-mandatory field, and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.");
						CsvWriter.customerWithContractErrorList = errorList;
						return errorList;
					}
				
					logger.log(Level.SEVERE, "Excel Read Successfully! custcount="+custcount);
					BlobKey blobkeyValue = DocumentUploadTaskQueue.contractUploadBlobKey;
					String strblobkey = blobkeyValue.getKeyString();
					logger.log(Level.SEVERE, "strblobkey"+strblobkey);
					
					ArrayList<String> fullnamelist=new ArrayList<String>();
					ArrayList<String> compnamelist=new ArrayList<String>();
					ArrayList<String> namelist=new ArrayList<String>();
					
					for(int i=3;i<exceldatalist.size();i=i+3) {
						int rowNo=i/3+1;
						if(!exceldatalist.get(i+1).trim().equalsIgnoreCase("business")&&!exceldatalist.get(i+1).trim().equalsIgnoreCase("individual"))
						{	
							errorList.add("Row "+rowNo);
							errorList.add("Put either business or individual in Customer Sub Type column");						
						}
						if(exceldatalist.get(i).trim().equalsIgnoreCase("na")) {
							errorList.add("Row "+rowNo);
							errorList.add("Display Name column is mandatory!");
						}
						String validCustID=serverapputility.validateNumericCloumn(exceldatalist.get(2),exceldatalist.get(i+2));
						
						if(!validCustID.equals("")) {
							errorList.add("Row "+rowNo);
							errorList.add(validCustID);
						}
					
						if(exceldatalist.get(i+1).trim().equalsIgnoreCase("business"))
							compnamelist.add(exceldatalist.get(i).trim());
						else {
							fullnamelist.add(exceldatalist.get(i).trim());
						}
						namelist.add(exceldatalist.get(i).trim());
					}
					if(errorList.size()>0) {
						CsvWriter.customerWithContractErrorList = errorList;
						return errorList;
					}
					
					List<Customer> customerlist=new ArrayList<Customer>();
					List<Company> companyList = ofy().load().type(Company.class).list();
					Company comp=null;
					if(companyList!=null) {
						if(companyList.size()==1)
							comp=companyList.get(0);
						else {
							for(Company c:companyList) {
								if(c.getAccessUrl().equals("my"))
									comp=c;
							}
						}
					}
					
					List<Customer> fullnamewisecustomerlist=null;
					if(fullnamelist!=null&&fullnamelist.size()>0)
						fullnamewisecustomerlist=ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("fullname IN",fullnamelist ).list();
					List<Customer> companynamewisecustomerlist=null;
					if(compnamelist!=null&&compnamelist.size()>0)						
						companynamewisecustomerlist=ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("companyName IN",compnamelist ).list();
					if(fullnamewisecustomerlist!=null)
						customerlist.addAll(fullnamewisecustomerlist);
					
					if(companynamewisecustomerlist!=null)
						customerlist.addAll(companynamewisecustomerlist);
					
					
					for(int i=3;i<exceldatalist.size();i=i+3) {
						int rowNo=i/3+1;
						if(customerlist!=null&&customerlist.size()>0) {
							Customer c= getCustomerByName(customerlist,exceldatalist.get(i));
							if(c!=null) {
								c.setZohoCustomerId(Long.parseLong(exceldatalist.get(i+2)));
								ofy().save().entity(c);
								errorList.add(exceldatalist.get(i));
								errorList.add("Zoho customer id saved successfully in customer ID "+c.getCount());
							}else {
								errorList.add(exceldatalist.get(i));
								errorList.add("No customer found with this name.");							
							}
						}else {
							errorList.add(exceldatalist.get(i));
							errorList.add("No customer found with this name.");							
						}
					}
					
					DocumentUploadTaskQueue.contractUploadBlobKey = null;
					
				} else {
					logger.log(Level.SEVERE, "headers not matched");
					errorList.add("File");
					errorList.add("Invalid excel sheet new");					
				}
//				logger.log(Level.SEVERE, "columns are "+exceldatalist.get(0).trim()+","+exceldatalist.get(1).trim()+","+exceldatalist.get(2).trim()+","+exceldatalist.get(3).trim()+","+exceldatalist.get(4).trim()+","+exceldatalist.get(5).trim()+","+exceldatalist.get(6).trim()+","+exceldatalist.get(7).trim());
//				if (exceldatalist.get(0).trim().equalsIgnoreCase("Company Name")
//						&& exceldatalist.get(1).trim().equalsIgnoreCase("First Name")
//						&& exceldatalist.get(2).trim().equalsIgnoreCase("Last Name")
//						&& exceldatalist.get(3).trim().equalsIgnoreCase("Phone")
//						&& exceldatalist.get(4).trim().equalsIgnoreCase("Customer Sub Type")
//						&& exceldatalist.get(5).trim().equalsIgnoreCase("EmailID")
//						&& exceldatalist.get(6).trim().equalsIgnoreCase("MobilePhone")
//						&& exceldatalist.get(7).trim().equalsIgnoreCase("Contact ID")
//						) {
//					logger.log(Level.SEVERE, "headers matched");
//					int custcount = exceldatalist.size()/8;
//					if(custcount>1000){
//						errorList.add("File");
//						errorList.add("Upload excel sheet should not be more than 1000 records!");
//						CsvWriter.customerWithContractErrorList = errorList;
//						return errorList;
//					}
//					if(exceldatalist.size()%8!=0){ //Ashwini Patil Date:27-12-2023 added blank cell validation
//						errorList.add("File");
//						errorList.add("Blank cells were found! Put NA instead of blank for the non-mandatory field, and put the correct existing value that is already in your ERP system instead of blank for the mandatory field.");
//						CsvWriter.customerWithContractErrorList = errorList;
//						return errorList;
//					}
//				
//					logger.log(Level.SEVERE, "Excel Read Successfully! custcount="+custcount);
//					BlobKey blobkeyValue = DocumentUploadTaskQueue.contractUploadBlobKey;
//					String strblobkey = blobkeyValue.getKeyString();
//					logger.log(Level.SEVERE, "strblobkey"+strblobkey);
//					
//					ArrayList<String> fullnamelist=new ArrayList<String>();
//					ArrayList<String> compnamelist=new ArrayList<String>();
//					ArrayList<String> namelist=new ArrayList<String>();
//					
//					for(int i=8;i<exceldatalist.size();i=i+8) {
//						int rowNo=i/8+1;
//						if(!exceldatalist.get(i+4).trim().equalsIgnoreCase("business")&&!exceldatalist.get(i+4).trim().equalsIgnoreCase("individual"))
//						{	
//							errorList.add("Row "+rowNo);
//							errorList.add("Put either business or individual in Customer Sub Type column");						
//						}
//						if(exceldatalist.get(i).trim().equalsIgnoreCase("na")&&exceldatalist.get(i+1).trim().equalsIgnoreCase("na")) {
//							errorList.add(exceldatalist.get(i+1));
//							errorList.add("Customer Name column is mandatory!");
//						}
//						String validPhone=serverapputility.validateNumericCloumn(exceldatalist.get(3),exceldatalist.get(i+3));
//						String validMobile=serverapputility.validateNumericCloumn(exceldatalist.get(6),exceldatalist.get(i+6));
//						String validCustID=serverapputility.validateNumericCloumn(exceldatalist.get(7),exceldatalist.get(i+7));
//						
//						if(!validPhone.equals("")) {
//							errorList.add("Row "+rowNo);
//							errorList.add(validPhone);
//						}
//						if(!validMobile.equals("")) {
//							errorList.add("Row "+rowNo);
//							errorList.add(validMobile);
//						}
//						if(!validCustID.equals("")) {
//							errorList.add("Row "+rowNo);
//							errorList.add(validCustID);
//						}
//					
//						if(exceldatalist.get(i+4).trim().equalsIgnoreCase("business"))
//							compnamelist.add(exceldatalist.get(i).trim());
//						else {
//							String fullName=exceldatalist.get(i+1).trim();
//							if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("na"))
//								fullName=fullName+" "+exceldatalist.get(i+2);
//							fullnamelist.add(fullName);
//						}
//						namelist.add(exceldatalist.get(i+1).trim());
//					}
//					if(errorList.size()>0) {
//						CsvWriter.customerWithContractErrorList = errorList;
//						return errorList;
//					}
//					
//					List<Customer> customerlist=new ArrayList<Customer>();
//					List<Company> companyList = ofy().load().type(Company.class).list();
//					Company comp=null;
//					if(companyList!=null) {
//						if(companyList.size()==1)
//							comp=companyList.get(0);
//						else {
//							for(Company c:companyList) {
//								if(c.getAccessUrl().equals("my"))
//									comp=c;
//							}
//						}
//					}
//					
//					List<Customer> fullnamewisecustomerlist=null;
//					if(fullnamelist!=null&&fullnamelist.size()>0)
//						fullnamewisecustomerlist=ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("fullname IN",fullnamelist ).list();
//					List<Customer> companynamewisecustomerlist=null;
//					if(compnamelist!=null&&compnamelist.size()>0)						
//						companynamewisecustomerlist=ofy().load().type(Customer.class).filter("companyId", comp.getCompanyId()).filter("companyName IN",compnamelist ).list();
//					if(fullnamewisecustomerlist!=null)
//						customerlist.addAll(fullnamewisecustomerlist);
//					
//					if(companynamewisecustomerlist!=null)
//						customerlist.addAll(companynamewisecustomerlist);
//					
//					
//					for(int i=8;i<exceldatalist.size();i=i+8) {
//						int rowNo=i/8+1;
//						if(customerlist!=null&&customerlist.size()>0) {
//							Customer c= getCustomerByName(customerlist,exceldatalist.get(i),exceldatalist.get(i+1),exceldatalist.get(i+2));
//							if(c!=null) {
//								c.setZohoCustomerId(Long.parseLong(exceldatalist.get(i+7)));
//								ofy().save().entity(c);
//								errorList.add("Row "+rowNo);
//								errorList.add("Zoho customer id saved successfully in customer ID "+c.getCount());
//							}else {
//								errorList.add("Row "+rowNo);
//								errorList.add("No customer found with this name.");							
//							}
//						}else {
//							errorList.add("Row "+rowNo);
//							errorList.add("No customer found with this name.");							
//						}
//					}
//					
//					DocumentUploadTaskQueue.contractUploadBlobKey = null;
//					
//				} else {
//					logger.log(Level.SEVERE, "headers not matched");
//					errorList.add("File");
//					errorList.add("Invalid excel sheet new");					
//				}
				CsvWriter.customerWithContractErrorList = errorList;
				
				return errorList;
			}

			public Customer getCustomerByName(List<Customer> customerlist,String compName,String firstName,String lastName) {
				Customer cust=null;
				String fullName=firstName.trim();
				if(!lastName.trim().equalsIgnoreCase("na"))
					fullName=fullName+" "+lastName;
				for(Customer c:customerlist) {
					if(c.isCompany()) {
						if(c.getCompanyName().equalsIgnoreCase(compName)) {
							return c;
						}
					}else {
						if(c.getFullname().equalsIgnoreCase(fullName)) {
							return c;
						}
					}
				}
				return cust;
			}
			public Customer getCustomerByName(List<Customer> customerlist,String compName) {
				Customer cust=null;
				
				for(Customer c:customerlist) {
					if(c.isCompany()) {
						if(c.getCompanyName().equalsIgnoreCase(compName)) {
							return c;
						}
					}else {
						if(c.getFullname().equalsIgnoreCase(compName)) {
							return c;
						}
					}
				}
				return cust;
			}

			@Override
			public ArrayList<String> ReverseServiceCompletionAndRemoveDuplicate() {

				ArrayList<String> errorList=new ArrayList<String>();
				ArrayList<String> exceldatalist =  readExcelAndGetArraylist(DocumentUploadTaskQueue.contractUploadBlobKey);
				ServerAppUtility serverapputility = new ServerAppUtility();

				if (exceldatalist == null || exceldatalist.size() == 0) {
					errorList.add("File");
					errorList.add("Unable read excel data please try again!");	
					CsvWriter.reverseServiceCompletionErrorList = errorList;
					return errorList;
				}
				if (exceldatalist.get(0).trim().equalsIgnoreCase("Service ID")) {
					logger.log(Level.SEVERE, "headers matched");
					int count = exceldatalist.size();
					if(count>500){
						errorList.add("File");
						errorList.add("Upload excel sheet should not be more than 500 records!");
						CsvWriter.reverseServiceCompletionErrorList = errorList;
						return errorList;
					}
				
					logger.log(Level.SEVERE, "Excel Read Successfully! count="+count);
					BlobKey blobkeyValue = DocumentUploadTaskQueue.contractUploadBlobKey;
					String strblobkey = blobkeyValue.getKeyString();
					logger.log(Level.SEVERE, "strblobkey"+strblobkey);
					
					ArrayList<Integer> serviceidlist=new ArrayList<Integer>();
					
					for(int i=1;i<exceldatalist.size();i++) {
						
						String validCustID=serverapputility.validateNumericCloumn(exceldatalist.get(0),exceldatalist.get(i));
						
						if(!validCustID.equals("")) {
							errorList.add(exceldatalist.get(i));
							errorList.add(validCustID);
						}
						serviceidlist.add(Integer.parseInt(exceldatalist.get(i).trim()));
					}
					if(errorList.size()>0) {
						CsvWriter.reverseServiceCompletionErrorList = errorList;
						return errorList;
					}
					
					List<Company> companyList = ofy().load().type(Company.class).list();
					Company comp=null;
					if(companyList!=null) {
						if(companyList.size()==1)
							comp=companyList.get(0);
						else {
							for(Company c:companyList) {
								if(c.getAccessUrl().equals("my"))
									comp=c;
							}
						}
					}
				
					if(serviceidlist!=null&&serviceidlist.size()>0) {
						List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", comp.getCompanyId()).filter("count IN", serviceidlist).list();
						if(servicelist!=null&&servicelist.size()>0) {
							logger.log(Level.SEVERE, "servicelist size "+servicelist.size());
							HashMap<Integer,Service> serviceIdWiseMap=new HashMap<Integer, Service>();
							for(Service s:servicelist) {
								serviceIdWiseMap.put(s.getCount(), s);
							}
							logger.log(Level.SEVERE, "serviceIdWiseMap size "+serviceIdWiseMap.size());
							 SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();		 
								
							long finalno=numGen.getLastUpdatedNumber("Service",comp.getCompanyId(), serviceIdWiseMap.size());
								
							ArrayList<Service> updatedServicesList=new ArrayList<Service>();
							for(Service s:servicelist) {
								if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)&& (s.getUptestReport()==null||s.getUptestReport().getName()==null||s.getUptestReport().getName().equals(""))) {
									int serId= (int) finalno;
									s.setStatus(Service.SERVICESTATUSSCHEDULE);
									s.setServiceCompleteDuration(0);
									s.setServiceCompleteRemark("");
									s.setServiceCompletionDate(null);
									errorList.add(s.getCount()+"");
									errorList.add(serId+"");	
									s.setCount(serId);
									updatedServicesList.add(s);
									finalno++;
								}else {
									errorList.add(s.getCount()+"");
									errorList.add("No modification done as Service status is "+s.getStatus());	
								}
							}
							ofy().save().entities(updatedServicesList);
							logger.log(Level.SEVERE, "updatedServicesList size "+updatedServicesList.size());
														
						}
					}
					
					DocumentUploadTaskQueue.contractUploadBlobKey = null;
					
				}else {
					logger.log(Level.SEVERE, "headers not matched");
					errorList.add("File");
					errorList.add("Invalid excel sheet new");					
				}
				CsvWriter.reverseServiceCompletionErrorList = errorList;
				return errorList;
			
			}
}
