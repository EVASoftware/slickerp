package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.services.CustomerNameChangeService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CustomerNameChangeServiceImpl extends RemoteServiceServlet implements CustomerNameChangeService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2460953498288472309L;

	Logger logger = Logger.getLogger("Name of logger");

	ArrayList<Integer> intlist = new ArrayList<Integer>();
	boolean custFlag = false;
	Customer customer;

	@Override
	public ArrayList<Integer> SaveCustomerChangeInAllDocs(Customer model, boolean customerAddressUpdateFlag) {

		
		/**
		 * Date 22-03-2019 by Vijay Des :- for customer name updation moved into task
		 * queue. belew same method i have used in task queue to call
		 * UpdateAllDocuments(model)
		 */
		String iscompany = "No";
		if (model.isCompany()) {
			iscompany = "Yes";
		}
		String cellNo = model.getCellNumber1() + "";
		String customerId = model.getCount() + "";
		String emailId = "";
		if (model.getEmail() != null) {
			emailId = model.getEmail();
		}

//		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
//		Queue queue = QueueFactory.getQueue("CustomerNameChangeTaskQueue-queue");
//		queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerNameChangeTaskQueue").param("OperationName", "CustomerNameChangeUpdation")
//				.param("companyId", model.getCompanyId()+"").param("isCompany", iscompany).param("CompanyName", model.getCompanyName())
//				.param("FullName", model.getFullname()).param("CellNumber", cellNo).param("CustomerId", customerId).param("email",emailId));

		logger.log(Level.SEVERE, "Inside SaveCustomerImpl for customer : " + customerId+" name : "+model.getFullname()+" cell no : "+cellNo);

		if (customerAddressUpdateFlag) {
			String updatedServiceAddress = getCustomerAddress(model.getSecondaryAdress());
			String updatedBillingAddress = getCustomerAddress(model.getAdress());
			Queue queue = QueueFactory.getQueue("CustomerNameChangeTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerNameChangeTaskQueue")
					.param("OperationName", "CustomerNameChangeUpdation").param("companyId", model.getCompanyId() + "")
					.param("isCompany", iscompany).param("CompanyName", model.getCompanyName())
					.param("FullName", model.getFullname()).param("CellNumber", cellNo).param("CustomerId", customerId)
					.param("email", emailId).param("updateCustomerAddress", "yes")
					.param("customerServiceAddress", updatedServiceAddress)
					.param("customerBillingAddress", updatedBillingAddress));

		} else {
			Queue queue = QueueFactory.getQueue("CustomerNameChangeTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerNameChangeTaskQueue")
					.param("OperationName", "CustomerNameChangeUpdation").param("companyId", model.getCompanyId() + "")
					.param("isCompany", iscompany).param("CompanyName", model.getCompanyName())
					.param("FullName", model.getFullname()).param("CellNumber", cellNo).param("CustomerId", customerId)
					.param("email", emailId).param("updateCustomerAddress", "no"));

		}

		return intlist;
	}

	private String getCustomerAddress(Address address) {
		String serviceAddress = address.getAddrLine1() + "$@" + address.getAddrLine2() + "$@" + address.getLandmark()
				+ "$@" + address.getLocality() + "$@" + address.getCity() + "$@" + address.getState() + "$@"
				+ address.getCountry() + "$@" + address.getPin() + "";
		return serviceAddress;
	}

	public void UpdateAllDocuments(Customer model, String customerAddressUpdatedFlag) {
		boolean updateCustomerAddressFlag = false;
		logger.log(Level.SEVERE,
				"customerAddressUpdatedFlag " + customerAddressUpdatedFlag + " customer id : " + model.getCount());

		if (customerAddressUpdatedFlag.trim().equals("yes")) {
			updateCustomerAddressFlag = true;
			logger.log(Level.SEVERE, "Billing Address " + model.getAdress().getCompleteAddress());
			logger.log(Level.SEVERE, "Service Address " + model.getSecondaryAdress().getCompleteAddress());

		}
		logger.log(Level.SEVERE, "updateCustomerAddressFlag " + updateCustomerAddressFlag);

		String servicemodule = SaveCustomerNameChnageInServiceRelatedEntity(model, updateCustomerAddressFlag);
		logger.log(Level.SEVERE, "after service module updated" + servicemodule);
		SaveCustomerNameChangeInSalesRelatedEntity(model, updateCustomerAddressFlag);

		/****
		 * Date 25-1-2019
		 * 
		 * @author Amol for to update the customer name in cnc when we change the
		 *         customer name from customer
		 ****/
		SaveCustomerNameChangeInCNCRelatedEntity(model);
	}

	/**********************************
	 * Service Entity updation Start here
	 * 
	 * @param customerAddressUpdatedFlag
	 * @return
	 *****************************************/

	private String SaveCustomerNameChnageInServiceRelatedEntity(Customer model, boolean customerAddressUpdatedFlag) {

		/**
		 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
		 * first we assign new customer id manually in the system then we will update
		 * all documents according to customer Name
		 */
		boolean processConfigFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("DuplicateCustomerID",
				"UpdateAllDocumentsAccordingToFullName", model.getCompanyId());

		boolean updateAllserviceFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service",
				AppConstants.PC_UPDATEALLSERVICEADDRESS, model.getCompanyId());
		/*********
		 * here i am updating customer name in customer Branch entity
		 **********************/
		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<CustomerBranchDetails> customerbranchdetailsEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					customerbranchdetailsEntity = ofy().load().type(CustomerBranchDetails.class)
							.filter("companyId", model.getCompanyId()).filter("cinfo.fullName", model.getCompanyName())
							.list();
				else
					customerbranchdetailsEntity = ofy().load().type(CustomerBranchDetails.class)
							.filter("companyId", model.getCompanyId()).filter("cinfo.fullName", model.getFullname())
							.list();

			} else {
				customerbranchdetailsEntity = ofy().load().type(CustomerBranchDetails.class)
						.filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
			}

//			List<CustomerBranchDetails> customerbranchdetailsEntity = ofy().load().type(CustomerBranchDetails.class).filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
			logger.log(Level.SEVERE, "customer Branch entity Size === " + customerbranchdetailsEntity.size());

			if (customerbranchdetailsEntity.size() > 0) {
				for (int i = 0; i < customerbranchdetailsEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						customerbranchdetailsEntity.get(i).setCinfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						customerbranchdetailsEntity.get(i).setCinfo(personinfo);
					}

				}
				ofy().save().entities(customerbranchdetailsEntity);
				logger.log(Level.SEVERE, " Here customer Branch entity updated successfully");
				intlist.add(9);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here customer Branch entity updation" + e);
		}
		/**********************************
		 * customer Branch entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in Complain entity
			 **********************/
			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<Complain> complainEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					complainEntity = ofy().load().type(Complain.class).filter("companyId", model.getCompanyId())
							.filter("personinfo.fullName", model.getCompanyName()).list();
				else
					complainEntity = ofy().load().type(Complain.class).filter("companyId", model.getCompanyId())
							.filter("personinfo.fullName", model.getFullname()).list();

			} else {
				complainEntity = ofy().load().type(Complain.class).filter("companyId", model.getCompanyId())
						.filter("personinfo.count", model.getCount()).list();
			}

//			List<Complain> complainEntity = ofy().load().type(Complain.class).filter("companyId", model.getCompanyId()).filter("personinfo.count", model.getCount()).list();
			logger.log(Level.SEVERE, "Complain entity Size === " + complainEntity.size());

			if (complainEntity.size() > 0) {
				for (int i = 0; i < complainEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						complainEntity.get(i).setPersoninfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						complainEntity.get(i).setPersoninfo(personinfo);
					}

					if (customerAddressUpdatedFlag) {
						complainEntity.get(i).setServiceAddress(model.getSecondaryAdress().getCompleteAddress());
					}

				}
				ofy().save().entities(complainEntity);
				logger.log(Level.SEVERE, " Here Complain entity updated successfully");

				intlist.add(11);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here Complain entity updation" + e);
		}
		/**********************************
		 * Complain entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in InteractionType entity
			 **********************/

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<InteractionType> interactiontypeEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					interactiontypeEntity = ofy().load().type(InteractionType.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).list();
				else
					interactiontypeEntity = ofy().load().type(InteractionType.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).list();

			} else {
				interactiontypeEntity = ofy().load().type(InteractionType.class)
						.filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			}

//			List<InteractionType> interactiontypeEntity = ofy().load().type(InteractionType.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			logger.log(Level.SEVERE, "InteractionType entity Size === " + interactiontypeEntity.size());

			if (interactiontypeEntity.size() > 0) {
				for (int i = 0; i < interactiontypeEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						interactiontypeEntity.get(i).setPersonInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						interactiontypeEntity.get(i).setPersonInfo(personinfo);
					}

				}
				ofy().save().entities(interactiontypeEntity);
				logger.log(Level.SEVERE, " Here InteractionType entity updated successfully");

				intlist.add(12);
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Here InteractionType entity updation" + e);

		}
		/**********************************
		 * InteractionType entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in ContractRenewal entity
			 **********************/

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<ContractRenewal> contractRenewalEntity = null;
			if (processConfigFlag) {
//				 ContactPersonEntity = ofy().load().type(ContactPersonIdentification.class).filter("companyId", model.getCompanyId()).filter("personInfo.fullName", model.getFullname()).list();
				if (model.isCompany())
					contractRenewalEntity = ofy().load().type(ContractRenewal.class)
							.filter("companyId", model.getCompanyId()).filter("customerName", model.getCompanyName())
							.list();
				else
					contractRenewalEntity = ofy().load().type(ContractRenewal.class)
							.filter("companyId", model.getCompanyId()).filter("customerName", model.getFullname())
							.list();

			} else {
				contractRenewalEntity = ofy().load().type(ContractRenewal.class)
						.filter("companyId", model.getCompanyId()).filter("customerId", model.getCount()).list();

			}

//			List<ContractRenewal> contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId", model.getCompanyId()).filter("customerId", model.getCount()).list();
			logger.log(Level.SEVERE, "Contract Renewal entity Size === " + contractRenewalEntity.size());

			if (contractRenewalEntity.size() > 0) {
				for (int i = 0; i < contractRenewalEntity.size(); i++) {
					if (model.isCompany()) {
						contractRenewalEntity.get(i).setCustomerId(model.getCount());
						contractRenewalEntity.get(i).setCustomerName(model.getCompanyName());
						contractRenewalEntity.get(i).setCustomerCell(model.getCellNumber1());

					} else {
						contractRenewalEntity.get(i).setCustomerId(model.getCount());
						contractRenewalEntity.get(i).setCustomerName(model.getFullname());
						contractRenewalEntity.get(i).setCustomerCell(model.getCellNumber1());
					}
				}
				ofy().save().entities(contractRenewalEntity);
				logger.log(Level.SEVERE, " Here contractRenewal entity updated successfully");
				intlist.add(14);
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here contractRenewal entity updation" + e);
		}

		/*********
		 * here i am updating customer name in Quotation entity
		 **********************/
		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<Quotation> quotationEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					quotationEntity = ofy().load().type(Quotation.class).filter("companyId", model.getCompanyId())
							.filter("cinfo.fullName", model.getCompanyName()).list();
				else
					quotationEntity = ofy().load().type(Quotation.class).filter("companyId", model.getCompanyId())
							.filter("cinfo.fullName", model.getFullname()).list();

			} else {
				quotationEntity = ofy().load().type(Quotation.class).filter("companyId", model.getCompanyId())
						.filter("cinfo.count", model.getCount()).list();

			}
//			List<Quotation> quotationEntity = ofy().load().type(Quotation.class).filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
			System.out.println("Quotation entity Size === " + quotationEntity.size());
			logger.log(Level.SEVERE, "Quotation entity Size === " + quotationEntity.size());

			if (quotationEntity.size() > 0) {
				for (int i = 0; i < quotationEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						quotationEntity.get(i).setCinfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						quotationEntity.get(i).setCinfo(personinfo);
					}
					if (customerAddressUpdatedFlag) {
						quotationEntity.get(i).setServiceAddress(model.getSecondaryAdress());
						quotationEntity.get(i).setBillingAddress(model.getAdress());
					}
				}
				ofy().save().entities(quotationEntity);
				System.out.println(" Here Quotation entity updated successfully");
				logger.log(Level.SEVERE, " Here Quotation entity updated successfully");

				intlist.add(2);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here Quotation entity updation" + e);
		}
		/**********************************
		 * Quotation entity end here
		 ***************************************************/

		/*********
		 * here i am updating customer name in Contract entity
		 **********************/

		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */

			List<Contract> contractEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					contractEntity = ofy().load().type(Contract.class).filter("companyId", model.getCompanyId())
							.filter("cinfo.fullName", model.getCompanyName()).list();
				else
					contractEntity = ofy().load().type(Contract.class).filter("companyId", model.getCompanyId())
							.filter("cinfo.fullName", model.getFullname()).list();

			} else {
				contractEntity = ofy().load().type(Contract.class).filter("companyId", model.getCompanyId())
						.filter("cinfo.count", model.getCount()).list();

			}

//			List<Contract> contractEntity = ofy().load().type(Contract.class).filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
			System.out.println("Contract entity Size === " + contractEntity.size());
			logger.log(Level.SEVERE, "Contract entity Size === " + contractEntity.size());

			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "UpdateAddress",
					contractEntity.get(0).getCompanyId())) {
				custFlag = true;
			}

			if (custFlag) {
				customer = ofy().load().type(Customer.class).filter("companyId", model.getCompanyId())
						.filter("count", model.getCount()).first().now();
			}
			if (contractEntity.size() > 0) {
				for (int i = 0; i < contractEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						if (model.getEmail() != null) {
							personinfo.setEmail(model.getEmail());
						}
						contractEntity.get(i).setCinfo(personinfo);

						contractEntity.get(i).setNewcompanyname(model.getCompanyName());
						contractEntity.get(i).setNewcustomerfullName(model.getFullname());
						contractEntity.get(i).setNewcustomercellNumber(model.getCellNumber1());
						contractEntity.get(i).setNewcustomerEmail(model.getEmail());
						if (custFlag) {
							logger.log(Level.SEVERE, "CUSTOMER FLAG IN CONTRACT 1.1" + custFlag);
							contractEntity.get(i).setNewcustomerAddress(customer.getAdress());
							contractEntity.get(i).setCustomerServiceAddress(customer.getSecondaryAdress());
							logger.log(Level.SEVERE, "CUSTOMER Billing Address IN CONTRACT" + customer.getAdress());
							logger.log(Level.SEVERE,
									"CUSTOMER Service Address IN CONTRACT" + customer.getSecondaryAdress());
						}
						contractEntity.get(i).setCustomersaveflag(true);

					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						if (model.getEmail() != null) {
							personinfo.setEmail(model.getEmail());
						}
						contractEntity.get(i).setCinfo(personinfo);
						if (custFlag) {
							logger.log(Level.SEVERE, "CUSTOMER FLAG1  IN CONTRACT 1.2" + custFlag);
							contractEntity.get(i).setNewcustomerAddress(customer.getAdress());
							contractEntity.get(i).setCustomerServiceAddress(customer.getSecondaryAdress());

							logger.log(Level.SEVERE,
									"CUSTOMER Billing Addresssssss" + customer.getAdress().getCompleteAddress());
							logger.log(Level.SEVERE, "CUSTOMER Service Addresssssss"
									+ customer.getSecondaryAdress().getCompleteAddress());
						}

						contractEntity.get(i).setNewcustomerfullName(model.getFullname());
						contractEntity.get(i).setNewcustomercellNumber(model.getCellNumber1());
						contractEntity.get(i).setNewcustomerEmail(model.getEmail());
						contractEntity.get(i).setNewcompanyname("");

						contractEntity.get(i).setCustomersaveflag(true);
					}
					if (customerAddressUpdatedFlag) {
						contractEntity.get(i).setCustomerServiceAddress(model.getSecondaryAdress());
						contractEntity.get(i).setNewcustomerAddress(model.getAdress());

					}

				}
				ofy().save().entities(contractEntity);
				System.out.println(" Here Contract entity updated successfully");
				logger.log(Level.SEVERE, " Here Contract entity updated successfully");

				intlist.add(3);
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here Contract entity updation" + e);
		}
		/**********************************
		 * Contract entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in fumigation entity
			 **********************/
			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<Fumigation> fumigationEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					fumigationEntity = ofy().load().type(Fumigation.class).filter("companyId", model.getCompanyId())
							.filter("cInfo.fullName", model.getCompanyName()).list();
				else
					fumigationEntity = ofy().load().type(Fumigation.class).filter("companyId", model.getCompanyId())
							.filter("cInfo.fullName", model.getFullname()).list();

			} else {
				fumigationEntity = ofy().load().type(Fumigation.class).filter("companyId", model.getCompanyId())
						.filter("cInfo.count", model.getCount()).list();

			}

//			List<Fumigation> fumigationEntity = ofy().load().type(Fumigation.class).filter("companyId", model.getCompanyId()).filter("cInfo.count", model.getCount()).list();
			System.out.println("fumigation entity Size === " + fumigationEntity.size());
			logger.log(Level.SEVERE, "fumigation entity Size === " + fumigationEntity.size());

			if (fumigationEntity.size() > 0) {
				for (int i = 0; i < fumigationEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						fumigationEntity.get(i).setcInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						fumigationEntity.get(i).setcInfo(personinfo);
					}

				}
				ofy().save().entities(fumigationEntity);
				System.out.println(" Here fumigation entity updated successfully");
				logger.log(Level.SEVERE, " Here fumigation entity updated successfully");

				intlist.add(10);
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here fumigation entity updation" + e);
		}

		/**********************************
		 * fumigation entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in contact Person entity
			 **********************/

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<ContactPersonIdentification> ContactPersonEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					ContactPersonEntity = ofy().load().type(ContactPersonIdentification.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).list();
				else
					ContactPersonEntity = ofy().load().type(ContactPersonIdentification.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).list();

			} else {
				ContactPersonEntity = ofy().load().type(ContactPersonIdentification.class)
						.filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			}

//			List<ContactPersonIdentification> ContactPersonEntity = ofy().load().type(ContactPersonIdentification.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			System.out.println("contact Person entity Size === " + ContactPersonEntity.size());
			logger.log(Level.SEVERE, "contact Person entity Size === " + ContactPersonEntity.size());

			if (ContactPersonEntity.size() > 0) {
				for (int i = 0; i < ContactPersonEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						ContactPersonEntity.get(i).setPersonInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						ContactPersonEntity.get(i).setPersonInfo(personinfo);
					}

				}
				ofy().save().entities(ContactPersonEntity);
				System.out.println(" Here contact Person entity updated successfully");
				logger.log(Level.SEVERE, " Here contact Person entity updated successfully");

				intlist.add(13);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here contact Person entity updation" + e);

		}
		/**********************************
		 * contact Person entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in lead entity
			 **********************/
			List<Lead> leadEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					leadEntity = ofy().load().type(Lead.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).list();
				else
					leadEntity = ofy().load().type(Lead.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).list();

			} else {
				leadEntity = ofy().load().type(Lead.class).filter("companyId", model.getCompanyId())
						.filter("personInfo.count", model.getCount()).list();
			}
//			List<Lead> leadEntity = ofy().load().type(Lead.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			logger.log(Level.SEVERE, "lead entity Size === " + leadEntity.size());

			if (leadEntity.size() > 0) {
				for (int i = 0; i < leadEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						leadEntity.get(i).setPersonInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						leadEntity.get(i).setPersonInfo(personinfo);
					}

				}
				ofy().save().entities(leadEntity);
				logger.log(Level.SEVERE, " Here lead entity updated successfully");

				intlist.add(1);
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here lead entity updation" + e);
		}
		/**********************************
		 * lead entity end here
		 ***************************************************/

		/*********
		 * here i am updating customer name in Billing entity
		 **********************/
		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<BillingDocument> billingEntity = null;

			/*
			 * Ashwini Patil Date:15-03-2024 added .filter("accountType", "AR") while
			 * loading payments beacause vendor payments are also getting updated if vendor
			 * id and customer id is same in the system.
			 */
			if (processConfigFlag) {
				if (model.isCompany())
					billingEntity = ofy().load().type(BillingDocument.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).filter("accountType", "AR").list();
				else
					billingEntity = ofy().load().type(BillingDocument.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).filter("accountType", "AR").list();

			} else {
				billingEntity = ofy().load().type(BillingDocument.class).filter("companyId", model.getCompanyId())
						.filter("personInfo.count", model.getCount()).filter("accountType", "AR").list();

			}

//			List<BillingDocument> billingEntity = ofy().load().type(BillingDocument.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			System.out.println("Billing entity Size === " + billingEntity.size());
			logger.log(Level.SEVERE, "Billing entity Size === " + billingEntity.size());

			if (billingEntity.size() > 0) {
				for (int i = 0; i < billingEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						billingEntity.get(i).setPersonInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						billingEntity.get(i).setPersonInfo(personinfo);
					}

				}
				ofy().save().entities(billingEntity);
				System.out.println(" Here Billing entity updated successfully");
				logger.log(Level.SEVERE, " Here Billing entity updated successfully");

				intlist.add(6);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here Billing entity updation" + e);
		}
		/**********************************
		 * Billing entity end here
		 ***************************************************/

		/*********
		 * here i am updating customer name in Invoice entity
		 **********************/
		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<Invoice> invoiceEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).list();
				else
					invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).list();

			} else {
				invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", model.getCompanyId())
						.filter("personInfo.count", model.getCount()).list();
			}

//			List<Invoice> invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			System.out.println("Invoice entity Size === " + invoiceEntity.size());
			logger.log(Level.SEVERE, "Invoice entity Size === " + invoiceEntity.size());

			if (invoiceEntity.size() > 0) {
				for (int i = 0; i < invoiceEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						invoiceEntity.get(i).setPersonInfo(personinfo);

					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						invoiceEntity.get(i).setPersonInfo(personinfo);
					}

				}
				ofy().save().entities(invoiceEntity);
				System.out.println(" Here Invoice entity updated successfully");
				logger.log(Level.SEVERE, " Here Invoice entity updated successfully");

				intlist.add(7);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here Invoice entity updation" + e);
		}
		/**********************************
		 * Invoice entity end here
		 ***************************************************/

		/*********
		 * here i am updating customer name in Payment entity
		 **********************/
		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<CustomerPayment> customerpaymentEntity = null;
			/*
			 * Ashwini Patil Date:15-03-2024 added .filter("accountType", "AR") while
			 * loading payments beacause vendor payments are also getting updated if vendor
			 * id and customer id is same in the system.
			 */
			if (processConfigFlag) {
				if (model.isCompany())
					customerpaymentEntity = ofy().load().type(CustomerPayment.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).filter("accountType", "AR").list();
				else
					customerpaymentEntity = ofy().load().type(CustomerPayment.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).filter("accountType", "AR").list();

			} else {
				customerpaymentEntity = ofy().load().type(CustomerPayment.class)
						.filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount())
						.filter("accountType", "AR").list();
			}

//			List<CustomerPayment> customerpaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			System.out.println("Payment entity Size === " + customerpaymentEntity.size());
			logger.log(Level.SEVERE, "Payment entity Size === " + customerpaymentEntity.size());

			if (customerpaymentEntity.size() > 0) {
				for (int i = 0; i < customerpaymentEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						customerpaymentEntity.get(i).setPersonInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						customerpaymentEntity.get(i).setPersonInfo(personinfo);
					}

				}
				ofy().save().entities(customerpaymentEntity);
				System.out.println(" Here Payment entity updated successfully");
				logger.log(Level.SEVERE, " Here Payment entity updated successfully");

				intlist.add(8);
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, " Here Payment entity updation" + e);
		}
		/**********************************
		 * Payment entity end here
		 ***************************************************/

		try {

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<Service> serviceEntity = null;
			if (processConfigFlag || updateAllserviceFlag) {
				if (model.isCompany())
					serviceEntity = ofy().load().type(Service.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).list();
				else
					serviceEntity = ofy().load().type(Service.class).filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).list();
			} else {
				/**
				 * @author Anil @since 18-08-2021 If customer has large number of services to
				 *         update then task gets failed so in below method loading services in
				 *         partially and update them and continue to do that until all services
				 *         gets updated Raised by Ashwini for Pecopp
				 */
//				serviceEntity = ofy().load().type(Service.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
				logger.log(Level.SEVERE, "Inside of else part of Service Updation ");
				Service serviceObject = ofy().load().type(Service.class).filter("companyId", model.getCompanyId())
						.filter("personInfo.count", model.getCount()).first().now();
				String oldCustName = null;
				long oldCustNum = 0;
				boolean custCellNullFlag = false;
				if (serviceObject != null) {
					oldCustName = serviceObject.getPersonInfo().getFullName();
					if (serviceObject.getPersonInfo().getCellNumber() != null) // Ashwini Patil Date:4-04-2024 program
																				// was not updating services because of
																				// null pointer exception
						oldCustNum = serviceObject.getPersonInfo().getCellNumber();
					else
						custCellNullFlag = true;

					logger.log(Level.SEVERE, "OLD NAME AND NUM : " + oldCustName + " / " + oldCustNum
							+ " NEW NAME AND NUM : " + model.getCompanyName() + " / " + model.getCellNumber1());
					if (!model.getCompanyName().equals(oldCustName) || model.getCellNumber1() != oldCustNum) {
						updateServices(model, oldCustName, oldCustNum, customerAddressUpdatedFlag, custCellNullFlag);
					}
				}

			}

			/*********
			 * here i am updating customer name in Service entity
			 **********************/
//			List<Service> serviceEntity = ofy().load().type(Service.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			
			if (serviceEntity != null && serviceEntity.size() > 0) {
				System.out.println("Service entity Size === " + serviceEntity.size());
				logger.log(Level.SEVERE, "Service entity Size === " + serviceEntity.size());

				for (int i = 0; i < serviceEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						serviceEntity.get(i).setPersonInfo(personinfo);
						if (custFlag) {
							logger.log(Level.SEVERE, "CUSTOMER FLAG IN SERVICE 2.1" + custFlag);
							serviceEntity.get(i).setAddress(customer.getSecondaryAdress());
							logger.log(Level.SEVERE, "IN Service" + customer.getSecondaryAdress().getCompleteAddress());
						}
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						serviceEntity.get(i).setPersonInfo(personinfo);
						if (custFlag) {
							logger.log(Level.SEVERE, "CUSTOMER FLAG IN SERVICE 2.2" + custFlag);
							serviceEntity.get(i).setAddress(customer.getSecondaryAdress());
							logger.log(Level.SEVERE,
									"IN Serviceeeeee" + customer.getSecondaryAdress().getCompleteAddress());
						}
					}

					/*
					 * Ashwini Patil Date:28-08-2023 Added "service address" condition to avoid
					 * address updation in services of customer branches. Issue faced and reported
					 * by Orion
					 */
					//Ashwini Patil Date:29-04-2023 Added if service not completed then only change address
					if (customerAddressUpdatedFlag && serviceEntity.get(i).getServiceBranch().equals("Service Address")&&!serviceEntity.get(i).getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
						serviceEntity.get(i).setAddress(model.getSecondaryAdress());
					}
				}
				ofy().save().entities(serviceEntity);
				System.out.println(" Here Service entity updated successfully");
				logger.log(Level.SEVERE, " Here Service entity updated successfully");

				intlist.add(4);
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Servie Entity updation failed" + e);
			e.printStackTrace();

		}
		/**********************************
		 * Service entity end here
		 ***************************************************/

		try {

			/*********
			 * here i am updating customer name in Service entity
			 **********************/

			/**
			 * Date 26-03-2018 By Vijay if added condition for duplicates customer id issue.
			 * first we will assign new customer id manually in the system then we will
			 * update all documents according to customer Name
			 */
			List<ServiceProject> serviceprojectEntity = null;
			if (processConfigFlag) {
				if (model.isCompany())
					serviceprojectEntity = ofy().load().type(ServiceProject.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getCompanyName()).list();
				else
					serviceprojectEntity = ofy().load().type(ServiceProject.class)
							.filter("companyId", model.getCompanyId())
							.filter("personInfo.fullName", model.getFullname()).list();

			} else {
				serviceprojectEntity = ofy().load().type(ServiceProject.class).filter("companyId", model.getCompanyId())
						.filter("personInfo.count", model.getCount()).list();
			}

//			List<ServiceProject> serviceprojectEntity = ofy().load().type(ServiceProject.class).filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
			System.out.println("Service project entity Size === " + serviceprojectEntity.size());
			logger.log(Level.SEVERE, "Service project entity Size === " + serviceprojectEntity.size());

			if (serviceprojectEntity.size() > 0) {
				for (int i = 0; i < serviceprojectEntity.size(); i++) {
					if (model.isCompany()) {
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getCompanyName());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						serviceprojectEntity.get(i).setPersonInfo(personinfo);
					} else {
						System.out.println(" for normal customer ");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(model.getFullname());
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
						personinfo.setCount(model.getCount());
						serviceprojectEntity.get(i).setPersonInfo(personinfo);
					}

					/**
					 * @author Anil , Date : 22-11-2019
					 */
					serviceprojectEntity.get(i).setCustNameChangeFlag(true);

					if (customerAddressUpdatedFlag) {
						serviceprojectEntity.get(i).setAddr(model.getAdress());
					}
				}
				ofy().save().entities(serviceprojectEntity);
				System.out.println(" Here Service project entity updated successfully");
				logger.log(Level.SEVERE, " Here Service project entity updated successfully");

				intlist.add(5);
			}

			/**********************************
			 * Service entity end here
			 ***************************************************/

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Servie Project updation" + e);

		}
		
		/*
		 * Ashwini Patil
		 * Date: 11-06-2024
		 * Ultra reported that update customer address program is not updating accounting interface entries
		 */
		try {
			List<AccountingInterface> AccountingInterfaceEntity = null;
			AccountingInterfaceEntity = ofy().load().type(AccountingInterface.class)
						.filter("companyId", model.getCompanyId())
						.filter("customerID", model.getCount()).list();
			
			if(AccountingInterfaceEntity!=null) {
				for(AccountingInterface ac:AccountingInterfaceEntity) {
					if (model.isCompany()) {
						ac.setCustomerName(model.getCompanyName());						
						ac.setCustomerCell(model.getCellNumber1());
					} else {
						ac.setCustomerName(model.getFullname());
						ac.setCustomerCell(model.getCellNumber1());
					}
				}
				ofy().save().entities(AccountingInterfaceEntity);
				logger.log(Level.SEVERE, "AccountingInterface updation completed");
			}
		}catch(Exception e) {
			logger.log(Level.SEVERE, "AccountingInterface updation" + e);
		}
		return "Success";
	}

	/**
	 * @author Anil @since 18-08-2021 This method loads 1000 services at time at a
	 *         time based on old name and number and update those with updated name
	 *         and number until all services gets updated
	 * @param model
	 * @param oldCustName
	 * @param oldCustNum
	 * @param customerAddressUpdatedFlag
	 */
	private void updateServices(Customer model, String oldCustName, long oldCustNum, boolean customerAddressUpdatedFlag,
			boolean custCellNullFlag) {
		List<Service> serviceEntity = null;
		if (custCellNullFlag)
			serviceEntity = ofy().load().type(Service.class).filter("companyId", model.getCompanyId())
					.filter("personInfo.count", model.getCount()).limit(1000).list();
		else
			serviceEntity = ofy().load().type(Service.class).filter("companyId", model.getCompanyId())
					.filter("personInfo.count", model.getCount()).filter("personInfo.fullName", oldCustName)
					.filter("personInfo.cellNumber", oldCustNum).limit(1000).list();

		
		if (serviceEntity != null && serviceEntity.size() > 0) {
			logger.log(Level.SEVERE, "In updateServices Service entity Size === " + serviceEntity.size());

			for (int i = 0; i < serviceEntity.size(); i++) {

				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getCompanyName());
					personinfo.setCount(model.getCount());
					/**
					 * Added By Priyanka Des : the cell number, POC Name will update only for
					 * Service Address's of Service. Requiremnet raised by Rahul For PestAid.
					 */
					if (serviceEntity.get(i).getServiceBranch()!=null&&serviceEntity.get(i).getServiceBranch().equals("Service Address")) {
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
					}else {
						logger.log(Level.SEVERE,"ServiceBranch is not serviceaddress");
						//Ashwini Patil Date:4-04-2024 added blank check condition as ultra pest's customer number is not getting updated in services if customer branch is blank
						if(serviceEntity.get(i).getServiceBranch()==null) {
							personinfo.setPocName(model.getFullname());
							personinfo.setCellNumber(model.getCellNumber1());
						}else if(serviceEntity.get(i).getServiceBranch().equals("SELECT")||serviceEntity.get(i).getServiceBranch().length()==0){
							logger.log(Level.SEVERE, "ServiceBranch length=" + serviceEntity.get(i).getServiceBranch().length()+" branchname="+serviceEntity.get(i).getServiceBranch());
							personinfo.setPocName(model.getFullname());
							personinfo.setCellNumber(model.getCellNumber1());
						}
					}
					serviceEntity.get(i).setPersonInfo(personinfo);
					if (custFlag) {
//						logger.log(Level.SEVERE, "CUSTOMER FLAG IN SERVICE 2.1" + custFlag);
						serviceEntity.get(i).setAddress(customer.getSecondaryAdress());
//						logger.log(Level.SEVERE, "IN Service" + customer.getSecondaryAdress().getCompleteAddress());
					}
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getFullname());
//						personinfo.setPocName(model.getFullname());
//						personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					/**
					 * Added By Priyanka Des : the cell number, POC Name will update only for
					 * Service Address's of Service. Requiremnet raised by Rahul For PestAid.
					 */
					if (serviceEntity.get(i).getServiceBranch()!=null&&serviceEntity.get(i).getServiceBranch().equals("Service Address")) {
						personinfo.setPocName(model.getFullname());
						personinfo.setCellNumber(model.getCellNumber1());
					}else {
						logger.log(Level.SEVERE,"ServiceBranch is not serviceaddress");
						//Ashwini Patil Date:4-04-2024 added blank check condition as ultra pest's customer number is not getting updated in services if customer branch is blank
						if(serviceEntity.get(i).getServiceBranch()==null) {
							personinfo.setPocName(model.getFullname());
							personinfo.setCellNumber(model.getCellNumber1());
						}else if(serviceEntity.get(i).getServiceBranch().equals("SELECT")||serviceEntity.get(i).getServiceBranch().length()==0){
							logger.log(Level.SEVERE, "ServiceBranch length=" + serviceEntity.get(i).getServiceBranch().length()+" branchname="+serviceEntity.get(i).getServiceBranch());
							personinfo.setPocName(model.getFullname());
							personinfo.setCellNumber(model.getCellNumber1());
						}
						
					}
					serviceEntity.get(i).setPersonInfo(personinfo);
					if (custFlag) {
//						logger.log(Level.SEVERE, "CUSTOMER FLAG IN SERVICE 2.2" + custFlag);
						serviceEntity.get(i).setAddress(customer.getSecondaryAdress());
//						logger.log(Level.SEVERE,
//								"IN Serviceeeeee" + customer.getSecondaryAdress().getCompleteAddress());
					}
				}

				/*
				 * Ashwini Patil Date:28-08-2023 Added "service address" condition to avoid
				 * address updation in services of customer branches. Issue faced and reported
				 * by Orion
				 */
				//Ashwini Patil Date:29-04-2023 Added if service not completed then only change address
				if (customerAddressUpdatedFlag && serviceEntity.get(i).getServiceBranch().equals("Service Address")&&!serviceEntity.get(i).getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
					serviceEntity.get(i).setAddress(model.getSecondaryAdress());
				}
			}
			ofy().save().entities(serviceEntity).now();
			System.out.println(" Here Service entity updated successfully");
			logger.log(Level.SEVERE, " Here Service entity updated successfully");
			intlist.add(4);

			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();

			updateServices(model, oldCustName, oldCustNum, customerAddressUpdatedFlag, custCellNullFlag);
		}
	}

	/**********************************
	 * Serivce Entity updation end here
	 * 
	 * @param updateCustomerAddressFlag
	 *****************************************/

	private void SaveCustomerNameChangeInSalesRelatedEntity(Customer model, boolean updateCustomerAddressFlag) {

//		ArrayList<Integer> intlist = new ArrayList<Integer>();

		/*********
		 * here i am updating customer name in Sales Quotation entity
		 **********************/
		List<SalesQuotation> salesquotationEntity = ofy().load().type(SalesQuotation.class)
				.filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
		System.out.println("Sales Quotation entity Size === " + salesquotationEntity.size());
		logger.log(Level.SEVERE, "Sales Quotation entity Size === " + salesquotationEntity.size());

		if (salesquotationEntity.size() > 0) {
			for (int i = 0; i < salesquotationEntity.size(); i++) {
				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getCompanyName());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					salesquotationEntity.get(i).setCinfo(personinfo);
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getFullname());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					salesquotationEntity.get(i).setCinfo(personinfo);
				}

			}
			ofy().save().entities(salesquotationEntity);
			System.out.println(" Here Sales Quotation entity updated successfully");
			logger.log(Level.SEVERE, " Here Sales Quotation entity updated successfully");

			intlist.add(14);
		}
		/**********************************
		 * Quotation entity end here
		 ***************************************************/

		/*********
		 * here i am updating customer name in Sales Order entity
		 **********************/
		List<SalesOrder> salesoerderEntity = ofy().load().type(SalesOrder.class)
				.filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
		System.out.println("Sales Order entity Size === " + salesoerderEntity.size());
		logger.log(Level.SEVERE, "Sales Order entity Size === " + salesoerderEntity.size());

		if (salesoerderEntity.size() > 0) {
			for (int i = 0; i < salesoerderEntity.size(); i++) {
				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getCompanyName());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					salesoerderEntity.get(i).setCinfo(personinfo);
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getFullname());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					salesoerderEntity.get(i).setCinfo(personinfo);
				}
				if (updateCustomerAddressFlag) {
					salesoerderEntity.get(i).setShippingAddress(model.getSecondaryAdress());
					salesoerderEntity.get(i).setNewcustomerAddress(model.getAdress());
				}
			}
			ofy().save().entities(salesoerderEntity);
			System.out.println(" Here Sales Order entity updated successfully");
			logger.log(Level.SEVERE, " Here Sales Order entity updated successfully");

			intlist.add(15);
		}
		/**********************************
		 * Sales Order entity end here
		 ***************************************************/

		/*********
		 * here i am updating customer name in delivery note entity
		 **********************/
		List<DeliveryNote> deliveryNoteEntity = ofy().load().type(DeliveryNote.class)
				.filter("companyId", model.getCompanyId()).filter("cinfo.count", model.getCount()).list();
		System.out.println("DeliveryNote Order entity Size === " + deliveryNoteEntity.size());
		logger.log(Level.SEVERE, "DeliveryNote Order entity Size === " + deliveryNoteEntity.size());

		if (deliveryNoteEntity.size() > 0) {
			for (int i = 0; i < deliveryNoteEntity.size(); i++) {
				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getCompanyName());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					deliveryNoteEntity.get(i).setCinfo(personinfo);
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getFullname());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					deliveryNoteEntity.get(i).setCinfo(personinfo);
				}

				if (updateCustomerAddressFlag) {
					deliveryNoteEntity.get(i).setShippingAddress(model.getSecondaryAdress());
				}
			}
			ofy().save().entities(deliveryNoteEntity);
			System.out.println(" Here DeliveryNote entity updated successfully");
			logger.log(Level.SEVERE, " Here DeliveryNote entity updated successfully");
			intlist.add(16);
		}

		intlist.add(17);

		/**********************************
		 * Quotation entity end here
		 ***************************************************/
	}

	/*** Date 25-1-2019 @amol ***/

	/*************************
	 * Here I am Updating Customer Name In CNC Entity
	 **************************************/

	private void SaveCustomerNameChangeInCNCRelatedEntity(Customer model) {
		List<CNC> cncEntity = ofy().load().type(CNC.class).filter("companyId", model.getCompanyId())
				.filter("personInfo.count", model.getCount()).list();
		System.out.println("CNC entity Size === " + cncEntity.size());
		logger.log(Level.SEVERE, "CNC entity Size === " + cncEntity.size());

		if (cncEntity.size() > 0) {
			for (int i = 0; i < cncEntity.size(); i++) {
				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personInfo = new PersonInfo();
					personInfo.setFullName(model.getCompanyName());
					personInfo.setPocName(model.getFullname());
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					cncEntity.get(i).setPersonInfo(personInfo);
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personInfo = new PersonInfo();
					personInfo.setFullName(model.getFullname());
					personInfo.setPocName(model.getFullname());
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					cncEntity.get(i).setPersonInfo(personInfo);
				}

			}
			ofy().save().entities(cncEntity);
			System.out.println(" Here CNC entity updated successfully");
			logger.log(Level.SEVERE, " Here CNC entity updated successfully");

			intlist.add(18);
		}

		/******************************
		 * Here I am Updating Customer Name In HR Project Entity
		 ******************************************/
		List<HrProject> hrProjectEntity = ofy().load().type(HrProject.class).filter("companyId", model.getCompanyId())
				.filter("personInfo.count", model.getCount()).list();
		System.out.println("HR project entity Size === " + hrProjectEntity.size());
		logger.log(Level.SEVERE, "HR project  entity Size === " + hrProjectEntity.size());

		if (hrProjectEntity.size() > 0) {
			for (int i = 0; i < hrProjectEntity.size(); i++) {
				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getCompanyName());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					hrProjectEntity.get(i).setPersoninfo(personinfo);
					;
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personinfo = new PersonInfo();
					personinfo.setFullName(model.getFullname());
					personinfo.setPocName(model.getFullname());
					personinfo.setCellNumber(model.getCellNumber1());
					personinfo.setCount(model.getCount());
					hrProjectEntity.get(i).setPersoninfo(personinfo);
					;
				}

			}
			ofy().save().entities(hrProjectEntity);
			System.out.println(" Here HR project  entity updated successfully");
			logger.log(Level.SEVERE, " Here HR project  entity updated successfully");
			intlist.add(19);
		}

		/******************************
		 * Here I am Updating Customer Name In Project Allocation Entity
		 ******************************************/
		List<ProjectAllocation> projectAllocationEntity = ofy().load().type(ProjectAllocation.class)
				.filter("companyId", model.getCompanyId()).filter("personInfo.count", model.getCount()).list();
		System.out.println(" Project Allocation  entity Size === " + projectAllocationEntity.size());
		logger.log(Level.SEVERE, " Project Allocation  entity Size === " + projectAllocationEntity.size());

		if (projectAllocationEntity.size() > 0) {
			for (int i = 0; i < projectAllocationEntity.size(); i++) {
				if (model.isCompany()) {
					System.out.println(" if customer is company");
					PersonInfo personInfo = new PersonInfo();
					personInfo.setFullName(model.getCompanyName());
					personInfo.setPocName(model.getFullname());
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					projectAllocationEntity.get(i).setPersonInfo(personInfo);
				} else {
					System.out.println(" for normal customer ");
					PersonInfo personInfo = new PersonInfo();
					personInfo.setFullName(model.getFullname());
					personInfo.setPocName(model.getFullname());
					personInfo.setCellNumber(model.getCellNumber1());
					personInfo.setCount(model.getCount());
					projectAllocationEntity.get(i).setPersonInfo(personInfo);
				}

			}
			ofy().save().entities(projectAllocationEntity);
			System.out.println(" Here  Project Allocation entity updated successfully");
			logger.log(Level.SEVERE, " Here Project Allocation  entity updated successfully");
			intlist.add(20);
		}

	}

	/**
	 * Date 26-4-2019
	 * 
	 * @author Amol Added a method for to update the cell no of Employee when we
	 *         updated it in employee master and it should be reflect in salary slip
	 *         ,CTC and Leave balance
	 *
	 */
	@Override
	public ArrayList<Integer> SaveEmployeeChangeInAllDocs(Employee model) {
		List<PaySlip> payslipentity = ofy().load().type(PaySlip.class).filter("companyId", model.getCompanyId())
				.filter("empid", model.getCount()).list();
		System.out.println("Payslip entity Size === " + payslipentity.size());
		logger.log(Level.SEVERE, "Payslip entity Size === " + payslipentity.size());

		if (payslipentity.size() > 0) {
			for (int i = 0; i < payslipentity.size(); i++) {
				payslipentity.get(i).setEmpCellNo(model.getCellNumber1());
				payslipentity.get(i).setEmployeeName(model.getFullName());

			}
			ofy().save().entities(payslipentity);
			System.out.println(" Here Payslip entity updated successfully");
			logger.log(Level.SEVERE, " Here Payslip entity updated successfully");

			intlist.add(21);
		}

		/********************************************
		 * Here we updated the CTC Entity
		 ***************************************************/
		List<CTC> ctcentity = ofy().load().type(CTC.class).filter("companyId", model.getCompanyId())
				.filter("empid", model.getCount()).list();
		System.out.println("Payslip entity Size === " + ctcentity.size());
		logger.log(Level.SEVERE, "Payslip entity Size === " + ctcentity.size());

		if (ctcentity.size() > 0) {
			for (int i = 0; i < ctcentity.size(); i++) {
				ctcentity.get(i).setEmpCellNo(model.getCellNumber1());
				ctcentity.get(i).setEmployeeName(model.getFullName());

			}
			ofy().save().entities(ctcentity);
			System.out.println(" Here Payslip entity updated successfully");
			logger.log(Level.SEVERE, " Here Payslip entity updated successfully");

			intlist.add(22);
		}
		/********************************************
		 * Here we updated the LeaveBalance Entity
		 ***************************************************/
		List<LeaveBalance> leaveBalanceenity = ofy().load().type(LeaveBalance.class)
				.filter("companyId", model.getCompanyId()).filter("empInfo.empCount ", model.getCount()).list();
		System.out.println("Payslip entity Size === " + leaveBalanceenity.size());
		logger.log(Level.SEVERE, "Payslip entity Size === " + leaveBalanceenity.size());

		if (leaveBalanceenity.size() > 0) {
			for (int i = 0; i < leaveBalanceenity.size(); i++) {
				leaveBalanceenity.get(i).getEmpInfo().setFullName(model.getFullName());
				leaveBalanceenity.get(i).getEmpInfo().setCellNumber(model.getCellNumber1());

			}
			ofy().save().entities(leaveBalanceenity);
			System.out.println(" Here Payslip entity updated successfully");
			logger.log(Level.SEVERE, " Here Payslip entity updated successfully");

			intlist.add(23);
		}

		/********************************************
		 * Here we updated the User Entity
		 ***************************************************/
		// Ashwini Patil Date:23-08-2023
		User user = ofy().load().type(User.class).filter("companyId", model.getCompanyId())
				.filter("empCount", model.getCount()).first().now();

		if (user != null) {
			user.setEmployeeName(model.getFullName());
			ofy().save().entities(user);
			System.out.println(" Here User entity updated successfully");
			logger.log(Level.SEVERE, " Here user entity updated successfully");

			intlist.add(24);
		}
		intlist.add(25);
		return intlist;
	}

	/**
	 * Date 22-05-2019
	 * 
	 * @author Vijay Chougule Des :- NBHC CCPM Complaint Screen service creation
	 */

//	@Override
//	public String createComplaintService(Complain complain, int complaintID) {
//		logger.log(Level.SEVERE,"Complaint Service creation");
//		Service service = new Service();
//		service.setCompanyId(complain.getCompanyId());
//		service.setPersonInfo(complain.getPersoninfo());
//		ServiceProduct serviceProdEntity = ofy().load().type(ServiceProduct.class).filter("companyId",complain.getCompanyId())
//											.filter("count", complain.getPic().getProdID()).first().now();
//		if(serviceProdEntity!=null){
//			service.setProduct(serviceProdEntity);
//		}
//		service.setBranch(complain.getBranch());
//		service.setServiceBranch(complain.getCustomerBranch());
//		service.setStatus(Service.SERVICESTATUSSCHEDULE);
//		service.setServiceType("Follow UP Service");
//		service.setServiceDate(complain.getNewServiceDate());
//		logger.log(Level.SEVERE,"Complaint Service creation 2");
//			CustomerBranchDetails customerbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", complain.getCompanyId())
//													.filter("buisnessUnitName", complain.getCustomerBranch())
//													.filter("cinfo.count", complain.getPersoninfo().getCount()).first().now();
//			if(customerbranch!=null){
//				service.setAddress(customerbranch.getAddress());
//			}
//			else{
//				Customer customer = ofy().load().type(Customer.class).filter("companyId", complain.getCompanyId())
//									.filter("count", complain.getPersoninfo().getCount()).first().now();
//				if(customer!=null){
//					service.setAddress(customer.getSecondaryAdress());
//				}
//			}
//			
//		if(complain.getServicedetails()!=null && complain.getServicedetails().size()!=0){
//			service.setContractCount(complain.getServicedetails().get(0).getContractId());
//			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", service.getCompanyId())
//										.filter("count", service.getContractCount()).first().now();
//			if(contractEntity!=null){
//				service.setContractStartDate(contractEntity.getStartDate());
//				service.setContractEndDate(contractEntity.getEndDate());
//			}
//		}
//		logger.log(Level.SEVERE,"Complaint Service creation 3");
//		service.setTicketNumber(complaintID);234
//		GenricServiceImpl impl = new GenricServiceImpl();
//		ReturnFromServer server = new ReturnFromServer();
//		server = impl.save(service);
//		logger.log(Level.SEVERE,"Complaint Service created successfully Service Id"+server.count);
//
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnableCreateAssementForComplaintService", service.getCompanyId())){
//			createAssessment(service,server.count,complaintID,service.getBranch());
//			
//			
//			logger.log(Level.SEVERE, "After Assesment Creation method ");
//		}
//
//		/**
//		 * Date 12-08-2019 by Vijay for When complaint raised then send email
//		 */
//		sendEmailForComplaintRegister(complain,complaintID);
//		
//		return "Success";
//	}

	private void sendEmailForComplaintRegister(Complain complain, int complaintID) {
		Company company = ofy().load().type(Company.class).filter("companyId", complain.getCompanyId()).first().now();

		String mailSub = "Complaint Id -" + complaintID;
		String mailMsg = "";
		StringBuilder builder = new StringBuilder();
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append(
				"<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("Dear Sir/Madam,");
		builder.append("<br>");
		builder.append("<br>");
		builder.append("Your Complaint has been registered. Your Complaint Id is " + complaintID + "</br>" + "</br>"
				+ "</br>");
		builder.append("<br>");
		builder.append("<br>");
		builder.append("<br>");

		builder.append("</br></br></br>");
		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("") && company.getAddress().getAddrLine2() != null) {
			builder.append(company.getAddress().getAddrLine1() + " " + company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		} else {
			builder.append(company.getAddress().getAddrLine1() + " " + company.getAddress().getLocality());
		}
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");

		if (!company.getAddress().getLandmark().equals("") && company.getAddress().getLandmark() != null) {
			builder.append(company.getAddress().getLandmark() + "," + company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		} else {
			builder.append(company.getAddress().getCity() + "," + company.getAddress().getPin() + ","
					+ company.getAddress().getState() + "," + company.getAddress().getCountry());
		}

		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);

		builder.append("</body>");
		builder.append("</html>");
		mailMsg = builder.toString();
		System.out.println("mailMsg" + mailMsg);
		if (complain.getEmail() != null && !complain.getEmail().equals("")) {
			Email email = new Email();
			String fromEmail = complain.getEmail();
			ArrayList<String> toList = new ArrayList<String>();
			toList.add(fromEmail);
			/**
			 * Date 30-09-2019 By Vijay Des :- NBHC CCPM Complaint creation send email cc to
			 * Technical Manager
			 */
			ArrayList<String> cclist = new ArrayList<String>();
			List<Employee> employeeList = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId())
					.filter("roleName", "Technical Manager").list();
			if (employeeList.size() != 0) {
				for (Employee emp : employeeList) {
					if (emp.getEmail() != null)
						cclist.add(emp.getEmail());
				}
			}
			logger.log(Level.SEVERE, "Email CC " + cclist);

			logger.log(Level.SEVERE, "Email Sending method");
			/**
			 * @author Anil , Date : 10-04-2020 added display name for from email id
			 *         ,company.getDisplayNameForCompanyEmailId()
			 */
			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",
					company.getCompanyId())) {
				logger.log(Level.SEVERE, " company email --" + fromEmail + "  to email  " + toList);
				SendGridEmailServlet sdEmail = new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmail, toList, cclist, null, mailSub, mailMsg, "text/html", null, null,
						null, company.getDisplayNameForCompanyEmailId());
			} else {
				email.sendMailWithGmail(company.getEmail(), toList, cclist, null, mailSub, mailMsg, "text/html", null,
						null, null);
			}
		}

	}

	private void createAssessment(Service service, int serviceId, int complaintId, String branch) {
		logger.log(Level.SEVERE, "Inside Assement Creation");
		AssesmentReport assesmentreport = new AssesmentReport();
		assesmentreport.setCinfo(service.getPersonInfo());
		assesmentreport.setCompanyId(service.getCompanyId());
		assesmentreport.setServiceId(serviceId);
		assesmentreport.setComplaintId(complaintId);
		assesmentreport.setStatus(AssesmentReport.CREATED);
		assesmentreport.setBranch(branch);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(assesmentreport);

		logger.log(Level.SEVERE, "Assement Created successfully");
	}

	/**
	 * Date 30-07-2019 by Vijay for NBHC CCPM Service revenue loss report for
	 * Approved contracts only
	 */
	@Override
	public String getCancelledServiceOfApprovedContract(long companyId, Date fromDate, Date toDate, boolean flag,
			ArrayList<String> brnachlist, ArrayList<String> emailIdList) {

		if (flag) {
			try {
				List<Service> serviceList = getServiceListData(companyId, fromDate, toDate, brnachlist, emailIdList);
				if (serviceList.size() == 0) {
					return "No Data Found";
				} else {
					ArrayList<Service> updatedServiceList = new ArrayList<Service>();
					updatedServiceList.addAll(serviceList);
					CsvWriter.services = updatedServiceList;
					return "Success";
				}

			} catch (Exception e) {
				// TODO: handle exception
				return e.getMessage();
			}
		} else {
			logger.log(Level.SEVERE, "Inside else Document tasque call");
			try {
				Gson json = new Gson();
				logger.log(Level.SEVERE, "branch Size" + brnachlist.size());
				String branches = json.toJson(brnachlist);
				String mailes = json.toJson(emailIdList);

				logger.log(Level.SEVERE, "branch Size" + emailIdList.size());

				Queue queue = QueueFactory.getQueue("ServiceRevenueLossReportTaskQueue-queue");
//		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/ServiceRevenueLossReportTaskQueue")
						.param("OperationName", "RevenueLossReportWithMail").param("companyId", companyId + "")
						.param("fromDate", fromDate + "").param("toDate", toDate + "").param("flag", true + "")
						.param("brnachlist", branches).param("EmailList", mailes));

				return "Report is sent on mail kindly check in 3-5 minutes";

			} catch (Exception e) {
				return e.getMessage();
			}

		}

//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
//		try {
//			Calendar cal=Calendar.getInstance();
//			cal.setTime(toDate);
//			cal.add(Calendar.DATE, 0);
//			Date ToDate=null;
//			try {
//				ToDate=dateFormat.parse(dateFormat.format(cal.getTime()));
//				cal.set(Calendar.HOUR_OF_DAY,23);
//				cal.set(Calendar.MINUTE,59);
//				cal.set(Calendar.SECOND,59);
//				cal.set(Calendar.MILLISECOND,999);
//				ToDate=cal.getTime();
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
//			
//			Calendar cal2=Calendar.getInstance();
//			cal2.setTime(fromDate);
//			cal2.add(Calendar.DATE, 0);
//			Date FromDate=null;
//			try {
//				FromDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
//				cal2.set(Calendar.HOUR_OF_DAY,00);
//				cal2.set(Calendar.MINUTE,00);
//				cal2.set(Calendar.SECOND,00);
//				cal2.set(Calendar.MILLISECOND,000);
//				FromDate=cal2.getTime();
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}

//		System.out.println("Company Id"+companyId);
//		logger.log(Level.SEVERE, "companyId "+companyId);
//
//		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
//									.filter("cancellationDate >=", FromDate).filter("cancellationDate <=", ToDate)
//									.filter("branch IN",brnachlist)
//									.filter("status", Service.SERVICESTATUSCANCELLED).list();
//		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());

//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRecordsWithServiceDate", companyId)){
//			List<Service> latestservicelist = ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceDate >=", FromDate).filter("serviceDate <=", ToDate)
//					.filter("branch IN",brnachlist)
//					.filter("status", Service.SERVICESTATUSCANCELLED).list();
//			logger.log(Level.SEVERE, "latestservicelist size"+latestservicelist.size());
//
//			servicelist.addAll(latestservicelist);
//		}
//			logger.log(Level.SEVERE, "Total servicelist "+servicelist.size());

//		if(servicelist.size()!=0){
//			HashSet<Integer> contractIds = new HashSet<Integer>();
//			for(Service serviceEntity : servicelist){
//				contractIds.add(serviceEntity.getContractCount());
//			}
//			ArrayList<Integer> contractIdlist = new ArrayList<Integer>(contractIds);
//			List<Contract> Contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
//										.filter("status", Contract.APPROVED).filter("count IN",contractIdlist).list();
//			logger.log(Level.SEVERE, "Contract List"+Contractlist.size());
//			
//			ArrayList<Service> updatedServicelist = new ArrayList<Service>();
//			if(Contractlist.size()!=0){
//				for(Contract contract : Contractlist){
//					for(Service service : servicelist){
//						if(contract.getCount() == service.getContractCount()){
//							updatedServicelist.add(service);
//						}
//					}
//				}

//				CsvWriter.services = updatedServicelist;
//				System.out.println("updated services list =="+updatedServicelist.size());
//				return "Success";
//			}
//		}
//		else{
//			return "No Data Found";
//		}

//		} catch (Exception e) {
//			logger.log(Level.SEVERE, "Exception"+e.getLocalizedMessage());
//		}

	}

	public List<Service> getServiceListData(long companyId, Date fromDate, Date toDate, ArrayList<String> brnachlist,
			ArrayList<String> emailIdList) {

		ArrayList<Service> updatedServicelist = new ArrayList<Service>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(toDate);
			cal.add(Calendar.DATE, 0);
			Date ToDate = null;
			try {
				ToDate = dateFormat.parse(dateFormat.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				cal.set(Calendar.MILLISECOND, 999);
				ToDate = cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(fromDate);
			cal2.add(Calendar.DATE, 0);
			Date FromDate = null;
			try {
				FromDate = dateFormat.parse(dateFormat.format(cal2.getTime()));
				cal2.set(Calendar.HOUR_OF_DAY, 00);
				cal2.set(Calendar.MINUTE, 00);
				cal2.set(Calendar.SECOND, 00);
				cal2.set(Calendar.MILLISECOND, 000);
				FromDate = cal2.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}

			System.out.println("Company Id" + companyId);
			logger.log(Level.SEVERE, "companyId " + companyId);

			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("cancellationDate >=", FromDate).filter("cancellationDate <=", ToDate)
					.filter("branch IN", brnachlist).filter("status", Service.SERVICESTATUSCANCELLED).list();

			if (servicelist.size() != 0) {
				HashSet<Integer> contractIds = new HashSet<Integer>();
				for (Service serviceEntity : servicelist) {
					contractIds.add(serviceEntity.getContractCount());
				}
				ArrayList<Integer> contractIdlist = new ArrayList<Integer>(contractIds);
				List<Contract> Contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
						.filter("status", Contract.APPROVED).filter("count IN", contractIdlist).list();
				logger.log(Level.SEVERE, "Contract List" + Contractlist.size());

				if (Contractlist.size() != 0) {
					for (Contract contract : Contractlist) {
						for (Service service : servicelist) {
							if (contract.getCount() == service.getContractCount()) {
								updatedServicelist.add(service);
								logger.log(Level.SEVERE, "updatedServicelist size" + updatedServicelist.size());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception" + e.getLocalizedMessage());
		}

		if (updatedServicelist != null && updatedServicelist.size() != 0) {
			return updatedServicelist;
		} else {
			return null;
		}

	}

	@Override
	public String createServiceAssesmentForComplain(Complain complain) {
		logger.log(Level.SEVERE, "Complaint Service creation");
		try {
			Service service = new Service();
			service.setCompanyId(complain.getCompanyId());
			service.setPersonInfo(complain.getPersoninfo());
			ServiceProduct serviceProdEntity = ofy().load().type(ServiceProduct.class)
					.filter("companyId", complain.getCompanyId()).filter("count", complain.getPic().getProdID()).first()
					.now();
			if (serviceProdEntity != null) {
				service.setProduct(serviceProdEntity);
			}
			service.setBranch(complain.getBranch());
			service.setServiceBranch(complain.getCustomerBranch());
			service.setStatus(Service.SERVICESTATUSSCHEDULE);
			service.setServiceType("Follow UP Service");
			service.setServiceDate(complain.getNewServiceDate());
			logger.log(Level.SEVERE, "Complaint Service creation 2");
			CustomerBranchDetails customerbranch = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", complain.getCompanyId())
					.filter("buisnessUnitName", complain.getCustomerBranch())
					.filter("cinfo.count", complain.getPersoninfo().getCount()).first().now();
			if (customerbranch != null) {
				service.setAddress(customerbranch.getAddress());
			} else {
				Customer customer = ofy().load().type(Customer.class).filter("companyId", complain.getCompanyId())
						.filter("count", complain.getPersoninfo().getCount()).first().now();
				if (customer != null) {
					service.setAddress(customer.getSecondaryAdress());
				}
			}

			if (complain.getServicedetails() != null && complain.getServicedetails().size() != 0) {
				service.setContractCount(complain.getServicedetails().get(0).getContractId());
				Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", service.getCompanyId())
						.filter("count", service.getContractCount()).first().now();
				if (contractEntity != null) {
					service.setContractStartDate(contractEntity.getStartDate());
					service.setContractEndDate(contractEntity.getEndDate());
				}
			}
			logger.log(Level.SEVERE, "Complaint Service creation 3");

			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain",
					"EnableCreateAssementForComplaintService", service.getCompanyId())) {
				AssesmentReport assesmentreport = new AssesmentReport();
				assesmentreport.setCinfo(service.getPersonInfo());
				assesmentreport.setCompanyId(service.getCompanyId());
				assesmentreport.setStatus(AssesmentReport.CREATED);
				assesmentreport.setBranch(service.getBranch());

				GenricServiceImpl impl = new GenricServiceImpl();
				ReturnFromServer server = new ReturnFromServer();
				server = impl.save(complain);
				logger.log(Level.SEVERE, "Complain Created Successfully");

				service.setTicketNumber(server.count);
				GenricServiceImpl implnew = new GenricServiceImpl();
				ReturnFromServer server2 = new ReturnFromServer();
				server2 = implnew.save(service);
				logger.log(Level.SEVERE, "Service Created Successfully");

				assesmentreport.setServiceId(server2.count);
				assesmentreport.setComplaintId(server.count);

				GenricServiceImpl impl3 = new GenricServiceImpl();
				ReturnFromServer server3 = new ReturnFromServer();
				server3 = impl3.save(assesmentreport);
				logger.log(Level.SEVERE, "Assement Created successfully");

				/**
				 * Date 12-08-2019 by Vijay for When complaint raised then send email
				 */
				sendEmailForComplaintRegister(complain, server.count);

				return "Complaint Created Please check details as follows :- Complain Id -" + server.count
						+ " Service Id -" + server2.count + " Assesment Id -" + server3.count;
			} else {

				GenricServiceImpl impl = new GenricServiceImpl();
				ReturnFromServer server = new ReturnFromServer();
				server = impl.save(complain);

				service.setTicketNumber(server.count);
				GenricServiceImpl implnew = new GenricServiceImpl();
				ReturnFromServer server2 = new ReturnFromServer();
				server2 = implnew.save(service);

				/**
				 * Date 12-08-2019 by Vijay for When complaint raised then send email
				 */
				sendEmailForComplaintRegister(complain, server.count);
				return "Complaint Created Please check details as follows :- Complain Id -" + server.count
						+ " Service Id -" + server2.count;
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Excepion" + e.getMessage());
			return e.getMessage();
		}

	}

	@Override
	public String createComplaintService(Complain complain, int complaintId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateCustomerEmailIdInContract(long companyId, Date fromDate, Date toDate) {
		logger.log(Level.SEVERE, "INSIDE RPC METHOD");
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		Queue queue = QueueFactory.getQueue("CustomerNameChangeTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerNameChangeTaskQueue")
				.param("OperationName", "CustomerMailIDUpdateInContracts").param("companyId", companyId + "")
				.param("fromDate", fromDate + "").param("toDate", toDate + ""));

		return "Customers Mail-ID Update SuccessFully!";
	}

	public void UpdateCustomerEmailInContracts(long companyId, Date fromDate, Date toDate) {
		logger.log(Level.SEVERE, "INSIDE UPDATE MAILID OF CUSTOMERS METHOD");
		List<Contract> contractEntity = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(toDate);
			cal.add(Calendar.DATE, 0);
			Date ToDate = null;
			try {
				ToDate = dateFormat.parse(dateFormat.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				cal.set(Calendar.MILLISECOND, 999);
				ToDate = cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "To Date" + ToDate);
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(fromDate);
			cal2.add(Calendar.DATE, 0);
			Date FromDate = null;
			try {
				FromDate = dateFormat.parse(dateFormat.format(cal2.getTime()));
				cal2.set(Calendar.HOUR_OF_DAY, 00);
				cal2.set(Calendar.MINUTE, 00);
				cal2.set(Calendar.SECOND, 00);
				cal2.set(Calendar.MILLISECOND, 000);
				FromDate = cal2.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "From Date" + FromDate);

			contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("creationDate >=", FromDate).filter("creationDate <=", ToDate).list();
			logger.log(Level.SEVERE, "Contract Size" + contractEntity.size());
			HashSet<Integer> custSet = new HashSet<Integer>();
			for (Contract obj : contractEntity) {
				custSet.add(obj.getCustomerId());
			}
			List<Integer> custList = new ArrayList<Integer>(custSet);

			List<Customer> customerList = null;
			customerList = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count IN", custList)
					.list();
			logger.log(Level.SEVERE, "Customer List Size" + customerList.size());
			if (customerList.size() > 0) {
				for (Contract con : contractEntity) {
					for (Customer cust : customerList) {
						if (con.getCustomerId() == cust.getCount()) {
							con.getCinfo().setEmail(cust.getEmail());
							logger.log(Level.SEVERE, "Customer email" + cust.getEmail());
						}
					}
				}
				ofy().save().entities(contractEntity).now();
				logger.log(Level.SEVERE, "Customer Contracts Update Successsfully");
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception" + e.getLocalizedMessage());
		}

	}

	@Override
	public String updateServicesStatusOpen(long companyId) {

		logger.log(Level.SEVERE, "INSIDE UPDATE SERVICE STATUS UPDATES");
		try {
			List<Service> serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("status", Service.SERVICESTATUSSCHEDULE).limit(1000).list();
			logger.log(Level.SEVERE, "ServicesList of SCHEDULE STATUS" + serviceList.size());

			for (Service service : serviceList) {
				if (service.getStatus().equals(Service.SERVICESTATUSSCHEDULE)) {
					service.setStatus(Service.SERVICESTATUSOPEN);
				}
			}
			logger.log(Level.SEVERE, "updated status list" + serviceList.size());
			if (serviceList.size() != 0) {
				ofy().save().entities(serviceList).now();
				return "SERVICESS STATUS SUCCESSFULLY UPDATED";
			} else {
				return "SERVICESS STATUS UNSUCCESSFULL";
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception" + e.getLocalizedMessage());
			return e.getMessage();
		}

	}

	@Override
	public String getInvoicePrefixDetails(long companyId, String branchName) {
		logger.log(Level.SEVERE, "inside getInvoicePrefixDetails" + branchName);
		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		Branch branch = ofy().load().type(Branch.class).filter("companyId", companyId)
				.filter("buisnessUnitName", branchName).first().now();
		String invoicePrefix = "";

		if (branch != null) {
			if (branch.getInvoicePrefix() != null) {
				invoicePrefix = branch.getInvoicePrefix();
			} else {
				invoicePrefix = "";
			}
		}
		if (invoicePrefix.equals("")) {
			if (company.getInvoicePrefix() != null) {
				invoicePrefix = company.getInvoicePrefix();
			} else {
				invoicePrefix = "";
			}
		}

		logger.log(Level.SEVERE, "invoicePrefix value " + invoicePrefix);
		return invoicePrefix;
	}

	@Override
	public String updateCustomerBranchServiceAddress(long companyId, int contractId, String customerBranch) {
		String msg = "";
		try {
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "Update CustomerBranch ServiceAddress").param("companyId", companyId + "")
					.param("contractId", contractId + "").param("customerBranch", customerBranch));
			msg = "Service updation process started. It will update only open services!";
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE,
					"Exception while calling customer branch service address updation" + e.getMessage());
			msg = "Process Failed Please contact to EVA Support";

		}

		return msg;
	}

	@Override
	public String updateCustomerServiceAddress(long companyId, int customerId) {

		Queue queue = QueueFactory.getQueue("CustomerNameChangeTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerNameChangeTaskQueue")
				.param("OperationName", "UpdateServiceAddressInServiceAndContract").param("companyId", companyId + "")
				.param("customerId", customerId + ""));

		return "Task queue called";
	}

	public void updateServiceAddressInServicesAndContracts(long companyId, int customerId) {

		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add(Service.SERVICESTATUSSCHEDULE);
		statuslist.add(Service.SERVICESTATUSRESCHEDULE);
		statuslist.add(Service.SERVICESTATUSOPEN);
		statuslist.add(Service.SERVICESTATUSPENDING);

		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("personInfo.count", customerId).filter("status IN", statuslist).list();
		logger.log(Level.SEVERE, "open service list size" + servicelist.size());

		List<Customer> customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId)
				.filter("count", customerId).list();
		logger.log(Level.SEVERE, "customerEntity size" + customerEntity.size());

		if (customerEntity.size() >= 2) {

		} else {
			if (customerEntity.size() == 1) {
				for (Service serviceEntity : servicelist) {
					serviceEntity.setAddress(customerEntity.get(0).getSecondaryAdress());
				}
				ofy().save().entities(servicelist);
				logger.log(Level.SEVERE, "Customer Service address updated in open services");

				List<Contract> contractlist = ofy().load().type(Contract.class).filter("cinfo.count", customerId)
						.filter("companyId", companyId).filter("status", Contract.APPROVED).list();
				if (contractlist.size() > 0) {
					ArrayList<Contract> activeContractslist = new ArrayList<Contract>();
					for (Contract contractEntity : contractlist) {
						if (contractEntity.getEndDate().after(new Date())
								|| contractEntity.getEndDate().equals(new Date())) {
							activeContractslist.add(contractEntity);
						}
					}

					for (Contract contract : activeContractslist) {
						contract.setCustomerServiceAddress(customerEntity.get(0).getSecondaryAdress());
					}
					logger.log(Level.SEVERE, "activeContractslist size" + activeContractslist.size());

					ofy().save().entities(activeContractslist);
				}

			}

		}

	}

	@Override
	public ArrayList<Integer> updateVendor(Vendor model) {
		// TODO Auto-generated method stub

		Queue queue = QueueFactory.getQueue("CustomerNameChangeTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/CustomerNameChangeTaskQueue")
				.param("OperationName", "VendorUpdation").param("companyId", model.getCompanyId() + "")
				.param("vendorId", model.getCount() + ""));

		return intlist;
	}

}
