package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.accountinginterface.AccountingInterfaceService;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class AccountingInterfaceServiceImpl extends RemoteServiceServlet implements AccountingInterfaceService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3964874334302127978L;

	@Override
	public void getAccountingInterfaceDataSynched(List<AccountingInterface> acclist, String status, String syncBy) {
		
		System.out.println("hi form impl");
		List<AccountingInterface> updatedList = new ArrayList<AccountingInterface>();
		
		System.out.println("in server side lis size before "+updatedList.size());
		for (AccountingInterface accountingInterface : acclist) {
		
			accountingInterface.setDateofSynch(new Date());
			accountingInterface.setStatus(status);
			accountingInterface.setSynchedBy(syncBy);
			
			updatedList.add(accountingInterface);
		}
		
		System.out.println("in server side list after "+updatedList.size());
		
		ofy().save().entities(updatedList).now();
	}

	
	@Override
	public void updateSynchDataWithSynchDate(List<AccountingInterface> acclist) {
		
		List<AccountingInterface> updatedList = new ArrayList<AccountingInterface>();
		for (AccountingInterface accountingInterface : acclist) {
			
			accountingInterface.setDateofSynch(new Date());
			updatedList.add(accountingInterface);
		}
		
		
		ofy().save().entities(updatedList).now();
	}

}
