package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.mindrot.jbcrypt.BCrypt;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.documenthistorydetails.SaveHistory;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


// TODO: Auto-generated Javadoc
/**
 * The Class GenricServiceImpl.
 */
@SuppressWarnings("serial")
public class GenricServiceImpl extends RemoteServiceServlet implements GenricService {
	  
	  Logger logger = Logger.getLogger("NameOfYourLogger");
	
static{
	
	try
	{
	
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
    }



/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.libservice.GenricService#save(com.simplesoftwares.client.library.appstructure.SuperModel)
 */
	@SuppressWarnings("unchecked")
	@Override
	public ReturnFromServer save(SuperModel m){
		ReturnFromServer server = new ReturnFromServer();
		server.error="";
		if (m.getCount() == 0) {
			long finalno = 0;
			Logger logger = Logger.getLogger("Saving Logger");
			String sr[] = m.getClass().getName().split("\\.");

			if (!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")
					&& !m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")) {

				NumberGeneration ng = null;
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();

				
				if (m instanceof Contract) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						String selectedNumberRange = ((Contract) m).getNumberRange();
						String prefixselectedNumberRange = "C_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}

				} else if (m instanceof BillingDocument) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
//						
						/*
						 * Commented by Ashwini
						 */
//						String selectedNumberRange = ((BillingDocument) m).getNumberRange();
//						String prefixselectedNumberRange = "B_"+ selectedNumberRange;
//						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
//					} else {
//						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
//					}
						
						/*
						 * Date:24/07/2018
						 * Developer:Ashwini
						 * Des:To provide billing document with number range define in sales order
						 */
						if(((BillingDocument) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
			           System.out.println("service billingdocument");
						String selectedNumberRange = ((BillingDocument) m).getNumberRange();
						String prefixselectedNumberRange = "B_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
						}
						else if(((BillingDocument) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
							System.out.println("Sales order billingdocument");
							String selectedNumberRange = ((BillingDocument) m).getNumberRange();
							String prefixselectedNumberRange = "SOB_"+ selectedNumberRange;
							finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);	
							
						}
							
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}
					/*
					 * End by Ashwini
					 */

				} else if (m instanceof Invoice) {
					if (((Invoice) m).getInvoiceType().equals("Proforma Invoice")) {
						String prefixselectedNumberRange = "P_Invoice";
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					}else{ 
						boolean processconfigflag = checkprocessconfiguration(m);
						if (processconfigflag) {
							
							/*
							 * Commented by Ashwini
							 */
//							String selectedNumberRange = ((Invoice) m).getNumberRange();
//							String prefixselectedNumberRange = "I_"+ selectedNumberRange;
//							finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
//						} else {
//							finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
//						}
							/**
							 * @author Vijay Date 10-11-2020
							 * Des :- As per nitin Sir Instruction if sales and service invoice number series different required
							 * for client then we can use below process config and old code added in else block for normal flow
							 */
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", AppConstants.PC_DIFFERENTNUMBERSERIESFORSALESANDSERVICE, m.getCompanyId())){
								/*
								 * Date:25/07/2018
								 * Developer:Ashwini
								 * Des:To provide Invoice with number range define in sales order
								 */
								logger.log(Level.SEVERE,"Type = "+((Invoice) m).getTypeOfOrder());

								if(((Invoice) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
									logger.log(Level.SEVERE,"Order TYPE IN SALESINVOICE ");
									String selectedNumberRange = ((Invoice) m).getNumberRange();
									String prefixselectedNumberRange = "I_"+ selectedNumberRange;
									finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
							    } 
								else if(((Invoice) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
									logger.log(Level.SEVERE,"Order TYPE IN Service Invoice ");
									String selectedNumberRange = ((Invoice) m).getNumberRange();
									String prefixselectedNumberRange = "SOI_"+ selectedNumberRange;
									finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
								}
								else{
									logger.log(Level.SEVERE,"Normal");
									finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
								}
							}
							else{
								/*
								 * Date:25/07/2018
								 * Developer:Ashwini
								 * Des:To provide Invoice with number range define in sales order
								 */
								if(((Invoice) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)||((Invoice) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
									logger.log(Level.SEVERE,"Order TYPE IN SALESINVOICE ");
									String selectedNumberRange = ((Invoice) m).getNumberRange();
									String prefixselectedNumberRange = "I_"+ selectedNumberRange;
									finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
							    }
								/**Date 25-9-2020 by Amol commented this condition for common number generation 
								 * for sales invoice and service invoice raised by Ashwini Bhagwat**/
//								else if(((Invoice) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
//									String selectedNumberRange = ((Invoice) m).getNumberRange();
//									String prefixselectedNumberRange = "SOI_"+ selectedNumberRange;
//									finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
//								/*
//								 * End by Ashwini
//								 */
//								}
							}
							
						}else {
							finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
						}		
				}		
			} else if (m instanceof CustomerPayment) {

					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						
						/*
						 * Commented by Ashwini
						 */
//						String selectedNumberRange = ((CustomerPayment) m).getNumberRange();
//						String prefixselectedNumberRange = "P_"+ selectedNumberRange;
//						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
//					} else {
//						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
//					}
//					
					/*
					 * Date:25/07/2018
					 * Developer:Ashwini
					 * Des:To provide CustomerPayment with number range define in sales order
					 */
					if(((CustomerPayment) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)){
						String selectedNumberRange = ((CustomerPayment) m).getNumberRange();
						String prefixselectedNumberRange = "P_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
				} 
					else if(((CustomerPayment) m).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
						String selectedNumberRange = ((CustomerPayment) m).getNumberRange();
						String prefixselectedNumberRange = "SOP_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
				}
				} else {
					finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
				}
				/*
				 * Ashwini end here
				 */
				} else if (m instanceof Service) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						String selectedNumberRange = ((Service) m).getNumberRange();
						String prefixselectedNumberRange = "S_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}
				}/***
				 * Date : 15-05-2018 BY ANIL
				 * added number range process for employee
				 */
				else if (m instanceof Employee) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						String selectedNumberRange = ((Employee) m).getNumberRange();
						String prefixselectedNumberRange = "E_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}
				}
				
				/*
				 * Date:23/07/2018
				 * Developer:Ashwini
				 * Des:To add number generation for sales order.
				 */
				else if (m instanceof SalesOrder) {
					System.out.println("Sales Order");
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						System.out.println("Sales Order");
						String selectedNumberRange = ((SalesOrder) m).getNumberRange();
						String prefixselectedNumberRange = "SO_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}
				} 
				else if (m instanceof DeliveryNote) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						
						String selectedNumberRange = ((DeliveryNote) m).getNumberRange();
						String prefixselectedNumberRange = "D_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}		System.out.println("sr1[sr1.length - 1");
				} 
		
				/*
				 * End by Ashwini
				 */
				/**
				 * @author Vijay Date :- 19-07-2021 
				 * Des :- Number Range functionality added
				 */
				else if (m instanceof Lead) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						String selectedNumberRange = ((Lead) m).getNumberRange();
						String prefixselectedNumberRange = "L_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}

				}
				else if (m instanceof Quotation) {
					boolean processconfigflag = checkprocessconfiguration(m);
					if (processconfigflag) {
						String selectedNumberRange = ((Quotation) m).getQuotationNumberRange();
						String prefixselectedNumberRange = "Q_"+ selectedNumberRange;
						finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange, m.getCompanyId(), 1);
					} else {
						finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}

				}				
				else {
					if(m.getCompanyId()!=null){
					finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1], m.getCompanyId(), 1);
					}
				}
			}

			if (m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")
					|| m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")) {
				int count = 0;
				if (m.getCompanyId() == null) {
					count = ofy().load().type(m.getClass()).count();
				} else {
					count = ofy().load().type(m.getClass()).filter("companyId", m.getCompanyId()).count();
				}
				m.setCount(count + 1);
			} else {
				int dd = (int) finalno;
				m.setCount(dd);
			}
		}

		/**********************************************/
		// comment code for without encryption
		String sr[] = m.getClass().getName().split("\\.");
		String str = sr[sr.length - 1];
		
		
		 if(str.equals("User")){
			 
			 /**
			  * @author Vijay Date :- 06-09-2022
			  * Des :- Password Encryption with process config
			  */
			 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("User", AppConstants.PC_ENABLEPASSWORDENCRYPTION, m.getCompanyId())) {
				 User currentUserObject = (User) m;
				 User userEntity = ofy().load().type(User.class).filter("companyId", currentUserObject.getCompanyId()).filter("userName", currentUserObject.getUserName()).first().now();
				 String originalPassword = currentUserObject.getPassword();
				 try {
				 logger.log(Level.SEVERE, "currentUserObject.getPassword() "+currentUserObject.getPassword());
				 logger.log(Level.SEVERE, "userEntity.getPassword() "+userEntity.getPassword());
			
				 boolean matched = false;
				 
					// below line when first time password saving in encryption to get exception to save catch block
					 matched = BCrypt.checkpw(currentUserObject.getPassword(),userEntity.getPassword()); 
					 if(!currentUserObject.getPassword().trim().equals(userEntity.getPassword().trim())) {
						 matched = BCrypt.checkpw(currentUserObject.getPassword(),userEntity.getPassword());
						 logger.log(Level.SEVERE, "matched "+matched);
						 if(!matched) {
					 			String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword,
								BCrypt.gensalt());
								currentUserObject.setPassword(generatedSecuredPasswordHash);
								logger.log(Level.SEVERE, "generatedSecuredPasswordHash "+generatedSecuredPasswordHash); 
						 } 
					 }
					 
				} catch (Exception e) {
						e.printStackTrace();
						String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword,
						BCrypt.gensalt());
						currentUserObject.setPassword(generatedSecuredPasswordHash);
						logger.log(Level.SEVERE, "generatedSecuredPasswordHash "+generatedSecuredPasswordHash);
						
				}
				    
			 }
			 /**
			  * ends here
			  */
		 }


		/************************************************/

		if (str.equals("ConfigCategory")) {
			ConfigCategory conCat = (ConfigCategory) m;
			conCat.setCategoryCode("CC" + conCat.getCount());
		}

		if (str.equals("Type")) {
			Type conCat = (Type) m;
			conCat.setTypeCode("TT" + conCat.getCount());
		}

		/**
		 * This code auto generate product code for ItemProduct Date :
		 * 07-10-2016 By Anil Release Date: 30 Sept 2016 Project : PURCHASE
		 * MODIFICATION (NBHC)
		 */

		if (str.equals("ItemProduct")) {
			ItemProduct entity = (ItemProduct) m;
			System.out.println("ITEM PRODUCT COUNT :: " + entity.getCount());
			if (entity.getProductCode().equals("")) {
				entity.setProductCode("P" + entity.getCount());
			}
		}

		/**
		 * @author Vijay Date 15-01-2021
		 * Des :- added code to raise exception to identify duplicate invoice when duplicate invoice contract start date and end date is null
		 */
		if(m instanceof Invoice && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableExceptionDuplicateInvoice", m.getCompanyId())){
			Invoice invoice = ((Invoice) m);
			if(invoice.getContractStartDate()==null && invoice.getContractEndDate()==null) {
				logger.log(Level.SEVERE, "Invoice Contract start date and end date null ");
				Branch branch = null;	
				branch.getAddress();
				branch.getCalendar();
				
			}
		}
		/**
		 * ends here
		 */
		/*
		 * Added by Sheetal : 01-10-2021
		 * Des :Customer info should update automatically by clicking on save button only
		 */
		if(m instanceof Customer){
			Customer customer = ((Customer) m);
			logger.log(Level.SEVERE, "Get Id" +customer.getCount());
			logger.log(Level.SEVERE, "Get Name" +customer.getFullname());
			logger.log(Level.SEVERE, "Get Cell No" +customer.getCellNumber1());
			logger.log(Level.SEVERE, "Get Email" +customer.getEmail());
			
			Customer customerEntity = ofy().load().type(Customer.class).filter("count", customer.getCount()).first().now();
			
			if(customerEntity!=null){
				System.out.println(!customerEntity.getFullname().equals(customer.getFullname()));
				System.out.println(!customerEntity.getCellNumber1().equals(customer.getCellNumber1()));
				System.out.println(!customerEntity.getEmail().equals(customer.getEmail()));

			 if(!customerEntity.getFullname().equals(customer.getFullname())||!customerEntity.getCellNumber1().equals(customer.getCellNumber1())||!customerEntity.getEmail().equals(customer.getEmail())){
				 System.out.println("inside name change method");
				 CustomerNameChangeServiceImpl cust = new CustomerNameChangeServiceImpl();
					cust.SaveCustomerChangeInAllDocs(customer,false);
				}
			}
		}
		/*
		 * End
		 */
		ofy().save().entity(m).now();
		
		if (m instanceof Customer && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks", m.getCompanyId())) {
			
			Customer cust=(Customer)m;
			if(cust.getZohoCustomerId()<=0) {
				logger.log(Level.SEVERE, "starting process to create customer in zoho");
				TaxInvoiceServiceImpl taxService=new TaxInvoiceServiceImpl();
				Customer object=(Customer)m;
				String validationResponse=taxService.validateCustomerForZohoIntegration(object);
				if(validationResponse.equalsIgnoreCase("Success")) {
					String res=taxService.createCustomerInZohoBooks(m.getCount(), m.getCompanyId());
					if(res.equalsIgnoreCase("zohosuccess"))
						server.error="";
					else
						server.error= "customer saved successfully in EVA but failed to create in Zoho books. Error : "+res;
					
				}else
					server.error="customer saved successfully in EVA but failed to create in Zoho books. Error : "+validationResponse;
				
			}else
				logger.log(Level.SEVERE, "zoho customer id already present in customer");
		}

		server.id = m.getId();
		System.out.println("MCOUNT" + m.getCount());
		server.count = m.getCount();
//		if(m instanceof ProjectAllocation){
//			
//			ProjectAllocation project = (ProjectAllocation) m;
//				List<Integer> empIdList = new ArrayList<Integer>();
//				
//				/**
//				 * @author Vijay @Since 21-09-2022
//				 * Loading employee project allocation from different entity
//				 */
//				   CommonServiceImpl commonservice = new CommonServiceImpl();
//				   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(project);
//			    /**
//			     * ends here
//			     */
////				for(EmployeeProjectAllocation alloc : project.getEmployeeProjectAllocationList()){
//					for(EmployeeProjectAllocation alloc : empprojectallocationlist){
//
//					if(alloc.getEmployeeId()!=null && !alloc.getEmployeeId().equals("")){
//					if (alloc.getStartDate() != null && alloc.getEndDate() != null) {
//						Date currentDate = new Date();
//						if (currentDate.after(alloc.getStartDate()) && currentDate.before(alloc.getEndDate())) {
//							empIdList.add(Integer.parseInt(alloc.getEmployeeId()));
//						}
//					  }
//					}
//				}
//				if(empIdList.size()>0){
//					List<Employee> empList = ofy().load().type(Employee.class).filter("companyId", project.getCompanyId()).filter("count IN", empIdList).list();
//					for(Employee emp : empList){		
//						emp.setProjectName(project.getProjectName());
//					}		
//					ofy().save().entities(empList);
//				}
//		}	
		logger.log(Level.SEVERE, "SERVER.ERROR="+server.error+" ERROR LENGTH="+server.error.length());
		return server;
	}


	public void updateonProjectAllocationSave(ProjectAllocation projectAllocation) {

//		ProjectAllocation project = (ProjectAllocation) m;
		List<Integer> empIdList = new ArrayList<Integer>();
		
//		/**
//		 * @author Vijay @Since 21-09-2022
//		 * Loading employee project allocation from different entity
//		 */
//		   CommonServiceImpl commonservice = new CommonServiceImpl();
//		   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
//	    /**
//	     * ends here
//	     */
		for(EmployeeProjectAllocation alloc : projectAllocation.getEmployeeProjectAllocationList()){
			if(alloc.getEmployeeId()!=null && !alloc.getEmployeeId().equals("")){
			if (alloc.getStartDate() != null && alloc.getEndDate() != null) {
				Date currentDate = new Date();
				if (currentDate.after(alloc.getStartDate()) && currentDate.before(alloc.getEndDate())) {
					empIdList.add(Integer.parseInt(alloc.getEmployeeId()));
				}
			  }
			}
		}
		if(empIdList.size()>0){
			List<Employee> empList = ofy().load().type(Employee.class).filter("companyId", projectAllocation.getCompanyId()).filter("count IN", empIdList).list();
			for(Employee emp : empList){		
				emp.setProjectName(projectAllocation.getProjectName());
			}		
			ofy().save().entities(empList);
	}
	
}


/**
 * Date : 02-04-2018 By ANIL
 * old code for saving data 
 */
	
//	/* (non-Javadoc)
//	 * @see com.simplesoftwares.client.library.libservice.GenricService#save(com.simplesoftwares.client.library.appstructure.SuperModel)
//	 */
//	@SuppressWarnings("unchecked")
//	@Override
//	public ReturnFromServer save(SuperModel m) 
//	{
//		System.out.println("In Save Server"+m.getCount());
//		int mode=ofy().load().type(ProcessName.class).filter("companyId",m.getCompanyId()).count();
//		if(m.getCount()!=0)
//		{
//			if(!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")&&!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")){
//				String sr[]=m.getClass().getName().split("\\.");
//				if(mode!=0){
////					SaveHistory.saveHistoryData(m.getCompanyId(), null, sr[sr.length-1], m.getCount(), DocumentHistory.UPDATED, m.getCreatedBy(),m.getUserId());
//				}
//			}
//		}
//		
//		if(m.getCount()==0)
//		{
//			long finalno = 0;
//			Logger logger = Logger.getLogger("Saving Logger");
//			logger.log(Level.SEVERE,"Class name"+m.getClass().getName().trim());
//			String sr[]=m.getClass().getName().split("\\.");
//			logger.log(Level.SEVERE,"Class Name"+m.getClass().getName().trim());
//			logger.log(Level.SEVERE,"Length"+sr.length);
//			logger.log(Level.SEVERE,"Ind"+sr[sr.length-1]);
//			
//			
//			
//			if(mode==0){
//				 int  count=ofy().load().type(m.getClass()).count();
//				m.setCount(count+1);
//			}
//			else{
//				if(!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")&&!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")){
//					
//					NumberGeneration ng = null;
//					
//					
//					if(m.getCompanyId()==null){
//						ng=ofy().load().type(NumberGeneration.class).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//					}
//					else{
////						ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//					
//					//  code by vijay for new number range ***********
//						
//						List<NumberGeneration> numberGenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", m.getCompanyId()).list();
//
//						if(m instanceof Contract){
//							
//							logger.log(Level.SEVERE,"In the Contract entity");
//							boolean processconfigflag = checkprocessconfiguration(m);
//							logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);
//
//							System.out.println("Value-----"+processconfigflag);
//							if(processconfigflag){
//								System.out.println("Inside process config");
////									List<NumberGeneration> numberGenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", m.getCompanyId()).list();
//									System.out.println("selected type from drop down=="+((Contract) m).getNumberRange());
//									logger.log(Level.SEVERE,"selected type from drop down=="+((Contract) m).getNumberRange());
//
//									if(((Contract) m).getNumberRange()!=null){
//										
//									String selectedNumberRange = ((Contract) m).getNumberRange();
//									String prefixselectedNumberRange = "C_"+selectedNumberRange;
//									System.out.println("string added prefix:"+prefixselectedNumberRange);
//									
//									for(int i=0;i<numberGenrationList.size();i++){
////										System.out.println("Process name==="+numberGenrationList.get(i).getProcessName());
//										if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//											System.out.println("DONE=====");
//											logger.log(Level.SEVERE,"Done here number gen");
//											ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//										}
//									}
//									
//								}else{
//									
//									System.out.println("if number range is null means not selected");
//									logger.log(Level.SEVERE,"if number range is null means not selected");
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//								}
//								
//							}else{
//								System.out.println("Inside no process config");
//								logger.log(Level.SEVERE,"if process config is false then normal procedure");
//								ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//							}
//							
//						}
//						else if(m instanceof BillingDocument){
//							System.out.println(" inside quic contract billing document");
//							 logger.log(Level.SEVERE,"In the BillingDocument entity");
//								boolean processconfigflag = checkprocessconfiguration(m);
//								
//								logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);					
//								System.out.println("Value-----"+processconfigflag);
//								if(processconfigflag){
//									System.out.println("Inside process config");
//									if(((BillingDocument) m).getNumberRange()!=null){
//										
//									String selectedNumberRange = ((BillingDocument) m).getNumberRange();
//									String prefixselectedNumberRange ="B_"+selectedNumberRange;
//									System.out.println("string added prefix===:"+prefixselectedNumberRange);
//									  logger.log(Level.SEVERE,"string added prefix===:"+prefixselectedNumberRange);
//
//									for(int i=0;i<numberGenrationList.size();i++){
//										System.out.println("Process name==="+numberGenrationList.get(i).getProcessName());
//										if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//											System.out.println("DONE=====");
//											logger.log(Level.SEVERE,"Done here new number gen");
//											ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//										}
//									}
//										
//									}else{
//										  logger.log(Level.SEVERE,"if number range is null means not selected");
//										System.out.println("if number range is null means not selected");
//										ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//									}
//									
//								}else{
//									logger.log(Level.SEVERE,"if process config is false then normal procedure");
//									System.out.println("Inside no process config");
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//								}
//								
//						}else if(m instanceof Invoice){
//							System.out.println("for invoice entity");
//							logger.log(Level.SEVERE,"In the Invoice entity");
//
//							boolean processconfigflag = checkprocessconfiguration(m);
//							logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);
//
//							System.out.println("Value-----"+processconfigflag);
//							if(processconfigflag){
//								System.out.println("Inside process config");
//								
//								System.out.println("hi Invoice");
////									List<NumberGeneration> numberGenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", m.getCompanyId()).list();
//									System.out.println("selected type from drop down=="+((Invoice) m).getNumberRange());
//									logger.log(Level.SEVERE,"selected type from drop down=="+((Invoice) m).getNumberRange());
//		
//									/**
//								 * Date : 01-08-2017 By ANIL
//								 * If invoice type is proforma and
//								 * If number range is active and number range type is defined 
//								 * then also we have to assign proforma invoice number to given invoice 
//								 */
//								
//								if(((Invoice) m).getInvoiceType().equals("Proforma Invoice")){
//									String prefixselectedNumberRange = "P_Invoice";
//									for(int i=0;i<numberGenrationList.size();i++){
//										if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//											System.out.println("Roan DONE=====");
//											logger.log(Level.SEVERE," Rohan Done here new number gen");
//											ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//										}
//									}
//								}else if(((Invoice) m).getNumberRange()!=null){
//										
//									String selectedNumberRange = ((Invoice) m).getNumberRange();
//									String prefixselectedNumberRange = "I_"+selectedNumberRange;
//									System.out.println("string added prefix:"+prefixselectedNumberRange);
//									
//									for(int i=0;i<numberGenrationList.size();i++){
////										System.out.println("Process name==="+numberGenrationList.get(i).getProcessName());
//										if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//											System.out.println("DONE=====");
//											logger.log(Level.SEVERE,"Done here new number gen");
//											ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//										}
//									}
//									
//								}else{
//									
//									System.out.println("if number range is null means not selected");
//									logger.log(Level.SEVERE,"if number range is null means not selected");
//
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//								}
//								
//							}else{
//								
//								//   old running code for invoice save
//								
////								System.out.println("Inside no process config");
////								logger.log(Level.SEVERE,"if process config is false then normal procedure");
////								ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//								
//// ***********************************  rohan added this for separate number range for proforma and Tax invoice   
//								
//								if(((Invoice) m).getInvoiceType().equals("Proforma Invoice")){
//								String prefixselectedNumberRange = "P_Invoice";
//								for(int i=0;i<numberGenrationList.size();i++){
////									System.out.println("Process name==="+numberGenrationList.get(i).getProcessName());
//									if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//										System.out.println("Roan DONE=====");
//										logger.log(Level.SEVERE," Rohan Done here new number gen");
//										ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//									}
//								}
//								}
//								else
//								{
//									System.out.println("Inside no process config");
//									logger.log(Level.SEVERE,"if process config is false then normal procedure");
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//									
//								}
//								
//								// ************changes ends here ******************
//								
//							}
//						}
//						else if(m instanceof CustomerPayment){
//							
//							System.out.println("for CustomerPayment entity");
//							logger.log(Level.SEVERE,"In the CustomerPayment entity");
//
//
//							boolean processconfigflag = checkprocessconfiguration(m);
//							System.out.println("Value-----"+processconfigflag);
//							logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);
//
//							if(processconfigflag){
//								System.out.println("Inside process config");
//								
//								System.out.println("hi Invoice");
////									List<NumberGeneration> numberGenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", m.getCompanyId()).list();
//									System.out.println("selected type from drop down=="+((CustomerPayment) m).getNumberRange());
//									logger.log(Level.SEVERE,"selected type from drop down=="+((CustomerPayment) m).getNumberRange());
//
//									if(((CustomerPayment) m).getNumberRange()!=null){
//										
//									String selectedNumberRange = ((CustomerPayment) m).getNumberRange();
//									String prefixselectedNumberRange = "P_"+selectedNumberRange;
//									System.out.println("string added prefix:"+prefixselectedNumberRange);
//									
//									for(int i=0;i<numberGenrationList.size();i++){
//										System.out.println("Process name==="+numberGenrationList.get(i).getProcessName());
//										if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//											System.out.println("DONE=====");
//											logger.log(Level.SEVERE,"Done here new number gen");
//											ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//										}
//									}
//									
//								}else{
//									
//									System.out.println("if number range is null means not selected");
//									logger.log(Level.SEVERE,"if number range is null means not selected");
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//								}
//								
//							}else{
//								System.out.println("Inside no process config");
//								logger.log(Level.SEVERE,"if process config is false then normal procedure");
//								ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//							}
//						}
//						else if(m instanceof Service){
//							 System.out.println("In service ===");
//							 logger.log(Level.SEVERE,"In the Service entity");
//
//							  logger.log(Level.SEVERE,"In Service ");
//
//								boolean processconfigflag = checkprocessconfiguration(m);
//								logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);					
//								
//								if(processconfigflag){
//									System.out.println("In the process config true");
//									List<NumberGeneration> numbergenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", m.getCompanyId()).list();
//									if(((Service) m).getNumberRange()!=null){
//										
//										String selectedNumberRange = ((Service) m).getNumberRange();
//										String prefixselectedNumberRange = "S_"+selectedNumberRange;
//										
//										  logger.log(Level.SEVERE,"selected type from drop down=="+((Service) m).getNumberRange());
//
//										for(int i=0;i<numbergenrationList.size();i++){
//											
//											if(prefixselectedNumberRange.equals(numbergenrationList.get(i).getProcessName())){
//												logger.log(Level.SEVERE,"Done here new number gen");
//												ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//											}
//										}
//									}
//									else{
//										  logger.log(Level.SEVERE,"if number range is null means not selected");
//										ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//									}
//								}
//								else{
//									logger.log(Level.SEVERE,"if process config is false then normal procedure");
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//								}
//							 
//						 }
//						else{
//							System.out.println("Entity ============"+m.getClass());
//							logger.log(Level.SEVERE,"Entity ============"+m.getClass());
//							logger.log(Level.SEVERE,"Last Number is=="+sr[sr.length-1]);
//							System.out.println("for normal senario of all number genration");
//							logger.log(Level.SEVERE,"This is a normal senario of all number genration ");
//							ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//							
//						}
//						
//					//   code ends here ******************************
//					}
//					
////					logger.log(Level.SEVERE,"Last Number is=="+ng.getNumber());
//					
//					ng.setNumber(ng.getNumber()+1);
//					/**
//					 * Date : 05-03-2017 BY Anil
//					 * Old code for saving number generation
//					 * 
//					 */
////					this.save(ng);
//					/**
//					 * Date : 05-03-2017 By Anil
//					 * New code of saving number generation entity
//					 */
//					 ofy().save().entity(ng).now();
//					finalno=ng.getNumber();
//				}
//			}
//			
//			if(m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")||m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")){
//				
//				int count=0;
//				if(m.getCompanyId()==null){
//					count=ofy().load().type(m.getClass()).count();
//				}
//				else{
//					count=ofy().load().type(m.getClass()).filter("companyId",m.getCompanyId()).count();
//				}
//				
//				m.setCount(count+1);
//			}
//			else{
//				if(mode>0){
//					int dd=(int)finalno;
////					SaveHistory.saveHistoryData(m.getCompanyId(), null, sr[sr.length-1], dd, DocumentHistory.CREATED, m.getCreatedBy(),m.getUserId());
//					m.setCount(dd);
//				}
//			}
//		}
//		
//		
//		/**********************************************/  // comment code for  without encryption 
//		String sr[]=m.getClass().getName().split("\\.");
//		String str = sr[sr.length-1];
////	    if(str.equals("User")){
////			System.out.println("hiiiiiiiiii");
////			User use = (User) m;
////			
////			String  originalPassword = use.getPassword();
////	        String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt());
////	        System.out.println("Hashcode:"+generatedSecuredPasswordHash);
////	        use.setPassword(generatedSecuredPasswordHash);
////	        System.out.println("End password");
////		}
//		
//		//System.out.println("before==============");
//		
//		
//		/************************************************/
//		
//		
//		if(str.equals("ConfigCategory")){
//			ConfigCategory conCat=(ConfigCategory) m;
//			System.out.println("CATEGORY CONFIG :: "+conCat.getCount());
//			conCat.setCategoryCode("CC"+conCat.getCount());
//		}
//		
//		if(str.equals("Type")){
//			Type conCat=(Type) m;
//			System.out.println("TYPE CONFIG :: "+conCat.getCount());
//			conCat.setTypeCode("TT"+conCat.getCount());
//		}
//		
//		
//		
//		/**
//		 * This code auto generate product code for ItemProduct
//		 * Date : 07-10-2016 By Anil
//		 * Release Date: 30 Sept 2016
//		 * Project : PURCHASE MODIFICATION (NBHC)
//		 */
//		
//		if(str.equals("ItemProduct")){
//			ItemProduct entity=(ItemProduct) m;
//			System.out.println("ITEM PRODUCT COUNT :: "+entity.getCount());
//			if(entity.getProductCode().equals("")){
//				entity.setProductCode("P"+entity.getCount());
//			}
//		}
//		/*
//		 *ENd 
//		 */
//		
//		
//	    ofy().save().entity(m).now();
//	    
//	    //Setting count
////	  int  count=ofy().load().type(m.getClass()).count();
//	   
//	    ReturnFromServer server = new ReturnFromServer();
//	    server.id=m.getId();
//	    System.out.println("MCOUNT"+m.getCount());
//	    server.count=m.getCount();
//	   return server;
//	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.libservice.GenricService#update(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public void update(SuperModel m) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.libservice.GenricService#delete(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public void delete(SuperModel m) {
		ofy().delete().entity(m);
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.libservice.GenricService#print(java.lang.Long)
	 */
	@Override
	public void print(Long id) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.libservice.GenricService#download(java.util.ArrayList)
	 */
	@Override
	public void download(ArrayList<SuperModel> m) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.libservice.GenricService#email(java.lang.Long)
	 */
	@Override
	public void email(Long id) {
		// TODO Auto-generated method stub
		
	}

	

	
   /* (non-Javadoc)
    * @see com.simplesoftwares.client.library.libservice.GenricService#getSearchResult(com.simplesoftwares.client.library.appstructure.search.MyQuerry)
    */
   @Override
   public ArrayList<SuperModel> getSearchResult(MyQuerry quer)
   {
	   String serviceBranch="";//Ashwini Patil Date:13-03-2023
	   boolean isServiceSearch=false;
	   boolean isBillSearch=false;
	   boolean isExpenseSearch=false;
	   
	   String orderType="";
	   String branch="";
	   String status="";
	   int personCount=0;
	   ArrayList<SuperModel> list=new ArrayList<SuperModel>();
	   try{
	   ofy().clear();
	   Query<? extends SuperModel> query = null;
	  
	   
	   /**
	    *   Old Code
	    */
	   
	   //Get the querry
//	   query=ofy().load().type(quer.getQuerryObject().getClass());
	   
	   
	   /**
	    *  New Code
	    */
	   
//	   StringBuilder str=new StringBuilder("query");
	   String[] filterFieldArray=new String[20];
	   Object[] filterValueArray=new Object[20];
	   
	   
	   
	   
	   
	   
	   Logger logger = Logger.getLogger("NameOfYourLogger");
//	   logger.log(Level.SEVERE,"Class Loaded. Size Of Total Records Is"+query.count());
	   
	   int vendorID=0;
	   //Get the 
	   
	   if(quer.getQuerryObject().getClass().getName().equals("com.slicktechnologies.shared.Service"))
			   isServiceSearch=true;
	   
	   if(quer.getQuerryObject().getClass().getName().equals("com.slicktechnologies.shared.common.salesprocess.BillingDocument"))
	   { 	   isBillSearch=true;
	   }
	   if(quer.getQuerryObject().getClass().getName().equals("com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt"))
	   { 	   isExpenseSearch=true;
	   }
	   logger.log(Level.SEVERE,"quer.getQuerryObject().getClass().getName()="+quer.getQuerryObject().getClass().getName()+" isBillSearch="+isBillSearch);
	   
	   boolean isdateFilterActive=false;
	   
	   if(quer.getFilters()!=null)
	   {
		   /**
		    *  Counter for from date and to date
		    */
		   int ctr=1;
		   logger.log(Level.SEVERE,"Number Of Filters Applied"+quer.getFilters().size());
		   /*****************************************/
		   
	      for(int i=0;i<quer.getFilters().size();i++)
		  {
			      String conditionField=quer.getFilters().get(i).getQuerryString();
			      Object value=quer.getFilters().get(i).geFilterValue();
			      
			      
			      if(conditionField.equals("vendorDetails.vendorId"))
			    	  vendorID=(int)value;
			    
			      logger.log(Level.SEVERE,"Processing Filters conditionField="+conditionField);
			      
			      Date condDate=null;
			      
			      if(value instanceof Date)
			      {
			    	  condDate=DateUtility.getDateWithTimeZone("IST", (Date) value);
			    	  Date newconDate=null;
			    	  if(ctr==1){
			    		  	System.out.println("Conditional  Field :: "+conditionField);
							System.out.println("From Date Format :::: "+condDate);
							if(conditionField.contains("<=")||conditionField.contains(">=")||conditionField.contains(">")||conditionField.contains("<")){
								Calendar calendar = Calendar.getInstance();
						        calendar.setTime(condDate);
	//					        calendar.set(Calendar.MILLISECOND, 000);
						        calendar.set(Calendar.SECOND, 00);
						        calendar.set(Calendar.MINUTE, 00);
						        calendar.set(Calendar.HOUR, 00);
						        newconDate=calendar.getTime();
						        value=newconDate;
						        System.out.println("Date Format :::: "+newconDate);
						        logger.log(Level.SEVERE,"From Date :: "+value);
						        ctr++;
							}else{
								value=condDate;
								 logger.log(Level.SEVERE,"EQUAL Date :: "+value);
								 System.out.println("EQUAL DATE ::: "+value);
							}
			    	  }
			    	  else
			    	  {
			    		  	System.out.println("To Date Format :::: "+condDate);
							Calendar calendar = Calendar.getInstance();
					        calendar.setTime(condDate);
//					        calendar.set(Calendar.MILLISECOND, 999);
					        calendar.set(Calendar.SECOND, 59);
					        calendar.set(Calendar.MINUTE, 59);
					        calendar.set(Calendar.HOUR, 23);
					        newconDate=calendar.getTime();
					        value=newconDate;
					        System.out.println("Date Format :::: "+newconDate);
					        logger.log(Level.SEVERE,"From Date :: "+value);
			    	  }
			    	  isdateFilterActive=true;
			    }
			      
			      
//			      if(value instanceof Date)
//			      {
//			    	  
//			    	  logger.log(Level.SEVERE,"Condition Field "+conditionField);
//				        logger.log(Level.SEVERE,"Condition Value "+value);
//				        logger.log(Level.SEVERE,"Size of Querry "+query.count());
//				        GWT.log("Condition Field "+conditionField);
//				        GWT.log("Condition Value "+value);
//			    	  logger.log(Level.SEVERE,"Condition Value before parsed "+value.toString());
//			    	  Date condDate= parseDate((Date) value);
//			    	 // Very dangerous patch learn internationalization and recode
//			    	 Calendar c = Calendar.getInstance(); 
//			    	 c.setTime(condDate); 
//			    	 c.add(Calendar.DATE, 1);
//			    	 condDate = c.getTime();
//			    	 condDate= parseDate((Date) condDate);
//			    	 logger.log(Level.SEVERE,"Condition Value When parsed "+condDate);
//			    	 logger.log(Level.SEVERE,"Condition Field When parsed "+conditionField);
//			    	 query=query.filter(conditionField,condDate);
//			    	}
//			      else
//			      {
			      
			      
			      
			      
			      
			      
//			      logger.log(Level.SEVERE,"Condition Field "+conditionField);
//			      logger.log(Level.SEVERE,"Condition Value "+value);
			      
			      
			      /**
			       *   Old Code
			       */
			      
//			      query=query.filter(conditionField,value);
			      
			      
			      /**
			       *  New Code
			       */
			      logger.log(Level.SEVERE,"isdateFilterActive="+isdateFilterActive+" isExpenseSearch="+isExpenseSearch);
			      if(isdateFilterActive&&isBillSearch&&conditionField.equals("typeOfOrder")) {
		    			orderType=(String) value;
		    			int size=quer.getFilters().size();
					    filterFieldArray[i]=quer.getFilters().get(size-1).getQuerryString();
				    	filterValueArray[i]=quer.getFilters().get(size-1).geFilterValue();
			      }else if(isdateFilterActive&&isExpenseSearch&&(conditionField.equals("branch")||conditionField.equals("personInfo.count")||conditionField.equals("status"))) {
		    			
			    	  if(conditionField.equals("branch"))
			    	  		branch=(String) value;
			    	  if(conditionField.equals("personInfo.count"))
			    		  personCount=(int)value;
			    	  if(conditionField.equals("status"))
			    		  status=(String)value;
		    			int size=quer.getFilters().size();
					    filterFieldArray[i]=quer.getFilters().get(size-1).getQuerryString();
				    	filterValueArray[i]=quer.getFilters().get(size-1).geFilterValue();
			      }
			      else {			      
			    	  filterFieldArray[i]=conditionField;
			    	  filterValueArray[i]=value;
			      }
			      
			      
			      
			      if(i==quer.getFilters().size()-1){
			    	  for(int j=quer.getFilters().size()-1;j<filterFieldArray.length;j++){
//			    		logger.log(Level.SEVERE,"in for conditionField="+conditionField+" isdateFilterActive="+isdateFilterActive+" isBillSearch="+isBillSearch+" value="+value);
			    		if(isServiceSearch&&conditionField.equals("serviceBranch"))  {
			    			serviceBranch=(String) value;			    			
			    		}
			    		else{
			    			filterFieldArray[j]=conditionField;
			    			filterValueArray[j]=value;
			    		}
			    	  }
			      }
			      
//			      logger.log(Level.SEVERE,"Size of Querry "+query.count());        
			      
			//}
		  }
	      if(orderType!=null)
	      logger.log(Level.SEVERE,"orderType="+orderType);
		}  
	   
	   logger.log(Level.SEVERE,"Proceeding For Query Execution");
	   String className=quer.getQuerryObject().getClass().getName();
	   query=ofy().load().type(quer.getQuerryObject().getClass())
			   .filter(filterFieldArray[0],filterValueArray[0])
			   .filter(filterFieldArray[1],filterValueArray[1])
			   .filter(filterFieldArray[2],filterValueArray[2])
			   .filter(filterFieldArray[3],filterValueArray[3])
			   .filter(filterFieldArray[4],filterValueArray[4])
			   .filter(filterFieldArray[5],filterValueArray[5])
			   .filter(filterFieldArray[6],filterValueArray[6])
			   .filter(filterFieldArray[7],filterValueArray[7])
			   .filter(filterFieldArray[8],filterValueArray[8])
			   .filter(filterFieldArray[9],filterValueArray[9])
			   .filter(filterFieldArray[10],filterValueArray[10])
			   .filter(filterFieldArray[11],filterValueArray[11])
			   .filter(filterFieldArray[12],filterValueArray[12])
			   .filter(filterFieldArray[13],filterValueArray[13])
			   .filter(filterFieldArray[14],filterValueArray[14])
			   .filter(filterFieldArray[15],filterValueArray[15])
			   .filter(filterFieldArray[16],filterValueArray[16])
			   .filter(filterFieldArray[17],filterValueArray[17])
			   .filter(filterFieldArray[18],filterValueArray[18])
			   .filter(filterFieldArray[19],filterValueArray[19]);
	   
	   logger.log(Level.SEVERE,"Querry To String"+query.toString());
	   logger.log(Level.SEVERE,"Querry Count"+query.count());
	   
	   logger.log(Level.SEVERE,"Query Executed And Reseting Objectify");
	      ObjectifyService.reset();
	      logger.log(Level.SEVERE,"Objectify Consistency Strong");
	      
	      ofy().consistency(Consistency.STRONG);
//	      logger.log(Level.SEVERE,"Size of Querry "+query.count());
	      logger.log(Level.SEVERE,"Type Of"+quer.getQuerryObject().getClass());
	      
		   ofy().clear();
		   int count=0;
			for(SuperModel i:query){
		
				//Ashwini Patil Date:29-08-2022 We are storing vendor details in list but when we search po by vendor, we get list of all po in which that vendor deatils present, even though it is not selecetd. So to resolve that problem filtering list through additional code.				  
				if(vendorID!=0 && className.equals("com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder")) {//!vendorName.equals("")&&
					logger.log(Level.SEVERE,"vendor present");
					PurchaseOrder po=(PurchaseOrder)i;
					List<VendorDetails> vendorList=new ArrayList<VendorDetails>();
					if(po.getVendorDetails()!=null) {
						vendorList =po.getVendorDetails();
						for(VendorDetails vd:vendorList) {
							if(vd.getVendorId()==vendorID&&vd.getStatus()==true){
								list.add(i);
								count++;
								}
						}
					}
				}else if(className.equals("com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip")){
					logger.log(Level.SEVERE,"In payslip search");
					PaySlip payslip=(PaySlip)i;
					String salaryPeriod=payslip.getSalaryPeriod();
					String year=salaryPeriod.substring(0, 5);
					String month=salaryPeriod.substring(5);
					String sp="";

					switch(month){
					case "Jan":
						sp=year+"01";
						break;
					case "Feb":
						sp=year+"02";
						break;
					case "Mar":
						sp=year+"03";
						break;
					case "Apr":
						sp=year+"04";
						break;
					case "May":
						sp=year+"05";
						break;
					case "Jun":
						sp=year+"06";
						break;
					case "Jul":
						sp=year+"07";
						break;
					case "Aug":
						sp=year+"08";
						break;
					case "Sep":
						sp=year+"09";
						break;
					case "Oct":
						sp=year+"10";
						break;
					case "Nov":
						sp=year+"11";
						break;
					case "Dec":
						sp=year+"12";
						break;
					}
					payslip.setSalaryPeriod(sp);
					list.add(payslip);
				}else if(isServiceSearch&&!serviceBranch.equals("")){
					Service service=(Service)i;
					
					if(service.getServiceBranch()!=null&&service.getServiceBranch().equals(serviceBranch))
						list.add(service);
				}else if(isdateFilterActive&&isBillSearch&&orderType!=null&&!orderType.equals("")){
					BillingDocument bill=(BillingDocument)i;
					if(bill.getTypeOfOrder().equals(orderType))
						list.add(bill);
				}else if(isdateFilterActive&&isExpenseSearch&&branch!=null&&!branch.equals("")){
					MultipleExpenseMngt exp=(MultipleExpenseMngt)i;
					if(exp.getBranch().equals(branch))
						list.add(exp);
				}else if(isdateFilterActive&&isExpenseSearch&&personCount>0){
					MultipleExpenseMngt exp=(MultipleExpenseMngt)i;
					if(exp.getPersonInfo().getCount()== personCount)
						list.add(exp);
				}else if(isdateFilterActive&&isExpenseSearch&&status!=null&&!status.equals("")){
					MultipleExpenseMngt exp=(MultipleExpenseMngt)i;
					if(exp.getStatus().equals(status))
						list.add(exp);
				}else {
				    list.add(i);
					count++;
				}
					
			}
			 logger.log(Level.SEVERE,"Data Binded To List");
	   }
	   catch(Exception e)
	   {
		   e.printStackTrace();
	   }
	      	
		
	return list;
   }
   
   /* (non-Javadoc)
    * @see com.simplesoftwares.client.library.libservice.GenricService#getCount(com.simplesoftwares.client.library.appstructure.SuperModel)
    */
   @Override
   public int getCount(SuperModel m)
   {
	   if(m.getCount()==0)
	   {
	   int count =ofy().load().type(m.getClass()).count();
	   return count;
	   }
	   return m.getCount();
   }
   
   
   
   
   /**
    * Parses the date.
    *
    * @return the date
    */
   public static Date parseDate()
	{   
	   Date todayWithZeroTime=null;
	   try{
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		todayWithZeroTime =formatter.parse(formatter.format(today));
		Logger logger = Logger.getLogger("NameOfYourLogger");
       	logger.log(Level.SEVERE,todayWithZeroTime+"");
       	logger.log(Level.SEVERE,"SAVIAM");
	     
	}
	catch(Exception e)
	{
		e.printStackTrace();
		
	}
		
	   return todayWithZeroTime;
	}
   
   
   /**
    * Parses the date.
    *
    * @param date the date
    * @return the date
    */
   public static Date parseDate(Date date)
   
  	{   
  	   Date todayWithZeroTime=null;
  	   try{
  		
  		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        todayWithZeroTime =formatter.parse(formatter.format(date));
  	     
  	}
  	catch(Exception e)
  	{
  		e.printStackTrace();
  		System.out.println("NAME IS ");
  	}
  		
  	   return todayWithZeroTime;
  	}

   
   
   /* (non-Javadoc)
    * @see com.simplesoftwares.client.library.libservice.GenricService#save(java.util.ArrayList)
    */
   @Override
	public ArrayList<ReturnFromServer> save(ArrayList<SuperModel> m) {
		ArrayList<ReturnFromServer> server = new ArrayList<ReturnFromServer>();
		SuperModel model = m.get(0);
		int listSize = m.size();
		long finalno = 0;

		SingletoneNumberGeneration numGen = SingletoneNumberGeneration.getSingletonInstance();

		if (model.getCount() == 0) {
			String sr[] = model.getClass().getName().split("\\.");
			NumberGeneration ng = null;

			if (model instanceof BillingDocument) {
				boolean processconfigflag = checkprocessconfiguration(model);
				if (processconfigflag) {
					/*
					 * Commented by Ashwini
					 */
					// String selectedNumberRange = ((BillingDocument)
					// model).getNumberRange();
					// String prefixselectedNumberRange = "B_"+
					// selectedNumberRange;
					// finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange,
					// model.getCompanyId(), listSize);
					// } else {
					// finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1],
					// model.getCompanyId(), listSize);
					// }
					
					/*
					 * Date:25/07/2018 Developer:Ashwini Des:
					 */
					/**
					 * @author Anil , Date : 24-04-2019
					 * BUG reported by Pecopp and Rohan
					 * Billing ids were getting duplicaticated
					 * this is because while reading number generation and updating number generation, 
					 * it was updated by one actully it should be the as per the no. of records sent for save operation
					 */
					if (((BillingDocument) model).getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)) {
						System.out.println("ordertypeservice");
						String selectedNumberRange = ((BillingDocument) model).getNumberRange();
						String prefixselectedNumberRange = "B_"+ selectedNumberRange;
						finalno = numGen.getLastUpdatedNumber(prefixselectedNumberRange,model.getCompanyId(), listSize);
					} else if (((BillingDocument) model).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)) {
						System.out.println("ordertypesales");
						String selectedNumberRange = ((BillingDocument) model).getNumberRange();
						String prefixselectedNumberRange = "SOB_"+ selectedNumberRange;
						finalno = numGen.getLastUpdatedNumber(prefixselectedNumberRange,model.getCompanyId(), listSize);
					}

				} else {
					finalno = numGen.getLastUpdatedNumber(sr[sr.length - 1],model.getCompanyId(), listSize);
				}

				/*
				 * End by Ashwini
				 */
			} else if (model instanceof Service) {
				boolean processconfigflag = checkprocessconfiguration(model);
				if (processconfigflag) {
					/*
					 * Commented by Ashwini
					 */

					String selectedNumberRange = ((Service) model).getNumberRange();
					String prefixselectedNumberRange = "S_"+ selectedNumberRange;
					finalno = numGen.getLastUpdatedNumber(prefixselectedNumberRange, model.getCompanyId(),listSize);
				} else {
					finalno = numGen.getLastUpdatedNumber(sr[sr.length - 1],model.getCompanyId(), listSize);
				}

			} else if (model instanceof CustomerPayment) {
				boolean processconfigflag = checkprocessconfiguration(model);
				if (processconfigflag) {
					/*
					 * Commented by Ashwini
					 */

					// String selectedNumberRange = ((CustomerPayment)
					// model).getNumberRange();
					// String prefixselectedNumberRange = "P_"+
					// selectedNumberRange;
					// finalno=numGen.getLastUpdatedNumber(prefixselectedNumberRange,
					// model.getCompanyId(), listSize);
					// } else {
					// =numGen.getLastUpdatedNumber(sr[sr.length - 1],
					// model.getCompanyId(), listSize);
					// }
					// }else {
					// finalno=numGen.getLastUpdatedNumber(sr[sr.length - 1],
					// model.getCompanyId(), listSize);
					// }

					if (((CustomerPayment) model).getTypeOfOrder().equals(AppConstants.ORDERTYPESERVICE)) {
						String selectedNumberRange = ((CustomerPayment) model).getNumberRange();
						String prefixselectedNumberRange = "P_"+ selectedNumberRange;
						finalno = numGen.getLastUpdatedNumber(prefixselectedNumberRange,model.getCompanyId(), listSize);
					} else if (((CustomerPayment) model).getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)) {
						String selectedNumberRange = ((CustomerPayment) model).getNumberRange();
						String prefixselectedNumberRange = "SOP_"+ selectedNumberRange;
						finalno = numGen.getLastUpdatedNumber(prefixselectedNumberRange,model.getCompanyId(), listSize);
					}

				} else {
					finalno = numGen.getLastUpdatedNumber(sr[sr.length - 1],model.getCompanyId(), listSize);
				}
			}
			/**
			 * Date - 24-09-2018 By Vijay Des :- Bug :- document id genrated as
			 * 0 so else block added to create id
			 */
			else {
				finalno = numGen.getLastUpdatedNumber(sr[sr.length - 1],model.getCompanyId(), listSize);
			}

		}
		if (model.getCount() == 0) {
			for (SuperModel model1 : m) {
				ReturnFromServer temp = new ReturnFromServer();
				temp.id = model1.getId();
				int dd = (int) finalno;
				model1.setCount(dd);
				temp.count = model1.getCount();
				finalno++;
				server.add(temp);
			}
		} else {
			for (SuperModel model1 : m) {
				ReturnFromServer temp = new ReturnFromServer();
				temp.id = model1.getId();
				temp.count = model1.getCount();
				server.add(temp);
			}
		}

		ofy().save().entities(m).now();
		// for (int i = 0; i < m.size(); i++) {
		// if (server.get(i).id == null)
		// server.get(i).id = m.get(i).getId();
		// }

		return server;
	}
   
   /**
    * Date : 02-04-2018 By Anil
    * Old code for saving data in bulk,used for bill and service creation process
    */
   
///* (non-Javadoc)
// * @see com.simplesoftwares.client.library.libservice.GenricService#save(java.util.ArrayList)
// */
//@Override
//public ArrayList<ReturnFromServer> save(ArrayList<SuperModel> m) {
//	Logger logerSave=Logger.getLogger("Save Logger");
//	System.out.println("AryLIst");
//	int count=ofy().load().type(m.get(0).getClass()).count();
//	ArrayList<ReturnFromServer>server=new ArrayList<ReturnFromServer>();
//	
//	logerSave.log(Level.SEVERE,"Logger save");
//	for(SuperModel model:m)
//	{
//		ReturnFromServer temp=new ReturnFromServer();
//		server.add(temp);
//		temp.id=model.getId();
//	
//	  if(model.getCount()==0)
//      {
//		  logerSave.log(Level.SEVERE,"Count is 0");
//		/*******************************/
//
//
//		  /*********************** vijay *****************************/
//			long finalno = 0;
//			String sr[]=model.getClass().getName().split("\\.");
//			NumberGeneration ng = null;
//			
////			List<NumberGeneration> numberGenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", model.getCompanyId()).list();
//
//			 if(model instanceof BillingDocument){
//				 logger.log(Level.SEVERE,"In the BillingDocument entity");
//					boolean processconfigflag = checkprocessconfiguration(model);
//					
//					logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);					
//					System.out.println("Value-----"+processconfigflag);
//					if(processconfigflag){
//						System.out.println("Inside process config");
//						List<NumberGeneration> numberGenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", model.getCompanyId()).list();
//						System.out.println("Class number range type:::"+((BillingDocument) model).getNumberRange());
//						  logger.log(Level.SEVERE,"selected type from drop down=="+((BillingDocument) model).getNumberRange());
//
//						if(((BillingDocument) model).getNumberRange()!=null){
//							
//						String selectedNumberRange = ((BillingDocument) model).getNumberRange();
//						String prefixselectedNumberRange ="B_"+selectedNumberRange;
//						System.out.println("string added prefix===:"+prefixselectedNumberRange);
//						  logger.log(Level.SEVERE,"string added prefix===:"+prefixselectedNumberRange);
//
//						for(int i=0;i<numberGenrationList.size();i++){
//							System.out.println("Process name==="+numberGenrationList.get(i).getProcessName());
//							if(prefixselectedNumberRange.equals(numberGenrationList.get(i).getProcessName())){
//								System.out.println("DONE=====");
//								logger.log(Level.SEVERE,"Done here new number gen");
//								ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//							}
//						}
//							
//						}else{
//							  logger.log(Level.SEVERE,"if number range is null means not selected");
//							System.out.println("if number range is null means not selected");
//							ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//						}
//						
//					}else{
//						logger.log(Level.SEVERE,"if process config is false then normal procedure");
//						System.out.println("Inside no process config");
//						ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//					}
//					
//			 }
//			 else if(model instanceof Service){
//				 System.out.println("In service ===");
//				 logger.log(Level.SEVERE,"In the Service entity");
//
//				  logger.log(Level.SEVERE,"In Service ");
//
//					boolean processconfigflag = checkprocessconfiguration(model);
//					logger.log(Level.SEVERE,"After process configuration checking Value is::"+processconfigflag);					
//					
//					if(processconfigflag){
//						System.out.println("In the process config true");
//						List<NumberGeneration> numbergenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", model.getCompanyId()).list();
//						if(((Service) model).getNumberRange()!=null){
//							
//							String selectedNumberRange = ((Service) model).getNumberRange();
//							String prefixselectedNumberRange = "S_"+selectedNumberRange;
//							
//							  logger.log(Level.SEVERE,"selected type from drop down=="+((Service) model).getNumberRange());
//
//							for(int i=0;i<numbergenrationList.size();i++){
//								
//								if(prefixselectedNumberRange.equals(numbergenrationList.get(i).getProcessName())){
//									logger.log(Level.SEVERE,"Done here new number gen");
//									ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//								}
//							}
//						}
//						else{
//							  logger.log(Level.SEVERE,"if number range is null means not selected");
//							ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//						}
//					}
//					else{
//						logger.log(Level.SEVERE,"if process config is false then normal procedure");
//						ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//					}
//				 
//			 }
//			 else{
//				 	System.out.println("For other entity");
//					 logger.log(Level.SEVERE,"This is a normal senario of all number genration ");
//					ng=ofy().load().type(NumberGeneration.class).filter("companyId",model.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//
//			 }
//			
//			/******************* changes end here ******************/ 
//		  
//		  
//		  
////			long finalno = 0;
////			String sr[]=model.getClass().getName().split("\\.");
//			
//			System.out.println("Not Static Int Company Successfully Registered");
////			NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("companyId", model.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
//			
//			 logger.log(Level.SEVERE,"Last number ==== "+ng.getNumber());
//
//			System.out.println("After"+ng.getNumber());
//			ng.setNumber(ng.getNumber()+1);
//			this.save(ng);
//			finalno=ng.getNumber();
//			System.out.println("NGGGG"+ng.getNumber());
//			
//			
//			int dd=(int)finalno;
//			model.setCount(dd);
//		
////			SaveHistory.saveHistoryData(model.getCompanyId(), null, sr[sr.length-1], dd, DocumentHistory.CREATED, model.getCreatedBy(),model.getUserId());  
//		  /*************************************/
//		  
//    	//model.setCount(count+1);
//    	temp.count=model.getCount();
//
//    	//count++;
// 
//      }
//	  logerSave.log(Level.SEVERE,"For loop end");
//	}
//    //Setting count
//	logerSave.log(Level.SEVERE,"Ofy Save Calling");
//	
//   ofy().save().entities(m).now();
//   for(int i=0;i<m.size();i++)
//   {
//	  if(server.get(i).id==null)
//		  server.get(i).id=m.get(i).getId();
//   }
//   
//    
//   return server;
//}

@Override
public void putInMemCache(SuperModel model) {
	// TODO Auto-generated method stub
	
}

@SuppressWarnings("unchecked")
@Override
public ReturnFromServer saveDemoConfigData(SuperModel m) {
	System.out.println("In Save Server"+m.getCount());
	int mode=ofy().load().type(ProcessName.class).filter("companyId",m.getCompanyId()).count();
	if(m.getCount()!=0)
	{
		if(!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")&&!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")){
			String sr[]=m.getClass().getName().split("\\.");
			if(mode!=0){
//				SaveHistory.saveHistoryData(m.getCompanyId(), null, sr[sr.length-1], m.getCount(), DocumentHistory.UPDATED, m.getCreatedBy(),m.getUserId());
			}
		}
	}
	
	if(m.getCount()==0)
	{
		long finalno = 0;
		Logger logger = Logger.getLogger("Saving Logger");
		logger.log(Level.SEVERE,"Class name"+m.getClass().getName().trim());
		String sr[]=m.getClass().getName().split("\\.");
		logger.log(Level.SEVERE,"Length"+sr.length);
		logger.log(Level.SEVERE,"Ind"+sr[sr.length-1]);
		
		
		if(mode==0){
			 int  count=ofy().load().type(m.getClass()).count();
			m.setCount(count+1);
		}
		else{
			if(!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")&&!m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")){
				
				NumberGeneration ng;
				
				
				if(m.getCompanyId()==null){
					ng=ofy().load().type(NumberGeneration.class).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
				}
				else{
					ng=ofy().load().type(NumberGeneration.class).filter("companyId",m.getCompanyId()).filter("processName", sr[sr.length-1]).filter("status", true).first().now();
				}
				
				ng.setNumber(ng.getNumber()+1);
				this.save(ng);
				finalno=ng.getNumber();
			}
		}
		
		if(m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.NumberGeneration")||m.getClass().getName().equals("com.slicktechnologies.shared.common.numbergenerator.ProcessName")){
			
			int count=0;
			if(m.getCompanyId()==null){
				count=ofy().load().type(m.getClass()).count();
			}
			else{
				count=ofy().load().type(m.getClass()).filter("companyId",m.getCompanyId()).count();
			}
			
			m.setCount(count+1);
		}
		else{
			if(mode>0){
				int dd=(int)finalno;
//				SaveHistory.saveHistoryData(m.getCompanyId(), null, sr[sr.length-1], dd, DocumentHistory.CREATED, m.getCreatedBy(),m.getUserId());
				m.setCount(dd);
			}
		}
	}
	String sr[]=m.getClass().getName().split("\\.");
	String str = sr[sr.length-1];
	
	if(str.equals("ConfigCategory")){
		ConfigCategory conCat=(ConfigCategory) m;
		System.out.println("CATEGORY CONFIG :: "+conCat.getCount());
		conCat.setCategoryCode("CC"+conCat.getCount());
	}
	
	if(str.equals("Type")){
		Type conCat=(Type) m;
		System.out.println("TYPE CONFIG :: "+conCat.getCount());
		conCat.setTypeCode("TT"+conCat.getCount());
	}
	
    ofy().save().entity(m).now();
    //Setting count
//  int  count=ofy().load().type(m.getClass()).count();
   
    ReturnFromServer server = new ReturnFromServer();
    server.id=m.getId();
    System.out.println("MCOUNT"+m.getCount());
    server.count=m.getCount();
   return server;
}

/**
 *  Date: 90/06/2017  Rahul merge this code of process config for Nbhc  
 * 
 */


private boolean checkprocessconfiguration(SuperModel m) {
	boolean flag = false;
	boolean loadProcess = false;
	if (m instanceof Contract) {
		Contract contract = (Contract) m;
		if (contract.getNumberRange() != null
				&& !contract.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	} else if (m instanceof BillingDocument) {
		BillingDocument billdoc = (BillingDocument) m;
		if (billdoc.getNumberRange() != null
				&& !billdoc.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	} else if (m instanceof Invoice) {
		Invoice invoice = (Invoice) m;
		if (invoice.getNumberRange() != null
				&& !invoice.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	} else if (m instanceof CustomerPayment) {
		CustomerPayment customerPayment = (CustomerPayment) m;
		if (customerPayment.getNumberRange() != null
				&& !customerPayment.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}// below else if code added by vijay on 14-02-2018 for number range issue for services
	else if (m instanceof Service) {
		Service service = (Service) m;
		if (service.getNumberRange() != null
				&& !service.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}
	/**
	 * Date : 15-05-2018 BY ANIL
	 * added number range process for employee
	 * requirement by Sasha ERP.
	 */
	else if (m instanceof Employee) {
		Employee emp = (Employee) m;
		if (emp.getNumberRange() != null&& !emp.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}

//	/*
//	 * Date:23/07/2018
//	 * Developer:Ashwini
//	 * Des:To add number range in sales order and delivery notes.
//	 */
	else if (m instanceof SalesOrder) {
		SalesOrder sale = (SalesOrder) m;
		if (sale.getNumberRange() != null&& !sale.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}

	else if (m instanceof DeliveryNote) {
		DeliveryNote deliver = (DeliveryNote) m;
		if (deliver.getNumberRange() != null&& !deliver.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}
	
	/*
	 * End by Ashwini
	 */
	
	else if (m instanceof Lead) {
		Lead lead = (Lead) m;
		if (lead.getNumberRange() != null&& !lead.getNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}
	else if (m instanceof Quotation) {
		Quotation quotation = (Quotation) m;
		if (quotation.getQuotationNumberRange() != null&& !quotation.getQuotationNumberRange().equals("")) {
			loadProcess = true;
		} else {
			return flag;
		}
	}
	
	//Ashwini Patil Date:4-04-2022 As per Nitin sir's instruction, no need to have any other configuration to apply number range. If number range selected then it will get applied.
//	if (loadProcess) {
//		
//		ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","NumberRange").filter("status",true).filter("companyId", m.getCompanyId()).first().now();
//		ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "NumberRange").filter("processList.processType", "NumberRangeProcess").filter("processList.status", true).filter("companyId", m.getCompanyId()).first().now();			
//		if (processName != null) {
//		if(processconfig!=null){
//			if(processconfig.isConfigStatus()){
//				for(int l=0;l<processconfig.getProcessList().size();l++){
//					if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("NumberRange") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("NumberRangeProcess") && processconfig.getProcessList().get(l).isStatus()==true){
//						 flag=true;
//					}
//				}
//		   }
//		}	
//		}
//		
//	}
	if (loadProcess) {
		flag=true;
	}

	return flag;
}
/**
 *  End Here  
 */

@Override
public ArrayList<Contract> getContractSearchResult(MyQuerry quer) {

	List<Contract> contractList=new ArrayList<Contract>();
	List<Contract> filteredContractList=new ArrayList<Contract>();
	   try{
	   ofy().clear();
	   
	   String[] filterFieldArray=new String[20];
	   Object[] filterValueArray=new Object[20];
	   
	   /**
	    * @author Ashwini Patil
	    * @since 20-04-2022
	    * New logic has been added to make date filters work in combination with other filters.
	    */
	   String[] noIndexfilterFieldArray=new String[20];
	   Object[] noIndexfilterValueArray=new Object[20];
	   boolean datefilterFlag=false;
	   boolean contractDatefilterFlag=false;
	   boolean statusfilterFlag=false;
	   boolean noIndexFilterFlag=false;
	   int noIndexFilterCount=0;
	   int otherFilterCount=0;
		
	   
	   Logger logger = Logger.getLogger("NameOfYourLogger");
	   if(quer.getFilters()!=null)
	   {
			int ctr = 1;
			logger.log(Level.SEVERE, "Number Of Filters Applied"+ quer.getFilters().size());
			
			for (int i = 0; i < quer.getFilters().size(); i++) {
				String conditionField = quer.getFilters().get(i).getQuerryString();
				Object value = quer.getFilters().get(i).geFilterValue();

				logger.log(Level.SEVERE, "Processing Filters "+conditionField);

				Date condDate = null;

				if (value instanceof Date) {
					condDate = DateUtility.getDateWithTimeZone("IST",(Date) value);
					Date newconDate = null;
					if (ctr == 1) {
						if (conditionField.contains("<=")|| conditionField.contains(">=")
								|| conditionField.contains(">")|| conditionField.contains("<")) {
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(condDate);
							// calendar.set(Calendar.MILLISECOND, 000);
							calendar.set(Calendar.SECOND, 00);
							calendar.set(Calendar.MINUTE, 00);
							calendar.set(Calendar.HOUR, 00);
							newconDate = calendar.getTime();
							value = newconDate;
							logger.log(Level.SEVERE, "From Date :: "+ value);
							ctr++;
						} else {
							value = condDate;
							logger.log(Level.SEVERE, "EQUAL Date :: "+ value);
						}
					} else {
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(condDate);
						// calendar.set(Calendar.MILLISECOND, 999);
						calendar.set(Calendar.SECOND, 59);
						calendar.set(Calendar.MINUTE, 59);
						calendar.set(Calendar.HOUR, 23);
						newconDate = calendar.getTime();
						value = newconDate;
						logger.log(Level.SEVERE, "From Date :: " + value);
					}
					if(conditionField.equals("contractDate >=")||conditionField.equals("contractDate <=")) {
						contractDatefilterFlag=true;
					}
					datefilterFlag=true;
					
				}

				if(conditionField.equals("cinfo.cellNumber")||conditionField.equals("cinfo.count")||conditionField.equals("cinfo.fullName")||conditionField.equals("count")||conditionField.equals("items.serviceProduct.count")||conditionField.equals("leadCount")||conditionField.equals("quotationCount")||conditionField.equals("refNo")||conditionField.equals("refNo2")||conditionField.equals("ticketNumber")||conditionField.equals("branch") ) {
					noIndexFilterFlag=true;
					noIndexfilterFieldArray[noIndexFilterCount] = conditionField;
					noIndexfilterValueArray[noIndexFilterCount] = value;	
					noIndexFilterCount++;
					logger.log(Level.SEVERE, "added to noIndexFilter");					
				}else {
					if(conditionField.equals("status"))
						statusfilterFlag=true;
					filterFieldArray[otherFilterCount] = conditionField;
					filterValueArray[otherFilterCount] = value;		
					otherFilterCount++;
					logger.log(Level.SEVERE, "added to other filter");
				}
				
				if (i == quer.getFilters().size() - 1) {
//					for (int j = quer.getFilters().size() - 1; j < filterFieldArray.length; j++) {
//						filterFieldArray[j] = conditionField;
//						filterValueArray[j] = value;
//					}
					if(datefilterFlag) {
						if(contractDatefilterFlag&&!statusfilterFlag) {
							List<String> statuslist=new ArrayList<String>();
							statuslist.add("Created");statuslist.add("Created");statuslist.add("Requested");statuslist.add("Invoiced");statuslist.add("Approved");
							filterFieldArray[otherFilterCount] = "status IN";
							filterValueArray[otherFilterCount] = statuslist;
							otherFilterCount++;
							for (int j = otherFilterCount; j < filterFieldArray.length; j++) {
								filterFieldArray[j] = conditionField;
								filterValueArray[j] = value;
							}
							logger.log(Level.SEVERE,"status filter added manually");
						}else {
							for (int j = otherFilterCount; j < filterFieldArray.length; j++) {
								filterFieldArray[j] = conditionField;
								filterValueArray[j] = value;
							}
						}
											
						logger.log(Level.SEVERE,"in datefilterFlag "+conditionField+ "added to all empty places");
					}else {
						for (int j = 0; j < noIndexFilterCount; j++) {
							filterFieldArray[otherFilterCount] = noIndexfilterFieldArray[j];
							filterValueArray[otherFilterCount] = noIndexfilterValueArray[j];
							otherFilterCount++;
						}
						for (int j = otherFilterCount; j < filterFieldArray.length; j++) {
							filterFieldArray[j] = conditionField;
							filterValueArray[j] = value;
						}		
						logger.log(Level.SEVERE,conditionField+ "added to all empty places");						
					}				
					
				}
			}
		}  
	   
	   logger.log(Level.SEVERE,"Proceeding For Query Execution");
	   contractList=ofy().load().type(Contract.class)
			   .filter(filterFieldArray[0],filterValueArray[0])
			   .filter(filterFieldArray[1],filterValueArray[1])
			   .filter(filterFieldArray[2],filterValueArray[2])
			   .filter(filterFieldArray[3],filterValueArray[3])
			   .filter(filterFieldArray[4],filterValueArray[4])
			   .filter(filterFieldArray[5],filterValueArray[5])
			   .filter(filterFieldArray[6],filterValueArray[6])
			   .filter(filterFieldArray[7],filterValueArray[7])
			   .filter(filterFieldArray[8],filterValueArray[8])
			   .filter(filterFieldArray[9],filterValueArray[9])
			   .filter(filterFieldArray[10],filterValueArray[10])
			   .filter(filterFieldArray[11],filterValueArray[11])
			   .filter(filterFieldArray[12],filterValueArray[12])
			   .filter(filterFieldArray[13],filterValueArray[13])
			   .filter(filterFieldArray[14],filterValueArray[14])
			   .filter(filterFieldArray[15],filterValueArray[15])
			   .filter(filterFieldArray[16],filterValueArray[16])
			   .filter(filterFieldArray[17],filterValueArray[17])
			   .filter(filterFieldArray[18],filterValueArray[18])
			   .filter(filterFieldArray[19],filterValueArray[19]).list();
	   
	   logger.log(Level.SEVERE,"Querry Count"+contractList.size());
	   logger.log(Level.SEVERE,"Query Executed And Reseting Objectify");
       ObjectifyService.reset();
       logger.log(Level.SEVERE,"Objectify Consistency Strong");
	   ofy().consistency(Consistency.STRONG);
	   logger.log(Level.SEVERE,"Type Of"+quer.getQuerryObject().getClass());
	   ofy().clear();
	   logger.log(Level.SEVERE,"Data Binded To List");
	
	   	
	   if(datefilterFlag&&noIndexFilterFlag) {
		   logger.log(Level.SEVERE,"in if(datefilterFlag&&noIndexFilterFlag)");
		   for (int j = 0; j < noIndexFilterCount; j++) {
			  logger.log(Level.SEVERE,"noIndexfilterFieldArray[j]="+noIndexfilterFieldArray[j]);
			  if(noIndexfilterFieldArray[j].equals("cinfo.count")) {
				  for(Contract c:contractList) {
					  int ct=(int)noIndexfilterValueArray[j];
					  if(c.getCinfo().getCount()==ct)
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("count")) {
				  for(Contract c:contractList) {
					  int ct=(int)noIndexfilterValueArray[j];
					  if(c.getCount()==ct)
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("leadCount")) {
				  for(Contract c:contractList) {
					  int ct=(int)noIndexfilterValueArray[j];
					  if(c.getLeadCount()==ct)
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("quotationCount")) {
				  for(Contract c:contractList) {
					  int ct=(int)noIndexfilterValueArray[j];
					  if(c.getQuotationCount()==ct)
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("refNo")) {
				  for(Contract c:contractList) {
					  if(c.getRefNo()==noIndexfilterValueArray[j])
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("refNo2")) {
				  for(Contract c:contractList) {
					  if(c.getRefNo2()==noIndexfilterValueArray[j])
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("ticketNumber")) {
				  for(Contract c:contractList) {
					  if(c.getTicketNumber()==noIndexfilterValueArray[j])
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }
			  if(noIndexfilterFieldArray[j].equals("items.serviceProduct.count")) {
				  for(Contract c:contractList) {
					  List<SalesLineItem> prodList=new ArrayList<SalesLineItem>();
					  prodList=c.getItems();
					  for(SalesLineItem s:prodList) {
						  logger.log(Level.SEVERE,"product id="+s.getPrduct().getCount()+"and arrayValue="+noIndexfilterValueArray[j]);
						  if(s.getPrduct().getCount()==(int)noIndexfilterValueArray[j])
							  filteredContractList.add(c);	
					  }
				  }
				  contractList=filteredContractList;
			  }		
			  //Ashwini Patil Date:23-03-2023
			  if(noIndexfilterFieldArray[j].equals("branch")) {
				  for(Contract c:contractList) {
					  if(c.getBranch().equals(noIndexfilterValueArray[j]))
						  filteredContractList.add(c);
				  }
				  contractList=filteredContractList;
			  }	
		   }
	   }
	   }
	   catch(Exception e)
	   {
		   e.printStackTrace();
	   }
	   
	 ArrayList<Contract> list=new ArrayList<Contract>(contractList);     	
	 logger.log(Level.SEVERE,"Data Binded To List  "+list.size());
	 

	 
	return list;

}


@Override
public String nbhcContractServiceCycleDelProcess(Long companyId) {
	
	try {
		Queue queue = QueueFactory.getQueue("NBHCInterfaceDeleteProcess-Queue");

		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/nbhcinterfacedeleteprocessqueue").param(
						"companyId", companyId+""));
		return "Success";
	} catch (Exception e) {
		logger.log(Level.SEVERE,e.getMessage() );
		return "Fails";
	}
	
	
	
	// TODO Auto-generated method stub
	
}

/**
 * Date : 22-03-2018 BY ANIL
 * This method is used to save data at the time of link creation when process name and number generation is not defined 
 */

@Override
public ReturnFromServer saveForNewLink(SuperModel m) {
	Logger logger = Logger.getLogger("Saving Logger");
	logger.log(Level.SEVERE,"Class name"+m.getClass().getName().trim());
	if(m.getCount()==0){
		long finalno = 0;
		logger.log(Level.SEVERE,"Class name"+m.getClass().getName().trim());
		String sr[]=m.getClass().getName().split("\\.");
		logger.log(Level.SEVERE,"Class Name"+m.getClass().getName().trim());
		logger.log(Level.SEVERE,"Length"+sr.length);
		logger.log(Level.SEVERE,"Ind"+sr[sr.length-1]);
		int count=0;
		if(m.getCompanyId()==null){
			count=ofy().load().type(m.getClass()).count();
		}else{
			count=ofy().load().type(m.getClass()).filter("companyId",m.getCompanyId()).count();
		}
		m.setCount(count+1);
	}


	/**********************************************/  // comment code for  without encryption 
	String sr[]=m.getClass().getName().split("\\.");
	String str = sr[sr.length-1];
//    if(str.equals("User")){
//		System.out.println("hiiiiiiiiii");
//		User use = (User) m;
//		
//		String  originalPassword = use.getPassword();
//        String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt());
//        System.out.println("Hashcode:"+generatedSecuredPasswordHash);
//        use.setPassword(generatedSecuredPasswordHash);
//        System.out.println("End password");
//	}
	
	//System.out.println("before==============");
	
	
	/************************************************/


	ofy().save().entity(m).now();

	ReturnFromServer server = new ReturnFromServer();
	server.id = m.getId();
	System.out.println("MCOUNT" + m.getCount());
	server.count = m.getCount();
	return server;
}


@Override
public String contractServiceCycleCanceltionProcess(Long companyId) {
	
	try {
		Queue queue = QueueFactory.getQueue("ServiceCancellationTaskQueue-Queue");

		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/ServiceCancellationTaskQueue").param(
						"companyId", companyId+"").param("processName", "ServiceCanceltion"));
		return "Success";
	} catch (Exception e) {
		logger.log(Level.SEVERE,e.getMessage() );
		return "Fails";
	}
	
	
	
	// TODO Auto-generated method stub
	
}


	@Override
	public String productPriceUtilty(Long companyId) {
		// TODO Auto-generated method stub
		
		try {
			Queue queue = QueueFactory.getQueue("ServiceCancellationTaskQueue-Queue");

			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/ServiceCancellationTaskQueue").param(
							"companyId", companyId+"").param("processName", "ProductUtilty"));
			return "Success";
		} catch (Exception e) {
			logger.log(Level.SEVERE,e.getMessage() );
			return "Fails";
		}
	}

}





