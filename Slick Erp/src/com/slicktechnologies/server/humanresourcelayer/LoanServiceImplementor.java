package com.slicktechnologies.server.humanresourcelayer;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.humanresource.advance.LoanService;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;

public class LoanServiceImplementor extends RemoteServiceServlet implements LoanService 
{

	private static final long serialVersionUID = -174200722713344477L;
	@Override
	public double getPreviousBalance(int empId, long companyId) 
	{
		Double amt=Loan.calculatePreviousBalance(empId, companyId);
		System.out.println("Value of Amount is "+amt);
		return amt;
	}

}
