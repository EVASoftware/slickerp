package com.slicktechnologies.server.humanresourcelayer;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.humanresource.allocation.ctcallocation.CtcAllocationService;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.arrears.ArrearsInfoBean;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CtcAllocationServiceImpl extends RemoteServiceServlet implements CtcAllocationService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3044904479121373797L;
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	@Override
	public ArrayList<AllocationResult> getAllocationStatus(ArrayList<Filter> filter) {
		ArrayList<AllocationResult> allocationResult = new ArrayList<AllocationResult>();
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new EmployeeInfo());
		if (filter.size() != 0) {
			querry.getFilters().addAll(filter);
		}

		GenricServiceImpl impl = new GenricServiceImpl();
		List<SuperModel> modelLis = impl.getSearchResult(querry);
		
		for (SuperModel temp : modelLis) {
			AllocationResult result = new AllocationResult();
			EmployeeInfo employee = (EmployeeInfo) temp;
			result.setInfo(employee);
			result.setStatus(true);
			if(employee.getLeaveCalendar()==null){
//				result.setReason("Calendar is not allocated.");
			}else{
				result.setReason(employee.getCalendarName());
			}
			allocationResult.add(result);
		}
		return allocationResult;
	}

	@Override
	public ArrayList<AllocationResult> allocateCtc(ArrayList<AllocationResult> allocationArray,CTCTemplate ctcTemplate,Date applicableFromDate,Date applicableToDate,Date effectiveFromDate,String createdBy,Date effectiveToDate,String projectName) {
		DateUtility.getDateWithTimeZone("IST", applicableFromDate);
		DateUtility.getDateWithTimeZone("IST", applicableToDate);
		DateUtility.getDateWithTimeZone("IST", effectiveFromDate);
		
		if(effectiveToDate!=null){
			DateUtility.getDateWithTimeZone("IST", effectiveToDate);
		}
		
//		ArrayList<AllocationResult> updateExistingResult=new ArrayList<AllocationResult>(); 
		ArrayList<AllocationResult> createNewCtc=new ArrayList<AllocationResult>();
		ArrayList<Integer> empIdList=new ArrayList<Integer>();
		
		for(AllocationResult obj:allocationArray){
			if(obj.getInfo().isCtcCreated()==true){
//				updateExistingResult.add(obj);
				empIdList.add(obj.getEmpCount());
			}else{
				createNewCtc.add(obj);
			}
		}
		
		//Here we update existing ctc 
		if(empIdList.size()!=0){
			List<CTC> ctcList=ofy().load().type(CTC.class).filter("companyId", ctcTemplate.getCompanyId()).filter("empid IN", empIdList).list();
			if(ctcList!=null){
				for(CTC obj:ctcList){
					AllocationResult allObj=getAllocationResult(obj.getEmpid(), allocationArray);
					obj.setCtcAmount(ctcTemplate.getTotalCtcAmount());
					obj.setGrossEarning(ctcTemplate.getTotalCtcAmount());
					obj.setEarning(ctcTemplate.getEarning());
					obj.setDeduction(ctcTemplate.getDeduction());
					obj.setStatus(CTC.ACTIVE);
					allObj.getInfo().setCtcAllocated(true);
					
					obj.setFromdate(applicableFromDate);
					obj.setTodate(applicableToDate);
					obj.setEffectiveFromDate(effectiveFromDate);
					
					obj.setDeduction(ctcTemplate.getDeduction());
					obj.setEmployeesTotalDeduction(ctcTemplate.getTotalDeductionAmount());
					
					obj.setCompaniesContribution(ctcTemplate.getCompanyContribution());
					obj.setCompaniesTotalDeduction(ctcTemplate.getCompanyTotalDeduction());
									
					/**
					 * Rahul Verma added on 16 July 2018
					 */
					try{
						obj.setPaidLeaveApplicable(ctcTemplate.isPaidLeavesAvailable());
					}catch(Exception e){
						e.printStackTrace();
					}
					/**
					 * Ends
					 */
					allObj.setStatus(true);
					allObj.getInfo().setCtcAmount(obj.getCtcAmount());
					/**Date 11-4-2019 by Amol set the CTC Template Name**/
					allObj.getInfo().setCtcTemplateName(ctcTemplate.getCtcTemplateName());
                   
					/**
					 * Date : 21-08-2018 By ANIL
					 * Setting ctc template information in CTC master
					 */
					obj.setCtcTemplateId(ctcTemplate.getCount());
					obj.setCtcTemplateName(ctcTemplate.getCtcTemplateName());
					obj.setCtcTempAllocationDate(new Date());
					obj.setCtcTempAllocatedBy(createdBy);
					/**
					 * End
					 */
					
					/**
					 * Date : 27-08-2018 By ANIL
					 * setting paid leave and bonus 
					 */
					obj.setPaidLeaveAmt(ctcTemplate.getPaidLeaveAmt());
					obj.setBonus(ctcTemplate.isBonus());
					obj.setBonusAmt(ctcTemplate.getBonusAmt());
					obj.setMonthly(ctcTemplate.isMonthly());
					obj.setAnnually(ctcTemplate.isAnnually());
					obj.setMonth(ctcTemplate.getMonth());
					/**
					 * End
					 */
					
					/**
					 * @author Anil , Date : 19-06-2019
					 * Storing paid leave and bonus additional details
					 */
					
					obj.setPlName(ctcTemplate.getPlName());
					obj.setPlCorresponadenceName(ctcTemplate.getPlCorresponadenceName());
					obj.setPlIncludedInEarning(ctcTemplate.isPlIncludedInEarning());
					obj.setBonusName(ctcTemplate.getBonusName());
					obj.setBonusCorresponadenceName(ctcTemplate.getBonusCorresponadenceName());
					obj.setBonusIncludedInEarning(ctcTemplate.isBonusIncludedInEarning());
					
					/**
					 * @author Anil , Date : 13-07-2019
					 * Storing project name for site wise payroll generatiom
					 * Storing effective to date for arrears calculation for given duration 
					 */
					if(effectiveToDate!=null){
						obj.setEffectiveToDate(effectiveToDate);
					}
					if(projectName!=null){
						obj.setProjectName(projectName);
					}
					
					
					/**
					 * @author Anil ,Date : 13-04-2019
					 * If process configuration (LwfAsPerEmployeeGroup) is active then Lwf should be auto fetched and applicable as per the employee group
					 * Process Type : LwfAsPerEmployeeGroup Process Name : CTCAllocation 
					 */
//					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "LwfAsPerEmployeeGroup", ctcTemplate.getCompanyId())
//							&&allObj.getInfo().getEmpGrp()!=null&&allObj.getInfo().getEmpGrp().equalsIgnoreCase("Direct")){
						updatDeductions(obj,allObj.getInfo());
//					}
					
					
				}
				ofy().save().entities(ctcList).now();
			}
		}
		
		//Here we create new CTC
		if(createNewCtc.size()!=0){
			
			
			NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("processName", "CTC").filter("status", true).first().now();
			long count=ng.getNumber();
			ng.setNumber(ng.getNumber()+createNewCtc.size());
			ofy().save().entity(ng).now();
			
			ArrayList<CTC> ctcList=new ArrayList<CTC>();
			for(AllocationResult obj:createNewCtc){
				CTC ctc=new CTC();
				ctc.setCompanyId(ctcTemplate.getCompanyId());
				ctc.setCount((int) count);
				ctc.setEmpid(obj.getEmpCount());
				ctc.setEmployeeName(obj.getFullName());
				ctc.setEmpCellNo(obj.getCellNumber());
				ctc.setEmployeeType(obj.getEmployeeType());
				ctc.setEmployeeRole(obj.getEmployeerole());
				ctc.setEmployeedDesignation(obj.getDesignation());
				ctc.setBranch(obj.getBranch());
				ctc.setDepartment(obj.getDepartment());
				ctc.setCalender(obj.getCalendarName());
				ctc.setFromdate(applicableFromDate);
				ctc.setTodate(applicableToDate);
				ctc.setStatus(CTC.ACTIVE);
				ctc.setEarning(ctcTemplate.getEarning());
				ctc.setDeduction(ctcTemplate.getDeduction());
				ctc.setCtcAmount(ctcTemplate.getTotalCtcAmount());
				ctc.setGrossEarning(ctcTemplate.getTotalCtcAmount());
				count++;
				
				ctc.setFromdate(applicableFromDate);
				ctc.setTodate(applicableToDate);
				ctc.setEffectiveFromDate(effectiveFromDate);
				
				/**
				 * Rahul Verma added on 16 July 2018
				 */
				try{
					ctc.setPaidLeaveApplicable(ctcTemplate.isPaidLeavesAvailable());
				}catch(Exception e){
					e.printStackTrace();
				}
				/**
				 * Ends
				 */
				
				ctc.setDeduction(ctcTemplate.getDeduction());
				ctc.setEmployeesTotalDeduction(ctcTemplate.getTotalDeductionAmount());
				
				ctc.setCompaniesContribution(ctcTemplate.getCompanyContribution());
				ctc.setCompaniesTotalDeduction(ctcTemplate.getCompanyTotalDeduction());
				obj.getInfo().setCtcAmount(ctc.getCtcAmount());
				/**Date 12-4-2019 added by Amol**/
				obj.getInfo().setCtcTemplateName(ctcTemplate.getCtcTemplateName());
				/**
				 * Date : 21-08-2018 By ANIL
				 * Setting ctc template information in CTC master
				 */
				ctc.setCtcTemplateId(ctcTemplate.getCount());
				ctc.setCtcTemplateName(ctcTemplate.getCtcTemplateName());
				ctc.setCtcTempAllocationDate(new Date());
				ctc.setCtcTempAllocatedBy(createdBy);
				/**
				 * End
				 */
				

				/**
				 * Date : 27-08-2018 By ANIL
				 * setting paid leave and bonus 
				 */
				ctc.setPaidLeaveAmt(ctcTemplate.getPaidLeaveAmt());
				ctc.setBonus(ctcTemplate.isBonus());
				ctc.setBonusAmt(ctcTemplate.getBonusAmt());
				ctc.setMonthly(ctcTemplate.isMonthly());
				ctc.setAnnually(ctcTemplate.isAnnually());
				ctc.setMonth(ctcTemplate.getMonth());
				/**
				 * End
				 */
				
				/**
				 * @author Anil , Date : 19-06-2019
				 * Storing paid leave and bonus additional details
				 */
				
				ctc.setPlName(ctcTemplate.getPlName());
				ctc.setPlCorresponadenceName(ctcTemplate.getPlCorresponadenceName());
				ctc.setPlIncludedInEarning(ctcTemplate.isPlIncludedInEarning());
				ctc.setBonusName(ctcTemplate.getBonusName());
				ctc.setBonusCorresponadenceName(ctcTemplate.getBonusCorresponadenceName());
				ctc.setBonusIncludedInEarning(ctcTemplate.isBonusIncludedInEarning());
				
				/**
				 * @author Anil , Date : 13-07-2019
				 * Storing project name for site wise payroll generatiom
				 * Storing effective to date for arrears calculation for given duration 
				 */
				if(effectiveToDate!=null){
					ctc.setEffectiveToDate(effectiveToDate);
				}
				if(projectName!=null){
					ctc.setProjectName(projectName);
				}
				
				ctcList.add(ctc);
				obj.getInfo().setCtcAllocated(true);
				
				/**
				 * @author Anil ,Date : 13-04-2019
				 * If process configuration (LwfAsPerEmployeeGroup) is active then Lwf should be auto fetched and applicable as per the employee group
				 * Process Type : LwfAsPerEmployeeGroup Process Name : CTCAllocation 
				 */
//				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "LwfAsPerEmployeeGroup", ctcTemplate.getCompanyId())
//						&&obj.getInfo().getEmpGrp()!=null&&obj.getInfo().getEmpGrp().equalsIgnoreCase("Direct")){
					updatDeductions(ctc,obj.getInfo());
//				}
			}
			
			ofy().save().entities(ctcList).now();
		}
		
		
		return allocationArray;
	}
	public AllocationResult getAllocationResult(int empId,ArrayList<AllocationResult> allocationResult){
		AllocationResult alloRes=null;
		for(AllocationResult obj:allocationResult){
			if(obj.getEmpCount()==empId){
				return obj;
			}
		}
		return null;
	}

	@Override
	public ArrayList<EmployeeInfo> reviseSalary(ArrayList<EmployeeInfo> allocationArray, CTCTemplate ctcTemplate,Date applicableFromDate, Date applicableToDate,Date effectiveFromDate,String createdBy,Date effectiveToDate,String projectName,boolean tempWiseRevisionFlag) {
		/**
		 * @author Anil , Date:03-10-2019
		 * template wise salary revision
		 * if component wise revision is true then we can not revise it template wise
		 */
		if(tempWiseRevisionFlag){
			if(ctcTemplate!=null){
				for(CtcComponent comp:ctcTemplate.getEarning()){
					if(comp.isRevised()){
						tempWiseRevisionFlag=false;
						break;
					}
				}
			}
		}
		
		/**
		 * @author Anil , Date : 13-07-2019
		 * checking sitewisepayrollforreliever
		 * raised by rahul tiwari and Nitin kshirsagar for riseon
		 */
		boolean isSiteWasPayroll=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "SiteWisePayrollForReliever", ctcTemplate.getCompanyId())){
			isSiteWasPayroll=true;
		}
		
		/**
		 * @author Anil
		 * @since 10-09-2020
		 * EnableSiteLocation
		 */
		boolean siteLocation=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "EnableSiteLocation", ctcTemplate.getCompanyId())){
			siteLocation=true;
		}
		
		boolean isArrears=true;
		// TODO Auto-generated method stub
		DateUtility.getDateWithTimeZone("IST", applicableFromDate);
		DateUtility.getDateWithTimeZone("IST", applicableToDate);
		DateUtility.getDateWithTimeZone("IST", effectiveFromDate);
		
		if(effectiveToDate!=null){
			DateUtility.getDateWithTimeZone("IST", effectiveToDate);
		}
		
		
		ArrayList<Integer> empIdList=new ArrayList<Integer>();
		
		if(allocationArray!=null){
			for(EmployeeInfo obj:allocationArray){
				empIdList.add(obj.getEmpCount());
			}
		}
		
		if(empIdList.size()!=0){
			List<CTC> ctcList=ofy().load().type(CTC.class).filter("companyId", ctcTemplate.getCompanyId()).filter("empid IN", empIdList).filter("status","Active").list();
			if(ctcList!=null){
				for(CTC obj:ctcList){
					
					if(isSiteWasPayroll){
						if(projectName!=null&&!projectName.equals("")){
							if(obj.getProjectName()!=null&&!obj.getProjectName().equals("")){
								if(obj.getProjectName().equals(projectName)){
									obj.setStatus("Inactive");
									obj.setCTCstatus(false);
								}
							}else{
								obj.setStatus("Inactive");
								obj.setCTCstatus(false);
								
								/**
								 * @author Anil
								 * @since 10-09-2020
								 * if siteLocation process configuration is active and employee is reliever then in that case we will 
								 * not mark default ctc inactive
								 */
								if(isRelieverEmployee(allocationArray,obj.getEmpid())&&siteLocation){
									obj.setStatus("Active");
									obj.setCTCstatus(true);
								}
							}
						}else{
							obj.setStatus("Inactive");
							obj.setCTCstatus(false);
						}
					}else{
						obj.setStatus("Inactive");
						obj.setCTCstatus(false);
					}
				}
				ofy().save().entities(ctcList).now();
			}
		}
		
		/**
		 * Date : 10-09-2018 By ANIL
		 * Save and ctc allocation 
		 */
		if(allocationArray==null&&ctcTemplate!=null){
			allocationArray=new ArrayList<EmployeeInfo>();
			empIdList=new ArrayList<Integer>();
			
			List<CTC> ctcList=ofy().load().type(CTC.class).filter("companyId", ctcTemplate.getCompanyId()).filter("ctcTemplateId", ctcTemplate.getCount()).filter("status","Active").list();
			if(ctcList!=null){
				for(CTC obj:ctcList){
					
					/**
					 * @author Anil , Date : 13-07-2019
					 * If existing ctc is against different project then we will not mark that project as Inactive
					 */
					if(isSiteWasPayroll){
						if(projectName!=null&&!projectName.equals("")){
							if(obj.getProjectName()!=null&&!obj.getProjectName().equals("")){
								if(obj.getProjectName().equals(projectName)){
									obj.setStatus("Inactive");
									obj.setCTCstatus(false);
								}
							}else{
								obj.setStatus("Inactive");
								obj.setCTCstatus(false);
							}
						}else{
							obj.setStatus("Inactive");
							obj.setCTCstatus(false);
						}
					}else{
						obj.setStatus("Inactive");
						obj.setCTCstatus(false);
					}
				}
				ofy().save().entities(ctcList).now();
				
				for(CTC ctc:ctcList){
					empIdList.add(ctc.getEmpid());
				}
				
				if(empIdList.size()!=0){
					List<EmployeeInfo> empInfoArray=ofy().load().type(EmployeeInfo.class).filter("companyId", ctcTemplate.getCompanyId()).filter("empCount IN", empIdList).list();
					if(empInfoArray!=null){
						allocationArray.addAll(empInfoArray);
					}
				}
			}else{
				
			}
		}
		/**
		 * End
		 */
		
		NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("processName", "CTC").filter("status", true).first().now();
		long count=ng.getNumber();
		ng.setNumber(ng.getNumber()+allocationArray.size());
		ofy().save().entity(ng).now();
		
		NumberGeneration arrearsNg=ofy().load().type(NumberGeneration.class).filter("processName", "ArrearsDetails").filter("status", true).first().now();
		long arrearsCount=arrearsNg.getNumber();
		
		
		ArrayList<CTC> ctcList=new ArrayList<CTC>();
		ArrayList<ArrearsDetails> arrearsDetailsList=new ArrayList<ArrearsDetails>();
		
		for(EmployeeInfo obj:allocationArray){
			CTC ctc=new CTC();
			ctc.setCompanyId(ctcTemplate.getCompanyId());
			ctc.setCount((int) count);
			ctc.setEmpid(obj.getEmpCount());
			ctc.setEmployeeName(obj.getFullName());
			ctc.setEmpCellNo(obj.getCellNumber());
			ctc.setEmployeeType(obj.getEmployeeType());
			ctc.setEmployeeRole(obj.getEmployeerole());
			ctc.setEmployeedDesignation(obj.getDesignation());
			ctc.setBranch(obj.getBranch());
			ctc.setDepartment(obj.getDepartment());
			ctc.setCalender(obj.getCalendarName());
			ctc.setFromdate(applicableFromDate);
			ctc.setTodate(applicableToDate);
			ctc.setStatus(CTC.ACTIVE);
			ctc.setEarning(ctcTemplate.getEarning());
			ctc.setDeduction(ctcTemplate.getDeduction());
			ctc.setCtcAmount(ctcTemplate.getTotalCtcAmount());
			ctc.setGrossEarning(ctcTemplate.getTotalCtcAmount());
			count++;
			
			ctc.setFromdate(applicableFromDate);
			ctc.setTodate(applicableToDate);
			ctc.setEffectiveFromDate(effectiveFromDate);
			
			ctc.setDeduction(ctcTemplate.getDeduction());
			ctc.setEmployeesTotalDeduction(ctcTemplate.getTotalDeductionAmount());
			
			ctc.setCompaniesContribution(ctcTemplate.getCompanyContribution());
			ctc.setCompaniesTotalDeduction(ctcTemplate.getCompanyTotalDeduction());
			ctc.setArrears(isArrears);
			obj.setCtcAmount(ctc.getCtcAmount());
			/**Date 11-4-2019 added by Amol for set the ctctemplate name**/
			obj.setCtcTemplateName(ctcTemplate.getCtcTemplateName());
			ctcList.add(ctc);
			obj.setCtcAllocated(true);
			
			/**
			 * Date : 21-08-2018 By ANIL
			 * Setting ctc template information in CTC master
			 */
			ctc.setCtcTemplateId(ctcTemplate.getCount());
			ctc.setCtcTemplateName(ctcTemplate.getCtcTemplateName());
			ctc.setCtcTempAllocationDate(new Date());
			ctc.setCtcTempAllocatedBy(createdBy);
			/**
			 * End
			 */
			
			/**
			 * Date : 27-08-2018 By ANIL
			 * setting paid leave and bonus 
			 */
			ctc.setPaidLeaveAmt(ctcTemplate.getPaidLeaveAmt());
			ctc.setBonus(ctcTemplate.isBonus());
			ctc.setBonusAmt(ctcTemplate.getBonusAmt());
			ctc.setMonthly(ctcTemplate.isMonthly());
			ctc.setAnnually(ctcTemplate.isAnnually());
			ctc.setMonth(ctcTemplate.getMonth());
			/**
			 * End
			 */
			
			
			/**
			 * Date : 31-12-2018 By ANIL
			 * added code for revising bonus and paid leave
			 */
			ctc.setPaidLeaveAndBonusRevised(ctcTemplate.isPaidLeaveAndBonusRevised());
			ctc.setPreviousPaidLeaveAmt(ctcTemplate.getPreviousPaidLeaveAmt());
			ctc.setPreviousBonusAmt(ctcTemplate.getPreviousBonusAmt());
			
			/**
			 * End
			 */
			
			/**
			 * @author Anil , Date : 19-06-2019
			 * Storing paid leave and bonus additional details
			 */
			
			ctc.setPlName(ctcTemplate.getPlName());
			ctc.setPlCorresponadenceName(ctcTemplate.getPlCorresponadenceName());
			ctc.setPlIncludedInEarning(ctcTemplate.isPlIncludedInEarning());
			ctc.setBonusName(ctcTemplate.getBonusName());
			ctc.setBonusCorresponadenceName(ctcTemplate.getBonusCorresponadenceName());
			ctc.setBonusIncludedInEarning(ctcTemplate.isBonusIncludedInEarning());
			
			/**
			 * @author Anil , Date : 13-07-2019
			 * Storing project name for site wise payroll generatiom
			 * Storing effective to date for arrears calculation for given duration 
			 */
			if(effectiveToDate!=null){
				ctc.setEffectiveToDate(effectiveToDate);
			}
			if(projectName!=null){
				ctc.setProjectName(projectName);
			}
			
			
			
			ArrearsDetails arrearsDetails=getRevisionInSalary(ctc,projectName,tempWiseRevisionFlag);
			if(arrearsDetails!=null){
				if(arrearsDetails.getCount()==0){
					arrearsCount++;
					arrearsDetails.setCount((int) arrearsCount);
				}
				
				arrearsDetailsList.add(arrearsDetails);
			}
			/**
			 * @author Anil ,Date : 13-04-2019
			 * If process configuration (LwfAsPerEmployeeGroup) is active then Lwf should be auto fetched and applicable as per the employee group
			 * Process Type : LwfAsPerEmployeeGroup Process Name : CTCAllocation 
			 */
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "LwfAsPerEmployeeGroup", ctcTemplate.getCompanyId())
//					&&obj.getEmpGrp()!=null&&obj.getEmpGrp().equalsIgnoreCase("Direct")){
				updatDeductions(ctc,obj);
//			}
			
		}
		
		ofy().save().entities(ctcList).now();
		
		if(arrearsDetailsList.size()!=0){
			arrearsNg.setNumber(arrearsCount);
			ofy().save().entity(arrearsNg).now();
			ofy().save().entities(arrearsDetailsList).now();
		}
		
		return allocationArray;
	}

	private boolean isRelieverEmployee(ArrayList<EmployeeInfo> allocationArray,int empid) {
		// TODO Auto-generated method stub
		for(EmployeeInfo info:allocationArray){
			if(info.getEmpCount()==empid){
				return info.isReliever();
			}
		}
		return false;
	}

	private ArrearsDetails getRevisionInSalary(CTC ctc, String projectName, boolean tempWiseRevisionFlag) {
		DecimalFormat df = new DecimalFormat("0.00");
//		ArrayList<CtcComponent> earning, Date applicableFromDate, Date effectiveFromDate
		ArrayList<CtcComponent> earning=ctc.getEarning();
		Date applicableFromDate=ctc.getFromdate();
		Date effectiveFromDate=ctc.getEffectiveFromDate();
		
		Date effectiveToDate=ctc.getEffectiveToDate();
		
		
		Date startDate=null;
		Date endDate=null;
		
//		if(effectiveFromDate.before(applicableFromDate)){
//			startDate=DateUtility.getDateWithTimeZone("IST", effectiveFromDate);
//			endDate=DateUtility.getDateWithTimeZone("IST", applicableFromDate);
//		}else{
//			endDate=DateUtility.getDateWithTimeZone("IST", effectiveFromDate);
//			startDate=DateUtility.getDateWithTimeZone("IST", applicableFromDate);
//		}
		
			startDate=DateUtility.getDateWithTimeZone("IST", effectiveFromDate);
			endDate=DateUtility.getDateWithTimeZone("IST", effectiveToDate);
		
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);

		int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		
		final SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
		isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
//		for(){
//			
//		}
		HashSet<String> monthHs=new HashSet<String>();
		while(startDate.before(endDate)|| startDate.equals(endDate)){
			String month=isoFormat.format(startDate);
			monthHs.add(month);
			startCalendar.add(Calendar.MONTH, 1);
			startDate=startCalendar.getTime();
		}
		ArrayList<String> monthList=new ArrayList<String>(monthHs);
		logger.log(Level.SEVERE, "MONTH LIST : "+monthList);
		logger.log(Level.SEVERE,"Total No. of month between dates : "+diffMonth);
		
		List<PaySlip> paySlipList=new ArrayList<PaySlip>();
		if(monthList.size()!=0){
			if(projectName!=null&&!projectName.equals("")){
				paySlipList=ofy().load().type(PaySlip.class).filter("companyId", ctc.getCompanyId()).filter("empid", ctc.getEmpid()).filter("salaryPeriod IN", monthList).filter("projectName", projectName).list();
			}else{
				paySlipList=ofy().load().type(PaySlip.class).filter("companyId", ctc.getCompanyId()).filter("empid", ctc.getEmpid()).filter("salaryPeriod IN", monthList).list();
			}
			
		
		}
		
		
		/**
		 * @author Anil , Date : 04-10-2018
		 */
		Comparator<PaySlip> monthComp=new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip arg0, PaySlip arg1) {
				// TODO Auto-generated method stub
				Date date1=null;
				try {
					date1=isoFormat.parse(arg0.getSalaryPeriod());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Date date2=null;
				try {
					date2=isoFormat.parse(arg1.getSalaryPeriod());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return date1.compareTo(date2);
			}
		};
		if(paySlipList.size()!=0){
			Collections.sort(paySlipList, monthComp);
		}
		
		/**
		 * Date : 27-08-2018 BY ANIL
		 */
		List<ArrearsInfoBean> arrearBinList=new ArrayList<ArrearsInfoBean>();
		boolean arrearFlag=false;
		double arrearAmount=0;
		
		double bonusArrears=0;
		double paidLeaveArrears=0;

		for(CtcComponent ctcTemp:earning){
			if(ctcTemp.isRevised()||tempWiseRevisionFlag){
				ArrearsInfoBean infoBean=new ArrearsInfoBean();
				infoBean.setHead("Arrears");
				infoBean.setEarningCompName(ctcTemp.getName());
				infoBean.setFromDate(isoFormat.format(applicableFromDate));
				infoBean.setToDate(isoFormat.format(applicableFromDate));
				infoBean.setOperation("ADD");
				infoBean.setProjectName(projectName);
				arrearAmount=0;
				for(PaySlip pay:paySlipList){
					boolean arrFlag=checkArrear(pay.getEarningList());
					for(CtcComponent payComp:pay.getEarningList()){
						if(ctcTemp.getName().equals(payComp.getName())){
							arrearFlag=true;
							/**
							 * Old code for revision
							 */
//							double monthlyAmount=ctcTemp.getAmount()/12;
//							double difference=monthlyAmount-ctcTemp.getOldAmountMonthly();
//							double perDayAmt=difference/pay.getMonthlyDays();
//							arrearAmount+=perDayAmt*pay.getPaidDays();
//							infoBean.setAmount(arrearAmount);
							
							double difference=(ctcTemp.getAmount()/12)-(payComp.getAmount()/12);
							if(difference>0){
								double perDayAmt=difference/pay.getMonthlyDays();
								arrearAmount+=perDayAmt*pay.getPaidDays();
								if(arrFlag==true){
									arrearAmount-=getArrearAmount(pay.getEarningList(), payComp.getName());
								}
								infoBean.setAmount(arrearAmount);
							}
						}
					}
				}
				arrearBinList.add(infoBean);
			}
		}
		
		/**
		 * Date : 31-12-2018 BY ANIL
		 * Revising bonus and paid leave 
		 */
		bonusArrears=0;
		paidLeaveArrears=0;
		ArrearsInfoBean infoBean=null;
		ArrearsInfoBean infoBean1=null;
		for(PaySlip pay:paySlipList){
			if(ctc.isPaidLeaveAndBonusRevised()||tempWiseRevisionFlag){
				arrearFlag=true;
				
				double monthlyBonusAmount=ctc.getBonusAmt();
				/**
				 * old code commented
				 */
//				double bonusDifference=monthlyBonusAmount-ctc.getPreviousBonusAmt();
				double payBonusRate=0;
				if(pay.getBonus()!=0){
					payBonusRate=(pay.getBonus()/pay.getPaidDays())*pay.getMonthlyDays();
				}
				double bonusDifference=monthlyBonusAmount-payBonusRate;
				double perDayBonusAmt=bonusDifference/pay.getMonthlyDays();
				bonusArrears+=perDayBonusAmt*pay.getPaidDays();
				
				infoBean=new ArrearsInfoBean();
				infoBean.setAmount(bonusArrears);
				infoBean.setHead("Bonus");
				infoBean.setEarningCompName("Bonus");
				infoBean.setFromDate(isoFormat.format(applicableFromDate));
				infoBean.setToDate(isoFormat.format(applicableFromDate));
				infoBean.setOperation("ADD");
				infoBean.setBonusAndPaidLeaveRevised(true);
				
				
				double monthlyPaidLeaveAmount=ctc.getPaidLeaveAmt();
				/**
				 * old code commented
				 */
//				double paidLeaveDifference=monthlyPaidLeaveAmount-ctc.getPreviousPaidLeaveAmt();
				double payPlRate=0;
				if(pay.getPaidLeaves()!=0){
					payPlRate=(pay.getPaidLeaves()/pay.getPaidDays())*pay.getMonthlyDays();
				}
				
				double paidLeaveDifference=monthlyPaidLeaveAmount-payPlRate;
				double perDayPaidLeaveAmt=paidLeaveDifference/pay.getMonthlyDays();
				paidLeaveArrears+=perDayPaidLeaveAmt*pay.getPaidDays();
				
				infoBean1=new ArrearsInfoBean();
				infoBean1.setAmount(paidLeaveArrears);
				infoBean1.setHead("Paid Leave");
				infoBean1.setEarningCompName("Paid Leave");
				infoBean1.setFromDate(isoFormat.format(applicableFromDate));
				infoBean1.setToDate(isoFormat.format(applicableFromDate));
				infoBean1.setOperation("ADD");
				infoBean1.setBonusAndPaidLeaveRevised(true);
				
			}
		}
		if(infoBean!=null){
			arrearBinList.add(infoBean);
		}
		if(infoBean1!=null){
			arrearBinList.add(infoBean1);
		}

		ArrearsDetails adhocPayment=null;
		if(arrearFlag){
//			adhocPayment=createAdhocPayment(ctc,Double.parseDouble(df.format(arrearAmount)));
			adhocPayment=createAdhocPayment(ctc,arrearBinList);
		}
		
		return adhocPayment;
	}

	private boolean checkArrear(ArrayList<CtcComponent> earningList) {
		// TODO Auto-generated method stub
		if(earningList!=null&&earningList.size()!=0){
			for(CtcComponent comp:earningList){
				if(comp.isArrears()==true){
					return true;
				}
			}
		}
		return false;
	}
	
	private double getArrearAmount(ArrayList<CtcComponent> earningList,String compName) {
		// TODO Auto-generated method stub
		if(earningList!=null&&earningList.size()!=0){
			for(CtcComponent comp:earningList){
				if(comp.isArrears()==true&&comp.getArrearOf().equals(compName)){
					return comp.getActualAmount();
				}
			}
		}
		return 0;
	}
	

	private ArrearsDetails createAdhocPayment(CTC ctc, List<ArrearsInfoBean> arrearBinList) {
		
		ArrearsDetails arrearsDetails=ofy().load().type(ArrearsDetails.class).filter("companyId", ctc.getCompanyId()).filter("empId", ctc.getEmpid()).first().now();
		ArrearsDetails obj=new ArrearsDetails();
		if(arrearsDetails!=null){
			logger.log(Level.SEVERE,"Existing ARREARS: ");
			obj=arrearsDetails;
			for(ArrearsInfoBean db:obj.getArrearsList()){
				for(ArrearsInfoBean newObj:arrearBinList){
					if(db.getEarningCompName().equals(newObj.getEarningCompName())
							&&db.getFromDate().equals(newObj.getFromDate())
							&&db.getToDate().equals(newObj.getToDate())){
						logger.log(Level.SEVERE,"UPDATING ARREARS: ");
						db.setAmount(newObj.getAmount());
						newObj.setOperation("Delete");
					}
				}
			}
			for(int i=0;i<arrearBinList.size();i++){
				ArrearsInfoBean newObj=arrearBinList.get(i);
				if(newObj.getOperation().equals("Delete")){
					logger.log(Level.SEVERE,"DELETING ARREARS: ");
					arrearBinList.remove(newObj);
					i--;
				}
			}
			
			if(arrearBinList.size()!=0){
				obj.getArrearsList().addAll(arrearBinList);
			}
		}else{
			logger.log(Level.SEVERE,"NEW ARREARS: ");
			obj.setCompanyId(ctc.getCompanyId());
			obj.setEmpId(ctc.getEmpid());
			obj.setEmpName(ctc.getEmployeeName());
			obj.setEmpCellNo(ctc.getEmpCellNo());
			obj.setArrearsList(arrearBinList);
			
		}

	
		return obj;
		
	}

	private void updatDeductions(CTC obj, EmployeeInfo employeeInfo) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"BEFORE EMP CONT : "+obj.getEmployeesTotalDeduction());
	    logger.log(Level.SEVERE,"BEFORE COM CONT : "+obj.getCompaniesTotalDeduction());
		double monthlyGrossEarning=obj.getGrossEarning()/12;
		PaySlipServiceImpl impl=new PaySlipServiceImpl();
		
		List<LWF> lwfList=new ArrayList<LWF>();
		List<ProfessionalTax> ptaxDeduction=new ArrayList<ProfessionalTax>();
		Branch branch=ofy().load().type(Branch.class).filter("companyId", employeeInfo.getCompanyId()).filter("buisnessUnitName", employeeInfo.getBranch()).first().now();
	    if(branch!=null){
	    	logger.log(Level.SEVERE,"BRANCH STATE : "+branch.getAddress().getState());
	    	ptaxDeduction=ofy().load().type(ProfessionalTax.class).filter("companyId", employeeInfo.getCompanyId()).filter("state", branch.getAddress().getState()).filter("status", true).list();
	    	lwfList=ofy().load().type(LWF.class).filter("companyId", employeeInfo.getCompanyId()).filter("state", branch.getAddress().getState()).filter("status", true).list();
	    }
	    logger.log(Level.SEVERE,"PT LIST SIZE  : "+ptaxDeduction.size());
	    logger.log(Level.SEVERE,"LWF LIST SIZE  : "+lwfList.size());
		
	    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CtcAllocation", "LwfAsPerEmployeeGroup", employeeInfo.getCompanyId())
				&&employeeInfo.getEmpGrp()!=null&&employeeInfo.getEmpGrp().equalsIgnoreCase("Direct")){  
	    
	    if(lwfList!=null&&lwfList.size()!=0){
			logger.log(Level.SEVERE,"DEDUCTION SIZE BEFOR LWF "+obj.getDeduction().size());
//			int i=0;
//			for(CtcComponent comp:obj.getDeduction()){
			for(int i=0;i<obj.getDeduction().size();i++){
				CtcComponent comp=obj.getDeduction().get(i);
				if(comp.getShortName().equalsIgnoreCase("LWF")
						||comp.getShortName().trim().equalsIgnoreCase("MLWF")
						||comp.getShortName().trim().equalsIgnoreCase("MWF")){
					obj.getDeduction().remove(i);
				}
			}
			logger.log(Level.SEVERE,"DEDUCTION SIZE After removing LWF "+obj.getDeduction().size());
			ArrayList<CtcComponent> list=impl.getEmployeeContributionLwfList(lwfList);
			ArrayList<CtcComponent> ded1=new ArrayList<CtcComponent>();
			ded1.addAll(list);
			for(CtcComponent deductions:ded1){
				if(monthlyGrossEarning>=deductions.getConditionValue()){
					double amtRange=0;
					for(int k=0;k<list.size();k++){
						CtcComponent c=list.get(k);
						amtRange=c.getConditionValue();
						if(deductions.getConditionValue()>amtRange){
							list.remove(k);
						}
					}
				}
			}
			obj.getDeduction().addAll(list);
			logger.log(Level.SEVERE,"DEDUCTION SIZE After adding LWF "+obj.getDeduction().size());
			
			
			logger.log(Level.SEVERE,"EMPLOYER SIZE BEFOR LWF "+obj.getCompaniesContribution().size());
//			int j=0;
			for(int j=0;j<obj.getCompaniesContribution().size();j++){
//			for(CtcComponent comp:obj.getCompaniesContribution()){
				CtcComponent comp=obj.getCompaniesContribution().get(j);
				if(comp.getShortName().equalsIgnoreCase("LWF")
						||comp.getShortName().trim().equalsIgnoreCase("MLWF")
						||comp.getShortName().trim().equalsIgnoreCase("MWF")){
					obj.getCompaniesContribution().remove(j);
				}
			}
			logger.log(Level.SEVERE,"EMPLOYER SIZE After removing LWF "+obj.getCompaniesContribution().size());
			ArrayList<CtcComponent> list1=impl.getEmployerContributionLwfList(lwfList);
			
			ArrayList<CtcComponent> ded2=new ArrayList<CtcComponent>();
			ded2.addAll(list1);
			for(CtcComponent deductions:ded2){
				if(monthlyGrossEarning>=deductions.getConditionValue()){
					double amtRange=0;
					for(int k=0;k<list1.size();k++){
//					for(CtcComponent c:list1){
						CtcComponent c=list1.get(k);
						amtRange=c.getConditionValue();
						if(deductions.getConditionValue()>amtRange){
							list1.remove(k);
						}
					}
				}
			}
			
			obj.getCompaniesContribution().addAll(list1);
			logger.log(Level.SEVERE,"EMPLOYER SIZE After adding LWF "+obj.getCompaniesContribution().size());
		}
	    
	    }
	    
	    if(ptaxDeduction.size()!=0){
	    	PaySlip paySlip=new PaySlip();
	    	
	    	logger.log(Level.SEVERE,"DEDUCTION SIZE BEFOR PT "+obj.getDeduction().size());
//			int i=0;
			for(int i=0;i<obj.getDeduction().size();i++){
//			for(CtcComponent comp:obj.getDeduction()){
				CtcComponent comp=obj.getDeduction().get(i);
				if(comp.getShortName().equalsIgnoreCase("PT")){
					obj.getDeduction().remove(i);
				}
			}
			logger.log(Level.SEVERE,"DEDUCTION SIZE After removing PT "+obj.getDeduction().size());
			for(ProfessionalTax pt:ptaxDeduction){
				pt.setExceptionForSpecificMonth(false);
			}
			
			CtcComponent ptComponent=paySlip.getPtDetailsForEmployee(ptaxDeduction,monthlyGrossEarning,null,employeeInfo.getGender());
			if(ptComponent!=null){
				ptComponent.setAmount(ptComponent.getAmount()*12);
				obj.getDeduction().add(ptComponent);
			}
			logger.log(Level.SEVERE,"DEDUCTION SIZE After Adding PT "+obj.getDeduction().size());
	    }
	    
	    double empContribution=0;
	    double employerContribution=0;
	    for(CtcComponent com:obj.getDeduction()){
	    	empContribution=empContribution+com.getAmount();
	    	logger.log(Level.SEVERE,"EMP SUM AMOUNT "+empContribution+" Amount "+com.getAmount());
	    }
	    obj.setEmployeesTotalDeduction(empContribution);
	    logger.log(Level.SEVERE,"AFTER EMP CONT : "+obj.getEmployeesTotalDeduction());
	    for(CtcComponent com:obj.getCompaniesContribution()){
	    	employerContribution=employerContribution+com.getAmount();
	    	logger.log(Level.SEVERE,"COM SUM AMOUNT "+employerContribution+" Amount "+com.getAmount());
	    }
	    obj.setCompaniesTotalDeduction(employerContribution);
	    logger.log(Level.SEVERE,"AFTER COM CONT : "+obj.getCompaniesTotalDeduction());
		
		
	}

}
