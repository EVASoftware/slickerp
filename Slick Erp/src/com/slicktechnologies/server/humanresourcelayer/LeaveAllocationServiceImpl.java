
package com.slicktechnologies.server.humanresourcelayer;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;










import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.LeaveAllocationService;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Actual allocation of Leave and Calendar Happens in this Servlet.
 */
public class LeaveAllocationServiceImpl extends RemoteServiceServlet implements
LeaveAllocationService
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8054740195179342903L;
	

	@Override
	public ArrayList<AllocationResult> allocateLeave(ArrayList<LeaveBalance> leavebal) {
		System.out.println("Inside Allocate Leave......");
		
		// Holds the valid Leave Balances which are valid and can be allocated.
		ArrayList<SuperModel> leaveBalance = new ArrayList<SuperModel>();
		// Holds the valid Employees for Calendar Allocation Calendar Allocation
		ArrayList<SuperModel> employeeInfo = new ArrayList<SuperModel>();
		// Holds the Result of Leave Allocation
		ArrayList<AllocationResult> leaveAllocationResult = new ArrayList<AllocationResult>();

		GenricServiceImpl genimpl = new GenricServiceImpl();

		for (LeaveBalance bal : leavebal) {

			EmployeeInfo info = bal.getEmpInfo();
			Calendar cal = info.getLeaveCalendar();
//			Overtime ovr=info.getOvertime();
//			System.out.println("oVeRtImE  "+ovr.getShortName());
		
			
			
			Date startDate = cal.getCalStartDate();
			// Check weather Calendar is Allocatable or not
			boolean isCalendarAllocatable = info.isAllocatable(startDate);
			// If We can Allocate both leave and Calendar then Allocate
			
			/**
			 * Date : 05-06-2018 BY ANIL
			 * Patch for reallocating calendar
			 */
//			bal.getEmpInfo().setLeaveAllocated(false);
//			isCalendarAllocatable=true;
			
			/**
			 * End
			 */
			
			
			if (bal.getEmpInfo().isLeaveAllocated() == false && isCalendarAllocatable == true) {
				
				
				getValidAllocatedLeaves(bal);
				addPreviousYearBalance(bal);
				
				bal.getEmpInfo().setLeaveAllocated(true);
				employeeInfo.add(bal.getEmpInfo());
				
				bal.getEmpInfo().setLeaveAllocated(true);
				leaveBalance.add(bal);
				
				AllocationResult result = new AllocationResult();
				result.setInfo(bal.getEmpInfo());
				result.setStatus(true);
				result.setReason("N.A");
				
				leaveAllocationResult.add(result);
			}
			// It means that Calendar and Leaves are Allocated
			else {
				AllocationResult result = new AllocationResult();
				result.setInfo(bal.getEmpInfo());
				result.setStatus(false);
				bal.getEmpInfo().setLeaveAllocated(false);
				result.setReason("Already Allocated");
				leaveAllocationResult.add(result);
			}
		}

		if (leavebal.size() != 0 && employeeInfo.size() != 0) {
			genimpl.save(leaveBalance);
			// Printing Allocated Leave Balance
			genimpl.save(employeeInfo);
		}
		return leaveAllocationResult;

	}

	/**
	 *  
	 * @param balance
	 * This method returns valid leaves depends on monthly allocation.
	 */
	
	private void getValidAllocatedLeaves(LeaveBalance balance){
		System.out.println("INSIDE MONTHLY ALLOCATION METHOD");
		System.out.println("BEFORE");
		for(int i=0;i<balance.getAllocatedLeaves().size();i++){
			System.out.println("LEV NM :: "+balance.getAllocatedLeaves().get(i).getName()+" MONTHLY Allocate :: "+balance.getAllocatedLeaves().get(i).getMonthlyAllocation()+"  LEV BAL :: "+balance.getAllocatedLeaves().get(i).getBalance()  );
		}
		for (AllocatedLeaves alLeaves : balance.getAllocatedLeaves()) {
			if(alLeaves.getMonthlyAllocation()){
				System.out.println("INSIDE MONTHLY ALLOCATION TRUE : "+alLeaves.getName());
				alLeaves.setBalance(0d);
			}
		}
		System.out.println("AFTER");
		for(int i=0;i<balance.getAllocatedLeaves().size();i++){
			System.out.println("LEV NM :: "+balance.getAllocatedLeaves().get(i).getName()+" MONTHLY Allocate :: "+balance.getAllocatedLeaves().get(i).getMonthlyAllocation()+"  LEV BAL :: "+balance.getAllocatedLeaves().get(i).getBalance()  );
		}
	}
	
	
	/**
	 * The method adds any Previous period carry Forward Leaves which will be
	 * added in Current Leave.
	 *
	 * @param balance
	 * {@link LeaveBalance} object on which Previous year Leaves will be added.
	 * 
	 */
	
	private void addPreviousYearBalance(LeaveBalance balance) {
		List<LeaveBalance> list;
		// Search Leave Balance for Employee

		if (balance.getCompanyId() != null) {
			int count = balance.getEmpCount();
			list = ofy().load().type(LeaveBalance.class).filter("companyId", balance.getCompanyId()).filter("empInfo.empCount", count).list();
		} else {
			int count = balance.getEmpCount();
			list = ofy().load().type(LeaveBalance.class).filter("empInfo.empCount", count).list();
		}

		if (list == null || list.size() == 0) {
			return;
		}
		else {
			Collections.sort(list, new Comparator<LeaveBalance>() {
				@Override
				public int compare(LeaveBalance o1, LeaveBalance o2) {
					return o2.getValidtill().compareTo(o1.getValidtill());
				}
			});
		}

		if (list.size() == 0) {
			return;
		}

		LeaveBalance prevPeriodLeavebalance = list.get(0);

		if (prevPeriodLeavebalance != null) {
			// Get those Leaves which has carry forward properties allowable
			ArrayList<AllocatedLeaves> prevPeriodAllocatedLeaves = prevPeriodLeavebalance.getAllocatedLeaves();
			if (prevPeriodAllocatedLeaves != null) {
				for (AllocatedLeaves alLeaves : prevPeriodAllocatedLeaves) {
					// Check if Leave Type allows Carry Forward
					if (alLeaves.getRelapseAllowCarryOver() == true) {
						System.out.println("Previous Year Relapsed Leave Balance "+ alLeaves.getName());
						try {
							// Create Copy of Object so the older Leave Balance
							// will not
							// get Copy.
							AllocatedLeaves temp = (AllocatedLeaves) alLeaves.clone();
							if (temp.getMaxCarryForward() < temp.getBalance()) {
								System.out.println("Balance"+ temp.getBalance());
								double maxCarryForward = temp.getMaxCarryForward();
								temp.setBalance(maxCarryForward);
								System.out.println("Increased Balance"+ temp.getBalance());
							}
							// Serach weather This Leave Type exist in New Leave
							// Balance if yes add the balnce
							// other wise insert it
							boolean found = false;

							for (AllocatedLeaves temp1 : balance.getAllocatedLeaves()) {
								if (temp.getName().trim().equals(temp1.getName().trim())) {
									double newBalance = temp.getBalance()+ temp1.getBalance();
									temp1.setBalance(newBalance);
									found = true;
								}
							}
							if (found == false) {
								balance.getAllocatedLeaves().add(temp);
							}

						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.slicktechnologies.client.views.humanresource.allocation.Allocation
	 * #getAllocationStatus(java.util.ArrayList)
	 */
	@Override
	public ArrayList<AllocationResult> getAllocationStatus(ArrayList<Filter> filter) {
		System.out.println("inside SER IMP ..... ");

		ArrayList<AllocationResult> allocationResult = new ArrayList<AllocationResult>();
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new EmployeeInfo());
		if (filter.size() != 0) {
			querry.getFilters().addAll(filter);
		}

		GenricServiceImpl impl = new GenricServiceImpl();
		List<SuperModel> modelLis = impl.getSearchResult(querry);
		System.out.println("Result SIZE.. "+modelLis.size());
		
		for (SuperModel temp : modelLis) {
			AllocationResult result = new AllocationResult();
			EmployeeInfo employee = (EmployeeInfo) temp;
			result.setInfo(employee);
			result.setStatus(employee.isLeaveAllocated());
			/**
			 * Date : 05-06-2018 By  ANIL
			 * Patch for reallocating Calendar
			 */
//			result.setStatus(false);
			/**
			 * End
			 */
			allocationResult.add(result);
		}
		return allocationResult;
	}

	@Override
	public ArrayList<AllocationResult> deallocateCalendar(ArrayList<EmployeeInfo> allocatedEmp) {
		HashSet<Integer> hsEmpId=new HashSet<Integer>();
		for(EmployeeInfo info:allocatedEmp){
			hsEmpId.add(info.getEmpCount());
		}
		ArrayList<Integer> empIdList=new ArrayList<Integer>(hsEmpId);
		
		System.out.println("EMP COUNT "+empIdList.size()+" "+allocatedEmp.get(0).getCompanyId());
		List<EmployeeInfo> employeeInfoList=new ArrayList<EmployeeInfo>();
		List<LeaveBalance> empLeaveBalanceList=new ArrayList<LeaveBalance>();
		if(empIdList.size()!=0){
			employeeInfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", allocatedEmp.get(0).getCompanyId()).filter("empCount IN", empIdList).list();
			empLeaveBalanceList=ofy().load().type(LeaveBalance.class).filter("companyId", allocatedEmp.get(0).getCompanyId()).filter("empInfo.empCount IN", empIdList).list();
		}
		System.out.println("EMP LIST COUNT  "+employeeInfoList.size());
		System.out.println("EMP BAL COUNT "+empLeaveBalanceList.size());
		
		for(EmployeeInfo info:employeeInfoList){
			info.setCalenderKey(null);
			info.setLeaveCalendar(null);
			info.setLeaveAllocated(false);
		}
		if(employeeInfoList!=null&&employeeInfoList.size()!=0)
			ofy().save().entities(employeeInfoList).now();
		
		if(empLeaveBalanceList!=null&&empLeaveBalanceList.size()!=0)
			ofy().delete().entities(empLeaveBalanceList).now();
		
		return null;
	}
	
	


	
}
