package com.slicktechnologies.server.humanresourcelayer;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.ibm.icu.impl.duration.impl.DataRecord.EPluralization;
import com.itextpdf.text.pdf.Pfm2afm;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.client.views.humanresource.allocation.payslipallocation.PaySlipAllocationService;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.CreatePayRollPDFServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.arrears.ArrearsInfoBean;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestmentBean;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.EsicContributionPeriod;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.AvailedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.EmployeeOvertimeHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWFBean;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipPrintHelper;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;



/**
 * Servelet for Pay Roll Processing.
 * To Do : Rename , Make method Pay Slip Service Implementor small.
 * @author AJAY
 *
 */

public class PaySlipServiceImpl extends RemoteServiceServlet implements PaySlipAllocationService
{
	private static final long serialVersionUID = 280522638068088468L;
	
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	 /**
		 * Creates the {@link PaySlip} list corresponding to List of Passed {@link EmployeeInfo}
		 * To Do : re write using oop way.
		 * 
		 * @param empinfoarray list of Employees Corresponding to whom {@link PaySlip} will be created.
		 * @param payRollPeriod String representation of whole PayRoll Period.
		 * @return List of {@link AllocationResult}
		 */
		ArrayList<AvailedLeaves> availedLeaves;
//		CompanyPayrollRecord compPayRec=null;
		
		/**
		 * Date : 05-06-2018 By ANIL
		 */
		List<EmployeeOvertime> otList=new ArrayList<EmployeeOvertime>();
		
		/**
		 *Date : 30-08-2018 BY ANIL
		 * 
		 *
		 **/
		boolean nhOtFlag=false;
		
		/**
		 * Date : 14-12-2018 BY ANIL
		 * variable stores the no of leave day from monthly attendance
		 * variable stores the no of ot day from monthly attendance
		 */
		int noOfLeaveDays;
		int noOfOtDays;
		int noOfPresentDays;
		
		/**
		 * @author Anil,Date : 22-02-2019
		 * paid leave and bonus as additional allowance
		 */
		boolean paidLeaveAndBonusAsAdditionalAllowance=false;
		
		/**
		 * @author Anil, Date : 12-06-2019
		 */
		ArrayList<EmployeeOvertimeHistory> empOtHisList=new ArrayList<EmployeeOvertimeHistory>();
		
		/**
		 * @author Anil , Date : 27-06-2019
		 * Declaring Pf 
		 */
		ProvidentFund pf;
		/**
		 * @author Anil , Date : 24-07-2019
		 * if we get pf as deduction on deduction list then only the pf is applicable
		 */
		boolean isPfApplicable=false;
		/**
		 * @author Anil , Date : 24-07-2019
		 * Declaring ESIC
		 */
		Esic esic;
		boolean isEsicApplicable=false;
		
		ProvidentFund masterPf=null;
		Esic masterEsic=null;
		
		private int calCulateHolidayFromAttendance(List<Attendance> attendanceList) {
			int hCounter=0;
			for(Attendance attendance:attendanceList){
				if(attendance.isHoliday()==true||attendance.getActionLabel().equalsIgnoreCase("H")){
					hCounter++;
				}
			}
			return hCounter;
		}
		
		private int calculateWeekDayFromAttendance(List<Attendance> attendanceList) {
			int woCounter=0;
			for(Attendance attendance:attendanceList){
				if(attendance.isWeeklyOff()==true||attendance.getActionLabel().equalsIgnoreCase("WO")){
					woCounter++;
				}
			}
			return woCounter;
		}
		
		public ArrayList<AllocationResult> allocatePaySlip(ArrayList<EmployeeInfo> empinfoarray,String payRollPeriod,Date fromDate,Date toDate,boolean isProvisional) 
		{
			logger.log(Level.SEVERE,"---------- Pay Roll Period :: "+payRollPeriod);
			isPfApplicable=false;
			isEsicApplicable=false;
			/** @author Abhinav Bihade @since 30/12/2019 */
			boolean isDeductionUANAndESIC=false;
			boolean isFixedPayrollDaysDefinedInProject=false;//Ashwini Patil Date:03-11-2023 for spick and span
			/**
			 * @author Anil , Date : 15-07-2019
			 * for capturing site wise attendance
			 */
			HashMap<String,ArrayList<Attendance>> siteWiseAttendanceMap=new HashMap<String,ArrayList<Attendance>>();
			HashSet<String> projectHs=new HashSet<String>();
			HashMap<String, ArrayList<Attendance>> innerSiteAttendanceMap=new HashMap<String, ArrayList<Attendance>>();
			
			/**
			 * @author Anil @since 04-02-2021
			 * Earlier key was the employee id now we are storing payslip record and attendance against the key which is the combination of 
			 * employee id and state
			 */
//			HashMap<Integer,ArrayList<PaySlip>> paySlipMap=new HashMap<Integer, ArrayList<PaySlip>>();
//			HashMap<Integer,ArrayList<CompanyPayrollRecord>> employerContributionMap=new HashMap<Integer, ArrayList<CompanyPayrollRecord>>();
			
			HashMap<String,ArrayList<PaySlip>> paySlipMap=new HashMap<String, ArrayList<PaySlip>>();
			HashMap<String,ArrayList<CompanyPayrollRecord>> employerContributionMap=new HashMap<String, ArrayList<CompanyPayrollRecord>>();
			
			HashMap<String,ArrayList<PaySlip>> processedPaySlipMap=new HashMap<String, ArrayList<PaySlip>>();
			HashMap<String,ArrayList<CompanyPayrollRecord>> processedEmployerContributionMap=new HashMap<String, ArrayList<CompanyPayrollRecord>>();
			
			/**
			 * Date : 30-08-2018 BY ANIL
			 * Added configuration for excluding national holiday from OT and include in salary earning
			 * PROCESS TYPE : NH_OT_Include_In_Earning
			 */
			nhOtFlag=false;
			Long companyId=(Long) empinfoarray.get(0).getCompanyId();
			//List of Allocation Result will be filled by Allocation corresponding to each Employee.
			ArrayList<AllocationResult>allcationResult=new ArrayList<AllocationResult>();
			//List of Pay Slips created for each Successful allocation
			List<PaySlip>paySlips=new ArrayList<PaySlip>();
			//List of Pay Slip Records.
			List<PaySlipRecord>paySlipRecordList=new ArrayList<PaySlipRecord>();
			
			/**
			 * Date : 12-05-2018 BY ANIL
			 * Earlier we were loading all deduction component defined in the system.
			 * right now we have stored the employee deduction and company contribution in ctc 
			 */
//			List<CtcComponent>deduction= ofy().load().type(CtcComponent.class).filter("isDeduction",true).filter("companyId",companyId).list();
//			List<CtcComponent>dedRecord= ofy().load().type(CtcComponent.class).filter("isDeduction",true).filter("isRecord",true).filter("isNonMonthly",true).filter("companyId",companyId).list();
			List<CtcComponent>deduction=new ArrayList<CtcComponent>();
			List<CtcComponent>dedRecord=new ArrayList<CtcComponent>();
			/**
			 * End
			 */
			
			/**
			 * Date : 17-05-2018 By ANIL
			 */
			List<CtcComponent>ptDeduction=new ArrayList<CtcComponent>();
			List<CompanyPayrollRecord>compPayrollRecord=new ArrayList<CompanyPayrollRecord>();	
			List<LeaveBalance>leaveBalance=new ArrayList<LeaveBalance>();
			
			/**
			 * Date : 27-07-2018 BY ANIL
			 * this flag check whether to process payroll on the basis of time report or attendance
			 */
			boolean attendanceFlag=false;
			
			/**
			 * Date : 29-05-2018 By ANIL
			 * Loading process configuration for PF Calculation and ESIC deduction
			 * @author Anil ,Date : 10-04-2019
			 * this flag is used to fetch LWF automatically
			 * @author Anil , Date : 15-04-2019
			 * flag which indicated OT should be included in gross earning and ESIC should be deducted on it
			 * @author Anil , Date : 13-07-2019
			 * Checking multisite flag for processing payroll site wise
			 */
			boolean otIncludedInGrossEarning=false;
			boolean isAutoFetchLwf=false;
			boolean isSiteWasPayroll=false;
			
			double applicableDisabiltyPercentage=0;
			double pfMaxFixedValue=0;
			String deductionTypeName="";
			ArrayList<String> processNameList=new ArrayList<String>();
			processNameList.add("PfMaxFixedValue");
			processNameList.add("EsicForDirect");
			processNameList.add("PaySlip");
			
			/**
			 * @author Anil
			 * @since 21-08-2020
			 * PT and LWF calculation as per Project state
			 */
			boolean ptAndLwfProjectWise=false;
			
			/**
			 * @author Anil
			 * @since 01-09-2020
			 * enable site location for sun facility
			 * raised by rahul tiwari
			 */
			boolean siteLocation=false;
			
			/**
			 * @author Anil @since 04-02-2021
			 * this variable stores pf capping status
			 * if it is true at process configuration level then we will not read and update from
			 * Project Level
			 * For sun rise raised by Rahul (Multi site payroll)
			 */
			boolean pfCappingFlag=false;
			String state="";
			
			/**
			 * @author Anil @since 02-03-2021
			 * Adding number of days validation for sasha
			 * if attendance less than a month days then user will not allow to process payroll of that employee
			 * Raised by Rahul Tiwari for Sasha
			 */
			boolean noOfDaysValidationForPayrollFlag=false;
			
			List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName IN", processNameList).filter("configStatus", true).list();
			if(processConfigList!=null){
				for(ProcessConfiguration processConf:processConfigList){
					for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
						if(processConf.getProcessName().equalsIgnoreCase("PfMaxFixedValue")&&ptDetails.isStatus()==true){
							pfMaxFixedValue=Double.parseDouble(ptDetails.getProcessType().trim());
							pfCappingFlag=true;
						}
						if(processConf.getProcessName().equalsIgnoreCase("EsicForDirect")&&ptDetails.isStatus()==true){
							deductionTypeName=ptDetails.getProcessType().trim();
						}
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("PayrollByAttendance")){
							attendanceFlag=true;
						}
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("NH_OT_Include_In_Earning")){
							nhOtFlag=true;
						}
						if(processConf.getProcessName().equalsIgnoreCase("ApplicableDisabilityPercentage")&&processConf.isConfigStatus()==true){
							applicableDisabiltyPercentage=Double.parseDouble(ptDetails.getProcessType().trim());
						}
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("LwfAsPerEmployeeGroup")){
							isAutoFetchLwf=true;
						}
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("IncludeOtInGrossEarning")){
							otIncludedInGrossEarning=true;
						}
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("SiteWisePayrollForReliever")){
							isSiteWasPayroll=true;
						}
						
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("DeductionAsPerUANNOAndESICNumber")){
							isDeductionUANAndESIC=true;
						}
						
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("CalculatProjectSatewisePtAndLwf")){
							ptAndLwfProjectWise=true;
						}
						
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("EnableSiteLocation")){
							siteLocation=true;
						}
						if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
								&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("NoOfDaysValidationForPayroll")){
							noOfDaysValidationForPayrollFlag=true;
						}
						
						
					}
				}
			}
			logger.log(Level.SEVERE,pfCappingFlag +" PF Max value "+pfMaxFixedValue+" ESIC DED ALLOWANCE NM "+deductionTypeName +"ATTENDANCE FLAG : "+attendanceFlag+" OT included in Gross : "+otIncludedInGrossEarning);
			
			attendanceFlag=true;
			SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat mmm = new SimpleDateFormat("MMM");
			mmm.setTimeZone(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			Calendar calPay = Calendar.getInstance();
			Date payrollPeriodDate=null;
			Date srtDt=null;
			Date endDt=null;
			try{
				payrollPeriodDate = isoFormat.parse(payRollPeriod);
				calPay.setTime(payrollPeriodDate);
				
				srtDt=DateUtility.getDateWithTimeZone("IST", calPay.getTime());
				srtDt=DateUtility.getStartDateofMonth(srtDt);
				endDt=DateUtility.getDateWithTimeZone("IST", calPay.getTime());
				endDt=DateUtility.getEndDateofMonth(endDt);
			}catch (ParseException e) {
				e.printStackTrace();
			}
			
			String month="";
			String monthForSalary=sdf.format(payrollPeriodDate);
			month=mmm.format(payrollPeriodDate);
			
			ServerAppUtility app = new ServerAppUtility();
			String year = "";
			Calendar cal = Calendar.getInstance();
			Date date=null;
			try{
				date = isoFormat.parse(payRollPeriod);
				cal.setTime(date);
				year = app.getFinancialYearOfDate(date);
				logger.log(Level.SEVERE , "Year :" + year + "payroll period :" +payRollPeriod+" DATE :: "+date+" MONTH :: "+monthForSalary+" MMM "+month);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			
			/**
			 * @author Anil , Date : 27-06-2019
			 * updated PF logic for payroll 
			 * raised by Pravin Sasha
			 */
			masterPf=ofy().load().type(ProvidentFund.class).filter("companyId", companyId).filter("status", true).first().now();
			
			/**
			 * @author Anil , Date : 24-07-2019
			 * updated Esic logic for payroll 
			 * raised by Pravin Sasha
			 */
			masterEsic=ofy().load().type(Esic.class).filter("companyId", companyId).filter("status", true).first().now();
			
			
			if(masterPf!=null){
				logger.log(Level.SEVERE,"MASTER PF NOT NULL");
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(masterPf.getEffectiveFromDate());
				
				Date pfEffectiveDate=DateUtility.getDateWithTimeZone("IST", cal1.getTime());
				pfEffectiveDate=DateUtility.getStartDateofMonth(pfEffectiveDate);
				
//				logger.log(Level.SEVERE,"PAYROLL DATE : "+payrollPeriodDate+" MONTH START DT : "+srtDt+" MONTH END DT : "+endDt+" PF EFFECTIVE DT : "+masterPf.getEffectiveFromDate()+" Modified Pf Eff Dt : "+pfEffectiveDate);
				
				if(srtDt.after(pfEffectiveDate)||srtDt.equals(pfEffectiveDate)){	
//					logger.log(Level.SEVERE,"EFFECTIVE DATE IS IN RANGE");
				}else{
					masterPf=null;
//					logger.log(Level.SEVERE,"EFFECTIVE DATE IS NOT IN RANGE");
				}
			}else{
				logger.log(Level.SEVERE,"MASTER PF NULL");
			}
			
			if(masterEsic!=null){
				logger.log(Level.SEVERE,"MASTER ESIC NOT NULL");
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(masterEsic.getEffectiveFromDate());
				
				Date esicEffectiveDate=DateUtility.getDateWithTimeZone("IST", cal1.getTime());
				esicEffectiveDate=DateUtility.getStartDateofMonth(esicEffectiveDate);
				
//				logger.log(Level.SEVERE,"PAYROLL DATE : "+payrollPeriodDate+" MONTH START DT : "+srtDt+" MONTH END DT : "+endDt+" ESIC EFFECTIVE DT : "+masterEsic.getEffectiveFromDate()+" Modified esic Eff Dt : "+esicEffectiveDate);
				
				if(srtDt.after(esicEffectiveDate)||srtDt.equals(esicEffectiveDate)){	
//					logger.log(Level.SEVERE,"esic EFFECTIVE DATE IS IN RANGE");
				}else{
					masterEsic=null;
//					logger.log(Level.SEVERE,"esic EFFECTIVE DATE IS NOT IN RANGE");
				}
			}else{
				logger.log(Level.SEVERE,"MASTER ESIC NULL");
			}
			
			/**
			 * Date : 24-11-2018 By ANIL
			 */
			if(isProvisional){
				attendanceFlag=false;
			}
			
			/**
			 * Date : 15-12-2018 BY ANIL
			 * for clearing all loaded data from appengine cache memory
			 */
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			/**
			 * End
			 */
			
		   List<ProfessionalTax> ptaxDeduction=new ArrayList<ProfessionalTax>();
		   List<LWF> lwfList=new ArrayList<LWF>();
			
		   HashSet<ProfessionalTax> ptHs=new HashSet<ProfessionalTax>();
		   HashSet<LWF> lwfHs=new HashSet<LWF>();
		   
		   for(EmployeeInfo info:empinfoarray){
			   
			   try {
				
			   
			   /**
			    * @author Anil
			    * @since 29-07-2020
			    * Adding payroll cycle start and end date
			    */
			   Date payrollStartDate=null;
			   Date payrollEndDate=null;
			   List<Attendance> attenList=new ArrayList<Attendance>();
				if(attendanceFlag){
					logger.log(Level.SEVERE,"SALARY PERIOD FOR ATTENDANCE : "+monthForSalary);
					attenList=ofy().load().type(Attendance.class).filter("companyId",info.getCompanyId()).filter("empId",info.getEmpCount()).filter("month",monthForSalary.trim()).list();
				}
//				logger.log(Level.SEVERE,"ATTENDANCE LIST SIZE : "+attenList.size());
				
//				if(attenList.size()!=0){
//					if(attenList.get(0).getStartDate()!=null&&attenList.get(0).getEndDate()!=null){
//						payrollStartDate=attenList.get(0).getStartDate();
//						payrollEndDate=attenList.get(0).getEndDate();
//						logger.log(Level.SEVERE,"payrollStartDate "+payrollStartDate+"   payrollEndDate "+payrollEndDate );
//					}
//				}
				
				/**
				 * @author Anil , Date : 15-07-2019
				 * if sitewisepayroll is active then we will check site wise attendance
				 */
				innerSiteAttendanceMap=getSiteWiseAttendance(attenList,projectHs,info.isReliever(),siteLocation);
				if(innerSiteAttendanceMap.size()>1){
					siteWiseAttendanceMap.putAll(innerSiteAttendanceMap);
//					logger.log(Level.SEVERE, "siteWiseAttendanceMap : "+siteWiseAttendanceMap.size() +" Key : "+siteWiseAttendanceMap.keySet());
				}
				
				/**
				 * Date : 24-08-2018 By Anil 
				 * Separate Pt Master for calculating PT 
				 * Pick state name from branch which is assigned to employee
				 * Date : 11-04-2019 BY ANIL
				 * LWF list
				 */
			 	lwfList=new ArrayList<LWF>();
//			   	List<LWF> lwfList=new ArrayList<LWF>();
			   	logger.log(Level.SEVERE,"BRANCH : "+info.getBranch());
			    logger.log(Level.SEVERE,"PROJECT : "+info.getProjectName());
			    Branch branch=ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", info.getBranch()).first().now();
			  
			    /**
			     * @author Anil
			     * @since 21-08-2020
			     * Loading PT and LWF details from project if process configuration is active
			     */
			    if(!ptAndLwfProjectWise){
				    if(branch!=null){
				    	logger.log(Level.SEVERE,"BRANCH STATE : "+branch.getAddress().getState());
				    	ptaxDeduction=ofy().load().type(ProfessionalTax.class).filter("companyId", companyId).filter("state", branch.getAddress().getState()).filter("status", true).list();
				    
				    	if(isAutoFetchLwf&&
				    			((info.getEmpGrp()!=null&&!info.getEmpGrp().equals("")&&info.getEmpGrp().equalsIgnoreCase("Direct"))
				    			||(info.getNumberRangeAsEmpTyp()!=null&&!info.getNumberRangeAsEmpTyp().equals("")&&info.getNumberRangeAsEmpTyp().equalsIgnoreCase("Direct")))){
				    		lwfList=ofy().load().type(LWF.class).filter("companyId", companyId).filter("state", branch.getAddress().getState()).filter("status", true).list();
				    	}
				    }
				    
				    if(ptaxDeduction!=null&&ptaxDeduction.size()>0) {
				    	ptHs.addAll(ptaxDeduction);
				    }
				    if(lwfList!=null&&lwfList.size()>0) {
				    	logger.log(Level.SEVERE,"lwfList size="+lwfList.size()); 
				    	lwfHs.addAll(lwfList);
				    }
			    }
//			    logger.log(Level.SEVERE,"PT LIST SIZE  : "+ptaxDeduction.size());
//			    logger.log(Level.SEVERE,"LWF LIST SIZE  : "+lwfList.size());
				
				
			   	/**
			   	 * Date : 20-08-2018 By ANIL
			   	 */
			   	boolean invalidAttendanceFlag=false;
			   
				//Check weather Pay Slip is already Created or Not.
			   	companyId=info.getCompanyId();
				AllocationResult temp=new AllocationResult();
				logger.log(Level.SEVERE,"Employee "+info.getFullName());
				
				/**
				 * @author Anil @since 03-02-2021
				 * Checking payslip validation project wise
				 * if payroll for site is done then only will restrict user to process payroll
				 * For SunRise Raised By Rahul Tiwari
				 */
				List<PaySlip> processedPayslipList=new ArrayList<PaySlip>();
				if(siteLocation){
					processedPayslipList=isPaySlipCreated1(info, payRollPeriod);
					
				}
				
				if(siteLocation){
					if(projectHs!=null&&projectHs.size()>0){
						logger.log(Level.SEVERE,"projectHs "+projectHs);
						if(processedPayslipList!=null&&processedPayslipList.size()>0){
							logger.log(Level.SEVERE,"processedPayslipList "+processedPayslipList.size());
							int counter=0;
							HashSet<String> psProjecths=new HashSet<String>();
							for(PaySlip pay:processedPayslipList){
								psProjecths.add(pay.getProjectName());
							}
							
							for(String project:psProjecths){
								logger.log(Level.SEVERE,"getProjectName "+project);
								if(projectHs.contains(project)){
									counter++;
								}
							}
							
							logger.log(Level.SEVERE,"counter "+counter+"");
							if(counter==projectHs.size()){
								temp.setInfo(info);
								temp.setStatus(false);
								temp.setReason("Already Processed !");
								allcationResult.add(temp);
								continue;
							}
						}
					}
				}
				
				if(!siteLocation){
					boolean b=isPaySlipCreated(info, payRollPeriod);
					logger.log(Level.SEVERE,"Pay "+b);
					
					if(b==true){
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Already Processed !");
						allcationResult.add(temp);
						continue;
					}
				}
//				boolean b=isPaySlipCreated(info, payRollPeriod);
//				logger.log(Level.SEVERE,"Pay "+b);
				
				
//				logger.log(Level.SEVERE,"atten list 1 "+attenList.size());
				
				if(attenList.size()==0){
//					logger.log(Level.SEVERE,"atten list 3 "+info.getFullName());
					temp.setInfo(info);
					temp.setStatus(false);
					temp.setReason("No attendance found.");
					allcationResult.add(temp);
					continue;
				}
				
//				if(b==true){
//					temp.setInfo(info);
//					temp.setStatus(false);
//					temp.setReason("Already Proceeseed !");
//					allcationResult.add(temp);
//					continue;
//				}
				
					
				if(info.getCalendarName().trim().equals("")==true)
				{
					temp.setInfo(info);
					temp.setStatus(false);
					temp.setReason("Calendar Not Allocated !");
					allcationResult.add(temp);
					continue;
				}
				/** date 21.11.2018 added by komal to restrict payroll of 
				 *  resigned , terminated and absconded employee 
				 */
				if(!info.isRealeaseForPayment()){
					if(info.isAbsconded())
					{
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Employee absconded !");
						allcationResult.add(temp);
						continue;
					}
					if(info.isResigned())
					{
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Employee resigned !");
						allcationResult.add(temp);
						continue;
					}
					if(info.isTerminated())
					{
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Employee Terminated !");
						allcationResult.add(temp);
						continue;
					}
				}
				int count=info.getEmpCount();
				Long cId=info.getCompanyId();
//				String calendarName=info.getCalendarName();
				
				/**
				 * @author Anil @since 04-02-2021
				 * Capturing processed payroll data if any
				 */
				if(siteLocation&&processedPayslipList!=null&&processedPayslipList.size()>0){
					for(PaySlip pay:processedPayslipList){
						if(processedPaySlipMap!=null&&processedPaySlipMap.size()!=0){
							if(processedPaySlipMap.containsKey(pay.getEmpid()+"$"+pay.getState())){
								processedPaySlipMap.get(pay.getEmpid()+"$"+pay.getState()).add(pay);
							}else{
								ArrayList<PaySlip> slipList=new ArrayList<PaySlip>();
								slipList.add(pay);
								processedPaySlipMap.put(pay.getEmpid()+"$"+pay.getState(), slipList);
							}
							
						}else{
							ArrayList<PaySlip> slipList=new ArrayList<PaySlip>();
							slipList.add(pay);
							processedPaySlipMap.put(pay.getEmpid()+"$"+pay.getState(), slipList);
						}
						
						
						List<CompanyPayrollRecord> compPayRecList=ofy().load().type(CompanyPayrollRecord.class).filter("salaryPeriod", payRollPeriod).filter("empId",info.getEmpCount()).list();
						
						if(compPayRecList!=null&&compPayRecList.size()!=0){
							for(CompanyPayrollRecord compPayRec1:compPayRecList){
								if(processedEmployerContributionMap!=null&&processedEmployerContributionMap.size()!=0){
									if(processedEmployerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
										processedEmployerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
									}else{
										ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
										slipList.add(compPayRec1);
										processedEmployerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
									}
									
								}else{
									ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
									slipList.add(compPayRec1);
									processedEmployerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
								}
							}
						}
					}
					
					logger.log(Level.SEVERE,"SITE LOCATION FLAG TRUE \n" +"PROCESSED PAYSLIP MAP "+processedPaySlipMap.size()+"\n PROCESSED EMPLOYER MAP "+processedEmployerContributionMap.size()); 
				}
				
				List<Attendance> attendanceList=new ArrayList<Attendance>();
				String projectName="";
				String siteLocationName="";
				for (Map.Entry<String, ArrayList<Attendance>> entry : innerSiteAttendanceMap.entrySet())  {
					isPfApplicable=false;
					isEsicApplicable=false;
					pf=masterPf;
					esic=masterEsic;
					temp=new AllocationResult();
					logger.log(Level.SEVERE,"Key = " + entry.getKey()); 
					attendanceList=new ArrayList<Attendance>();
					attendanceList=entry.getValue();
					
					/**
					 * @author Anil
					 * @since 09-09-2020
					 * Payroll no of days error-Multicycle payroll issue
					 */
					if(attendanceList.size()!=0){
						if(attendanceList.get(0).getStartDate()!=null&&attendanceList.get(0).getEndDate()!=null){
							payrollStartDate=attendanceList.get(0).getStartDate();
							payrollEndDate=attendanceList.get(0).getEndDate();
							logger.log(Level.SEVERE,"payrollStartDate "+payrollStartDate+"   payrollEndDate "+payrollEndDate );
						}
					}
					
					projectName=attendanceList.get(0).getProjectName();
					siteLocationName=attendanceList.get(0).getSiteLocation();
					logger.log(Level.SEVERE,"ATTENDANCE PROJECT NAME :  "+projectName+" siteLocation "+siteLocationName);
					
					/**
					 * @author Anil @since 05-02-2021
					 */
					boolean continueFlag=false;
					if(siteLocation){
						for(PaySlip pay:processedPayslipList){
							if(pay.getProjectName().equals(projectName)&&siteLocationName.equals(pay.getSiteLocation())){
								continueFlag=true;
								break;
							}
						}
						if(continueFlag){
							temp.setInfo(info);
							temp.setStatus(false);
							temp.setReason("Payroll already processed for "+projectName+" / "+siteLocationName);
							allcationResult.add(temp);
							continue;
						}
					}
					
					/**
				     * @author Anil
				     * @since 24-08-2020
				     * Loading PT and LWF details from project if process configuration is active
				     */
				    if(ptAndLwfProjectWise){
				    	lwfList=new ArrayList<LWF>();
				    	HrProject project=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", projectName).first().now();
				    	if(project!=null){
				    		logger.log(Level.SEVERE,"PROJECT STATE : "+project.getState());
				    		if(project.getState()!=null&&!project.getState().equals("")){
				    			ptaxDeduction=ofy().load().type(ProfessionalTax.class).filter("companyId", companyId).filter("state",project.getState()).filter("status", true).list();
				    			if(isAutoFetchLwf&&
						    			((info.getEmpGrp()!=null&&!info.getEmpGrp().equals("")&&info.getEmpGrp().equalsIgnoreCase("Direct"))
						    			||(info.getNumberRangeAsEmpTyp()!=null&&!info.getNumberRangeAsEmpTyp().equals("")&&info.getNumberRangeAsEmpTyp().equalsIgnoreCase("Direct")))){
						    		lwfList=ofy().load().type(LWF.class).filter("companyId", companyId).filter("state", project.getState()).filter("status", true).list();
						    	}
				    		}
				    	}
				    	
				    	if(ptaxDeduction!=null&&ptaxDeduction.size()>0) {
					    	ptHs.addAll(ptaxDeduction);
					    }
					    if(lwfList!=null&&lwfList.size()>0) {
					    	logger.log(Level.SEVERE,"lwfList size="+lwfList.size()); 
					    	lwfHs.addAll(lwfList);
					    }
				    	
				    	if(project.getState()!=null&&!project.getState().equals("")){
				    		state=project.getState();
				    		logger.log(Level.SEVERE,"STATE NAME AS PER PROJECT : "+state);
				    	}
				    	
				    	if(!pfCappingFlag&&project!=null&&project.getPfCappingAmt()!=0){
				    		pfMaxFixedValue=project.getPfCappingAmt();
				    		logger.log(Level.SEVERE,"PF CAPPING AMT : "+pfMaxFixedValue);
				    	}
				    }
					
					if(attendanceList.size()==0){
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("No attendance found.");
						allcationResult.add(temp);
						continue;
					}
					Comparator<Attendance> attenCompare = new  Comparator<Attendance>() {			
						@Override
						public int compare(Attendance o1, Attendance o2) {
							// TODO Auto-generated method stub
							return o1.getAttendanceDate().compareTo(o2.getAttendanceDate());
						}
					};
					Collections.sort(attendanceList,attenCompare);
					
					Date attendanceStartDate=DateUtility.getDateWithTimeZone("IST", attendanceList.get(0).getAttendanceDate());
					Date attendanceEndDate=DateUtility.getDateWithTimeZone("IST", attendanceList.get(attendanceList.size()-1).getAttendanceDate());
					
					logger.log(Level.SEVERE,"Attendance Start Date : "+attendanceStartDate);
					logger.log(Level.SEVERE,"Attendance End Date : "+attendanceEndDate);
					
				/**
				 * Date : 12-05-2018 By ANIL
				 * Commented old retrieval 
				 * Date : 17-07-2019 By Anil
				 * updated CTC loading part for process payroll
				 * this we are doing because of site wise payroll
				 */
				
				CTC ctc=null;
				List<CTC> ctcList=null;
				ctcList=ofy().load().type(CTC.class).filter("empid",count).filter("companyId",cId).filter("status", "Active").list();	
				
				if(ctcList==null||ctcList.size()==0){
					if(ctc==null){
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Ctc do Not Exists !");
						allcationResult.add(temp);
						continue;
					}
				}else if(ctcList.size()==1&&innerSiteAttendanceMap.size()==1){
					ctc=ctcList.get(0);
				}else{
					boolean projectFlag=false;
					for(CTC obj:ctcList){
						if(obj.getProjectName()!=null&&!obj.getProjectName().equals("")){
							projectFlag=true;
							if(projectName.equals(obj.getProjectName())){
								ctc=obj;
								break;
							}
						}
					}
					
					if(projectFlag&&ctc==null){
						for(CTC obj:ctcList){
							if(obj.getProjectName()==null||obj.getProjectName().equals("")){
								ctc=obj;
								break;
							}
						}
					}
					
					/**
					 * @author Anil
					 * @since 01-08-2020
					 * site location
					 */
					if(siteLocation&&ctc==null&&!projectFlag){
						for(CTC obj:ctcList){
							if(obj.getProjectName()==null||obj.getProjectName().equals("")){
								ctc=obj;
								break;
							}
						}
					}
					
					if(ctc==null){
						temp=new AllocationResult();
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Ctc do Not Exists for "+projectName);
						allcationResult.add(temp);
						continue;
					}
				}
				
				
				ArrayList<CtcComponent> dedList = new ArrayList<CtcComponent>();
				if(ctc!=null)
				{
					/**
					 * Date : 12-05-2018 By  ANIL
					 */
					if(ctc.getDeduction()!=null){
						
						/**
						 * @author Anil
						 * @since 10-09-2020
						 * adding clone deduction component on deduction list
						 */
//						deduction=ctc.getDeduction();
						deduction=new ArrayList<CtcComponent>();
						for(CtcComponent comp:ctc.getDeduction()){
							CtcComponent ded=comp.Myclone();
							deduction.add(ded);
						}
						
						
//						logger.log(Level.SEVERE,"DEDUCTION SIZE BF "+deduction.size()+" "+deduction );
						if(lwfList!=null&&lwfList.size()!=0){
//							logger.log(Level.SEVERE,"DEDUCTION SIZE BEFOR LWF "+deduction.size());
							for(int i=0;i<deduction.size();i++){
								CtcComponent comp=deduction.get(i);
								if(comp.getShortName().equalsIgnoreCase("LWF")
										||comp.getShortName().trim().equalsIgnoreCase("MLWF")
										||comp.getShortName().trim().equalsIgnoreCase("MWF")){
									deduction.remove(i);
									i--;
								}
							}
//							logger.log(Level.SEVERE,"DEDUCTION SIZE After removing LWF "+deduction.size());
							ArrayList<CtcComponent> list=getEmployeeContributionLwfList(lwfList);
							deduction.addAll(list);
//							logger.log(Level.SEVERE,"DEDUCTION SIZE After adding LWF "+deduction.size());
						}else if(isAutoFetchLwf&&(lwfList==null||lwfList.size()==0)){
							 /**
						     * @author Anil
						     * @since 24-08-2020
						     * If process configuration is active and LWF component is entered in 
						     * CTC though employee is not applicable for same as its employee group 
						     * gets changed in that case from LWF from CTC
						     */
							for(int i=0;i<deduction.size();i++){
								CtcComponent comp=deduction.get(i);
								if(comp.getShortName().equalsIgnoreCase("LWF")
										||comp.getShortName().trim().equalsIgnoreCase("MLWF")
										||comp.getShortName().trim().equalsIgnoreCase("MWF")){
									deduction.remove(i);
									i--;
								}
							}
							ArrayList<CtcComponent> list=getEmployeeContributionLwfList(lwfList);
							deduction.addAll(list);
						}
						
						/**
						 * @author Anil , Date : 03-07-2019
						 */
						for(int i=0;i<deduction.size();i++){
							CtcComponent comp=deduction.get(i);
							/**
							 * @author Anil , Date : 27-06-2019 
							 * if master pf not null then we remove pf from deduction component
							 */
							 if(pf!=null&&(comp.getShortName().equalsIgnoreCase("PF")||comp.getName().contains("PF"))){
								 deduction.remove(i);
								 i--;
								 isPfApplicable=true;
							 }
							 
							 if(esic!=null&&(comp.getShortName().equalsIgnoreCase("ESIC")||comp.getName().contains("ESIC"))){
								 deduction.remove(i);
								 i--;
								 isEsicApplicable=true;
							 }
						}
						
						//komal
						dedList = getDedList(ctc.getDeduction());
//						logger.log(Level.SEVERE,"DEDUCTION SIZE AF "+deduction.size()+" "+deduction);
					}
					if(ctc.getCompaniesContribution()!=null){
						
						/**
						 * @author Anil
						 * @since 10-09-2020
						 */
//						dedRecord=ctc.getCompaniesContribution();
						dedRecord=new ArrayList<CtcComponent>();
						for(CtcComponent comp:ctc.getCompaniesContribution()){
							CtcComponent ded=comp.Myclone();
							dedRecord.add(ded);
						}
						
						/**
						 * 
						 */
						if(lwfList!=null&&lwfList.size()!=0){
//							logger.log(Level.SEVERE,"EMPLOYER SIZE BEFOR LWF "+dedRecord);
							for(int i=0;i<dedRecord.size();i++){
//							for(CtcComponent comp:dedRecord){
								CtcComponent comp=dedRecord.get(i);
								if(comp.getShortName().equalsIgnoreCase("LWF")
										||comp.getShortName().trim().equalsIgnoreCase("MLWF")
										||comp.getShortName().trim().equalsIgnoreCase("MWF")){
									dedRecord.remove(i);
									i--;
								}
							}
//							logger.log(Level.SEVERE,"EMPLOYER SIZE After removing LWF "+dedRecord.size());
							ArrayList<CtcComponent> list=getEmployerContributionLwfList(lwfList);
							dedRecord.addAll(list);
//							logger.log(Level.SEVERE,"EMPLOYER SIZE After adding LWF "+dedRecord);
						}
						
						/**
						 * @author Anil , Date : 03-07-2019
						 */
						for(int i=0;i<dedRecord.size();i++){
							CtcComponent comp=dedRecord.get(i);
							/**
							 * @author Anil , Date : 27-06-2019 
							 * if master pf not null then we remove pf from deduction component
							 */
							 if(pf!=null&&(comp.getShortName().equalsIgnoreCase("PF")||comp.getName().contains("PF"))){
								 dedRecord.remove(i);
								 i--;
							 }
							 
							 if(esic!=null&&(comp.getShortName().equalsIgnoreCase("ESIC")||comp.getName().contains("ESIC"))){
								 dedRecord.remove(i);
								 i--;
								 isEsicApplicable=true;
							 }
						}
//						logger.log(Level.SEVERE,"EMPLOYER SIZE After "+dedRecord);
					}
					
					//If we do not get pf in employee deduction then pf is not applicable for that employee
					if(!isPfApplicable){
						pf=null;
						logger.log(Level.SEVERE,"PF not applicable as not mention in deduction list.");
					}
					
					//If we do not get pf in employee deduction then pf is not applicable for that employee
					if(!isEsicApplicable){
						esic=null;
						logger.log(Level.SEVERE,"ESIC not applicable as not mention in deduction list.");
					}
					
					/***
					 * @author Abhinav Bihde
					 * @since 30/12/2019
					 * As per Rahul Tiwari's Requirements Sachin : During Payroll if an Employee don't have UAN in employee master,
					 * then PF shouldn't be deducted during Payroll.
					 * Similarly if an Employee don't have ESIC number in Employee master then his ESIC shouldn't be deducted as well.
					 */
					if (isDeductionUANAndESIC) {
//						logger.log(Level.SEVERE, "Flag 22:"+ isDeductionUANAndESIC);
						Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId)
								.filter("count", info.getEmpCount()).first().now();
						if (emp != null) {
//							logger.log(Level.SEVERE, "emp if:" + emp);

						}
						if (emp.getUANno() == null || emp.getUANno().equals("")) {
//							logger.log(Level.SEVERE,"Falg UAN1:" + emp.getUANno());
							isPfApplicable = false;
							pf = null;
//							logger.log(Level.SEVERE, "PF UAN2:" + pf);
						}
						if (emp.getEmployeeESICcode() == null|| emp.getEmployeeESICcode().equals("")) {
//							logger.log(Level.SEVERE,"ESICcode1:" + emp.getEmployeeESICcode());
							isEsicApplicable = false;
							esic = null;
//							logger.log(Level.SEVERE, "ESICcode2:"+ isEsicApplicable);
						}
					}
				}
				
				/**
				 * Date : 21-08-2018 By ANIL
				 * Arrears Calculation logic
				 * Date : 31-12-2018 BY ANIL
				 * Added code for calculating arrears of bonus and paid leave
				 */
				ArrayList<CtcComponent>earningArrearsList=new ArrayList<CtcComponent>();
				ArrayList<CtcComponent>deductionArrearsList=new ArrayList<CtcComponent>();
				ArrayList<CtcComponent>bonusAndPaidLeaveArrearsList=new ArrayList<CtcComponent>();
				
				calculateArrears(earningArrearsList,deductionArrearsList,bonusAndPaidLeaveArrearsList,companyId,count,date,projectName);
				
				/**
				 * Arrears End
				 */
//				int totalDaysInMonth=this.daysInMonth(payRollPeriod, "IST");
				int totalDaysInMonth=0;
				if(payrollStartDate!=null&&payrollEndDate!=null){
					totalDaysInMonth=DateUtility.getNumberOfDaysBetweenDates(payrollEndDate, payrollStartDate)+1;
				}else{
					totalDaysInMonth=this.daysInMonth(payRollPeriod, "IST");
				}
				
				logger.log(Level.SEVERE,"DAYS IN MONTH "+totalDaysInMonth);
				
				if(attendanceFlag==true&&attendanceList.size()==0){
					temp.setInfo(info);
					temp.setStatus(false);
					temp.setReason("Please upload attendance for processing payroll.");
					allcationResult.add(temp);
					continue;
				}
				
				/**
				 * Date : 10-09-2018 BY ANIL
				 */
				int shortDays = 0;
				if(attendanceFlag==true&&attendanceList.size()<totalDaysInMonth){
					
					if(noOfDaysValidationForPayrollFlag) {
						/**
						 * @author Anil
						 * @since 26-11-2020
						 * Uncommenting below code, if attendance is less then a month then return error
						 * raised by Rahul for sunrise
						 */
						if(attenList.size()<totalDaysInMonth){
							temp.setInfo(info);
							temp.setStatus(false);
							temp.setReason("Please upload attendance for whole month.");
							allcationResult.add(temp);
							invalidAttendanceFlag=true;
							continue;
						}
					}
						
					shortDays=totalDaysInMonth-attendanceList.size();
						
					
				}else if(attendanceFlag==true&&attendanceList.size()>totalDaysInMonth){
					temp.setInfo(info);
					temp.setStatus(false);
					temp.setReason("Please delete and upload attendance again.");
					allcationResult.add(temp);
					invalidAttendanceFlag=true;
					continue;
				}
				
					
				int weekdayinperiod=0;
//				int weekdayinperiod=calCulateWeekDay(info,payRollPeriod,"IST");
				int holidayPeriod=0;
//				int holidayPeriod=this.calCulateHoliday(info, payRollPeriod, "IST");
				double noEligibleDays=0;
//				double noEligibleDays=this.daysInMonth(payRollPeriod, "IST")-(weekdayinperiod+holidayPeriod);
				int extraDay = 0;
				int absentDays=0;
				
				/**
				 * Date : 15-12-2018 BY ANIL
				 */
				int leavesInDays=0;
				int otInDays=0;
				
				if(attendanceFlag){
					paidLeaveAndBonusAsAdditionalAllowance=checkPlAndBonusStatus(attendanceList);
					int totalAttendance=attendanceList.size();
					if(nhOtFlag){
						extraDay=getNationalHolidayDetails(attendanceList,info);
						totalAttendance=totalAttendance+extraDay;
					}
					weekdayinperiod=calculateWeekDayFromAttendance(attendanceList);
					holidayPeriod=this.calCulateHolidayFromAttendance(attendanceList);
					/**
					 * Date : 31-08-2018 BY ANIL
					 * NA should not be included in present days
					 */
					absentDays=getTotalAbsentDays(attendanceList);
					totalAttendance=totalAttendance-absentDays;
					
					/**
					 * @author Anil , Date : 22-06-2019
					 */
					totalAttendance=totalAttendance-getActualAbsentDays(attendanceList);
					noEligibleDays=totalAttendance-(weekdayinperiod+holidayPeriod);
					/**
					 * Date : 15-12-2018 By ANIL
					 */
					logger.log(Level.SEVERE,"Total Attendance "+totalAttendance+" WO "+weekdayinperiod+" H "+holidayPeriod+" NO. OF ELIGIBLE DAYS "+noEligibleDays+" Absent Days : "+absentDays);
					
					/**
					 * Date : 02-01-2019 BY ANIL
					 * Adding parameter as  info for checking whether weekly off is assigned to employee 
					 */
					leavesInDays=getNoOfLeaveDays(attendanceList,info);
					otInDays=getNoOfOtDays(attendanceList);
					noOfPresentDays=getNoOfPresentDays(attendanceList);
					logger.log(Level.SEVERE,"NO OF LEAVES DAYS : "+leavesInDays+" NO OF OT DAYS : "+otInDays+" NO OF PRESENT DAYS : "+noOfPresentDays);
				}else{
					weekdayinperiod=calCulateWeekDay(info,payRollPeriod,"IST",payrollStartDate,payrollEndDate);
					holidayPeriod=this.calCulateHoliday(info, payRollPeriod, "IST",payrollStartDate,payrollEndDate);
//					noEligibleDays=this.daysInMonth(payRollPeriod, "IST")-(weekdayinperiod+holidayPeriod);
					noEligibleDays=totalDaysInMonth-(weekdayinperiod+holidayPeriod);
				}
				double unpaidLeaveHrs=getUnpaidLeaveHrs(info, payRollPeriod, "IST",attendanceStartDate,attendanceEndDate,projectName);
				double overtimeHrs=getOverTimeHrs(info, payRollPeriod, "IST",attendanceStartDate,attendanceEndDate,projectName);
				double overtimeDays=overtimeHrs/info.getLeaveCalendar().getWorkingHours();
				double overtimeAmt=getOverTimeAmount(info,payRollPeriod,"IST",overtimeHrs,ctc,totalDaysInMonth,otList,attendanceStartDate,attendanceEndDate,projectName);
				/**
				 * Date : 02-06-2018 By  ANIL
				 */
				double earnedLeave=0;
				double unpaidDays=unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours();
				if(!attendanceFlag){
					/**
					 * @author Anil, Date : 29-06-2019
					 * updating working hours,subtracting unpaid days from eligible days
					 */
					noEligibleDays=noEligibleDays-unpaidDays;
				}
				
				/**
				 * Date : 31-08-2018 BY ANIL
				 * added absent days NA
				 */
				double paidDays;
				int woCount=0;
				HrProject project=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", projectName).first().now();
		    	if(project!=null&&project.isRotationalWeeklyOff()){ //Ashwini Patil Date:31-10-2023
		    		
		    		if(project.getFixedPayrollDays()!=null&&project.getFixedPayrollDays()>0) {
//		    			paidDays=(project.getFixedPayrollDays()+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-(absentDays+shortDays);					
		    			paidDays= noOfPresentDays+holidayPeriod; //Holidays added to paid days on 16-1-2023
		    			isFixedPayrollDaysDefinedInProject=true;
		    			logger.log(Level.SEVERE,"FixedPayrollDays="+project.getFixedPayrollDays()+" paidDays="+paidDays);	   			    		
		    		}
		    		else {
		    			if(info.getLeaveCalendar().getWeeklyoff()!=null){
			    			String[] res=payRollPeriod.split("-",0);
			    			String salmonth = res[1].trim();
			    			int m=ServerAppUtility.getMonthNumber(salmonth);			
			    			int salyear =Integer.parseInt(res[0].trim());
			    			logger.log(Level.SEVERE,"salmonth="+salmonth+" salyear="+salyear);//+" date="+cal.getTime()+ "day of month="+cal.getActualMaximum(Calendar.DAY_OF_MONTH)
			    			woCount=ServerAppUtility.calculateEligibleWo(info.getLeaveCalendar().getWeeklyoff(), salyear, m);
			    			logger.log(Level.SEVERE,"wocount="+woCount);
			    		}
			    		int woWiseEligibleDays=totalDaysInMonth-woCount;
			    		paidDays=(woWiseEligibleDays+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-(absentDays+shortDays);		
			    		logger.log(Level.SEVERE,"extraDay="+extraDay+" unpaidLeaveHrs="+unpaidLeaveHrs+" absentDays="+absentDays+" shortDays="+shortDays+" info.getLeaveCalendar().getWorkingHours()="+info.getLeaveCalendar().getWorkingHours());	   	
			    		
			    		logger.log(Level.SEVERE,"woWiseEligibleDays="+woWiseEligibleDays+" paidDays="+paidDays+" ");	   	
		    		}
		    	}else if(project!=null&&project.getFixedPayrollDays()!=null&&project.getFixedPayrollDays()>0) {
		    		paidDays= noOfPresentDays;//(project.getFixedPayrollDays()+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-(absentDays+shortDays);					
		    		isFixedPayrollDaysDefinedInProject=true;
		    		logger.log(Level.SEVERE,"FixedPayrollDays="+project.getFixedPayrollDays()+" paidDays="+paidDays);	   			    		
	    		
		    	}else
		    		paidDays=(totalDaysInMonth+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-(absentDays+shortDays);
				
				
//				logger.log(Level.SEVERE,"Availed Leave List SIZE_------ ::: "+availedLeaves.size()+" Paid Days : "+paidDays);
				
				/**
				 * Date : 18-05-2018 by ANIL
				 */
				String empGender="";
				if(info.getGender()!=null){
					empGender=info.getGender();
				}
				
				/**
				 * Date :15-12-2018 BY ANIL
				 */
				if(attendanceFlag&&noOfLeaveDays!=leavesInDays){
					temp.setInfo(info);
					temp.setStatus(false);
					temp.setReason("Number of leave days are not matching");
					allcationResult.add(temp);
					continue;
				}
				
				if(attendanceFlag&&noOfOtDays!=otInDays){
					temp.setInfo(info);
					temp.setStatus(false);
					temp.setReason("Number of ot days are not matching");
					allcationResult.add(temp);
					continue;
				}
				
				if(attendanceFlag&&paidDays<noOfPresentDays){
					if(!isFixedPayrollDaysDefinedInProject) { //It can happen that fix payroll days are 26 but employee present for 28 days
						temp.setInfo(info);
						temp.setStatus(false);
						temp.setReason("Paid days is less than present days");
						allcationResult.add(temp);
						continue;					
					}
				}
				
				/**
				 * End
				 */
				
				/**
				 * @author Anil , Date : 31-08-2019
				 * Calculation of ESIC  based on Contribution period
				 * In case ESIC is not applicable for current payroll month 
				 * but employer have to pay ESIC till the contribution period
				 */
				if(esic==null&&masterEsic!=null){
					if(masterEsic.getContributionPeriodList()!=null&&masterEsic.getContributionPeriodList().size()!=0){
						ArrayList<String> payrollMonthList=new ArrayList<String>();
						Date payDate=DateUtility.getDateWithTimeZone("IST", date);
						payDate=DateUtility.getStartDateofMonth(payDate);
						
						logger.log(Level.SEVERE,"PAY DATE  --- ::: "+payDate);
						
						SimpleDateFormat yearFmt = new SimpleDateFormat("yyyy");
						String payrollYear=yearFmt.format(payDate);
						
						for(EsicContributionPeriod obj:masterEsic.getContributionPeriodList()){
							
							/**
							 * @author Anil @since 05-03-2021
							 * Payroll was getting failed due to ESIC contribution period get modified becuase of loop 
							 * Raised by Rahul for Sunrise
							 */
							
//							obj.setFromPeriod(payrollYear+"-"+obj.getFromPeriod());
//							obj.setToPeriod(payrollYear+"-"+obj.getToPeriod());
							
							String fromPeriod=payrollYear+"-"+obj.getFromPeriod();
							String toPeriod=payrollYear+"-"+obj.getToPeriod();
							
							logger.log(Level.SEVERE,"PERIOD FROM : "+obj.getFromPeriod()+" / "+fromPeriod);
							logger.log(Level.SEVERE,"PERIOD TO : "+obj.getToPeriod()+" / "+toPeriod);
							
							Calendar frmCal = Calendar.getInstance(); 
							Date frmDt = null;
							try {
//								frmDt = isoFormat.parse(obj.getFromPeriod());
								frmDt = isoFormat.parse(fromPeriod);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							frmCal.setTime(frmDt);
							frmCal.add(Calendar.DATE, 1);
							frmDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
							
							Calendar toCal = Calendar.getInstance(); 
							Date toDt = null;
							try {
//								toDt = isoFormat.parse(obj.getToPeriod());
								toDt = isoFormat.parse(toPeriod);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							toCal.setTime(toDt);
							toCal.add(Calendar.DATE, 1);
							toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
							
							if(toDt.before(frmDt)){
								if(toDt.before(payDate)){
									toCal.add(Calendar.YEAR, 1);
									toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
//									logger.log(Level.SEVERE,"PAY > UPDATED PERIOD TO : "+obj.getToPeriod()+" "+toDt);
									logger.log(Level.SEVERE,"PAY > UPDATED PERIOD TO : "+toPeriod+" "+toDt);
								}else{
									frmCal.add(Calendar.YEAR, -1);
									frmDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
//									logger.log(Level.SEVERE,"PAY < UPDATED PERIOD From  : "+obj.getFromPeriod()+" "+frmDt);
									logger.log(Level.SEVERE,"PAY < UPDATED PERIOD From  : "+fromPeriod+" "+frmDt);
								}
								
							}
							
							Date startDate=null;
							Date endDate=null;
							startDate=DateUtility.getStartDateofMonth(frmDt);
							endDate=DateUtility.getStartDateofMonth(toDt);
							
							logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
							logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
							logger.log(Level.SEVERE,"PAY DATE  --- ::: "+payDate);
							
							
							if((payDate.after(startDate)||payDate.equals(startDate))
									&&(payDate.before(endDate)||payDate.equals(endDate))){
								logger.log(Level.SEVERE,"CONDITION TRUE : "+startDate);
								while(startDate.before(endDate)||startDate.equals(endDate)){
									payrollMonthList.add(isoFormat.format(startDate));
									Calendar startDtCal = Calendar.getInstance(); 
									startDtCal.setTime(startDate);
									startDtCal.add(Calendar.MONTH, 1);
									startDate=DateUtility.getDateWithTimeZone("IST", startDtCal.getTime());
								}
								break;
							}
						}
//						logger.log(Level.SEVERE,"PAY MONTH SIZE  --- ::: "+payrollMonthList.size());
						
						if(payrollMonthList.size()!=0){
							List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("salaryPeriod IN", payrollMonthList).filter("empid", info.getEmpCount()).filter("companyId", companyId).list();
							if(paySlipList!=null&&paySlipList.size()!=0){
								for(PaySlip slip:paySlipList){
									boolean processFlag=false;
									for(CtcComponent comp:slip.getDeductionList()){
										if(comp.getShortName().equalsIgnoreCase("ESIC")){
											esic=masterEsic;
											esic.setCalculateOnActual(true);
											processFlag=true;
//											logger.log(Level.SEVERE,"ESIC FALLs ON CONTRIBUTION PERIOD : "+processFlag);
											break;
										}
									}
									if(processFlag==true){
										break;
									}
								}
							}
						}
						
					}
				}
				
				/**
				 * @author Anil , Date : 29-11-2019
				 * Salary calculation as per the paid days in month rather than month days
				 * for sachin enterprises raised by Rahul Tiwari
				 */
				
				Date startDate=null;
				Date endDate=null;
				
				if(payrollStartDate!=null&&payrollEndDate!=null){
					startDate=payrollStartDate;
					endDate=payrollEndDate;
				}else{
					startDate=DateUtility.getStartDateofMonth(payrollPeriodDate);
					endDate=DateUtility.getStartDateofMonth(payrollPeriodDate);
				}
				
				int actualNoOfWo=getTotalNoOfWeeklyOffBetweenDates(info, startDate, endDate);
				
				double paidDaysInMonth=0;
				double totalValidDaysExceptWoDays=(paidDays-weekdayinperiod)+unpaidDays+absentDays+shortDays;
				double totalPaidDaysExceptWoDays=paidDays-weekdayinperiod;
				double totalUnpaidDays=unpaidDays+absentDays+shortDays;
				double woDayAsExtraDay=0;
				double calculatedWoDays=0;
				String extraDayCompName=null;
				boolean fixedDayWisePayroll=false;
				int actualDaysInMonth=totalDaysInMonth;
				if(branch!=null&&branch.getPaidDaysInMonth()!=0&&info.getEmpGrp()!=null&&info.getEmpGrp().equalsIgnoreCase("Direct")){
					fixedDayWisePayroll=true;
					logger.log(Level.SEVERE,"IN SIDE BRANCH PAID DAYS "+branch.getPaidDaysInMonth()+" "+branch.getBusinessUnitName());
					/**
					 * for 30 days
					 * 26,4
					 * 25,5
					 * for 31 days
					 * 27,4
					 * 26,5
					 */
					paidDaysInMonth=branch.getPaidDaysInMonth();
					if(branch.isExceptionForMonth()){
						String[] salarySplitData=payRollPeriod.split("-");
						if(salarySplitData[1].trim().equalsIgnoreCase(branch.getSpecificMonth().trim())){
							paidDaysInMonth=branch.getPaidDaysForExceptionalMonth();
						}
					}
					
					calculatedWoDays=totalDaysInMonth-paidDaysInMonth;
					extraDayCompName=branch.getExtraDayCompName();
					
					logger.log(Level.SEVERE,"PAID DAYS "+paidDaysInMonth+" ACTUAL WO "+actualNoOfWo+" CAL WO "+calculatedWoDays+" CAP WO "+weekdayinperiod);
					logger.log(Level.SEVERE,"VALID DAYS "+totalValidDaysExceptWoDays+" VALID PAID DAYS EXCEPT WO "+totalPaidDaysExceptWoDays);
					
					if(totalValidDaysExceptWoDays>paidDaysInMonth||totalValidDaysExceptWoDays==paidDaysInMonth){
//						logger.log(Level.SEVERE,"Valid days greater/equal");
						if(totalPaidDaysExceptWoDays>paidDaysInMonth||totalPaidDaysExceptWoDays==paidDaysInMonth){
//							logger.log(Level.SEVERE,"paid days greater");
							woDayAsExtraDay=totalPaidDaysExceptWoDays-paidDaysInMonth;
							paidDays=paidDaysInMonth;
						}else if(totalPaidDaysExceptWoDays<paidDaysInMonth){
//							logger.log(Level.SEVERE,"paid days less");
							if(actualNoOfWo!=0&&actualNoOfWo>calculatedWoDays){
								double diff=actualNoOfWo-calculatedWoDays;
								paidDays=totalPaidDaysExceptWoDays+diff;
								weekdayinperiod=(int) (weekdayinperiod-diff);
							}else if(weekdayinperiod!=0&&weekdayinperiod>calculatedWoDays){
								double diff=weekdayinperiod-calculatedWoDays;
								paidDays=totalPaidDaysExceptWoDays+diff;
								weekdayinperiod=(int) (weekdayinperiod-diff);
							}else if(weekdayinperiod!=0&&weekdayinperiod<calculatedWoDays){
								double diff=calculatedWoDays-weekdayinperiod;
								paidDays=totalPaidDaysExceptWoDays;
//								weekdayinperiod=(int) (weekdayinperiod-diff);
							}else if(weekdayinperiod!=0&&weekdayinperiod==calculatedWoDays){
								double diff=paidDaysInMonth-totalPaidDaysExceptWoDays;
								paidDays=totalPaidDaysExceptWoDays;
							}
						}
					}else if(totalValidDaysExceptWoDays<paidDaysInMonth){
//						logger.log(Level.SEVERE,"Valid days less");
						if(totalPaidDaysExceptWoDays<paidDaysInMonth){
//							logger.log(Level.SEVERE,"paid days less");
							if(actualNoOfWo!=0&&actualNoOfWo>calculatedWoDays){
								double diff=actualNoOfWo-calculatedWoDays;
								paidDays=totalPaidDaysExceptWoDays+diff;
								weekdayinperiod=(int) (weekdayinperiod-diff);
							}else if(weekdayinperiod!=0&&weekdayinperiod>calculatedWoDays){
								double diff=weekdayinperiod-calculatedWoDays;
								paidDays=totalPaidDaysExceptWoDays+diff;
								weekdayinperiod=(int) (weekdayinperiod-diff);
							}else if(weekdayinperiod!=0&&weekdayinperiod<calculatedWoDays){
								double diff=calculatedWoDays-weekdayinperiod;
								paidDays=totalPaidDaysExceptWoDays;
//								weekdayinperiod=(int) (weekdayinperiod-diff);
							}else if(weekdayinperiod!=0&&weekdayinperiod==calculatedWoDays){
								double diff=paidDaysInMonth-totalPaidDaysExceptWoDays;
								paidDays=totalPaidDaysExceptWoDays;
							}
						}
					}
					totalDaysInMonth=(int) paidDaysInMonth;
					
					logger.log(Level.SEVERE,"MONTH DAYS "+totalDaysInMonth+" WEEK OFF "+weekdayinperiod+" EXTRA DAY "+woDayAsExtraDay+" PAID DAYS "+paidDays);
					
				}
//				else if(project!=null&&project.getPaidDaysInMonth()!=0){
//					
//				}
				
				/**
				 * End
				 */
				
				/***
				 * @author Anil @since 26-10-2021
				 * Here we will check whether arrars had been added to any of salary slip of that project for particular employee
				 * if added then we will make arrars list zero so that it should not be added to another payslip
				 * Raised by Rahul Tiwari for Sunrise
				 */
				if(paySlipMap!=null&&paySlipMap.size()!=0){
					logger.log(Level.SEVERE,"Payslip Map not null/zero "+paySlipMap.size());
					if((earningArrearsList!=null&&earningArrearsList.size()!=0)
							||(deductionArrearsList!=null&&deductionArrearsList.size()!=0)
							||(bonusAndPaidLeaveArrearsList!=null&&bonusAndPaidLeaveArrearsList.size()!=0)){
						logger.log(Level.SEVERE,"Anyone of arrear list is not zero ");
						logger.log(Level.SEVERE,"earningArrearsList "+earningArrearsList.size());
						logger.log(Level.SEVERE,"deductionArrearsList "+deductionArrearsList.size());
						logger.log(Level.SEVERE,"bonusAndPaidLeaveArrearsList "+bonusAndPaidLeaveArrearsList.size());
						

						String key=info.getEmpCount()+projectName;
						if(info.isReliever()){
							key=info.getEmpCount()+"";
						}
						if(siteLocation){
							key=info.getEmpCount()+projectName+siteLocationName;
						}
						
						logger.log(Level.SEVERE,"Key for payslip :- "+key);
						
						 for (Map.Entry<String,ArrayList<PaySlip>> e1 : paySlipMap.entrySet()){
							 String modifiedKey=info.getEmpCount()+projectName;
							 for(PaySlip p:e1.getValue()){
								 logger.log(Level.SEVERE,"EMP Id : " +p.getEmpid());
								 logger.log(Level.SEVERE,"Project Name : " + p.getProjectName());
								 if(p.getEmpid()==info.getEmpCount()&&p.getProjectName().equals(projectName)){
									logger.log(Level.SEVERE,"payslip exist :- ");
									earningArrearsList=new ArrayList<CtcComponent>();
									deductionArrearsList=new ArrayList<CtcComponent>();
									bonusAndPaidLeaveArrearsList=new ArrayList<CtcComponent>();
									
									logger.log(Level.SEVERE,"After earningArrearsList "+earningArrearsList.size());
									logger.log(Level.SEVERE,"After deductionArrearsList "+deductionArrearsList.size());
									logger.log(Level.SEVERE,"After bonusAndPaidLeaveArrearsList "+bonusAndPaidLeaveArrearsList.size());
									break;
								 }
							 }
							 
						 }
					}
				}
				
				/**
				 * 
				 */
				
				PaySlip paySlip=PaySlip.createPaySlip(ctc,totalDaysInMonth,weekdayinperiod,holidayPeriod,unpaidDays,
						availedLeaves,noEligibleDays,paidDays,payRollPeriod,deduction,overtimeHrs,
						overtimeDays,overtimeAmt,earnedLeave,ptDeduction,empGender,pfMaxFixedValue,
						deductionTypeName,info.getNumberRangeAsEmpTyp(),info,earningArrearsList,deductionArrearsList,
						ptaxDeduction,month,extraDay,applicableDisabiltyPercentage,date,isProvisional,bonusAndPaidLeaveArrearsList,
						paidLeaveAndBonusAsAdditionalAllowance,otIncludedInGrossEarning,otList,empOtHisList,pf,projectName,esic,woDayAsExtraDay,extraDayCompName,actualDaysInMonth,fixedDayWisePayroll);
				logger.log(Level.SEVERE, "paySlip"+paySlip);
				
				paySlip.setStartDate(payrollStartDate);
				paySlip.setEndDate(payrollEndDate);
				paySlip.setSiteLocation(siteLocationName);
				paySlip.setState(state);
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "AddTDS", companyId)){
					if(pf!=null&&isPfApplicable){
						
						double pfRateAmt=paySlip.calculatePfAmt(pf,paySlip.getEarningList(),pfMaxFixedValue,paySlip.getMonthlyDays(),paidDays,true,false,false,paySlip,false);
						pfRateAmt=pfRateAmt*12;
						CtcComponent comp=new CtcComponent();
						comp.setDeduction(true);
						comp.setName(pf.getPfName());
						comp.setShortName(pf.getPfShortName());
//						comp.setActualAmount(pfActualAmt);
						comp.setAmount(pfRateAmt);
//						comp.setBaseAmount(pfBaseAmt);
						comp.setMaxPerOfCTC(pf.getEmployeeContribution());
						
						dedList.add(comp);
						logger.log(Level.SEVERE,"TDS PF "+pfRateAmt);
					}
					
					calculateTds(companyId,year,paySlip,ctc,dedList,info,cal,isProvisional);
				}
				
				List<CompanyPayrollRecord> payRecList=new ArrayList<CompanyPayrollRecord>();
				try{
					payRecList=calculateCompanyContribution(dedRecord,totalDaysInMonth,paidDays,ctc,info.getEmpCount(),info.getFullName(),payRollPeriod,pfMaxFixedValue,paySlip,deductionTypeName,isSiteWasPayroll,siteWiseAttendanceMap,employerContributionMap,projectName,info,siteLocation,siteLocationName,state);
				}catch(Exception e){
					e.printStackTrace();
				}
				try {
					logger.log(Level.SEVERE,"COMPANY'S CONTRIBUTION LIST SIZE "+payRecList.size());
				} catch (Exception e) {
				}
				
				
				
				
				temp.setInfo(info);
				temp.setStatus(true);
				temp.setReason("N.A");
				allcationResult.add(temp);
				paySlips.add(paySlip);
				
				logger.log(Level.SEVERE,"allcationResult"+allcationResult.size());

				PaySlipRecord record=new PaySlipRecord();
				record.setEmployeeKey(Key.create(info));
				record.setSalaryPeriod(payRollPeriod);
				paySlipRecordList.add(record);
				
				if(payRecList.size()!=0){
//					logger.log(Level.SEVERE,"COMPANY'S CONTRIBUTION LIST SIZE NOT ZERO "+payRecList.size());
					compPayrollRecord.addAll(payRecList);
				}
				LeaveBalance levbal=allocateMonthlyleaves(cId,info,paidDays,unpaidDays,paySlip,totalDaysInMonth);
				leaveBalance.add(levbal);
				/**
				 * @author Anil ,Date : 09-03-2019
				 */
				if(levbal!=null&&paySlip.getPaidLeaves()==0){
					paySlip.setLeaveBalList(getLeaveBalDetails(levbal.getAllocatedLeaves(),paySlip.getAviledLeaves(),info));
				}
				
				if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
					String key=paySlip.getEmpid()+paySlip.getProjectName();
					/**
					 * @author Anil @since 04-02-2021
					 */
					if(info.isReliever()){
						key=paySlip.getEmpid()+"";
					}
					if(siteLocation){
						key=paySlip.getEmpid()+paySlip.getProjectName()+paySlip.getSiteLocation();
					}
					
					if(siteWiseAttendanceMap.containsKey(key)){
						if(paySlipMap!=null&&paySlipMap.size()!=0){
							if(paySlipMap.containsKey(paySlip.getEmpid()+"$"+paySlip.getState())){
								paySlipMap.get(paySlip.getEmpid()+"$"+paySlip.getState()).add(paySlip);
							}else{
								ArrayList<PaySlip> slipList=new ArrayList<PaySlip>();
								slipList.add(paySlip);
								paySlipMap.put(paySlip.getEmpid()+"$"+paySlip.getState(), slipList);
							}
							
						}else{
							ArrayList<PaySlip> slipList=new ArrayList<PaySlip>();
							slipList.add(paySlip);
							paySlipMap.put(paySlip.getEmpid()+"$"+paySlip.getState(), slipList);
						}
					}
				}
				
				logger.log(Level.SEVERE,"logger test 2");

				}
			

			   	} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			logger.log(Level.SEVERE,"logger test 3");

		   if(paySlipMap!=null&&paySlipMap.size()!=0){
//			   logger.log(Level.SEVERE, "INSIDE MULTIPLE PAYSLIP DEDUCTION UPDATE METHOD.........."+paySlipMap.size());
			   updatePaySlipDeductions(paySlipMap,pfMaxFixedValue,deductionTypeName,new ArrayList<ProfessionalTax>(ptHs),empinfoarray,employerContributionMap,month,processedPaySlipMap,new ArrayList<LWF>(lwfHs));
		   }

		   if(employerContributionMap!=null&&employerContributionMap.size()!=0){
//			   logger.log(Level.SEVERE, "INSIDE MULTIPLE EMPLOYER CONTRIBUTION UPDATE METHOD.........."+employerContributionMap.size());
			   updateEmployerDeduction(employerContributionMap,pfMaxFixedValue,deductionTypeName,empinfoarray,0,null,false,processedEmployerContributionMap);
		   }
		   
			logger.log(Level.SEVERE,"logger test 5");
			logger.log(Level.SEVERE, "paySlips.size() "+paySlips.size());
			logger.log(Level.SEVERE,"isProvisional"+isProvisional);

			//Needs to be loaded first
		   /**
			 * Date : 24-11-2018 By ANIL
			 */
			if(paySlips.size()!=0&&!isProvisional)
			{
				logger.log(Level.SEVERE,"INSIDE SAVING DATA");
				if(compPayrollRecord.size()!=0){
					logger.log(Level.SEVERE,"INSIDE SAVING COMPANY PAYROLL DETAILS");
					ofy().save().entities(compPayrollRecord).now();
				}
				
				ofy().save().entities(paySlips).now();
				ofy().save().entities(paySlipRecordList);
				ofy().save().entities(leaveBalance);
				//Start iterating the Pay Slip List to Create the PaySlipPrin Helper
				
				/**
				 * @author Anil @since 06-04-2021
				 * Commenting this code 
				 * after doing payroll if user prints salary slip from statutory report screen
				 * double salary slip is getting printed
				 * Raised by Rahul Tiwari for Alkosh
				 */
//				for(PaySlip payslip:paySlips){
//					PaySlipPrintHelper helper=payslip.getPaySlipPrintHelper();
//					payslip.toString();
//					//Set them in Static List
//					CreatePayRollPDFServlet.list.add(helper);
//				}
			}
			
			/**
			 * Date : 24-11-2018 BY ANIL
			 */
			if(paySlips.size()!=0&&isProvisional){
				CsvWriter.paySlipList=new ArrayList<PaySlip>();
				CsvWriter.paySlipList.addAll(paySlips);
			}
			/**
			 * End
			 * @param date 
			 * @param count 
			 */
			
			
			return allcationResult;

		}
		
		private void updateEmployerDeduction(
				HashMap<String, ArrayList<CompanyPayrollRecord>> employerContributionMap,
				double pfMaxFixedValue, String deductionTypeName,ArrayList<EmployeeInfo> empinfoarray,int empId,String projectName,boolean esicFlag, HashMap<String, ArrayList<CompanyPayrollRecord>> processedEmployerContributionMap) {
			// TODO Auto-generated method stub
			
			
			if(employerContributionMap!=null&&employerContributionMap.size()!=0){
				
				for(Map.Entry<String, ArrayList<CompanyPayrollRecord>> entry:employerContributionMap.entrySet()){
					
					logger.log(Level.SEVERE, "EMPLOYER CONTRIBUTION :::: EMPLOYEE ID :::: "+entry.getKey());
					
					/**
					 * @author Anil @since 04-02-2021
					 */
					int employeeId=0;
					try{
						String array[] = entry.getKey().split("[$]");
						employeeId=Integer.parseInt(array[0]);
					}catch(Exception e){
						
					}
					
					double pfAmt=0;
					double lwfAmt=0;
					boolean lwfFlag=false;
					boolean pfFlag=false;
					
					double pfPercent=0;
					double esicRangeAmt=0;
					
					/**
					 * @author Anil @since 04-02-2021
					 */
					LinkedList<CompanyPayrollRecord> employerDeductionList=new LinkedList<CompanyPayrollRecord>();
					if(processedEmployerContributionMap!=null&&processedEmployerContributionMap.size()>0){
						if(processedEmployerContributionMap.containsKey(entry.getKey())){
							employerDeductionList.addAll(processedEmployerContributionMap.get(entry.getKey()));
						}
					}
					employerDeductionList.addAll(entry.getValue());
					
					if(employerDeductionList.size()==1){
						continue;
					}
					
//					for(int i=0;i<entry.getValue().size();i++){
//						CompanyPayrollRecord employerCont=entry.getValue().get(i);
					for(int i=0;i<employerDeductionList.size();i++){
						CompanyPayrollRecord employerCont=employerDeductionList.get(i);
						logger.log(Level.SEVERE, ":::: PROJECT :::: "+employerCont.getProjectName());
						if(empId!=0&&esicFlag&&projectName.equals(employerCont.getProjectName())){
							if(employerCont.getCtcCompShortName().equalsIgnoreCase("ESIC")||employerCont.getCtcCompShortName().equalsIgnoreCase("ESIC")){
//								entry.getValue().remove(i);
								employerDeductionList.remove(i);
								i--;
								logger.log(Level.SEVERE, "ESIC 1 AMT : ");
							}	
						}else{
							//PF Update Part
							if(pfFlag&&employerCont.getCtcCompShortName().equalsIgnoreCase("PF")){
								double cappedPfAmt=0;
								if(pfMaxFixedValue!=0){
									if(pfPercent!=0){
										cappedPfAmt=(pfMaxFixedValue*pfPercent)/100;
									}else{
										cappedPfAmt=(pfMaxFixedValue*13)/100;
									}
								}
								logger.log(Level.SEVERE, "EMP PF 2 AMT : "+pfAmt+" CAPPED AMT : "+cappedPfAmt);
								if(cappedPfAmt!=0){
									if(pfAmt>cappedPfAmt){
										double extraDiff=pfAmt-cappedPfAmt;
//										employerCont.setCtcAmount(Math.round(employerCont.getCtcAmount()-extraDiff));
										
										double actualPfDeduction=Math.round(employerCont.getCtcAmount()-extraDiff);
										if(actualPfDeduction<0){
											actualPfDeduction=0;	
										}
										employerCont.setCtcAmount(Math.round(actualPfDeduction));
										
										
										logger.log(Level.SEVERE, "EMP PF 5 AMT : "+pfAmt+" CAPPED AMT : "+cappedPfAmt+" PF AMT : "+employerCont.getCtcAmount());
									}else{
										pfAmt+=employerCont.getCtcAmount();
										pfPercent=employerCont.getCtcCompPercent();
										pfFlag=true;
										logger.log(Level.SEVERE, "EMP PF 3 AMT : "+pfAmt+" CAPPED AMT : "+cappedPfAmt);
										if(cappedPfAmt!=0){
											if(pfAmt>cappedPfAmt){
												double extraDiff=pfAmt-cappedPfAmt;
												double actualPfDeduction=Math.round(employerCont.getCtcAmount()-extraDiff);
												if(actualPfDeduction<0){
													actualPfDeduction=0;	
												}
												employerCont.setCtcAmount(Math.round(actualPfDeduction));
												pfAmt=pfAmt-extraDiff;
												logger.log(Level.SEVERE, "EMP PF 4 AMT : "+pfAmt+" CAPPED AMT : "+cappedPfAmt+" PF AMT : "+employerCont.getCtcAmount());
											}
										}
									}
								}
								
							}else if(employerCont.getCtcCompShortName().equalsIgnoreCase("PF")){
								pfAmt+=employerCont.getCtcAmount();
								pfPercent=employerCont.getCtcCompPercent();
								pfFlag=true;
								logger.log(Level.SEVERE, "EMP PF 1 AMT : "+pfAmt);
							}
							
							//LWF Update Part
							if(lwfFlag&&employerCont.getCtcCompShortName().equalsIgnoreCase("LWF")||employerCont.getCtcCompShortName().equalsIgnoreCase("MLWF")){
//								entry.getValue().remove(i);
								employerDeductionList.remove(i);
								i--;
								logger.log(Level.SEVERE, "EMP LWF 2 AMT : "+lwfAmt);
							}else if(employerCont.getCtcCompShortName().equalsIgnoreCase("LWF")||employerCont.getCtcCompShortName().equalsIgnoreCase("MLWF")){
								lwfAmt+=employerCont.getCtcAmount();
								lwfFlag=true;
								logger.log(Level.SEVERE, "EMP LWF 1 AMT : "+lwfAmt);
							}
						}
					}
				}
			}
		}

		private void updatePaySlipDeductions(HashMap<String, ArrayList<PaySlip>> paySlipMap, double pfMaxFixedValue, String deductionTypeName, List<ProfessionalTax> ptaxDeduction, ArrayList<EmployeeInfo> empinfoarray, HashMap<String, ArrayList<CompanyPayrollRecord>> employerContributionMap, String month, HashMap<String, ArrayList<PaySlip>> processedPaySlipMap,List<LWF> lwfList) {
			// TODO Auto-generated method stub
			
			if(paySlipMap!=null&&paySlipMap.size()!=0){
				for(Map.Entry<String, ArrayList<PaySlip>> entry:paySlipMap.entrySet()){
					
					/**
					 * @author Anil @since 04-02-2021
					 */
					int employeeId=0;
					String state="";
					try{
						String array[] = entry.getKey().split("[$]");
						employeeId=Integer.parseInt(array[0]);
						state=array[1];
					}catch(Exception e){
						
					}
					
					EmployeeInfo employeeInfo=null;
					for(EmployeeInfo empInfo:empinfoarray){
						if(empInfo.getEmpCount()==employeeId){
							employeeInfo=empInfo;
							break;
						}
					}
					
					double ptAmt=0;
					double pfAmt=0;
					double esicAmt=0;
					double lwfAmt=0;
					
					double grossAmount=0;
					double esicGrossAmount=0;
					
					boolean lwfFlag=false;
					boolean ptFlag=false;
					boolean pfFlag=false;
					boolean esicFlag=false;
					boolean esicUpdateFlag=false;
					
					double pfPercent=0;
					double esicRangeAmt=0;
					
					if(deductionTypeName!=null&&!deductionTypeName.equals("")){
						esicGrossAmount=getEsicGrossAmt(entry.getValue(),deductionTypeName);
					}
					logger.log(Level.SEVERE, ":::: EMPLOYEE ID :::: "+entry.getKey() +" PAY SIZE : "+entry.getValue().size()+" EMP ID "+employeeId+" STATE "+state);
					
					/**
					 * @author Anil @since 04-02-2021
					 */
					LinkedList<PaySlip> paySlipList=new LinkedList<PaySlip>();
					if(processedPaySlipMap!=null&&processedPaySlipMap.size()>0){
						if(processedPaySlipMap.containsKey(entry.getKey())){
							paySlipList.addAll(processedPaySlipMap.get(entry.getKey()));
						}
					}
					paySlipList.addAll(entry.getValue());
					
					if(paySlipList.size()==1){
						continue;
					}
					
					
//					for(PaySlip paySlip:entry.getValue()){
					for(PaySlip paySlip:paySlipList){	
						logger.log(Level.SEVERE, ":::: PROJECT :::: "+paySlip.getProjectName()+" STATE : "+paySlip.getState());
						boolean updateFlag=false;
						grossAmount+=paySlip.getGrossEarningWithoutOT();
						
						for(int i=0;i<paySlip.getDeductionList().size();i++){
							CtcComponent deduction=paySlip.getDeductionList().get(i);
							
							//PT Update Part
							if(ptFlag&&deduction.getShortName().equalsIgnoreCase("PT")){
								double applicablePt=getApplicablePtAmount(ptaxDeduction,employeeInfo,grossAmount,month,paySlip.getState());
								logger.log(Level.SEVERE, "Applicable PT AMT : "+applicablePt);
								if(ptAmt==0){
									ptAmt+=deduction.getActualAmount();
									logger.log(Level.SEVERE, "PT : 2 , AMT : "+ptAmt);
								}else{
									ptAmt+=deduction.getActualAmount();
									if(ptAmt>applicablePt){
										double ptDiff=ptAmt-applicablePt;
										logger.log(Level.SEVERE, "PT : 3A , AMT : "+ptAmt+" DIFF : "+ptDiff);
										deduction.setActualAmount(Math.round(deduction.getActualAmount()-ptDiff));
										ptAmt=ptAmt-ptDiff;
										logger.log(Level.SEVERE, "PT : 3B AMT : "+ptAmt);
										logger.log(Level.SEVERE, "PT : 3 , AMT : "+deduction.getActualAmount());
										updateFlag=true;
									}
								}
							}else if(deduction.getShortName().equalsIgnoreCase("PT")){
								ptAmt+=deduction.getActualAmount();
								logger.log(Level.SEVERE, "PT : 1 , AMT : "+ptAmt);
								ptFlag=true;
							}
							
							
							//PF Update Part
							if(pfFlag&&deduction.getShortName().equalsIgnoreCase("PF")){
								double cappedPfAmt=0;
								if(pfMaxFixedValue!=0){
									if(pfPercent!=0){
										cappedPfAmt=(pfMaxFixedValue*pfPercent)/100;
									}else{
										cappedPfAmt=(pfMaxFixedValue*12)/100;
									}
								}
								if(cappedPfAmt!=0){
									pfAmt+=deduction.getActualAmount();
									if(pfAmt>cappedPfAmt){
										double extraDiff=pfAmt-cappedPfAmt;
										double actualPfDeduction=Math.round(deduction.getActualAmount()-extraDiff);
										if(actualPfDeduction<0){
											actualPfDeduction=0;	
										}
										deduction.setActualAmount(Math.round(actualPfDeduction));
										updateFlag=true;
										logger.log(Level.SEVERE, "PF : 4 , AMT : "+pfAmt+" % : "+pfPercent+" Capped Amt : "+cappedPfAmt+" PF AMT : "+deduction.getActualAmount());
									}else{
										pfPercent=deduction.getMaxPerOfCTC();
										pfFlag=true;	
										logger.log(Level.SEVERE, "PF : 2 , AMT : "+pfAmt+" % : "+pfPercent);
										if(cappedPfAmt!=0){
											if(pfAmt>cappedPfAmt){
												double extraDiff=pfAmt-cappedPfAmt;
												double actualPfDeduction=Math.round(deduction.getActualAmount()-extraDiff);
												if(actualPfDeduction<0){
													actualPfDeduction=0;	
												}
												deduction.setActualAmount(Math.round(actualPfDeduction));
												pfAmt=pfAmt-extraDiff;
												logger.log(Level.SEVERE, "PF : 3B AMT : "+pfAmt);
												updateFlag=true;
												logger.log(Level.SEVERE, "PF : 3 , AMT : "+pfAmt+" % : "+pfPercent+" Capped Amt : "+cappedPfAmt+" PF AMT : "+deduction.getActualAmount());
											}
										}
									}
								}
								
							}else if(deduction.getShortName().equalsIgnoreCase("PF")){
								pfAmt+=deduction.getActualAmount();
								pfPercent=deduction.getMaxPerOfCTC();
								pfFlag=true;
								logger.log(Level.SEVERE, "PF : 1 , AMT : "+pfAmt+" % : "+pfPercent);
							}
							
							
							//Esic Update Part
							if(esicFlag&&deduction.getShortName().equalsIgnoreCase("ESIC")){
								if(esicRangeAmt!=0){
									if(esicGrossAmount!=0){
										if(esicGrossAmount>esicRangeAmt){
											logger.log(Level.SEVERE, "ESIC : 2 , AMT RANGE : "+esicRangeAmt+" GROSS : "+esicGrossAmount);
											esicUpdateFlag=true;
											updateEmployerDeduction(employerContributionMap,pfMaxFixedValue,deductionTypeName,empinfoarray,employeeId,paySlip.getProjectName(),true,null);
										}
										
									}else{
										if(grossAmount>esicRangeAmt){
											logger.log(Level.SEVERE, "ESIC : 3 , AMT RANGE : "+esicRangeAmt+" GROSS : "+grossAmount);
											esicUpdateFlag=true;
											updateEmployerDeduction(employerContributionMap,pfMaxFixedValue,deductionTypeName,empinfoarray,employeeId,paySlip.getProjectName(),true,null);
										}
									}
								}
								
							}else if(deduction.getShortName().equalsIgnoreCase("ESIC")){
								esicAmt+=deduction.getActualAmount();
								if(deduction.getConditionValue()!=null){
									esicRangeAmt=deduction.getConditionValue();
									logger.log(Level.SEVERE, "ESIC : 1 , AMT RANGE : "+esicRangeAmt);
								}
								esicFlag=true;
							}
							
							
							//LWF Update Part
//							if(lwfFlag&&deduction.getShortName().equalsIgnoreCase("LWF")||deduction.getShortName().equalsIgnoreCase("MLWF")){
//								paySlip.getDeductionList().remove(i);
//								i--;
//								updateFlag=true;
//								logger.log(Level.SEVERE, "LWF : 2 , AMT RANGE : "+lwfAmt);
////								lwfFlag=false;
//							}else if(deduction.getShortName().equalsIgnoreCase("LWF")||deduction.getShortName().equalsIgnoreCase("MLWF")){
//								lwfAmt+=deduction.getActualAmount();
//								logger.log(Level.SEVERE, "LWF : 1 , AMT RANGE : "+lwfAmt);
//								lwfFlag=true;
//							}
							
							
							if(lwfFlag&&deduction.getShortName().equalsIgnoreCase("LWF")||deduction.getShortName().equalsIgnoreCase("MLWF")){
								double applicableLwf=getApplicableLwfAmount(lwfList,employeeInfo,grossAmount,month,paySlip.getState());
								logger.log(Level.SEVERE, "Applicable LWF AMT : "+applicableLwf);
								if(lwfAmt==0){
									lwfAmt+=deduction.getActualAmount();
									logger.log(Level.SEVERE, "LWF : 2 , AMT : "+lwfAmt);
								}else{
									lwfAmt+=deduction.getActualAmount();
									if(lwfAmt>applicableLwf){
										double lwfDiff=lwfAmt-applicableLwf;
										logger.log(Level.SEVERE, "LWF : 3A , AMT : "+lwfAmt+" DIFF : "+lwfDiff);
										deduction.setActualAmount(Math.round(deduction.getActualAmount()-lwfDiff));
										lwfAmt=lwfAmt-lwfDiff;
										logger.log(Level.SEVERE, "LWF : 3B AMT : "+lwfAmt);
										logger.log(Level.SEVERE, "LWF : 3 , AMT : "+deduction.getActualAmount());
										updateFlag=true;
									}
								}
							}else if(deduction.getShortName().equalsIgnoreCase("LWF")||deduction.getShortName().equalsIgnoreCase("MLWF")){
								lwfAmt+=deduction.getActualAmount();
								logger.log(Level.SEVERE, "LWF : 1 , AMT : "+lwfAmt);
								lwfFlag=true;
							}
						}
						
						if(updateFlag){
							double totalDeduction=0;
							for(CtcComponent comp:paySlip.getDeductionList()){
								totalDeduction+=comp.getActualAmount();
							}
							paySlip.setTotalDeduction(totalDeduction);
							paySlip.setNetEarning(paySlip.getGrossEarning()-totalDeduction);
							paySlip.setNetEarningWithoutOT(paySlip.getGrossEarningActualWithoutOT()-totalDeduction);
						}
						
						if(esicUpdateFlag){
							for(PaySlip pay:entry.getValue()){
								boolean updateFlag1=false;
								for(int i=0;i<pay.getDeductionList().size();i++){
									CtcComponent comp=pay.getDeductionList().get(i);
									if(comp.getShortName().equalsIgnoreCase("ESIC")){
										pay.getDeductionList().remove(i);
										i--;
										updateFlag=true;
									}
								}
								if(updateFlag1){
									double totalDeduction=0;
									for(CtcComponent comp:paySlip.getDeductionList()){
										totalDeduction+=comp.getActualAmount();
									}
									pay.setTotalDeduction(totalDeduction);
									pay.setNetEarning(paySlip.getGrossEarning()-totalDeduction);
									pay.setNetEarningWithoutOT(paySlip.getGrossEarningActualWithoutOT()-totalDeduction);
								}
							}
						}
					}
					
					double applicablePt=getApplicablePtAmount(ptaxDeduction,employeeInfo,grossAmount,month,state);
					logger.log(Level.SEVERE, "applicablePt : "+applicablePt+" "+ptFlag);
					if(!ptFlag){
						if(applicablePt!=0){
							CtcComponent comp=new CtcComponent();
							comp.setDeduction(true);
							comp.setName("PT");
							comp.setShortName("PT");
							comp.setActualAmount(Math.round(applicablePt));
							comp.setAmount(applicablePt);
							PaySlip payslip=entry.getValue().get(0);
							payslip.getDeductionList().add(comp);
							double totalDeduction=0;
							for(CtcComponent obj:payslip.getDeductionList()){
								totalDeduction+=obj.getActualAmount();
							}
							payslip.setTotalDeduction(totalDeduction);
							payslip.setNetEarning(payslip.getGrossEarning()-totalDeduction);
							payslip.setNetEarningWithoutOT(payslip.getGrossEarningActualWithoutOT()-totalDeduction);
						}
					}else if(ptAmt!=0){
						if(ptAmt<applicablePt){
							PaySlip payslip=entry.getValue().get(0);
							boolean flag=false;
							double diff=0;
							diff=applicablePt-ptAmt;
							for(CtcComponent obj:payslip.getDeductionList()){
								if(obj.getShortName().equalsIgnoreCase("PT")){
									flag=true;
									
									obj.setActualAmount(Math.round(obj.getActualAmount()+diff));
								}
							}
							if(!flag){
								CtcComponent comp=new CtcComponent();
								comp.setDeduction(true);
								comp.setName("PT");
								comp.setShortName("PT");
								comp.setActualAmount(Math.round(diff));
								comp.setAmount(applicablePt);
								payslip.getDeductionList().add(comp);
							}
							
							double totalDeduction=0;
							for(CtcComponent obj:payslip.getDeductionList()){
								totalDeduction+=obj.getActualAmount();
							}
							payslip.setTotalDeduction(totalDeduction);
							payslip.setNetEarning(payslip.getGrossEarning()-totalDeduction);
							payslip.setNetEarningWithoutOT(payslip.getGrossEarningActualWithoutOT()-totalDeduction);
						}
					}
					
					
					double applicableLwf=getApplicableLwfAmount(lwfList,employeeInfo,grossAmount,month,state);
					if(!lwfFlag){
						if(applicableLwf!=0){
							CtcComponent comp=new CtcComponent();
							comp.setDeduction(true);
							comp.setName("LWF");
							comp.setShortName("LWF");
							comp.setActualAmount(Math.round(applicableLwf));
							comp.setAmount(applicableLwf);
							PaySlip payslip=entry.getValue().get(0);
							payslip.getDeductionList().add(comp);
							double totalDeduction=0;
							for(CtcComponent obj:payslip.getDeductionList()){
								totalDeduction+=obj.getActualAmount();
							}
							payslip.setTotalDeduction(totalDeduction);
							payslip.setNetEarning(payslip.getGrossEarning()-totalDeduction);
							payslip.setNetEarningWithoutOT(payslip.getGrossEarningActualWithoutOT()-totalDeduction);
						}
					}else if(lwfAmt!=0){
						if(lwfAmt<applicableLwf){
							PaySlip payslip=entry.getValue().get(0);
							boolean flag=false;
							double diff=0;
							diff=applicableLwf-lwfAmt;
							for(CtcComponent obj:payslip.getDeductionList()){
								if(obj.getShortName().equalsIgnoreCase("LWF")||obj.getShortName().equalsIgnoreCase("MLWF")){
									flag=true;
									
									obj.setActualAmount(Math.round(obj.getActualAmount()+diff));
								}
							}
							if(!flag){
								CtcComponent comp=new CtcComponent();
								comp.setDeduction(true);
								comp.setName("LWF");
								comp.setShortName("LWF");
								comp.setActualAmount(Math.round(diff));
								comp.setAmount(applicableLwf);
								payslip.getDeductionList().add(comp);
							}
							
							double totalDeduction=0;
							for(CtcComponent obj:payslip.getDeductionList()){
								totalDeduction+=obj.getActualAmount();
							}
							payslip.setTotalDeduction(totalDeduction);
							payslip.setNetEarning(payslip.getGrossEarning()-totalDeduction);
							payslip.setNetEarningWithoutOT(payslip.getGrossEarningActualWithoutOT()-totalDeduction);
						}
					}
				}
			}
		}
		
		private double getApplicableLwfAmount(List<LWF> lwfList,EmployeeInfo employeeInfo, double grossAmount, String month,String state) {
			if(lwfList!=null){
				for(LWF lwf:lwfList){
					logger.log(Level.SEVERE, "lwf state : "+lwf.getState()+" PAY SLIP STATE : "+state+" Month "+month);
					if(state!=null&&lwf.getState().equalsIgnoreCase(state)){
						if(lwf.getFromAmt()!=0&&grossAmount>=lwf.getFromAmt()&&grossAmount<=lwf.getToAmt()){
							if(lwf.getLwfList()!=null&&lwf.getLwfList().size()>0){
								for(LWFBean bean:lwf.getLwfList()){
									logger.log(Level.SEVERE," Month "+bean.getDeductionMonth());
									if(bean.getDeductionMonth().equalsIgnoreCase(month)){
										return lwf.getEmployeeContribution();
									}
								}
							}
						}else if(lwf.getToAmt()==0&&grossAmount>=lwf.getToAmt()){
							if(lwf.getLwfList()!=null&&lwf.getLwfList().size()>0){
								for(LWFBean bean:lwf.getLwfList()){
									if(bean.getDeductionMonth().equalsIgnoreCase(month)){
										return lwf.getEmployeeContribution();
									}
								}
							}
						}
					}
				}
			}
			return 0;
		}

		private double getEsicGrossAmt(ArrayList<PaySlip> value, String deductionTypeName) {
			// TODO Auto-generated method stub
			double esicGrossAmt=0;
			
			for(PaySlip payslip:value){
				for(CtcComponent comp:payslip.getEarningList()){
					if(!comp.getName().equalsIgnoreCase(deductionTypeName)&&!comp.getShortName().equalsIgnoreCase(deductionTypeName)){
						esicGrossAmt+=comp.getActualAmount();
					}
				}
			}
			return esicGrossAmt;
		}

		public double getApplicablePtAmount(List<ProfessionalTax> ptaxDeduction, EmployeeInfo employeeInfo, double grossAmount, String month,String state){
			if(ptaxDeduction!=null){
				for(ProfessionalTax pt:ptaxDeduction){
					/**
					 * @author Anil @since 08-01-2021
					 * checked state for pT calculation
					 */
					if(state!=null&&pt.getState().equalsIgnoreCase(state)){
						logger.log(Level.SEVERE, "PT state , AMT : "+pt.getState()+" PAY SLIP STATE : "+state+" Month "+month+" spc month "+pt.getSpecificMonth());
						if(pt.getRangeToAmt()!=0&&grossAmount>=pt.getRangeFromAmt()&&grossAmount<=pt.getRangeToAmt()){
							if(pt.getGender().equals("All")){
								if(pt.isExceptionForSpecificMonth()){
									if(pt.getSpecificMonth().equalsIgnoreCase(month)){
										return pt.getSpecificMonthAmount();
									}else{
										return pt.getPtAmount();
									}
								}else{
									return pt.getPtAmount();
								}
							}else if(pt.getGender().equals(employeeInfo.getGender())){
								if(pt.isExceptionForSpecificMonth()){
									if(pt.getSpecificMonth().equalsIgnoreCase(month)){
										return pt.getSpecificMonthAmount();
									}else{
										return pt.getPtAmount();
									}
								}else{
									return pt.getPtAmount();
								}
							}
						}else if(pt.getRangeToAmt()==0&&grossAmount>=pt.getRangeFromAmt()){
							if(pt.getGender().equals("All")){
								if(pt.isExceptionForSpecificMonth()){
									if(pt.getSpecificMonth().equalsIgnoreCase(month)){
										return pt.getSpecificMonthAmount();
									}else{
										return pt.getPtAmount();
									}
								}else{
									return pt.getPtAmount();
								}
							}else if(pt.getGender().equals(employeeInfo.getGender())){
								if(pt.isExceptionForSpecificMonth()){
									if(pt.getSpecificMonth().equalsIgnoreCase(month)){
										return pt.getSpecificMonthAmount();
									}else{
										return pt.getPtAmount();
									}
								}else{
									return pt.getPtAmount();
								}
							}
						}
					}
				}
			}
			return 0;
		}

		private void updateDeductionList(CTC ctc, List<CtcComponent> deduction,
				List<CtcComponent> dedRecord, ArrayList<CtcComponent> dedList,
				List<LWF> lwfList, ProvidentFund pf2) {
			// TODO Auto-generated method stub
			if(ctc!=null)
			{
				/**
				 * Date : 12-05-2018 By  ANIL
				 */
				if(ctc.getDeduction()!=null){
					deduction=ctc.getDeduction();
					logger.log(Level.SEVERE,"DEDUCTION SIZE BF "+deduction.size()+" "+deduction );
					/**
					 * 
					 */
					if(lwfList!=null&&lwfList.size()!=0){
						logger.log(Level.SEVERE,"DEDUCTION SIZE BEFOR LWF "+deduction.size());
						for(int i=0;i<deduction.size();i++){
//						for(CtcComponent comp:deduction){
							CtcComponent comp=deduction.get(i);
							if(comp.getShortName().equalsIgnoreCase("LWF")
									||comp.getShortName().trim().equalsIgnoreCase("MLWF")
									||comp.getShortName().trim().equalsIgnoreCase("MWF")){
								deduction.remove(i);
								i--;
							}
							
//							/**
//							 * @author Anil , Date : 27-06-2019 
//							 * if master pf not null then we remove pf from deduction component
//							 */
//							 if(pf!=null&&(comp.getShortName().equalsIgnoreCase("PF")||comp.getName().contains("PF"))){
//								 deduction.remove(i);
//								 i--;
//							 }
					
						}
						logger.log(Level.SEVERE,"DEDUCTION SIZE After removing LWF "+deduction.size());
						ArrayList<CtcComponent> list=getEmployeeContributionLwfList(lwfList);
						deduction.addAll(list);
						logger.log(Level.SEVERE,"DEDUCTION SIZE After adding LWF "+deduction.size());
					}
					
					/**
					 * @author Anil , Date : 03-07-2019
					 */
					for(int i=0;i<deduction.size();i++){
						CtcComponent comp=deduction.get(i);
						/**
						 * @author Anil , Date : 27-06-2019 
						 * if master pf not null then we remove pf from deduction component
						 */
						 if(pf!=null&&(comp.getShortName().equalsIgnoreCase("PF")||comp.getName().contains("PF"))){
							 deduction.remove(i);
							 i--;
						 }
					}
					//komal
					dedList = getDedList(ctc.getDeduction());
					logger.log(Level.SEVERE,"DEDUCTION SIZE AF "+deduction.size()+" "+deduction);
				}
				if(ctc.getCompaniesContribution()!=null){
					
					dedRecord=ctc.getCompaniesContribution();
					
					/**
					 * 
					 */
					if(lwfList!=null&&lwfList.size()!=0){
						logger.log(Level.SEVERE,"EMPLOYER SIZE BEFOR LWF "+dedRecord.size());
						for(int i=0;i<dedRecord.size();i++){
//						for(CtcComponent comp:dedRecord){
							CtcComponent comp=dedRecord.get(i);
							if(comp.getShortName().equalsIgnoreCase("LWF")
									||comp.getShortName().trim().equalsIgnoreCase("MLWF")
									||comp.getShortName().trim().equalsIgnoreCase("MWF")){
								dedRecord.remove(i);
								i--;
							}
							
//							/**
//							 * @author Anil , Date : 27-06-2019 
//							 * if master pf not null then we remove pf from deduction component
//							 */
//							 if(pf!=null&&(comp.getShortName().equalsIgnoreCase("PF")||comp.getName().contains("PF"))){
//								 dedRecord.remove(i);
//								 i--;
//							 }
						}
						logger.log(Level.SEVERE,"EMPLOYER SIZE After removing LWF "+dedRecord.size());
						ArrayList<CtcComponent> list=getEmployerContributionLwfList(lwfList);
						dedRecord.addAll(list);
						logger.log(Level.SEVERE,"EMPLOYER SIZE After adding LWF "+dedRecord.size());
					}
					
					/**
					 * @author Anil , Date : 03-07-2019
					 */
					for(int i=0;i<dedRecord.size();i++){
						CtcComponent comp=dedRecord.get(i);
						/**
						 * @author Anil , Date : 27-06-2019 
						 * if master pf not null then we remove pf from deduction component
						 */
						 if(pf!=null&&(comp.getShortName().equalsIgnoreCase("PF")||comp.getName().contains("PF"))){
							 dedRecord.remove(i);
							 i--;
						 }
					}
				}
			}
			/**
			 * End
			 */
		}

		private void calculateArrears(
				ArrayList<CtcComponent> earningArrearsList,
				ArrayList<CtcComponent> deductionArrearsList,
				ArrayList<CtcComponent> bonusAndPaidLeaveArrearsList,long companyId, int count, Date date, String projectName) {
			// TODO Auto-generated method stub
			
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			ArrearsDetails arrearsDetails=ofy().load().type(ArrearsDetails.class).filter("companyId", companyId).filter("empId", count).first().now();
			if(arrearsDetails!=null){
				try {
					
					for(ArrearsInfoBean bean:arrearsDetails.getArrearsList()){
						
						Calendar frmCal = Calendar.getInstance(); 
						Date frmDt=isoFormat.parse(bean.getFromDate());
						frmCal.setTime(frmDt);
						frmCal.add(Calendar.DATE, 1);
						frmDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
						
						Calendar toCal = Calendar.getInstance(); 
						Date toDt=isoFormat.parse(bean.getToDate());
						toCal.setTime(toDt);
						toCal.add(Calendar.DATE, 1);
						toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
						
						Date startDate=null;
						Date endDate=null;
						startDate=DateUtility.getStartDateofMonth(frmDt);
						endDate=DateUtility.getEndDateofMonth(toDt);
						
						logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
						logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
						
//						if((startDate.after(date)||startDate.equals(date))
//								&&(endDate.before(date)||endDate.equals(date))){
						if((date.after(startDate)||date.equals(startDate))
								&&(date.before(endDate)||date.equals(endDate))){	
							/**
							 * Date : 31-12-2018 BY ANIL
							 * removing bonus and paid leave from arrears list
							 */
							System.out.println("inside if......");
							if(bean.getProjectName()!=null&&!bean.getProjectName().equals("")&&bean.getProjectName().equals(projectName)){
								if(bean.getOperation().equalsIgnoreCase("ADD")&&!bean.isBonusAndPaidLeaveRevised()){
									CtcComponent earnArrear=new CtcComponent();
									earnArrear.setName(bean.getHead());
									earnArrear.setShortName(bean.getHead());
									earnArrear.setActualAmount(Math.round(bean.getAmount()));
									earnArrear.setArrears(true);
									earnArrear.setArrearOf(bean.getEarningCompName());
									earningArrearsList.add(earnArrear);
								}
								if(bean.getOperation().equalsIgnoreCase("DEDUCT")){
									CtcComponent deducArrear=new CtcComponent();
									deducArrear.setName(bean.getHead());
									deducArrear.setShortName(bean.getHead());
									deducArrear.setActualAmount(Math.round(bean.getAmount()));
									deducArrear.setDeduction(true);
									deducArrear.setArrears(true);
									deducArrear.setArrearOf(bean.getEarningCompName());
									deductionArrearsList.add(deducArrear);
								}
								
								if(bean.getOperation().equalsIgnoreCase("ADD")&&bean.isBonusAndPaidLeaveRevised()){
									CtcComponent bonusArrear=new CtcComponent();
									bonusArrear.setName(bean.getHead());
									bonusArrear.setShortName(bean.getHead());
									bonusArrear.setActualAmount(Math.round(bean.getAmount()));
									bonusArrear.setArrears(true);
									bonusArrear.setArrearOf(bean.getEarningCompName());
									bonusAndPaidLeaveArrearsList.add(bonusArrear);
								}
							}else if(bean.getProjectName()==null||bean.getProjectName().equals("")){
								if(bean.getOperation().equalsIgnoreCase("ADD")&&!bean.isBonusAndPaidLeaveRevised()){
									CtcComponent earnArrear=new CtcComponent();
									earnArrear.setName(bean.getHead());
									earnArrear.setShortName(bean.getHead());
									earnArrear.setActualAmount(Math.round(bean.getAmount()));
									earnArrear.setArrears(true);
									earnArrear.setArrearOf(bean.getEarningCompName());
									earningArrearsList.add(earnArrear);
								}
								if(bean.getOperation().equalsIgnoreCase("DEDUCT")){
									CtcComponent deducArrear=new CtcComponent();
									deducArrear.setName(bean.getHead());
									deducArrear.setShortName(bean.getHead());
									deducArrear.setActualAmount(Math.round(bean.getAmount()));
									deducArrear.setDeduction(true);
									deducArrear.setArrears(true);
									deducArrear.setArrearOf(bean.getEarningCompName());
									deductionArrearsList.add(deducArrear);
								}
								
								if(bean.getOperation().equalsIgnoreCase("ADD")&&bean.isBonusAndPaidLeaveRevised()){
									CtcComponent bonusArrear=new CtcComponent();
									bonusArrear.setName(bean.getHead());
									bonusArrear.setShortName(bean.getHead());
									bonusArrear.setActualAmount(Math.round(bean.getAmount()));
									bonusArrear.setArrears(true);
									bonusArrear.setArrearOf(bean.getEarningCompName());
									bonusAndPaidLeaveArrearsList.add(bonusArrear);
								}
							}
						}
						
						
					}
					
					System.out.println("EARNING ARREARS LIST : "+earningArrearsList.size());
					System.out.println("DEDUCTION ARREARS LIST : "+deductionArrearsList.size());
					System.out.println("Bonus ARREARS LIST : "+bonusAndPaidLeaveArrearsList.size());
					
				}catch(Exception e){
					
				}
		}
		}

		/**
		 * 
		 * @param companyId
		 * @param year - financial year (2019-2020)
		 * @param paySlip - 
		 * @param ctc - Current CTC
		 * @param dedList - Updated deduction list derived from ctc deduction part(added LWF from master and removed PF and ESIC)
		 * @param info
		 * @param cal - Calendar with current date
		 * @param isProvisional
		 * @author Anil : 15-07-2019
		 * Made this part as separate method
		 * 
		 */
		private void calculateTds(Long companyId, String year, PaySlip paySlip, CTC ctc, ArrayList<CtcComponent> dedList, EmployeeInfo info, Calendar cal, boolean isProvisional) {
			// TODO Auto-generated method stub
			
			/** date 20.7.2018 added by komal for tax calculation**/
			logger.log(Level.SEVERE ,"size : "+dedList.size());
			List<TaxSlab> taxSlabList = ofy().load().type(TaxSlab.class).filter("companyId", companyId).filter("year", year).list();
			
			double tds = 0;
			if(ctc.getCtcAmount() >= 250000){
				double consideredDeduction = calculateDeductions(ctc ,dedList , paySlip.getDeductionList() ,cal,true);
				double actualDeduction = calculateDeductions(ctc ,dedList , paySlip.getDeductionList() ,cal,false);

				logger.log(Level.SEVERE , "Considered deduction : "+consideredDeduction + "actual ded : "+actualDeduction);
				EmployeeInvestment emp = ofy().load().type(EmployeeInvestment.class).filter("companyId", info.getCompanyId()).filter("empId", info.getCount()).first().now();
				double inv = calculateInvestments(info, year, cal, emp);
				logger.log(Level.SEVERE ,"Employee Investment : "+inv);
				double calculatedTaxable =  calculateTaxableAmount(paySlip,ctc  ,cal ,consideredDeduction,inv,true);
				logger.log(Level.SEVERE ,"Taxable Amount : "+calculatedTaxable);
				double tdsValue = calculateTax(ctc ,calculatedTaxable , taxSlabList ,cal ,info);
				logger.log(Level.SEVERE ,"TDS Value : "+tdsValue);
				tds = calculateTDS(paySlip ,ctc , paySlip.getDeductionList() , info ,year ,cal,emp,taxSlabList,actualDeduction);
				logger.log(Level.SEVERE ,"TDS Amt : "+tdsValue);
				
				CtcComponent ctcComp = new CtcComponent(); 
				ctcComp.setName(AppConstants.TDS);
				ctcComp.setShortName(AppConstants.TDS);
				ctcComp.setAmount(tdsValue);
				ctcComp.setActualAmount(tds);
				paySlip.getDeductionList().add(ctcComp);
				paySlip.setNetEarning(Math.round(paySlip.getNetEarning() - tds));
				paySlip.setNetEarningWithoutOT(Math.round(paySlip.getNetEarningWithoutOT() - tds));
				paySlip.setTotalDeduction(Math.round(paySlip.getTotalDeduction() + tds));
			}
			double cumulativeCTC =ctc.getCumulativeCTC()+ paySlip.getGrossEarning();
			logger.log(Level.SEVERE , "cumulative ctc : "+cumulativeCTC + "  " +paySlip.getGrossEarning());
			ctc.setDeduction(dedList);
			ctc.setCumulativeCTC(cumulativeCTC);
			//ctc.setCumulativeDeduction(ctc.getCumulativeDeduction() + totalDeductions);
			ctc.setCumulativeTax(ctc.getCumulativeTax() + tds);
			/**
			 * Date : 24-11-2018 By ANIL
			 */
			if(!isProvisional){
				ofy().save().entity(ctc);
			}
		}
		/**
		 * @author Anil
		 * @since 01-09-2020
		 * @param siteLocation
		 * @return
		 */
		private HashMap<String, ArrayList<Attendance>> getSiteWiseAttendance(List<Attendance> attendanceList, HashSet<String> projectHs, boolean isReliever, boolean siteLocation) {
			// TODO Auto-generated method stub
			
			HashMap<String, ArrayList<Attendance>> innerSiteAttendanceMap=new HashMap<String, ArrayList<Attendance>>();
			
//			if(attendanceList!=null&&attendanceList.size()!=0){
			if(attendanceList!=null){
				logger.log(Level.SEVERE, "attendanceList!=null");
				if(attendanceList.size()>0) {
					logger.log(Level.SEVERE, "attendanceList.size()="+attendanceList.size());
				for(Attendance attendance:attendanceList){
					projectHs.add(attendance.getProjectName());
					String attendanceKey="";
					if(isReliever){
						attendanceKey=attendance.getEmpId()+"";
					}else{
						attendanceKey=attendance.getEmpId()+attendance.getProjectName();
					}
					/**
					 * @author Anil
					 * @since 01-09-2020
					 */
					if(siteLocation){
						attendanceKey=attendance.getEmpId()+attendance.getProjectName()+attendance.getSiteLocation();
					}
					
					if(innerSiteAttendanceMap!=null&&innerSiteAttendanceMap.size()!=0){
						if(innerSiteAttendanceMap.containsKey(attendanceKey)){
							innerSiteAttendanceMap.get(attendanceKey).add(attendance);
						}else{
							ArrayList<Attendance> attList=new ArrayList<Attendance>();
							attList.add(attendance);
							innerSiteAttendanceMap.put(attendanceKey, attList);
						}
					}else{
						ArrayList<Attendance> attList=new ArrayList<Attendance>();
						attList.add(attendance);
						innerSiteAttendanceMap.put(attendanceKey, attList);
					}
				}
				logger.log(Level.SEVERE, "innerSiteAttendanceMap : "+innerSiteAttendanceMap.size() +" Key : "+innerSiteAttendanceMap.keySet()+" Proj Size :: "+projectHs.size());
			}
			}
			
			return innerSiteAttendanceMap;
		}

		private int getActualAbsentDays(List<Attendance> attendanceList) {
			int absentCounter=0;
			for(Attendance attendance:attendanceList){
				if(attendance.getActionLabel().equalsIgnoreCase("A")||attendance.getActionLabel().equalsIgnoreCase("L")){
					absentCounter++;
				}
			}
			return absentCounter;
		}

		private ArrayList<AllocatedLeaves> getLeaveBalDetails(ArrayList<AllocatedLeaves> allocatedLeaves,ArrayList<AvailedLeaves> aviledLeaves, EmployeeInfo info) {
			// TODO Auto-generated method stub
			ArrayList<AllocatedLeaves> leaveBalList=new ArrayList<AllocatedLeaves>();
			for(AllocatedLeaves leaves:allocatedLeaves){
				AllocatedLeaves lev=new AllocatedLeaves();
				lev.setName(leaves.getName());
				lev.setShortName(leaves.getShortName());
				lev.setBalance(leaves.getBalance());
				lev.setOpeningBalance(leaves.getOpeningBalance());
				if(leaves.getMonthlyAllocation()==true){
					lev.setEarned(leaves.getBalance()-leaves.getOpeningBalance());
				}
				for(AvailedLeaves avail:aviledLeaves){
					if(avail.getLeaveName().equals(leaves.getName())){
						lev.setAvailedDays(avail.getLeavehour()/info.getLeaveCalendar().getWorkingHours());
						if(leaves.getMonthlyAllocation()==true){
							lev.setEarned((leaves.getBalance()+lev.getAvailedDays())-leaves.getOpeningBalance());
						}
						break;
					}
				}
				leaveBalList.add(lev);
			}
			return leaveBalList;
		}

		private boolean checkPlAndBonusStatus(List<Attendance> attendanceList) {
			// TODO Auto-generated method stub
			HashSet<String> hsProjectName=new HashSet<String>();
			
			for(Attendance attendance:attendanceList){
				hsProjectName.add(attendance.getProjectName());
			}
			logger.log(Level.SEVERE, " Project Size : "+hsProjectName.size());
			ArrayList<String> projectList=new ArrayList<String>(hsProjectName);
			
			if(projectList.size()!=0){
				List<ProjectAllocation> projects=ofy().load().type(ProjectAllocation.class).filter("companyId", attendanceList.get(0).getCompanyId()).filter("projectName IN", projectList).list();
				if(projects!=null){
					logger.log(Level.SEVERE, " Project Size 1 : "+projects.size());
					for(ProjectAllocation project:projects){
						if(project.isPaidLeaveAndBonusAsAdditionalAllowance()==true){
							return true;
						}
					}
				}
			}
			
			return false;
		}

		private int getNoOfPresentDays(List<Attendance> attendanceList) {
			// TODO Auto-generated method stub
			int presentDays=0;
			for(Attendance atten:attendanceList){
				if(atten.getActionLabel().trim().equalsIgnoreCase("P")){
					presentDays++;
				}
			}
			return presentDays;
		}

		private int getNoOfOtDays(List<Attendance> attendanceList) {
			// TODO Auto-generated method stub
			int otDays=0;
			for(Attendance atten:attendanceList){
				if(atten.isOvertime()){
					otDays++;
				}
			}
			return otDays;
		}

		private int getNoOfLeaveDays(List<Attendance> attendanceList, EmployeeInfo info) {
			// TODO Auto-generated method stub
			
			boolean woFlag=false;
			if(info.getLeaveCalendar().getWeeklyoff().isSUNDAY()==true){
				woFlag=true;
			}else if(info.getLeaveCalendar().getWeeklyoff().isMONDAY()==true){
				woFlag=true;
			}else if(info.getLeaveCalendar().getWeeklyoff().isTUESDAY()==true){
				woFlag=true;
			}else if(info.getLeaveCalendar().getWeeklyoff().isWEDNESDAY()==true){
				woFlag=true;
			}else if(info.getLeaveCalendar().getWeeklyoff().isTHRUSDAY()==true){
				woFlag=true;
			}else if(info.getLeaveCalendar().getWeeklyoff().isFRIDAY()==true){
				woFlag=true;
			}else if(info.getLeaveCalendar().getWeeklyoff().isSATAURDAY()==true){
				woFlag=true;
			}
			
			int leavesDays=0;
			for(Attendance atten:attendanceList){
				/**
				 * @author Anil , Date : 01-08-2019
				 */
				if(woFlag==false&&atten.isLeave()&&!atten.isWeeklyOff()&&!atten.getActionLabel().equalsIgnoreCase("H")&&!atten.getActionLabel().equalsIgnoreCase("PH")){
					leavesDays++;
				}else if(woFlag==true&&atten.isLeave()&&!atten.getActionLabel().equalsIgnoreCase("WO")&&!atten.getActionLabel().equalsIgnoreCase("H")&&!atten.getActionLabel().equalsIgnoreCase("PH")){
					leavesDays++;
				}
			}
			return leavesDays;
		}

		public int getTotalAbsentDays(List<Attendance> attendanceList) {
			// TODO Auto-generated method stub
			int absentCounter=0;
			for(Attendance attendance:attendanceList){
				if(attendance.getActionLabel().equalsIgnoreCase("NA")){
					absentCounter++;
				}
			}
			return absentCounter;
		}

		public int getNationalHolidayDetails(List<Attendance> attendanceList, EmployeeInfo info) {
			int extraDays=0;
			for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
				if(holiday.getHolidayType().trim().equalsIgnoreCase("National Holiday")){
					for(Attendance attendance:attendanceList){
						if(attendance.getAttendanceDate().equals(holiday.getHolidaydate())&&attendance.getWorkedHours()!=0){
							if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Direct")){
								extraDays+=extraDays+2;
							}else if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Indirect")){
								extraDays+=extraDays+1;
							}
						}
					}
				}
			}
			
			return extraDays;
		}
		
		public int getPublicHolidayDetails(List<Attendance> attendanceList, EmployeeInfo info) {
			int extraDays=0;
			for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
				if(holiday.getHolidayType().trim().equalsIgnoreCase("Public Holiday")){
					for(Attendance attendance:attendanceList){
						if(attendance.getAttendanceDate().equals(holiday.getHolidaydate())&&attendance.getWorkedHours()!=0){
							logger.log(Level.SEVERE ," getAttendanceDate "+attendance.getAttendanceDate()+" getHolidaydate "+holiday.getHolidaydate());
//							if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Direct")){
//								extraDays+=extraDays+2;
//							}else if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Indirect")){
								extraDays=extraDays+1;
//							}
								attendance.setHolidayLabel(null);
						}
					}
				}
			}
			
			logger.log(Level.SEVERE ," PH "+extraDays);
			
//			for(Attendance attendance:attendanceList){
//				if(attendance.getHolidayLabel()!=null&&attendance.getHolidayLabel().equals("PH")){
//					extraDays=extraDays+1;
//				}
//			}
			/**
			 * @author Vijay Date 08-03-2021
			 * Des :- As per rahul if public holiday label is PH and total workedhours >0 then it will consider into Public Holiday
			 * above old code commented
			 */
			for(Attendance attendance:attendanceList){
				if(attendance.getHolidayLabel()!=null&&attendance.getHolidayLabel().equals("PH") && attendance.getTotalWorkedHours()>0){
					extraDays=extraDays+1;
				}
			}
			
			logger.log(Level.SEVERE ," PH "+extraDays);
			return extraDays;
		}
		
		/**
		 * 
		 * @param companyId
		 * @param info
		 * @param paidDays
		 * @param unpaidDays
		 * @param paySlip 
		 * @return
		 * 
		 * This method allocate leave monthly.
		 */
		
		private LeaveBalance allocateMonthlyleaves(Long companyId,EmployeeInfo info,double paidDays,double unpaidDays, PaySlip paySlip,double monthlyDays){
			
			logger.log(Level.SEVERE,"INSIDE ALLOCATE LEAVE MONTHLY.........!!!!!");
			
			/**
			 * @author Anil,Date : 03-05-2019
			 * If paid leaves is  paid then we will not credit PL leave
			 * raised by Nitin Sir,for
			 */
			boolean validPL=true;
			
			if(paySlip.getPaidLeaves()!=0){
				validPL=false;
			}
			
			LeaveBalance levBal=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount", info.getEmpCount()).first().now();
			
			if(levBal!=null){
				for(int i=0;i<levBal.getAllocatedLeaves().size();i++){
					if(levBal.getAllocatedLeaves().get(i).getMonthlyAllocation()){
						
						if(levBal.getAllocatedLeaves().get(i).getShortName().equalsIgnoreCase("PL")&&!validPL){
							
							levBal.getAllocatedLeaves().get(i).setOpeningBalance(levBal.getAllocatedLeaves().get(i).getBalance());
							levBal.getAllocatedLeaves().get(i).setBalance(levBal.getAllocatedLeaves().get(i).getBalance());
							
							continue;
						}
						/**
						 *@author Anil,Date : 12-04-2019 
						 */
						double availedLeaves=0;
						for(AvailedLeaves avail:paySlip.getAviledLeaves()){
							if(avail.getLeaveName().equals(levBal.getAllocatedLeaves().get(i).getName())){
								availedLeaves=avail.getLeavehour()/info.getLeaveCalendar().getWorkingHours();
							}
						}
						/**
						 * 
						 */
						double validDays=monthlyDays;
//						double validDays=paidDays+unpaidDays;
						double oneDayLeave=levBal.getAllocatedLeaves().get(i).getMonthlyAllocationLeave()/validDays;
						double balanceLeave=oneDayLeave*paidDays;
						/**
						 * Date : 16-12-2018 By ANIL
						 * Setting Earned leave column
						 */
						double earnedLeave=balanceLeave;
						levBal.getAllocatedLeaves().get(i).setEarned(levBal.getAllocatedLeaves().get(i).getEarned()+earnedLeave);
						/**
						 * End
						 */
						/**
						 * @author Anil ,Date :08-03-2019 
						 * Setting closing leave balance while updating leave
						 */
						levBal.getAllocatedLeaves().get(i).setOpeningBalance(levBal.getAllocatedLeaves().get(i).getBalance()+availedLeaves);
						/**
						 * End
						 */
						balanceLeave+=levBal.getAllocatedLeaves().get(i).getBalance();
						levBal.getAllocatedLeaves().get(i).setBalance(balanceLeave);
						
					}
				}
			}
			return levBal;
		}

		/**
		 * Calculates Week Days in a given month for an Employee
		 * @param info Employee Info
		 * @param payRollPeriod
		 * @param timeZone
		 * @param payrollEndDate 
		 * @param payrollStartDate 
		 * @return
		 */
		public int calCulateWeekDay(EmployeeInfo info, String payRollPeriod,String timeZone, Date payrollStartDate, Date payrollEndDate) {
			com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal=info.getLeaveCalendar();
			Calendar c = Calendar.getInstance(); 
			logger.log(Level.SEVERE," :::Salary period CWDS :::  "+payRollPeriod);
			int sum=0;
			if(cal!=null)
			{
				SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
				isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
				try {
					Date d=isoFormat.parse(payRollPeriod);
					c.setTime(d);
					c.add(Calendar.DATE, 1);
					logger.log(Level.SEVERE," :::Salary period DATE CWDS :::  "+c.getTime());
//					d=c.getTime();
					d=DateUtility.getDateWithTimeZone("IST", c.getTime());
					Date startDate=new Date(d.getTime());
					Date endDate=new Date(d.getTime());
					
					if(payrollStartDate!=null&&payrollEndDate!=null){
						startDate=payrollStartDate;
						endDate=payrollEndDate;
					}else{
						startDate=DateUtility.getStartDateofMonth(d);
						endDate=DateUtility.getEndDateofMonth(d);
					}
					
					logger.log(Level.SEVERE," :::START DATE CWDS :::  "+startDate);
					logger.log(Level.SEVERE," :::START DATE DATE CWDS :::  "+endDate);
					
					c.setTime(startDate); 
					while(startDate.before(endDate)||startDate.equals(endDate))
					{
						if(cal.isWeekleyOff(startDate)){
							sum++;
						}
						c.add(Calendar.DATE, 1);
						startDate = c.getTime();
					}
				}
				catch(Exception e)
				{
				}
				
				return sum;
			}
			return 0;
		}
		
		public int calCulateHoliday(EmployeeInfo info, String payRollPeriod,String timeZone, Date payrollStartDate, Date payrollEndDate) {
			com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal=info.getLeaveCalendar();
			Calendar c = Calendar.getInstance(); 
			logger.log(Level.SEVERE," :::Salary period CHDS :::  "+payRollPeriod);
			int sum=0;
			if(cal!=null)
			{
				SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
				isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
				try {
					Date d=isoFormat.parse(payRollPeriod);
					c.setTime(d);
					c.add(Calendar.DATE, 1);
					d=DateUtility.getDateWithTimeZone("IST", c.getTime());
					logger.log(Level.SEVERE," :::Salary period DATE CHDS :::  "+c.getTime());
					Date startDate=new Date(d.getTime());
					Date endDate=new Date(d.getTime());
					
					if(payrollStartDate!=null&&payrollEndDate!=null){
						startDate=payrollStartDate;
						endDate=payrollEndDate;
					}else{
						startDate=DateUtility.getStartDateofMonth(d);
						endDate=DateUtility.getEndDateofMonth(d);
					}
					
					c.set(java.util.Calendar.HOUR_OF_DAY, 00);
					c.set(java.util.Calendar.MINUTE, 00);
					c.set(java.util.Calendar.SECOND, 00);
					c.set(java.util.Calendar.MILLISECOND, 00);
					
					logger.log(Level.SEVERE," :::START DATE CHDS :::  "+startDate);
					logger.log(Level.SEVERE," :::START DATE DATE CHDS :::  "+endDate);
					
					while(startDate.before(endDate)||startDate.equals(endDate))
					{
//						logger.log(Level.SEVERE," :::START DATE CHDS11111 :::  "+startDate);
						if(cal.isHolidayDate1(startDate))
						{
							sum++;
						}
						c.add(Calendar.DATE, 1);
						startDate = c.getTime();
					}
				}
				catch(Exception e)
				{
				}
				logger.log(Level.SEVERE,"HOLIDAY SUM ::: "+sum);
				logger.log(Level.SEVERE," :::HOLIDAY SUM :::   "+sum);
			return sum;
			}
			return 0;
		}
		
		public int daysInMonth(String payRollPeriod,String timeZone)
		{
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			Calendar c = Calendar.getInstance(); 
			isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			logger.log(Level.SEVERE," :::Salary period DIM :::  "+payRollPeriod);
			
			try {
				Date d=isoFormat.parse(payRollPeriod);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				Date startDate=new Date(d.getTime());
				Date endDate=new Date(d.getTime());
				startDate=DateUtility.getStartDateofMonth(d);
				logger.log(Level.SEVERE," :::START DATE DIM :::  "+startDate);
				logger.log(Level.SEVERE," :::Total Days DIM :::  "+c.getActualMaximum(Calendar.DAY_OF_MONTH));
				return c.getActualMaximum(Calendar.DAY_OF_MONTH);
			}
			catch(Exception e)
			{
			}
			return 0;
		}
		
		
		public double getUnpaidLeaveHrs(EmployeeInfo info, String payRollPeriod,String timeZone, Date attendanceStartDate, Date attendanceEndDate, String projectName) 
		{
			if(info.isReliever()){
				projectName="";
			}
			Calendar c = Calendar.getInstance(); 
		 	availedLeaves=new ArrayList<AvailedLeaves>();
			double leaveHrs=0d;
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			logger.log(Level.SEVERE," :::Salary period UPLH :::  "+payRollPeriod);
			try {
				Date d=isoFormat.parse(payRollPeriod);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				Date startDate=new Date(d.getTime());
				Date endDate=new Date(d.getTime());
				startDate=DateUtility.getStartDateofMonth(d);
				endDate=DateUtility.getEndDateofMonth(d);
				
				
				if(attendanceStartDate!=null){
					startDate=attendanceStartDate;
				}
				
				if(attendanceEndDate!=null){
					endDate=attendanceEndDate;
				}
				
				
				logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
				logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
				
				/**
				 * Date : 14-05-2018 By ANIL
				 * Retrieving employee overtime by emp id
				 */
				Key<EmployeeInfo>employeeKey=Key.create(info);
//				List<EmployeeLeave> leaves=ofy().load().type(EmployeeLeave.class).filter("leaveDate >=",startDate).filter("employeeKey", employeeKey).filter("companyId",info.getCompanyId()).list();
				List<EmployeeLeave> leaves=ofy().load().type(EmployeeLeave.class).filter("companyId",info.getCompanyId()).filter("emplId", info.getEmpCount()).filter("leaveDate >=",startDate).list();
//				logger.log(Level.SEVERE,"Total Leave SIZE --- ::: "+leaves.size());
//				logger.log(Level.SEVERE,"Total Leave SIZE --- ::: "+leaves.size());
				List<EmployeeLeave> validleaves=new ArrayList<EmployeeLeave>();
				
				for(EmployeeLeave temp:leaves)
				{
					if(temp.getLeaveDate().equals(endDate)||temp.getLeaveDate().before(endDate)){
						/**
						 * @author Anil , Date : 17-07-2019
						 */
						if(temp.getProjectName()!=null&&!temp.getProjectName().equals("")&&projectName!=null&&!projectName.equals("")){
							if(temp.getProjectName().equals(projectName)){
								validleaves.add(temp);
							}
						}else{
							validleaves.add(temp);	
						}
						
						
					}
				}
//				logger.log(Level.SEVERE,"valid Leaves SIZE --- ::: "+validleaves.size());
//				logger.log(Level.SEVERE,"valid Leaves SIZE --- ::: "+validleaves.size());
				
				/**
				 * Date : 15-12-2018 BY ANIL
				 * getting the count of total unpaid leave stored in employee master
				 */
				noOfLeaveDays=validleaves.size();
				for(EmployeeLeave temp:validleaves)
				{
					boolean flag=false;
					if(temp.getUnpaidLeave()==false){
						if(availedLeaves.size()==0){
							AvailedLeaves avLeav=new AvailedLeaves();
							avLeav.setLeaveName(temp.getLeaveTypeName());
							avLeav.setLeavehour(temp.getLeaveHrs());
							avLeav.setUnPaid(true);
							availedLeaves.add(avLeav);
						}else{
							for(int i=0;i<availedLeaves.size();i++){
								if(availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())){
//									logger.log(Level.SEVERE,"Inside Existingggggggg");
									flag=true;
									double hrs=0;
									hrs=availedLeaves.get(i).getLeavehour();
									hrs=hrs+temp.getLeaveHrs();
									availedLeaves.get(i).setLeavehour(hrs);
									availedLeaves.get(i).setUnPaid(true);
								}
							}
							if(flag==false){
//								logger.log(Level.SEVERE,"Inside Otherrrrrrrrrrr");
								AvailedLeaves avLeav=new AvailedLeaves();
								avLeav.setLeaveName(temp.getLeaveTypeName());
								avLeav.setLeavehour(temp.getLeaveHrs());
								avLeav.setUnPaid(true);
								availedLeaves.add(avLeav);
							}
						}
						
						
						leaveHrs=leaveHrs+temp.getLeaveHrs();
					}
					else{
						if(availedLeaves.size()==0){
							AvailedLeaves avLeav=new AvailedLeaves();
							avLeav.setLeaveName(temp.getLeaveTypeName());
							avLeav.setLeavehour(temp.getLeaveHrs());
							avLeav.setUnPaid(false);
							availedLeaves.add(avLeav);
						}else{
							
							for(int i=0;i<availedLeaves.size();i++){
								if(availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())){
									flag=true;
									double hrs=availedLeaves.get(i).getLeavehour();
									hrs=hrs+temp.getLeaveHrs();
									availedLeaves.get(i).setLeavehour(hrs);
									availedLeaves.get(i).setUnPaid(false);
								}
							}
							if(flag==false){
								AvailedLeaves avLeav=new AvailedLeaves();
								avLeav.setLeaveName(temp.getLeaveTypeName());
								avLeav.setLeavehour(temp.getLeaveHrs());
								avLeav.setUnPaid(false);
								availedLeaves.add(avLeav);
							}
						}
					}
				}
//				logger.log(Level.SEVERE,"Availed Leave List SIZE----- ::"+availedLeaves.size());
				for(AvailedLeaves leave:availedLeaves){
					logger.log(Level.SEVERE,"Leave Name : "+leave.getLeaveName()+" HOUR : "+leave.getLeavehour());
				}
				logger.log(Level.SEVERE,"NO OF LEAVE DAYS ----- :: "+noOfLeaveDays);
			 }
			 catch(Exception e)
			 {
			 }
			
			return leaveHrs;
			
		}
		
		//////////////////////////////////////*************************/////////////////////////////////////
		
		public double getOverTimeHrs(EmployeeInfo info, String payRollPeriod,String timeZone, Date attendanceStartDate, Date attendanceEndDate, String projectName) 
		{
			if(info.isReliever()){
				projectName="";
			}
			otList=new ArrayList<EmployeeOvertime>();
			Calendar c = Calendar.getInstance(); 
			double otHrs=0d;
			
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			
			logger.log(Level.SEVERE," :::Salary period OTH :::  "+payRollPeriod);
			try {
				Date d=isoFormat.parse(payRollPeriod);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				Date startDate=new Date(d.getTime());
				Date endDate=new Date(d.getTime());
				
				startDate=DateUtility.getStartDateofMonth(d);
				endDate=DateUtility.getEndDateofMonth(d);
				
				if(attendanceStartDate!=null){
					startDate=attendanceStartDate;
				}
				
				if(attendanceEndDate!=null){
					endDate=attendanceEndDate;
				}
				
				logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
				logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
				
				Key<EmployeeInfo>employeeKey=Key.create(info);
				/**
				 * Date : 14-05-2018 By ANIL
				 * Retrieving employee overtime by emp id
				 */
//				List<EmployeeOvertime> otList=ofy().load().type(EmployeeOvertime.class).filter("otDate >=",startDate).filter("employeeKey", employeeKey).filter("companyId",info.getCompanyId()).list();
//				List<EmployeeOvertime> otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",info.getCompanyId()).filter("emplId", info.getEmpCount()).filter("otDate >=",startDate).list();
				otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",info.getCompanyId()).filter("emplId", info.getEmpCount()).filter("otDate >=",startDate).filter("otDate <=",endDate).list();
//				logger.log(Level.SEVERE,"Total OT LIST SIZE --- ::: "+otList.size());
//				logger.log(Level.SEVERE,"Total OT LIST SIZE --- ::: "+otList.size());
				
				List<EmployeeOvertime> validotList=new ArrayList<EmployeeOvertime>();
				
				for(EmployeeOvertime temp:otList)
				{
					if(temp.getOtDate().equals(endDate)||temp.getOtDate().before(endDate)){
						/**
						 * @author Anil , Date : 17-07-2019
						 */
						if(temp.getProjectName()!=null&&!temp.getProjectName().equals("")&&projectName!=null&&!projectName.equals("")){
							if(projectName.equals(temp.getProjectName())){
								validotList.add(temp);
							}
						}else{
							validotList.add(temp);
						}
					}
				}
				/**
				 * Date : 15-12-2018 BY ANIL
				 * getting the no. of ot counts stored in employee ot master
				 */
				noOfOtDays=validotList.size();
//				logger.log(Level.SEVERE,"valid OT LIST SIZE --- ::: "+validotList.size());
				logger.log(Level.SEVERE,"NO OF OT DAYS --- ::: "+noOfOtDays);
				
				for(EmployeeOvertime temp:validotList)
				{
					/**
					 * Date : 30-08- 2018 By ANIL
					 */
					if(nhOtFlag){
						boolean isHoliday = info.getLeaveCalendar().isHolidayDate(temp.getOtDate());
						if(isHoliday){
							for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
								if(holiday.getHolidaydate().equals(temp.getOtDate())
										&&holiday.getHolidayType().equalsIgnoreCase("National Holiday")){
									temp.setOtHrs(0);
								}
							}
						}
					}
					/**
					 * End
					 */
					otHrs=otHrs+temp.getOtHrs();
				}
			 }
			 catch(Exception e)
			 {
			 }
			
			return otHrs;
			
		}
		
		private double getOverTimeAmount(EmployeeInfo info,String payRollPeriod,String timeZone, double overtimeHrs,CTC ctc,int monthlyDays, List<EmployeeOvertime> otList, Date attendanceStartDate, Date attendanceEndDate, String projectName) {
			empOtHisList=new ArrayList<EmployeeOvertimeHistory>();
			double otAmount=0;
			
			if(overtimeHrs==0){
				return 0;
			}
			
			/**
			 * Date : 05-06-2018 BY ANIL
			 */
			for(EmployeeOvertime empOt:otList){
	//			Overtime ovrtym=info.getOvertime();
				/**
				 * Date : 30-08- 2018 By ANIL
				 */
				if(nhOtFlag){
					boolean isHoliday = info.getLeaveCalendar().isHolidayDate(empOt.getOtDate());
					if(isHoliday){
						for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
							if(holiday.getHolidaydate().equals(empOt.getOtDate())
									&&holiday.getHolidayType().equalsIgnoreCase("National Holiday")){
								empOt.setOtHrs(0);
							}
						}
					}
				}
				/**
				 * End
				 */
				Overtime ovrtym=empOt.getOvertime();
				
				/**
				 * @author Anil , Date : 12-06-2019
				 * this variable stores ot amt for storing bifurcation history
				 */
				double otAmt=0;
				if(ovrtym.isHourly()){
					otAmt=getOtAmtByHourly(ovrtym.getHourlyRate(),empOt.getOtHrs());
					otAmount+=otAmt;
					/**
					 * @author Anil , Date : 12-06-2019
					 */
					captureOtBifurcation(empOt,otAmt);
				}
				
				if(ovrtym.isFlat()){
					/**
					 * @author Anil @since 02-04-2021
					 * updated flat ot calculation logic
					 * raised by Rahul Tiwari for Alkosh
					 */
//					otAmt=getOtAmtByFlateRate(ovrtym.getFlatRate(),payRollPeriod,timeZone,info);
					otAmt=ovrtym.getFlatRate();
					otAmount+=otAmt;
					
					/**
					 * @author Anil , Date : 12-06-2019
					 */
					captureOtBifurcation(empOt,otAmt);
				}
				
				if(ovrtym.getPercentOfCTC()!=0){
					otAmt=getOtAmtByCTCPercent(ovrtym.getPercentOfCTC(),empOt.getOtHrs(),ctc,monthlyDays,info.getLeaveCalendar().getWorkingHours());
					otAmount+=otAmt;
					/**
					 * @author Anil , Date : 12-06-2019
					 */
					captureOtBifurcation(empOt,otAmt);
				}
			}
			
			return Math.round(otAmount);
		}
		
		private void captureOtBifurcation(EmployeeOvertime empOt, double otAmt) {
			// TODO Auto-generated method stub
			if(empOtHisList!=null&&empOtHisList.size()!=0){
				boolean otAddedFlag=false;
				for(EmployeeOvertimeHistory otHis:empOtHisList){
					if(otHis.getOtId()==empOt.getOvertime().getCount()){
						otAddedFlag=true;
						otHis.setOtHours(otHis.getOtHours()+empOt.getOtHrs());
						otHis.setOtAmount(otHis.getOtAmount()+otAmt);
					}
				}
				if(otAddedFlag==false){
					EmployeeOvertimeHistory obj= new EmployeeOvertimeHistory();
					obj.setOtId(empOt.getOvertime().getCount());
					obj.setOtName(empOt.getOvertime().getName());
					obj.setOtHours(empOt.getOtHrs());
					obj.setOtAmount(otAmt);
					obj.setPayOtAsOther(empOt.getOvertime().isPayOtAsOther());
					obj.setCorrespondanceName(empOt.getOvertime().getOtCorrespondanceName());
					empOtHisList.add(obj);
				}
				
				
			}else{
				EmployeeOvertimeHistory obj= new EmployeeOvertimeHistory();
				obj.setOtId(empOt.getOvertime().getCount());
				obj.setOtName(empOt.getOvertime().getName());
				obj.setOtHours(empOt.getOtHrs());
				obj.setOtAmount(otAmt);
				obj.setPayOtAsOther(empOt.getOvertime().isPayOtAsOther());
				obj.setCorrespondanceName(empOt.getOvertime().getOtCorrespondanceName());
				empOtHisList.add(obj);
			}
		}

		private double getOtAmtByCTCPercent(double percent,double hrs,CTC ctc,int monthlyDays,double wrkingHrs) {
			double amt=0;
			
			double ctcAmount=ctc.getCtcAmount();
			double monthlySal=ctcAmount/12;
			double oneDaySal=monthlySal/monthlyDays;
			
			double hrsSal=oneDaySal/wrkingHrs;
			
			double hrsRate=hrsSal*percent/100;
			
			amt=hrs*hrsRate;
			
			return amt;
		}

		private double getOtAmtByFlateRate(double flateRate,String payRollPeriod,String timeZone,EmployeeInfo info) {
			double amt=0;
			int totalDays=0;
			
			Calendar c = Calendar.getInstance(); 
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
			
			try {
				Date d=isoFormat.parse(payRollPeriod);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				Date startDate=new Date(d.getTime());
				Date endDate=new Date(d.getTime());
				
				startDate=DateUtility.getStartDateofMonth(d);
				endDate=DateUtility.getEndDateofMonth(d);
				
				Key<EmployeeInfo>employeeKey=Key.create(info);
				
				List<EmployeeOvertime> otList=ofy().load().type(EmployeeOvertime.class).filter("otDate >=",startDate).filter("employeeKey", employeeKey).filter("companyId",info.getCompanyId()).list();
				
				List<EmployeeOvertime> validotList=new ArrayList<EmployeeOvertime>();
				
				for(EmployeeOvertime temp:otList)
				{
					if(temp.getOtDate().equals(endDate)||temp.getOtDate().before(endDate)){
						validotList.add(temp);
					}
				}
				totalDays=validotList.size();
				amt=totalDays*flateRate;
			}
			catch(Exception e)
			{
			}
			
			return amt;
		}

		private double getOtAmtByHourly(double hourlyRate,double hrs) {
			double amt=0;
			amt=Math.round(hrs*hourlyRate);
			return amt;
		}

		private double getEarnedLeaveFrmOverTymHrs(EmployeeInfo info, double overtimeHrs) {
			logger.log(Level.SEVERE,"Inside EARNED LEAVE METHOD ");
			double earnedleave=0;
			Overtime ovrTym=info.getOvertime();
			
			if(overtimeHrs==0){
				return 0;
			}
			if(ovrTym.isLeave()){
				overtimeHrs=overtimeHrs*ovrTym.getLeaveMultiplier();
				earnedleave=overtimeHrs/info.getLeaveCalendar().getWorkingHours();
			}
			
			logger.log(Level.SEVERE,"earned Leave ::: "+earnedleave);
			logger.log(Level.SEVERE,"IS Leave ::: "+ovrTym.isLeave());
			
			if(ovrTym.isLeave()){
				LeaveBalance levBal=ofy().load().type(LeaveBalance.class).filter("companyId",info.getCompanyId()).filter("empInfo.empCount", info.getEmpCount()).first().now();
				
				if(levBal!=null){
					logger.log(Level.SEVERE,"IS Leave BAL  ::: "+levBal.getEmpCount());
					for(int i=0;i<levBal.getLeaveGroup().getAllocatedLeaves().size();i++){
						if(ovrTym.getLeaveName().trim().equals(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim())){
							
							double balEarned=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getEarned();
							balEarned=balEarned+earnedleave;
							levBal.getLeaveGroup().getAllocatedLeaves().get(i).setEarned(balEarned);
							
							double balLeave=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getBalance();
							logger.log(Level.SEVERE,"Leave ::: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim()+" BAL "+balLeave);
							
							balLeave=balLeave+earnedleave;
							logger.log(Level.SEVERE,"BAL--- "+balLeave);
							levBal.getLeaveGroup().getAllocatedLeaves().get(i).setBalance(balLeave);
						}
					}
				}
				ofy().save().entity(levBal).now();
			}
			
			return earnedleave;
		}

		//////////////////////////////////////*************************/////////////////////////////////////

		/*************************************************************************************************************/

	/**
	    * Gets the Employees whose Salary Slip has not been allocated at this month.
	    * To Do : Right now implementation is ugly as we create key from Employee Info and than
	    * from that key search Pay Slip Record helper 
	    */
		public ArrayList<AllocationResult> getAllocationStatus(ArrayList<Filter> filter,String salaryPeriod,Date frmDate,Date toDate) 
		{
			GenricServiceImpl impl=new GenricServiceImpl();
			MyQuerry querry=new MyQuerry();
			querry.getFilters().addAll(filter);
			querry.setQuerryObject(new EmployeeInfo());
			ArrayList<SuperModel>employees=impl.getSearchResult(querry);
			
			List<Key<EmployeeInfo>>employeeKey=new ArrayList<Key<EmployeeInfo>>();
			
			for(SuperModel temp:employees)
			{
				EmployeeInfo info=(EmployeeInfo) temp;
				Key<EmployeeInfo>empkeys=Key.create(info);
				employeeKey.add(empkeys);
			}
			
			ArrayList<AllocationResult>allocationStatus=new ArrayList<AllocationResult>();
			
			try{
				List<PaySlipRecord>record=ofy().load().type(PaySlipRecord.class).filter("employeeKey in ",employeeKey).filter("salaryPeriod",salaryPeriod).list();
				
				for(PaySlipRecord temp:record)
				{
					Key<EmployeeInfo>empKey=temp.getEmployeeKey();
					for(SuperModel model:employees)
					{
						EmployeeInfo info=(EmployeeInfo) model;
						Key<EmployeeInfo>ekey=Key.create(info);
						if(ekey.equals(empKey))
						{
							AllocationResult results=new AllocationResult();
							results.setInfo(info);
							results.setStatus(true);
							allocationStatus.add(results);
							employees.remove(model);
							break;
						}
					}
				}
			}catch(Exception e){
				
			}
			
			ArrayList<Integer> employeeIdList2=new ArrayList<Integer>();
			for(SuperModel model:employees)
			{
				EmployeeInfo temp=(EmployeeInfo) model;
				AllocationResult results=new AllocationResult();
				results.setInfo(temp);
				results.setStatus(false);
				allocationStatus.add(results);
				
				employeeIdList2.add(temp.getEmpCount());
			}
			
			/**
			 * @author Anil @since 03-02-2021
			 * if site location process configuration is active then we will add filter 
			 * on employee id instead of project name
			 * this we are doing for payroll processing of employee worked on multiple sites
			 * Raised by Rahul Tiwari for SUNRISE 
			 */
			
			logger.log(Level.SEVERE,"SITELOCATION PROCESS ACTIVE");
			String projectName="";
			long companyId=0;
			for(Filter temp:filter){
				if(temp.getQuerryString().equals("projectName")){
					if(projectName!=null){
						projectName=temp.getStringValue();
					}
				}
				if(temp.getQuerryString().equals("empCount")){
					projectName=null;
				}
				if(temp.getQuerryString().equals("companyId")){
					companyId=temp.getLongValue();
				}
			}
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "EnableSiteLocation", companyId)){	
				logger.log(Level.SEVERE,"projectName "+projectName);
				if(projectName!=null&&!projectName.equals("")){
					HrProject hrProject=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", projectName).first().now();
					HashSet<Integer> hsEmpId=new HashSet<Integer>();
					if(hrProject!=null){
						
						/**
						 * @author Vijay Date :- 08-09-2022
						 * Des :- used common method to read OT from project or from entity HrProjectOvertime`
						 */
						ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
						CommonServiceImpl commonservice = new CommonServiceImpl();
						overtimelist = commonservice.getHRProjectOvertimelist(hrProject);
						/**
						 * ends here
						 */
						
//						if(hrProject.getOtList()!=null&&hrProject.getOtList().size()!=0){
						if(overtimelist!=null&&overtimelist.size()!=0){
							for(Overtime ot:overtimelist){
								hsEmpId.add(ot.getEmpId());
							}
							
							if(hsEmpId.size()>0){
								
								ArrayList<Integer> employeeIdList1=new ArrayList<Integer>(hsEmpId);	
								
								logger.log(Level.SEVERE,"employeeIdList1 "+employeeIdList1);
								logger.log(Level.SEVERE,"employeeIdList2 "+employeeIdList2);
								
								if(employeeIdList2.size()>0){
									employeeIdList1.removeAll(employeeIdList2);
								}
								
								logger.log(Level.SEVERE,"employeeIdList1 "+employeeIdList1);
								
								if(employeeIdList1.size()>0){
									List<EmployeeInfo> employeeInfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", employeeIdList1).list();
									if(employeeInfoList!=null&&employeeInfoList.size()>0){
										logger.log(Level.SEVERE,"employeeInfoList "+employeeInfoList.size());
										for(EmployeeInfo info:employeeInfoList){
											AllocationResult results=new AllocationResult();
											results.setInfo(info);
											results.setStatus(false);
											allocationStatus.add(results);
										}
									}
								}
							}
						}
					}		
							
				}
			}
			
			return allocationStatus;
		}
		
		/**
		 * 
		 * @param info Employee Info Object For which Pay Slip Exists or not is to be checked.
		 * @param salPeriod Salary Period.
		 * @return true if Salary Slip is created for that Period , false if not.
		 */
		protected boolean isPaySlipCreated(EmployeeInfo info,String salPeriod)
		{
			Key<EmployeeInfo>key=Key.create(info);
			int count=0;
			if(key!=null){
				count=ofy().load().type(PaySlipRecord.class).filter("salaryPeriod", salPeriod).filter("employeeKey",key).count();
			}
			
			if(count==0){
				return false;
			}
			else{
				return true;
			}
		}
		
		protected List<PaySlip> isPaySlipCreated1(EmployeeInfo info,String salPeriod){
			List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("salaryPeriod", salPeriod).filter("empid",info.getEmpCount()).list();
			return paySlipList;
		}
		/**
		 * Method retrives the Ctc for corresponding Employee.
		 * @param employeeId id of the Employee for which Ctc is being recieved
		 * @param companyId companyId
		 * @return {@link CTC} object corresponding to that Employee.Will return null if No Valid CTC Exists
		 */
		protected CTC retriveCtc(int employeeId,long companyId,String calendarName)
		{
			logger.log(Level.SEVERE,"AAAAAAAAAAAA");
		
			CTC ctc=ofy().load().type(CTC.class).filter("empid",employeeId).filter("companyId",companyId).filter("calendarName", calendarName).first().now();
			if(ctc!=null){
				return ctc;
			}
			else{
				return null;
			}
		}
		
		
		/**
		 * @param deductionTypeName 
		 * @param siteWiseAttendanceMap 
		 * @param isSiteWasPayroll 
		 * @param employerContributionMap 
		 * @param projectName ******************************************************************************************************/
		
		
		
		private List<CompanyPayrollRecord> calculateCompanyContribution(List<CtcComponent> deductionList,int monthlyDays,double paidDays,CTC ctc,int empId,String empName,String payRollPeriod, double pfMaxFixedValue, PaySlip paySlip, String deductionTypeName, boolean isSiteWasPayroll, HashMap<String, ArrayList<Attendance>> siteWiseAttendanceMap, HashMap<String, ArrayList<CompanyPayrollRecord>> employerContributionMap, String projectName,EmployeeInfo info,boolean siteLocation,String siteLocationName,String state) {
			logger.log(Level.SEVERE,"");
			logger.log(Level.SEVERE,"");
			logger.log(Level.SEVERE,"INSIDE COMPANY PAYROLL RECORD "+deductionList.size());
			List<CompanyPayrollRecord>compPayRecList=new ArrayList<CompanyPayrollRecord>();
			
			try {
				
			String ctcComponentName = null;
			double ctcAmount = 0;
			
			/**
			 * Date : 28-08-2018 By ANIL
			 */
			boolean pfFlag=false;
			double pfArrears=0;
			pfArrears=paySlip.getPFArrearsAmount();
			
			double otherArrears=0;
			otherArrears=paySlip.getOtherArrearsAmount();
			
			for (CtcComponent deductions : deductionList) {
				/**
				 * @author Anil , Date : 01-07-2019
				 * Capturing base amount on which deduction is applicable
				 */
				double baseAmount=0;
				
				double dedAmount = 0;
				double actualSumDeadAmount = 0;
	
				if (deductions.getCtcComponentName() != null&& !deductions.getCtcComponentName().equals("")
						&& deductions.getMaxPerOfCTC() != null&& deductions.getMaxPerOfCTC() != 0
						&& (deductions.getMaxAmount() == null || deductions.getMaxAmount() == 0)
						&& deductions.getAmount() != 0) {
					
					logger.log(Level.SEVERE, "INSIDE COMPANY PAYROLL % NOT NULL ");
					
					double dedutionAmountMonthly = 0;
					dedutionAmountMonthly = getCtcComponentAmount(deductions.getCtcComponentName(), ctc.getEarning(),monthlyDays, paidDays);
	
					if (deductions.getCondition() != null&& !deductions.getCondition().equals("")) {
	
						logger.log(Level.SEVERE,"INSIDE COMPANY PAYROLL CONDITION NOT NULL ");
	
						/**
						 * Date : 14-05-2018 By ANIL Earlier we were putting annual
						 * value in conditional value field ,now we are storing
						 * monthly value in it.
						 */
	
						int applicableAmtMonthly = deductions.getConditionValue();
						double applicableAmountAnnualy = applicableAmtMonthly * 12;
						
	
						if(deductions.getShortName().equalsIgnoreCase("ESIC")){
							dedutionAmountMonthly=dedutionAmountMonthly-getESICComponentAmount(deductionTypeName, ctc.getEarning(),monthlyDays, paidDays);;
						}
						
						logger.log(Level.SEVERE, "APPLICABLE AMOUNT ANNUALY :: "+ applicableAmountAnnualy);
						logger.log(Level.SEVERE, "APPLICABLE AMOUNT MONTHLY :: "+ applicableAmtMonthly);
						logger.log(Level.SEVERE, "DEDUCTION AMOUNT MONTHLY :: "+ dedutionAmountMonthly);
						logger.log(Level.SEVERE,"CONDITION  :: " + deductions.getCondition());
	
						if (deductions.getCondition().trim().equals("<")) {
							if (dedutionAmountMonthly < applicableAmtMonthly) {
								logger.log(Level.SEVERE, "AMOUNT <");
								dedAmount = dedutionAmountMonthly* deductions.getMaxPerOfCTC() / 100;
								ctcComponentName = deductions.getName();
							}
						}
						if (deductions.getCondition().trim().equals(">")) {
							if (dedutionAmountMonthly > applicableAmtMonthly) {
								logger.log(Level.SEVERE, "AMOUNT >");
								dedAmount = dedutionAmountMonthly* deductions.getMaxPerOfCTC() / 100;
								ctcComponentName = deductions.getName();
							}
						}
						if (deductions.getCondition().trim().equals("=")) {
							if (dedutionAmountMonthly == applicableAmtMonthly) {
								logger.log(Level.SEVERE, "AMOUNT =");
								dedAmount = dedutionAmountMonthly* deductions.getMaxPerOfCTC() / 100;
								ctcComponentName = deductions.getName();
							}
						}
						
						baseAmount=dedutionAmountMonthly;
	
					} else {
	
						// if condition equal to null like for PF
						logger.log(Level.SEVERE, "AMOUNT COMPANY CONTIBUTED PF");
	//					dedAmount = dedutionAmountMonthly* deductions.getMaxPerOfCTC() / 100;
						ctcComponentName = deductions.getName();
						
						/**
						 * Date : 13-08-2018 By ANIL
						 * Old code for calculating employers contributions like for PF
						 */
	//					dedAmount = deductions.getAmount() / 12;
						dedutionAmountMonthly=paySlip.getCtcComponentAmount(deductions.getCtcComponentName());
						
						if(pfMaxFixedValue!=0&&deductions.getShortName().equalsIgnoreCase("PF")&&pfFlag==false){
							pfFlag=true;
							dedutionAmountMonthly=0;
							for(CtcComponent ded:deductionList){
								if(ded.getShortName().equalsIgnoreCase("PF")){
									dedutionAmountMonthly+=paySlip.getCtcComponentAmount(ded.getCtcComponentName());
								}
							}
							if(dedutionAmountMonthly>pfMaxFixedValue){
//								pfMaxLimitFlag=true;
								dedutionAmountMonthly=pfMaxFixedValue;
							}
						}else if(pfMaxFixedValue!=0&&deductions.getShortName().equalsIgnoreCase("PF")&&pfFlag==true){
							dedutionAmountMonthly=0;
						}
						
						/**
						 * End
						 */
						
						dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
						deductions.setAmount(dedAmount);
	//					deducList.add(deductions);
					}
				} 
				
				
				if(deductions.getShortName().equalsIgnoreCase("LWF")||deductions.getShortName().equalsIgnoreCase("MLWF")||deductions.getShortName().equalsIgnoreCase("MWF")){
					dedAmount = deductions.getAmount();
					ctcComponentName = deductions.getName();
				}
			
	
				CompanyPayrollRecord compPayRec1 = null;
				if (ctcComponentName != null && dedAmount != 0) {
					/**
					 * Rahul Verma added Specific Month changes Date : 27 June 2018
					 * Description : this condition is added for specific months
					 */
					boolean isSpecificMonth = false;
					try {
						if (deductions.isSpecificMonth()) {
							isSpecificMonth = true;
						}
					} catch (Exception e) {
						e.printStackTrace();
						isSpecificMonth = false;
					}
					
					
					if (!isSpecificMonth) {
						logger.log(Level.SEVERE, "CTC COMPONENT NAME : "+ ctcComponentName);
						logger.log(Level.SEVERE, "DEDUCTION AMOUNT : " + dedAmount);
	
						double oneDayDeduction = dedAmount / monthlyDays;
	//					double actualDeduction = oneDayDeduction * paidDays;
						/**
						 * Date : 28-08-2018 By ANIL
						 * For PF arrears
						 */
						
						double actualDeduction=0;
						if(deductions.getShortName().equalsIgnoreCase("PF")&&deductions.getAmount()!=0){
//							actualDeduction = (oneDayDeduction*paidDays)+(pfArrears*deductions.getMaxPerOfCTC()/100);
							/**
							 * Date : 11-10-2018 By ANIL
							 */
							double actualPfAmount=paySlip.getPFAmount();
							if(actualPfAmount>pfMaxFixedValue){
								baseAmount=pfMaxFixedValue;
								actualDeduction = (pfMaxFixedValue*deductions.getMaxPerOfCTC()/100)+(pfArrears*deductions.getMaxPerOfCTC()/100);
							}else{
								baseAmount=actualPfAmount;
								actualDeduction = (actualPfAmount*deductions.getMaxPerOfCTC()/100)+(pfArrears*deductions.getMaxPerOfCTC()/100);
							}
							/**
							 * End
							 */
						}
//						/** Date : 09-10-2018 BY ANIL*/
//						else if(deductions.getShortName().equalsIgnoreCase("PF")&&deductions.getAmount()!=0&&pfMaxLimitFlag&&pfMaxFixedValue!=0){
//							pfMaxLimitFlag=false;
//							actualDeduction = (pfMaxFixedValue*deductions.getMaxPerOfCTC()/100)+(pfArrears*deductions.getMaxPerOfCTC()/100);
//						}
						else if(deductions.getShortName().equalsIgnoreCase("ESIC")&&deductions.getAmount()!=0){
							baseAmount=baseAmount+pfArrears+otherArrears;
							actualDeduction=dedAmount+((pfArrears+otherArrears)*deductions.getMaxPerOfCTC()/100);
						}else{
							actualDeduction = oneDayDeduction*paidDays;
						}
						/**
						 * Ends
						 */
						
						/**
						 * Date : 11-07-2018 By ANIL Setting actual deduction to
						 * deduction list
						 * Date : 21-07-2018 BY ADDED ESIC ROUND OFF CODE
						 */
						if(deductions.getShortName().equalsIgnoreCase("ESIC")){
							PaySlipServiceImpl impl=new PaySlipServiceImpl();
							actualDeduction=impl.getEsicRoundOff(actualDeduction);
						}
						/**
						 * End
						 */
						
	
						actualSumDeadAmount = actualSumDeadAmount + actualDeduction;
						ctcAmount = Math.round(actualSumDeadAmount);
						logger.log(Level.SEVERE, "CTC COMPONENT AMOUNT : "+ ctcAmount);
	
						compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
								empId, empName, payRollPeriod, ctcComponentName,
								ctcAmount, ctc.getCompanyId(),deductions.getShortName(),0,baseAmount,projectName,deductions.getMaxPerOfCTC(),siteLocationName,state);
						compPayRecList.add(compPayRec1);
					} else {
						logger.log(Level.SEVERE, "DEDUCTION AMOUNT : " + dedAmount);
						String[] salarySplitData = payRollPeriod.split("-");
						if (deductions.isSpecificMonth()) {
							logger.log(Level.SEVERE, "DEDUCTION MONTH : " + deductions.getSpecificMonthValue().trim()+" :: "+salarySplitData[1]);
							if (salarySplitData[1].trim().equalsIgnoreCase(deductions.getSpecificMonthValue().trim())) {
								ctcAmount = deductions.getAmount();
								// deductions.setAmount(specificDeductionValue);
								ctcComponentName = deductions.getName();
								logger.log(Level.SEVERE, "DEDUCTION NAME AMOUNT : " + ctcComponentName+" :: "+ctcAmount);
								compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(empId, empName,
												payRollPeriod, ctcComponentName,
												ctcAmount, ctc.getCompanyId(),deductions.getShortName(),deductions.getConditionValue(),baseAmount,projectName,deductions.getMaxPerOfCTC(),siteLocationName,state);
								
								/**
								 * @author Anil ,Date : 11-04-2019
								 */
//								compPayRecList.add(compPayRec1);
//								double grossEarning=paySlip.getCtcComponentAmount("Gross Earning");
//								if(grossEarning>=deductions.getConditionValue()){
								if(paySlip.getGrossEarningWithoutOT()>=deductions.getFromRangeAmount()&&paySlip.getGrossEarningWithoutOT()<=deductions.getToRangeAmount()){	
									double amtRange=0;
//									int counter=0;
									boolean isValid=true;
									for(int counter=0;counter<compPayRecList.size();counter++){
//									for(CompanyPayrollRecord c:compPayRecList){
										CompanyPayrollRecord c=compPayRecList.get(counter);
										if(c.getCtcCompShortName().equalsIgnoreCase("LWF")
												||c.getCtcCompShortName().equalsIgnoreCase("MWF")
												||c.getCtcCompShortName().equalsIgnoreCase("MLWF")){
											isValid=false;
											amtRange=c.getAmtRange();
//											if(deductions.getConditionValue()>amtRange){
											if(paySlip.getGrossEarningWithoutOT()>=deductions.getFromRangeAmount()&&paySlip.getGrossEarningWithoutOT()<=deductions.getToRangeAmount()){
												compPayRecList.remove(counter);
//												counter--;
												isValid=true;
											}
										}
//										counter++;
									}
									if(isValid){
										compPayRecList.add(compPayRec1);
									}
								}
							}
						}
	
					}
	
				}
				
				
				/**
				 * 
				 */
				
				if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0&&compPayRec1!=null){
					String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
					
					/**
					 * @author Anil @since 04-02-2021
					 * retreiving attendance as per the key
					 */
					if(info.isReliever()){
						key=compPayRec1.getEmpId()+"";
					}
					if(siteLocation){
						key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
					}
					
					if(siteWiseAttendanceMap.containsKey(key)){
						if(employerContributionMap!=null&&employerContributionMap.size()!=0){
							if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
								employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
							}else{
								ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
								slipList.add(compPayRec1);
								employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
							}
							
						}else{
							ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
							slipList.add(compPayRec1);
							employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
						}
					}
				}
				
				
				/**
				 * 
				 */
				
				
			}
			
			if(pf!=null){
				double employerPf=paySlip.calculatePfAmt(pf, paySlip.getEarningList(), pfMaxFixedValue, monthlyDays, paidDays, false, false, true,paySlip,false);
				double baseAmt=paySlip.calculatePfAmt(pf, paySlip.getEarningList(), pfMaxFixedValue, monthlyDays, paidDays, false, false, false,paySlip,true);
				logger.log(Level.SEVERE, "EMPLOYR PF : "+ employerPf);
				CompanyPayrollRecord compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
						empId, empName, payRollPeriod, pf.getPfName(),
						employerPf, ctc.getCompanyId(),pf.getPfShortName(),0,baseAmt,projectName,pf.getEmployerContribution(),siteLocationName,state);
				compPayRecList.add(compPayRec1);
				
				/**
				 * 
				 */
				
				if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
					String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
					/**
					 * @author Anil @since 04-02-2021
					 * retreiving attendance as per the key
					 */
					if(info.isReliever()){
						key=compPayRec1.getEmpId()+"";
					}
					if(siteLocation){
						key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
					}
					if(siteWiseAttendanceMap.containsKey(key)){
						if(employerContributionMap!=null&&employerContributionMap.size()!=0){
							if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
								employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
							}else{
								ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
								slipList.add(compPayRec1);
								employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
							}
							
						}else{
							ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
							slipList.add(compPayRec1);
							employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
						}
					}
				}
				
				
				/**
				 * 
				 */
			}
			
			/**
			 * @author Anil , Date : 25-07-2019
			 * Esic employer contribution
			 * 
			 */
			double esicBaseAmt=0;
			double employerEsic=0;
			if(esic!=null){
				//ESIC,PAYSLIP,EMP CONT,EMPLR CONT,RATE,BASE
				employerEsic=paySlip.calculateEsicAmt(esic, paySlip, false, true, false,false);
				esicBaseAmt=paySlip.calculateEsicAmt(esic, paySlip, false, false, false,true);
				logger.log(Level.SEVERE, "EMPLOYR ESIC : "+ employerEsic);
				CompanyPayrollRecord compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
						empId, empName, payRollPeriod, esic.getEsicName(),
						employerEsic, ctc.getCompanyId(),esic.getEsicShortName(),
						0,esicBaseAmt,projectName,esic.getEmployerContribution(),siteLocationName,state);
				compPayRecList.add(compPayRec1);
				
				/**
				 * 
				 */
				
				if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
					String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
					/**
					 * @author Anil @since 04-02-2021
					 * retreiving attendance as per the key
					 */
					if(info.isReliever()){
						key=compPayRec1.getEmpId()+"";
					}
					if(siteLocation){
						key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
					}
					if(siteWiseAttendanceMap.containsKey(key)){
						if(employerContributionMap!=null&&employerContributionMap.size()!=0){
							if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
								employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
							}else{
								ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
								slipList.add(compPayRec1);
								employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
							}
							
						}else{
							ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
							slipList.add(compPayRec1);
							employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
						}
					}
				}
				
				
				/**
				 * 
				 */
			}
			
			
			/**
			 * @author Anil, Date : 23-02-2019
			 */
			
			if(paySlip.getBonus()!=0||paySlip.getPaidLeaves()!=0){
				if(paySlip.isPaidLeaveAndBonusAsAdditionalAllowance()){
					ArrayList<CtcComponent> employersContributionList=new ArrayList<CtcComponent>();
					employersContributionList.addAll(deductionList);
					CtcComponent esicComp=PaySlip.getEsicComponentFromDeductionList(employersContributionList);
					
					if(esicComp!=null){
						double washingAllowanceAmt=0;
						for(CtcComponent earn:paySlip.getEarningList()){
							if(earn.getName().equalsIgnoreCase(deductionTypeName)){
								washingAllowanceAmt=earn.getActualAmount();
								break;
							}
						}
						double esicGrossEarning=paySlip.getGrossEarningActualWithoutOT()-washingAllowanceAmt;
						if(esicGrossEarning<=esicComp.getConditionValue()){
							double additionalAllowance=paySlip.getBonus()+paySlip.getPaidLeaves();
							double upddatedApplicableGrossForEsicDed=esicGrossEarning+additionalAllowance;
							if(upddatedApplicableGrossForEsicDed<=esicComp.getConditionValue()){
								if(paySlip.getBonus()!=0){
									double deductionAmt=PaySlip.getEsicRoundOff1(paySlip.getBonus()*esicComp.getMaxPerOfCTC()/100);
//									deductionList.add(PaySlip.calculateEsicOnAdditionalComp(esicComp,"Bonus",deductionAmt));
									
									CompanyPayrollRecord compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
											paySlip.getEmpid(), paySlip.getEmployeeName(), payRollPeriod, "Bonus ESIC",
											deductionAmt, ctc.getCompanyId(),"ESIC",0,paySlip.getBonus(),projectName,esicComp.getMaxPerOfCTC(),siteLocationName,state);
									compPayRecList.add(compPayRec1);
									
									/**
									 * 
									 */
									
									if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
										String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
										/**
										 * @author Anil @since 04-02-2021
										 * retreiving attendance as per the key
										 */
										if(info.isReliever()){
											key=compPayRec1.getEmpId()+"";
										}
										if(siteLocation){
											key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
										}
										if(siteWiseAttendanceMap.containsKey(key)){
											if(employerContributionMap!=null&&employerContributionMap.size()!=0){
												if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
													employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
												}else{
													ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
													slipList.add(compPayRec1);
													employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
												}
												
											}else{
												ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
												slipList.add(compPayRec1);
												employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
											}
										}
									}
									
									
									/**
									 * 
									 */
								}
								if(paySlip.getPaidLeaves()!=0){
									double deductionAmt1=PaySlip.getEsicRoundOff1(paySlip.getPaidLeaves()*esicComp.getMaxPerOfCTC()/100);
//									deductionList.add(PaySlip.calculateEsicOnAdditionalComp(esicComp,"Paid Leave",deductionAmt1));
									CompanyPayrollRecord compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
											paySlip.getEmpid(), paySlip.getEmployeeName(), payRollPeriod, "PL ESIC",
											deductionAmt1, ctc.getCompanyId(),"ESIC",0,paySlip.getPaidLeaves(),projectName,esicComp.getMaxPerOfCTC(),siteLocationName,state);
									compPayRecList.add(compPayRec1);
									
									/**
									 * 
									 */
									
									if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
										String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
										/**
										 * @author Anil @since 04-02-2021
										 * retreiving attendance as per the key
										 */
										if(info.isReliever()){
											key=compPayRec1.getEmpId()+"";
										}
										if(siteLocation){
											key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
										}
										if(siteWiseAttendanceMap.containsKey(key)){
											if(employerContributionMap!=null&&employerContributionMap.size()!=0){
												if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
													employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
												}else{
													ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
													slipList.add(compPayRec1);
													employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
												}
												
											}else{
												ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
												slipList.add(compPayRec1);
												employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
											}
										}
									}
									
									
									/**
									 * 
									 */
								}
							}
						}
					}else{
						
						//Existing ESIC NULL 
						//Calculating As per the master
						if(esic!=null&&employerEsic!=0){
							/**
							 * @author Anil
							 * @since 06-06-2020
							 * Checking range condition before deducting employers ESIC
							 */
							boolean esicDeductionApplicable=false;
							double washingAllowanceAmt=0;
							for(CtcComponent earn:paySlip.getEarningList()){
								if(earn.getName().equalsIgnoreCase(deductionTypeName)){
									washingAllowanceAmt=earn.getActualAmount();
									break;
								}
							}
							double esicGrossEarning=paySlip.getGrossEarningActualWithoutOT()-washingAllowanceAmt;
							if(esicGrossEarning<=esic.getEmployerToAmount()){
								double additionalAllowance=paySlip.getBonus()+paySlip.getPaidLeaves();
								double upddatedApplicableGrossForEsicDed=esicGrossEarning+additionalAllowance;
								if(upddatedApplicableGrossForEsicDed<=esic.getEmployerToAmount()){
									esicDeductionApplicable=true;
								}
							}
							
							if(paySlip.getBonus()!=0&&esicDeductionApplicable){
								double deductionAmt=PaySlip.getEsicRoundOff1(paySlip.getBonus()*esic.getEmployerContribution()/100);
//								deductionList.add(PaySlip.calculateEsicOnAdditionalComp(esicComp,"Bonus",deductionAmt));
								
								CompanyPayrollRecord compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
										paySlip.getEmpid(), paySlip.getEmployeeName(), payRollPeriod, "Bonus ESIC",
										deductionAmt, ctc.getCompanyId(),"ESIC",0,paySlip.getBonus(),projectName,esic.getEmployerContribution(),siteLocationName,state);
								compPayRecList.add(compPayRec1);
								
								if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
									String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
									/**
									 * @author Anil @since 04-02-2021
									 * retreiving attendance as per the key
									 */
									if(info.isReliever()){
										key=compPayRec1.getEmpId()+"";
									}
									if(siteLocation){
										key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
									}
									if(siteWiseAttendanceMap.containsKey(key)){
										if(employerContributionMap!=null&&employerContributionMap.size()!=0){
											if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
												employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
											}else{
												ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
												slipList.add(compPayRec1);
												employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
											}
										}else{
											ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
											slipList.add(compPayRec1);
											employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
										}
									}
								}
							}
							
							if(paySlip.getPaidLeaves()!=0&&esicDeductionApplicable){
								double deductionAmt1=PaySlip.getEsicRoundOff1(paySlip.getPaidLeaves()*esic.getEmployerContribution()/100);
								CompanyPayrollRecord compPayRec1 = CompanyPayrollRecord.createCompPayrollRec(
										paySlip.getEmpid(), paySlip.getEmployeeName(), payRollPeriod, "PL ESIC",
										deductionAmt1, ctc.getCompanyId(),"ESIC",0,paySlip.getPaidLeaves(),projectName,esic.getEmployerContribution(),siteLocationName,state);
								compPayRecList.add(compPayRec1);
								
								if(isSiteWasPayroll&&siteWiseAttendanceMap!=null&&siteWiseAttendanceMap.size()!=0){
									String key=compPayRec1.getEmpId()+compPayRec1.getProjectName();
									/**
									 * @author Anil @since 04-02-2021
									 * retreiving attendance as per the key
									 */
									if(info.isReliever()){
										key=compPayRec1.getEmpId()+"";
									}
									if(siteLocation){
										key=compPayRec1.getEmpId()+compPayRec1.getProjectName()+compPayRec1.getSiteLocation();
									}
									if(siteWiseAttendanceMap.containsKey(key)){
										if(employerContributionMap!=null&&employerContributionMap.size()!=0){
											if(employerContributionMap.containsKey(compPayRec1.getEmpId()+"$"+compPayRec1.getState())){
												employerContributionMap.get(compPayRec1.getEmpId()+"$"+compPayRec1.getState()).add(compPayRec1);
											}else{
												ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
												slipList.add(compPayRec1);
												employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
											}
											
										}else{
											ArrayList<CompanyPayrollRecord> slipList=new ArrayList<CompanyPayrollRecord>();
											slipList.add(compPayRec1);
											employerContributionMap.put(compPayRec1.getEmpId()+"$"+compPayRec1.getState(), slipList);
										}
									}
								}
								
							}
						}
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
					}
				}
			}
			/**
			 * 
			 */
			
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			logger.log(Level.SEVERE, "logger "+compPayRecList);

			return compPayRecList;
			
			
		}
		
		private double getESICComponentAmount(String deductionTypeName,
				ArrayList<CtcComponent> earning, int monthlyDays,double paidDays) {
			double amount=0;
			for(CtcComponent ctc:earning){
				if(deductionTypeName.equals(ctc.getName())){
				amount+=ctc.getActualAmount();
				}
			}
			
			// TODO Auto-generated method stub
			return amount;
		}

		public double getCtcComponentAmount(String ctcComponentName,List<CtcComponent> earningList,int monthlyDays,double paidDays){
			double amount=0;
			if(ctcComponentName.equals("Gross Earning")){
				for (CtcComponent earnings : earningList) {
					
					double earning = earnings.getAmount() / 12;
					
					double oneDayEarning=earning/monthlyDays;
					double actualearning=oneDayEarning*paidDays;
					amount=amount+actualearning;
				}
			}else{
				for(int i=0;i<earningList.size();i++){
					if(earningList.get(i).getName().equals(ctcComponentName)){
						amount=earningList.get(i).getAmount()/ 12;
					}
				}
			}
			return amount;
		}
		
		/**
		 * Date : 21-07-2018 BY ANIL
		 * for calculating ESIC
		 */
		
		public double getEsicRoundOff(double amount){
			DecimalFormat df = new DecimalFormat("0");
			double esicAmount=0;
			esicAmount=Double.parseDouble(df.format(amount));
			
			if(esicAmount<amount){
				esicAmount++;
			}
			return esicAmount;
		}
		
		/** date 20.7.2018 added by komal for tax calculation**/
		
		private double calculateTDS(PaySlip paySlip , CTC ctc , ArrayList<CtcComponent> ctcList , EmployeeInfo info , String year, Calendar cal,EmployeeInvestment emp,List<TaxSlab> taxSlabList,double totalDeductions){
		
			Logger logger1=Logger.getLogger("Logger");
			
			logger1.log(Level.SEVERE , "SLAB LIST : " + taxSlabList.size());
			double tds = 0;
			logger1.log(Level.SEVERE , "Total deductions : " + totalDeductions);
			if(ctc.getCtcAmount() >= 250000){
			double totalInvestments = calculateInvestments(info ,year,cal,emp);
			logger1.log(Level.SEVERE , "Total investments : " + totalInvestments);
			double taxableIncome = calculateTaxableAmount(paySlip,ctc  ,cal ,totalDeductions,totalInvestments,false);
			if(taxableIncome >= 250000){
				tds = calculateTax(ctc ,taxableIncome , taxSlabList ,cal ,info);
			}
			}
			return tds;
		}
		
		private double calculateDeductions(CTC ctc ,ArrayList<CtcComponent> dedList ,ArrayList<CtcComponent> ctcList,Calendar cal,boolean flag){
		//	CTC ctc1=ofy().load().type(CTC.class).filter("empid",ctc.getEmpid()).filter("companyId",ctc.getCompanyId()).filter("status", "Active").first().now();
			double totalDeductions = 0;
			Logger logger1=Logger.getLogger("Logger" + flag);
			
			//int month = cal.get(Calendar.MONTH);
			double currentMonthDeductions = 0;
			for(CtcComponent c : ctcList){
				if(!c.isTDS()){
					currentMonthDeductions += c.getActualAmount();
					logger1.log(Level.SEVERE , "current month ded :" +currentMonthDeductions );
				}
			}
			logger1.log(Level.SEVERE , "total current month ded :" +currentMonthDeductions );
			//ArrayList<CtcComponent> ctcL = ctc1.getDeduction();
			double cDeduction = ctc.getCumulativeDeduction();
			logger1.log(Level.SEVERE , "getCumulativeDeduction :" +cDeduction );
			double ded = 0;
			for(CtcComponent ct : dedList){
				logger1.log(Level.SEVERE , "data from ctc :" +ct.getAmount());
				if(!ct.isTDS()){
					ded += ct.getAmount();
					logger1.log(Level.SEVERE , "Ctc component name :"+ct.getName() + " all  ded :" +ded );
				}
			}
			logger1.log(Level.SEVERE , "all moth ded :" +ded );
			
			double dMonthly = ded/12;
			double diff = 12- getMonthAsPerFinancialYear(cal);
			
//			double totalDed = ded -  cDeduction -currentMonthDeductions;
//			double dMonthly = totalDed/diff;
			if(flag){
				totalDeductions = (dMonthly *(diff+1)) + cDeduction ;
			}else{
				totalDeductions = (dMonthly *diff) + cDeduction + currentMonthDeductions;
				ctc.setCumulativeDeduction(currentMonthDeductions + cDeduction);
			}
			logger1.log(Level.SEVERE , "TotalDeduction :" +totalDeductions );
			
			return totalDeductions;
		}
		
		private double calculateInvestments(EmployeeInfo info , String year, Calendar cal , EmployeeInvestment emp){
			double totalInvestments = 0;
			int month = cal.get(Calendar.MONTH);
			if(emp != null){
				ArrayList<EmployeeInvestmentBean> list = emp.getList();
				for(EmployeeInvestmentBean bean : list){
				    if(bean.getYear().equalsIgnoreCase(year)){
				    	if(month>=3 && month<=11){
				    		if(bean.getDeclaredInvestment() <= bean.getUpperLimit()){
				    			totalInvestments += bean.getDeclaredInvestment();
				    		}else{
				    			totalInvestments += bean.getUpperLimit();
				    		}
				    	}else{
				    		if(bean.getActualInvestment() <= bean.getUpperLimit()){
				    			totalInvestments += bean.getActualInvestment();
				    		}else{
				    			totalInvestments += bean.getUpperLimit();
				    		}
				    	}
				    }
				}
			}
			
			return totalInvestments;
		}
		private double calculateTaxableAmount(PaySlip paySlip,CTC ctc,Calendar cal,double totalDeductions ,double totalInvestments, boolean flag){
			Logger logger1=Logger.getLogger("Logger");
			double totaltaxableamt , totalamt= 0;
			//int month = cal.get(Calendar.MONTH);
			double currentMonthTaxableAmt = 0;
			currentMonthTaxableAmt = paySlip.getGrossEarning();
			double cEarning = ctc.getCumulativeCTC();
			double actualCTC = ctc.getCtcAmount();
			
			logger1.log(Level.SEVERE, "Month "+ paySlip.getSalaryPeriod()+" currentMonthTaxableAmt "+currentMonthTaxableAmt+" Total CTC : "+actualCTC+" cumulative CTC : "+cEarning);
			logger1.log(Level.SEVERE, "Tot Ded "+ totalDeductions +" Tot Inv "+totalInvestments);
			double dMonthly = actualCTC/12;
			double diff = 12- getMonthAsPerFinancialYear(cal);
			if(flag){
				logger1.log(Level.SEVERE, "calculateTaxable Amount : flag "+ flag);
				totalamt = (dMonthly *(diff+1)) + cEarning;
			}else{
				logger1.log(Level.SEVERE, "calculateTaxableAmount : flag "+ flag);
				totalamt = (dMonthly *diff) + cEarning + currentMonthTaxableAmt;
			}
			//this.ctc.setCumulativeCTC(cEarning + dMonthly *diff);
			totaltaxableamt = totalamt - totalDeductions - totalInvestments;
			logger1.log(Level.SEVERE, "taxable amount" + totaltaxableamt);
			return totaltaxableamt;
		}
		
		private int getMonthAsPerFinancialYear(Calendar cal){
			/**
			 * @author Anil , Date : 10-02-2020
			 */
			cal.setTimeZone(TimeZone.getTimeZone("IST"));
			Logger logger1=Logger.getLogger("Logger");
			int mon = 0;
			logger1.log(Level.SEVERE , "time :"+ cal.getTime());
			int month = cal.get(Calendar.MONTH);
			logger1.log(Level.SEVERE , "month :"+ month);
			switch(month){
			case 3:
				mon = 1;break;
			case 4:
				mon = 2;break;	
			case 5:
				mon = 3;break;
			case 6:
				mon = 4;break;	
			case 7:
				mon = 5;break;
			case 8:
				mon = 6;break;
			case 9:
				mon = 7;break;
			case 10:
				mon = 8;break;
			case 11:
				mon = 9;break;
			case 0:
				mon = 10;break;
			case 1:
				mon = 11;break;
			case 2:
				mon = 12;break;
			}
			logger1.log(Level.SEVERE , "mon :"+ mon);
			return mon;
		}
		
		private double calculateTax(CTC ctc ,double taxableIncome , List<TaxSlab> taxSlabList1 ,Calendar cal ,EmployeeInfo empInfo){
			double tds = 0;
			Logger logger1=Logger.getLogger("Logger");
			//List<Double> upperLimitSet= new HashSet<Double>();
			Map<Double ,TaxSlab> map = new HashMap<Double,TaxSlab>();
			List<TaxSlab> taxSlabList = new ArrayList<TaxSlab>();
			for(TaxSlab slab :taxSlabList1){
				if(taxableIncome >= 1000001 && slab.getLowerLimit() == 1000001){
					
						slab.setUpperLimit(taxableIncome);
						taxSlabList.add(slab);
					
				}else{
					taxSlabList.add(slab);
				}
			}
			Comparator<TaxSlab> compare = new  Comparator<TaxSlab>() {			
				@Override
				public int compare(TaxSlab o1, TaxSlab o2) {
					// TODO Auto-generated method stub
					return Double.compare(o2.getUpperLimit() , o1.getUpperLimit());
				}
			};
			Collections.sort(taxSlabList,compare);
			double eTax = 0;
			double shTax = 0;
			for(TaxSlab slab :taxSlabList){
				double maxTax = 0;
				double amount = slab.getUpperLimit();
				if(slab.getTax()!=0){
					maxTax = ((amount- slab.getLowerLimit()+1) *slab.getTax()/100);
				}
				if(slab.getEducationTax()!=0)
					eTax = slab.getEducationTax();
				if(slab.getHsEducationTax()!=0)
					shTax = slab.getHsEducationTax();
				slab.setSlabMaxTax(maxTax);
				map.put(slab.getUpperLimit(), slab);
			}
			ArrayList<Double> taxList = new ArrayList<Double>();
			for(int i=0;i<taxSlabList.size() ; i++){
				TaxSlab slab = taxSlabList.get(i);
				double amount = 0;
				if(taxableIncome <=slab.getUpperLimit() && taxableIncome >=slab.getLowerLimit()){
					amount = taxableIncome - slab.getLowerLimit()+1;
					taxList.add(amount*(slab.getTax()/100));
					i++;
					while(i !=taxSlabList.size()){
						taxList.add(taxSlabList.get(i).getSlabMaxTax());
						i++;
					}
					break;
				} 
				if(taxableIncome == slab.getUpperLimit() && slab.getLowerLimit() == 1000001){
					amount = taxableIncome - 1000000;
					taxList.add(amount*(slab.getTax()/100));
					i++;
					while(i !=taxSlabList.size()){
						taxList.add(taxSlabList.get(i).getSlabMaxTax());
						i++;
					}
					break;
				
				}
			}
			double totalTax = 0;
			double diff = 13- getMonthAsPerFinancialYear(cal);
			double cumulativeTax = ctc.getCumulativeTax();
			for(int j=0 ;j<taxList.size();j++){
				totalTax += taxList.get(j);
			}
			totalTax += (totalTax*eTax/100)+(totalTax*shTax/100);
			logger1.log(Level.SEVERE, "Total tax : "+ totalTax);
			tds = (totalTax-cumulativeTax) / diff;
			logger1.log(Level.SEVERE, "TDS : "+ tds);
			return tds;
		}


	private ArrayList<CtcComponent> getDedList(List<CtcComponent> deduction){
		ArrayList<CtcComponent> list = new ArrayList<CtcComponent>();
		for(CtcComponent ctcComp : deduction){
			CtcComponent ctcComp1 = ctcComp.Myclone();
						list.add(ctcComp1);
			
		}
		
		return list;
	}
		
	/** date 06.08.2018 added by komal for full and final salary deduction calcaulation**/
	public String saveArrearsData(Employee emp ,double noticePeriod , double shortFall , Date dateOfResignation , Date lastWorkingDate){
		SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
		isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		logger.log(Level.SEVERE , "last working date :" +lastWorkingDate);
		CTC ctc = ofy().load().type(CTC.class).filter("companyId", emp.getCompanyId()).filter("empid", emp.getCount()).filter("status", "Active").first().now();
		if(ctc == null){
			return "Please allocate CTC to employee.";
		}
		/**
		 * @author Anil
		 * @since 30-04-2020
		 * notice period should be calculated on Basic and DA
		 * earlier it was calculated in gross earning
		 */
//		double ctcAmount = ctc.getCtcAmount();
		double ctcAmount =getBaseAmount(ctc);
		 logger.log(Level.SEVERE,"Base Amount "+ctcAmount);
		//Date demoDate = DateUtility.getDateWithTimeZone("IST", lastWorkingDate);
		Date expectedLastWorkingDate = DateUtility.addDaysToDate(lastWorkingDate,(int)shortFall+1);
		Calendar calendar = Calendar.getInstance(); 			
		calendar.setTime(expectedLastWorkingDate);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
		
		Date startDate =DateUtility.addDaysToDate(lastWorkingDate , 2);
		logger.log(Level.SEVERE, "Expected last working date " + expectedLastWorkingDate + "Date :"+startDate + "lastWorkingDate :"+lastWorkingDate);
		Calendar c = Calendar.getInstance(); 			
		c.setTime(startDate);
//		c.set(Calendar.HOUR_OF_DAY, 0);
//		c.set(Calendar.MINUTE,0);
//		c.set(Calendar.SECOND, 0);
//		c.set(Calendar.MILLISECOND, 0);
		startDate=DateUtility.getDateWithTimeZone("IST", c.getTime());
		logger.log(Level.SEVERE, "Expected last working date " + expectedLastWorkingDate + "Date :"+startDate +"LATEST DATE :"+ calendar.getTime());
		double amount = 0;
		while(startDate.before(expectedLastWorkingDate)){
			int days = DateUtility.getDaysInMonth(startDate);
			logger.log(Level.SEVERE, "last working date month :" + days);
			Double oneDaySalary = ctcAmount/(12 * days);
			Date lastDate = DateUtility.getEndDateofMonth(startDate);
			if(DateUtility.getMonth(expectedLastWorkingDate) == DateUtility.getMonth(startDate) && startDate.before(expectedLastWorkingDate)){
				lastDate = calendar.getTime();
			}
			logger.log(Level.SEVERE," :::=Last DATE = :::  "+lastDate);
			
			long diff=lastDate.getTime()-startDate.getTime();			
	        int noofdays=(int)(diff/(1000*24*60*60)) + 1;
	        logger.log(Level.SEVERE," :::=No of days  = :::  "+noofdays);
	        amount += (oneDaySalary * noofdays);
	        c.setTime(lastDate);
	        c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE,0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			c.add(Calendar.DATE, 1);
			startDate = c.getTime();
			logger.log(Level.SEVERE," :::=Next Start DATE = :::  "+startDate);
			logger.log(Level.SEVERE,"Total Amount "+amount);
		}
		List<ArrearsInfoBean> beanList = new ArrayList<ArrearsInfoBean>();
		HashMap<String , Double> map = new HashMap<String , Double>();
		 
		for(EmployeeAssetBean bean : emp.getCheckList()){
			if(!bean.isRecordSelect()){
				if(bean.getPrice()!=0){						
					double value = 0;
					if(!(bean.getAssetCategory().equals(""))){
						if(map.containsKey(bean.getAssetCategory())){
							value = map.get(bean.getAssetCategory());
							value += bean.getPrice();
							map.put(bean.getAssetCategory(), value);
						}else{
							value = bean.getPrice();
							map.put(bean.getAssetCategory(), value);
						}
					}
				}
			}

		}
		for (Map.Entry<String,Double> entry : map.entrySet())  {
            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
            ArrearsInfoBean infoBean = new ArrearsInfoBean();
			infoBean.setHead(entry.getKey());
			infoBean.setOperation(AppConstants.DEDUCT);
			infoBean.setAmount(entry.getValue());
			infoBean.setEarningCompName("FNF");
			if(emp.getFnfMonth() !=null){
				infoBean.setFromDate(isoFormat.format(emp.getFnfMonth()));
				infoBean.setToDate(isoFormat.format(emp.getFnfMonth()));
			}
			beanList.add(infoBean);

		}
//		for(EmployeeAssetBean bean : emp.getCheckList()){			
//			if(!bean.isRecordSelect()){
//				if(bean.getPrice()!=0){
//					ArrearsInfoBean infoBean = new ArrearsInfoBean();
//					infoBean.setHead(bean.getAssetName());
//					infoBean.setOperation(AppConstants.DEDUCT);
//					infoBean.setAmount(bean.getPrice());
//					infoBean.setEarningCompName("FNF");
//					beanList.add(infoBean);
//				}
//			}
//		}
		if(amount>0){
			ArrearsInfoBean infoBean = new ArrearsInfoBean();
			infoBean.setHead(AppConstants.SHORTFALLAMOUNT);
			infoBean.setOperation(AppConstants.DEDUCT);
			infoBean.setAmount(Math.round(amount*100.0)/100.0);
			infoBean.setEarningCompName("FNF");
			if(emp.getFnfMonth() !=null){
				infoBean.setFromDate(isoFormat.format(emp.getFnfMonth()));
				infoBean.setToDate(isoFormat.format(emp.getFnfMonth()));
			}
			beanList.add(infoBean);
		}
		ArrearsDetails arrearsDetails = null;
		if(beanList.size()>0){
			arrearsDetails = ofy().load().type(ArrearsDetails.class).filter("companyId", emp.getCompanyId()).filter("empId", emp.getCount()).first().now();
			ArrayList<ArrearsInfoBean> list = new ArrayList<ArrearsInfoBean>();
			if(arrearsDetails == null ){
				arrearsDetails = new ArrearsDetails();
			}else{
				list = arrearsDetails.getArrearsList();
			}
			arrearsDetails.setEmpId(emp.getCount());
			arrearsDetails.setEmpName(emp.getFullname());
			arrearsDetails.setEmpCellNo(emp.getCellNumber1());
			arrearsDetails.setCompanyId(emp.getCompanyId());
			list.addAll(beanList);
			arrearsDetails.setArrearsList(list);
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(arrearsDetails);
		}
		emp.setRealeaseForPayment(true);
		ofy().save().entity(emp);
		return "success";
		
	}

		private double getBaseAmount(CTC ctc) {
			double amount=0;
			if(ctc!=null) {
				for(CtcComponent comp:ctc.getEarning()) {
					if(comp.getShortName().trim().equalsIgnoreCase("BASIC")||comp.getShortName().trim().equalsIgnoreCase("DA")){
						amount+=comp.getAmount();
					}
				}
			}
			return amount;
		}

		@Override
		public String freezePayroll(ArrayList<EmployeeInfo> empInfoList,String salaryPeriod,String freezeBy) {
			String response="";
			ArrayList<Integer>empIdList=new ArrayList<Integer>();
			for(EmployeeInfo info:empInfoList){
				empIdList.add(info.getEmpCount());
			}
			
			
			if(empIdList.size()!=0){
				List<PaySlip> payrollList=ofy().load().type(PaySlip.class).filter("companyId", empInfoList.get(0).getCompanyId()).filter("empid IN", empIdList).filter("salaryPeriod", salaryPeriod).filter("isFreeze", false).list();
				if(payrollList!=null&&payrollList.size()!=0){
					for(PaySlip ps:payrollList){
						ps.setFreeze(true);
						ps.setDateOfFreeze(new Date());
						ps.setFreezeBy(freezeBy);
					}
					
					ofy().save().entities(payrollList).now();
					response="Payroll update successful.";
				}else{
					response="No new payroll found.";
				}
				
			}else{
				response="No payroll found.";
			}
			
			return response;
		}

		@Override
		public String getProvisionalPayroll(ArrayList<AllocationResult> result,String salaryPeriod) {
			// TODO Auto-generated method stub
			ArrayList<AllocationResult>payrollGenList=new ArrayList<AllocationResult>();
			ArrayList<AllocationResult>payrollNotGenList=new ArrayList<AllocationResult>();
			ArrayList<Integer> empIdList=new ArrayList<Integer>();
			ArrayList<EmployeeInfo> empInfoList=new ArrayList<EmployeeInfo>();
			long companyId=result.get(0).info.getCompanyId();
			
			for(AllocationResult alloc:result){
				if(alloc.status==true){
					payrollGenList.add(alloc);
					empIdList.add(alloc.info.getEmpCount());
				}else{
					payrollNotGenList.add(alloc);
					empInfoList.add(alloc.info);
				}
			}
			
			logger.log(Level.SEVERE,"Payroll generated list : "+payrollGenList.size());
			logger.log(Level.SEVERE,"Payroll not generated list : "+payrollNotGenList.size());
			logger.log(Level.SEVERE,"Employee Id List size : "+empIdList.size());
			logger.log(Level.SEVERE,"Employee Info List size : "+empInfoList.size());
			
			List<PaySlip> paySlipList=new ArrayList<PaySlip>();
			if(empIdList.size()!=0){
				paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid IN", empIdList).filter("salaryPeriod", salaryPeriod).list();
			}
			logger.log(Level.SEVERE,"Payroll List size : "+paySlipList.size());
			allocatePaySlip(empInfoList, salaryPeriod, null, null, true);
			if(paySlipList.size()!=0){
				CsvWriter.paySlipList.addAll(paySlipList);
			}
			
			return null;
		}
		
		public ArrayList<CtcComponent> getEmployeeContributionLwfList(List<LWF> lwfList) {
			ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
			for(LWF lwf:lwfList){
				for(LWFBean month:lwf.getLwfList()){
					CtcComponent comp=new CtcComponent();
					comp.setCtcComponentName("Gross Earning");
					comp.setDeduction(true);
					comp.setIsRecord(false);
					comp.setName(lwf.getName()+" "+month.getDeductionMonth());
					comp.setShortName(lwf.getShortName());
					comp.setMaxAmount((int) (lwf.getEmployeeContribution()));
					comp.setAmount(lwf.getEmployeeContribution());
					comp.setCondition(">");
					if(lwf.getToAmt()!=0){
						comp.setConditionValue((int) lwf.getToAmt());
					}else{
						comp.setConditionValue((int) lwf.getFromAmt());
					}
					comp.setSpecificMonth(true);
					comp.setSpecificMonthValue(month.getDeductionMonth());
					
					/**
					 * @author Anil
					 * @since 02-07-2020
					 * Adding LWF from and to range
					 */
					comp.setFromRangeAmount(lwf.getFromAmt());
					comp.setToRangeAmount(lwf.getToAmt());
					
					ctcCompList.add(comp);
				}
			}
			return ctcCompList;
		}
		
		public ArrayList<CtcComponent> getEmployerContributionLwfList(List<LWF> lwfList) {
			ArrayList<CtcComponent> ctcCompList=new ArrayList<CtcComponent>();
			for(LWF lwf:lwfList){
				for(LWFBean month:lwf.getLwfList()){
					CtcComponent comp1=new CtcComponent();
					comp1.setCtcComponentName("Gross Earning");
					comp1.setDeduction(true);
					comp1.setIsRecord(true);
					comp1.setName(lwf.getName()+" "+month.getDeductionMonth());
					comp1.setShortName(lwf.getShortName());
					comp1.setMaxAmount((int) (lwf.getEmployerContribution()));
					comp1.setAmount(lwf.getEmployerContribution());
					comp1.setCondition(">");
					if(lwf.getToAmt()!=0){
						comp1.setConditionValue((int) lwf.getToAmt());
					}else{
						comp1.setConditionValue((int) lwf.getFromAmt());
					}
					comp1.setSpecificMonth(true);
					comp1.setSpecificMonthValue(month.getDeductionMonth());
					
					/**
					 * @author Anil
					 * @since 02-07-2020
					 * Adding LWF from and to range
					 */
					comp1.setFromRangeAmount(lwf.getFromAmt());
					comp1.setToRangeAmount(lwf.getToAmt());
					
					ctcCompList.add(comp1);
				}
			}
			return ctcCompList;
		}
		
		public int getTotalNoOfWeeklyOffBetweenDates(EmployeeInfo empInfo,Date sDate,Date eDate){
			Date startDate=DateUtility.getDateWithTimeZone("IST", sDate);
			Date endDate=DateUtility.getDateWithTimeZone("IST", eDate);
			
			
			
			int totalWo=0;
			
			com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal=empInfo.getLeaveCalendar();
			if(cal!=null){
				boolean isWoDefFlag=false;
				ArrayList<Integer> dayList=new ArrayList<Integer>();
				if(cal.isSUNDAY()){
					dayList.add(0);
					isWoDefFlag=true;
				}
				if(cal.isMONDAY()){
					dayList.add(1);
					isWoDefFlag=true;
				}
				if(cal.isTUESDAY()){
					dayList.add(2);
					isWoDefFlag=true;
				}
				if(cal.isWEDNESDAY()){
					dayList.add(3);
					isWoDefFlag=true;
				}
				if(cal.isTHRUSDAY()){
					dayList.add(4);
					isWoDefFlag=true;
				}
				if(cal.isFRIDAY()){
					dayList.add(5);
					isWoDefFlag=true;
				}
				if(cal.isSATAURDAY()){
					dayList.add(6);
					isWoDefFlag=true;
				}
				
				while(!startDate.equals(endDate)){
					if(isWoDefFlag){
						int day=startDate.getDay();
						System.out.println("DAY : "+day);
						if(dayList.contains(day)){
							totalWo++;
						}
					}
//					CalendarUtil.addDaysToDate(startDate, 1);
					startDate = DateUtility.addDaysToDate(startDate,1);
				}
			}
			return totalWo;
		}
		
}
