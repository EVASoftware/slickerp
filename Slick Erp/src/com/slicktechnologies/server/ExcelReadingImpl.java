package com.slicktechnologies.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.ReadingExcelService;
import com.slicktechnologies.shared.common.ExcelRecordsList;

public class ExcelReadingImpl extends RemoteServiceServlet implements ReadingExcelService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3799053865514090114L;


	Logger logger = Logger.getLogger("NameOfYourLogger");
	
	static BlobKey blobkey;

	@Override
	public ArrayList<ExcelRecordsList> getAttendiesdetails(long companyId) {
		
		logger.log(Level.SEVERE,"Blob Key-----------------"+blobkey);
		long i;
		
	    ArrayList<String> exceldatalist = new ArrayList<String>();

	    ArrayList<ExcelRecordsList> excelrecords = new ArrayList<ExcelRecordsList>();

		
		try {
			
		 Workbook wb = null;
				try {
					wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				} catch (EncryptedDocumentException e) {
					e.printStackTrace();
				} catch (InvalidFormatException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
		    Sheet sheet = wb.getSheetAt(0);
		    
		    
		    // Get iterator to all the rows in current sheet 
		    Iterator<Row> rowIterator = sheet.iterator();
		    
		    
		    // Traversing over each row of XLSX file
		    while (rowIterator.hasNext()) {
		    	Row row = rowIterator.next();
		    // For each row, iterate through each columns 
		    	Iterator<Cell> cellIterator = row.cellIterator(); 
		    	while (cellIterator.hasNext()) { 
		    		Cell cell = cellIterator.next();
		    		switch (cell.getCellType()) {
		    		case Cell.CELL_TYPE_STRING: 
	                    exceldatalist.add(cell.getStringCellValue()+"");
		    			break; 
		    		case Cell.CELL_TYPE_NUMERIC: 
		    		   i=(long) cell.getNumericCellValue();
	                    exceldatalist.add(i+"");

		    		   break; 
		    		 case Cell.CELL_TYPE_BOOLEAN: 
	                    logger.log(Level.SEVERE,"ouput"+cell.getBooleanCellValue() +"\t");

		    			break;
		    			default : 
		    			}
		    		} 
		    	System.out.println("");

		    }

		    System.out.println("Total size:"+exceldatalist.size());
            logger.log(Level.SEVERE,"Total size:"+exceldatalist.size());

		    wb.close();
		    
		    
		    for(int j=4; j<exceldatalist.size();j+=4){
		    	
		    	ExcelRecordsList records = new ExcelRecordsList();
		    	
		    	records.setFirstName(exceldatalist.get(j));
		    	records.setLastName(exceldatalist.get(j+1));
		    	records.setCellNo(exceldatalist.get(j+2));
		    	records.setEmailId(exceldatalist.get(j+3));
		    	
		    	excelrecords.add(records);
		    	
		    	
		    }
		    
		    System.out.println("Size"+exceldatalist.size());
		    
            logger.log(Level.SEVERE,"excelrecord:"+excelrecords.size());

	   
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return excelrecords;
	}

}
