package com.slicktechnologies.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.approval.ApprovalService;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Activity;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;



public class ApprovableServiceImplementor extends RemoteServiceServlet implements ApprovalService
{

	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = -9156484478827741375L;
	Logger logger=Logger.getLogger("ApprovableServiceImplementor");
	/**
	 * Date : 26-02-2017 By Anil
	 * Change the return type of method from void to String
	 */
	@Override
	public String sendApproveRequest(Approvals model) {
//		System.out.println("Buisness Process Type "+model.getBusinessprocesstype());
//		ApprovableProcess process=retrieveApprovableProcess(model.getBusinessprocesstype(), model.getBusinessprocessId(),model.getStatus(),model.getCompanyId());
//		
//		System.out.println("11111111111111111111111111111111"+model.getStatus());
//		process.setStatus(model.getStatus());
//		System.out.println("22222222222222222222222222222222222222"+process.getStatus());
//		process.setRemark(model.getRemark());
//		System.out.println("333333333333333333333333333333333333333");
//		process.setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
//		
//		
//		System.out.println("444444444444444444444444444444444444444444444444");
//		if(model.getStatus().equals(ConcreteBusinessProcess.APPROVED)){
//			   process.reactOnApproval();
//		System.out.println("55555555555555555555555555555555555555555555");
//		}	
//		else
//		{
//			process.reactOnRejected();
//			System.out.println("6666666666666666666666666666666666666666666");
//		}
//		System.out.println("");
//		
//		GenricServiceImpl impl=new GenricServiceImpl();
//		
//		SuperModel mod=(SuperModel) process;
//		impl.save(model);
//		impl.save(mod);
		
		
		logger.log(Level.SEVERE,"Buisness Process Type "+model.getBusinessprocesstype());
		ApprovableProcess process=null;
		ArrayList<ApprovableProcess> processList=retrieveApprovableProcess(model.getBusinessprocesstype(), model.getBusinessprocessId(),model.getStatus(),model.getCompanyId());
		if(processList.size()==0){
			return "No document found for approval!";
		}
		else if(processList.size()>1){
//			return "More than one document exist with same id!";
			/**
			 * @author Vijay Chougule Date 10-09-2019 
			 * Des :- NBHC CCPM if invoice id duplicate then here i am assigning new invoice id and proceed the same
			 */
			if(model.getBusinessprocesstype().equals(ApproverFactory.INVOICEDETAILS) 
					&& ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableNewInvoiceIdForDuplicateId", model.getCompanyId())){
				NumberGeneration numberGen = ofy().load().type(NumberGeneration.class).filter("companyId", model.getCompanyId())
						.filter("processName", "Invoice").first().now();
				System.out.println("number gen"+numberGen);
				logger.log(Level.SEVERE, "numberGen not loaded "+numberGen);
				if(numberGen!=null){
					int newNumber = (int) (numberGen.getNumber()+1);
					numberGen.setNumber(numberGen.getNumber()+1);
					SuperModel newModel = getInvoice(model,newNumber);
					logger.log(Level.SEVERE, "newModel"+newModel);
					if(newModel!=null){
						ofy().save().entity(numberGen);
						for(ApprovableProcess proceess : processList){
							if(proceess.getBranch().equals(model.getBranchname())){
								process = proceess;
								model.setBusinessprocessId(newNumber);
								ofy().save().entity(model);
								break;
							}
						}
					}
					else{
						return "More than one document exist with same id!";
					}
					
				}
				else{
					return "More than one document exist with same id!";
				}
			}
			/**
			 * ends here
			 */
			else{
				return "More than one document exist with same id!";
			}
			
		}
		else{
			process=processList.get(0);
		}
		/**
		 * Date : 26-02-2017 By Anil
		 * For Contract,MIN,MMN and GRN ,we use task queue for completing approval process.
		 */
		if(model.getBusinessprocesstype().equals(AppConstants.APPROVALCONTRACT)
				||model.getBusinessprocesstype().equals(AppConstants.APPROVALMATERIALISSUENOTE)
				||model.getBusinessprocesstype().equals(AppConstants.APPROVALMATERIALMOVEMENTNOTE)
				||model.getBusinessprocesstype().equals(AppConstants.APPROVALGRN)
				||model.getBusinessprocesstype().equals(AppConstants.INVOICEDETAILS)
				||model.getBusinessprocesstype().equals(AppConstants.PHYSICALINVENTORY)){
			
			System.out.println("Rohan inside "+model.getId());
			
			String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			
			
		}else{
			process.setStatus(model.getStatus());
			process.setRemark(model.getRemark());
			process.setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
			if(model.getStatus().equals(ConcreteBusinessProcess.APPROVED)){
			    process.reactOnApproval();
			}else{
				process.reactOnRejected();
			}
			System.out.println("");
//			GenricServiceImpl impl=new GenricServiceImpl();
			SuperModel mod=(SuperModel) process;
//			impl.save(model);
//			impl.save(mod);
			
			ofy().save().entity(model);
			ofy().save().entity(mod);
		}
		
		return "Success";
	}
	
	
	



//	@Override
//	public void sendApproveRequest(Approvals model) {
//		System.out.println("Buisness Process Type "+model.getBusinessprocesstype());
//		ApprovableProcess process=retrieveApprovableProcess(model.getBusinessprocesstype(), model.getBusinessprocessId(),model.getStatus(),model.getCompanyId());
//		System.out.println("status value ===="+model.getStatus());
//		process.setStatus(model.getStatus());
//		process.setRemark(model.getRemark());
//		process.setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
//		
//		if(model.getStatus().equals(ConcreteBusinessProcess.APPROVED))
//			   process.reactOnApproval();
//			else
//				process.reactOnRejected();
//		
//		
//		GenricServiceImpl impl=new GenricServiceImpl();
//		
//		SuperModel mod=(SuperModel) process;
//		impl.save(model);
//		impl.save(mod);
//	}

	@Override
	public void cancelDocument(Approvals model) {
//		GenricServiceImpl impl=new GenricServiceImpl();
//		SuperModel saved=(SuperModel) model;
//		impl.save(saved);
//		System.out.println("Buisness Process Type "+model.getBusinessprocesstype());
//		ApprovableProcess process=retrieveApprovableProcess(model.getBusinessprocesstype(), model.getBusinessprocessId(),model.getStatus(),model.getCompanyId());
//		process.setStatus(ConcreteBusinessProcess.CREATED);
//		impl.save((SuperModel) process);
		
		System.out.println("Buisness Process Type "+model.getBusinessprocesstype());
		ApprovableProcess process=null;
		ArrayList<ApprovableProcess> processList=retrieveApprovableProcess(model.getBusinessprocesstype(), model.getBusinessprocessId(),model.getStatus(),model.getCompanyId());
		
		if(processList.size()==0){
//			return "No document found for approval!";
		}else if(processList.size()>1){
//			return "More than one document exist with same id!";
		}else{
			process=processList.get(0);
			
			GenricServiceImpl impl=new GenricServiceImpl();
			SuperModel saved=(SuperModel) model;
			impl.save(saved);
			
			process.setStatus(ConcreteBusinessProcess.CREATED);
			impl.save((SuperModel) process);
		}
		
	}
			 
	/**
	 * 
	 * @param businessprocessType
	 * @param businessprocessId
	 * @param status
	 * @param compnyId
	 * 
	 * Date: 26-02-2017 By Anil
	 * Change the return type From ApprovableProcess to ArrayList<ApprovableProcess>
	 */
	public  ArrayList<ApprovableProcess> retrieveApprovableProcess(String businessprocessType,int businessprocessId,final String status,final Long compnyId)
	{
		ArrayList<ApprovableProcess> processList=new ArrayList<ApprovableProcess>();
		
		Vector<Filter> vecfilter=new Vector<Filter>();
		Filter filterId=new Filter();
		filterId.setIntValue(businessprocessId);
		filterId.setQuerryString("count");
		vecfilter.add(filterId);
		
		/**
		 * For Company
		 */
		
		Filter filterComp=new Filter();
		filterComp.setLongValue(compnyId);
		filterComp.setQuerryString("companyId");
		vecfilter.add(filterComp);
		
		
		MyQuerry queryDoc=new MyQuerry(vecfilter,returnDocObject(businessprocessType));
		GenricServiceImpl impl=new GenricServiceImpl();
		ArrayList<SuperModel>lis=impl.getSearchResult(queryDoc);
		if(lis!=null&&lis.size()!=0)
		{
			
			for(SuperModel model :lis){
				ApprovableProcess entity=(ApprovableProcess) model;
				processList.add(entity);
			}
//			return (ApprovableProcess) lis.get(0);
		}
//	   return null;
		
		return processList;
		
	}

	private static SuperModel returnDocObject(String businessprocesstype)
	{
		ConcreteBusinessProcess businessprocess = null;
		switch (businessprocesstype) {
		case ApproverFactory.EXPENSEMANAGMENT:
			return businessprocess=new Expense();
		case ApproverFactory.QUOTATION:
			return businessprocess=new Quotation();
		case ApproverFactory.CONTRACT:
			return businessprocess=new Contract();
		case ApproverFactory.COMPLAIN:
			return businessprocess=new Activity();
		case ApproverFactory.BILLINGDETAILS:
			return businessprocess=new BillingDocument();
		case ApproverFactory.INVOICEDETAILS:
			return businessprocess=new Invoice();
		case ApproverFactory.LEAVEAPPLICATION:
			return new LeaveApplication();
		case ApproverFactory.ADVANCE:
			return new Loan();
		case ApproverFactory.LETTEROFINTENT:
			return new LetterOfIntent();
		case ApproverFactory.REQUESTFORQUOTATION:
			return new RequsestForQuotation();
		case ApproverFactory.PURCHASEORDER:
			return new PurchaseOrder();
		case ApproverFactory.PURCHASEREQUISITION:
			return new PurchaseRequisition();
		case ApproverFactory.GRN:
			return new GRN();
		case ApproverFactory.SALESQUOTATION:
			return businessprocess=new SalesQuotation();
		case ApproverFactory.SALESORDER:
			return businessprocess=new SalesOrder();
		case ApproverFactory.TIMEREPORT:
			return new TimeReport();
		case ApproverFactory.DELIVERYNOTE:
			return new DeliveryNote();
		case ApproverFactory.MATERIALREQUESTNOTE:
			return businessprocess=new MaterialRequestNote();
		case ApproverFactory.MATERIALISSUENOTE:
			return businessprocess=new MaterialIssueNote();
		case ApproverFactory.MATERIALMOVEMENTNOTE:
			return businessprocess=new MaterialMovementNote();
		case ApproverFactory.INSPECTION:
			return businessprocess=new Inspection();
		case ApproverFactory.WORKORDER:
			return businessprocess=new WorkOrder();
		
		case ApproverFactory.CUSTOMERTRAINING:
			return businessprocess=new CustomerTrainingDetails();		
			
		case ApproverFactory.MULTIPLEEXPENSEMANAGMENT:
			return businessprocess=new MultipleExpenseMngt();
			
		case ApproverFactory.SERVICEPO:
			return businessprocess=new ServicePo();
		/** date 7.9.2018 added by komal **/
		case ApproverFactory.VENDORINVOICEDETAILS:
			return businessprocess = new VendorInvoice();
		case ApproverFactory.CNC:
			return businessprocess = new CNC();
		case ApproverFactory.PHYSICALINVETORYDETAIL:
			return businessprocess = new PhysicalInventoryMaintaince();
			
		default:
			break;
		}
		System.out.println("Returning buisness process "+businessprocess);
		return businessprocess;
	}

	
	private SuperModel getInvoice(Approvals model, int newNumber) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> vecfilter=new Vector<Filter>();
		Filter filter = null;
		filter=new Filter();
		filter.setIntValue(model.getBusinessprocessId());
		filter.setQuerryString("count");
		vecfilter.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(Integer.parseInt(model.getBpId()));
		vecfilter.add(filter);
		
		querry.setFilters(vecfilter);
		querry.setQuerryObject(new Invoice());
		
		GenricServiceImpl impl = new GenricServiceImpl();
		ArrayList<SuperModel> list  = impl.getSearchResult(querry);
		logger.log(Level.SEVERE,"Invoice list Size"+list.size());
		if(list.size()==1){
		for(SuperModel spmodel : list){
			Invoice invoiceEntity = (Invoice) spmodel;
			BillingDocument billingDoc = getBillingDoc(invoiceEntity);
			logger.log(Level.SEVERE,"billingDoc"+billingDoc);
			if(billingDoc!=null){
				billingDoc.setInvoiceCount(newNumber);
				ofy().save().entity(billingDoc).now();
				logger.log(Level.SEVERE,"Billing document updated successfully");
				invoiceEntity.setCount(newNumber);;
				ofy().save().entity(invoiceEntity).now();
				logger.log(Level.SEVERE,"Invoice document updated successfully");
			}
			else{
				return null;	
			}
		}
			return list.get(0);
		}
		else{
			return null;
		}
		
	}


	private BillingDocument getBillingDoc(Invoice model) {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> vecfilter=new Vector<Filter>();
		Filter filter = null;
		filter=new Filter();
		filter.setIntValue(model.getCount());
		filter.setQuerryString("invoiceCount");
		vecfilter.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("personInfo.count");
		filter.setIntValue(model.getPersonInfo().getCount());
		vecfilter.add(filter);

		querry.setFilters(vecfilter);
		querry.setQuerryObject(new BillingDocument());
		
		GenricServiceImpl impl = new GenricServiceImpl();
		ArrayList<SuperModel> list  = impl.getSearchResult(querry);
		if(list.size()==1){
			BillingDocument billdoc = (BillingDocument) list.get(0);
			return   billdoc;
		}
		return null;
	}

	
}
