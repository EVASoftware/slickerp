package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.salesorder.SalesOrderService;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class SalesOrderServiceImpl extends RemoteServiceServlet implements
		SalesOrderService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8879665252154988914L;

	Logger logger = Logger.getLogger("Sales Order Service Impl");
	
	Long count = 0l;
	
	@Override
	public void changeStatus(SalesOrder salesorder) 
	{
	  if(salesorder!=null)
	  {
		  changeCustomerStatus(salesorder);
		  changeQuotationStatus(salesorder);
		  changeLeadStatus(salesorder);
		  /**
		   * @author Vijay Date :- 02-12-2020
		   * Des :- when bill will created from CNC Sales order then CNC version is storing in Sales Order Entity
		   */
		  
		  if(salesorder.getRefNo()!=null && !salesorder.getRefNo().equals("")){
			  int refNo = 0;
				try {
					refNo = Integer.parseInt(salesorder.getRefNo());
				} catch (Exception e) {
					refNo = 0;
				}
				if(refNo!=0){
					CNC cncentity = ofy().load().type(CNC.class)
							.filter("count", refNo)
							.filter("companyId", salesorder.getCompanyId())
							.first().now();
					if (cncentity != null) {
						salesorder.setCncVersion(cncentity.getVersionNumber()+"");
					}
				}
			  
		  }
		  /**
		   * ends here
		   */
		  
		  ofy().save().entity(salesorder);
		  
		  System.out.println("Start Date After Save is "+salesorder.getStartDate());
		  System.out.println("End Date is After Save "+salesorder.getEndDate());
	  }
	}
	
	
	/**
	 * Date 08-09-2017 added by vijay for changing leads status to successfull
	 * requirement from Tiyara
	 * @param salesorder
	 */
	@OnSave
	@GwtIncompatible
	private void changeLeadStatus(SalesOrder salesorder) {
		if(salesorder.getLeadCount()!=-1){
			Lead leadentity = ofy().load().type(Lead.class).filter("companyId", salesorder.getCompanyId()).filter("count", salesorder.getLeadCount()).first().now();
			if(leadentity!=null){
				leadentity.setStatus("Successful");
				ofy().save().entity(leadentity);
			}
		}
	}
	
	@OnSave
	@GwtIncompatible
	protected void changeCustomerStatus(SalesOrder so) {
		System.out.println("In Customr Status");
		if(so.getCinfo()!=null)
    	{
			List<Customer> customerList=null;
			if(so.getCompanyId()!=null)
			{
				customerList =	ofy().load().type(Customer.class).filter("count",so.getCinfo().getCount()).
				filter("companyId",so.getCompanyId())
				.list();
    	
			}
		else
			customerList =	ofy().load().type(Customer.class).filter("count",so.getCinfo().getCount()).list();
	    
		if(customerList!=null&&customerList.size()!=0)
    	 {
    	
			if(so.getStatus().equals(SalesOrder.APPROVED))
			{
				for(Customer cust:customerList)
				{
					System.out.println("Activated");
					cust.setStatus(Customer.CUSTOMERACTIVE);
				}
			}
			
			
			System.out.println("Saved Cust fter activation");
			ofy().save().entities(customerList).now();
    	 }
		}
		
	}
	
    
	@OnSave
	@GwtIncompatible
	protected void changeQuotationStatus(SalesOrder so)
	{
    	int quotationId=so.getQuotationCount();
    	List<SalesQuotation> squotation=null;
    	if(quotationId==-1)
    		return;
    	if(so.getCompanyId()!=null)
    	{
    	   squotation =	ofy().load().type(SalesQuotation.class).filter("count",quotationId).filter("companyId",so.getCompanyId()).list();
    	}
    	
    	else{
    	   squotation =ofy().load().type(SalesQuotation.class).filter("count",quotationId).list();
    	}
    	    	
    	if(squotation==null){
    	  return;
    	}
    	if(squotation.size()==0){
    		return;
    	}
    	
    	if(so.getStatus().equals(SalesOrder.APPROVED))
    	{
    	  for(SalesQuotation temp:squotation)	
    	  {
    		  temp.setStatus(SalesQuotation.SALESQUOTATIONSUCESSFUL);
    	  }
    	}
    	
    	else if(so.getStatus().equals(SalesOrder.SALESORDERCANCEL))
    	{
    		for(SalesQuotation temp:squotation)	
      	  {
      		  temp.setStatus(SalesQuotation.SALESQUOTATIONUNSUCESSFUL);
      	  }
    	}
    	
    	ofy().save().entities(squotation).now();
    }
	
	/** 
	 * date 08/02/2018 added by komal for sales order  cancel (new)
	 */	
	@Override
	public ArrayList<CancelContract> getRecordsForSalesOrderCancel(Long companyId , int orderId, String customerName){
       UpdateServiceImpl  updateServiceImpl = new UpdateServiceImpl();

		List<AccountingInterface> accInterfaceList = new ArrayList<AccountingInterface>();
		ArrayList<CancelContract> cancelContractList = new ArrayList<CancelContract>();
		List<BillingDocument> billingList = ofy().load()
				.type(BillingDocument.class).filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPEFORSALES).list();
		System.out.println("billing" + billingList.size() + "");
		if (billingList.size() != 0) {
			for (BillingDocument bDoc : billingList) {
				if (!(bDoc.getStatus().equals(BillingDocument.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(
							bDoc.getCount(), bDoc.getBranch(), bDoc.getName(),
							bDoc.getStatus(), AppConstants.BILLINGDETAILS));
				}
			}
		}
		List<Invoice> invoiceList = ofy().load().type(Invoice.class)
				.filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPEFORSALES).list();
		System.out.println("invoice" + invoiceList.size() + "");
		if (invoiceList.size() != 0) {
			for (Invoice invoice : invoiceList) {
				if (invoice.getStatus().equals(Invoice.APPROVED)) {
					AccountingInterface acc = ofy()
							.load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", invoice.getCount())
							.filter("documentType",
									AppConstants.PROCESSCONFIGINV).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(invoice.getStatus().equalsIgnoreCase(Invoice.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(
							invoice.getCount(), invoice.getBranch(),
							invoice.getName(), invoice.getStatus(),
							invoice.getInvoiceType()));
				}
			}
		}
		List<CustomerPayment> paymentList = ofy().load()
				.type(CustomerPayment.class).filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPEFORSALES).list();
		System.out.println("payment" + paymentList.size() + "");
		if (paymentList.size() != 0) {
			for (CustomerPayment payment : paymentList) {
				if (payment.getStatus().equals(CustomerPayment.CLOSED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", payment.getCount())
							.filter("documentType", AppConstants.PAYMENT)
							.first().now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(payment.getStatus().equals(CustomerPayment.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(
							payment.getCount(), payment.getBranch(),
							payment.getName(), payment.getStatus(),
							AppConstants.PAYMENT));
				}
			}
		}

		List<WorkOrder> workOrderList = ofy().load().type(WorkOrder.class)
				.filter("companyId", companyId)
				.filter("orderId", orderId).list();
		System.out.println("work order " + workOrderList.size() + "");
		if (workOrderList.size() != 0) {
			for (WorkOrder workOrder : workOrderList) {
				if (!(workOrder.getStatus().equals(WorkOrder.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(
							workOrder.getCount(), workOrder.getBranch(),
							customerName , workOrder.getStatus(),
							AppConstants.WORK_ORDER));
				}
			}
		}
		List<DeliveryNote> deliveryNoteList = ofy().load()
				.type(DeliveryNote.class).filter("companyId", companyId)
				.filter("salesOrderCount", orderId).list();
		System.out.println("delivery note" + deliveryNoteList.size() + "");
		if (deliveryNoteList.size() != 0) {
			for (DeliveryNote dNote : deliveryNoteList) {
				if (!(dNote.getStatus().equals(DeliveryNote.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(dNote
							.getCount(), dNote.getBranch(),dNote.getCinfo().getFullName(), dNote.getStatus(), AppConstants.DELIVERYNOTE));
				}
			}
		}
		List<MaterialRequestNote> mrnList = ofy().load()
				.type(MaterialRequestNote.class).filter("companyId", companyId)
				.filter("mrnSoId", orderId).list();
		System.out.println("mrn" + mrnList.size() + "");
		if (mrnList.size() != 0) {
			for (MaterialRequestNote mrn : mrnList) {
				if (mrn.getStatus().equals(MaterialRequestNote.APPROVED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", mrn.getCount())
							.filter("documentType", AppConstants.MRN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(mrn.getStatus().equals(MaterialRequestNote.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(mrn
							.getCount(), mrn.getBranch(), customerName, mrn.getStatus(), AppConstants.MRN));
				}
			}
		}
		List<MaterialIssueNote> minList = ofy().load()
				.type(MaterialIssueNote.class).filter("companyId", companyId)
				.filter("minSoId", orderId).list();
		System.out.println("min" + minList.size() + "");
		if (minList.size() != 0) {
			for (MaterialIssueNote min : minList) {
				if (min.getStatus().equals(MaterialIssueNote.APPROVED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", min.getCount())
							.filter("documentType", AppConstants.MIN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(min.getStatus().equals(MaterialIssueNote.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(min
							.getCount(), min.getBranch(), min.getCinfo()
							.getFullName(), min.getStatus(), AppConstants.MIN));
				}
			}
		}
		List<MaterialMovementNote> mmnList = ofy().load()
				.type(MaterialMovementNote.class)
				.filter("companyId", companyId).filter("orderID", orderId + "")
				.list();
		System.out.println("mmn" + mmnList.size() + "");
		if (mmnList.size() != 0) {
			for (MaterialMovementNote mmn : mmnList) {
				if (mmn.getStatus().equals(MaterialMovementNote.APPROVED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", mmn.getCount())
							.filter("documentType", AppConstants.MMN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(mmn.getStatus().equals(MaterialMovementNote.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(
							mmn.getCount(), mmn.getBranch(), "",
							mmn.getStatus(), AppConstants.MMN));
				}
			}
		}
		List<AccountingInterface> acntsList = ofy().load()
				.type(AccountingInterface.class).filter("companyId", companyId)
				.filter("documentID", orderId)
				.filter("documentType", "Contract").list();
		acntsList.addAll(accInterfaceList);
		System.out.println("acc interface" + acntsList.size() + "");
		if (acntsList.size() != 0) {
			for (AccountingInterface accInterface : acntsList) {
				if (!(accInterface.getDocumentStatus()
						.equals(AccountingInterface.CANCELLED))) {
					cancelContractList.add(updateServiceImpl.setDataInCancelContract(
							accInterface.getCount(), accInterface.getBranch(),
							accInterface.getCustomerName(),
							accInterface.getDocumentStatus(),
							AppConstants.PROCESSTYPEACCINTERFACE));
				}
			}

		}
		System.out.println("list" + cancelContractList.size() + "");
		accInterfaceList.clear();
		
		return cancelContractList;

	}
	@Override
	public void saveSalesOrderCancelData(SalesOrder salesOrder,String remark,String loginUser ,ArrayList<CancelContract> cancelContractList){

		UpdateServiceImpl  updateServiceImpl = new UpdateServiceImpl();
		logger.log(Level.SEVERE, "Company Id " + salesOrder.getCompanyId());
		System.out.println("companyId" + salesOrder.getCompanyId());

		ArrayList<Integer> billingIdList = new ArrayList<Integer>();
		ArrayList<Integer> paymentIdList = new ArrayList<Integer>();
		ArrayList<Integer> invoiceIdList = new ArrayList<Integer>();
		ArrayList<Integer> workOrderIdList = new ArrayList<Integer>();
		ArrayList<Integer> mrnIdList = new ArrayList<Integer>();
		ArrayList<Integer> deliveryNoteIdList = new ArrayList<Integer>();
		ArrayList<Integer> minIdList = new ArrayList<Integer>();
		ArrayList<Integer> mmnIdList = new ArrayList<Integer>();
		ArrayList<CancelContract> deliveryNoteminmmnList = new ArrayList<CancelContract>();
		ArrayList<Integer> accInterfaceIdList = new ArrayList<Integer>();
		String documentType;
		String typeducumentName;
		Queue queue;
		for (CancelContract c : cancelContractList) {
			documentType = c.getDocumnetName();
			switch (documentType) {
			case AppConstants.BILLINGDETAILS:
				billingIdList.add(c.getDocumnetId());
				break;
			case AppConstants.PROCESSCONFIGINV:
				invoiceIdList.add(c.getDocumnetId());
				break;
			case AppConstants.PAYMENT:
				paymentIdList.add(c.getDocumnetId());
				break;
			case AppConstants.WORK_ORDER:
				workOrderIdList.add(c.getDocumnetId());
                break;
			case AppConstants.DELIVERYNOTE:
				deliveryNoteIdList.add(c.getDocumnetId());
				CancelContract c1 = new CancelContract();
				c1.setDocumnetId(c.getDocumnetId());
				c1.setDocumnetName(AppConstants.DELIVERYNOTE);
				deliveryNoteminmmnList.add(c1);
				break;
			case AppConstants.MRN:
				mrnIdList.add(c.getDocumnetId());
				break;
			case AppConstants.MIN:
				minIdList.add(c.getDocumnetId());
				CancelContract c2 = new CancelContract();
				c2.setDocumnetId(c.getDocumnetId());
				c2.setDocumnetName(AppConstants.MIN);
				deliveryNoteminmmnList.add(c2);
				break;
			case AppConstants.MMN:
				mmnIdList.add(c.getDocumnetId());
				CancelContract c3 = new CancelContract();
				c3.setDocumnetId(c.getDocumnetId());
				c3.setDocumnetName(AppConstants.MMN);
				deliveryNoteminmmnList.add(c3);
				break;
			case AppConstants.PROCESSTYPEACCINTERFACE:
				accInterfaceIdList.add(c.getDocumnetId());
				break;
			}
		}

		typeducumentName = "ClearListForSalesOrder" + "$" + salesOrder.getCompanyId();
		logger.log(Level.SEVERE, "Company Id " + typeducumentName);
		queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/documentCancellationQueue").param(
				"taskKeyAndValue", typeducumentName));

		List<MaterialIssueNote> minList = new ArrayList<MaterialIssueNote>();
		List<MaterialMovementNote> mmnList = new ArrayList<MaterialMovementNote>();
		List<DeliveryNote> deliveryNoteList = new ArrayList<DeliveryNote>();
		Long count1 = 0l;
		Long count2 = 0l ;
		Long mmnCount = 0l;
		NumberGeneration numberGeneration = ofy().load()
				.type(NumberGeneration.class)
				.filter("companyId", salesOrder.getCompanyId())
				.filter("processName", "ProductInventoryTransaction").first()
				.now();
		count = numberGeneration.getNumber();
		count1 = numberGeneration.getNumber();
		
		/**
		 * nidhi
		 * 3-10-2018		
		 */
				HashMap<String, Double> openingSr = new HashMap<String, Double>();
				if (deliveryNoteIdList.size() > 0) {
					deliveryNoteList = ofy().load().type(DeliveryNote.class)
							.filter("companyId", salesOrder.getCompanyId())
							.filter("count IN", deliveryNoteIdList)
							.filter("status", MaterialIssueNote.APPROVED).list();
					if (deliveryNoteList.size() > 0) {
						openingSr = deliveryNoteTransactionDetails(salesOrder.getCompanyId(), deliveryNoteList);
						
					}
				}
				
		
		if (minIdList.size() > 0) {
			minList = ofy().load().type(MaterialIssueNote.class)
					.filter("companyId", salesOrder.getCompanyId())
					.filter("count IN", minIdList)
					.filter("status", MaterialIssueNote.APPROVED).list();
			if (minList.size() > 0) {
				count2 = updateServiceImpl.minTransactionDetails(salesOrder.getCompanyId(), minList);
			}
		}
		if (mmnIdList.size() > 0) {

			mmnList = ofy().load().type(MaterialMovementNote.class)
					.filter("companyId", salesOrder.getCompanyId())
					.filter("count IN", mmnIdList)
					.filter("status", MaterialMovementNote.APPROVED).list();
			if (mmnList.size() > 0) {
				mmnCount = updateServiceImpl.mmnTransactionDetails(salesOrder.getCompanyId(), mmnList);
			}
		}
		
		numberGeneration.setNumber(count + count2 + mmnCount);
		ofy().save().entity(numberGeneration);
		Gson gson = new Gson();
		queue = QueueFactory.getQueue("documentCancellation-queue");
		if (deliveryNoteList.size() > 0) {
			String str1 = gson.toJson(deliveryNoteList);
			/**
			 * nidhi
			 * 6-09-2018
			 */
			String str2 = gson.toJson(openingSr);
			
			/**
			 * end
			 */
			String typeducumentName1 = "InventoryTransaction" + "$"
					+ salesOrder.getCompanyId() + "$" + str1 + "$" + AppConstants.DELIVERYNOTE;
			logger.log(Level.SEVERE, "Company Id " + typeducumentName1);
			/*queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/documentCancellationQueue")
					.param("taskKeyAndValue", typeducumentName1)
					.param("count", count1.toString()));*/
			/**
			 * nidhi
			 * 3-10-2018
			 */
			queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/documentCancellationQueue")
					.param("taskKeyAndValue", typeducumentName1)
					.param("count", count1.toString())
					.param("openingStr", str2));
		}
		
		if (minList.size() > 0) {
			String str = gson.toJson(minList);
			typeducumentName = "InventoryTransaction" + "$"
					+ salesOrder.getCompanyId() + "$" + str + "$" + AppConstants.MIN;
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/documentCancellationQueue")
					.param("taskKeyAndValue", typeducumentName)
					.param("count", count.toString()));
		}
		if (mmnList.size() > 0) {
			String str1 = gson.toJson(mmnList);
			String typeducumentName1 = "InventoryTransaction" + "$"
					+ salesOrder.getCompanyId() + "$" + str1 + "$" + AppConstants.MMN;
			logger.log(Level.SEVERE, "Company Id " + typeducumentName1);
			Long total = count2 + count;
			queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/documentCancellationQueue")
					.param("taskKeyAndValue", typeducumentName1)
					.param("count", total.toString()));
		}

		for (Integer billingId : billingIdList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ billingId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.BILLINGDETAILS + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		for (Integer invoiceId : invoiceIdList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ invoiceId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.PROCESSCONFIGINV + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		for (Integer paymentId : paymentIdList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ paymentId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.PAYMENT + "$" + cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		for (Integer mrnId : mrnIdList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ mrnId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.MRN + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		
		for (Integer workOrderId : workOrderIdList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ workOrderId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.WORK_ORDER + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}

		for (CancelContract cancelContract : deliveryNoteminmmnList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ cancelContract.getDocumnetId() + "$" + remark + "$"
					+ loginUser + "$" + cancelContract.getDocumnetName() + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}

		for (Integer accInterfaceId : accInterfaceIdList) {
			typeducumentName = "SelectedSalesOrderCancel" + "$"
					+ salesOrder.getCompanyId() + "$" + salesOrder.getCount() + "$"
					+ accInterfaceId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.PROCESSTYPEACCINTERFACE + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		int documentId = 0;
       if(!(salesOrder.getStatus().equalsIgnoreCase(SalesOrder.CANCELLED))){
		   	typeducumentName = "SelectedSalesOrderCancel" + "$" + salesOrder.getCompanyId()
					+ "$" + salesOrder.getCount() + "$" + salesOrder.getCount() + "$" + remark + "$"
					+ loginUser + "$" + AppConstants.SALESORDER + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", typeducumentName));
       }

	}
	
	public HashMap<String,Double> deliveryNoteTransactionDetails(Long companyId,
			List<DeliveryNote> deliveryNoteList) {/*
		HashSet<Integer> hset = new HashSet<Integer>();
		HashMap<String, Double> deliveryNoteMap = new HashMap<String, Double>();
		HashMap<String, Double> openingStockMap = new HashMap<String, Double>();
		HashMap<String, Double> closingStockMap = new HashMap<String, Double>();
		for (DeliveryNote deliveryNote : deliveryNoteList) {
			for (DeliveryLineItems product : deliveryNote.getDeliveryItems()) {
				hset.add(product.getProdId());
				String key = product.getProdId()
						+ "-"
						+ product.getWareHouseName().toUpperCase()
								.trim()
						+ "-"
						+ product.getStorageLocName()
								.toUpperCase().trim()
						+ "-"
						+ product.getStorageBinName().toUpperCase()
								.trim();
				if (deliveryNoteMap != null
						&& deliveryNoteMap.containsKey(product.getProdId()
								+ "-"
								+ product.getWareHouseName()
										.toUpperCase().trim()
								+ "-"
								+ product.getStorageLocName()
										.toUpperCase().trim()
								+ "-"
								+ product.getStorageBinName()
										.toUpperCase().trim())) {
					deliveryNoteMap.put(
							product.getProdId()
									+ "-"
									+ product.getWareHouseName()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getStorageLocName()
											.toUpperCase().trim()
									+ "-"
									+ product.getStorageBinName()
											.toUpperCase().trim(),
											deliveryNoteMap.get(product.getProdId()
									+ "-"
									+ product.getWareHouseName()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getStorageLocName()
											.toUpperCase().trim()
									+ "-"
									+ product.getStorageBinName()
											.toUpperCase().trim())
									+ product
											.getProdAvailableQuantity());
				} else {
					deliveryNoteMap.put(product.getProdId()
							+ "-"
							+ product.getWareHouseName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageLocName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageBinName()
									.toUpperCase().trim(),
							product.getProdAvailableQuantity());
				}
			}
		}

		List<Integer> prodIdList = new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList = ofy().load()
				.type(ProductInventoryView.class)
				.filter("companyId", companyId)
				.filter("productinfo.prodID IN", prodIdList).list();
		List<ProductInventoryViewDetails> prodInvViewDetailsList = ofy().load()
				.type(ProductInventoryViewDetails.class)
				.filter("companyId", companyId)
				.filter("prodid IN", prodIdList).list();
		for (ProductInventoryView productView : prodInvViewList) {
			for (ProductInventoryViewDetails pvDetails : productView
					.getDetails()) {
				double openingStock = 0;
				double closingStock = 0;
				String key1 = pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim();
				if (deliveryNoteMap.containsKey(pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim())) {
					openingStock = pvDetails.getAvailableqty();
					openingStockMap.put(pvDetails.getProdid()
							+ "-"
							+ pvDetails.getWarehousename().toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation().toUpperCase()
									.trim() + "-"
							+ pvDetails.getStoragebin().toUpperCase().trim(),
							openingStock);
					closingStock = openingStock
							+ deliveryNoteMap.get(pvDetails.getProdid()
									+ "-"
									+ pvDetails.getWarehousename()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragelocation()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragebin().toUpperCase()
											.trim());
					pvDetails.setAvailableqty(closingStock);

				}
			}
		}
		for (ProductInventoryViewDetails pvDetails : prodInvViewDetailsList) {
			double openingStock = 0;
			double closingStock = 0;
			String key = pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim();
			if (deliveryNoteMap.containsKey(pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim())) {
				openingStock = pvDetails.getAvailableqty();
				closingStock = openingStock
						+ deliveryNoteMap.get(pvDetails.getProdid()
								+ "-"
								+ pvDetails.getWarehousename().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragelocation().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragebin().toUpperCase()
										.trim());
				pvDetails.setAvailableqty(closingStock);
			}
		}
		ofy().save().entities(prodInvViewList);
		ofy().save().entities(prodInvViewDetailsList);

		for (DeliveryNote deliveryNote : deliveryNoteList) {
			for (DeliveryLineItems product : deliveryNote.getDeliveryItems()) {
				double stock = 0;
				String key = product.getProdId()
						+ "-"
						+ product.getWareHouseName().toUpperCase()
								.trim()
						+ "-"
						+ product.getStorageLocName()
								.toUpperCase().trim()
						+ "-"
						+ product.getStorageBinName().toUpperCase()
								.trim();
				if (openingStockMap.containsKey(product.getProdId()
						+ "-"
						+ product.getWareHouseName().toUpperCase()
								.trim()
						+ "-"
						+ product.getStorageLocName()
								.toUpperCase().trim()
						+ "-"
						+ product.getStorageBinName().toUpperCase()
								.trim())) {
					stock = openingStockMap.get(product.getProdId()
							+ "-"
							+ product.getWareHouseName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageLocName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageBinName()
									.toUpperCase().trim());
					product.setProdAvailableQuantity(stock);
					openingStockMap
							.put(product.getProdId()
									+ "-"
									+ product.getWareHouseName()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getStorageLocName()
											.toUpperCase().trim()
									+ "-"
									+ product.getStorageBinName()
											.toUpperCase().trim(),
									openingStockMap.get(product
											.getProdId()
											+ "-"
											+ product
													.getWareHouseName()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getStorageLocName()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getStorageBinName()
													.toUpperCase().trim())
											+ product
													.getProdAvailableQuantity());

				}
				count++;
			}
		}
		return count;
	*/

		HashMap<String,Double> returnValues  = new HashMap<String, Double>();
		HashMap<String, ArrayList<ProductSerialNoMapping>> proSer =  new HashMap<String, ArrayList<ProductSerialNoMapping>>();
		
		
		HashSet<Integer> hset = new HashSet<Integer>();
		HashMap<String, Double> deliveryNoteMap = new HashMap<String, Double>();
		HashMap<String, Double> openingStockMap = new HashMap<String, Double>();
		
		
		for (DeliveryNote deliveryNote : deliveryNoteList) {
			for (DeliveryLineItems product : deliveryNote.getDeliveryItems()) {
				hset.add(product.getProdId());
				String key = product.getProdId()
						+ "-"+ product.getWareHouseName().toUpperCase().trim()
						+ "-"+ product.getStorageLocName().toUpperCase().trim()
						+ "-"+ product.getStorageBinName().toUpperCase().trim();
				
				if (deliveryNoteMap != null
						&& deliveryNoteMap.containsKey(product.getProdId()
								+ "-"+ product.getWareHouseName().toUpperCase().trim()
								+ "-"+ product.getStorageLocName().toUpperCase().trim()
								+ "-"+ product.getStorageBinName().toUpperCase().trim())) {
					
					deliveryNoteMap.put( product.getProdId()
									+ "-"+ product.getWareHouseName().toUpperCase().trim()
									+ "-"+ product.getStorageLocName().toUpperCase().trim()
									+ "-"+ product.getStorageBinName().toUpperCase().trim(),
											deliveryNoteMap.get(product.getProdId()
									+ "-"+ product.getWareHouseName().toUpperCase().trim()
									+ "-"+ product.getStorageLocName().toUpperCase().trim()
									+ "-"+ product.getStorageBinName().toUpperCase().trim())
//									+ product.getProdAvailableQuantity());
									+ product.getQuantity());
					/**
					 * nidhi
					 * 3-10-2018
					 */
					if(proSer.containsKey(product.getProdId()
							+ "-"
							+ product.getWareHouseName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageLocName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageBinName()
									.toUpperCase().trim())){
					
					if(product.getProSerialNoDetails()!=null &&
							product.getProSerialNoDetails().get(0)!=null &&
							product.getProSerialNoDetails().get(0).size()>0){
						proSer.get(product.getProdId()
								+ "-"
								+ product.getWareHouseName()
										.toUpperCase().trim()
								+ "-"
								+ product.getStorageLocName()
										.toUpperCase().trim()
								+ "-"
								+ product.getStorageBinName()
										.toUpperCase().trim()).addAll(product.getProSerialNoDetails().get(0));
					}
				}else{
					if(product.getProSerialNoDetails()!=null &&
							product.getProSerialNoDetails().get(0)!=null &&
							product.getProSerialNoDetails().get(0).size()>0){
						proSer.put(product.getProdId()
							+ "-"
							+ product.getWareHouseName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageLocName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageBinName()
									.toUpperCase().trim(), product.getProSerialNoDetails().get(0));
					}
				}
					/**
					 * end
					 */
					
				} else {
					deliveryNoteMap.put(product.getProdId()
							+ "-"+ product.getWareHouseName().toUpperCase().trim()
							+ "-"+ product.getStorageLocName().toUpperCase().trim()
							+ "-"+ product.getStorageBinName().toUpperCase().trim(),
//							product.getProdAvailableQuantity());
							product.getQuantity());
					/**
					 * nidhi
					 * 3-10-2018
					 */
					if(product.getProSerialNoDetails()!=null &&
							product.getProSerialNoDetails().get(0)!=null &&
							product.getProSerialNoDetails().get(0).size()>0){
						proSer.put(product.getProdId()
							+ "-"
							+ product.getWareHouseName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageLocName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageBinName()
									.toUpperCase().trim(), product.getProSerialNoDetails().get(0));
					}
				}
			}
		}

		List<Integer> prodIdList = new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList = ofy().load()
				.type(ProductInventoryView.class)
				.filter("companyId", companyId)
				.filter("productinfo.prodID IN", prodIdList).list();
		List<ProductInventoryViewDetails> prodInvViewDetailsList = ofy().load()
				.type(ProductInventoryViewDetails.class)
				.filter("companyId", companyId)
				.filter("prodid IN", prodIdList).list();
		for (ProductInventoryView productView : prodInvViewList) {
			for (ProductInventoryViewDetails pvDetails : productView
					.getDetails()) {
				double openingStock = 0;
				double closingStock = 0;
				String key1 = pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim();
				if (deliveryNoteMap.containsKey(pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim())) {
					

					/**
					 * nidhi
					 * 6-09-2018
					 */
					if(!returnValues.containsValue(pvDetails.getProdid()
							+ "-"
							+ pvDetails.getWarehousename()
									.toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation()
									.toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragebin().toUpperCase()
									.trim())){
					returnValues.put(pvDetails.getProdid()
							+ "-"
							+ pvDetails.getWarehousename()
									.toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation()
									.toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragebin().toUpperCase()
									.trim(),pvDetails.getAvailableqty());
				
					}
					/**
					 * end
					 */
					
					
					
					openingStock = pvDetails.getAvailableqty();
					openingStockMap.put(pvDetails.getProdid()
							+ "-"
							+ pvDetails.getWarehousename().toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation().toUpperCase()
									.trim() + "-"
							+ pvDetails.getStoragebin().toUpperCase().trim(),
							openingStock);
					closingStock = openingStock
							+ deliveryNoteMap.get(pvDetails.getProdid()
									+ "-"
									+ pvDetails.getWarehousename()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragelocation()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragebin().toUpperCase()
											.trim());
					pvDetails.setAvailableqty(closingStock);

				}
				if (proSer.containsKey(pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim())) {
					boolean get =false;
					if(pvDetails.getProSerialNoDetails().containsValue(0)){
						get =true;
					}else{
						get =false;
					}
					ArrayList<ProductSerialNoMapping> proSr = new ArrayList<ProductSerialNoMapping>();
					ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
					
					proList.addAll(proSer.get(pvDetails.getProdid() + "-"
							+ pvDetails.getWarehousename().toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation().toUpperCase().trim()
							+ "-" + pvDetails.getStoragebin().toUpperCase().trim()));
					for(ProductSerialNoMapping pro : proList){
						 ProductSerialNoMapping element = new ProductSerialNoMapping();
						 element.setAvailableStatus(pro.isAvailableStatus());
						 element.setNewAddNo(pro.isNewAddNo());
						 element.setStatus(pro.isStatus());
						 element.setProSerialNo(pro.getProSerialNo());
						 
						 proSr.add(element);
					}
					if(get){
						pvDetails.getProSerialNoDetails().get(0).addAll(proSr);
					}else{
						pvDetails.getProSerialNoDetails().put(0,proSr);
					}
					
				}
			}
		}
		for (ProductInventoryViewDetails pvDetails : prodInvViewDetailsList) {
			double openingStock = 0;
			double closingStock = 0;
			String key = pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim();
			if (deliveryNoteMap.containsKey(pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim())) {
				openingStock = pvDetails.getAvailableqty();
				closingStock = openingStock
						+ deliveryNoteMap.get(pvDetails.getProdid()
								+ "-"
								+ pvDetails.getWarehousename().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragelocation().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragebin().toUpperCase()
										.trim());
				pvDetails.setAvailableqty(closingStock);
				
				if (proSer.containsKey(pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim())) {
					boolean get =false;
					if(pvDetails.getProSerialNoDetails().containsValue(0)){
						get =true;
					}else{
						get =false;
					}
					ArrayList<ProductSerialNoMapping> proSr = new ArrayList<ProductSerialNoMapping>();
					ArrayList<ProductSerialNoMapping> proList = new ArrayList<ProductSerialNoMapping>();
					
					proList.addAll(proSer.get(pvDetails.getProdid() + "-"
							+ pvDetails.getWarehousename().toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation().toUpperCase().trim()
							+ "-" + pvDetails.getStoragebin().toUpperCase().trim()));
					for(ProductSerialNoMapping pro : proList){
						 ProductSerialNoMapping element = new ProductSerialNoMapping();
						 element.setAvailableStatus(pro.isAvailableStatus());
						 element.setNewAddNo(pro.isNewAddNo());
						 element.setStatus(pro.isStatus());
						 element.setProSerialNo(pro.getProSerialNo());
						 
						 proSr.add(element);
					}
					if(get){
						pvDetails.getProSerialNoDetails().get(0).addAll(proSr);
					}else{
						pvDetails.getProSerialNoDetails().put(0,proSr);
					}
					
				}
			}
		}
		ofy().save().entities(prodInvViewList);
		ofy().save().entities(prodInvViewDetailsList);

		for (DeliveryNote deliveryNote : deliveryNoteList) {
			for (DeliveryLineItems product : deliveryNote.getDeliveryItems()) {
				double stock = 0;
				String key = product.getProdId()
						+ "-"
						+ product.getWareHouseName().toUpperCase()
								.trim()
						+ "-"
						+ product.getStorageLocName()
								.toUpperCase().trim()
						+ "-"
						+ product.getStorageBinName().toUpperCase()
								.trim();
				if (openingStockMap.containsKey(product.getProdId()
						+ "-"
						+ product.getWareHouseName().toUpperCase()
								.trim()
						+ "-"
						+ product.getStorageLocName()
								.toUpperCase().trim()
						+ "-"
						+ product.getStorageBinName().toUpperCase()
								.trim())) {
					stock = openingStockMap.get(product.getProdId()
							+ "-"
							+ product.getWareHouseName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageLocName()
									.toUpperCase().trim()
							+ "-"
							+ product.getStorageBinName()
									.toUpperCase().trim());
					product.setProdAvailableQuantity(stock);
					openingStockMap
							.put(product.getProdId()
									+ "-"
									+ product.getWareHouseName()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getStorageLocName()
											.toUpperCase().trim()
									+ "-"
									+ product.getStorageBinName()
											.toUpperCase().trim(),
									openingStockMap.get(product
											.getProdId()
											+ "-"
											+ product
													.getWareHouseName()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getStorageLocName()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getStorageBinName()
													.toUpperCase().trim())
											+ product
													.getProdAvailableQuantity());

				}
				count++;
			}
		}
		logger.log(Level.SEVERE,"get return values --" + returnValues.size());
		return returnValues;
	
	
	}
	/**
	* end komal
	*/
    
}
