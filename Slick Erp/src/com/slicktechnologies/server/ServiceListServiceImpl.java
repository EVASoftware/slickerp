package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.GenericServlet;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.ServiceListService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalService;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.CreateCustomerServicePdfServlet;
import com.slicktechnologies.server.addhocprinting.CreatePayRollPDFServlet;
import com.slicktechnologies.server.addhocprinting.CreateServicePdfServlet;
import com.slicktechnologies.server.addhocprinting.MaterialReportPdfServlet;
import com.slicktechnologies.server.addhocprinting.SummaryReport;
import com.slicktechnologies.server.android.ReportedServiceDetailsSaveServlet;
import com.slicktechnologies.server.android.contractwiseservice.MarkCompletedMultipleServices;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.taskqueue.DocumentNameTaskQueue;
import com.slicktechnologies.server.taskqueue.DocumentTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;
public class ServiceListServiceImpl extends RemoteServiceServlet implements ServiceListService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8349280124963228795L;

	Logger logger=Logger.getLogger("Logger");

	@Override
	public String setServicesSelectedArrayList(ArrayList<Service> serviceList) {
		ArrayList<Service> assignedserviceList = new ArrayList<Service>();
		System.out.println("assignedserviceList:::::::::::::::::::"+ assignedserviceList.size());
		ArrayList<Service> unAssignedserviceList = new ArrayList<Service>();
		System.out.println("unAssignedserviceList:::::::::::::::::::"+ unAssignedserviceList.size());
		
		for(int i=0;i<serviceList.size();i++){
			if(serviceList.get(i).getTeam() !=null && !serviceList.get(i).getTeam().equals("")){
				assignedserviceList.add(serviceList.get(i));
			}
			else{
				unAssignedserviceList.add(serviceList.get(i));
			}
		}
		System.out.println("serviceList in assignedserviceList ????????????????????????"+ assignedserviceList.size());
		System.out.println("serviceList in unAssignedserviceList ??????????????????????????"+ unAssignedserviceList.size());
		
		logger.log(Level.SEVERE,"serviceList in unAssignedserviceList:::::::::::::::"+unAssignedserviceList.size());

		/******************code for sorting*******************/		
		Collections.sort(assignedserviceList, new Comparator<Service>() {
			@Override
			public int compare(Service o1, Service o2) {
				return o1.getTeam().compareTo(o2.getTeam());
			}
		});
		
		
		/******************code for HashSet*******************/		
		HashSet<String> hs = new HashSet<String>();
		for (int i = 0; i < assignedserviceList.size(); i++) {
			if (assignedserviceList.get(i).getTeam() !=null && !assignedserviceList.get(i).getTeam().equals("")) {
				hs.add(assignedserviceList.get(i).getTeam());
			}
		}
		System.out.println("HashSet :::::::::::::::::::::"+hs.size());
		
		
		
							/************HashMap starts**************/
	HashMap<String, ArrayList<Service>> teamMap=new HashMap<String, ArrayList<Service>>(); 
	
	ArrayList<String> stringList = new ArrayList<String>();
	stringList.addAll(hs);
	
	System.out.println("stringList :::::::::::::::::::::"+stringList.size());
	
	for(int i=0; i<stringList.size(); i++){
		 ArrayList<Service> teamList = getTeamWiseServiceList(stringList.get(i), assignedserviceList);	
		 teamMap.put(stringList.get(i), teamList);
	}
	System.out.println("TeamMap:::::::::::::::::"+teamMap.size());
	
	
	
	//*****************vaishnavi***********************
	if(unAssignedserviceList.size() !=0){
		teamMap.put("Unassigned Team", unAssignedserviceList);
	}
	System.out.println("TeamMap1:::::::::::::::::"+teamMap.size());
	
	logger.log(Level.SEVERE,"TeamMap1:::::::::::::::"+teamMap.size());
	
	
//	HashMap<String, ArrayList<ProductGroupList>> hmap=new HashMap<String, ArrayList<ProductGroupList>>();
	
	HashMap<String, ArrayList<SummaryReport>> materialMap=new HashMap<String, ArrayList<SummaryReport>>();
	
	Set set = teamMap.entrySet();
    Iterator it = set.iterator();
    
    while(it.hasNext()) {
    	Map.Entry me = (Map.Entry) it.next();
    	System.out.print(me.getKey() + ": ");
    	String teamName=me.getKey().toString();
		ArrayList<Service> serList = (ArrayList<Service>) me.getValue();
		
		System.out.println(" SERVICE LIST SIZE :: "+serList.size());
		System.out.println();
		SchedulingImpl impl=new SchedulingImpl();
		for(int i=0;i<serList.size();i++){
			System.out.println("BEFOR DATE :: "+serList.get(i).getServiceDate());
			serList.get(i).setServiceDate(impl.getServiceDate(serList.get(i).getServiceDate()));
			System.out.println("AFTER DATE :: "+serList.get(i).getServiceDate());
		}
		System.out.println();
		//*********sorting date****************
		Collections.sort(serList, new Comparator<Service>() {
			@Override
			public int compare(Service o1, Service o2) {
				return o1.getServiceDate().compareTo(o2.getServiceDate());
			}
		});
		
		//*********to get unique date****************
		HashSet<Date> hset=new HashSet<Date>();
		for(Service service:serList){
			hset.add(service.getServiceDate());
		}
		System.out.println("DATE SET SIZE :: "+hset.size());
		ArrayList<Date> dateList=new ArrayList<Date>();
		dateList.addAll(hset);
		System.out.println("DATE LIST SIZE :: "+dateList.size());
		
		ArrayList<SummaryReport> matSummerryList=new ArrayList<SummaryReport>();
		
		for(int i=0;i<dateList.size();i++){
			
			ArrayList<Service>dateWiseServices=getDateWiseServiceList(dateList.get(i),serList);
			System.out.println("PARTICULAR DATE WISE SERVICE LIST SIZE :: "+dateWiseServices.size());
			ArrayList<ProductGroupList> grplist = getProductDetailTeamWise(dateWiseServices);
			if(grplist.size()!=0){
				ArrayList<SummaryReport> matyList=getMaterialSummarryServiceDateWise(grplist,dateList.get(i),teamName);
				matSummerryList.addAll(matyList);
			}
		}
		
		if(matSummerryList.size()!=0){
			materialMap.put(teamName, matSummerryList);
		}
		
//		ArrayList<ProductGroupList> grplist = getProductDetailTeamWise(serList);
//		System.out.println("GROUP LIST SIZE :: "+grplist.size());
//		hmap.put(teamName, grplist);
		
    }
    System.out.println("HASH MAP SIZE :: "+materialMap.size());
    
	
    /***********************************************************************/
    MaterialReportPdfServlet.hmap.clear();
	MaterialReportPdfServlet.hmap=materialMap;
	System.out.println("Product list size 22222 ::: "+MaterialReportPdfServlet.hmap.size());
    
	return null;

		}
	

	@Override
	public String setServicesSelectedListArrayList(ArrayList<Service> serviceList) {
		String msgString="";

		for (int i = 0; i < serviceList.size(); i++) {

			ServiceProject serProj = ofy().load().type(ServiceProject.class).filter("companyId", serviceList.get(i).getCompanyId()).filter("serviceId", serviceList.get(i).getCount()).first().now();

			if (serProj != null) {
				if(serProj.getProdDetailsList().size()!=0){
					MaterialIssueNote minObj = ofy().load().type(MaterialIssueNote.class).filter("companyId", serviceList.get(i).getCompanyId()).filter("serviceId", serviceList.get(i).getCount()).first().now();
					if(minObj!=null){
						
//						msgString=msgString+serviceList.get(i).getCount()+"-"+AppConstants.YES+"-"+AppConstants.MINCREATED+"$";
						msgString=msgString+"Min is already created for Service Id - "+serviceList.get(i).getCount()+"\n";
						
					}else{
						createMin(serProj);
						
					}
					
					
				}else{
//					msgString=msgString+serviceList.get(i).getCount()+"-"+AppConstants.YES+"-"+AppConstants.NOPRODUCT+"$";
					msgString=msgString+"Service Id - "+serviceList.get(i).getCount()+" / Product is not added in Project Id -"+serProj.getCount()+"\n";
				}
				
				
			} else {
				msgString=msgString+"Project is not created for Service Id - "+serviceList.get(i).getCount()+"\n";
//				msgString=msgString+""+serviceList.get(i).getCount()+"-"+AppConstants.NO+"$";
			}
		}

		return msgString;
	}



	public void createMin(ServiceProject serProj) {
		
		GenricServiceImpl impl=new GenricServiceImpl();
		
		MaterialIssueNote min=new MaterialIssueNote();
		
		min.setMinSoId(serProj.getContractId());
		min.setMinSoDate(serProj.getContractStartDate());
		min.setMinSalesPerson(serProj.getEmp());
		min.setMinTitle("Project-"+serProj.getCount());
		min.setMinDate(new Date());
		min.setStatus(MaterialIssueNote.CREATED);
		min.setServiceId(serProj.getserviceId());
		min.setCompanyId(serProj.getCompanyId());
		
		ArrayList<MaterialProduct> productlis=new ArrayList<MaterialProduct>();
		  
		  for(int i=0;i<serProj.getProdDetailsList().size();i++){
			  
			MaterialProduct material=new MaterialProduct();
			
			material.setMaterialProductId(serProj.getProdDetailsList().get(i).getProduct_id());
			material.setMaterialProductCode(serProj.getProdDetailsList().get(i).getCode());
			material.setMaterialProductName(serProj.getProdDetailsList().get(i).getName());
			material.setMaterialProductUOM(serProj.getProdDetailsList().get(i).getUnit());
			material.setMaterialProductRequiredQuantity(serProj.getProdDetailsList().get(i).getQuantity());
			/** date 18.3.2019 added by komal to store planned quantity for nbhc**/
			
			//Ashwini Patil Date:8-07-2024 added else as planned quantity was not getting mapped in MIN
			if(serProj.getProdDetailsList().get(i).getPlannedQty()>0)
				material.setMaterialProductPlannedQuantity(serProj.getProdDetailsList().get(i).getPlannedQty());
			else
				material.setMaterialProductPlannedQuantity(serProj.getProdDetailsList().get(i).getQuantity());
			logger.log(Level.SEVERE,"Mapping planned quantity to MIN");
			
			productlis.add(material);
		  }
		
		min.setSubProductTablemin(productlis);
		impl.save(min);
		
	}
	
	@Override
	public String printMultipleServices(ArrayList<Service> serviceList) {

//		CustomerProjectServiceImpl impl = new CustomerProjectServiceImpl();
//
//		for (int i = 0; i < serviceList.size(); i++) {
//			impl.createCustomerProject(serviceList.get(i).getCompanyId(),serviceList.get(i).getContractCount(), serviceList.get(i).getCount());
//		}
//
//		CreateCustomerServicePdfServlet.serviceList = serviceList;
		return null;
	}
	
	private ArrayList<Service> getDateWiseServiceList(Date date,ArrayList<Service> serList) {
		ArrayList<Service> list=new ArrayList<Service>();
		for(Service service:serList){
			if(date.equals(service.getServiceDate())){
				list.add(service);
			}
		}
		return list;
	}
	
	private ArrayList<Service> getTeamWiseServiceList(String teamName, ArrayList<Service> serviceList){
		ArrayList<Service> array = new ArrayList<Service>();
		for(int i=0; i<serviceList.size(); i++){
			if(serviceList.get(i).getTeam().equals(teamName)){
				array.add(serviceList.get(i));
			}
		}
		return array;
	}

	
	private ArrayList<ProductGroupList> getProductDetailTeamWise(ArrayList<Service> serviceList) {
		
		System.out.println("SERVICE LIST ::: "+serviceList.size());
			
			ArrayList<ProductGroupList> productDetailsList = new ArrayList<ProductGroupList>();
			ArrayList<ServiceProject> projectList=new ArrayList<ServiceProject>();
			
			for(int i=0;i<serviceList.size();i++){
				ServiceProject serProj=ofy().load().type(ServiceProject.class).filter("companyId",serviceList.get(i).getCompanyId()).filter("serviceId", serviceList.get(i).getCount()).first().now();
				if (serProj != null) {
					projectList.add(serProj);
				}
			}
			System.out.println("PROJECT LIST ::: "+projectList.size());
			
			if(projectList.size()!=0){
				
				for(int i=0;i<projectList.size();i++){
					logger.log(Level.SEVERE,"Size project list:::::::::::::::"+projectList.size());
					System.out.println("PROJECT PRODUCT LIST ::: "+projectList.get(i).getProdDetailsList().size());

					for (int j = 0; j < projectList.get(i).getProdDetailsList().size(); j++) {
						
						if (productDetailsList.size() == 0) {
							System.out.println("PRODUCT LIST NULL");
							System.out.println("PROJECT PRODUCT LIST ::: BEFORE "+productDetailsList.size());
							
							ProductGroupList matProd = projectList.get(i).getProdDetailsList().get(j);
							productDetailsList.add(matProd);
							
							System.out.println("PROJECT PRODUCT LIST ::: AFTER "+productDetailsList.size());
						}
						else {
							boolean flag = false;
			
							for (int k = 0; k < productDetailsList.size(); k++) {
								logger.log(Level.SEVERE,"Size Of Products"+productDetailsList.size());
									System.out.println("11111111111111111 ----- "+productDetailsList.get(k).getProduct_id());
									System.out.println("22222222222222222 ----- "+projectList.get(i).getProdDetailsList().get(j).getProduct_id());
									if (productDetailsList.get(k).getProduct_id().equals(projectList.get(i).getProdDetailsList().get(j).getProduct_id())) {
										System.out.println("PRODUCT LIST EXISTING ");
										
										ProductGroupList matProd = projectList.get(i).getProdDetailsList().get(j);
										double qty = productDetailsList.get(k).getQuantity();
										qty = qty + matProd.getQuantity();
										productDetailsList.get(k).setQuantity(qty);
										flag = true;
									}
								}

						if (flag == false) {
							System.out.println("PRODUCT LIST NEW ");
								ProductGroupList matProd = projectList.get(i).getProdDetailsList().get(j);
									productDetailsList.add(matProd);
//									logger.log(Level.SEVERE,"inside material product2"+productDetailsList.add(matProd));
									
						}
					}
				}
			}
		}
		
		System.out.println("Product list size 11111 ::: "+productDetailsList.size());
		return productDetailsList;
		
		}
	
	
	private ArrayList<SummaryReport> getMaterialSummarryServiceDateWise(ArrayList<ProductGroupList> grplist, Date date, String teamName) {
		ArrayList<SummaryReport> list=new ArrayList<SummaryReport>();
		for(ProductGroupList prodGrp:grplist){
			SummaryReport rep=new SummaryReport();
			rep.setDate(date);
			rep.setTeam(teamName);
			rep.setProduct_id(prodGrp.getProduct_id());
			rep.setProdName(prodGrp.getName());
			rep.setQuantity(prodGrp.getQuantity());
			rep.setUnit(prodGrp.getUnit());
			list.add(rep);
		}
		return list;
	}
	
	

	/******************* vijay for complete service on list *****************/

	@Override
	public ArrayList<Integer> markcompleteservice(Date serviceCompetionDate,Service service) {
		/**
		 * nidhi
		 * for service invoice mapping
		 * 25-10-2018
		 */
		HashMap<Integer, Integer> serContractList = new HashMap<Integer, Integer>();
		HashSet<Integer> contractIdSet = new HashSet<Integer>();
		HashMap<Integer, ArrayList<Integer>> contSerSet = new HashMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, ArrayList<Service>> contSerDtSet = new HashMap<Integer, ArrayList<Service>>();
		boolean serviceInvMappingFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceInvoiceMappingOnCompletion",service.getCompanyId() );
		/**
		 * end
		 */
		ArrayList<Integer> intlist = new ArrayList<Integer>();
//		ServiceProject serviceproject = ofy().load().type(ServiceProject.class).filter("serviceId",service.getCount()).
//				filter("companyId",service.getCompanyId()).first().now();
		List<ServiceProject> projEntityList=ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount()).filter("serviceId", service.getCount()).list();
		if(projEntityList!=null&&projEntityList.size() > 0){
			for (int i = 0; i < projEntityList.size(); i++) {
				if(!projEntityList.get(i).getProjectStatus().equals("Completed")){
					System.out.println("If project then");
					GenricServiceImpl genimpl = new GenricServiceImpl();
					projEntityList.get(i).setProjectStatus("Completed");
					genimpl.save(projEntityList.get(i));
					
					intlist.add(1);
					
					GenricServiceImpl genricimpl = new GenricServiceImpl();
					service.setStatus("Completed");
					service.setServiceCompletionDate(serviceCompetionDate);
					genricimpl.save(service);
					/** Date 26-06-2020 send SMS on service completion **/
					sendServiceComletionSMS(service);
					callSRCopyEmailTaskQueue(service);
					intlist.add(2);
					
				}else if(projEntityList.get(i).getProjectStatus().equals("Completed")){
					
					GenricServiceImpl genricimpl = new GenricServiceImpl();
					service.setStatus("Completed");
					service.setServiceCompletionDate(serviceCompetionDate);
					genricimpl.save(service);
					
					/** Date 26-06-2020 send SMS on service completion **/
					sendServiceComletionSMS(service);
					callSRCopyEmailTaskQueue(service);
					intlist.add(2);
					
				}else{
				GenricServiceImpl genricimpl = new GenricServiceImpl();
				service.setStatus("Completed");
				service.setServiceCompletionDate(serviceCompetionDate);
				genricimpl.save(service);
				intlist.add(2);
				//  rohan added 
				CustomerProjectServiceImpl impl = new CustomerProjectServiceImpl();
				impl.createProject(service.getCompanyId(), service.getContractCount(), service.getCount(),null);
				intlist.add(1);
				//  ends here 
				/** Date 26-06-2020 send SMS on service completion **/
				sendServiceComletionSMS(service);
				callSRCopyEmailTaskQueue(service);
			}
		}
	}else{
		/**
		 * Else condition added by vijay on 16 Feb 2017 if no project then also complete service no need to create project
		 */
		System.out.println("No project then ");
		GenricServiceImpl genricimpl = new GenricServiceImpl();
		service.setStatus("Completed");
		service.setServiceCompletionDate(serviceCompetionDate);
		genricimpl.save(service);
		intlist.add(2);
		/** Date 26-06-2020 send SMS on service completion **/
		sendServiceComletionSMS(service);
		callSRCopyEmailTaskQueue(service);		
	}
	
	/**
	 * Date : 09-11-2017 BY ANIL
	 * if service wise billing is true then following method will be executed for creating bill.
	 */
	ServiceImplementor serImpl=new ServiceImplementor();
	serImpl.updateRateContractBilling(service, false);
	/**
	 * End
	 */
	
	/**
	 * @author Anil , Date : 25-04-2020
	 * Changing complaint status to complete if service completed from customer service list
	 */
	boolean complainFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCompleteComplaintService", service.getCompanyId());
	if(complainFlag){
		serImpl.reactonCompleteComplaintService(service);
	}
	/**
	 * 
	 */
	
	/**
	 * @author Vijay Chougule Date 22-07-2020
	 * Des :- when we mark complete service from customer list (Service Table) the is service is Service wise billing then
	 * its bill will create
	 */
	
	System.out.println("status"+service.getStatus());
	if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
		ServiceImplementor serviceimpl = new ServiceImplementor();
		serviceimpl.createBillOnServiceCompletion(service);
		
	}
	
	
	/**
	 * nidhi
	 * for service invoice mapping
	 * 25-10-2018
	 */
	if(serviceInvMappingFlag) {
		if(contSerSet.containsKey(service.getContractCount())){
			contSerSet.get(service.getContractCount()).add(service.getCount());
			contSerDtSet.get(service.getContractCount()).add(service);


		}else{
			ArrayList<Integer> serList = new ArrayList<Integer>();
			serList.add(service.getCount());
			
			ArrayList<Service> serDtList = new ArrayList<Service>();
			serDtList.add(service);
			
			contSerSet.put(service.getContractCount(), serList);
			contSerDtSet.put(service.getContractCount(), serDtList);

		
		}
		contractIdSet.add(service.getContractCount());
		
			Gson gson = new Gson();
			String	jsonContSerDtSet = gson.toJson(contSerDtSet);
			
			gson = new Gson();
			String	jsonContSerSet = gson.toJson(contSerSet);
			
			gson = new Gson();
			String	jsonContractIdSet = gson.toJson(contractIdSet);
			
			Queue queue = QueueFactory.getQueue("InvoiceMapOnServiceCompletionTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/invoiceMapOnServiceCompletionTaskQueue").param("jsonContSerDtSet",jsonContSerDtSet)
									.param("jsonContSerSet",jsonContSerSet).param("jsonContractIdSet",jsonContractIdSet)
									.param("companyId", service.getCompanyId()+""));
			
			
			
	}
	return intlist;
}
	
	//Ashwini Patil Date:22-07-2022 As per Universal Pest requirement sending sr copy email when service is getting completed from erp
	public void callSRCopyEmailTaskQueue(Service service) {
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", service.getCompanyId())){

			logger.log(Level.SEVERE, "startSREmailSendProcess called serviceid ="+service.getCount() );
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			format1.setTimeZone(TimeZone.getTimeZone("IST"));
			String srNumber = format1.format(new Date());
			String branchEmail="";
			String taskName="SendSRCopyEmail"+"$"+service.getCompanyId()+"$"+service.getCount()+"$"+srNumber+"$"+branchEmail;
			logger.log(Level.SEVERE, "task call= "+taskName );
			Queue queue = QueueFactory.getQueue("SRCopyEmailQueueRevised-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/SRCopyEmailTaskQueue").param("taskKeyAndValue", taskName)
					.param("serviceid", service.getCount()+"").etaMillis(System.currentTimeMillis()+180000));
		}
	}


	/**
	 * Date 10 oct 2017 added by vijay for Technician Scheduling list Mark Complete
	 * its same as old logic inservice list mark complte
	 */


	@Override
	public ArrayList<Integer> TechnicianSchedulingListMarkcompleteservice(Service service, String loggedInUser, int projectId , ArrayList<ProductGroupList> materialList) {

		MaterialIssueNote minObj = ofy().load().type(MaterialIssueNote.class).filter("companyId", service.getCompanyId()).filter("serviceId",service.getCount())
				.filter("minSoId", service.getContractCount()).first().now();
		logger.log(Level.SEVERE,"MIN === " + minObj);
		
		if(minObj != null && minObj.getStatus().equalsIgnoreCase(MaterialIssueNote.APPROVED)){
			UpdateServiceImpl impl = new UpdateServiceImpl();
			minObj.setStatus(MaterialIssueNote.CANCELLED);
			minObj.setMinDescription("MIN Id ="
						+ minObj.getCount() + " " + "MIN Status = Approved"	+ "\n"
						+ "has been cancelled by "
						+ loggedInUser
						+ " with remark." + "\n" + "Remark ="
						+ "MIN Cancelled due to return quantity change in service.");
			String result = impl.cancelMIN(minObj);
			logger.log(Level.SEVERE,"cancel MIN === " + result);
		}
		ArrayList<Integer> intlist = new ArrayList<Integer>();
		
		logger.log(Level.SEVERE,"MIN === ");
		System.out.println("MIN === ");
		System.out.println("Company Id =="+service.getCompanyId());
		System.out.println("Service Id =="+service.getCount());
		System.out.println("Project Id"+service.getProjectId());
		ServiceProject projEntity=ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", service.getCount()).filter("count", projectId).first().now();
		System.out.println(projEntity);
		
		if(projEntity!=null){
			projEntity.setProdDetailsList(materialList);
				ofy().save().entity(projEntity);
				logger.log(Level.SEVERE,"11111111");
				if(projEntity.getProdDetailsList().size()!=0){
				if(projEntity.getProdDetailsList().get(0).getWarehouse()!=null && !projEntity.getProdDetailsList().get(0).getWarehouse().equals("") &&
						projEntity.getProdDetailsList().get(0).getStorageLocation()!=null && !projEntity.getProdDetailsList().get(0).getStorageLocation().equals("") &&
						projEntity.getProdDetailsList().get(0).getStorageBin()!=null && !projEntity.getProdDetailsList().get(0).getStorageBin().equals("")
						){
					System.out.println("MIN Creating");
					logger.log(Level.SEVERE,"MIN Creating");

					CreateMIN(projEntity, service.getBranch(),loggedInUser,false); 
				}
			}
				
			intlist.add(2);
			
		}
		
		return intlist;
	}

	
	/**
	 * Date 11 oct 2017 added by vijay for Technician scheduling Creating MIN
	 * @param serProj
	 * @param loggedInUser 
	 * @param string 
	 */

	public void CreateMIN(ServiceProject serProj, String branch, String loggedInUser, boolean androidFlag) {

		
		GenricServiceImpl impl=new GenricServiceImpl();
		
		MaterialIssueNote min=new MaterialIssueNote();
		
		min.setMinSoId(serProj.getContractId());
		min.setMinSoDate(serProj.getContractStartDate());
		min.setMinSalesPerson(serProj.getEmp());
		min.setMinTitle("Project-"+serProj.getCount());
		min.setMinDate(new Date());
		min.setStatus(MaterialIssueNote.APPROVED);
		min.setServiceId(serProj.getserviceId());
		min.setCompanyId(serProj.getCompanyId());
		
		if(serProj.getPersonInfo()!=null)
			min.setCinfo(serProj.getPersonInfo());
		
		min.setApproverName(loggedInUser);
		min.setMinIssuedBy(loggedInUser);
		min.setEmployee(loggedInUser);
		min.setBranch(branch);
		min.setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
		
		ArrayList<MaterialProduct> productlis=new ArrayList<MaterialProduct>();
		 
		  
		  for(int i=0;i<serProj.getProdDetailsList().size();i++){
			  
			MaterialProduct material=new MaterialProduct();
			
			material.setMaterialProductId(serProj.getProdDetailsList().get(i).getProduct_id());
			material.setMaterialProductCode(serProj.getProdDetailsList().get(i).getCode());
			material.setMaterialProductName(serProj.getProdDetailsList().get(i).getName());
			material.setMaterialProductUOM(serProj.getProdDetailsList().get(i).getUnit());
			
			/**
			 * @author Anil @since 01-09-2021
			 * if service is completed from Plan button from customer service list then MIN was not created with product details
			 * Issue raised by Rahul Tiwari for Surat Pest Control
			 * This method was also used for creating MIN from android app
			 * Added androide flag 
			 */
			
			/** date 16.3.2019 added by komal to create MIN based on consumed quantity **/
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity", serProj.getCompanyId())&& androidFlag){
			//	if(serProj.getProdDetailsList().get(i).getReturnQuantity()!=0){
					System.out.println("IF consume then");
					material.setMaterialProductRequiredQuantity(serProj.getProdDetailsList().get(i).getReturnQuantity());
			//	}
//				else{
//					System.out.println("NO  return then");
//					material.setMaterialProductRequiredQuantity(serProj.getProdDetailsList().get(i).getQuantity());
//				}
			}else{
				if(serProj.getProdDetailsList().get(i).getReturnQuantity()!=0){
					System.out.println("IF return then");
					material.setMaterialProductRequiredQuantity(serProj.getProdDetailsList().get(i).getQuantity()-serProj.getProdDetailsList().get(i).getReturnQuantity());
				}else{
					System.out.println("NO  return then");
					material.setMaterialProductRequiredQuantity(serProj.getProdDetailsList().get(i).getQuantity());
				}
			}
			
			/** date 18.3.2019 added by komal to store planned quantity for nbhc**/
			//Ashwini Patil Date:8-07-2024 added else as planned quantity was not getting mapped in MIN
			if(serProj.getProdDetailsList().get(i).getPlannedQty()>0)
				material.setMaterialProductPlannedQuantity(serProj.getProdDetailsList().get(i).getPlannedQty());
			else
				material.setMaterialProductPlannedQuantity(serProj.getProdDetailsList().get(i).getQuantity());
			logger.log(Level.SEVERE,"Mapping planned quantity to MIN");
			
			material.setMaterialProductWarehouse(serProj.getProdDetailsList().get(i).getWarehouse());
			material.setMaterialProductStorageLocation(serProj.getProdDetailsList().get(i).getStorageLocation());
			material.setMaterialProductStorageBin(serProj.getProdDetailsList().get(i).getStorageBin());
			
			/**
			 * @author Vijay Chougule Date :- 01-09-2020
			 * Des :- if material quantity is 0 then it should not include in MIN document as Per Nitin Sir
			 */
			if(material.getMaterialProductRequiredQuantity()!=0){
				productlis.add(material);
			}
			
			
		  }
		
		min.setSubProductTablemin(productlis);
		min.setCreatedFromApp(androidFlag);
		impl.save(min);
		
	
		
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(MaterialProduct product:min.getSubProductTablemin()){
			InventoryTransactionLineItem item=new InventoryTransactionLineItem();
			item.setCompanyId(min.getCompanyId());
			item.setBranch(min.getBranch());
			item.setDocumentType(AppConstants.MIN);
			item.setDocumentId(min.getCount());
			item.setDocumentDate(min.getCreationDate());
			item.setDocumnetTitle(min.getMinTitle());
			if(min.getServiceId()>0)
				item.setServiceId(min.getServiceId()+"");
			item.setProdId(product.getMaterialProductId());
			item.setUpdateQty(product.getMaterialProductRequiredQuantity());
			item.setOperation(AppConstants.SUBTRACT);
			item.setWarehouseName(product.getMaterialProductWarehouse());
			item.setStorageLocation(product.getMaterialProductStorageLocation());
			item.setStorageBin(product.getMaterialProductStorageBin());
			itemList.add(item);
		}
		if(itemList.size()!=0)
		UpdateStock.setProductInventory(itemList);
		
		
		accountingInterface(min);
		
	}
	
	/**************Accounting Interface*********************
	 * Created by Rahul Verma on 24 April 2017
	 * Created for Interface with other system
	 * @param min 
	 */
	@GwtIncompatible
	private void accountingInterface(MaterialIssueNote min) {

		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", min.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGMIN)
					.filter("companyId", min.getCompanyId())
					.filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag = 0;

		if (processConfig != null) {
			System.out.println("Process Config Not Null");
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				System.out
						.println("----------------------------------------------------------");
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i)
								.getProcessType());
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i).isStatus());
				System.out
						.println("----------------------------------------------------------");
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {
			System.out.println("ExecutingAccInterface");

			if (min.getCount() != 0) {

				System.out.println("Product List Size :: "
						+ min.getProductTablemin().size());

				/*************************************************************************************************************/
				for (int i = 0; i < min.getSubProductTablemin().size(); i++) {

					/************************************************* Start ************************************************/

					String unitofmeasurement = min.getSubProductTablemin()
							.get(i).getMaterialProductUOM();
					int prodId = min.getSubProductTablemin().get(i)
							.getMaterialProductId();
					String productCode = min.getSubProductTablemin().get(i)
							.getMaterialProductCode();
					String productName = min.getSubProductTablemin().get(i)
							.getMaterialProductName();
					double productQuantity = min.getSubProductTablemin()
							.get(i).getMaterialProductRequiredQuantity();
					double availableQuantity = min.getSubProductTablemin()
							.get(i).getMaterialProductAvailableQuantity();
					String remarks = min.getSubProductTablemin().get(i)
							.getMaterialProductRemarks().trim();
					String warehouseName = min.getSubProductTablemin().get(i)
							.getMaterialProductWarehouse().trim();
					System.out.println("warehouseName" + warehouseName);
					WareHouse warehouse = ofy().load().type(WareHouse.class)
							.filter("companyId", min.getCompanyId())
							.filter("buisnessUnitName", warehouseName).first()
							.now();
					System.out.println("warehouse" + warehouse);
					String warehouseCode = "";
					if (warehouse.getWarehouseCode().trim() != null) {
						warehouseCode = warehouse.getWarehouseCode().trim();
					}
					String storageLocation = min.getSubProductTablemin()
							.get(i).getMaterialProductStorageLocation().trim();
					String storageBin = min.getSubProductTablemin().get(i)
							.getMaterialProductStorageBin().trim();

					UpdateAccountingInterface.updateTally(
							new Date(),// accountingInterfaceCreationDate
							min.getEmployee(),// accountingInterfaceCreatedBy
							min.getStatus(),
							AppConstants.STATUS,// Status
							"",// remark
							"Inventory",// module
							"MIN",// documentType
							min.getCount(),// docID
							min.getMinTitle(),// DOCtile
							min.getMinDate(), // docDate
							AppConstants.DOCUMENTGL,// docGL
							min.getMinMrnId() + "",// ref doc no.1 (Work Order
													// Id)
							min.getMinMrnDate(),// ref doc date.1 (Work Order
													// Date)
							AppConstants.REFDOCMRN,// ref doc type 1
							min.getMinSoId() + "",// ref doc no 2 (Order ID)
							min.getMinSoDate(),// ref doc date 2 (Order Date)
							AppConstants.REFDOCMIN,// ref doc type 2
							"",// accountType
							0,// custID
							"",// custName
							0l,// custCell
							0,// vendor ID
							"",// vendor NAme
							0l,// vendorCell
							0,// empId
							" ",// empNAme
							min.getBranch(),// branch
							min.getMinIssuedBy(),// personResponsible (Issued
													// By)
							min.getEmployee(),// requestedBY
							min.getApproverName(),// approvedBY
							"",// paymentmethod
							null,// paymentdate
							"",// cheqno.
							null,// cheqDate
							null,// bankName
							null,// bankAccount
							0,// transferReferenceNumber
							null,// transactionDate
							null,// contract start date
							null, // contract end date
							prodId,// prod ID
							productCode,// prod Code
							productName,// productNAme
							productQuantity,// prodQuantity
							null,// productDate
							0,// duration
							0,// services
							unitofmeasurement,// unit of measurement
							0.0,// productPrice
							0.0,// VATpercent
							0.0,// VATamt
							0,// VATglAccount
							0.0,// serviceTaxPercent
							0.0,// serviceTaxAmount
							0,// serviceTaxGLaccount
							"",// cform
							0,// cformpercent
							0,// cformamount
							0,// cformGlaccount
							0.0,// totalamouNT
							0.0,// NET PAPAYABLE
							"",// Contract Category
							0,// amount Recieved
							0.0,// base Amt for tdscal in pay by rohan
							0.0, // tds percentage by rohan
							0.0,// tds amount by rohan
							"", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "",
							0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0,
							"", 0.0, "", "", "", "", "", "", 0l,
							min.getCompanyId(), null, // billing from date
														// (rohan)
							null, // billing to date (rohan)
							warehouseName, // Warehouse
							warehouseCode, // warehouseCode
							"", // ProductRefId
							AccountingInterface.OUTBOUNDDIRECTION, // Direction
							AppConstants.CCPM, // sourceSystem
							AppConstants.NBHC, // Destination System
							null,null,null
							);

				}

			}
		}
	}

	
	/**
	 * Date 04-05-2018
	 * Developer :- Vijay
	 * Description :- services list complete services with bulk  
	 */
	
	@Override
	public Integer markCompleteServicesWithBulk(ArrayList<Service> serviceList,String remark, Date serviceCompletionDate) {
		logger.log(Level.SEVERE,"Service mark completion with bulk");
		logger.log(Level.SEVERE,"Service mark completion with bulk tech remark and rating is"+serviceList.get(0).getTechnicianRemark()+"-"+serviceList.get(0).getCustomerFeedback());

		
		/**
		 * Date 30-11-2018 By Vijay
		 * Des :- if process config is active then if service is servicewisebilling then its bill
		 * will create. and this applicable max to max 100 services only
		 */
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableServiceWiseBillingServiceListBulkComplete", serviceList.get(0).getCompanyId())){
//			
//			if(serviceList.size()<=100){
//				for(int i=0;i<serviceList.size();i++){
//					ServiceImplementor serviceimplementor = new ServiceImplementor();
//					serviceimplementor.createBillOnServiceCompletion(serviceList.get(i));
//				}
//				multipleServicesMarkComplete(serviceList, remark, serviceCompletionDate);
//
//			}else{
//				return -2;
//			}
//		
//		}
//		else{
//			if(serviceList.size()>50){
				logger.log(Level.SEVERE,"Service mark completion with bulk, Task queue ");
				
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String strCompletionDate ="NoDate";
				if(serviceCompletionDate!=null)
					strCompletionDate = df.format(serviceCompletionDate);
				logger.log(Level.SEVERE,"strCompletionDate =="+strCompletionDate);
			//	DocumentTaskQueue.servicelist = serviceList;
				List<Integer> serviceIdList = new ArrayList<Integer>();
				long companyId = 0;
				for(Service ser : serviceList){
					serviceIdList.add(ser.getCount());
					companyId = ser.getCompanyId();
				}
				Gson gson = new Gson();
				String str = gson.toJson(serviceIdList);

				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "ServiceMarkComplete").param("Remark", remark)
						.param("ServiceCompletionDate", strCompletionDate).param("serviceId", str).param("companyId", companyId+""));
				
				
//			}
//			else{
//				logger.log(Level.SEVERE,"Service mark completion with bulk Normal RPC for less than 100 services");
//				multipleServicesMarkComplete(serviceList, remark, serviceCompletionDate);
//			}
//		}

		return 1;
	}

	/**
	 * Date 04-05-2018
	 * Developer :- Vijay
	 * Description :- services list complete services with bulk  
	 */

	public void multipleServicesMarkComplete(ArrayList<Service> serviceList, String remark, Date serviceCompletionDate) {
		GenricServiceImpl genricimpl = new GenricServiceImpl();
		/**
		 * nidhi
		 * for service invoice mapping
		 * 25-10-2018
		 */
		HashMap<Integer, Integer> serContractList = new HashMap<Integer, Integer>();
		HashSet<Integer> contractIdSet = new HashSet<Integer>();
		HashMap<Integer, ArrayList<Integer>> contSerSet = new HashMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, ArrayList<Service>> contSerDtSet = new HashMap<Integer, ArrayList<Service>>();
		boolean serviceInvMappingFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceInvoiceMappingOnCompletion",serviceList.get(0).getCompanyId() );
		/**
		 * end
		 */
		/**
		 * Date 23-05-2019 by Vijay
		 * Des :- NBHC CCPM if complaint service completed then complaint also mark complete  
		 */
		ServiceImplementor markcompletecomplaint = new ServiceImplementor();
		boolean markCompletecomplainFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCompleteComplaintService", serviceList.get(0).getCompanyId());
		/**
		 * ends here
		 */
		for(int i=0;i<serviceList.size();i++){
		List<ServiceProject> projEntityList=ofy().load().type(ServiceProject.class).filter("companyId", serviceList.get(i).getCompanyId()).filter("contractId", serviceList.get(i).getContractCount()).filter("serviceId", serviceList.get(i).getCount()).list();
		if(projEntityList!=null&&projEntityList.size() > 0){
			for (int j = 0; j < projEntityList.size(); j++) {
				if(!projEntityList.get(j).getProjectStatus().equals("Completed")){
					System.out.println("If project then");
					projEntityList.get(j).setProjectStatus("Completed");
					genricimpl.save(projEntityList.get(j));
					
					serviceList.get(i).setStatus("Completed");
					if(serviceCompletionDate!=null){
						serviceList.get(i).setServiceCompletionDate(serviceCompletionDate);
					}else{
						serviceList.get(i).setServiceCompletionDate(serviceList.get(i).getServiceDate());
					}
					serviceList.get(i).setServiceCompleteRemark(remark);
					genricimpl.save(serviceList.get(i));
					
					/** Date 26-06-2020 send SMS on service completion **/
					sendServiceComletionSMS(serviceList.get(i));
					callSRCopyEmailTaskQueue(serviceList.get(i));
					
				}else if(projEntityList.get(j).getProjectStatus().equals("Completed")){
					
					serviceList.get(i).setStatus("Completed");
					if(serviceCompletionDate!=null){
						serviceList.get(i).setServiceCompletionDate(serviceCompletionDate);
					}else{
						serviceList.get(i).setServiceCompletionDate(serviceList.get(i).getServiceDate());
					}
					serviceList.get(i).setServiceCompleteRemark(remark);
					genricimpl.save(serviceList.get(i));
					
					/** Date 26-06-2020 send SMS on service completion **/
					sendServiceComletionSMS(serviceList.get(i));
					callSRCopyEmailTaskQueue(serviceList.get(i));
					
				}else{
					serviceList.get(i).setStatus("Completed");
					if(serviceCompletionDate!=null){
						serviceList.get(i).setServiceCompletionDate(serviceCompletionDate);
					}else{
						serviceList.get(i).setServiceCompletionDate(serviceList.get(i).getServiceDate());
					}
					serviceList.get(i).setServiceCompleteRemark(remark);
					genricimpl.save(serviceList.get(i));
					
					/** Date 26-06-2020 send SMS on service completion **/
					sendServiceComletionSMS(serviceList.get(i));
					callSRCopyEmailTaskQueue(serviceList.get(i));
			}
				updateInventoryAfterServiceCompletionFromErp(serviceList.get(i),projEntityList.get(j));
		}
	}else{
		/**
		 * Else condition added by vijay on 16 Feb 2017 if no project then also complete service no need to create project
		 */
		System.out.println("No project then ");
		serviceList.get(i).setStatus("Completed");
		if(serviceCompletionDate!=null){
			serviceList.get(i).setServiceCompletionDate(serviceCompletionDate);
		}else{
			serviceList.get(i).setServiceCompletionDate(serviceList.get(i).getServiceDate());
		}
		serviceList.get(i).setServiceCompleteRemark(remark);
		genricimpl.save(serviceList.get(i));
		
		/** Date 26-06-2020 send SMS on service completion **/
		sendServiceComletionSMS(serviceList.get(i));
		callSRCopyEmailTaskQueue(serviceList.get(i));
	}
		
		/**
		 * nidhi
		 * for service invoice mapping
		 * 25-10-2018
		 */
		if(serviceInvMappingFlag) {
			if(contSerSet.containsKey(serviceList.get(i).getContractCount())){
				contSerSet.get(serviceList.get(i).getContractCount()).add(serviceList.get(i).getCount());
				

			}else{
				ArrayList<Integer> serList = new ArrayList<Integer>();
				serList.add(serviceList.get(i).getCount());
				
				
				
				contSerSet.put(serviceList.get(i).getContractCount(), serList);
				

			
			}
			
			if(contSerDtSet.containsKey(serviceList.get(i).getContractCount())){
				contSerDtSet.get(serviceList.get(i).getContractCount()).add(serviceList.get(i));

			}else{
				ArrayList<Service> serDtList = new ArrayList<Service>();
				serDtList.add(serviceList.get(i));
				
				contSerDtSet.put(serviceList.get(i).getContractCount(), serDtList);
			}
			contractIdSet.add(serviceList.get(i).getContractCount());
		}
		/**
		 * Date 09-11-2019
		 * @author Vijay Chougule
		 * Des :- NBHC CCPM WMS Fumigation service TCompletetion creating Deggasing Service
		 * Degassing Service normaly create from app when technician marked as Tcompleted. 
		 * when we create degassing service for perticular fumigation service then updating degassing service id in fumigation service
		 * In short  service.getDeggassingServiceId()!=0 then from here degassing service will create
		 */
		ArrayList<Service> wmsFumigationservicelist = new ArrayList<Service>();
		if(serviceList.get(i).isWmsServiceFlag() && serviceList.get(i).getProduct()!=null && serviceList.get(i).getProduct().getProductCode().trim().equals("STK-01")
				&& serviceList.get(i).getDeggassingServiceId()==0 ){
			wmsFumigationservicelist.add(serviceList.get(i));
		}
		
		/**
		 * Date 09-11-2019
		 * @author Vijay Chougule
		 * Des :- NBHC CCPM WMS Fumigation service completetion creating Deggasing Service
		 * @param productCode 
		 */
		logger.log(Level.SEVERE, "wmsFumigationservicelist"+wmsFumigationservicelist.size());
		if(wmsFumigationservicelist.size()!=0){
			ReportedServiceDetailsSaveServlet service = new ReportedServiceDetailsSaveServlet();
			service.createFumigationDeggassingService(wmsFumigationservicelist,"DSSG",null);
		}
		/**
		 * ends here
		 */
		
		/**
		 * Date 10-12-2019 by Vijay 
		 * Des :- Degassing Services Tcompleted then updated this status in its fumigation Service
		 */
		if(serviceList.get(i).isWmsServiceFlag() && serviceList.get(i).getProduct()!=null && serviceList.get(i).getProduct().getProductCode().trim().equals("DSSG")){
			if(serviceList.get(i).getFumigationServiceId()!=0){
				Service fumigationService = ofy().load().type(Service.class).filter("companyId", serviceList.get(i).getCompanyId())
						.filter("count", serviceList.get(i).getFumigationServiceId()).first().now();
				if(fumigationService!=null){
					fumigationService.setDegassingServiceStatus(Service.SERVICESTATUSCOMPLETED);
					ofy().save().entities(fumigationService);
					logger.log(Level.SEVERE, "Deggasing Service status updated in its fumigation service");
				}
			}
			
		}
		logger.log(Level.SEVERE, "hew we are now");
		/**
		 * ends here
		 */
		/** Date 14-11-2019 if comlaint service then its complaint also must complete NBHC CCPM ***/
		if(serviceList.get(i).getTicketNumber()!=-1 && markCompletecomplainFlag){
			markcompletecomplaint.reactonCompleteComplaintService(serviceList.get(i));
		}
		
		/**
		 * @author Vijay Chougule Date 23-12-2019
		 * project :- Fumigation Tracker
		 * Des :- when wms services mark completed then updating its status in fumigationReportEntity
		 */
		if(serviceList.get(i).isWmsServiceFlag()){
			logger.log(Level.SEVERE, "Updating fumigation report entity when service is marked completed");
			MarkCompletedMultipleServices updateFumigationReport = new MarkCompletedMultipleServices();
			updateFumigationReport.UpdateServiceCompletetionInFumigationServiceReport(serviceList.get(i));
		}
		/**
		 * ends here
		 */
		if(i == serviceList.size()-1){
			if(serviceInvMappingFlag) {
				Gson gson = new Gson();
				String	jsonContSerDtSet = gson.toJson(contSerDtSet);
				
				gson = new Gson();
				String	jsonContSerSet = gson.toJson(contSerSet);
				
				gson = new Gson();
				String	jsonContractIdSet = gson.toJson(contractIdSet);
				
				Queue queue = QueueFactory.getQueue("InvoiceMapOnServiceCompletionTaskQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/invoiceMapOnServiceCompletionTaskQueue").param("jsonContSerDtSet",jsonContSerDtSet)
										.param("jsonContSerSet",jsonContSerSet).param("jsonContractIdSet",jsonContractIdSet)
										.param("companyId", serviceList.get(i).getCompanyId()+""));
				
			}
			
			if(DocumentTaskQueue.servicelist!=null){
				DocumentTaskQueue.servicelist.clear();
			}
		}
		/**
		 * end
		 */
		
		/*if(i == serviceList.size()-1){
			if(DocumentTaskQueue.servicelist!=null){
				DocumentTaskQueue.servicelist.clear();
			}
		}*/
	}
		
		/**
		 * @author Anil
		 * @since 12-05-2020
		 */
		updateComplainDocument(serviceList, false, false, false, true);
		
//		/**
//		 * Date 09-11-2019
//		 * @author Vijay Chougule
//		 * Des :- NBHC CCPM WMS Fumigation service completetion creating Deggasing Service
//		 * @param productCode 
//		 */
//		if(wmsFumigationservicelist.size()!=0){
//			ReportedServiceDetailsSaveServlet service = new ReportedServiceDetailsSaveServlet();
//			service.createFumigationDeggassingService(wmsFumigationservicelist,"DSSG",null);
//		}
//		/**
//		 * ends here
//		 */
			
		
	}
	
	/**
	 * ends here
	 */
	

	@Override
	public Integer checkScheduleServices(ArrayList<Integer> contractCount,
			Date serDate, Date billDate) {
		int serCount =0;
		
		List<String> statusList = new ArrayList<String>();
        statusList.add(Service.SERVICESTATUSSCHEDULE);
        statusList.add(Service.SERVICESTATUSRESCHEDULE);
        
        if(serDate!=null && billDate !=null){
        	 List<Service> serlist = ofy().load().type(Service.class).filter("contractCount IN", contractCount)
        			 .filter("serviceDate >=", serDate).filter("serviceDate <=", billDate).filter("status IN", statusList).list();
        			
        	 if(serlist!=null){
        		 serCount = serlist.size();
        	 }
        	 
        }else if(serDate == null && billDate !=null){
        	
        	List<Service> serlist = ofy().load().type(Service.class).filter("contractCount IN", contractCount)
    			 .filter("serviceDate <", billDate).filter("status IN", statusList).list();
        	if(serlist!=null){
       		 serCount = serlist.size();
       	 }
        }
       
		return serCount;
	}
	
	/**
	 * ends here
	 */
	
	@Override
	public Integer setServiceTechnicianToServices(ArrayList<Service> serviceList, String remark,String techEmployee,Long companyId) {
		
		/**
		 * @author Anil @since 23-03-2021
		 * In case after scheduling of services we are changing technician then stock to be allocated to new technician
		 * requirement raised by Nitin Sir for Pestico
		 */
		ArrayList<Service> toUpdateServiceList=new ArrayList<Service>();
		ArrayList<ProductGroupList> productGroupList=new ArrayList<ProductGroupList>();
		boolean stockUpdateFlag=false;
		ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
		ArrayList<Integer> contractIdList=new ArrayList<Integer>();
		
		Integer count =0 ;
		Employee info = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", techEmployee).first().now();
		if(serviceList!=null){
			for(int i = 0 ; i<serviceList.size(); i++){
				
				/**
				 * @author Anil @since 23-03-2021
				 */
				try{
					if(serviceList.get(i).isServiceScheduled()){
						if(!serviceList.get(i).getEmployee().equals(techEmployee)){
							toUpdateServiceList.add(serviceList.get(i));
							serviceIdList.add(serviceList.get(i).getCount());
							contractIdList.add(serviceList.get(i).getContractCount());
							stockUpdateFlag=true;
						}
					}
				}catch(Exception e){
					
				}
				
				
				serviceList.get(i).setEmployee(techEmployee);
				count++;
			}
			ofy().save().entities(serviceList).now();
		}
		if(serviceList!=null){
			for(int i = 0 ; i<serviceList.size(); i++){
				ServiceProject serProj = ofy().load().type(ServiceProject.class).filter("companyId", serviceList.get(i).getCompanyId()).filter("serviceId", serviceList.get(i).getCount()).first().now();
				if(serProj != null){
					serProj.setEmp(techEmployee);
					if(info != null){
						EmployeeInfo empInfoEntity = new EmployeeInfo();
				  		
				  		empInfoEntity.setEmpCount(info.getCount());
				  		empInfoEntity.setFullName(info.getFullName());
				      	empInfoEntity.setCellNumber(info.getCellNumber1());
				      	empInfoEntity.setDesignation(info.getDesignation());
				      	empInfoEntity.setDepartment(info.getDepartMent());
				      	empInfoEntity.setEmployeeType(info.getEmployeeType());
				      	empInfoEntity.setEmployeerole(info.getRoleName());
				      	empInfoEntity.setBranch(info.getBranchName());
				      	empInfoEntity.setCountry(info.getCountry());
				      	
				      	if(serProj.getTechnicians() != null && serProj.getTechnicians().size() > 0){
				      		serProj.getTechnicians().set(0, empInfoEntity);
				      	}else{
				      		List<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
				      		list.add(empInfoEntity);
				      		serProj.setTechnicians(list);
				      	}
					}
					ofy().save().entity(serProj).now();
				}
				
			}
			
		}
		
		/**
		 * @author Anil
		 * @since 12-05-2020
		 */
		updateComplainDocument(serviceList, false, true, false, false);
		
		try{
			if(stockUpdateFlag){
				if(serviceIdList.size()!=0&&contractIdList.size()!=0){
					GeneralServiceImpl impl=new GeneralServiceImpl();
					productGroupList=impl.getCombinedMaterialList(companyId, serviceIdList, contractIdList);
					TechnicianWareHouseDetailsList techWh=ofy().load().type(TechnicianWareHouseDetailsList.class).filter("companyId", companyId).filter("wareHouseList.technicianName", techEmployee).first().now();
					if(techWh!=null){
						
						for(TechnicianWarehouseDetails details : techWh.getWareHouseList()){
							if(details.getTechnicianName().equalsIgnoreCase(techEmployee)){
								
								for(ProductGroupList object:productGroupList){
									object.setParentWarentwarehouse(details.getParentWareHouse());
									object.setParentStorageLocation(details.getParentStorageLocation());
									object.setParentStorageBin(details.getPearentStorageBin());
									
									object.setWarehouse(details.getTechnicianWareHouse());
									object.setStorageLocation(details.getTechnicianStorageLocation());
									object.setStorageBin(details.getTechnicianStorageBin());
									object.setCreatedBy(techEmployee);
								}
								
								break;
							}
						}
						
						if(productGroupList.size()!=0){
							impl.createMMNForTechnician(companyId, productGroupList, techEmployee, serviceIdList);
						}
					}else{
						for(ProductGroupList object:productGroupList){
							object.setParentWarentwarehouse("");
							object.setParentStorageLocation("");
							object.setParentStorageBin("");
							
							object.setWarehouse("");
							object.setStorageLocation("");
							object.setStorageBin("");
							
							object.setCreatedBy(techEmployee);
						}
					}
					
				}
			}
		}catch(Exception e){
			
		}
		
		
		return count;
	}
	
	/**
	 * ends here
	 */

	
	/**
	 * Updated By:Viraj
	 * Date:17-06-2019
	 * Description: service list to assign branch
	 */
	@Override
	public Integer setSeviceBranchToService(ArrayList<Service> serviceList,
			String remark, String branch, Long companyId) {
		Integer count =0 ;
		
		if(serviceList!=null){
			for(int i = 0 ; i<serviceList.size(); i++){
				serviceList.get(i).setBranch(branch);
				count++;
			}
			ofy().save().entities(serviceList).now();
		}
		return count;
	}
	/** Ends **/

	/**
	 * Updated By:Viraj
	 * Date:29-06-2019
	 * Description: service list to assign status
	 */
	@Override
	public Integer setServiceStatusToService(ArrayList<Service> serviceList,
			String remark, String status, Long companyId) {
		Integer count =0 ;
		
		if(serviceList!=null){
			for(int i = 0 ; i<serviceList.size(); i++){
				serviceList.get(i).setStatus(status);
				count++;
			}
			ofy().save().entities(serviceList).now();
		}
		return count;
	}
	/** Ends **/
	
	
	
	/**
	 * @author Anil
	 * @since 12-05-2020
	 * Updpating complaint document date and status as per the service update
	 */
	public String updateComplainDocument(ArrayList<Service> serviceList,boolean isSchedule,boolean isAssignTechnician,boolean isScheduleTech,boolean isServiceCompletion){
		logger.log(Level.SEVERE, "updateComplainDocument");
		SimpleDateFormat fmt24Hrs=new SimpleDateFormat("HH:mm");
		ServiceImplementor impl=new ServiceImplementor();
		String status="";
		ArrayList<Integer> complainIdList=new ArrayList<Integer>();
		for(Service obj:serviceList){
			if(obj.getTicketNumber()!=0||obj.getTicketNumber()!=-1){
				complainIdList.add(obj.getTicketNumber());
			}
		}
		logger.log(Level.SEVERE, "complainIdList "+complainIdList.size());
		if(complainIdList.size()!=0){
			List<Complain> complainList=ofy().load().type(Complain.class).filter("companyId", serviceList.get(0).getCompanyId()).filter("count IN", complainIdList).list();
			logger.log(Level.SEVERE, "complainList "+complainList.size());
			if(complainList.size()!=0){
				for(Complain comp:complainList){
					Service service=getServiceDetails(comp.getCount(),serviceList);
					if(service!=null){
						
						if(isSchedule){
							logger.log(Level.SEVERE, "isSchedule "+isSchedule);
							comp.setNewServiceDate(service.getServiceDate());
							if(DateUtility.convert12Hrsto24HrsFormat(service.getServiceTime())!=null){
								comp.setNewServiceTime(DateUtility.convert12Hrsto24HrsFormat(service.getServiceTime()));
							}
							/**
							 * @author Anil
							 * @since 27-05-2020
							 * at the time of reschedule also updating assignDate and time
							 */
							comp.setAssignedDate(DateUtility.getDateWithTimeZone("IST", new Date()));
							comp.setAssignedTime(DateUtility.getTimeWithTimeZone1("IST", new Date()));
					
						}
						
						if(isAssignTechnician){
							logger.log(Level.SEVERE, "isAssignTechnician "+isAssignTechnician);
							comp.setAssignto(service.getEmployee());
							comp.setAssignedDate(DateUtility.getDateWithTimeZone("IST", new Date()));
							comp.setAssignedTime(DateUtility.getTimeWithTimeZone1("IST", new Date()));
						}
						
						if(isScheduleTech){
							logger.log(Level.SEVERE, "isScheduleTech "+isScheduleTech);
							comp.setAssignto(service.getEmployee());
							comp.setAssignedDate(DateUtility.getDateWithTimeZone("IST", new Date()));
							comp.setAssignedTime(DateUtility.getTimeWithTimeZone1("IST", new Date()));
							comp.setNewServiceTime(DateUtility.convert12Hrsto24HrsFormat(service.getServiceTime()));
						}
						
						if(isServiceCompletion){
							comp.setCompStatus(Service.SERVICESTATUSCOMPLETED);
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime", service.getCompanyId())) {
							Branch branch=ofy().load().type(Branch.class).filter("companyId", service.getCompanyId()).filter("buisnessUnitName", comp.getBranch()).first().now();
							Shift shift=null;
							Calendar calendar=null;
							if(branch!=null){
								if(branch.getShift()!=null){
									shift=branch.getShift();
								}
								if(branch.getCalendar()!=null){
									calendar=branch.getCalendar();
								}
							}
							
							/**
							 * @author Anil
							 * @since 27-05-2020
							 * If complain registration date and time is greater/less than shift time then we will
							 * recalculate complain registration date and time as per shift
							 */
							logger.log(Level.SEVERE,"BF Complain Registration Date Time : "+comp.getDate());
							Date compRegDate=new Date(DateUtility.getDateWithTimeZone("IST",comp.getDate()).getTime());
							logger.log(Level.SEVERE,"IST Complain Registration Date Time : "+compRegDate);
							String fromTimeArr[] =comp.getTime().split(":");
							compRegDate.setHours(Integer.parseInt(fromTimeArr[0]));
							compRegDate.setMinutes(Integer.parseInt(fromTimeArr[1]));
							logger.log(Level.SEVERE,"Complain Registration Date Time : "+compRegDate);
							Date shiftStartTime=impl.getDateWithShiftStartDate(compRegDate, shift);
							Date shiftEndTime=impl.getDateWithShiftEndDate(compRegDate, shift);
							logger.log(Level.SEVERE,"Shift Start Date Time : "+shiftStartTime);
							logger.log(Level.SEVERE,"Shift End Date Time : "+shiftEndTime);
							if(compRegDate.before(shiftStartTime)){
								logger.log(Level.SEVERE,"Complain Registration Date Time is less");
								checkForWoAndHoliday(compRegDate, calendar,shift);
								compRegDate=impl.getDateWithShiftStartDate(compRegDate, shift);
							}else if(compRegDate.after(shiftEndTime)){
								logger.log(Level.SEVERE,"Complain Registration Date Time is greater");
								int noOfDays=DateUtility.getNumberOfDaysBetweenDates(compRegDate, shiftEndTime);
								if(noOfDays==0){
									compRegDate=DateUtility.addDaysToDate(compRegDate, 1);
								}
								checkForWoAndHoliday(compRegDate, calendar,shift);
								compRegDate=impl.getDateWithShiftStartDate(compRegDate, shift);
							}else{
								checkForWoAndHoliday(compRegDate, calendar,shift);
							}
							logger.log(Level.SEVERE,"Updated complain reg date : "+compRegDate);
							String compRegTime=compRegDate.getHours()+":"+compRegDate.getMinutes();
							
							
							logger.log(Level.SEVERE, "isServiceCompletion "+isServiceCompletion);
							comp.setCompStatus(Service.SERVICESTATUSCOMPLETED);
							comp.setCompletionDate(DateUtility.getDateWithTimeZone("IST", new Date()));
							comp.setCompletionTime(DateUtility.getTimeWithTimeZone1("IST", new Date()));
							comp.setTat_Tech(impl.convertNumbersToTime(impl.calculateTurnAroundTime(comp.getCompletionDate(), comp.getCompletionTime(), comp.getNewServiceDate(), comp.getNewServiceTime(),shift,calendar)));
							comp.setTat_Actual(impl.convertNumbersToTime(impl.calculateTurnAroundTime(comp.getCompletionDate(), comp.getCompletionTime(),compRegDate, compRegTime,shift,calendar)));
							comp.setTat_Sup(impl.convertNumbersToTime(impl.calculateTurnAroundTime(comp.getAssignedDate(), comp.getAssignedTime(), compRegDate,compRegTime,shift,calendar)));
							}
							}
						
						
						
						
					}
				}
				
				ofy().save().entities(complainList).now();
				
				
			}
			
			/**
			 * @author Vijay Chougule Date :- 21-09-2020
			 * Des :- when complaint will complete then it also send SMS for the sms
			 * @author Anil @since 02-09-2021
			 * commenting complaint completion sms from here 
			 * moving complaint sms logic to reactonCompleteComplaintService in ServiceImplementor class
			 */
//			if(complainList.size()!=0){
//				Company company = ofy().load().type(Company.class).filter("companyId", complainList.get(0).getCompanyId()).first().now();
//				if(company!=null){
//				for(Complain comp : complainList){
//					if(comp.getCompStatus().equals(Service.SERVICESTATUSCOMPLETED)){
//						SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
//						smsserviceimpl.checkSMSConfigAndSendComplaintResolvedSMS(comp.getCompanyId(), comp.getServiceDate(), comp.getPersoninfo().getFullName(), comp.getCount(), company.getBusinessUnitName(), comp.getPersoninfo().getCellNumber());
//						
//					}
//				}
//			   }
//			}
			
			/**
			 * ends here
			 */
		}
		
		
		
		return status;
	}


	private Service getServiceDetails(int count, ArrayList<Service> serviceList) {
		// TODO Auto-generated method stub
		for(Service obj:serviceList){
			if(obj.getTicketNumber()==count){
				return obj;
			}
		}
		return null;
	}
	
	public Date checkForWoAndHoliday(Date date,Calendar calendar,Shift shift){
		DataMigrationTaskQueueImpl dataImpl=new DataMigrationTaskQueueImpl();
		boolean updateFlag=false;
		Date date1=new Date(date.getTime());
		if(calendar!=null&&dataImpl.isWeeklyOff(calendar.getWeeklyoff(), date1)){
//		if(calendar!=null&&calendar.isWeekleyOff(date)){
			logger.log(Level.SEVERE,"WO");
			date=DateUtility.addDaysToDate(date, 1);
		}
		if(calendar!=null&&calendar.isHolidayDate1(date)){
			date=DateUtility.addDaysToDate(date, 1);
			logger.log(Level.SEVERE,"HOLIDAYY");
		}
		if(updateFlag){
			ServiceImplementor impl=new ServiceImplementor();
			date=impl.getDateWithShiftStartDate(date, shift);
			checkForWoAndHoliday(date, calendar,shift);
		}
		return date;
	}
	
	/**
	 * @author Vijay Chougule Date 26-09-2020
	 * Des :- Service Completion sending SMS
	 */
	@Override
	public String sendServiceComletionSMS(Service service) {
		
		if(service.getPersonInfo().getCellNumber()!=null){
		/**
		 * @author Anil @since 02-09-2021
		 * If service is of type complaint then we will not send service completion sms 
		 * will send sms on complaint completion
		 * Requirement raise by Rutuza, Vaishnavi and Nitin sir for Universal Pest Control
		 */
		if(service.getServiceType()!=null&&!service.getServiceType().equals("")&&service.getServiceType().equals("Complaint Service")
				&&service.getTicketNumber()!=null&&service.getTicketNumber()>0){
			logger.log(Level.SEVERE,"Complain Service No SMS will be sent");
			return "";
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		ServerAppUtility serverUtility = new ServerAppUtility();
		
		
//		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",service.getCompanyId()).filter("status",true).first().now();
//		logger.log(Level.SEVERE,"in the smsconfig !=null for SMS"+smsconfig);
//		if(smsconfig!=null){
//			
//			String accountsid = smsconfig.getAccountSID();
//			String userName = smsconfig.getAuthToken();
//			String password = smsconfig.getPassword();
			
			boolean dndstatusFlag = serverUtility.validateCustomerDNDStatus(service.getPersonInfo().getCount(), service.getCompanyId());
			if(!dndstatusFlag){
			
				
			SmsTemplate smsTemplateEntity = ofy().load().type(SmsTemplate.class).filter("companyId", service.getCompanyId()).filter("event", "Service Completion").filter("status",true).first().now();
			logger.log(Level.SEVERE,"smsTemplateEntity "+smsTemplateEntity);
			if(smsTemplateEntity!=null){
				
				Company company = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
				String companyName = company.getBusinessUnitName();
				
				final String templateMsgwithbraces = smsTemplateEntity.getMessage(); 
				final String prodName = service.getProductName();
				final String serNo = service.getServiceSerialNo()+"";
				final String serDate = dateFormat.format(service.getServiceDate());
				
				String custName = service.getPersonInfo().getFullName();
				String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
				String productName = cutomerName.replace("{ProductName}", prodName);
				String serviceNo = productName.replace("{ServiceNo}", serNo);
				String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
				String actualMsg = serviceDate.replace("{companyName}", companyName);
				
				long cellNo = service.getPersonInfo().getCellNumber();
				
				/**
				 * @author Anil @since 09-03-2021
				 * loading feedback form as per the customer category
				 */
				if(company!=null&&company.getFeedbackUrlList()!=null&&company.getFeedbackUrlList().size()!=0){
					/**
					 * @author Anil @since 03-03-2021
					 * Adding feedback form url on service completion sms
					 * Raised by Nitin Sir for Pecopp
					 */
					String feedbackFormUrl="";
//					ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", service.getCompanyId()).filter("processName", "ZohoFeedbackForm").filter("configStatus", true).first().now();
//					if(processConfig!=null){
//						for(ProcessTypeDetails obj:processConfig.getProcessList()){
//							if(obj.isStatus()==true){
//								feedbackFormUrl=obj.getProcessType();
//							}
//						}
//					}
					Customer customer=ofy().load().type(Customer.class).filter("companyId",service.getCompanyId()).filter("count",service.getPersonInfo().getCount()).first().now();
					if(customer!=null){
						if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
							for(FeedbackUrlAsPerCustomerCategory feedback:company.getFeedbackUrlList()){
								if(feedback.getCustomerCategory().equals(customer.getCategory())){
									feedbackFormUrl=feedback.getFeedbackUrl();
									break;
								}
							}
						}
					}
					
					
					boolean customergroupHighFrequency = true;
					if(customer!=null){
							
						String customerGroup = customer.getGroup();
						if(customerGroup.trim().equals(AppConstants.HIGHFREQUENCYSERVICES)){
							Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", service.getCompanyId())
									.filter("count", service.getContractCount()).first().now();
								double frequency =0;
								for(SalesLineItem saleslineitem : contractEntity.getItems()){
									if(saleslineitem.getProductCode().trim().equals(service.getProduct().getProductCode().trim())){
										frequency = saleslineitem.getDuration()/saleslineitem.getNumberOfServices();
										break;
									}
								}
								logger.log(Level.SEVERE, "frequency" + frequency);
								logger.log(Level.SEVERE, "company.getMinimumDuration()" + company.getMinimumDuration());
					
								if(frequency<company.getMinimumDuration()){
									customergroupHighFrequency = false;
								}
						}
						
					}
						
					if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("") && customergroupHighFrequency){
						logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
						StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
				        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
				        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
				        sbPostData.append("&serviceId="+service.getCount());
				        sbPostData.append("&serviceDate="+serDate);
				        sbPostData.append("&serviceName="+service.getProductName());
				        sbPostData.append("&technicianName="+service.getEmployee());
				        sbPostData.append("&branch="+service.getBranch());
				        feedbackFormUrl = sbPostData.toString();
				        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
				        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
				        /**
				         * @author Anil @since 10-03-2021
				         * Modified feedback Url i.e. tiny utl
				         */
				        try {
				        	feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,company.getCompanyId());
				        }catch(Exception e) {
				        	e.printStackTrace();
				        }
				        
				        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
						actualMsg=actualMsg.replace("{Link}", feedbackFormUrl);
					}
					/**
					 * @author Anil @since 19-05-2021
					 * As per Nitin sir's instruction if no schduling URL found then 
					 * remove {Link} keyword from SMS template as well
					 */
					else{
//						actualMsg=actualMsg.replace("{Link}", "");
						actualMsg=actualMsg.replace(" Please share your feedback by clicking on this link {Link} -", "-");
						actualMsg=actualMsg.trim();
					}
				}
				
				if(actualMsg.contains("{DocumentId}")){
					actualMsg =  actualMsg.replace("{DocumentId}", service.getCount()+"");
				}
				
				
				/*
				 * Ashwini Patil
				 * Date:21-10-2024
				 * Ultra pest requested that sr copy url shall get sent along with service completion whatsapp msg
				 */
				if(actualMsg.contains("{PDFLink}")){
					CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
					String pdflink = commonserviceimpl.getPDFURL("Service", service, service.getCount(), service.getCompanyId(), company, commonserviceimpl.getCompleteURL(company),null);
					logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
					String tinyurl = ServerAppUtility.getTinyUrl(pdflink,company.getCompanyId());
					actualMsg =  actualMsg.replace("{PDFLink}", tinyurl);
				}
				
				
				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				smsimpl.sendSmsToClient(actualMsg, cellNo, accountsid, userName, password, service.getCompanyId());

				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.SERVICE,smsTemplateEntity.getEvent(),smsTemplateEntity.getCompanyId(),actualMsg,cellNo,false);
				logger.log(Level.SEVERE,"after sendMessage method");
				
			}
		}
		else{
			return "Can not send SMS customer DND status is active";
		}
//		}
		
		return "";	
		}
		return null;
	}

	/**
	 * @author Anil @since 23-03-2021
	 * In case after scheduling of services we are changing technician then stock to be allocated to new technician
	 * requirement raised by Nitin Sir for Pestico
	 */
	@Override
	public ArrayList<String> validateTechnicianStock(long companyId,String technicianName, ArrayList<Service> serviceList) {
		// TODO Auto-generated method stub
		ArrayList<String> errorList=new ArrayList<String>();
		ArrayList<Service> toUpdateServiceList=new ArrayList<Service>();
		ArrayList<ProductGroupList> productGroupList=new ArrayList<ProductGroupList>();
		boolean stockUpdateFlag=false;
		ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
		ArrayList<Integer> contractIdList=new ArrayList<Integer>();
		
		if(serviceList.size()!=0){
			for(Service service:serviceList){
				
				try{
					if(service.isServiceScheduled()){
						if(!service.getEmployee().equals(technicianName)){
							toUpdateServiceList.add(service);
							serviceIdList.add(service.getCount());
							contractIdList.add(service.getContractCount());
							stockUpdateFlag=true;
						}
					}
				}catch(Exception e){
					
				}
			}
			try{
				if(stockUpdateFlag){
					if(serviceIdList.size()!=0&&contractIdList.size()!=0){
						GeneralServiceImpl impl=new GeneralServiceImpl();
						productGroupList=impl.getCombinedMaterialList(companyId, serviceIdList, contractIdList);
						TechnicianWareHouseDetailsList techWh=ofy().load().type(TechnicianWareHouseDetailsList.class).filter("companyId", companyId).filter("wareHouseList.technicianName", technicianName).first().now();
						if(techWh!=null){
							
							
							for(TechnicianWarehouseDetails details : techWh.getWareHouseList()){
								if(details.getTechnicianName().equalsIgnoreCase(technicianName)){
									
									for(ProductGroupList object:productGroupList){
										object.setParentWarentwarehouse(details.getParentWareHouse());
										object.setParentStorageLocation(details.getParentStorageLocation());
										object.setParentStorageBin(details.getPearentStorageBin());
										
										object.setWarehouse(details.getTechnicianWareHouse());
										object.setStorageLocation(details.getTechnicianStorageLocation());
										object.setStorageBin(details.getTechnicianStorageBin());
										object.setCreatedBy(technicianName);
									}
									
									break;
								}
							}
							
							
							
						}else{
							for(ProductGroupList object:productGroupList){
								object.setParentWarentwarehouse("");
								object.setParentStorageLocation("");
								object.setParentStorageBin("");
								
								object.setWarehouse("");
								object.setStorageLocation("");
								object.setStorageBin("");
								object.setCreatedBy(technicianName);
							}
						}
						
						if(productGroupList.size()!=0){
							errorList=impl.validateTechnicianWarehouseQuantity(companyId, toUpdateServiceList, productGroupList);
						}
					}
				}
			}catch(Exception e){
				
			}
		}
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		return errorList;
	}
	
	//Ashwini Patil Date:1-03-2023 to create MIN and update inventory when service completed from erp
		public void updateInventoryAfterServiceCompletionFromErp(Service service,ServiceProject serProj) {
			logger.log(Level.SEVERE , "in updateInventoryAfterServiceCompletionFromErp" );
//			ServiceProject serProj = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", service.getCount())
//					.filter("contractId", service.getContractCount()).first().now();
			String msgString = "";
			
			if (serProj != null && service!=null) {
				JSONArray materialArray=new JSONArray();
				logger.log(Level.SEVERE , "createMaterialReturnDoc : 1" );
				if(serProj.getProdDetailsList().size()!=0){
					logger.log(Level.SEVERE , "createMaterialReturnDoc : 2" );
					
					for(ProductGroupList details:serProj.getProdDetailsList()){
						JSONObject itemObj=new JSONObject();
						itemObj.put("materialName", details.getName());
						if(details.getReturnQuantity()>0)
							itemObj.put("returnQty",details.getReturnQuantity() );
						else
							itemObj.put("returnQty",0);
						itemObj.put("materialQty",details.getQuantity() );
						itemObj.put("materialCode",details.getCode() );
						
						materialArray.put(itemObj);
					}
					
					MaterialIssueNote minObj = ofy().load().type(MaterialIssueNote.class).filter("companyId", service.getCompanyId()).filter("serviceId", service.getCount())
								.filter("minSoId", service.getContractCount() ).first().now();
					if(minObj!=null){
						SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
						minObj.setStatus(MaterialIssueNote.CANCELLED);
						minObj.setMinDescription("MIN ID ="+minObj.getCount()+" "+"MIN status ="+minObj.getStatus()+"\n"
								+"has been cancelled as Service is completed from scheduled popup. And creating another MIN as per current material."+" Cancellation Date ="+format1.format(new Date()));
						UpdateServiceImpl updateService=new UpdateServiceImpl();
						String cancellationResponse=updateService.cancelMIN(minObj);						
						
						if(cancellationResponse.equalsIgnoreCase("MIN Cancelled Successfully.")){
							logger.log(Level.SEVERE , "createMaterialReturnDoc : 3" );	
							TechnicianAppOperation techapi=new TechnicianAppOperation();
							logger.log(Level.SEVERE , "Calling ReadMaterialAndDoMIN with employee "+ service.getEmployee());	
							techapi.ReadMaterialAndDoMIN(serProj,materialArray,service.getBranch(),service.getEmployee());						
							
						}else{
							logger.log(Level.SEVERE , cancellationResponse );
							
						}
						msgString=msgString+"Min is already created for Service Id - "+service.getCount()+"\n";
						logger.log(Level.SEVERE , "Min is already created for Service Id - "+service.getCount()+ "cancelling MIN" );
					}else{
						logger.log(Level.SEVERE , "createMaterialReturnDoc : 4" );	
						TechnicianAppOperation techapi=new TechnicianAppOperation();
						logger.log(Level.SEVERE , "Calling ReadMaterialAndDoMIN with employee "+ service.getEmployee());	
						techapi.ReadMaterialAndDoMIN(serProj,materialArray,service.getBranch(),service.getEmployee());												
					}				
					logger.log(Level.SEVERE , "Material response :" + msgString);
				}else
					logger.log(Level.SEVERE , "No material found in project");
				}else
					logger.log(Level.SEVERE , "Service or Project null");
		}


		@Override
		public String setServicesForScheduleOrCompleteViaExcelDonwload(
				ArrayList<Service> serviceList) {
			CsvWriter.serviceDownloadForScheduleOrCompleteViaExcel = serviceList;
			logger.log(Level.SEVERE , "Servicelist set to serviceDownloadForScheduleOrCompleteViaExcel");
			return "success";
		}


		@Override
		public Integer getContractServiceCount(Contract con) {

			if(con!=null) {
				int count = ofy().load().type(Service.class).filter("companyId", con.getCompanyId())
					.filter("contractCount", con.getCount()).count();
				return count;
			}else
				return null;
		}


		@Override
		public Integer getContractBillingCount(Contract con) {
			if(con!=null) {
				int count = ofy().load().type(BillingDocument.class).filter("companyId", con.getCompanyId())
					.filter("contractCount", con.getCount()).filter("accountType", "AR").count();
				return count;
			}else
				return null;
		}


}
