package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.cnc.CncBillService;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.inventorydashboard.MmnTableProxy;
import com.slicktechnologies.server.addhocprinting.CustomCNCInvoicePdf;
import com.slicktechnologies.server.humanresourcelayer.PaySlipServiceImpl;
import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.CNCBillAnnexure;
import com.slicktechnologies.shared.CNCBillAnnexureBean;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.OtRateDetails;
import com.slicktechnologies.shared.common.cnc.OtherServices;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.AvailedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesAnnexureType;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class CncBillServiceImpl extends RemoteServiceServlet implements CncBillService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -945733671421501132L;
	Logger logger = Logger.getLogger("CNC Bill");
	ServerAppUtility serverapp = new ServerAppUtility();
	PaySlipServiceImpl paySlipService = new PaySlipServiceImpl();
	Branch branch;
	Customer customer;
	private static int salesOrderId = 0;
	GenricServiceImpl genricServiceImpl = new GenricServiceImpl();
	
	int noOfStaff=0;
	
	/**
	 * @author Anil @since 05-04-2021
	 * Storing OT hours
	 * For Alkosh -OT bill Calculation raised by Rahul
	 */
//	double otHours=0;
	HashMap<String,Double>desgWiswOtMap=new HashMap<String, Double>();
	
	@Override
	public String generateBill(Date fromDate, Date toDate, CNC cnc,int orderId) {
		
		String response="";//contractId
		if(cnc!=null&&cnc.getStatus().equals(CNC.CONTRACTCONFIRMED)){
			//Gson gson = new GsonBuilder().create();	
			//String str = gson.toJson(cnc, CNC.class);
			ofy().save().entity(cnc);
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			String str = cnc.getCount()+"";
			String date1 = dateFormat.format(fromDate);
			String date2 = dateFormat.format(toDate);
			String parameter = "NA";
			salesOrderId = orderId;
			/** date 22.1.2019 added by komal to add validation for attendance **/
			if(orderId != 0){
				parameter = AppConstants.CONSUMABLES;
			}else{
				String message = validateAttendance(fromDate , toDate , cnc);
				if(!message.equalsIgnoreCase("success")){
					return message;
				}
			}
	        Queue queue = QueueFactory.getQueue("documentCancellation-queue");
	        String typeducumentName = "CreateCNCBill"+"$"+cnc.getCompanyId()+"$"+str+"$"+date1+"$"+date2+"$"+parameter;
	   	  	logger.log(Level.SEVERE, "Company Id "	+ typeducumentName);
	   	  	queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", typeducumentName));			
	   	 	response="Billings created successfully.";
			
		}else{
			response="First mark confirmed CNC.";
		}
		
		
		
		
		return response;
	}

	public SuperModel getBillingDocument(Date fromDate, Date toDate, CNC cnc,ProjectAllocation projectAllocation, String billingParameter,HashMap<String ,ArrayList<Attendance>> attendanceMap,List<EmployeeInfo> empInfoList, int noofdays,Customer customer ,Branch branch,
			HashMap<String , ArrayList<Integer>> employeeMap) {
		//calculate
		SalesOrder salesOrder = null ;
		if(billingParameter.equalsIgnoreCase(AppConstants.CONSUMABLES)){
			salesOrder = ofy().load().type(SalesOrder.class).filter("companyId", cnc.getCompanyId()).
					          filter("count", salesOrderId).first().now();
			if(salesOrder != null){
				billingParameter = salesOrder.getSubBillType();
			}
		}
		BillingDocument billingDocEntity = null; 
		ArrayList<SalesOrderProductLineItem> productList=new ArrayList<SalesOrderProductLineItem>();
		ArrayList<SalesLineItem> salesItem = new ArrayList<SalesLineItem>();
		boolean flag = false;
		billingDocEntity = ofy().load().type(BillingDocument.class).filter("contractCount", cnc.getCount()).filter("subBillType", billingParameter).first().now();
			
		if(billingDocEntity == null){
			billingDocEntity = new BillingDocument();
			flag = true;
		}else{
			if(billingDocEntity.getStatus().equalsIgnoreCase(BillingDocument.CREATED)){
				flag = true;
			}else if(!billingDocEntity.getStatus().equalsIgnoreCase(BillingDocument.REQUESTED)){
				billingDocEntity = new BillingDocument();
				flag = true;
			}
		}
		if(flag){
			
		/**
		 * @author Vijay @Since 21-09-2022
		 * Loading employee project allocation from different entity
		 */
		   CommonServiceImpl commonservice = new CommonServiceImpl();
		   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
	    /**
	     * ends here
	     */
			   
		billingDocEntity.setSubBillType(billingParameter);
		billingDocEntity.setRefNumber(cnc.getCount()+"");
		List<SalesOrderProductLineItem> salesProdLis=null;
		List<ContractCharges> billTaxLis=null;
		ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
		PaymentTerms paymentTerms=new PaymentTerms();
		Date conStartDate=null;
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		/**  date 5.9.2018 added by komal for staffing**/
		SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		//String monthForSalary=sdf.format(date);
		double totBillAmt= 0; 
		double totalDiscount = 0;
				if((billingParameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL)
						|| billingParameter.equalsIgnoreCase(AppConstants.STAFFING)
						|| billingParameter.equalsIgnoreCase(AppConstants.EQUIPMENTRENTAL)
						|| billingParameter.equalsIgnoreCase(AppConstants.ARREARSBILL))
						&& projectAllocation !=null){
		//	Map<String ,Integer> employeeList = new HashMap<String , >
		//	List<Integer> empIdList = new ArrayList<Integer>();
			ArrayList<Integer> cncBillAnnexureId = new ArrayList<Integer>();
			
//			if(projectAllocation.getEmployeeProjectAllocationList().size()>0 || cnc.getEquipmentRentalList().size() > 0){
			if(empprojectallocationlist.size()>0 || cnc.getEquipmentRentalList().size() > 0){
				productList = getSalesLineItemList(fromDate, toDate , projectAllocation.getCompanyId() , cnc , attendanceMap,empInfoList ,noofdays,billingParameter,customer ,branch,salesOrder,employeeMap);
				for(SalesOrderProductLineItem item : productList){
					totBillAmt += item.getBaseBillingAmount();
					if(billingParameter.equalsIgnoreCase(AppConstants.ARREARSBILL)){
						if(item.getProSerialNo() != null && !item.getProSerialNo().equalsIgnoreCase("")){
							try{
								String[] id = item.getProSerialNo().split("/");
					             for(int l = 0 ; l < id.length ; l++){
					            	 try{
					            	 cncBillAnnexureId.add(Integer.parseInt(id[l]));
					            	 }catch(Exception e){
					            		 cncBillAnnexureId.add(0);
					            	 }
					             }
								
							}catch(Exception e){
								
							}
						}
					}
				}
			//	totalDiscount =  cnc.getEquipmentDiscountAmount();
				if(productList.size() == 0){
					billingDocEntity.setCount(-1);
					//return billingDocEntity;
				}
			}
			billingDocEntity.setCncBillAnnexureId(cncBillAnnexureId);
		}
		
		if(billingParameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY) && projectAllocation !=null){
//			if(projectAllocation.getEmployeeProjectAllocationList().size()>0 || cnc.getEquipmentRentalList().size() > 0){
			if(empprojectallocationlist.size()>0 || cnc.getEquipmentRentalList().size() > 0){
				logger.log(Level.SEVERE , AppConstants.NATIONALHOLIDAY);
				productList = getSalesLineItemList(fromDate, toDate , projectAllocation.getCompanyId() , cnc , attendanceMap,empInfoList ,noofdays,AppConstants.NATIONALHOLIDAY,customer ,branch,salesOrder,employeeMap);
				for(SalesOrderProductLineItem item : productList){
					totBillAmt += item.getBaseBillingAmount();
				}
				if(productList.size() == 0){
					billingDocEntity.setCount(-1);
					//return billingDocEntity;
				}
			}
		}
		if(billingParameter.equalsIgnoreCase(AppConstants.OVERTIME) && projectAllocation !=null){
//			if(projectAllocation.getEmployeeProjectAllocationList().size()>0 || cnc.getEquipmentRentalList().size() > 0){
			if(empprojectallocationlist.size()>0 || cnc.getEquipmentRentalList().size() > 0){
				logger.log(Level.SEVERE , AppConstants.OVERTIME);
				productList = getSalesLineItemList(fromDate, toDate , projectAllocation.getCompanyId() , cnc , attendanceMap,empInfoList ,noofdays,AppConstants.OVERTIME,customer ,branch ,salesOrder,employeeMap);
				for(SalesOrderProductLineItem item : productList){
					totBillAmt += item.getBaseBillingAmount();
				}
				if(productList.size() == 0){
					billingDocEntity.setCount(-1);
					//return billingDocEntity;
				}
			}
		}
		
		/**
		 * @author Anil
		 * @since 21-10-2020
		 * creating Public holiday bill
		 */
		if(billingParameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY) && projectAllocation !=null){
//			if(projectAllocation.getEmployeeProjectAllocationList().size()>0 || cnc.getEquipmentRentalList().size() > 0){
			if(empprojectallocationlist.size()>0 || cnc.getEquipmentRentalList().size() > 0){
				logger.log(Level.SEVERE , AppConstants.PUBLICHOLIDAY);
				productList = getSalesLineItemList(fromDate, toDate , projectAllocation.getCompanyId() , cnc , attendanceMap,empInfoList ,noofdays,AppConstants.PUBLICHOLIDAY,customer ,branch,salesOrder,employeeMap);
				for(SalesOrderProductLineItem item : productList){
					totBillAmt += item.getBaseBillingAmount();
				}
				if(productList.size() == 0){
					billingDocEntity.setCount(-1);
					//return billingDocEntity;
				}
			}
		}
		
		
		
		if(billingParameter.equalsIgnoreCase(AppConstants.OTHERSERVICES)){
			SalesOrderProductLineItem salesLineItem = null;
			if(cnc.getOtherServicesList().size()>0){
				for(OtherServices service : cnc.getOtherServicesList()){
					if(service.getOtherService()!=null && !service.getOtherService().equals("")){
						if(service.getServiceProduct()!=null){
						    ServiceProduct serviceProduct = service.getServiceProduct();
							  logger.log(Level.SEVERE , serviceProduct.toString());
							   if(!serviceProduct.getProductGroup().equalsIgnoreCase("OTHER")){
								  salesLineItem = getProductLineItem(serviceProduct,customer ,branch);
								//  salesLineItem.setPrduct(serviceProduct);
								 // salesLineItem.setArea((m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity())+"");
								  
//								  salesLineItem.setProdId(serviceProduct.getCount());
//								  salesLineItem.setProdCode(serviceProduct.getProductCode());
//								  salesLineItem.setProdCategory(serviceProduct.getProductCategory());
//								  salesLineItem.setProdName(serviceProduct.getProductName());
								  salesLineItem.setPrice(service.getChargableRates());
								//  salesLineItem.setQuantity((m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity()));
								  salesLineItem.setTotalAmount(service.getChargableRates());
								  salesLineItem.setQuantity(0.0);
								  double services = 1.0;
//								  if(service.getFrequencyOfServices()!= null && !service.getFrequencyOfServices().equals(""))
//								  {	   try{
//									     services = Double.parseDouble(service.getFrequencyOfServices());
//								  		}catch(Exception e){
//								  			services = 0;
//								  		} 
//								  }
//								  if(services!=0){
									  salesLineItem.setArea(services+"");
//								  }else{
//									  salesLineItem.setArea("NA");
//								  }
//								  salesLineItem.setVatTax(serviceProduct.getVatTax());
//								  salesLineItem.setServiceTax(serviceProduct.getServiceTax());
								  salesLineItem.setDiscountAmt(0.0);
								 // salesLineItem.setBasePaymentAmount(m.getTotalAmount());
								  salesLineItem.setBaseBillingAmount(service.getChargableRates());
								  salesLineItem.setPaymentPercent(100.0);
								  salesLineItem.setBasePaymentAmount(service.getChargableRates());
								  productList.add(salesLineItem);
//								  SalesLineItem item=new SalesLineItem();
//								  item.setPrduct(serviceProduct);
//								  item.setQuantity(1.0);
//								  item.setTotalAmount(service.getChargableRates());		
//								  item.setPrice(service.getChargableRates());
//								  item.setVatTax(serviceProduct.getVatTax());
//								  item.setServiceTax(serviceProduct.getServiceTax());
//								  salesItem.add(item);
								  totBillAmt += service.getChargableRates();
							}
						}
					}
				}
			}
		}
		
		if(billingParameter.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES) || billingParameter.equalsIgnoreCase(AppConstants.CONSUMABLES)){
			//	Map<String ,Integer> employeeList = new HashMap<String , >
			//	List<Integer> empIdList = new ArrayList<Integer>();
			//	if(projectAllocation.getEmployeeProjectAllocationList().size()>0 || cnc.getEquipmentRentalList().size() > 0){
					productList = getSalesLineItemList(fromDate, toDate , cnc.getCompanyId() , cnc , attendanceMap,empInfoList ,noofdays,billingParameter,customer ,branch,salesOrder,employeeMap);
					for(SalesOrderProductLineItem item : productList){
						totBillAmt += item.getBaseBillingAmount();
					}
				//	totalDiscount =  cnc.getEquipmentDiscountAmount();
					if(productList.size() == 0){
						billingDocEntity.setCount(-1);
						//return billingDocEntity;
					}
				}
		//	}

		
		  List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
		  List<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
		  try {
			list.addAll(serverapp.addProdTaxes1(productList , taxList , AppConstants.BILLINGACCOUNTTYPEAR));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
			double assessValue=0;
			double tax = 0;
			for(int i=0;i<list.size();i++){
				ContractCharges taxDetails=new ContractCharges();
				taxDetails.setTaxChargeName(list.get(i).getChargeName());
				taxDetails.setTaxChargePercent(list.get(i).getChargePercent());
				assessValue=list.get(i).getAssessableAmount();//*paymentRecieved/100;
				taxDetails.setTaxChargeAssesVal(assessValue);				
				taxDetails.setIdentifyTaxCharge(list.get(i).getIndexCheck());
				tax = tax + taxDetails.getTaxChargeAssesVal()*taxDetails.getTaxChargePercent()/100;
				arrBillTax.add(taxDetails);
			}
			billingDocEntity.setBillingTaxes(arrBillTax);
		
		Date calBillingDate=null;
		Calendar calBilling=Calendar.getInstance();
		if(fromDate!=null){
			calBilling.setTime(DateUtility.getDateWithTimeZone("IST",fromDate ));
		}
		calBillingDate=calBilling.getTime();
		
		
		Date calPaymentDate=new Date(calBillingDate.getTime());
		int creditVal=cnc.getCreditPeriod();
		Calendar calPayment=Calendar.getInstance();
		if(calPaymentDate!=null){
			calPayment.setTime(DateUtility.getDateWithTimeZone("IST",calPaymentDate));
		}
		calPayment.add(Calendar.DATE, creditVal);
		calPaymentDate=calPayment.getTime();
		
		
		
		if(cnc.getPersonInfo()!=null)
			billingDocEntity.setPersonInfo(cnc.getPersonInfo());
		if(cnc.getCount()!=0){
			if(billingParameter.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES) 
					|| billingParameter.equalsIgnoreCase(AppConstants.CONSUMABLES)){
				billingDocEntity.setContractCount(salesOrderId);
			}else{
				billingDocEntity.setContractCount(cnc.getCount());
			}
			billingDocEntity.setRefNumber(cnc.getCount()+"");
		}
		
//		if(cnc.getNetpayable()!=0)
//			billingDocEntity.setTotalSalesAmount(cnc.getNetpayable());
		
		
//		salesProdLis=cnc.retrieveSalesProducts(100);
//		ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
//		salesOrdArr.addAll(salesProdLis);
		System.out.println("Size here here"+productList.size());
		if(productList.size()!=0)
			billingDocEntity.setSalesOrderProducts(productList);
		if(cnc.getCompanyId()!=null)
			billingDocEntity.setCompanyId(cnc.getCompanyId());
		
		billingDocEntity.setBillingDate(calBillingDate);
		billingDocEntity.setInvoiceDate(calPaymentDate);
		billingDocEntity.setPaymentDate(calPaymentDate);
	
		if(cnc.getPaymentMethod()!=null)
			billingDocEntity.setPaymentMethod(cnc.getPaymentMethod());
		if(cnc.getApproverName()!=null)
			billingDocEntity.setApproverName(cnc.getApproverName());
		if(cnc.getSalesPerson()!=null){
//			billingDocEntity.setEmployee(cnc.getEmployee());
			/** CNC Sales Person name mapping in Billing Document ***/
			billingDocEntity.setEmployee(cnc.getSalesPerson());
		}
		if(cnc.getBranch()!=null)
			billingDocEntity.setBranch(cnc.getBranch());
		
		
//		ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
//		if(cnc.getProductTaxes().size()!=0){
//			ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
//			billTaxLis=cnc.listForBillingTaxes(cnc.getPaymentTermsList().get(i).getPayTermPercent());
//			billTaxArr.addAll(billTaxLis);
//			billingDocEntity.setBillingTaxes(billTaxArr);
//			billTaxArrROHAN.addAll(billTaxLis);
//		}

		if(cnc.getContractStartDate()!=null){
			billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST", cnc.getContractStartDate()));
		}
		if(cnc.getContractEndDate()!=null){
			billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST", cnc.getContractEndDate()));
		}
		
//		double grossValue=cnc.getTotalAmount();
//		billingDocEntity.setGrossValue(grossValue);
		
		  //cnc.getTotalBillAmt(salesProdLis);
	//	double taxAmt= cnc.getTotalFromTaxTable(billTaxArrROHAN);
	//	double totalBillingAmount = totBillAmt+taxAmt;
	//	billingDocEntity.setTotalBillingAmount(totalBillingAmount);
		billingDocEntity.setOrderCreationDate(cnc.getCreationDate());
		
		int payTrmsDays=0;
		double payTrmsPercent=100;
		String payTrmsComment="";
		
		paymentTerms.setPayTermDays(payTrmsDays);
		paymentTerms.setPayTermPercent(payTrmsPercent);
		paymentTerms.setPayTermComment(payTrmsComment);
		billingPayTerms.add(paymentTerms);
		billingDocEntity.setArrPayTerms(billingPayTerms);
		/**
		 * @author Anil @since 08-11-2021
		 * If it is consumable bill then type of order should be sales order
		 * Raised by Rahul Tiwari for Sunrise
		 */
		if(billingParameter.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)||billingParameter.equalsIgnoreCase(AppConstants.CONSUMABLES)){
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESALES);
		}else{
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
		}
		billingDocEntity.setStatus(BillingDocument.CREATED);
		billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
		billingDocEntity.setOrderCformStatus("");
		billingDocEntity.setOrderCformPercent(-1);


		billingDocEntity.setBillingPeroidFromDate(fromDate);
		billingDocEntity.setBillingPeroidToDate(toDate);
		
		billingDocEntity.setTotalAmount(totBillAmt);
		
		
		//double totalfinalAmt=totBillAmt-discountamt;
	//	billingDocEntity.setFinalTotalAmt(totalfinalAmt);
		billingDocEntity.setFinalTotalAmt(totBillAmt - totalDiscount);
		double totalamtincludingtax=totBillAmt+tax;
		billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
//		double roundoff=0;
//		if(cnc.getRoundOffAmt()!=0){
//			roundoff=cnc.getRoundOffAmt()/cnc.getPaymentTermsList().size();
//			billingDocEntity.setRoundOffAmt(roundoff);
//		}
		billingDocEntity.setDiscountAmt(totalDiscount);
		billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
//		billingDocEntity.setTotalBillingAmount(totalamtincludingtax+roundoff);
		billingDocEntity.setTotalBillingAmount(Math.round(totalamtincludingtax));
		billingDocEntity.setTotalSalesAmount(Math.round(totalamtincludingtax));
//		if(cnc.getRefNum()!=null){
//			billingDocEntity.setRefNumber(cnc.getRefNum());
//		}
		
		/*** Date 11-08-2020 by Vijay for Sasha project Name storing in billing and invoice ***/
		if(cnc.getProjectName()!=null){
			billingDocEntity.setProjectName(cnc.getProjectName());
		}
		if(cnc.getContractNumber()!=null){
			billingDocEntity.setCncContractNumber(cnc.getContractNumber());
		}
		/*** Date 12-08-2020 by Vijay for Sasha Credit Period storing in billing and invoice to calculate payment Date ***/
		billingDocEntity.setCreditPeriod(cnc.getCreditPeriod());
		
		 /**
		  * @author Vijay Chougule Date - 16-09-2020
		  * Des :- When bill raising from CNC storing that from date and To Date in billing document
		  * and also storing its CNC version As per the Rahul Tiwari
		  */
		if(fromDate!=null)
		billingDocEntity.setCncbillfromDate(fromDate);
		if(toDate!=null)
		billingDocEntity.setCncbillToDate(toDate);
		billingDocEntity.setCncVersion(cnc.getVersionNumber()+"");
		/**
		 * ends here
		 */
		}
		
		/**
		 * @author Anil @since 05-04-2021
		 * Setting management percent for printing on invoice
		 * Raised  by Rahul Tiwari for Alkosh
		 */
		billingDocEntity.setManagementFeesPercent(cnc.getManagementFees());
		
		return billingDocEntity;
	}

	@Override
	public String generateProject(CNC cnc) {
		// TODO Auto-generated method stub
		/** date 21.8.2018 added by komal to store version **/
		cnc.setIslatestVersion(true);
		cnc.setVersionNumber(Math.ceil(cnc.getVersionNumber()));
		ofy().save().entity(cnc);
		GenricServiceImpl impl = new GenricServiceImpl();
		if(cnc.getStatus().equals(CNC.CONTRACTCONFIRMED) || cnc.getStatus().equals(AppConstants.APPROVED)){
			CNCVersion cncVer = new CNCVersion();
			//cnc.setIslatestVersion(false);
			cncVer.setCnc(cnc);
			cncVer.getCnc().setIslatestVersion(false);
			cncVer.setVersionCreationDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			cncVer.setVersionStatus(cnc.getStatus());
			cncVer.setVersion(Math.ceil(cnc.getVersionNumber())+"");
			cncVer.setCompanyId(cnc.getCompanyId());	
			impl.save(cncVer);
		}

		/** date 20.8.2018 changed by komal**/
		ProjectAllocation projectAllocation =  ofy().load().type(ProjectAllocation.class).filter("companyId", cnc.getCompanyId()).filter("contractId", cnc.getCount()).first().now();
		if(projectAllocation == null){
			projectAllocation=new ProjectAllocation();
		}
		projectAllocation.setBranch(cnc.getBranch());
		/**Date 4-7-2020 by Amol for Sunrise Facility raised by Rahul**/
		if(cnc.getCustomerBranch()!=null) {
			projectAllocation.setCustomerBranch(cnc.getCustomerBranch());	
		}
		projectAllocation.setContractId(cnc.getCount());
		projectAllocation.setEndDate(cnc.getContractEndDate());
		projectAllocation.setStartDate(cnc.getContractStartDate());
		projectAllocation.setInterestOnMachinary(cnc.getInterestOnMachinary());
		projectAllocation.setMaintainanceCharges(cnc.getMaintainanceCharges());
		projectAllocation.setManagementFees(cnc.getManagementFees());
		projectAllocation.setNoOfMonthsForRental(cnc.getNoOfMonthsForRental());
		projectAllocation.setOverheadCostPerEmployee(cnc.getOverheadCostPerEmployee());
		projectAllocation.setPersonInfo(cnc.getPersonInfo());
		projectAllocation.setProjectName(cnc.getProjectName());
		projectAllocation.setStatus(CNC.CREATED);
		ArrayList<EmployeeProjectAllocation> employeeProjectAllocationList=new ArrayList<EmployeeProjectAllocation>();
		/** date 27.11.2018 added by komal for revised cnc effect on cnc project **/
		Map<String , ArrayList<EmployeeProjectAllocation>> designationCountMap = new HashMap<String ,ArrayList<EmployeeProjectAllocation>>();
		ArrayList<EmployeeProjectAllocation> list = new ArrayList<EmployeeProjectAllocation>();
		if(projectAllocation.getCount() != 0){
			/**
			 * @author Anil
			 * @since 24-08-2020
			 * if state is updated on CNC then site location should be set to null in Project location
			 */
			boolean siteLocFlag=false;
			if(cnc.getState()!=null&&projectAllocation.getState()!=null){
				if(!cnc.getState().equals(projectAllocation.getState())){
					siteLocFlag=true;
				}
			}else{
				siteLocFlag=true;
			}
			
			/**
			 * @author Vijay @Since 21-09-2022
			 * Loading employee project allocation from different entity
			 */
			   CommonServiceImpl commonservice = new CommonServiceImpl();
			   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
		    /**
		     * ends here
		     */
//			for(EmployeeProjectAllocation staff : projectAllocation.getEmployeeProjectAllocationList()){
			for(EmployeeProjectAllocation staff : empprojectallocationlist){
			    if(siteLocFlag){
			    	staff.setSiteLocation(null);
			    }
				
				if(designationCountMap.containsKey(staff.getEmployeeRole())){
					list = designationCountMap.get(staff.getEmployeeRole());
					list.add(staff);
					designationCountMap.put(staff.getEmployeeRole(), list);
				}else{
					list = new ArrayList<EmployeeProjectAllocation>();
					list.add(staff);
					designationCountMap.put(staff.getEmployeeRole(), list);
				}
			}
			
		}
		
		Map<String , ArrayList<EmployeeProjectAllocation>> newdesignationCountMap = new HashMap<String ,ArrayList<EmployeeProjectAllocation>>();
		newdesignationCountMap.putAll(designationCountMap);
		
		for (int i = 0; i < cnc.getSaffingDetailsList().size(); i++) {
			int count = 0;
			list = new ArrayList<EmployeeProjectAllocation>();
			if(designationCountMap.containsKey(cnc.getSaffingDetailsList().get(i).getDesignation())){
				list = designationCountMap.get(cnc.getSaffingDetailsList().get(i).getDesignation());
				employeeProjectAllocationList.addAll(list);
				if(newdesignationCountMap.size()!=0){
					newdesignationCountMap.remove(cnc.getSaffingDetailsList().get(i).getDesignation());
				}
			}
			if(cnc.getSaffingDetailsList().get(i).getNoOfStaff() >= list.size()){
				count = list.size();
			}
			System.out.println("count ="+count);
			System.out.println("cnc.getSaffingDetailsList().get(i).getDesignation()"+cnc.getSaffingDetailsList().get(i).getDesignation());
			for (int j = count; j < cnc.getSaffingDetailsList().get(i).getNoOfStaff(); j++) {
				StaffingDetails staffingDetails=new StaffingDetails();
				staffingDetails=cnc.getSaffingDetailsList().get(i);
				EmployeeProjectAllocation employeeProject=new EmployeeProjectAllocation(staffingDetails.getDesignation(),"","","","");
				/** date 22.8.2018 added by komal**/
			//	if(employeeProject.getStartDate() == null)
				employeeProject.setStartDate(cnc.getContractStartDate());
				employeeProject.setEndDate(cnc.getContractEndDate());
				/** end komal**/
				employeeProjectAllocationList.add(employeeProject);
				System.out.println("added");
			}
		}
		
		if(projectAllocation.getCount() != 0 && newdesignationCountMap!=null && newdesignationCountMap.size()!=0){
			for (Map.Entry<String, ArrayList<EmployeeProjectAllocation>> projectallocation : newdesignationCountMap.entrySet()) {
				System.out.println("key name" + projectallocation.getKey());
				ArrayList<EmployeeProjectAllocation> projectallocationlist = projectallocation.getValue();
				System.out.println("projectallocationlist =="+projectallocationlist.size());
				employeeProjectAllocationList.addAll(projectallocationlist);
			
			}
		}

		
		
		projectAllocation.setEmployeeProjectAllocationList(employeeProjectAllocationList);
		
		/**
		 * @author Vijay @Since 19-09-2022
		 * if Employee Project Allocation list greater than 600 then storing it into seperate intity 
		 */
		if(projectAllocation.getEmployeeProjectAllocationList().size()>AppConstants.employeeProjectAllocationListNumber || projectAllocation.isSeperateEntityFlag()){
			logger.log(Level.SEVERE,"for updating seperate entity");
				projectAllocation.getEmployeeProjectAllocationList().clear();
				CommonServiceImpl commonservice = new CommonServiceImpl();
				commonservice.updateEmployeeProjectAllocationList(employeeProjectAllocationList, "Save");
		}
		/**
		 * ends here
		 */
		
		ArrayList<Consumables> listConsumables=new ArrayList<Consumables>();
		listConsumables.addAll(cnc.getConsumablesList());
		projectAllocation.setConsumablesList(listConsumables);
		
		ArrayList<EquipmentRental> listEquipment=new ArrayList<EquipmentRental>();
		listEquipment.addAll(cnc.getEquipmentRentalList());
		projectAllocation.setEquipmentRentalList(listEquipment);
		
		ArrayList<OtherServices> listOtherServices=new ArrayList<OtherServices>();
		listOtherServices.addAll(cnc.getOtherServicesList());
		projectAllocation.setOtherServicesList(listOtherServices);
		
		projectAllocation.setCompanyId(cnc.getCompanyId());
		projectAllocation.setPayrollStartDay(cnc.getPayrollStartDay());
		/**
		 * @author Anil
		 * @since 20-08-2020
		 */
		projectAllocation.setState(cnc.getState());
//		NumberGeneration ng = new NumberGeneration();
//		ng = ofy().load().type(NumberGeneration.class)
//				.filter("companyId",cnc.getCompanyId())
//				.filter("processName", "ProjectAllocation").filter("status", true)
//				.first().now();
//		long number=ng.getNumber();
//		int count=(int)number;
//		ng.setNumber(count+1);
//		ofy().save().entity(ng);
//		projectAllocation.setCount(count+1);
//		ofy().save().entity(projectAllocation);
		impl.save(projectAllocation);
		/** date 18.8.2018 added by komal **/
		//generateHRProject(projectAllocation);
		
		return "Project Successfully Created!";
		
	}
	/** date 18.8.2018 added by komal **/
	public void generateHRProject(ProjectAllocation projectAllocation) {
		logger.log(Level.SEVERE, "Gerating HR Project ");
		boolean updateFlag = true;
		HrProject hrProject= ofy().load().type(HrProject.class).filter("companyId", projectAllocation.getCompanyId()).filter("cncProjectCount", projectAllocation.getCount()).first().now();
		if(hrProject == null){
			updateFlag = false;
			hrProject= new HrProject();
		}
		hrProject.setPersoninfo(projectAllocation.getPersonInfo());
		if(projectAllocation.getStartDate()!=null){
			hrProject.setFromDate(projectAllocation.getStartDate());
		}
		if(projectAllocation.getEndDate()!=null){
			hrProject.setToDate(projectAllocation.getEndDate());
		}
		if(projectAllocation.getProjectName()!=null){
			hrProject.setProjectName(projectAllocation.getProjectName());
		}
		hrProject.setCncProjectCount(projectAllocation.getCount());
		hrProject.setStatus(true);
		if(projectAllocation.getSupervisorName()!= null){
			hrProject.setSupervisor(projectAllocation.getSupervisorName());
		}
		if(projectAllocation.getManagerName()!=null){
			hrProject.setManager(projectAllocation.getManagerName());
		}
		if(projectAllocation.getBranchMangerName()!=null){
			hrProject.setBranchManager(projectAllocation.getBranchMangerName());
		}
		
		/**
		 * Date : 11-10-2018 BY ANIL
		 * Setting CNC branch to HR Project Branch
		 */
		
		if(projectAllocation.getBranch()!=null){
			hrProject.setBranch(projectAllocation.getBranch());
		}
		
		if(projectAllocation.getCustomerBranch()!=null){
			hrProject.setCustomerBranch(projectAllocation.getCustomerBranch());
		}
		
		hrProject.setCompanyId(projectAllocation.getCompanyId());
		/**
		 * @author Vijay @Since 21-09-2022
		 * Loading employee project allocation from different entity
		 */
		   CommonServiceImpl commonservice = new CommonServiceImpl();
	   
//		ArrayList<EmployeeProjectAllocation> list = projectAllocation.getEmployeeProjectAllocationList();
//		logger.log(Level.SEVERE, "list size "+list.size());
		ArrayList<EmployeeProjectAllocation> list = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
		 /**
	     * ends here
	     */
		
		ArrayList<Overtime> overtimeList = new ArrayList<Overtime>();
		ArrayList<Integer> otIdList = new ArrayList<Integer>();
		
		if(updateFlag){
			
			/**
			 * @author Vijay Date :- 08-09-2022
			 * Des :- used common method to read OT from project or from entity HrProjectOvertime`
			 */
			ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
			overtimelist = commonservice.getHRProjectOvertimelist(hrProject);
			/**
			 * ends here
			 */
			
			ArrayList<Overtime> hrEmpList = new ArrayList<Overtime>();
			
				for(EmployeeProjectAllocation allocation:list){
					int count = 0;
					boolean flag = false;
//					for (Overtime ot : hrProject.getOtList()) {
					for (Overtime ot : overtimelist) {
						try {
							if (ot.getEmpId() == Integer.parseInt(allocation.getEmployeeId())) {
								if (allocation.getShift() != null) {
									ot.setShift(allocation.getShift());
								}
								if (allocation.getStartDate() != null) {
									ot.setStartDate(allocation.getStartDate());
								}
								if (allocation.getEndDate() != null) {
									ot.setEndDate(allocation.getEndDate());
								}
								/**
								 * @author Anil
								 * @since 24-08-2020
								 * setting site location from project allocation to Hr project
								 */
								ot.setSiteLocation(allocation.getSiteLocation());
								
								hrEmpList.add(ot);
								flag = true;
							}
						} catch (Exception e) {
	
						}
					}
					if(!flag){


						 Overtime ot = new Overtime();
							
							try{
								ot.setEmpId(Integer.parseInt(allocation.getEmployeeId()));
							}catch(Exception e){
								ot.setEmpId(0);
							}
							ot.setEmpName(allocation.getEmployeeName());
							if(allocation.getShift()!=null){
								ot.setShift(allocation.getShift());
							}
							if(allocation.getOvertime()!=null){
								ot.setName(allocation.getOvertime());
							}
							if(allocation.getEmployeeRole()!=null){
								ot.setEmpDesignation(allocation.getEmployeeRole());
							}
							ot.setCompanyId(projectAllocation.getCompanyId());
							ot.setCount(allocation.getOtCount());
							if(allocation.getStartDate()!=null){
								ot.setStartDate(allocation.getStartDate());
							}
							if(allocation.getEndDate()!=null){
								ot.setEndDate(allocation.getEndDate());
							}
							/**
							 * @author Anil
							 * @since 24-08-2020
							 * setting site location from project allocation to Hr project
							 */
							ot.setSiteLocation(allocation.getSiteLocation());
							hrEmpList.add(ot);
					
					
					}
			}
			hrProject.setOtList(hrEmpList);
		}else{
		for(EmployeeProjectAllocation allocation:list){
			otIdList.add(allocation.getOtCount());
		}
		List<Overtime> otList = new ArrayList<Overtime>();
		if(otIdList.size() >0){
			otList = ofy().load().type(Overtime.class).filter("companyId", projectAllocation.getCompanyId())
									.filter("count IN", otIdList).list();
		}
		HashMap<Integer , Overtime> otMap = new HashMap<Integer , Overtime>();
		for(Overtime overtime : otList){
			
			otMap.put(overtime.getCount(), overtime.Myclone());
		}
		Overtime ot , ot1 = null;
		for(EmployeeProjectAllocation allocation:list){
			 ot = new Overtime();
			if(otMap.containsKey(allocation.getOtCount())){
				ot1 = new Overtime();
				ot1 = otMap.get(allocation.getOtCount()).Myclone();
				ot = Myclone(ot1);
			}
			try{
				ot.setEmpId(Integer.parseInt(allocation.getEmployeeId()));
			}catch(Exception e){
				ot.setEmpId(0);
			}
			ot.setEmpName(allocation.getEmployeeName());
			if(allocation.getShift()!=null){
				ot.setShift(allocation.getShift());
			}
			if(allocation.getOvertime()!=null){
				ot.setName(allocation.getOvertime());
			}
			if(allocation.getEmployeeRole()!=null){
				ot.setEmpDesignation(allocation.getEmployeeRole());
			}
			ot.setCompanyId(projectAllocation.getCompanyId());
			ot.setCount(allocation.getOtCount());
			if(allocation.getStartDate()!=null){
				ot.setStartDate(allocation.getStartDate());
			}
			if(allocation.getEndDate()!=null){
				ot.setEndDate(allocation.getEndDate());
			}
			/**
			 * @author Anil
			 * @since 24-08-2020
			 * setting site location from project allocation to Hr project
			 */
			ot.setSiteLocation(allocation.getSiteLocation());
			overtimeList.add(ot);
		}
		hrProject.setOtList(overtimeList);
		}
		/**
		 * date 10.1.2019 added by komal
		 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
		 *               which one is allowed to mark attendance
		 **/
		
		hrProject.setSubmitFrequency(projectAllocation.getSubmitFrequency());
		hrProject.setSyncFrequency(projectAllocation.getSyncFrequency());
		hrProject.setAllowedRadius(projectAllocation.getAllowedRadius());
		hrProject.setLocationLatitude(projectAllocation.getLocationLatitude());
		hrProject.setLocationLongitude(projectAllocation.getLocationLongitude());
		hrProject.setMiOtMinutes(projectAllocation.getMiOtMinutes());
		/**
		 * @author Anil
		 * @since 24-07-2020
		 */
		hrProject.setPayrollStartDay(projectAllocation.getPayrollStartDay());
		
		/**
		 * @author Anil
		 * @since 20-08-2020
		 */
		hrProject.setState(projectAllocation.getState());
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(hrProject);
//		NumberGeneration ng = new NumberGeneration();
//		ng = ofy().load().type(NumberGeneration.class)
//				.filter("companyId",projectAllocation.getCompanyId())
//				.filter("processName", "HrProjects").filter("status", true)
//				.first().now();
//		long number=ng.getNumber();
//		int count=(int)number;
//		ng.setNumber(count+1);
//		ofy().save().entity(ng);
//		hrProject.setCount(count+1);
//		ofy().save().entity(hrProject);
		
		

	}
	private ArrayList<SalesOrderProductLineItem> getSalesLineItemList(Date fromDate, Date toDate , Long companyId ,CNC cnc , HashMap<String ,ArrayList<Attendance>> attendanceMap,List<EmployeeInfo> empInfoList, int noofdays , String parameter,Customer customer ,Branch branch,SalesOrder salesOrder,
			HashMap<String , ArrayList<Integer>> employeeMap){
		ArrayList<SalesOrderProductLineItem> productList = new ArrayList<SalesOrderProductLineItem>();
		SalesOrderProductLineItem salesLineItem;
		boolean uploadConsumableFlag=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget", companyId);
		if(parameter.equalsIgnoreCase(AppConstants.STAFFING) || parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL) 
				|| parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY) || parameter.equalsIgnoreCase(AppConstants.OVERTIME)
				 || parameter.equalsIgnoreCase(AppConstants.ARREARSBILL)|| parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
			if(attendanceMap.size() > 0){
				
				logger.log(Level.SEVERE ,parameter);
				
				List<String> processNameList=new ArrayList<String>();
				processNameList.add("CNC");
				processNameList.add("PaySlip");
				
				List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName IN", processNameList).filter("configStatus", true).list();
				boolean attendanceFlag = false , nhOtFlag = false, otRateFlag = false;
				
				if(processConfigList!=null){
					for(ProcessConfiguration processConf:processConfigList){
						for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
							if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
									&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("PayrollByAttendance")){
								attendanceFlag=true;
							}
							
//							if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
//									&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("NH_OT_Include_In_Earning")){
//								nhOtFlag=true;
//							}
							
							if(processConf.getProcessName().equalsIgnoreCase("CNC")&&processConf.isConfigStatus()==true
									&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("EnableSiteLocation")){
								otRateFlag=true;
							}
						}
					}
				}
				
				logger.log(Level.SEVERE ,"otRateFlag "+otRateFlag);
				
				HashMap<String , Double> paidDaysMap = new HashMap<String , Double>();
				if(parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL) || parameter.equalsIgnoreCase(AppConstants.STAFFING)
						 || parameter.equalsIgnoreCase(AppConstants.ARREARSBILL)){
					paidDaysMap = loadPaidDaysOfEmployee(fromDate , toDate ,attendanceMap , noofdays , empInfoList , attendanceFlag , nhOtFlag);				
				}else if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)){
					paidDaysMap = loadDaysOfEmployee(fromDate ,toDate,attendanceMap, empInfoList, parameter,nhOtFlag,otRateFlag);
				}else if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
//					otHours=0;
					desgWiswOtMap=new HashMap<String, Double>();
					paidDaysMap = loadDaysOfEmployee(fromDate ,toDate ,attendanceMap, empInfoList, parameter,nhOtFlag,otRateFlag);
				}
				/**
				 * @author Anil
				 * @since 21-10-2020
				 */
				else if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)&&otRateFlag){
					paidDaysMap = loadDaysOfEmployee(fromDate ,toDate,attendanceMap, empInfoList, parameter,nhOtFlag,otRateFlag);
				}
				logger.log(Level.SEVERE ,"paidDaysMap size : "+paidDaysMap.size());
				
				ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productName", "Designation").first().now();
				 
				logger.log(Level.SEVERE , "Service Product "+ serviceProduct);
				String key = "";
				if(serviceProduct !=null){
				   for(StaffingDetails staff :cnc.getSaffingDetailsList()){
				   	  salesLineItem = getProductLineItem(serviceProduct,customer ,branch);							
					  salesLineItem.setProdName(staff.getDesignation());
					  double totalPay = 0 , mgntFees = 0 , total = 0;
					  try{
						  mgntFees = Double.parseDouble(staff.getManagementFees()) / staff.getNoOfStaff();
					  }catch(Exception e){
						  mgntFees = 0;
					  }
					  try{
						  total = Double.parseDouble(staff.getTotalOfPaidAndOverhead());
					  }catch(Exception e){
						  total = 0;
					  }
					  totalPay = total + mgntFees;
					  if(parameter != null && parameter.equalsIgnoreCase(AppConstants.ARREARSBILL)){
						  
						salesLineItem =  getArrearsPrice(fromDate , toDate ,salesLineItem,cnc,staff,totalPay,noofdays,employeeMap,mgntFees, empInfoList);
						  
					  }else{
					  
						  salesLineItem.setPrice(Math.round((totalPay / noofdays)*100.0)/100.0);
						  if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)){
							  salesLineItem.setPrice(2 *(totalPay / noofdays));
							  /*** Date 18-08-2020 by Vijay added Bill Sub Type at productLevel To calculate Man Days And Man Power **/
							  salesLineItem.setBillType("Holiday");
							 
							  /**
							   * @author Anil
							   * @since 21-10-2020
							   */
							  if(otRateFlag){
								  if(cnc.getOtRateDetailsList()!=null&&cnc.getOtRateDetailsList().size()!=0){
									  for(OtRateDetails otRate:cnc.getOtRateDetailsList()){
										  if(otRate.getDesignation().equals(staff.getDesignation())&&otRate.getOtName().equals("National Holiday OT")){
											  if(otRate.getFlatRate()!=0){
//												  if(otRate.getType().equals("Day")){
													  salesLineItem.setPrice(otRate.getFlatRate());
//												  }else if(otRate.getType().equals("Hourly")){
//													  if(cnc.getWorkingHours()!=0){
//														  salesLineItem.setPrice(Math.round(otRate.getFlatRate()*cnc.getWorkingHours())); 
//													  }
//												  }
											  }else if(otRate.getFactorRate()!=0){
//												  if(otRate.getType().equals("Day")){
													  salesLineItem.setPrice((Math.round((totalPay / noofdays)*100.0)/100.0)*otRate.getFactorRate());
//												  }else if(otRate.getType().equals("Hourly")){
//													  if(cnc.getWorkingHours()!=0){
//														  salesLineItem.setPrice(((Math.round((totalPay / noofdays)*100.0)/100.0)*cnc.getWorkingHours())*otRate.getFactorRate()); 
//													  }
//												  }
											  }
											  salesLineItem.setBillType(AppConstants.NATIONALHOLIDAY);
										  }
									  }
								  }
							  }
						  }else if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
							  if(staff.getPayableOTRates()!=0){
								  salesLineItem.setPrice(staff.getPayableOTRates());
							  }
							  /*** Date 18-08-2020 by Vijay added Bill Sub Type at productLevel To calculate Man Days And Man Power **/
							  salesLineItem.setBillType(AppConstants.OVERTIME);
							  
							  /**
							   * @author Anil
							   * @since 21-10-2020
							   */
							  if(otRateFlag){
								  if(cnc.getOtRateDetailsList()!=null&&cnc.getOtRateDetailsList().size()!=0){
									  for(OtRateDetails otRate:cnc.getOtRateDetailsList()){
										  if(otRate.getDesignation().equals(staff.getDesignation())&&otRate.getOtName().equals("Normal Day OT")){
											  if(otRate.getFlatRate()!=0){
//												  if(otRate.getType().equals("Day")){
													  salesLineItem.setPrice(otRate.getFlatRate());
//												  }else if(otRate.getType().equals("Hourly")){
//													  if(cnc.getWorkingHours()!=0){
//														  salesLineItem.setPrice(Math.round(otRate.getFlatRate()*cnc.getWorkingHours())); 
//													  }
//												  }
											  }else if(otRate.getFactorRate()!=0){
//												  if(otRate.getType().equals("Day")){
													  salesLineItem.setPrice((Math.round((totalPay / noofdays)*100.0)/100.0)*otRate.getFactorRate());
//												  }else if(otRate.getType().equals("Hourly")){
//													  if(cnc.getWorkingHours()!=0){
//														  salesLineItem.setPrice(((Math.round((totalPay / noofdays)*100.0)/100.0)*cnc.getWorkingHours())*otRate.getFactorRate());  
//													  }
//												  }
											  }
										  }
									  }
								  }
							  }
						  }else if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
							  if(staff.getPayableOTRates()!=0){
								  salesLineItem.setPrice(staff.getPayableOTRates());
							  }
							  salesLineItem.setBillType(AppConstants.PUBLICHOLIDAY);
							  
							  /**
							   * @author Anil
							   * @since 21-10-2020
							   */
							  if(otRateFlag){
								  if(cnc.getOtRateDetailsList()!=null&&cnc.getOtRateDetailsList().size()!=0){
									  for(OtRateDetails otRate:cnc.getOtRateDetailsList()){
										  if(otRate.getDesignation().equals(staff.getDesignation())&&otRate.getOtName().equals("Public Holiday OT")){
											  if(otRate.getFlatRate()!=0){
//												  if(otRate.getType().equals("Day")){
													  salesLineItem.setPrice(otRate.getFlatRate());
//												  }else if(otRate.getType().equals("Hourly")){
//													  if(cnc.getWorkingHours()!=0){
//														  salesLineItem.setPrice(Math.round(otRate.getFlatRate()*cnc.getWorkingHours())); 
//													  }
//												  }
											  }else if(otRate.getFactorRate()!=0){
//												  if(otRate.getType().equals("Day")){
													  salesLineItem.setPrice((Math.round((totalPay / noofdays)*100.0)/100.0)*otRate.getFactorRate());
//												  }else if(otRate.getType().equals("Hourly")){
//													  if(cnc.getWorkingHours()!=0){
//														  salesLineItem.setPrice(((Math.round((totalPay / noofdays)*100.0)/100.0)*cnc.getWorkingHours())*otRate.getFactorRate());  
//													  }
//												  }
											  }
											  salesLineItem.setBillType(AppConstants.PUBLICHOLIDAY);
										  }
									  }
								  }
							  }
						  }
					  }
					  if(paidDaysMap.containsKey(staff.getDesignation())){
						  if(paidDaysMap.get(staff.getDesignation()) == 0){
							  if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY) || parameter.equalsIgnoreCase(AppConstants.OVERTIME)|| parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
								  continue;
							  }else{
								  salesLineItem.setArea("NA");
								  /**
								   * @author Vijay Chougule
								   * @Since 18-08-2020
								   * Des :- adding billType To product Level To Calculate Total Man Days And Total Man Power 
								   */
								  salesLineItem.setBillType(getBillType(parameter));

							  }
						  }else{
							  salesLineItem.setArea(paidDaysMap.get(staff.getDesignation())+"");
							  /**
							   * @author Anil @since 05-04-2021
							   * For OT bills calculation we are picking working hours from CNC
							   * Raised by Rahul for Alkosh
							   */
							  if(staff.getWorkingHours()!=0){
								 if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
									 logger.log(Level.SEVERE,"OVERTIME BILL WORKING HOURS NOT ZERO "+staff.getWorkingHours());
									 double ot=0;
									 if(desgWiswOtMap!=null&&desgWiswOtMap.size()!=0){
										 if(desgWiswOtMap.containsKey(staff.getDesignation())){
											 ot=desgWiswOtMap.get(staff.getDesignation());
											 logger.log(Level.SEVERE,"OT "+ot);
										 }
									 }
									 if(ot!=0){
										 double totalWorkedHours=ot/staff.getWorkingHours();
										 logger.log(Level.SEVERE,"totalWorkedHours "+totalWorkedHours);
										 salesLineItem.setArea(totalWorkedHours+"");
									 }
								 }
							  }
							  
							  if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY) || parameter.equalsIgnoreCase(AppConstants.OVERTIME)|| parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
									HashMap<String , Integer> noofStaffMap = new HashMap<String , Integer>();
									noofStaffMap =  loadNoOFStaff(fromDate ,toDate,attendanceMap, empInfoList, parameter,nhOtFlag,otRateFlag);
									salesLineItem.setManpower(noofStaffMap.get(staff.getDesignation())+"");
									 logger.log(Level.SEVERE,"No of staff from Attendance == "+salesLineItem.getManpower());

							  }
							  else {
							  /*** Date 13-08-2020 by Vijay for Sasha (standard) showing Man power (no of employee in the attendance)in billing details ***/
								 int number = getNoofEmployeeOfattedanace(staff.getDesignation(), attendanceMap,companyId,empInfoList);
							  		logger.log(Level.SEVERE,"No of staff from Attendance"+number);
								 salesLineItem.setManpower(number +"");
							  }
						  
							  /**
							   * @author Vijay Chougule
							   * @Since 18-08-2020
							   * Des :- adding billType To product Level To Calculate Total Man Days And Total Man Power 
							   */
							  salesLineItem.setBillType(getBillType(parameter));
							  
							  /**
							   * @author Vijay Chougule Date 16-09-2020
							   * Des :- No Of staff (ManPower) value from CNC not from the attendance if process config is active 
							   * requirment raised by Rahul only for Labour
							   */
							  	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.BILLINGDOCUMENT, AppConstants.PC_PICKkNOOFStAFFFROMCNC, companyId)
							  			&& parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL) || parameter.equalsIgnoreCase(AppConstants.STAFFING)){
							  		salesLineItem.setManpower(staff.getNoOfStaff()+"");
							  		logger.log(Level.SEVERE,"No of staff from CNC"+staff.getNoOfStaff());
							  	}
							  	/**
							  	 * ends here
							  	 */
							  	
							  
						  }
						  salesLineItem.setTotalAmount(paidDaysMap.get(staff.getDesignation()) * salesLineItem.getPrice());
						  
						  /**
						   * @author Anil @since 05-04-2021
						   * For OT bills calculation we are picking working hours from CNC
						   * Raised by Rahul for Alkosh
						   * @author Anil @since 05-05-2021
						   * bill creation issue at the time of bill creation
						   */
						  if(staff.getWorkingHours()!=0&&salesLineItem.getArea()!=null&&!salesLineItem.getArea().equals("")&&!salesLineItem.getArea().equals("NA")&&parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
							  salesLineItem.setTotalAmount(Double.parseDouble(salesLineItem.getArea()) * salesLineItem.getPrice());
						  }
						  
					  }else{
						  salesLineItem.setArea("NA");
						  salesLineItem.setTotalAmount(0.0);
						  
						  /**
						   * @author Vijay Chougule
						   * @Since 18-08-2020
						   * Des :- adding billType To product Level To Calculate Total Man Days And Total Man Power 
						   */
						  salesLineItem.setBillType(getBillType(parameter));

					  }							
					 
					  salesLineItem.setDiscountAmt(0.0);
					  salesLineItem.setBaseBillingAmount(salesLineItem.getTotalAmount());
					  salesLineItem.setPaymentPercent(100.0);
					  salesLineItem.setBasePaymentAmount(salesLineItem.getTotalAmount());
					 /**
					  * @author Vijay Date 16-09-2020
					  * Des :- added management fees and eact staff CTC amount in billing document to use in invoice, requirement raised by Rahul Tiwari
					  */
					  if(staff.getManagementFees()!=null && !staff.getManagementFees().equals("")){
						  if(!salesLineItem.getBillType().equals(AppConstants.ARREARSBILL)){
							  double managementfees = (Double.parseDouble(staff.getManagementFees())/ staff.getNoOfStaff());
							  System.out.println("managementfees "+managementfees);
							  salesLineItem.setManagementFees(managementfees);
						  }else{
							  logger.log(Level.SEVERE, "getManagementFees FEES "+salesLineItem.getManagementFees());
						  }
					  }
					  if(staff.getTotalOfPaidAndOverhead()!=null && !staff.getTotalOfPaidAndOverhead().equals("")){
						  if(!salesLineItem.getBillType().equals(AppConstants.ARREARSBILL)){
							  salesLineItem.setCtcAmount(Double.parseDouble(staff.getTotalOfPaidAndOverhead()));
						  }else{
							  logger.log(Level.SEVERE, "getCtcAmount FEES  FEES "+salesLineItem.getCtcAmount());
						  }
					  }
					  /**
					   * ends here
					   */
					  
					  productList.add(salesLineItem);
				   }
				}
				
				for (Map.Entry<String,Double> entry : paidDaysMap.entrySet())  {
		            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue()); 
		            if(entry.getKey().contains("Additional")){
		            	salesLineItem = getProductLineItem(serviceProduct,customer,branch);							
						  salesLineItem.setProdName(entry.getKey());
						  salesLineItem.setPrice(serviceProduct.getPrice());
						  salesLineItem.setArea(entry.getValue()+"");
						  salesLineItem.setTotalAmount(entry.getValue() * salesLineItem.getPrice());
						  salesLineItem.setDiscountAmt(0.0);
						  salesLineItem.setBaseBillingAmount(salesLineItem.getTotalAmount());
						  salesLineItem.setPaymentPercent(100.0);
						  salesLineItem.setBasePaymentAmount(salesLineItem.getTotalAmount());
						  productList.add(salesLineItem);
		              }
				}
			}
		}
		if(parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL) || parameter.equalsIgnoreCase(AppConstants.EQUIPMENTRENTAL)){
			if(cnc.getEquipmentRentalList().size() > 0){
//				ArrayList<Integer> productIdList = new ArrayList<Integer>();
//				ArrayList<String> productNameList = new ArrayList<String>();
//				for(EquipmentRental rental : cnc.getEquipmentRentalList()){
//					if(rental.getProductId()!=0){
//						productIdList.add(rental.getProductId());
//					}
//					productNameList.add(rental.getMachine());
//				}
//				List<ItemProduct> itemList = new ArrayList<ItemProduct>();
//				if(productIdList.size() > 0 && productIdList.size() == cnc.getEquipmentRentalList().size()){
//					itemList = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("count IN", productIdList).list();
//				}else if(productNameList.size()>0){
//					itemList = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productName IN", productNameList).list();
//				}
			ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productName", "Equipment").first().now();				 
			logger.log(Level.SEVERE , "Service Product "+ serviceProduct);
			
			salesLineItem = getProductLineItem(serviceProduct,customer,branch);	
			salesLineItem.setProdName("Housekeeping Equipment Charges");
			salesLineItem.setTotalAmount(cnc.getNetPayableOFEquipment());
			salesLineItem.setBaseBillingAmount(cnc.getNetPayableOFEquipment());
			salesLineItem.setBasePaymentAmount(cnc.getNetPayableOFEquipment());
			salesLineItem.setArea(1+"");
			salesLineItem.setPrice(cnc.getNetPayableOFEquipment());
			salesLineItem.setPaymentPercent(100.0);
			/*** Date 18-08-2020 by Vijay added Bill Sub Type at productLevel To calculate Man Days And Man Power **/
			 salesLineItem.setBillType("Rental");
			productList.add(salesLineItem);
		  }
		}
		if(parameter.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
			ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productName", "Consumable").first().now();				 
			logger.log(Level.SEVERE , "Service Product "+ serviceProduct+" uploadConsumableFlag "+uploadConsumableFlag);
			double amount = 0;
			double totalAmount = 0;
			if (cnc != null) {
				for(Consumables consumables : cnc.getConsumablesList()){
					/**
					 * @author Anil @since 12-11-2021
					 * If Consumable with site location is active then
					 * check bill type for particular location
					 * Sunrise SCM
					 */
					if(uploadConsumableFlag){
						try{
							if(salesOrder!=null&&salesOrder.getCustBranch()!=null&&!salesOrder.getCustBranch().equals("")&&consumables.getSiteLocation()!=null&&!consumables.getSiteLocation().equals("")){
								if(salesOrder.getCustBranch().equals(consumables.getSiteLocation())){
									if(consumables.getBillingType().equalsIgnoreCase("FIXED")){
										try{
											amount = Double.parseDouble(consumables.getChargable());
										}catch(Exception e){
											amount = 0;
										}
										totalAmount = totalAmount + amount;
									}
									break;
								}
							}
						}catch(Exception e){
							
						}
					}else{
						if(consumables.getBillingType().equalsIgnoreCase("FIXED")){
							try{
								amount = Double.parseDouble(consumables.getChargable());
							}catch(Exception e){
								amount = 0;
							}
							totalAmount = totalAmount + amount;
						}
					}
				}
			}	
			salesLineItem = getProductLineItem(serviceProduct,customer,branch);
			salesLineItem.setPrice(totalAmount);
			salesLineItem.setProdName("Housekeeping Consumables");
			salesLineItem.setTotalAmount(totalAmount);
			salesLineItem.setBaseBillingAmount(totalAmount);
			salesLineItem.setBasePaymentAmount(totalAmount);
			salesLineItem.setArea(1+"");
			salesLineItem.setPaymentPercent(100.0);
			/*** Date 18-08-2020 by Vijay added Bill Sub Type at productLevel To calculate Man Days And Man Power **/
			 salesLineItem.setBillType("Consumable");
			 
			 /**
			  * @author Anil @since 13-11-2021
			  * Sunrise SCM
			  */
			 if(uploadConsumableFlag){
				 try{
					salesLineItem.setSalesAnnexureMap(getSalesAnnexureDetails(salesOrder, salesLineItem));
					salesLineItem.setBillProductName(salesLineItem.getProdName());
				 }catch(Exception e){
					 logger.log(Level.SEVERE, "Exception while setting sales annexure");
				 }
			 }
		

			productList.add(salesLineItem);

		}
		if(parameter.equalsIgnoreCase(AppConstants.CONSUMABLES)){
			
			if(salesOrder != null){
				ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productName", "Consumable").first().now();				 
				logger.log(Level.SEVERE , "Service Product "+ serviceProduct);
				HashSet<Double> percentSet = new HashSet<Double>();
				HashMap<Double, HashMap<String , ProductOtherCharges>> taxMap = new HashMap<Double , HashMap<String ,ProductOtherCharges>>();
				HashMap<String , ProductOtherCharges> map = new HashMap<String , ProductOtherCharges>();
				for(ProductOtherCharges charges : salesOrder.getProductTaxes()){
					percentSet.add(charges.getChargePercent());
					if(taxMap.containsKey(charges.getChargePercent())){
						  map = taxMap.get(charges.getChargePercent());
						  
						  map.put(charges.getChargeName(), charges);
						  taxMap.put(charges.getChargePercent(), map);
					}else{
						  map = new HashMap<String , ProductOtherCharges>();
						 
						  map.put(charges.getChargeName(), charges);
						  taxMap.put(charges.getChargePercent(), map);
					}
					
				}
				for (Map.Entry<Double, HashMap<String , ProductOtherCharges>> entry : taxMap.entrySet())  {
		            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
		        salesLineItem = new SalesOrderProductLineItem();
				salesLineItem = getProductLineItem(serviceProduct,customer,branch);	
				
				salesLineItem.setProdName("Housekeeping Consumables @"+entry.getKey()+" GST");
				for (Map.Entry<String , ProductOtherCharges> entry1 : entry.getValue().entrySet())  {
		            logger.log(Level.SEVERE ,"Key = " + entry1.getKey() + ", Value = " + entry1.getValue());
		            ProductOtherCharges charges = entry1.getValue();
		            Tax tax1 = new Tax();
					tax1.setTaxName(charges.getChargeName()+"@"+charges.getChargePercent());
					tax1.setPercentage(charges.getChargePercent());
					tax1.setTaxPrintName(charges.getChargeName());
					tax1.setTaxConfigName(charges.getChargeName()+"@"+charges.getChargePercent());
		            if(entry1.getKey().equals("CGST") || entry1.getKey().equals("IGST") ){			            
		            	salesLineItem.setServiceTax(tax1);
		            }else if(entry1.getKey().equals("SGST")){
		            	salesLineItem.setVatTax(tax1);
		            }
		            if(entry1.getKey().equalsIgnoreCase("IGST")){
		              Tax tax = new Tax();
		  			  tax.setTaxName("NA");
		  			  tax.setPercentage(0);
		  			  tax.setTaxPrintName(" ");
		  			  tax.setTaxConfigName("NA");
		  			  salesLineItem.setVatTax(tax);
		            }
					
						
						salesLineItem.setPrice(charges.getAssessableAmount());
					
		            
				}
				salesLineItem.setTotalAmount(salesLineItem.getPrice());
				salesLineItem.setBaseBillingAmount(salesLineItem.getPrice());
				salesLineItem.setBasePaymentAmount(salesLineItem.getPrice());
				salesLineItem.setPaymentPercent(100.0);
				salesLineItem.setArea(1+"");
				/*** Date 18-08-2020 by Vijay added Bill Sub Type at productLevel To calculate Man Days And Man Power **/
				 salesLineItem.setBillType("Consumable");
				 
				 /**
				  * @author Anil @since 15-11-2021
				  * Sunrise SCM
				  */
				 if(uploadConsumableFlag){
					 try{
						salesLineItem.setSalesAnnexureMap(getSalesAnnexureDetails(salesOrder, salesLineItem));
						salesLineItem.setBillProductName(salesLineItem.getProdName());
					 }catch(Exception e){
						 logger.log(Level.SEVERE, "Exception while setting sales annexure");
					 }
				 }
				

				productList.add(salesLineItem);
				}
				double taxAmount = 0;
				for(SalesLineItem item :salesOrder.getItems()){
					if(item.getServiceTax() != null && item.getVatTax() != null){
						logger.log(Level.SEVERE, "0 TAX AMOUNT :" + item.getServiceTax().getPercentage() +
								" "+item.getVatTax().getPercentage() +" "+item.getTotalAmount());
						double value = 0.0;
						if(item.getServiceTax().getPercentage() <= value && item.getVatTax().getPercentage() <= 0.0){
							taxAmount += calculateTotal(item);
						}
					}else if(item.getServiceTax() == null && item.getVatTax() == null){
						taxAmount += calculateTotal(item);
					}
				}
				logger.log(Level.SEVERE, "0 TAX AMOUNT :" + taxAmount);
				if(taxAmount > 0){
					salesLineItem = new SalesOrderProductLineItem();
					salesLineItem = getProductLineItem(serviceProduct,customer,branch);	
					
					salesLineItem.setProdName("Housekeeping Consumables @"+"0.0"+" GST");
					Tax tax = new Tax();
					tax.setTaxName("NA");
		  			tax.setPercentage(0);
		  			tax.setTaxPrintName(" ");
		  			tax.setTaxConfigName("NA");
		  			
		  			/**
		  			 * @author Anil @since 17-11-2021
		  			 */
		  			Tax tax1 = new Tax();
					tax1.setTaxName("NA");
		  			tax1.setPercentage(0);
		  			tax1.setTaxPrintName(" ");
		  			tax1.setTaxConfigName("NA");
		  			
		  			salesLineItem.setVatTax(tax);
		  			salesLineItem.setServiceTax(tax1);
		            
		  			salesLineItem.setPrice(taxAmount);
		  			salesLineItem.setTotalAmount(salesLineItem.getPrice());
					salesLineItem.setBaseBillingAmount(salesLineItem.getPrice());
					salesLineItem.setBasePaymentAmount(salesLineItem.getPrice());
					salesLineItem.setPaymentPercent(100.0);
					salesLineItem.setArea(1+"");
					
					/**
					 * @author Anil @since 15-11-2021
					 * if tax amount was not set then no bill type was set
					 * Sunrise SCM
					 */
					 salesLineItem.setBillType("Consumable");
					 
					 /**
					  * @author Anil @since 15-11-2021
					  * Sunrise SCM
					  */
					 if(uploadConsumableFlag){
						 try{
							salesLineItem.setSalesAnnexureMap(getSalesAnnexureDetails(salesOrder, salesLineItem));
							salesLineItem.setBillProductName(salesLineItem.getProdName());
						 }catch(Exception e){
							 logger.log(Level.SEVERE, "Exception while setting sales annexure");
						 }
					 }
					 
					productList.add(salesLineItem);
				}
				
			}
		}		
		return productList;
	}
	

	/**
	 * @author Anil,Date : 07-05-2019
	 * Added parameter project name for loading attendance
	 * raised by Nitin sir,sonu
	 * @param empInfoList 
	 */
	public HashMap<String ,ArrayList<Attendance>> loadAttandace(Date fromDate ,Date toDate ,HashMap<String , ArrayList<Integer>> map , Long companyId, String projectName, List<EmployeeInfo> empInfoList){
		HashMap<String ,ArrayList<Attendance>> attendanceMap = new HashMap<String ,ArrayList<Attendance>>();
		List<Attendance> attendanceList = new ArrayList<Attendance>();
		List<Integer> idList = new ArrayList<Integer>();
		DateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		Calendar c = Calendar.getInstance();
		c.setTime(fromDate);
		c.set(Calendar.HOUR_OF_DAY,00);
		c.set(Calendar.MINUTE,00);
		c.set(Calendar.SECOND,00);
		c.set(Calendar.MILLISECOND,000);
		logger.log(Level.SEVERE , "From date :" + c.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(toDate);
		cal.set(Calendar.HOUR_OF_DAY,23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		cal.set(Calendar.MILLISECOND,999);
		logger.log(Level.SEVERE , "To date :" + cal.getTime());
		for (Map.Entry<String,ArrayList<Integer>> entry : map.entrySet())  {
//            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue()+" Project Name = "+projectName);
			logger.log(Level.SEVERE ,"Key = " + entry.getKey() +" Project Name = "+projectName);
            idList = entry.getValue();
            if(idList.size()>0){
            attendanceList=ofy().load().type(Attendance.class).filter("companyId",companyId).filter("empId IN", idList).filter("projectName", projectName).filter("attendanceDate >=", c.getTime()).filter("attendanceDate <=", cal.getTime()).list();
            
            List<Attendance> reliverattendelist = getreliverattendencelist(companyId,projectName, c.getTime(), cal.getTime(),idList,entry.getKey(),empInfoList);
            if(reliverattendelist.size()>0){
            	attendanceList.addAll(reliverattendelist);
            }
            if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", AppConstants.PC_ENABLESITELOCATION, companyId)) {
            	String str = getAttendenceWiseDesignationMap(attendanceList,entry.getKey(),attendanceMap,idList);
            }
            else 
            if(attendanceList != null){
            	logger.log(Level.SEVERE ,"Attendance List : " + attendanceList.size());
            	ArrayList<Attendance> list = new ArrayList<Attendance>();
            	list.addAll(attendanceList);
//            	logger.log(Level.SEVERE ,"Attendance List : " + attendanceList.toString());
            	attendanceMap.put(entry.getKey(), list);
              }else if(attendanceList == null && entry.getKey().contains("Additional")){
            	  ArrayList<Attendance> list = new ArrayList<Attendance>();
            	  attendanceMap.put(entry.getKey(), list);
              }
           
            }
            
		}
     	
		return attendanceMap;
	}
	

	

	
	private HashMap<String , Double> loadPaidDaysOfEmployee(Date fromDate , Date toDate ,HashMap<String ,ArrayList<Attendance>> attendanceMap , int totalPaidDays , List<EmployeeInfo> emInfoList , boolean attendanceFlag  ,boolean nhOtFlag){
		HashMap<String , Double> paidDaysMap = new HashMap<String , Double>();
		ArrayList<Attendance> attendanceList = new ArrayList<Attendance>();
		HashSet<Integer> set = new HashSet<Integer>();
		
		
		for (Map.Entry<String,ArrayList<Attendance>> entry : attendanceMap.entrySet())  {
//            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
            double totalDays = 0;
            set = new HashSet();
            for(Attendance att : entry.getValue()){
            	set.add(att.getEmpId());
            }
            for(Integer num : set){
            	attendanceList = getEmployeeAttendance(entry.getValue() , num);
            	DocumentUploadTaskQueue	taskQueue = new DocumentUploadTaskQueue();
        		EmployeeInfo info = taskQueue.getEmployeeInfo(num, emInfoList);
        		double unpaidLeaveHrs=getUnpaidLeaveHrs(fromDate, toDate, info);
            	double days = calculatePaidDays(attendanceList ,info , totalPaidDays , attendanceFlag , nhOtFlag , unpaidLeaveHrs);
            	if(entry.getKey().contains("Additional")){
                	if(info != null && info.getLeaveCalendar() != null){
                		totalDays += (days * info.getLeaveCalendar().getWorkingHours());
                	}
            	}else{
            		totalDays += days;
            	}
             
            }
            paidDaysMap.put(entry.getKey(), totalDays);
		}
		return paidDaysMap;
	}
	private ArrayList<Attendance> getEmployeeAttendance(ArrayList<Attendance> list , int num){
		ArrayList<Attendance> attList = new ArrayList<Attendance>();
		for(Attendance att : list){
			if(num == att.getEmpId()){
				attList.add(att);
			}
		}
		return attList;
	}
	private double calculatePaidDays(ArrayList<Attendance> attendanceList  , EmployeeInfo info , int totalPaidDays , boolean attendanceFlag , boolean nhOtFlag , double unpaidLeaveHrs){
		logger.log(Level.SEVERE , "Attendance List " + info.getEmpCount() + " "+attendanceList.size());
		double paidDays = 0;
		int extraDay = 0;
		int absentDays=0;
		int listSize = attendanceList.size();
		if(listSize == 0){
			listSize = totalPaidDays;
		}
		if(nhOtFlag){
			if(info != null && info.getLeaveCalendar()!=null){
				extraDay=paySlipService.getNationalHolidayDetails(attendanceList,info);
			}
		}
		absentDays=paySlipService.getTotalAbsentDays(attendanceList);	
		if(info != null && info.getLeaveCalendar() != null){
			paidDays=(listSize+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-absentDays;	
		}
		logger.log(Level.SEVERE , "paid days for emp id: " + info.getEmpCount() + " "+paidDays);
		return paidDays;
	}
	public double getUnpaidLeaveHrs(Date startDate , Date endDate ,EmployeeInfo info) 
	{
		Calendar c = Calendar.getInstance(); 
		ArrayList<AvailedLeaves> availedLeaves=new ArrayList<AvailedLeaves>();
		double leaveHrs=0d;
//		logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
//		logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
		
			Key<EmployeeInfo>employeeKey=Key.create(info);
//			List<EmployeeLeave> leaves=ofy().load().type(EmployeeLeave.class).filter("leaveDate >=",startDate).filter("employeeKey", employeeKey).filter("companyId",info.getCompanyId()).list();
			List<EmployeeLeave> leaves=ofy().load().type(EmployeeLeave.class).filter("companyId",info.getCompanyId()).filter("emplId", info.getEmpCount()).filter("leaveDate >=",startDate).list();
//			logger.log(Level.SEVERE,"Total Leave SIZE --- ::: "+leaves.size());
//			logger.log(Level.SEVERE,"Total Leave SIZE --- ::: "+leaves.size());
			List<EmployeeLeave> validleaves=new ArrayList<EmployeeLeave>();
			
			for(EmployeeLeave temp:leaves)
			{
				if(temp.getLeaveDate().equals(endDate)||temp.getLeaveDate().before(endDate)){
					validleaves.add(temp);
				}
			}
//			logger.log(Level.SEVERE,"valid Leaves SIZE --- ::: "+validleaves.size());
//			logger.log(Level.SEVERE,"valid Leaves SIZE --- ::: "+validleaves.size());
			for(EmployeeLeave temp:validleaves)
			{
				boolean flag=false;
				if(temp.getUnpaidLeave()==false){
					if(availedLeaves.size()==0){
						AvailedLeaves avLeav=new AvailedLeaves();
						avLeav.setLeaveName(temp.getLeaveTypeName());
						avLeav.setLeavehour(temp.getLeaveHrs());
						avLeav.setUnPaid(true);
						availedLeaves.add(avLeav);
					}else{
						for(int i=0;i<availedLeaves.size();i++){
							if(availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())){
//								logger.log(Level.SEVERE,"Inside Existingggggggg");
								flag=true;
								double hrs=0;
								hrs=availedLeaves.get(i).getLeavehour();
								hrs=hrs+temp.getLeaveHrs();
								availedLeaves.get(i).setLeavehour(hrs);
								availedLeaves.get(i).setUnPaid(true);
							}
						}
						if(flag==false){
//							logger.log(Level.SEVERE,"Inside Otherrrrrrrrrrr");
							AvailedLeaves avLeav=new AvailedLeaves();
							avLeav.setLeaveName(temp.getLeaveTypeName());
							avLeav.setLeavehour(temp.getLeaveHrs());
							avLeav.setUnPaid(true);
							availedLeaves.add(avLeav);
						}
					}
					
					
					leaveHrs=leaveHrs+temp.getLeaveHrs();
				}
				else{
					if(availedLeaves.size()==0){
						AvailedLeaves avLeav=new AvailedLeaves();
						avLeav.setLeaveName(temp.getLeaveTypeName());
						avLeav.setLeavehour(temp.getLeaveHrs());
						avLeav.setUnPaid(false);
						availedLeaves.add(avLeav);
					}else{
						
						for(int i=0;i<availedLeaves.size();i++){
							if(availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())){
								flag=true;
								double hrs=availedLeaves.get(i).getLeavehour();
								hrs=hrs+temp.getLeaveHrs();
								availedLeaves.get(i).setLeavehour(hrs);
								availedLeaves.get(i).setUnPaid(false);
							}
						}
						if(flag==false){
							AvailedLeaves avLeav=new AvailedLeaves();
							avLeav.setLeaveName(temp.getLeaveTypeName());
							avLeav.setLeavehour(temp.getLeaveHrs());
							avLeav.setUnPaid(false);
							availedLeaves.add(avLeav);
						}
					}
				}
//				logger.log(Level.SEVERE,"Availed Leave List SIZE----- ::"+availedLeaves.size());
//				logger.log(Level.SEVERE,"Availed Leave List SIZE in----- ::"+availedLeaves.size());
			}
		return leaveHrs;
		
	}
	private SalesOrderProductLineItem getProductLineItem(SuperProduct product,Customer customer ,Branch branch){
		  SalesOrderProductLineItem salesLineItem = new SalesOrderProductLineItem();
		  salesLineItem.setPrduct(product);				  
		  salesLineItem.setProdId(product.getCount());
		  if(product.getHsnNumber()!=null){
			  salesLineItem.setHsnCode(product.getHsnNumber());
		  }
		  salesLineItem.setProdCode(product.getProductCode());
		  salesLineItem.setProdCategory(product.getProductCategory());
		  salesLineItem.setProdName(product.getProductName());	
		  if(ServerAppUtility.isAddressStateDifferent(branch.getAddress(), customer.getAdress())){
			  Tax tax = new Tax();
			  tax.setTaxName("NA");
			  tax.setPercentage(0);
			  tax.setTaxPrintName(" ");
			  tax.setTaxConfigName("NA");
			  Tax tax1 = new Tax();
			  tax1.setTaxName("IGST@18");
			  tax1.setPercentage(18);
			  tax1.setTaxPrintName("IGST");
			  tax1.setTaxConfigName("IGST@18");
			  salesLineItem.setVatTax(tax);
			  salesLineItem.setServiceTax(tax1);
		  }else{
			  salesLineItem.setVatTax(product.getVatTax());
			  salesLineItem.setServiceTax(product.getServiceTax());
		  }
		  salesLineItem.setDiscountAmt(0.0);
		  salesLineItem.setPaymentPercent(100.0);
		  return salesLineItem;
	}
	private ItemProduct getItemProduct(List<ItemProduct> itemList , int prodId){
		ItemProduct product = new ItemProduct();
		for(ItemProduct item : itemList){
			if(item.getCount() == prodId){
				product = item;
				break;
			}
		}
		return product;
	}
	private HashMap<String , Double> loadDaysOfEmployee(Date fromDate , Date toDate ,HashMap<String ,ArrayList<Attendance>> attendanceMap , List<EmployeeInfo> emInfoList , String parameter ,boolean nhOtFlag, boolean otRateFlag){
		HashMap<String , Double> paidDaysMap = new HashMap<String , Double>();
		ArrayList<Attendance> attendanceList = new ArrayList<Attendance>();
		HashSet<Integer> set = new HashSet<Integer>();
		for (Map.Entry<String,ArrayList<Attendance>> entry : attendanceMap.entrySet())  {
//            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
			 logger.log(Level.SEVERE ,"Key = " + entry.getKey() );
            double totalDays = 0;
            set = new HashSet();
            for(Attendance att : entry.getValue()){
            	set.add(att.getEmpId());
            }
            double days = 0;
            for(Integer num : set){
            	attendanceList = getEmployeeAttendance(entry.getValue() , num);
            	DocumentUploadTaskQueue	taskQueue = new DocumentUploadTaskQueue();
        		EmployeeInfo info = taskQueue.getEmployeeInfo(num, emInfoList);
        		days = 0;
        		if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)){
        			days=paySlipService.getNationalHolidayDetails(attendanceList,info);
        			if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Direct")){
        				totalDays += (days/2);
					}else if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Indirect")){
						totalDays += days;
					}
        		}if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
        			
        			/**
        			 * @author Anil @since 05-04-2021
        			 */
        			double overtimeHours=getOverTimeHrs(info, fromDate, toDate, nhOtFlag,otRateFlag,entry.getKey());
//        			otHours+=overtimeHours;
        			if(desgWiswOtMap!=null&&desgWiswOtMap.size()!=0){
        				if(desgWiswOtMap.containsKey(entry.getKey())){
        					overtimeHours+=desgWiswOtMap.get(entry.getKey());
        					desgWiswOtMap.put(entry.getKey(), overtimeHours);
        				}else{
        					desgWiswOtMap.put(entry.getKey(), overtimeHours);
        				}
        			}else{
        				desgWiswOtMap.put(entry.getKey(), overtimeHours);
        			}
//        			days = getOverTimeHrs(info, fromDate, toDate, nhOtFlag,otRateFlag,entry.getKey()) /info.getLeaveCalendar().getWorkingHours();
        			days = overtimeHours /info.getLeaveCalendar().getWorkingHours();
        			totalDays += days;

        		}
        		if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
        			days=paySlipService.getPublicHolidayDetails(attendanceList,info);
        			logger.log(Level.SEVERE ,parameter+" PH "+days);
					totalDays += days;
        		}
        		logger.log(Level.SEVERE ,parameter+" = " + totalDays+" EMP ID- "+num+" Attendance- "+attendanceList.size());
            }
            paidDaysMap.put(entry.getKey(), totalDays);
		}
		return paidDaysMap;
	}
	public double getOverTimeHrs(EmployeeInfo info, Date startDate , Date endDate ,boolean nhOtFlag, boolean otRateFlag, String attendenceDesignation) 
	{
		List<EmployeeOvertime> otList=new ArrayList<EmployeeOvertime>();
		Calendar c = Calendar.getInstance(); 
		double otHrs=0d;	
		try {

			DateUtility.getDateWithTimeZone("IST", startDate);
			DateUtility.getDateWithTimeZone("IST", endDate);
//			logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
//			logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
			
			Key<EmployeeInfo>employeeKey=Key.create(info);
			otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",info.getCompanyId()).filter("emplId", info.getEmpCount()).filter("otDate >=",startDate).list();
//			logger.log(Level.SEVERE,"Total OT LIST SIZE --- ::: "+otList.size());
//			logger.log(Level.SEVERE,"Total OT LIST SIZE --- ::: "+otList.size());
			
			List<EmployeeOvertime> validotList=new ArrayList<EmployeeOvertime>();
			
			for(EmployeeOvertime temp:otList)
			{
				/**
				 * @author Vijay Date :- 18-12-2020
				 * Des :- as per new logic for OT calculation it will check attendece designation matched then it will calculate those employee OT
				 * proeprly. else block added for old code which having no employee attendence designation in EmployeeOvertime
				 * requirement raised by Rahul Tiwari
				 */
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", AppConstants.PC_ENABLESITELOCATION, info.getCompanyId()) && 
						temp.getEmpDesignation()!=null && !temp.getEmpDesignation().equals("")) {
					if(temp.getEmpDesignation().equals(attendenceDesignation)) {
						if(temp.getOtDate().equals(endDate)||temp.getOtDate().before(endDate)){
							validotList.add(temp);
						}
					}
				}
				else {
					if(temp.getOtDate().equals(endDate)||temp.getOtDate().before(endDate)){
						validotList.add(temp);
					}
				}
					
				
			}
			
//			logger.log(Level.SEVERE,"valid OT LIST SIZE --- ::: "+validotList.size());
//			logger.log(Level.SEVERE,"valid OT LIST SIZE --- ::: "+validotList.size());
			
			for(EmployeeOvertime temp:validotList)
			{
				/**
				 * Date : 30-08- 2018 By ANIL
				 */
			//	if(nhOtFlag){
					boolean isHoliday = info.getLeaveCalendar().isHolidayDate(temp.getOtDate());
					if(isHoliday){
						for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
							if(holiday.getHolidaydate().equals(temp.getOtDate())
									&&holiday.getHolidayType().equalsIgnoreCase("National Holiday")){
								temp.setOtHrs(0);
							}
							/**
							 * @author Anil
							 * @since 21-10-2020
							 */
							if(otRateFlag&&holiday.getHolidaydate().equals(temp.getOtDate())
									&&holiday.getHolidayType().equalsIgnoreCase("Public Holiday")){
								temp.setOtHrs(0);
							}
						}
					}
					
					if(otRateFlag&&temp.getHolidayLabel()!=null&&temp.getHolidayLabel().equals("PH")){
						temp.setOtHrs(0);
					}
			//	}
				/**
				 * End
				 */

				otHrs=otHrs+temp.getOtHrs();
			}
		 }
		 catch(Exception e)
		 {
		 }

		return otHrs;
		
	}
	/** date 22.1.2019 added by komal to validate attendance***/
	private String validateAttendance(Date fromDate ,Date toDate , CNC cnc){
		ProjectAllocation projectAllocation = ofy().load().type(ProjectAllocation.class).filter("companyId", cnc.getCompanyId()).filter("contractId",cnc.getCount()).first().now();
		ArrayList<Integer> empIdList= new ArrayList<Integer>();
		HashMap<String ,ArrayList<Attendance>> attendanceMap = new HashMap<String ,ArrayList<Attendance>>();
		
		
		/**
		 * @author Vijay @Since 21-09-2022
		 * Loading employee project allocation from different entity
		 */
		   CommonServiceImpl commonservice = new CommonServiceImpl();
		   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
	    /**
	     * ends here
	     */
//		if(projectAllocation.getEmployeeProjectAllocationList().size() >0){
		if(empprojectallocationlist.size() >0){
		
			HashMap<String , ArrayList<Integer>> employeeMap =  new HashMap<String , ArrayList<Integer>>();
			logger.log(Level.SEVERE , "From date :" + fromDate);
			logger.log(Level.SEVERE , "To date :" + toDate);
			long diff= toDate.getTime() - fromDate.getTime();			
	        int noofdays= ((int) (diff / (24 * 60 * 60 * 1000))) + 1;
	        logger.log(Level.SEVERE," :::=No of days  = :::  "+noofdays);
	        HashSet<Integer> idSet = new HashSet<Integer>();
//			for(EmployeeProjectAllocation pr : projectAllocation.getEmployeeProjectAllocationList()){
			for(EmployeeProjectAllocation pr : empprojectallocationlist){
				if(pr.getEmployeeId() != null && !(pr.getEmployeeId().equals(""))){
					String key = "";
					if(pr.isExtra())
						 key = "Additional " + pr.getEmployeeRole();
					else
						key = pr.getEmployeeRole();
					int count = Integer.parseInt(pr.getEmployeeId());
					idSet.add(count);
					if(employeeMap.containsKey(key)){
						empIdList = employeeMap.get(key);
						empIdList.add(count);
						employeeMap.put(key, empIdList);
					}else{
						empIdList = new ArrayList<Integer>();
						empIdList.add(count);
						employeeMap.put(key, empIdList);
					}
					
				}
			}
			if(employeeMap.size()>0){
				attendanceMap = loadAttandace(fromDate , toDate ,employeeMap , cnc.getCompanyId(),projectAllocation.getProjectName(),null);
			}
		
			int count = 0;
			for (Map.Entry<String ,ArrayList<Attendance>> entry : attendanceMap.entrySet())  {
	            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue()); 
				count += entry.getValue().size();
			}
			if(count == 0){
				return "First mark attendance of all employess in project.";
		  }
		}
			return "success";
		
	}

	@Override
	public String validateProjectAttendanceAndBilling(CNC cnc) {
		// TODO Auto-generated method stub
		List<Attendance> projectAttendanceList = ofy().load().type(Attendance.class).filter("companyId", cnc.getCompanyId()).filter("projectName", cnc.getProjectName()).list();
		if(projectAttendanceList != null && projectAttendanceList.size() > 0){
			return "CNC can not be cancelled as Attendance is marked against this project.";
		}
		List<BillingDocument> billingList = ofy().load().type(BillingDocument.class).filter("companyId", cnc.getCompanyId()).filter("contractCount", cnc.getCount()).filter("typeOfOrder", AppConstants.ORDERTYPEFORSERVICE).list();
		int count = 0;
		for(BillingDocument billing : billingList){
	    	if(!billing.getStatus().equalsIgnoreCase(BillingDocument.CANCELLED)){
	    		count++;
	    	}
	    }
		if(count > 0){
			return "First cancel billing document against this CNC.";
		}
		return "success";
	}

	/** date 23.1.2019 added by komal
	 *  Description : to cancel cnc and to inactive cnc and hr project
	 */
	@Override
	public String cancelCNC(CNC cnc , String remark) {
		// TODO Auto-generated method stub
		CancellationSummary table1;
		Approvals approve;
			table1 = new CancellationSummary();
			table1.setDocID(cnc.getCount());
			table1.setDocType(AppConstants.CNC);
			table1.setRemark(remark);
			table1.setLoggedInUser(cnc.getCreatedBy());
			table1.setOldStatus(cnc.getStatus());
			table1.setNewStatus(Contract.CANCELLED);
			table1.setSrNo(1);

				cnc.setStatus(Contract.CANCELLED);
				if (cnc.getDescription() != null) {
					cnc.setDescription(cnc.getDescription() + "\n"
							+ "Contract Id =" + cnc.getCount() + " "
							+ "Contract Status = Approved" + "\n"
							+ "has been cancelled by " + cnc.getCreatedBy()
							+ " with remark" + "\n" + "Remark =" + remark);
				} else {
					cnc.setDescription("Contract Id =" + cnc.getCount() + " "
							+ "Contract Status = Approved" + "\n"
							+ "has been cancelled by " + cnc.getCreatedBy()
							+ " with remark" + "\n" + "Remark =" + remark);
				}

				ofy().save().entity(cnc).now();
				approve = ofy().load().type(Approvals.class)
						.filter("companyId", cnc.getCompanyId())
						.filter("businessprocessId", cnc.getCount()).first()
						.now();
				if (approve != null) {
					approve.setStatus(Approvals.CANCELLED);
					approve.setRemark("Order Id =" + cnc.getCount() + " "
							+ "has been cancelled by " + cnc.getCreatedBy()
							+ " with remark " + "\n" + "Remark =" + remark);
					ofy().save().entity(approve).now();
				}

		ProjectAllocation allocation = ofy().load().type(ProjectAllocation.class).filter("companyId", cnc.getCompanyId()).filter("contractId", cnc.getCount()).first().now();
		if(allocation != null){
			allocation.setProjectStatus(false);
			ofy().save().entity(allocation);
			HrProject hrProject = ofy().load().type(HrProject.class).filter("companyId", cnc.getCompanyId()).filter("cncProjectCount", allocation.getCount()).first().now();
			if(hrProject != null){
				hrProject.setStatus(false);
				ofy().save().entity(hrProject);
			}
		}
			
				
		return "CNC cancelled successfully.";
	}
	public Overtime Myclone(Overtime overtime) {
		// TODO Auto-generated method stub
		Overtime temp=new Overtime();
		temp.setId(overtime.getId());
		temp.setCompanyId(overtime.getCompanyId());
		temp.setStatus(overtime.isStatus());
		temp.setCreatedBy(overtime.getCreatedBy());
		temp.setCount(overtime.getCount());
		temp.setCreatedBy(overtime.getCreatedBy());
		temp.setShortName(overtime.getShortName());
		temp.setName(overtime.getName());
		temp.setHourly(overtime.isHourly());
		temp.setHourlyRate(overtime.getHourlyRate());
		temp.setFlatRate(overtime.getFlatRate());
		temp.setPercentOfCTC(overtime.getPercentOfCTC());
		temp.setLeave(overtime.isLeave());
		temp.setLeaveMultiplier(overtime.getLeaveMultiplier());
		temp.setHolidayType(overtime.getHolidayType());
		temp.setWeeklyOff(overtime.isWeeklyOff());
		temp.setNormalDays(overtime.isNormalDays());
		temp.setEmpId(overtime.getEmpId());
		temp.setEmpName(overtime.getEmpName());
		temp.setEmpDesignation(overtime.getEmpDesignation());
		temp.setShift(overtime.getShift());
		temp.setStartDate(overtime.getStartDate());
		temp.setEndDate(overtime.getEndDate());
		return temp;
	}
	private double calculateTotal(SalesLineItem object){
		ServerAppUtility serverAppUtility = new ServerAppUtility();
		double total=0;
		SuperProduct product=object.getPrduct();
		double tax=serverAppUtility.removeAllTaxes(product);
		double origPrice=object.getPrice()-tax;
		
		
//		if(object.getPercentageDiscount()==null){
//			total=origPrice*object.getQty();
//		}
//		if(object.getPercentageDiscount()!=null){
//			total=origPrice-(origPrice*object.getPercentageDiscount()/100);
//			total=total*object.getQty();
////			total=Math.round(total);
//		}
		
		
		/** Date 05-08-2017
		 *  added by vijay add code for area calculation
		 */
		
		if(object.getArea().equals("") || object.getArea().equals("0")){
			object.setArea("NA");
		}
		System.out.println("area == "+object.getArea());
		//****************new code for discount by rohan ******************
		if((object.getPercentageDiscount()==null && object.getPercentageDiscount()==0) && (object.getDiscountAmt()==0) ){
			
			System.out.println("inside both 0 condition");
			/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
			System.out.println("Get value from area =="+ object.getArea());
			System.out.println("total amount before area calculation =="+total);
			if(!object.getArea().equalsIgnoreCase("NA") ){
			double area = Double.parseDouble(object.getArea());
			 total = origPrice*area;
			System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+total);
			}else{
				total=origPrice*object.getQty();
			}
		}
		
		else if((object.getPercentageDiscount()!=null)&& (object.getDiscountAmt()!=0)){
			
			System.out.println("inside both not null condition");
			/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
			System.out.println("total amount before area calculation =="+total);
			if(!object.getArea().equalsIgnoreCase("NA")){
				double area = Double.parseDouble(object.getArea());
				total=origPrice*area;
				System.out.println("total before discount per ===="+total);
				total = total - (total*object.getPercentageDiscount()/100);
				System.out.println("after discount per total === "+total);
				total = total - object.getDiscountAmt();
				System.out.println("after discount AMT total === "+total);

			System.out.println(" Final TOTAL   discount per &  discount Amt ==="+total);
			}else{
				
				total=origPrice*object.getQty();
				total=total-(total*object.getPercentageDiscount()/100);
				total=total-object.getDiscountAmt();
			}
			
		}
		else 
		{
			System.out.println("inside oneof the null condition");
			
				if(object.getPercentageDiscount()!=null){
					System.out.println("inside getPercentageDiscount oneof the null condition");
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
					if(!object.getArea().equalsIgnoreCase("NA")){
						double area = Double.parseDouble(object.getArea());
						total=origPrice*area;
						total = total - (total*object.getPercentageDiscount()/100);
						
					}else{
						total=origPrice*object.getQty();
						total=total-(total*object.getPercentageDiscount()/100);
					}
				}
				else 
				{
					System.out.println("inside getDiscountAmt oneof the null condition");
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block old code ***************************/
					if(!object.getArea().equalsIgnoreCase("NA")){
						double area = Double.parseDouble(object.getArea());
						total=origPrice*area;
						total = total - object.getDiscountAmt();
						
					}else{
						total=origPrice*object.getQty();
						total=total-object.getDiscountAmt();
					}
				}
				
//				total=total*object.getQty();
				
		}
		
		
		return total;
	
	}
	public String generateArrearsBill(Date fromDate,Date toDate,CNC cnc,ArrayList<CNCArrearsDetails> list){
		String response="";//contractId
		if(cnc!=null&&cnc.getStatus().equals(CNC.CONTRACTCONFIRMED)){
			cnc.setCncArrearsList(list);
			ofy().save().entity(cnc);
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			String str = cnc.getCount()+"";
			String date1 = dateFormat.format(fromDate);
			String date2 = dateFormat.format(toDate);
			String parameter = AppConstants.ARREARSBILL;
			String message = validateAttendance(fromDate , toDate , cnc);
			if(!message.equalsIgnoreCase("success")){
					return message;
			}
	        Queue queue = QueueFactory.getQueue("documentCancellation-queue");
	        String typeducumentName = "CreateCNCBill"+"$"+cnc.getCompanyId()+"$"+str+"$"+date1+"$"+date2+"$"+parameter;
	   	  	logger.log(Level.SEVERE, "Company Id "	+ typeducumentName);
	   	  	queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", typeducumentName));			
	   	 	response="Billing created successfully.";
			
		}else{
			response="First mark confirmed CNC.";
		}
		return response;
	}
	
	public SalesOrderProductLineItem getArrearsPrice(Date fromDate , Date toDate ,SalesOrderProductLineItem salesLineItem,CNC cnc,StaffingDetails staff,double totalPayment, int noofdays, HashMap<String , ArrayList<Integer>> employeeMap, double managementFees, List<EmployeeInfo> empInfoList){
		DateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat mmmFormat = new SimpleDateFormat("MMM-yyyy");
		mmmFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		double  grandTotal = 0 , grandExcludingMgntFeesTotal =0 , grandManagementFees = 0;
		String annexureString = "";
		int totalNoOfDays = 0;
		double arrears = 0;
		
		logger.log(Level.SEVERE , "managementFees :"+ managementFees);
		/**
		 * @author Anil
		 * @since 17-11-2020 Capturing CTC AMout for arrears bill
		 */
		double ctcAmount = 0;
		
		if(cnc.getCncArrearsList() != null && cnc.getCncArrearsList().size() > 0){
			for(CNCArrearsDetails details : cnc.getCncArrearsList()){
				CNCVersion cncVersion = ofy().load().type(CNCVersion.class).filter("companyId", cnc.getCompanyId())
						.filter("cnc.count", cnc.getCount()).filter("version", details.getVesion()).first().now();
				logger.log(Level.SEVERE , "CNC VERSION :"+ cncVersion);
			
				if (cncVersion != null) {
					Calendar c = Calendar.getInstance();
					c.setTime(details.getFromDate());
					c.set(Calendar.HOUR_OF_DAY, 00);
					c.set(Calendar.DAY_OF_MONTH, 1);
					c.set(Calendar.MINUTE, 00);
					c.set(Calendar.SECOND, 00);
					c.set(Calendar.MILLISECOND, 000);
					logger.log(Level.SEVERE, "From date :" + c.getTime());

					Calendar cal = Calendar.getInstance();
					cal.setTime(details.getToDate());
					cal.set(Calendar.HOUR_OF_DAY, 23);
					cal.set(Calendar.MINUTE, 59);
					cal.set(Calendar.SECOND, 59);
					cal.set(Calendar.MILLISECOND, 999);
					logger.log(Level.SEVERE, "To date :" + cal.getTime());
					
					logger.log(Level.SEVERE, "1 :" + staff.getDesignation()+" "+cncVersion.getCnc().getSaffingDetailsList().size());
					
					for(StaffingDetails st : cncVersion.getCnc().getSaffingDetailsList()){
                    	logger.log(Level.SEVERE, "2 :" + st.getDesignation());
                    	double totalPay = 0 , mgntFees = 0 , total = 0 ;
                    	int i = 0;
                    	while (c.getTime().before(cal.getTime()) && st.getDesignation().equalsIgnoreCase(staff.getDesignation())) {
						int days = c.getActualMaximum(Calendar.DAY_OF_MONTH);
						totalNoOfDays += days;
						Calendar calender = Calendar.getInstance();
						if(i == 0){
							calender.setTime(details.getFromDate());
							calender.set(Calendar.HOUR_OF_DAY, 00);
							calender.set(Calendar.MINUTE, 00);
							calender.set(Calendar.SECOND, 00);
							calender.set(Calendar.MILLISECOND, 000);
						}else{
							calender.setTime(c.getTime());
							
						}
						
						try{
							mgntFees = Double.parseDouble(st.getManagementFees()) / st.getNoOfStaff();
						}catch(Exception e){
							mgntFees = 0;
						}
					  
						logger.log(Level.SEVERE , "mgntFees :"+ mgntFees);
					  
						try{
							total = Double.parseDouble(st.getTotalOfPaidAndOverhead());
						}catch(Exception e){
							total = 0;
						}
						totalPay = total + mgntFees;
						
						Calendar calender1 = Calendar.getInstance();
						if(c.getTime().after(cal.getTime())){
							calender1.setTime(c.getTime());
						}else{
							calender1.setTime(c.getTime());
							calender1.set(Calendar.DAY_OF_MONTH, days);
						}
						calender1.set(Calendar.HOUR_OF_DAY, 00);					 
						calender1.set(Calendar.MINUTE, 00);
						calender1.set(Calendar.SECOND, 00);
						calender1.set(Calendar.MILLISECOND, 000);
					  
						long diff = calender1.getTimeInMillis() - calender.getTimeInMillis();
						long diffDays = (int)(diff / (24 * 60 * 60 * 1000)) + 1;
					  
						arrears = totalPayment - totalPay; 
						logger.log(Level.SEVERE , "Arrears :" + arrears);
						double perDay =  Math.round((arrears / days)*100.0)/100.0;
						logger.log(Level.SEVERE , "Perday rate :" + perDay);
						grandTotal += (Math.round((diffDays * perDay)*100.0)/100.0);
						
						grandExcludingMgntFeesTotal += (diffDays * (((totalPayment - managementFees) - total)/days));
						grandManagementFees += (diffDays * ((managementFees - mgntFees)/days));
						logger.log(Level.SEVERE , "grand total :" + grandTotal);
						logger.log(Level.SEVERE , "grand total management fees exclude :" + grandExcludingMgntFeesTotal);
						logger.log(Level.SEVERE , "grand management fees :" + grandManagementFees);
					 		ctcAmount=ctcAmount+arrears;	
					 		logger.log(Level.SEVERE , "ctcAmount management fees :" + ctcAmount);
							if(employeeMap.size()>0){
								HashMap<String ,ArrayList<Attendance>> attendanceMap = loadAttandace(calender.getTime() , calender1.getTime() ,employeeMap , cnc.getCompanyId(),cnc.getProjectName(),null);
								HashMap<String , Double> paidDaysMap = new HashMap<String , Double>();
								paidDaysMap = loadPaidDaysOfEmployee(calender.getTime() , calender1.getTime() ,attendanceMap , noofdays , empInfoList , true , false);
								
								ArrayList<CNCBillAnnexureBean> beanList = new ArrayList<CNCBillAnnexureBean>();
								CNCBillAnnexureBean bean = new CNCBillAnnexureBean();
								bean.setProductName(st.getDesignation());
								bean.setNoOfStaff(st.getNoOfStaff());
								bean.setMonth(mmmFormat.format(calender.getTime()));
								bean.setOldCTC(totalPayment);
								bean.setNewCTC(totalPay);
								bean.setArrearsCTC(arrears);
								bean.setManDays(paidDaysMap.get(st.getDesignation()));
								bean.setPerDayRate(perDay);
								bean.setTotal(bean.getPerDayRate() * bean.getManDays());
								bean.setTaxableAmount(bean.getPerDayRate() * bean.getManDays());
								CNCBillAnnexure annexure = new CNCBillAnnexure();
								annexure = ofy().load().type(CNCBillAnnexure.class).filter("companyId", cnc.getCompanyId())
										.filter("month", mmmFormat.format(calender.getTime()))
										.filter("cncId", cnc.getCount()).filter("billingId", 0).first().now();
								if(annexure == null){
									annexure = new CNCBillAnnexure();
									annexure.setCompanyId(cnc.getCompanyId());
									annexure.setCncId(cnc.getCount());
									annexure.setMonth(mmmFormat.format(calender.getTime()));						
									beanList.add(bean);
									annexure.setCNCBillAnnexureList(beanList);
									ReturnFromServer server = genricServiceImpl.save(annexure);
									annexureString += server.count+"/";
									
								}else{
									beanList = annexure.getCNCBillAnnexureList();
									beanList.add(bean);
									annexure.setCNCBillAnnexureList(beanList);
									ofy().save().entity(annexure);
								}
								
							}
					  i++;
					 Date d = DateUtility.addDaysToDate(c.getTime() ,32);
						c.setTime(d);
						c.set(Calendar.HOUR_OF_DAY, 00);
						c.set(Calendar.DAY_OF_MONTH, 1);
						c.set(Calendar.MINUTE, 00);
						c.set(Calendar.SECOND, 00);
						c.set(Calendar.MILLISECOND, 000);  
					logger.log(Level.SEVERE, "Total pay" + totalPay);
				}
				
			 }
			}
		}
		}
		
		salesLineItem.setPrice(Math.round((grandTotal / totalNoOfDays)*100.0)/100.0);
		salesLineItem.setProdAvailableQuantity(arrears);
		salesLineItem.setWarrantyPeriod(staff.getNoOfStaff());
		salesLineItem.setManagementFees(grandManagementFees);
		salesLineItem.setTotalExcludingManagementFees(grandExcludingMgntFeesTotal);
		if(!annexureString.equals("")){
			salesLineItem.setProSerialNo(annexureString.substring(0, annexureString.length()-1));
		}
		ctcAmount=ctcAmount-grandManagementFees;
		logger.log(Level.SEVERE, "ctcAmount .. " + ctcAmount);
		salesLineItem.setCtcAmount(ctcAmount);
		
		logger.log(Level.SEVERE, "Price" + grandTotal);
		
		return salesLineItem;
	}
	
	private int getNoofEmployeeOfattedanace(String designation, HashMap<String, ArrayList<Attendance>> attendanceMap, Long companyId, List<EmployeeInfo> empInfoList) {
		// TODO Auto-generated method stub
		
		Set set = new HashSet();
		for (Map.Entry<String,ArrayList<Attendance>> entry : attendanceMap.entrySet())  {
//            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
            logger.log(Level.SEVERE ,"designation "+designation);
            if(entry.getKey().equals(designation)) {
            	 for(Attendance att : entry.getValue()){
                 	set.add(att.getEmpId());
                 }
            }
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.BILLINGDOCUMENT, AppConstants.PC_ENABLESITELOCATION, companyId)){
	  		if(set.size()!=0){
	  			if(empInfoList.size()!=0){
	  				ArrayList<Integer> emplist = new ArrayList<Integer>(set);
					List<Employee> employeelist = ofy().load().type(Employee.class).filter("companyId", companyId).filter("count IN", emplist).list();
					if(employeelist.size()!=0){
						for(Employee emp : employeelist){
							if(emp.isReliever()){
			                 	set.remove(emp.getCount());
							}
						}
					}
	  			}
	  			else{
	  				for(EmployeeInfo empinfo : empInfoList){
	  					if(empinfo.isReliever()){
		                 	set.remove(empinfo.getCount());
						}
	  				}
	  			}
				
	  		}
	  	}
		
		return set.size();
	}
	
	private String getBillType(String parameter) {
		 
		  if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)){
			  return "Holiday";
		  }
		  else if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
			  return AppConstants.OVERTIME;
		  }
		  else if(parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL) || parameter.equalsIgnoreCase(AppConstants.STAFFING)){
			  return "Labour";
		  }
		  else if(parameter.equalsIgnoreCase(AppConstants.ARREARSBILL)){
			  return "Arrears Bill";
		  }else if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
			  return "Public Holiday";
		  }
		  else{
			  return "";
		  }
	}
	
	
	private List<Integer> getNewEmplIdfromAttendencelist(List<Attendance> attendanceList, List<Integer> idList) {
		logger.log(Level.SEVERE, "idList" + idList);

		HashSet<Integer> hsempid = new HashSet<Integer>();
		for(Attendance attendence : attendanceList) {
			if(idList.contains(attendence.getEmpId())) {
			}
			else {
				hsempid.add(attendence.getEmpId());
			}
		}
		logger.log(Level.SEVERE, "EMP hsempid size" + hsempid.size());
		List<Integer> list = new ArrayList<Integer>(hsempid);
		return list;
	}
	
	private List<Attendance> getreliverattendencelist(Long companyId, String projectName, Date fromdate, Date toDate, List<Integer> idList, String employeeDesignatiom, List<EmployeeInfo> empInfoList) {
		// TODO Auto-generated method stub
		
		List<Attendance> attendanceList=ofy().load().type(Attendance.class).filter("companyId",companyId).filter("projectName", projectName).filter("attendanceDate >=", fromdate).filter("attendanceDate <=", toDate).list();
 		logger.log(Level.SEVERE , "attendanceList " +attendanceList.size());

         List<Integer> empidlist = getNewEmplIdfromAttendencelist(attendanceList,idList);
 		logger.log(Level.SEVERE , "other than project allocation empidlist " +empidlist.size());
 		logger.log(Level.SEVERE , "other than project allocation empidlist " +empidlist);
 		HashSet<Integer> hsempid = new HashSet<Integer>();

 		if(empidlist.size()>0) {
         	List<EmployeeInfo> employeelist = ofy().load().type(EmployeeInfo.class).filter("count IN", empidlist).filter("companyId", companyId).list();
     		logger.log(Level.SEVERE , "employeelist " +employeelist.size());
         	if(employeelist.size()!=0) {
         		for(EmployeeInfo empinfo : employeelist) {
         			if(empinfo.isReliever()) {
         				hsempid.add(empinfo.getEmpCount());
         				
         				if(empInfoList!=null) {
             				empInfoList.add(empinfo);
                     		logger.log(Level.SEVERE , "Reliver empInfoList == " +empInfoList.size());
             				}
         				}
         			}
         	}
         }
         
 		ArrayList<Integer> reliverempid = new ArrayList<Integer>(hsempid);
 		List<Attendance> reliverattendanceList = new ArrayList<Attendance>();
 		if(reliverempid.size()>0) {
     	reliverattendanceList=ofy().load().type(Attendance.class).filter("companyId",companyId).filter("projectName", projectName)
     			.filter("empId IN", reliverempid).filter("empDesignation", employeeDesignatiom).filter("attendanceDate >=", fromdate).filter("attendanceDate <=", toDate).list();
 		logger.log(Level.SEVERE , "updated reliver attendanceList === " +reliverattendanceList.size());
 		}
		return reliverattendanceList;
	}
	
	private String getAttendenceWiseDesignationMap(List<Attendance> attendanceList,
			String empDesignation, HashMap<String, ArrayList<Attendance>> attendanceMap, List<Integer> idList) {
		
			for(Attendance attendence : attendanceList) {
				
				if(attendanceMap.containsKey(attendence.getEmpDesignation())) {
					ArrayList<Attendance> list = attendanceMap.get(attendence.getEmpDesignation());
			 		boolean duplicateflag = false;
			 		for(Attendance attendenceEntity : list) {
			 			if(attendence.getCount()==attendenceEntity.getCount()) {
			 				duplicateflag = true;
			 				break;
			 			}
			 		}
			 		if(!duplicateflag) {
			 			list.add(attendence);
						attendanceMap.put(attendence.getEmpDesignation(), list);
			 		}
				}
				else {
					ArrayList<Attendance> list = new ArrayList<Attendance>();
					list.add(attendence);
					attendanceMap.put(attendence.getEmpDesignation(), list);
				}
			}
		

			return "";
	}
	
	private HashMap<String , Integer> loadNoOFStaff(Date fromDate , Date toDate ,HashMap<String ,ArrayList<Attendance>> attendanceMap , List<EmployeeInfo> emInfoList , String parameter ,boolean nhOtFlag, boolean otRateFlag){
		HashMap<String , Integer> staffMap = new HashMap<String , Integer>();
		ArrayList<Attendance> attendanceList = new ArrayList<Attendance>();
		HashSet<Integer> set = new HashSet<Integer>();
		
		
		for (Map.Entry<String,ArrayList<Attendance>> entry : attendanceMap.entrySet())  {
//            logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
			 logger.log(Level.SEVERE ,"Key = " + entry.getKey() );
            double totalDays = 0;
            set = new HashSet();
            for(Attendance att : entry.getValue()){
            	set.add(att.getEmpId());
            }
            double days = 0;
            int empcount=0;
            for(Integer num : set){
            	attendanceList = getEmployeeAttendance(entry.getValue() , num);
            	DocumentUploadTaskQueue	taskQueue = new DocumentUploadTaskQueue();
        		EmployeeInfo info = taskQueue.getEmployeeInfo(num, emInfoList);
        		days = 0;
        		if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)){
        			days=paySlipService.getNationalHolidayDetails(attendanceList,info);
        			
        		}if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){

        			days = getOverTimeHrs(info, fromDate, toDate, nhOtFlag,otRateFlag,entry.getKey()) /info.getLeaveCalendar().getWorkingHours();

        		}
        		if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
        			days=paySlipService.getPublicHolidayDetails(attendanceList,info);
        		}
        		logger.log(Level.SEVERE ,parameter+" = " + totalDays+" EMP ID- "+num+" Attendance- "+attendanceList.size());
        		if(days>0) {
        			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.BILLINGDOCUMENT, AppConstants.PC_ENABLESITELOCATION, info.getCompanyId())){
				  		if(!info.isReliever()){
				  			empcount++;
				  		}
				  	}
        			else{
            			empcount++;
        			}
        		}
            }
            staffMap.put(entry.getKey(), empcount);
		}
		return staffMap;
	}

	@Override
	public HashMap<String, ArrayList<AnnexureType>> getAnnexureDetailsForCNCBill(long companyId, Invoice invoice) {
		PdfUtility pdfUtility=new PdfUtility();
		
		boolean normalDayOtFlag=false;
		boolean publicHolidayOtFlag=false;
		boolean nationalHolidayOtFlag=false;
		boolean staffingFlag=false;
		
		HashMap<String,ArrayList<AnnexureType>> annexureMap=new HashMap<String, ArrayList<AnnexureType>>();
		
		if(invoice!=null){
			
			if(invoice.getSubBillType()!=null&&!invoice.getSubBillType().equals("")){
				if(invoice.getSubBillType().equals("Public Holiday")
						||invoice.getSubBillType().equals("National Holiday")
						||invoice.getSubBillType().equals("Overtime")
						||invoice.getSubBillType().equals("Holiday")
						||invoice.getSubBillType().equals("Staffing")
						||invoice.getSubBillType().equals("Staffing And Rental")
						||invoice.getSubBillType().equals("Labour")){
					
//					otTypeList.add(invoice.getSubBillType());
					
					if(invoice.getSubBillType().equals("Staffing")
							||invoice.getSubBillType().equals("Staffing And Rental")
							||invoice.getSubBillType().equals("Labour")){
						staffingFlag=true;
					}
					if(invoice.getSubBillType().equals("Overtime")){
						normalDayOtFlag=true;
					}
					if(invoice.getSubBillType().equals("National Holiday")||invoice.getSubBillType().equals("Holiday")){
						nationalHolidayOtFlag=true;
					}
					if(invoice.getSubBillType().equals("Public Holiday")){
						publicHolidayOtFlag=true;
					}
				}else if(invoice.getSubBillType().equals("Consolidate Bill")){
										
					for(SalesOrderProductLineItem product : invoice.getSalesOrderProducts()){
						logger.log(Level.SEVERE, "product.getBillType() "+product.getBillType());
						if(product.getBillType()!=null && !product.getBillType().equals("")){
							if(product.getBillType().equals("Overtime")
									||product.getBillType().equals("Holiday")
									||product.getBillType().equals("National Holiday")
									||product.getBillType().equals("Public Holiday")
									||product.getBillType().equals("Staffing")
									||product.getBillType().equals("Staffing And Rental")
									||product.getBillType().equals("Labour")){
								
//								otTypeList.add(product.getBillType());
								
								if(product.getBillType().equals("Staffing")
										||product.getBillType().equals("Staffing And Rental")
										||product.getBillType().equals("Labour")){
									staffingFlag=true;
								}
								
								if(product.getBillType().equals("Overtime")){
									normalDayOtFlag=true;
								}
								if(product.getBillType().equals("National Holiday")||product.getBillType().equals("Holiday")){
									nationalHolidayOtFlag=true;
								}
								
								if(product.getBillType().equals("Public Holiday")){
									publicHolidayOtFlag=true;
								}
								
							}
						}
					}
				}
			}
		}
		
		HashMap<String , ArrayList<Integer>> employeeMap =  new HashMap<String , ArrayList<Integer>>();
		
		CNC cnc = ofy().load().type(CNC.class).filter("companyId", invoice.getCompanyId()).filter("count", invoice.getContractCount()).first().now();
		ProjectAllocation projectAllocation = ofy().load().type(ProjectAllocation.class).filter("companyId", cnc.getCompanyId()).filter("contractId",cnc.getCount()).first().now();
		
		Date fromDate=invoice.getBillingPeroidFromDate();
		Date toDate=invoice.getBillingPeroidToDate();
		
		DateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		Calendar c = Calendar.getInstance();
		c.setTime(fromDate);
		c.set(Calendar.HOUR_OF_DAY,00);
		c.set(Calendar.MINUTE,00);
		c.set(Calendar.SECOND,00);
		c.set(Calendar.MILLISECOND,000);
		fromDate=c.getTime();
		
		logger.log(Level.SEVERE , "From date :" + fromDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(toDate);
		cal.set(Calendar.HOUR_OF_DAY,23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		cal.set(Calendar.MILLISECOND,999);
		toDate=cal.getTime();
		logger.log(Level.SEVERE , "To date :" + toDate);
		
		ArrayList<Integer> empIdList= new ArrayList<Integer>();
		HashMap<String ,ArrayList<Attendance>> attendanceMap = new HashMap<String ,ArrayList<Attendance>>();
//		CncBillServiceImpl serviceImpl = new CncBillServiceImpl();
		
		/**
		 * @author Vijay @Since 21-09-2022
		 * Loading employee project allocation from different entity
		 */
		   CommonServiceImpl commonservice = new CommonServiceImpl();
		   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
	    /**
	     * ends here
	     */
//		if(projectAllocation.getEmployeeProjectAllocationList().size() >0){
		if(empprojectallocationlist.size() >0){
			logger.log(Level.SEVERE , "From date :" + fromDate);
			logger.log(Level.SEVERE , "To date :" + toDate);
			
	        HashSet<Integer> idSet = new HashSet<Integer>();
			for(EmployeeProjectAllocation pr : empprojectallocationlist){
				if(pr.getEmployeeId() != null && !(pr.getEmployeeId().equals(""))){
					String key = "";
					if(pr.isExtra()){
						 key = "Additional " + pr.getEmployeeRole();
					}else{
						key = pr.getEmployeeRole();
					}
					int count = Integer.parseInt(pr.getEmployeeId());
					idSet.add(count);
					if(employeeMap.containsKey(key)){
						empIdList = employeeMap.get(key);
						empIdList.add(count);
						employeeMap.put(key, empIdList);
					}else{
						empIdList = new ArrayList<Integer>();
						empIdList.add(count);
						employeeMap.put(key, empIdList);
					}
					
				}
			}
			if(employeeMap.size()>0){
				attendanceMap = loadAttandace(fromDate , toDate ,employeeMap , cnc.getCompanyId(),projectAllocation.getProjectName(),null);
			}
			
			int i=0;
			int sumOfNoOfStaff = 0;
			double sumOfRate = 0;
			double sumOfNormalOt = 0;
			double sumOfNormalNHOT = 0;
			double sumOfNormalPHOT = 0;
			double sumOfNormalTotalOtAmt = 0;
			double sumOfNormalRateAndOtAMt = 0;
			
			double sumSgst = 0;
			double sumCgst = 0;
			double sumIgst = 0;
			
			double sumNetTotalAmt = 0;
			double sumOfNumPresent=0;
			
//			HashMap<String,ArrayList<AnnexureType>> annexureMap=new HashMap<String, ArrayList<AnnexureType>>();
			
			for(SalesOrderProductLineItem product : invoice.getSalesOrderProducts()){
				if(product.getBillType()!=null && !product.getBillType().equals("")){
					if(product.getBillType().equals("Overtime")
							||product.getBillType().equals("Holiday")
							||product.getBillType().equals("National Holiday")
							||product.getBillType().equals("Public Holiday")
							||product.getBillType().equals("Staffing")
							||product.getBillType().equals("Staffing And Rental")
							||product.getBillType().equals("Labour")){
						
//						
						
						String siteLoc=" ";
						double noOfPresentDay=0;
						ArrayList<Attendance> attendanceList=new ArrayList<Attendance>();
						if(attendanceMap!=null&&attendanceMap.size()!=0){
							if(attendanceMap.containsKey(product.getProdName())){
								attendanceList=attendanceMap.get(product.getProdName());
							}
						}
						
						logger.log(Level.SEVERE ,"$$$$$$$$$$$$$$$$$$"+product.getBillType()+ " attendanceList " + attendanceList.size()+" DESG -- "+product.getProdName());
						
						HashMap<String,ArrayList<Attendance>> siteAndDesgWiseAttMap=new HashMap<String, ArrayList<Attendance>>();
						
						for(Attendance attendance:attendanceList){
							String key=attendance.getEmpDesignation()+attendance.getSiteLocation();
							if(siteAndDesgWiseAttMap.size()==0){
								ArrayList<Attendance>list=new ArrayList<Attendance>();
								list.add(attendance);
								siteAndDesgWiseAttMap.put(key, list);
							}else{
								if(siteAndDesgWiseAttMap.containsKey(key)){
									siteAndDesgWiseAttMap.get(key).add(attendance);
								}else{
									ArrayList<Attendance>list=new ArrayList<Attendance>();
									list.add(attendance);
									siteAndDesgWiseAttMap.put(key, list);
								}
							}
						}
						
						logger.log(Level.SEVERE ,product.getBillType()+ "################ siteAndDesgWiseAttMap ########### " + siteAndDesgWiseAttMap.size());
						
						for (Map.Entry<String,ArrayList<Attendance>> entry : siteAndDesgWiseAttMap.entrySet()){
							noOfStaff=0;
							logger.log(Level.SEVERE ,product.getBillType()+ "=============== DESG/LOC ATTEN ============= "+entry.getValue().size());
							
							ArrayList<Attendance>attenLIst=entry.getValue();
							siteLoc=attenLIst.get(0).getSiteLocation();
							logger.log(Level.SEVERE ,product.getBillType()+ " siteLoc:: " + siteLoc+"  ||  "+" desg:: "+product.getProdName());
							noOfPresentDay=getNoOfPresentDaysOnSite(attenLIst,fromDate,toDate,product.getBillType(),siteLoc);
							
							logger.log(Level.SEVERE ,product.getBillType()+ " noOfPresentDay " + noOfPresentDay+" noOfStaff "+noOfStaff);
							
							if(siteLoc==null){
								siteLoc=" ";
							}
							
							
							String key=siteLoc+product.getProdName()+pdfUtility.getRoundOffValue(product.getCtcAmount());
							
							AnnexureType type=new AnnexureType();
							type.setLocationName(siteLoc);
							type.setDesignation(product.getProdName());
//							type.setNoOfStaff(product.getManpower());
							type.setNoOfStaff(noOfStaff+"");
							type.setRatePerBoy((product.getCtcAmount()+product.getManagementFees())+"");
							type.setCtcAmount(product.getCtcAmount());
							
							
							
							if(staffingFlag){
								if(product.getBillType().equals("Staffing")
										||product.getBillType().equals("Staffing And Rental")
										||product.getBillType().equals("Labour")){
									try{
		//								sumOfNumPresent=(int) (sumOfNumPresent+Double.parseDouble(product.getArea()));
										sumOfNumPresent=sumOfNumPresent+noOfPresentDay;
										type.setNoOfDaysPresent(noOfPresentDay);
										
										type.setBasePrice(product.getPrice());
									}catch(Exception e){
									}
								}
							}
							
							
							if(normalDayOtFlag){
								if(product.getBillType().equals("Overtime")){
									try{
//										sumOfNormalOt=sumOfNormalOt+Double.parseDouble(product.getArea());
//										type.setNormalDayOt(Double.parseDouble(product.getArea()));
										
										sumOfNormalOt=sumOfNormalOt+noOfPresentDay;
										type.setNormalDayOt(noOfPresentDay);
										
										type.setBasePriceNormalOt(product.getPrice());
										
									}catch(Exception e){
									}
								}
							}
							
							if(publicHolidayOtFlag){
								if((product.getBillType().equals("Public Holiday"))){
									try{
//										sumOfNormalPHOT=sumOfNormalPHOT+Double.parseDouble(product.getArea());
//										type.setPhOt(Double.parseDouble(product.getArea()));
										
										sumOfNormalPHOT=sumOfNormalPHOT+noOfPresentDay;
										type.setPhOt(noOfPresentDay);
										
										type.setBasePricePhOt(product.getPrice());
										
									}catch(Exception e){
									}
								}
							}
							
							if(nationalHolidayOtFlag){
								if(product.getBillType().equals("National Holiday")||product.getBillType().equals("Holiday")){
									try{
//										sumOfNormalNHOT=sumOfNormalNHOT+Double.parseDouble(product.getArea());
//										type.setNhOt(Double.parseDouble(product.getArea()));
										
										sumOfNormalNHOT=sumOfNormalNHOT+noOfPresentDay;
										type.setNhOt(noOfPresentDay);
										
										type.setBasePriceNhOt(product.getPrice());
										
									}catch(Exception e){
									}
								}
							}
							
//							type.setBasePrice(product.getPrice());
							
							double totalAmount=product.getPrice()*noOfPresentDay;
							
							logger.log(Level.SEVERE ,"Total AMOUNT : "+product.getPrice()+" :: "+totalAmount);
							
							
							if(product.getBillType().equals("Overtime")
									||product.getBillType().equals("Holiday")
									||product.getBillType().equals("National Holiday")
									||product.getBillType().equals("Public Holiday")){
								type.setOtAmount(totalAmount);
							}
							
//							type.setOtAmount(product.getPrice());
//							type.setTotalAmount(product.getTotalAmount());
							type.setTotalAmount(totalAmount);
							
							double sgstAmount=0;
							double cgstAmount=0;
							double igstAmount=0;
							double taxPercent=0;
//							double baseAmount=product.getTotalAmount();
							double baseAmount=totalAmount;
							double totalNetAmt=0;
							try{
								if(product.getVatTax()!=null){
									if(product.getVatTax().getTaxConfigName().contains("cgst")||product.getVatTax().getTaxConfigName().contains("CGST")
											||product.getVatTax().getTaxName().contains("cgst")||product.getVatTax().getTaxName().contains("CGST")
											||product.getVatTax().getTaxPrintName().contains("cgst")||product.getVatTax().getTaxPrintName().contains("CGST")){
										taxPercent=product.getVatTax().getPercentage();
										
										cgstAmount=(baseAmount*taxPercent)/100;
										type.setCgstPer(taxPercent);
									}
									
									if(product.getVatTax().getTaxConfigName().contains("sgst")||product.getVatTax().getTaxConfigName().contains("SGST")
											||product.getVatTax().getTaxName().contains("sgst")||product.getVatTax().getTaxName().contains("SGST")
											||product.getVatTax().getTaxPrintName().contains("sgst")||product.getVatTax().getTaxPrintName().contains("SGST")){
										taxPercent=product.getVatTax().getPercentage();
										
										sgstAmount=(baseAmount*taxPercent)/100;
										type.setSgstPer(taxPercent);
									}
									
									if(product.getVatTax().getTaxConfigName().contains("igst")||product.getVatTax().getTaxConfigName().contains("IGST")
											||product.getVatTax().getTaxName().contains("igst")||product.getVatTax().getTaxName().contains("IGST")
											||product.getVatTax().getTaxPrintName().contains("igst")||product.getVatTax().getTaxPrintName().contains("IGST")){
										taxPercent=product.getVatTax().getPercentage();
										
										igstAmount=(baseAmount*taxPercent)/100;
										type.setIgstPer(taxPercent);
									}
								}
								
								
								if(product.getServiceTax()!=null){
			
									if(product.getServiceTax().getTaxConfigName().contains("cgst")||product.getServiceTax().getTaxConfigName().contains("CGST")
											||product.getServiceTax().getTaxName().contains("cgst")||product.getServiceTax().getTaxName().contains("CGST")
											||product.getServiceTax().getTaxPrintName().contains("cgst")||product.getServiceTax().getTaxPrintName().contains("CGST")){
										taxPercent=product.getServiceTax().getPercentage();
										
										cgstAmount=(baseAmount*taxPercent)/100;
										type.setCgstPer(taxPercent);
									}
									
									if(product.getServiceTax().getTaxConfigName().contains("sgst")||product.getServiceTax().getTaxConfigName().contains("SGST")
											||product.getServiceTax().getTaxName().contains("sgst")||product.getServiceTax().getTaxName().contains("SGST")
											||product.getServiceTax().getTaxPrintName().contains("sgst")||product.getServiceTax().getTaxPrintName().contains("SGST")){
										taxPercent=product.getServiceTax().getPercentage();
										
										sgstAmount=(baseAmount*taxPercent)/100;
										type.setSgstPer(taxPercent);
									}
									
									if(product.getServiceTax().getTaxConfigName().contains("igst")||product.getServiceTax().getTaxConfigName().contains("IGST")
											||product.getServiceTax().getTaxName().contains("igst")||product.getServiceTax().getTaxName().contains("IGST")
											||product.getServiceTax().getTaxPrintName().contains("igst")||product.getServiceTax().getTaxPrintName().contains("IGST")){
										taxPercent=product.getServiceTax().getPercentage();
										
										igstAmount=(baseAmount*taxPercent)/100;
										type.setIgstPer(taxPercent);
									}
								
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							
							sumCgst+=cgstAmount;
							sumSgst+=sgstAmount;
							sumIgst+=igstAmount;
							totalNetAmt=baseAmount+cgstAmount+sgstAmount+igstAmount;
							sumNetTotalAmt+=totalNetAmt;
							
							
							type.setCgstAmt(cgstAmount);
							type.setSgstAmt(sgstAmount);
							type.setIgstAmt(igstAmount);
							type.setNetTotalAmt(totalNetAmt);
							
							
							
//							sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+product.getPrice();
//							sumOfNormalRateAndOtAMt=sumOfNormalRateAndOtAMt+(product.getTotalAmount());
							
							if(product.getBillType().equals("Overtime")
									||product.getBillType().equals("Holiday")
									||product.getBillType().equals("National Holiday")
									||product.getBillType().equals("Public Holiday")){
								sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+totalAmount;
							}
//							sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+product.getPrice();
							sumOfNormalRateAndOtAMt=sumOfNormalRateAndOtAMt+totalAmount;
							
							
							
							
							if(annexureMap.size()==0){
								ArrayList<AnnexureType> value=new ArrayList<AnnexureType>();
								value.add(type);
								annexureMap.put(key, value);
							}else{
								if(annexureMap.containsKey(key)){
									ArrayList<AnnexureType> list=annexureMap.get(key);
									type.setNoOfDaysPresent(type.getNoOfDaysPresent()+list.get(0).getNoOfDaysPresent());
									type.setNormalDayOt(type.getNormalDayOt()+list.get(0).getNormalDayOt());
									type.setPhOt(type.getPhOt()+list.get(0).getPhOt());
									type.setNhOt(type.getNhOt()+list.get(0).getNhOt());
									type.setOtAmount(type.getOtAmount()+list.get(0).getOtAmount());
									type.setTotalAmount(type.getTotalAmount()+list.get(0).getTotalAmount());
									type.setCgstAmt(type.getCgstAmt()+list.get(0).getCgstAmt());
									type.setSgstAmt(type.getSgstAmt()+list.get(0).getSgstAmt());
									type.setIgstAmt(type.getIgstAmt()+list.get(0).getIgstAmt());
									type.setNetTotalAmt(type.getNetTotalAmt()+list.get(0).getNetTotalAmt());
									
									if(type.getBasePrice()!=0){
										type.setBasePrice(type.getBasePrice());
									}else{
										type.setBasePrice(list.get(0).getBasePrice());
									}
									
									if(type.getBasePriceNormalOt()!=0){
										type.setBasePriceNormalOt(type.getBasePriceNormalOt());
									}else{
										type.setBasePriceNormalOt(list.get(0).getBasePriceNormalOt());
									}
									
									if(type.getBasePriceNhOt()!=0){
										type.setBasePriceNhOt(type.getBasePriceNhOt());
									}else{
										type.setBasePriceNhOt(list.get(0).getBasePriceNhOt());
									}
									
									if(type.getBasePricePhOt()!=0){
										type.setBasePricePhOt(type.getBasePricePhOt());
									}else{
										type.setBasePricePhOt(list.get(0).getBasePricePhOt());
									}
									
									
									type.setCtcAmount(type.getCtcAmount());
									type.setCgstPer(type.getCgstPer());
									type.setSgstPer(type.getSgstPer());
									type.setIgstPer(type.getIgstPer());
									
									ArrayList<AnnexureType> value=new ArrayList<AnnexureType>();
									value.add(type);
									annexureMap.put(key, value);
									
								}else{
									ArrayList<AnnexureType> value=new ArrayList<AnnexureType>();
									value.add(type);
									annexureMap.put(key, value);
								}
							}
						}
						
					}
				}
			}
//			}
		}
			
		return annexureMap;
	}
	
	private double getNoOfPresentDaysOnSite(ArrayList<Attendance> list,Date fromDate, Date toDate,String parameter, String siteLoc) {
		
		HashMap<String,ArrayList<Attendance>>empAttenMap=new HashMap<String, ArrayList<Attendance>>();
		
		String key="";
        for(Attendance att : list){
        	
        	if(att.getSiteLocation().equals(siteLoc)) {
	        	key=att.getEmpId()+"/"+att.getSiteLocation()+"/"+att.getEmpDesignation();
	        	if(empAttenMap.size()==0){
	        		ArrayList<Attendance> list1=new ArrayList<Attendance>();
	        		list1.add(att);
	        		empAttenMap.put(key, list1);
	        	}else{
	        		if(empAttenMap.containsKey(key)){
	        			empAttenMap.get(key).add(att);
	        		}else{
	        			ArrayList<Attendance> list1=new ArrayList<Attendance>();
	            		list1.add(att);
	            		empAttenMap.put(key, list1);
	        		}
	        	}
        	}
        }
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.BILLINGDOCUMENT, AppConstants.PC_ENABLESITELOCATION, list.get(0).getCompanyId())){
			HashMap<String,ArrayList<Attendance>>newempAttenMap=new HashMap<String, ArrayList<Attendance>>();
			newempAttenMap.putAll(empAttenMap);
			noOfStaff=getnoOfStaff(empAttenMap,newempAttenMap,list.get(0).getCompanyId());
		}
		else{
	        noOfStaff=empAttenMap.size();
		}
        
//        logger.log(Level.SEVERE , "To date :" + toDate);
		long diff= toDate.getTime() - fromDate.getTime();			
        int noofdays= ((int) (diff / (24 * 60 * 60 * 1000))) + 1;
//        logger.log(Level.SEVERE," :::=No of days  = :::  "+noofdays);
        
        List<String> processNameList=new ArrayList<String>();
		processNameList.add("CNC");
		processNameList.add("PaySlip");
		
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", list.get(0).getCompanyId()).filter("processName IN", processNameList).filter("configStatus", true).list();
		boolean nhOtFlag = false, otRateFlag = false;
		
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
							&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("NH_OT_Include_In_Earning")){
						nhOtFlag=true;
					}
					if(processConf.getProcessName().equalsIgnoreCase("CNC")&&processConf.isConfigStatus()==true
							&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("EnableSiteLocation")){
						otRateFlag=true;
					}
				}
			}
		}
		
//		logger.log(Level.SEVERE ,"otRateFlag "+otRateFlag);
		PaySlipServiceImpl paySlipService=new PaySlipServiceImpl();
        double totalDays=0;
        for (Map.Entry<String,ArrayList<Attendance>> entry : empAttenMap.entrySet()){ 
        	EmployeeInfo info=ofy().load().type(EmployeeInfo.class).filter("companyId", entry.getValue().get(0).getCompanyId()).filter("empCount", entry.getValue().get(0).getEmpId()).first().now();
        	double days =0;
        	if(parameter.equalsIgnoreCase(AppConstants.STAFFING)||parameter.equalsIgnoreCase("Labour")||parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL)){
	        	double unpaidLeaveHrs=getUnpaidLeaveHrs(fromDate, toDate, entry.getValue().get(0));
	        	logger.log(Level.SEVERE,"Unpaid Leave HRS : "+unpaidLeaveHrs);
		    	days = calculatePaidDays(entry.getValue() ,info , noofdays , nhOtFlag , unpaidLeaveHrs);
		    	totalDays=totalDays+days;
        	}else if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)||parameter.equalsIgnoreCase("Holiday")){
    			days=paySlipService.getNationalHolidayDetails(entry.getValue(),info);
    			if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Direct")){
    				totalDays += (days/2);
    				logger.log(Level.SEVERE,"DIRECT : "+totalDays);
				}else if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Indirect")){
					totalDays += days;
					logger.log(Level.SEVERE,"INDIRECT : "+totalDays);
				}
    		}else if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
    			days = getOverTimeHrs(info, fromDate, toDate, nhOtFlag,otRateFlag,entry.getValue().get(0)) /info.getLeaveCalendar().getWorkingHours();
    			totalDays += days;
    		}else if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
//    			days=paySlipService.getPublicHolidayDetails(entry.getValue(),info);
    			/**
    			 * @author Vijay Date :- 13-02-2021
    			 * Des :- getting issue at sunrise raised by Rahul
    			 * So updated the code with new method 
    			 */
    			days=getPublicHolidayDetails(entry.getValue(),info);
    			logger.log(Level.SEVERE ,parameter+" PH "+days);
				totalDays += days;
    		}
    		logger.log(Level.SEVERE ,parameter+" = " + days+" EMP ID- "+entry.getKey()+" Attendance- "+entry.getValue().size());
        }
		return totalDays;
	}
	
	

	private int getnoOfStaff(HashMap<String, ArrayList<Attendance>> empAttenMap,HashMap<String, ArrayList<Attendance>> newempAttenMap, Long companyId) {
		
		for (Map.Entry<String,ArrayList<Attendance>> entry : empAttenMap.entrySet()){ 
				ArrayList<Attendance> list = entry.getValue();

	        	Employee employee=ofy().load().type(Employee.class).filter("companyId", list.get(0).getCompanyId()).filter("count", list.get(0).getEmpId()).first().now();
	        	if(employee!=null && employee.isReliever()){
	        		newempAttenMap.remove(entry.getKey());
			        logger.log(Level.SEVERE, "isReliever "+newempAttenMap.size());

	        	}
		 } 
		return newempAttenMap.size();
	}

	public double getUnpaidLeaveHrs(Date startDate, Date endDate,Attendance attendance) {
		ArrayList<AvailedLeaves> availedLeaves = new ArrayList<AvailedLeaves>();
		double leaveHrs = 0d;
		
		List<EmployeeLeave> leaves = ofy().load().type(EmployeeLeave.class)
				.filter("companyId", attendance.getCompanyId())
				.filter("emplId", attendance.getEmpId())
				.filter("leaveDate >=", startDate).list();
		
		logger.log(Level.SEVERE ,"EMP ID: "+attendance.getEmpId()+" START DT: "+startDate+" LEAVE LIST: "+leaves.size());
		List<EmployeeLeave> validleaves = new ArrayList<EmployeeLeave>();
		for (EmployeeLeave temp : leaves) {
			if (temp.getLeaveDate().equals(endDate)|| temp.getLeaveDate().before(endDate)) {
				
				if(temp.getSiteLocation()==null){
					validleaves.add(temp);
				}else{
					if(attendance.getSiteLocation()!=null&&temp.getSiteLocation().equals(attendance.getSiteLocation())){
						validleaves.add(temp);
					}
				}
			}
		}
		
		for (EmployeeLeave temp : validleaves) {
			boolean flag = false;
			if (temp.getUnpaidLeave() == false) {
				if (availedLeaves.size() == 0) {
					AvailedLeaves avLeav = new AvailedLeaves();
					avLeav.setLeaveName(temp.getLeaveTypeName());
					avLeav.setLeavehour(temp.getLeaveHrs());
					avLeav.setUnPaid(true);
					availedLeaves.add(avLeav);
				} else {
					for (int i = 0; i < availedLeaves.size(); i++) {
						if (availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())) {
							flag = true;
							double hrs = 0;
							hrs = availedLeaves.get(i).getLeavehour();
							hrs = hrs + temp.getLeaveHrs();
							availedLeaves.get(i).setLeavehour(hrs);
							availedLeaves.get(i).setUnPaid(true);
						}
					}
					if (flag == false) {
						AvailedLeaves avLeav = new AvailedLeaves();
						avLeav.setLeaveName(temp.getLeaveTypeName());
						avLeav.setLeavehour(temp.getLeaveHrs());
						avLeav.setUnPaid(true);
						availedLeaves.add(avLeav);
					}
				}

				leaveHrs = leaveHrs + temp.getLeaveHrs();
			} else {
				if (availedLeaves.size() == 0) {
					AvailedLeaves avLeav = new AvailedLeaves();
					avLeav.setLeaveName(temp.getLeaveTypeName());
					avLeav.setLeavehour(temp.getLeaveHrs());
					avLeav.setUnPaid(false);
					availedLeaves.add(avLeav);
				} else {

					for (int i = 0; i < availedLeaves.size(); i++) {
						if (availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())) {
							flag = true;
							double hrs = availedLeaves.get(i).getLeavehour();
							hrs = hrs + temp.getLeaveHrs();
							availedLeaves.get(i).setLeavehour(hrs);
							availedLeaves.get(i).setUnPaid(false);
						}
					}
					if (flag == false) {
						AvailedLeaves avLeav = new AvailedLeaves();
						avLeav.setLeaveName(temp.getLeaveTypeName());
						avLeav.setLeavehour(temp.getLeaveHrs());
						avLeav.setUnPaid(false);
						availedLeaves.add(avLeav);
					}
				}
			}
		}
		return leaveHrs;
	}
	
	private double calculatePaidDays(ArrayList<Attendance> attendanceList,EmployeeInfo info,int totalPaidDays,boolean nhOtFlag,double unpaidLeaveHrs){
		PaySlipServiceImpl paySlipService=new PaySlipServiceImpl();
		double paidDays = 0;
		int extraDay = 0;
		int absentDays=0;
		int listSize = attendanceList.size();
		if(listSize == 0){
			listSize = totalPaidDays;
		}
		logger.log(Level.SEVERE,"listSize : "+listSize+" paid days "+totalPaidDays);
		if(nhOtFlag){
			if(info != null && info.getLeaveCalendar()!=null){
				extraDay=paySlipService.getNationalHolidayDetails(attendanceList,info);
			}
		}
		absentDays=paySlipService.getTotalAbsentDays(attendanceList);	
		
		logger.log(Level.SEVERE,"Extra day : "+extraDay+" Absent Day: "+absentDays+" wh "+info.getLeaveCalendar().getWorkingHours());
		if(info != null && info.getLeaveCalendar() != null){
			paidDays=(listSize+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-absentDays;	
		}
		return paidDays;
	}
	
	public double getOverTimeHrs(EmployeeInfo info, Date startDate,Date endDate, boolean nhOtFlag, boolean otRateFlag,Attendance attendance) {
		List<EmployeeOvertime> otList = new ArrayList<EmployeeOvertime>();
		double otHrs = 0d;
		try {
			DateUtility.getDateWithTimeZone("IST", startDate);
			DateUtility.getDateWithTimeZone("IST", endDate);

			otList = ofy().load().type(EmployeeOvertime.class)
					.filter("companyId", info.getCompanyId())
					.filter("emplId", info.getEmpCount())
					.filter("otDate >=", startDate).list();

			List<EmployeeOvertime> validotList = new ArrayList<EmployeeOvertime>();

			for (EmployeeOvertime temp : otList) {
				if (temp.getOtDate().equals(endDate)|| temp.getOtDate().before(endDate)) {
					if(temp.getSiteLocation()==null){
						validotList.add(temp);
					}else{
						if(attendance.getSiteLocation()!=null&&temp.getSiteLocation().equals(attendance.getSiteLocation())){
							validotList.add(temp);
						}
					}
				}
			}

			for (EmployeeOvertime temp : validotList) {
				boolean isHoliday = info.getLeaveCalendar().isHolidayDate(temp.getOtDate());
				if (isHoliday) {
					for (Holiday holiday : info.getLeaveCalendar().getHoliday()) {
						if (holiday.getHolidaydate().equals(temp.getOtDate())
								&& holiday.getHolidayType().equalsIgnoreCase("National Holiday")) {
							temp.setOtHrs(0);
						}
						if (otRateFlag&& holiday.getHolidaydate().equals(temp.getOtDate())
								&& holiday.getHolidayType().equalsIgnoreCase("Public Holiday")) {
							temp.setOtHrs(0);
						}
					}
				}
				if (otRateFlag && temp.getHolidayLabel() != null&& temp.getHolidayLabel().equals("PH")) {
					temp.setOtHrs(0);
				}
				otHrs = otHrs + temp.getOtHrs();
			}
		} catch (Exception e) {
		}

		return otHrs;

	}
	
	private double getPublicHolidayDetails(ArrayList<Attendance> attendanceList, EmployeeInfo info) {

		int extraDays=0;
		for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
			if(holiday.getHolidayType().trim().equalsIgnoreCase("Public Holiday")){
				for(Attendance attendance:attendanceList){
					if(attendance.getAttendanceDate().equals(holiday.getHolidaydate())&&attendance.getWorkedHours()!=0){
						logger.log(Level.SEVERE ," getAttendanceDate "+attendance.getAttendanceDate()+" getHolidaydate "+holiday.getHolidaydate());
							extraDays=extraDays+1;
							attendance.setHolidayLabel(null);
					}
				}
			}
		}
		
		logger.log(Level.SEVERE ," PH "+extraDays);
		
		for(Attendance attendance:attendanceList){
			if(attendance.getHolidayLabel()!=null&&attendance.getHolidayLabel().equals("PH") && attendance.getTotalWorkedHours()>0){
				extraDays=extraDays+1;
			}
		}
		logger.log(Level.SEVERE ," PH "+extraDays);
		return extraDays;
	
	}
	
	/**
	 * @author Anil @since 13-11-2021
	 * @param bill
	 * @param so
	 * @param product
	 * @return
	 */
	public HashMap<String,ArrayList<SalesAnnexureType>> getSalesAnnexureDetails(SalesOrder so,SalesOrderProductLineItem product){
		HashMap<String,ArrayList<SalesAnnexureType>> salesAnnexureMap=new HashMap<String, ArrayList<SalesAnnexureType>>();
		try{
			double sgstAmount=0;
			double cgstAmount=0;
			double igstAmount=0;
			double utgstAmount=0;
			double baseAmount=product.getBaseBillingAmount();
			double totalNetAmt=0;
			
			double sgstPer=0;
			double cgstPer=0;
			double igstPer=0;
			double utgstper=0;
			
			try{
				if(product.getVatTax()!=null){
					if(product.getVatTax().getTaxConfigName().contains("cgst")||product.getVatTax().getTaxConfigName().contains("CGST")
							||product.getVatTax().getTaxName().contains("cgst")||product.getVatTax().getTaxName().contains("CGST")
							||product.getVatTax().getTaxPrintName().contains("cgst")||product.getVatTax().getTaxPrintName().contains("CGST")){
						cgstPer=product.getVatTax().getPercentage();
						cgstAmount=(baseAmount*cgstPer)/100;
					}
					
					if(product.getVatTax().getTaxConfigName().contains("sgst")||product.getVatTax().getTaxConfigName().contains("SGST")
							||product.getVatTax().getTaxName().contains("sgst")||product.getVatTax().getTaxName().contains("SGST")
							||product.getVatTax().getTaxPrintName().contains("sgst")||product.getVatTax().getTaxPrintName().contains("SGST")){
						sgstPer=product.getVatTax().getPercentage();
						sgstAmount=(baseAmount*sgstPer)/100;
					}
					
					if(product.getVatTax().getTaxConfigName().contains("igst")||product.getVatTax().getTaxConfigName().contains("IGST")
							||product.getVatTax().getTaxName().contains("igst")||product.getVatTax().getTaxName().contains("IGST")
							||product.getVatTax().getTaxPrintName().contains("igst")||product.getVatTax().getTaxPrintName().contains("IGST")){
						igstPer=product.getVatTax().getPercentage();
						igstAmount=(baseAmount*igstPer)/100;
					}
					
					if(product.getVatTax().getTaxConfigName().contains("utgst")||product.getVatTax().getTaxConfigName().contains("UTGST")
							||product.getVatTax().getTaxName().contains("utgst")||product.getVatTax().getTaxName().contains("UTGST")
							||product.getVatTax().getTaxPrintName().contains("utgst")||product.getVatTax().getTaxPrintName().contains("UTGST")){
						utgstper=product.getVatTax().getPercentage();
						utgstAmount=(baseAmount*utgstper)/100;
					}
				}
				
				
				if(product.getServiceTax()!=null){
					if(product.getServiceTax().getTaxConfigName().contains("cgst")||product.getServiceTax().getTaxConfigName().contains("CGST")
							||product.getServiceTax().getTaxName().contains("cgst")||product.getServiceTax().getTaxName().contains("CGST")
							||product.getServiceTax().getTaxPrintName().contains("cgst")||product.getServiceTax().getTaxPrintName().contains("CGST")){
						cgstPer=product.getServiceTax().getPercentage();
						cgstAmount=(baseAmount*cgstPer)/100;
					}
					
					if(product.getServiceTax().getTaxConfigName().contains("sgst")||product.getServiceTax().getTaxConfigName().contains("SGST")
							||product.getServiceTax().getTaxName().contains("sgst")||product.getServiceTax().getTaxName().contains("SGST")
							||product.getServiceTax().getTaxPrintName().contains("sgst")||product.getServiceTax().getTaxPrintName().contains("SGST")){
						sgstPer=product.getServiceTax().getPercentage();
						sgstAmount=(baseAmount*sgstPer)/100;
					}
					
					if(product.getServiceTax().getTaxConfigName().contains("igst")||product.getServiceTax().getTaxConfigName().contains("IGST")
							||product.getServiceTax().getTaxName().contains("igst")||product.getServiceTax().getTaxName().contains("IGST")
							||product.getServiceTax().getTaxPrintName().contains("igst")||product.getServiceTax().getTaxPrintName().contains("IGST")){
						igstPer=product.getServiceTax().getPercentage();
						igstAmount=(baseAmount*igstPer)/100;
					}
					
					if(product.getServiceTax().getTaxConfigName().contains("utgst")||product.getServiceTax().getTaxConfigName().contains("UTGST")
							||product.getServiceTax().getTaxName().contains("utgst")||product.getServiceTax().getTaxName().contains("UTGST")
							||product.getServiceTax().getTaxPrintName().contains("utgst")||product.getServiceTax().getTaxPrintName().contains("UTGST")){
						utgstper=product.getServiceTax().getPercentage();
						utgstAmount=(baseAmount*utgstper)/100;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			totalNetAmt=baseAmount+cgstAmount+sgstAmount+igstAmount+utgstAmount;
			SalesAnnexureType type=new SalesAnnexureType(so.getCount(), so.getSalesOrderDate(), so.getNetpayable(), so.getBudget(), product.getBaseBillingAmount(), so.getCustBranch(), so.getCarpetArea(), so.getCostCode(), igstAmount, utgstAmount, sgstAmount, cgstAmount, igstPer, utgstper, sgstPer, cgstPer, totalNetAmt,product.getProdName());
			ArrayList<SalesAnnexureType> salesAnnexureList=new ArrayList<SalesAnnexureType>();
			salesAnnexureList.add(type);
			
			salesAnnexureMap.put(product.getProdName(), salesAnnexureList);
		
		}catch(Exception e){
			
		}
		
		return salesAnnexureMap;
	}
	
}
