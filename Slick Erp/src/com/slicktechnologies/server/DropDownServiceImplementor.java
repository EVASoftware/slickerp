package com.slicktechnologies.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.DropDownService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class DropDownServiceImplementor extends RemoteServiceServlet implements DropDownService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3702205550138129021L;

	 Logger logger = Logger.getLogger("NameOfYourLogger");
	 
	@Override
	public ArrayList<SuperModel> getDropDown(String searchType) {
		
		
		ArrayList<SuperModel> array=new ArrayList<SuperModel>();
		System.out.println("Search Type"+searchType);
		String[] dataret=searchType.split("-");
		
		String type=dataret[0];
		System.out.println("Type"+type);
		long companyID=Long.parseLong(dataret[1]);
		System.out.println("Company Id"+companyID);
		
	
		
	
		
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALCONFIG)){
			List<Config> configData=ofy().load().type(Config.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(configData);
			System.out.println("Config Size"+configData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALCATEGORY)){
			List<ConfigCategory> categoryData=ofy().load().type(ConfigCategory.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(categoryData);
			System.out.println("Category Size"+categoryData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALTYPE)){
			List<Type> typeData=ofy().load().type(Type.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(typeData);
			System.out.println("Type Size"+typeData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALEMPLOYEE)){
			/**
			 * @author Anil @since 25-05-2021
			 * Added status filter
			 */
			List<Employee> empData=ofy().load().type(Employee.class).filter("companyId", companyID).filter("status",true).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(empData);
			System.out.println("Emp Size"+empData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALBRANCH)){
			List<Branch> branchData=ofy().load().type(Branch.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(branchData);
			System.out.println("Branch Size"+branchData.size());
			
			/**
			 * Date :- 06-07-2018 By Vijay
			 * Des :- for Data uploading global data using for validation purpose
			 */
			DataMigrationImpl.globalBranchlist = branchData;
			/**
			 * ends here
			 */
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALCOUNTRY)){
			List<Country> countryData=ofy().load().type(Country.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(countryData);
			System.out.println("Country Size"+countryData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALSTATE)){
			List<State> stateData=ofy().load().type(State.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(stateData);
			System.out.println("State Size"+stateData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALCITY)){
			List<City> cityData=ofy().load().type(City.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(cityData);
			System.out.println("City Size"+cityData.size());
			
			/**
			 * Date :- 06-07-2018 By Vijay
			 * Des :- for Data uploading global data using for validation purpose
			 */
			DataMigrationImpl.globalCitylist = cityData;
			/**
			 * ends here
			 */
		}
		
		/**
		 * Date 17-04-2018 By vijay
		 * if DontLoadAllLocalityAndCity process configuration is active then we are not loading all global data for City And Locality
		 * because if we locality has heavy data then our system not respondig properly so this cases we will load locality on basis of City in address composite
		 */
		
		if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity", companyID)){
			if(type.trim().equals(AppConstants.GLOBALRETRIEVALLOCALITY)){
				List<Locality> localityData=ofy().load().type(Locality.class).filter("companyId", companyID).list();
				ObjectifyService.reset();
			    ofy().consistency(Consistency.STRONG);
			    ofy().clear();
				array.addAll(localityData);
				System.out.println("Locality Size"+localityData.size());
			}
		}
		
		
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALPROCESSCONFIG)){
			List<ProcessConfiguration> processConfigData=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(processConfigData);
			System.out.println("Process Config Size"+processConfigData.size());
			
			
			/**
			 * Date : 10-04-2018 BY ANIL
			 * here we will set sms configuration status as active or inactive depending upon the license end date
			 */
			
			Company company=ofy().load().type(Company.class).filter("companyId", companyID).first().now();
			if(company!=null){
				ArrayList<LicenseDetails> licList=new ArrayList<LicenseDetails>();
				if(company.getLicenseDetailsList()!=null){
					for(LicenseDetails obj:company.getLicenseDetailsList()){
						if(obj.getLicenseType().equals(AppConstants.LICENSETYPELIST.EVA_SMS_LICENSE)){
							licList.add(obj);
						}
					}
					Comparator<LicenseDetails> licDtComp=new Comparator<LicenseDetails>() {
						@Override
						public int compare(LicenseDetails arg0, LicenseDetails arg1) {
							return arg0.getStartDate().compareTo(arg1.getStartDate());
						}
					};
					Collections.sort(licList, licDtComp);
					
					Date todaysDate=DateUtility.getDateWithTimeZone("IST", new Date());
					
					if(licList.size()!=0){
						int noOfLicense=0;
						int expiredLicenseCount=0;
						int notActiveLicenseCount=0;
						boolean validLicense=false;
						for(LicenseDetails license:licList){
							if(todaysDate.after(license.getStartDate())){
								if(todaysDate.before(license.getEndDate())){
									noOfLicense=noOfLicense+license.getNoOfLicense();
									validLicense=true;
								}else{
									expiredLicenseCount++;
								}
							}else{
								notActiveLicenseCount++;
							}
						}
						if(validLicense==false&&(expiredLicenseCount>0&&notActiveLicenseCount>0)){
							List<SmsConfiguration>smsConfiList=ofy().load().type(SmsConfiguration.class).filter("companyId", companyID).list();
							if(smsConfiList!=null){
								for(SmsConfiguration sms:smsConfiList){
									sms.setStatus(false);
								}
								ofy().save().entities(smsConfiList).now();
							}
						}
					}
				}
			}
			/**
			 * End
			 */
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALPROCESSNAME)&&array.size()==0)
		{
			List<ProcessName> processNameData=ofy().load().type(ProcessName.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(processNameData);
			System.out.println("Process Name Size"+processNameData.size());
		}
		
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL)&&array.size()==0)
		{
			List<MultilevelApproval> levelApprovalData=ofy().load().type(MultilevelApproval.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(levelApprovalData);
			System.out.println("Multi Level Approval Size"+levelApprovalData.size());
		}
		
		
		/**
		 * Date: 14-02-2017 By ANil
		 */
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALROLEDEFINITION)&&array.size()==0)
		{
			List<RoleDefinition> levelApprovalData=ofy().load().type(RoleDefinition.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(levelApprovalData);
			System.out.println("Role Definition Size"+levelApprovalData.size());
		}
		/**
		 * End
		 */
		
		/**
		 * Date: 31-05-2018 By ANil
		 */
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALOVERTIME)&&array.size()==0)
		{
			List<Overtime> levelApprovalData=ofy().load().type(Overtime.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(levelApprovalData);
			System.out.println("Overtime Size"+levelApprovalData.size());
		}
		/**
		 * End
		 */
		
		/**
		 * Date 19-06-2019 By Vijay 
		 * Des :- NBHC Inventory Management Loading all Warehouses for expired warehouses should not allow material 
		 */
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALWAREHOUSE)&&array.size()==0)
		{
			List<WareHouse> warehouseData=ofy().load().type(WareHouse.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(warehouseData);
			System.out.println("Overtime Size"+warehouseData.size());
		}
		/**
		 * ends here
		 */
		
		
		/**
		 * @author Vijay Chougule
		 * Date :- 19-July-2020
		 * Des :- For HRProject Dropdown HRProject loading globally
		 */
		
		if(type.trim().equals(AppConstants.GLOBALHRPROJECT) &&array.size()==0 )
		{
			logger.log(Level.SEVERE, "Global HR Project loading RPC "+array.size());
			List<HrProject> HRProjectData=ofy().load().type(HrProject.class).filter("companyId", companyID).filter("status", true).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(HRProjectData);
			System.out.println("GLOBALHRPROJECT Size"+HRProjectData.size());
			logger.log(Level.SEVERE, "HRProjectData size"+array.size());

		}

		/**
		 * @author Anil
		 * @since 18-12-2020
		 * loading screen menu configuration configuration
		 */
		if(type.trim().equals(AppConstants.SCREENMENUCONFIGURATION)){
			List<ScreenMenuConfiguration> screenMenuConfgLis=ofy().load().type(ScreenMenuConfiguration.class).filter("companyId", companyID).filter("status", true).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(screenMenuConfgLis);
			System.out.println("screenMenuConfgLis Size"+screenMenuConfgLis.size());
		}
		if(type.trim().equals(AppConstants.GLOBALRETRIEVALEMPLOYEEINFO)){
			List<EmployeeInfo> empData=ofy().load().type(EmployeeInfo.class).filter("companyId", companyID).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
			array.addAll(empData);
			logger.log(Level.SEVERE,"Employee info list"+empData.size());
			System.out.println("Emp Size"+empData.size());
		}
		
		return array;
	}

	@Override
	public ArrayList<String> getApprovalDropDown(long companyId,ArrayList<MultilevelApprovalDetails> approvalLis, int level) {
		List<String> empList=new ArrayList<String>();
		for(int k=0;k<approvalLis.size();k++)
		{
			if(Integer.parseInt(approvalLis.get(k).getLevel())==level)
			{
				List<User> userLis=ofy().load().type(User.class).filter("companyId", companyId).filter("role.roleName", approvalLis.get(k).getEmployeeRole().trim()).list();
				if(userLis.size()!=0){
					for(int y=0;y<userLis.size();y++)
					{
						empList.add(userLis.get(y).getEmployeeName());
					}
				}
			}
		}
		ArrayList<String> approvalLevel1Employees=new ArrayList<String>(new HashSet<String>(empList));
		return approvalLevel1Employees;
	}

	@Override
	public ArrayList<SuperModel> getEmployeeDropDown(List<String> branchList,Long companyId) {
		
		logger.log(Level.SEVERE,"getEmployeeDropDown");
		List<String> adminList=new ArrayList<String>();
		List<User> userList=ofy().load().type(User.class).filter("companyId", companyId).list();
		if(userList.size()!=0){
			for(User user:userList){
				if(user.getRoleName().equalsIgnoreCase("Admin")&&user.getStatus().equals(true)){
					adminList.add(user.getEmployeeName());
				}
			}
		}
		
		ArrayList<SuperModel> array=new ArrayList<SuperModel>();
		
		List<Employee> empData=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
	    ofy().clear();
	   logger.log(Level.SEVERE,"Emp Size BRANCH  "+empData.size());
	   logger.log(Level.SEVERE,"Total BRANCH ASSIGNED "+branchList.size());
	   logger.log(Level.SEVERE,"Total ADMIN USER "+adminList.size());
	    
	    for(String adminUser:adminList){
			for(int i=0;i<empData.size();i++){
				if(empData.get(i).getFullname().trim().equals(adminUser.trim())){
					array.add(empData.get(i));
	 	    		empData.remove(i);
	 	    		i--;
				}
			}
	    }
	    
	    for(String branch:branchList){
	    	for(int i=0;i<empData.size();i++){
	    		boolean branchFlag=false;
	 	    	if(empData.get(i).getBranchName().trim().equals(branch.trim())){
	 	    		branchFlag=true;
	 	    		array.add(empData.get(i));
	 	    		empData.remove(i);
	 	    		i--;
	 	    	}
	 	    	
	 	    	if(!branchFlag){
	 	    		Employee emp=empData.get(i);
	 	    		if(emp.getEmpBranchList()!=null&&emp.getEmpBranchList().size()!=0){
	 	    			for(EmployeeBranch empB:emp.getEmpBranchList()){
	 	    				if(empB.getBranchName().trim().equals(branch.trim())){
	 	    					array.add(empData.get(i));
	 	   	 	    			empData.remove(i);
	 	   	 	    			i--;
	 	   	 	    			break;
	 	    				}
	 	    			}
	 	    		}
	 	    	}
	 	    }
    	}
	    
	   logger.log(Level.SEVERE,"Emp Size BRANCH WISE  "+array.size());
	   
		
		
		/**
		
		List<Employee> empData=ofy().load().type(Employee.class).filter("companyId", companyId).filter("branchName IN", branchList).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
	    ofy().clear();
		array.addAll(empData);
		Console.log("Emp Size BRANCH  "+empData.size());
		
		List<Employee> empData2=ofy().load().type(Employee.class).filter("companyId", companyId).filter("empBranchList.BranchName IN", branchList).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
	    ofy().clear();
	    
	   logger.log(Level.SEVERE,"Emp Size BRANCH LIST "+empData2.size());
	    for(Employee emp:empData){
	    	for(int i=0;i<empData2.size();i++){
	    		if(emp.getCount()==empData2.get(i).getCount()){
	    			empData2.remove(i);
	    			i--;
	    		}
	    	}
	    }
	   logger.log(Level.SEVERE,"Emp Size BRANCH LIST After removing "+empData2.size());
		array.addAll(empData2);
		
		for(int i=0;i<adminList.size();i++){
			for(Employee employee:empData){
				if(employee.getFullname().equals(adminList.get(i))){
					adminList.remove(i);
					i--;
				}
			}
			
			for(Employee employee:empData2){
				if(employee.getFullname().equals(adminList.get(i))){
					adminList.remove(i);
					i--;
				}
			}
		}
		
		if(adminList.size()!=0){
			List<Employee> empData1=ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname IN", adminList).list();
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
		    array.addAll(empData1);
		}
		
		**/
		
		logger.log(Level.SEVERE,"TOTAL EMPLOYEE AT BRANCH RESTRICTION"+array.size());

		return array;
	}
	
	

}
