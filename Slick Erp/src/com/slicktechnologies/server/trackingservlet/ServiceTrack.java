package com.slicktechnologies.server.trackingservlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;

public class ServiceTrack extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4026553793171829824L;

	/**
	 * 
	 */
	static org.json.JSONArray deviceArray = new JSONArray();

	Logger logger = Logger.getLogger("ServiceTrack.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String data = req.getParameter("data").trim();
		logger.log(Level.SEVERE, "data:::::"+data);
		if (!data.trim().equalsIgnoreCase("fetchLocation")) {
			logger.log(Level.SEVERE, "Calling from EVA Priora");
			org.json.JSONObject jsonObject = null;
			try {
				jsonObject = new org.json.JSONObject(data);
				logger.log(Level.SEVERE, "jsonObject:::::"+jsonObject);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (jsonObject != null) {
				String userName = jsonObject.optString("userName")
						.trim();
				logger.log(Level.SEVERE, "userName:::::"+userName);

				ServiceTrackEmbed serviceEmbed = ServiceTrackEmbed
						.getInstance();
				JSONArray array = new JSONArray();
				JSONArray array1 = new JSONArray();
				array = serviceEmbed.getJsonArray();
				if (array != null&&array.length()>0) {
					array1=array;
					logger.log(Level.SEVERE, "Inside !=null Array:::::");
					for (int i = 0; i < array1.length(); i++) {
						org.json.JSONObject vehicleObj1 = null;
						try {
							vehicleObj1 = array1.getJSONObject(i);
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						org.json.JSONObject vehicleObj = vehicleObj1;
						if (vehicleObj.optString("userName").trim()
								.equalsIgnoreCase(userName)) {
							logger.log(Level.SEVERE,"MAtched Record");
							try {

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								logger.log(Level.SEVERE, "ERROR1:::::" + e);
							}
							vehicleObj = createJSONData(vehicleObj);
							try {
								array1.getJSONObject(i).put(userName.trim(),
										vehicleObj);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								logger.log(Level.SEVERE, "ERROR2:::::" + e);
							}
//							array1.put(vehicleObj);
							break;
						} else {
							logger.log(Level.SEVERE,"not MAtched Record");
							vehicleObj = createJSONData(jsonObject);
							try {
								array1.getJSONObject(i).put(userName.trim(),
										vehicleObj);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								logger.log(Level.SEVERE, "ERROR2:::::" + e);
							}
//							array1.put(vehicleObj);
							break;
						}

					}
				} else {
					logger.log(Level.SEVERE, "Inside ==null Array:::::");
					org.json.JSONObject vehicleObj1 = new org.json.JSONObject();
					org.json.JSONObject vehicleObj = new org.json.JSONObject();

					vehicleObj = createJSONData(jsonObject);
					logger.log(Level.SEVERE,"vehicleObj:::::"+vehicleObj.toString());
					try {
						vehicleObj1.put(userName.trim(), vehicleObj);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.log(Level.SEVERE, "ERROR2:::::" + e);
					}
					array1.put(vehicleObj1);
				}
				logger.log(Level.SEVERE,"array1:::::"+array1.toString());
				serviceEmbed.setJsonArray(array1);
				String jsonString = serviceEmbed.getJsonArray().toString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, jsonString);
				resp.getWriter().println(jsonString);
				resp.getWriter().println();
			}
		} else {
			logger.log(Level.SEVERE, "Calling from EVA Pedio");
			ServiceTrackEmbed serviceEmbed = ServiceTrackEmbed.getInstance();
			String jsonString = serviceEmbed.getJsonArray().toString()
					.replaceAll("\\\\", "");
			logger.log(Level.SEVERE, jsonString);
			resp.getWriter().println(jsonString);
			resp.getWriter().println();
		}
	}

	private org.json.JSONObject createJSONData(org.json.JSONObject vehicleObj) {
		// TODO Auto-generated method stub
		org.json.JSONObject obj = new org.json.JSONObject();
		try {
			obj.put("status", vehicleObj.getString("status").trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR3:::::" + e);
		}
		try {
			obj.put("dateTime", vehicleObj.getString("dateTime").trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR4:::::" + e);
		}
		try {
			obj.put("userName", vehicleObj.getString("userName").trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR5:::::" + e);
		}
		try {
			obj.put("lat", vehicleObj.getString("lat").trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR6:::::" + e);
		}
		try {
			obj.put("long", vehicleObj.getString("long").trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR7:::::" + e);
		}
		logger.log(Level.SEVERE, "CREATION OBJ:::::::" + obj.toString());
		return obj;
	}
}
