package com.slicktechnologies.server.trackingservlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class ActivateFlagForDeviceTrack extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1283209584383891146L;
	/**
	 * 
	 */
	Logger logger = Logger.getLogger("ActivateFlagForDeviceTrack");
	/**
	 * API KEY for evadev017
	 */
	public static final String API_KEY = "AAAA0W20xk4:APA91bFrcfUmZby2NuFIHpYCHt4JC4DJl_18I4QFnTQuQJ-zQuE4h3utAX_-O16R3apFfVOP-KBj9X_anTzBp4hjfJ9xI_7HIXg12KTJWXj2GCjHFQ6ufLwXQOF7A4rJldAp3d0cE9Kn";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
		String userName = req.getParameter("userName").trim();
		String response = req.getParameter("response").trim();
		String appName=req.getParameter("applicationName").trim();

		String url = null;
		if (urlCalled.contains("https")) {
			url = urlCalled.replace("https://", "");
		} else {
			url = urlCalled.replace("http://", "");
		}
		String[] splitUrl = url.split("\\.");
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		sendPushNotification(company.getCompanyId(), userName, response,
				resp,appName);
	}

	private void sendPushNotification(long companyId, String userName,
			String response, HttpServletResponse resp, String appName) {
		ArrayList<String> registrationIdList = new ArrayList<String>();
		if (userName.trim().equalsIgnoreCase("All")) {
			List<RegisterDevice> deviceList = ofy().load()
					.type(RegisterDevice.class).filter("companyId", companyId)
					.list();
			for (int i = 0; i < deviceList.size(); i++) {
				registrationIdList.add(deviceList.get(i).getRegId()
						.trim());
			}
		} else {
			RegisterDevice device = ofy().load().type(RegisterDevice.class)
					.filter("companyId", companyId)
					.filter("userName", userName.trim()).filter("applicationName", appName.trim()).first().now();
			if (device != null) {
				logger.log(Level.SEVERE,
						"Device ... " + device.getRegId() + " --- "+device.getUserName()+"----"
								+ device.toString());
				registrationIdList.add(device.getRegId().trim());
			}
		}

			final String apiKey = "AAAAZPGA6AE:APA91bHH0r_HsFXl6Uhht_XlSoWf2-THhqDp0VfRyT_Ni5eWs9AaeX2URd35nmnryMldb8ZRTeEypee6HgPyJD0oFUneHx1nUuwwTdhwuPsp3zVLK-bmiJF29e8q_QCkT7BEYsXzA_li";
			for (String deviceName : registrationIdList) {
				URL url = null;
				try {
					url = new URL("https://fcm.googleapis.com/fcm/send");
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpURLConnection conn = null;
				try {
					conn = (HttpURLConnection) url.openConnection();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				conn.setDoOutput(true);
				try {
					conn.setRequestMethod("POST");
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				conn.setRequestProperty("Content-Type",
						"application/json; charset=UTF-8");
				conn.setRequestProperty("Authorization", "key=" + apiKey);
				conn.setDoOutput(true);
				conn.setDoInput(true);

				try {
					conn.setRequestMethod("POST");
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE,
							"ProtocolException ERROR::::::::::" + e);
					e.printStackTrace();
				}
				OutputStream os = null;
				try {
					os = conn.getOutputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
					e.printStackTrace();
				}
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os));
				// Log.d("Test", "From Get Post Method" + getPostData(values));
				try {
					writer.write(createJSONData(response, deviceName));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
					e.printStackTrace();
				}
				try {
					writer.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
					e.printStackTrace();
				}

				InputStream is = null;
				try {
					is = conn.getInputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
					e.printStackTrace();
				}
				BufferedReader br = new BufferedReader(
						new InputStreamReader(is));
				String temp;
				String data = null;
				try {
					while ((temp = br.readLine()) != null) {
						data = data + temp;
					}
					logger.log(Level.SEVERE, "data data::::::::::" + data);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
					e.printStackTrace();
				}

				logger.log(Level.SEVERE, "Data::::::::::" + data);
			
		}
	}

	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		JSONObject jObj1 = new JSONObject();
		jObj1.put("message", message);

		JSONObject jObj = new JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}

}
