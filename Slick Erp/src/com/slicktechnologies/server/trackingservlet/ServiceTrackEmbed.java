package com.slicktechnologies.server.trackingservlet;


import org.json.JSONArray;



public class ServiceTrackEmbed {

	static JSONArray jsonArray;
	private static ServiceTrackEmbed _instance;

	public synchronized static ServiceTrackEmbed getInstance() {
		if (_instance == null) {
			_instance = new ServiceTrackEmbed();
			jsonArray =new JSONArray();
		}
		return _instance;
	}
	
	public ServiceTrackEmbed() {
		// TODO Auto-generated constructor stub
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	
	
}
