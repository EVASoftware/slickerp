package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.NameService;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class ClientNameServiceImpl extends RemoteServiceServlet implements NameService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8217214210872456121L;

	@Override
	public String getClientName(String url) {
		Company company=ofy().load().type(Company.class).filter("accessUrl",url).first().now();
		Logger loger=Logger.getLogger("Komal");
		loger.log(Level.SEVERE,"Company Url "+url+"---   Company "+company);
		if(company==null)
		{
			return null;
					
		}
		
		else
			return company.getBusinessUnitName();
		
	}
	
	
	
	

}
