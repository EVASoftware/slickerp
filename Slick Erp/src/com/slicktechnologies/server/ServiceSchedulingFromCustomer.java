package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;

public class ServiceSchedulingFromCustomer extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8737183364817344483L;
	Logger logger = Logger.getLogger("ServiceSchedulingFromCustomer.class");
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy h:mm a");
	SimpleDateFormat fmt1 = new SimpleDateFormat("h:mm a");
	SimpleDateFormat fmt2 = new SimpleDateFormat("dd-MM-yyyy");
	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*");

		RegisterServiceImpl impl = new RegisterServiceImpl();
		String urlCalled = req.getRequestURL().toString().trim();
		String[] splitUrl = ServerAppUtility.getCompnayName(urlCalled);
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt2.setTimeZone(TimeZone.getTimeZone("IST"));
		
		try {
			logger.log(Level.SEVERE, "accessUrl " + splitUrl[0]);
			comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
			resp.setContentType("text/plain");
			
//			String customerId = req.getParameter("customerId").trim();
//			String serviceName = req.getParameter("serviceName").trim();
//			String serviceDate = req.getParameter("serviceDate").trim();
//			String customerName = req.getParameter("customerName").trim();
			
			String action =req.getParameter("action").trim();
			String serviceIdInString =req.getParameter("serviceId").trim();
			
			String rescheduleDateInString="";
			String rescheduleTime = "";
			
			String ratingStr="";
			int rating=-1;
			
			String rangStr="";
			int range=-1;
			
			try{
				rescheduleDateInString = req.getParameter("rescheduledDate").trim();
				rescheduleTime = req.getParameter("rescheduledTime").trim();
			}catch(Exception e){
				
			}
			try{
				ratingStr=req.getParameter("ratings").trim();
			}catch(Exception e){
				
			}
			
			try{
				rating=Integer.parseInt(ratingStr);
			}catch(Exception e){
				
			}
			
			try{
				rangStr=req.getParameter("range").trim();
			}catch(Exception e){
				
			}
			
			try{
				range=Integer.parseInt(rangStr);
			}catch(Exception e){
				
			}
			
			String feedbackremark="";
			try{
				feedbackremark=req.getParameter("remark").trim();
			}catch(Exception e){
				
			}
			
			String apiCallFrom="";
			try{
				apiCallFrom=req.getParameter("apiCallFrom").trim();
				logger.log(Level.SEVERE, "apiCallFrom ="+apiCallFrom);
			}catch(Exception e){
				
			}
			
//			String companyIdInString =  comp.getCompanyId() + "";
//			long companyId = Long.parseLong(companyIdInString);
			
			int serviceId=Integer.parseInt(serviceIdInString);
			
			logger.log(Level.SEVERE, "Service Id" + serviceIdInString+" || ReschduleDate "+rescheduleDateInString+" || ReschduleTime "+rescheduleTime+" || Action "+action);
			
			Service service=ofy().load().type(Service.class).filter("companyId", comp.getCompanyId()).filter("count", serviceId).first().now();
			
			if(service!=null){
				
				if(action.equals("Customer Support")){
					logger.log(Level.SEVERE, "rating " + rating+" / "+ratingStr+" / "+service.getStatus()+" | "+rangStr+" | "+range);
					
					if (rating > 0 && rating <= 3) {
						service.setCustomerFeedback("Poor");
					} else if (rating > 3 && rating <= 5) {
						service.setCustomerFeedback("Average");
					} else if (rating > 5 && rating <= 7) {
						service.setCustomerFeedback("Good");
					} else if (rating > 7 && rating <= 9) {
						service.setCustomerFeedback("Very Good");
					} else if (rating > 9 && rating <= 10) {
						service.setCustomerFeedback("Excellent");
					} else {
						service.setCustomerFeedback("");
					}
					
					/**
					 * @author Vijay Date :- 06-12-2022
					 * Des :- added to update feedback remark
					 */
					try {
						if(feedbackremark!=null && !feedbackremark.equals("")){
							service.setServiceCompleteRemark(feedbackremark);
						}
					} catch (Exception e) {
					}
				
					/**
					 * ends here
					 */
					
					if(rating==-1){
						return;
					}
					if(rating>range){
						GenricServiceImpl impl1=new GenricServiceImpl();
						impl1.save(service);
						return;
					}
					if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						if(apiCallFrom!=null&&!apiCallFrom.equals("")&&apiCallFrom.equals(AppConstants.CUSTOMERPORTAL)){//Ashwini Patil Date:18-06-2023
							logger.log(Level.SEVERE, "Customer portal is already requesting complain creation api so no need to invoke complain creation method");
							GenricServiceImpl impl1=new GenricServiceImpl();
							impl1.save(service);
							return;
						}
						else {
							createComplainBasedOnServiceFeedback(service,rating);
							return;
						}
					}
				}
				
				if(service.getStatus().equals(Service.SERVICESTATUSSCHEDULE)
						||service.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)
						||service.getStatus().equals(Service.SERVICESTATUSPENDING)){
					
					ModificationHistory history=new ModificationHistory();
					history.oldServiceDate=service.getServiceDate();
					try{
						history.resheduleDate=fmt.parse(rescheduleDateInString);
						history.resheduleTime=fmt1.format(fmt.parse(rescheduleTime));	
						
						logger.log(Level.SEVERE, " ReschduleDate "+history.resheduleDate+" || ReschduleTime "+history.resheduleTime);
					}catch(Exception e){
						
					}
					
					history.reason="";
					history.userName="By Customer";
					history.systemDate=new Date();
					service.getListHistory().add(history);
					
					service.setServiceTime(history.resheduleTime);
					service.setServiceDate(history.resheduleDate);
					try{
						service.setServiceDay(serviceDay(history.resheduleDate));
					}catch(Exception e){
						
					}
					service.setStatus(Service.SERVICESTATUSRESCHEDULE);
					service.setScheduledByClient(true);
					
					/**
					 * @author Anil @since 19-05-2021
					 * As per Nitin sir's instruction this service should also schedule for material
					 */
					if(service.getEmployee()!=null&&!service.getEmployee().equals("")){
						service.setServiceScheduled(true);
					}
					
					GenricServiceImpl genImpl=new GenricServiceImpl();
					genImpl.save(service);
					
					resp.getWriter().println("Service scheduled successfully.");
				}else {
					resp.getWriter().println("Can not update the service, as it is in "+service.getStatus()+" state.");
				}
			}else{
				resp.getWriter().println("No service found.");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "ERROR---->" + e);
		}
	}
	
	private void createComplainBasedOnServiceFeedback(Service service,int rating) {
		// TODO Auto-generated method stub
		
		logger.log(Level.SEVERE, "Inside complain creation !!");
		Complain complain=new Complain();
		
		complain.setCompanyId(service.getCompanyId());
		complain.setCompStatus("Created");
		complain.setDate(DateUtility.setTimeToMidOfDay( new Date()));
		complain.setComplainDate(DateUtility.setTimeToMidOfDay( new Date()));
		complain.setComplainTitle("Unsatisfactory rating "+rating+" against service "+service.getProductName()+"/"+service.getCount());
		
		complain.setPersoninfo(service.getPersonInfo());
		complain.setExistingServiceId(service.getCount());
		complain.setExistingServiceDate(service.getServiceDate());
		complain.setExistingContractId(service.getContractCount());
		complain.setServiceEngForExistingService(service.getEmployee());
		complain.setBranch(service.getBranch());
		complain.setSalesPerson(service.getEmployee());
		complain.setAssignto(service.getEmployee());
		
		ProductInfo prodInfo=new ProductInfo();
		prodInfo.setProdID(service.getProduct().getCount());
		prodInfo.setProductCode(service.getProduct().getProductCode());
		prodInfo.setProductName(service.getProduct().getProductName());
		prodInfo.setProductCategory(service.getProduct().getProductCategory());
		prodInfo.setProductPrice(service.getProduct().getPrice());
		complain.setPic(prodInfo);
		
		complain.setServiceDetails(service.getProduct().getProductName()+"/"+fmt2.format(service.getServiceDate())+"/"+service.getCount());
		complain.setServicetime("");
		complain.setRemark("Unsatisfactory rating "+rating+" against service "+service.getProductName()+"/"+service.getCount());
		
		GenricServiceImpl impl=new GenricServiceImpl();
		ReturnFromServer obj=impl.save(complain);
		logger.log(Level.SEVERE, "Ticket Id : "+obj.count +" Service Id : "+service.getCount());
		service.setTicketNumber(obj.count);
		impl.save(service);
		
		
		
	}

	public static String serviceDay(Date d){
		String day=null;
		SimpleDateFormat fmt = new SimpleDateFormat("c");
		String dayOfWeek = fmt.format(d);
		if(dayOfWeek.equals("0"))
		   return day="Sunday";
		if(dayOfWeek.equals("1"))
			return day="Monday";
		if(dayOfWeek.equals("2"))
			return day="Tuesday";
		if(dayOfWeek.equals("3"))
			return day="Wednesday";
		if(dayOfWeek.equals("4"))
			return day="Thursday";
		if(dayOfWeek.equals("5"))
			return day="Friday";
		if(dayOfWeek.equals("6"))
			return day="Saturday";
		
		return day;
	}
}
