package com.slicktechnologies.server.utility;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocprinting.EnglishNumberToWords;
import com.slicktechnologies.server.addhocprinting.ServiceInvoicePdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PdfUtility {
	/**
	 * @author Anil
	 * @since 24-06-2020
	 * @param phName-Cell Value
	 * @param font-font size
	 * @param horizontalAlignment
	 * @param border-default-1,for no border-0
	 * @param rowspan-default-1
	 * @param colspan-default-1
	 * @param fixedHeight-default-0
	 * @return
	 */
	Logger logger=Logger.getLogger("PdfUtility.class");
	public boolean printBankDetailsFlag=true;
	
	public boolean isPrintBankDetailsFlag() {
		return printBankDetailsFlag;
	}

	public void setPrintBankDetailsFlag(boolean printBankDetailsFlag) {
		this.printBankDetailsFlag = printBankDetailsFlag;
	}
	
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	
	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	
	Font font13 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
	
	Font nameAddressBoldFont=new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);//Ashwini Patil
	Font nameAddressFont=new Font(Font.FontFamily.HELVETICA, 7);//Ashwini Patil
	Font nameAddressFont6 = new Font(Font.FontFamily.HELVETICA, 7);
	

	public PdfPCell getCell(String phName,Font font,int horizontalAlignment,int rowspan,int colspan,int fixedHeight){
		Phrase phrase;
		if(phName != null) {
			phrase = new Phrase(phName,font);
		}else {
			phrase = new Phrase("",font);
		}
		PdfPCell cell=new PdfPCell(phrase);
		cell.setHorizontalAlignment(horizontalAlignment);
		if(fixedHeight!=0){
			cell.setFixedHeight(fixedHeight);
		}
		if(rowspan!=0){
			cell.setRowspan(rowspan);
		}if(colspan!=0){
			cell.setColspan(colspan);
		}
		return cell;
	}
	
	public PdfPCell getPhotoCell(DocumentUpload docUpload,float scalePercent,float fixedHeight,int rowspan,int colspan,float imageBorder){
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		PdfPCell imageCell = null;
		Image image = null;
		try {
			image = Image.getInstance(new URL(hostUrl+docUpload.getUrl()));
			image.scalePercent(scalePercent);
//			image.setScaleToFitLineWhenOverflow(true);
			image.setScaleToFitHeight(true);
			image.scaleAbsoluteWidth(100f);
			image.scaleAbsoluteHeight(100f);
			image.scalePercent(100f, 100f);
			image.scaleToFit(100f, 100f);
			
			if(imageBorder>0){
				image.setBorder(Rectangle.BOX);
				image.setBorderWidth(imageBorder);
			}
			
			imageCell = new PdfPCell(image,true);
			imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			imageCell.setPaddingTop(1);
			imageCell.setPaddingBottom(1);
			imageCell.setPaddingLeft(1);
			imageCell.setPaddingRight(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			try{
				/**
				 * In case image is uploaded from android app
				 */
				image = Image.getInstance(new URL(docUpload.getUrl()));
				image.scalePercent(scalePercent);
				image.setScaleToFitLineWhenOverflow(true);
				image.setScaleToFitHeight(true);
				image.scaleAbsoluteWidth(100f);
				image.scaleAbsoluteHeight(100f);
				image.scalePercent(100f, 100f);
				image.scaleToFit(100f, 100f);
				
				if(imageBorder>0){
					image.setBorder(Rectangle.BOX);
					image.setBorderWidth(imageBorder);
				}
				
				imageCell = new PdfPCell(image,true);
				imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				imageCell.setPaddingTop(1);
				imageCell.setPaddingBottom(1);
				imageCell.setPaddingLeft(1);
				imageCell.setPaddingRight(1);
				
			}catch(Exception e1){
				imageCell = new PdfPCell();
				imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				imageCell.setPaddingTop(2);
				imageCell.setPaddingBottom(2);
				imageCell.setPaddingLeft(2);
				imageCell.setPaddingRight(2);
			}
		}
		if(fixedHeight!=0){
			imageCell.setFixedHeight(fixedHeight);
		}
		if(rowspan!=0){
			imageCell.setRowspan(rowspan);
		}if(colspan!=0){
			imageCell.setColspan(colspan);
		}
		return imageCell;
	}
	
	
	public PdfPCell getAbsolutePhotoCell(DocumentUpload docUpload,float width,float height,int rowspan,int colspan,float imageBorder){
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		PdfPCell imageCell = null;
		Image image = null;
		try {
			image = Image.getInstance(new URL(hostUrl+docUpload.getUrl()));
//			image.scalePercent(scalePercent);
////			image.setScaleToFitLineWhenOverflow(true);
//			image.setScaleToFitHeight(true);
//			image.scaleAbsoluteWidth(100f);
//			image.scaleAbsoluteHeight(100f);
//			image.scalePercent(100f, 100f);
//			image.scaleToFit(100f, 100f);
			image.scaleAbsolute(width,height);
			
			
			if(imageBorder>0){
				image.setBorder(Rectangle.BOX);
				image.setBorderWidth(imageBorder);
			}
			
			imageCell = new PdfPCell(image,true);
			imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			imageCell.setPaddingTop(1);
			imageCell.setPaddingBottom(1);
			imageCell.setPaddingLeft(1);
			imageCell.setPaddingRight(1);
			
		} catch (Exception e) {
			e.printStackTrace();
			try{
				/**
				 * In case image is uploaded from android app
				 */
				image = Image.getInstance(new URL(docUpload.getUrl()));
//				image.scalePercent(scalePercent);
//				image.setScaleToFitLineWhenOverflow(true);
//				image.setScaleToFitHeight(true);
//				image.scaleAbsoluteWidth(100f);
//				image.scaleAbsoluteHeight(100f);
//				image.scalePercent(100f, 100f);
//				image.scaleToFit(100f, 100f);
				image.scaleAbsolute(width,height);
				
				
				if(imageBorder>0){
					image.setBorder(Rectangle.BOX);
					image.setBorderWidth(imageBorder);
				}
				
				imageCell = new PdfPCell(image,true);
				imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				imageCell.setPaddingTop(1);
				imageCell.setPaddingBottom(1);
				imageCell.setPaddingLeft(1);
				imageCell.setPaddingRight(1);
				
			}catch(Exception e1){
				imageCell = new PdfPCell();
				imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				imageCell.setPaddingTop(2);
				imageCell.setPaddingBottom(2);
				imageCell.setPaddingLeft(2);
				imageCell.setPaddingRight(2);
			}
		}
//		if(fixedHeight!=0){
//			imageCell.setFixedHeight(fixedHeight);
//		}
		if(rowspan!=0){
			imageCell.setRowspan(rowspan);
		}if(colspan!=0){
			imageCell.setColspan(colspan);
		}
		return imageCell;
	}
	
	public String getRoundOffValue(double value) {
		if(value != 0){
			DecimalFormat df = new DecimalFormat("##,##,##,##,##0.00");
			return df.format(value);
		}else{
			return "";
		}
	}
	
	/**
	 * @author Anil @since 01-10-2021
	 * @param invoice
	 * @param branch
	 * @param defaultInvoiceTitle
	 * @param printBankDetailsFlag
	 * 
	 * If we are printing non gst invoice then invoice title to be picked from number generation or branch
	 * requirement raised by Rahul Tiwari and Nitin Sir
	 * @return
	 */
	public String getInvoiceTitle(Invoice invoice,Branch branch,String defaultInvoiceTitle){
		logger.log(Level.SEVERE,"Inside getInvoiceTitle Method : "+defaultInvoiceTitle +" | "+printBankDetailsFlag);
		boolean taxInvoiceFlag=false;
		
		/**
		 * @author Ashwini Patil
		 * @since 18-02-2022
		 * added one condition in if "&&!invoice.getInvoiceType().equals("Proforma Invoice")"
		 * Nithila reported issue of Surat Pest Control that because number range in Proforma Invoice title was printing as "Tax Invoice"
		 */
		if(invoice!=null&&!invoice.getInvoiceType().equals("Proforma Invoice")){
			if(invoice.getBillingTaxes()!=null&&invoice.getBillingTaxes().size()!=0){
				taxInvoiceFlag=true;
			}
			if(taxInvoiceFlag){
				logger.log(Level.SEVERE,"Default TITLE : "+defaultInvoiceTitle +" | "+printBankDetailsFlag);
				if(invoice.getPaymentMode()!=null&&!invoice.getPaymentMode().equals("")){
					printBankDetailsFlag=true;
				}
				return defaultInvoiceTitle;
			}
			if(invoice.getNumberRange()!=null&&!invoice.getNumberRange().equals("")){
				logger.log(Level.SEVERE,"Number Range : "+invoice.getNumberRange());
				String numberRange=invoice.getNumberRange();
				
				Config ng = ofy().load().type(Config.class).filter("companyId",invoice.getCompanyId()).filter("name",numberRange).filter("type", 91).filter("status", true).first().now();
				if(ng!=null){
					if(ng.getInvoiceTitle()!=null&&!ng.getInvoiceTitle().equals("")){
						defaultInvoiceTitle=ng.getInvoiceTitle();
						printBankDetailsFlag=ng.isPrintBankDetails();
						logger.log(Level.SEVERE,"Number Range TITLE : "+defaultInvoiceTitle +" | "+printBankDetailsFlag);
						if(invoice.getPaymentMode()!=null&&!invoice.getPaymentMode().equals("")){
							printBankDetailsFlag=true;
						}
						return defaultInvoiceTitle;
					}
				}
			}
			
			if(branch!=null){
				if(branch.getInvoiceTitle()!=null&&!branch.getInvoiceTitle().equals("")){
					defaultInvoiceTitle=branch.getInvoiceTitle();
					printBankDetailsFlag=branch.isPrintBankDetails();
					logger.log(Level.SEVERE,"Branch TITLE : "+defaultInvoiceTitle +" | "+printBankDetailsFlag);
					if(invoice.getPaymentMode()!=null&&!invoice.getPaymentMode().equals("")){
						printBankDetailsFlag=true;
					}
					return defaultInvoiceTitle;
				}
			}
			
			
		}
		//Ashwini Patil Date:15-01-2025 If client is going to maintain invoices in zoho then it's illegal to maintain tax invoice in eva as well. So changing title of tax  invoice to proforma invoice as per nitin sir's instruction
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks", invoice.getCompanyId())) {
			defaultInvoiceTitle="Proforma Invoice";			
		}
		
		return defaultInvoiceTitle;
	}
	
	public void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void createCompanyNameAsFooter(Document doc, Company comp) {
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,10f); //40f	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*Added by Sheetal:29-11-2021*/
	public PdfPCell getBankDetails(CompanyPayment comppayment){
		/**
		 * @author Anil
		 * @since 02-02-2022
		 * For printing bank details on thai font as well
		 */
		try{
			BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			font8bold = new Font(boldFont, 8);
			font8 = new Font(regularFont, 8);
		}catch(Exception e){
			
		}

		if (comppayment != null) {

			PdfPTable bankDetailsTable = new PdfPTable(1);
			bankDetailsTable.setWidthPercentage(100f);

			/*
			 * nidhi
			 * 9-04-2018
			 * favour of company payment will print instand of company name
			 * 
			 * String favourOf = "";
			if (comppayment.getPaymentComName() != null
					&& !comppayment.getPaymentComName().equals("")) {
				favourOf = "Cheque should be in favour of '"
						+ comppayment.getPaymentComName() + "'";
			}*/
			
			
			String favourOf = "";
			if (comppayment.getPaymentFavouring() != null
					&& !comppayment.getPaymentFavouring().equals("")) {
				favourOf = "Cheque should be in favour of '"
						+ comppayment.getPaymentFavouring() + "'";
			}
			/**
			 *  end
			 */
			Phrase favouring = new Phrase(favourOf, font8bold);
			PdfPCell favouringCell = new PdfPCell(favouring);
			favouringCell.setBorder(0);
			bankDetailsTable.addCell(favouringCell);

			Phrase heading = new Phrase("Bank Details", font8bold);
			PdfPCell headingCell = new PdfPCell(heading);
			headingCell.setBorder(0);
			bankDetailsTable.addCell(headingCell);

			float[] columnWidths3 = { 1.7f, 0.35f, 4.5f };
			PdfPTable bankDetails3Table = new PdfPTable(3);
			bankDetails3Table.setWidthPercentage(100f);
			try {
				bankDetails3Table.setWidths(columnWidths3);
			} catch (DocumentException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			Phrase bankNamePh = new Phrase("Name", font8bold);
			PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
			bankNamePhCell.setBorder(0);
			bankDetails3Table.addCell(bankNamePhCell);

			Phrase dot = new Phrase(":", font8bold);
			PdfPCell dotCell = new PdfPCell(dot);
			dotCell.setBorder(0);
			bankDetails3Table.addCell(dotCell);

			String bankName = "";
			if (comppayment.getPaymentBankName() != null
					&& !comppayment.getPaymentBankName().equals("")) {
				bankName = comppayment.getPaymentBankName();
			}
			Phrase headingValue = new Phrase(bankName, font8);
			PdfPCell headingValueCell = new PdfPCell(headingValue);
			headingValueCell.setBorder(0);
			headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			bankDetails3Table.addCell(headingValueCell);

			// this is for branch
			Phrase bankBranch = new Phrase("Branch", font8bold);
			PdfPCell bankBranchCell = new PdfPCell(bankBranch);
			bankBranchCell.setBorder(0);
			bankDetails3Table.addCell(bankBranchCell);
			bankDetails3Table.addCell(dotCell);

			String bankBranchValue = "";
			if (comppayment.getPaymentBranch() != null
					&& !comppayment.getPaymentBranch().equals("")) {
				bankBranchValue = comppayment.getPaymentBranch();
			}
			Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
			PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
			bankBranchValuePhCell.setBorder(0);
			bankBranchValuePhCell
					.setHorizontalAlignment(Element.ALIGN_LEFT);
			bankDetails3Table.addCell(bankBranchValuePhCell);

			Phrase bankAc = new Phrase("A/c No", font8bold);
			PdfPCell bankAcCell = new PdfPCell(bankAc);
			bankAcCell.setBorder(0);
			bankDetails3Table.addCell(bankAcCell);
			bankDetails3Table.addCell(dotCell);

			String bankAcNo = "";
			if (comppayment.getPaymentAccountNo() != null
					&& !comppayment.getPaymentAccountNo().equals("")) {
				bankAcNo = comppayment.getPaymentAccountNo();
			}
			Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
			PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
			bankAcNoValueCell.setBorder(0);
			bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			bankDetails3Table.addCell(bankAcNoValueCell);

			String bankIFSCNo = "";
			if(comppayment.getPaymentIFSCcode() != null	&& !comppayment.getPaymentIFSCcode().equals("")){
				Phrase bankIFSC = new Phrase("IFSC", font8bold);
				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
				bankIFSCCell.setBorder(0);


				
				if (comppayment.getPaymentIFSCcode() != null
						&& !comppayment.getPaymentIFSCcode().equals("")) {
					bankIFSCNo = comppayment.getPaymentIFSCcode();
				}
				if(bankIFSCNo!=null &&!bankIFSCNo.equals("")){
				
					bankDetails3Table.addCell(bankIFSCCell);
					bankDetails3Table.addCell(dotCell);	
				}
				
			}
			else{
				Phrase bankIFSC = new Phrase("IBAN No", font8bold);
				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
				bankIFSCCell.setBorder(0);


				
				if (comppayment.getPaymentIBANNo() != null
						&& !comppayment.getPaymentIBANNo().equals("")) {
					bankIFSCNo = comppayment.getPaymentIBANNo();
				}
				if(bankIFSCNo!=null &&!bankIFSCNo.equals("")){				
					bankDetails3Table.addCell(bankIFSCCell);
					bankDetails3Table.addCell(dotCell);
				}
			}
			if(bankIFSCNo!=null &&!bankIFSCNo.equals("")){
				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
				bankIFSCNoValueCell.setBorder(0);
				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankIFSCNoValueCell);
			}
			
			if(comppayment.getPaymentMICRcode()!=null &&!comppayment.getPaymentMICRcode().equals("")){
				bankDetails3Table.addCell(getCell("MICR", font8bold, Element.ALIGN_LEFT, 0, 0, 0, 0));
				bankDetails3Table.addCell(dotCell);
				bankDetails3Table.addCell(getCell(comppayment.getPaymentMICRcode(), font8, Element.ALIGN_LEFT, 0, 0, 0, 0));					
			}
				
			if(comppayment.getPaymentSWIFTcode()!=null &&!comppayment.getPaymentSWIFTcode().equals("")){
				bankDetails3Table.addCell(getCell("SWIFT", font8bold, Element.ALIGN_LEFT, 0, 0, 0, 0));
				bankDetails3Table.addCell(dotCell);
				bankDetails3Table.addCell(getCell(comppayment.getPaymentSWIFTcode(), font8, Element.ALIGN_LEFT, 0, 0, 0, 0));					
			}
			
			if(comppayment.getArticleTypeDetails()!=null&&comppayment.getArticleTypeDetails().size()>0){
				for(ArticleType article:comppayment.getArticleTypeDetails()){
					bankDetails3Table.addCell(getCell(article.getArticleTypeName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0, 0));
					bankDetails3Table.addCell(dotCell);
					bankDetails3Table.addCell(getCell(article.getArticleTypeValue(), font8, Element.ALIGN_LEFT, 0, 0, 0, 0));					
				}
								
			}
			

			PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
			bankDetails3TableCell.setBorder(0);
			bankDetailsTable.addCell(bankDetails3TableCell);

			PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
			bankDetailsTableCell.setBorder(0);
	//		middletTable.addCell(bankDetailsTableCell);	
		  
			return bankDetailsTableCell;
		}
	
		return null;
		
	}
	
	/**
	 * @author Anil
	 * @since 18-01-2022
	 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
	 */
	public String removePinAsHeadingFromAddress(long companyId,String processName,String address){
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(processName, "PC_DO_NOT_PRINT_PIN_CODE", companyId)){
			address=address.replace("  Pin:"," ");
			address=address.replace("  PIN:"," ");
		}
		return address;
	}
	
	/**
	 * @author Ashwini Patil
	 * Since 20-01-2022
	 * This method will print branch annexure table
	 */
		public PdfPTable print_customer_branches(Customer cust,List<CustomerBranchDetails> branchlist,HashMap<String,ArrayList<String>> branchWiseProductMap,long companyId, int customer_id){
			
			Font font10bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
			Font font7 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
			Font font7bold=new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
			List<CustomerBranchDetails> selectedBranches=new ArrayList<CustomerBranchDetails>();
			PdfPTable branchAnnexureTable=new PdfPTable(7);
			if(branchlist!=null){
				System.out.println("in print method if branchlist!=null");
				selectedBranches=branchlist;
			}else if(customer_id!=0&&companyId!=0){
				System.out.println("in print method else of if branchlist!=null");
				selectedBranches=ofy().load().type(CustomerBranchDetails.class)
				.filter("companyId",companyId)
				.filter("cinfo.count",customer_id).list();			
			}
			if(selectedBranches!=null){	
				
				//Ashwini Patil Date:8-05-2024 Pepcopp wanted customer branch name sorted
				Comparator<CustomerBranchDetails> branchNameComp=new Comparator<CustomerBranchDetails>() {
					@Override
					public int compare(CustomerBranchDetails arg0, CustomerBranchDetails arg1) {
						return arg0.getBusinessUnitName().toUpperCase().compareTo(arg1.getBusinessUnitName().toUpperCase());
					}
				};
				Collections.sort(selectedBranches, branchNameComp);
				
				System.out.println("in if(selectedBranches!=null)");
				branchAnnexureTable.setWidthPercentage(100);
				float[] columnCollonWidth = { 0.7f,1.5f, 1.5f, 1.5f, 1.5f, 2.5f, 3.5f};
				try {
					branchAnnexureTable.setWidths(columnCollonWidth);
				} catch (DocumentException e) {
					e.printStackTrace();
				}	
				
				
			Paragraph ServiceAnnexureTitle = new Paragraph();
			ServiceAnnexureTitle.add("Service Branches Annexure");
			ServiceAnnexureTitle.setFont(font10bold);
			ServiceAnnexureTitle.setAlignment(Element.ALIGN_CENTER);
			ServiceAnnexureTitle.add(Chunk.NEWLINE);
			ServiceAnnexureTitle.add(Chunk.NEWLINE);
			
			PdfPCell annexCell = new PdfPCell();
			annexCell.addElement(ServiceAnnexureTitle);
			annexCell.setBorder(0);
			annexCell.setColspan(7);
			
			branchAnnexureTable.addCell(annexCell);
			
			String serviceLocation,contactPerson,mobile,email,address;
			int srno;
			Phrase titlephrase_srno=new Phrase("Sr. No.",font7bold);
			PdfPCell titlecell_srno=new PdfPCell(titlephrase_srno);
			titlecell_srno.setBorder(Rectangle.BOX);
			titlecell_srno.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase titlephrase_serviceLocation=new Phrase("Service Location",font7bold);
			PdfPCell titlecell_serviceLocation=new PdfPCell(titlephrase_serviceLocation);
			titlecell_serviceLocation.setBorder(Rectangle.BOX);
			titlecell_serviceLocation.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase titlephrase_services=new Phrase("Services",font7bold);
			PdfPCell titlecell_services=new PdfPCell(titlephrase_services);
			titlecell_services.setBorder(Rectangle.BOX);
			titlecell_services.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase titlephrase_contactPerson=new Phrase("Contact Person",font7bold);
			PdfPCell titlecell_contactPerson=new PdfPCell(titlephrase_contactPerson);
			titlecell_contactPerson.setBorder(Rectangle.BOX);
			titlecell_contactPerson.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase titlephrase_mobile=new Phrase("Mobile",font7bold);
			PdfPCell titlecell_mobile=new PdfPCell(titlephrase_mobile);
			titlecell_mobile.setBorder(Rectangle.BOX);
			titlecell_mobile.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase titlephrase_email=new Phrase("email",font7bold);
			PdfPCell titlecell_email=new PdfPCell(titlephrase_email);
			titlecell_email.setBorder(Rectangle.BOX);
			titlecell_email.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase titlephrase_address=new Phrase("Address",font7bold);
			PdfPCell titlecell_address=new PdfPCell(titlephrase_address);
			titlecell_address.setBorder(Rectangle.BOX);
			titlecell_address.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			branchAnnexureTable.addCell(titlecell_srno);
			branchAnnexureTable.addCell(titlecell_serviceLocation);
			branchAnnexureTable.addCell(titlecell_services);
			branchAnnexureTable.addCell(titlecell_contactPerson);
			branchAnnexureTable.addCell(titlecell_mobile);
			branchAnnexureTable.addCell(titlecell_email);
			branchAnnexureTable.addCell(titlecell_address);	
			int count=1;
			if(branchWiseProductMap.containsKey("Service Address"))
			{	count++;
			Phrase phrase_srno=new Phrase("1",font7);
			PdfPCell cell_srno=new PdfPCell(phrase_srno);
			cell_srno.setBorder(Rectangle.BOX);
			cell_srno.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase phrase_serviceLocation=new Phrase("Service Address",font7);
			PdfPCell cell_serviceLocation=new PdfPCell(phrase_serviceLocation);
			cell_serviceLocation.setBorder(Rectangle.BOX);
			cell_serviceLocation.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			ArrayList<String> parray=branchWiseProductMap.get("Service Address");
			String services="";
			if(parray!=null){
				for(String p:parray){
					services=services+p+"\n\n";
				}				
			}
			Phrase phrase_services=new Phrase(services,font7);
			PdfPCell cell_services=new PdfPCell(phrase_services);
			cell_services.setBorder(Rectangle.BOX);
			cell_services.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase phrase_contactPerson=null;
			if(cust!=null&&cust.getFullname()!=null)
				 phrase_contactPerson=new Phrase(cust.getFullname(),font7);
			else
				 phrase_contactPerson=new Phrase("",font7);
			PdfPCell cell_contactPerson=new PdfPCell(phrase_contactPerson);
			cell_contactPerson.setBorder(Rectangle.BOX);
			cell_contactPerson.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase phrase_mobile=null;
			if(cust!=null&&cust.getCellNumber1()!=null)
				 phrase_mobile=new Phrase(cust.getCellNumber1()+"",font7);
			else
				phrase_mobile=new Phrase("",font7);
			PdfPCell cell_mobile=new PdfPCell(phrase_mobile);
			cell_mobile.setBorder(Rectangle.BOX);
			cell_mobile.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase phrase_email=null;
			if(cust!=null&&cust.getEmail()!=null)
				phrase_email=new Phrase(cust.getEmail(),font7);
			else
				phrase_email=new Phrase("",font7);
			PdfPCell cell_email=new PdfPCell(phrase_email);
			cell_email.setBorder(Rectangle.BOX);
			cell_email.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase phrase_address=null;
			if(cust!=null)
				phrase_address=new Phrase(cust.getContacts().get(1).getAddress().getCompleteAddress(),font7);
			else
				phrase_address=new Phrase("",font7);
			PdfPCell cell_address=new PdfPCell(phrase_address);
			cell_address.setBorder(Rectangle.BOX);
			cell_address.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			branchAnnexureTable.addCell(cell_srno);
			branchAnnexureTable.addCell(cell_serviceLocation);
			branchAnnexureTable.addCell(cell_services);
			branchAnnexureTable.addCell(cell_contactPerson);
			branchAnnexureTable.addCell(cell_mobile);
			branchAnnexureTable.addCell(cell_email);
			branchAnnexureTable.addCell(cell_address);
				
			}

			for(int i=0;i<selectedBranches.size();i++){
				CustomerBranchDetails c=selectedBranches.get(i);
				srno=i+count;
				serviceLocation=c.getBusinessUnitName();
				contactPerson=c.getPocName();
				mobile=c.getCellNumber1().toString();
				email=c.getEmail();
				address=c.getAddress().getCompleteAddress();
				
				Phrase phrase_srno=new Phrase(srno+"",font7);
				PdfPCell cell_srno=new PdfPCell(phrase_srno);
				cell_srno.setBorder(Rectangle.BOX);
				cell_srno.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase phrase_serviceLocation=new Phrase(serviceLocation,font7);
				PdfPCell cell_serviceLocation=new PdfPCell(phrase_serviceLocation);
				cell_serviceLocation.setBorder(Rectangle.BOX);
				cell_serviceLocation.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				ArrayList<String> parray=branchWiseProductMap.get(serviceLocation);
				String services="";
				if(parray!=null){
					for(String p:parray){
						services=services+p+"\n\n";
					}				
				}
				Phrase phrase_services=new Phrase(services,font7);
				PdfPCell cell_services=new PdfPCell(phrase_services);
				cell_services.setBorder(Rectangle.BOX);
				cell_services.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase phrase_contactPerson=new Phrase(contactPerson,font7);
				PdfPCell cell_contactPerson=new PdfPCell(phrase_contactPerson);
				cell_contactPerson.setBorder(Rectangle.BOX);
				cell_contactPerson.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase phrase_mobile=new Phrase(mobile,font7);
				PdfPCell cell_mobile=new PdfPCell(phrase_mobile);
				cell_mobile.setBorder(Rectangle.BOX);
				cell_mobile.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase phrase_email=new Phrase(email,font7);
				PdfPCell cell_email=new PdfPCell(phrase_email);
				cell_email.setBorder(Rectangle.BOX);
				cell_email.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase phrase_address=new Phrase(address,font7);
				PdfPCell cell_address=new PdfPCell(phrase_address);
				cell_address.setBorder(Rectangle.BOX);
				cell_address.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				branchAnnexureTable.addCell(cell_srno);
				branchAnnexureTable.addCell(cell_serviceLocation);
				branchAnnexureTable.addCell(cell_services);
				branchAnnexureTable.addCell(cell_contactPerson);
				branchAnnexureTable.addCell(cell_mobile);
				branchAnnexureTable.addCell(cell_email);
				branchAnnexureTable.addCell(cell_address);		
			}
			}
		
			return branchAnnexureTable;
		}
		
		
		
		public PdfPCell getPdfCell(String phName,Font font,int horizontalAlignment,Integer verticalAlignment, int rowspan,
				int colspan,int fixedHeight,int border, int paddingTop, int borderWidthTop, int borderWidthBottom, 
				int borderWidthLeft, int borderWidthRight ){
			Phrase phrase;
			if(phName != null) {
				phrase = new Phrase(phName,font);
			}else {
				phrase = new Phrase("",font);
			}
			PdfPCell cell=new PdfPCell(phrase);
			cell.setHorizontalAlignment(horizontalAlignment);
			
			if(verticalAlignment!=null){
				cell.setVerticalAlignment(verticalAlignment);
			}
				
			if(fixedHeight!=0){
				cell.setFixedHeight(fixedHeight);
			}
			if(rowspan!=0){
				cell.setRowspan(rowspan);
			}
			if(colspan!=0){
				cell.setColspan(colspan);
			}
			
			if(border!=-1)
				cell.setBorder(border);
			
			if(borderWidthTop!=-1)
				cell.setBorderWidthTop(borderWidthTop);
			
			if(borderWidthBottom!=-1)
				cell.setBorderWidthBottom(borderWidthBottom);
			
			if(borderWidthLeft!=-1)
				cell.setBorderWidthLeft(borderWidthLeft);
			
			if(borderWidthRight!=-1)
				cell.setBorderWidthRight(borderWidthRight);
			
			if(paddingTop!=0){
				cell.setPaddingTop(paddingTop);
			}
			
			return cell;
		}
		
		/**
		 * @author Vijay Date :- 08-03-2022
		 * Des :- This method for logo with header
		 */
		public PdfPTable createHeader(Company comp, boolean checkheaderLeft, boolean checkheaderRight, boolean checkEmailId,
				boolean UniversalFlag, String contractGroupName, String branchName, boolean hoEmail, String contractNumberRange,
				boolean nonBillingFlag) {

			DocumentUpload logodocument = comp.getLogo();

			String hostUrl;
			String environment = System
					.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
				String applicationId = System
						.getProperty("com.google.appengine.application.id");
				String version = System
						.getProperty("com.google.appengine.application.version");
				hostUrl = "http://" + version + "." + applicationId
						+ ".appspot.com/";
			} else {
				hostUrl = "http://localhost:8888";
			}
			PdfPCell imageSignCell = null;
			Image image2 = null;
			try {
				image2 = Image
						.getInstance(new URL(hostUrl + logodocument.getUrl()));
				image2.scalePercent(20f);
				// image2.setAbsolutePosition(40f,765f);
				// doc.add(image2);

				imageSignCell = new PdfPCell(image2);
				imageSignCell.setBorder(0);
				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				imageSignCell.setFixedHeight(40);
			} catch (Exception e) {
				e.printStackTrace();
			}
			/**
			 * Ends For Jayshree
			 */

			/**
			 * Developer : Jayshree Date : 21 Nov 2017 Description : Logo position
			 * adjustment
			 */
			PdfPTable logoTab = new PdfPTable(1);
			logoTab.setWidthPercentage(100);

			if (imageSignCell != null) {
				logoTab.addCell(imageSignCell);
			} else {
				Phrase logoblank = new Phrase(" ");
				PdfPCell logoblankcell = new PdfPCell(logoblank);
				logoblankcell.setBorder(0);
				logoTab.addCell(logoblankcell);
			}
			/**
			 * Ends for Jayshree
			 */
			Phrase companyName = null;
			if (comp != null) {
				companyName = new Phrase(comp.getBusinessUnitName().trim(),
						font16bold);
			}

			Paragraph companyNamepara = new Paragraph();
			companyNamepara.add(companyName);
			/**
			 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
			 * alignment
			 */

			if (checkheaderLeft == true) {
				companyNamepara.setAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				companyNamepara.setAlignment(Element.ALIGN_RIGHT);
			} else {
				companyNamepara.setAlignment(Element.ALIGN_CENTER);
			}

			// companyNamepara.setAlignment(Element.ALIGN_CENTER);

			PdfPCell companyNameCell = new PdfPCell();
			companyNameCell.addElement(companyNamepara);
			companyNameCell.setBorder(0);
			companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase companyAddr = null;
			if (comp != null) {
				companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
						.trim(), font12);
			}
			Paragraph companyAddrpara = new Paragraph();
			companyAddrpara.add(companyAddr);
			/**
			 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
			 * alignment
			 */

			if (checkheaderLeft == true) {
				companyAddrpara.setAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				companyAddrpara.setAlignment(Element.ALIGN_RIGHT);
			} else {
				companyAddrpara.setAlignment(Element.ALIGN_CENTER);
			}

			// companyAddrpara.setAlignment(Element.ALIGN_CENTER);
			/**
			 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
			 * alignment
			 */
			PdfPCell companyAddrCell = new PdfPCell(companyAddrpara);
			// companyAddrCell.addElement();
			companyAddrCell.setBorder(0);
			if (checkheaderLeft == true) {
				companyAddrCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				companyAddrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			} else {
				companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			/**
			 * Ends
			 */

			// companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase companyGSTTIN = null;
			String gstinValue = "";
			if (UniversalFlag) {
				if (contractGroupName.equalsIgnoreCase(
						"Universal Pest Control Pvt. Ltd.")) {

					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
						if (comp.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
							gstinValue = comp.getArticleTypeDetails().get(i)
									.getArticleTypeName()
									+ " : "
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeValue().trim();
							break;
						}
					}

					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
						if (!comp.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
							gstinValue = gstinValue
									+ ","
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeName()
									+ " : "
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeValue().trim();
						}
					}
				}
			} else {

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
							.equalsIgnoreCase("GSTIN")) {
						gstinValue = comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
						break;
					}
				}

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
							.equalsIgnoreCase("GSTIN")) {
						gstinValue = gstinValue
								+ ","
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
					}
				}
			}

			if (!gstinValue.equals("")) {
				companyGSTTIN = new Phrase(gstinValue, font12bold);
			}

			Paragraph companyGSTTINpara = new Paragraph();
			companyGSTTINpara.add(companyGSTTIN);
			companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

			PdfPCell companyGSTTINCell = new PdfPCell();
			companyGSTTINCell.addElement(companyGSTTINpara);
			companyGSTTINCell.setBorder(0);
			companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			/**
			 * Date 12/1/2018 Dev.Jayshree Des.To add the company Email And Branch
			 * Email
			 */
			String branchmail = "";
			ServerAppUtility serverApp = new ServerAppUtility();

			if (checkEmailId == true) {
				branchmail = serverApp.getBranchEmail(comp,	branchName);
				System.out.println("server method " + branchmail);

			} else {
				branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
				System.out.println("server method 22" + branchmail);
			}

			String email = null;
			if (branchmail != null) {
				email = "Email : " + branchmail;
			} else {
				email = "Email : ";
			}

			/**
			 * By Jayshree Date 28/12/2017 to handle null condition
			 */

			String website = "";
			if (comp.getWebsite() == null || comp.getWebsite().equals("")) {
				website = " ";
			} else {

				website = "Website : " + comp.getWebsite();
			}
			/**
			 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
			 * add the LandLine no.
			 */

			
			/**
			 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
			 * add the LandLine no.
			 */
			String number = "";
			String landline = "";

			if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
				System.out.println("pn11");
				number = comp.getCountryCode()+comp.getCellNumber1() + "";
			}
			if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
				if (!number.trim().isEmpty()) {
					number = number + " , " +comp.getCountryCode()+comp.getCellNumber2() + "";
				} else {
					number = comp.getCountryCode()+comp.getCellNumber2() + "";
				}
				System.out.println("pn33" + number);
			}
			if (comp.getLandline() != 0 && comp.getLandline() != null) {
				if (!number.trim().isEmpty()) {
					number = number + " , " + comp.getStateCode()+comp.getLandline() + "";
				} else {
					number = comp.getStateCode()+comp.getLandline() + "";
				}
				System.out.println("pn44" + number);
			}


			
			/**
			 * Date 31-3-2018
			 * By jayshree
			 * Des.to print the head off.email
			 */
			String hoid=null;
			if(hoEmail==true){
				hoid="HO Email : "+comp.getEmail();
			}
			
			Phrase hoemail=new Phrase(hoid,font11);
			PdfPCell hocell=new PdfPCell(hoemail);
			hocell.setBorder(0);
			if (checkheaderLeft == true) {
				hocell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				hocell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			} else {
				hocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			
			
			Phrase branchmailph=new Phrase(email,font11);
			PdfPCell branchmailcell=new PdfPCell(branchmailph);
			branchmailcell.setBorder(0);
			if (checkheaderLeft == true) {
				branchmailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				branchmailcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			} else {
				branchmailcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			
			
			
			PdfPCell companyphoneCell = null;
			if (number != null && !number.trim().isEmpty()) {
				companyphoneCell = new PdfPCell(new Phrase( "Phone " + number, font11));
			} else {
				companyphoneCell = new PdfPCell(new Phrase("", font11));
			}
			companyphoneCell.setBorder(0);
			if (checkheaderLeft == true) {
				companyphoneCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				companyphoneCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			} else {
				companyphoneCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			
			PdfPCell companyweb = new PdfPCell(new Phrase("" + website, font11));
			companyweb.setBorder(0);
			
			if (checkheaderLeft == true) {
				companyweb.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else if (checkheaderRight == true) {
				companyweb.setHorizontalAlignment(Element.ALIGN_RIGHT);
			} else {
				companyweb.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			

			PdfPTable pdfPTable = new PdfPTable(1);
			// pdfPTable.setWidthPercentage(100);
			pdfPTable.addCell(companyNameCell);
			pdfPTable.addCell(companyAddrCell);
			pdfPTable.addCell(branchmailcell);
			if(hoEmail==true){
				pdfPTable.addCell(hocell);
			}
			pdfPTable.addCell(companyphoneCell);
			pdfPTable.addCell(companyweb);

			//End By jayshree 31-3-2018
			
			/**
			 * Developer:Jayshree Date 21/11/2017 Description:changes are done to
			 * add the logo and website at proper position
			 */
			PdfPTable mainheader = new PdfPTable(2);
			mainheader.setWidthPercentage(100);

			try {
				mainheader.setWidths(new float[] { 20, 80 });
			} catch (DocumentException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			

			if(contractNumberRange!=null &&!contractNumberRange.equals("")){
			if(nonBillingFlag==true &&contractNumberRange.equalsIgnoreCase("NonBilling")){
				Phrase blank =new Phrase(" ");
				PdfPCell blankcell = new PdfPCell(blank);
				blankcell.setBorder(0);
				blankcell.setColspan(2);
				mainheader.addCell(blankcell);
			}
			else{
				if (imageSignCell != null) {
				PdfPCell leftCell = new PdfPCell(logoTab);
				leftCell.setBorder(0);
				mainheader.addCell(leftCell);

				PdfPCell rightCell = new PdfPCell(pdfPTable);
				rightCell.setBorder(0);
				mainheader.addCell(rightCell);
			} else {
				PdfPCell rightCell = new PdfPCell(pdfPTable);
				rightCell.setBorder(0);
				rightCell.setColspan(2);
				mainheader.addCell(rightCell);
			
					}
				}
			}
			else{
				if (imageSignCell != null) {
				PdfPCell leftCell = new PdfPCell(logoTab);
				leftCell.setBorder(0);
				mainheader.addCell(leftCell);

				PdfPCell rightCell = new PdfPCell(pdfPTable);
				rightCell.setBorder(0);
				mainheader.addCell(rightCell);
			} else {
				PdfPCell rightCell = new PdfPCell(pdfPTable);
				rightCell.setBorder(0);
				rightCell.setColspan(2);
				mainheader.addCell(rightCell);
			
			}
			
			}
			
			return mainheader;
		
		}

		public String getCustomerCompanyName(CustomerBranchDetails customerBranch, Customer customer,boolean serviceAddressFlag) {
			String customerName = "";
			boolean isSociety=false;
			
			//Ashwini Patil Date:5-08-2024 If the customer is society then do not print M/S against it as per ultima requirement
			if((customer!=null&&customer.getCategory()!=null&&customer.getCategory().equalsIgnoreCase("Society"))||(customer!=null&&customer.getType()!=null&&customer.getType().equalsIgnoreCase("Society")))
				isSociety=true;
			if(serviceAddressFlag) {
				if(customerBranch!=null){
					if(customerBranch.getServiceAddressName()!=null&&!customerBranch.getServiceAddressName().equals(""))
						customerName = customerBranch.getServiceAddressName();
					else
						customerName = customerBranch.getBusinessUnitName();
				}else {
					if(customer.getServiceAddressName()!=null&&!customer.getServiceAddressName().equals("")) {
						customerName=customer.getServiceAddressName();
					}else if (customer.isCompany() == true && customer.getCompanyName() != null) {
						if(isSociety||ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, customer.getCompanyId())){
								customerName = customer.getCompanyName().trim();
						}
						else{
							customerName = "M/S " + " " + customer.getCompanyName().trim();
						}
					}else if (customer.getSalutation() != null
							&& !customer.getSalutation().equals("")) {
						customerName = customer.getSalutation() + " "
								+ customer.getFullname().trim();
					} else {
						customerName = customer.getFullname().trim();
					}
				}
			}else {
				String tosir="";
			
				if (customer.getCustPrintableName() != null
						&& !customer.getCustPrintableName().equals("")) {
					customerName = customer.getCustPrintableName().trim();
				} 
				else {
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "ChangeCompanyNameAndFullNameLable", customer.getCompanyId())&&((customer.isCompany() == true)||customer.isCompany() == false))
					{    
						tosir=customer.getSalutation();						
						if (customer.isCompany() == true && customer.getCompanyName() != null) {							 
							customerName = customer.getCompanyName().trim();
						} else								
						{    
							customerName =  customer.getFullname().trim();
						}
					}		
					else {
						if (customer.isCompany() == true && customer.getCompanyName() != null) {
							if(isSociety||ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, customer.getCompanyId())){								
								customerName = customer.getCompanyName().trim();
							}
							else{
								customerName = "M/S " + " " + customer.getCompanyName().trim();
							}
						} else if (customer.getSalutation() != null
								&& !customer.getSalutation().equals("")) {
							customerName = customer.getSalutation() + " "
									+ customer.getFullname().trim();
						} else {
							customerName = customer.getFullname().trim();
						}
					}
				}
				if (customer!=null) {
					
					if (customer.getCustPrintableName() == null	|| customer.getCustPrintableName().equals("")) {
						customerName =tosir + " " + customerName;
					}				
				}

			}
			
			//old code commented by Ashwini Patil Date:03-03-2023 as old contract and invoice prints have different logic to print customer name
//			if(customerBranch!=null){
//				customerName = customerBranch.getBusinessUnitName();
//			}
//			else{
//				
//				if(customer.isCompany()){
//					if(customer.getCustPrintableName()!=null && !customer.getCustPrintableName().equals("")){
//							customerName = customer.getCustPrintableName();
//					}
//					else{
//						if(customer.getSalutation()!=null && !customer.getSalutation().equals("")){
//							customerName = customer.getSalutation() +" "+ customer.getCompanyName();
//						}
//						else{
//							customerName = customer.getCompanyName();
//						}
//					}
//				}
//				else{
//					if(customer.getSalutation()!=null && !customer.getSalutation().equals("")){
//						customerName = customer.getSalutation() +" "+customer.getFullname();
//					}
//					else{
//						customerName = customer.getFullname();
//					}
//				}
//			}
			return customerName;
		}

		public String getCustomerAtten(CustomerBranchDetails customerBranch, Customer customer) {
			if(customerBranch!=null){
				if(customerBranch.getPocName()!=null && !customerBranch.getPocName().equals("")){
					return customerBranch.getPocName();
				}
			}
			else{
				
				if(customer.isCompany() && customer.getCustPrintableName()!=null && !customer.getCustPrintableName().equals("")){
					return customer.getCustPrintableName();
				}
				else{
					return "";
				}
				
			}
			return "";
		}

		public String getAddress(CustomerBranchDetails customerBranch, Customer customer, boolean addresstouppercaseFlag, Contract con,
				boolean billingAddressFlag, boolean serviceAddressFlag) {
		
			if(billingAddressFlag){
				if(customerBranch!=null){
					if(addresstouppercaseFlag)
						return customerBranch.getBillingAddress().getCompleteAddress().toUpperCase();
					else
						return customerBranch.getBillingAddress().getCompleteAddress();
				}
				else if(con!=null&&con.getNewcustomerAddress()!=null){
					
					if(addresstouppercaseFlag)
						return con.getNewcustomerAddress().getCompleteAddress().toUpperCase();
					else
						return con.getNewcustomerAddress().getCompleteAddress();
				}
				else{
					if(addresstouppercaseFlag)
						return customer.getAdress().getCompleteAddress().toUpperCase();
					else
						return customer.getAdress().getCompleteAddress();
				}
				
			}
			else if(serviceAddressFlag){

				if(customerBranch!=null){
					if(addresstouppercaseFlag)
						return customerBranch.getAddress().getCompleteAddress().toUpperCase();
					else
						return customerBranch.getAddress().getCompleteAddress();
				}
				else if(con!=null && con.getCustomerServiceAddress()!=null){
					if(addresstouppercaseFlag)
						return con.getCustomerServiceAddress().getCompleteAddress().toUpperCase();
					else
						return con.getCustomerServiceAddress().getCompleteAddress();
				}
				else{
					if(addresstouppercaseFlag)
						return customer.getSecondaryAdress().getCompleteAddress().toUpperCase();
					else
						return customer.getSecondaryAdress().getCompleteAddress();
				}
			
			}
			else{
				return "";
			}
		}

		public String getCustomerEmail(CustomerBranchDetails customerBranch, Customer customer) {

			if(customerBranch!=null && customerBranch.getEmail()!=null && !customerBranch.getEmail().equals("")){
				return customerBranch.getEmail();
			}
			else{
				if(customer.getEmail()!=null && !customer.getEmail().equals("")){
					return customer.getEmail();
				}
			}
			return "";
		}
		
		
		public String getBillingAddressCountryName(CustomerBranchDetails customerBranch, Customer customer,
				Contract con, boolean billingAddressFlag, boolean serviceAddressFlag) {
			
			if(billingAddressFlag){
				
				if(customerBranch!=null){
					return customerBranch.getBillingAddress().getCountry();
				}
				else if(con!=null && con.getNewcustomerAddress()!=null){
						return con.getNewcustomerAddress().getCountry();
				}
				else{
						return customer.getAdress().getCountry();
				}
				
			}
			else if(serviceAddressFlag){
				if(customerBranch!=null){
					return customerBranch.getAddress().getCountry();
				}
				else if(con!=null && con.getCustomerServiceAddress()!=null){
						return con.getCustomerServiceAddress().getCountry();
				}
				else{
						return customer.getSecondaryAdress().getCountry();
				}
			}
			else{
				return "";
			}
			
		}
		
		public String getServiceAddressCountryName(CustomerBranchDetails customerBranch, Customer customer, Contract con) {
			
			if(customerBranch!=null){
					return customerBranch.getAddress().getCountry();
			}
			else if(con!=null){
					return con.getCustomerServiceAddress().getCountry();
			}
			else{
					return customer.getSecondaryAdress().getCountry();
			}
		}

		public String getPhoneNumber(CustomerBranchDetails customerBranch, Customer customer, String countryName,long companyId) {
			
			String phoneNo = "";
			ServerAppUtility serverappUtility = new ServerAppUtility();
			
			if(customerBranch!=null){
				
				if(customerBranch.getCellNumber1()!=null && customerBranch.getCellNumber1()!=0){
					phoneNo=customerBranch.getCellNumber1()+"";
					/**
					 * @author Vijay Date :- 22-09-2021
					 * Des :- Adding country code(from country master) in cell number.
					 */
					phoneNo = serverappUtility.getMobileNoWithCountryCode(phoneNo, countryName, companyId);
				
				}
				if(customerBranch.getCellNumber2()!=null && customerBranch.getCellNumber2()!=0)
				{
					if(!phoneNo.trim().isEmpty())
						{
						String cellNumber2 = customerBranch.getCellNumber2()+"";
						phoneNo = serverappUtility.getMobileNoWithCountryCode(phoneNo, countryName, companyId);
						
						phoneNo=phoneNo+" / "+cellNumber2+"";

						}
					else{
						phoneNo=customerBranch.getCellNumber2()+"";
						phoneNo = serverappUtility.getMobileNoWithCountryCode(phoneNo, countryName,companyId);

						}
				}
				if(customerBranch.getLandline()!=0 && customerBranch.getLandline()!=null)
				{
					if(!phoneNo.trim().isEmpty()){
						phoneNo=phoneNo+" / "+customerBranch.getLandline()+"";
					}
					else{
						phoneNo=customerBranch.getLandline()+"";
					}
				}

				
			}
			else{

				if(customer.getCellNumber1()!=null && customer.getCellNumber1()!=0){
					phoneNo=customer.getCellNumber1()+"";
					phoneNo = serverappUtility.getMobileNoWithCountryCode(phoneNo, countryName, companyId);
				
				}
				if(customer.getCellNumber2()!=null && customer.getCellNumber2()!=0)
				{
					if(!phoneNo.trim().isEmpty())
						{
						String cellNumber2 = customer.getCellNumber2()+"";
						phoneNo = serverappUtility.getMobileNoWithCountryCode(phoneNo, countryName, companyId);
						
						phoneNo=phoneNo+" / "+cellNumber2+"";

					}
					else{
						phoneNo=customer.getCellNumber2()+"";
						phoneNo = serverappUtility.getMobileNoWithCountryCode(phoneNo, countryName, companyId);

					}
				}
				if(customer.getLandline()!=0 && customer.getLandline()!=null)
				{
					if(!phoneNo.trim().isEmpty()){
						phoneNo=phoneNo+" / "+customer.getLandline()+"";
					}
					else{
						phoneNo=customer.getLandline()+"";
					}
				}

			
			}
			return phoneNo;
		}

		public PdfPTable getAddressTable(CustomerBranchDetails customerBranch, Customer cust,
				Contract con, PdfPTable colonTable, PdfUtility pdfUtility, boolean adresstouppercase, 
				boolean addcellNoFlag, long companyId, String pocName, String documentName,
				boolean billingAddressFlag, boolean serviceAddressFlag,boolean gstNumberPrintFlagAsPerNumberRange) {
			
			
			boolean doNotPrintGSTNOServiceAddress=false;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","DONOTPRINTGSTNUMBERINSERVICEADDRESS", cust.getCompanyId())){
				doNotPrintGSTNOServiceAddress=true;
			}
			//logger.log(Level.SEVERE,"doNotPrintGSTNOServiceAddress="+doNotPrintGSTNOServiceAddress+" serviceAddressFlag="+serviceAddressFlag+" documentName="+documentName);
			
			String customerName = pdfUtility.getCustomerCompanyName(customerBranch,cust,serviceAddressFlag);
			String customerBranchName="";
			if(customerBranch!=null){
				customerBranchName = customerBranch.getBusinessUnitName();
			}
			String	Atten = pdfUtility.getCustomerAtten(customerBranch,cust);
			String	address = pdfUtility.getAddress(customerBranch,cust,adresstouppercase,con,billingAddressFlag,serviceAddressFlag);
			String email = pdfUtility.getCustomerEmail(customerBranch,cust);
			
			/**
			 * @author Anil
			 * @since 18-01-2022
			 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
			 */
			address=pdfUtility.removePinAsHeadingFromAddress(companyId, "Invoice", address);
			
			colonTable.addCell(pdfUtility.getPdfCell("Name", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
			colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
			colonTable.addCell(pdfUtility.getPdfCell(customerName, nameAddressBoldFont, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
			
			//Ashwini Patil Date:1-07-2024 Gingerbay want customer branch name to get printed in billing address after customer name. Nitin sir told to make it standard for all
			if(billingAddressFlag&&customerBranchName!=null&&!customerBranchName.equals("")) {
				colonTable.addCell(pdfUtility.getPdfCell("Branch", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(customerBranchName, nameAddressFont, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
				
			}
			
			if(!Atten.equals("")){
				colonTable.addCell(pdfUtility.getPdfCell("Attn", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(Atten, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			}
			colonTable.addCell(pdfUtility.getPdfCell("Address", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
			colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			colonTable.addCell(pdfUtility.getPdfCell(address, nameAddressFont, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
			if(!email.equals("")){
				colonTable.addCell(pdfUtility.getPdfCell("Email", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(email, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			}

			String countryName=pdfUtility.getBillingAddressCountryName(customerBranch, cust, con,billingAddressFlag,serviceAddressFlag);
			String phoneNumber = pdfUtility.getPhoneNumber(customerBranch,cust,countryName,companyId);
			
			if(addcellNoFlag && !phoneNumber.equals("")){
				phoneNumber += " ("+pocName+ ")";
			}
			if(!phoneNumber.equals("")){
				colonTable.addCell(pdfUtility.getPdfCell("Phone", font10bold, Element.ALIGN_LEFT, null,0, 0, 0,0,0,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null,0, 0, 0,0,0,-1,-1,-1,-1));
				colonTable.addCell(pdfUtility.getPdfCell(phoneNumber, font10, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
			}
		
			
			if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
				for(ArticleType type:cust.getArticleTypeDetails()){
					if((type.getDocumentName().equalsIgnoreCase(documentName)&&type.getArticlePrint().equalsIgnoreCase("Yes")) ||
							(type.getDocumentName().equalsIgnoreCase(documentName)&&type.getArticlePrint().equalsIgnoreCase("Yes"))){
						if(type.getArticleTypeValue()!=null && !type.getArticleTypeValue().equals("")){
							
							
							//Ashwini Patil Date:03-08-2023 CSAT requirement is not to print GSTNO in service address. Managing this through process config.
							if(documentName.equals("Invoice Details")&&serviceAddressFlag&&doNotPrintGSTNOServiceAddress){
								logger.log(Level.SEVERE,"in if of article");
								if(!type.getArticleTypeName().equalsIgnoreCase("GSTIN")){
									colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeName(), font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
									colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
									colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeValue(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));		
								}								
							}else{
								logger.log(Level.SEVERE,"in else of article name="+type.getArticleTypeName());
								/*
								 * Ashwini Patil
								 * Date:15-01-2023
								 * Pecopp reported that invoice print is showing customer GST number in both billing and service address even though there is different GST number in customer branch.
								 * Modifying program to pick GST number from customer branch first and if it is not there then it will be picked from customer
								 */
								if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")&&customerBranch!=null&&customerBranch.getGSTINNumber()!=null&&!customerBranch.getGSTINNumber().equals("")){
									logger.log(Level.SEVERE,"in customer branch condition");
									if(gstNumberPrintFlagAsPerNumberRange) {
										colonTable.addCell(pdfUtility.getPdfCell("GSTIN", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
										colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
										colonTable.addCell(pdfUtility.getPdfCell(customerBranch.getGSTINNumber(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
									}
								}else if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")){
									
									if(gstNumberPrintFlagAsPerNumberRange) {
										colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeName(), font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
										colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
										colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeValue(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
									}
								}else {								
									colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeName(), font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
									colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
									colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeValue(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
								}
								}
						}
											
					}
				}
			}
			
			
			return colonTable;
		}
		
		public String checkTableSizeAndAddBlanks(PdfPTable productTable,int colsPan,double height) {

			Phrase blankPhrase = new Phrase(" ", font10bold);
			PdfPCell blank = new PdfPCell(blankPhrase);
			blank.setBorderWidthTop(0);
			blank.setBorderWidthBottom(0);
			if(colsPan!=-1)
			blank.setColspan(colsPan);			
			
			if(productTable.getTotalHeight()<height){
				System.out.println("prod table height"+productTable.getTotalHeight());
				productTable.addCell(blank);
				checkTableSizeAndAddBlanks(productTable,colsPan,height);
			}
			else{
				return "";
			}
			return "";
		}
		
		/**
		 * @author Ashwini Patil
		 * @since 14-04-2022
		 * Amount in words was not printing decimal part. Also different countries have different currency.
		 * This standard method will convert number value in words as per flags and customer currency.
		 */
		public String getAmountInWords(double num,Long companyId,Customer cust){
			logger.log(Level.SEVERE,"getAmountInWords() called. Num="+num+" CompanyId="+companyId);
			String amtInWords="";
			boolean PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag=false;
			boolean AmountInWordsHundreadFormatFlag=false;
			Config custCurrency=null;
			
			if (companyId != null) {
			if(cust!=null){
				logger.log(Level.SEVERE,"Customer country in address="+cust.getAdress().getCountry());
				Country custCountry=ofy().load().type(Country.class)
						.filter("countryName", cust.getAdress().getCountry())
						.filter("companyId", companyId).first().now();
				if(custCountry==null)
						custCountry=ofy().load().type(Country.class)
						.filter("countryName", "India")
						.filter("companyId", companyId).first().now();
				if(custCountry!=null&&custCountry.getCurrency()!=null){
					logger.log(Level.SEVERE,"Customer country="+custCountry.getCountryName());
					custCurrency= ofy().load().type(Config.class)
						.filter("type", 18)
						.filter("name", custCountry.getCurrency())
						.filter("companyId", companyId).first().now();
					if(custCurrency!=null)
						logger.log(Level.SEVERE,"Customer Currency="+custCurrency.getName());
				}
			}
			ServerAppUtility serverAppUtility = new ServerAppUtility();
			PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag = serverAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT, companyId);
			AmountInWordsHundreadFormatFlag=serverAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY,AppConstants.PC_AMOUNTINWORDSHUNDREADSTRUCTURE,companyId);
			}
			
			if(PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag)
			{logger.log(Level.SEVERE, "in if(PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag) amtinwords");
				DecimalFormat decimalformat = new DecimalFormat("0.00");
				
				logger.log(Level.SEVERE,"num="+num);
				
				String number=decimalformat.format(num);
				logger.log(Level.SEVERE,"number="+number);
				StringTokenizer st = new StringTokenizer(number, ".");
			    String beforeDot = st.nextToken();
			    String afterDot = (st.hasMoreTokens()) ? st.nextToken() : null; 
			    logger.log(Level.SEVERE,"beforeDot="+beforeDot);
			    logger.log(Level.SEVERE,"afterDot="+afterDot);
			    double rs=Double.parseDouble(beforeDot);
			    double ps=Double.parseDouble(afterDot);
			    logger.log(Level.SEVERE,"paise="+ps);
				if(AmountInWordsHundreadFormatFlag){
					String rupee=EnglishNumberToWords.convert(rs);
					String paise=EnglishNumberToWords.convert(ps);
					if(ps>0) {
						if(custCurrency!=null){
							amtInWords=rupee+" "+custCurrency.getWholeAmtLabel()+" and "+paise+" "+custCurrency.getDecimalLabel()+" only";
						}
						else
							amtInWords=rupee+" Rupees and "+paise+" Paise"+" only";
					
					}else {
						if(custCurrency!=null){
							amtInWords=rupee+" "+custCurrency.getWholeAmtLabel()+" only";
						}
						else
							amtInWords=rupee+" Rupees only";
					}
				
					logger.log(Level.SEVERE,"New Amount="+amtInWords);
				}
				else{
					String rupee=ServiceInvoicePdf.convert(rs);
					String paise=ServiceInvoicePdf.convert(ps);
					if(ps>0) {
						if(custCurrency!=null){
							amtInWords=rupee+" "+custCurrency.getWholeAmtLabel()+" and "+paise+" "+custCurrency.getDecimalLabel()+" only";
						}
						else
							amtInWords=rupee+" Rupees and "+paise+" Paise"+" only";
					
					}else {
						if(custCurrency!=null){
							amtInWords=rupee+" "+custCurrency.getWholeAmtLabel()+" only";
						}
						else
							amtInWords=rupee+" Rupees only";
						
					}
					logger.log(Level.SEVERE,"New Amount ServiceInvoicePdf="+amtInWords);		
				}
			}else{
				int np = (int)num;
				if(custCurrency!=null) {
					if(AmountInWordsHundreadFormatFlag){
						amtInWords = EnglishNumberToWords.convert(np)+" "+custCurrency.getWholeAmtLabel()+" only";
					}
					else{
						amtInWords = ServiceInvoicePdf.convert(np)+" "+custCurrency.getWholeAmtLabel()+" only";
					}
					
				}else {
					if(AmountInWordsHundreadFormatFlag){
						amtInWords = EnglishNumberToWords.convert(np)+" Rupees"+" only";
					}
					else{
						amtInWords = ServiceInvoicePdf.convert(np)+" Rupees"+" only";
					}
				}			
			}
			return amtInWords;
		}
		
		
		public PdfPCell getCell(String phName,Font font,int horizontalAlignment,int rowspan,int colspan,int fixedHeight,int border){
			Phrase phrase;
			if(phName != null) {
				phrase = new Phrase(phName,font);
			}else {
				phrase = new Phrase("",font);
			}
			PdfPCell cell=new PdfPCell(phrase);
			cell.setHorizontalAlignment(horizontalAlignment);
			if(fixedHeight!=0){
				cell.setFixedHeight(fixedHeight);
			}
			if(rowspan!=0){
				cell.setRowspan(rowspan);
			}if(colspan!=0){
				cell.setColspan(colspan);
			}
			if(border!= -1){
				cell.setBorder(border);
			}
			
			return cell;
		}
		
		
		
		public PdfPCell getPdfCell(String phName,Font font,int horizontalAlignment,Integer verticalAlignment, int rowspan,
				int colspan,int fixedHeight,int border, int borderWidthTop, int borderWidthBottom, 
				int borderWidthLeft, int borderWidthRight,int paddingTop, int paddingbottom, int paddingleft, int paddingright ){
			Phrase phrase;
			if(phName != null) {
				phrase = new Phrase(phName,font);
			}else {
				phrase = new Phrase("",font);
			}
			PdfPCell cell=new PdfPCell(phrase);
			cell.setHorizontalAlignment(horizontalAlignment);
			
			if(verticalAlignment!=null){
				cell.setVerticalAlignment(verticalAlignment);
			}
				
			if(fixedHeight!=0){
				cell.setFixedHeight(fixedHeight);
			}
			if(rowspan!=0){
				cell.setRowspan(rowspan);
			}
			if(colspan!=0){
				cell.setColspan(colspan);
			}
			
			if(border!=-1)
				cell.setBorder(border);
			
			if(borderWidthTop!=-1)
				cell.setBorderWidthTop(borderWidthTop);
			
			if(borderWidthBottom!=-1)
				cell.setBorderWidthBottom(borderWidthBottom);
			
			if(borderWidthLeft!=-1)
				cell.setBorderWidthLeft(borderWidthLeft);
			
			if(borderWidthRight!=-1)
				cell.setBorderWidthRight(borderWidthRight);
			
			if(paddingTop!=0){
				cell.setPaddingTop(paddingTop);
			}
			if(paddingbottom!=0){
				cell.setPaddingBottom(paddingbottom);
			}
			
			if(paddingleft!=0){
				cell.setPaddingLeft(paddingleft);
			}
			if(paddingright!=0){
				cell.setPaddingRight(paddingright);
			}
			
			return cell;
		}
		
		
		public PdfPCell getPdfCell(String phName,Font font,int horizontalAlignment,Integer verticalAlignment, int rowspan,
				int colspan,int fixedHeight,int border, int paddingTop, float borderWidthTop, float borderWidthBottom, 
				float borderWidthLeft, float borderWidthRight ){
			Phrase phrase;
			if(phName != null) {
				phrase = new Phrase(phName,font);
			}else {
				phrase = new Phrase("",font);
			}
			PdfPCell cell=new PdfPCell(phrase);
			cell.setHorizontalAlignment(horizontalAlignment);
			
			if(verticalAlignment!=null){
				cell.setVerticalAlignment(verticalAlignment);
			}
				
			if(fixedHeight!=0){
				cell.setFixedHeight(fixedHeight);
			}
			if(rowspan!=0){
				cell.setRowspan(rowspan);
			}
			if(colspan!=0){
				cell.setColspan(colspan);
			}
			
			if(border!=-1)
				cell.setBorder(border);
			
			if(borderWidthTop!=-1)
				cell.setBorderWidthTop(borderWidthTop);
			
			if(borderWidthBottom!=-1)
				cell.setBorderWidthBottom(borderWidthBottom);
			
			if(borderWidthLeft!=-1)
				cell.setBorderWidthLeft(borderWidthLeft);
			
			if(borderWidthRight!=-1)
				cell.setBorderWidthRight(borderWidthRight);
			
			if(paddingTop!=0){
				cell.setPaddingTop(paddingTop);
			}
			
			return cell;
		}
		
		public String adddBlanks(PdfPTable productTable,int colsPan, int noOfBlanklines) {

			Phrase blankPhrase = new Phrase(" ", font10bold);
			PdfPCell blank = new PdfPCell(blankPhrase);
			blank.setBorderWidthTop(0);
			blank.setBorderWidthBottom(0);
			if(colsPan!=-1)
			blank.setColspan(colsPan);			
			System.out.println("prod table height"+productTable.getTotalHeight());
			
			for(int i=0 ;i <noOfBlanklines;i++){
				productTable.addCell(blank);
			}
			return "";
			
		}
		
		
		public ArrayList<Paragraph> addCompanyIntroduction(String stringvalue, Font boldFont, Font normalFont) {
				try {
					ArrayList<Paragraph> paraList=new ArrayList<Paragraph>();
					String companyIntroduction = stringvalue;
					if(companyIntroduction!=null && !companyIntroduction.equals("")) {
						
						if(companyIntroduction.contains("<pagebreak>")){
							String [] introductionPara = companyIntroduction.split("<pagebreak>");
							for(int p=0;p<introductionPara.length;p++){

								String [] introduction = introductionPara[p].split("<b>");
								Phrase alldata = new Phrase();

								for(int i=0;i<introduction.length;i++){
									
									if(introduction[i].contains("</b>")){
										String [] strbolddata = introduction[i].split("</b>");
										
										Chunk chk2 = new Chunk(strbolddata[0], boldFont);
										Chunk chk3 = new Chunk(strbolddata[1], normalFont);

										alldata.add(chk2);
										alldata.add(chk3);

									}
									else{
										Chunk chk1 = new Chunk(introduction[i], normalFont);
										alldata.add(chk1);

									}
									
								}
								Paragraph para2 = new Paragraph(11);//Ashwini Patil Date:24-04-2023 added parameter for line spacing
								para2.add(alldata);
								
								paraList.add(para2);
							}							
							
						}else{
							String [] introduction = companyIntroduction.split("<b>");
							Phrase alldata = new Phrase();

							for(int i=0;i<introduction.length;i++){
								
								if(introduction[i].contains("</b>")){
									String [] strbolddata = introduction[i].split("</b>");
									
									Chunk chk2 = new Chunk(strbolddata[0], boldFont);
									Chunk chk3 = new Chunk(strbolddata[1], normalFont);

									alldata.add(chk2);
									alldata.add(chk3);

								}
								else{
									Chunk chk1 = new Chunk(introduction[i], normalFont);
									alldata.add(chk1);

								}
								
							}
							Paragraph para2 = new Paragraph(12);//Ashwini Patil Date:24-04-2023 added parameter for line spacing
							para2.add(alldata);
							
							paraList.add(para2);
						
						
						}
						return paraList;
						
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
				
				
		}
		
		//Ashwini Patil Date:16-08-2023
		public PdfPTable getIRNTable(Invoice invoiceentity) {
			PdfPTable IRNtable = new PdfPTable(2);
			IRNtable.setWidthPercentage(100f);
			float[] tblcol6width = {8f,2f};
			try {
				IRNtable.setWidths(tblcol6width);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			String irn="",ackNo="",ackDate="";
			if(invoiceentity.getIRN()!=null)
				irn=invoiceentity.getIRN();
			if(invoiceentity.getIrnAckNo()!=null)
				ackNo=invoiceentity.getIrnAckNo();
			if(invoiceentity.getIrnAckDate()!=null&&!invoiceentity.getIrnAckDate().equals("")) {
				 String sDate=invoiceentity.getIrnAckDate().substring(0, 10);
				 String year=invoiceentity.getIrnAckDate().substring(0, 4);
				 String month=invoiceentity.getIrnAckDate().substring(5, 7);
				 String day=invoiceentity.getIrnAckDate().substring(8, 10);
				 ackDate=day+"-"+month+"-"+year;
			}
			
			PdfPCell qrCodeCell = null;
			if(invoiceentity.getIrnQrCode()!=null&&!invoiceentity.getIrnQrCode().equals("")) {
				BarcodeQRCode barcodeQRCode = new BarcodeQRCode(invoiceentity.getIrnQrCode(), 1000, 1000, null);
			     
				Image codeQrImage=null;
					try {
						codeQrImage = barcodeQRCode.getImage();
					} catch (BadElementException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			        codeQrImage.scaleAbsolute(30, 30);    			
				
				
				qrCodeCell = new PdfPCell(codeQrImage);
				qrCodeCell.setBorder(0);
				qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				qrCodeCell.setVerticalAlignment(Element.ALIGN_TOP);
				qrCodeCell.setRowspan(2);
			
			}


			
			
			
			IRNtable.addCell(getCell("IRN: "+irn, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			if(qrCodeCell!=null)
				IRNtable.addCell(qrCodeCell);
			else
				IRNtable.addCell(getCell("", font8bold, Element.ALIGN_CENTER, 2, 0, 0)).setBorder(0);				
			IRNtable.addCell(getCell("Acknowledgement No and Date: "+ackNo+" / "+ackDate, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			return IRNtable;
		}
		
}
