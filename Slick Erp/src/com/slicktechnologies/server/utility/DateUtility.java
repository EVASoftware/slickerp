package com.slicktechnologies.server.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DateUtility {
    
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	 Logger logger = Logger.getLogger("NameOfYourLogger");

public static Calendar getCalendarForDate(Date date) {
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(date);
    return calendar;
}

private static void setTimeToBeginningOfDay(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
}

private static void setTimeToEndofDay(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);
}

public static Date getStartDateofMonth(Date date)
{
	Calendar calendar = getCalendarForDate(date);
    calendar.set(Calendar.DAY_OF_MONTH,
            calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
    setTimeToBeginningOfDay(calendar);
    return calendar.getTime();
}

public static Date getEndDateofMonth(Date date)
{
	 Calendar calendar = getCalendarForDate(date);
     calendar.set(Calendar.DAY_OF_MONTH,
             calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
     setTimeToEndofDay(calendar);
     return calendar.getTime();
	
}

public static Date getDateWithTimeZone(String timeZone,Date date)
{
	 SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	 isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
	    try {
		  date = isoFormat.parse(isoFormat.format(date));
		  } catch (ParseException e) {
			e.printStackTrace();
		}
	    
	    return date;
}


public static String getTimeWithTimeZone(String timeZone,Date date)
{
	 SimpleDateFormat isoFormat = new SimpleDateFormat("HH:mm:ss");
	 String timeInTimeZone="";
	 isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
	    try {
		  date = isoFormat.parse(isoFormat.format(date));
		  timeInTimeZone=isoFormat.format(date);
		  } catch (ParseException e) {
			e.printStackTrace();
		}
	    
	    return timeInTimeZone;
}

/**
 * Date : 18-04-2018 BY ANIL
 */
public static Date addDaysToDate(Date date,int days)
{
	Calendar calendar = getCalendarForDate(date);
	calendar.setTime(date); 
	calendar.add(Calendar.DATE, days);
    return calendar.getTime();
}

/** date 06.08.2018 added by komal**/
public static int getDaysInMonth(Date date){
	 Calendar calendar = getCalendarForDate(date);
	 calendar.add(Calendar.DATE, 1);
	 return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
}

/** date 06.08.2018 added by komal**/
public static int getMonth(Date date){
	 int month = 0;
	 Calendar calendar = getCalendarForDate(date);
	 month = calendar.get(Calendar.MONTH);
	 return month;   
}

/**
 * @author Abhinav Bihade
 * @since 25/02/2020
 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
 * 
 */

public static Date setTimeToMidOfDay(Date date) {
	Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(date);
    calendar.setTimeZone(TimeZone.getTimeZone("IST"));
    
    calendar.set(Calendar.HOUR_OF_DAY, 12);
    calendar.set(Calendar.MINUTE, 00);
    calendar.set(Calendar.SECOND, 00);
    calendar.set(Calendar.MILLISECOND, 000);
    
    return calendar.getTime();
}

public static String getTimeWithTimeZone1(String timeZone,Date date)
{
	 SimpleDateFormat isoFormat = new SimpleDateFormat("HH:mm");
	 String timeInTimeZone="";
	 isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
	    try {
		  date = isoFormat.parse(isoFormat.format(date));
		  timeInTimeZone=isoFormat.format(date);
		  } catch (ParseException e) {
			e.printStackTrace();
		}
	    
	    return timeInTimeZone;
}

public static String convert24Hrsto12HrsFormat(String timeIn24){
	String timeIn12 = null;
	SimpleDateFormat fmt24Hrs=new SimpleDateFormat("HH:mm");
	SimpleDateFormat fmt12Hrs=new SimpleDateFormat("hh:mm a");
	fmt24Hrs.setTimeZone(TimeZone.getTimeZone("IST"));
	fmt12Hrs.setTimeZone(TimeZone.getTimeZone("IST"));
	try {
		timeIn12=fmt12Hrs.format(fmt24Hrs.parse(timeIn24));
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	timeIn12 = timeIn12.replaceAll("\\s","");
	return timeIn12;
}

public static String convert12Hrsto24HrsFormat(String timeIn12){
	
	if(!timeIn12.contains(" ")){
        if(timeIn12.contains("AM")){
        	timeIn12=timeIn12.replace("AM"," AM");
        }else{
        	timeIn12=timeIn12.replace("PM"," PM"); 
        }
        System.out.println(timeIn12); 
	}
	String timeIn24 = null;
	SimpleDateFormat fmt24Hrs=new SimpleDateFormat("HH:mm");
	SimpleDateFormat fmt12Hrs=new SimpleDateFormat("hh:mm a");
	fmt24Hrs.setTimeZone(TimeZone.getTimeZone("IST"));
	fmt12Hrs.setTimeZone(TimeZone.getTimeZone("IST"));
	try {
		timeIn24=fmt24Hrs.format(fmt12Hrs.parse(timeIn12));
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	if(timeIn24!=null){
		timeIn24 = timeIn24.replaceAll("\\s","");
	}
	return timeIn24;
}

public static int getNumberOfDaysBetweenDates(Date fromDate,Date toDate){
	int noOfDays=0;
	
	SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
	String dateFromString = myFormat.format(fromDate);
	String dateToString = myFormat.format(toDate);

	try {
		Date dateFrom = myFormat.parse(dateFromString);
		Date dateTo = myFormat.parse(dateToString);
		long difference =dateFrom.getTime()-dateTo.getTime();
		noOfDays = (int) (difference / (1000 * 60 * 60 * 24));
		
		System.out.println("Number of Days between dates: " + noOfDays);
	} catch (Exception e) {
		e.printStackTrace();
	}
			
	return noOfDays;		
}

/**
 * @author Vijay Chougule Date 19-06-2020
 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for the Google studio report
 */
public Date setTimeMidOftheDayToDate(Date date) {
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
	
	Calendar cal3=Calendar.getInstance();
	cal3.setTime(date);
	cal3.add(Calendar.DATE, 0);
	
	Date DatewithMidTimeOfTheDay=null;
	
	try {
		DatewithMidTimeOfTheDay=dateFormat.parse(dateFormat.format(cal3.getTime()));
		cal3.set(Calendar.HOUR_OF_DAY,13);
		cal3.set(Calendar.MINUTE,00);
		cal3.set(Calendar.SECOND,00);
		cal3.set(Calendar.MILLISECOND,000);
		DatewithMidTimeOfTheDay=cal3.getTime();
	} catch (ParseException e) {
		e.printStackTrace();
	}
	logger.log(Level.SEVERE, "Date after time set "+DatewithMidTimeOfTheDay);
    
    return DatewithMidTimeOfTheDay;
}
/**
 * ends here
 */


}