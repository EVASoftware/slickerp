package com.slicktechnologies.server.utility;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.mindrot.jbcrypt.BCrypt;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.api.GetUserRegistrationOtp;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveValidityInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

import java.io.FileNotFoundException;

/**
 * 
 * @author Rahul Verma Date : 31 Aug 2017
 * 
 */
public class ServerAppUtility {
	static Logger logger = Logger.getLogger("apputility");
	
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");

	public DocumentUpload getLogoOfCompany(Company company, String companyBranchName){ 
		DocumentUpload logo=null;
		Branch branch = ofy().load().type(Branch.class)
				.filter("companyId", company.getCompanyId())
				.filter("buisnessUnitName", companyBranchName).first()
				.now();
		if (branch != null&&!branch.equals("")) {
			logger.log(Level.SEVERE, "Branch Name "+branch);
			if (branch.getLogo() != null&&!branch.getLogo().equals("")) {
				logo = branch.getLogo();
				logger.log(Level.SEVERE, "inside branch if condition server app utility");
			} else {
				logo =null;
				logger.log(Level.SEVERE, "inside branch elsee condition server app utility");
			}
		}
		
		if(logo==null){
			if(company!=null&&company.getLogo()!=null){
				logo=company.getLogo();
				logger.log(Level.SEVERE, "inside Company if condition server app utility");
			}else{
				logo =null;
				logger.log(Level.SEVERE, "inside Company elseee condition server app utility");
			}
		}
		return logo;
	}

	public String getInvoicePrefix(Company company, String companyBranchName,
			Invoice invoice) {
		String invoicePrefix = "";
		if (companyBranchName != null && !companyBranchName.equals("")) {
			
			Branch branch = ofy().load().type(Branch.class)
					.filter("companyId", company.getCompanyId())
					.filter("buisnessUnitName", companyBranchName).first()
					.now();
			if (branch != null) {
				logger.log(Level.SEVERE, "inside  branch"+invoicePrefix);
				if (branch.getInvoicePrefix() != null) {
					invoicePrefix = branch.getInvoicePrefix();
				} else {
					invoicePrefix = "";
				}
			}
			if (invoicePrefix.equals("")) {
				logger.log(Level.SEVERE, "inside company"+invoicePrefix);
				if (company.getInvoicePrefix() != null) {
					invoicePrefix = company.getInvoicePrefix();
				}
			}
			logger.log(Level.SEVERE, "invoicePrefix no in apputility"+invoicePrefix);
			return invoicePrefix;
		} else {
			if (invoice.getCount() != 0) {
				invoicePrefix = invoice.getCount() + "";
			}
			logger.log(Level.SEVERE, "invoicePrefix no in apputility elsee "+invoicePrefix);
			return invoicePrefix;
		}

	}
	public String getGSTINOfCompany(Company company, String companyBranchName) {
		String gstinValue = "";
		if (companyBranchName != null && !companyBranchName.equals("")) {
			Branch branch = ofy().load().type(Branch.class)
					.filter("companyId", company.getCompanyId())
					.filter("buisnessUnitName", companyBranchName).first()
					.now();
			if (branch != null) {
				if (branch.getGSTINNumber() != null) {
					gstinValue = branch.getGSTINNumber();
				} else {
					gstinValue = "";
				}
			}
			if (gstinValue.equals("")) {
				if (company.getCompanyGSTType().equalsIgnoreCase(
						"GST Applicable")
						&& company.getCompanyGSTTypeText() != null) {
					if (company.getCompanyGSTTypeText() != null) {
						gstinValue = company.getCompanyGSTTypeText();
					}

				}
				if (gstinValue.equalsIgnoreCase("")) {
					for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {
						if (company.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
							gstinValue = company.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
							break;
						}
					}
				}

			}
			return gstinValue;
		} else {
			for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {
				if (company.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = company.getArticleTypeDetails().get(i)
							.getArticleTypeValue().trim();
					break;
				}
			}
			return gstinValue;
		}
	}

	/**
	 * Date 1/2/2018 By Jayshree 
	 * des.Add parameter document type
	 */
	
	public String getGSTINOfCustomer(Customer customer,
			String customerBranchName,String documentType) {
		String gstTinStr = "";
		if (customerBranchName != null && !customerBranchName.equals("")) {
			System.out.println("Inside if");
			CustomerBranchDetails branch = ofy().load()
					.type(CustomerBranchDetails.class)
					.filter("companyId", customer.getCompanyId())
					.filter("buisnessUnitName", customerBranchName.trim())
					.first().now();
			if (branch != null) {
				
				if (branch.getGSTINNumber() != null) {
					gstTinStr = branch.getGSTINNumber();
					System.out.println("Inside branch"+gstTinStr);
					logger.log(Level.SEVERE, "Customer branch inside server Apputility "+gstTinStr);
				}
			}
			if (gstTinStr.equals("")) {
				for (int i = 0; i < customer.getArticleTypeDetails().size(); i++) {
					if (customer.getArticleTypeDetails().get(i).getArticlePrint()
							.equalsIgnoreCase("YES")
							&& customer.getArticleTypeDetails().get(i).getDocumentName()
									.equals(documentType.trim())){
						System.out.println("Inside article type");
					if (customer.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						System.out.println("Inside article info");
						gstTinStr = customer.getArticleTypeDetails().get(i)
								.getArticleTypeValue().trim();
						logger.log(Level.SEVERE, "Customer master article type "+gstTinStr);
						break;
					}
					}
				}
			}
			return gstTinStr;
		} else if(customer!=null&&customer.getArticleTypeDetails()!=null){
			for (int i = 0; i < customer.getArticleTypeDetails().size(); i++) {
				if (customer.getArticleTypeDetails().get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& customer.getArticleTypeDetails().get(i).getDocumentName()
								.equals(documentType.trim())){
					System.out.println("Inside else");
				if (customer.getArticleTypeDetails().get(i)
						.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
					System.out.println("inside second else");
					gstTinStr = customer.getArticleTypeDetails().get(i)
							.getArticleTypeValue().trim();
					break;
				}
				}
			}
			return gstTinStr;
		}
		return gstTinStr;
	}

	
	
	public String getStateOfCompany(Company company, String companyBranchName,
			List<State> listOfSates) {

		String stateCode = "";
		if (companyBranchName != null && !companyBranchName.equals("")) {
			Branch branch = ofy().load().type(Branch.class)
					.filter("companyId", company.getCompanyId())
					.filter("buisnessUnitName", companyBranchName).first()
					.now();
			if (branch != null) {
				for (int i = 0; i < listOfSates.size(); i++) {
					if (listOfSates
							.get(i)
							.getStateName()
							.trim()
							.equalsIgnoreCase(
									branch.getAddress().getState().trim())) {
						stateCode = listOfSates.get(i).getStateCode().trim();
						break;
					}
				}
			}
			return stateCode;
		} else {
			for (int i = 0; i < listOfSates.size(); i++) {
				if (listOfSates
						.get(i)
						.getStateName()
						.trim()
						.equalsIgnoreCase(
								company.getAddress().getState().trim())) {
					stateCode = listOfSates.get(i).getStateCode().trim();
					break;
				}
			}
			return stateCode;
		}
	}

	/**
	 * Developed By : Nidhi Joshi Date : 4-12-2017
	 * 
	 * Purpose : This is global method use to load process confog
	 * 
	 * @return
	 */
	public static boolean checkForProcessConfigurartionIsActiveOrNot(
			String processName, String processType, Long companyId) {

		List<ProcessConfiguration> processList = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName", processName).list();
		int cnt = 0;
		for (int k = 0; k < processList.size(); k++) {
			if (processList.get(k).getProcessName().trim()
					.equalsIgnoreCase(processName)) {

				for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
					if (processList.get(k).getProcessList().get(i)
							.getProcessType().equalsIgnoreCase(processType)
							&& processList.get(k).getProcessList().get(i)
									.isStatus() == true) {
						cnt = cnt + 1;
					}
				}

			}
		}

		if (cnt > 0) {
			return true;
		}

		else {
			return false;
		}

	}

	public static boolean checkServiceTypeAvailableOrNot(String category,
			Long companyId, String approvalName) {
		boolean flag = false;
		try {
			List<Config> serviceType = ofy().load().type(Config.class)
					.filter("companyId", companyId).filter("type", 78).list();

			for (Config conf : serviceType) {
				if (conf.getName().trim().equalsIgnoreCase(category)) {
					flag = true;
					break;
				}

			}

			if (!flag && !category.trim().equals("") && category != null) {

				Config type = new Config();
				type.setCompanyId(companyId);
				type.setCreatedBy(approvalName);
				type.setName(category);
				type.setDescription("Saved Throught process configration.");
				type.setUserId(approvalName);
				type.setType(78);
				type.setStatus(true);
				GenricServiceImpl imp = new GenricServiceImpl();
				imp.save(type);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" GEt error in --" + e);
		}
		return flag;
	}

	public static boolean checkContractTypeAvailableOrNot(String type,
			String category, Long companyId, String approvalName,
			int internalType) {
		boolean flag = false;
		try {
			List<Type> serviceType = ofy().load().type(Type.class)
					.filter("companyId", companyId)
					.filter("internalType", internalType).list();

			for (Type conf : serviceType) {
				if (conf.getTypeName().trim().equalsIgnoreCase(type)
						&& conf.getCategoryName().equalsIgnoreCase(category)) {
					flag = true;
					break;
				}

			}

			if (!flag && !category.trim().equals("") && category != null
					&& type != null && !type.trim().equals("")) {

				Type billingtype = new Type();
				billingtype.setCompanyId(companyId);
				billingtype.setCreatedBy(approvalName);
				billingtype.setCategoryName(category);
				billingtype
						.setDescription("Saved Throught process configration.");
				billingtype.setUserId(approvalName);
				billingtype.setInternalType(internalType);
				billingtype.setTypeName(type);
				billingtype.setStatus(true);
				GenricServiceImpl imp = new GenricServiceImpl();
				imp.save(billingtype);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" GEt error in --" + e);

		}
		return flag;
	}

	public static boolean checkContractCategoryAvailableOrNot(String category,
			Long companyId, String approvalName, int internalType) {
		boolean flag = false;
		try {
			List<ConfigCategory> serviceType = ofy().load()
					.type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", internalType).list();

			for (ConfigCategory conf : serviceType) {
				if (conf.getCategoryName().trim().equalsIgnoreCase(category)) {
					flag = true;
					break;
				}

			}

			if (!flag && !category.trim().equals("") && category != null) {

				ConfigCategory billingtype = new ConfigCategory();
				billingtype.setCompanyId(companyId);
				billingtype.setCreatedBy(approvalName);
				billingtype.setCategoryName(category);
				billingtype
						.setDescription("Saved Throught process configration.");
				billingtype.setUserId(approvalName);
				billingtype.setInternalType(internalType);
				billingtype.setStatus(true);
				GenricServiceImpl imp = new GenricServiceImpl();
				imp.save(billingtype);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" GEt error in --" + e);
		}
		return flag;
	}

	public static boolean checkServiceTypeAvailableOrNot(String category,
			Long companyId, String approvalName, int internalType) {
		boolean flag = false;
		try {
			List<Config> serviceType = ofy().load().type(Config.class)
					.filter("companyId", companyId)
					.filter("type", internalType).list();

			for (Config conf : serviceType) {
				if (conf.getName().trim().equalsIgnoreCase(category)) {
					flag = true;
					break;
				}

			}

			if (!flag && !category.trim().equals("") && category != null) {

				Config type = new Config();
				type.setCompanyId(companyId);
				type.setCreatedBy(approvalName);
				type.setName(category);
				type.setDescription("Saved Throught process configration.");
				type.setUserId(approvalName);
				type.setType(internalType);
				type.setStatus(true);
				GenricServiceImpl imp = new GenricServiceImpl();
				imp.save(type);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" GEt error in --" + e);
		}
		return flag;
	}

	/**
	 * Date 12/1/2018 Dev.By jayshree Des.add the new methods which returns the
	 * company and branch email ids
	 */

	public String getCompanyEmail(Long companyid) {

		String companyMail = "";
		Company company = null;
		if (companyid != null) {
			company = ofy().load().type(Company.class)
					.filter("companyId", companyid).first().now();
	   /**
	    * Added BY Priyanka - 08-01-2021
	    * Des : PDF not Print as receive server error for PTSPL raised by Rahul Tiwari.
	    */
				if(company!=null){
				 companyMail = company.getEmail();
				}
                  
		}

		
		return companyMail;

	}

	public String getBranchEmail(Company company, String branchName) {
		String branchMail = "";

		if (branchName != null && !branchName.equals("")) {
			Branch branch = ofy().load().type(Branch.class)
					.filter("companyId", company.getCompanyId())
					.filter("buisnessUnitName", branchName).first().now();

			if (branch != null && !branch.equals("")){
				branchMail = branch.getEmail();
			}else{
				branchMail = company.getEmail();
			}
				

		} else {
			branchMail=company.getEmail();
		}
		return branchMail;
	}
	
	/**
	 * Developed By : Manisha Date : 27/3/2018
	 * 
	 * Purpose : This is used to calculate total amount..!!
	 * 
	 * @return
	 */
	
	private double getPercentAmount(SalesLineItem salesLineItem, boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		if (isAreaPresent) {
			percentAmount = ((salesLineItem.getPrice()
					* Double.parseDouble(salesLineItem.getArea().trim()) * salesLineItem
					.getPercentageDiscount()) / 100);
		} else {
			percentAmount = ((salesLineItem.getPrice() * salesLineItem
					.getPercentageDiscount()) / 100);
		}
		return percentAmount;
	}

	
	public double getAssessTotalAmount(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		double totalAmount = 0;
		if (salesLineItem.getArea().trim().equalsIgnoreCase("NA")
				|| salesLineItem.getArea().equalsIgnoreCase("")) {
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = salesLineItem.getPrice();
				} else {
					totalAmount = salesLineItem.getPrice()
							- salesLineItem.getDiscountAmt();
				}
			} else {
				double disPercentAmount = getPercentAmount(salesLineItem, false);
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = salesLineItem.getPrice() - disPercentAmount;
				} else {
					totalAmount = salesLineItem.getPrice() - disPercentAmount
							- salesLineItem.getDiscountAmt();
				}
			}
		} else {

			if (salesLineItem.getPercentageDiscount() == null
					&& salesLineItem.getPercentageDiscount() == 0) {
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = salesLineItem.getPrice()
							* Double.parseDouble(salesLineItem.getArea().trim());
				} else {
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- salesLineItem.getDiscountAmt();
				}
			} else {
				double disPercentAmount = getPercentAmount(salesLineItem, true);
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount;
				} else {
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount - salesLineItem.getDiscountAmt();
				}
			}

		}
		return totalAmount;
	}
	
	/**
	 * nidhi
	 * 06-04-2018
	 * for branchASCompany
	 */
	
	public static Company changeBranchASCompany(Branch branch,Company company){
		Company comp = new Company();
		
		/**
		 * @author Anil @since 13-04-2021
		 * Facing error while printing Delivery Note for sasha
		 * raised by Rahul Tiwari
		 */
		comp.setCompanyId(branch.getCompanyId());
		
		comp.setBusinessUnitName(branch.getBusinessUnitName());
		if(branch.getCorrespondenceName()!=null&&!branch.getCorrespondenceName().equals("")){
			comp.setBusinessUnitName(branch.getCorrespondenceName());
		}

		if(branch.getEmail() != null && !branch.getEmail().trim().equals("")){
			comp.setEmail(branch.getEmail());
		}
		
		if(branch.getCellNumber1()!=null){
			comp.setCellNumber1(branch.getCellNumber1());
		}
		
		if(branch.getCellNumber2()!=null){
			comp.setCellNumber2(branch.getCellNumber2());
		}
		

		if(branch.getFaxNumber()!=null){
			comp.setFaxNumber(branch.getFaxNumber());
		}

		if(branch.getGSTINNumber()!=null && !branch.getGSTINNumber().trim().equals("")){
			comp.setCompanyGSTTypeText(branch.getGSTINNumber());
			/**
			 * @author Priyanka Bhagwat @since 02-04-2021
			 * This was needed to print gst number on new contract renewal pdf
			 * below data is hard coded becuase this field was not present in branch
			 */
			comp.setCompanyGSTType("GST Applicable");
		}
		

		if(branch.getLandline()!=null){
			comp.setLandline(branch.getLandline());
		}

		if(branch.getPocName()!=null && !branch.getPocName().trim().equals("") ){
			comp.setPocName(branch.getPocName());
		}
		
		if(branch.getPocCell()!= null){
			comp.setPocCell(branch.getPocCell());
		}
		
		if(branch.getPocEmail()!=null && !branch.getPocEmail().trim().equals("")){
			comp.setPocEmail(branch.getPocEmail());
		}
		
		if(branch.getPocLandline()!=null ){
			comp.setPocLandline(branch.getPocLandline());
		}
		
		if(branch.getLogo()!=null && branch.getLogo().getUrl().trim().length()>0){
			comp.setLogo(branch.getLogo());
		}else{
			/**
			 * @author Anil @since 13-04-2021
			 * If logo is not uploaded in branch then pick it from company master
			 * Raised by Rahul Tiwari for Sasha
			 */
			comp.setLogo(company.getLogo());
		}
		
		if(branch.getUploadHeader()!=null && branch.getUploadHeader().getUrl().trim().length()>0){
			comp.setUploadHeader(branch.getUploadHeader());
		}else{
			/**
			 * @author Anil @since 13-04-2021
			 * If logo is not uploaded in branch then pick it from company master
			 * Raised by Rahul Tiwari for Sasha
			 */
			comp.setUploadHeader(company.getUploadHeader());
		}
		
		if(branch.getUploadFooter()!=null && branch.getUploadFooter().getUrl().trim().length()>0){
			comp.setUploadFooter(branch.getUploadFooter());
		}else{
			/**
			 * @author Anil @since 13-04-2021
			 * If logo is not uploaded in branch then pick it from company master
			 * Raised by Rahul Tiwari for Sasha
			 */
			comp.setUploadFooter(company.getUploadFooter());
		}
		
		if(branch.getUploadDigitalSign()!=null && branch.getUploadDigitalSign().getUrl().trim().length()>0){
			comp.setUploadDigitalSign(branch.getUploadDigitalSign());
		}else{
			/**
			 * @author Anil @since 13-04-2021
			 * If logo is not uploaded in branch then pick it from company master
			 * Raised by Rahul Tiwari for Sasha
			 */
			comp.setUploadDigitalSign(company.getUploadDigitalSign());
		}
		
		if(branch.getSignatoryText() != null && branch.getSignatoryText().trim()!=null){
			comp.setSignatoryText(branch.getSignatoryText());
		}else{
			/**
			 * @author Anil @since 13-04-2021
			 * If logo is not uploaded in branch then pick it from company master
			 * Raised by Rahul Tiwari for Sasha
			 */
			comp.setSignatoryText(company.getSignatoryText());
		}
		
		if(branch.getAddress()!=null){
			comp.setAddress(branch.getAddress());
		}
		
		if(branch.getPANNumber()!=null){
			comp.setPanCard(branch.getPANNumber());
		}
		
		
		/**
		 * @author Vijay Date :- 14-07-2022
		 * Des :- as per new logic article information added in the branch so updated this code
		 */
		if(branch.getArticleTypeDetails()!=null && branch.getArticleTypeDetails().size()>0){
			 comp.setArticleTypeDetails(branch.getArticleTypeDetails());
		}
		else{
			/**
			 * @author Anil @since 23-04-2021
			 * Setting article type details to comp object from company master
			 */
			/**Added by sheetal:21-01-2022
		    Des:On documents print need branch PAN No , requirement raised by Surat pest control**/

			 if(branch.getPANNumber()!=null && !branch.getPANNumber().equals("")){
				 ArrayList<ArticleType> updatedlist = new  ArrayList<ArticleType>();
				 for (int i = 0; i < company.getArticleTypeDetails().size(); i++){
					 if (!company.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("PAN NO")){
						 updatedlist.add(company.getArticleTypeDetails().get(i));
					 }
				 }
				 ArticleType Articletypedetails = new ArticleType();
				 Articletypedetails.setDocumentName("Invoice Details");
				 Articletypedetails.setArticleTypeName("PAN NO");
				 Articletypedetails.setArticleTypeValue(branch.getPANNumber());
				 Articletypedetails.setArticlePrint("YES");
				 
				 updatedlist.add(Articletypedetails);
				 comp.setArticleTypeDetails(updatedlist);
			 }
			 else{
				 comp.setArticleTypeDetails(company.getArticleTypeDetails());
			 }
			/**end**/	
		}
		if(company.getBranchUsernamePasswordList()!=null)
			comp.setBranchUsernamePasswordList(company.getBranchUsernamePasswordList());
		
		if(company.getApiId()!=null&&!company.getApiId().equals(""))
			comp.setApiId(company.getApiId());
		
		if(company.getApiSecret()!=null&&!company.getApiSecret().equals(""))
			comp.setApiSecret(company.getApiSecret());
		
		if(company.getCustomerId()!=null&&!company.getCustomerId().equals(""))
			comp.setCustomerId(company.getCustomerId());
		if(company.getEinvoicevendor()!=null)
			comp.setEinvoicevendor(company.getEinvoicevendor());
		if(company.geteInvoiceEnvironment()!=null)
			comp.seteInvoiceEnvironment(company.geteInvoiceEnvironment());
		
		
//		 comp.setArticleTypeDetails(company.getArticleTypeDetails());
		 
		return comp;
		
	}
	
	public String getFinancialYearOfDate(Date date) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
		sdf2.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		int currentYear=Integer.parseInt(sdf.format(date).trim());
		int previousYear=currentYear-1;
		int upComingYear=currentYear;
		
		String startDateofFin="01/04/"+previousYear;
		String endDateofFin="31/03/"+upComingYear;
		
		if(sdf2.parse(startDateofFin).before(sdf2.parse(sdf2.format(date)))&&sdf2.parse(sdf2.format(date)).before(sdf2.parse(endDateofFin))){
			return previousYear+"-"+upComingYear;
		}else {
			return (previousYear+1)+"-"+(upComingYear+1);
		}
		
	}
	public int checkTaxPercent(double taxValue,String taxName ,List<ProductOtherCharges> taxesList)
	{
		
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
			
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
			
		}
		return -1;
	}
	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){			
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;

				}
				else{
					
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}

	public List<ProductOtherCharges> addProdTaxes(ArrayList<SalesLineItem> salesLineItemLis ,List<ProductOtherCharges> taxList) throws Exception
	{
		DecimalFormat nf=new DecimalFormat("0.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
			
					
			if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
							}else{
								System.out.println("old code");
								priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
								priceqty=Double.parseDouble(nf.format(priceqty));
							}
						/**********************************************/
					}
					
					else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
						
						System.out.println("inside both not null condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=priceqty*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
//							priceqty=priceqty*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
							
						}
						
						
						/***************************************************************/

					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						
						if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}else{
//								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}
							
						}
						else 
						{
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equals("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}else{
//								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}
						}
						priceqty=Double.parseDouble(nf.format(priceqty));
						System.out.println("Price ====== "+priceqty);
						
					}
			//****************changes ends here *********************************
		
			
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
				else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxName());
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT",taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						pocentity.setAssessableAmount(priceqty);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
				else{
				
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("Service Tax");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxName());
					pocentity.setChargeName("Service");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service",taxList);
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					taxList.add(pocentity);
					
					System.out.println("DONE ....");
				}
			}
		}
	return taxList;
	}

	
	/**
     * Developed By : nidhi
     * 2-04-2018
     * Purpose : This is global method use to get active process type   
     * @return 
     */
    public static String getForProcessConfigurartionIsActiveOrNot(String processName,Long companyId) {
    	
    	List<ProcessConfiguration> processList = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName", processName).list();
    	
    	int cnt =0;
    	for(int k=0;k<processList.size();k++){	
			if(processList.get(k).getProcessName().trim().equalsIgnoreCase(processName)){
			
				for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
					if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("AMCBillDateRestriction"))
					{
						DateTimeFormat smpDate = DateTimeFormat.getFormat("dd/MM/yyyy");
						System.out.println("get smp Date --" +smpDate);
						Date datefmt = new Date();
						
						try {
							datefmt = smpDate.parse(processList.get(k).getProcessList().get(i).getProcessType());
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
						} catch (Exception e) {
							return null;
						}
					}else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("GRNDateRestriction"))
					{
						if(processList.get(k).getProcessList().get(i).getProcessType().trim().matches("[0-9]"))
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
					}else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("MINDateRestriction"))
					{
						if(processList.get(k).getProcessList().get(i).getProcessType().trim().matches("[0-9]"))
							return processList.get(k).getProcessList().get(i).getProcessType().trim();
					}else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("CancelServiceUptoDate"))
					{
						return processList.get(k).getProcessList().get(i).getProcessType().trim();
					}
					else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("NewFumigationServiceDate"))
					{
						return processList.get(k).getProcessList().get(i).getProcessType().trim();
					}
					else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("GetReviewsForQuotationPdf"))
					{
						return processList.get(k).getProcessList().get(i).getProcessType().trim();
					}
					else if(processList.get(k).getProcessList().get(i).isStatus()==true  && processList.get(k).getProcessName().equals("GetReviewsLinkForQuotationPdf"))
					{
						return processList.get(k).getProcessList().get(i).getProcessType().trim();
					}
				}
				
			}	
    	}
				
				return null; 	
			
	}
    
    
  
    
  
    /** date 5.7.2018 added by komal for sasha leave validation**/
	Key<EmployeeInfo> employeeInfoKey = null;
	public void ApproveLeave(Attendance attendace){
		List<Attendance> trLis = new ArrayList<Attendance>();
		List<Attendance> otLis = new ArrayList<Attendance>();
		ArrayList<String> otList=new ArrayList<String>();
		if (attendace.isLeave()) {
			// System.out.println("Employee On Leave ...");
			
			if(attendace.isWeeklyOff()){
				return;
			}
			if(attendace.isHoliday()){
				return;
			}
			String prjName = attendace.getLeaveType().replace("Leave :", "");
			if (attendace.getLeaveHours() != 0) {
				trLis.add(attendace);
//				System.out.println("LEAVE List Size :: " + trLis.size());
			}
		}
		
		
		if (attendace.isOvertime()) {
			// System.out.println("Employee On Leave ...");
//			String prjName = attendace.getOvertimeType().replace("OT :", "");
			String prjName = attendace.getOvertimeType();
			otList.add(prjName);
			if (attendace.getOvertimeHours()!= 0) {
				otLis.add(attendace);
			}
		}
		if(trLis.size()>0){
			grantLeave(trLis , attendace.getCompanyId() ,attendace.getEmpId());
		}
		if(otLis.size()>0){
//			System.out.println("EMP ID : "+attendace.getEmpId()+" Date : "+attendace.getAttendanceDate()+" "+otLis.size()+" "+otList.size());
			getOvertime(otLis,otList,attendace.getCompanyId(),attendace.getEmpId());
		}
	}
	
	public void grantLeave(List<Attendance>  entrey ,Long companyId , int empId ) {
		System.out.println("Inside grant leave method ...");
		LeaveBalance temp = null;
		List<LeaveBalance> bal = null;
		
		// Get the List of Leave Balances in Kind , there will be a List as
		// previous year Leave Balance
		// can exist.Ideally the Querry should be from Leave Balance
		ObjectifyService.reset();
	     ofy().consistency(Consistency.STRONG);
		 ofy().clear();
		if (companyId != null) {
			bal = ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount", empId).list();
		} else {
			bal = ofy().load().type(LeaveBalance.class).filter("empInfo.empCount", empId).list();
		}
		ObjectifyService.reset();
	     ofy().consistency(Consistency.STRONG);
		 ofy().clear();
		// If for some reason balance is null return , accurate place to through
		// an Exception
		if (bal == null) {
			System.out.println("Leave Balance Is Not Found....");
			return;
		}

		// Initalize temp with Leave balance valid for current period only.
		// To This to happen, There should beich only one Entity in Kind Leave
		// Balance whichs valid Till
		// is greater then Current Date, Leave Allocation should ensure this.
		List<EmployeeLeave> employeeLeave = new ArrayList<EmployeeLeave>();

		/**
		 * @author Anil,Date : 23-01-2019
		 * user need to update back dated attendance and process payroll
		 * we are comparing leave balance validity with attendance date
		 */
		Date date=entrey.get(0).getAttendanceDate();
		
		for (LeaveBalance balance : bal) {
			/**
			 * @author Anil , Date : 02-01-2020
			 * After condition check day before till date , need to consider last day as well
			 */
			if (balance.getValidtill().after(date)||balance.getValidtill().equals(date)) {
				temp = balance;
				break;
			}

		}

		// If for some reason temp is null return , accurate place to through an
		// Exception
		if (temp == null) {
			System.out.println("Leave balance null while assigning...");
			return;
		}

		employeeInfoKey = temp.getEmpInfokey();
		ArrayList<AllocatedLeaves> allLeave = temp.getAllocatedLeaves();

		for (Attendance rep : entrey) {

			String levaeType = rep.getLeaveType();
			levaeType = levaeType.replace("Leave :", "");
			Double hrs = rep.getLeaveHours();
			logger.log(Level.SEVERE ,"Name Of Leave Type IN TIME REPORT " + levaeType);

			for (AllocatedLeaves altemp : allLeave) {

//				logger.log(Level.SEVERE ,"Name Of Leave Type IN Leave Balance "+ altemp.getName());
				
				if (altemp.getName().equalsIgnoreCase(levaeType)) {
					logger.log(Level.SEVERE ,"Name Of Leave Type " + levaeType+" BAL "+altemp.getBalance());
					// Only Reduce The Balance when it is zero
					double balHrs = altemp.getBalance() * temp.getWorkingHrs();
					logger.log(Level.SEVERE ,"Balance Hrs" + balHrs);
					logger.log(Level.SEVERE ,"Asked Hrs" + hrs);
					double approvedDay = hrs / temp.getWorkingHrs();
					logger.log(Level.SEVERE ,"Approved Days" + approvedDay);
					/**
					 * Date : 31-08-2018 BY ANIL
					 * for considering leave without pay
					 * if leave is unpaid dont check leave balance
					 */
					if (balHrs > 0||altemp.getPaidLeave()==false) {
						
						altemp.setAvailedDays(altemp.getAvailedDays()+ approvedDay);
						if(balHrs<=0){
							altemp.setBalance(0d);
						}else{
							altemp.setBalance(altemp.getBalance() - approvedDay);
						}

						EmployeeLeave empLeave = new EmployeeLeave();
						empLeave.setCompanyId(companyId);
						empLeave.setEmployeeKey(employeeInfoKey);
						empLeave.setLeaveType(altemp);
						empLeave.setLeaveDate(rep.getAttendanceDate());
						empLeave.setLeaveHrs(rep.getLeaveHours());
						/**
						 * Date : 14-05-2018 By ANIL
						 */
						empLeave.setEmplId(rep.getEmpId());
						/**
						 * Date : 17-07-2019 BY ANIL
						 */
						empLeave.setProjectName(rep.getProjectName());
						/**
						 * @author Anil
						 * @since 01-12-2020
						 */
						empLeave.setSiteLocation(rep.getSiteLocation());
						employeeLeave.add(empLeave);
						logger.log(Level.SEVERE ,"employeeLeave created for employee: "+empLeave.getEmplId()+" leave date:"+empLeave.getLeaveDate());
						
					}

				}

			}

		}
		// Save the Leave Balance entity

		ofy().save().entity(temp).now();

		if (employeeLeave.size() != 0) {
			ofy().save().entities(employeeLeave).now();
				}
	}
	
	public void getOvertime(List<Attendance> ovrTymList, ArrayList<String> otList,Long companyId, int empId) {
//		System.out.println("Inside Overtime method......");
		if(ovrTymList.size()==0){
			return;
		}
		
		EmployeeInfo info=null;
		if (companyId != null) {
			info = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount", empId).first().now();
		} else {
			info = ofy().load().type(EmployeeInfo.class).filter("empCount", empId).first().now();
		}
		logger.log(Level.SEVERE ,"employee info  in ot" +info);
		if(info==null){
			return;
		}
		
		/**
		 * Date : 01-06-2018 By ANIL
		 */
		List<Overtime> overtimeList=ofy().load().type(Overtime.class).filter("companyId", companyId).filter("name IN", otList).filter("status", true).list();
		
//		System.out.println("OVERTIME LIST SIZE :: "+overtimeList.size());
		
		List<EmployeeOvertime> employeeOvertime = new ArrayList<EmployeeOvertime>();
		
		for (Attendance rep : ovrTymList) {

			String otName = rep.getOvertimeType();
//			otName = otName.replace("OT :", "");
			Double hrs = rep.getWorkedHours();
			
//			System.out.println("OT ::: "+otName);
//			System.out.println("HRS ::: "+hrs);

			EmployeeOvertime empOvrTym = new EmployeeOvertime();
			
			empOvrTym.setCompanyId(companyId);
			empOvrTym.setEmployee(info);
			empOvrTym.setEmployeeKey(employeeInfoKey);
			
			empOvrTym.setOvertime(getOvertimeInfo(otName,overtimeList));
//			empOvrTym.setOvertime(info.getOvertime());
			empOvrTym.setOtDate(rep.getAttendanceDate());
			empOvrTym.setOtHrs(rep.getOvertimeHours());
			/**
			 * Date : 14-05-2018 By ANIL
			 */
			empOvrTym.setEmplId(empId);
			/**
			 * Date : 17-07-2019 By ANIL
			 */
			empOvrTym.setProjectName(rep.getProjectName());
			
			empOvrTym.setHolidayLabel(rep.getHolidayLabel());
			
			/**
			 * @author Anil
			 * @since 01-12-2020
			 */
			empOvrTym.setSiteLocation(rep.getSiteLocation());
			/**
			 * @author Vijay 18-12-2020
			 * Des :- added employee attendece designation here
			 */
			if(rep.getEmpDesignation()!=null) {
			empOvrTym.setEmpDesignation(rep.getEmpDesignation());
			}
			
			employeeOvertime.add(empOvrTym);
		}
		if (employeeOvertime.size() != 0) {
			ofy().save().entities(employeeOvertime).now();
		}
		grantCompensatoryLeave(employeeOvertime , companyId , empId);
		
	}
	
	private void grantCompensatoryLeave(List<EmployeeOvertime> employeeOvertime , Long companyId , int empId) {
		LeaveBalance levBal=ofy().load().type(LeaveBalance.class).filter("companyId",companyId).filter("empInfo.empCount", empId).first().now();
		
		for(EmployeeOvertime eot:employeeOvertime){
			
			if(eot.getOvertime()!=null && eot.getOvertime().isLeave()==true){
				double earnedleave=0;
				double overtimeHrs=eot.getOtHrs();
				overtimeHrs=overtimeHrs*eot.getOvertime().getLeaveMultiplier();
				earnedleave=overtimeHrs/eot.getEmployee().getLeaveCalendar().getWorkingHours();
//				System.out.println("earned Leave ::: "+earnedleave);
				
				if(levBal!=null){
//					System.out.println("IS Leave BAL  ::: "+levBal.getEmpCount());
					for(int i=0;i<levBal.getLeaveGroup().getAllocatedLeaves().size();i++){
						if(eot.getOvertime().getLeaveName().trim().equals(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim())){
							
							double balEarned=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getEarned();
							balEarned=balEarned+earnedleave;
							levBal.getLeaveGroup().getAllocatedLeaves().get(i).setEarned(balEarned);
							
							double balLeave=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getBalance();
//							System.out.println("Earned Leave ::: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim()+" BAL "+balLeave);
							
							balLeave=balLeave+earnedleave;
//							System.out.println("BAL--- "+balLeave);
							levBal.getLeaveGroup().getAllocatedLeaves().get(i).setBalance(balLeave);
							
							LeaveValidityInfo leaveInfo=new LeaveValidityInfo();
							leaveInfo.setLeaveName(eot.getOvertime().getLeaveName());
							
							leaveInfo.setLeaveCount(earnedleave);
							
							Date validDate=new Date(eot.getOtDate().getTime());
//							System.out.println("OT DATE : "+eot.getOtDate());
//							System.out.println("NO OF DAYS : "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getNo_of_days_to_avail());
							Date validDate1=DateUtility.addDaysToDate(validDate, levBal.getLeaveGroup().getAllocatedLeaves().get(i).getNo_of_days_to_avail());
//							System.out.println("Valid Until : "+validDate+" "+validDate1 );
							leaveInfo.setValidUntil(validDate1);
							
							if(levBal.getLeaveValidityList()!=null){
								levBal.getLeaveValidityList().add(leaveInfo);
							}else{
								List<LeaveValidityInfo> leaveLis=new ArrayList<LeaveValidityInfo>();
								leaveLis.add(leaveInfo);
								levBal.setLeaveValidityList(leaveLis);
							}
						}
					}
					ofy().save().entity(levBal).now();
				}
			}
		}
	}
	
	private Overtime getOvertimeInfo(String otName, List<Overtime> overtimeList) {
		for(Overtime ot:overtimeList){
			if(ot.getName().equalsIgnoreCase(otName)){
				return ot;
			}
		}
		return null;
	}
	public List<ProductOtherCharges> addProdTaxes1(ArrayList<SalesOrderProductLineItem> salesLineItemLis ,List<ProductOtherCharges> taxList,String accType) throws Exception
	{
		DecimalFormat nf=new DecimalFormat("0.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
			
					
			if((salesLineItemLis.get(i).getProdPercDiscount()==null && salesLineItemLis.get(i).getProdPercDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct() , accType, salesLineItemLis.get(i).getPrice());
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
							}else{
								System.out.println("old code");
//								if(salesLineItemLis.get(i).getQuantity() !=0 ){
								priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice) *salesLineItemLis.get(i).getQuantity();
								priceqty=Double.parseDouble(nf.format(priceqty));
//								}
//								priceqty = 0;
							}
						/**********************************************/
					}
					
					else if(((salesLineItemLis.get(i).getProdPercDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
						
						System.out.println("inside both not null condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct() , accType, salesLineItemLis.get(i).getPrice());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=priceqty*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
	//						priceqty=priceqty*salesLineItemLis.get(i).getQty();
//							if(salesLineItemLis.get(i).getQuantity() !=0 ){
							priceqty = priceqty * salesLineItemLis.get(i).getQuantity();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
//							}
//							priceqty = 0;
						}
						
						
						/***************************************************************/
	
					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct() , accType, salesLineItemLis.get(i).getPrice());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						
						if(salesLineItemLis.get(i).getProdPercDiscount()!=null){
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
							}else{
	//							priceqty=priceqty*salesLineItemLis.get(i).getQty();
//								if(salesLineItemLis.get(i).getQuantity() !=0 ){
								priceqty = priceqty * salesLineItemLis.get(i).getQuantity();
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getProdPercDiscount()/100);
//								}
//								priceqty = 0; 
							}
							
						}
						else 
						{
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equals("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}else{
	//							priceqty=priceqty*salesLineItemLis.get(i).getQty();
//								if(salesLineItemLis.get(i).getQuantity() !=0 ){
								priceqty = priceqty * salesLineItemLis.get(i).getQuantity();
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
						//	}
//								priceqty = 0;
							}
						}
						priceqty=Double.parseDouble(nf.format(priceqty));
						System.out.println("Price ====== "+priceqty);
						
					}
			//****************changes ends here *********************************
			
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
	//					pocentity.setAssessableAmount(priceqty);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
				else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxName());
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT",taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
	//					pocentity.setAssessableAmount(priceqty);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
				else{
				
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("Service Tax");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxName());
					pocentity.setChargeName("Service");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service",taxList);
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					taxList.add(pocentity);
					
					System.out.println("DONE ....");
				}
			}
		}
	return taxList;
	}
	public List<ProductOtherCharges> addProdTaxes(ArrayList<SalesLineItem> salesLineItemLis ,List<ProductOtherCharges> taxList,String accType) throws Exception
	{
		DecimalFormat nf=new DecimalFormat("0.00");
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=0,taxPrice=0;
			
					
			if((salesLineItemLis.get(i).getPercentageDiscount()==null && salesLineItemLis.get(i).getPercentageDiscount()==0) && (salesLineItemLis.get(i).getDiscountAmt()==0) ){
						
						System.out.println("inside both 0 condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct() , accType, salesLineItemLis.get(i).getPrice());
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice)*squareArea;
							priceqty=Double.parseDouble(nf.format(priceqty));
							System.out.println("RRRRRRRRRRRRRR price == "+priceqty);
							}else{
								System.out.println("old code");
								priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice) *salesLineItemLis.get(i).getQty() ;
								priceqty=Double.parseDouble(nf.format(priceqty));
							}
						/**********************************************/
					}
					
					else if(((salesLineItemLis.get(i).getPercentageDiscount()!=null)&& (salesLineItemLis.get(i).getDiscountAmt()!=0))){
						
						System.out.println("inside both not null condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct() , accType, salesLineItemLis.get(i).getPrice());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
						if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
							double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
							priceqty=priceqty*squareArea;
							priceqty= priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
						}else{
							priceqty=priceqty*salesLineItemLis.get(i).getQty();
							priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							priceqty=Double.parseDouble(nf.format(priceqty));
							
						}
						
						
						/***************************************************************/
	
					}
					else 
					{
						System.out.println("inside oneof the null condition");
						
						taxPrice=removeAllTaxes(salesLineItemLis.get(i).getPrduct() , accType, salesLineItemLis.get(i).getPrice());
						priceqty=(salesLineItemLis.get(i).getPrice()-taxPrice);
						
						if(salesLineItemLis.get(i).getPercentageDiscount()!=null){
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}else{
								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-(priceqty*salesLineItemLis.get(i).getPercentageDiscount()/100);
							}
							
						}
						else 
						{
							/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
							if(!salesLineItemLis.get(i).getArea().equals("NA")){
								Double squareArea = Double.parseDouble(salesLineItemLis.get(i).getArea());
								priceqty = priceqty*squareArea;
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}else{
								priceqty=priceqty*salesLineItemLis.get(i).getQty();
								priceqty=priceqty-salesLineItemLis.get(i).getDiscountAmt();
							}
						}
						priceqty=Double.parseDouble(nf.format(priceqty));
						System.out.println("Price ====== "+priceqty);
						
					}
			//****************changes ends here *********************************
			
					if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
	//					pocentity.setAssessableAmount(priceqty);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
				else{
					System.out.println("Vattx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxName());
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),"VAT",taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
			}
			
			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
	//					pocentity.setAssessableAmount(priceqty);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					taxList.add(pocentity);
				}
				else{
				
					System.out.println("ServiceTx");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					//   old code commented by rohan on date : 22-6-2017
	//				pocentity.setChargeName("Service Tax");
					//   new code by rohan Date 22-6-2017 
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxName());
					pocentity.setChargeName("Service");
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),"Service",taxList);
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						double assessValue=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessValue=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
					}
					if(indexValue==-1){
						System.out.println("Index As -1");
						double assessVal=0;
						if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
							assessVal=priceqty+(priceqty*salesLineItemLis.get(i).getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(salesLineItemLis.get(i).getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(priceqty);
						}
						
					}
					taxList.add(pocentity);
					
					System.out.println("DONE ....");
				}
			}
		}
	return taxList;
	}
	public double removeAllTaxes(SuperProduct entity , String accType , double price)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(accType.equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getPurchaseTax1()!=null&&prod.getPurchaseTax1().isInclusive()==true){
				service=prod.getPurchaseTax1().getPercentage();
			}
			if(prod.getPurchaseTax2()!=null&&prod.getPurchaseTax2().isInclusive()==true){
				vat=prod.getPurchaseTax2().getPercentage();
			}
		}
	
		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getPurchaseTax2()!=null&&prod.getPurchaseTax2().isInclusive()==true){
				vat=prod.getPurchaseTax2().getPercentage();
			}
			if(prod.getPurchaseTax1()!=null&&prod.getPurchaseTax1().isInclusive()==true){
				service=prod.getPurchaseTax1().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=price/(1+(vat/100));
			retrVat=price-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=price/(1+service/100);
			retrServ=price-retrServ;
		}
		if(service!=0&&vat!=0){			
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getPurchaseTax1().getTaxPrintName()!=null && ! prod.getPurchaseTax1().getTaxPrintName().equals("")
				   && prod.getPurchaseTax2().getTaxPrintName()!=null && ! prod.getPurchaseTax2().getTaxPrintName().equals(""))
				{
	
					double dot = service + vat;
					retrServ=(price/(1+dot/100));
					retrServ=price-retrServ;
	
				}
				else{
					
					double removeServiceTax=(price/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=price-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getPurchaseTax1().getTaxPrintName()!=null && ! prod.getPurchaseTax1().getTaxPrintName().equals("")
				   && prod.getPurchaseTax2().getTaxPrintName()!=null && ! prod.getPurchaseTax2().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(price/(1+dot/100));
					retrServ=price-retrServ;
				}
				else
				{
					double removeServiceTax=(price/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=price-retrServ;
				}
			}
		}
		}else{
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
					service=prod.getServiceTax().getPercentage();
				}
				if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
					vat=prod.getVatTax().getPercentage();
				}
			}
	
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
					vat=prod.getVatTax().getPercentage();
				}
				if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
					service=prod.getServiceTax().getPercentage();
				}
			}
			
			if(vat!=0&&service==0){
				retrVat=price/(1+(vat/100));
				retrVat=price-retrVat;
			}
			if(service!=0&&vat==0){
				retrServ=price/(1+service/100);
				retrServ=price-retrServ;
			}
			if(service!=0&&vat!=0){			
				
				if(entity instanceof ItemProduct)
				{
					ItemProduct prod=(ItemProduct) entity;
					if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
					   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
					{
	
						double dot = service + vat;
						retrServ=(price/(1+dot/100));
						retrServ=price-retrServ;
	
					}
					else{
						
						double removeServiceTax=(price/(1+service/100));
						retrServ=(removeServiceTax/(1+vat/100));
						retrServ=price-retrServ;
					}
				}
				
				if(entity instanceof ServiceProduct)
				{
					ServiceProduct prod=(ServiceProduct) entity;
					if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
					   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
					{
						double dot = service + vat;
						retrServ=(price/(1+dot/100));
						retrServ=price-retrServ;
					}
					else
					{
						double removeServiceTax=(price/(1+service/100));
						retrServ=(removeServiceTax/(1+vat/100));
						retrServ=price-retrServ;
					}
				}
		}
		}
		tax=retrVat+retrServ;
		return tax;
	}

	/** date 14.10.2018 added by komal
	 * year format 18-19 or 1819 format
	 * @return
	 * @throws ParseException 
	 */
	public String getFinancialYearFormat(Date date , boolean flag) throws ParseException{
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
				sdf2.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				int currentYear=Integer.parseInt(sdf.format(date).trim());
				String previousYear=(currentYear-1)+"";
				String upComingYear=currentYear+"";
				
				String previousYear1=currentYear+"";
				String upComingYear1=(currentYear+1)+"";
				
				String startDateofFin="01/04/"+previousYear;
				String endDateofFin="31/03/"+upComingYear;
				String yearString = "";
				if(sdf2.parse(startDateofFin).before(sdf2.parse(sdf2.format(date)))&&sdf2.parse(sdf2.format(date)).before(sdf2.parse(endDateofFin))){
					if(flag){
						yearString = previousYear.substring(2, previousYear.length())+"-"+upComingYear.substring(2, upComingYear.length());
					}else{
						yearString = previousYear.substring(2, previousYear.length())+""+upComingYear.substring(2, upComingYear.length());
					}
				}else {
					if(flag){
						yearString = previousYear1.substring(2, previousYear1.length())+"-"+upComingYear1.substring(2, upComingYear1.length());
					}else{
						yearString = previousYear1.substring(2, previousYear1.length())+""+upComingYear1.substring(2, upComingYear1.length());
					}
				}
				return yearString;
			}
	/** date 20.10.2018 added by komal to compare states between 2 addresses**/
	public static boolean isAddressStateDifferent(Address addr1 , Address addr2){
		if(addr1.getState().equalsIgnoreCase(addr2.getState())){
			return false;
		}else{
			return true;
		}
		
	}	
	
	/***
	 * Date 29-04-2019 by Vijay
	 * @param user
	 * @param username
	 * @param password
	 * @param companyId
	 * @return
	 * Des :- For android common method to check valid login credential
	 */
	public boolean checkUserLogin(User user, String username, String password,
			long companyId) {

		
		if (user.getStatus()) {

			if (user != null) {
				if(user.getPassword().trim().equals(password.trim())){
					return true;
				}else{
					boolean matched = BCrypt.checkpw(password.trim(),
							user.getPassword());
					logger.log(Level.SEVERE, "Login matched value:::::" + matched);
	
					if (matched) {
						return true;
					}
				}
			}
		}
		return false;
	}
     /** date 29.05.2019 added by komal to get company name for android app **/
		public static String[] getCompnayName(String urlCalled){
			String url;
			String[] splitUrl;
			if(urlCalled.contains("-dot-")){
				url=urlCalled.replace("https://","");
				splitUrl=url.split("\\-");
			}else{	
				url=urlCalled.replace("http://", "");
				splitUrl=url.split("\\.");
			}
			return splitUrl;
		}
    
		
		/**
		 * Date :- 26-07-2019 by Vijay 
		 * Des :- to send first level request for approval of any document while creating automatic or by uploading
		 */
		public void sendApprovalRequest(String branch, int documentId, Long companyId, String approvarName, String personResonsible, String documentName) {
			Approvals approval=new Approvals();
			if(approvarName!=null)
			approval.setApproverName(approvarName);
			approval.setRequestedBy(approvarName);
			approval.setBranchname(branch);
			approval.setBusinessprocessId(documentId);
			if(UserConfiguration.getInfo()!=null && !UserConfiguration.getInfo().getFullName().equals("")){
				approval.setRequestedBy(UserConfiguration.getInfo().getFullName());
			}
			// here i am setting person responsible to send email in cc for person responsible
			if(personResonsible!=null){
				approval.setPersonResponsible(personResonsible);
			}
			approval.setApprovalLevel(1);
			approval.setStatus(Approvals.PENDING);
			approval.setBusinessprocesstype(documentName);
			approval.setDocumentValidation(false);
			approval.setCompanyId(companyId);
			approval.setCreationDate(new Date());
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(approval);
		
		}
		
		
	/**
	 * @anil ,Date : 27-01-2020
	 * @param text
	 * @return
	 */
	public String decodeText(String text) {
//		try {
//			return URLDecoder.decode(text,StandardCharsets.UTF_8.toString());
//		} catch (UnsupportedEncodingException e) {
//			return text;
////			throw new RuntimeException(e.getCause());
//		}
		
		
		try {
			byte[] decodedBytes = Base64.decodeBase64(text);
			text= new String(decodedBytes);
			System.out.println("decodedBytes " +text);
			return text;
		} catch (Exception e) {
			e.printStackTrace();
			return text;
		}
	}
	
	public String encodeText(String text) {
//		try {
//			return URLEncoder.encode(text,StandardCharsets.UTF_8.toString());
//		} catch (UnsupportedEncodingException e) {
//			return text;
////			throw new RuntimeException(e.getCause());
//		}
		
		
		try {
			byte[] encodedBytes = Base64.encodeBase64(text.getBytes());
			text=new String(encodedBytes);
			System.out.println("encodedBytes " +text );
			return text;
		} catch (Exception e) {
			e.printStackTrace();
			return text;
		}
	}


//public void sendApprovalRequestPRUpload(PurchaseRequisition prequisitionEntity, ArrayList<PurchaseRequisition> prlist, String branch, int documentId, Long companyId, String approvarName, String personResonsible, String documentName) {
	//Deepak Salve 24/02/2020 update reagain 03/03/2020
	public void sendApprovalRequestPRUpload(PurchaseRequisition prequisitionEntity, String prlist, String branch, int documentId, Long companyId, String approvarName, String personResonsible, String documentName) {			
	        List<Company> compEntity = ofy().load().type(Company.class).filter("companyId", companyId).list();
			logger.log(Level.SEVERE,"Company load size "+compEntity.size()+" Comapny Value "+compEntity);
			Approvals approval=new Approvals();
			if(approvarName!=null){
				approval.setApproverName(approvarName);
				approval.setRequestedBy(personResonsible);
				approval.setBranchname(branch);
				approval.setBusinessprocessId(documentId);
//				if(UserConfiguration.getInfo()!=null && !UserConfiguration.getInfo().getFullName().equals("")){
//					System.out.println(" Requested by in Approval Request "+UserConfiguration.getInfo().getFullName());
//				approval.setRequestedBy(UserConfiguration.getInfo().getFullName());
//				}
//				try{
//				System.out.println(" Requested by in Approval Request "+UserConfiguration.getInfo().getFullName());
//				}catch(Exception e){
//					System.out.println("inside Requested by in Approval Request Exception "+e+" Value "+UserConfiguration.getInfo().getFullName());
//				}
				if(personResonsible!=null){
					System.out.println(" Responsible Person "+personResonsible);
					approval.setPersonResponsible(personResonsible);
				}
				try{
				System.out.println(" Responsible Person "+personResonsible);
				}catch(Exception e){
					System.out.println("inside Responsible Person Exception "+e+" Value "+personResonsible);
				}
				approval.setApprovalLevel(1);
				approval.setStatus(Approvals.PENDING);
				approval.setBusinessprocesstype(documentName);
				approval.setDocumentValidation(false);
				approval.setCompanyId(companyId);
				logger.log(Level.SEVERE,"Company Id at Approval Method");
				Date date=new Date();
				approval.setCreationDate(date);
				if(approval.getCreationDate()!=null)
				{
				  System.out.println(" Approval Creation Date "+approval.getCreationDate());	
				}
				ReturnFromServer server = new ReturnFromServer();
				GenricServiceImpl impl = new GenricServiceImpl();
				server=impl.save(approval);
				sendMail(approvarName,companyId,server.count,personResonsible,compEntity.get(0),prequisitionEntity,"","",null,null);
			}
		}      


   
   		private void sendMail(String approvarName,Long companyId, int documentId, String personResonsible, Company company, PurchaseRequisition prequisitionEntity, String string, String string2,  ArrayList<String> tableHeader, ArrayList<String> table1) 
		{
			// TODO Auto-generated method stub
			
			logger.log(Level.SEVERE,"inside Email Method");
			logger.log(Level.SEVERE,"CompanyId at sendMail Method :- "+companyId);
			logger.log(Level.SEVERE,"Company at sent mail method  "+company);
			
			ArrayList<String> employeeList=new ArrayList<String>();
			employeeList.add(personResonsible);
			employeeList.add(approvarName);
			String approvalEmail="";
			String responsibleEmail="";
			List<Employee> employee = ofy().load().type(Employee.class).filter("companyId",companyId).filter("fullname IN", employeeList).list();
			for(Employee emp:employee){
				if(emp.getFullname().equals(approvarName)){
					approvalEmail=emp.getEmail();
				}
				if(emp.getFullname().equals(personResonsible)){
					responsibleEmail=emp.getEmail();
				}
			}
			
			System.out.println(" Employee List size "+employee.size());
			
			logger.log(Level.SEVERE," Approval Email id "+approvalEmail);
			logger.log(Level.SEVERE," Responsible Person email id "+responsibleEmail);
			
			String	msgBody ="</br></br> Dear Sir/Madam,"
					+ "<br><br>"
					+ "Following PR Approval Request has been assigned for Approval please check and do needful "
					+ "<br><br>"
					+ "PR ID :- "+prequisitionEntity.getCount()
					+ "<br><br>"
					+ "Requested By "+personResonsible
					+ "<br><br>";
			
		   String mailSubject="Subject : PR Approval Request "+ prequisitionEntity.getCount() +" has been assigned for Approval";
		   
		 //Table header 1
			ArrayList<String> tbl1_header=new ArrayList<String>();
			tbl1_header.add("PR Approval Request");
			tbl1_header.add("PR DATE");
			System.out.println("Tbl1 header Size :"+tbl1_header.size());
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			// Table 1
			ArrayList<String> tbl1=new ArrayList<String>();
			tbl1.add(documentId+"");
			tbl1.add(dateFormat.format(new Date()));
			
			String prTitle = "PR Approval Request";
			
			ArrayList<String> tbl2_header=new ArrayList<String>();
			tbl2_header.add("Product PR No");
			tbl2_header.add("Description");
			tbl2_header.add("Cluster");
			tbl2_header.add("Delivery Location");
			tbl2_header.add("Delivery Address");
			tbl2_header.add("Automatic / Manual");
			tbl2_header.add("Order Quantity");
			tbl2_header.add("Rate per Piece");
			tbl2_header.add("Total Value");
			tbl2_header.add("Stock in hand");
			tbl2_header.add("Forecast");
			tbl2_header.add("Safety Stock");
			tbl2_header.add("Reordering Level");
			tbl2_header.add("Min Order Qty");
			// Table 2
			ArrayList<String> tbl2=new ArrayList<String>();
			
			tbl2.add("");
			
			
//			for(ProductDetails prProdDetail : prequisitionEntity.getPrproduct()){
//				for(ProductInventoryViewDetails prodDetails : prodDetailslist){
//					if(prodDetails.getWarehousename().trim().equalsIgnoreCase(prProdDetail.getWarehouseName().trim())){
//						tbl2.add(prProdDetail.getProductName());
//						tbl2.add(purchaseRequisition.getDescription());
//						tbl2.add(prProdDetail.getWarehouseName());
//						tbl2.add(prProdDetail.getWarehouseName());
//						tbl2.add(prProdDetail.getWarehouseAddress());
//						tbl2.add("Automatic");
//						tbl2.add(prProdDetail.getProductQuantity()+"");
//						tbl2.add(prProdDetail.getProdPrice()+"");
//						tbl2.add(prProdDetail.getTotal()+"");
//						tbl2.add(prodDetails.getAvailableqty()+"");
//						tbl2.add(prodDetails.getForecastDemand()+"");
//						tbl2.add(prodDetails.getSafetyStockLevel()+"");
//						tbl2.add(prodDetails.getReorderlevel()+"");
//						tbl2.add(prodDetails.getMinOrderQty()+"");
//					}
//				}
//				
//			}
			
			ArrayList<String> toEmailLis=new ArrayList<String>();
			toEmailLis.add(approvalEmail);
			ArrayList<String> ccEmailLis=new ArrayList<String>();
			ccEmailLis.add(responsibleEmail);
			String fromEmail=company.getEmail();
			
			
			string2="";
//			tableHeader.add("");
//			table1.add("");
			try{
			Email email = new Email();
			email.sendEmailforhtml(mailSubject, msgBody, toEmailLis, ccEmailLis, fromEmail);
//			email.htmlformatsendEmail(approvalMailList, mailSubject, "", company, msgBody, null, null, tbl1_header, tbl1,
//					prTitle,null,null,string2,tableHeader,table1);
			}catch(Exception e){
				logger.log(Level.SEVERE," HTML method Call Exception ");
			}
			
//			approvalMailList, mailSubject, "", company, msgBody, null, null, tbl1_header, tbl1,
//			prTitle,tbl2_header,tbl2,string2,tableHeader,table1
			
		}
   		
   		
   		public  List<ServiceSchedule> updateSchedulingListComponentWise(ServiceSchedule serSch,BranchWiseScheduling branchSch){
   			List<ServiceSchedule> list=new ArrayList<ServiceSchedule>();
   			if(branchSch.getAssetQty()>0){
   				if(branchSch.getComponentList()!=null&&branchSch.getComponentList().size()!=0){
   					int counter=0;
   					for(ComponentDetails component:branchSch.getComponentList()){
   						if(counter==0){
   							setAssetAndComponentDetails(serSch, branchSch, component);
   						}else{
   							ServiceSchedule obj=new ServiceSchedule();
   							obj=serSch.copyOfObject();
   							setAssetAndComponentDetails(obj, branchSch, component);
   							list.add(obj);
   						}
   						counter++;
   					}
   				}else{
   					for(int i=0;i<branchSch.getAssetQty()-1;i++){
   						ServiceSchedule obj=new ServiceSchedule();
   						obj=serSch.copyOfObject();
   						list.add(obj);
   					}
   				}
   			}else{
   				return null;
   			}
   			
   			return list;
   		}
   		
   		public static ServiceSchedule setAssetAndComponentDetails(ServiceSchedule serSch,BranchWiseScheduling branchSch,ComponentDetails compObj){
   			serSch.setTierName(branchSch.getTierName());
   			serSch.setTat(branchSch.getTat());
   			serSch.setComponentName(compObj.getComponentName());
   			serSch.setMfgNum(compObj.getMfgNum());
   			serSch.setMfgDate(compObj.getMfgDate());
   			serSch.setReplacementDate(compObj.getReplacementDate());
   			serSch.setSrNo(compObj.getSrNo());
   			serSch.setAssetUnit(compObj.getAssetUnit());
   			return serSch;
   		}
   		
   		
   		
   		public String getPANNumber(Company company, String BranchName, String DocumentName) {
   			String PanNumber = "";
   			if (BranchName != null && !BranchName.equals("")) {
   				Branch branch = ofy().load().type(Branch.class)
   						.filter("companyId", company.getCompanyId())
   						.filter("buisnessUnitName", BranchName).first()
   						.now();
   				if (branch != null) {
   					if (branch.getPANNumber() != null) {
   						PanNumber = branch.getPANNumber();
   					} else {
   						PanNumber = "";
   					}
   				}
   				return PanNumber;
   			} else {
   				for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {
   					if (company.getArticleTypeDetails().get(i).getDocumentName().equals(DocumentName) &&
   							company.getArticleTypeDetails().get(i).getArticleTypeName()
   							.equalsIgnoreCase("PAN No")) {
   						PanNumber = company.getArticleTypeDetails().get(i)
   								.getArticleTypeValue().trim();
   						break;
   					}
   				}
   				return PanNumber;
   			}
   		}
   		
   		public static String parseDate(Date date)
		{
			SimpleDateFormat format =  new SimpleDateFormat("dd/MM/yyyy");
			return format.format(date);
		}
   		
   		/**
   		 * @author Vijay Chougule Date 02-01-2020
   		 * Des :- The method will accept input as Blobkey and it will read excel file and return in arraylist
   		 * if blob key value is null then it will return null
   		 * This method is reusable to read excel sheet for Uploads
   		 * 
   		 */
		public ArrayList<String> readExcelAndGetArraylist(BlobKey blobkey, String dateformat) {
			SimpleDateFormat fmt;
			if(dateformat!=null && !dateformat.equals("")){
				fmt = new SimpleDateFormat(dateformat);
			}
			else{
				fmt = new SimpleDateFormat("MM/dd/yyyy");;
			}
			
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			

			ArrayList<String> exceldatalist = new ArrayList<String>();
			if (blobkey != null) {
				try {
					logger.log(Level.SEVERE, "Reading excel ");
					logger.log(Level.SEVERE, "Blobkey value" + blobkey);
					long i;
					Workbook wb = null;
					try {
						wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
					} catch (EncryptedDocumentException e) {
						e.printStackTrace();
					} catch (InvalidFormatException e) {
						e.printStackTrace();
					}
					Sheet sheet = wb.getSheetAt(0);
					// Get iterator to all the rows in current sheet
					Iterator<Row> rowIterator = sheet.iterator();
					// Traversing over each row of XLSX file
					while (rowIterator.hasNext()) {
						Row row = rowIterator.next();
						// For each row, iterate through each columns
						Iterator<Cell> cellIterator = row.cellIterator();
						while (cellIterator.hasNext()) {
							Cell cell = cellIterator.next();
							switch (cell.getCellType()) {
							case Cell.CELL_TYPE_STRING:
								exceldatalist.add(cell.getStringCellValue());
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if (DateUtil.isCellDateFormatted(cell)) {
									exceldatalist.add(fmt.format(cell.getDateCellValue()));
								} else {
									// i = (long) cell.getNumericCellValue();
									// logger.log(Level.SEVERE, "Value : "+i);
//									logger.log(Level.SEVERE, "Value : " + cell.getNumericCellValue());
									// exceldatalist.add(cell.getNumericCellValue() +
									// "");
									DecimalFormat decimalFormat = new DecimalFormat("#########.##");
									int value1 = 0, value2 = 0;
									String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
									if (array.length > 0) {
										// value1 = Integer.parseInt(array[0]);
									}
									if (array.length > 1) {
										value2 = Integer.parseInt(array[1]);
									}
									if (value2 == 0) {
										i = (long) cell.getNumericCellValue();
										exceldatalist.add(i + "");
									} else {
										exceldatalist.add(cell.getNumericCellValue() + "");
									}

								}
								break;
							case Cell.CELL_TYPE_BOOLEAN:

								break;
							default:
							}
						}
					}

					System.out.println("Total size:" + exceldatalist.size());
					logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist.size());

					wb.close();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				return null;
			}
			return exceldatalist;
		}

		
		
		/**
		 * @author Vijay Chougule Date :- 02-01-2020
		 * Des :- Below Method will check all columns as as per the excel upload file
		 * this method will retun tru if excel file is proper format
		 * else it will return false
		 */
		public boolean validateExcelSheet(ArrayList<String> exceldatalist, ArrayList<String> uploadfileHeader) {
			boolean flag = false;
			for(int i=0;i<uploadfileHeader.size();i++) {
				if (exceldatalist.get(i).trim().equalsIgnoreCase(uploadfileHeader.get(i).trim())){
					flag = true;
				}
				else {
					flag = false;
					break;
				}
			}
			return flag;
		}

		/**
		 * @author Vijay  10-01-2020
		 * Des :- created some reusable methos for excel uploading purpose 
		 * @return
		 */
		public List<Invoice> loadInvoices(long companyId, ArrayList<Integer> arrayinvoiceId) {
			List<Invoice> invoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId)
					.filter("count IN", arrayinvoiceId).list();
			return invoicelist;
		}

		public List<CustomerPayment> loadCustomerPayment(long companyId, ArrayList<Integer> arryinvoiceId, String status) {
			List<CustomerPayment> paymentdetailslist = ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
					.filter("invoiceCount IN", arryinvoiceId).filter("status", status).list();
			return paymentdetailslist;

		}

		public double getDoubleValue(String strNumber) {
			try {
				Double value = Double.parseDouble(strNumber);
				return value;
			} catch (Exception e) {
				return -1;
			}
		}
		
		public double getIntegerValue(String strNumber) {
			try {
				Integer value = Integer.parseInt(strNumber);
				return value;
			} catch (Exception e) {
				return -1;
			}
		}

		public boolean validateNumricValue(String strvalue) {
			
			if(strvalue.matches("[0-9]+(\\.){0,1}[0-9]*") ) {
				return true;
			}
			else {
				return false;
			}
		}
		
		
		public boolean validateStringValue(String strvalue) {
			
			Pattern pattern = Pattern.compile("[a-zA-Z]");
			Matcher matcher = pattern.matcher(strvalue);
			boolean b = matcher.find();
			if(b) {
				Pattern pattern2 = Pattern.compile("[0-9]");
				Matcher matcher2 = pattern2.matcher(strvalue);
				boolean b2 = matcher2.find();
				if(b2) {
					return false;
				}
				else {
					return true;
				}
			}
			else {
				return false;
			}
			
		}
		
		public String validateStringCloumn(String columnName, String columnValue) {
			try {
			String str = "";
			if(!columnValue.trim().equalsIgnoreCase("NA") && !columnValue.trim().equalsIgnoreCase("N A")) {
				if(!validateStringValue(columnValue)) {
					str += " "+columnName+" Column should be alphabetic value. ";
				}
				if(!validatespecialCharacter(columnValue,false)) {
					str += " Comma special character not allowed in Column "+columnName+". ";
				}
			}
			return str;
			} catch (Exception e) {
				return " Please enter alphabetic value in "+columnName +" column";

			}
		}
		public String validateNumericCloumn(String columnName, String columnValue) {
			try {
			String str = "";
			if(!columnValue.trim().equalsIgnoreCase("NA") && !columnValue.trim().equalsIgnoreCase("N A")) {
				if(!validateNumricValue(columnValue)) {
					str += " "+columnName+" Column should be numeric value. ";
				}
				if(!validatespecialCharacter(columnValue,true)) {
					str += " Special character not allowed in Column "+columnName+". ";
				}
			}
			return str;
			} catch (Exception e) {
				return " Please enter numeric value in "+columnName +" column";
			}
		}
		
		
		
		public boolean validatespecialCharacter(String strvalue, boolean specialcharFlag) {
			boolean flag = true;
			String specialCharatcters = ",";
			if(specialcharFlag){
				specialCharatcters = "[!@#$%&*()_+=|<>?{}\\\\[\\\\]~-]";
			}
			else{
				specialCharatcters = ",";
			}
			
			for(int i=0; i<strvalue.length();i++) {
				if(specialCharatcters.contains(Character.toString(strvalue.charAt(i)))) {
					flag = false;
					break;
				}
			}
			return flag;
			
	}
		public String validateSpecialCharComma(String columnName, String columnValue) {
			String str = "";
			if(!validatespecialCharacter(columnValue,false)) {
				str += " Comma special character not allowed in Column "+columnName+". ";

			}
			return str;
		}
		
		public boolean validatestr(String strvalue) {
			Pattern pattern = Pattern.compile("[a-zA-Z]*");
			Matcher matcher = pattern.matcher(strvalue);
			if(matcher.matches()) {
				return true;
			}
			else {
				return false;
			}

	}

		public List<Config> loadConfigsData(int type, long companyId) {
			List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId).filter("status", true)
					.filter("type", type).list();
			return configEntity;
		}
		
		public List<ConfigCategory> loadConfigsCategoryData(int type, long companyId) {
			List<ConfigCategory> configCategoryEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", type).filter("status", true).list();
			return configCategoryEntity;
		}
		
		public List<Type> loadCategoryTypeData(int type, long companyId) {
			List<Type> configCategoryEntity = ofy().load().type(Type.class).filter("companyId", companyId)
					.filter("internalType", type).filter("status", true).list();
			return configCategoryEntity;
		}

		public boolean validateConfig(List<Config> configEntitylist, String configName) {
			if(configEntitylist!=null){
				for(Config configEntity : configEntitylist) {
					if(configEntity.getName().trim().equals(configName.trim())) {
						return true;
					}
				}
			}
			return false;
		}
		
		private boolean validateConfigCategory(List<ConfigCategory> configCategorylist, String configCategoryName) {
			if(configCategorylist!=null){
				for(ConfigCategory configCategoryEntity : configCategorylist) {
					if(configCategoryEntity.getCategoryName().trim().equals(configCategoryName.trim())) {
						return true;
					}
				}
			}
			return false;
		}
		
		private boolean validateCategoryType(List<Type> typeEntitylist, String categoryName, String typeName) {
			if(typeEntitylist!=null){
				for(Type typeEntity : typeEntitylist) {
					if(typeEntity.getCategoryName().trim().equals(categoryName.trim()) && typeEntity.getTypeName().trim().equals(typeName.trim())) {
						return true;
					}
				}
			}
			return false;
		}
		
		
		
		public boolean isDateValid(String strdate,String DATE_FORMAT) 
		{
			
			if(DATE_FORMAT.trim().equals("dd-MMM-yyyy")) {
				if(strdate.trim().length()>11 || strdate.trim().length()<11) {
					 return false;
				}
			}
			else {
				if(strdate.trim().length()>10 || strdate.trim().length()<10) {
					 return false;
				}
			}
			
	        try {
	            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
	            df.setLenient(false);
	            Date date = df.parse(strdate);
	            return true;
	        } catch (Exception e) {
	        	e.printStackTrace();
	            return false;
	        }
		}
		
		public String validateConfigsNames(List<Config> configlist, String configName,String columnName) {
			String str = "";
			if(!configName.trim().equalsIgnoreCase("NA")){
				if(!validateConfig(configlist, configName)) {
					str = " "+columnName +" - "+configName+" does not exist or not match in the system!. ";
				}
			}
			return str;
		}
		
		public String validateCategoryName(List<ConfigCategory> categoryList, String categoryName, String columnName) {
			String str = "";
			if(!categoryName.trim().equalsIgnoreCase("NA")){
				if(!validateConfigCategory(categoryList, categoryName)) {
					str = columnName+ " - "+categoryName+" does not exist or not match in the system!. ";
				}
			}
			return str;
		}
		
		public String validateCategoryTypeNames(List<Type> typeEntitylist, String categoryName, String typeName, String columnName) {
			String str = "";
			if(!typeName.trim().equalsIgnoreCase("NA")){
				if(!validateCategoryType(typeEntitylist, categoryName,typeName)) {
					str = columnName+ " - "+typeName+" does not exist or does not matched with "+categoryName+" in the system!. ";
				}
			}
			
			return str;
		}
		
		
		public String validateSalesPersonName(List<Employee> employeelist, String salesPersonName,String columnName) {
			String str = "";
			if(!salesPersonName.trim().equalsIgnoreCase("NA")){
				if(!validateSalesPerson(employeelist, salesPersonName)) {
					str = columnName+ " - "+salesPersonName+" does not exist or not matched in the system!. ";
				}
			}
			return str;
		}
		
		private boolean validateSalesPerson(List<Employee> employeelist, String salesperonName) {
			for(Employee salesPerson:employeelist){
				if(salesPerson.getFullname().trim().equals(salesperonName)){
					return true;
				}
			}
			return false;
		}

		public String validateCityName(List<City> globalCitylist, String cityName, String stateName,String columnName) {
			String str = "";
			if(!cityName.trim().equalsIgnoreCase("NA")){
				if(!validateCity(globalCitylist, cityName,stateName)) {
					str =  columnName+ " - "+cityName+" does not exist or not matched in the system!. ";
				}
			}
			return str;
		}
		
		public String validateLocalityName(List<Locality> globalLocalitylist, String localityName, String CityName,String columnName) {
			String str = "";
			if(!localityName.trim().equalsIgnoreCase("NA")){
				if(!validateLocality(globalLocalitylist, localityName,CityName)) {
					str = " "+columnName+ " - "+localityName+" does not exist or not matched in the system!. ";
				}
			}
			
			return str;
		}
		
		public String validateStateName(List<State> globalStatelist, String stateName, String countryName,String columnName) {
			String str = "";
			if(!stateName.trim().equalsIgnoreCase("NA")){
				if(!validateState(globalStatelist, stateName,countryName)) {
					str =" "+ columnName+ " - "+stateName+" does not exist or not matched in the system!. ";
				}
			}
			
			return str;
		}
		
		public String validateCountryName(List<Country> globalCountrylist, String countryName,String columnName) {
			String str = "";
			if(globalCountrylist!=null){
				if(!countryName.trim().equalsIgnoreCase("NA")){
					if(!validateCountry(globalCountrylist, countryName)) {
						str =" "+ columnName+ " - "+countryName+" does not exist or not match in the system!. ";
					}
				}
			}
			
			return str;
		}
		
		public String validateBranchName(List<Branch> branchlist, String branchName, String columnName) {
			String str = "";
			if(!branchName.trim().equalsIgnoreCase("NA")){
				if(!validateBranch(branchlist, branchName)) {
					str =" "+ columnName+ " - "+branchName+" does not exist or not match in the system!. ";
				}
			}
			return str;
		}
		
		private boolean validateBranch(List<Branch> branchlist, String branchName) {
			if(branchlist!=null){
				for(Branch branchEntity : branchlist) {
					if(branchEntity.getstatus() && branchEntity.getBusinessUnitName().trim().equals(branchName)) {
						return true;
					}
				}
			}
			return false;
		}

		private boolean validateCity(List<City> globalCitylist, String cityName, String stateName) {
			if(globalCitylist!=null){
				for(City cityentity : globalCitylist) {
					if(cityentity.getStateName().trim().equals(stateName.trim()) && 
							cityentity.getCityName().trim().equals(cityName.trim())) {
						return true;
					}
				}
			}
			return false;
		}

		private boolean validateLocality(List<Locality> globalLocalitylist, String localityName, String CityName) {
			if(globalLocalitylist!=null){
				for(Locality localityEntity : globalLocalitylist) {
					if(localityEntity.getCityName().trim().equals(CityName.trim()) && 
							localityEntity.getLocality().trim().equals(localityName.trim())) {
						return true;
					}
				}	
			}
			return false;
		}

		private boolean validateState(List<State> globalStatelist, String stateName, String countryName) {
			if(globalStatelist!=null){
				for(State stateEntity : globalStatelist) {
					if(stateEntity.getCountry().trim().equals(countryName.trim()) && 
							stateEntity.getStateName().trim().equals(stateName.trim())) {
						return true;
					}
				}
			}
			return false;
		}

		private boolean validateCountry(List<Country> globalCountrylist, String columnvalue) {
			if(globalCountrylist!=null){
				for(Country countryEntity : globalCountrylist) {
					if(countryEntity.getCountryName().trim().equals(columnvalue.trim())) {
						return true;
					}
				}
			}
			return false;
		}
		
		private boolean validateEmployee(List<Employee> listofemployee, String employeeName) {
			if(listofemployee!=null){
				for(Employee employeeEntity : listofemployee) {
					if(employeeEntity.getFullName().trim().equals(employeeName.trim())) {
						return true;
					}
				}
			}
			return false;
		}
		
		public String validateEmployeeName(List<Employee> employeelist, String salesPersonName,String columnName) {
			String str = "";
			if(employeelist!=null){
				if(!salesPersonName.trim().equalsIgnoreCase("NA")){
					if(!validateEmployee(employeelist, salesPersonName)) {
						 str = columnName+ " - "+salesPersonName+" does not exist or not matched in the system!. ";
					}
				}
			}
			return str;
		}

		public String validateEmailId(String columnName, String email) {
			String str="";
			if(!email.equalsIgnoreCase("NA")){
				logger.log(Level.SEVERE,"email id"+(!isValidateEmailID(email)));
				if(!isValidateEmailID(email)){
					str = " Invalid email address in "+columnName+" column. ";
				}
			}
			return str;
		 }
		
		
		public boolean isValidateEmailID(String email) {
			
			String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		      return email.matches(regex);
		 }
		
		public String validateDuplicateEmployeeName(List<Employee> employeelist, String employeeName,String columnName,String empAdharNumber) {
			String str = "";
			if(employeelist!=null){
				if(!validationforemployee(employeelist, employeeName, empAdharNumber)) {
					 str =" "+ columnName+ " - "+employeeName+" Already exist in the system. Employee name with same name and same employee adhar number not allowed. ";
				}
			}
			return str;
		}
		
		private boolean validationforemployee(List<Employee> listofemployee,String EmployeeName, String empadharNumber) {
			boolean value = true;
			long adharNumber = 0;
			if(listofemployee!=null){
				if(!empadharNumber.equalsIgnoreCase("NA")){
					try {
						adharNumber = Long.parseLong(empadharNumber);
					} catch (Exception e) {
						return false;
					}
				}
				for (int j = 0; j < listofemployee.size(); j++) {

					if ((listofemployee.get(j).getFullName().trim()
							.equalsIgnoreCase(EmployeeName.trim()) && adharNumber==listofemployee.get(j).getAadharNumber())) {

						return false;
					}
				}
			}
			return value;
		}

		public String validateMandatory(String columnValue, String columnName) {
			String str ="";
			if(columnValue.trim().equalsIgnoreCase("NA")){
				str = " "+columnName+" is Mandatory! It should not be NA. ";
			}
			return str;
			
		}

		public boolean validateEmpDepartment(List<Department> departmentlist, String departmentName) {
			if(departmentlist!=null){
				for(Department department : departmentlist){
					if(department.getStatus() && department.getDeptName().trim().equals(departmentName.trim())){
						return true;
					}
				}
			}
			return false;
			
		}
		public boolean validateEmpDepartmentStatus(List<Department> departmentlist, String departmentName) {
			if(departmentlist!=null){
				for(Department department : departmentlist){
					if(department.getStatus() && department.getDeptName().trim().equals(departmentName.trim())){
						return true;
					}
				}
			}
			return false;
			
		}

		public String validateDateFormat(String datevalue, String dateformat, String columnName) {
			String error="";
			if(!datevalue.trim().equalsIgnoreCase("NA")){
				try {
					SimpleDateFormat fmt = new SimpleDateFormat(dateformat);
					Date date  = fmt.parse(datevalue.trim());
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					error +=" "+ columnName+" Column Invalid date format. Date format must be "+dateformat +". ";
				}
			}
			
			return error;
		}
		
		public String validateEmployeeMartialStatus(ArrayList<String> martialstatus, String columnValue, String columnName) {
			String str ="";
			if(!columnValue.trim().equalsIgnoreCase("NA")){
				if(!validateEmployeeMartialStatus(martialstatus, columnValue)) {
					str += " "+columnName +" - "+columnValue+" does not exist in the system. Or not match! ";
				}

			}
			return str;
			
		}
		
		private boolean validateEmployeeMartialStatus(ArrayList<String> martialstatus, String columnvalue) {
			for(String strName : martialstatus) {
				if(strName.trim().equals(columnvalue.trim())) {
					return true;
				}
			}
			return false;
		}

		public String ValidateStatus(String columnName, String columnValue) {
			if(columnValue.trim().equalsIgnoreCase("Yes") || columnValue.trim().equalsIgnoreCase("No")){
				return "";
			}
			return " Please add status column value either yes or no!";
		}
		
		public String validateDate(String datevalue, String dateformat, String columnName){
			String error ="";
			if(!datevalue.trim().equalsIgnoreCase("NA") && !datevalue.trim().equalsIgnoreCase("N A")){
				if(!isDateValid(datevalue,dateformat)) {
					error +=" "+ columnName+" Column Invalid date. ";
				}
			}
			return error;
		}

		public void createnewCities(List<City> globalCitylist,ArrayList<City> newcitylist, long companyId) {
			
			if(globalCitylist!=null && newcitylist!=null){
				ArrayList<City> updatednewcitylist = new ArrayList<City>();
				updatednewcitylist.addAll(newcitylist);
				
				
				for(City newcityEntity : newcitylist){
					for(City cityEntity : globalCitylist){
						if(cityEntity.getCityName().trim().equals(newcityEntity.getCityName().trim()) && cityEntity.getStateName().trim().equals(newcityEntity.getStateName().trim())){
							updatednewcitylist.remove(newcityEntity);
						}
					}
				}
					
			
				if(updatednewcitylist.size()>0){
					for(City cityEntity : updatednewcitylist){
						cityEntity.setCompanyId(companyId);
						cityEntity.setCityName(cityEntity.getCityName());
						cityEntity.setState(cityEntity.getStateName());
						cityEntity.setStatus(true);
					}
					ArrayList<SuperModel> supermodelist = new ArrayList<SuperModel>();
					supermodelist.addAll(updatednewcitylist);
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(supermodelist);
					logger.log(Level.SEVERE, "updatednewcitylist created size"+updatednewcitylist.size());
				}
				
			}
			
		}

		public List<Category> loadproductCategory(long companyId) {
			List<Category> productCategory = ofy().load().type(Category.class).filter("companyId", companyId).list();
			return productCategory;
		}

		public String validateServiceProduct(List<ServiceProduct> listofserviceproduct, String productCode, String columnName) {
			String str ="";
			if(!productCode.trim().equalsIgnoreCase("NA")){
				if(!validateserviceProduct(listofserviceproduct, productCode)) {
					str += columnName +" - "+productCode+" Already exit! ";
				}

			}
			return str;
		}

		private boolean validateserviceProduct(	List<ServiceProduct> listofserviceproduct, String productCode) {

			for(ServiceProduct serviceprod : listofserviceproduct ){
				if(serviceprod.getProductCode().trim().equals(productCode)){
					return false;
				}
			}
			
			return true;
		}

		public String validateServiceProductCategory(List<Category> productCategorylist, String columnName, String productCategory) {
			String str = "";
			boolean flag = false;
			if(!productCategory.trim().equalsIgnoreCase("NA")){
				for(Category categoryEntity : productCategorylist){
					if(categoryEntity.getCatName().trim().equals(productCategory.trim()) && categoryEntity.isServiceCategory()){
						flag = true;
						break;
					}
				}
			}
			if(flag){
				
			}
			else{
				str += columnName +" - "+productCategory+" does not exist in the system. Or not matched! ";

			}
			return str;
		}

		public String validateTaxName(List<TaxDetails> taxlist, String columnName,	String taxName) {
			String str ="";
			if(!taxName.trim().equalsIgnoreCase("NA")){
				if(!validateTaxName(taxlist,taxName)){
					str += columnName +" - "+taxName+" does not exist in the system. Or not matched! ";
				}
			}
			return str;
		}

		private boolean validateTaxName(List<TaxDetails> taxlist, String taxName) {
			for(TaxDetails taxEntity : taxlist){
				if(taxEntity.getTaxChargeName().trim().equals(taxName.trim())){
					return true;
				}
			}
			return false;
		}


		public String validateYesNo(String columnName, String columnValue) {
			if(columnValue.trim().equalsIgnoreCase("Yes") || columnValue.trim().equalsIgnoreCase("No")){
				return "";
			}
			return "Please add "+columnName+" column value either yes or no! ";
		
		}

		public TaxDetails gettaxDetails(List<TaxDetails> taxlist, String taxName) {
			for(TaxDetails taxEntity : taxlist){
				if(taxEntity.getTaxChargeName().trim().equals(taxName)){
					return taxEntity;
				}
			}
			return null;
			
		}

		public String validateDuplicateIntheExcel(HashMap<String, Integer> hashduplicate, String columnName,
				String columnValue) {
			String error = "";
			if(!columnValue.trim().equalsIgnoreCase("na")){
				
				if(hashduplicate!=null && hashduplicate.size()!=0){
					if(hashduplicate.containsKey(columnValue.trim())){
						int num = hashduplicate.get(columnValue.trim());
					  num++;
					  hashduplicate.put(columnValue.trim(),num);
					  if(num>1){
						 error += "Duplicate "+ columnName+ " found in the excel please check the excel. Duplicate "+columnName+" not allowed. ";
					  }
					}
					else{
						hashduplicate.put(columnValue.trim(), 1);
					}
				}
				else{
					hashduplicate.put(columnValue.trim(), 1);
				}
			}

			return error;
		}

		public String validateItemProduct(List<ItemProduct> listofitemproduct, String productCode, String columnName) {
			String str ="";
			if(!productCode.trim().equalsIgnoreCase("NA")){
				if(!validateItemProduct(listofitemproduct, productCode)) {
					str += columnName +" - "+productCode+" Already exit! ";
				}

			}
			return str;
		}

		private boolean validateItemProduct(List<ItemProduct> listofitemproduct, String productCode) {
			for(ItemProduct itemprod : listofitemproduct ){
				if(itemprod.getProductCode().trim().equals(productCode)){
					return false;
				}
			}
			
			return true;
		}
		
		public String validateItemProductCategory(List<Category> productCategorylist, String columnName, String productCategory) {
			String str = "";
			boolean flag = false;
			if(!productCategory.trim().equalsIgnoreCase("NA")){
				for(Category categoryEntity : productCategorylist){
					if(categoryEntity.getCatName().trim().equals(productCategory.trim()) && categoryEntity.getIsSellCategory()){
						flag = true;
						break;
					}
				}
			}
			if(flag){
				
			}
			else{
				str += columnName +" - "+productCategory+" does not exist in the system. Or not matched! ";

			}
			return str;
		}

		public String validateCellNo(String columnName, String CellNumber) {
			if(CellNumber.length()!=10){
				return columnName +" should be 10 digit! ";
			}
			return "";
		}

		public String validateServiceTime(String ColumnName, String serviceTime) {
			if(!serviceTime.trim().equalsIgnoreCase("NA")){
				
				try {
				
				String []servicetime = serviceTime.split(":");
				String serTime = "";
				if(servicetime.length>0){
					serTime = servicetime[0];
					Integer.parseInt(serTime);
				}
				if(servicetime.length>1){
					System.out.println("servicetime[1] =="+servicetime[1]);
					Integer.parseInt(servicetime[1]);
					serTime += ":"+servicetime[1]; 
				}
				if(servicetime.length>2){
					serTime += servicetime[2]; 
					System.out.println("servicetime[2] "+servicetime[2]);
					if(servicetime[2].trim().equals("AM") || servicetime[2].trim().equals("PM") ){
					}
					else {
						logger.log(Level.SEVERE, "AM PM "+servicetime[2]);
						return "Service Time should be in HH:MM:AM/PM format";
					}
				}
				} catch (Exception e) {
					logger.log(Level.SEVERE, "EXcep"+e.getCause());
					return "Service Time should be in HH:MM:AM/PM format";
				}

			}
			return "";
		}

		public String validateEmpAdharNumber(List<Employee> listofemployee,	String columnName, String columnValue) {
			if(!columnValue.trim().equalsIgnoreCase("NA")){
				int addharnumber =0;
				try {
					addharnumber = Integer.parseInt(columnValue);
				} catch (Exception e) {
					// TODO: handle exception
				}
				for(Employee emp : listofemployee){
					if(emp.isStatus() && emp.getAadharNumber()!=0 && addharnumber!=0 && emp.getAadharNumber()==addharnumber){
						return columnName +"-" +addharnumber + " already exist with employee name - "+emp.getFullname();
					}
				}
			}
			return "";
		}

		public String validateFormulla(String columnName, String columnValue) {
			if(!columnValue.trim().equalsIgnoreCase("NA") && !columnValue.trim().equalsIgnoreCase("N A")){
				if(columnValue.trim().contains("=")){
					return "Formulla value not allowed in the column - "+columnName;
				}
			}
			return "";
		}
		
		public static String getTinyUrl(String feedbackUrl, long companyId){
   			logger.log(Level.SEVERE,"getTinyUrl "+feedbackUrl);
   			
   		  StringBuilder sbPostData; 
   			if(feedbackUrl.contains("&")) {
   	   			logger.log(Level.SEVERE,"feedbackUrl contains & "+feedbackUrl);

   				String str[] = feedbackUrl.split("&");
   				String urlpart = str[0];
   	   			logger.log(Level.SEVERE,"urlpart"+urlpart);
   	   			
   		        sbPostData= new StringBuilder(urlpart);

   		        for(int i=1; i<str.length;i++) {
   		        	sbPostData.append(URLEncoder.encode("&"));
   	   		        sbPostData.append(URLEncoder.encode(str[i]));

   		        }
   		        logger.log(Level.SEVERE,"sbPostData.toString() "+sbPostData.toString());

   		        
   			}
   			else {
   				sbPostData= new StringBuilder(feedbackUrl);
   			}
   			
	        String encodedfeedbackUrl = sbPostData.toString();
	        logger.log(Level.SEVERE,"encodedtinyUrl "+encodedfeedbackUrl);


   			final String tinyUrl="https://api.tinyurl.com/create?url="+encodedfeedbackUrl;
   			
   			

   			String updatedTinyUrl="";
   			
   			try{
	   			URL url = null;
	   			try {
	   				url = new URL(tinyUrl);

	   			} catch (MalformedURLException e) {
	   				logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   			
	   	        HttpURLConnection con = null;
	   			try {
	   				con = (HttpURLConnection) url.openConnection();
	   			} catch (IOException e) {
	   				logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   	        con.setDoInput(true);
	   	        con.setDoOutput(true);
	   	        
	   	        if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", AppConstants.PC_ENABLETINYURLACCOUNTNO2, companyId)){
		   	        con.setRequestProperty("Authorization","Bearer "+AppConstants.TinyURLAccount2AuthCode);
	   	        }
	   	        else{
		   	        con.setRequestProperty("Authorization","Bearer "+"A3iwWeOUwmSve1a8RZGjqTDULBPIaL9KZ0NSgD1dIVsuKSbxVYO1cnSfo6vt");
	   	        }
	
	   	        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	   	        
	   	        try {
	   				con.setRequestMethod("POST");
	   			} catch (ProtocolException e) {
	   				// TODO Auto-generated catch block
	   				logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   	        OutputStream os = null;
	   			try {
	   				os = con.getOutputStream();

	   			} catch (IOException e) {
	   				// TODO Auto-generated catch block
	   				logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   	        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
	//   	        try {
	//   				writer.write(createJSONObject(entityAI));
	//   			} catch (IOException e) {
	//   				// TODO Auto-generated catch block
	//   				logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
	//   				e.printStackTrace();
	//   			}
	   	        
	   	        try {
	   				writer.flush();
	   			} catch (IOException e) {
	   				// TODO Auto-generated catch block
	   				logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   	        
	   	        InputStream is = null;
	   			try {
	   				is = con.getInputStream();
	   				
	   			} catch (IOException e) {
	   				// TODO Auto-generated catch block
	   				logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   	        BufferedReader br = new BufferedReader(new InputStreamReader(is));
	   	        
	   	        String jsonString = "";
	   	        String temp;
	   	        try {
	   				while ((temp = br.readLine()) != null) {
	   					jsonString = jsonString + temp;
	   				}
	   				logger.log(Level.SEVERE,"data data::::::::::"+jsonString);
	   			} catch (IOException e) {
	   				// TODO Auto-generated catch block
	   				logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
	   				e.printStackTrace();
	   			}
	   	        
	   	        
	   	       
	   	        logger.log(Level.SEVERE, "jsonString:::" + jsonString);
	   	        JSONObject mainObject = null;
	   	        try {
	   	        	mainObject = new JSONObject(jsonString);
	   	        } catch (JSONException e) {
	   	        	e.printStackTrace();
	   	        }

				
				String dataString=mainObject.optString("data");
				
				JSONObject obj = null;
				try {
					obj = new JSONObject(dataString);
				} catch (JSONException e) {
					e.printStackTrace();
				}
	   	        updatedTinyUrl=obj.optString("tiny_url");
	   	        
   			}catch(Exception e){
   				updatedTinyUrl=feedbackUrl;
   			}
   			logger.log(Level.SEVERE, "updatedTinyUrl:::" + updatedTinyUrl);
   			return updatedTinyUrl;
   			
   		}
		public List<Config> loadallConfigsData(int type, long companyId) {
			List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
					.filter("type", type).list();
			return configEntity;
		}
		
		public String getFirstName(String Name) {
			try {
				String [] customerName = Name.split("\\s+");
				logger.log(Level.SEVERE, "First Name" + customerName[0]);

				return customerName[0];
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return "";
		}
		
		
		/**
		 * @author Vijay Date :- 12-07-2021
		 * Des :- I have created new method for encoding and decoding with Base64 for Payment Gateway
		 * This method is reusable method
		 */
		public String decodeTextBase64(String text) {
			/** below code support for java 8 so for local use comment below code and for service uncomment below code ***/
			try {
				java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
				String str = encoder.encodeToString(text.getBytes()); 
				return str;
			} catch (Exception e) {
				return text;
			}
			
//			return text;

		}
		
		public String encodeTextUTF8(String text) {
			/** below code support for java 8 so for local use comment below code and for service uncomment below code ***/
			try {
				java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();  
				String str = new String(decoder.decode(text));  
				return str;
			} catch (Exception e) {
				return text;
			}
//			return text;
		}
		/**
		 * ends here
		 */
		
		public boolean validateCustomerDNDStatus(int customerId, long companyId){
			
			Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId)
								.filter("count", customerId).first().now();
			logger.log(Level.SEVERE, "customerEntity"+customerEntity);
			if(customerEntity!=null){
				logger.log(Level.SEVERE, "customerEntity.isSmsDNDStatus()"+customerEntity.isSmsDNDStatus());
				if(customerEntity.isSmsDNDStatus()){
					return true;
				}
			}
			return false;
		}
		
		public boolean validateCustomerDNDStatus(long customerCellNo, long companyId, boolean flag){
			
			Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId)
								.filter("contacts.cellNo1", customerCellNo).first().now();
			logger.log(Level.SEVERE, "customerEntity"+customerEntity);
			if(customerEntity!=null){
				logger.log(Level.SEVERE, "customerEntity.isSmsDNDStatus()"+customerEntity.isSmsDNDStatus());
				if(customerEntity.isSmsDNDStatus()){
					return false;
				}
			}
			return true;
		}
		
		
		public void sendEmail(long companyId, String strSubject, String emailbody) {

			ArrayList<String> toEmailList = new ArrayList<String>();
			toEmailList.add("support@evasoftwaresolutions.com");
			try {
				logger.log(Level.SEVERE,"Inside Email Method");
				Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				Email email = new Email();
				
				email.sendEmail(strSubject, emailbody, toEmailList, null, comp.getEmail());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		public String getMobileNoWithCountryCode(String mobileNo, String countryName, long companyId) {
			if(countryName!=null && !countryName.equals("")){
				Country countryEntity = ofy().load().type(Country.class).filter("companyId", companyId).filter("countryName", countryName).first().now();
				if(countryEntity!=null) {
					if(countryEntity.getCountryCode()!=null && !countryEntity.getCountryCode().equals("")) {
						String mobileNoWithCountryCode = countryEntity.getCountryCode()+mobileNo;
						return mobileNoWithCountryCode;
					}
					else {
						return mobileNo;
					}
				}
				return mobileNo;
			}
			return mobileNo;
			
		}
		
		public int getValueWithMinMaxRoundUp(double value){
			logger.log(Level.SEVERE,"inside round up method value "+value);
			System.out.println("inside round up method value "+value);
			String strvalue = value+"";
			try {
				
			String[] stringvalue = strvalue.split("\\.");
			if(stringvalue.length>0) {
				if(!stringvalue[1].startsWith("0")){
					int intvalue = (int) value;
					intvalue++;
					logger.log(Level.SEVERE, "final rounded value "+intvalue);
					return intvalue;
				}
				else{
					int intvalue = (int) value;
					logger.log(Level.SEVERE, "else block final rounded value "+intvalue);
					return intvalue;
				}
			}
			else {
				int intvalue = (int) value;
				logger.log(Level.SEVERE, "final else block final rounded value "+intvalue);
				return intvalue;
			}
			
			} catch (Exception e) {
				
			}
			
			int intvalue = (int) value;
			logger.log(Level.SEVERE, "final final rounded value"+intvalue);

			return intvalue;
			
		
		}
		
		public String getstringWithoutSpecialCharacter(String value){
			try {
				String data = value.replaceAll("^\"|\"$", "");
				data = data.replaceAll("\"", "");
				logger.log(Level.SEVERE, "After reomving double quotes chars value "+value);
				return data;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return value;
		}
		
		/**Added by Sheetal : 09-12-2021
		 * Des: Creating common method for css**/
		
		public static String getCss(String str){
		          String strcolor = "<head>" +
	                "<style type=\"text/css\">" +
	                "  .btn {\r\n" + 
	                "  background: #3498db;\r\n" + 
	                "  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: -moz-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: -ms-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: -o-linear-gradient(top, #3498db, #2980b9);\r\n" + 
	                "  background-image: linear-gradient(to bottom, #3498db, #2980b9);\r\n" + 
	                "  -webkit-border-radius: 28;\r\n" + 
	                "  -moz-border-radius: 28;\r\n" + 
	                "  border-radius: 28px;\r\n" + 
	                "  font-family: Arial;\r\n" + 
	                "  color: #ffffff;\r\n" + 
	                "  font-size: 20px;\r\n" + 
	                "  padding: 10px 20px 10px 20px;\r\n" + 
	                "  border: solid #75787a 2px; \r\n"+
	                "  text-decoration: none;\r\n" + 
	                "}\r\n" + 
	                "\r\n" + 
	                ".btn:hover {\r\n" + 
	                "  background: #3cb0fd;\r\n" + 
	                "  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);\r\n" + 
	                "  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);\r\n" + 
	                "  text-decoration: none;\r\n" + 
	                "}" +
	                "</style>" +
	                "</head>";
			
			return strcolor;
			
		}
		
		
		public double getLastTwoDecimalOnly(double AmountIncludingmoredecimal) {

				String strtotalamtincludingtax = AmountIncludingmoredecimal+"";
				if(strtotalamtincludingtax.contains(".")){
					String [] includingTaxAmtString = strtotalamtincludingtax.split("\\.");
					String firstDecimal = includingTaxAmtString[0];
					String secondDecimal = includingTaxAmtString[1];
					String twoDecimal = null;
					for(int p=0;p<secondDecimal.length();p++){
						if(p==0){
							twoDecimal = secondDecimal.charAt(p)+"";
						}
						else{
							twoDecimal += secondDecimal.charAt(p)+"";
						}
						if(p==1){
							break;
						}
					}
					System.out.println("twoDecimal "+twoDecimal);
					if(twoDecimal!=null){
						firstDecimal = includingTaxAmtString[0] +"."+twoDecimal;
						return Double.parseDouble(firstDecimal);
					}
					else{
						return AmountIncludingmoredecimal;
					}
				}
				return AmountIncludingmoredecimal;

			}
		
		public Company loadCompany(long companyId){
			
			Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			
			return companyEntity;
		}
		
		
		public SalesLineItem updateTaxesDetails(SalesLineItem saleslineitem, Company companyEntity, List<TaxDetails> taxeslist, String customerBillingaddresState ) {

			try {

					if( (!saleslineitem.getVatTax().getTaxConfigName().equals("") ||
							!saleslineitem.getServiceTax().getTaxConfigName().equals("")) ){
						logger.log(Level.SEVERE,"customerBillingaddresState "+customerBillingaddresState);
						logger.log(Level.SEVERE,"company address "+companyEntity.getAddress().getState());

						/**
						 * @author Vijay Date :- 28-07-2021
						 * Des :- added if condition auto GST should not work if tax is VAT so added if conditions
						 */
						logger.log(Level.SEVERE, "First Condition =="+(saleslineitem.getVatTax().getTaxPrintName()!=null && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("NA")));
						logger.log(Level.SEVERE, "2nd OR Condition == "+(saleslineitem.getServiceTax().getTaxPrintName()!=null && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("NA")));
						
						if( ( (saleslineitem.getVatTax().getTaxPrintName()!=null && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getVatTax().getTaxPrintName().equalsIgnoreCase("NA")) ||
								(saleslineitem.getServiceTax().getTaxPrintName()!=null && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("") && !saleslineitem.getServiceTax().getTaxPrintName().equalsIgnoreCase("NA"))) ){
							
							logger.log(Level.SEVERE,"VAT Condition =="+(!saleslineitem.getVatTax().getTaxPrintName().trim().equalsIgnoreCase("VAT") && !saleslineitem.getServiceTax().getTaxPrintName().trim().equalsIgnoreCase("NA")));
							logger.log(Level.SEVERE, "second VAT Condition "+(!saleslineitem.getServiceTax().getTaxPrintName().trim().equalsIgnoreCase("VAT") && !saleslineitem.getVatTax().getTaxPrintName().trim().equalsIgnoreCase("NA")));

							if(saleslineitem.getVatTax().getTaxPrintName().trim().equalsIgnoreCase("VAT") || saleslineitem.getServiceTax().getTaxPrintName().trim().equalsIgnoreCase("VAT")) {
								
							}
							else {
							
							logger.log(Level.SEVERE,"Inside Auto GST logic ");
							if(customerBillingaddresState!=null && !customerBillingaddresState.equals("") 
									&& companyEntity!=null && companyEntity.getAddress().getState()!=null && !companyEntity.getAddress().getState().equals("") ){
								
								if(!customerBillingaddresState.trim().equalsIgnoreCase(companyEntity.getAddress().getState().trim())){
									logger.log(Level.SEVERE,"For different State");

									if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
										saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
									}
									if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
										saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
									}
									
									if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
										Tax tax1 = new Tax();
										double tax1percentage = 0;
										if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getVatTax().getTaxPrintName().trim().equals("SGST")){
												 if(saleslineitem.getServiceTax()!=null ){
													 tax1percentage = saleslineitem.getVatTax().getPercentage() + saleslineitem.getServiceTax().getPercentage();
												 }
												 else{
													 tax1percentage = saleslineitem.getVatTax().getPercentage();
												 }
											 }
											 else{
												 tax1percentage = saleslineitem.getVatTax().getPercentage();
											 }
											 tax1 =	 getTaxDetails("IGST",taxeslist,tax1percentage);
										 }
										 else{
											 tax1 =	 getTaxDetails("NA",taxeslist,0);
										 }
										 
										 if(tax1!=null){
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);
											 saleslineitem.setVatTaxEdit(tax1.getTaxConfigName());
										 }
										 else{
											 tax1 =	 getTaxDetails("NA",taxeslist,0);
											 saleslineitem.setVatTax(tax1);
											 saleslineitem.setVatTaxEdit(tax1.getTaxConfigName());
										 }
										 Tax tax2 = new Tax();
										 tax2 =	 getTaxDetails("NA",taxeslist,0);
										 saleslineitem.setServiceTax(tax2);
										 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());
									}
									if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
		
									 System.out.println("tax 2 started here");
									 double tax2percentage = 0;
									 Tax tax2 = new Tax();
									 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
										 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("CGST") || saleslineitem.getServiceTax().getTaxPrintName().trim().equals("SGST")){
											 if(saleslineitem.getVatTax()!=null ){
												 tax2percentage = saleslineitem.getServiceTax().getPercentage() + saleslineitem.getVatTax().getPercentage();
											 }
											 else{
												 tax2percentage = saleslineitem.getVatTax().getPercentage();
											 }
										 }
										 else{
											 tax2percentage = saleslineitem.getVatTax().getPercentage();
										 }
										 tax2 =	 getTaxDetails("IGST",taxeslist,tax2percentage);
									 }
									 else{
										 tax2 =	 getTaxDetails("NA",taxeslist,0);
									 }
									 if(tax2!=null){
										 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
										 saleslineitem.setServiceTax(tax2);
										 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());
									 }
									 else{
										 System.out.println("tax 2 ");
										 tax2 =	 getTaxDetails("NA",taxeslist,0);
										 saleslineitem.setServiceTax(tax2);
										 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

									 }
									 Tax tax = new Tax();
									 tax =	 getTaxDetails("NA",taxeslist,0);
									 saleslineitem.setVatTax(tax);
									 saleslineitem.setVatTaxEdit(tax.getTaxConfigName());

							  	}	
										 
								}
								else{
									 System.out.println("else block 2 else");
									 logger.log(Level.SEVERE, "for same state");
									 if(saleslineitem.getRefproductmasterTax1()!=null && saleslineitem.getRefproductmasterTax1().getPercentage()!=0){
											saleslineitem.setVatTax(saleslineitem.getRefproductmasterTax1());
										}
										if(saleslineitem.getRefproductmasterTax2()!=null && saleslineitem.getRefproductmasterTax2().getPercentage()!=0){
											saleslineitem.setServiceTax(saleslineitem.getRefproductmasterTax2());
										}
									 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
										 Tax tax1 = new Tax();
										 double tax1percentage = 0;
										 if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getPercentage()!=0){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST")){
													 tax1percentage = saleslineitem.getVatTax().getPercentage()/2;
											 }
											 else{
												 tax1percentage = saleslineitem.getVatTax().getPercentage();
											 }
											 tax1 =	 getTaxDetails("CGST",taxeslist,tax1percentage);
										 }
										 else{
											 tax1 =	 getTaxDetails("NA",taxeslist,0);
										 }
										 if(tax1!=null){
											 if(saleslineitem.getVatTax().getTaxPrintName().trim().equals("IGST") && 
													 tax1.getTaxPrintName().trim().equals("CGST")){
												 Tax tax2 = new Tax();
												 tax2 =	 getTaxDetails("SGST",taxeslist,tax1.getPercentage());
												 logger.log(Level.SEVERE, "saleslineitem.getVatTax().isInclusive() "+saleslineitem.getVatTax().isInclusive());
												 tax2.setInclusive(saleslineitem.getVatTax().isInclusive());
												 saleslineitem.setServiceTax(tax2);
												 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

											 }
											 tax1.setInclusive(saleslineitem.getVatTax().isInclusive());
											 logger.log(Level.SEVERE, "saleslineitem.getVatTax().isInclusive()== "+saleslineitem.getVatTax().isInclusive());
											 saleslineitem.setVatTax(tax1);
											 saleslineitem.setVatTaxEdit(tax1.getTaxConfigName());

										 }
										 else{
											 tax1 =	 getTaxDetails("NA",taxeslist,0);
											 saleslineitem.setVatTax(tax1);
											 saleslineitem.setVatTaxEdit(tax1.getTaxConfigName());

											 Tax tax2 = new Tax();
											 tax2 =	 getTaxDetails("NA",taxeslist,0);
											 saleslineitem.setServiceTax(tax2);
											 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

										 }
										 
									 }
									 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){

										 double tax2percentage = 0;
										 Tax tax2 = new Tax();
										 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getPercentage()!=0){
											 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST")){
												 if(saleslineitem.getServiceTax()!=null ){
													 tax2percentage = saleslineitem.getServiceTax().getPercentage()/2;
												 }
											 }
											 else{
												 tax2percentage = saleslineitem.getServiceTax().getPercentage();
											 }
											 System.out.println("tax2percentage **&& "+tax2percentage);
											 tax2 =	 getTaxDetails("SGST",taxeslist,tax2percentage);
										 }
										 else{
											 tax2 =	 getTaxDetails("NA",taxeslist,0);
										 }
										 if(tax2!=null){
											 if(saleslineitem.getServiceTax().getTaxPrintName().trim().equals("IGST") && 
													 tax2.getTaxPrintName().trim().equals("SGST")){
												 Tax tax1 = new Tax();
												 tax1 =	 getTaxDetails("CGST",taxeslist,tax2.getPercentage());
												 logger.log(Level.SEVERE, "saleslineitem.getServiceTax().isInclusive() "+saleslineitem.getServiceTax().isInclusive());
												 tax1.setInclusive(saleslineitem.getServiceTax().isInclusive());
												 saleslineitem.setVatTax(tax1);
												 saleslineitem.setVatTaxEdit(tax1.getTaxConfigName());

											 }
											 tax2.setInclusive(saleslineitem.getServiceTax().isInclusive());
											 logger.log(Level.SEVERE, "saleslineitem.getServiceTax().isInclusive() == "+saleslineitem.getServiceTax().isInclusive());

											 saleslineitem.setServiceTax(tax2);
											 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

										 }
										 else{
											 tax2 =	 getTaxDetails("NA",taxeslist,0);
											 saleslineitem.setServiceTax(tax2);
											 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

											 Tax tax = new Tax();
											 tax =	 getTaxDetails("NA",taxeslist,0);
											 saleslineitem.setServiceTax(tax);
											 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

										 }
									 }
									 
								}
				
								
							}
						
						
					}
					}
						
				}
				else{
						System.out.println("NOn GST Applicable");
						 Tax tax1 = new Tax();
						 tax1 =	 getTaxDetails("NA",taxeslist,0);
						 if(tax1!=null){
							 saleslineitem.setVatTax(tax1);
							 saleslineitem.setVatTaxEdit(tax1.getTaxConfigName());
						 }
						 
						 Tax tax2 = new Tax();
						 tax2 =	 getTaxDetails("NA",taxeslist,0);
						 if(tax2!=null){
							 saleslineitem.setServiceTax(tax2);
							 saleslineitem.setServiceTaxEdit(tax2.getTaxConfigName());

						 }
						 
				}
						
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return saleslineitem;	

		}
			
		
		private Tax getTaxDetails(String taxNamePrintName,List<TaxDetails> globalTaxList, double percentage) {
			
			try {
			
				logger.log(Level.SEVERE, "taxNamePrintName"+taxNamePrintName);
				logger.log(Level.SEVERE, "percentage"+percentage);

				if(taxNamePrintName.trim().equalsIgnoreCase("NA") && percentage==0) {
					logger.log(Level.SEVERE, "inside NA Tax");

					for(TaxDetails taxDetails : globalTaxList){
						if(taxDetails.getTaxChargeStatus() && taxDetails.getTaxChargeName().trim().equalsIgnoreCase(taxNamePrintName.trim()) && percentage==taxDetails.getTaxChargePercent()){
							Tax tax = new Tax();
							tax.setPercentage(taxDetails.getTaxChargePercent());
							tax.setTaxConfigName(taxDetails.getTaxChargeName());
							tax.setTaxName(taxDetails.getTaxChargeName());
							tax.setTaxPrintName(taxDetails.getTaxPrintName());
							return tax;
						}
					}
				}
				else {
					logger.log(Level.SEVERE, "Normal Taxes");
					for(TaxDetails taxDetails : globalTaxList){
						if(taxDetails.getTaxChargeStatus() && taxDetails.getTaxPrintName().trim().equalsIgnoreCase(taxNamePrintName.trim()) && percentage==taxDetails.getTaxChargePercent()){
							Tax tax = new Tax();
							tax.setPercentage(taxDetails.getTaxChargePercent());
							tax.setTaxConfigName(taxDetails.getTaxChargeName());
							tax.setTaxName(taxDetails.getTaxChargeName());
							tax.setTaxPrintName(taxDetails.getTaxPrintName());
							return tax;
						}
					}
				}
			
			
			
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			return null;
		}
		
		
		
		public double getServiceValue(SalesLineItem item, String customerBranch, int serviceNo, Long companyId, boolean servicewisebillingFlag) {
			
			
			
			
			boolean serviceAmountflag = false;
			double serviceValue = 0;
			
			try {
				
			if(item.getCustomerBranchSchedulingInfo()!=null && item.getCustomerBranchSchedulingInfo().size()>0){
				ArrayList<BranchWiseScheduling> list = item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
				if(list != null && list.size() > 0){
					 for(BranchWiseScheduling obj : list){
						 if(obj.isCheck() && obj.getAmount()>0){
							 serviceAmountflag = true;
						}
					 }

				}
			}	
			
			logger.log(Level.SEVERE, "item.getProductCategory() =="+item.getProductCategory());
			Category category = null;
			double firstServicePercentage = 0;
				
			if(item.getProductCategory()!=null && !item.getProductCategory().equals("")){
//				category = ofy().load().type(Category.class).filter("companyId", companyId)
//						.filter("catName", item.getProductCategory()).first().now();
//				if(category==null){
					ServiceProduct serviceproduct = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
									.filter("productCode", item.getProductCode()).first().now();
					if(serviceproduct!=null){
						category = ofy().load().type(Category.class).filter("companyId", companyId)
								.filter("catName", serviceproduct.getProductCategory()).first().now();
						logger.log(Level.SEVERE, "category"+category);
						if(category!=null && category.getServiceValuePercentage()!=0){
							firstServicePercentage = category.getServiceValuePercentage();
						}
					}
//				}
//				else{
//					if(category.getServiceValuePercentage()!=0){
//						firstServicePercentage = category.getServiceValuePercentage();
//					}
//				}
			}
				
			logger.log(Level.SEVERE, "firstServicePercentage"+firstServicePercentage);

			
//			if(servicewisebillingFlag && firstServicePercentage==0) {
//				if(item.getArea()!=null&&!item.getArea().equals("")&&!item.getArea().equalsIgnoreCase("NA")){
//					double quantity=0;
//					try{
//						quantity=Double.parseDouble(item.getArea());
//					}catch(Exception e){
//						
//					}
//					
//					if(quantity>0){
//						logger.log(Level.SEVERE, "quantity "+quantity);
//						logger.log(Level.SEVERE, "item.getPrice() "+item.getPrice());
//						return item.getPrice(); 
//					}
//				}
//			}
			logger.log(Level.SEVERE, "firstServicePercentage "+firstServicePercentage);

			if(serviceAmountflag){
				logger.log(Level.SEVERE, "inside service amount condition ");
				ArrayList<BranchWiseScheduling> list = item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
				if(list != null && list.size() > 0){
					 for(BranchWiseScheduling obj : list){
						 if(obj.isCheck() && obj.getAmount()>0 && obj.getBranchName().equals(customerBranch)){
							
							 if(firstServicePercentage>0){
								 logger.log(Level.SEVERE, "inside Termite amount condition ");
								 logger.log(Level.SEVERE, "serviceNo"+serviceNo);
								 logger.log(Level.SEVERE, "serviceNo"+(serviceNo == 1));
								 if(firstServicePercentage==100) {
										if(serviceNo == 1) {
											serviceValue = obj.getAmount();
										}
										else {
											serviceValue = 0;
										}
										serviceValue = Double.parseDouble(String.format("%.2f",(serviceValue)));

										return serviceValue;
								 }
								 
								 if(item.getNumberOfServices()==1){
									 serviceValue = obj.getAmount();
								 }
								 else if(serviceNo == 1){
									 serviceValue = (obj.getAmount()/100)*firstServicePercentage;
									 logger.log(Level.SEVERE, "inside pecentage condition first Service serviceValue "+serviceValue);

								 }
								 else{
									 double firstServiceValue = (obj.getAmount()/100)*firstServicePercentage;
									 double remainingServiceAmount = obj.getAmount() - firstServiceValue;
									 int noofservice = item.getNumberOfServices()-1;
									 serviceValue =  remainingServiceAmount/noofservice;
									 logger.log(Level.SEVERE, "inside pecentage condition remaining Service serviceValue "+serviceValue);

								 }
							 }
							 else{
								 serviceValue =  obj.getAmount()/item.getNumberOfServices();
	 
							 }
						 }
					 }

				}
			}
			else{
				if(firstServicePercentage>0){
					 logger.log(Level.SEVERE, "inside Termite amount condition== ");
					 logger.log(Level.SEVERE, "serviceNo"+serviceNo);
					 logger.log(Level.SEVERE, "serviceNo"+(serviceNo == 1));
					 
					 if(firstServicePercentage==100) {
							if(serviceNo == 1) {
								serviceValue = item.getTotalAmount();
							}
							else {
								serviceValue = 0;
							}
							serviceValue = Double.parseDouble(String.format("%.2f",(serviceValue)));
							return serviceValue;
					 }
					 
					 if(item.getQty()>1) {
						 if(item.getNumberOfServices()==1) {
							 serviceValue = item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
							 serviceValue = Double.parseDouble(String.format("%.2f",(serviceValue)));
								logger.log(Level.SEVERE, "serviceValue "+serviceValue);
							 return serviceValue;
						 }
					 }
					 if((item.getNumberOfServices()*item.getQty())==1){
						 serviceValue = item.getTotalAmount();
					 }
					 else if(serviceNo == 1){
						 double totalAmount =  item.getTotalAmount() / item.getQty();
						 serviceValue = (totalAmount/100)*firstServicePercentage;
						 logger.log(Level.SEVERE, "inside condition first Service serviceValue "+serviceValue);

					 }
					 else{
						 double firstServiceValue = (item.getTotalAmount()/100)*firstServicePercentage;
						 double remainingServiceAmount = item.getTotalAmount() - firstServiceValue;
						 double noofservice = (item.getNumberOfServices()*item.getQty())-item.getQty();
						 int noofservices = (int) noofservice;
						 serviceValue =  remainingServiceAmount/noofservices;
						 logger.log(Level.SEVERE, "inside condition remaining Service serviceValue "+serviceValue);

					 }
				}
				else{
					serviceValue = item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
				}
			}
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			serviceValue = Double.parseDouble(String.format("%.2f",(serviceValue)));
			logger.log(Level.SEVERE, "serviceValue "+serviceValue);
			return serviceValue;
		}
		
		/**
		 * @author Ashwini Patil
		 * @since 26-04-2022
		 * To Load CompanyPayment from invoice or default 
		 */
		public CompanyPayment loadCompanyPaymentByDocument(String documentName,int documentId,Long companyId) {
			CompanyPayment comppayment = null;
			if(documentName.equalsIgnoreCase("Invoice")) {
				logger.log(Level.SEVERE,"in invoice doc companyid="+companyId);
				Invoice invoiceentity=ofy().load().type(Invoice.class)
						.filter("count", documentId)
						.filter("companyId", companyId).first()
						.now();
				if(invoiceentity.getPaymentMode()!=null && invoiceentity.getPaymentMode().trim().length()>0){
					List<String> paymentDt = Arrays.asList(invoiceentity.getPaymentMode().trim().split("/"));
					logger.log(Level.SEVERE,"in if payment mode="+invoiceentity.getPaymentMode());
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", companyId).first()
								.now();
						
					}
				}
								
			}
			if(documentName.equalsIgnoreCase("Contract")) {
				logger.log(Level.SEVERE,"in contract doc companyid="+companyId);
				Contract contractentity=ofy().load().type(Contract.class)
						.filter("count", documentId)
						.filter("companyId", companyId).first()
						.now();
				if(contractentity.getPaymentMode()!=null && !contractentity.getPaymentMode().equals("")) {
					List<String> paymentDt = Arrays.asList(contractentity.getPaymentMode().trim().split("/"));
					logger.log(Level.SEVERE,"in if payment mode contract="+contractentity.getPaymentMode());
					if(paymentDt.get(0).trim().matches("[0-9]+")) {
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", companyId).first()
								.now();
						
					}
				}
				
				
			}
			if(comppayment==null) {
				logger.log(Level.SEVERE,"in else");
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("companyId", companyId)
						.filter("paymentDefault", true).first().now();
			}
			logger.log(Level.SEVERE,"bank name="+comppayment.getPaymentBankName());
			return comppayment;
		}
		
		/**Added by Ashwini Patil : 28-04-2022
		 * Des : Creating Common method for sending company signature part through any email**/
		 public static String getCompanySignature(Company comp, Branch branch){
			 Company company=null;
			 if(branch!=null) {
//				 logger.log(Level.SEVERE,"In branch condition");
				 if(comp!=null)
					 company=ServerAppUtility.changeBranchASCompany(branch, comp);	
				 else {
					 Company comp1 = ofy().load().type(Company.class).filter("companyId", branch.getCompanyId()).first().now();						
					 company=ServerAppUtility.changeBranchASCompany(branch, comp1);							
				 }
			 }else if(comp!=null) {
//				 logger.log(Level.SEVERE,"In company condition");
				 company=comp;
			 }
			 
			 
			 if(company!=null) {
//				 logger.log(Level.SEVERE,"In company!=null");
			  String companySignature = "";
			  StringBuilder builder = new StringBuilder();
			  
				builder.append("<br><b>" + "Thanks & Regards" + "</b><br>");
				// Company Details
				builder.append("<br>");
				builder.append("<b>" + company.getBusinessUnitName() + "</b><br>");
				builder.append("<table>");
		
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("Address : ");
				builder.append("</td>");
				builder.append("<td>");
				if (!company.getAddress().getAddrLine2().equals("")
						&& company.getAddress().getAddrLine2() != null)
					builder.append(company.getAddress().getAddrLine1() + " "
							+ company.getAddress().getAddrLine2() + " "
							+ company.getAddress().getLocality());
				else
					builder.append(company.getAddress().getAddrLine1() + " "
							+ company.getAddress().getLocality());
				builder.append("</td>");
				builder.append("</tr>");
		
				builder.append("<tr>");
				builder.append("<td>");
				builder.append("</td>");
				builder.append("<td>");
				if (!company.getAddress().getLandmark().equals("")
						&& company.getAddress().getLandmark() != null)
					builder.append(company.getAddress().getLandmark() + ","
							+ company.getAddress().getCity() + ","
							+ company.getAddress().getPin()
							+ company.getAddress().getState() + ","
							+ company.getAddress().getCountry());
				else
					builder.append(company.getAddress().getCity() + ","
							+ company.getAddress().getPin() + ","
							+ company.getAddress().getState() + ","
							+ company.getAddress().getCountry());
				builder.append("</td>");
				builder.append("</tr>");
				
				if(company.getCellNumber1()!=null) {
					builder.append("<tr>");
					builder.append("<td>");
					builder.append("Phone No ");
					builder.append("</td>");
					builder.append("<td>");
					builder.append(": "+company.getCellNumber1());
					builder.append("</td>");
					builder.append("<tr>");				
				}
				if(company.getEmail()!=null&&!company.getEmail().equals("")) {
					builder.append("<tr>");
					builder.append("<td>");
					builder.append("Email ");
					builder.append("</td>");
					builder.append("<td>");
					builder.append(": "+company.getEmail());
					builder.append("</td>");
					builder.append("</tr>");
				}
				if(company.getWebsite()!=null&&!company.getWebsite().equals("")) {
					builder.append("<tr>");
					builder.append("<td>");
					builder.append("Website ");
					builder.append("</td>");
					builder.append("<td>");
					builder.append(": "+company.getWebsite());
					builder.append("</td>");
					builder.append("</tr>");				
				}
		
				
		
				builder.append("</tr>");
				builder.append("</table>");

				companySignature = builder.toString();
				
				
//				logger.log(Level.SEVERE,"returning companySignature"+companySignature);
				return companySignature;
			 }
			 return "";
			  
		  }
		 
		 
		 /**
			 * @author Vijay Date :- 21-05-2022
			 * Des:- Common method to display attendance report for an API
			 * we can use this method to display attendance report.
			 * 
			 */
			
			public ArrayList<AttendanceBean> getAttendaceBeanList(List<Attendance> attendanceList){
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				
				SimpleDateFormat hhMMForamt = new SimpleDateFormat("HH:mm");

				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				Date today = new Date();
				
			    boolean actionFlag=false;
			    boolean workingHourFlag = false;
			    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "ShowAttendanceLabel", attendanceList.get(0).getCompanyId())) {
			    	actionFlag=true;
			    }
			    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "ShowWorkingHours", attendanceList.get(0).getCompanyId())) {
			    	workingHourFlag=true;
			    }

				boolean siteLocation = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "EnableSiteLocation", attendanceList.get(0).getCompanyId());
				
				Map<String, String> leaveMap = new HashMap<String, String>(); //Ashwini Patil

				List<LeaveType> leavetypelist = ofy().load().type(LeaveType.class).filter("companyId", attendanceList.get(0).getCompanyId()).list();
	    		logger.log(Level.SEVERE, "leavetypelist "+leavetypelist.size());
				for(LeaveType leavetype : leavetypelist) {
					leaveMap.put(leavetype.getName(),leavetype.getShortName());
				}

				ArrayList<AttendanceBean> list = new ArrayList<AttendanceBean>();
				Map<String , ArrayList<Attendance>> attMap = new HashMap<String ,ArrayList<Attendance>>();
				for(Attendance attendace : attendanceList){
					ArrayList<Attendance> attList = new ArrayList<Attendance>();
					String key ="";
					/**
					 * @author Anil
					 * @since 27-08-2020
					 */
					if(siteLocation){
						key = attendace.getProjectName()+"-"+attendace.getEmpId()+"-"+attendace.getMonth()+"-"+attendace.getSiteLocation();
					}else{
						key = attendace.getProjectName()+"-"+attendace.getEmpId()+"-"+attendace.getMonth();
					}
					
					if(attMap.containsKey(key)){
						attList = attMap.get(key);
						attList.add(attendace);
						attMap.put(key, attList);
					}else{
						attList.add(attendace);
						attMap.put(key, attList);
					}
//					if(multicyleFlag){
//						hsProject.add(attendace.getProjectName());
//					}
				}
//				Console.log("LOG 3 hsProject "+hsProject.size());
				
				
				for ( Map.Entry<String , ArrayList<Attendance>> entry : attMap.entrySet()) {
				    String key = entry.getKey();
				    ArrayList<Attendance> alist = entry.getValue();
				    AttendanceBean bean = new AttendanceBean();
				   
				    Attendance att = alist.get(0);
				    bean.setEmpId(att.getEmpId());
				    bean.setEmployeeName(att.getEmployeeName());
				    bean.setEmpDesignation(att.getEmpDesignation());
				    bean.setProjectName(att.getProjectName());
				    bean.setOvertimeType(att.getOvertimeType());
				    bean.setMonth(att.getMonth());
				    bean.setShift(att.getShift());
				    bean.setBranchName(att.getBranch());
				    bean.setSiteLocation(att.getSiteLocation());
				    bean.setCompanyId(att.getCompanyId());
				    
				    TreeMap<Date , Attendance> tMap = new TreeMap<Date ,Attendance>();
				    String hours = "";
//					DateTimeFormat fmt = DateTimeFormat.getFormat("dd/MM/yyyy");
				    Date date = new Date();
				   
				    int noOfDays=31;
				    if(alist.size()>0){
				    	/**
				    	 * @author Anil
				    	 * @since 04-08-2020
				    	 */
				    	if(alist.get(0).getStartDate()!=null){
				    		date =alist.get(0).getStartDate();
				    		noOfDays = (int) (ChronoUnit.DAYS.between(alist.get(0).getStartDate().toInstant(), alist.get(0).getEndDate().toInstant())+1);
				    		logger.log(Level.SEVERE, "noOfDays "+noOfDays);
				    		bean.setFromDate(alist.get(0).getStartDate());
				    		bean.setToDate(alist.get(0).getEndDate());
				    	}else{
				    		date = alist.get(0).getAttendanceDate();
				    		
				    		Calendar cal=Calendar.getInstance();
							cal.setTime(today);
							noOfDays = cal.getActualMaximum(Calendar.DATE);
							
				    		Calendar cal2=Calendar.getInstance();
				    		cal2.setTime(today);
				    		cal2.add(Calendar.DATE, +noOfDays);
							Date endDate=null;
							try {
								endDate=dateFormat.parse(dateFormat.format(cal.getTime()));
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
				    		bean.setFromDate(date);
				    		bean.setToDate(endDate);
				    	}
				    	
				    	Calendar cal=Calendar.getInstance();
						cal.setTime(date);
						try {
							date=dateFormat.parse(dateFormat.format(cal.getTime()));
						} catch (ParseException e) {
							e.printStackTrace();
						}
			    		logger.log(Level.SEVERE, "date "+date);

				    }
				    
				    int daysArray[]=new int[noOfDays];
				    for(int i=0;i<noOfDays;i++){
				    	try {
							tMap.put(dateFormat.parse(dateFormat.format(date)), new Attendance());
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
				    	daysArray[i]=date.getDate();
				    	Calendar cal=Calendar.getInstance();
						cal.setTime(date);
						cal.add(Calendar.DATE, 1);
						try {
							date=dateFormat.parse(dateFormat.format(cal.getTime()));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
				    }
				    daysArray=daysArray;
				    String daysArrStr="";
					for(int i=0;i<daysArray.length;i++){
						daysArrStr=daysArrStr+daysArray[i]+" ";
					}
		    		logger.log(Level.SEVERE, "daysArray "+daysArrStr);

					bean.setDaysArray(daysArray);
				    /**
				    * @author Anil,Date :24-01-2019
				    * Added total OT hours column
				    * 
				    */
				    double totalOtHours=0;		    
				    int totalWorkedDays=0; //Ashwini Patil
				    int totalLeaveDays=0;  //Ashwini Patil
				    for(Attendance a : alist){
			    		logger.log(Level.SEVERE, "a.getActionLabel()= "+a.getActionLabel()+"-"+ a.getAttendanceDate());

				    	if(a.isOvertime()){
				    		  bean.setOvertimeType(a.getOvertimeType());
				    		  totalOtHours+=a.getOvertimeHours();
				    	}
				    	if(a.isPresent()){
				    		if(a.getActionLabel().equals("P")) {
				    			totalWorkedDays++;}
				    		/**
				    		 * 
				    		 */
				    		if(actionFlag){
				    			hours = a.getActionLabel();
				    		}else{
				    			if(workingHourFlag){
				    				hours = a.getTotalWorkedHours()+"";
				    	    		logger.log(Level.SEVERE, "TotalWorkedHours "+a.getTotalWorkedHours() +"-"+hours);

				    			}else{
				    				double hh = Math.floor(a.getTotalWorkedHours());
				    	    		logger.log(Level.SEVERE, "value hh: "+hh);

				    				double mm = (a.getTotalWorkedHours() - hh) * 60.0;
				    	    		logger.log(Level.SEVERE, "value mm:"+mm);

				    				hours = hh+":"+Math.round(mm);
				    				try{
				    					hours = hhMMForamt.format( hhMMForamt.parse(hours));
				    				}catch(Exception e){
				    					
				    				}
				    	    		logger.log(Level.SEVERE, "value hh:mm"+hours);

				    			}
				    			if(a.getInTime() != null){
				    				hours += "   "+hhMMForamt.format(a.getInTime());
				    			}
				    			if(a.getOutTime() != null){
				    				hours += " - "+hhMMForamt.format(a.getOutTime());
				    			}
			    	    		logger.log(Level.SEVERE, "Hours : "+hours);

				    		}
				    	}else if(a.getLeaveType().equals("")){
				    		if(a.getInTime() != null&&a.getOutTime() == null) {
			    	    		logger.log(Level.SEVERE, "only intime");

				    			hours="IN \n"+hhMMForamt.format(a.getInTime());
				    		}else{
				    			hours = a.getActionLabel();
				    			if(!a.getActionLabel().equals("WO"))
				    				totalLeaveDays++;
				    		}
				    	}else{	
				    		try {
								
				    			/**
					    		 * Date : 04-10-2018 BY ANIL
					    		 * setting action label instead of leave name and holiday name
					    		 */

					    		String leaveshortname=leaveMap.get(a.getLeaveType());
					    		if(leaveshortname.equals("LWP"))
					    			hours="<html><font color=\"red\">"+leaveshortname+"</font></html>";
					    		else
					    			hours="<html><font color=\"orange\">"+leaveshortname+"</font></html>";
					    		
					    		if(!leaveshortname.equals("WO")){
					    			totalLeaveDays++;
				    	    		logger.log(Level.SEVERE, "totalLeaveDays in other than WO"+totalLeaveDays);

					    		}
					    			
					    		
							} catch (Exception e) {
								e.printStackTrace();
							}
				    		
				    	
				    		/**
				    		 * End
				    		 */
				    	}	

	    	    		try {
	        	    		logger.log(Level.SEVERE, "leave type="+a.getLeaveType()+"and action label="+a.getActionLabel());

	    			    	if(tMap.containsKey(a.getAttendanceDate())){
	    			    		a.setGroup(hours);
	    	    	    		logger.log(Level.SEVERE, "a.setGroup(hours)="+hours);

	    			    		tMap.put(a.getAttendanceDate(), a);
	    			    	}
						} catch (Exception e) {
							e.printStackTrace();
						}
				    }
				    bean.setOvertimeType(totalOtHours+"");
				    bean.setWorkedDays(totalWorkedDays+"");
				    bean.setLeaveDays(totalLeaveDays+"");
		    		logger.log(Level.SEVERE, "totalWorkedDays="+totalWorkedDays+" totalLeaveDays="+totalLeaveDays);

				    bean.setAttendanceObjectMap(tMap);
				    list.add(bean);
				}
		    		logger.log(Level.SEVERE, "LOG 4 ");

				return list;
			}
			
			
			public String sendApprovalRequest(String branch, int documentId, Long companyId, String approvarName, String requestedBy, String personResonsible, String documentName) {
				Approvals approval=new Approvals();
				if(approvarName!=null)
				approval.setApproverName(approvarName);
				approval.setRequestedBy(requestedBy);
				approval.setBranchname(branch);
				approval.setBusinessprocessId(documentId);
				if(personResonsible!=null){
					approval.setPersonResponsible(personResonsible);
				}
				approval.setApprovalLevel(1);
				approval.setStatus(Approvals.PENDING);
				approval.setBusinessprocesstype(documentName);
				approval.setDocumentValidation(false);
				approval.setCompanyId(companyId);
				approval.setCreationDate(new Date());
				
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(approval);
				return "approval request sent";
			
			}
			
			public String getGSTINOfCompany(Company company) {
				String gstinValue = "";
				
				for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {
					if (company.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = company.getArticleTypeDetails().get(i)
								.getArticleTypeValue().trim();
						return gstinValue;
					}
				}
				
				if (gstinValue.equals("")) {
					if (company.getCompanyGSTType().equalsIgnoreCase(
							"GST Applicable")
							&& company.getCompanyGSTTypeText() != null) {
						if (company.getCompanyGSTTypeText() != null) {
							gstinValue = company.getCompanyGSTTypeText();
						}

					}

				}
				
				return gstinValue;
			}

			/**
			 * @author Vijay Date :- 15-12-2022
			 * Encyption auth code validation method for customer portal
			 */
			public boolean validateAuthCode(String encryptedAuthCode, long originalCompanyId) {
				try {
					logger.log(Level.SEVERE, "encryptedAuthCode"+encryptedAuthCode);
					String decryptedCompanyId = decodeText(encryptedAuthCode);
					logger.log(Level.SEVERE, "decryptedCompanyId"+decryptedCompanyId);
					logger.log(Level.SEVERE, "originalCompanyId"+originalCompanyId);
					long companyId = Long.parseLong(decryptedCompanyId);
					logger.log(Level.SEVERE, "companyId"+companyId);
					if(companyId==originalCompanyId) {
						return true;
					}
				} catch (Exception e) {
				}
				return false;
			}
			/**
			 * ends here
			 */
			
			/**
			 * @author Vijay Date :- 02-01-2023
			 * Created common method to use in API to send validation message or success message in json format
			 */
			public  String getMessageInJson(String msg) {
				JSONObject msgjsonobj = new JSONObject();

				try {
					msgjsonobj.put("message", msg);
				} catch (JSONException e) {
				}
				
				String jsonstring = msgjsonobj.toString().replaceAll("\\\\", "");
				
				return jsonstring;
				
			}
			/**
			 * ends here
			 */
			
			public String getvendorCellNumber(Long companyId, int vendorId) {
				logger.log(Level.SEVERE, "vendorId"+vendorId);
				Vendor vendorEntity = ofy().load().type(Vendor.class).filter("companyId", companyId)
										.filter("count", vendorId).first().now();
				if(vendorEntity!=null && vendorEntity.getCellNumber1()!=0){
					return vendorEntity.getCellNumber1()+"";
				}
				return "";
			}
			
			/**Added by Ashwini Patil : 8-02-2023
			 * Des : Creating Common method for creating company signature**/
			 public static String getCompanySignature(String branchName,Long companyId ){
				 Company finalcompanydetails=null;
				 Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();	
				 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
					 Branch branchObj = ofy().load().type(Branch.class).filter("companyId",companyId).filter("buisnessUnitName",branchName).first().now();
					 finalcompanydetails=ServerAppUtility.changeBranchASCompany(branchObj, company);	
				 }else {
					 finalcompanydetails=company;
				 }
 
				 
				 if(finalcompanydetails!=null) {
//					 logger.log(Level.SEVERE,"In company!=null");
				  String companySignature = "";
				  StringBuilder builder = new StringBuilder();
				  
					builder.append("<b>" + "Thanks & Regards" + "</b><br>");
					// Company Details
					builder.append("<br>");
					builder.append("<b>" + finalcompanydetails.getBusinessUnitName() + "</b><br>");
					builder.append("<table>");
			
					builder.append("<tr>");
					builder.append("<td>");
					builder.append("Address : ");
					builder.append("</td>");
					builder.append("<td>");
					if (!finalcompanydetails.getAddress().getAddrLine2().equals("")
							&& finalcompanydetails.getAddress().getAddrLine2() != null)
						builder.append(finalcompanydetails.getAddress().getAddrLine1() + " "
								+ finalcompanydetails.getAddress().getAddrLine2() + " "
								+ finalcompanydetails.getAddress().getLocality());
					else
						builder.append(finalcompanydetails.getAddress().getAddrLine1() + " "
								+ finalcompanydetails.getAddress().getLocality());
					builder.append("</td>");
					builder.append("</tr>");
			
					builder.append("<tr>");
					builder.append("<td>");
					builder.append("</td>");
					builder.append("<td>");
					if (!finalcompanydetails.getAddress().getLandmark().equals("")
							&& finalcompanydetails.getAddress().getLandmark() != null)
						builder.append(finalcompanydetails.getAddress().getLandmark() + ","
								+ finalcompanydetails.getAddress().getCity() + ","
								+ finalcompanydetails.getAddress().getPin()
								+ finalcompanydetails.getAddress().getState() + ","
								+ finalcompanydetails.getAddress().getCountry());
					else
						builder.append(finalcompanydetails.getAddress().getCity() + ","
								+ finalcompanydetails.getAddress().getPin() + ","
								+ finalcompanydetails.getAddress().getState() + ","
								+ finalcompanydetails.getAddress().getCountry());
					builder.append("</td>");
					builder.append("</tr>");
					
					if(finalcompanydetails.getCellNumber1()!=null) {
						builder.append("<tr>");
						builder.append("<td>");
						builder.append("Phone No ");
						builder.append("</td>");
						builder.append("<td>");
						builder.append(": "+finalcompanydetails.getCellNumber1());
						builder.append("</td>");
						builder.append("<tr>");				
					}
					if(finalcompanydetails.getEmail()!=null&&!finalcompanydetails.getEmail().equals("")) {
						builder.append("<tr>");
						builder.append("<td>");
						builder.append("Email ");
						builder.append("</td>");
						builder.append("<td>");
						builder.append(": "+finalcompanydetails.getEmail());
						builder.append("</td>");
						builder.append("</tr>");
					}
					if(finalcompanydetails.getWebsite()!=null&&!finalcompanydetails.getWebsite().equals("")) {
						builder.append("<tr>");
						builder.append("<td>");
						builder.append("Website ");
						builder.append("</td>");
						builder.append("<td>");
						builder.append(": "+finalcompanydetails.getWebsite());
						builder.append("</td>");
						builder.append("</tr>");				
					}
			
					
			
					builder.append("</tr>");
					builder.append("</table>");

					companySignature = builder.toString();
					
					
//					logger.log(Level.SEVERE,"returning companySignature"+companySignature);
					return companySignature;
				 }
				 return "";
				  
			  }
			 
			 
			 public static String getCompanyName(String branchName,Company company) {
				 String companyName="";
				 if(company!=null) {
					 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
						 Branch branchObj = ofy().load().type(Branch.class).filter("companyId",company.getCompanyId()).filter("buisnessUnitName",branchName).first().now();
						 if(branchObj!=null) {
							 if(branchObj.getCorrespondenceName()!=null&&!branchObj.getCorrespondenceName().equals(""))
								companyName=branchObj.getCorrespondenceName();
							else
								companyName=branchObj.getBusinessUnitName();
						 }else
							 companyName=company.getBusinessUnitName();
					 }else {
						 companyName=company.getBusinessUnitName();
					 }				 
				 }
				 return companyName;
			 }
			 
			 public static String getCompanyName(String branchName,Long companyId) {
				 String companyName="";
				 Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();	
				 if(company!=null) {
					 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
						 Branch branchObj = ofy().load().type(Branch.class).filter("companyId",companyId).filter("buisnessUnitName",branchName).first().now();
						 if(branchObj!=null) {
							 if(branchObj.getCorrespondenceName()!=null&&!branchObj.getCorrespondenceName().equals(""))
								companyName=branchObj.getCorrespondenceName();
							else
								companyName=branchObj.getBusinessUnitName();
						 }else
							 companyName=company.getBusinessUnitName();
					 }else {
						 companyName=company.getBusinessUnitName();
					 }				 
				 }
				 return companyName;
			 }
			 
			 public static String getCompanyEmail(String branchName,Long companyId) {
				 String companyEmail="";
				 Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();	
				 if(company!=null) {
					 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
						 Branch branchObj = ofy().load().type(Branch.class).filter("companyId",companyId).filter("buisnessUnitName",branchName).first().now();
						 if(branchObj!=null) {
							 if(branchObj.getEmail()!=null&&!branchObj.getEmail().equals(""))
								 companyEmail=branchObj.getEmail();
							 else
								 companyEmail=company.getEmail();
						 }else
							 companyEmail=company.getEmail();
					 }else {
						 companyEmail=company.getEmail();
					 }				 
				 }
				 return companyEmail;
			 }
			 public static String getCompanyEmail(String branchName,Company company) {
				 String companyEmail="";
				 if(company!=null) {
					 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
						 Branch branchObj = ofy().load().type(Branch.class).filter("companyId",company.getCompanyId()).filter("buisnessUnitName",branchName).first().now();
						 if(branchObj!=null) {
							 if(branchObj.getEmail()!=null&&!branchObj.getEmail().equals(""))
								 companyEmail=branchObj.getEmail();
							 else
								 companyEmail=company.getEmail();
						 }else
							 companyEmail=company.getEmail();
					 }else {
						 companyEmail=company.getEmail();
					 }				 
				 }
				 return companyEmail;
			 }		 
		 
			 public static String getCustomerPortalLink(Company comp) {
				 	
					String Url ="";
					if(comp.getCompanyURL()!=null) {					
						Url=comp.getCompanyURL();
					}
					String Appid = "";
					if(Url!=null&&!Url.equals("")) {
						if(Url.contains("-dot-")){
							String [] urlarray = Url.split("-dot-");
							 Appid = urlarray[1];
						}
						else{
							String [] urlarray = Url.split(".");
							 Appid = urlarray[1];
						}	
						Appid=Appid.replace(".appspot.com", "");
					}
					
					if(Appid!=null&&!Appid.equals("")) {
						String link=AppConstants.CustomerPortalLink+"my-dot-"+Appid;
						return getTinyUrl(link,comp.getCompanyId());
					}else {
						return "failed";
					}
				 
			 }
			 
			 
			//Ashwini Patil Date:28-02-2023
				public void createTermAndConditionsPage(Document document, String documentName,Long companyId) {
					logger.log(Level.SEVERE,"in createTermAndConditionsPage");
					PdfUtility pdfUtility=new PdfUtility();
					Font font10bold,font8;
					font8 = new Font(Font.FontFamily.HELVETICA, 8);
					font10bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
					// TODO Auto-generated method stub
					Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
					try {
						document.add(nextpage);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
					
					PdfPTable table = new PdfPTable(1);
					table.setWidthPercentage(100f);
					
					List<TermsAndConditions> terms = ofy().load().type(TermsAndConditions.class)
							.filter("companyId",companyId)
							.filter("document", documentName)
							.filter("status", true).list();
					
					if(terms!=null){
						logger.log(Level.SEVERE,"terms size=" +terms.size());
//						
						Comparator<TermsAndConditions> sequenceComparator=new Comparator<TermsAndConditions>() {
							@Override
							public int compare(TermsAndConditions arg0, TermsAndConditions arg1) {
								return arg0.getSequenceNumber().compareTo(arg1.getSequenceNumber());
							}
						};
						Collections.sort(terms, sequenceComparator);
						for(TermsAndConditions term:terms){
							table.addCell(pdfUtility.getCell(term.getTitle(), font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);;
							table.addCell(pdfUtility.getCell(term.getMsg(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);	
							table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);					
						}
						try {
							document.add(table);
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else
						return;
					
					
				}
				
				//Ashwini Patil Date:9-05-2023
				public String validateCTCTemplateName(List<CTCTemplate> ctcTemplatelist, String tName, String columnName) {
					String str = "";
					if(!tName.trim().equalsIgnoreCase("NA")){						
						
						if(!validateCTC(ctcTemplatelist, tName)) {
							str =" "+ columnName+ " - "+tName+" does not exist or not match in the system!. ";
						}
					}
					return str;
				}
				
				private boolean validateCTC(List<CTCTemplate> ctcTemplatelist, String tName) {
					if(ctcTemplatelist!=null){
						for(CTCTemplate entity : ctcTemplatelist) {
							if(entity.getCtcTemplateName().trim().equals(tName)) {
								return true;
							}
						}
					}
					return false;
				}
				
				public String validateCalendarName(List<com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar> calendarlist, String cName, String columnName) {
					String str = "";
					if(!cName.trim().equalsIgnoreCase("NA")){						
						
						if(!validateCalendar(calendarlist, cName)) {
							str =" "+ columnName+ " - "+cName+" does not exist or not match in the system!. ";
						}
					}
					return str;
				}
				
				private boolean validateCalendar(List<com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar> calendarlist, String cName) {
					if(calendarlist!=null){
						for(com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar entity : calendarlist) {
							if(entity.getCalName().trim().equals(cName)) {
								return true;
							}
						}
					}
					return false;
				}
				
				public String validateLeaveGroup(List<LeaveGroup> leaveGrouplist, String lName, String columnName) {
					String str = "";
					if(!lName.trim().equalsIgnoreCase("NA")){						
						
						if(!validateLeave(leaveGrouplist, lName)) {
							str = " "+columnName+ " - "+lName+" does not exist or not match in the system!. ";
						}
					}
					return str;
				}
				
				private boolean validateLeave(List<LeaveGroup> leaveGrouplist, String lName) {
					if(leaveGrouplist!=null){
						for(LeaveGroup entity : leaveGrouplist) {
							if(entity.getGroupName().trim().equals(lName)) {
								return true;
							}
						}
					}
					return false;
				}
				
				
				public String validateProjectName(List<HrProject> projectlist, String pName, String columnName,String employeebranch) {
					String str = "";
					if(!pName.trim().equalsIgnoreCase("NA")){						
						
						if(!validateProject(projectlist, pName)) {
							str = " "+columnName+ " - "+pName+" does not exist or not match in the system!. ";
						}else{
							for(HrProject project:projectlist){
								if(project.getProjectName().equals(pName)){
									if(!project.getBranch().equals(employeebranch)){
										str =" "+ columnName+ " -  Employee's and project branch should be same.";										
										break;
									}
								}
							}
						}
					}
					return str;
				}
				
				private boolean validateProject(List<HrProject> projectlist, String pName) {
					if(projectlist!=null){
						for(HrProject entity : projectlist) {
							if(entity.getProjectName().trim().equals(pName)) {
								return true;
							}
						}
					}
					return false;
				}
				
				public String validateOTName(List<Overtime> otlist, String otName, String columnName) {
					String str = "";
					if(!otName.trim().equalsIgnoreCase("NA")){						
						
						if(!validateOT(otlist, otName)) {
							str =" "+ columnName+ " - "+otName+" does not exist or not match in the system!. ";
						}
					}
					return str;
				}
				
				private boolean validateOT(List<Overtime> otlist, String otName) {
					if(otlist!=null){
						for(Overtime entity : otlist) {
							if(entity.getName().trim().equals(otName)) {
								return true;
							}
						}
					}
					return false;
				}
				
				public String validateShiftName(List<Shift> shiftlist, String sName, String columnName) {
					String str = "";
					if(!sName.trim().equalsIgnoreCase("NA")){						
						
						if(!validateShift(shiftlist, sName)) {
							str = " "+columnName+ " - "+sName+" does not exist or not match in the system!. ";
						}
					}
					return str;
				}
				
				private boolean validateShift(List<Shift> shiftlist, String sName) {
					if(shiftlist!=null){
						for(Shift entity : shiftlist) {
							if(entity.getShiftName().trim().equals(sName)) {
								return true;
							}
						}
					}
					return false;
				}
				
				
				
		public boolean checkEInvoiceConfigAtivation(long companyId, String branchName){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
				Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", branchName).first().now();
				logger.log(Level.SEVERE,"branchEntity "+branchEntity);
				if(branchEntity!=null && branchEntity.isEinvoiceApplicable()){
					return true;
				}
				return false;
			}
			Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			logger.log(Level.SEVERE,"companyEntity "+companyEntity);
			if(companyEntity.getApiId()!=null && !companyEntity.getApiId().equals("") && 
					companyEntity.getApiSecret()!=null && !companyEntity.getApiSecret().equals("") &&
					companyEntity.getUserName()!=null && !companyEntity.getUserName().equals("") &&
					companyEntity.getPassword()!=null && !companyEntity.getPassword().equals("")){
				return true;
			}
			
			return false;
		}
		//Ashwini Patil Date:27-06-2023
		public String getUINOfCompany(Company company, String companyBranchName) {
			String uinValue = "";
			if (companyBranchName != null && !companyBranchName.equals("")) {
				Branch branch = ofy().load().type(Branch.class)
						.filter("companyId", company.getCompanyId())
						.filter("buisnessUnitName", companyBranchName).first()
						.now();
				if (branch != null&&branch.getArticleTypeDetails()!=null) {
					for (int i = 0; i < branch.getArticleTypeDetails().size(); i++) {
						if (branch.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("UIN")) {
							uinValue = branch.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
							break;
						}
					}
				}
				if (uinValue.equals("")) {					
					if (company.getArticleTypeDetails()!=null) {
						for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {
							if (company.getArticleTypeDetails().get(i)
									.getArticleTypeName().equalsIgnoreCase("UIN")) {
								uinValue = company.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
								break;
							}
						}
					}

				}
				return uinValue;
			}else {
				if (company.getArticleTypeDetails()!=null) {
					for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {
						if (company.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("UIN")) {
							uinValue = company.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
							break;
						}
					}					
				}
				return uinValue;
			}
			
		}
		
		//Ashwini Patil Date:27-06-2023
		public String validatePaymentMode(List<String> modelist, String mode,String columnName) {
					
			if(modelist!=null){
				for(String item : modelist) {
							System.out.println("in validatePaymentMode item="+item+" excel value="+mode);
							if(item.equals(mode)) {
								return "";
							}
				}
			}
		return " "+columnName+ " - "+mode+" does not exist or not match in the system!. ";
		}
		
		public static long getCompanyCellNo(String branchName,Company company) {
			 long cellno=0;
			 if(company!=null) {
				 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", company.getCompanyId())){
					 Branch branchObj = ofy().load().type(Branch.class).filter("companyId",company.getCompanyId()).filter("buisnessUnitName",branchName).first().now();
					 if(branchObj!=null) {
						 if(branchObj.getCellNumber1()!=null&&branchObj.getCellNumber1()>0)
							 cellno=branchObj.getCellNumber1();
						 else
							 cellno=company.getCellNumber1();
					 }else
						 cellno=company.getCellNumber1();
				 }else {
					 cellno=company.getCellNumber1();
				 }				 
			 }
			 return cellno;
		 }	
		
		//Ashwini Patil Date:22-07-2023
		public static String validateLicense(Company comp,String licenceCode) {
			GetUserRegistrationOtp api=new GetUserRegistrationOtp();
			String result=api.validateLicense(comp,licenceCode);
			return result;
		}
		
		/*
		 * Ashwini Patil 
		 * Date:22-07-2023
		 * We are adding PCAudit licence(product) to essevaerp
		 * Irrespective of licence start date, end date and number of licenses, we need to check if the license exist or not
		 * if license found then only audit button will be shown in pedio else it will be hidden
		 */
		public static boolean checkIfLicensePresent(Company c,String licenceCode) {
			
			if (c!=null&&c.getLicenseDetailsList() != null&& c.getLicenseDetailsList().size() != 0) {
				logger.log(Level.SEVERE, "TOTAL EVA LICENSE LIST : "+ c.getLicenseDetailsList().size()+ "checking licenceCode="+licenceCode);
				// Console.log("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
				for (LicenseDetails license : c.getLicenseDetailsList()) {
					logger.log(Level.SEVERE, "existing license="+ license.getLicenseType());					
						if (licenceCode.equalsIgnoreCase(license.getLicenseType())) {
							return true;
						}	
				}
			}
			else{
				return false;
			}
			return false;
		}

		
		/*
		 * Ashwini Patil 
		 * Date:6-09-2023
		 * A common method to get number of active licenses
		 */
		public int getNumberOfActiveLicense(Company c,String licenceCode) {

			if(c!=null) {
				String returnValue = "Success";
				Date todaydate = new Date();
				ArrayList<LicenseDetails> licenseDetailsList = new ArrayList<LicenseDetails>();
				if (c.getLicenseDetailsList() != null&& c.getLicenseDetailsList().size() != 0) {
					logger.log(Level.SEVERE, "TOTAL EVA LICENSE LIST : "+ c.getLicenseDetailsList().size());
					// Console.log("TOTAL EVA LICENSE LIST : "+c.getLicenseDetailsList().size());
					for (LicenseDetails license : c.getLicenseDetailsList()) {
							if (licenceCode.equalsIgnoreCase(license.getLicenseType())
									&& license.getStatus() == true) {
								licenseDetailsList.add(license);
							}				

					}
					logger.log(Level.SEVERE, "ACTIVE "+licenceCode+" LICENSE LIST SIZE : "+ licenseDetailsList.size());
				}
				else{
					return 0;
				}


				int noOfLicense = 0;
				int expiredLicenseCount = 0;
				int notActiveLicenseCount = 0;
				boolean validLicense = false;
				if (licenseDetailsList.size() != 0) {
					for (LicenseDetails license : licenseDetailsList) {
						logger.log(Level.SEVERE, license.getLicenseType()
								+ " No. Of License : " + license.getNoOfLicense()
								+ " LICENSE START DATE : " + license.getStartDate()
								+ " LICENSE END DATE : " + (license.getEndDate()));
						
						SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
						logger.log(Level.SEVERE,"Today="+sdf.format(todaydate)+"license start day="+sdf.format(license.getStartDate()));
						//Ashwini Patil Date:20-12-2022 added one more condition so that license should work from the currect day
						if (todaydate.after(license.getStartDate())||sdf.format(todaydate).equals(sdf.format(license.getStartDate()))) {
							if (todaydate.before(license.getEndDate())) {
								noOfLicense = noOfLicense + license.getNoOfLicense();
								validLicense = true;
							} else {
								expiredLicenseCount++;
							}
						} else {
							notActiveLicenseCount++;
						}
					}

					// Console.log("TOTAL NO. OF ACTIVE LICENSE : "+noOfLicense);
					logger.log(Level.SEVERE, "TOTAL NO. OF ACTIVE LICENSE : "
							+ noOfLicense);
//					c.setNoOfUser(noOfLicense);
//					if (validLicense) {
//						return noOfLicense;
//					} else if (validLicense == false && notActiveLicenseCount > 0) {
//						returnValue = licenceCode+" license is not active. Please Contact your service provider."+c.getCellNumber1();
//					} else if (validLicense == false && expiredLicenseCount > 0) {
//						returnValue = licenceCode+" license has expired. Please Contact your service provider. "+c.getCellNumber1();
//					}
					
				}
				return noOfLicense;
			
			}else {
				return 0;
			}
		}
		
		//Ashwini Patil Date:28-09-2023
		public String checkIfCommaQuoteAmpersandPresent(String columnName, String columnValue) {
			try {
			String str = "";
			if(!columnValue.trim().equalsIgnoreCase("NA") && !columnValue.trim().equalsIgnoreCase("N A")) {
				if(columnValue.contains(","))
					str += " Comma not allowed in Column "+columnName+". ";
				if(columnValue.contains("\""))
					str += " Double quot not allowed in Column "+columnName+". ";
				if(columnValue.contains("&"))
					str += " & character not allowed in Column "+columnName+". ";	
				if(columnValue.contains("$"))
					str += " $ character not allowed in Column "+columnName+". ";
			}
				return str;
			} catch (Exception e) {
				return " Please enter alphabetic value in "+columnName +" column";

			}
		}
		
		//Ashwini Patil Date:25-10-2023
		public static String getAppid(Company comp){
			String Url ="";
			if(comp.getCompanyURL()!=null) {					
				Url=comp.getCompanyURL();
			}
			String Appid = "";
			if(Url!=null&&!Url.equals("")) {
				if(Url.contains("-dot-")){
					String [] urlarray = Url.split("-dot-");
					 Appid = urlarray[1];
				}
				else{
					String [] urlarray = Url.split(".");
					 Appid = urlarray[1];
				}	
				Appid=Appid.replace(".appspot.com", "");
			}
			return Appid;
		}
		
		public static int getMonthNumber(String month){
			int m=0;
			switch(month){
			case "Jan":
				m=0;
				break;
			case "Feb":
				m=1;
				break;
			case "Mar":
				m=2;
				break;
			case "Apr":
				m=3;
				break;
			case "May":
				m=4;
				break;
			case "Jun":
				m=5;
				break;
			case "Jul":
				m=6;
				break;
			case "Aug":
				m=7;
				break;
			case "Sep":
				m=8;
				break;
			case "Oct":
				m=9;
				break;
			case "Nov":
				m=10;
				break;
			case "Dec":
				m=11;
				break;
			
				
			
			}
			return m;
		}
		
		public static int calculateEligibleWo(WeekleyOff wo,int year, int month){
			int count=0;
			if(wo.isSUNDAY())
				count+=countDayOccurence(year, month, Calendar.SUNDAY);
			if(wo.isMONDAY())
				count+=countDayOccurence(year, month, Calendar.MONDAY);
			if(wo.isTUESDAY())
				count+=countDayOccurence(year, month, Calendar.TUESDAY);
			if(wo.isWEDNESDAY())
				count+=countDayOccurence(year, month, Calendar.WEDNESDAY);
			if(wo.isTHRUSDAY())
				count+=countDayOccurence(year, month, Calendar.THURSDAY);
			if(wo.isFRIDAY())	
				count+=countDayOccurence(year, month, Calendar.FRIDAY);
			if(wo.isSATAURDAY())
				count+=countDayOccurence(year, month, Calendar.SATURDAY);
			return count;
		}
		public static int countDayOccurence(int year, int month,int dayToFindCount) {//Calendar.MONDAY
			java.util.Calendar calendar = java.util.Calendar.getInstance();
		    // Note that month is 0-based in calendar, bizarrely.
		    calendar.set(year, month, 1);
		    int daysInMonth = calendar.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);

		    int count = 0;
		    for (int day = 1; day <= daysInMonth; day++) {
		        calendar.set(year, month, day);
		        int dayOfWeek = calendar.get(java.util.Calendar.DAY_OF_WEEK);
		        if (dayOfWeek == dayToFindCount) {
		            count++;
		            // Or do whatever you need to with the result.
		        }
		    }
		    return count;
		}
		
			//31-10-2023
		public String validateTrueFalse(String columnName, String columnValue) {
			if(columnValue.trim().equalsIgnoreCase("True") || columnValue.trim().equalsIgnoreCase("False")){
				return "";
			}
			return "Please add "+columnName+" column value either True or False! ";
		
		}
		
		public String validateServiceStatus(String columnvalue, ArrayList<String> statusList, String columnName) {
			if(columnvalue!=null && statusList!=null){
				for(String strName : statusList) {
					if(strName.trim().equals(columnvalue.trim())) {
						return "";
					}
				}
				String str=statusList.toString();
				str=str.replace(",", " / ");
				return "Please add "+columnName+" column value as "+ str;
				
			}else
				return "null status";
		}
				
}
