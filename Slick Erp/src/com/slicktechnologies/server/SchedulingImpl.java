package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.views.schedulingandrouting.SchedulingService;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.scheduling.HolidayDates;
import com.slicktechnologies.shared.common.scheduling.TeamWorkingHours;

public class SchedulingImpl extends RemoteServiceServlet implements SchedulingService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3963816184472808615L;

	HashMap<String,HashMap<String,HashMap<String,HashMap<Integer,Integer>>>> dateWiseMap=new HashMap<String,HashMap<String,HashMap<String,HashMap<Integer,Integer>>>>();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	ArrayList<Service> serList;
	
	HashMap<String,HashMap<String,ArrayList<String>>> dailyTeamLocAllocn=new HashMap<String, HashMap<String,ArrayList<String>>>();
	ArrayList<Service> otherServiceList;
	int remainingHrs;
	int previousfromTime;
	ArrayList<TeamWorkingHours> listOfTeam;
	ArrayList<HolidayDates> listOfHolidays;
	Logger logger = Logger.getLogger("Inside get Services For Scheduling....");
	@Override
	public ArrayList<Service> getServicesForScheduling(ArrayList<Filter> filter) {

		System.out.println("Inside get Services For Scheduling....");
		
		ArrayList<Service> serviceList = new ArrayList<Service>();

		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Service());
		if (filter.size() != 0) {
			querry.getFilters().addAll(filter);
		}

		GenricServiceImpl impl = new GenricServiceImpl();
		List<SuperModel> modelLis = impl.getSearchResult(querry);
		System.out.println("Result SIZE.. " + modelLis.size());
		logger.log(Level.SEVERE, "Result SIZE.. " + modelLis.size());

		for (SuperModel temp : modelLis) {
			Service entity = (Service) temp;

			serviceList.add(entity);

		}
		return serviceList;
	}

	@Override
	public ArrayList<Service> schedulingServices(Date fromDate, Date toDate,
			ArrayList<Service> serviceList,
			ArrayList<TeamWorkingHours> teamList,
			ArrayList<HolidayDates> holidayList) {
		
		serList=new ArrayList<Service>();
		dateWiseMap=new HashMap<String,HashMap<String,HashMap<String,HashMap<Integer,Integer>>>>();
		dailyTeamLocAllocn=new HashMap<String, HashMap<String,ArrayList<String>>>();
		listOfTeam=teamList;
		listOfHolidays=holidayList;
		
		logger.log(Level.SEVERE, "FROM DATE " + fromDate);
		logger.log(Level.SEVERE, "TO DATE " + toDate);
		
		System.out.println();
		System.out.println("$$$ FROM DATE :: " + fromDate);
		System.out.println("$$$ TO DATE :: " + toDate);
		System.out.println();
		
		fromDate=getNewDate(fromDate);
		toDate=getNewDate(toDate);
		
		logger.log(Level.SEVERE, "NEW FROM DATE " + fromDate);
		logger.log(Level.SEVERE, "NEW TO DATE " + toDate);
		
		
		System.out.println("HOLIDAY LIST SIZE :: "+holidayList.size());
		
		for (int i = 0; i < holidayList.size(); i++) {
			System.out.println("Holiday Name :: "+ holidayList.get(i).getHolidayName() + " Holiday Date :: "+ sdf.format(holidayList.get(i).getHolidayDate()));
		}
		System.out.println();
		
		ArrayList<Service> specificDayServiceList = new ArrayList<Service>();
		ArrayList<Service> othrThanspfDySrvcList = new ArrayList<Service>();
		
		
		for (int i = 0; i < serviceList.size(); i++) {
			if (!serviceList.get(i).getServiceDay().equals("Not Select")&&!serviceList.get(i).getServiceDay().equals("")) {
				specificDayServiceList.add(serviceList.get(i));
			} else {
				othrThanspfDySrvcList.add(serviceList.get(i));
			}
		}
		
		System.out.println();
		System.out.println("TOTAL SERVICES ::: " + serviceList.size());
		System.out.println("SPECIFIC SERVICES ::: "+ specificDayServiceList.size());
		System.out.println("OTHER SERVICES ::: " + othrThanspfDySrvcList.size());
		System.out.println();
		
		
		for(int i=0;i<othrThanspfDySrvcList.size();i++){
			if(!othrThanspfDySrvcList.get(i).getServiceTime().equals("Flexible")&&!othrThanspfDySrvcList.get(i).getServiceTime().equals("")){
				specificDayServiceList.add(othrThanspfDySrvcList.get(i));
				System.out.println("      row    "+i );
				othrThanspfDySrvcList.remove(i);
			}
		}
		
		System.out.println("######## NEW SPECIFICDAY LIST SIZE ::  "+specificDayServiceList.size());
		System.out.println("######## NEW OTHER LIST SIZE ::  "+othrThanspfDySrvcList.size());
		System.out.println();
		
		
		/*****************************************************************************************/
		
		
		
		
		/***
		 * 
		 * SCHEDULING SPECIFIC DAY SERVICEES
		 * 
		 */
		
		
		if(specificDayServiceList.size()!=0){
//			System.out.println();
//			System.out.println("************* SPECIFIC SERVICES *****************");
//			System.out.println();
			
			ArrayList<Service>specificTimeServiceList=new ArrayList<Service>();
			ArrayList<Service>flexibleTimeServiceList=new ArrayList<Service>();
			
			for(int i=0;i<specificDayServiceList.size();i++){
				if(!specificDayServiceList.get(i).getServiceTime().equals("Flexible")&&!specificDayServiceList.get(i).getServiceTime().equals("")){
					specificTimeServiceList.add(specificDayServiceList.get(i));
				}
				else{
					flexibleTimeServiceList.add(specificDayServiceList.get(i));
				}
			}
			
			
			System.out.println("SPECIFIC TIME SERVICES ::: " + specificTimeServiceList.size());
			System.out.println("FLEXIBLE TIME SERVICES ::: " + flexibleTimeServiceList.size());
			
			
			/*************  ------------ SPECIFIC TIME SERVICES -------------  ***********/
			
			if(specificTimeServiceList.size()!=0){
				System.out.println();
//				System.out.println(" ------------ SPECIFIC TIME SERVICES -------------");
				for(int i=0;i<specificTimeServiceList.size();i++){
					System.out.println();
					
//					System.out.println("SERVICE DAY "+specificTimeServiceList.get(i).getServiceDay());
//					System.out.println("SERVICE DATE "+specificTimeServiceList.get(i).getServiceDate());
//					System.out.println("SERVICE ID "+specificTimeServiceList.get(i).getCount());
//					System.out.println("SERVICING TIME "+specificTimeServiceList.get(i).getServicingTime());
//					System.out.println("SERVICE LOCALITY "+specificTimeServiceList.get(i).getLocality());
					String date=sdf.format(specificTimeServiceList.get(i).getServiceDate());
//					System.out.println("SERVICE DATE IN STRING "+date);
//					System.out.println("SERVICE TIME "+specificTimeServiceList.get(i).getServiceTime());
					int fromTime=getTimeIn24HrsFormate(specificTimeServiceList.get(i).getServiceTime());
//					System.out.println("SERVICE TIME IN 24 HRS "+fromTime);
					
					String locality=specificTimeServiceList.get(i).getLocality();
					int serviceId=specificTimeServiceList.get(i).getCount();
					Double d=specificTimeServiceList.get(i).getServicingTime();
					int servicingTime=d.intValue();
//					System.out.println("SERVICING TIME IN INTEGER :: "+servicingTime);
					Service service=specificTimeServiceList.get(i);
					ArrayList<TeamWorkingHours> tList=getTeamListForSpecificServiceTime(teamList, fromTime,servicingTime);
//					System.out.println("TEAM LIST SIZE :: "+tList.size());
					boolean dateFlag=false;
					boolean locFlage=false;
					boolean teamFlag=false;
					boolean flexibleTimeFlag=false;
					boolean specificServFlag=true;
					
					boolean isOff=checkWeeklyOffnHoliday(getServiceDate(specificTimeServiceList.get(i).getServiceDate()));
				
					if(isOff==false){
					
					if(dateWiseMap.size()!=0){
//						System.out.println();
//						System.out.println("DATE MAP LIST NOT EMPTY..!");
						if(checkDateInShedulingMap(date)){
							if(checkLocalityInShedulingMap(date, locality)){
								
//								System.out.println("DATE AND LOCALITY IS PRESENT IN DATE MAP..!");
								dateFlag=true;
								locFlage=true;
								String teamName;
								boolean teamTimeSlotFlag=false;
								
								if(tList.size()!=0){
									for(int j=0;j<tList.size();j++){
										teamName=tList.get(j).getTeamName();
										if(checkTeamInShedulingMap(date, locality, teamName)){
											teamFlag=true;
											int serviceTime=fromTime;
											int count=1;
											int hrsCtr=0;
											
											while(count<=servicingTime){
												if(checkHrsInShedulingMap(date,locality,teamName,serviceTime)){
													hrsCtr++;
												}
												count++;
												serviceTime++;
											}
											
											if(hrsCtr==0){
												teamTimeSlotFlag=true;
												schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
												break;
											}
										}else{
											teamFlag=false;
											schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
											break;
										}
									}
									if(teamFlag==true&&teamTimeSlotFlag==false){
										addServiceToUnscheduledList(service, "Team time slote is not available!",true);
									}
								}else{
									addServiceToUnscheduledList(service, "Team is not available!",true);
								}
								
								
								
							}else{
//								System.out.println();
//								System.out.println("DATE IS BUT LOC IS NOT PRESENT IN DATE MAP LIST ..!");
								dateFlag=true;
								if(tList.size()!=0){
									String teamName=tList.get(0).getTeamName();
									schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
								}else{
									addServiceToUnscheduledList(service, "Team is not available!",true);
								}
							}
						}else{
							System.out.println();
							System.out.println("DATE IS NOT PRESENT IN DATE MAP LIST ..!");
							if(tList.size()!=0){
								String teamName=tList.get(0).getTeamName();
								schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
							}else{
								addServiceToUnscheduledList(service, "Team is not available!",true);
							}
						}
					}else{
//						System.out.println();
//						System.out.println("DATE MAP LIST EMPTY..!");
						if(tList.size()!=0){
							String teamName=tList.get(0).getTeamName();
							schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
						}else{
							addServiceToUnscheduledList(service, "Team is not available!",true);
						}
					}
					
				}else{
					addServiceToUnscheduledList(service, getHolidayName(getServiceDate(specificTimeServiceList.get(i).getServiceDate())),true);
				}
					
					
				}
			}
			
			
			/*************  ------------ FLEXIBLE TIME SERVICES -------------  ***********/
			
			if(flexibleTimeServiceList.size()!=0){
				System.out.println();
				System.out.println(" ------------ FLEXIBLE TIME SERVICES -------------");
				for(int i=0;i<flexibleTimeServiceList.size();i++){
					System.out.println();
					
//					System.out.println("SERVICE DAY "+flexibleTimeServiceList.get(i).getServiceDay());
//					System.out.println("SERVICE ID "+flexibleTimeServiceList.get(i).getCount());
//					System.out.println("SERVICING TIME "+flexibleTimeServiceList.get(i).getServicingTime());
//					System.out.println("SERVICE LOCALITY "+flexibleTimeServiceList.get(i).getLocality());
//					System.out.println("SERVICE DATE "+flexibleTimeServiceList.get(i).getServiceDate());
					String date=sdf.format(flexibleTimeServiceList.get(i).getServiceDate());
//					System.out.println("SERVICE DATE IN STRING "+date);
//					System.out.println("SERVICE TIME "+flexibleTimeServiceList.get(i).getServiceTime());
					
					int fromTime=0;
					
					String locality=flexibleTimeServiceList.get(i).getLocality();
					int serviceId=flexibleTimeServiceList.get(i).getCount();
					
					Double d=flexibleTimeServiceList.get(i).getServicingTime();
					int servicingTime=d.intValue();
					
					Service service=flexibleTimeServiceList.get(i);
					
					boolean dateFlag=false;
					boolean locFlage=false;
					boolean teamFlag=false;
					boolean flexibleTimeFlag=true;
					boolean specificServFlag=true;
					
					boolean isOff=checkWeeklyOffnHoliday(getServiceDate(specificTimeServiceList.get(i).getServiceDate()));
					
					if(isOff==false){
					
					
					if(dateWiseMap.size()!=0){
//						System.out.println();
//						System.out.println("DATE MAP LIST NOT EMPTY..!");
						if(checkDateInShedulingMap(date)){
							if(checkLocalityInShedulingMap(date, locality)){
//								System.out.println("DATE AND LOCALITY IS PRESENT IN DATE MAP..!");
								dateFlag=true;
								locFlage=true;
								String teamName;
								boolean teamTimeSlotFlag=false;
								if(isTeamAssigenToLocalityForDay(date, locality, teamList)){
									ArrayList<TeamWorkingHours> tList=getTeamListAssigenToLocalityForDay(date, locality, teamList);
									
									for(int x=0;x<tList.size();x++){
										TeamWorkingHours team=tList.get(x);
										teamName=tList.get(x).getTeamName();
										if(isTeamTimeSlotAvailable(team, date, locality, servicingTime)){
											teamTimeSlotFlag=true;
											teamFlag=true;
											fromTime=getTeamFromServicingTime(team, date, locality, servicingTime);
											schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
											break;
										}
									}
									
									if(teamTimeSlotFlag==false){
										teamFlag=false;
										ArrayList<TeamWorkingHours> tlist1=getAvailableTeamListForDay(date, locality, teamList);
										if(tlist1.size()!=0){
											TeamWorkingHours team=tlist1.get(0);
											teamName=team.getTeamName();
											fromTime=(int) team.getFromTime();
//											System.out.println("TEAM NAME :: "+teamName);
//											System.out.println("TEAM FROM TIME :: "+fromTime);
											schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
										}else{
											addServiceToUnscheduledList(service, "Team is not available.",true);
										}
									}
								}else{
									
								}
								
							}else{
//								System.out.println();
//								System.out.println("DATE IS BUT LOC IS NOT PRESENT IN DATE MAP LIST ..!");
								dateFlag=true;
								
								ArrayList<TeamWorkingHours> tList=getAvailableTeamListForDay(date, locality, teamList);
								
								if(tList.size()!=0){
									TeamWorkingHours team=tList.get(0);
									String teamName=team.getTeamName();
									fromTime=(int) team.getFromTime();
//									System.out.println("TEAM NAME :: "+teamName);
//									System.out.println("TEAM FROM TIME :: "+fromTime);
									schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
								}else{
									addServiceToUnscheduledList(service, "Team is not available.",true);
								}
							}
						}else{
//							System.out.println();
//							System.out.println("DATE IS NOT PRESENT IN DATE MAP LIST ..!");
							TeamWorkingHours team=teamList.get(0);
							String teamName=team.getTeamName();
							fromTime=(int) team.getFromTime();
//							System.out.println("TEAM NAME :: "+teamName);
//							System.out.println("TEAM FROM TIME :: "+fromTime);
							
							
							schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
							
						}
					}else{
//						System.out.println();
//						System.out.println("DATE MAP LIST EMPTY..!");
						
						TeamWorkingHours team=teamList.get(0);
						String teamName=team.getTeamName();
						fromTime=(int) team.getFromTime();
						int serviceTime=fromTime;
//						System.out.println("TEAM NAME :: "+teamName);
//						System.out.println("TEAM FROM TIME :: "+fromTime);
						
						
						schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
						
					}
					
				}else{
					addServiceToUnscheduledList(service, getHolidayName(getServiceDate(specificTimeServiceList.get(i).getServiceDate())),true);
				}
					
					
					
					
				}
				
			}
			
			
			
		}
		
		/**********************************************************************************************/
		
		/***
		 * SCHEDULING OTHER SERVICES
		 * 
		 */
		
		if(othrThanspfDySrvcList.size()!=0){
			
			
			System.out.println();
			System.out.println("OTHER SERVICES");
			
			
			otherServiceList=new ArrayList<Service>();
			
			HashSet<String> locSet=new HashSet<String>();
			ArrayList<String> localityList=new ArrayList<String>();
			
			for(int i=0;i<othrThanspfDySrvcList.size();i++){
				locSet.add(othrThanspfDySrvcList.get(i).getLocality());
			}
			localityList.addAll(locSet);
			Collections.sort(localityList);
			
			Comparator<Service> servicingTimeComp = new Comparator<Service>() {  
				@Override  
				public int compare(Service o1, Service o2) {  
					return o2.getServicingTime().compareTo(o1.getServicingTime());  
				}  
			};  
			Collections.sort(othrThanspfDySrvcList,servicingTimeComp);
			
			for(int i=0;i<localityList.size();i++)
			{
				for(int j=0;j<othrThanspfDySrvcList.size();j++){
					if(localityList.get(i).equals(othrThanspfDySrvcList.get(j).getLocality())){
						otherServiceList.add(othrThanspfDySrvcList.get(j));
					}
				}
			}
			
			System.out.println();
			System.out.println("SERVIC LIST SORTED BY LOCALITY AND SERVICING HRS...");
			System.out.println();
			
			for(int i=0;i<otherServiceList.size();i++){
				System.out.println(i+" - "+"LOCALITY :: "+otherServiceList.get(i).getLocality()+"  : "+otherServiceList.get(i).getServicingTime());
			}
			System.out.println();
			
			Date strtDate=fromDate;
			Date edDate=toDate;
			Calendar startDate = Calendar.getInstance();
			Calendar endDate = Calendar.getInstance();
			
			startDate.setTime(strtDate);
			endDate.setTime(edDate);
			
			
			System.out.println("START DATE :: "+startDate.getTime());
			System.out.println("END DATE :: "+endDate.getTime());
			System.out.println();
			
			
			while(startDate.getTime().before(endDate.getTime())|| startDate.getTime().equals(endDate.getTime())){
				
				boolean isOffDay=checkWeeklyOffnHoliday(startDate.getTime());
				
				if(isOffDay==false){
					String date=sdf.format(startDate.getTime());
					System.out.println("DATE :: "+date);
					System.out.println("###### Date LOOP ::: ");
					for(int i=0;i<teamList.size();i++){
						TeamWorkingHours team=teamList.get(i);
						System.out.println("@@@@@@ LOOP FOR TEAM ::: "+team.getTeamName());
						
						if(otherServiceList.size()!=0){
							scheduleFlexibleDayServices(date, team);
						}else{
							break;
						}
					}
					
					if(otherServiceList.size()==0){
						break;
					}
				}
				startDate.add(Calendar.DATE, 1);
			}
			
			if(otherServiceList.size()!=0){
				for(int i=0;i<otherServiceList.size();i++){
					Service service=otherServiceList.get(i);
					addServiceToUnscheduledList(service, "Insufficient Team Hours !",false);
				}
			}
			
			
		}
				
		showShedulingOutput();
		System.out.println();
		System.out.println();
		showTeamLocalityAllocation();
		
		return serList;
	}
	
	
	/**
	 * This method is used to schedule the services of flexible days.
	 * 
	 * @param date
	 * @param team
	 */
	
	
	private void scheduleFlexibleDayServices(String date,TeamWorkingHours team){
		System.out.println();
		System.out.println("OTHER LIST SIZE :: "+otherServiceList.size());
		Service service=otherServiceList.get(0);
		int serviceId=service.getCount();
		String locality=service.getLocality();
		Double d1=service.getServicingTime();
		int servicingTime=d1.intValue();
		String teamName=team.getTeamName();
		Double d2=team.getFromTime();
		Double d3=team.getToTime();
		int fromTime=d2.intValue();
		int toTime=d3.intValue();
		boolean dateFlag=false;
		boolean locFlage=false;
		boolean teamFlag=false;
		boolean flexibleTimeFlag=true;
		
		System.out.println("SERVICE DATE :: "+date);
		System.out.println("SERVICE ID :: "+serviceId);
		System.out.println("SERVICE LOCALITY :: "+locality);
		System.out.println("SERVICING TIME :: "+servicingTime);
		System.out.println("FROM TIME :: "+fromTime);
		System.out.println("TEAM NAME :: "+teamName);
		
		
		
		if(checkDateInShedulingMap(date)){
			if(checkLocalityInShedulingMap(date, locality)){
				dateFlag=true;
				locFlage=true;
				
				boolean checkTeamFlag=false;
				
				System.out.println("DATE MAP NOT EMPTY DATE AND LOCALITY PRESENT");
				System.out.println("-------------------------- 3");
				
				ArrayList<TeamWorkingHours> tList=getTeamListWithAvailableTimeSlot(date, locality);
				
				System.out.println("TEAM SIZE --- :: "+tList.size());
				
				if(tList.size()!=0){
					System.out.println("TEAM LIST NOT ZERO");
					teamFlag=true;
					for(int i=0;i<tList.size();i++){
						if(teamName.equals(tList.get(i).getTeamName())){
							checkTeamFlag=true;
							break;
						}
					}
					
					System.out.println("CHECK FLAGE :: "+checkTeamFlag);
					for(int i=0;i<tList.size();i++){
						System.out.println("--TEAM LOOP "+i);
						TeamWorkingHours team1=tList.get(i);
						int totalHrs=getTeamsTotalAvailableHours(date, locality,team1);
						System.out.println("TTTTTOTAL FLAG :: "+totalHrs);
						previousfromTime=0;
						scheduleServicesOfTeamsHasAssignedSpecificDayServices(date,locality,team1,totalHrs);
					}
				}
				
				
				if(checkTeamFlag==false){
					teamFlag=false;
					System.out.println("DATE MAP NOT EMPTY DATE AND LOCALITY PRESENT BUT NEW TEAM");
					System.out.println("-------------------------- 4");
					
					System.out.println("SERVICE IIIID :: "+serviceId);
					
					if(serviceId!=otherServiceList.get(0).getCount()){
						System.out.println("NNNNEW DETAILS...");
						service=otherServiceList.get(0);
						serviceId=otherServiceList.get(0).getCount();
						locality=otherServiceList.get(0).getLocality();
						Double d=service.getServicingTime();
						servicingTime=d.intValue();
						
						if(checkLocalityInShedulingMap(date, locality)){
							locFlage=true;
						}else{
							locFlage=false;
						}
						
					}
					
					
					schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,false);
					otherServiceList.remove(0);
					System.out.println("OTHER LIST SIZE 1:: "+otherServiceList.size());
					remainingHrs=0;
					System.out.println("REM HOURS 1:: "+remainingHrs);
					remainingHrs=remainingHrs+servicingTime;
					remainingHrs=(int) (team.getTotalWorkingHrs()-remainingHrs);
					scheduleTeamLocalityWise(date, servicingTime, fromTime, locality, team,false);
				}
				
				
				
				
			}else{
				System.out.println("DATE MAP NOT EMPTY DATE PRESENT BUT NOT LOCALITY");
				System.out.println("-------------------------- 2");
				dateFlag=true;
				
				schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,false);
				otherServiceList.remove(0);
				System.out.println("OTHER LIST SIZE 1:: "+otherServiceList.size());
				remainingHrs=0;
				System.out.println("REM HOURS 1:: "+remainingHrs);
				remainingHrs=remainingHrs+servicingTime;
				System.out.println("AFTER ADDING PR SER TM REM HOURS "+remainingHrs);
				remainingHrs=(int) (team.getTotalWorkingHrs()-remainingHrs);
				System.out.println("TM WR HRS "+team.getTotalWorkingHrs());
				
				scheduleTeamLocalityWise(date, servicingTime, fromTime, locality, team,false);
				
			}
			
		}else{
			System.out.println("DATE MAP EMPTY");
			System.out.println("-------------------------- 1");
			schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,false);
			otherServiceList.remove(0);
			System.out.println("OTHER LIST SIZE 1:: "+otherServiceList.size());
			remainingHrs=0;
			System.out.println("REM HOURS 1:: "+remainingHrs);
			remainingHrs=remainingHrs+servicingTime;
			System.out.println("AFTER ADDING PR SER TM REM HOURS "+remainingHrs);
			remainingHrs=(int) (team.getTotalWorkingHrs()-remainingHrs);
			System.out.println("TM WR HRS "+team.getTotalWorkingHrs());
			
			scheduleTeamLocalityWise(date, servicingTime, fromTime, locality, team,false);
			
		}
		
		
		
		
		
		
	}
	
	
	/**
	 * This method schedule service for those team who has already assigned specific Day services.
	 */
	
	
	private void scheduleServicesOfTeamsHasAssignedSpecificDayServices(String date,String locality,TeamWorkingHours team,int totalHrs){
		
		boolean dateFlag=true;
		boolean locFlage=true;
		boolean teamFlag=true;
		boolean flexibleTimeFlag=true;
		boolean specificServFlag=false;
		String teamName=team.getTeamName();
		int serviceId = 0;
		int fromTime;
		int servicingTime;
		Service service = null;
		
		System.out.println("TEAM NAME :: "+teamName);
		System.out.println("TOTAL HOURS :: "+totalHrs);
		System.out.println("LOCALITY :: "+locality);
		System.out.println("PREVIOUS FROM TIME :: "+previousfromTime);
		servicingTime=getServicingHours(date, locality, team, previousfromTime);
		System.out.println("SERVICING HOURS :: "+servicingTime);
		fromTime=getServicingFromTime(date, locality, team, previousfromTime);
		System.out.println("TEMP FROM TIME :: "+fromTime);
		previousfromTime=fromTime;
		fromTime=fromTime-servicingTime;
		System.out.println("FROM TIME :: "+fromTime);
		
		if(otherServiceList.size()!=0){
			if(!locality.equals(otherServiceList.get(0).getLocality())){
				locality=otherServiceList.get(0).getLocality();
				System.out.println("NEW LOCALITY IS :: "+locality);
				locFlage=false;
			}
		}
		if(totalHrs>0){
			ArrayList<Service> list=getServiceList(locality, servicingTime);
			if(list.size()!=0){
				totalHrs=totalHrs-servicingTime;
				System.out.println("NEW TOTAL HOURS :: "+totalHrs);
				service=list.get(0);
				serviceId=service.getCount();
				System.out.println("SERVICE ID :: "+serviceId);
				schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,specificServFlag);
				removeFromServiceList(serviceId);
			}else{
				totalHrs=totalHrs-servicingTime;
				System.out.println("NEW TOTAL HRS :: "+totalHrs);
			}
		}
		
		if(totalHrs>0){
			scheduleServicesOfTeamsHasAssignedSpecificDayServices(date,locality,team,totalHrs);
		}
		
		
		
	}
	
	private int getServicingHours(String date,String locality,TeamWorkingHours team,int previousfromTime ){
		String teamName=team.getTeamName();
		Double d1=team.getFromTime();
		int fromTime=d1.intValue();
		Double d2=team.getToTime();
		int toTime=d2.intValue();
		
		int count=0;
		int time=fromTime;
		if(fromTime>toTime){
			toTime=(int) (fromTime+team.getTeamWorkingHrs());
			toTime=toTime-1;
		}
		
		
		for(int i=fromTime;i<=toTime;i++){
			if(time>24){
				time=1;
			}
			System.out.println("  loop  "+i);
			if(dateWiseMap.get(date).get(locality).get(teamName).containsKey(time)){
				if(count!=0&&i>previousfromTime){
					break;
				}
			}else{
				count++;
			}
			time++;
		}
		
		return count;
	}
	
	private int getServicingFromTime(String date,String locality,TeamWorkingHours team,int previousfromTime){
		String teamName=team.getTeamName();
		Double d1=team.getFromTime();
		int fromTime=d1.intValue();
		Double d2=team.getToTime();
		int toTime=d2.intValue();
		
		int count=0;
		int time=fromTime;
		if(fromTime>toTime){
			toTime=(int) (fromTime+team.getTeamWorkingHrs());
			toTime=toTime-1;
		}
		
		
		for(int i=fromTime;i<=toTime;i++){
			if(time>24){
				time=1;
			}
			if(dateWiseMap.get(date).get(locality).get(teamName).containsKey(time)){
				if(count!=0&&i>previousfromTime){
					break;
				}
			}else{
				count++;
			}
			time++;
		}
		
		return time;
	}
	
	
	/**
	 * This method returns the Team list who's Time Slot is available for scheduling
	 */
	
	private ArrayList<TeamWorkingHours> getTeamListWithAvailableTimeSlot(String date,String locality){
		ArrayList<TeamWorkingHours> teamList=new ArrayList<TeamWorkingHours>();
		ArrayList<String> tlist=new ArrayList<String>();
		ArrayList<TeamWorkingHours> validTeamList=new ArrayList<TeamWorkingHours>();
		
		Set set =dateWiseMap.get(date).get(locality).entrySet();
		Iterator iterator1=set.iterator();
		while(iterator1.hasNext()){
			Map.Entry me=(Entry) iterator1.next();
			tlist.add(me.getKey().toString());
		}
		
		for(int i=0;i<tlist.size();i++){
			for(int j=0;j<listOfTeam.size();j++){
				if(tlist.get(i).equals(listOfTeam.get(j).getTeamName())){
					teamList.add(listOfTeam.get(j));
				}
			}
		}
		
		System.out.println("TEAM L SIZ "+teamList.size());
		
		for(int i=0;i<teamList.size();i++){
			TeamWorkingHours team=teamList.get(i);
			System.out.println("TEAM NAME :: "+team.getTeamName());
			System.out.println("InSIDE TEAM AVAILABLE SLOT METHOD");
			int totalHrs=getTeamsTotalAvailableHours(date, locality,team);
			
			System.out.println("TOTAL HOURS :: "+totalHrs);
			if(isServiceAvailableForTeamInLocality(locality, totalHrs)){
				System.out.println("TRUE :::");
				validTeamList.add(teamList.get(i));
			}
		}
		System.out.println("DATE :: "+date+" : "+locality);
		System.out.println("TEAM SIZE ::: "+validTeamList.size());
		System.out.println();
		
		for(int i=0;i<validTeamList.size();i++){
			System.out.println("T NM "+validTeamList.get(i).getTeamName()+" "+validTeamList.get(i).getFromTime());
		}
		
		
		
		return validTeamList;
	}
	
	
	/**
	 * This method returns the teams available hours.
	 */
	private int getTeamsTotalAvailableHours(String date,String locality,TeamWorkingHours team){
		System.out.println();
		System.out.println("INSIDE T T AVAL HRS "+date+" "+locality  );
		String teamName=team.getTeamName();
		Double d1=team.getFromTime();
		int fromTime=d1.intValue();
		Double d2=team.getToTime();
		int toTime=d2.intValue();
		System.out.println("Team Name :: "+teamName);
		int count=0;
		int time=fromTime;
		if(fromTime>toTime){
			toTime=(int) (fromTime+team.getTeamWorkingHrs());
			toTime=toTime-1;
		}
		
		
		for(int i=fromTime;i<=toTime;i++){
			if(time>24){
				time=1;
			}
			System.out.println("TIME :: "+time);
			if(dateWiseMap.get(date).get(locality).get(teamName).containsKey(time)){
				System.out.println("T");
			}else{
				System.out.println("F");
				count++;
			}
			time++;
		}
		
		System.out.println("TTL HOURS "+count);
		return count;
		
	}
	
	/**
	 * This method checks whether given hours service is available in locality.
	 */
	
	public boolean isServiceAvailableForTeamInLocality(String locality,int hours){
		
		for(int i=0;i<otherServiceList.size();i++){
			if(locality.equals(otherServiceList.get(i).getLocality())&&hours<=otherServiceList.get(i).getServicingTime()){
				return true;
			}
		}
		return false;
	}
	
	
	
	/**
	 * This is a recursive function which scheduled services to team until time slot available.
	 * 
	 * @param date
	 * @param previouseServivingHrs
	 * @param previousFromTime
	 * @param locality
	 * @param team
	 * @return
	 */
	
	private boolean scheduleTeamLocalityWise(String date,int previouseServivingHrs,int previousFromTime,String locality,TeamWorkingHours team,boolean remFlag){
		
		if(remFlag==true){
			remainingHrs=remainingHrs-previouseServivingHrs;
		}
		
		System.out.println("SUB FRM WOR HRS "+remainingHrs);
		
		int fromTime=previousFromTime+previouseServivingHrs;
		
		String teamName=team.getTeamName();
		
		boolean dateFlag=true;
		boolean locFlage=true;
		boolean teamFlag=true;
		boolean flexibleTimeFlag=true;
		boolean recursiveFlag=false;
		
		System.out.println();
		System.out.println("INSIDE RECURSIVE METHOD...");
		System.out.println("PREVIOUS SERVICE HRS :: "+previouseServivingHrs);
		System.out.println("PREVIOUS FROM TIME :: "+previousFromTime);
		System.out.println("TEAM NAME :: "+teamName);
		System.out.println("TEAM REAMAINING WORKING HOURS :: "+remainingHrs);
		System.out.println("TEAM FROM TIME :: "+fromTime);
		System.out.println("LOCALITY :: "+locality);
		
		if(fromTime>24){
			System.out.println("FROM Time Greater Than 24...");
			fromTime=fromTime-24;
			System.out.println("NEW FROM TIME >>> "+fromTime);
		}
		
		if(otherServiceList.size()!=0){
			if(!locality.equals(otherServiceList.get(0).getLocality())){
				locality=otherServiceList.get(0).getLocality();
				System.out.println("NEW LOCALITY IS :: "+locality);
				locFlage=false;
			}
		}
		System.out.println();
		
		
		
		ArrayList<Service> list=getServiceList(locality, remainingHrs);
		System.out.println("LIST SIZE :: "+list.size());
		int servicingTime=0;
		if(list.size()!=0){
			recursiveFlag=true;
			Service service=list.get(0);
			int serviceId=service.getCount();
			Double d1=service.getServicingTime();
			servicingTime=d1.intValue();
			System.out.println("SERVICE ID :::: "+serviceId);
			System.out.println("SERVICING HRS :: "+servicingTime);
			schedulingAndMapingServices(date, dateFlag, locality, locFlage, teamName, teamFlag, serviceId, fromTime, servicingTime, service,flexibleTimeFlag,false);
			removeFromServiceList(serviceId);
			System.out.println("OTHER LIST SIZE 2:: "+otherServiceList.size());
		}
		
		if(recursiveFlag==true){
			scheduleTeamLocalityWise(date, servicingTime, fromTime, locality, team,true);
		}
		return false;
	}
	
	/**
	 * 
	 * @param locality
	 * @param reamainingHrs
	 * @return
	 * 
	 * This method returns the services of remaining Hours of team.
	 * 
	 */
	
	private ArrayList<Service> getServiceList(String locality,int reamainingHrs){
		ArrayList<Service> list=new ArrayList<Service>();
		for(int i=0;i<otherServiceList.size();i++){
			if(otherServiceList.get(i).getLocality().equals(locality)&&otherServiceList.get(i).getServicingTime()<=reamainingHrs){
				list.add(otherServiceList.get(i));
			}
		}
		Comparator<Service> servicingTimeComp = new Comparator<Service>() {  
			@Override  
			public int compare(Service o1, Service o2) {  
				return o2.getServicingTime().compareTo(o1.getServicingTime());  
			}  
		};  
		Collections.sort(list,servicingTimeComp);
		
		return list;
	}
	
	/**
	 * This method remove the service which is already scheduled
	 * @param serviceId
	 */
	
	
	private void removeFromServiceList(int serviceId){
		for(int i=0;i<otherServiceList.size();i++){
			if(serviceId==otherServiceList.get(i).getCount()){
				otherServiceList.remove(i);
				break;
			}
		}
	}
	
	/***
	 * 
	 * @param date
	 * @param locality
	 * @param teamList
	 * @return
	 * 
	 * 
	 * This method checks whether team is already assigned for particular locality. 
	 */ 
	
	public boolean isTeamAssigenToLocalityForDay(String date,String locality,ArrayList<TeamWorkingHours>teamList){
		for(int i=0;i<teamList.size();i++){
			String teamName=teamList.get(i).getTeamName();
			
			if(dailyTeamLocAllocn.containsKey(date)){
				if(dailyTeamLocAllocn.get(date).containsKey(teamName)){
					if(dailyTeamLocAllocn.get(date).get(teamName).contains(locality)){
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/***
	 * 
	 * @param date
	 * @param locality
	 * @param teamList
	 * @return
	 * 
	 * 
	 * This method return the list of team which is working on particular locality.
	 */
	public ArrayList<TeamWorkingHours> getTeamListAssigenToLocalityForDay(String date,String locality,ArrayList<TeamWorkingHours>teamList){
		ArrayList<TeamWorkingHours> teamLst=new ArrayList<TeamWorkingHours>();
		ArrayList<String> tList=new ArrayList<String>();
		Set set=dateWiseMap.get(date).get(locality).entrySet();
		Iterator j=set.iterator();
		while(j.hasNext()){
			Map.Entry me=(Entry) j.next();
			System.out.println("Team :: "+me.getKey());
			tList.add(me.getKey().toString());
		}
		
		for(int i=0;i<tList.size();i++){
			System.out.println("TEAM NAME :: "+tList.get(i));
		}
		
		for(int i=0;i<tList.size();i++){
			for(int k=0;k<teamList.size();k++){
				if(tList.get(i).equals(teamList.get(k).getTeamName())){
					teamLst.add(teamList.get(k));
				}
			}
		}
		System.out.println("TEAM LIST SIZE :: "+teamLst.size());
		
		return teamLst;
	}
	
	/**
	 * 
	 * @param team
	 * @param date
	 * @param locality
	 * @param servicingTime
	 * @return
	 * 
	 * This method checks whether teams time slot is available for servicing.
	 */
	public boolean isTeamTimeSlotAvailable(TeamWorkingHours team,String date,String locality,int servicingTime){
		
		String teamName=team.getTeamName();
		System.out.println("TEAM NAME "+teamName);
		Double d1=team.getFromTime();
		int fromTime=d1.intValue();
		System.out.println("TEAM FROM TIME :: "+fromTime);
		
		Double d2=team.getToTime();
		int toTime=d2.intValue();
		System.out.println("TEAM TO TIME :: "+toTime);
		System.out.println("TOTAL SERVICING HRS :: "+servicingTime);
		
		int count=0;
		
		if(fromTime>toTime){
			toTime=(int) (fromTime+team.getTeamWorkingHrs());
			toTime=toTime-1;
		}
		
		
		for(int i=fromTime;i<=toTime;i++){
			if(dateWiseMap.get(date).get(locality).get(teamName).containsKey(fromTime)){
				count=0;
			}else{
				count++;
				if(count==servicingTime){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * if time slot for team is available then it returns from time.
	 * @param team
	 * @param date
	 * @param locality
	 * @param servicingTime
	 * @return
	 */
	
	public int getTeamFromServicingTime(TeamWorkingHours team,String date,String locality,int servicingTime){
		int time=0;
		
		
		String teamName=team.getTeamName();
		System.out.println("TEAM NAME "+teamName);
		Double d1=team.getFromTime();
		int fromTime=d1.intValue();
		System.out.println("TEAM FROM TIME :: "+fromTime);
		
		Double d2=team.getToTime();
		int toTime=d2.intValue();
		System.out.println("TEAM TO TIME :: "+toTime);
		System.out.println("TOTAL SERVICING HRS :: "+servicingTime);
		
		int count=0;
		
		for(int i=fromTime;i<=toTime;i++){
			
			if(dateWiseMap.get(date).get(locality).get(teamName).containsKey(fromTime)){
				count=0;
			}else{
				count++;
				if(count==servicingTime){
					i++;
					time=i-servicingTime;
					return time;
				}
			}
		}
		return time;
		
	}
	
	
	/**
	 * 
	 * @param date
	 * @param locality
	 * @param teamList
	 * @return
	 * 
	 * This method return the  list of available team 
	 * 
	 * 
	 */
	public ArrayList<TeamWorkingHours> getAvailableTeamListForDay(String date,String locality,ArrayList<TeamWorkingHours>teamList){
		ArrayList<TeamWorkingHours> teamLst=new ArrayList<TeamWorkingHours>();
		
		for(int i=0;i<teamList.size();i++){
			String teamName=teamList.get(i).getTeamName();
			
			if(dailyTeamLocAllocn.containsKey(date)){
				if(dailyTeamLocAllocn.get(date).containsKey(teamName)){
					
				}else{
					teamLst.add(teamList.get(i));
				}
			}
		}
		
		return teamLst;
	}
	
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	
	
	public boolean checkDateInShedulingMap(String date){
		if(dateWiseMap.containsKey(date)){
			return true;
		}
		return false;
	}
	
	public boolean checkLocalityInShedulingMap(String date,String locality){
		if(dateWiseMap.get(date).containsKey(locality)){
			return true;
		}
		return false;
	}
	
	public boolean checkTeamInShedulingMap(String date,String locality,String team){
		if(dateWiseMap.get(date).get(locality).containsKey(team)){
			return true;
		}
		return false;
	}
	
	
	public boolean checkHrsInShedulingMap(String date,String locality,String team,int hours){
		if(dateWiseMap.get(date).get(locality).get(team).containsKey(hours)){
			return true;
		}
		return false;
	}
	
	
	/***
	 * 
	 * This method is used to schedule the services and make a record of those services which is used to schedule
	 * further services.  
	 * 
	 * @param date
	 * @param dateFlag
	 * @param locality
	 * @param locFlage
	 * @param teamName
	 * @param teamFlag
	 * @param serviceId
	 * @param fromTime
	 * @param servicingTime
	 * @param service
	 * @param flexibleTimeFlag
	 */
	
	
	public void schedulingAndMapingServices(String date,boolean dateFlag,String locality,boolean locFlage,String teamName,boolean teamFlag,int serviceId,int fromTime,int servicingTime,Service service,boolean flexibleTimeFlag,boolean specificServFlag){
		
		HashMap<String,HashMap<String,HashMap<Integer,Integer>>> localityWiseMap=new HashMap<String,HashMap<String,HashMap<Integer,Integer>>>();
		HashMap<String,HashMap<Integer,Integer>> teamWiseMap=new HashMap<String,HashMap<Integer,Integer>>();
		HashMap<Integer,Integer> hrsWiseMap=new HashMap<Integer,Integer>();
		int serviceTime=fromTime;
		
		HashMap<String,ArrayList<String>>teamLocMap=new HashMap<String, ArrayList<String>>();
		ArrayList<String> localityList=new ArrayList<String>();
		Date servDate = null;
		try {
			servDate=sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(dateFlag==true){
			if(locFlage==true){
				if(teamFlag==true){
					System.out.println();
					System.out.println(" +++ DATE-LOCALITY-TEAM +++ ");
					int count=1;
					while(count<=servicingTime){
						if(fromTime>24){
							fromTime=1;
						}
						dateWiseMap.get(date).get(locality).get(teamName).put(fromTime, serviceId);
						fromTime++;
						count++;
					}
					
					teamFlag=true;
					Service serv=service;
					serv.setScheduled(true);
					serv.setTeam(teamName);
					serv.setServiceDate(servDate);
					serv.setSpecificServFlag(specificServFlag);
					if(flexibleTimeFlag==true){
						String servTime=getTimeInString12HrsFormate(serviceTime);
						serv.setServiceTime(servTime);
					}
					serList.add(service);
					
					
				}else{
					System.out.println(" +++ DATE-LOCALITY +++ ");
					int count=1;
					while(count<=servicingTime){
						if(fromTime>24){
							fromTime=1;
						}
						hrsWiseMap.put(fromTime, serviceId);
						fromTime++;
						count++;
					}
					
					dateWiseMap.get(date).get(locality).put(teamName, hrsWiseMap);
					
					teamFlag=true;
					Service serv=service;
					serv.setScheduled(true);
					serv.setTeam(teamName);
					serv.setServiceDate(servDate);
					serv.setSpecificServFlag(specificServFlag);
					if(flexibleTimeFlag==true){
						String servTime=getTimeInString12HrsFormate(serviceTime);
						serv.setServiceTime(servTime);
					}
					serList.add(service);
				}
				
			}else{
				System.out.println(" +++ DATE +++ ");
				int count=1;
				while(count<=servicingTime){
					if(fromTime>24){
						fromTime=1;
					}
					hrsWiseMap.put(fromTime, serviceId);
					fromTime++;
					count++;
				}
				teamWiseMap.put(teamName, hrsWiseMap);
				dateWiseMap.get(date).put(locality, teamWiseMap);
				
				teamFlag=true;
				Service serv=service;
				serv.setScheduled(true);
				serv.setTeam(teamName);
				serv.setServiceDate(servDate);
				serv.setSpecificServFlag(specificServFlag);
				if(flexibleTimeFlag==true){
					String servTime=getTimeInString12HrsFormate(serviceTime);
					serv.setServiceTime(servTime);
				}
				serList.add(service);
			}
		}else{
			System.out.println(" +++ NEW +++ ");
			
			int count=1;
			while(count<=servicingTime){
				if(fromTime>24){
					fromTime=1;
				}
				hrsWiseMap.put(fromTime, serviceId);
				fromTime++;
				count++;
			}
			
			teamWiseMap.put(teamName, hrsWiseMap);
			localityWiseMap.put(locality, teamWiseMap);
			dateWiseMap.put(date, localityWiseMap);
			
			
			teamFlag=true;
			Service serv=service;
			serv.setScheduled(true);
			serv.setTeam(teamName);
			serv.setServiceDate(servDate);
			serv.setSpecificServFlag(specificServFlag);
			if(flexibleTimeFlag==true){
				String servTime=getTimeInString12HrsFormate(serviceTime);
				serv.setServiceTime(servTime);
			}
			serList.add(service);
		}
		
		/**
		 * Assigning locality to team
		 */
		
		
		if(dailyTeamLocAllocn.size()!=0){
			if(dailyTeamLocAllocn.containsKey(date)){
				if(dailyTeamLocAllocn.get(date).containsKey(teamName)){
					if(dailyTeamLocAllocn.get(date).get(teamName).contains(locality)){
						
					}else{
						dailyTeamLocAllocn.get(date).get(teamName).add(locality);
					}
					
				}else{
					localityList=new ArrayList<String>();
					localityList.add(locality);
					dailyTeamLocAllocn.get(date).put(teamName, localityList);
				}
				
			}else{
				localityList=new ArrayList<String>();
				localityList.add(locality);
				teamLocMap.put(teamName, localityList);
				dailyTeamLocAllocn.put(date, teamLocMap);
			}
		}else{
			localityList=new ArrayList<String>();
			localityList.add(locality);
			teamLocMap.put(teamName, localityList);
			dailyTeamLocAllocn.put(date, teamLocMap);
		}
		
		System.out.println();
	}
	
	/**
	 * This method sets the service dates , time hour and second to zero.
	 * 
	 * @param serviceDate
	 * @return
	 */
	
	public Date getServiceDate(Date serviceDate){
		Date date = null;
		
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(serviceDate);
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.HOUR, 00);
        date=calendar.getTime();
		return date;
	}
	
	
	/**
	 * This method convert 12 hrs format date to 24 hrs format.
	 */
	
	public int getTimeIn24HrsFormate(String serviceTime){
		int time = 0;
		
		if(serviceTime.contains("PM")){
//			System.out.println("PM");
			String arr[]=serviceTime.split(":");
			String no=arr[0];
			int noInt=Integer.parseInt(no);
			int totalhrs=noInt+12;
			time=totalhrs;
			return time;
		}else{
//			System.out.println("AM");
			serviceTime.replace("AM","");
			String arr[]=serviceTime.split(":");
			String no=arr[0];
			int totalhrs=Integer.parseInt(no);
			time=totalhrs;
			return time;
		}
	}
	
	/**
	 * This method convert 24 hrs format date to 12 hrs format.
	 */
	
	public String getTimeInString12HrsFormate(int serviceTime){
		String serTime;
		
		if(serviceTime>12){
			serviceTime=serviceTime-12;
			serTime=serviceTime+":00 PM";
		}else{
			serTime=serviceTime+":00 AM";
		}
		
		return serTime;
	}
	
	/***
	 * This methods returns the team list available for that specific time.
	 * 
	 * @param teamList
	 * @param serviceTime
	 * @param servicingTime
	 * @return
	 */
	
	public ArrayList<TeamWorkingHours> getTeamListForSpecificServiceTime(ArrayList<TeamWorkingHours>teamList,int serviceTime,int servicingTime){
		ArrayList<TeamWorkingHours> tList=new ArrayList<TeamWorkingHours>();
		int serviceToTime=serviceTime+servicingTime;
//		System.out.println("Service From Time :: "+serviceTime);
//		System.out.println("Service To Time :: "+serviceToTime);
		
		HashMap<String,ArrayList<Integer>> teamWorkingHrs=new HashMap<String,ArrayList<Integer>>();
//		HashMap<Integer,String> teamWorkingHrs=new HashMap<Integer, String>();
		ArrayList<Integer> intList=new ArrayList<Integer>();
		
		
		
		for(int i=0;i<teamList.size();i++){
			Double d1=teamList.get(i).getFromTime();
			int fromTime=d1.intValue();
//			System.out.println("TEAM FROM TIME :: "+fromTime);
			
			Double d2=teamList.get(i).getToTime();
			int toTime=d2.intValue();
//			System.out.println("TEAM TO TIME :: "+toTime);
			
			String teamName=teamList.get(i).getTeamName();
			
			
			if(fromTime>toTime){
//				System.out.println("INSIDE FROM TIME IS GREATER THAN TO TIME ...");
				toTime=(int) (fromTime+teamList.get(i).getTeamWorkingHrs());
				toTime=toTime-1;
//				System.out.println("NEW TEAM TO TIME :: "+toTime);
			}
//			System.out.println("BEFORE LOOP");
			int time=fromTime;
			for(int j=fromTime;j<=toTime;j++){
//				System.out.println("LOOP "+j +" FRM TM "+time);
				if(time>24){
					time=1;
				}
				
				if(teamWorkingHrs.size()!=0){
//					System.out.println("MAP NOT EMPTY");
					if(teamWorkingHrs.containsKey(teamName)){
//						System.out.println("TEAM ALREADY PRESENT");
						teamWorkingHrs.get(teamName).add(time);
					}else{
//						System.out.println("NEW TEAM ");
						intList=new ArrayList<Integer>();
						intList.add(time);
						teamWorkingHrs.put(teamName, intList);
					}
				}else{
//					System.out.println("MAP EMPTY");
					intList=new ArrayList<Integer>();
					intList.add(time);
					teamWorkingHrs.put(teamName, intList);
				}
				
				time++;
				
			}
			
		}
		
		for(int i=0;i<teamList.size();i++){
			if(teamWorkingHrs.get(teamList.get(i).getTeamName()).contains(serviceTime)&&teamWorkingHrs.get(teamList.get(i).getTeamName()).contains(serviceToTime)){
				tList.add(teamList.get(i));
			}
		}
		
		return tList;
	}
	
	/***
	 * this method checks holiday and weekly off.
	 * @param date
	 * @return
	 */
	
	
	private boolean checkWeeklyOffnHoliday(Date date){
		
		Date dummyDate=date;
		Calendar c = Calendar.getInstance();
		c.setTime(dummyDate);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date lastDayOfMonth = c.getTime();
		
		System.out.println("*****************888 LAST DAY OF DAY :: "+lastDayOfMonth);
		if(lastDayOfMonth.equals(date)){
			return true;
		}else{
			if(listOfHolidays.size()!=0){
				for(int i=0;i<listOfHolidays.size();i++){
					if(date.equals(listOfHolidays.get(i).getHolidayDate())){
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	private String getHolidayName(Date day){
		
		Date dummyDate=day;
		Calendar c = Calendar.getInstance();
		c.setTime(dummyDate);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date lastDayOfMonth = c.getTime();
		if(lastDayOfMonth.equals(day)){
			return "Last day of month.";
		}else{
			if(listOfHolidays.size()!=0){
				for(int i=0;i<listOfHolidays.size();i++){
					if(day.equals(listOfHolidays.get(i).getHolidayDate())){
						return listOfHolidays.get(i).getHolidayName();
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param servic
	 * @param reason
	 * 
	 * This method mark service as unscheduled
	 */
	
	public void addServiceToUnscheduledList(Service servic,String reason,boolean specific){
		Service service=servic;
		service.setScheduled(false);
		service.setSpecificServFlag(specific);
		service.setReason(reason);
		serList.add(service);
	}

	
	
	/***
	 * 
	 * DISPLAY METHOD.............
	 */
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void showShedulingOutput(){
		/**
		 * key -Date
		 * Key -Locality
		 * Key -Team
		 * Key -Hours ,Value -Service Number
		 * 
		 */
		
		Map<String,HashMap<String,HashMap<String,HashMap<Integer,Integer>>>> map3=new TreeMap<String, HashMap<String,HashMap<String,HashMap<Integer,Integer>>>>(dateWiseMap);
		
		Set set = map3.entrySet();
	    Iterator i = set.iterator();
	    while(i.hasNext()){
	    	 Map.Entry me = (Map.Entry)i.next();
	    	 System.out.println();
	    	 
		     HashMap<String,HashMap<String,HashMap<Integer,Integer>>> locWiseMap1=(HashMap<String, HashMap<String, HashMap<Integer, Integer>>>) me.getValue();
		     Map<String,HashMap<String,HashMap<Integer,Integer>>> locWiseMap=new TreeMap<String,HashMap<String,HashMap<Integer,Integer>>>(locWiseMap1);
		     Set set2 = locWiseMap.entrySet();
			 Iterator h = set2.iterator();
		     
			 while(h.hasNext()){
				 Map.Entry me0 = (Map.Entry)h.next();
				 
			     HashMap<String,HashMap<Integer,Integer>> teamwisemap1= (HashMap<String, HashMap<Integer, Integer>>) me0.getValue();
		    
			     Map<String,HashMap<Integer,Integer>> teamwisemap=new TreeMap<String,HashMap<Integer,Integer>>(teamwisemap1);
			     Set set1 = teamwisemap.entrySet();
				 Iterator j = set1.iterator();
				 
				 while(j.hasNext()){
					 Map.Entry me1 = (Map.Entry)j.next();
					 
					 HashMap<Integer,Integer> hrWiseMap1= (HashMap<Integer, Integer>) me1.getValue();
					 Map<Integer,Integer> hrWiseMap=new TreeMap<Integer, Integer>(hrWiseMap1);
					 Set set3=hrWiseMap.entrySet();
					 Iterator k=set3.iterator();
					 
					 System.out.println(me.getKey() + " : "+me0.getKey()+" : "+me1.getKey());
					 System.out.println();
					  
					 while(k.hasNext()){
						 Map.Entry me2 = (Map.Entry)k.next();
						 System.out.println(me2.getKey()+" :: "+me2.getValue());
					 }
				 }
			 }
	    }
	}
	
	
@SuppressWarnings({ "rawtypes", "unchecked" })
public void showTeamLocalityAllocation(){
		Map<String,HashMap<String,ArrayList<String>>> map1=new TreeMap<String,HashMap<String,ArrayList<String>>>(dailyTeamLocAllocn);
		
		Set set3 = map1.entrySet();
        Iterator iterator3 = set3.iterator();
        while(iterator3.hasNext()) {
            Map.Entry me2 = (Map.Entry)iterator3.next();
            System.out.print(me2.getKey() + ": ");
            System.out.println();
            HashMap<String,ArrayList<String>> teamMap=(HashMap<String, ArrayList<String>>) me2.getValue();
            Map<String, ArrayList<String>> map4= new TreeMap<String, ArrayList<String>>(teamMap); 
            
            Set set4 = map4.entrySet();
            Iterator iterator2 = set4.iterator();
            
            while(iterator2.hasNext()) {
            	 Map.Entry me3 = (Map.Entry)iterator2.next();
            	 
            	 ArrayList<String>locality=(ArrayList<String>) me3.getValue();
            	 Collections.sort(locality);
            	 for(int i=0;i<locality.size();i++){
            		 System.out.print(me3.getKey() + ": ");
                     System.out.println(locality.get(i));
            	 }
            	 System.out.println();
                 
            }
       }
	}
	
	
	private Date getNewDate(Date date){
		Date newDate=DateUtility.getDateWithTimeZone("IST", date);
		Calendar sDate = Calendar.getInstance();
		sDate.setTime(newDate);
		sDate.add(Calendar.DATE, 1);
		return sDate.getTime();
	}

	@Override
	public void saveServices(ArrayList<Service> services) {
		
		for(int i=0;i<services.size();i++){
			Service service=ofy().load().type(Service.class).filter("companyId", services.get(i).getCompanyId()).filter("count", services.get(i).getCount()).first().now();
			
			if(service!=null){
				service=services.get(i);
				ofy().save().entity(service).now();
			}
		}
		
	}
	
	/**
	 * referance from
	 *  https://examples.javacodegeeks.com/core-java/util/regex/matcher/validate-date-with-java-regular-expression-example/
	 */
	private static Pattern dateFrmtPtrn = 
            Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)");
     
	@Override
    public Boolean validateDateFormat(String userName){
         
		Matcher matcher;
	     matcher = dateFrmtPtrn.matcher(userName);
	 
	     if(matcher.matches()){
	 
	     matcher.reset();
	 
	     if(matcher.find()){
	 
	         String dd = matcher.group(1);
	         String mm = matcher.group(2);
	         int yy = Integer.parseInt(matcher.group(3));
	 
	         if (dd.equals("31") &&  (mm.equals("4") || mm .equals("6") || mm.equals("9") ||
	                  mm.equals("11") || mm.equals("04") || mm .equals("06") ||
	                  mm.equals("09"))) {
	            return false;
	         } else if (mm.equals("2") || mm.equals("02")) {
	          if(yy % 4==0){
	              if(dd.equals("30") || dd.equals("31")){
	                  return false;
	              }else{
	              }
	          }else{
	                 if(dd.equals("29")||dd.equals("30")||dd.equals("31")){
	                  return false;
	                 }else{
	                  return true;
	              }
	          }
	          }else{                
	        return true;                
	          }
	       }else{
	              return false;
	       }         
	     }else{
	      return false;
	     }   
	     return false;
    }
	

}
