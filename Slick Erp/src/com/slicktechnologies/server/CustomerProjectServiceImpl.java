package com.slicktechnologies.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.CustomerProjectService;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CustomerProjectServiceImpl extends RemoteServiceServlet implements CustomerProjectService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6061610173934647916L;
	Logger logger=Logger.getLogger("Project Creation Logger");
	@Override
	public Integer createCustomerProject(long companyId, int contractCount,int serviceCount,String serviceStatus) {
		
		Logger logger=Logger.getLogger("Project Creation Logger");
		int passingValue=0;
		List<ServiceProject> projEntityList=ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", contractCount).filter("serviceId", serviceCount).list();
		logger.log(Level.SEVERE,"SizeOf Project"+projEntityList.size());
		
		if(projEntityList.size()>0){
			logger.log(Level.SEVERE,"Size Greateer than 0");
			passingValue = 1;
		}
		else if(projEntityList.size()==0 && serviceStatus.equals("Completed") ){
			passingValue = -1;
			return passingValue;
		}
		else{
			logger.log(Level.SEVERE,"Size is 0");
			createProject(companyId,contractCount,serviceCount,null);
			passingValue=1;
		}
		logger.log(Level.SEVERE,"Passing Value=="+passingValue);
		return passingValue;
	}

//	@Override
//	public Integer checkCustomerProject(long companyId, int contractCount,int serviceCount,String serviceStatus,ArrayList<EmployeeInfo> technicians) {
//		int passingValue=0;
//		try {
//			
//			List<ServiceProject> projEntityList=ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", contractCount).filter("serviceId", serviceCount).list();
//			logger.log(Level.SEVERE,"SizeOf Project"+projEntityList.size());
//			
//			List<ServiceProject> projEntityListCreated = new ArrayList<ServiceProject>();
//			if(projEntityList.size()>0){
//				logger.log(Level.SEVERE,"Size Greateer than 0");
//				int com = 0,can = 0;
//				for(ServiceProject serPro : projEntityList){
//					if(serPro.getProjectStatus().equals(ServiceProject.CREATED)){
//						projEntityListCreated.add(serPro);
//					}
//					if(serPro.getProjectStatus().equals(ServiceProject.COMPLETED))
//					{
//						com++;
//					}
//					if(serPro.getProjectStatus().equals(ServiceProject.CANCELLED))
//					{
//						can++;
//					}
//					
//					if(com>0){
//						passingValue = -1;
//					}
//					if(can>0 && com==0){
//						passingValue = -2;
//					}
//				}
//				
//				if(projEntityListCreated.size()==1){
//					try {
//						logger.log(Level.SEVERE, "project id -- "+ projEntityListCreated.get(0).getCount());
//						projEntityListCreated.get(0).setTechnicians(technicians);
//						logger.log(Level.SEVERE, "technicians size -- "+ projEntityListCreated.get(0).getTechnicians().size());
//						ofy().save().entity(projEntityListCreated.get(0)).now();
//						
//					} catch (Exception e) {
//						e.printStackTrace();
//						System.out.println(e.getMessage());
//						logger.log(Level.SEVERE,"update project employyee - " + e.getMessage());
//					}
////					UpdateProjectTechEmployee(projEntityListCreated.get(0),technicians);
//					passingValue=1;
//				}else if(projEntityListCreated.size()>1){
//					Comparator<ServiceProject> serProIdComparator = new Comparator<ServiceProject>() {
//						public int compare(ServiceProject c1, ServiceProject c2) {
//						
//							if(c1.getCount()== c2.getCount()){
//								return 0;}
//							if(c1.getCount()> c2.getCount()){
//								return 1;}
//							else{
//								return -1;}
//						}
//					};
//					logger.log(Level.SEVERE, " before technicians size -- "+ projEntityListCreated.get(0).getTechnicians().size());
//					Collections.sort(projEntityListCreated,serProIdComparator);
//					try {
//						logger.log(Level.SEVERE, "project id -- "+ projEntityListCreated.get(0).getCount());
//						projEntityListCreated.get(0).setTechnicians(technicians);
//						logger.log(Level.SEVERE, "technicians size -- "+ projEntityListCreated.get(0).getTechnicians().size());
//						ofy().save().entity(projEntityListCreated.get(0)).now();
//						
//					} catch (Exception e) {
//						e.printStackTrace();
//						System.out.println(e.getMessage());
//						logger.log(Level.SEVERE,"update project employyee - " + e.getMessage());
//					}
////					UpdateProjectTechEmployee(projEntityListCreated.get(0),technicians);
//					passingValue=1;
//				}
//			}
//			else if(projEntityList.size()==0 ){
//				logger.log(Level.SEVERE,"Size is 0");
//				createProject(companyId,contractCount,serviceCount,technicians);
//				passingValue=1;
//			}
//			
//			logger.log(Level.SEVERE,"Passing Value=="+passingValue);
//			
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			e.printStackTrace();
//			logger.log(Level.SEVERE, "Check serivce pro - " + e.getMessage());
//			logger.log(Level.SEVERE, "Check serivce pro -  seri id - " + serviceCount);
//		}
//		return passingValue;
//		
//	}
	
	
	@Override
	public Integer checkCustomerProject(long companyId, int contractCount,int serviceCount,String serviceStatus,ArrayList<EmployeeInfo> technicians,Service serEntity,List<ProductGroupList> proGroupList,int serProjectID) {
		int passingValue=0;
		try {
			
			List<ServiceProject> projEntityList=ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", contractCount).filter("serviceId", serviceCount).list();
			logger.log(Level.SEVERE,"SizeOf Project"+projEntityList.size());
			
			List<ServiceProject> projEntityListCreated = new ArrayList<ServiceProject>();
			if(projEntityList.size()>0){
				logger.log(Level.SEVERE,"Size Greateer than 0");
				int com = 0,can = 0;
				for(ServiceProject serPro : projEntityList){
					if(serPro.getProjectStatus().equals(ServiceProject.CREATED)){
						projEntityListCreated.add(serPro);
					}
					if(serPro.getProjectStatus().equals(ServiceProject.COMPLETED))
					{
						com++;
					}
					if(serPro.getProjectStatus().equals(ServiceProject.CANCELLED))
					{
						can++;
					}
					
					if(com>0){
						passingValue = -1;
					}
					if(can>0 && com==0){
						passingValue = -2;
					}
				}
				
				if(projEntityListCreated.size()==1){
					try {
						logger.log(Level.SEVERE, "project id -- "+ projEntityListCreated.get(0).getCount());
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.getMessage());
						logger.log(Level.SEVERE,"update project employyee - " + e.getMessage());
					}
					passingValue=1;
				}else if(projEntityListCreated.size()>1){
					Comparator<ServiceProject> serProIdComparator = new Comparator<ServiceProject>() {
						public int compare(ServiceProject c1, ServiceProject c2) {
						
							if(c1.getCount()== c2.getCount()){
								return 0;}
							if(c1.getCount()> c2.getCount()){
								return 1;}
							else{
								return -1;}
						}
					};
					logger.log(Level.SEVERE, " before technicians size -- "+ projEntityListCreated.get(0).getTechnicians().size());
					Collections.sort(projEntityListCreated,serProIdComparator);
					try {
						logger.log(Level.SEVERE, "project id -- "+ projEntityListCreated.get(0).getCount());
						
						
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.getMessage());
						logger.log(Level.SEVERE,"update project employyee - " + e.getMessage());
					}
					passingValue=1;
				}
				if(serviceStatus.equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
					projEntityList.get(0).setProjectStatus(ServiceProject.COMPLETED);
					
				}
				passingValue = projEntityListCreated.get(0).getCount();
				if(proGroupList!=null && proGroupList.size()>0){
					projEntityListCreated.get(0).setProdDetailsList(proGroupList);
				}
				projEntityListCreated.get(0).setTechnicians(technicians);
				logger.log(Level.SEVERE, "technicians size -- "+ projEntityListCreated.get(0).getTechnicians().size());
				ofy().save().entity(projEntityListCreated.get(0)).now();
			}
			else if(projEntityList.size()==0 ){
				logger.log(Level.SEVERE,"Size is 0");
				if(serEntity!=null){
				 createProject(companyId,contractCount,serviceCount,technicians,serEntity,proGroupList,serProjectID);
				}else{
					createProject(companyId,contractCount,serviceCount,technicians);
				}
				 passingValue =1;
			}
			
			logger.log(Level.SEVERE,"Passing Value=="+passingValue);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			logger.log(Level.SEVERE, "Check serivce pro - " + e.getMessage());
			logger.log(Level.SEVERE, "Check serivce pro -  seri id - " + serviceCount);
		}
		return passingValue;
		
	}
	
	
	
	
	public void UpdateProjectTechEmployee(ServiceProject servPro,ArrayList<EmployeeInfo> technicians){
		try {
			logger.log(Level.SEVERE, "project id -- "+ servPro.getCount());
			servPro.setTechnicians(technicians);
			
			ofy().save().entity(servPro).now();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE,"update project employyee - " + e.getMessage());
		}
	}
	
	/*
	 * nidhi
	 * 17-11-2017
	 * method updated for MIN and customer project Upload 
	 * PRocess
	 */
	
	public void createProject(long companyId,int contractId,int serviceId,ArrayList<EmployeeInfo> technicians)
	{
		ServiceProject projectEntity=getFieldsToCustomerProjectScreen(companyId,contractId,serviceId,technicians);
			   GenricServiceImpl impl=new GenricServiceImpl();
				if(projectEntity!=null){
					impl.save(projectEntity);
				}
				
		/**** Date 26-06-2018 vijay for set project id  in service for schedule in customer service list and scheduling list**/				
		Service serviceEntity=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractId).filter("count", serviceId).first().now();
		serviceEntity.setProjectId(projectEntity.getCount());
		ofy().save().entity(serviceEntity);
		/**** ends here **********/
							
	}
	
	/*
	 * nidhi
	 * 17-11-2017
	 * method updated for MIN and customer project Upload 
	 * PRocess
	 */
	
	public void createProject(long companyId,int contractId,int serviceId,ArrayList<EmployeeInfo> technicians,Service serviceEntity, List<ProductGroupList> proGroupList,int serProjectId)
	{
		ServiceProject projectEntity=getFieldsCreateCustomerProject(companyId,contractId,serviceId,serviceEntity,technicians, proGroupList);
			
		if(serProjectId!=0){
			logger.log(Level.SEVERE,"get service id --"+ serviceId + " get project id --" + serProjectId);
			projectEntity.setCount(serProjectId);
			if(projectEntity!=null){
				ofy().save().entity(projectEntity);
			}
		}else{
			GenricServiceImpl impl=new GenricServiceImpl();
			if(projectEntity!=null){
			    impl.save(projectEntity);
			}
		}
		
		
	}
	
	/*
	 * nidhi
	 * 17-11-2017
	 * method updated for MIN and customer project Upload 
	 * PRocess
	 */

	private ServiceProject getFieldsToCustomerProjectScreen(long companyId,int contractCount,int serviceId,ArrayList<EmployeeInfo> technicians) {
		
		/** Service Entity Loading */
		Service serviceEntity=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractCount).filter("count", serviceId).first().now();
		
		/** Declaration Of Entities */
	
		return getFieldsCreateCustomerProject(companyId,contractCount,serviceId,serviceEntity, technicians,null);
	}
			
		private ServiceProject getFieldsCreateCustomerProject(long companyId,int contractCount,int serviceId,Service serviceEntity,ArrayList<EmployeeInfo> technicians , List<ProductGroupList> proGroupList){
			ServiceProject cusproject=new ServiceProject();
			try {
				
				 Customer cust=null;
				 Address addr=null;
				 Employee empEntity=null;
				 BillOfMaterial bomEntity=null;
				 
				 /** Loading Customer Information */
				 
				if(serviceEntity!=null&&serviceEntity.getPersonInfo().getCount()!=0)
				{
					 cust=ofy().load().type(Customer.class).filter("count",serviceEntity.getPersonInfo().getCount()).filter("companyId",companyId).first().now();
				}
				
				 if(cust!=null){
					  addr=cust.getSecondaryAdress();
				 }
			
				/** Loading Employee Information */
				 //  vijay modified if condition added( serviceEntity.getEmployee()!=null)this to codition )
				 //Date : 15/2/2017
				 if(serviceEntity.getCompanyId()!=null && serviceEntity.getEmployee()!=null){
					 empEntity=ofy().load().type(Employee.class).filter("fullname",serviceEntity.getEmployee().trim()).filter("companyId",companyId).first().now();
				 }
				 
				 
			 	 /**
				  * nidhi
				  * 28-09-2018
				  */
				 boolean bomProcessActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", serviceEntity.getCompanyId());
					
				
				 /** Loading Bill Of Material */
				 /**
				  * @author Anil, Date : 16-03-2020
				  * if bill of material process configuration is active then only we will load BOM
				  */
				 if(serviceEntity.getCompanyId()!=null&&bomProcessActive){
					 bomEntity=ofy().load().type(BillOfMaterial.class).filter("product_id", serviceEntity.getProduct().getCount()).filter("companyId",companyId).filter("status",true).first().now();
				 }
				
				
				
				cusproject.setserviceId(serviceId);
				cusproject.setCompanyId(companyId);
				cusproject.setserviceEngineer(serviceEntity.getEmployee());
				cusproject.setServiceStatus(serviceEntity.getStatus());
				cusproject.setserviceDate(serviceEntity.getServiceDate());
				cusproject.setContractId(contractCount);
				cusproject.setContractStartDate(serviceEntity.getContractStartDate());
				cusproject.setContractEndDate(serviceEntity.getContractEndDate());
				cusproject.setPersonInfo(serviceEntity.getPersonInfo());
//				cusproject.setPocName(serviceEntity.getEmployee());
				/**
				 *   rohan commented this line because of getting error As unexpected error
				 *   date: 25/01/2017
				 *   No need to set one field for 2 times 
				 */
	//					cusproject.setPocName(empEntity.getFullname());
				// ends here 
				
				/**
				 *  nidhi
				 *  11-12-2017
				 *  stop copy emp name and insted of emp name poc name as cutomer name
				 *  
				 */
//				cusproject.setPocName(serviceEntity.getEmployee());
				cusproject.setPocName(serviceEntity.getPersonInfo().getPocName());
				/**
				 *  end
				 */
				/** Date 13 Feb 2017
				 * if condition added by vijay for if service engineer is blank in service
				 * then dont set bellow details
				 */
				if(empEntity!=null){
					cusproject.setPocLandline(empEntity.getLandline());
					cusproject.setPocCell(empEntity.getCellNumber1());
					cusproject.setPocEmail(empEntity.getEmail());
				}
				/**
				 * end here
				 */
				cusproject.setProjectName("Project-"+serviceId);
				
				cusproject.setProjectStatus("Created");
				if(serviceEntity.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
					cusproject.setProjectStatus(cusproject.COMPLETED);
				}
				cusproject.setAddr(addr);
				cusproject.setServiceSrNo(serviceEntity.getServiceSerialNo());
				
				if(bomEntity!=null){
					List<CompanyAsset> companyAssetList=getToolsFromProductId(bomEntity);
					if(companyAssetList!=null){
						cusproject.setTooltable(companyAssetList);
					}
				}
				
				
				if(empEntity!=null){
					cusproject.setTechnicians(getTechiciansFromEmployeeInfo(empEntity));
				}
				if(technicians!=null){
					cusproject.setTechnicians(technicians);
				}
				
//				/**
//				 * nidhi
//				 * 28-09-2018
//				 */
//				boolean bomProcessActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", serviceEntity.getCompanyId());
				
				if(!bomProcessActive){
					if(proGroupList!=null && proGroupList.size()>0){
						
						cusproject.setProdDetailsList(proGroupList);
					}else{
						List<ProductGroupList> billProdList=getMaterialAndToolFromProductId(companyId,bomEntity,serviceEntity);
						
						if(billProdList.size()!=0){
							cusproject.setProdDetailsList(billProdList);
						}
						
					}
				}
				
				
				
//				cusproject.setComment("This customer project is created through min upload process.");
				
				/**
				 * nidhi
				 * 28-09-2018
				 *  *:*:*
				 */
				
				if(bomProcessActive && bomEntity != null && serviceEntity.getQuantity()>0){
							/**
							 * nidhi ||*|| 26-12-2018
							 */
						if(serviceEntity.getServiceProductList() != null && serviceEntity.getServiceProductList().size()>0){
							
							ArrayList<ProductGroupList> prodDetailslist = new ArrayList<ProductGroupList>();
							
						for(int i=0;i<serviceEntity.getServiceProductList().size();i++){
							ProductGroupList prodDetailsListEntity=new ProductGroupList();
							/**
							  * 27-12-2018 
							  * nidhi ||*||
							  */
							 /*
							 prodDetailsListEntity.setProduct_id(serviceEntity.getServiceProductList().get(i).getProduct_id());
							 prodDetailsListEntity.setCode(serviceEntity.getServiceProductList().get(i).getCode());
							 prodDetailsListEntity.setName(serviceEntity.getServiceProductList().get(i).getName());
							 prodDetailsListEntity.setQuantity(serviceEntity.getServiceProductList().get(i).getQuantity());
							 prodDetailsListEntity.setUnit(serviceEntity.getServiceProductList().get(i).getUnit());
							 prodDetailsListEntity.setProActualQty(serviceEntity.getServiceProductList().get(i).getProActualQty());
							 prodDetailsListEntity.setProActualUnit(serviceEntity.getServiceProductList().get(i).getProActualUnit());
							 *//*
							 prodDetailsListEntity.setPlannedQty(serviceEntity.getServiceProductList().get(i).getQuantity());
							 prodDetailsListEntity.setPlannedUnit(serviceEntity.getServiceProductList().get(i).getUnit());
							 */
							
							prodDetailsListEntity = serviceEntity.getServiceProdDt(serviceEntity.getServiceProductList().get(i));
							/**
							  * end
							  */
					
							 prodDetailslist.add(prodDetailsListEntity); 
						 
						}
							cusproject.setProdDetailsList(prodDetailslist);
						}else{
							if(serviceEntity.getQuantity()>0){
								ServerUnitConversionUtility  serUtility = new ServerUnitConversionUtility();
								
							 ArrayList<ProductGroupList> serProGroupList =	serUtility.getServiceitemProList(null, null, serviceEntity, bomEntity);
							 
							 cusproject.setProdDetailsList(serProGroupList);
							 
							 ArrayList<ServiceProductGroupList> listDt = new ArrayList<ServiceProductGroupList>();
							 
							 for(int k = 0 ; k<serProGroupList.size();k++ ){
								 listDt.add(serviceEntity.getServiceProdDt(serProGroupList.get(k)));
							 }
							 serviceEntity.setServiceProductList(listDt);
							 ofy().save().entity(serviceEntity);
							}
						}
						
				}else{

					if(proGroupList!=null && proGroupList.size()>0){
						
						cusproject.setProdDetailsList(proGroupList);
					}else{
						List<ProductGroupList> billProdList=getMaterialAndToolFromProductId(companyId,bomEntity,serviceEntity);
						
						if(billProdList.size()!=0){
							cusproject.setProdDetailsList(billProdList);
						}
						
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
				logger.log(Level.SEVERE, "Cretae customer projecct -- ");
			}
			return cusproject;
		}
	
//	public void createProject(long companyId,int contractId,int serviceId,ArrayList<EmployeeInfo> technicians)
//	{
//		ServiceProject projectEntity=getFieldsToCustomerProjectScreen(companyId,contractId,serviceId,technicians);
//			   GenricServiceImpl impl=new GenricServiceImpl();
//				if(projectEntity!=null){
//				    impl.save(projectEntity);
//				}
//	}
//
//			private ServiceProject getFieldsToCustomerProjectScreen(long companyId,int contractCount,int serviceId,ArrayList<EmployeeInfo> technicians) {
//				
//				/** Service Entity Loading */
//				Service serviceEntity=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractCount).filter("count", serviceId).first().now();
//				
//				/** Declaration Of Entities */
//				
//				 Customer cust=null;
//				 Address addr=null;
//				 Employee empEntity=null;
//				 BillOfMaterial bomEntity=null;
//				 
//				 /** Loading Customer Information */
//				 
//				if(serviceEntity!=null&&serviceEntity.getPersonInfo().getCount()!=0)
//				{
//					 cust=ofy().load().type(Customer.class).filter("count",serviceEntity.getPersonInfo().getCount()).filter("companyId",companyId).first().now();
//				}
//				
//				 if(cust!=null){
//					  addr=cust.getSecondaryAdress();
//				 }
//			
//				/** Loading Employee Information */
//				 //  vijay modified if condition added( serviceEntity.getEmployee()!=null)this to codition )
//				 //Date : 15/2/2017
//				 if(serviceEntity.getCompanyId()!=null && serviceEntity.getEmployee()!=null){
//					 empEntity=ofy().load().type(Employee.class).filter("fullname",serviceEntity.getEmployee().trim()).filter("companyId",companyId).first().now();
//				 }
//				
//				 /** Loading Bill Of Material */
//				 
//				 if(serviceEntity.getCompanyId()!=null){
//					 bomEntity=ofy().load().type(BillOfMaterial.class).filter("product_id", serviceEntity.getProduct().getCount()).filter("companyId",companyId).filter("status",true).first().now();
//				 }
//				
//				ServiceProject cusproject=new ServiceProject();
//				
//				cusproject.setserviceId(serviceId);
//				cusproject.setCompanyId(companyId);
//				cusproject.setserviceEngineer(serviceEntity.getEmployee());
//				cusproject.setServiceStatus(serviceEntity.getStatus());
//				cusproject.setserviceDate(serviceEntity.getServiceDate());
//				cusproject.setContractId(contractCount);
//				cusproject.setContractStartDate(serviceEntity.getContractStartDate());
//				cusproject.setContractEndDate(serviceEntity.getContractEndDate());
//				cusproject.setPersonInfo(serviceEntity.getPersonInfo());
//				cusproject.setPocName(serviceEntity.getEmployee());
//				/**
//				 *   rohan commented this line because of getting error As unexpected error
//				 *   date: 25/01/2017
//				 *   No need to set one field for 2 times 
//				 */
////				cusproject.setPocName(empEntity.getFullname());
//				// ends here 
//				
//				/** Date 13 Feb 2017
//				 * if condition added by vijay for if service engineer is blank in service
//				 * then dont set bellow details
//				 */
//				if(empEntity!=null){
//					cusproject.setPocLandline(empEntity.getLandline());
//					cusproject.setPocCell(empEntity.getCellNumber1());
//					cusproject.setPocEmail(empEntity.getEmail());
//				}
//				/**
//				 * end here
//				 */
//				cusproject.setProjectName("Project-"+serviceId);
//				cusproject.setProjectStatus("Created");
//				cusproject.setAddr(addr);
//				cusproject.setServiceSrNo(serviceEntity.getServiceSerialNo());
//				
//				if(bomEntity!=null){
//					List<CompanyAsset> companyAssetList=getToolsFromProductId(bomEntity);
//					if(companyAssetList!=null){
//						cusproject.setTooltable(companyAssetList);
//					}
//				}
//				
//				
//				if(empEntity!=null){
//					cusproject.setTechnicians(getTechiciansFromEmployeeInfo(empEntity));
//				}
//				if(technicians!=null){
//					cusproject.setTechnicians(technicians);
//				}
//				
//				List<ProductGroupList> billProdList=getMaterialAndToolFromProductId(companyId,bomEntity,serviceEntity);
//				
//				if(billProdList.size()!=0){
//					cusproject.setProdDetailsList(billProdList);
//				}
//				
//				return cusproject;
//			}

			
			
			private List<CompanyAsset> getToolsFromProductId(BillOfMaterial bomEntity) {
				
				if(bomEntity!=null){
					return bomEntity.getToolItems();
				}
				else{
					return null;
				}
			}

				private List<ProductGroupList> getMaterialAndToolFromProductId(long companyId,BillOfMaterial bomEntity,Service serEntity) {
					
					int prodId=0;
					List<ProductGroupList> prodDetailslist = new ArrayList<ProductGroupList>();
					List<ProductGroupList> prodDetailslis = null;
					
					if(bomEntity!=null)
					{
						if(bomEntity.getBillProdItems().get(0).getProdGroupId()!=0){
							prodId=bomEntity.getBillProdItems().get(0).getProdGroupId();
						}
					}
					
					if(prodId!=0)
					{
						 if(serEntity.getCompanyId()!=null){
							 prodDetailslis=ofy().load().type(ProductGroupList.class).filter("productGroupId", prodId).
										filter("companyId",companyId).filter("status",true).list();
						 }
						 else{
							 prodDetailslis=ofy().load().type(ProductGroupList.class).filter("productGroupId", prodId).filter("status",true).list();
						 }
					}
					 
					if(prodDetailslis!=null)
					{
						 if(prodDetailslis.size()!= 0)
						 {
							 for(int i=0;i<prodDetailslis.size();i++)
							 {
								 ProductGroupList prodDetailsListEntity=new ProductGroupList();
								 
								 prodDetailsListEntity.setProduct_id(prodDetailslis.get(i).getProduct_id());
								 prodDetailsListEntity.setCode(prodDetailslis.get(i).getCode());
								 prodDetailsListEntity.setName(prodDetailslis.get(i).getName());
								 prodDetailsListEntity.setQuantity(prodDetailslis.get(i).getQuantity());
								 prodDetailsListEntity.setUnit(prodDetailslis.get(i).getUnit());
								 
								 prodDetailslist.add(prodDetailsListEntity); 
							 }
						 }
					}
					
					return prodDetailslist;
				}


			/**
			 * This method is used for retrieving Technicians information for saving in ServiceProject Entity 
			 */

			private List<EmployeeInfo> getTechiciansFromEmployeeInfo(Employee empEntity) {
				
				ArrayList<EmployeeInfo> technicians=new ArrayList<EmployeeInfo>();
				
				EmployeeInfo empInfo=new EmployeeInfo();
				empInfo.setFullName(empEntity.getFullName());
				empInfo.setCellNumber(empEntity.getCellNumber1());
				empInfo.setEmpCount(empEntity.getCount());
				technicians.add(empInfo);
				
				return technicians;
			}
		   

			@Override
			public ArrayList<EmployeeInfo> projectTechnicianList(long companyId, int contractCount, int serviceCount,String serviceStatus) {
						List<EmployeeInfo> technician = new ArrayList<EmployeeInfo>();
						try {
							
							List<ServiceProject> projEntityList=ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", contractCount).filter("serviceId", serviceCount).list();
							logger.log(Level.SEVERE,"SizeOf Project"+projEntityList.size());
							
							if(projEntityList.size()>0){
								logger.log(Level.SEVERE,"Size Greateer than 0");
								
								ArrayList<EmployeeInfo> empinfo = new ArrayList<EmployeeInfo>();
								if(projEntityList.size()==1){
									
//									UpdateProjectTechEmployee(projEntityListCreated.get(0),technicians);
								}else{
									Comparator<ServiceProject> serProIdComparator = new Comparator<ServiceProject>() {
										public int compare(ServiceProject c1, ServiceProject c2) {
										
											if(c1.getCount()== c2.getCount()){
												return 0;}
											if(c1.getCount()> c2.getCount()){
												return 1;}
											else{
												return -1;}
										}
									};
									Collections.sort(projEntityList,serProIdComparator);
									
								}
								empinfo.addAll(projEntityList.get(0).getTechnicians());
								return empinfo ;
							}
						
							
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, " projectTechnicianList method -- " + e.getMessage());
						System.out.println(e.getMessage());
					}
						return null;
			}
				   
	
	
	
}
