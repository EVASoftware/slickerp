package com.slicktechnologies.server;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.device.SaveService;
import com.slicktechnologies.server.android.MarkCompletedSubmitServlet;
import com.slicktechnologies.server.android.ReportedServiceDetailsSaveServlet;
import com.slicktechnologies.server.android.contractwiseservice.MarkCompletedMultipleServices;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ServiceImplementor extends RemoteServiceServlet implements SaveService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1561049114069788566L;
	Logger logger=Logger.getLogger("Bill Creation for service wise billing");
	
	@Override
	public boolean saveStatus(Service service) {
		

		/**
		 * Date : 24-10-2017 BY ANIL
		 * this serProjCompletionFlag is used to complete the project if it is not completed against service
		 * this requirement is for NBHC
		 * ProcessType-MarkProjectCompleteOnServiceComplete
		 */

		/**
		 * nidhi
		 * 13-04-2018
		 * for restricting service completion if customer project not exist
		 * only for nbhc
		 */
		boolean serNbhc = false;
		boolean serProjCompletionFlag=false;
		
		/**
		 * Date 23-05-2019 by Vijay
		 * Des :- NBHC CCPM if complaint service completed then complaint also mark complete  
		 */
		boolean complainFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCompleteComplaintService", service.getCompanyId());
		if(complainFlag&&service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
			reactonCompleteComplaintService(service);
		}
		
		ArrayList<String> processName = new ArrayList<String>();
		processName.add("ServiceProject");
		processName.add("Service");
		
		ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", service.getCompanyId())
				.filter("processName", "ServiceProject").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(ProcessTypeDetails obj:processConfig.getProcessList()){
				if(obj.processName.equals("ServiceProject")
						&&obj.getProcessType().equalsIgnoreCase("MarkProjectCompleteOnServiceComplete")
						&&obj.isStatus().equals(true)){
					serProjCompletionFlag=true;
				}
			/**
			 *for nbhc 
			 *nidhi
			 * 13-04-2018
			 */	if(obj.processName.equals("Service")
			 
						&&obj.getProcessType().equalsIgnoreCase("OnlyForNBHC")
						&&obj.isStatus().equals(true)){
					serNbhc=true;
				}
			}
		}
		
		Logger logger=Logger.getLogger("Service Logger");
		System.out.println("Entered In Implementor");
		if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
		{
			logger.log(Level.SEVERE,"If Condition");
			System.out.println("If Condition First");
			int count=ofy().load().type(ServiceProject.class).filter("serviceId",service.getCount()).
			filter("companyId",service.getCompanyId()).filter("contractId", service.getContractCount()).count();
			
			
		/**
		 * nidhi
		 * for nbhc
		 * 13-04-2018
		 * 
		 */	if(count== 0 && serNbhc){
				return false;
			}else if(count == 0 && !serNbhc)
			{ 
				logger.log(Level.SEVERE,"If Count ==0");
//				service.setServiceCompletionDate(DateUtility.getDateWithTimeZone("IST",new Date()));
				
				
				/***
				 * @author Anil ,Date : 09-07-2019
				 */
				
				if(service.isCompletedByApp()){
					logger.log(Level.SEVERE,"If Count ==01");
					// Here we are not going save service again ,because its data is already updated
				}else{
					ofy().save().entity(service).now();
				}
				/***
				 * Date : 06-10-2017 BY ANIL
				 * if service wise flag is true then we will create bill on service completion
				 */
				
				createBillOnServiceCompletion(service);
				/**
				 * End
				 */
				
				/**
				 * Date 31-10-2017 BY ANIL
				 * This method is used to update the bills of rate contract on completion of service
				 */
				
				updateRateContractBilling(service, false);
				/**
				 * End
				 */
				
				
				/**
				 * Date 09-11-2019
				 * @author Vijay Chougule
				 * Des :- NBHC CCPM WMS Fumigation service Completetion creating Degassing Service
				 * Degassing Service normaly create from app when technician marked as Tcompleted. 
				 * when we create degassing service for perticular fumigation service then updating degassing service id in fumigation service
				 * In short  service.getDeggassingServiceId()!=0 then from here degassing service will create
				 */
				ArrayList<Service> wmsFumigationservicelist = new ArrayList<Service>();

				logger.log(Level.SEVERE, "This service degassing Service id"+service.getDeggassingServiceId());
				if(service.isWmsServiceFlag() && service.getProduct()!=null && service.getProduct().getProductCode().trim().equals("STK-01")
						&& service.getDeggassingServiceId()==0){
					wmsFumigationservicelist.add(service);
				}
				/**
				 * Date 09-11-2019
				 * @author Vijay Chougule
				 * Des :- NBHC CCPM WMS Fumigation service completetion creating Deggasing Service
				 * @param productCode 
				 */
				if(wmsFumigationservicelist.size()!=0){
					logger.log(Level.SEVERE, "Creating degassing service for fumigation service");
					ReportedServiceDetailsSaveServlet reportedService = new ReportedServiceDetailsSaveServlet();
					reportedService.createFumigationDeggassingService(wmsFumigationservicelist,"DSSG",null);
				}
				/**
				 * ends here
				 */
				
				/**
				 * Date 10-12-2019 by Vijay 
				 * Des :- Degassing Services Tcompleted/completed then updated this status in its fumigation Service
				 * this information needed to mark success or failure in CIO App
				 */
				if(service.isWmsServiceFlag() && service.getProduct()!=null && service.getProduct().getProductCode().trim().equals("DSSG")){
					if(service.getFumigationServiceId()!=0){
						Service fumigationService = ofy().load().type(Service.class).filter("companyId", service.getCompanyId())
								.filter("count", service.getFumigationServiceId()).first().now();
						if(fumigationService!=null){
							fumigationService.setDegassingServiceStatus(Service.SERVICESTATUSCOMPLETED);
							ofy().save().entities(fumigationService);
							logger.log(Level.SEVERE, "Deggasing Service status updated in its fumigation service");
						}
					}
					
				}
				logger.log(Level.SEVERE, "here we are now");
				
				/**
				 * @author Vijay Chougule Date 23-12-2019
				 * project :- Fumigation Tracker
				 * Des :- when wms services mark completed then updating its status in fumigationReportEntity
				 */
				if(service.isWmsServiceFlag()){
					logger.log(Level.SEVERE, "Updating fumigation report entity when service is marked completed");
					MarkCompletedMultipleServices updateFumigationReport = new MarkCompletedMultipleServices();
					updateFumigationReport.UpdateServiceCompletetionInFumigationServiceReport(service);
				}
				/**
				 * nidhi
				 * for service invoice mapping
				 * 25-10-2018
				 */
				HashMap<Integer, Integer> serContractList = new HashMap<Integer, Integer>();
				HashSet<Integer> contractIdSet = new HashSet<Integer>();
				HashMap<Integer, ArrayList<Integer>> contSerSet = new HashMap<Integer, ArrayList<Integer>>();
				HashMap<Integer, ArrayList<Service>> contSerDtSet = new HashMap<Integer, ArrayList<Service>>();
				boolean serviceInvMappingFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceInvoiceMappingOnCompletion",service.getCompanyId() );
				/**
				 * end
				 */
				
				/**
				 * nidhi
				 * for service invoice mapping
				 * 25-10-2018
				 */
				if(serviceInvMappingFlag) {
					if(contSerSet.containsKey(service.getContractCount())){
						contSerSet.get(service.getContractCount()).add(service.getCount());
						contSerDtSet.get(service.getContractCount()).add(service);


					}else{
						ArrayList<Integer> serList = new ArrayList<Integer>();
						serList.add(service.getCount());
						
						ArrayList<Service> serDtList = new ArrayList<Service>();
						serDtList.add(service);
						
						contSerSet.put(service.getContractCount(), serList);
						contSerDtSet.put(service.getContractCount(), serDtList);

					
					}
					contractIdSet.add(service.getContractCount());
					
						Gson gson = new Gson();
						String	jsonContSerDtSet = gson.toJson(contSerDtSet);
						
						gson = new Gson();
						String	jsonContSerSet = gson.toJson(contSerSet);
						
						gson = new Gson();
						String	jsonContractIdSet = gson.toJson(contractIdSet);
						
						Queue queue = QueueFactory.getQueue("InvoiceMapOnServiceCompletionTaskQueue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/invoiceMapOnServiceCompletionTaskQueue").param("jsonContSerDtSet",jsonContSerDtSet)
												.param("jsonContSerSet",jsonContSerSet).param("jsonContractIdSet",jsonContractIdSet)
												.param("companyId", service.getCompanyId()+""));
						
				}
				/**
				 * End
				 */
//				/** DATE 29.3.2019 added by komal to send sr copy to customer after completion of service **/
//				String taskName="SendSRCopyEmail"+"$"+service.getCompanyId()+"$"+service.getCount();
//				Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//				queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
				
				return true;
				
			}
			else
			{
				logger.log(Level.SEVERE,"Else Condition");
				System.out.println("Else Condition ");
				List<ServiceProject>lisProject=ofy().load().type(ServiceProject.class).filter("serviceId",service.getCount()).
				filter("companyId",service.getCompanyId()).filter("contractId", service.getContractCount()).list();
				for(ServiceProject proj:lisProject)
				{
					logger.log(Level.SEVERE,"Project Not Completd Condition");
					System.out.println("Project Not Completed");
					
					/**
					 * Date : 24-10-2017 BY ANIL
					 */
					if(serProjCompletionFlag){
						proj.setProjectStatus(ServiceProject.COMPLETED);
						proj.setServiceStatus(ServiceProject.COMPLETED);
					}else{
						if(!(proj.getProjectStatus().equals(ServiceProject.COMPLETED))){
							logger.log(Level.SEVERE,"Returning False");
							return false;
						}
					}
					
				}
				if(serProjCompletionFlag){
					if(lisProject!=null&&lisProject.size()!=0){
						ofy().save().entities(lisProject).now();
					}
				}
				ofy().save().entity(service).now();
				
				/***
				 * Date : 06-10-2017 BY ANIL
				 * if service wise flag is true then we will create bill on service completion
				 */
				createBillOnServiceCompletion(service);
				/**
				 * End
				 */
				
				/**
				 * Date 31-10-2017 BY ANIL
				 * This method is used to update the bills of rate contract on completion of service
				 */
				
				updateRateContractBilling(service, false);
				/**
				 * End
				 */
				
				/**
				 * Date 09-11-2019
				 * @author Vijay Chougule
				 * Des :- NBHC CCPM WMS Fumigation service Completetion creating Degassing Service
				 * Degassing Service normaly create from app when technician marked as Tcompleted. 
				 * when we create degassing service for perticular fumigation service then updating degassing service id in fumigation service
				 * In short  service.getDeggassingServiceId()!=0 then from here degassing service will create
				 */
				ArrayList<Service> wmsFumigationservicelist = new ArrayList<Service>();
				logger.log(Level.SEVERE, "This service degassing Service id"+service.getDeggassingServiceId());
				if(service.isWmsServiceFlag() && service.getProduct()!=null && service.getProduct().getProductCode().trim().equals("STK-01")
						&& service.getDeggassingServiceId()==0){
					wmsFumigationservicelist.add(service);
				}
				/**
				 * Date 09-11-2019
				 * @author Vijay Chougule
				 * Des :- NBHC CCPM WMS Fumigation service completetion creating Deggasing Service
				 * @param productCode 
				 */
				if(wmsFumigationservicelist.size()!=0){
					logger.log(Level.SEVERE, "Creating degassing service for fumigation service");
					ReportedServiceDetailsSaveServlet reportedService = new ReportedServiceDetailsSaveServlet();
					reportedService.createFumigationDeggassingService(wmsFumigationservicelist,"DSSG",null);
				}
				/**
				 * ends here
				 */
				
				/**
				 * Date 10-12-2019 by Vijay 
				 * Des :- Degassing Services Tcompleted/completed then updated this status in its fumigation Service
				 * this information needed to mark success or failure in CIO App
				 */
				if(service.isWmsServiceFlag() && service.getProduct()!=null && service.getProduct().getProductCode().trim().equals("DSSG")){
					if(service.getFumigationServiceId()!=0){
						Service fumigationService = ofy().load().type(Service.class).filter("companyId", service.getCompanyId())
								.filter("count", service.getFumigationServiceId()).first().now();
						if(fumigationService!=null){
							fumigationService.setDegassingServiceStatus(Service.SERVICESTATUSCOMPLETED);
							ofy().save().entities(fumigationService);
							logger.log(Level.SEVERE, "Deggasing Service status updated in its fumigation service");
						}
					}
					
				}
				logger.log(Level.SEVERE, "here we are now");
				
				/**
				 * @author Vijay Chougule Date 23-12-2019
				 * project :- Fumigation Tracker
				 * Des :- when wms services mark completed then updating its status in fumigationReportEntity
				 */
				if(service.isWmsServiceFlag()){
					logger.log(Level.SEVERE, "Updating fumigation report entity when service is marked completed");
					MarkCompletedMultipleServices updateFumigationReport = new MarkCompletedMultipleServices();
					updateFumigationReport.UpdateServiceCompletetionInFumigationServiceReport(service);
				}
				
				/**
				 * nidhi
				 * for service invoice mapping
				 * 25-10-2018
				 */
				HashMap<Integer, Integer> serContractList = new HashMap<Integer, Integer>();
				HashSet<Integer> contractIdSet = new HashSet<Integer>();
				HashMap<Integer, ArrayList<Integer>> contSerSet = new HashMap<Integer, ArrayList<Integer>>();
				HashMap<Integer, ArrayList<Service>> contSerDtSet = new HashMap<Integer, ArrayList<Service>>();
				boolean serviceInvMappingFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceInvoiceMappingOnCompletion",service.getCompanyId() );
				/**
				 * end
				 */
				
				/**
				 * nidhi
				 * for service invoice mapping
				 * 25-10-2018
				 */
				if(serviceInvMappingFlag) {
					if(contSerSet.containsKey(service.getContractCount())){
						contSerSet.get(service.getContractCount()).add(service.getCount());
						contSerDtSet.get(service.getContractCount()).add(service);


					}else{
						ArrayList<Integer> serList = new ArrayList<Integer>();
						serList.add(service.getCount());
						
						ArrayList<Service> serDtList = new ArrayList<Service>();
						serDtList.add(service);
						
						contSerSet.put(service.getContractCount(), serList);
						contSerDtSet.put(service.getContractCount(), serDtList);

					
					}
					contractIdSet.add(service.getContractCount());
					
						Gson gson = new Gson();
						String	jsonContSerDtSet = gson.toJson(contSerDtSet);
						
						gson = new Gson();
						String	jsonContSerSet = gson.toJson(contSerSet);
						
						gson = new Gson();
						String	jsonContractIdSet = gson.toJson(contractIdSet);
						
						Queue queue = QueueFactory.getQueue("InvoiceMapOnServiceCompletionTaskQueue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/invoiceMapOnServiceCompletionTaskQueue").param("jsonContSerDtSet",jsonContSerDtSet)
												.param("jsonContSerSet",jsonContSerSet).param("jsonContractIdSet",jsonContractIdSet)
												.param("companyId", service.getCompanyId()+""));
						
				}
				/**
				 * End
				 */
				
//				/** DATE 29.3.2019 added by komal to send sr copy to customer after completion of service **/
//				String taskName="SendSRCopyEmail"+"$"+service.getCompanyId()+"$"+service.getCount();
//				Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//				queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));

				
				return true;
			}
		}
		else{
			logger.log(Level.SEVERE,"Overall Else Condition");
			ofy().save().entity(service).now();
		}
		return true;
	}
	
	public void createBillOnServiceCompletion(Service service) {
		// TODO Auto-generated method stub
		Logger logger=Logger.getLogger("BILL CREATION LOGGER");
		logger.log(Level.SEVERE,"INSIDE CREATE BILL METHOD ON SERVICE COMPLETION");
		if(service.isServiceWiseBilling()){
			Contract contract=ofy().load().type(Contract.class).filter("companyId",service.getCompanyId()).filter("count", service.getContractCount()).first().now();
			if(contract!=null){
				createBillingDocs(contract,service);
			}
		}
		
	}
	
	
	protected void createBillingDocs(Contract contract,Service service){
		
		
		 /**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", contract.getCompanyId());
		 
		 /**
		  * end
		  */
		 
		Logger billingLogger=Logger.getLogger("Billing Logger");
		billingLogger.log(Level.SEVERE,"BIlling Process Started for Creation");
		
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
	    
		ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
		/** date 17.02.2018 added by komal for area wise billing **/
		//BillingDocument billingDocEntity=new BillingDocument();
		BillingDocument billingDocEntity ;
		billingDocEntity = ofy().load().type(BillingDocument.class).filter("companyId", contract.getCompanyId()).filter("rateContractServiceId", service.getCount()).first().now();
		if(billingDocEntity == null){
			billingDocEntity =new BillingDocument();
		}
		List<SalesOrderProductLineItem> salesProdLis=null;
		List<ContractCharges> billTaxLis=null;
		ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
		PaymentTerms paymentTerms=new PaymentTerms();
		
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		billingLogger.log(Level.SEVERE,"Contract Date As per IST"+DateUtility.getDateWithTimeZone("IST",contract.getContractDate()));
		billingLogger.log(Level.SEVERE,"Contract Date As Per Saved"+contract.getContractDate());
		
		if(contract.getCinfo()!=null)
			billingDocEntity.setPersonInfo(contract.getCinfo());
		if(contract.getCount()!=0)
			billingDocEntity.setContractCount(contract.getCount());
		if(contract.getStartDate()!=null)
			billingDocEntity.setContractStartDate(contract.getStartDate());
		if(contract.getEndDate()!=null)
			billingDocEntity.setContractEndDate(contract.getEndDate());
		if(contract.getNetpayable()!=0)
			billingDocEntity.setTotalSalesAmount(contract.getNetpayable());
		
		salesProdLis=this.retrieveSalesProducts(100,contract,service);/** ANIL **/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_PedioFindingAsServiceQuantityInBilling", contract.getCompanyId())) {
			salesProdLis = updateServiceQtyAndPrice(salesProdLis,service);
		}
		
		ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
		salesOrdArr.addAll(salesProdLis);
		if(salesOrdArr.size()!=0)
			billingDocEntity.setSalesOrderProducts(salesOrdArr);
		if(contract.getCompanyId()!=null)
			billingDocEntity.setCompanyId(contract.getCompanyId());
		
		billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", currentDate));
		billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", currentDate));
		billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", currentDate));
					
		if(contract.getPaymentMethod()!=null)
			billingDocEntity.setPaymentMethod(contract.getPaymentMethod());
		if(contract.getApproverName()!=null)
			billingDocEntity.setApproverName(contract.getApproverName());
//		if(contract.getEmployee()!=null)
//			billingDocEntity.setEmployee(contract.getEmployee());
		
		/**
		 * Date 11-05-2018
		 * Developer : Vijay
		 * Des :- When Service wise Billing is true then Set Billing document Branch from Servicing branch From Service and if servicing branch is not exist then set normal
		 * branch from contract 
		 * Requirement :- Orion
		 * old code added in else block standard code
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableSetBranchAsServicingBranch", contract.getCompanyId())){
			if(service.getServiceBranch()!=null && !service.getServiceBranch().equals("")){
				billingDocEntity.setBranch(service.getServiceBranch());	
			}else{
				if(contract.getBranch()!=null)
					billingDocEntity.setBranch(contract.getBranch());	
			}
		}else{
			if(contract.getBranch()!=null)
				billingDocEntity.setBranch(contract.getBranch());
		}
		/**
		 * ends here
		 */
		
		if(contract.getBranch()!=null)
			billingDocEntity.setBranch(contract.getBranch());
		if(contract.getSegment()!=null){
			billingDocEntity.setSegment(contract.getSegment());
		}
		if(contract.getStartDate()!=null){
			billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST", contract.getStartDate()));
		}
		if(contract.getEndDate()!=null){
			billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST", contract.getEndDate()));
		}
		
		int totalServices=0;
		double discountAmt=0;
		double roundOffAmt=0;
		for(SalesLineItem item:contract.getItems()){
			totalServices=(int) (totalServices+(item.getNumberOfServices()*item.getQty()));
		}
		if(contract.getDiscountAmt()!=0){
			discountAmt=contract.getDiscountAmt()/totalServices;
			discountAmt = Math.round(discountAmt * 100.0) / 100.0;
		}
		if(contract.getRoundOffAmt()!=0){
			roundOffAmt=contract.getRoundOffAmt()/totalServices;
			roundOffAmt = Math.round(roundOffAmt * 100.0) / 100.0;
		}
		
		billingDocEntity.setBillingTaxes(getTaxList(billingDocEntity.getSalesOrderProducts(),discountAmt,billingDocEntity.getTotalOtherCharges()));
		
		double grossValue=contract.getTotalAmount();
		billingDocEntity.setGrossValue(grossValue);
		
		double totBillAmt=contract.getTotalBillAmt(salesProdLis);
		ArrayList<ContractCharges> list=new ArrayList<ContractCharges>();
		list.addAll(billingDocEntity.getBillingTaxes());
		double taxAmt= contract.getTotalFromTaxTable(list);
		
		
		
		double totalBillingAmount = totBillAmt+taxAmt-discountAmt;
		billingDocEntity.setTotalBillingAmount(totalBillingAmount);
		
		billingDocEntity.setOrderCreationDate(contract.getCreationDate());
		
		int payTrmsDays=0;
		double payTrmsPercent=100;
		String payTrmsComment="";
		
		paymentTerms.setPayTermDays(payTrmsDays);
		paymentTerms.setPayTermPercent(payTrmsPercent);
		paymentTerms.setPayTermComment(payTrmsComment);
		billingPayTerms.add(paymentTerms);
		billingDocEntity.setArrPayTerms(billingPayTerms);
		billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
		/**
		 * Date 21-02-2018 by Vijay
		 * if condition for Orion pest control needed billing document must in approved status 
		 * and old code added in else block
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ServiceWiseBillingApprovedStatus", contract.getCompanyId())){
			billingDocEntity.setStatus(BillingDocument.APPROVED);
		}else{
			billingDocEntity.setStatus(BillingDocument.CREATED);

		}
		billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
		billingDocEntity.setOrderCformStatus("");
		billingDocEntity.setOrderCformPercent(-1);
		if(contract.getNumberRange()!=null)
			billingDocEntity.setNumberRange(contract.getNumberRange());
		
		billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", currentDate));
		billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST", currentDate));
		
		billingDocEntity.setTotalAmount(totBillAmt);
		
		billingDocEntity.setDiscountAmt(discountAmt);
		billingDocEntity.setFinalTotalAmt(totBillAmt-discountAmt);
		billingDocEntity.setTotalAmtIncludingTax(totalBillingAmount);
		billingDocEntity.setRoundOffAmt(roundOffAmt);
		billingDocEntity.setGrandTotalAmount(totalBillingAmount+roundOffAmt);
		
		
		
		billingDocEntity.setRateContractServiceId(service.getCount());
		
		/**
		 * Date :24-10-2017 BY ANIL
		 * Copying contract reference no to billing reference 
		 * as required by NBHC
		 */
		if(contract.getRefNo()!=null){
			billingDocEntity.setRefNumber(contract.getRefNo());
		}
		/**
		 * End
		 */
		/**
		 *  nidhi
		 *  Date : 4-12-2017
		 *  For copy configration details to bill
		 */
		if(confiFlag){
			billingDocEntity.setBillingCategory(contract.getCategory());
			billingDocEntity.setBillingType(contract.getType());
			billingDocEntity.setBillingGroup(contract.getGroup());
		}
		/**
		 *  end
		 */
		/**
		 * nidhi
		 * 2-05-2018
		 */
		billingDocEntity.setRenewContractFlag(contract.isRenewContractFlag());
		/**
		 * end
		 * nidhi
		 */
		
		/**
		 * @author Anil @since 14-05-2021
		 * for envocare if customer branch is map at service level then it will copy to billing in customer branch
		 * Raise by Vaishnavi
		 */
		if(service.getServiceBranch()!=null){
			billingDocEntity.setCustomerBranch(service.getServiceBranch());
		}
		
		/**
		 * @author Vijay 28-03-2022 for do not print service address flag updating to billing and invoice
		 */
		billingDocEntity.setDonotprintServiceAddress(contract.isDonotprintServiceAddress());
		
		/**Sheetal:30-03-2022 for payment mode to updating to billing and invoice**/
		if(contract.getPaymentModeName()!=null && !contract.getPaymentModeName().equals("")) {
		  billingDocEntity.setPaymentMode(contract.getPaymentModeName());
		}
		
		/**
		 * @author Vijay Date :- 25-01-2022
		 * Des :- to manage invoice pdf print on invoice
		 */
		if(contract.getInvoicePdfNameToPrint()!=null){
			billingDocEntity.setInvoicePdfNameToPrint(contract.getInvoicePdfNameToPrint());
		}
		/**
		 * ends here
		 */
		
		arrbilling.add(billingDocEntity);
		GenricServiceImpl impl=new GenricServiceImpl();
		if(arrbilling.size()!=0){
			ReturnFromServer reServer = impl.save(billingDocEntity);
			/**
			 * nidhi
			 *  11-10-2018
			 *   for map service and billing ||*
			 */
			if(reServer.count!=0){
				service.setBillingCount(reServer.count);
				ofy().save().entity(service).now();
			}
			/**
			 * end
			 */
		}
		
		/**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		if(confiFlag){
			boolean updateFlag = false;
			updateFlag = ServerAppUtility.checkContractCategoryAvailableOrNot(contract.getCategory(), contract.getCompanyId(), contract.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkContractTypeAvailableOrNot(contract.getType(), contract.getCategory(), contract.getCompanyId(), contract.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkServiceTypeAvailableOrNot(contract.getGroup(), contract.getCompanyId(), contract.getApproverName(), 46);
		}
		/**
		 * end
		 */
		
			
		billingLogger.log(Level.SEVERE,"Billing Process Completed");
	}
	
	
	

	private List<SalesOrderProductLineItem> retrieveSalesProducts(double payTermPercent,Contract contract,Service service)
	{
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0,discAmt=0;
		String prodDesc1="",prodDesc2="";
		
		for(int i=0;i<contract.getItems().size();i++)
		{
			if(contract.getItems().get(i).getPrduct().getCount()==service.getProduct().getCount()
					&&contract.getItems().get(i).getProductSrNo()==service.getServiceSrNo()){
				
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", contract.getItems().get(i).getPrduct().getCount()).first().now();
			if(superProdEntity!=null){
				if(superProdEntity.getComment()!=null){
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
			//SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProductSrNumber(contract.getItems().get(i).getProductSrNo());
			salesOrder.setProdId(contract.getItems().get(i).getPrduct().getCount());
			salesOrder.setProdCategory(contract.getItems().get(i).getProductCategory());
			salesOrder.setProdCode(contract.getItems().get(i).getProductCode());
			salesOrder.setProdName(contract.getItems().get(i).getProductName());
			salesOrder.setQuantity(contract.getItems().get(i).getQty());
			salesOrder.setOrderDuration(contract.getItems().get(i).getDuration());
			salesOrder.setOrderServices(contract.getItems().get(i).getNumberOfServices());
			
			totalTax=contract.removeTaxAmt(contract.getItems().get(i).getPrduct());
			prodPrice=contract.getItems().get(i).getPrice()-totalTax;
			
			int totalNoOfServices=(int) (contract.getItems().get(i).getQty()*contract.getItems().get(i).getNumberOfServices());
			prodPrice=prodPrice/totalNoOfServices;
			
			salesOrder.setPrice(prodPrice);
			
			
			salesOrder.setVatTaxEdit(contract.getItems().get(i).getVatTaxEdit());
			salesOrder.setServiceTaxEdit(contract.getItems().get(i).getServiceTaxEdit());
			salesOrder.setVatTax(contract.getItems().get(i).getVatTax());
			salesOrder.setServiceTax(contract.getItems().get(i).getServiceTax());
			
			salesOrder.setArea(contract.getItems().get(i).getArea());
			salesOrder.setHsnCode(contract.getItems().get(i).getPrduct().getHsnNumber());
			
			salesOrder.setProdPercDiscount(contract.getItems().get(i).getPercentageDiscount());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			if(contract.getItems().get(i).getUnitOfMeasurement()!=null){
				salesOrder.setUnitOfMeasurement(contract.getItems().get(i).getUnitOfMeasurement());
			}
			
			salesOrder.setDiscountAmt(contract.getItems().get(i).getDiscountAmt());
			
			/**
			 * @author Anil 
			 * @since 21-01-2022
			 * Setting area from mark complete popup
			 * raised by Nitin sir and Nithila
			 */
			if(contract.isServiceWiseBilling()){
				if(service.getQuantity()!=0){
					salesOrder.setArea(service.getQuantity()+"");
					contract.getItems().get(i).setArea(service.getQuantity()+"");
				}
			}
			
			/** date 17.02.2018 added by komal for area wise billing**/
			if(contract.isServiceWiseBilling()){
				double tax = 0;
				double origPrice = 0;
				if(!(contract.getItems().get(i).getArea().equalsIgnoreCase("NA"))){
				logger.log(Level.SEVERE , "inside area");
				CustomerBranchDetails branches = ofy().load().type(CustomerBranchDetails.class).filter("companyId", contract.getCompanyId())	.filter("status", true).filter("cinfo.count", contract.getCinfo().getCount()).filter("buisnessUnitName", service.getServiceBranch()).first().now();
				logger.log(Level.SEVERE , "branches :" + branches);
				if(branches != null){
				if(branches.getArea()!=0){
					logger.log(Level.SEVERE , "branches area:" + branches.getArea());
					salesOrder.setPrice(service.getPerUnitPrice());		
					salesOrder.setArea(service.getQuantity()+"");
					contract.getItems().get(i).setArea(service.getQuantity()+"");
					contract.getItems().get(i).setPrice(service.getPerUnitPrice());
					prodPrice = service.getPerUnitPrice();
					}
				}	
			}
		}
		/**
		 * end komal
		 */
			/** date 29.5.2018 added by komal for billing from complain **/
			if(contract.getItems().get(i).isComplainService()){
				if(service.getQuantity()!=0){
					logger.log(Level.SEVERE , "inside complain");
					salesOrder.setArea(service.getQuantity()+"");
					contract.getItems().get(i).setArea(service.getQuantity()+"");
					if(service.getUom()!=null && !(service.getUom().equals(""))){
						contract.getItems().get(i).setUnitOfMeasurement(service.getUom());
					}
				}
			}
			/**
			 * end komal
			 */
			
			if((contract.getItems().get(i).getPercentageDiscount()==null 
					&& contract.getItems().get(i).getPercentageDiscount()==0) 
					&& (contract.getItems().get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				
				if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
					percAmt=prodPrice*squareArea;
				}else{
					percAmt=prodPrice;
				}
				
			}else if((contract.getItems().get(i).getPercentageDiscount()!=null)
					&& (contract.getItems().get(i).getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				
				if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
					percAmt=prodPrice*squareArea;
					percAmt= percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
					percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
				}else{
					
					percAmt=prodPrice;
					percAmt=percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
					percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
				}
			}else {
				System.out.println("inside oneof the null condition");
				
				if(contract.getItems().get(i).getPercentageDiscount()!=null 
						&& contract.getItems().get(i).getPercentageDiscount()!=0){
					
					if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA")){
						Double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
						percAmt = prodPrice*squareArea;
						percAmt=percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
					}else{
						percAmt=prodPrice;
						percAmt=percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
					}
				}else {
					if(!contract.getItems().get(i).getArea().equals("NA")){
						Double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
						percAmt = prodPrice*squareArea;
						percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
					}else{
						percAmt=prodPrice;
						percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
					}
				}
			}
			
			
			salesOrder.setTotalAmount(percAmt);
			if(payTermPercent!=0){
				baseBillingAmount=(percAmt*payTermPercent)/100;
			}
//			salesOrder.setBaseBillingAmount(Math.round(baseBillingAmount));
			/**
			 * Date 25-01-2018 By Vijay
			 * above line commeted old code and below new one line code added for base billing decimal value issue
			 */
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			/**
			 * ends here
			 */
			salesOrder.setPaymentPercent(100.0);
			
//			salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
			/**
			 * Date 14/3/2018 By Manisha
			 * above line commeted old code and below new one line code added for BasePaymentAmount decimal value issue
			 */
			salesOrder.setBasePaymentAmount(baseBillingAmount);
			/**
			 * ends here
			 */
			salesOrder.setIndexVal(i+1);
			salesOrder.setProductSrNumber(contract.getItems().get(i).getProductSrNo());
			
			/**
			 * Date 06/06/2018 By vijay
			 * for product warranty need to show in invoice pdf
			 *  Requirement :- Neatedge Services
			 */
			if(contract.getItems().get(i).getWarrantyPeriod()!=0)
			salesOrder.setWarrantyPeriod(contract.getItems().get(i).getWarrantyPeriod());
			
			/**
			 * ends here
			 */
			
			salesOrder.setPrduct(superProdEntity);
			/**
			 * nidhi
			 * 20-08-2018
			 */
			salesOrder.setProSerialNo(contract.getItems().get(i).getProSerialNo());
			salesOrder.setProModelNo(contract.getItems().get(i).getProModelNo());
			/**
			 * ends
			 */
			salesProdArr.add(salesOrder);
			
			}
		}
		return salesProdArr;
	}
	
	
	/**
	 * Date : 07-10-2017 BY ANIL
	 * creating tax list
	 */
	public List<ContractCharges> getTaxList(List<SalesOrderProductLineItem> itemList,double discountAmt,double otherChargesAmt){
		
		List<ContractCharges> taxList=new ArrayList<ContractCharges>();
		
		for(SalesOrderProductLineItem item:itemList){
			/**
			 * If GST tax is not applicable then we will consider tax1 as vat tax field and tax2 as service tax field.
			 */
			if(item.getVatTax().getTaxPrintName()!=null&&!item.getVatTax().getTaxPrintName().equals("")){
				boolean updateFlag=true;
				if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("SELECT")){
//					return taxList;
					updateFlag=false;
				}
				if(item.getVatTax().getPercentage()==0){
//					return taxList;
					updateFlag=false;
				}
				if(updateFlag){
				System.out.println("INSIDE GST TAX1 PRINT NAME : "+item.getVatTax().getTaxPrintName());
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName(item.getVatTax().getTaxPrintName());
				pocentity.setTaxChargePercent(item.getVatTax().getPercentage());
				int indexValue=this.checkTaxPercent(item.getVatTax().getPercentage(),item.getVatTax().getTaxPrintName(),taxList);
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal((otherChargesAmt+item.getBasePaymentAmount()-discountAmt)+taxList.get(indexValue).getTaxChargeAssesVal());
					taxList.remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherChargesAmt+item.getBasePaymentAmount()-discountAmt);
				}
				pocentity.setPayableAmt(pocentity.getTaxChargeAssesVal()*pocentity.getTaxChargePercent()/100);
				taxList.add(pocentity);
				}
			}else{
				boolean updateFlag=true;
				if(item.getVatTax().getPercentage()==0){
//					return taxList;
					updateFlag=false;
				}
				if(updateFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName("VAT");
				pocentity.setTaxChargePercent(item.getVatTax().getPercentage());
				int indexValue=this.checkTaxPercent(item.getVatTax().getPercentage(),"VAT",taxList);
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal((otherChargesAmt+item.getBasePaymentAmount()-discountAmt)+taxList.get(indexValue).getTaxChargeAssesVal());
					taxList.remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherChargesAmt+item.getBasePaymentAmount()-discountAmt);
				}
				pocentity.setPayableAmt(pocentity.getTaxChargeAssesVal()*pocentity.getTaxChargePercent()/100);
				taxList.add(pocentity);
				}
			}
			
			if(item.getServiceTax().getTaxPrintName()!=null&&!item.getServiceTax().getTaxPrintName().equals("")){
				boolean updateFlag=true;
				System.out.println("INSIDE GST TAX2 PRINT NAME : "+item.getServiceTax().getTaxPrintName());
				if(item.getServiceTax().getTaxPrintName().equalsIgnoreCase("SELECT")){
//					return taxList;
					updateFlag=false;
				}
				if(item.getServiceTax().getPercentage()==0){
//					return taxList;
					updateFlag=false;
				}
				if(updateFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName(item.getServiceTax().getTaxPrintName());
				pocentity.setTaxChargePercent(item.getServiceTax().getPercentage());
				int indexValue=this.checkTaxPercent(item.getServiceTax().getPercentage(),item.getServiceTax().getTaxPrintName(),taxList);
				if(indexValue!=-1){
					pocentity.setTaxChargeAssesVal((otherChargesAmt+item.getBasePaymentAmount()-discountAmt)+taxList.get(indexValue).getTaxChargeAssesVal());
					taxList.remove(indexValue);
				}
				if(indexValue==-1){
					pocentity.setTaxChargeAssesVal(otherChargesAmt+item.getBasePaymentAmount()-discountAmt);
				}
				pocentity.setPayableAmt(pocentity.getTaxChargeAssesVal()*pocentity.getTaxChargePercent()/100);
				taxList.add(pocentity);
				}
			}else{
				boolean updateFlag=true;
				System.out.println("ST OR NON GST");
				if(item.getServiceTax().getPercentage()==0){
//					return taxList;
					updateFlag=false;
				}
				if(item.getVatTax().getTaxPrintName()!=null&&!item.getVatTax().getTaxPrintName().equals("")){
//					return taxList;
					updateFlag=false;
				}
				if(updateFlag){
				ContractCharges pocentity=new ContractCharges();
				pocentity.setTaxChargeName("Service Tax");
				pocentity.setTaxChargePercent(item.getServiceTax().getPercentage());
				int indexValue=this.checkTaxPercent(item.getServiceTax().getPercentage(),"Service",taxList);
				if(indexValue!=-1){
				 	double assessValue=(otherChargesAmt+item.getBasePaymentAmount()-discountAmt)+((otherChargesAmt+item.getBasePaymentAmount()-discountAmt)*item.getVatTax().getPercentage()/100);
					pocentity.setTaxChargeAssesVal(assessValue+taxList.get(indexValue).getTaxChargeAssesVal());
					taxList.remove(indexValue);
				}
				if(indexValue==-1){
					double assessValue=otherChargesAmt+item.getBasePaymentAmount()-discountAmt+((otherChargesAmt+item.getBasePaymentAmount()-discountAmt)*item.getVatTax().getPercentage()/100);
					pocentity.setTaxChargeAssesVal(assessValue);
				}
				pocentity.setPayableAmt(pocentity.getTaxChargeAssesVal()*pocentity.getTaxChargePercent()/100);
				taxList.add(pocentity);
				}
			}
		}
		
		return taxList;
	}
	
	
	public int checkTaxPercent(double taxValue,String taxName,List<ContractCharges> taxesList)
	{
//		List<ContractCharges> taxesList=this.billingTaxTable.getDataprovider().getList();
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getTaxChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getTaxChargePercent()&&taxval!=0&&taxesList.get(i).getTaxChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getTaxChargeName().trim().equals("IGST")){
					return i;
				}
			}
		}
		return -1;
	}
	
	
	
	
	/**
	 * Date : 30-10-2017 BY ANIL
	 * this method is used to update billing document of rate contract on completion of service
	 */
	
	public void updateRateContractBilling(Service service, boolean rateContractService) {
		if(service.getQuantity()!=0&&!service.getUom().trim().equals("")){
			List<BillingDocument> billList=ofy().load().type(BillingDocument.class).filter("companyId",service.getCompanyId()).filter("contractCount",service.getContractCount()).filter("rateContractServiceId",service.getCount()).list();
			if (billList.size() > 0) {
				for (BillingDocument billingDocument : billList) {
					if (service.isRateContractService()&&
							(billingDocument.getStatus().equals(BillingDocument.CREATED)
									||billingDocument.getStatus().equals(BillingDocument.REQUESTED)
									||billingDocument.getStatus().equals(BillingDocument.APPROVED))) {
						billingDocument.setQuantity(service.getQuantity());
						billingDocument.setUom(service.getUom());
						/**
						 * Date 28-09-2018 by Vijay
						 * Des :- NBHC CCPM Rate card Contract Billing must be create in Created status only because they change amount in bills
						 * as per vaishali madam and standerd old code added in else block for Approved status
						 */
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC", service.getCompanyId())){
							billingDocument.setStatus(BillingDocument.CREATED);
						}else{
							billingDocument.setStatus(BillingDocument.APPROVED);
						}
						updateProductTable(billingDocument,service.getQuantity());
						double sumOfProductPrice=0;
						for(SalesOrderProductLineItem item:billingDocument.getSalesOrderProducts()){
							sumOfProductPrice=sumOfProductPrice+item.getBasePaymentAmount();
						}
						double totalAfterDisc=sumOfProductPrice-billingDocument.getDiscountAmt();
						
						billingDocument.setTotalAmount(sumOfProductPrice);
						
						billingDocument.setFinalTotalAmt(totalAfterDisc);
						billingDocument.setBillingTaxes(getTaxList(billingDocument.getSalesOrderProducts(), billingDocument.getDiscountAmt(), billingDocument.getTotalOtherCharges()));
//						double 
						ArrayList<ContractCharges> list=new ArrayList<ContractCharges>();
						list.addAll(billingDocument.getBillingTaxes());
						Contract contract=new Contract();
						double taxAmt= contract.getTotalFromTaxTable(list);
						double totalBillingAmount = sumOfProductPrice+taxAmt-billingDocument.getDiscountAmt();
						
						billingDocument.setTotalAmtIncludingTax(totalBillingAmount);
						
						billingDocument.setGrandTotalAmount(totalBillingAmount+billingDocument.getRoundOffAmt());
						billingDocument.setTotalBillingAmount(totalBillingAmount+billingDocument.getRoundOffAmt());
						
						/**
						 *  nidhi
						 *  3-march-2018
						 *  for save service value from billing document
						 */
						service.setServiceValue(billingDocument.getGrandTotalAmount());
						ofy().save().entity(service);
						/**
						 * end
						 */
					} else {
						billingDocument.setQuantity(service.getQuantity());
						billingDocument.setUom(service.getUom());
					}
					
					
				}
				ofy().save().entities(billList);
			}
		}
	}

	private void updateProductTable(BillingDocument billingDocument, double quantity) {
		// TODO Auto-generated method stub
		for (SalesOrderProductLineItem salesOrderProductLineItem : billingDocument.getSalesOrderProducts()) {
			salesOrderProductLineItem.setArea(quantity+"");
			double totalAmount=getAssessTotalAmount(salesOrderProductLineItem);
			salesOrderProductLineItem.setTotalAmount(totalAmount);
			double baseBillingAmount=getPercentAmount(totalAmount,billingDocument.getArrPayTerms().get(0).getPayTermPercent());
			salesOrderProductLineItem.setBaseBillingAmount(baseBillingAmount);
			double basePaymentAmountwithFlatDisc=baseBillingAmount-salesOrderProductLineItem.getFlatDiscount();
			double basePaymentAmount=getPercentAmount(basePaymentAmountwithFlatDisc, salesOrderProductLineItem.getPaymentPercent());
			salesOrderProductLineItem.setBasePaymentAmount(basePaymentAmount);	
		}
	}
	public double getAssessTotalAmount(SalesOrderProductLineItem salesOrderProductLineItem) {
		// TODO Auto-generated method stub
		double totalAmount = 0;
		if (salesOrderProductLineItem.getArea().trim().equalsIgnoreCase("NA")
				|| salesOrderProductLineItem.getArea().equalsIgnoreCase("")) {
			if (salesOrderProductLineItem.getProdPercDiscount() == null
					|| salesOrderProductLineItem.getProdPercDiscount() == 0) {
				if (salesOrderProductLineItem.getDiscountAmt() == 0) {
					totalAmount = salesOrderProductLineItem.getPrice();
				} else {
					totalAmount = salesOrderProductLineItem.getPrice()
							- salesOrderProductLineItem.getDiscountAmt();
				}
			} else {
				double disPercentAmount = getPercentAmount(salesOrderProductLineItem, false);
				if (salesOrderProductLineItem.getDiscountAmt() == 0) {
					totalAmount = salesOrderProductLineItem.getPrice() - disPercentAmount;
				} else {
					totalAmount = salesOrderProductLineItem.getPrice() - disPercentAmount
							- salesOrderProductLineItem.getDiscountAmt();
				}
			}
		} else {

			if (salesOrderProductLineItem.getProdPercDiscount() == null
					&& salesOrderProductLineItem.getProdPercDiscount() == 0) {
				if (salesOrderProductLineItem.getDiscountAmt() == 0) {
					totalAmount = salesOrderProductLineItem.getPrice()
							* Double.parseDouble(salesOrderProductLineItem.getArea().trim());
				} else {
					totalAmount = (salesOrderProductLineItem.getPrice() * Double
							.parseDouble(salesOrderProductLineItem.getArea().trim()))
							- salesOrderProductLineItem.getDiscountAmt();
				}
			} else {
				double disPercentAmount = getPercentAmount(salesOrderProductLineItem, true);
				if (salesOrderProductLineItem.getDiscountAmt() == 0) {
					totalAmount = (salesOrderProductLineItem.getPrice() * Double
							.parseDouble(salesOrderProductLineItem.getArea().trim()))
							- disPercentAmount;
				} else {
					totalAmount = (salesOrderProductLineItem.getPrice() * Double
							.parseDouble(salesOrderProductLineItem.getArea().trim()))
							- disPercentAmount - salesOrderProductLineItem.getDiscountAmt();
				}
			}

		}
		return totalAmount;
	}

	
	private double getPercentAmount(SalesOrderProductLineItem salesOrderProductLineItem, boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		if (isAreaPresent) {
			percentAmount = ((salesOrderProductLineItem.getPrice()
					* Double.parseDouble(salesOrderProductLineItem.getArea().trim()) * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		} else {
			percentAmount = ((salesOrderProductLineItem.getPrice() * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		}
		return percentAmount;
	}
	
	private double getPercentAmount(double amount, double percentage) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		
			percentAmount = ( amount/ 100)*percentage;
		
		return percentAmount;
	}
	
	
	@Override
	public ArrayList<Service> mapBillingDetails(ArrayList<Service> servicelist) {
	
		HashSet<Integer> serviceId = new HashSet<Integer>();
		
		
		HashSet<Integer> billinSet = new HashSet<Integer>();
		Long companyId = null;
		if(servicelist.size()>0){
			companyId = servicelist.get(0).getCompanyId();
		}
		
		
		for(Service ser : servicelist){
			if(ser.getBillingCount()!=0){
				billinSet.add(ser.getBillingCount());
			}else if(ser.isRateContractService() || ser.isServiceWiseBilling()){
				serviceId.add(ser.getCount());
			}
		}
		
		List<BillingDocument> mainBillList = new ArrayList<BillingDocument>();
		
		if(billinSet.size()>0 && companyId!=null){
			ArrayList<Integer> billArr = new ArrayList<Integer>();
			billArr.addAll(billinSet);
			List<BillingDocument> billingList  =  ofy().load().type(BillingDocument.class).filter("companyId", companyId)
					.filter("count IN", billArr).list();
			if(billingList!=null && billingList.size()>0){
				mainBillList.addAll(billingList);
			}
		}
		
		if(serviceId.size()>0 && companyId!=null){
			ArrayList<Integer> serArr = new ArrayList<Integer>();
			serArr.addAll(serviceId);
			List<BillingDocument> billingList  =  ofy().load().type(BillingDocument.class).filter("companyId", companyId)
					.filter("rateContractServiceId IN", serArr).list();
			if(billingList!=null && billingList.size()>0){
				mainBillList.addAll(billingList);
			}
		}
		
		if(mainBillList.size()>0){
			
			for(BillingDocument bill : mainBillList){
				for(Service ser : servicelist){
					if(ser.getContractCount() == bill.getContractCount() 
							&& ser.getPersonInfo().getCount() == bill.getPersonInfo().getCount()
							&& ser.getBillingCount()==0 && (ser.isServiceWiseBilling() || ser.isRateContractService())
							&& ser.getCount() == bill.getRateContractServiceId()){
						ser.setBillingCount(bill.getCount());
						ser.setBillingStatus(bill.getStatus());
						ser.setInvoiceId(bill.getInvoiceCount());
						break;
					}else if(ser.getBillingCount()!=0 && ser.getBillingCount() == bill.getCount()
							&& ser.getPersonInfo().getCount() == bill.getPersonInfo().getCount()){
						ser.setBillingCount(bill.getCount());
						ser.setBillingStatus(bill.getStatus());
						ser.setInvoiceId(bill.getInvoiceCount());
						break;
					}
				}
			}
			
		}
		
		ofy().save().entities(servicelist);
		return servicelist;
	}
	
	/**
	 * Date 23-05-2019 by Vijay
	 * Des :- NBHC CCPM if complaint service completed then complaint also mark complete  
	 */
	public void reactonCompleteComplaintService(Service service) {

		Complain complaintEntity = ofy().load().type(Complain.class).filter("companyId", service.getCompanyId())
										.filter("count", service.getTicketNumber()).first().now();
		logger.log(Level.SEVERE,"complaintEntity ="+complaintEntity);
		if(complaintEntity!=null){
			complaintEntity.setCompStatus(Service.SERVICESTATUSCOMPLETED);
			
			/**
			 * @author Anil
			 * @since 04-05-2020
			 * completion date and time
			 */
			complaintEntity.setCompletionDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			complaintEntity.setCompletionTime(DateUtility.getTimeWithTimeZone1("IST", new Date()));
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime", service.getCompanyId())) {
				/**
				 * @author Anil
				 * @since 25-05-2020
				 * Restricting complaint completion if service is not completed
				 */
				
				if(!service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
					return;
				}
				
				Branch branch=ofy().load().type(Branch.class).filter("companyId", service.getCompanyId()).filter("buisnessUnitName", complaintEntity.getBranch()).first().now();
				Shift shift=null;
				Calendar calendar=null;
				if(branch!=null){
					if(branch.getShift()!=null){
						shift=branch.getShift();
					}
					if(branch.getCalendar()!=null){
						calendar=branch.getCalendar();
					}
				}
				
				/**
				 * @author Anil
				 * @since 27-05-2020
				 * If complain registration date and time is greater/less than shift time then we will
				 * recalculate complain registration date and time as per shift
				 */
				logger.log(Level.SEVERE,"BF Complain Registration Date Time : "+complaintEntity.getDate());
				Date compRegDate=new Date(DateUtility.getDateWithTimeZone("IST",complaintEntity.getDate()).getTime());
				logger.log(Level.SEVERE,"IST Complain Registration Date Time : "+compRegDate);
				String fromTimeArr[] =complaintEntity.getTime().split(":");
				compRegDate.setHours(Integer.parseInt(fromTimeArr[0]));
				compRegDate.setMinutes(Integer.parseInt(fromTimeArr[1]));
				logger.log(Level.SEVERE,"Complain Registration Date Time : "+compRegDate);
				Date shiftStartTime=getDateWithShiftStartDate(compRegDate, shift);
				Date shiftEndTime=getDateWithShiftEndDate(compRegDate, shift);
				logger.log(Level.SEVERE,"Shift Start Date Time : "+shiftStartTime);
				logger.log(Level.SEVERE,"Shift End Date Time : "+shiftEndTime);
				if(compRegDate.before(shiftStartTime)){
					logger.log(Level.SEVERE,"Complain Registration Date Time is less");
					checkForWoAndHoliday(compRegDate, calendar,shift);
					compRegDate=getDateWithShiftStartDate(compRegDate, shift);
				}else if(compRegDate.after(shiftEndTime)){
					logger.log(Level.SEVERE,"Complain Registration Date Time is greater");
					int noOfDays=DateUtility.getNumberOfDaysBetweenDates(compRegDate, shiftEndTime);
					if(noOfDays==0){
						compRegDate=DateUtility.addDaysToDate(compRegDate, 1);
					}
					checkForWoAndHoliday(compRegDate, calendar,shift);
					compRegDate=getDateWithShiftStartDate(compRegDate, shift);
				}else{
					checkForWoAndHoliday(compRegDate, calendar,shift);
				}
				logger.log(Level.SEVERE,"Updated complain reg date : "+compRegDate);
				String compRegTime=compRegDate.getHours()+":"+compRegDate.getMinutes();
				
				complaintEntity.setTat_Tech(convertNumbersToTime(calculateTurnAroundTime(complaintEntity.getCompletionDate(), complaintEntity.getCompletionTime(), complaintEntity.getNewServiceDate(), complaintEntity.getNewServiceTime(),shift,calendar)));
				complaintEntity.setTat_Actual(convertNumbersToTime(calculateTurnAroundTime(complaintEntity.getCompletionDate(), complaintEntity.getCompletionTime(), compRegDate, compRegTime,shift,calendar)));
				complaintEntity.setTat_Sup(convertNumbersToTime(calculateTurnAroundTime(complaintEntity.getAssignedDate(), complaintEntity.getAssignedTime(), compRegDate, compRegTime,shift,calendar)));
			}
			
			ofy().save().entity(complaintEntity).now();
			SmsServiceImpl smsService =new SmsServiceImpl();

			//Ashwini Patil Date:17-08-2023
			smsService.checkEmailConfigAndSendComplaintResolvedEmail(complaintEntity.getCompanyId(), complaintEntity.getNewServiceDate(), complaintEntity.getPersoninfo().getFullName(), complaintEntity.getCount(), "", complaintEntity.getPersoninfo().getCellNumber(),complaintEntity.getPersoninfo().getCount());
			
			
			/**
			 * @author Anil @since 02-09-2021
			 * Below code is used to send the complaint sms on completion stage
			 */
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			Company company = ofy().load().type(Company.class).filter("companyId", complaintEntity.getCompanyId()).first().now();
			if(company!=null){
				if(complaintEntity.getCompStatus().equals(Service.SERVICESTATUSCOMPLETED)){
					SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
					smsserviceimpl.checkSMSConfigAndSendComplaintResolvedSMS(complaintEntity.getCompanyId(), complaintEntity.getServiceDate(), complaintEntity.getPersoninfo().getFullName(), complaintEntity.getCount(), company.getBusinessUnitName(), complaintEntity.getPersoninfo().getCellNumber(),complaintEntity.getPersoninfo().getCount());
					
				}
		   }
		}
		logger.log(Level.SEVERE,"Complaint marked completed successfully");
	}
	
	public double calculateTurnAroundTime(Date date1,String fromTime,Date date2,String toTime,Shift shift, Calendar calendar) {
		 SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date fromDate=new Date(date1.getTime());
		Date toDate=new Date(date2.getTime());
		double tat=0;
		
		String fromTimeArr[] =fromTime.split(":");
		String f_hh = fromTimeArr[0];
		String f_mm = fromTimeArr[1];
		
		fromDate.setHours(Integer.parseInt(f_hh));
		fromDate.setMinutes(Integer.parseInt(f_mm));
		logger.log(Level.SEVERE,"from Date1 "+fromDate);
		
		String toTimeArr[] =toTime.split(":");
		String t_hh = toTimeArr[0];
		String t_mm = toTimeArr[1];
		
		toDate.setHours(Integer.parseInt(t_hh));
		toDate.setMinutes(Integer.parseInt(t_mm));
		
		logger.log(Level.SEVERE,"to Date1 "+toDate);
		
		
		int noOfDays=DateUtility.getNumberOfDaysBetweenDates(fromDate, toDate);
		logger.log(Level.SEVERE,"NO OF DAYS "+noOfDays);
		if(noOfDays>0&&shift!=null){
			Date startTimeOfShift=getDateWithShiftStartDate(new Date(fromDate.getTime()),shift);
			Date endTimeOfShift=getDateWithShiftEndDate(new Date(toDate.getTime()),shift);
			
			logger.log(Level.SEVERE,"startTimeOfShift "+startTimeOfShift);
			logger.log(Level.SEVERE,"endTimeOfShift "+endTimeOfShift);
			
			double tat1=getHoursBetweenTwoDates(fromDate, startTimeOfShift);
			logger.log(Level.SEVERE,"tat1 "+tat1);
			double tat2=getHoursBetweenTwoDates(endTimeOfShift, toDate);
			logger.log(Level.SEVERE,"tat2 "+tat2);
			tat=tat1+tat2;
			
			logger.log(Level.SEVERE,"BEFORE "+tat);
			
			/**
			 * @author Anil
			 * @since 30-05-2020
			 */
			if(noOfDays>1) {
				
				Date frmDt=new Date(date2.getTime());
				Date toDt=new Date(date1.getTime());
				try {
					frmDt=isoFormat.parse(isoFormat.format(frmDt));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				try {
					toDt=isoFormat.parse(isoFormat.format(toDt));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				logger.log(Level.SEVERE,"frm dt"+frmDt);
				logger.log(Level.SEVERE,"to dt"+toDt);
				frmDt=DateUtility.addDaysToDate(frmDt, 1);
				while(frmDt.before(toDt)) {
					logger.log(Level.SEVERE,"frm dt1 "+frmDt);
					tat=tat+isWoOrHoliday(frmDt, calendar);
					frmDt=DateUtility.addDaysToDate(frmDt, 1);
				}
				logger.log(Level.SEVERE,"INSIDE MORE THAN ONE DAYS "+tat);
			}
		}else{
			tat=getHoursBetweenTwoDates(fromDate, toDate);
			
			/***
			 * @author Anil
			 * @since 09-12-2020
			 * if service is completing on weekly off and holiday then tat should be zero
			 * raised by Rahul Tiwari for PTSPL
			 */
			
			if(isWoOrHoliday(fromDate, calendar)==0){
				tat=0;
			}
		}
		
		tat=Math.round(tat* 100.0) / 100.0;
		logger.log(Level.SEVERE,""+tat);
		return tat;
	}
	
	public Date checkForWoAndHoliday(Date date,Calendar calendar,Shift shift){
		DataMigrationTaskQueueImpl dataImpl=new DataMigrationTaskQueueImpl();
		boolean updateFlag=false;
		Date date1=new Date(date.getTime());
		if(calendar!=null&&dataImpl.isWeeklyOff(calendar.getWeeklyoff(), date1)){
//		if(calendar!=null&&calendar.isWeekleyOff(date)){
			logger.log(Level.SEVERE,"WO");
			date=DateUtility.addDaysToDate(date, 1);
		}
		if(calendar!=null&&calendar.isHolidayDate1(date)){
			date=DateUtility.addDaysToDate(date, 1);
			logger.log(Level.SEVERE,"HOLIDAYY");
		}
		if(updateFlag){
			date=getDateWithShiftStartDate(date, shift);
			checkForWoAndHoliday(date, calendar,shift);
		}
		return date;
	}
	
	public double isWoOrHoliday(Date date,Calendar calendar){
		DataMigrationTaskQueueImpl dataImpl=new DataMigrationTaskQueueImpl();
		Date date1=new Date(date.getTime());
		if(calendar!=null&&dataImpl.isWeeklyOff(calendar.getWeeklyoff(), date1)){
//		if(calendar!=null&&calendar.isWeekleyOff(date)){
			logger.log(Level.SEVERE,"WO");
			return 0;
		}
		if(calendar!=null&&calendar.isHolidayDate1(date)){
			logger.log(Level.SEVERE,"HOLIDAYY");
			return 0;
		}
		return calendar.getWorkingHours();
	}
	
	public String convertNumbersToTime(double hours){
//		logger.log(Level.SEVERE,"TAT(SLA) "+hours);
		String hrsInString=null;
		try{
			String time=hours+"";
			String timeArr[] =time.split("\\.");
			String hh = timeArr[0];
			String mm = timeArr[1];
			if(mm.length()<2&&Integer.parseInt(mm)<9){
				mm=mm+0;
			}
			int mins=(Integer.parseInt(mm)*60)/100;
			hrsInString=hh+":"+mins;
			logger.log(Level.SEVERE,"TAT HH:MM "+hrsInString);
		}catch(Exception e){
			
		}
		return hrsInString;
	}
	
	public Date getDateWithShiftEndDate(Date date,Shift shift) {
		Date date1=new Date(date.getTime());
//		logger.log(Level.SEVERE,"date: "+date1);
		String shiftEndTime=getShiftToTime(shift);
		String endTimeArr[] =shiftEndTime.split(":");
		String e_hh = endTimeArr[0];
		String e_mm = endTimeArr[1];
//		logger.log(Level.SEVERE,"HH:MM "+e_hh+":"+e_mm);
		
		date1.setHours(Integer.parseInt(e_hh));
		date1.setMinutes(Integer.parseInt(e_mm));
//		logger.log(Level.SEVERE,"shift end date: "+date1);
		return date1;
	}
	
	public Date getDateWithShiftStartDate(Date date,Shift shift) {
		Date date1=new Date(date.getTime());
//		logger.log(Level.SEVERE,"date: "+date1);
		String shiftEndTime=getShiftFromTime(shift);
		String endTimeArr[] =shiftEndTime.split(":");
		String e_hh = endTimeArr[0];
		String e_mm = endTimeArr[1];
//		logger.log(Level.SEVERE,"HH:MM "+e_hh+":"+e_mm);
		
		date1.setHours(Integer.parseInt(e_hh));
		date1.setMinutes(Integer.parseInt(e_mm));
//		logger.log(Level.SEVERE,"shift start date: "+date1);
		return date1;
	}
	
	public String getShiftFromTime(Shift shift) {
		String time="";
		if(shift!=null) {
			String timeInString=shift.getFromTime()+"";
			String timeArr[] =timeInString.split("\\.");
			if(timeArr.length==2) {
				String hh = timeArr[0];
				String mm = timeArr[1];

				if(mm.length()<2&&Integer.parseInt(mm)<9){
					mm=mm+0;
				}
				int mins=(Integer.parseInt(mm)*60)/100;
				time=hh+":"+mins;
			}else {
				int fromTime=shift.getFromTime().intValue();
				time=fromTime+":"+"00";
			}
			logger.log(Level.SEVERE,"HH:MM "+time);
		}
		return time;
	}
	
	public String getShiftToTime(Shift shift) {
		String time="";
		if(shift!=null) {
			String timeInString=shift.getToTime()+"";
			String timeArr[] =timeInString.split("\\.");
			if(timeArr.length==2) {
				String hh = timeArr[0];
				String mm = timeArr[1];

				if(mm.length()<2&&Integer.parseInt(mm)<9){
					mm=mm+0;
				}
				int mins=(Integer.parseInt(mm)*60)/100;
				time=hh+":"+mins;
			}else {
				int fromTime=shift.getToTime().intValue();
				time=fromTime+":"+"00";
			}
			logger.log(Level.SEVERE,"HH:MM "+time);
		}
		return time;
	}
	
	public double getHoursBetweenTwoDates(Date date1,Date date2){
		double diff=0;
		Date fromDate=new Date(date1.getTime());
		Date toDate=new Date(date2.getTime());
		/**
		 * @author Anil
		 * @since 27-05-2020
		 **/
		long secs =0;
		if(fromDate.after(toDate)){
			secs = (fromDate.getTime() - toDate.getTime()) / 1000;
		}else if(fromDate.before(toDate)){
			secs = (toDate.getTime() - fromDate.getTime()) / 1000;
		}else{
			secs = (fromDate.getTime() - toDate.getTime()) / 1000;
		}
//		long secs = (fromDate.getTime() - toDate.getTime()) / 1000;
		
		int hours = (int) (secs / 3600);  
//		logger.log(Level.SEVERE,"hours: "+hours);
		secs = secs % 3600;
		int mins = (int) (secs / 60);
//		logger.log(Level.SEVERE,"mins: "+mins);
		secs = secs % 60;
		double minutesInNum=mins/60.0;
//		logger.log(Level.SEVERE,"Minutes in Numberrrr : "+minutesInNum);
		diff=hours+minutesInNum;
		return diff;
	}
	
	/**
	 * @author Vijay Date :- 11-11-2021
	 * Des :- updating billing document as per service qty * service value for UDS with PC
	 */
	private List<SalesOrderProductLineItem> updateServiceQtyAndPrice(List<SalesOrderProductLineItem> salesProdLis, Service service) {
		logger.log(Level.SEVERE, "Inside Qty and service value updation in billing document for UDS");
		for(SalesOrderProductLineItem prodlineItem : salesProdLis) {
			prodlineItem.setPrice(service.getServiceValue());
			prodlineItem.setArea(service.getQuantity()+"");
			System.out.println("prodlineItem.getTotalAmount() "+prodlineItem.getTotalAmount());
			if(service.getQuantity()>0) {
				prodlineItem.setTotalAmount(prodlineItem.getPrice()*service.getQuantity());
			}
			prodlineItem.setBaseBillingAmount(prodlineItem.getTotalAmount());
			prodlineItem.setBasePaymentAmount(prodlineItem.getTotalAmount());
		}
		return salesProdLis;
	}
}
