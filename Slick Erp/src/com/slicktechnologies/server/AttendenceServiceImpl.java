package com.slicktechnologies.server;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.log.SysoCounter;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;

import static com.googlecode.objectify.ObjectifyService.ofy;

@SuppressWarnings("serial")
public class AttendenceServiceImpl extends HttpServlet
{
	 List<EmployeeAttendance> listOfEmployeeAttendance;
	 List<String> inavlidEmployees;
	 
	 BufferedReader bufferedReader;
	 String line;
	 String[] array;
	 EmployeeAttendance employeeAttendance;
	 Long companyId;
	 static{
	
	 }
	 
	 public void doPost(HttpServletRequest req, HttpServletResponse res)throws ServletException, IOException
	 {
		 listOfEmployeeAttendance=new ArrayList<EmployeeAttendance>();
		
		 //readCsvFileFromClient(req);
		  setCompanyId(req);
		
		 System.out.println("comp"+companyId+"size"+listOfEmployeeAttendance.size());
		 if(listOfEmployeeAttendance.size()!=0)
		 {
			 for(EmployeeAttendance att:listOfEmployeeAttendance)
			 {
				 att.setCompanyId(companyId);
			    System.out.println("Company ID "+att.getCompanyId());
			 
			 }
		 }
		 if(listOfEmployeeAttendance.size()!=0)
        	 ofy().save().entities(listOfEmployeeAttendance);
		 
		// 
		 
		 
		
		
		
			 
	 } 

	 protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException 
	 {
		 
	 }
	 
	 public void create(String data)
	 {
		 employeeAttendance=new EmployeeAttendance();
		 array=data.split(",");
			// System.out.println("$$$$$$$$$$$$$4"+array[0].toString().trim());
			 try{
			 int employeeId=Integer.parseInt(array[0].trim());
			 employeeAttendance.setEmpId(employeeId);
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
				 this.inavlidEmployees.add(line+","+"\n");
				 
			 }
			 
			 
			 String punchCardNo=array[1];
			 employeeAttendance.setPunchCardNo(punchCardNo.trim());
			 
			 String employeeName=array[2].trim();
			 employeeAttendance.setEmpName(employeeName.trim());
			 
			 try{
				 SimpleDateFormat d=new SimpleDateFormat("mm/DD/yyyy");
			 Date date=d.parse(array[3].trim());
			 employeeAttendance.setDate(date);
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
				 this.inavlidEmployees.add(line+","+"\n");
				 
			 }
			 
//			 try
//			 {
//			 double inTime=Double.parseDouble(array[4].trim());
//			 employeeAttendance.setInTime(inTime);
//			 }
//			 catch(Exception e)
//			 {
//				 e.printStackTrace();
//				 this.inavlidEmployees.add(line+","+"\n");
//				
//			 }
			 try
			 {
				 /**
				  *  nidhi 
				  *  8-08-2017
				  *  for date formate
				  */
				 SimpleDateFormat d=new SimpleDateFormat("mm/DD/yyyy hh:mm:ss");
				
				 if(array[3].trim()!= null){
					 Date date=d.parse(array[4].trim());
					 employeeAttendance.setInTime(date);
				 }
			 
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
				 this.inavlidEmployees.add(line+","+"\n");
				
			 }
			 try{
//			 double outTime=Double.parseDouble(array[5].trim());
//			 employeeAttendance.setOutTime(outTime);
				 
				 try
				 {
					 /**
					  *  nidhi 
					  *  8-08-2017
					  *  for date formate
					  */
					 SimpleDateFormat d=new SimpleDateFormat("mm/DD/yyyy hh:mm:ss");
					
					 if(array[3].trim()!= null){
						 Date date=d.parse(array[5].trim());
						 employeeAttendance.setOutTime(date);
					 }
				 
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
					 this.inavlidEmployees.add(line+","+"\n");
					
				 }
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
				 this.inavlidEmployees.add(line+","+"\n");
				 
			 }
			 String status=array[6].trim();
			 employeeAttendance.setStatus(status);
			 
		    // if(employeeAttendance.isValidEmployee()&&employeeAttendance.isValidAttendance())
		    	 this.listOfEmployeeAttendance.add(employeeAttendance);
		    	 System.out.println("Created--");
		    	 
	 }
	 
 
	 
	 public  void readCsvFileFromClient(HttpServletRequest request,InputStream stream) throws IOException
	 {
		 String contentType = request.getContentType();

         //here we are checking the content type is not equal to Null and as well as the passed data from mulitpart/form-data is greater than or equal to 0
         if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
             DataInputStream in = new DataInputStream(request.getInputStream());
             //we are taking the length of Content type data
             int formDataLength = request.getContentLength();
             byte dataBytes[] = new byte[formDataLength];
             int byteRead = 0;
             int totalBytesRead = 0;
             //this loop converting the uploaded file into byte code
             while (totalBytesRead < formDataLength) {
                 byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                 totalBytesRead += byteRead;
                 }

             String file = new String(dataBytes);

             //for saving the file name
             String saveFile = file.substring(file.indexOf("filename=\"") + 10);
             System.out.println("Save File "+saveFile);
             saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
             saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,saveFile.indexOf("\""));

             int lastIndex = contentType.lastIndexOf("=");
             String boundary = contentType.substring(lastIndex + 1,contentType.length());
             int pos;
             //extracting the index of file 
             pos = file.indexOf("filename=\"");
             pos = file.indexOf("\n", pos) + 1;
             pos = file.indexOf("\n", pos) + 1;
             pos = file.indexOf("\n", pos) + 1;
             int boundaryLocation = file.indexOf(boundary, pos) - 4;
             int startPos = ((file.substring(0, pos)).getBytes()).length;
             int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
             InputStream input = new ByteArrayInputStream(dataBytes,startPos,endPos-startPos);
             InputStreamReader ireader=new InputStreamReader(input);
             BufferedReader reader=new BufferedReader(ireader);
            String line="";
            System.out.println("Size is "+reader.readLine());
            if(reader!=null)
            	reader.readLine();
             while((line=reader.readLine())!=null)
            	 create(line);
				 
		 }
         
   }
	 
	 private void setCompanyId(HttpServletRequest req) throws IOException
	 {
		 ServletFileUpload upload = new ServletFileUpload();
		// readCsvFileFromClient(req);

         try {
        	 FileItemIterator iter = upload.getItemIterator(req);
			
			while (iter.hasNext()) {
				
				FileItemStream  item = iter.next();
				String name = item.getFieldName();
				InputStream stream = item.openStream();
				System.out.println("Name is"+name);
			    if (item.isFormField()) {
			    	
			    	
			    	
			    	
			    	if(name.equals("companyId"))
			    	{
			    	 Long cId=	Long.parseLong(Streams.asString(stream).toString().trim());
			    	 this.companyId=cId;
			    	 
			    	}
			        
			    }
			    else 
		    	{
		    		System.out.println("Inside Read");
		    		InputStreamReader ireader=new InputStreamReader(stream);
		             BufferedReader reader=new BufferedReader(ireader);
		            String line="";
		            System.out.println("Size is "+reader.readLine());
		            if(reader!=null)
		            	reader.readLine();
		             while((line=reader.readLine())!=null)
		            	 create(line);
		       }
			        
			       
			    } 
		
         }
		catch (FileUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	 }
	 
}
