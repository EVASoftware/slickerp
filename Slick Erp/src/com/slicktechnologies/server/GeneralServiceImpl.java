package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.OnSave;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.NBHCAssessmentReportPdf;
import com.slicktechnologies.server.addhocprinting.PdfUpdated;
import com.slicktechnologies.server.android.MarkCompletedSubmitServlet;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.cronjobimpl.AutomaticPRCreationCronJobImpl;
import com.slicktechnologies.server.cronjobinteration.HrCronJobImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.attendance.AttandanceError;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpenseReturnHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.SerialNumberDeviation;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;


public class GeneralServiceImpl extends RemoteServiceServlet implements GeneralService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2700258137658700795L;


	Logger logger = Logger.getLogger("Name of logger");
	
	ArrayList<Integer> intlist = new ArrayList<Integer>();
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	public static List<Branch> globalBranchlist = null;
	public static List<Employee> globalEmployeelist = null;
	public static List<ConfigCategory> globalConfig = null;
	public static List<City> globalCitylist = null;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


	@Override
	public ArrayList<Integer> createBillingDocs(Contract model) {
		// TODO Auto-generated method stub
		model.setStatus(Contract.APPROVED);
		
		GenricServiceImpl genricimpl = new GenricServiceImpl();
		genricimpl.save(model);
		
		/**
		 * Date 29-03-2017
		 * added by vijay
		 * for all operation after click on quickcontract submit button then it call task quque
		 * old code commented by vijay
		 */
		
		String contractinfo = model.getCompanyId()+"$"+model.getCount()+"$"+model.getStatus();
		
		Queue queue = QueueFactory.getQueue("quickcontractSubmit-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/quickContractSubmitTaskQueue").param("quickcontractkey", contractinfo));
		
		/**
		 * ends here
		 */
		
		intlist.add(1);
		return intlist;
	}

	

	/**
	 * Date 14 july 2017 added by vijay for Quick Sales submit Implementation
	 */
	
	@Override
	public String quickSalesSumbit(SalesOrder model) {

		String validationString = new String();
		
		try {
			
		model.setStatus(SalesOrder.APPROVED);
		GenricServiceImpl genservice = new  GenricServiceImpl();
		genservice.save(model);
		
		String quickSalesOrderInfo = model.getCompanyId()+"$"+model.getCount();
		
		
		Queue queue = QueueFactory.getQueue("quickSalesOrder-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/quickSalesOrderTaskQueue").param("quicksalesorderkey", quickSalesOrderInfo));
		
		validationString ="Submitted Successfully";
		
		} catch (Exception e) {
			System.out.println("ERROR"+e);
			validationString="Unexpected Error Ouccured!";
		}
		/** date 06/12/2017 added by komal for hvac on approval create quick contract **/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("QuickSalesOrder", "SIDDHISERVICES" , model.getCompanyId())) {
			    System.out.println("Inside quick contract creation");
				Contract contract = new Contract();
				contract.setCinfo(model.getCinfo());				
				contract.setRefNo2(model.getCount()+"");
				contract.setReferenceNumber(model.getRefNo());
				System.out.println("Hi =="+model.getCount()+"");
				contract.setRefDate(model.getSalesOrderDate());
				contract.setBranch(model.getBranch());
				contract.setEmployee(model.getEmployee());
//				contract.setPersonName(model.getCustomerFullName());
//			    contract.setProductTaxes(model.getProductTaxes());
//			    contract.setOtherCharges(model.getOtherCharges());
//				contract.setProductCharges(model.getProductCharges());
//				contract.setInclutaxtotalAmount(model.getInclutaxtotalAmount());
//				contract.setTotalAmount(model.getTotalAmount());
//				contract.setNetpayable(model.getNetpayable());
				contract.setContractDate(new Date());
				contract.setQuickContractInvoiceDate(new Date());
				//contract.setItems(model.getItems());
				ArrayList<SalesLineItem>  list = new ArrayList<SalesLineItem>();
				for(SalesLineItem s : model.getItems()){
					s.setStartDate(new Date());
					s.setEndDate(new Date());
					list.add(s);
					if(s.getProductCategory().equalsIgnoreCase("ASSET")){
						contract.setAMCNumber(s.getAmcNumber());
						  CompanyAsset companyAsset = ofy().load().type(CompanyAsset.class).filter("companyId", model.getCompanyId()).filter("name", contract.getAMCNumber() ).first().now();
						  if(companyAsset != null){
							  companyAsset.setStatus(false);
							  companyAsset.setAmcStatus("Retired");
							  ofy().save().entity(companyAsset);
						  }
					}
				}
				contract.setItems(list);
				contract.setApproverName(model.getApproverName());
				//contract.setp
				contract.setStatus(Contract.CREATED);	
				contract.setContractstatus(Contract.CREATED);	
				contract.setCompanyId(model.getCompanyId());
				saveContract(contract);
			}

		return validationString;
	}
	
	
	/**
	 * Date 07-08-2017 added by vijay for return expense  amount deposited to pettycash
	 */

	@Override
	public MultipleExpenseMngt returnExpenseAmount(MultipleExpenseMngt multiexpenseModel, double expenseReturnAmt,String loginUser) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside return Expense Amt");
		
		 double returnAvailableTotalamt = getAvailableTotalAmount(multiexpenseModel);
		
		 
		if(expenseReturnAmt <= returnAvailableTotalamt){
			
		  if(multiexpenseModel.getReturnExpenseHistory().size()!=0){
			  
			  ArrayList<ExpenseReturnHistory> expensehistorylist = multiexpenseModel.getReturnExpenseHistory();
			  ExpenseReturnHistory expensereturn = new ExpenseReturnHistory();
			  expensereturn.setExpenseReturnDate(new Date());
			  expensereturn.setExpenseReturnAmt(expenseReturnAmt);
			  expensereturn.setPersonResponsible(loginUser);
			  expensehistorylist.add(expensereturn);
			  
			  multiexpenseModel.setReturnExpenseHistory(expensehistorylist);
			  
		  }else{
			  
			  ArrayList<ExpenseReturnHistory> expensehistorylist = new ArrayList<ExpenseReturnHistory>();
			  ExpenseReturnHistory expensereturn = new ExpenseReturnHistory();
			  expensereturn.setExpenseReturnDate(new Date());
			  expensereturn.setExpenseReturnAmt(expenseReturnAmt);
			  expensereturn.setPersonResponsible(loginUser);

			  expensehistorylist.add(expensereturn);
			  
			  multiexpenseModel.setReturnExpenseHistory(expensehistorylist);
		  }
		 
		  
		  ofy().save().entities(multiexpenseModel);
		  
		  SaveExpenseInTransactionList(multiexpenseModel, expenseReturnAmt);

		  
		  addintoPettyCash(multiexpenseModel.getPettyCashName(),expenseReturnAmt, multiexpenseModel.getCompanyId());

		}
		
		 return multiexpenseModel;
	}
	
	
	private void addintoPettyCash(String pettyCashName, double expenseReturnAmt, Long companyId) {

		PettyCash pettycash = ofy().load().type(PettyCash.class).filter("companyId", companyId).filter("pettyCashName", pettyCashName).first().now();
		double pettyCashAmt = pettycash.getPettyCashBalance()+expenseReturnAmt;
		pettycash.setPettyCashBalance(pettyCashAmt);
		
		ofy().save().entity(pettycash);
	}



	private double getAvailableTotalAmount(MultipleExpenseMngt multiexpenseModel) {

		double totalReturnAmt=multiexpenseModel.getTotalAmount();
		
		if(multiexpenseModel.getReturnExpenseHistory().size()!=0){
			
			double returnamt = 0;
			for(int i=0;i<multiexpenseModel.getReturnExpenseHistory().size();i++){
				
				returnamt += multiexpenseModel.getReturnExpenseHistory().get(i).getExpenseReturnAmt();
			}
			totalReturnAmt = totalReturnAmt - returnamt ;
			 
		}
		
		return totalReturnAmt;
	}



	private void SaveExpenseInTransactionList(MultipleExpenseMngt multiexpenseModel, double expenseReturnAmt) {
		
		
		PettyCash pettycash =ofy().load().type(PettyCash.class).filter("pettyCashName", multiexpenseModel.getPettyCashName()).first().now();
		
		if(pettycash!=null){
			
		double pettycashCurrentBalance = pettycash.getPettyCashBalance();
		
		PettyCashTransaction pettycashTransaction = new PettyCashTransaction();
		pettycashTransaction.setOpeningStock(pettycashCurrentBalance);
		pettycashTransaction.setTransactionAmount(expenseReturnAmt);
		pettycashTransaction.setClosingStock(pettycashCurrentBalance+expenseReturnAmt);
		pettycashTransaction.setDocumentId(multiexpenseModel.getCount());
		pettycashTransaction.setDocumentName("Expense Management (Return)");
		Date saveDate = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy h:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String datestring =  sdf.format(saveDate);
		Logger logger = Logger.getLogger("NameOfYourLogger");
		String[] dateString=datestring.split(" ");
		String time=dateString[1];
		pettycashTransaction.setTransactionTime(time);
		pettycashTransaction.setTransactionAmount(expenseReturnAmt);
		try {
			pettycashTransaction.setTransactionDate(sdf.parse(datestring));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE," date parse exception  =="+e);
		}
		pettycashTransaction.setAppoverName(multiexpenseModel.getApproverName());
		pettycashTransaction.setPersonResponsible(multiexpenseModel.getEmployee());
		pettycashTransaction.setAction("+");
		pettycashTransaction.setDocumentTitle(multiexpenseModel.getPettyCashName());
		pettycashTransaction.setCompanyId(multiexpenseModel.getCompanyId());
		
		pettycashTransaction.setPettyCashName(multiexpenseModel.getPettyCashName());
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(pettycashTransaction);
		
		}
	}
	
	
	/**
	 * Date 18-08-2017 added by vijay for Quick Purchase submit Implementation
	 */
	
	@Override
	public String quickPurchaseOrderSubmit(PurchaseOrder model) {
		String validationString = new String();
		
		try {
			
		model.setStatus(PurchaseOrder.APPROVED);
		GenricServiceImpl genservice = new  GenricServiceImpl();
		genservice.save(model);
		
		String quickPurchaseOderInfo = model.getCompanyId()+"$"+model.getCount();
		
		
		Queue queue = QueueFactory.getQueue("quickPurchaseOrder-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/quickPurchaseOrderTaskQueue").param("purchaseoderkey", quickPurchaseOderInfo));
		
		validationString ="Submitted Successfully";
		
		} catch (Exception e) {
			System.out.println("ERROR"+e);
			validationString="Unexpected Error Ouccured!";
		}
		return validationString;
	}
	
	
	
	@Override
	public Integer TechnicianSchedulingPlan(Service service,	ArrayList<EmployeeInfo> technicianlist,
			ArrayList<CompanyAsset> technicianToollist,	ArrayList<ProductGroupList> materialInfoList, Date serviceDate, String serviceTime, int invoiceId,String invoiceDate,String TechnicianName, int projectId) {

		int id = createProject(service,technicianlist,
			technicianToollist,materialInfoList,serviceDate,serviceTime, invoiceId,invoiceDate, TechnicianName, projectId);

		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "UpdateProjectMaterial", service.getCompanyId())){
//			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			Queue queue = QueueFactory.getQueue("ProjectCreation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "UpdateProjectMaterial")
					.param("companyId", service.getCompanyId() + "")
					.param("serviceId", service.getCount()+"" )
					.param("projectId", id+""));
			logger.log(Level.SEVERE, "UpdateProjectMaterial");
		}
		
		return id;
	}


	

	private ServiceProject getFieldsToCustomerProjectScreen(ArrayList<EmployeeInfo> technicianlist,
			ArrayList<CompanyAsset> technicianToollist,
			ArrayList<ProductGroupList> materialInfoList, long companyId, int contractCount, int serviceId) {
		
		/** Service Entity Loading */
		Service serviceEntity=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractCount).filter("count", serviceId).first().now();
		
		/** Declaration Of Entities */
		
		 Customer cust=null;
		 Address addr=null;
		 Employee empEntity=null;
		 BillOfMaterial bomEntity=null;
		 
		 /** Loading Customer Information */
		 
		if(serviceEntity!=null&&serviceEntity.getPersonInfo().getCount()!=0)
		{
			 cust=ofy().load().type(Customer.class).filter("count",serviceEntity.getPersonInfo().getCount()).filter("companyId",companyId).first().now();
		}
		
		 if(cust!=null){
			  addr=cust.getSecondaryAdress();
		 }
	
		/** Loading Employee Information */
		 //  vijay modified if condition added( serviceEntity.getEmployee()!=null)this to codition )
		 //Date : 15/2/2017
		 if(serviceEntity.getCompanyId()!=null && serviceEntity.getEmployee()!=null){
			 empEntity=ofy().load().type(Employee.class).filter("fullname",serviceEntity.getEmployee().trim()).filter("companyId",companyId).first().now();
		 }
		
		 /** Loading Bill Of Material */
		 
		 if(serviceEntity.getCompanyId()!=null){
			 bomEntity=ofy().load().type(BillOfMaterial.class).filter("product_id", serviceEntity.getProduct().getCount()).filter("companyId",companyId).filter("status",true).first().now();
		 }
		
		ServiceProject cusproject=new ServiceProject();
		
		cusproject.setserviceId(serviceId);
		cusproject.setCompanyId(companyId);
		cusproject.setserviceEngineer(serviceEntity.getEmployee());
		cusproject.setServiceStatus(serviceEntity.getStatus());
		cusproject.setserviceDate(serviceEntity.getServiceDate());
		cusproject.setContractId(contractCount);
		cusproject.setContractStartDate(serviceEntity.getContractStartDate());
		cusproject.setContractEndDate(serviceEntity.getContractEndDate());
		cusproject.setPersonInfo(serviceEntity.getPersonInfo());
		cusproject.setPocName(serviceEntity.getEmployee());
		/**
		 *   rohan commented this line because of getting error As unexpected error
		 *   date: 25/01/2017
		 *   No need to set one field for 2 times 
		 */
//		cusproject.setPocName(empEntity.getFullname());
		// ends here 
		
		/** Date 13 Feb 2017
		 * if condition added by vijay for if service engineer is blank in service
		 * then dont set bellow details
		 */
		if(empEntity!=null){
			cusproject.setPocLandline(empEntity.getLandline());
			cusproject.setPocCell(empEntity.getCellNumber1());
			cusproject.setPocEmail(empEntity.getEmail());
		}
		/**
		 * end here
		 */
		cusproject.setProjectName("Project-"+serviceId);
		cusproject.setProjectStatus("Created");
		cusproject.setAddr(addr);
		cusproject.setServiceSrNo(serviceEntity.getServiceSerialNo());
		
			if(technicianToollist!=null){
				/**
				 * @author Vijay Date 20-07-2023
				 * Des :- getting structure issue so here article info not required so set as null to schedule the services
				 */
				ArrayList<CompanyAsset> assetlist = new ArrayList<CompanyAsset>();
				for(CompanyAsset compasset : technicianToollist) {
					assetlist.add(getAssetInfoWithoutArticleinfo(compasset));
				}
				/**
				 * ends here
				 */
				cusproject.setTooltable(assetlist);
			}
		
			/**
			 * nidhi
			 * 19-06-2018
			 */
			if(technicianlist.size() > 0 && technicianlist.get(0).getEmpCount()==0){
				ArrayList<EmployeeInfo> techlist = new ArrayList<EmployeeInfo>();
				for(int i=0;i<technicianlist.size();i++){
					Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", technicianlist.get(i).getFullName().trim()).first().now();
			  		if(emp!=null){
					EmployeeInfo empInfoEntity = new EmployeeInfo();
			  		empInfoEntity.setEmpCount(emp.getCount());
			  		empInfoEntity.setFullName(emp.getFullName());
			      	empInfoEntity.setCellNumber(emp.getCellNumber1());
			      	empInfoEntity.setDesignation(emp.getDesignation());
			      	empInfoEntity.setDepartment(emp.getDepartMent());
			      	empInfoEntity.setEmployeeType(emp.getEmployeeType());
			      	empInfoEntity.setEmployeerole(emp.getRoleName());
			      	empInfoEntity.setBranch(emp.getBranchName());
			      	empInfoEntity.setCountry(emp.getCountry());
			      	techlist.add(empInfoEntity);
			  		}
				}
				cusproject.setTechnicians(techlist);
				logger.log(Level.SEVERE,"Technician list set as techlist"+techlist.size());
			}else{
				cusproject.setTechnicians(technicianlist);
				logger.log(Level.SEVERE,"Technician list set as technicianlist"+technicianlist.size());
			}
			
			
			cusproject.setProdDetailsList(materialInfoList);
		
		return cusproject;
	}
	


	private CompanyAsset getAssetInfoWithoutArticleinfo(CompanyAsset compasset) {
		CompanyAsset asset = new CompanyAsset();
		asset.setArticleTypeDetails(null);
		asset.setAssetMovementList(null);
		asset.setBranch(compasset.getBranch());
		asset.setBrand(compasset.getBrand());
		asset.setCompanyId(compasset.getCompanyId());
		asset.setCount(compasset.getCount());
		if(compasset.getDateOfInstallation()!=null)
		asset.setDateOfInstallation(compasset.getDateOfInstallation());
		if(compasset.getDateOfManufacture()!=null)
		asset.setDateOfManufacture(compasset.getDateOfManufacture());
	
		asset.setModelNo(compasset.getModelNo());
		asset.setName(compasset.getName());
		
		return asset;
	}



	@Override
	public String saveSelectedService(ArrayList<Service> servicelist, long companyId) {

		NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("companyId",servicelist.get(0).getCompanyId()).filter("processName", "TechnicianScheduling").filter("status", true).first().now();
		
		long number=0;
		if(ng!=null){
			
			number = ng.getNumber()+1;
			for(int i=0;i<servicelist.size();i++){
				
				servicelist.get(i).setSchedulingNumber(number);
			}
			ng.setNumber(number);
			ofy().save().entity(ng);
			
			
		}
		if(servicelist != null && servicelist.size() > 0){
			for(Service ser : servicelist){
				logger.log(Level.SEVERE , "service :" + ser.getCount());
	
				ofy().save().entity(ser);
			}
			
		}
		/**
		 * @author Anil @since 12-08-2021
		 * Reseting cache memory after saving service details
		 */
		 ObjectifyService.reset();
		 ofy().consistency(Consistency.STRONG);
		 ofy().clear();
		 
		return number+"";
	}



	@Override
	public String saveServiceProjectStatus(Service service) {

		ofy().save().entity(service);
		
		ServiceProject project = ofy().load().type(ServiceProject.class).filter("count", service.getProjectId()).filter("companyId", service.getCompanyId())
				.filter("serviceId",service.getCount()).first().now();
		if(project!=null){
			project.setProjectStatus(ServiceProject.COMPLETED);
			ofy().save().entity(project);
		}
		
		/**
		 * @author Vijay Chougule Date 22-07-2020
		 * Des :- when we mark complete service from customer list (Service Table) the is service is Service wise billing then
		 * its bill will create
		 */
		
		System.out.println("status"+service.getStatus());
		if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
			ServiceImplementor serviceimpl = new ServiceImplementor();
			serviceimpl.createBillOnServiceCompletion(service);
			
		}
		
		/** DATE 29.3.2019 added by komal to send sr copy to customer after completion of service **/
		Email email = new Email();
		MarkCompletedSubmitServlet servlet = new MarkCompletedSubmitServlet();
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", service.getCompanyId())){
			servlet.sendEmailtoCustomer(service , email);
		}
		return "";
	}
	
	 /** date 26/10/2017 added by komal  to update followup date in quotation and lead **/
	@Override
	public String updateLeadQuotationFollowUpDate(long companyId) {
		String taskName="updateFollowUpDateTaskQueueProcess"+"$"+companyId;
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		return null;
	}

	/**
	 * Date 22-12-2017 added by vijay
	 * for all payment document updating for TDS flag index
	 */

	@Override
	public Integer updateAllPaymentDocumentWithTDSflagIndex(long companyId) {

		List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("companyId", companyId).list();
		
		ofy().save().entities(paymentlist);
		
		return 1;
	}



	//	date 01-02-2018 added by komal for nbhc  closing stock
	@Override
	public ArrayList<ProductInventoryViewDetails> getProductInventoryViewDetails(
			long companyId, String branch, String warehouse) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE , "company id : "+companyId);
		List<ProductInventoryViewDetails> pList = new ArrayList<ProductInventoryViewDetails>();
		ArrayList<ProductInventoryViewDetails> productList = new ArrayList<ProductInventoryViewDetails>();
		List<WareHouse> warehouseList = ofy().load().type(WareHouse.class).filter("companyId", companyId).list();
		logger.log(Level.SEVERE , "size : "+warehouseList.size());
		//String branchesStr = "";
		String branch1 = "";
		Map<String , String> map = new HashMap<String,String>();
		if(branch == null){
			branch1 = "";
		}else{
			branch1=branch.trim();
		}
		List<String> branchwiseWarehouseList = new ArrayList<String>();
		for(WareHouse w : warehouseList){
			if(w.getBranchdetails() != null){
			  if(w.getBranchdetails().size() != 0){
				for(BranchDetails b : w.getBranchdetails()){
					if(branch1.equalsIgnoreCase(b.getBranchName().trim())){
						branchwiseWarehouseList.add(w.getBusinessUnitName());
					}
					logger.log(Level.SEVERE , "branch name : "+b.getBranchName());
					if(b.getBranchName()!=null){
					if(map.containsKey(w.getBusinessUnitName())){
						map.put(w.getBusinessUnitName() , map.get(w.getBusinessUnitName())+b.getBranchName()+"/");
					}else{
						map.put(w.getBusinessUnitName(), b.getBranchName()+"/");
					}
				  }
				}
			}
		   }	  
		}
		logger.log(Level.SEVERE , "branch size : "+branchwiseWarehouseList.size());
		if(branch != null && warehouse == null){	
			if(branchwiseWarehouseList.size()>0){
				pList = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).filter("warehousename IN", branchwiseWarehouseList).list();
			}
		}
		if(branch == null && warehouse != null){
			pList = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).filter("warehousename", warehouse).list();		
		}
		if(branch != null && warehouse != null){
			String str ="";
			for(String s :branchwiseWarehouseList){
				if(s.equalsIgnoreCase(warehouse)){
					str = s;
				}
			}
			logger.log(Level.SEVERE , "warehouse : "+str);
			pList = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).filter("warehousename", str).list();			
		}
		if(branch == null && warehouse == null){
			pList = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).list();			
		}
		if(branch == null){
			for(ProductInventoryViewDetails p :pList){
				if(map.get(p.getWarehousename())!=null){
				p.setCreatedBy(map.get(p.getWarehousename()).substring(0, map.get(p.getWarehousename()).length()-1));
				productList.add(p);
				}
			}
		}else{
			productList.addAll(pList);
		}
		
		return productList;
	}


	/**	date 02-02-2018 added by komal for nbhc purchase requisition **/
	@Override
	public ArrayList<PurchaseRequisition> getPurchaseRequisitionDetails(long companyId,String bussinessType ,
			ArrayList<PurchaseRequisition> prList) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		HashSet<Integer> hSet = new HashSet<Integer>();
		for (PurchaseRequisition p : prList) {
			hSet.add(p.getCount());
		}
		logger.log(Level.SEVERE , "hset size : "+hSet.size());
		List<Integer> idList = new ArrayList<Integer>(hSet);
		// TODO Auto-generated method stub
		ArrayList<Approvals> approvalList1 = new ArrayList<Approvals>();
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		List<Approvals> approvalList = ofy().load().type(Approvals.class)
				.filter("companyId", companyId)
				.filter("businessprocesstype", bussinessType)
				.filter("businessprocessId IN", idList).list();
		String str = "";
		logger.log(Level.SEVERE , "approval list size : "+approvalList.size());
		Comparator<Approvals> compare = new  Comparator<Approvals>() {
			
			@Override
			public int compare(Approvals o1, Approvals o2) {
				// TODO Auto-generated method stub
				return Integer.compare(o2.getCount(), o1.getCount());
			}
		};
		Collections.sort(approvalList,compare);
		for (Approvals a : approvalList) {
			if (map.containsKey(a.getBusinessprocessId())) {
				str = "REQ BY: " + a.getRequestedBy() + " "
						+ "REQ DT: " + fmt.format(a.getCreationDate())
						+ " " + "APP LVL: " + a.getApprovalLevel() + " "
						+ "APP NAME: " + a.getApproverName() + " "
						+ "STATUS: " + a.getStatus() + " " + "REMARK: "
						+ a.getRemark() + " ";
				if(a.getApprovalOrRejectDate()!=null){
					str = str+ "APP/REJ DT: " + fmt.format(a.getApprovalOrRejectDate()) + " ";
				}
					str = str + "," +map.get(a.getBusinessprocessId());
				map.put(a.getBusinessprocessId(), str);
			} else {
				str = "REQ BY: " + a.getRequestedBy() + " "
						+ "REQ DT: " + fmt.format(a.getCreationDate())
						+ " " + "APP LVL: " + a.getApprovalLevel() + " "
						+ "APP NAME: " + a.getApproverName() + " "
						+ "STATUS: " + a.getStatus() + " " + "REMARK: "
						+ a.getRemark() + " ";
				if(a.getApprovalOrRejectDate()!=null){
					str = str+ "APP/REJ DT: " + fmt.format(a.getApprovalOrRejectDate()) + " " + ",";
				}else{
					str += ",";
				}
				map.put(a.getBusinessprocessId(), str);
			}
		}
		logger.log(Level.SEVERE , "map size : "+map.size());
		ArrayList<PurchaseRequisition> list = new ArrayList<PurchaseRequisition>();
		for (PurchaseRequisition pr : prList) {
			if(map.containsKey(pr.getCount())){
				pr.setApprovalStatus(map.get(pr.getCount()).substring(0 ,map.get(pr.getCount()).length() - 1));  
			}else{
				pr.setApprovalStatus("");
			}
          list.add(pr);
		}
		logger.log(Level.SEVERE , "list size : "+list.size());
		return list;
	}
	@Override
	public Contract createAndGetContractDetails(long companyId,
			Complain complain , String user) {
		Contract contract = new Contract();
		if(complain.getExistingContractId()==null){
		contract.setCinfo(complain.getPersoninfo());
		contract.setBranch(complain.getBranch());
		contract.setEmployee(complain.getAssignto());
		contract.setServiceWiseBilling(true);
		contract.setComplaintFlag(true);
		contract.setTicketNumber(complain.getCount());
		contract.setIsQuotation(false);
		contract.setStatus(contract.APPROVED);
		contract.setContractDate(complain.getNewServiceDate());
		contract.setStartDate(complain.getNewServiceDate());
		contract.setEndDate(complain.getNewServiceDate());
		List<SalesLineItem> list = new ArrayList<SalesLineItem>();
		int prodId = complain.getPic().getProdID();
		SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", prodId).filter("status", true).first().now();
		SalesLineItem lineItem=new SalesLineItem();
		lineItem.setPrduct(product);
		lineItem.setQuantity(1.0);
		lineItem.setStartDate(complain.getNewServiceDate());
		lineItem.setEndDate(complain.getNewServiceDate());
		lineItem.setDuration(1);
		lineItem.setComplainService(true);
		/**
	     * nidhi
	     * 21-08-2018
	     */
	    lineItem.setProModelNo(complain.getModelNumber());
	    lineItem.setProSerialNo(complain.getProSerialNo());
		list.add(lineItem);
		contract.setItems(list);
		contract.setCompanyId(companyId);
		NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("companyId" , companyId).filter("processName", "Contract").filter("status", true).first().now();
		long number = ng.getNumber();
	    ng.setNumber(number + 1);
	    ofy().save().entity(ng);
	    contract.setCount((int)(number + 1));
	    ofy().save().entity(contract);
	    complain.setContrtactId((int)(number + 1));
	    
	    Service service = createServiceFromComplain(companyId , complain , "" , (int)(number + 1));
	   		}else{
			contract.setCount(complain.getExistingContractId());
			Service service = createServiceFromComplain(companyId , complain , "" , complain.getExistingContractId());
		}
	    
		return contract;
	}


	public Service createServiceFromComplain(long companyId,
			Complain complain, String user , int contractId) {
		// TODO Auto-generated method stub
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");

		Calendar c = Calendar.getInstance(); 
    	c.setTime(complain.getNewServiceDate()); 
    	String day = simpleDateformat.format(c.getTime());
		NumberGeneration ng=ofy().load().type(NumberGeneration.class).filter("companyId" , companyId).filter("processName", "Service").filter("status", true).first().now();
		long number = ng.getNumber();
	    ng.setNumber(number + 1);
	    ofy().save().entity(ng);
		Service service = new Service();
		service.setCount((int)number+1);
		service.setServiceBranch("Service Address");
		service.setCompanyId(companyId);
		service.setBranch(complain.getBranch());
		service.setServiceDate(complain.getNewServiceDate());
		service.setServiceDay(day);
		service.setPersonInfo(complain.getPersoninfo());
		service.setStatus(Service.SERVICESTATUSSCHEDULE);
	//	service.setServiceCompletionDate(complain.getNewServiceDate());
		service.setEmployee(complain.getAssignto());
		service.setServiceCompleteDuration(1);
		service.setTicketNumber(complain.getCount());
		service.setContractCount(contractId);
		/**
		 * nidhi
		 * 
		 */
		service.setProModelNo(complain.getModelNumber());
		service.setProModelNo(complain.getProSerialNo());
		Contract contract = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", contractId).first().now();
		List<Service> serviceList = new ArrayList<Service>();
		if(contract!=null){
			service.setContractStartDate(contract.getStartDate());
			service.setContractEndDate(contract.getEndDate());
			/**
			 * @author Anil , Date : 21-09-2019
			 * Added service product  id filter to load products
			 */
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractId).filter("product.count", complain.getPic().getProdID()).list();
		}else{
			service.setContractStartDate(complain.getNewServiceDate());
			service.setContractEndDate(complain.getNewServiceDate());
		}
		service.setServiceType("Complain");
		if(complain.getDestription()!=null){
		    service.setDescription(complain.getDestription());
		}
		if(serviceList.size()>0){
			service.setServiceSerialNo(serviceList.size() +1 );
			/**
			 * @author Anil , Date : 21-09-2019
			 * Setting up service sr no which will be used at the time of bill generation
			 */
			service.setServiceSrNo(serviceList.get(0).getServiceSrNo());
		}else{
			service.setServiceSerialNo(1);
		}
		
		/**
		 * @author Anil
		 * @since 22-06-2020
		 * setting asset id,asset unit and mfg num in description
		 */
		String descHead="";
		String descVal="";
		if(complain.getAssetId()!=0){
			descHead="Asset Id";
			descVal=complain.getAssetId()+"";
			service.setAssetId(complain.getAssetId());
		}
		if(complain.getAssetUnit()!=null&&!complain.getAssetUnit().equals("")){
			if(!descHead.equals("")){
				descHead=descHead+"/"+"Asset Unit";
				descVal=descVal+"/"+complain.getAssetUnit();
			}else{
				descHead="Asset Unit";
				descVal=complain.getAssetUnit();
			}
			service.setAssetUnit(complain.getAssetUnit());
		}
		if(complain.getMfgNum()!=null&&!complain.getMfgNum().equals("")){
			if(!descHead.equals("")){
				descHead=descHead+"/"+"Mfg No.";
				descVal=descVal+"/"+complain.getMfgNum();
			}else{
				descHead="Mfg No.";
				descVal=complain.getMfgNum();
			}
			service.setMfgNo(complain.getMfgNum());
		}
		
		if(complain.getMfgDate()!=null){
			service.setMfgDate(complain.getMfgDate());
		}
		
		if(complain.getReplacementDate()!=null){
			service.setReplacementDate(complain.getReplacementDate());
		}
		
		if(complain.getComponentName()!=null){
			service.setComponentName(complain.getComponentName());
		}
		
		/**
		 * @author Anil
		 * @since 04-05-2020
		 * Setting expected completion date and time in service description
		 */
		String description="";
		if(!descHead.equals("")){
			description=descHead+" : "+descVal+" ";
		}
		
		if(complain.getExpectedCompletionDate()!=null&&complain.getExpectedCompletionTime()!=null&&!complain.getExpectedCompletionTime().equals("")) {
			description=description+"Exp Date/Time : "+dateFormat.format(complain.getExpectedCompletionDate())+"/"+complain.getExpectedCompletionTime();
		}
		
		
		/**
		 * @author Anil
		 * @since 11-05-2020
		 **/
		
		if(!description.equals("")){
			description=description+" "+service.getDescription();
		}else{
			description=service.getDescription();
		}
		
		service.setDescription(description);
		
		List<SalesLineItem> list = new ArrayList<SalesLineItem>();
		int prodId = complain.getPic().getProdID();
		SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", prodId).filter("status", true).first().now();
        service.setProduct((ServiceProduct)product);
        /***27-12-2019 Deepak Salve added this code for set complain product name into complain***/
        service.getProduct().setProductName(complain.getProductName());
        /***End***/
        //changed by komal on 28.4.2018 
        if(complain.getProductName()!=null && !complain.getProductName().equals("") && complain.getProductName().contains("/")){
        	String [] array = complain.getProductName().split("/");
        	service.setPremises(array[1]);
        }
        
        Customer cust=ofy().load().type(Customer.class).filter("companyId", companyId).filter("count",complain.getPersoninfo().getCount()).
				   first().now();  
        if(cust !=null){
	        service.setAddress(cust.getSecondaryAdress());
	        service.setRefNo(cust.getRefrNumber1());
        }
        service.setServiceWiseBilling(true);
        
        /**
         * @author Anil
         * @since 01-06-2020
         * If customer branch is selected on complain then set customer branch name as service branch
         * and also pick customer branch service address as service address
         */
        if(complain.getCustomerBranch()!=null&&!complain.getCustomerBranch().equals("")){
        	service.setServiceBranch(complain.getCustomerBranch());
        	CustomerBranchDetails custBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).filter("buisnessUnitName", complain.getCustomerBranch()).filter("cinfo.count",complain.getPersoninfo().getCount()).first().now(); 
        	if(custBranch!=null){
        		if(custBranch.getAddress()!=null){
        			service.setAddress(custBranch.getAddress());
        			/**
        			 * @author Anil
        			 * @since 08-06-2020
        			 */
        			service.setTierName(custBranch.getTierName());
        		}
        	}
        }
        
        /**
         * @author Anil
         * @since 11-05-2020
         */
        if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "ComplainServiceWithTurnAroundTime", service.getCompanyId())) {
        	service.setServiceTime(DateUtility.convert24Hrsto12HrsFormat(complain.getNewServiceTime()));
        }else{
        	service.setServiceTime(complain.getServicetime());
        }
        
        if(complain.getServiceType()!=null && !complain.getServiceType().equals("")){
        	service.setServiceType(complain.getServiceType());
        }
        
        ofy().save().entity(service);
        complain.setServiceDate(service.getServiceDate());
        complain.setServiceId(service.getCount());
        
        ofy().save().entity(complain);
		return service;
	}


	 /** date 05/12/2017 added by komal to  save customer log details **/
		@Override
		public CustomerLogDetails saveCustomerCallLogDetails(CustomerLogDetails customerLog,
				long  companyId ) {
		    GenricServiceImpl genricimpl = new GenricServiceImpl();
		  //  Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("companyName", customerLog.getCompanyName()).first().now();
		   // Customer checkCustomer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("contacts.cellNo1", customerLog.getCinfo().getCellNumber()).filter("fullname" , customerLog.getCinfo().getFullName().toUpperCase().trim()).first().now();
		    Customer checkCustomer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count" , customerLog.getCinfo().getCount()).first().now();
		    //  System.out.println("customer" + customer);
   	        System.out.println("customer exists" + checkCustomer+""+customerLog.getCinfo().getCount());
		    if(customerLog.getCount() != 0){
			if (customerLog.getVisitStatus() != null) {
				if (!(customerLog.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED))) {
					if (customerLog.getVisitStatus().equalsIgnoreCase("Resolved")) {
						customerLog.setStatus(CustomerLogDetails.COMPLETED);
					}
					if (customerLog.getVisitStatus().equalsIgnoreCase("Material Pending")) {
						customerLog.setStatus(CustomerLogDetails.MATERIALPENDNG);
					}
					if (customerLog.getVisitStatus().equalsIgnoreCase("Cancelled")) {
						customerLog.setStatus(CustomerLogDetails.CANCELLED);
					}
					if(customerLog.getVisitStatus().equalsIgnoreCase("Hold")){
						customerLog.setStatus(customerLog.CREATED);
					}
				}
			}
		    	checkCustomer.setSecondaryAdress(customerLog.getAddress());
		    	checkCustomer.setCellNumber2(customerLog.getCellNumber2());
		    	ofy().save().entity(checkCustomer);
		    	
		    	
		    }
		    else{
		    customerLog.setCreatedOn(new Date());  
   	        if(checkCustomer == null){
//		    Customer cust=new Customer();	
//			if(!customerLog.getCinfo().getFullName().equals("")){
//				cust.setFullname(customerLog.getCinfo().getFullName().toUpperCase().trim());
//				cust.setCompanyName(customerLog.getCinfo().getFullName().toUpperCase().trim());
//				String customerName = customerLog.getCinfo().getFullName().toUpperCase().trim();
//				customerLog.getCinfo().setFullName(customerName);
//			}		
//			if(!customerLog.getCompanyName().equals(""))
//			{
//				cust.setCustPrintableName(customerLog.getCompanyName().toUpperCase().trim());	
//			}
//			if(!customerLog.getCinfo().getCellNumber().equals("")){
//				cust.setCellNumber1(customerLog.getCinfo().getCellNumber());
//			}
////			if(!customer.getAdress().equals("")){
////				cust.setAdress(customer.getAdress());
////			}
//				cust.setCompanyId(companyId);
//				ReturnFromServer entity = genricimpl.save(cust);
//				customerLog.getCinfo().setCount(entity.count);
//				customerLog.getCinfo().setCount(cust.getCount());
//				customerLog.getCinfo().setFullName(cust.getFullname());
//				customerLog.getCinfo().setCellNumber(cust.getCellNumber1());
   	        	/** date 10.5.2018 added by komal **/
//				if(checkCustomer.getAdress()!=null){
//					customerLog.setAddress(checkCustomer.getAdress());
//				}
//				customerLog.setStatus(CustomerLogDetails.CREATED);
			} else {
				customerLog.getCinfo().setCount(checkCustomer.getCount());
				/**
				 * Date 13-6-2018
				 * by jayshree
				 */
				if(checkCustomer.getCompanyName()!=null){
				customerLog.getCinfo().setFullName(checkCustomer.getCompanyName());
				}
				else{
					customerLog.getCinfo().setFullName(checkCustomer.getCompanyName());
				}
				
				//End By Jayshree
				customerLog.getCinfo().setCellNumber(checkCustomer.getCellNumber1());
				/** date 10.5.2018 added by komal **/
				if(checkCustomer.getAdress()!=null){
					customerLog.setAddress(checkCustomer.getAdress());
				}
				customerLog.setStatus(CustomerLogDetails.CREATED);
			}
		}
		    /**
		     * Date 21-6-2018
		     * by jayshree comment this
		     */
		  //  if(customerLog.getCount() != 0){
		    	if(customerLog.getServiceCharges().size()>0){
		    		MaterialRequired m = customerLog.getServiceCharges().get(0);
		    		SuperProduct superProduct = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("productCode", m.getProductCode()).filter("count", m.getProductId()).filter("status", true).first().now();
		    		m.setPrduct(superProduct);
		    		m.setTotalAmount(m.getProductPrice());
					m.setVatTax(superProduct.getVatTax());
					m.setServiceTax(superProduct.getServiceTax());
					customerLog.getServiceCharges().set(0, m);
					logger.log(Level.SEVERE ,"service charge :"+ customerLog.getServiceCharges().size());
		    	}
		 //   }
		   if(customerLog.getCount() != 0){
				if(customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") &&  !(customerLog.getCallType().equalsIgnoreCase("Installation"))){
					if(customerLog.getStatus().equalsIgnoreCase(CustomerLogDetails.COMPLETED)){			
						ArrayList<MaterialRequired> serviceList = new ArrayList<MaterialRequired>();
						if(customerLog.getCustomerCost().size()==0){
							createBilling(customerLog, customerLog.getCount(), "", serviceList, companyId);
						}
					}
				  }
				}

		    GenricServiceImpl genricimpl1 = new GenricServiceImpl();
		    ReturnFromServer entity = genricimpl1.save(customerLog);
		    customerLog.setCount(entity.count);
		    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerLogDetails","CallWiseService" , customerLog.getCompanyId())){
//				if(customerLog.getCount()!=0){
					SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
					Calendar c = Calendar.getInstance(); 
					Date date = new Date();
					if(customerLog.getAppointmentDate()!=null){
						date = customerLog.getAppointmentDate();
					}
			    	c.setTime(date); 
			    	String day = simpleDateformat.format(c.getTime());
					Service service = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", customerLog.getCount()).first().now();
					if(service == null){
						service = new Service();
					}
//					service.setPremises(customerLog.getModelNo()+""+customerLog.getProdSerialNumber());
					System.out.println("customerLog.getModelNo()"+customerLog.getModelNo()+""+customerLog.getProdSerialNumber());
					/**
					 * nidhi
					 * 21-08-2018
					 */
					service.setProModelNo(customerLog.getModelNo());
					service.setProSerialNo(customerLog.getProdSerialNumber());
					/**
					 * end
					 */
					service.setServiceBranch("Service Address");
					service.setCompanyId(companyId);
					service.setBranch(customerLog.getCompanyName());
					service.setServiceDate(date);
					service.setServiceDay(day);
					service.setPersonInfo(customerLog.getCinfo());
					service.setStatus(Service.SERVICESTATUSSCHEDULE);
					// by Apeksha Gunjal on 29/06/2018 @16:47
					String serviceEmployee = service.getEmployee();
					
					service.setEmployee(customerLog.getTechnician());
					service.setServiceCompleteDuration(1);
					service.setContractCount(customerLog.getCount());
					service.setServiceSerialNo(1);
                    service.setCallwiseService(true);
					if(customerLog.getRemark()!=null){
					    service.setDescription(customerLog.getRemark());
					}
					
					if(customerLog.getAddress()!=null){
						service.setAddress(customerLog.getAddress());
					}
					if(customerLog.getServiceCharges().size()>0){
						service.setProduct((ServiceProduct)customerLog.getServiceCharges().get(0).getPrduct());
					}
					
					/**
					 * Date 18-5-2018 by jayshree
					 * des.to set the premises value 
					 */
//					if(customerLog.getModelNo()!=null && customerLog.getProdSerialNumber()!=null){
						//service.setPremises(customerLog.getModelNo()+customerLog.getProdSerialNumber());
						System.out.println("customerLog.getModelNo()"+customerLog.getModelNo()+customerLog.getProdSerialNumber());
//					}
						/*
						 * Name:Apeksha Gunjal
						 * Date: 29/06/2018 @16:47
						 * Note: Send Notification to Technician on EVA Pedio after assigning respective technician
						 */
					ReturnFromServer r = genricimpl1.save(service);
					service.setCount(r.count);
					/** date 18.7.2018 added by komal to create service project **/
					createProject(service);
					try{
						logger.log(Level.SEVERE, "CustomerCallLogDetails service.getEmployee(): "+serviceEmployee+" customerLog.getTechnician(): "+customerLog.getTechnician());
						
						if(!serviceEmployee.trim().equalsIgnoreCase(customerLog.getTechnician().trim())){
							sendPushNotificationToTechnician(service, companyId);
						}
					}catch(Exception e){
						logger.log(Level.SEVERE, "CustomerCallLogDetails Error in sending push notification to technician: "+e.getMessage());
					}
//				}
			}
			
		 //   ReturnFromServer entity = genricimpl1.save(customerLog);	
						
			/**
	    	 * By jayshree
	    	 */
	    	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerLogDetails", "OnlyForMASCOM",customerLog.getCompanyId())){
	    		
	    		customerLog.setComplaintNumber(entity.count+"");
	    	
	    	ofy().save().entity(customerLog);
	    	}
	    	return customerLog;
	}
		
		 /** date 07/11/2017 added by komal for creating quick contract for hvac **/
		  private void saveContract(final Contract contract){
				System.out.println("1-INSIDE CONTRACT SAVE");
				GenricServiceImpl service= new GenricServiceImpl();
				ReturnFromServer result = service.save(contract);
				System.out.println("inside quick contract" + result.count);
				}
		  /** date 07/11/2017 added by komal  for hvac **/
		@Override
		public String createBilling(CustomerLogDetails customerLog ,int customerLogId,String approverName ,ArrayList<MaterialRequired> serviceList ,long companyId) {
			// TODO Auto-generated method stub
			Gson gson = new Gson();
			System.out.println("inside create Invoice 1 :" + approverName );
			System.out.println("Customer log object :" + customerLog.toString() );
			//CustomerLogDetails customerLog = ofy().load().type(CustomerLogDetails.class).filter("companyId", companyId).filter("count", customerLogId ).first().now();
			if(serviceList.size()!=0)
			customerLog.setServiceCharges(serviceList);
			customerLog.setStatus(CustomerLogDetails.CLOSED);
			customerLog.setClosingDate(new Date());
			ofy().save().entity(customerLog);
			String str = gson.toJson(customerLog, CustomerLogDetails.class);
	        Queue queue = QueueFactory.getQueue("documentCancellation-queue");
	        String typeducumentName = "InvoiceCreation"+"$"+customerLog.getCompanyId()+"$"+customerLog.getCount()+"$"+str;
	   	  	logger.log(Level.SEVERE, "Company Id "	+ typeducumentName);
	   	  	queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", typeducumentName));	
			return "success";
		}
		 /** date 07/11/2017 added by komal  for hvac **/
		@Override
		public String sendDefective(ArrayList<MaterialMovementNote> selectedList,
				Long companyId, String approverName) {
			     Gson gson = new Gson();
			     String str = "";
			     String typeducumentName = "";
		         Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		         if(selectedList.size() > 0){
		        	 str = gson.toJson(selectedList);
		         	 typeducumentName = "CreateGRN"+"$"+companyId+"$"+str;
		         	 logger.log(Level.SEVERE, "Company Id "	+ typeducumentName);
		         	 queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", typeducumentName).param("approverName", approverName));
		         }	
		       
		         //CreateGRN
			return null;
		}
		//hvac
		@Override
		public GRN getGRNForDefective(Integer grnId, Long companyId) {
			// TODO Auto-generated method stub
			GRN grn = ofy().load().type(GRN.class).filter("companyId", companyId).filter("count", grnId).first().now();
			return grn;
		}
		@Override
		public String saveAndApproveDocument(SuperModel m,
				String customerId, String customerName, String documentName) {
			// TODO Auto-generated method stub
			String count = "";
			MaterialIssueNote min = null;
			MaterialMovementNote mmn = null;
			DeliveryNote dNote = null;
			/**
			 * Updated By: Viraj
			 * Date: 28-05-2019
			 * Description: For approval of PurchaseOrder and SalesOrder
			 */
			PurchaseOrder Po = null;
			SalesOrder So = null;
			/** Ends **/
			ReturnFromServer result;
			Approvals approval = null;
			ApprovableServiceImplementor approvalService= new ApprovableServiceImplementor();
			ApproverFactory appFactory=new ApproverFactory(); 
			GenricServiceImpl genricimpl = new GenricServiceImpl();
			String id = customerId+"";
			if(documentName.equalsIgnoreCase(AppConstants.MIN)){
				min = (MaterialIssueNote) m;
				result = genricimpl.save(min);
				count = result.count+"";
				min.setCount(result.count);
				approval=appFactory.getApprovalMMN(min.getEmployee(),min.getApproverName()
						, min.getBranch(), min.getCount(),min.getCreatedBy(),customerId,customerName,AppConstants.MIN);
				approval.setCompanyId(min.getCompanyId());
			}
			if(documentName.equalsIgnoreCase(AppConstants.MMN)){
				mmn = (MaterialMovementNote) m;
				result = genricimpl.save(mmn);
				count = result.count+"";
				mmn.setCount(result.count);
				approval=appFactory.getApprovalMMN(mmn.getEmployee(),mmn.getApproverName()
						, mmn.getBranch(), mmn.getCount(),mmn.getCreatedBy(),customerId,customerName,AppConstants.MMN);
				approval.setCompanyId(mmn.getCompanyId());
			}
			if(documentName.equalsIgnoreCase(AppConstants.DELIVERYNOTE)){
				dNote = (DeliveryNote) m;
				result = genricimpl.save(dNote);
				count = result.count+"";
				dNote.setCount(result.count);
				approval=appFactory.getApprovalMMN(dNote.getEmployee(),dNote.getApproverName()
						, dNote.getBranch(), dNote.getCount(),dNote.getCreatedBy(),customerId,customerName,AppConstants.DELIVERYNOTE);
				approval.setCompanyId(dNote.getCompanyId());
			}
			/**
			 * Updated By: Viraj
			 * Date: 28-05-2019
			 * Description: For approval of PurchaseOrder and SalesOrder
			 */
			if(documentName.equalsIgnoreCase(AppConstants.PURCHASEORDER)){
				Po = (PurchaseOrder) m;
				result = genricimpl.save(Po);
				count = result.count+"";
				Po.setCount(result.count);
				approval=appFactory.getApprovalMMN(Po.getEmployee(),Po.getApproverName()
						, Po.getBranch(), Po.getCount(),Po.getCreatedBy(),customerId,customerName,AppConstants.PURCHASEORDER);
				approval.setCompanyId(Po.getCompanyId());
			}
			
			if(documentName.equalsIgnoreCase(AppConstants.SALESORDER)){
				So = (SalesOrder) m;
				result = genricimpl.save(So);
				count = result.count+"";
				So.setCount(result.count);
				approval=appFactory.getApprovalMMN(So.getEmployee(),So.getApproverName()
						, So.getBranch(), So.getCount(),So.getCreatedBy(),customerId,customerName,AppConstants.SALESORDER);
				approval.setCompanyId(So.getCompanyId());
			}
			/** Ends **/
			approval.setStatus(ConcreteBusinessProcess.APPROVED);
			approval.setApprovalOrRejectDate(new Date());
			result = genricimpl.save(approval);
			approval.setId(result.id);
			String approve = approvalService.sendApproveRequest(approval);
			if(approve.equals("Success")){
				System.out.println("Success : "+"Submited successfully!");	
				logger.log(Level.SEVERE , "Success : "+"Submited successfully!");	
			}else{
				System.out.println("result : "+result);
				logger.log(Level.SEVERE , "result : "+result);	
			}
			return count;
		}
		
		
		/**
		 * Date 09-05-2018
		 * Developer : Vijay
		 * Des :- Payment document update with follow up date for status created only because we load created status dashboard only
		 */

		@Override
		public Integer updatePaymentDocsWithFollowupDate(Long companyId) {

			try {
				
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "UpdateFollowUpDatePaymentDocs").param("companyId", companyId+""));
				
			} catch (Exception e) {
				logger.log(Level.SEVERE,"Exception payment Date follow up update =="+e.getMessage());
				return -1;
			}
			
			return 1;
		}
		/**
		 * ends here
		 */
		@Override
		public Invoice generateInvoiceForFumigation(
				long companyId, Fumigation fumigation) {
			// TODO Auto-generated method stub
			Invoice	invoice = new Invoice();
			if(fumigation.getContractID()!=null && fumigation.getContractID()!=0){
				invoice = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", fumigation.getContractID()).filter("rateContractServiceId", fumigation.getServiceID()).first().now();
			}
			//	Invoice invoice = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", fumigation.getCount()).first().now();
		//	if(invoice == null){
			//Invoice	invoice = new Invoice();
			else{
			ServerAppUtility serverapp = new ServerAppUtility();
			Customer cust = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count", fumigation.getcInfo().getCount()).first().now();
			SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("productName", fumigation.getNameoffumigation()).filter("status", true).first().now();
			ArrayList<SalesOrderProductLineItem> productlist = new ArrayList<SalesOrderProductLineItem>();
			SalesOrderProductLineItem salesLineItem=new SalesOrderProductLineItem();
			salesLineItem.setPrduct(product);
			/**
			 * Updated By: Viraj
			 * Date: 17-04-2019
			 * Description: changed quantity from double to string in fumigation to handle this change below code is writen
			 */
			double quantity = 0.0;
			if(fumigation.getQuantity() != null) {
				quantity = Double.parseDouble(fumigation.getQuantity());
			}
			/** Ends **/
			salesLineItem.setTotalAmount(product.getPrice() * quantity);
			salesLineItem.setProdId(product.getCount());
			salesLineItem.setProdCode(product.getProductCode());
			salesLineItem.setProdCategory(product.getProductCategory());
			salesLineItem.setProdName(product.getProductName());
			salesLineItem.setPrice(product.getPrice());
			salesLineItem.setVatTax(product.getVatTax());
			salesLineItem.setServiceTax(product.getServiceTax());
			salesLineItem.setBaseBillingAmount(product.getPrice() * quantity);
			salesLineItem.setPaymentPercent(100.0);
			salesLineItem.setBasePaymentAmount(product.getPrice() * quantity);
			productlist.add(salesLineItem);
			invoice.setContractCount(fumigation.getCount());
			invoice.setSalesOrderProductFromBilling(productlist);
			  ArrayList<PaymentTerms> arrPayTerms = new ArrayList<PaymentTerms>();
			  PaymentTerms pt = new PaymentTerms();
			  pt.setPayTermDays(0);
			  pt.setPayTermPercent(100.0d);
			  pt.setPayTermComment("Payment");
			  arrPayTerms.add(pt);
			  invoice.setArrPayTerms(arrPayTerms);
			  
			  if(cust!=null){
					if(cust.getArticleTypeDetails()!=null){  
						for(ArticleType object:cust.getArticleTypeDetails()){
							if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
								invoice.setGstinNumber(object.getArticleTypeValue());
								break;
							}
						}
					}
				}
			  PersonInfo personInfo =new PersonInfo();
			  personInfo.setCount(cust.getCount());
			  personInfo.setCellNumber(cust.getCellNumber1());
			  personInfo.setFullName(cust.getCompanyName());
			  personInfo.setPocName(cust.getFirstName());
			  invoice.setPersonInfo(personInfo);
			  invoice.setStatus(Invoice.CREATED);
			  invoice.setCompanyId(companyId);
			  invoice.setBranch(fumigation.getBranch());
			  invoice.setAccountType("AR");
			  invoice.setInvoiceType("Tax Invoice");
			  ArrayList<SalesLineItem> salesItem = new ArrayList<SalesLineItem>();
			  SalesLineItem item=new SalesLineItem();
			  item.setPrduct(product);
			  item.setQuantity(quantity);
			  item.setTotalAmount(product.getPrice() * quantity);		
			  item.setPrice(product.getPrice());
			  item.setVatTax(product.getVatTax());
			  item.setServiceTax(product.getServiceTax());
			  salesItem.add(item);

			  List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
			  List<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
			  try {
				list.addAll(serverapp.addProdTaxes(salesItem , taxList));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
				double assessValue=0;
				double tax = 0;
				for(int i=0;i<list.size();i++){
					ContractCharges taxDetails=new ContractCharges();
					taxDetails.setTaxChargeName(list.get(i).getChargeName());
					taxDetails.setTaxChargePercent(list.get(i).getChargePercent());
					assessValue=list.get(i).getAssessableAmount();//*paymentRecieved/100;
					taxDetails.setTaxChargeAssesVal(assessValue);				
					taxDetails.setIdentifyTaxCharge(list.get(i).getIndexCheck());
					tax = tax + taxDetails.getTaxChargeAssesVal()*taxDetails.getTaxChargePercent()/100;
					arrBillTax.add(taxDetails);
				}
			  invoice.setBillingTaxes(arrBillTax);
			  invoice.setTotalBillingAmount(Math.round(salesLineItem.getTotalAmount()+tax));
			  invoice.setFinalTotalAmt(Math.round(salesLineItem.getTotalAmount()));
			  invoice.setTotalAmtIncludingTax(Math.round(salesLineItem.getTotalAmount()+tax));
			  invoice.setTotalSalesAmount(Math.round(salesLineItem.getTotalAmount()+tax));
			  invoice.setTotalSalesAmount(Math.round(salesLineItem.getTotalAmount()+tax));
			  invoice.setTotalAmtExcludingTax(Math.round(salesLineItem.getTotalAmount()));
			  invoice.setNetPayable(Math.round(salesLineItem.getTotalAmount()+tax));
			  invoice.setPaymentDate(new Date());
			  invoice.setInvoiceDate(new Date());
			  invoice.setInvoiceAmount(Math.round(salesLineItem.getTotalAmount()+tax));
			  invoice.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
//				ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
//				BillingDocumentDetails billing = new BillingDocumentDetails();
//				billing.setOrderId(fumigation.getCount());
//				billing.setBillAmount(Math.round(salesLineItem.getTotalAmount()+tax));
//				billing.setBillingDate(new Date());
//				billtablearr.add(billing);
//				invoice.setArrayBillingDocument(billtablearr);

		//	}
			}
			return invoice;
		}



		/*** Date 01-06-2018 By vijay for Updating service address from contract only for non multilocation services ***/
		@Override
		public boolean updatedServiceAddress(Contract contract,boolean customerbranchservicesflag) {
			// for updating address in contract
			ofy().save().entity(contract);
			
			logger.log(Level.SEVERE,"Branch wise services flag"+customerbranchservicesflag);
			if(!customerbranchservicesflag){
				ArrayList<String> serviceStatus = new ArrayList<String>();
				serviceStatus.add("Scheduled");
				serviceStatus.add("Rescheduled");
				serviceStatus.add("Open");
				serviceStatus.add("Planned");
				List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", contract.getCompanyId()).filter("contractCount", contract.getCount()).filter("status IN", serviceStatus).list();
				logger.log(Level.SEVERE,"Service list size =="+servicelist.size());
				logger.log(Level.SEVERE,"contract.getCustomerServiceAddress() locality =="+contract.getCustomerServiceAddress().getLocality());
				for(int i=0;i<servicelist.size();i++){
					servicelist.get(i).setAddress(contract.getCustomerServiceAddress());
				}
				if(servicelist.size()!=0)
				ofy().save().entities(servicelist);
			}
			else {
				/**
				 * @author Vijay Date 22-12-2020 
				 * Des :- if contract has multilocation branch if user wants to update Service Address branch in services
				 * to updated Service Adddress as Service Branch updated the Code.s
				 */
				ArrayList<String> serviceStatus = new ArrayList<String>();
				serviceStatus.add("Scheduled");
				serviceStatus.add("Rescheduled");
				serviceStatus.add("Open");
				serviceStatus.add("Planned");
				List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", contract.getCompanyId()).filter("contractCount", contract.getCount())
											.filter("serviceBranch", "Service Address").filter("status IN", serviceStatus).list();
				logger.log(Level.SEVERE,"Multilocation Contract Service Address Services updating its Service address");
				logger.log(Level.SEVERE,"Service list size for =="+servicelist.size());
				logger.log(Level.SEVERE,"contract.getCustomerServiceAddress() locality =="+contract.getCustomerServiceAddress().getLocality());
				for(int i=0;i<servicelist.size();i++){
					servicelist.get(i).setAddress(contract.getCustomerServiceAddress());
				}
				if(servicelist.size()!=0)
				ofy().save().entities(servicelist);
			}
			
			return true;
		}
		/**
		 * ends here
		 */


		/*
		 * Name:Apeksha Gunjal
		 * Date: 29/06/2018 @16:47
		 * Note: Send Notification to Technician on EVA Pedio after assigning respective technician
		 */
		@Override
		public String sendPushNotificationToTechnician(Service service,
				long companyId) {
			// TODO Auto-generated method stub
			
			try {
				
			
				Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				logger.log(Level.SEVERE, "CustomerCallLogDetails CompanyId: "+comp.getCompanyId());
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//				String response="Service Id : "+service.getCount()+" is assigned to you!! on "
//				+sdf.format(service.getServiceDate()) + " "+service.getServiceTime();
				/**
				 * Rahul Verma changed message to a genric message as suggested by Rohan and Nitin Sir on 08 Sept 2018
				 */
				String response="Service Id : "+service.getCount()+"/"+sdf.format(service.getServiceDate())+" is assigned to you!!";// on "
//						+ + " "+service.getServiceTime();
				User user=ofy().load().type(User.class).filter("companyId", comp.getCompanyId()).filter("employeeName", service.getEmployee().trim()).first().now();
			 	List<RegisterDevice> registerDeviceList=ofy().load().type(RegisterDevice.class).filter("companyId",comp.getCompanyId()).filter("applicationName",AppConstants.IAndroid.EVA_PEDIO).filter("userName", user.getUserName()).list();//.first().now();
		for (RegisterDevice registerDevice : registerDeviceList) {
			try {
				String deviceName = registerDevice.getRegId().trim();
				logger.log(Level.SEVERE, "CustomerCallLogDetails deviceName"
						+ deviceName);
				URL url = null;
				try {
					url = new URL("https://fcm.googleapis.com/fcm/send");
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpURLConnection conn = null;
				try {
					conn = (HttpURLConnection) url.openConnection();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				conn.setDoOutput(true);
				try {
					conn.setRequestMethod("POST");
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				conn.setRequestProperty("Content-Type",
						"application/json; charset=UTF-8");
				conn.setRequestProperty("Authorization", "key="
						+ comp.getFcmServerKeyForPriora().trim());
				conn.setDoOutput(true);
				conn.setDoInput(true);

				try {
					conn.setRequestMethod("POST");
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE,
							"CustomerCallLogDetails ProtocolException ERROR::::::::::"
									+ e);
					e.printStackTrace();
				}
				OutputStream os = null;
				try {
					os = conn.getOutputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE,
							"CustomerCallLogDetails IOException ERROR::::::::::"
									+ e);
					e.printStackTrace();
				}
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os));
				// Log.d("Test", "From Get Post Method" + getPostData(values));
				try {
					writer.write(createJSONData(response, deviceName));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
					e.printStackTrace();
				}
				try {
					writer.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
					e.printStackTrace();
				}

				InputStream is = null;
				try {
					is = conn.getInputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
					e.printStackTrace();
				}
				BufferedReader br = new BufferedReader(
						new InputStreamReader(is));
				String temp;
				String data = null;
				try {
					while ((temp = br.readLine()) != null) {
						data = data + temp;
					}
					logger.log(Level.SEVERE, "data data::::::::::" + data);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
					e.printStackTrace();
				}

				logger.log(Level.SEVERE, "Data::::::::::" + data);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Exception ==" + e.getMessage());

			}
			return "";
						
		}
					
		/*
		 * Name:Apeksha Gunjal
		 * Date: 29/06/2018 @16:47
		 * Note: Send Notification to Technician on EVA Pedio after assigning respective technician
		 */
		private String createJSONData(String message, String deviceName) {
			// TODO Auto-generated method stub
			org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
			jObj1.put("message", message);

			org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
			jObj.put("data", jObj1);
			jObj.put("to", deviceName);

			String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

			return jsonString;
		}
		
		
		
		/**
		 * Date 07-07-2018 By Vijay
		 * Des :- Admin can able to cancel multile billing documents which are not invoiced
		 * Requirements :- NBHC CCPM
		 */
		
		@Override
		public boolean MultipleBillingDocsCancellation(	ArrayList<BillingDocument> billinglist,String remark) {
			List<BillingDocument> requestedBillingList = new ArrayList<BillingDocument>();
			for(int i=0;i<billinglist.size();i++){
				if(billinglist.get(i).getStatus().equals(BillingDocument.REQUESTED)){
					requestedBillingList.add(billinglist.get(i));
					billinglist.remove(i);
				}
			}
			
			for(int j=0;j<billinglist.size();j++){
				billinglist.get(j).setStatus(BillingDocument.CANCELLED);
				billinglist.get(j).setRemark(remark);
			}
			
			for(int k=0;k<requestedBillingList.size();k++){
				Approvals approval = ofy().load().type(Approvals.class).filter("companyId", billinglist.get(0).getCompanyId()).filter("Status", "Pending").first().now();
				
				approval.setStatus(BillingDocument.CANCELLED);
				ofy().save().entity(approval);
				
				requestedBillingList.get(k).setStatus(BillingDocument.CANCELLED);
				requestedBillingList.get(k).setRemark(remark);
			}
			
			ofy().save().entities(billinglist);
			ofy().save().entities(requestedBillingList);
			
			return true;
		}

		/**
		 * ends here
		 */


		/**
		 * Date 26-07-2018 By Vijay
		 * Des :- For Pepcopp Invoice edited by other than Admin roll then email must send to their email Id
		 * this is customized so email id put here as hardcoded.
		 */
		
		@Override
		public boolean sendInvoiceEditedEmail(int invoiceId, String branch,String loggedInUser, double oldNetPayable,double newNetPayable, Date oldInvoiceDate, Date newInvoiceDate, long companyId,String customerName) {
			
			logger.log(Level.SEVERE,"Sending invoice edited email after approval");
			
			ArrayList<String> toEmailList=new ArrayList<String>();
//			toEmailList.add("accounts@pecopp.com"); //Ashwini Patil Date:17-10-2022 commented hardcoded email as other clients email were going to this email id

			String fromEmail=null;
				Company comp=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				if(comp!=null){
					if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
						fromEmail=comp.getEmail();
					}
				}
				
				 DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
				String mailSub="Invoice Edited By "+loggedInUser +" Invoice Id -"+invoiceId+" Branch -"+branch;

				String mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
						+"Customer Name:"+customerName+"     Invoice Id:"+invoiceId+"     Branch:"+branch+"    User:"+ loggedInUser +" has been updated the Invoice."+"\n"+"\n"+ "The Details are as follows :"+"\n"+
						"Old Invoice Date : "+fmt.format(oldInvoiceDate)+" New Invoice Date : "+fmt.format(newInvoiceDate)+"\n"
						+"Old Invoice Net Payable : "+oldNetPayable +" New Invoice Net Payable : "+newNetPayable+"."+"\n"+"\n"+"\n"+"\n"+"\n"+"Thanks & Regards,"+"\n"+loggedInUser;
					
				Email obj=new Email();
				obj.sendEmail(mailSub, mailMsg, toEmailList, null, fromEmail);
				
				logger.log(Level.SEVERE,"Email Sent sucessfully");
				
			return true;
		}
		
		/**
		 * ends here
		 */
		/** date 18.7.2018 added by komal to create service project **/
		public void createProject(Service service){
		ServiceProject serproject = null;
		serproject = ofy().load().type(ServiceProject.class)
				.filter("companyId", service.getCompanyId())
				.filter("serviceId", service.getCount())
				.filter("serviceSrNo", service.getServiceSerialNo()).first()
				.now();
		if (serproject == null) {
			serproject = new ServiceProject();
		}
		serproject.setCompanyId(service.getCompanyId());
		serproject.setAddr(service.getAddress());
		if(service.getContractEndDate()!=null)
		serproject.setContractEndDate(service.getContractEndDate());
		if(service.getContractStartDate()!=null){
			serproject.setContractStartDate(service.getContractStartDate());
		}
		serproject.setContractId(service.getContractCount());
		serproject.setserviceDate(service.getServiceDate());
		serproject.setserviceId(service.getCount());
		serproject.setServiceSrNo(service.getServiceSerialNo());
		serproject.setProjectName("CallLog Project");
		serproject.setProjectStatus(Service.SERVICESTATUSSCHEDULE);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(serproject);
 }
		
		
		/**
		 * Date 10-09-2018 by Vijay
		 * Des :- customer call log Product inventory master Transfer to warehouse defined or not checking validation
		 */
		@Override
		public ArrayList<SuperModel> getProductInventoryDetails(ArrayList<Integer> productIdlist, long companyId) {
			
			ArrayList<SuperModel> list = new ArrayList<SuperModel>();
			List<ProductInventoryView> prodInventoryList = ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).filter("details.prodid IN", productIdlist).list();
			System.out.println("List size ==="+prodInventoryList.size());
			if(prodInventoryList.size()!=0){
				list.addAll(prodInventoryList);
				return list;
			}
			
			return null;
		}

		/**
		 * ends here
		 */
		
		/**
		 * Date : 21-09-2018 Merged By ANIL
		 * 
		 */
		
		@Override
		public ArrayList<Attendance> setAttendance(long companyId, String branch,
				String month, int empId, String projectName) {
			// TODO Auto-generated method stub
			ArrayList<Attendance> list = new ArrayList<Attendance>();
			if(empId == 0 && projectName == null){
				List<Attendance> list1 = ofy().load().type(Attendance.class).filter("companyId", companyId).filter("branch", branch).filter("month", month).list();
				list.addAll(list1);
				return list;
			}
			if(empId != 0 && projectName != null){
				List<Attendance> list2 = ofy().load().type(Attendance.class).filter("companyId", companyId).filter("branch", branch).filter("month", month).filter("projectName", projectName).filter("empId", empId).list();
				list.addAll(list2);
				return list;
			}
			if(empId != 0){
				List<Attendance> list3 = ofy().load().type(Attendance.class).filter("companyId", companyId).filter("branch", branch).filter("month", month).filter("empId", empId).list();
				list.addAll(list3);
				return list;
			}
			if(projectName != null){
				List<Attendance> list4 = ofy().load().type(Attendance.class).filter("companyId", companyId).filter("branch", branch).filter("month", month).filter("projectName", projectName).list();
				list.addAll(list4);
				return list;
			}
			return list;
		}
		
		@Override
		public void paySlipAllocationTaskQueue(
				ArrayList<EmployeeInfo> empinfoarray, String payRollPeriod,
				Date fromDate, Date toDate,String projectname) {
			// TODO Auto-generated method stub
			
			
			/**
			 * @author Vijay Date :- 20-12-2022
			 * Des :- updated code checking attendance of employee of that specific project
			 * those whose having attendace only those sending to payroll process
			 * and those whose not having the attendance sending details on email.
			 */
			
			SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			Calendar calPay = Calendar.getInstance();
			Date payrollPeriodDate=null;
			try{
				payrollPeriodDate = isoFormat.parse(payRollPeriod);
				calPay.setTime(payrollPeriodDate);
				
			}catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "empinfoarray "+empinfoarray.size());

			String monthForSalary=sdf.format(payrollPeriodDate);
			logger.log(Level.SEVERE, "monthForSalary"+monthForSalary);
			
			ArrayList<EmployeeInfo> updatedEmpinfo = new ArrayList<EmployeeInfo>();
			String projectName="";
			if(projectName!=null && !projectName.equals("")) {
				projectName = projectname;
			}
			
			List<Attendance> attenList=ofy().load().type(Attendance.class).filter("companyId",empinfoarray.get(0).getCompanyId())
								.filter("month",monthForSalary.trim()).filter("projectName", projectName).list();
			logger.log(Level.SEVERE, "attenList size"+attenList.size());
			
			if(attenList.size()!=0) {
				ArrayList<EmployeeInfo> noattendanceemplist = new ArrayList<EmployeeInfo>();
				noattendanceemplist.addAll(empinfoarray);
				logger.log(Level.SEVERE, "noattendanceemplist"+noattendanceemplist.size());

				for(EmployeeInfo emp : empinfoarray) {
					for(Attendance attendance : attenList) {
						if(emp.getCount() == attendance.getEmpId()) {
							updatedEmpinfo.add(emp);
							noattendanceemplist.remove(emp);
						}
					}
				}
				
				logger.log(Level.SEVERE, "final no attendanceemplist"+noattendanceemplist.size());
				if(noattendanceemplist.size()!=0) {
					SendEmailforNoAttendaceofEmployeelist(noattendanceemplist,payRollPeriod,noattendanceemplist.get(0).getCompanyId());
				}

			}
			else {
				updatedEmpinfo.addAll(empinfoarray);
			}
			
			/**
			 * @author Vijay Date:- 19-12-2022 
			 * Des :- When employee size is greater than 50 then the list is splitting with 50 and calling the task queue
			 * SUnrize getting issue due to task queue time out so updated the code with 50 employee size and calling task queue
			 */
			logger.log(Level.SEVERE, "updatedEmpinfo "+updatedEmpinfo.size());
			
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlipAllocationProcess", "EnablePayrollProcess", updatedEmpinfo.get(0).getCompanyId())) {
				
			
			if(updatedEmpinfo.size()>AppConstants.payrollProcessTaskQueueLimit) {
				logger.log(Level.SEVERE, "empinfoarray size "+updatedEmpinfo.size());

				ArrayList<Integer> empIdList=new ArrayList<Integer>();

				for(int i=0; i<updatedEmpinfo.size(); i++) {
					empIdList.add(updatedEmpinfo.get(i).getEmpCount());
					
					if(empIdList.size()==AppConstants.payrollProcessTaskQueueLimit) {
						String str = payRollprocessThroughTaskQueue(empIdList,updatedEmpinfo.get(0).getCompanyId(),payRollPeriod);
						logger.log(Level.SEVERE, "str "+str);
						empIdList = new ArrayList<Integer>();
						try {
							logger.log(Level.SEVERE, "empIdList size "+empIdList.size());
						} catch (Exception e) {
						}
					}
				}
				logger.log(Level.SEVERE, "remining empIdList size "+empIdList.size());

				String strresponse = payRollprocessThroughTaskQueue(empIdList,updatedEmpinfo.get(0).getCompanyId(),payRollPeriod);
				logger.log(Level.SEVERE, "strresponse "+strresponse);

			}
			else {
				
				logger.log(Level.SEVERE, "else block"+updatedEmpinfo.size());

				ArrayList<Integer> empIdList=new ArrayList<Integer>();
				for(EmployeeInfo info : updatedEmpinfo){
					empIdList.add(info.getEmpCount());
				}
				
				String strresponse = payRollprocessThroughTaskQueue(empIdList,updatedEmpinfo.get(0).getCompanyId(),payRollPeriod);
				logger.log(Level.SEVERE, "strresponse "+strresponse);

				
//				Gson gson =  new Gson();
////				String empArrayInStr = gson.toJson(empinfoarray);
//				String empArrayInStr = gson.toJson(empIdList);
//				
////				for(EmployeeInfo info : empinfoarray){
//				
//				Queue queue = QueueFactory.getQueue("documentCancellation-queue");	
////				String empDetail = gson.toJson(info, EmployeeInfo.class);
//				logger.log(Level.SEVERE,"EmpInfo object to string -- "+empArrayInStr);
//				String taskName="ProcessPayRoll"+"$"+empinfoarray.get(0).getCompanyId()+"$"+empArrayInStr;
//				queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName).param("PayRollPeriod",payRollPeriod));
//				
////				}
				
			}
			
//			}
		
		}
		
		



		private String payRollprocessThroughTaskQueue(ArrayList<Integer> empIdList, Long companyId, String payRollPeriod) {
			
			logger.log(Level.SEVERE, "empIdList size == "+empIdList.size());

			Gson gson =  new Gson();
//			String empArrayInStr = gson.toJson(empinfoarray);
			String empArrayInStr = gson.toJson(empIdList);
			
//			for(EmployeeInfo info : empinfoarray){
			
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");	
//			String empDetail = gson.toJson(info, EmployeeInfo.class);
			logger.log(Level.SEVERE,"EmpInfo object to string -- "+empArrayInStr);
			String taskName="ProcessPayRoll"+"$"+companyId+"$"+empArrayInStr;
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName).param("PayRollPeriod",payRollPeriod));
			
//			}
			return "payrol process task queue called";
		}



		@Override
		public String updateEmployeeInfoStatus(long companyId,String action,int employeeId,String payrollPeriod,String project,String branch,String roleName) {
			
			/**
			 * @author Anil , Date : 15-11-2019
			 */
			if(action.equalsIgnoreCase("Delete Cancelled Services")){
				Queue queue = QueueFactory.getQueue("UpdateServices-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
						.param("OperationName", "Delete Cancelled Services")
						.param("companyId", companyId+""));
				logger.log(Level.SEVERE, "Deleting..... Cancelled Servicess !!!");
				return "Delete process started.";
			}
			
			DataMigrationTaskQueueImpl impl=new DataMigrationTaskQueueImpl();
			
			SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MMM");
			isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			Calendar c = Calendar.getInstance(); 
			Date startDate=null;
			Date endDate=null;
			try {
				Date d=isoFormat.parse(payrollPeriod);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				startDate=new Date(d.getTime());
				endDate=new Date(d.getTime());
				
				startDate=DateUtility.getStartDateofMonth(d);
				endDate=DateUtility.getEndDateofMonth(d);
				
				logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
				logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
			}catch(Exception e){
				
			}
			
			String msg="";
			
			/**
			 * @author Anil
			 * @since 08-12-2020
			 * If employee id is other than zero then validate  that id from employee master
			 */
			
			if(employeeId!=0&&(action.equalsIgnoreCase("Delete Attendance")||action.equalsIgnoreCase("Delete Payroll"))){
				Employee employee =ofy().load().type(Employee.class).filter("companyId", companyId).filter("count", employeeId).first().now();
				if(employee==null){
					return "Employee id does not exist - "+employeeId;
				}
			}
			
			if(action.equalsIgnoreCase("Update")){
				List<Employee> employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).list();
				/** date 10.8.2018 added by komal for to update checkList Data **/
				List<EmployeeCheckListType> list = ofy().load().type(EmployeeCheckListType.class).filter("companyId", companyId).list();
				for(Employee e : employeeList){
					if(e.getEmployeeType()!=null && !(e.getEmployeeType().equals(""))){
						for(EmployeeCheckListType type : list){
							if(type.getEmployeeType().equalsIgnoreCase(e.getEmployeeType())){
								e.setCheckListType(type.getCheckListType());
							}
						}
					}
					if(e.getAadharNumber()==0){
						for (int i = 0; i < e.getArticleTypeDetails().size(); i++) {
							if (e.getArticleTypeDetails().get(i).getArticleTypeName().trim().toUpperCase().contains("AADHAR")) {
								String aadhar = e.getArticleTypeDetails().get(i).getArticleTypeValue().trim().replace(" ", "");
								try{
									long a=Long.parseLong(aadhar);
									e.setAadharNumber(a);
								}catch(Exception exp){
									
								}
							}
						}
					}
					/**
					 * Date : 24-10-2018 By ANIL
					 * to update date of retirement 
					 */
					int age=0;
					ArrayList<String> processNameList=new ArrayList<String>();
					processNameList.add("UpdateDateOfRetirement");
					
					List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName IN", processNameList).filter("configStatus", true).list();
					if(processConfigList!=null){
						for(ProcessConfiguration processConf:processConfigList){
							for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
								if(processConf.getProcessName().equalsIgnoreCase("UpdateDateOfRetirement")&&ptDetails.isStatus()==true){
									age=Integer.parseInt(ptDetails.getProcessType().trim());
								}
							}
						}
					}
					logger.log(Level.SEVERE,"Retirement Age : "+age);
					
					if(age!=0){
						if(e.getDateOfRetirement()==null){
							Calendar cal = Calendar.getInstance(); 
//							Date frmDt=isoFormat.parse(bean.getFromDate());
							Date retirementDate=DateUtility.getDateWithTimeZone("IST", e.getDob());
							cal.setTime(retirementDate);
							cal.add(Calendar.YEAR, age);
							retirementDate=cal.getTime();
							e.setDateOfRetirement(retirementDate);
						}
					}
					/**
					 * End
					 */
					
				} 
				/** end komal
				 * 
				 */
				
				
				if(employeeList!=null&&employeeList.size()!=0){
					ofy().save().entities(employeeList).now();
					msg="Employee master update successful.";
				}else{
					msg="No employee found.";
				}
			}else if(action.equalsIgnoreCase("Delete Attendance")){
				
				SimpleDateFormat attFormat = new SimpleDateFormat("MMM-yyyy");
				attFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				String attPeriod=attFormat.format(startDate);
				/**
				 * Date : 14-09-2018 By ANIL
				 * 
				 * @author Anil
				 * @since 26-11-2020
				 * updating payroll load part, adding restriction to attendance delete if payroll is already created
				 * raised by Rahul for sunrise
				 */
				
//				PaySlip paySlip=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).filter("salaryPeriod", payrollPeriod).filter("isFreeze", true).first().now();
//				if(!roleName.equalsIgnoreCase("Admin")&&paySlip!=null){
//					return "Can not delete as payroll is freezed against attendance.";
//				}
				
				/**
				 * @author Anil @since 05-03-2021
				 * If project is selected alongwith employee id then consider both for deletion of attendance and payroll
				 * earlier it was not happening
				 * issue raised by Rahul Tiwari for Sunrise
				 */
				List<PaySlip> paySlipList=null;
				if(employeeId!=0&&project!=null&&!project.equals("")){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).filter("projectName", project).filter("salaryPeriod", payrollPeriod).list();
				}else if(employeeId!=0){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).filter("salaryPeriod", payrollPeriod).list();
				}else{
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", payrollPeriod).filter("projectName", project).list();
				}
				
				if(paySlipList!=null){
					boolean payrollFlag=false;
					String errorMsg=null;
					ArrayList<AttandanceError>errorList=new ArrayList<AttandanceError>();
					for(PaySlip paySlip:paySlipList){
						payrollFlag=true;
						if(paySlip.isFreeze()==true&&!roleName.equalsIgnoreCase("Admin")){
							AttandanceError error=new AttandanceError();
							error.setEmpId(paySlip.getEmpid());
							error.setEmpName(paySlip.getEmployeeName());
							error.setProjectName(paySlip.getProjectName());
							error.setRemark("Can not delete as payroll is freezed against attendance.");
							errorMsg="ERROR";
							errorList.add(error);
//							return "Can not delete as payroll is freezed against attendance.";
						}else{
							AttandanceError error=new AttandanceError();
							error.setEmpId(paySlip.getEmpid());
							error.setEmpName(paySlip.getEmployeeName());
							error.setProjectName(paySlip.getProjectName());
							error.setRemark("Payroll has been already generated against attendance. Please delete payroll first.");
							errorMsg="ERROR";
							errorList.add(error);
//							return "Payroll is generated against attendance. Please delete payroll first.";
						}
					}
					
					if(payrollFlag){
						if(errorMsg!=null){
							CsvWriter.attendanceErrorList=errorList;
							return errorMsg;
						}
					}
					
					
				}
				
				
				
				/**
				 * Date : 15-12-2018 BY ANIL
				 * commented delete statement , added new method which will update employee leave balance and delete records as well
				 */
				/**
				 * @author Anil @since 05-03-2021
				 * If project is selected alongwith employee id then consider both for deletion of attendance and payroll
				 * earlier it was not happening
				 * issue raised by Rahul Tiwari for Sunrise
				 */
				List<Attendance> attendanceList=null;
				if(employeeId!=0&&project!=null&&!project.equals("")){
					attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("empId", employeeId).filter("projectName", project).filter("month", attPeriod).list();
				}else if(employeeId!=0){
					attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("empId", employeeId).filter("month", attPeriod).list();
				}else{
					attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("projectName", project).filter("month", attPeriod).list();
				}
				
				/**
				 * @author ANIL
				 * @since 29-06-2020
				 */
				if(attendanceList.size()!=0){
					if(attendanceList.get(0).getStartDate()!=null&&attendanceList.get(0).getEndDate()!=null){
						startDate=attendanceList.get(0).getStartDate();
						endDate=attendanceList.get(0).getEndDate();
						
						logger.log(Level.SEVERE,"UPDATED START DATE --- ::: "+startDate);
						logger.log(Level.SEVERE,"UPDATED END DATE  --- ::: "+endDate);
					}
				}
				
				  
				 ArrayList<Integer> empId=null;
				if(attendanceList!=null&&attendanceList.size()!=0){
					 HashSet<Integer> empset=new HashSet<>();
				       for(Attendance attendace:attendanceList){
				    	   empset.add(attendace.getEmpId());
				       }
				       empId=new ArrayList<Integer>(empset);
					logger.log(Level.SEVERE,"attendanceList "+attendanceList.size());
//					ofy().delete().entities(attendanceList).now();
				}else{
					msg="Attendance not found."+"\n";
				}
				
				List<EmployeeOvertime> otList=null;
				if(employeeId!=0){
					otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",companyId).filter("emplId", employeeId).filter("otDate >=",startDate).filter("otDate <=",endDate).list();
				}else{
					otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",companyId).filter("emplId IN", empId).filter("otDate >=",startDate).filter("otDate <=",endDate).list();	
				}
				
				if(otList!=null&&otList.size()!=0){
					logger.log(Level.SEVERE,"otList "+otList.size());
//					ofy().delete().entities(otList).now();
				}
				
				List<EmployeeLeave> leaves=null;
				if(employeeId!=0){
					leaves=ofy().load().type(EmployeeLeave.class).filter("companyId",companyId).filter("emplId", employeeId).filter("leaveDate >=",startDate).filter("leaveDate <=",endDate).list();
				}else{
					leaves=ofy().load().type(EmployeeLeave.class).filter("companyId",companyId).filter("emplId IN", empId).filter("leaveDate >=",startDate).filter("leaveDate <=",endDate).list();	
				}
				
				if(leaves!=null&&leaves.size()!=0){
					logger.log(Level.SEVERE,"leaves "+leaves.size());
//					ofy().delete().entities(leaves).now();
				}
				
				List<EmployeeInfo> empInfoList=null;
				if(employeeId!=0){
					empInfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount", employeeId).list();	
				}else{
					empInfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empId).list();	
				}
				
				if(empInfoList==null||empInfoList.size()==0){
					msg="No employee found.";
				}
				
				ArrayList<Attendance> attenList=new ArrayList<Attendance>();
				ArrayList<EmployeeLeave> empLeaveList=new ArrayList<EmployeeLeave>();
				ArrayList<EmployeeOvertime> empOtList=new ArrayList<EmployeeOvertime>();
				
				if(attendanceList!=null&&attendanceList.size()!=0){
					attenList=new ArrayList<Attendance>(attendanceList);
				}
				if(leaves!=null&&leaves.size()!=0){
					/**
					 * @author Anil @since 05-03-2021
					 * If project is selected alongwith employee id then consider both for deletion of attendance and payroll
					 * earlier it was not happening
					 * issue raised by Rahul Tiwari for Sunrise
					 */
					if(project!=null&&!project.equals("")){
						empLeaveList=new ArrayList<EmployeeLeave>();
						for(EmployeeLeave leave:leaves){
							if(leave.getProjectName().equals(project)){
								empLeaveList.add(leave);
							}
						}
					}else{
						empLeaveList=new ArrayList<EmployeeLeave>(leaves);
					}
				}
				if(otList!=null&&otList.size()!=0){
					/**
					 * @author Anil @since 05-03-2021
					 * If project is selected alongwith employee id then consider both for deletion of attendance and payroll
					 * earlier it was not happening
					 * issue raised by Rahul Tiwari for Sunrise
					 */
					if(project!=null&&!project.equals("")){
						empOtList=new ArrayList<EmployeeOvertime>();
						for(EmployeeOvertime ot:otList){
							if(ot.getProjectName().equals(project)){
								empOtList.add(ot);
							}
						}
					}else{
						empOtList=new ArrayList<EmployeeOvertime>(otList);
					}
				}
				
//				DataMigrationTaskQueueImpl impl=new DataMigrationTaskQueueImpl();
				
				impl.updateLeaveBalAndRemoveOldRecords(attenList, empLeaveList, empOtList, empInfoList);
				
				/**
				 * End
				 */
				logger.log(Level.SEVERE,"attenList.size()="+attenList.size()+" ,empLeaveList.size()= "+empLeaveList.size()+" , empOtList.size()= "+empOtList.size()+" , empInfoList.size()="+empInfoList.size());
				//Ashwini Patil Date:25-05-2022 Description: even though there is no attendance or employee, below msg was getting populated so added one condition  
				if(attenList.size()>0||empLeaveList.size()>0||empOtList.size()>0) {
					msg="Attendance delete successful.";}
				return msg;
				
			}else if(action.equalsIgnoreCase("Delete Payroll")){
				
//				boolean empFlag=false;
				List<PaySlip> paySlipList=new ArrayList<PaySlip>();
//				if(project!=null&&branch!=null){
//					empFlag=true;
//					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).filter("salaryPeriod", payrollPeriod).list();
//				}else{
//					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).filter("salaryPeriod", payrollPeriod).list();
//				}
				/**
				 * Date : 14-09-2018 By ANIL
				 */
				/**
				 * @author Anil @since 05-03-2021
				 * If project is selected alongwith employee id then consider both for deletion of attendance and payroll
				 * earlier it was not happening
				 * issue raised by Rahul Tiwari for Sunrise
				 */
				if (roleName.trim().equalsIgnoreCase("Admin")) {
					if (employeeId != 0&&project!=null&&!project.equals("")) {
						paySlipList = ofy().load().type(PaySlip.class)
								.filter("companyId", companyId)
								.filter("empid", employeeId)
								.filter("projectName",project)
								.filter("salaryPeriod", payrollPeriod).list();
					}else if (employeeId != 0) {
						paySlipList = ofy().load().type(PaySlip.class)
								.filter("companyId", companyId)
								.filter("empid", employeeId)
								.filter("salaryPeriod", payrollPeriod).list();
					} else {
						paySlipList = ofy().load().type(PaySlip.class)
								.filter("companyId", companyId)
								.filter("projectName", project)
								.filter("salaryPeriod", payrollPeriod).list();
					}
				} else {
					if (employeeId != 0&&project!=null&&!project.equals("")) {
						paySlipList = ofy().load().type(PaySlip.class)
								.filter("companyId", companyId)
								.filter("empid", employeeId)
								.filter("projectName",project)
								.filter("salaryPeriod", payrollPeriod)
								.filter("isFreeze", false).list();
					}else if (employeeId != 0) {
						paySlipList = ofy().load().type(PaySlip.class)
								.filter("companyId", companyId)
								.filter("empid", employeeId)
								.filter("salaryPeriod", payrollPeriod)
								.filter("isFreeze", false).list();
					} else {
						paySlipList = ofy().load().type(PaySlip.class)
								.filter("companyId", companyId)
								.filter("projectName",project)
								.filter("salaryPeriod", payrollPeriod)
								.filter("isFreeze", false).list();
					}
				}
				
				/**
				 * Date  : 15-12-2018 By ANIL
				 * updating monthly leaves which will be allocated to employee at the time of payroll
				 */
				
				updateMonthlyAllocatedLeaves(paySlipList);
				
				/**
				 * End
				 */
				ArrayList<Integer>empList=null;
				if(paySlipList!=null&&paySlipList.size()!=0){
					HashSet<Integer>empSet=new HashSet<>();
					for(PaySlip payslip:paySlipList){
						empSet.add(payslip.getEmpid());
					}
					empList=new ArrayList<>(empSet);
					
					/**
					 * @author Anil , Date :05-03-2020
					 * Deleting Tds history also
					 */
					updateTDSCalculation(paySlipList, empList);
					
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
					ofy().delete().entities(paySlipList).now();
				}else{
					return "Payroll is either freezed or not generated.";
				}
				
				List<PaySlipRecord>record=null;
				List<EmployeeInfo> infoList=null;
				
				if(employeeId!=0){
					infoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount", employeeId).list();
				}else{
					infoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empList).list();	
				}
				
				for (EmployeeInfo empInfo : infoList) {
					Key<EmployeeInfo> empkeys = Key.create(empInfo);
	
					if (empkeys != null) {
						record = ofy().load().type(PaySlipRecord.class)
								.filter("employeeKey", empkeys)
								.filter("salaryPeriod", payrollPeriod).list();
					} 
	
					if (record != null && record.size() != 0) {
						logger.log(Level.SEVERE, "PaySlipRecord " + record.size());
						ofy().delete().entities(record).now();
					}

				}
				
				
				List<CompanyPayrollRecord> compRecordList;
				if(employeeId!=0&&project!=null&&!project.equals("")){
					compRecordList=ofy().load().type(CompanyPayrollRecord.class).filter("companyId", companyId).filter("empId", employeeId).filter("projectName", project).filter("payrollMonth", payrollPeriod).list();	
				}else if(employeeId!=0){
					compRecordList=ofy().load().type(CompanyPayrollRecord.class).filter("companyId", companyId).filter("empId", employeeId).filter("payrollMonth", payrollPeriod).list();	
				}else{
					compRecordList=ofy().load().type(CompanyPayrollRecord.class).filter("companyId", companyId).filter("empId IN", empList).filter("payrollMonth", payrollPeriod).list();		
				}
				
				if(compRecordList!=null&&compRecordList.size()!=0){
					logger.log(Level.SEVERE,"CompanyPayrollRecord "+compRecordList.size());
					ofy().delete().entities(compRecordList).now();
				}
				msg="Payroll delete successful.";
				
				return msg;
				
			}else if(action.equalsIgnoreCase("Update Branch")){
				logger.log(Level.SEVERE,"Update Branch ");
				
				/***
				 * commented this code which is used to update payslip
				 * Date : 15-12-2018 BY ANIL
				 * 
//				List<String> branchList=new ArrayList<String>();
//				branchList.add("2018-Jul");
//				branchList.add("2018-Jun");
				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).list();
				
//				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("branch IN", branchList).list();
				
				if(paySlipList!=null&&paySlipList.size()!=0){
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
					
//					HashSet<Integer> empIdHs=new HashSet<Integer>();
//					for(PaySlip ps:paySlipList){
//						empIdHs.add(ps.getEmpid());
//					}
//					ArrayList<Integer> empIdList=new ArrayList<Integer>(empIdHs);
//					if(empIdList.size()!=0){
//					
//						List<EmployeeInfo> empinfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empIdList).list();
//						if(empinfoList!=null&&empinfoList.size()!=0){
//							logger.log(Level.SEVERE,"EMP LOADED");
//							for(PaySlip ps:paySlipList){
//								
//								ps.setBranch(getBranchFormEmployee(ps.getEmpid(),empinfoList));	
//								
//							}
//							msg="Branch update successful.";
//						}
//					}
					
					ofy().save().entities(paySlipList).now();
				}
				
				**/
				
				/**
				 * Date : 29-09-2018 BY ANIL
				 * Temp code for managing local data
				 */
				
//				List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", companyId).limit(1000).list();
//				if(serviceList!=null&&serviceList.size()!=0){
//					ofy().delete().entities(serviceList).now();
//				}
				
				/**
				 * End
				 */
				
				
				/**
				 * @author Anil , Date : 11-09-2019
				 * Commented programme to update created by name
				 * 
				 */
				
//				/** date 25.10.2018 added by komal to change name of of nitin to someone else name**/
//				String createdBy = "NITIN P. KSHIRSAGAR";   //"Dipak V Hole";
//				String newCreatedByDirect = "Kavita Sachin Gaokar";
//				String newCreatedByIndirect = "Baljinder Kaur Sokhi";
//				
//				/**
//				 * Date : 29-11-2018 BY ANIL
//				 * Removing Roshan dummy name with kavita and baljinder
//				 */
//				List<String> createdByList=new  ArrayList<String>();
//				createdByList.add(createdBy);
//				createdByList.add("ROSHAN DUMMY");
//				
//				List<Employee> employeeList = ofy().load().type(Employee.class).filter("companyId", companyId).filter("createdBy IN", createdByList).list();
//				List<Employee> list = new ArrayList<Employee>();
//				for(Employee emp : employeeList){
//					if(emp.getNumberRange() != null){
//						if(emp.getNumberRange().equalsIgnoreCase("Direct")){
//							emp.setCreatedBy(newCreatedByDirect);
//						}else if(emp.getNumberRange().equalsIgnoreCase("Indirect")){
//							emp.setCreatedBy(newCreatedByIndirect);
//						}
//					}
//					list.add(emp);
//				}
//				ofy().save().entities(employeeList);
				
				/**
				 * 
				 */
				
				/**
				 * @author Anil , Date : 11-09-2019
				 * Writing a programme to update pf , esic and uan number of employee in payslip
				 */
				ArrayList<String> payRollMonthList=new ArrayList<String>();
				payRollMonthList.add("2019-Aug");
				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod IN", payRollMonthList).list();
				
				ArrayList<Integer> empIdList=new ArrayList<Integer>();
				List<Employee> employeeList=new ArrayList<Employee>();
				if(paySlipList!=null&&paySlipList.size()!=0){
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
					for(PaySlip pay:paySlipList){
						empIdList.add(pay.getEmpid());
					}
					if(empIdList.size()!=0){
						employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("count IN", empIdList).list();
					}
					
					for(PaySlip pay:paySlipList){
						Employee employee=getEmployeeDetails(pay.getEmpid(),employeeList);
						if(employee!=null){
							if(pay.getPfNumber()==null || pay.getPfNumber().equals("")){
								pay.setPfNumber(employee.getPPFNaumber());
							}
							if(pay.getEsicNumber()==null || pay.getEsicNumber().equals("")){
								pay.setEsicNumber(employee.getEmployeeESICcode());
							}
							if(pay.getUanNo()==null || pay.getUanNo().equals("")){
								pay.setUanNo(employee.getUANno());
							}
							if(pay.getAccountNumber()==null||pay.getAccountNumber().equals("")){
								pay.setAccountNumber(employee.getEmployeeBankAccountNo());
							}
							
							if(pay.getIfscNumber()==null||pay.getIfscNumber().equals("")){
								pay.setIfscNumber(employee.getIfscCode());
							}
							if(pay.getBankBranch()==null||pay.getBankBranch().equals("")){
								pay.setBankBranch(employee.getBankBranch());
							}
							if(pay.getBankName()==null||pay.getBankName().equals("")){
								pay.setBankName(employee.getEmployeeBankName());
							}
							
//							accountNumber = employee.getEmployeeBankAccountNo();
//							ifscNumber = employee.getIfscCode();
//							bankBranch =employee.getBankBranch();
//							bankName =employee.getEmployeeBankName(); 
//							branch=employee.getBranchName();
							
							String aadhar="";
							for (int i = 0; i < employee.getArticleTypeDetails().size(); i++) {
								if (employee.getArticleTypeDetails().get(i).getArticleTypeName().trim().toUpperCase().contains("AADHAR")) {
									aadhar = employee.getArticleTypeDetails().get(i).getArticleTypeValue();
								}
							}
							if(employee.getAadharNumber()!=0){
								aadhar=employee.getAadharNumber()+"";
							}
//							this.aadharNo=aadhar;
							
							if(pay.getAadharNo()==null||pay.getAadharNo().equals("")){
								pay.setAadharNo(aadhar);
							}
						}
					}
					ofy().save().entities(paySlipList).now();
				}
				
				
			}else if(action.equalsIgnoreCase("Update PT")){
				/**
				 * Commented this code on 15-12-2018 BY ANIL
				 * this code is used to update PT at banglore branch
				 */
//				ArrayList<String> branchList=new ArrayList<String>();
//				branchList.add("Banglore");
//				
//				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("branch IN", branchList).list();
//				if(paySlipList!=null&&paySlipList.size()!=0){
//					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
//					
//					for(PaySlip ps:paySlipList){
//						double ptAmount=0;
//						if(ps.getGrossEarningWithoutOT()<15000){
//							for(int i=0;i<ps.getDeductionList().size();i++){
//								if(ps.getDeductionList().get(i).getShortName().equalsIgnoreCase("PT")
//										&&ps.getDeductionList().get(i).getActualAmount()!=0){
//									ptAmount=ps.getDeductionList().get(i).getActualAmount();
//									ps.getDeductionList().remove(i);
//									break;
//								}
//							}
//							if(ptAmount!=0){
//								double totalDeduction=ps.getTotalDeduction();
//								ps.setTotalDeduction(totalDeduction-ptAmount);
//								
//								double netEarningWithoutOt=ps.getNetEarningWithoutOT();
//								double netEarningOt=ps.getNetEarning();
//								
//								netEarningOt=netEarningOt+Math.round(ptAmount);
//								netEarningWithoutOt=netEarningWithoutOt+Math.round(ptAmount);
//								
//								ps.setNetEarning(netEarningOt);
//								ps.setNetEarningWithoutOT(netEarningWithoutOt);
//								
//							}
//							
//						}
//					}
//					ofy().save().entities(paySlipList).now();
//				}
				
				/**
				 * @author Anil ,  Date : 07-08-2019
				 */
				ArrayList<String> payRollMonthList=new ArrayList<String>();
				payRollMonthList.add("2019-Jul");
				payRollMonthList.add("2019-Aug");
				payRollMonthList.add("2019-Jun");
				Esic masterEsic=ofy().load().type(Esic.class).filter("companyId", companyId).filter("status", true).first().now();
				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod IN", payRollMonthList).list();
				
				if(paySlipList!=null&&paySlipList.size()!=0){
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
					if(masterEsic!=null){
						for(PaySlip ps:paySlipList){
							for(CtcComponent comp:ps.getDeductionList()){
								if(comp.getShortName().equalsIgnoreCase("ESIC")){
									double esicBaseAmt=ps.calculateEsicAmt(masterEsic,ps,true,true,false,true);
									comp.setBaseAmount(esicBaseAmt);
								}
							}
						}
						
						ofy().save().entities(paySlipList).now();
					}
				}
				
				
				
				
			}else if(action.equalsIgnoreCase("Update Advance")){
				/**
				 * Commented this code on 15-12-2018 By ANIL
				 * this code is used to update the advance in pune brach for sasha
				 */
//				ArrayList<String> branchList=new ArrayList<String>();
//				branchList.add("Pune");
////				2018-Jul
//				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", "2018-Jul").filter("branch IN", branchList).list();
//				if(paySlipList!=null&&paySlipList.size()!=0){
//					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
//					
//					for(PaySlip ps:paySlipList){
//						double advAmount=0;
////						if(ps.getGrossEarningWithoutOT()<15000){
//							for(int i=0;i<ps.getDeductionList().size();i++){
//								if(ps.getDeductionList().get(i).getShortName().equalsIgnoreCase("Advance")
//										&&ps.getDeductionList().get(i).getActualAmount()!=0){
//									advAmount=ps.getDeductionList().get(i).getActualAmount();
//									ps.getDeductionList().remove(i);
//									break;
//								}
//							}
//							if(advAmount!=0){
//								double totalDeduction=ps.getTotalDeduction();
//								ps.setTotalDeduction(totalDeduction-advAmount);
//								
//								double netEarningWithoutOt=ps.getNetEarningWithoutOT();
//								double netEarningOt=ps.getNetEarning();
//								
//								netEarningOt=netEarningOt+Math.round(advAmount);
//								netEarningWithoutOt=netEarningWithoutOt+Math.round(advAmount);
//								
//								ps.setNetEarning(netEarningOt);
//								ps.setNetEarningWithoutOT(netEarningWithoutOt);
//								
//							}
//							
////						}
//					}
//					ofy().save().entities(paySlipList).now();
//				}
				
				/**
				 * @author Anil ,  Date : 08-08-2019
				 */
				ArrayList<String> payRollMonthList=new ArrayList<String>();
				payRollMonthList.add("2019-Jul");
				payRollMonthList.add("2019-Jun");
//				Esic masterEsic=ofy().load().type(Esic.class).filter("companyId", companyId).filter("status", true).first().now();
				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod IN", payRollMonthList).list();
				
				if(paySlipList!=null&&paySlipList.size()!=0){
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
//					if(masterEsic!=null){
						for(PaySlip ps:paySlipList){
							for(CtcComponent comp:ps.getDeductionList()){
								if(comp.getShortName().equalsIgnoreCase("PF")){
									double pfBaseAmt=getPfBaseAmount(ps,comp);
									comp.setBaseAmount(pfBaseAmt);
								}
							}
						}
						ofy().save().entities(paySlipList).now();
//					}
				}
				
				
			}else if(action.equalsIgnoreCase("Update Attendance and Payroll")){
				
				/**
				 * Date : 14-09-2019 BY ANIL
				 */
				if(!roleName.trim().equalsIgnoreCase("Admin")){
					return "This option is available for admin user only.";
				}
				/**
				 * End
				 */
				
				SimpleDateFormat attFormat = new SimpleDateFormat("MMM-yyyy");
				attFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				String attPeriod=attFormat.format(startDate);
				
				List<Attendance> attendanceList=new ArrayList<Attendance>();
				attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("projectName", project).filter("branch", branch).filter("month", attPeriod).list();
				
				HashSet<Integer> empHs=new HashSet<Integer>();
				
				/**
				 * @author Anil, Date : 07-12-2019
				 * Commented log as printing/calling size method of list causes concurrent exception
				 */
				if(attendanceList!=null&&attendanceList.size()!=0){
//					logger.log(Level.SEVERE,"attendanceList "+attendanceList.size());
					/**
					 * @author ANIL
					 * @since 29-06-2020
					 */
					if(attendanceList.get(0).getStartDate()!=null&&attendanceList.get(0).getEndDate()!=null){
						startDate=attendanceList.get(0).getStartDate();
						endDate=attendanceList.get(0).getEndDate();
						
						logger.log(Level.SEVERE,"UPDATED START DATE --- ::: "+startDate);
						logger.log(Level.SEVERE,"UPDATED END DATE  --- ::: "+endDate);
					}
					
					for(Attendance atten:attendanceList){
						empHs.add(atten.getEmpId());
					}
				}else{
					msg="No Attendance found ,please delete data one by one.";
					return msg;
				}
				
				ArrayList<Integer> empIdList=new ArrayList<Integer>(empHs);
				List<EmployeeOvertime> otList=new ArrayList<EmployeeOvertime>();
				otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",companyId).filter("emplId IN", empIdList).filter("otDate >=",startDate).filter("otDate <=",endDate).list();
				
					if(otList!=null&&otList.size()!=0){
					logger.log(Level.SEVERE,"otList "+otList.size());
//					ofy().delete().entities(otList).now();
				}
				
				List<EmployeeLeave> leaves=new ArrayList<EmployeeLeave>();
				leaves=ofy().load().type(EmployeeLeave.class).filter("companyId",companyId).filter("emplId IN", empIdList).filter("leaveDate >=",startDate).filter("leaveDate <=",endDate).list();
				if(leaves!=null&&leaves.size()!=0){
					logger.log(Level.SEVERE,"leaves "+leaves.size());
//					ofy().delete().entities(leaves).now();
				}
				
				
				List<PaySlip> paySlipList=new ArrayList<PaySlip>();
				paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid IN", empIdList).filter("salaryPeriod", payrollPeriod).list();
				if(paySlipList!=null&&paySlipList.size()!=0){
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
					/**
					 * 
					 */
					
					updateMonthlyAllocatedLeaves(paySlipList);
					/**
					 * 
					 */
					/**
					 * @author Anil , Date : 05-03-2020
					 * Deleting tds history
					 */
					updateTDSCalculation(paySlipList, empIdList);
					
					ofy().delete().entities(paySlipList).now();
				}
				
				List<CompanyPayrollRecord> compRecordList=ofy().load().type(CompanyPayrollRecord.class).filter("companyId", companyId).filter("empId IN", empIdList).filter("payrollMonth", payrollPeriod).list();
				if(compRecordList!=null&&compRecordList.size()!=0){
					logger.log(Level.SEVERE,"CompanyPayrollRecord "+compRecordList.size());
					ofy().delete().entities(compRecordList).now();
				}
				
				List<EmployeeInfo> infoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empIdList).list();
				for(EmployeeInfo info:infoList){
					Key<EmployeeInfo>empkeys=Key.create(info);
					List<PaySlipRecord>record=ofy().load().type(PaySlipRecord.class).filter("employeeKey",empkeys).filter("salaryPeriod",payrollPeriod).list();
					if(record!=null&&record.size()!=0){
						logger.log(Level.SEVERE,"PaySlipRecord "+record.size());
						ofy().delete().entities(record).now();
					}
				}
				
//				ofy().delete().entities(attendanceList).now();
				
				/**
				 * Date : 15-12-2018 By ANIL
				 */
//				ArrayList<Attendance> attenList=new ArrayList<Attendance>();
//				ArrayList<EmployeeLeave> empLeaveList=new ArrayList<EmployeeLeave>();
//				ArrayList<EmployeeOvertime> empOtList=new ArrayList<EmployeeOvertime>();
				/**
				 * @author Anil, Date : 07-12-2019
				 * Commented log as printing/calling size method of list causes concurrent exception
				 */
//				if(attendanceList!=null){
//					attenList=new ArrayList<Attendance>(attendanceList);
//				}
//				if(leaves!=null&&leaves.size()!=0){
//					empLeaveList=new ArrayList<EmployeeLeave>(leaves);
//				}
//				if(otList!=null&&otList.size()!=0){
//					empOtList=new ArrayList<EmployeeOvertime>(otList);
//				}
				
//				DataMigrationTaskQueueImpl impl=new DataMigrationTaskQueueImpl();
//				impl.updateLeaveBalAndRemoveOldRecords(attenList, empLeaveList, empOtList, infoList);
				impl.updateLeaveBalAndRemoveOldRecords(attendanceList, leaves, otList, infoList);
				
				/**
				 * End
				 */
				
				
				msg="Attendance and payroll delete successful.";
				
				return msg;
			}else if(action.equalsIgnoreCase("Update Project")){
				
				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).list();
				
				/**
				 * 
				 */
				HashSet<Integer> empIdHs=new HashSet<Integer>();
				for(PaySlip ps:paySlipList){
					empIdHs.add(ps.getEmpid());
				}
				ArrayList<Integer> empIdList=new ArrayList<Integer>(empIdHs);
				List<EmployeeInfo> empinfoList=new ArrayList<EmployeeInfo>();
				if(empIdList.size()!=0){
					
					empinfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empIdList).list();
					if(empinfoList!=null&&empinfoList.size()!=0){
						for(PaySlip ps:paySlipList){
							ps.setProjectName(getProjectNameFormEmployee(ps.getEmpid(),empinfoList));	
						}
					}
				}
				
				if(paySlipList.size()!=0){
					ofy().save().entities(paySlipList).now();
				}
				
				
			}else if(action.equalsIgnoreCase("Update Payslip")){
//				long companyId,String action,int employeeId,String payrollPeriod,String project,String branch,String roleName
				logger.log(Level.SEVERE,"Update Payslip");
				
				List<PaySlip> paySlipList=new ArrayList<PaySlip>();
				if(employeeId!=-1&&payrollPeriod!=null){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).filter("salaryPeriod", payrollPeriod).list();
				}else if(employeeId!=-1&&payrollPeriod==null){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", employeeId).list();
				}else if(employeeId==-1&&payrollPeriod!=null){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", payrollPeriod).list();
				}else if(branch!=null&&project!=null){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", payrollPeriod).filter("branch", branch).filter("projectName", project).list();
				}else if(branch!=null&&project==null){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", payrollPeriod).filter("branch", branch).list();
				}else if(branch==null&&project!=null){
					paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", payrollPeriod).filter("projectName", project).list();
				}else{
					return "Invalid combination!";
				}
				
				
				
				/**
				 * @author Anil , Date : 11-09-2019
				 * Writing a programme to update pf , esic and uan number of employee in payslip
				 */
//				ArrayList<String> payRollMonthList=new ArrayList<String>();
//				payRollMonthList.add("2019-Aug");
//				List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod IN", payRollMonthList).list();
				
				ArrayList<Integer> empIdList=new ArrayList<Integer>();
				List<Employee> employeeList=new ArrayList<Employee>();
				if(paySlipList!=null&&paySlipList.size()!=0){
					logger.log(Level.SEVERE,"PaySlip "+paySlipList.size());
					for(PaySlip pay:paySlipList){
						empIdList.add(pay.getEmpid());
					}
					if(empIdList.size()!=0){
						employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("count IN", empIdList).list();
					}
					
					for(PaySlip pay:paySlipList){
						Employee employee=getEmployeeDetails(pay.getEmpid(),employeeList);
						if(employee!=null){
							if(pay.getPfNumber()==null || pay.getPfNumber().equals("")){
								pay.setPfNumber(employee.getPPFNaumber());
							}
							if(pay.getEsicNumber()==null || pay.getEsicNumber().equals("")){
								pay.setEsicNumber(employee.getEmployeeESICcode());
							}
							if(pay.getUanNo()==null || pay.getUanNo().equals("")){
								pay.setUanNo(employee.getUANno());
							}
							if(pay.getAccountNumber()==null||pay.getAccountNumber().equals("")){
								pay.setAccountNumber(employee.getEmployeeBankAccountNo());
							}
							
							if(pay.getIfscNumber()==null||pay.getIfscNumber().equals("")){
								pay.setIfscNumber(employee.getIfscCode());
							}
							if(pay.getBankBranch()==null||pay.getBankBranch().equals("")){
								pay.setBankBranch(employee.getBankBranch());
							}
							if(pay.getBankName()==null||pay.getBankName().equals("")){
								pay.setBankName(employee.getEmployeeBankName());
							}
							
							String aadhar="";
							for (int i = 0; i < employee.getArticleTypeDetails().size(); i++) {
								if (employee.getArticleTypeDetails().get(i).getArticleTypeName().trim().toUpperCase().contains("AADHAR")) {
									aadhar = employee.getArticleTypeDetails().get(i).getArticleTypeValue();
								}
							}
							if(employee.getAadharNumber()!=0){
								aadhar=employee.getAadharNumber()+"";
							}
							
							if(pay.getAadharNo()==null||pay.getAadharNo().equals("")){
								pay.setAadharNo(aadhar);
							}
						}
					}
					ofy().save().entities(paySlipList).now();
				}
				
				
			}
			
			/**
			 * Date : 15-12-2018 BY ANIL
			 * for clearing all loaded data from appengine cache memory
			 */
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			/**
			 * End
			 */
			msg="update successful.";
			
			return msg;
		}

		private Employee getEmployeeDetails(int empid, List<Employee> employeeList) {
			// TODO Auto-generated method stub
			if(employeeList!=null&&employeeList.size()!=0){
				for(Employee emp:employeeList){
					if(emp.getCount()==empid){
						return emp;
					}
				}
			}
			return null;
		}



		private double getPfBaseAmount(PaySlip ps, CtcComponent comp) {
			// TODO Auto-generated method stub
			double pfBaseAmt=0;
			if(ps!=null&&comp!=null){
				for(CtcComponent obj:ps.getEarningList()){
					if(comp.getCtcComponentName()!=null&&comp.getCtcComponentName().equalsIgnoreCase(obj.getName())){
						return obj.getActualAmount();
					}
				}
			}
			return 0;
		}



		/**
		 * @param paySlipList
		 * while deleting payroll we have to reverse the leaves which are allocated to employee at the time of payroll generation
		 */
		private void updateMonthlyAllocatedLeaves(List<PaySlip> paySlipList) {
			// TODO Auto-generated method stub
			HashSet<Integer> empHs=new HashSet<Integer>();
			for(PaySlip slip:paySlipList){
				empHs.add(slip.getEmpid());
			}
			logger.log(Level.SEVERE,"PAYSLIP LIST SIZE "+paySlipList.size());
			ArrayList<Integer> empIntList=new ArrayList<Integer>();
			if(empHs.size()!=0){
				empIntList=new ArrayList<Integer>(empHs);
			}
			logger.log(Level.SEVERE,"UNIQUE EMP LIST SIZE "+empIntList.size());
			List<LeaveBalance> leaveBalanceList=new ArrayList<LeaveBalance>();
			if(empIntList.size()!=0){
				leaveBalanceList=ofy().load().type(LeaveBalance.class).filter("companyId", paySlipList.get(0).getCompanyId()).filter("empInfo.empCount IN", empIntList).list();
			}
			logger.log(Level.SEVERE,"LEAVE BALANCE LIST SIZE "+leaveBalanceList.size());
			
			if(leaveBalanceList!=null){
				for(PaySlip slip:paySlipList){
					/**
					 * @author Anil,Date : 26-07-2019
					 * If paid leaves is  paid then we will not credit/debit PL leave
					 * raised by Nitin Sir,for
					 */
					boolean validPL=true;
					
					if(slip.getPaidLeaves()!=0){
						validPL=false;
					}
					for(LeaveBalance levBal:leaveBalanceList){
						if(slip.getEmpid()==levBal.getEmpInfo().getEmpCount()){
							for(AllocatedLeaves leaves:levBal.getAllocatedLeaves()){
								if(leaves.getMonthlyAllocation()){
									if(leaves.getShortName().equalsIgnoreCase("PL")&&!validPL){
//										leaves.setOpeningBalance(leaves.getBalance());
//										leaves.setBalance(leaves.getBalance());
										continue;
									}
									
									double oneDayLeave=leaves.getMonthlyAllocationLeave()/slip.getMonthlyDays();
									double earnedLeave=oneDayLeave*slip.getPaidDays();
									double balanceLeave=leaves.getBalance()-earnedLeave;
									leaves.setBalance(balanceLeave);
									/**
									 * date : 16-12-2018 BY ANIL
									 */
									leaves.setEarned(leaves.getEarned()-earnedLeave);
								}
							}
						}
					}
				}
			}
			
			if(leaveBalanceList.size()!=0){
				ofy().save().entities(leaveBalanceList).now();
			}
			logger.log(Level.SEVERE,"EARNED LEAVE REVERSED ");
		}



		private String getProjectNameFormEmployee(int empid,List<EmployeeInfo> empinfoList) {
			String projectName="";
			for(EmployeeInfo info:empinfoList){
				if(empid==info.getEmpCount()){
					projectName=info.getProjectName();
					break;
				}
			}
			return projectName;
		}
		
		/**
		 * End
		 */
		 	/** Date 13.12.2018 added by komal for update accounting interface **/
		@Override
		public String  updateAccountingInterface(SuperModel model) {
			// TODO Auto-generated method stub
			List<AccountingInterface> tallyentityList = ofy().load().type(AccountingInterface.class).filter("companyId", model.getCompanyId())
					.filter("documentID", model.getCount()).filter("documentType", "Invoice").list();
			
			if(tallyentityList != null){
				for(AccountingInterface tallyInterface :tallyentityList){
					if(tallyInterface.getStatus().equals(AccountingInterface.TALLYCREATED)){
						ofy().delete().entity(tallyInterface);
					}
				}
			}
			Invoice invoice = null;
			if(model instanceof Invoice) {
				invoice = (Invoice) model;
			}else if(model instanceof VendorInvoice){
				invoice = (VendorInvoice) model;
			}
			if(invoice != null){
				invoice.accountingInterface(invoice);
			}
				
			return "Accounting Interface updated successfully.";		
		}
		
		/**
		 * End
		 */
		
		
		@Override
		public String updatePRStatus(int purchaseReqId) {
			GRN grn = new GRN();
			grn.updatePR(purchaseReqId);
			return "Success";
		}



		@Override
		public String updateGRNDocs(long companyId) {
			ArrayList<String> statuslist = new ArrayList<String>();
			statuslist.add(GRN.CREATED);
			statuslist.add(GRN.REQUESTED);
			List<GRN> grnlist = ofy().load().type(GRN.class).filter("companyId", companyId).filter("status IN", statuslist).list();
			if(grnlist.size()!=0){
				for(GRN grn : grnlist){
					for(GRNDetails grndetails : grn.getInventoryProductItem()){
						SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", grndetails.getProductID()).filter("status", true).first().now();
						if(product!=null){
							grndetails.setPrduct(product);
						}
					}
					ofy().save().entity(grn);
					logger.log(Level.SEVERE,"GRN Updated sucessfully");
				}
			}
			return "GRN Updated sucessfuly";
		}



		@Override
		public String updatePRDoc(Approvals approval, boolean documentflag, int vendorId, double vendorPrice) {
			approval.setDocumentValidation(documentflag);
			approval.setVendorPrice(vendorPrice);
			if(vendorId!=-1){
				approval.setVendorId(vendorId);
			}
			ofy().save().entities(approval);
			
			PurchaseRequisition purchaseReq = ofy().load().type(PurchaseRequisition.class).filter("companyId", approval.getCompanyId())
					.filter("count", approval.getBusinessprocessId()).first().now();
			Vendor vendor = null;
			if(purchaseReq!=null){
				vendor = ofy().load().type(Vendor.class).filter("count", vendorId).filter("companyId", approval.getCompanyId()).first().now();
				if(vendor!=null){
					VendorDetails vendordetails = new VendorDetails();
					vendordetails.setVendorId(vendor.getCount());
					vendordetails.setVendorEmailId(vendor.getEmail());
					vendordetails.setVendorName(vendor.getVendorName());
					vendordetails.setVendorPhone(vendor.getCellNumber1());
					if(purchaseReq.getVendlist()==null){
						 ArrayList<VendorDetails> vendorlist = new ArrayList<VendorDetails>();
						 vendorlist.add(vendordetails);
						 purchaseReq.setVendlist(vendorlist);
					}
					else{
						purchaseReq.getVendlist().add(vendordetails);
					}
					logger.log(Level.SEVERE, "vendorId "+vendorId);
					if(purchaseReq.isAutomaticPRFlag() && vendorId!=-1){
						for(ProductDetails prodDetails : purchaseReq.getPrproduct()){
							prodDetails.setProdPrice(vendorPrice);
							prodDetails.setTotal(prodDetails.getProdPrice()*prodDetails.getProductQuantity());
							logger.log(Level.SEVERE, "product Total"+prodDetails.getTotal());
						}
					}
					ofy().save().entity(purchaseReq);
				}
				
			}
			if(purchaseReq!=null){
				PriceList vendorpricelist = ofy().load().type(PriceList.class).filter("companyId", purchaseReq.getCompanyId())
														.filter("prodID", purchaseReq.getPrproduct().get(0).getProductID()).first().now();
				System.out.println("vendorpricelist =="+vendorpricelist);
				if(vendorpricelist!=null && vendor!=null){
					System.out.println("111111");
					updateVendorPricelist(purchaseReq,vendor,vendorPrice,approval.getCompanyId(),true);
						
				}
				else if(vendorpricelist==null && vendor!=null){
					System.out.println("2222222");
					updateVendorPricelist(purchaseReq,vendor,vendorPrice,approval.getCompanyId(),false);

				}
			}
			return "Successful";
		}



		private void updateVendorPricelist(PurchaseRequisition purchaseReq, Vendor vendor, double vendorPrice, Long comapnyid, boolean flag) {

		PriceList vendorprice = new PriceList();
			
		ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId", comapnyid)
							.filter("count",  purchaseReq.getPrproduct().get(0).getProductID()).first().now();
		if(itemproduct!=null){
		Date toDate = null;
		try {
		toDate = new SimpleDateFormat("yyyy-MM-dd").parse("9999-12-31");
		} catch (ParseException e) {
		e.printStackTrace();
		}
		
		PriceListDetails info = new PriceListDetails();
		info.setCount(vendor.getCount());
		info.setFullName(vendor.getVendorName());
		info.setCellNumber(vendor.getCellNumber1());
		info.setProductName(itemproduct.getProductName());
		info.setProductCode(itemproduct.getProductCode());
		info.setProductCategory(itemproduct.getProductCategory());
		info.setProductPrice(vendorPrice);
		info.setUnitofMeasure(itemproduct.getUnitOfMeasurement());
		info.setProdID(itemproduct.getCount());
		info.setPocName(vendor.getfullName());
		if(toDate!=null)
		info.setTodate(toDate);
		info.setFromdate(new Date());
		
		vendorprice.getPriceInfo().add(info);
		vendorprice.setCompanyId(comapnyid);
		if(flag==false){
			vendorprice.setProdID(itemproduct.getCount());
			vendorprice.setProdName(itemproduct.getProductName());
			vendorprice.setProdCode(itemproduct.getProductCode());
			vendorprice.setProdCategory(itemproduct.getProductCategory());
			vendorprice.setProdPrice(vendorPrice);
			vendorprice.setUnitOfmeasurement(itemproduct.getUnitOfMeasurement());
		}
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(vendorprice);
		}
		
		}

		/**
		 * Updated By: Viraj
		 * Date: 22-01-2019
		 * Description: To return list of billing, vendorInvoice and payment document in purchase order
		 */
		@Override
		public ArrayList<CancelContract> getCancellingDocumentPurchaseOrder(
				Long companyId, int orderId) {
			
			List<AccountingInterface> accInterfaceList = new ArrayList<AccountingInterface>();
			ArrayList<CancelContract> cancelContractList = new ArrayList<CancelContract>();
			UpdateServiceImpl uServiceImpl = new UpdateServiceImpl();
			
			/**
			 * Updated By: Viraj
			 * Date: 14-02-2019
			 * Description: To include GRN documents in cancellation List
			 */
			List<GRN> grnList = ofy().load()
					.type(GRN.class).filter("companyId", companyId)
					.filter("contractCount", orderId)
					.filter("typeOfOrder", AppConstants.ORDERTYPEFORPURCHASE).list();
			System.out.println("billing" + grnList.size() + "");
			if (grnList.size() != 0) {
				for (GRN bDoc : grnList) {
					cancelContractList.add(uServiceImpl.setDataInCancelContract(
							bDoc.getCount(), bDoc.getBranch(), bDoc.getTitle(),
							bDoc.getStatus(), AppConstants.GRN));
				}
			}
			/** Ends **/
			
			List<BillingDocument> billingList = ofy().load()
					.type(BillingDocument.class).filter("companyId", companyId)
					.filter("contractCount", orderId)
					.filter("typeOfOrder", AppConstants.ORDERTYPEFORPURCHASE).list();
			System.out.println("billing" + billingList.size() + "");
			if (billingList.size() != 0) {
				for (BillingDocument bDoc : billingList) {
					cancelContractList.add(uServiceImpl.setDataInCancelContract(
							bDoc.getCount(), bDoc.getBranch(), bDoc.getName(),
							bDoc.getStatus(), AppConstants.BILLINGDETAILS));
				}
			}
			List<VendorInvoice> invoiceList = ofy().load().type(VendorInvoice.class)
					.filter("companyId", companyId)
					.filter("contractCount", orderId)
					.filter("typeOfOrder", AppConstants.ORDERTYPEFORPURCHASE).list();
			System.out.println("billing" + invoiceList.size() + "");
			if (invoiceList.size() != 0) {
				for (VendorInvoice invoice : invoiceList) {
					cancelContractList.add(uServiceImpl.setDataInCancelContract(
							invoice.getCount(), invoice.getBranch(), invoice.getPersonInfo().getFullName(),
							invoice.getStatus(), AppConstants.PROCESSCONFIGINVVENDOR));
					if (invoice.getStatus().equals(VendorInvoice.APPROVED)) {
						AccountingInterface acc = ofy()
								.load()
								.type(AccountingInterface.class)
								.filter("companyId", companyId)
								.filter("documentID", invoice.getCount())
								.filter("documentType",
										AppConstants.PROCESSCONFIGINVVENDOR).first()
								.now();
						if (acc != null)
							accInterfaceList.add(acc);
					}
				}
			}
			List<CustomerPayment> paymentList = ofy().load()
					.type(CustomerPayment.class).filter("companyId", companyId)
					.filter("contractCount", orderId)
					.filter("typeOfOrder", AppConstants.ORDERTYPEFORPURCHASE).list();
			System.out.println("billing" + paymentList.size() + "");
			if (paymentList.size() != 0) {
				for (CustomerPayment payment : paymentList) {
					cancelContractList.add(uServiceImpl.setDataInCancelContract(
							payment.getCount(), payment.getBranch(), payment.getPersonInfo().getFullName(),
							payment.getStatus(), AppConstants.PAYMENTDETAILS));
					if (payment.getStatus().equals(CustomerPayment.CLOSED)) {
						AccountingInterface acc = ofy().load()
								.type(AccountingInterface.class)
								.filter("companyId", companyId)
								.filter("documentID", payment.getCount())
								.filter("documentType", AppConstants.PAYMENT)
								.first().now();
						if (acc != null)
							accInterfaceList.add(acc);
					}
				}
			}
			
			List<AccountingInterface> acntsList = ofy().load()
					.type(AccountingInterface.class).filter("companyId", companyId)
					.filter("documentID", orderId)
					.filter("documentType", "PurchaseOrder").list();
			acntsList.addAll(accInterfaceList);
			System.out.println("billing" + acntsList.size() + "");
			if (acntsList.size() != 0) {
				for (AccountingInterface accInterface : acntsList) {
					if (!(accInterface.getDocumentStatus()
							.equals(AccountingInterface.CANCELLED))) {
						cancelContractList.add(uServiceImpl.setDataInCancelContract(
								accInterface.getCount(), accInterface.getBranch(),
								accInterface.getCustomerName(),
								accInterface.getDocumentStatus(),
								AppConstants.PROCESSTYPEACCINTERFACE));
					}
				}

			}
			System.out.println("list" + cancelContractList.size() + "");
			accInterfaceList.clear();
			
			return cancelContractList;
		}

		@Override
		public void savePOCancellationData(PurchaseOrder po, String remark,
				String loginUser, ArrayList<CancelContract> cancelContractList) {
			
			logger.log(Level.SEVERE, "Company Id " + po.getCompanyId());
			System.out.println("companyId" + po.getCompanyId());

			ArrayList<Integer> billingIdList = new ArrayList<Integer>();
			ArrayList<Integer> vendorInvoiceList = new ArrayList<Integer>();
			ArrayList<Integer> paymentList = new ArrayList<Integer>();
			ArrayList<Integer> grnList = new ArrayList<Integer>();
			
//			ArrayList<CancelContract> cancelList = new ArrayList<CancelContract>();
			ArrayList<Integer> accInterfaceIdList = new ArrayList<Integer>();
			String documentType;
			String typeducumentName;
			Integer poId = 0;
			Queue queue;

			for (CancelContract c : cancelContractList) {
				documentType = c.getDocumnetName();
				switch (documentType) {
				
				case AppConstants.PURCHASEORDER :
					poId = c.getDocumnetId();
					break;
					
				case AppConstants.BILLINGDETAILS :
					billingIdList.add(c.getDocumnetId());
					break;
					
				case AppConstants.PROCESSCONFIGINVVENDOR :
					vendorInvoiceList.add(c.getDocumnetId());
					break;
					
				case AppConstants.PAYMENTDETAILS :
					paymentList.add(c.getDocumnetId());
					break;
					
				case AppConstants.PROCESSTYPEACCINTERFACE:
					accInterfaceIdList.add(c.getDocumnetId());
					break;
					
				/**
				 * Updated By: Viraj
				 * Date: 14-02-2019
				 * Description: To include GRN documents in cancellation List
				 */
				case AppConstants.GRN:
					grnList.add(c.getDocumnetId());
				}
			}
			
			typeducumentName = "ClearList" + "$" + po.getCompanyId();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
			if(poId != 0) {
				typeducumentName = "SelectedCancelPO" + "$"
						+ po.getCompanyId() + "$" + po.getCount() + "$"
						+ poId + "$" + remark + "$" + loginUser + "$"
						+ AppConstants.PURCHASEORDER + "$"
						+ cancelContractList.size();
				logger.log(Level.SEVERE, "Company Id " + typeducumentName);
				queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", typeducumentName));
			}
			
			/**
			 * Updated By: Viraj
			 * Date: 14-02-2019
			 * Description: To include GRN documents in cancellation List
			 */
			for (Integer grnId : grnList) {
				typeducumentName = "SelectedCancelPO" + "$"
						+ po.getCompanyId() + "$" + po.getCount() + "$"
						+ grnId + "$" + remark + "$" + loginUser + "$"
						+ AppConstants.GRN + "$"
						+ cancelContractList.size();
				logger.log(Level.SEVERE, "Company Id " + typeducumentName);
				queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", typeducumentName));
			}
			/** Ends **/
			for (Integer billingId : billingIdList) {
				typeducumentName = "SelectedCancelPO" + "$"
						+ po.getCompanyId() + "$" + po.getCount() + "$"
						+ billingId + "$" + remark + "$" + loginUser + "$"
						+ AppConstants.BILLINGDETAILS + "$"
						+ cancelContractList.size();
				logger.log(Level.SEVERE, "Company Id " + typeducumentName);
				queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", typeducumentName));
			}
			
			for (Integer invoiceId : vendorInvoiceList) {
				typeducumentName = "SelectedCancelPO" + "$"
						+ po.getCompanyId() + "$" + po.getCount() + "$"
						+ invoiceId + "$" + remark + "$" + loginUser + "$"
						+ AppConstants.PROCESSCONFIGINVVENDOR + "$"
						+ cancelContractList.size();
				logger.log(Level.SEVERE, "Company Id " + typeducumentName);
				queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", typeducumentName));
			}
			
			for (Integer paymentId : paymentList) {
				typeducumentName = "SelectedCancelPO" + "$"
						+ po.getCompanyId() + "$" + po.getCount() + "$"
						+ paymentId + "$" + remark + "$" + loginUser + "$"
						+ AppConstants.PAYMENTDETAILS + "$" + cancelContractList.size();
				logger.log(Level.SEVERE, "Company Id " + typeducumentName);
				queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", typeducumentName));
			}
			
			for (Integer accInterfaceId : accInterfaceIdList) {
				typeducumentName = "SelectedCancelPO" + "$"
						+ po.getCompanyId() + "$" + po.getCount() + "$"
						+ accInterfaceId + "$" + remark + "$" + loginUser + "$"
						+ AppConstants.PROCESSTYPEACCINTERFACE + "$"
						+ cancelContractList.size();
				logger.log(Level.SEVERE, "Company Id " + typeducumentName);
				queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", typeducumentName));
			}	
			
		}
		/** Ends **/

		/**
		 * Date 28-01-2019 NBHC Inventory Management Updating product inventory with Warehouse type
		 * to identify warehouse and cluster warehouse. calling task queue for bulk data
		 */

		@Override
		public String updateProductInventoryWithWarehouseType(long companyId) {
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "UpdateWarehouseType", companyId)){
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue"); 
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
						.param("OperationName", "UpdateProductInventoryWithWarehouseType")
						.param("companyId", companyId+""));
				return "Success";
			}
			else{
				return "failed";
			}
			
		}
		/**
		 * ends here
		 */




		@Override
		public String validateDueService(long companyid, Date startDate, Date dueDate, ArrayList<String> branchlist,ArrayList<Integer> listcontractId) {
			/**
			 * @author Anil
			 * @since 25-05-2020
			 * added in house service flag
			 */
			logger.log(Level.SEVERE,"Validation for Invoice impl");
			List<Service> scheduledServicelist = ofy().load().type(Service.class).filter("companyId", companyid).filter("branch IN", branchlist)
										.filter("status", Service.SERVICESTATUSSCHEDULE).filter("wmsServiceFlag",false).filter("serviceDate >=", startDate)
										.filter("serviceDate <=", dueDate).list();
			logger.log(Level.SEVERE, "scheduledServicelist =="+scheduledServicelist.size());
			
			if(scheduledServicelist.size()>0){
				return "Services Pending";
			}
			
			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyid).filter("branch IN", branchlist)
					.filter("status", Service.SERVICESTATUSRESCHEDULE).filter("wmsServiceFlag",false).filter("serviceDate >=", startDate)
					.filter("serviceDate <=", dueDate).list();
			logger.log(Level.SEVERE, "servicelist =="+servicelist.size());
			if(servicelist.size()>0){
				return "Services Pending";
			}
			
			/**
			 * Date 10-07-2019 As per vaishali mam she need T+72 validation without contract wise 
			 *  So below validation i have commented
			 */
			
//			/*** Above code was with if any services pending with T+72 then cant allow to create any invoice. Vaishali mam requested change this only with contract wise ***/
//
//			/** Date 11-06-2019 by Vijay for NBHC CCPM Validation with contract wise ****/
//			HashSet<Integer> contractlist = new HashSet<Integer>(listcontractId);
//			ArrayList<Integer> updatedcontractlist = new ArrayList<Integer>();
//			for(int contractid :contractlist){
//				updatedcontractlist.add(contractid);
//			}
//			System.out.println("updatedcontractlist"+updatedcontractlist.size());
//			
//			List<Service> scheduledServicelist = ofy().load().type(Service.class).filter("companyId", companyid).filter("contractCount IN", updatedcontractlist)
//					.filter("status", Service.SERVICESTATUSSCHEDULE).filter("serviceDate >=", startDate)
//					.filter("serviceDate <=", dueDate).list();
//			logger.log(Level.SEVERE, "scheduledServicelist =="+scheduledServicelist.size());
//
//			if(scheduledServicelist.size()>0){
//				return "Services Pending";
//			}
//			
//			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyid).filter("contractCount IN", updatedcontractlist)
//					.filter("status", Service.SERVICESTATUSRESCHEDULE).filter("serviceDate >=", startDate)
//					.filter("serviceDate <=", dueDate).list();
//			logger.log(Level.SEVERE, "servicelist =="+servicelist.size());
//			
//			if(servicelist.size()>0){
//				return "Services Pending";
//			}
			
			return "Success";
		}


		/**
		 * Date 10-05-2019 by Vijay
		 * Des :- creating Storage location and Storage Bin for the existing warehouse
		 */
		@Override
		public String createStoraLocationBin(long companyId) {
			
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue"); 
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "CreateStorageLocationAndStorageBinForWarehouse")
					.param("companyId", companyId+""));
			return "Success";
		}


		/**
		 * Date 20-05-2019 by Vijay
		 * Des :- updating contract Discontinue Date with discontinue approval date
		 */
		@Override
		public String updateContractDiscontinued(long companyId) {
			List<Approvals> approvallist = ofy().load().type(Approvals.class).filter("companyId", companyId)
											.filter("businessprocesstype", "Contract Discontinue").filter("status", "Approved").list();
			logger.log(Level.SEVERE, "Discontinue Approval list size"+approvallist.size());
			ArrayList<Integer> contractIdlist = new ArrayList<Integer>();
			for(Approvals approval : approvallist){
				contractIdlist.add(approval.getBusinessprocessId());
			}
			
			List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count IN", contractIdlist).list();
			for(Contract contract: contractlist){
				for(Approvals approval : approvallist){
					if(contract.getCount()==approval.getBusinessprocessId()){
						contract.setCancellationDate(approval.getApprovalOrRejectDate());
					}
				}
			}
			
			ofy().save().entities(contractlist);
			logger.log(Level.SEVERE, "Record Updated sucessfully");

			return "Success";
		}
		
		
		@Override
		public String deleteExtraServicesAgainstContract(Contract contract,int productId) {
			// TODO Auto-generated method stub
			logger.log(Level.SEVERE, "INSIDE DELETE EXTRA SERVICES METHOD");
			List<Service> serviceList=new ArrayList<Service>();
			List<Service> extraServiceList=new ArrayList<Service>();
			String msg="";
			if(contract!=null&&contract.getStatus().equals("Approved")){
				
				int totalService=0;
				for(SalesLineItem item:contract.getItems()){
					totalService=(int) (totalService+(item.getQty()*item.getNumberOfServices()));
				}
				logger.log(Level.SEVERE, "Total services in contract : "+totalService);
				
				if(productId!=0){
					logger.log(Level.SEVERE, "Product ID "+productId);
					serviceList=ofy().load().type(Service.class).filter("companyId", contract.getCompanyId()).filter("contractCount", contract.getCount()).filter("product.count", productId).list();
				}else{
					serviceList=ofy().load().type(Service.class).filter("companyId", contract.getCompanyId()).filter("contractCount", contract.getCount()).list();
				}
				logger.log(Level.SEVERE, "Total services actual created : "+serviceList.size());
				int actualTotalServices=serviceList.size();
				
				if(serviceList.size()!=0){
					for(int i=0;i<serviceList.size();i++){
						Service object1=serviceList.get(i);
						Service prev=null;
						Service current=null;
						int counter=0;
						for(int j=0;j<serviceList.size();j++){
							
							Service object2=serviceList.get(j);
							
							if(object1.getBranch().equals(object2.getBranch())
									&&object1.getProduct().getCount()==object2.getProduct().getCount()
									&&object1.getServiceSerialNo()==object2.getServiceSerialNo()
									&&object1.getServiceBranch().equals(object2.getServiceBranch())){
								
								current=object2;
								counter++;
								if(counter>1){
									if(object2.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
										if(prev!=null&&!prev.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
											extraServiceList.add(prev);
											serviceList.remove(j);
//											i--;
											j--;
										}
									}else{
										extraServiceList.add(object2);
										serviceList.remove(j);
//										i--;
										j--;
									}
								}
								prev=current;
							}
							
						}
					}
					logger.log(Level.SEVERE, "Total extra services created : "+extraServiceList.size());
					
					if(extraServiceList.size()!=0){
						msg="Total services in contract "+totalService+", Actual number of created services "+actualTotalServices+". Extra services deleted "+extraServiceList.size();
						ofy().delete().entities(extraServiceList).now();
					}
					
				}else{
					msg="No services created against contract.";
				}
			}else{
				msg="Contract not found or status not approved.";
			}
			
			
			return msg;
		}
		/**
		 * Updated By: Viraj
		 * Date: 28-03-2019
		 * Description: To create customer in brand if master franchise creates customer with customer type franchise
		 */
		public void createCustomerInBrand(Long companyId, Customer customer) {
			
			Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			ArrayList<Customer> custList = new ArrayList<Customer>();
			

			/**
			 * @author Anil @since 28-04-2021
			 * Suspected because of this NBHC wont able to save customer
			 * but customer number range get increases becuase of this on large customer data process
			 * user was not able to login, system go on hang mode
			 * Also added error handling in below code
			 */
			if(comp==null){
				return;
			}
			if(comp.getFranchiseType()==null || comp.getFranchiseType().equals("")){
				return;
			}
			if(comp.getBrandUrl()==null || comp.getBrandUrl().equals("")){
				return;
			}
			
			if(comp.getFranchiseType().equalsIgnoreCase("MASTER FRANCHISE")) {
				if(customer.getFranchiseType().equalsIgnoreCase("FRANCHISE") ) {
					
					Customer cust = new Customer();
					cust.setFullname(customer.getFullname());
					cust.setCellNumber1(customer.getCellNumber1());
					cust.setBranch(customer.getBranch());
					cust.setEmployee(customer.getEmployee());
					cust.setCategory(customer.getCategory());
					cust.setType(customer.getType());
					cust.getAdress().setAddrLine1(customer.getAdress().getAddrLine1());
					cust.getAdress().setCity(customer.getAdress().getCity());
					cust.getAdress().setState(customer.getAdress().getState());
					cust.getAdress().setCountry(customer.getAdress().getCountry());
					cust.getSecondaryAdress().setAddrLine1(customer.getSecondaryAdress().getAddrLine1());
					cust.getSecondaryAdress().setCity(customer.getSecondaryAdress().getCity());
					cust.getSecondaryAdress().setState(customer.getSecondaryAdress().getState());
					cust.getSecondaryAdress().setCountry(customer.getSecondaryAdress().getCountry());
					cust.setFranchiseType(customer.getFranchiseType());
					cust.setFranchiseUrl(customer.getFranchiseUrl());
					cust.setMasterFranchiseUrl(comp.getCompanyURL());
					custList.add(cust);
				}
				logger.log(Level.SEVERE,"Brand URL : "+comp.getBrandUrl());
				final String CLIENTURL=comp.getBrandUrl()+AppConstants.FRANCHISETYPELIST.CUSTOMERUPDATE_URL;
				logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
				
				String data="";
				URL url = null;
				try {
					url = new URL(CLIENTURL);
				} catch (MalformedURLException e) {
					logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
					e.printStackTrace();
				}
				logger.log(Level.SEVERE,"STAGE 1");
		        HttpURLConnection con = null;
				try {
					con = (HttpURLConnection) url.openConnection();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
					e.printStackTrace();					
				}
				logger.log(Level.SEVERE,"STAGE 2");
		        con.setDoInput(true);
		        con.setDoOutput(true);
		        try {
					con.setRequestMethod("POST");
				} catch (ProtocolException e) {
					logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
					e.printStackTrace();					
				}
		        logger.log(Level.SEVERE,"STAGE 3");
		        OutputStream os = null;
				try {
					os = con.getOutputStream();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
					e.printStackTrace();
				}
				logger.log(Level.SEVERE,"STAGE 4");
		        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		        try {
					writer.write(getDataTosendBrand(comp, custList));
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        try {
					writer.flush();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        logger.log(Level.SEVERE,"STAGE 5");
		        
		        
		        
		        InputStream is = null;
				try {
					is = con.getInputStream();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
					e.printStackTrace();
				}
				
				 logger.log(Level.SEVERE,"STAGE 6");
		        BufferedReader br = new BufferedReader(new InputStreamReader(is));
		        String temp;
		        try {
					while ((temp = br.readLine()) != null) {
					    data = data + temp+"\n"+"\n";
					}
					logger.log(Level.SEVERE,"data data::::::::::"+data);
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        
		        logger.log(Level.SEVERE,"STAGE 7");
		        
		        logger.log(Level.SEVERE,"Data::::::::::"+data);
			}
			/** Ends **/
			
		}



		private String getDataTosendBrand(Company comp,
				ArrayList<Customer> custList) {
			StringBuilder strB=new StringBuilder();
			strB.append("jsonString");
			strB.append("=");
			strB.append(createJSONObjectFromCustomer(comp, custList));
			
			return strB.toString();
		}
		
		private Object createJSONObjectFromCustomer(Company comp,
				ArrayList<Customer> custList) {
			
			Gson gson = new Gson();  
			org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
			
			JSONArray jsonArray = null;
			try {
				jsonArray = new JSONArray(gson.toJson(custList));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE,"Json array exception : "+e);
				
			}
			jObj.put("cust_list",jsonArray);
			jObj.put("comp_id", comp.getCompanyId());
			jObj.put("comp_name", comp.getBusinessUnitName());
			jObj.put("comp_brandUrl", comp.getBrandUrl());
			
			String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE,"Customer Data:: "+jsonString);
			
			
			return jsonString;
		}
		/** Ends **/

		/**
		 * Updated By: Viraj
		 * Date: 06-04-2019
		 * Description: To fetch all the product inventory list data of the customer
		 * @param customerId
		 * @return 
		 */
		@Override
		public ArrayList<ProductInventoryViewDetails> fetchCustomerProductViewList(long companyId ,int customerId) {
			
			Customer cust = ofy().load().type(Customer.class).filter("companyId", companyId)
							.filter("count",customerId).first().now();
			logger.log(Level.SEVERE,"customerId : "+customerId);
			logger.log(Level.SEVERE,"companyId : "+companyId);
			logger.log(Level.SEVERE,"cust : "+cust);
			logger.log(Level.SEVERE,"franchise url : "+cust.getFranchiseUrl());
			logger.log(Level.SEVERE,"mfranchise url : "+cust.getMasterFranchiseUrl());
			String custUrl="";
			if(cust.getFranchiseUrl() != null && !cust.getFranchiseUrl().equals("")) {
				custUrl = cust.getFranchiseUrl();
			}
			if(cust.getMasterFranchiseUrl() != null && !cust.getMasterFranchiseUrl().equals("")) {
				custUrl = cust.getMasterFranchiseUrl();
			}
			logger.log(Level.SEVERE,"Cust URL : "+custUrl);
			final String CLIENTURL=custUrl+AppConstants.FRANCHISETYPELIST.CUSTPRODINVLIST_URL;
			logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
			
			String data="";
			URL url = null;
			try {
				url = new URL(CLIENTURL);
			} catch (MalformedURLException e) {
				logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"STAGE 1");
	        HttpURLConnection con = null;
			try {
				con = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
				e.printStackTrace();					
			}
			logger.log(Level.SEVERE,"STAGE 2");
	        con.setDoInput(true);
	        con.setDoOutput(true);
	        try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
				e.printStackTrace();					
			}
	        logger.log(Level.SEVERE,"STAGE 3");
	        OutputStream os = null;
			try {
				os = con.getOutputStream();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"STAGE 4");
	        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
	        try {
				writer.write(getDataTosendProductViewList(cust));
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
				e.printStackTrace();
			}
	        try {
				writer.flush();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
				e.printStackTrace();
			}
	        logger.log(Level.SEVERE,"STAGE 5");
	        
	        
	        
	        InputStream is = null;
			try {
				is = con.getInputStream();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
				e.printStackTrace();
			}
			
			 logger.log(Level.SEVERE,"STAGE 6");
	        BufferedReader br = new BufferedReader(new InputStreamReader(is));
	        String temp;
	        try {
				while ((temp = br.readLine()) != null) {
				    data = data + temp+"\n"+"\n";
				}
				logger.log(Level.SEVERE,"data data::::::::::"+data);
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
				e.printStackTrace();
			}
	        
	        logger.log(Level.SEVERE,"STAGE 7");
	        logger.log(Level.SEVERE,"Data::::::::::"+data);
	        
	        Gson gson = new Gson();
			JSONObject object = null;
			try {
				object = new JSONObject(data.trim());
			} catch (JSONException e) {
				e.printStackTrace();
			}
	        
			JSONArray piv_array = null;
			try {
				piv_array = object.getJSONArray("piv_list");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			Type type = new TypeToken<ArrayList<ProductInventoryViewDetails>>(){}.getType();
			ArrayList<ProductInventoryViewDetails> pivList=new ArrayList<ProductInventoryViewDetails>();
			pivList=gson.fromJson(piv_array.toString(), type);
			logger.log(Level.SEVERE,"pivList to be returned::::::::::"+pivList);
			
			return pivList;
		}



		private String getDataTosendProductViewList(Customer cust) {
			StringBuilder strB=new StringBuilder();
			strB.append("jsonString");
			strB.append("=");
			strB.append(createJSONObjectFromCustomerPVL(cust));
			
			return strB.toString();
		}



		private Object createJSONObjectFromCustomerPVL(Customer cust) {
			Gson gson = new Gson();  
			org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
			
			if(cust.getFranchiseType().equalsIgnoreCase("MASTER FRANCHISE")){
				jObj.put("cust_companyURL",cust.getMasterFranchiseUrl());
			}else if(cust.getFranchiseType().equalsIgnoreCase("FRANCHISE")){
				jObj.put("cust_companyURL",cust.getFranchiseUrl());
			}
			
			String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE,"Customer Data:: "+jsonString);
			
			
			return jsonString;
		}
		/** Ends **/

		/**
		 * Updated By: Viraj
		 * Date: 16-05-2019
		 * Description: To fetch sales register information from master franchise or franchise depending customer franchise type
		 * @param toDate 
		 * @param fromDate 
		 */
		@Override
		public String fetchSalesRegisterList(long companyId,
				int customerId, Date fromDate, Date toDate) {
			
			Customer cust = ofy().load().type(Customer.class).filter("companyId", companyId)
					.filter("count",customerId).first().now();
			logger.log(Level.SEVERE,"customerId : "+customerId);
			logger.log(Level.SEVERE,"companyId : "+companyId);
			logger.log(Level.SEVERE,"cust : "+cust);
			logger.log(Level.SEVERE,"franchise url : "+cust.getFranchiseUrl());
			logger.log(Level.SEVERE,"mfranchise url : "+cust.getMasterFranchiseUrl());
			String custUrl="";
			if(cust.getFranchiseUrl() != null && !cust.getFranchiseUrl().equals("")) {
				custUrl = cust.getFranchiseUrl();
			}
			if(cust.getMasterFranchiseUrl() != null && !cust.getMasterFranchiseUrl().equals("")) {
				custUrl = cust.getMasterFranchiseUrl();
			}
			logger.log(Level.SEVERE,"Cust URL : "+custUrl);
			
			final String CLIENTURL=custUrl+AppConstants.FRANCHISETYPELIST.CUSTSALESREGLIST_URL;
			logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
			
			String data="";
			URL url = null;
			try {
				url = new URL(CLIENTURL);
			} catch (MalformedURLException e) {
				logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"STAGE 1");
	        HttpURLConnection con = null;
			try {
				con = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
				e.printStackTrace();					
			}
			logger.log(Level.SEVERE,"STAGE 2");
	        con.setDoInput(true);
	        con.setDoOutput(true);
	        try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
				e.printStackTrace();					
			}
	        logger.log(Level.SEVERE,"STAGE 3");
	        OutputStream os = null;
			try {
				os = con.getOutputStream();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"STAGE 4");
	        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
	        try {
				writer.write(getDataTosendSalesRegisterList(cust, fromDate,toDate));
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
				e.printStackTrace();
			}
	        try {
				writer.flush();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
				e.printStackTrace();
			}
	        logger.log(Level.SEVERE,"STAGE 5");
	        
	        
	        
	        InputStream is = null;
			try {
				is = con.getInputStream();
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
				e.printStackTrace();
			}
			
			 logger.log(Level.SEVERE,"STAGE 6");
	        BufferedReader br = new BufferedReader(new InputStreamReader(is));
	        String temp;
	        try {
				while ((temp = br.readLine()) != null) {
				    data = data + temp+"\n"+"\n";
				}
				logger.log(Level.SEVERE,"data data::::::::::"+data);
			} catch (IOException e) {
				logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
				e.printStackTrace();
			}
	        
	        logger.log(Level.SEVERE,"STAGE 7");
	        logger.log(Level.SEVERE,"Data::::::::::"+data);
	        
	        Gson gson = new Gson();
			JSONObject object = null;
			try {
				object = new JSONObject(data.trim());
			} catch (JSONException e) {
				e.printStackTrace();
			}
	        
			JSONArray salesReg_array = null;
//			JSONArray salesCust_array = null;
			try {
				salesReg_array = object.getJSONArray("sr_list");
//				salesCust_array = object.getJSONArray("cust_list");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			Type type = new TypeToken<ArrayList<Invoice>>(){}.getType();
			ArrayList<Invoice> salesRegisterList=new ArrayList<Invoice>();
			salesRegisterList=gson.fromJson(salesReg_array.toString(), type);
			logger.log(Level.SEVERE,"srList to be returned::::::::::"+salesRegisterList);
			
//			Type type1 = new TypeToken<ArrayList<Customer>>(){}.getType();
//			ArrayList<Customer> custList=new ArrayList<Customer>();
//			custList=gson.fromJson(salesCust_array.toString(), type1);
//			logger.log(Level.SEVERE,"custList to be returned::::::::::"+custList);
			
			String salesRegList = object.optString("sr_String").trim();
			logger.log(Level.SEVERE,"srList to be returned::::::::::"+salesRegisterList);
			
			CsvWriter.salesRegList = salesRegList;
			
			return salesRegList;
		}
		
		private String getDataTosendSalesRegisterList(Customer cust, Date fromDate, Date toDate) {
			StringBuilder strB=new StringBuilder();
			strB.append("jsonString");
			strB.append("=");
			strB.append(createJSONObjectFromCustomerSRL(cust, fromDate, toDate));
			
			return strB.toString();
		}
		
		private Object createJSONObjectFromCustomerSRL(Customer cust, Date fromDate, Date toDate) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			Gson gson = new Gson();  
			org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
			
			if(cust.getFranchiseType().equalsIgnoreCase("MASTER FRANCHISE")){
				jObj.put("cust_companyURL",cust.getMasterFranchiseUrl());
			}else if(cust.getFranchiseType().equalsIgnoreCase("FRANCHISE")){
				jObj.put("cust_companyURL",cust.getFranchiseUrl());
			}
			
			jObj.put("from_Date", sdf.format(fromDate));
			jObj.put("to_Date", sdf.format(toDate));
			
			String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE,"Customer Data:: "+jsonString);
			
			
			return jsonString;
		}
		/** Ends **/
		
			@Override
		public String updateServices(long companyId, Date startDate, Date endDate ,int customerId, String branch) {
			// TODO Auto-generated method stub
			
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue"); 
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "UpdateServices")
					.param("companyId", companyId+"").param("fromDate", startDate+"")
					.param("toDate", endDate+"").param("customerId", customerId+"").param("branch", branch));
			return "Success";
		
	}
		@Override
		public ArrayList<ProductGroupList> getCombinedMaterialList(
				long companyId, ArrayList<Integer> serviceIdList, ArrayList<Integer> contractIdList) {
			// TODO Auto-generated method stub
			List<ServiceProject> serviceProjectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).
														filter("serviceId IN", serviceIdList).filter("contractId IN", contractIdList).list();
			
			logger.log(Level.SEVERE, " first list : " + serviceProjectList.size());
			Map<String , ServiceProject> projectMap =new HashMap<String , ServiceProject>();
			for(ServiceProject project : serviceProjectList){
				if(project.getserviceId() != 0){
					projectMap.put(project.getserviceId()+"-"+project.getCount(), project);
				}		
			}
			List<Service> serviceList = ofy().load().type(Service.class).filter("count IN", serviceIdList).filter("companyId", companyId).filter("contractCount IN", contractIdList).list();
			logger.log(Level.SEVERE, "second List : " + serviceList.size());
			List<ServiceProject> serviceProjectList1 = new ArrayList<ServiceProject>();
			for(Service ser : serviceList){
				logger.log(Level.SEVERE, "pr id : " + ser.getProjectId());
				 if(projectMap.containsKey(ser.getCount()+"-"+ser.getProjectId())){
					 serviceProjectList1.add(projectMap.get(ser.getCount()+"-"+ser.getProjectId()));
					 projectMap.remove(ser.getCount()+"-"+ser.getProjectId());			
				 }
			}
			
			for(Map.Entry<String , ServiceProject> entry : projectMap.entrySet()){
				logger.log(Level.SEVERE, " delete project id : " + entry.getKey());
				ofy().delete().entity(entry.getValue());
			}
			logger.log(Level.SEVERE, " removed map size : " + projectMap.size());
			logger.log(Level.SEVERE, " added list : " + serviceProjectList1.size());
			Map<String ,ProductGroupList> technicianTotalMaterialMap = new HashMap<String , ProductGroupList>();
			for(ServiceProject serProject : serviceProjectList1){
				if(serProject != null && serProject.getProdDetailsList() != null && serProject.getProdDetailsList().size() >0){
					for(ProductGroupList group : serProject.getProdDetailsList()){
						String key = group.getName()+"-"+serProject.getTechnicians().get(0).getFullName();
						
						if(technicianTotalMaterialMap.containsKey(key)){
							ProductGroupList productGroupList = technicianTotalMaterialMap.get(key);
							productGroupList.setProActualQty(productGroupList.getProActualQty() + group.getProActualQty());
							productGroupList.setPlannedQty(productGroupList.getPlannedQty() + group.getPlannedQty());
							productGroupList.setReturnQuantity(productGroupList.getReturnQuantity() + group.getReturnQuantity());
							productGroupList.setQuantity(productGroupList.getQuantity() + group.getQuantity());
							technicianTotalMaterialMap.put(key, productGroupList);
						}else{
							ProductGroupList productGroupList = new ProductGroupList();
							productGroupList.setName(group.getName());
							productGroupList.setProduct_id(group.getProduct_id());
							productGroupList.setCode(group.getCode());
							productGroupList.setCreatedBy(serProject.getTechnicians().get(0).getFullName());
							productGroupList.setProActualQty(group.getProActualQty());
							productGroupList.setPlannedQty(group.getPlannedQty());
							productGroupList.setQuantity(group.getQuantity());
							productGroupList.setReturnQuantity(group.getReturnQuantity());
							productGroupList.setUnit(group.getUnit());		
							productGroupList.setParentWarentwarehouse(group.getParentWarentwarehouse());
							productGroupList.setParentStorageLocation(group.getParentStorageLocation());
							productGroupList.setParentStorageBin(group.getParentStorageBin());
							productGroupList.setWarehouse(group.getWarehouse());
							productGroupList.setStorageLocation(group.getStorageLocation());
							productGroupList.setStorageBin(group.getStorageBin());
							technicianTotalMaterialMap.put(key, productGroupList);
						}
					}
				}
			}
			ArrayList<ProductGroupList> productList = new ArrayList<ProductGroupList>();
			for(Map.Entry<String,  ProductGroupList> entry : technicianTotalMaterialMap.entrySet()){
				productList.add(entry.getValue());
			}
			return productList;
		}



		@Override
		public String createMMNForTechnician(long companyId,
				ArrayList<ProductGroupList> productList,String user,ArrayList<Integer> serviceIdList) {
			// TODO Auto-generated method stub
			Gson gson = new Gson();
			String str = gson.toJson(serviceIdList);
			
			String str1 = gson.toJson(productList);
			
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "createTechnicianMMN")
					.param("companyId", companyId + "")
					.param("user", user )
					.param("serviceIdList", str )
					.param("productList", str1));
			
			
//			logger.log(Level.SEVERE , "product list :" + productList.size());
//			List<Service> sList = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceIdList).list();
//			List<Service> newSList = new ArrayList<Service>();
//			boolean noMMNFlag = false;
//			String branch = "";
//			noMMNFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "NoTechnicianwiseMMN", companyId);
//			for(Service s : sList){
//					branch = s.getBranch();
//					break;
//			}
////			ofy().save().entities(newSList);
//			Map<String , ArrayList<ProductGroupList>> technicianwiseMap = new HashMap<String , ArrayList<ProductGroupList>>();
//			ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
//			List<String> techList = new ArrayList<String>();
//			if(productList != null && productList.size() > 0 && !noMMNFlag){
//			for(ProductGroupList group : productList){
//				if(technicianwiseMap.containsKey(group.getCreatedBy())){
//					list = technicianwiseMap.get(group.getCreatedBy());
//					
//					list.add(group);
//					technicianwiseMap.put(group.getCreatedBy(), list);
//				}else{
//					list = new ArrayList<ProductGroupList>();
//					list.add(group);
//					technicianwiseMap.put(group.getCreatedBy(), list);
//					techList.add(group.getCreatedBy());
//				}
//			}
//			
//			for(Map.Entry<String , ArrayList<ProductGroupList>> entry : technicianwiseMap.entrySet()){
//				logger.log(Level.SEVERE , "key:" + entry.getKey());
//				logger.log(Level.SEVERE , "value" + entry.getValue().size());
//			}
//			List<String> statusList=new ArrayList<String>();
//			statusList.add(Service.SERVICESTATUSSCHEDULE);
//			statusList.add(Service.SERVICESTATUSRESCHEDULE);
//			
//			List<Service> serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("employee IN", techList).filter("isServiceScheduled", true).filter("status IN", statusList).list();
//			List<Integer> serIdList = new ArrayList<Integer>();
////			String branch = "";
//			for(Service ser : serviceList){
//				if(ser.isServiceScheduled() && !(ser.getServiceNumber().equals("") || ser.getServiceNumber() == null)){
//					if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
//						
//					}else{
//						serIdList.add(ser.getCount());
//					}
//				}
//			//	branch = ser.getBranch();
//			}
//			for(Integer c : serIdList){
//				logger.log(Level.SEVERE ,"servoce ids"+ c);
//			}
//			List<ServiceProject> projectList = new ArrayList<ServiceProject>();
//			if(serIdList != null && serIdList.size() > 0){
//			 projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("serviceId IN", serIdList).list();
//			}
//			Map<String , Double> requiredQuntityMap = new HashMap<String , Double>();
// 			Map<String , Double> availableQuntityMap = new HashMap<String , Double>();
//			for(ServiceProject serProject : projectList){
//				if(serProject != null && serProject.getProdDetailsList() != null && serProject.getProdDetailsList().size() >0){
//					for(ProductGroupList group : serProject.getProdDetailsList()){
//						double value = 0;
//						String key = group.getName()+"-"+group.getStorageBin().trim();
//						if(requiredQuntityMap.containsKey(key)){
//							value = requiredQuntityMap.get(key);
//							requiredQuntityMap.put(key ,value + group.getProActualQty());
//							logger.log(Level.SEVERE ,"required map*****************"+ group.getProActualQty() + " "+key);
//						}else{
//							requiredQuntityMap.put(key ,group.getProActualQty());
//							ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class)
//									.filter("companyId", companyId)
//									.filter("prodname",group.getName())
//									.filter("warehousename", group.getWarehouse())
//									.filter("storagelocation", group.getStorageLocation())
//									.filter("storagebin", group.getStorageBin().trim()).first().now();
//							if(prodInvDtls != null){
//								availableQuntityMap.put(group.getName()+"-"+group.getStorageBin().trim(), prodInvDtls.getAvailableqty());
//							}else{
//								availableQuntityMap.put(group.getName()+"-"+group.getStorageBin().trim(),0.0);
//							}
//							logger.log(Level.SEVERE ,"available map*****************"+availableQuntityMap.get(group.getName()+"-"+group.getStorageBin().trim()));
//						}
//					}
//				}
//			}
//			
//			for(ProductGroupList group : productList){
//				double value = 0;
//				String key = group.getName()+"-"+group.getStorageBin().trim();
////				if(requiredQuntityMap.containsKey(key)){
////					value = requiredQuntityMap.get(key);
////					requiredQuntityMap.put(key ,value + group.getProActualQty());
////					logger.log(Level.SEVERE ,"required map*****************"+ group.getProActualQty() + " "+key);
////				}else{
////					requiredQuntityMap.put(key ,group.getProActualQty());
//					ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class)
//							.filter("companyId", companyId)
//							.filter("prodname",group.getName())
//							.filter("warehousename", group.getWarehouse())
//							.filter("storagelocation", group.getStorageLocation())
//							.filter("storagebin", group.getStorageBin().trim()).first().now();
//					if(prodInvDtls != null){
//						availableQuntityMap.put(group.getName()+"-"+group.getStorageBin().trim(), prodInvDtls.getAvailableqty());
//					}else{
//						availableQuntityMap.put(group.getName()+"-"+group.getStorageBin().trim(),0.0);
//					}
//					logger.log(Level.SEVERE ,"available map*****************"+availableQuntityMap.get(group.getName()+"-"+group.getStorageBin().trim()));
////				}
//			}
//			for(Map.Entry<String, ArrayList<ProductGroupList>> entry : technicianwiseMap.entrySet()){
//			ArrayList<ProductGroupList> groupList = entry.getValue();
//			String technician = entry.getKey();
//			MaterialMovementNote mmn=new MaterialMovementNote();
//			mmn.setServiceId(0);
//			mmn.setCompanyId(companyId);		
//			mmn.setMmnDate(new Date());
//		    mmn.setBranch(branch);
//			mmn.setMmnTransactionType("TransferOUT");
//			mmn.setTransDirection("TRANSFEROUT");
//			mmn.setEmployee(technician);
//			mmn.setMmnPersonResposible(user);
//			mmn.setApproverName(user);
//			mmn.setCreatedBy(user);
//			mmn.setStatus(MaterialMovementNote.REQUESTED);
//			mmn.setMmnTitle("Technician Material Transfer");
//			mmn.setCreationDate(new Date());
//			logger.log(Level.SEVERE ,"TransToStorBin******************"+groupList.get(0).getStorageBin().trim());
//			mmn.setTransToStorBin(groupList.get(0).getStorageBin().trim());
//			logger.log(Level.SEVERE ,"TransToStorLoc*****************"+groupList.get(0).getStorageLocation());
//			mmn.setTransToStorLoc(groupList.get(0).getStorageLocation());
//			logger.log(Level.SEVERE ,"TransToWareHouse*************"+groupList.get(0).getWarehouse());
//			mmn.setTransToWareHouse(groupList.get(0).getWarehouse());
//			logger.log(Level.SEVERE ,"from popup product*************"+groupList.get(0).getName());
//		    ArrayList<MaterialProduct> materialProductList = new ArrayList<MaterialProduct>();
//		    for(ProductGroupList m : groupList){
//		    	MaterialProduct mp = new MaterialProduct();
//		    	Company c = new Company();
//		    	mp.setCompanyId(c.getCompanyId());
//		    	mp.setMaterialProductId(m.getProduct_id());
//		    	mp.setMaterialProductCode(m.getCode());
//		    	mp.setMaterialProductName(m.getName());
//		    
//		    	mp.setMaterialProductStorageBin(m.getParentStorageBin());
//		    	mp.setMaterialProductStorageLocation(m.getParentStorageLocation());
//		    	mp.setMaterialProductWarehouse(m.getParentWarentwarehouse());
//		    	mp.setMaterialProductUOM(m.getUnit());
//		    	String key = m.getName()+"-"+m.getStorageBin();
//		    	double quantity = 0;
//		    	logger.log(Level.SEVERE ,"Transfer quamt******************"+quantity +"  "+m.getName());
//		    	double value1 = 0 , value2 = 0;
//		    	if(requiredQuntityMap.containsKey(key)){
//		    		value1 = requiredQuntityMap.get(key);
//		    	}
//		    	logger.log(Level.SEVERE ,"value1******************"+value1);
//		    	if(availableQuntityMap.containsKey(key)){
//		    		value2 = availableQuntityMap.get(key);
//		    	}	
//		    	logger.log(Level.SEVERE ,"value2******************"+value2);
//		    	quantity = value1 + m.getProActualQty() - value2;
//		    	if(quantity <0){
//		    		quantity = 0;
//		    	}
//		    	logger.log(Level.SEVERE ,"Transfer quamt******************"+quantity);
//		    	mp.setMaterialProductRequiredQuantity(quantity);
//		    
//		    	materialProductList.add(mp);
//		    }
//		    System.out.println("product list*************"+list.toString());
////		    for(Service s : sList){
////				s.setServiceScheduled(true);
////				s.setServiceNumber(DateUtility.getDateWithTimeZone("IST", s.getServiceDate())+"-"+s.getTechnicians());
////				newSList.add(s);
////			}
////			ofy().save().entities(newSList);
//			mmn.setSubProductTableMmn(materialProductList);
//			mmn.setServiceIdList(serviceIdList);
//			mmn.setMmnCategory("Android");
//			SuperModel model = (SuperModel) mmn;
//			saveAndApproveDocument(model, 0+"", "", AppConstants.MMN);
//		}
//			}
//			for(Service s : sList){
//				s.setServiceScheduled(true);
//				s.setServiceNumber(DateUtility.getDateWithTimeZone("IST", s.getServiceDate())+"-"+s.getTechnicians());
//				newSList.add(s);
//			}
//			ofy().save().entities(newSList);
			return null;
		}
		
		
		public void updateProductCode(long companyId) {
			// TODO Auto-generated method stub
			String taskName="UpdateProductCode"+"$"+companyId;
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		}
		
		/**
		 * Date 30-07-2019 by Vijay for NBHC CCPM Service revenue loss report for Approved contracts only
		 */
		@Override
		public String getCancelledServiceOfApprovedContract(long companyId, Date fromDate, Date toDate,
				ArrayList<String> brnachlist) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			try {
				Calendar cal=Calendar.getInstance();
				cal.setTime(toDate);
				cal.add(Calendar.DATE, 0);
				Date ToDate=null;
				try {
					ToDate=dateFormat.parse(dateFormat.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
					cal.set(Calendar.MILLISECOND,999);
					ToDate=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				Calendar cal2=Calendar.getInstance();
				cal2.setTime(fromDate);
				cal2.add(Calendar.DATE, 0);
				Date FromDate=null;
				try {
					FromDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
					cal2.set(Calendar.HOUR_OF_DAY,00);
					cal2.set(Calendar.MINUTE,00);
					cal2.set(Calendar.SECOND,00);
					cal2.set(Calendar.MILLISECOND,000);
					FromDate=cal2.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			
			System.out.println("Company Id"+companyId);
			logger.log(Level.SEVERE, "companyId "+companyId);

			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
										.filter("cancellationDate >=", FromDate).filter("cancellationDate <=", ToDate)
										.filter("branch IN",brnachlist)
										.filter("status", Service.SERVICESTATUSCANCELLED).list();
			logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRecordsWithServiceDate", companyId)){
				List<Service> latestservicelist = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("serviceDate >=", FromDate).filter("serviceDate <=", ToDate)
						.filter("branch IN",brnachlist)
						.filter("status", Service.SERVICESTATUSCANCELLED).list();
				logger.log(Level.SEVERE, "latestservicelist size"+latestservicelist.size());

				servicelist.addAll(latestservicelist);
			}
				logger.log(Level.SEVERE, "Total servicelist "+servicelist.size());

			if(servicelist.size()!=0){
				HashSet<Integer> contractIds = new HashSet<Integer>();
				for(Service serviceEntity : servicelist){
					contractIds.add(serviceEntity.getContractCount());
				}
				ArrayList<Integer> contractIdlist = new ArrayList<Integer>(contractIds);
				List<Contract> Contractlist = ofy().load().type(Contract.class).filter("companyId", companyId)
											.filter("status", Contract.APPROVED).filter("count IN",contractIdlist).list();
				logger.log(Level.SEVERE, "Contract List"+Contractlist.size());
				
				List<Contract> contractlistTemp = ofy().load().type(Contract.class).filter("companyId", companyId)
						.filter("status", Contract.APPROVED).filter("count IN",contractIds).list();
				logger.log(Level.SEVERE, "Contract List contractlistTemp"+contractlistTemp.size());

				ArrayList<Service> updatedServicelist = new ArrayList<Service>();
				if(Contractlist.size()!=0){
					for(Contract contract : Contractlist){
						for(Service service : servicelist){
							if(contract.getCount() == service.getContractCount()){
								updatedServicelist.add(service);
							}
						}
					}
					
					CsvWriter.services = updatedServicelist;
					System.out.println("updated services list =="+updatedServicelist.size());
					return "Success";
				}
			}
			else{
				return "No Data Found";
			}
			
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Exception"+e.getLocalizedMessage());
			}
			
			return "Failed";
			
		}
		
		
		/**
		 * Date 22-07-2019 by Vijay
		 * Des :- for NBHC CCPM Contract upload. first it will validate the data 
		 * if excel data found ok then it will send data to task queue for creating contracts.
		 * it will also send request for approval.
		 */

		@Override
		public String contractUploadService(String oprationName,long companyId) {
			if(oprationName.trim().equals("Contract Upload")){
				return reactOnContractUplaod(companyId);
			}
			else{
				return "Invalid Operation";
			}
		
		}


	private String reactOnContractUplaod(long companyId) {
//		if (DocumentUploadTaskQueue.contractUploadBlobKey == null) {
			ArrayList<String> exceldatalist = readExcelAndGetArraylist(DocumentUploadTaskQueue.contractUploadBlobKey);
			if (exceldatalist == null || exceldatalist.size() == 0) {
				return "Unable read excel data please try again!";
			}
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Customer Id")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Customer Name")
					&& exceldatalist.get(2).trim().equalsIgnoreCase("Client Code")
					&& exceldatalist.get(3).trim().equalsIgnoreCase("Branch")
					&& exceldatalist.get(4).trim().equalsIgnoreCase("Sales Person")
					&& exceldatalist.get(5).trim().equalsIgnoreCase("Payment Method")
					&& exceldatalist.get(6).trim().equalsIgnoreCase("Approver Name")
					&& exceldatalist.get(7).trim().equalsIgnoreCase("Contract Group")
					&& exceldatalist.get(8).trim().equalsIgnoreCase("Contract Category")
					&& exceldatalist.get(9).trim().equalsIgnoreCase("Contract Type")
					&& exceldatalist.get(10).trim().equalsIgnoreCase("Contract Date")
					&& exceldatalist.get(11).trim().equalsIgnoreCase("Start Date")
					&& exceldatalist.get(12).trim().equalsIgnoreCase("Contract Duration")
					&& exceldatalist.get(13).trim().equalsIgnoreCase("Product Code")
					&& exceldatalist.get(14).trim().equalsIgnoreCase("Product Sr No")
					&& exceldatalist.get(15).trim().equalsIgnoreCase("No.Of. Services")
					&& exceldatalist.get(16).trim().equalsIgnoreCase("Unit")
					&& exceldatalist.get(17).trim().equalsIgnoreCase("UOM")
					&& exceldatalist.get(18).trim().equalsIgnoreCase("Price")
					&& exceldatalist.get(19).trim().equalsIgnoreCase("Customer Branch")
					&& exceldatalist.get(20).trim().equalsIgnoreCase("Service Address Line 1")
					&& exceldatalist.get(21).trim().equalsIgnoreCase("Service Address Line 2")
					&& exceldatalist.get(22).trim().equalsIgnoreCase("City(Service Address)")
					&& exceldatalist.get(23).trim().equalsIgnoreCase("Payment Terms")) {
				return reactonContractUpload(exceldatalist, companyId, DocumentUploadTaskQueue.contractUploadBlobKey);
			} else {
				return "Invalid Excel Sheet";
			}

//		} else {
//			return "Someone is uploading contracts please try after some time!";
//		}

	}



	private String reactonContractUpload(ArrayList<String> exceldatalist, long companyId, BlobKey blobkey) {
		
		try {
		if(globalBranchlist==null){
			globalBranchlist = ofy().load().type(Branch.class).filter("companyId", companyId).list();
		}
		if(globalEmployeelist==null){
			globalEmployeelist = ofy().load().type(Employee.class).filter("companyId", companyId).list();
		}
		if(globalConfig==null){
			globalConfig = ofy().load().type(ConfigCategory.class).filter("companyId",companyId).filter("internalType", 25).list();
		}
		System.out.println("===="+exceldatalist.size());
		int contractcount = exceldatalist.size()/24;
		if(contractcount>3000){
			return "Contract uploading excel sheet should not be more than 3000 records!";	
		}
		if(globalCitylist==null){
			globalCitylist = ofy().load().type(City.class).filter("companyId",companyId).list();
		}
		
		HashSet<String> hsprodCode=new HashSet<String>();
		HashSet<Integer> hsclientCode = new HashSet<Integer>();
		HashSet<String> hscustomerBranch = new HashSet<String>();

		for(int i=24;i<exceldatalist.size();i+=24){
			hsclientCode.add(Integer.parseInt(exceldatalist.get(i)));
			hsprodCode.add(exceldatalist.get(i+13));
			hscustomerBranch.add(exceldatalist.get(i+19));

		}
		ArrayList<Integer> clientcodelist = new ArrayList<Integer>(hsclientCode);
		List<Customer> customerlist = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count IN", clientcodelist).list();
		
		ArrayList<String> productCodelist = new ArrayList<String>(hsprodCode);
		List<ServiceProduct> serviceproductlist = ofy().load().type(ServiceProduct.class)
											.filter("companyId", companyId).filter("productCode IN", productCodelist).list();
		
		ArrayList<String> strcustomerBrnach = new ArrayList<String>(hscustomerBranch);
		
		List<CustomerBranchDetails> globalCustomerBranchDetailsList = ofy().load().type(CustomerBranchDetails.class)
										.filter("companyId", companyId).filter("buisnessUnitName IN", strcustomerBrnach).list();
		logger.log(Level.SEVERE,"globalCustomerBranchDetailsList"+globalCustomerBranchDetailsList.size());
		
		for(int i=24;i<exceldatalist.size();i+=24){
			System.out.println("i+44"+(i+44));
			if(exceldatalist.get(i).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Customer Id code should not be NA!";
			}
			if(!exceldatalist.get(i).trim().equalsIgnoreCase("NA")){
				int customerId = Integer.parseInt(exceldatalist.get(i).trim());
				if(!validateCustomer(customerId,customerlist)){
					updateBlobKeyValue();
					return "Customer does not exist in the system! Please check Customer Id! "+exceldatalist.get(i);
				}
			}
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableContractUploadClientCodeMandatory", companyId)){
				if(exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")){
					updateBlobKeyValue();
					return "Client Code not be NA!";
				}
			}
			
			if(exceldatalist.get(i+3).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Branch should not be NA!";
			}else{
				boolean branchflag = false;
				for(Branch branch : globalBranchlist){
					if(branch.getBusinessUnitName().trim().equals(exceldatalist.get(i+3).trim())){
						branchflag=true;
						break;
					}
				}
				if(branchflag==false){
					updateBlobKeyValue();
					return "Branch does not exist in the system! Please check Branch! "+exceldatalist.get(i+3).trim();
				}
			}
			if(exceldatalist.get(i+4).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Sales Person should not be NA!";
			}
			else{
				boolean employeeflag = false;
				for(Employee employee : globalEmployeelist){
					if(employee.getFullname().trim().equals(exceldatalist.get(i+4).trim())){
						employeeflag=true;
						break;
					}
				}
				if(employeeflag==false){
					updateBlobKeyValue();
					return "Sales Person does not exist in the system! Please check Employee! "+exceldatalist.get(i+4).trim();
				}
			}
			if(exceldatalist.get(i+5).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Payment method should not be NA!";
			}
			if(exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
				boolean employeeflag = false;
				for(Employee employee : globalEmployeelist){
					if(employee.getFullname().trim().equals(exceldatalist.get(i+6).trim())){
						employeeflag=true;
						break;
					}
				}
				if(employeeflag==false){
					updateBlobKeyValue();
					return "Approver Name does not exist in the system! Please check Employee! "+exceldatalist.get(i+6).trim();
				}
			}
			if(exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Contract Group should not be NA!";
			}
			if(exceldatalist.get(i+8).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Contract Category should not be NA!";
			}
			if(exceldatalist.get(i+9).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Contract Type should not be NA!";
			}
			if(exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Contract Date should not be NA!";
			}
			else{
				try {
					Date date = dateFormat.parse(exceldatalist.get(i+10).trim());
				} catch (Exception e) {
					updateBlobKeyValue();
					return "Contract date should be in 'dd/mm/yyyy' format only.";
				}
			}
			if(exceldatalist.get(i+11).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Contract Start Date should not be NA!";
			}
			else{
				try {
					Date date = dateFormat.parse(exceldatalist.get(i+11).trim());
				} catch (Exception e) {
					updateBlobKeyValue();
					return "Contract start date should be in 'dd/mm/yyyy' format only.";
				}
			}
			if(exceldatalist.get(i+12).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Contract Duration should not be NA!";
			}
			if(exceldatalist.get(i+13).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Product Code should not be NA!";
			}
			else{
				SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId)
										.filter("productCode", exceldatalist.get(i+13).trim()).first().now();
				if(!validateServiceProduct(exceldatalist.get(i+13),serviceproductlist)){
					updateBlobKeyValue();
					return "Product Code does not exist in the system! "+exceldatalist.get(i+13);
				}
			}
			if(exceldatalist.get(i+14).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Product Sr No should not be NA!";
			}
			if(exceldatalist.get(i+15).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "No of services should not be NA!";
			}
			if(exceldatalist.get(i+17).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "UOM should not be NA!";
			}
			if(exceldatalist.get(i+18).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Price should not be NA!";
			}
			if(exceldatalist.get(i+19).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Customer Branch should not be NA!";
			}
			if(!validateCustomerBranch(exceldatalist.get(i+19),globalCustomerBranchDetailsList,exceldatalist.get(i))){
				updateBlobKeyValue();
				return "Customer Branch does not exist in the system please create first! "+exceldatalist.get(i+19);
			}
			if(!exceldatalist.get(i+22).trim().equalsIgnoreCase("NA")){
				if(!validateCity(exceldatalist.get(i+22).trim())){
					updateBlobKeyValue();
					return "City does not exist in the system! "+exceldatalist.get(i+22);
				}
			}
			
			if(exceldatalist.get(i+23).trim().equalsIgnoreCase("NA")){
				updateBlobKeyValue();
				return "Payment Terms should not be NA!";
			}
			else{
				boolean paymentTermsFlag = false;
				for(ConfigCategory configcategory : globalConfig){
					if(configcategory.getCategoryName().equals(exceldatalist.get(i+23).trim())){
						paymentTermsFlag = true;
						break;
					}
				}
				if(paymentTermsFlag==false){
					updateBlobKeyValue();
					return "Payment Terms does not exist in the system! "+exceldatalist.get(i+23);
				}
			}
		
		}
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
		
		try {
			String blobKey = blobkey.getKeyString();
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", "Contract Upload").param("companyId", companyId+"").param("contractUploadblobkey", blobKey));
			
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception payment Date follow up update =="+e.getMessage());
			return "Unbale to uplad data please try again.";
		}
		
		
		return "Excel uploaded Sucessfully! Contract Upload Process Started!";
	}



	private boolean validateCity(String cityName) {
		for(City city : globalCitylist){
			if(city.getCityName().equals(cityName)){
				return true;
			}
		}
		return false;
	}



	private boolean validateCustomerBranch(String customerbranch, List<CustomerBranchDetails> globalCustomerBranchDetailsList, String customerid) {
		int customerId = Integer.parseInt(customerid);
		for(CustomerBranchDetails customerBranch : globalCustomerBranchDetailsList){
			if(customerBranch.getBusinessUnitName().trim().equals(customerbranch.trim()) &&
					customerBranch.getCinfo().getCount()==customerId){
				return true;
			}
		}
		return false;
	}



	private boolean validateServiceProduct(String productCode, List<ServiceProduct> serviceproductlist) {
		for(ServiceProduct product : serviceproductlist){
			if(productCode.equals(product.getProductCode())){
				return true;
			}
		}
		return false;
	}



	private boolean validateCustomer(int customerId, List<Customer> customerlist) {
		for(Customer customer : customerlist){
			if(customer.getCount()==customerId){
				return true;
			}
		}
		return false;
	}



	private void updateBlobKeyValue() {
		DocumentUploadTaskQueue.contractUploadBlobKey=null;
	}



/**
 * Date 22-07-2019 
 * Des :- Reading excel sheet and retuning aaraylist
 * @return
 */
	private ArrayList<String> readExcelAndGetArraylist(BlobKey blobkey) {

		ArrayList<String> exceldatalist = new ArrayList<String>();
		try {
			logger.log(Level.SEVERE, "Reading excel ");
			logger.log(Level.SEVERE, "Blobkey value" + blobkey);

			long i;
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {

							// i = (long) cell.getNumericCellValue();
							// logger.log(Level.SEVERE, "Value : "+i);
							logger.log(Level.SEVERE, "Value : " + cell.getNumericCellValue());
							// exceldatalist.add(cell.getNumericCellValue() +
							// "");
							DecimalFormat decimalFormat = new DecimalFormat("#########.##");
							int value1 = 0, value2 = 0;
							String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
							if (array.length > 0) {
								// value1 = Integer.parseInt(array[0]);
							}
							if (array.length > 1) {
								value2 = Integer.parseInt(array[1]);
							}
							if (value2 == 0) {
								i = (long) cell.getNumericCellValue();
								exceldatalist.add(i + "");
							} else {
								exceldatalist.add(cell.getNumericCellValue() + "");
							}

						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:
					}
				}
			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist.size());

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return exceldatalist;
	}
	
	
	/** 
	 * Date 22-05-2019
	 * @author Vijay Chougule 
	 * Des :- NBHC CCPM Complaint Screen service creation
	 */
	
	@Override
	public String createComplaintService(Complain complain, int complaintID) {
		Service service = new Service();
		service.setCompanyId(complain.getCompanyId());
		service.setPersonInfo(complain.getPersoninfo());
		ServiceProduct serviceProdEntity = ofy().load().type(ServiceProduct.class).filter("companyId",complain.getCompanyId())
											.filter("count", complain.getPic().getProdID()).first().now();
		if(serviceProdEntity!=null){
			service.setProduct(serviceProdEntity);
		}
		service.setBranch(complain.getBranch());
		service.setServiceBranch(complain.getCustomerBranch());
		service.setStatus(Service.SERVICESTATUSSCHEDULE);
		service.setServiceType("Follow UP Service");
		service.setServiceDate(complain.getNewServiceDate());
			CustomerBranchDetails customerbranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", complain.getCompanyId())
													.filter("buisnessUnitName", complain.getCustomerBranch())
													.filter("cinfo.count", complain.getPersoninfo().getCount()).first().now();
			if(customerbranch!=null){
				service.setAddress(customerbranch.getAddress());
			}
			else{
				Customer customer = ofy().load().type(Customer.class).filter("companyId", complain.getCompanyId())
									.filter("count", complain.getPersoninfo().getCount()).first().now();
				if(customer!=null){
					service.setAddress(customer.getSecondaryAdress());
				}
			}
			
		if(complain.getServicedetails()!=null && complain.getServicedetails().size()!=0){
			service.setContractCount(complain.getServicedetails().get(0).getContractId());
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", service.getCompanyId())
										.filter("count", service.getContractCount()).first().now();
			if(contractEntity!=null){
				service.setContractStartDate(contractEntity.getStartDate());
				service.setContractEndDate(contractEntity.getEndDate());
			}
		}
		service.setTicketNumber(complaintID);
		GenricServiceImpl impl = new GenricServiceImpl();
		ReturnFromServer server = new ReturnFromServer();
		server = impl.save(service);
		impl.save(service);
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnableCreateAssementForComplaintService", service.getCompanyId())){
			createAssessment(service,server.count,complaintID,service.getBranch());
		}

		/**
		 * Date 12-08-2019 by Vijay for When complaint raised then send email
		 */
		sendEmailForComplaintRegister(complain,complaintID);
		
		return "Success";
	}


	private void sendEmailForComplaintRegister(Complain complain, int complaintID) {
		Company company = ofy().load().type(Company.class).filter("companyId", complain.getCompanyId()).first().now();

		String mailSub="Complaint Id -"+complaintID;
		String mailMsg= "";
			StringBuilder builder = new StringBuilder();
			builder.append("<!DOCTYPE html>");
			builder.append("<html lang=\"en\">");
			builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
			builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

			builder.append("Dear Sir/Madam,");
			builder.append("<br>");
			builder.append("<br>");
			builder.append("Your Complaint has been registered. Your Complaint Id is "+complaintID+"</br>"+"</br>"+"</br>");
			builder.append("<br>");
			builder.append("<br>");
			builder.append("<br>");
			
			builder.append("</br></br></br>");
			// Company Details
			builder.append("<br>");
			builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
			builder.append("<table>");

			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Address : ");
			builder.append("</td>");
			builder.append("<td>");
			if (!company.getAddress().getAddrLine2().equals("")&& company.getAddress().getAddrLine2() != null){
				builder.append(company.getAddress().getAddrLine1() + " "+ company.getAddress().getAddrLine2() + " "
						+ company.getAddress().getLocality());
			}else{
				builder.append(company.getAddress().getAddrLine1() + " "
						+ company.getAddress().getLocality());
			}
			builder.append("</td>");
			builder.append("</tr>");

			builder.append("<tr>");
			builder.append("<td>");
			builder.append("</td>");
			builder.append("<td>");
			
			if (!company.getAddress().getLandmark().equals("")&& company.getAddress().getLandmark() != null){
				builder.append(company.getAddress().getLandmark() + ","+ company.getAddress().getCity() + ","
						+ company.getAddress().getPin()+ company.getAddress().getState() + ","
						+ company.getAddress().getCountry());
			}else{
				builder.append(company.getAddress().getCity() + ","+ company.getAddress().getPin() + ","
						+ company.getAddress().getState() + ","+ company.getAddress().getCountry());
			}
			
			builder.append("</td>");
			builder.append("</tr>");

			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Website : ");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(company.getWebsite());
			builder.append("</td>");
			builder.append("</tr>");

			builder.append("<tr>");
			builder.append("<td>");
			builder.append("Phone No : ");
			builder.append("</td>");
			builder.append("<td>");
			builder.append(company.getCellNumber1());
			builder.append("</td>");
			builder.append("<tr>");

			builder.append("</tr>");
			builder.append("</table>");
			builder.append("</body>");
			builder.append("</html>");

			String contentInHtml = builder.toString();
			System.out.println(contentInHtml);
			
			builder.append("</body>");
			builder.append("</html>");
			mailMsg = builder.toString();
		System.out.println("mailMsg"+mailMsg);
		if(complain.getEmail()!=null && !complain.getEmail().equals("")){		
			Email email = new Email();
			String fromEmail = complain.getEmail();
			ArrayList<String> toList = new ArrayList<String>();
			toList.add(fromEmail);
			/**
			 * Date 30-09-2019 By Vijay
			 * Des :- NBHC CCPM Complaint creation send email cc to Technical Manager
			 */
			ArrayList<String> cclist = new ArrayList<String>();
			List<Employee> employeeList = ofy().load().type(Employee.class).filter("companyId", company.getCompanyId())
					.filter("roleName", "Technical Manager").list();
			if(employeeList.size()!=0){
				for(Employee emp : employeeList){
					if(emp.getEmail()!=null)
					cclist.add(emp.getEmail());
				}
			}
			logger.log(Level.SEVERE, "Email CC "+cclist);

			logger.log(Level.SEVERE, "Email Sending method");
			/**
			 * @author Anil , Date : 10-04-2020
			 * added display name for from email id
			 * ,company.getDisplayNameForCompanyEmailId()
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",company.getCompanyId())){
				logger.log(Level.SEVERE," company email --" +fromEmail +"  to email  " +toList);
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(fromEmail, toList, cclist, null, mailSub, mailMsg, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
			}
			else{
				email.sendMailWithGmail(company.getEmail(), toList, cclist, null, mailSub, mailMsg, "text/html", null, null, null);
			}
		}
		
	}

	private void createAssessment(Service service, int serviceId, int complaintId, String branch) {
		AssesmentReport assesmentreport = new AssesmentReport();
		assesmentreport.setCinfo(service.getPersonInfo());
		assesmentreport.setCompanyId(service.getCompanyId());
		assesmentreport.setServiceId(serviceId);
		assesmentreport.setComplaintId(complaintId);
		assesmentreport.setStatus(AssesmentReport.CREATED);
		assesmentreport.setBranch(branch);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(assesmentreport);
	}

	@Override
	public String markCompleteComplaint(long companyId, int ticketId) {
		Complain comlainEntity = ofy().load().type(Complain.class).filter("companyId",companyId).filter("count",ticketId).first().now();
		if(comlainEntity!=null){
			comlainEntity.setCompStatus("Completed");
			ofy().save().entity(comlainEntity);
			logger.log(Level.SEVERE,"Complaint saved sucessfully");
			return "Success";
		}
		return "Failed";
	}


	/**
	 * Date 27-08-2019 by Vijay 
	 * Des :- NBHC CCPM Rate contracts Service value updation for Contract PNL Report 
	 */
	@Override
	public String updateServiceValueForRateContract(Date fromDate, Date toDate,String branch,long companyId) {
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
		String strfromDate = dateformat.format(fromDate);
		String strToDate = dateformat.format(toDate);
		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "updateServiceValueForRateContracts")
				.param("companyId", companyId + "")
				.param("fromDate", strfromDate )
				.param("toDate", strToDate )
				.param("branch", branch));

		return "Updation Process started";
	}



	@Override
	public String getLockSealDeviationReport(long companyId, Date fromDate, Date toDate, ArrayList<String> brnachlist) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(toDate);
			cal.add(Calendar.DATE, 0);
			Date ToDate = null;
			try {
				ToDate = dateFormat.parse(dateFormat.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				cal.set(Calendar.MILLISECOND, 999);
				ToDate = cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(fromDate);
			cal2.add(Calendar.DATE, 0);
			Date FromDate = null;
			try {
				FromDate = dateFormat.parse(dateFormat.format(cal2.getTime()));
				cal2.set(Calendar.HOUR_OF_DAY, 00);
				cal2.set(Calendar.MINUTE, 00);
				cal2.set(Calendar.SECOND, 00);
				cal2.set(Calendar.MILLISECOND, 000);
				FromDate = cal2.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}

			System.out.println("Company Id" + companyId);
			logger.log(Level.SEVERE, "companyId " + companyId);

			List<SerialNumberDeviation> serialNumberDeviationReportlist = ofy().load().type(SerialNumberDeviation.class)
					.filter("companyId", companyId).filter("transactionDate >=", FromDate)
					.filter("transactionDate <=", ToDate).filter("branch IN", brnachlist).list();
			logger.log(Level.SEVERE, "serialNumberDeviationReportlist size" + serialNumberDeviationReportlist.size());
			if(serialNumberDeviationReportlist.size()!=0){
				ArrayList<SerialNumberDeviation> arraylistserialNumberDeviation = new ArrayList<SerialNumberDeviation>();
				arraylistserialNumberDeviation.addAll(serialNumberDeviationReportlist);
				CsvWriter.serialNumberDeviation=arraylistserialNumberDeviation;
			}
			else{
				return "No Data Found";
			}
			return "Success";
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "Failed";
	}



	@Override
	public String createTaxInvoice(BillingDocument model,String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop,String approverName,String userName,boolean approveInvoiceCreatePaymentDocFlag) {
		logger.log(Level.SEVERE,"Inside Invoice creation method "+DateUtility.getDateWithTimeZone("IST", new Date()));
		String response="";
		if(model.getInvoiceCount()!=0&&model.getStatus().equals("Invoiced")){
			response="Invoice already created.";
			return response;
		}
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForEva",model.getCompanyId())){
			model.setRateContractServiceId(0);
		}
		if (model.getRateContractServiceId()!= 0){
			List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", model.getCompanyId()).filter("count", model.getRateContractServiceId()).list();
			if(serviceList!=null&&serviceList.size()!=0){
				for(Service obj:serviceList){
					if(!obj.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						response="Service for this bill is not completed. Please complete it and try again!!";
						return response;
					}
				}
			}
		}
		response=createNewInvoice(model, invoiceType, invoiceCategory, invoiceType2, invoiceGruop, approverName, userName,approveInvoiceCreatePaymentDocFlag);
		logger.log(Level.SEVERE,"RESPONSE : "+response);
		return response;
	}	


	
	
	
	
	
	
	

	
	
	public String createNewInvoice(BillingDocument model,String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop, String approverName, String userName, boolean approveInvoiceCreatePaymentDocFlag){

		boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType",model.getCompanyId());
		/*** Date 26-09-2019 by Vijay for NBHC CCPM Auto invoice process added here ***/
		boolean autoinvoiceflag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","EnableAutoInvoice", model.getCompanyId());
		logger.log(Level.SEVERE, "autoinvoiceflag "+autoinvoiceflag);
		
		Invoice inventity;
		if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
			inventity = new VendorInvoice();
		}else{
			inventity = new Invoice();
		}

		PersonInfo pinfo = new PersonInfo();
//		if (form.getTbpersonCount().getValue() != null) {
			pinfo.setCount(model.getPersonInfo().getCount());
//		}
//		if (form.getTbpersonName().getValue() != null) {
			pinfo.setFullName(model.getPersonInfo().getFullName());
//		}
//		if (form.getTbpersonCell().getValue() != null) {
			pinfo.setCellNumber(model.getPersonInfo().getCellNumber());
//		}
//		if (form.getTbpocName().getValue() != null) {
			pinfo.setPocName(model.getPersonInfo().getPocName());
//		}
			pinfo.setEmail(model.getPersonInfo().getEmail());
		if (model.getGrossValue() != 0) {
			inventity.setGrossValue(model.getGrossValue());
		}

//		if (form.getDopaytermspercent().getValue() != 0) {
			inventity.setTaxPercent(model.getArrPayTerms().get(0).getPayTermPercent());
//		}

		if (pinfo != null){
			inventity.setPersonInfo(pinfo);
		}
		
//		if(inventity.getContractCount()==0){
			inventity.setContractCount(model.getContractCount());
//		}
		
		if (model.getContractStartDate() != null) {
			inventity.setContractStartDate(model.getContractStartDate());
		}
		if (model.getContractEndDate() != null) {
			inventity.setContractEndDate(model.getContractEndDate());
		}
//		if (form.getDosalesamount().getValue()!=null)
			inventity.setTotalSalesAmount(model.getTotalSalesAmount());
		
//		List<BillingDocumentDetails> billtablelis = form.billingDocumentTable.getDataprovider().getList();
//		ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
//		billtablearr.addAll(billtablelis);
		inventity.setArrayBillingDocument(model.getArrayBillingDocument());
		
//		System.out.println("MULTIPLE CONTRAT STATUS ::: "+isMultipleContractBilling());
		if(isMultipleContractBilling(model.getArrayBillingDocument())){
			inventity.setMultipleOrderBilling(true);
			/**
			 * @author Anil
			 * @since 13-08-2020
			 * Earlier for multicontract bill we were setting contract count as zero which causes error at the 
			 * time of invoice printing
			 * So On Approval of Nitin sir and vaishali commenting this code
			 */
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForOrionPestSolutions",model.getCompanyId())==false){
//				inventity.setContractCount(0);
//			}
			inventity.setRemark(model.getRemark());
		}
		
//		if (form.getDototalbillamt().getValue() != null)
			inventity.setTotalBillingAmount(model.getTotalBillingAmount());
		
		
//		if (form.getDototalbillamt().getValue() != null)
			inventity.setNetPayable(model.getTotalBillingAmount());
		
			inventity.setDiscount(0);
		//   ends here 
		
			
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC",model.getCompanyId())){
			inventity.setInvoiceDate(new Date());
		}else{
//			if (form.getDbinvoicedate().getValue() != null)
				inventity.setInvoiceDate(model.getInvoiceDate());
		}
		
			
//		if (form.getDbinvoicedate().getValue() != null)
//			inventity.setInvoiceDate(form.getDbinvoicedate().getValue());
//		if (form.getDbpaymentdate().getValue() != null)
			inventity.setPaymentDate(model.getPaymentDate());
//		if (form.getOlbApproverName().getValue() != null)
			inventity.setApproverName(model.getApproverName());
//		if (form.getOlbEmployee().getValue() != null)
			inventity.setEmployee(model.getEmployee());
//		if (form.getOlbbranch().getValue() != null)
			inventity.setBranch(model.getBranch());
		
		inventity.setOrderCreationDate(model.getOrderCreationDate());
		inventity.setCompanyId(model.getCompanyId());
		inventity.setInvoiceAmount(model.getTotalBillingAmount());
		inventity.setInvoiceType(invoiceType);
		inventity.setPaymentMethod(model.getPaymentMethod());
		inventity.setAccountType(model.getAccountType());
		inventity.setTypeOfOrder(model.getTypeOfOrder());
		
		
		if(invoiceType.trim().equalsIgnoreCase(AppConstants.CREATETAXINVOICE.trim())){
			if(autoinvoiceflag){
				inventity.setStatus(Invoice.REQUESTED);
			}else{
				inventity.setStatus(Invoice.CREATED);
			}
		}else{
			inventity.setStatus(BillingDocument.PROFORMAINVOICE);
		}
		
		/*** Date 15-03-2021 added by vijay for sign up process Invoice will create in Approved state **/
		if(approveInvoiceCreatePaymentDocFlag){
			inventity.setStatus(Invoice.APPROVED);
		}
		
//		List<SalesOrderProductLineItem>productlist=form.getSalesProductTable().getDataprovider().getList();
		List<SalesOrderProductLineItem>productlist=model.getSalesOrderProducts();
		ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
		prodList.addAll(productlist);
		System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());
				
//		List<OtherCharges>OthChargeTbl=form.tblOtherCharges.getDataprovider().getList();
		List<OtherCharges>OthChargeTbl=model.getOtherCharges();
		ArrayList<OtherCharges>ocList=new ArrayList<OtherCharges>();
		ocList.addAll(OthChargeTbl);
		System.out.println("OTHER TAXES TABLE SIZE ::: "+ocList.size());
		
//		List<ContractCharges>taxestable=form.getBillingTaxTable().getDataprovider().getList();
		List<ContractCharges>taxestable=model.getBillingTaxes();
		ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
		taxesList.addAll(taxestable);
		System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
		
//		List<ContractCharges>otherChargesable=form.getBillingChargesTable().getDataprovider().getList();
		List<ContractCharges>otherChargesable=model.getBillingOtherCharges();
		ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
		otherchargesList.addAll(otherChargesable);
		System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
		
		ArrayList<SalesOrderProductLineItem> updatedProdList=new ArrayList<SalesOrderProductLineItem>();
		for (SalesOrderProductLineItem prod : prodList) {
			
			if(prod.getBillingDocBaseAmount()==0||prod.getBillingDocBaseAmount()==0.0){
				prod.setBillingDocBaseAmount(prod.getBaseBillingAmount());
			}
			prod.setBaseBillingAmount(prod.getBasePaymentAmount());
			prod.setPaymentPercent(100);
			prod.setBasePaymentAmount(prod.getBasePaymentAmount());
			
			updatedProdList.add(prod);
			
		}
		inventity.setSalesOrderProductFromBilling(updatedProdList);
		
		
		inventity.setOtherCharges(ocList);
//		if(form.dbOtherChargesTotal.getValue()!=null){
			inventity.setTotalOtherCharges(model.getTotalOtherCharges());
//		}
		
		inventity.setBillingTaxes(taxesList);
		inventity.setBillingOtherCharges(otherchargesList);
		
		inventity.setCustomerBranch(model.getCustomerBranch());

		inventity.setAccountType(model.getAccountType());
		inventity.setArrPayTerms(model.getArrPayTerms());
		
//		if (form.getOlbbillinggroup().getValue() != null) {
			inventity.setInvoiceGroup(model.getGroup());
//		}
		if (model.getOrderCformStatus() != null) {
			inventity.setOrderCformStatus(model.getOrderCformStatus());
		}
		if (model.getOrderCformPercent() != 0&& model.getOrderCformPercent() != -1) {
			inventity.setOrderCformPercent(model.getOrderCformPercent());
		} else {
			inventity.setOrderCformPercent(-1);
		}
		
		if(model.getNumberRange()!=null)
			inventity.setNumberRange(model.getNumberRange());
		
//		if(form.getDbbillingperiodFromDate().getValue()!=null)
			inventity.setBillingPeroidFromDate(model.getBillingPeroidFromDate());
//		if(form.getDbbillingperiodToDate().getValue()!=null)
			inventity.setBillingPeroidToDate(model.getBillingPeroidToDate());
		
//		if (form.getTbSegment().getValue() != null)
			inventity.setSegment(model.getSegment());
		
//		if (!form.getTbrateContractServiceid().getValue().equals(""))
			inventity.setRateContractServiceId(model.getRateContractServiceId());
		
//		if(form.getQuantity().getValue() !=null && form.getQuantity().getValue() > 0){
			inventity.setQuantity(model.getQuantity());
//		}
		
//		if(form.getOlbUOM().getValue()!=null){
			inventity.setUom(model.getUom());
//		}
		
//		if(form.getTbReferenceNumber().getValue()!=null){
			inventity.setRefNumber(model.getRefNumber());
//		}
	
		inventity.setTotalAmtExcludingTax(model.getTotalAmount());
		inventity.setDiscountAmt(model.getDiscountAmt());
		inventity.setFinalTotalAmt(model.getFinalTotalAmt());
		inventity.setTotalAmtIncludingTax(model.getTotalAmtIncludingTax());
		inventity.setTotalBillingAmount(model.getTotalBillingAmount());
//		if(form.getTbroundOffAmt().getValue()!=null&&!form.getTbroundOffAmt().getValue().equals("")){
			inventity.setDiscount(model.getRoundOffAmt());
//		}
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 * setting billing comment to invoice
		 * for NBHC
		 */
//		if(form.getTacomment().getValue()!=null){
			inventity.setComment(model.getComment());
//		}
		
		if(confiFlag){
			
			/**
			* @author Vijay Date :- 19-11-2020
			* Des :- bug - category type and group not mapped in invoice so updated the code
			*/
			inventity.setInvoiceCategory(model.getBillingCategory());
			inventity.setInvoiceConfigType(model.getBillingType());
			inventity.setInvoiceGroup(model.getBillingGroup());
		}
		
		inventity.setConsolidatePrice(model.isConsolidatePrice());
		
		inventity.setRenewContractFlag(model.isRenewContractFlag());
		
		if(model.getSubBillType()!= null && !model.getSubBillType().equals("")){
			inventity.setSubBillType(model.getSubBillType());
		}
		
		/** date 15.10.2018 added by komal for subbilltye **/
		if(model.getSubBillType()!= null && !model.getSubBillType().equals("")){
			inventity.setSubBillType(model.getSubBillType());
			if(model.getSubBillType().equalsIgnoreCase(AppConstants.ARREARSBILL)){
				inventity.setCncBillAnnexureId(model.getCncBillAnnexureId());
			}
		}
		
		if(model.getServiceId()!=null){
			inventity.setServiceId(model.getServiceId());
		}
		
		if(model.getCancleAmtRemark()!=null && !model.getCancleAmtRemark().equals("")){
			inventity.setCancleAmtRemark(model.getCancleAmtRemark());
		}
		
		/** Date 11-08-2020 by Vijay storing CNC Project Name and Contract Number in invoice ***/
		if(model.getProjectName()!=null){
			inventity.setProjectName(model.getProjectName());
		}
		/** Date 14-08-2020 by vijay mapping CNC contract number value in invoice details ***/
		if(model.getCncContractNumber()!=null){
			inventity.setContractNumber(model.getCncContractNumber());
		}
		/**
		 * @author Vijay Chougule
		 * @Since Date 18-08-2020 
		 * Des :- when below process config is Active then it will calculate the Total Man Power and Total Man Days
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn", inventity.getCompanyId())){
			double totalManPower = 0;
			double totalManDays=0;
			double labourManPower = 0;
			double overtimeManPower=0;
			double holidayManPower = 0;
			
			for(SalesOrderProductLineItem product : inventity.getSalesOrderProducts()){
				System.out.println("product.getBillType()"+product.getBillType());
				logger.log(Level.SEVERE, "product.getBillType() "+product.getBillType());
				if(product.getBillType()!=null && !product.getBillType().equals("") && ( product.getBillType().equals("Labour") || product.getBillType().equals("Overtime") || product.getBillType().equals("Holiday")) ){
					if(product.getManpower()!=null && !product.getManpower().equals("")){
						if(product.getBillType().equals("Labour")){
							labourManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Overtime")){
							overtimeManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Holiday")){
							holidayManPower += Double.parseDouble(product.getManpower());
						}
					}
					if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
						totalManDays +=Double.parseDouble(product.getArea());
						System.out.println("totalManDays "+totalManDays);
					}
				}
				
			}
			totalManPower = getmaxManPower(labourManPower,overtimeManPower,holidayManPower);
			inventity.setTotalManPower(totalManPower);
			inventity.setTotalManDays(totalManDays);
		}
		/**
		 * ends here
		 */
		
		/*** Date 12-08-2020 by Vijay for Sasha Credit Period storing in billing and invoice to calculate payment Date ***/
		inventity.setCreditPeriod(model.getCreditPeriod());
		if(model.getCreditPeriod()!=0 && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate", model.getCompanyId())){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(inventity.getInvoiceDate());
			cal.add(Calendar.DATE, inventity.getCreditPeriod());
			
			Date paymentDate=null;
			
			try {
				paymentDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"paymentDate "+paymentDate);
			inventity.setPaymentDate(paymentDate);
		
		}
		/**
		 * ends here
		 */
		
		/*** Date 26-09-2019 by Vijay for NBHC CCPM AutoInvoice invoice details from popup ****/
		if(autoinvoiceflag){	
			inventity.setInvoiceCategory(invoiceCategory);
			inventity.setInvoiceConfigType(invoiceType2);
			inventity.setInvoiceGroup(invoiceGruop);
			inventity.setApproverName(approverName);
			logger.log(Level.SEVERE, "Auto Invoice for NBHC CCPM");
			
		}
		/** Date 09-10-2020 by Vijay set value of created by requirement raised by Rahul Tiwari ***/
		if(userName!=null){
			inventity.setCreatedBy(userName);
		}
		
		/**
		 * @author Vijay 28-03-2022 for do not print service address flag updating to invoice
		 */
		inventity.setDonotprintServiceAddress(model.isDonotprintServiceAddress());
		
		/**Sheetal:30-03-2022 for payment mode to updating to invoice**/
		if(model.getPaymentMode()!=null && !model.getPaymentMode().equals("")) {
		  inventity.setPaymentMode(model.getPaymentMode());
		}
		
		GenricServiceImpl impl=new GenricServiceImpl();
		ReturnFromServer object=impl.save(inventity);
		logger.log(Level.SEVERE," Invoice Id : "+object.count);
		
		/*** Date 26-09-2019 by Vijay for NBHC CCPM AutoInvoice invoice sending Approval request  ****/
		if(autoinvoiceflag){
			logger.log(Level.SEVERE, "Auto Invoice sending approval request to first level");
			Approvals approval = new Approvals();
			approval.setApproverName(inventity.getApproverName());
			approval.setBranchname(inventity.getBranch());
			approval.setBusinessprocessId(object.count);
			if(userName!=null){
			approval.setRequestedBy(userName);
			}
			approval.setPersonResponsible(inventity.getEmployee());
			if(inventity.getCreatedBy()!=null){
				approval.setDocumentCreatedBy(inventity.getCreatedBy());
			}
			approval.setApprovalLevel(1);
			approval.setStatus(Approvals.PENDING);
			approval.setBusinessprocesstype(ApproverFactory.INVOICEDETAILS);
			approval.setBpId(inventity.getPersonInfo().getCount()+"");
			approval.setBpName(inventity.getPersonInfo().getFullName());
			approval.setDocumentValidation(false);
			approval.setCompanyId(model.getCompanyId());
			GenricServiceImpl implnew=new GenricServiceImpl();
			implnew.save(approval);
			
			logger.log(Level.SEVERE, "Auto Invoice Approval Sent successfully");

		}

		ArrayList<Integer> billIdList=new ArrayList<Integer>();
		ArrayList<Integer> billOrderIdList=new ArrayList<Integer>();
		for(int i=0;i<model.getArrayBillingDocument().size();i++){
			billIdList.add(model.getArrayBillingDocument().get(i).getBillId());
			billOrderIdList.add(model.getArrayBillingDocument().get(i).getOrderId());
		}
		
		List<BillingDocument>billingList=ofy().load().type(BillingDocument.class).filter("companyId", model.getCompanyId())
				.filter("contractCount IN", billOrderIdList).filter("count IN", billIdList).filter("typeOfOrder", model.getTypeOfOrder()).list();
		if(billingList!=null&&billIdList.size()!=0){
			logger.log(Level.SEVERE,"Billing List Size : "+billingList.size());
			for(BillingDocument bill:billingList){
//				for(Integer orderId:billOrderIdList){
//					if(bill.getContractCount()==orderId){
						bill.setStatus(BillingDocument.BILLINGINVOICED);
						bill.setInvoiceCount(object.count);
//					}
//				}
			}
			ofy().save().entities(billingList).now();
		}
		
		return object.count+"-";
			
	}
	
	private double getmaxManPower(double labourManPower,double overtimeManPower, double holidayManPower) {
		logger.log(Level.SEVERE, "labourManPower"+labourManPower);
		logger.log(Level.SEVERE, "overtimeManPower"+overtimeManPower);
		logger.log(Level.SEVERE, "holidayManPower"+holidayManPower);

		double maxManPower = 0;
		if(labourManPower>=overtimeManPower && labourManPower>=holidayManPower){
				maxManPower = labourManPower;
		}
		else if(overtimeManPower>=holidayManPower && overtimeManPower>=labourManPower){
				maxManPower = overtimeManPower;
		}
		else{
			maxManPower = holidayManPower;
		}
		
		return maxManPower;
	}



	public boolean isMultipleContractBilling(List<BillingDocumentDetails> billtablelis){
		int contractId=billtablelis.get(0).getOrderId();
		for(int i=0;i<billtablelis.size();i++){
			if(billtablelis.get(i).getOrderId()!=contractId){
				return true;
			}
		}
		return false;
	}
	@Override
		public void updateInvoices(long companyId,String fromDate,String toDate) {
			// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"INSIDE GENERAL SERVICE IN UPDATE INVOICES"+fromDate+toDate);
		
			String taskName="UpdateInvoices"+"$"+companyId+"$"+fromDate+"$"+toDate;
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		}
		
	/**
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM Assessment mark close and send an Email 
	 */
	@Override
	public String AssesmentMarkCloseAndSendEmail(AssesmentReport asseementReport) {
		asseementReport.setStatus(AssesmentReport.CLOSED);
		asseementReport.setSubmissionDate(new Date());//Ashwini Patil
		ofy().save().entity(asseementReport);
		logger.log(Level.SEVERE, "Assessment Report Updated Successfully");
		sendAssementReportEmail(asseementReport);
		return "Success";
	}

	private void sendAssementReportEmail(AssesmentReport asseementReport) {
		Company company = ofy().load().type(Company.class).filter("companyId", asseementReport.getCompanyId()).first()
				.now();
		Email email = new Email();
		NBHCAssessmentReportPdf nbhcassementReportpdf = new NBHCAssessmentReportPdf();
		nbhcassementReportpdf.document = new Document();
		nbhcassementReportpdf.document = new Document(PageSize.A4.rotate());
		email.pdfstream = new ByteArrayOutputStream();
		try {
			PdfWriter.getInstance(nbhcassementReportpdf.document, email.pdfstream);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		nbhcassementReportpdf.document.open();
		nbhcassementReportpdf.createPDFForEmail(asseementReport.getId());
		nbhcassementReportpdf.document.close();
		
		String[] fileName = { "AssessmentReport " + asseementReport.getCount() + " "
				+ asseementReport.getCinfo().getFullName() };

		ArrayList<String> toEmailList = new ArrayList<String>();

		Complain complain = ofy().load().type(Complain.class).filter("companyId", asseementReport.getCompanyId())
				.filter("count", asseementReport.getComplaintId()).first().now();
		if (complain != null) {
			if (complain.getEmail() != null && !complain.getEmail().equals("")) {
				toEmailList.add(complain.getEmail());
				try {
					logger.log(Level.SEVERE, "Ready To Call Common Method");
					email.sendNewEmail(toEmailList, "AssessmentReport", "AssessmentReport",
							asseementReport.getCount(), "AssessmentReport",
							asseementReport.getAssessmentDate(), company, "null",
							null, null, null, null, null,
							null, null, null, fileName, null,null,null);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		

	}
	@Override
	public String sendSrMail(long companyId, ArrayList<Integer> serviceIdList) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String str = gson.toJson(serviceIdList);
		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "SendSRMail")
				.param("companyId", companyId + "")
				.param("serviceId", str));
		
		return "success";
	}
		@Override
	public ArrayList<String> validateTechnicianWarehouse(
			ArrayList<Service> serviceList, List<ProductGroupList> groupList) {
		// TODO Auto-generated method stub
		String error = "";
		ArrayList<String> errorList = new ArrayList<String>();
		List<Integer> projectIdList = new ArrayList<Integer>();
		List<Integer> serviceIdList = new ArrayList<Integer>();
		List<Integer> contractIdList = new ArrayList<Integer>();
		List<String> empList = new ArrayList<String>();
		Map<Integer , ServiceProject> projectMap = new HashMap<Integer , ServiceProject> ();
	//	Map<Integer , ServiceProject> newProjectMap = new HashMap<Integer , ServiceProject> ();
		//else{
			for(Service ser : serviceList){
				serviceIdList.add(ser.getCount());
				projectIdList.add(ser.getProjectId());
				contractIdList.add(ser.getContractCount());
				projectMap.put(ser.getCount(), null);
				if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RemoveMaterialValidation", serviceList.get(0).getCompanyId())){
					if(!ser.isMaterialAdded()){
						error = "Material is not added for service id "+ ser.getCount()+"\n";
						errorList.add(error);
					}
				}
				
			}
		//}
		if(errorList.size() > 0){
			return errorList;
		}
		List<ServiceProject> projectList = ofy().load().type(ServiceProject.class)
				.filter("companyId", serviceList.get(0).getCompanyId()).filter("count IN", projectIdList)
				.filter("serviceId IN", serviceIdList)
				.filter("contractId IN", contractIdList).list();
		
		for(ServiceProject project :projectList){
			if(project.getProdDetailsList() != null){
				for(ProductGroupList group : project.getProdDetailsList()){
					if(group.getWarehouse() == null  || group.getStorageLocation() == null || group.getStorageBin() == null || 
							group.getWarehouse().equals("") || group.getStorageLocation().equals("") || group.getStorageBin().equals("") ||
							group.getWarehouse().contains("SELECT") || group.getStorageLocation().contains("SELECT") || group.getStorageBin().contains("SELECT")	){
						error = "Technician warehouse is not selected in service id "+ project.getserviceId()+"\n";
						errorList.add(error);
					}
					if(group.getParentWarentwarehouse() == null || group.getParentStorageLocation() == null || group.getParentStorageBin() == null || 
							group.getParentWarentwarehouse().equals("") || group.getParentStorageLocation().equals("") || group.getParentStorageBin().equals("")
						 || group.getParentWarentwarehouse().contains("SELECT") || group.getParentStorageLocation().contains("SELECT") || group.getParentStorageBin().contains("SELECT")	){
						error = "Parent warehouse is not selected in service id "+ project.getserviceId()+"\n";
						errorList.add(error);
					}
				}
			}
		}
		if(errorList.size() > 0){
			return errorList;
		}
						
		return null;
	}
	
	@Override
	public ArrayList<String> validateTechnicianWarehouseQuantity(long companyId , ArrayList<Service> serviceList, List<ProductGroupList> groupList)
	{
		logger.log(Level.SEVERE, "validateTechnicianWarehouseQuantity....!!!! ");
		String error = "";
		ArrayList<String> errorList = new ArrayList<String>();
//		List<String> techList = new ArrayList<String>();
		HashSet<String> hstechnician = new HashSet<String>();

		if(groupList.size() > 0){
			logger.log(Level.SEVERE, "Step 1 ....!!!! ");
			ArrayList<Integer> prodIdList = new ArrayList<Integer>();
			Map<String , Double> requestedQtyMap =  new HashMap<String , Double>();
			Map<Integer , ProductInventoryView> inventoryMap =  new HashMap<Integer , ProductInventoryView>();
			Map<String , Double> inUseQtyMap = new HashMap<String , Double>();
			for(ProductGroupList prodId : groupList){
				prodIdList.add(prodId.getProduct_id());
				requestedQtyMap.put(prodId.getProduct_id()+"-"+prodId.getCreatedBy(), prodId.getProActualQty());
	//			techList.add(prodId.getCreatedBy());
				hstechnician.add(prodId.getCreatedBy());
			}
			
			/**
			 * @author Anil @since 21-01-2021
			 * checking whether inUseStock is maintained or not
			 * if stock is maintained then we will skip calculation of in use stock
			 */
			boolean calculateUseStockFlag=false;
			ArrayList<SuperModel> result = getProductInventoryDetails(prodIdList,companyId);
			
			if(result!=null&&result.size()>0){
				logger.log(Level.SEVERE, "Step 2 ....!!!! ");
				for(ProductGroupList group : groupList){
					String techWh = group.getWarehouse();
					String techSL = group.getStorageLocation();
					String techSB = group.getStorageBin();
					logger.log(Level.SEVERE, "WH: "+techWh+" SL: "+techSL+" SB: "+techSB);
					
					for(SuperModel model:result){
						ProductInventoryView piv = (ProductInventoryView) model;
						if(group.getProduct_id() == piv.getProdID()){
							logger.log(Level.SEVERE, "Step 3 ....!!!! ");
							for(ProductInventoryViewDetails pivd: piv.getDetails()){
								if(pivd.getWarehousename().trim().equals(techWh.trim()) &&
									pivd.getStoragelocation().trim().equals(techSL.trim()) &&
									pivd.getStoragebin().trim().equals(techSB.trim()) ){
									
									if(pivd.getInUseStock()==null){
										calculateUseStockFlag =true;
										logger.log(Level.SEVERE, "Validating technician wise used stock....!!!! "+calculateUseStockFlag);
										break;
									}
								}
							}
						}
					}
				}
			}
			/**
			 * above we checked whether in use stock is maintained or not
			 * if not then we have to calculate the in use stock
			 * below is code used for calculating used stock
			 */
			if(calculateUseStockFlag){
				
				logger.log(Level.SEVERE, "Calculating technician wise used stock....!!!!");
				
				List<String> statusList=new ArrayList<String>();
				statusList.add(Service.SERVICESTATUSSCHEDULE);
				statusList.add(Service.SERVICESTATUSRESCHEDULE);
				statusList.add(Service.SERVICESTATUSSTARTED);
				statusList.add(Service.SERVICESTATUSREPORTED);
				/**
				 * @author Vijay Chougule
				 * Des :- To load services with IN Operator its taking much time so if there is only one technician so removed IN operator Querry
				 * and else part as old code with IN Operator
				 */
				List<String> techListnew = new ArrayList<String>(hstechnician);
				List<Service> serviceList1 =  new ArrayList<Service>();;
				if(techListnew.size()==1) {
					String technicianName =techListnew.get(0);
					serviceList1 = ofy().load().type(Service.class).filter("companyId", companyId).filter("employee", technicianName).filter("isServiceScheduled", true).filter("status IN", statusList).list();
				}else {
					serviceList1 = ofy().load().type(Service.class).filter("companyId", companyId).filter("employee IN", techListnew).filter("isServiceScheduled", true).filter("status IN", statusList).list();
				}
				/**
				 * ends here
				 */
				logger.log(Level.SEVERE, "SERVICE LIST: "+serviceList1.size());
				
				List<Integer> serIdList = new ArrayList<Integer>();
				List<Integer> projectIdList = new ArrayList<Integer>();
				for(Service ser : serviceList1){
					if(ser.isServiceScheduled() && !(ser.getServiceNumber().equals("") || ser.getServiceNumber() == null)){
						if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
							
						}else{
							serIdList.add(ser.getCount());
							projectIdList.add(ser.getProjectId());
						}
					}
				}	
				
				List<ServiceProject> projectList = new ArrayList<ServiceProject>();
				if(serIdList != null && serIdList.size() > 0){
					projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("serviceId IN", serIdList)
						 .filter("count IN", projectIdList).list();
				}	
				
				logger.log(Level.SEVERE, "PROJECT LIST: "+projectList.size()+" ID "+projectIdList.size());
			
//				Map<String , Double> inUseQtyMap = new HashMap<String , Double>();
				for(ServiceProject serProject : projectList){
					if(serProject != null && serProject.getProdDetailsList() != null && serProject.getProdDetailsList().size() >0){
						for(ProductGroupList group : serProject.getProdDetailsList()){
							double value = 0;
							String key = group.getProduct_id()+"-"+group.getCreatedBy();
							
							logger.log(Level.SEVERE, "IN USE MAP KEY: "+key);
							if(inUseQtyMap.containsKey(key)){
								value = inUseQtyMap.get(key);
								inUseQtyMap.put(key ,value + group.getProActualQty());
								logger.log(Level.SEVERE ,"required map*****************"+ group.getProActualQty() + " "+key  + " "+(value+group.getProActualQty()));
							}else{
								inUseQtyMap.put(key ,group.getProActualQty());
							}
						}
					}
				}
			}
			
			logger.log(Level.SEVERE, "IN USE MAP: "+inUseQtyMap.size() +"calculateUseStockFlag "+calculateUseStockFlag);
		
	
//			ArrayList<SuperModel> result = getProductInventoryDetails(prodIdList,companyId);
			if(result == null || result.size()==0){
				error = "Please Define Product Inventory Master First!";
				errorList.add(error);
				
			}else{
				logger.log(Level.SEVERE, "Step 4 ....!!!! ");
				for(ProductGroupList group : groupList){
					
					String transferToWarehouseName = group.getWarehouse();
					String transferToLocationName = group.getStorageLocation();
					String transferToBinName = group.getStorageBin();
					
					
					String parentWarehouseName ="";
					String parentLocationName = "";
					String parentBinName = "";
					if(group.getParentWarentwarehouse()!=null)
						parentWarehouseName = group.getParentWarentwarehouse();
					if(group.getParentStorageLocation()!=null)
						parentLocationName = group.getParentStorageLocation();
					if(group.getParentStorageBin()!=null)
						parentBinName = group.getParentStorageBin();
					logger.log(Level.SEVERE, "parentWarehouseName="+parentWarehouseName);
					for(SuperModel model : result){
						Double inUseQty = null;
						double availableQuantity = 0;
						double technicianQuantity = 0;
						boolean flag = false;
						boolean parentFlag = false;
						ProductInventoryView piv = (ProductInventoryView) model;
						inventoryMap.put(piv.getProdID(), piv);
						if(group.getProduct_id() == piv.getProdID()){
							for(ProductInventoryViewDetails pivd: piv.getDetails()){
							 
								if(pivd.getWarehousename().trim().equals(transferToWarehouseName.trim()) &&
									pivd.getStoragelocation().trim().equals(transferToLocationName.trim()) &&
									pivd.getStoragebin().trim().equals(transferToBinName.trim()) ){
									flag =true;
									technicianQuantity =  pivd.getAvailableqty();
									
									if(calculateUseStockFlag){
										if(inUseQtyMap.size()!=0){
											logger.log(Level.SEVERE, "Product MAP KEY: "+piv.getProdID()+"-"+group.getCreatedBy());
											if(inUseQtyMap.containsKey(piv.getProdID()+"-"+group.getCreatedBy())){
									    		inUseQty = inUseQtyMap.get(piv.getProdID()+"-"+group.getCreatedBy());
									    		pivd.setInUseStock(inUseQty);
									    		logger.log(Level.SEVERE, "Updating technician used stock....!!!!");
									    		piv.setEditFlag(true);
									    	}else{
									    		inUseQty = 0.0;
									    		pivd.setInUseStock(inUseQty);
									    		logger.log(Level.SEVERE, "In Use Stock - Other tech wise prod");
									    		piv.setEditFlag(true);
									    	}
										}else{
											inUseQty = 0.0;
								    		pivd.setInUseStock(inUseQty);
								    		logger.log(Level.SEVERE, "In Use Stock - not used");
								    		piv.setEditFlag(true);
										}
										
									}else{
										inUseQty=pivd.getInUseStock();
										logger.log(Level.SEVERE, "In Use Stock - maintained "+inUseQty);
									}
								}
								if(pivd.getWarehousename().trim().equals(parentWarehouseName.trim()) &&
										pivd.getStoragelocation().trim().equals(parentLocationName.trim()) &&
										pivd.getStoragebin().trim().equals(parentBinName.trim()) ){
									availableQuantity = pivd.getAvailableqty();
									if(result.size()!=0){
			//								if (validateQuantity(availableQuantity , map.get(productDetails.getProdid()), value1)) {										
			//									error = "Insufficient available quantity in parent warehouse for product : "+productDetails.getProdname() +" for technician "+group.getCreatedBy()+"\n";
			//									errorList.add(error);
			//								} 
									}
									parentFlag =true;
								}
							}
		
							if(flag==false){
								error  = "Please Define Product Inventory Master for TransferTo Warehouse of product "+piv.getProductName() +" for technician "+group.getCreatedBy()+"\n";
								errorList.add(error);
								
							}
							//commenting this code as this error is coming whenever admin schedules a service with material from pedio
							if(parentFlag==false){
//								error  = "Please Define Product Inventory Master for Parent Warehouse of product "+piv.getProductName() +" for technician "+group.getCreatedBy()+"\n";
//								errorList.add(error);
								
							}
							
							try{
								if (validateQuantity(availableQuantity , requestedQtyMap.get(piv.getProdID()+"-"+group.getCreatedBy()), inUseQty , technicianQuantity)) {										
									error = "Insufficient available quantity in parent warehouse for product : "+piv.getProductName() +" for technician "+group.getCreatedBy()+"\n";
									errorList.add(error);
								} 
							}catch(NullPointerException e){
								logger.log(Level.SEVERE, "In Use Stock - NULL ");
							}
						}
					}
				}
			}
			for(ProductGroupList group : groupList){
				if(inventoryMap.containsKey(group.getProduct_id())){
					
				}else{
					error = "Please Define Product Inventory Master for product "+ group.getName();
					errorList.add(error);
				}
			}
			
			if(calculateUseStockFlag){
				if(result!=null&&result.size()>0){
					logger.log(Level.SEVERE, "Saving updated technician wise used stock....!!!!");
					GenricServiceImpl impl=new GenricServiceImpl();
					impl.save(result);
				}
			}
		}
		logger.log(Level.SEVERE, "Step 5 ....!!!! ");
		
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		logger.log(Level.SEVERE, "errorList="+errorList.toString());
		return errorList;
	}
		public boolean validateQuantity(double availableQty , double reqQty , double totalUsedQuantity,
				double technicianQuantity) {
			
			double qunatity = totalUsedQuantity + reqQty - technicianQuantity;
			if(qunatity <0){
				qunatity = 0;
			}
			logger.log(Level.SEVERE, "Available Qunatity : "+ qunatity);
//			if (availableQty - qunatity == 0) {
//				return true;
//			} else 
				if (availableQty - qunatity < 0) {
				return true;
			}
		
		return false;
	 }
	
	public int createProject(Service service,	ArrayList<EmployeeInfo> technicianlist,
			ArrayList<CompanyAsset> technicianToollist,	ArrayList<ProductGroupList> materialInfoList, Date serviceDate, String serviceTime, int invoiceId,String invoiceDate,String TechnicianName, int projectId){
		long companyId = service.getCompanyId();
		
		int contractCount = service.getContractCount();
		int ServiceId = service.getCount();
		if(materialInfoList != null && materialInfoList.size() > 0){
			service.setMaterialAdded(true);
		}


	
	

		ServiceProject projectEntity = null;
		projectEntity = ofy().load().type(ServiceProject.class).filter("serviceId", service.getCount()).filter("companyId", service.getCompanyId()).filter("contractId", contractCount).first().now();
		if(projectEntity == null){
			logger.log(Level.SEVERE,"projectEntity == null");
				projectEntity =	getFieldsToCustomerProjectScreen(technicianlist,technicianToollist,materialInfoList,companyId,contractCount,ServiceId);
				   GenricServiceImpl impl=new GenricServiceImpl();
					if(projectEntity!=null){
						ReturnFromServer server = impl.save(projectEntity);
						projectEntity.setCount(server.count);
					}
					
			
		}else{
			service = ofy().load().type(Service.class).filter("count", ServiceId).filter("companyId", service.getCompanyId()).filter("contractCount", service.getContractCount()).first().now();
			logger.log(Level.SEVERE, "projectid :"+  service.getProjectId()+"  "+projectEntity.getCount());
		//	projectEntity = ofy().load().type(ServiceProject.class).filter("serviceId", service.getCount()).filter("companyId", service.getCompanyId()).filter("count", service.getProjectId()).first().now();
			
			/**
			 * nidhi
			 * 19-06-2018
			 */
			if(technicianlist.size() > 0 && technicianlist.get(0).getEmpCount()==0){
				ArrayList<EmployeeInfo> techlist = new ArrayList<EmployeeInfo>();
				for(int i=0;i<technicianlist.size();i++){
					Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", technicianlist.get(i).getFullName().trim()).first().now();
			  		if(emp!=null){
					EmployeeInfo empInfoEntity = new EmployeeInfo();
			  		empInfoEntity.setEmpCount(emp.getCount());
			  		empInfoEntity.setFullName(emp.getFullName());
			      	empInfoEntity.setCellNumber(emp.getCellNumber1());
			      	empInfoEntity.setDesignation(emp.getDesignation());
			      	empInfoEntity.setDepartment(emp.getDepartMent());
			      	empInfoEntity.setEmployeeType(emp.getEmployeeType());
			      	empInfoEntity.setEmployeerole(emp.getRoleName());
			      	empInfoEntity.setBranch(emp.getBranchName());
			      	empInfoEntity.setCountry(emp.getCountry());
			      	techlist.add(empInfoEntity);
			  		}
				}
				/**
				 * Date 11-11-2019 By Vijay
				 *  Des :- while scheduling when click on ok data not saved and here getting null pointer Exception so added if condition
				 */
				if(techlist.size()!=0){
					projectEntity.setTechnicians(techlist);
				}
			}else{
				projectEntity.setTechnicians(technicianlist);
			}
			
//			project.setTechnicians(technicianlist);
			
			/**
			 * @author Vijay Date 20-07-2023
			 * Des :- getting structure issue so here article info not required so set as null to schedule the services
			 */
			ArrayList<CompanyAsset> assetlist = new ArrayList<CompanyAsset>();
			for(CompanyAsset compasset : technicianToollist) {
				assetlist.add(getAssetInfoWithoutArticleinfo(compasset));
			}
			/**
			 * ends here
			 */
			projectEntity.setTooltable(assetlist);
			
			projectEntity.setProdDetailsList(materialInfoList);
			
			ofy().save().entity(projectEntity);
		}
		
		
		if(serviceDate!=null)
			service.setServiceDate(serviceDate);
			if(!serviceTime.equals(""))
			service.setServiceTime(serviceTime);
			if(projectEntity!=null)
			service.setProjectId(projectEntity.getCount());
			
			if(invoiceId!=0){
				service.setInvoiceId(invoiceId);
			}
			
			 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

			 TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			 fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			if(invoiceDate !=null && !invoiceDate.equals("")){
				try {
					service.setInvoiceDate(fmt.parse(invoiceDate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(!TechnicianName.equals("")){
				service.setEmployee(TechnicianName);
			}
			/**
			 * @author Vijay Chougule Date :- 03-09-2020
			 * Des :- If below process config is active then project will create when service will create from contract
			 * so setting schedule flag
			 */
		    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.AUTOMATICSCHEDULING, service.getCompanyId())
		    		&& materialInfoList.size()!=0){
				service.setServiceScheduled(true);
		    }
		    /**
		     * ends hete
		     */
			
			if(serviceDate!=null || !serviceTime.equals("") ){
				ofy().save().entity(service);
			}
			logger.log(Level.SEVERE,"Service Updated successfully.");
			
			/*
			 * Name:Apeksha Gunjal
			 * Date: 29/06/2018 @19:48
			 * Note: Send Notification to Technician on EVA Pedio after assigning respective technician
			 */
			try{
				logger.log(Level.SEVERE, "TechnicianSchedulingPlan service.getEmployee(): "+service.getEmployee());
				
				if(!service.getEmployee().isEmpty()){
					sendPushNotificationToTechnician(service, companyId);
				}
			}catch(Exception e){
				logger.log(Level.SEVERE, "TechnicianSchedulingPlan Error in sending push notification to technician: "+e.getMessage());
			}
		return service.getProjectId();
	}



	@Override
	public String createAttendance(ArrayList<Attendance> attendanceList) {
		
		if(attendanceList!=null&&attendanceList.size()!=0){
			long number=0;
			NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId",attendanceList.get(0).getCompanyId())
					.filter("processName","Attendance").filter("status", true).first().now();
	    	number=ng.getNumber();
			
			HashSet<Integer> empIdHs=new HashSet<Integer>();
			for(Attendance obj:attendanceList){
				empIdHs.add(obj.getEmpId());
				number++;
				obj.setCount((int) number);
				
			}
			ArrayList<Integer> empIdLis=new ArrayList<Integer>(empIdHs);
			
			if(empIdLis.size()!=0){
				List<Attendance> prevAttenList=ofy().load().type(Attendance.class).filter("companyId", attendanceList.get(0).getCompanyId()).filter("month", attendanceList.get(0).getMonth()).filter("empId IN", empIdLis).list();
				HashSet<String> empNmHs=new HashSet<String>();
				if(prevAttenList!=null&&prevAttenList.size()!=0){
					for(Attendance obj:prevAttenList){
						empNmHs.add(obj.getEmployeeName());
					}
					String errorMsg="Attendance already exist for below employee."+"\n";
					ArrayList<String> empNmList=new ArrayList<String>(empNmHs);
					for(String name:empNmList){
						errorMsg=errorMsg+name+"\n";
					}
					
					
					return errorMsg;
				}
			}
			
			ServerAppUtility utility=new ServerAppUtility();
			for(Attendance attendance:attendanceList){
				utility.ApproveLeave(attendance);
			}
			ofy().save().entities(attendanceList).now();
			
	    	ng.setNumber(number);
	    	ofy().save().entity(ng).now();
	    	
	    	return "Attendance Save Successful.";
			
			
		}else{
			return "No attendance found!";
		}
	}
		@Override
	public boolean updatedServiceAddress(SalesOrder salesorder,
			boolean customerbranchservicesflag) {
		// for updating address in contract
		ofy().save().entity(salesorder);
		
		logger.log(Level.SEVERE,"Branch wise services flag"+customerbranchservicesflag);
		if(!customerbranchservicesflag){
			ArrayList<String> salesorderstatus = new ArrayList<String>();
			salesorderstatus.add("Created");
			salesorderstatus.add("Requested");
			salesorderstatus.add("Approved");
			List<DeliveryNote> delnoteList = ofy().load().type(DeliveryNote.class).filter("companyId", salesorder.getCompanyId()).filter("salesOrderCount", salesorder.getCount()).filter("status IN", salesorderstatus).list();
			logger.log(Level.SEVERE,"Service list size =="+delnoteList.size());
			logger.log(Level.SEVERE,"contract.getCustomerServiceAddress() locality =="+salesorder.getShippingAddress().getLocality());
			for(int i=0;i<delnoteList.size();i++){
				delnoteList.get(i).setShippingAddress(salesorder.getShippingAddress());
				delnoteList.get(i).setBillingAddress(salesorder.getNewcustomerAddress());
				/**
				 * @author Anil
				 * @since 30-11-2020
				 * updating customer branch
				 * Raised by Rahul Tiwari ,PTSPL
				 */
				delnoteList.get(i).setCustBranch(salesorder.getCustBranch());
			}
			if(delnoteList.size()!=0)
			ofy().save().entities(delnoteList);
		}
		
		return true;
	}



	@Override
	public void updateCnc(long companyId) {
		logger.log(Level.SEVERE,"Inside update cnc ==");
		List<CNC> cncList=ofy().load().type(CNC.class).filter("companyId", companyId).filter("status", "Confirmed").list();
	    if(cncList!=null&&cncList.size()!=0){
	    	logger.log(Level.SEVERE,"Service list size =="+cncList.size());
	    	ofy().save().entities(cncList).now();
	    	
	    }
	
	}



	@Override
	public void updateDuplicateServices(long companyId, Date fromDate,Date toDate,int serviceId,Date day1,Date day2,int contractId,int fromServiceId,int toServiceId) {
		// TODO Auto-generated method stub
		String fromDateInString=null;
		String toDateInString=null;
		String day1String=null;
		String day2String=null;
		logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate+" Service Id : "+serviceId+" day1 :"+day1+" day2 :"+day2+" contractId :"+contractId+" fromServiceId :"+fromServiceId+" toServiceId :"+toServiceId);
		
		try{
			fromDate=DateUtility.getDateWithTimeZone("IST", fromDate);
			toDate=DateUtility.getDateWithTimeZone("IST", toDate);
			
			fromDateInString=dateFormat.format(fromDate);
			toDateInString=dateFormat.format(toDate);		
			
		}catch(Exception e){
			
		}
		
		try{
			day1=DateUtility.getDateWithTimeZone("IST", day1);
			day2=DateUtility.getDateWithTimeZone("IST", day2);
			
			day1String=dateFormat.format(day1);
			day2String=dateFormat.format(day2);
		}catch(Exception e){
			
		}
		
		logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate+" Service Id : "+serviceId+" day1 :"+day1+" day2 :"+day2);
		
//		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//		queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue")
//				.param("taskKeyAndValue", "UpdateDuplicateServices")
//				.param("companyId", companyId + "")
//				.param("fromDate", dateFormat.format(fromDate))
//				.param("toDate", dateFormat.format(toDate)));
				
		String taskName="UpdateDuplicateServices"+"$"+companyId+"$"+fromDateInString+"$"+toDateInString+"$"+serviceId+"$"+day1String+"$"+day2String+"$"+contractId+"$"+fromServiceId+"$"+toServiceId;
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			
	}



	@Override
	public void updateDuplicateCustomerBranch(long companyId, int custBranchId) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"COM ID : "+companyId+" custBranchId : "+custBranchId);
		String taskName="UpdateDuplicateCustomerBranch"+"$"+companyId+"$"+custBranchId;
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		
	}
	
	public void updateTDSCalculation(List<PaySlip> paySlipList,ArrayList<Integer>empIdList){
		if(paySlipList!=null){
			List<CTC> ctcList=ofy().load().type(CTC.class).filter("companyId", paySlipList.get(0).getCompanyId()).filter("empid IN", empIdList).filter("status", "Active").list();
			for(PaySlip pSlip:paySlipList){
				// Check whether TDS deducted
				// find todat deduction other than TDS in that month
				// Then subtract that deduction amount from Cumulative deduction in ctc
				// Subtract the gross earning of that month from cumalative earning
				// subtract tds amount of that month from cumulative tax amount
				if(isTdsDeducted(pSlip)){
					double tdsAmount=getTdsAmount(pSlip);
					double totalDeductionOtherThanTds=pSlip.getTotalDeduction()-tdsAmount;
					CTC ctc=getCtcDetails(pSlip.getEmpid(), ctcList);
					if(ctc!=null){
						ctc.setCumulativeDeduction(ctc.getCumulativeDeduction()-totalDeductionOtherThanTds);
						ctc.setCumulativeCTC(ctc.getCumulativeCTC()-pSlip.getGrossEarning());
						ctc.setCumulativeTax(ctc.getCumulativeTax()-tdsAmount);
						
						ofy().save().entity(ctc).now();
					}
				}
			}
		}
	}
	
	public boolean isTdsDeducted(PaySlip payslip){
		if(payslip!=null){
			for(CtcComponent dedComp:payslip.getDeductionList()){
				if(dedComp.getShortName().trim().equalsIgnoreCase("TDS")&&dedComp.getActualAmount()!=0){
					return true;
				}
			}
		}
		return false;
	}
	
	public double getTdsAmount(PaySlip payslip){
		if(payslip!=null){
			for(CtcComponent dedComp:payslip.getDeductionList()){
				if(dedComp.getShortName().trim().equalsIgnoreCase("TDS")&&dedComp.getActualAmount()!=0){
					return dedComp.getActualAmount();
				}
			}
		}
		return 0;
	}
	
	public CTC getCtcDetails(int empId,List<CTC> ctcList){
		if(ctcList!=null){
			for(CTC ctc:ctcList){
				if(empId==ctc.getEmpid()){
					return ctc;
				}
			}
		}
		return null;
	}



	@Override
	public void updateComponentDetailsOnServices(Contract contract) {
		try{
			List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", contract.getCompanyId()).filter("contractCount", contract.getCount()).list();
			if(serviceList!=null){
				for(SalesLineItem item:contract.getItems()){
					for(BranchWiseScheduling branchObj:item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
						for(ComponentDetails compObj:branchObj.getComponentList()){
							updateComponentDetails(compObj,serviceList);
						}
					}
				}
				
				ofy().save().entities(serviceList).now();
			}
		}catch(Exception e){
			
		}
	}



	private void updateComponentDetails(ComponentDetails compObj,List<Service> serviceList) {
		for(Service service:serviceList){
			if(compObj.getAssetId()==service.getAssetId()){
				service.setComponentName(compObj.getComponentName());
				service.setMfgNo(compObj.getMfgNum());
				service.setMfgDate(compObj.getMfgDate());
				service.setReplacementDate(compObj.getReplacementDate());
				service.setAssetUnit(compObj.getAssetUnit());
				
				/**
				 * @author Anil
				 * @since 23-06-2020
				 * setting asset id,asset unit and mfg num in description
				 */
				String descHead="";
				String descVal="";
				if(service.getAssetId()!=0){
					descHead="Asset Id";
					descVal=service.getAssetId()+"";
				}
				if(service.getAssetUnit()!=null&&!service.getAssetUnit().equals("")){
					if(!descHead.equals("")){
						descHead=descHead+"/"+"Asset Unit";
						descVal=descVal+"/"+service.getAssetUnit();
					}else{
						descHead="Asset Unit";
						descVal=service.getAssetUnit();
					}
				}
				if(service.getMfgNo()!=null&&!service.getMfgNo().equals("")){
					if(!descHead.equals("")){
						descHead=descHead+"/"+"Mfg No.";
						descVal=descVal+"/"+service.getMfgNo();
					}else{
						descHead="Mfg No.";
						descVal=service.getMfgNo();
					}
				}
				String description="";
				if(!descHead.equals("")){
					description=descHead+" : "+descVal+" ";
				}
				
				if(description.equals("")){
					description=service.getDescription();
				}
				service.setDescription(description);
			}
		}
	}



	@Override
	public void ping() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public AssesmentReport scheduleAssessment(AssesmentReport report,Date serviceDate,String time,String description) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, serviceDate+" | "+time+" | "+description);
		if(report!=null){
			if(report.getServiceId()!=0){
				Service service=ofy().load().type(Service.class).filter("companyId", report.getCompanyId()).filter("count", report.getServiceId()).first().now();
				if(service!=null){
					Service object=new Service();
					object.setId(null);
					object.setCount(0);
					object.setCompanyId(service.getCompanyId());
					object.setScheduled(false);
					object.setStatus(Service.SERVICESTATUSSCHEDULE);
					object.setServiceType("Assessment");
					object.setAssessmentServiceFlag(true);
					
					object.setServiceDate(DateUtility.getDateWithTimeZone("IST", serviceDate));
					object.setServiceTime(time);
					object.setDescription(description);
					
					object.setContractCount(service.getContractCount());
					object.setBranch(service.getBranch());
					object.setPersonInfo(service.getPersonInfo());
					ServiceProduct product=(ServiceProduct) service.getProduct();
					object.setProduct(product);
					object.setEmployee(service.getEmployee());
					object.setContractStartDate(service.getContractStartDate());
					object.setContractEndDate(service.getContractEndDate());
					
					object.setServiceBranch(service.getServiceBranch());
					object.setAddress(service.getAddress());
					SuperModel model=object;
					
					GenricServiceImpl impl=new GenricServiceImpl();
					ReturnFromServer ser=impl.save(model);
					
					
					if(report.getServiceIdList()==null){
						ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
						serviceIdList.add(report.getServiceId());
						serviceIdList.add(ser.count);
					}else{
						if(report.getServiceIdList().contains(report.getServiceId())){
							report.getServiceIdList().add(ser.count);
						}else{
							report.getServiceIdList().add(report.getServiceId());
							report.getServiceIdList().add(ser.count);
						}
					}
					logger.log(Level.SEVERE, "Assessment Id "+ser.count);
					impl.save(report);
					
				}else{
					return null;
				}
			}
		}
		
		return report;
	}



	
	
	   /**@author Sheetal : 17-05-2022
    * Des : This method is used to made status of user inactive after attempting 
    * for incorrect password
    * **/

	@Override
	public void inactiveUser(String username, long companyId) {
		 User user=ofy().load().type(User.class).filter("userName",username)
				 .filter("companyId",companyId).first().now();
		 logger.log(Level.SEVERE , "user sheetal== :" +user);
		 if(user!=null) {
			 user.setStatus(false);
			 ofy().save().entity(user);
			 logger.log(Level.SEVERE , "Inside user :");
		 }
		 logger.log(Level.SEVERE , "Inactive user for password :");
	}
	
	
	private void SendEmailforNoAttendaceofEmployeelist(ArrayList<EmployeeInfo> noattendanceemplist, String payRollPeriod, long companyId) {
		
		ArrayList<String> table1=new ArrayList<String>();
		ArrayList<String> table1_Header=new ArrayList<String>();
		table1_Header.add("Id");
		table1_Header.add("Name");
		table1_Header.add("Cell Number");
		table1_Header.add("Branch");
		table1_Header.add("Designation");
		table1_Header.add("Comment");
		
		for(EmployeeInfo obj:noattendanceemplist){
			table1.add(obj.getEmpCount()+"");
			table1.add(obj.getFullName()+"");
			table1.add(obj.getCellNumber()+"");
			table1.add(obj.getBranch()+"");
			table1.add(obj.getDesignation()+"");
			table1.add("No attendance found.");
		}
		
		Company company= ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		StringBuilder builder =new StringBuilder();
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
		builder.append("<h3 class=\"ex\" align=\"left\">" + "Payroll Issues "+payRollPeriod+ "</h3></br></br>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
//		builder.append("Please find attachment.");
		
		
		
		/************************ Table 1 ***************************************/

		if (table1 != null && !table1.isEmpty()) {
			System.out.println("Table  1");
			// builder.append("<h2>"+heading1+"</h2>");

			builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
			builder.append("<tr>");

			for(int i=0;i<table1_Header.size();i++)
			{
						builder.append("<th>" +  table1_Header.get(i) + "</th>");
						
			}
				
			builder.append("</tr>");

			System.out.println("Size of table 1 " + table1.size());

			builder.append("<tr>");
			for (int i = 0; i < table1.size(); i++) {
					builder.append("<td>" + table1.get(i) + "</td>");
				
				if (i > 0 && (i + 1) % table1_Header.size() == 0) {
					builder.append("</tr><tr>");

				}
			}
			builder.append(" </tr>  ");
			builder.append("</table></b></br>");
		}

		
		
		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		ArrayList<String> emailList = new ArrayList<String>();
		emailList.add(company.getEmail());
		/**
		 * @author Anil , Date : 07-12-2019
		 * Payroll issue mail should also go to company POC
		 */
		if(company.getPocEmail()!=null&&!company.getPocEmail().equals("")){
			emailList.add(company.getPocEmail());
		}

		/**
		 * @author Anil , Date : 10-04-2020
		 * added display name for from email id
		 * ,company.getDisplayNameForCompanyEmailId()
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",companyId)){
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + emailList.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), emailList, new ArrayList<String>(), new ArrayList<String>(), "Payroll Issues : "+ payRollPeriod, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
		}else{
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + emailList.toString());
			Email email = new Email();
			email.sendMailWithGmail(company.getEmail(), emailList,new ArrayList<String>(), new ArrayList<String>(), "Payroll Issues : "+ payRollPeriod, contentInHtml, "text/html",null,null,null);
		}
		
	}



	@Override
	public ArrayList<EmployeeInfo> getEmployeeinfolist(ArrayList<Integer> empidlist, long companyId) {

		ArrayList<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
		List<EmployeeInfo> employeeinfolist = ofy().load().type(EmployeeInfo.class).filter("empCount IN", empidlist).filter("companyId", companyId).list();
		logger.log(Level.SEVERE,"employeeinfolist size "+employeeinfolist.size());
		if(employeeinfolist!=null && employeeinfolist.size()>0) {
			list.addAll(employeeinfolist);
			return list;
		}
		return null;
	}



	@Override
	public String updateFrequency(long companyId, Date contractStartDate) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"contractStartDate = "+contractStartDate+ "Company id="+companyId);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String dateInString=dateFormat.format(contractStartDate);
		logger.log(Level.SEVERE,"dateInString="+dateInString);
		String toDate=dateFormat.format(new Date());
		logger.log(Level.SEVERE,"toDate="+toDate);
		List<Contract> contractList=null;
		try {
			contractList = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("startDate >=", sdf.parse(dateInString))
					.filter("startDate <", sdf.parse(toDate)).list();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"after query");
		if(contractList!=null&&contractList.size()>0) {
			logger.log(Level.SEVERE,"contractList size = "+contractList.size());
			for(Contract c:contractList) {
				
				String taskName="UpdateFrequencyInServices"+"$"+companyId+"$"+c.getCount();
				Queue queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			
			}
			return "Updation Process has started. Check after an hour.";
		}else
			return "No contracts found!";
		
		
		}



	@Override
	public boolean checkIfLicensePresent(long companyId, String licenseCode) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"companyId="+companyId);	
		Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		boolean flag= ServerAppUtility.checkIfLicensePresent(comp, licenseCode);
		return flag;
	}







	@Override
	public String updateEmployeeIdInUser(long companyId) {
		// TODO Auto-generated method stub
		
		logger.log(Level.SEVERE,"Inside updateEmployeeIdInUser");
		try{
			List<User> users = ofy().load().type(User.class).filter("companyId", companyId).list();
			if(users!=null){
				logger.log(Level.SEVERE,"users count="+users.size());
				ArrayList<String> employeeNameList=new ArrayList<String>();
				for(User u:users){
					employeeNameList.add(u.getEmployeeName());
				}
				
				List<Employee> empList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname IN", employeeNameList).list();
				if(empList!=null){
					logger.log(Level.SEVERE,"employee count="+empList.size());
					for(Employee emp:empList){
						for(User user:users){
							if(user.getEmployeeName().equals(emp.getFullname())){
								user.setEmpCount(emp.getCount());
							}
						}
					}
					
				}
				
				GenricServiceImpl genricimpl = new GenricServiceImpl();
				ofy().save().entities(users).now();
				logger.log(Level.SEVERE,"employee id saved successfully");
				return "Employee IDs updated in User Successfully!";
			}else{
				return "No users found for updation";
			}
		
		}
		catch(Exception e){
			e.printStackTrace();
			return "Process Failed";
		}
		
	}



	@Override
	public void deleteDuplicateLeaves(long companyId, Date fromDate, Date toDate) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		String fromDateInString=null;
		String toDateInString=null;
		logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate);
		try{
			fromDate=DateUtility.getDateWithTimeZone("IST", fromDate);
			toDate=DateUtility.getDateWithTimeZone("IST", toDate);
			
			fromDateInString=dateFormat.format(fromDate);
			toDateInString=dateFormat.format(toDate);		
			
		}catch(Exception e){
			
		}
		
		logger.log(Level.SEVERE,"2. COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate);
				
		String taskName="deleteDuplicateLeaves"+"$"+companyId+"$"+fromDateInString+"$"+toDateInString;
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		logger.log(Level.SEVERE,"After task queue call");
		
	}



	@Override
	public void rescheduleForPeriod(long companyId, Date fromDate, Date toDate,
			ArrayList<Service> serviceList) {
		// TODO Auto-generated method stub
		String fromDateInString=null;
		String toDateInString=null;
		logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate);
		try{
			fromDate=DateUtility.getDateWithTimeZone("IST", fromDate);
			toDate=DateUtility.getDateWithTimeZone("IST", toDate);
			
			fromDateInString=dateFormat.format(fromDate);
			toDateInString=dateFormat.format(toDate);		
			
		}catch(Exception e){
			
		}
		
		String serviceIdListString="";
		if(serviceList!=null){
			JSONArray idArray=new JSONArray();
			for(Service s:serviceList){
				idArray.put(s.getCount());
			}
			logger.log(Level.SEVERE,"idArray size="+idArray.length());
			serviceIdListString=idArray.toString();
		}
		
		logger.log(Level.SEVERE,"2. COM ID : "+companyId+" FRM DATE : "+fromDateInString+" TO DATE : "+toDateInString);
				
		String taskName="rescheduleForPeriod"+"$"+companyId+"$"+fromDateInString+"$"+toDateInString+"$"+serviceIdListString;
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		logger.log(Level.SEVERE,"After task queue call");
	}



	
}
