package com.slicktechnologies.server.contract;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.contract.UpdatePricesService;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class UpdatePricesImpl extends RemoteServiceServlet implements UpdatePricesService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6076386703459375982L;

	
	
	
	Logger logger = Logger.getLogger("UpdatePricesImpl.class");
	double quantity = 1;
	double totalAmount = 0;
	double totalAmtFix=0;
	double netPayFix=0;
	double netPayable = 0;
	double assessableAmount = 0;
	
	@Override
	public ArrayList<Integer> saveAfterPriceUpdateForContract_Billing(long companyId,int contractId,
			List<SalesLineItem> listOfItems) {
		// TODO Auto-generated method stub
		ArrayList<Integer> intList=new ArrayList<Integer>();
		intList.add(0);
		try{
			Contract contract=ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", contractId).first().now();
			
			contract.setItems(listOfItems);
			
			contract.getProductTaxes().clear();
			
			contract.getProductTaxes().addAll(updateProductTaxesTable(listOfItems));
			
			contract.setTotalAmount(totalAmtFix);
			contract.setNetpayable(netPayFix);
			contract.setGrandTotal(netPayFix);
			ofy().save().entity(contract);
			
			intList.clear();
			intList.add(updateBilling(companyId,contractId));
			return intList;
		}catch(Exception e){
			e.printStackTrace();
			intList.add(-1);
			return intList;
		}
		
	}

	/**
	 * Description : This will update the prices changed for particular contract
	 * @param companyId
	 * @param contractId
	 * Created By: Rahul Verma
	 * Created For : This can be used for updation of billing documents(Developed for NBHC)
	 * Creation Date: 18-10-2016
	 * 
	 */
	private int updateBilling(long companyId, int contractId) {
		// TODO Auto-generated method stub
	
		try{
		Contract contract=ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", contractId).first().now();
		logger.log(Level.SEVERE,"contract in billing update:::::::::"+contract);
		/*
		 * This billing was created only for NBHC and as it was created by Webservice. Therefore there is only one Billing Documents.
		 */
		BillingDocument billingDoc=ofy().load().type(BillingDocument.class).filter("companyId", contract.getCompanyId()).filter("contractCount", contract.getCount()).first().now();
		logger.log(Level.SEVERE,"billingDoc in billing update:::::::::"+billingDoc);
		
		billingDoc.setTotalSalesAmount(contract.getNetpayable());
		billingDoc.getBillingDocumentInfo().setBillAmount(
				contract.getNetpayable());
		
		/************ Product Details ***************/

		int noOfProduct = contract.getItems().size();
		System.out.println("The No of Product" + noOfProduct);

		ArrayList<SalesOrderProductLineItem> productTable = new ArrayList<SalesOrderProductLineItem>();

		for (int i = 0; i < noOfProduct; i++) {

			SalesOrderProductLineItem product = new SalesOrderProductLineItem();

			System.out
					.println("product.setProdId(contract.getItems().get(i).getPrduct().getCount())"
							+ (contract.getItems().get(i).getPrduct()
									.getCount()));

			if (contract.getItems().get(i).getPrduct().getCount() != 0) {

				product.setProdId(contract.getItems().get(i).getPrduct()
						.getCount());

			}

			product.setProdCategory(contract.getItems().get(i)
					.getProductCategory());
			product.setProdCode(contract.getItems().get(i).getProductCode());
			product.setProdName(contract.getItems().get(i).getProductName());
			product.setQuantity(contract.getItems().get(i).getQty());
			product.setPrice(contract.getItems().get(i).getPrice());
			product.setVatTax(contract.getItems().get(i).getVatTax());
			product.setServiceTax(contract.getItems().get(i)
					.getServiceTax());
			// Tax tax=new Tax();
			// tax.setPercentage(14.5);
			// product.setServiceTax(tax);
			product.setTotalAmount(contract.getItems().get(i).getPrice());
			product.setBaseBillingAmount((contract.getItems().get(i)
					.getQty()) * (contract.getItems().get(i).getPrice()));
			product.setPaymentPercent(100);
			// PAyable amount bacha hai

			productTable.add(product);
			
		}

		billingDoc.setSalesOrderProducts(productTable);
		System.out.println("contract.getNetpayable()"
				+ contract.getNetpayable());
		billingDoc.setTotalBillingAmount(contract.getNetpayable());

		// billing.setTotalSalesAmount(billing.getTotalBillingAmount());
		// System.out.println("The Total Value is :"+billing.getTotalBillingAmount());
		/************ Finish with Product Details ***************/
		
		/***** Tax Table *************/

		List<ContractCharges> taxTableList = new ArrayList<ContractCharges>();

		for (int i = 0; i < contract.getProductTaxes().size(); i++) {
			ContractCharges taxTable = new ContractCharges();
			taxTable.setTaxChargeName(contract.getProductTaxes().get(i)
					.getChargeName());
			taxTable.setTaxChargePercent(contract.getProductTaxes().get(i)
					.getChargePercent());
			taxTable.setTaxChargeAssesVal(contract.getProductTaxes().get(i)
					.getAssessableAmount());
			taxTable.setPayableAmt(contract.getProductTaxes().get(i)
					.getChargePayable());
			taxTableList.add(taxTable);
		}
		billingDoc.setBillingTaxes(taxTableList);
		/**** Done with Tax Table **************/
		
		ofy().save().entity(billingDoc);
		
		return 1;
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.SEVERE,"ERROR in Updating Billing:::::::::"+e);
			return 0;
		}
	}

	/**
	 * Description: This method will calculate all taxes of product and returns List of productOtherCharges
	 * 				@param List<SalesLineItem>
	 * 				@return List<ProductOtherCharges>
	 * Created By: Rahul Verma
	 * Created for : Reusable method and can be use for everyone(This was created for NBHC product.
	 * Creation Date:17-10-2016
	 * 
	 */
	private List<ProductOtherCharges> updateProductTaxesTable(List<SalesLineItem> listOfItems) {
		// TODO Auto-generated method stub
		
		List<ProductOtherCharges> productOtherChargesList = new ArrayList<ProductOtherCharges>();
		
		for (SalesLineItem salesLineItem : listOfItems) {
		
			logger.log(Level.SEVERE,"salesLineItem.getVatTax().getTaxConfigName().trim()"+salesLineItem.getVatTax().getTaxConfigName().trim());
			logger.log(Level.SEVERE,"salesLineItem.getVatTax().getTaxName().trim()"+salesLineItem.getVatTax().getTaxName().trim());
			
			if (((salesLineItem.getVatTax() != null) && (salesLineItem
					.getVatTax().isInclusive()))
					&& (salesLineItem.getServiceTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Inclusive and Service Tax Not Present::::::::::::");
				// this is the value on which the tax will get
				// calculated
				// Formula : (Price-Price*VAT%) .....Because it is
				// inclusive
				assessableAmount = ((salesLineItem.getPrice() * quantity) - ((salesLineItem
						.getPrice() * quantity * salesLineItem
						.getVatTax().getPercentage()) / 100));
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = assessableAmount;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(salesLineItem
								.getVatTax().getTaxConfigName().trim());
				productOtherCharges
						.setChargePercent(salesLineItem
								.getVatTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* salesLineItem.getVatTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				totalAmtFix=totalAmtFix+totalAmount;
				netPayable = salesLineItem.getPrice();
				netPayFix=netPayFix+netPayable;
				productOtherChargesList.add(productOtherCharges);
			} else if (((salesLineItem.getVatTax() != null) && !(salesLineItem
					.getVatTax().isInclusive()))
					&& (salesLineItem.getServiceTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Exclusive and Service Tax Not Present::::::::::::");
				assessableAmount = salesLineItem.getPrice()
						* quantity;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = salesLineItem.getPrice()
						* quantity;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(salesLineItem
								.getVatTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(salesLineItem
								.getVatTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* salesLineItem.getVatTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = totalAmount + chargePayable;
				totalAmtFix=totalAmtFix+totalAmount;
				netPayFix=netPayFix+netPayable;
			} else if (((salesLineItem.getServiceTax() != null) && (salesLineItem
					.getServiceTax().isInclusive()))
					&& (salesLineItem.getVatTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax inclusive::::::::::::");
				assessableAmount = ((salesLineItem.getPrice() * quantity) - ((salesLineItem
						.getPrice() * quantity * salesLineItem
						.getServiceTax().getPercentage()) / 100));
				totalAmount = assessableAmount;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(salesLineItem
								.getServiceTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(salesLineItem
								.getServiceTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* salesLineItem.getServiceTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = salesLineItem.getPrice();
				totalAmtFix=totalAmtFix+totalAmount;
				netPayFix=netPayFix+netPayable;
			} else if (((salesLineItem.getServiceTax() != null) && !(salesLineItem
					.getServiceTax().isInclusive()))
					&& (salesLineItem.getVatTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax Exclusive::::::::::::");
				assessableAmount = salesLineItem.getPrice()
						* quantity;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = salesLineItem.getPrice()
						* quantity;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(salesLineItem
								.getServiceTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(salesLineItem
								.getServiceTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* salesLineItem.getServiceTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = totalAmount + chargePayable;
				totalAmtFix=totalAmtFix+totalAmount;
				netPayFix=netPayFix+netPayable;
			} else if (salesLineItem.getServiceTax() != null
					&& salesLineItem.getVatTax() != null) {

				logger.log(Level.SEVERE,
						"Vat Tax Present  and Service Tax Present::::::::::::");

				if (salesLineItem.getServiceTax()
						.isInclusive()
						&& salesLineItem.getVatTax()
								.isInclusive()) {
					logger.log(Level.SEVERE,
							"Vat Tax Inclusive and Service Tax Inclusive::::::::::::");

					netPayable = salesLineItem.getPrice()
							* quantity;
					// For Tax
					double assessableAmountForST = (netPayable * 100)
							/ (100 + salesLineItem
									.getServiceTax()
									.getPercentage());
					logger.log(Level.SEVERE,
							"assessableAmountForST::::::::::::"
									+ assessableAmountForST);

					double assessableAmountForVT = (assessableAmountForST * 100)
							/ (100 + salesLineItem.getVatTax()
									.getPercentage());
					logger.log(Level.SEVERE,
							"assessableAmountForVT::::::::::::"
									+ assessableAmountForVT);
					totalAmount = assessableAmountForVT;
					totalAmtFix=totalAmtFix+totalAmount;
					netPayFix=netPayFix+netPayable;
				
					// For VAT
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(salesLineItem
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(salesLineItem
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountForVT);
					double chargePayableVT = assessableAmountForVT
							* salesLineItem.getVatTax()
									.getPercentage() / 100;
					productOtherCharges
							.setChargePayable(chargePayableVT);
					logger.log(Level.SEVERE,
							"chargePayableVT::::::::::::"
									+ chargePayableVT);
					productOtherChargesList
							.add(productOtherCharges);

					// For ST
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(salesLineItem
									.getServiceTax().getTaxConfigName());
					productOtherChargesST
							.setChargePercent(salesLineItem
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountForST);
					double chargePayableST = assessableAmountForST
							* salesLineItem.getServiceTax()
									.getPercentage() / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);

				} else if (!(salesLineItem.getServiceTax()
						.isInclusive())
						&& !(salesLineItem.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Exclusive and Service Tax Exclusive::::::::::::");
					totalAmount = salesLineItem.getPrice();
					double assessableAmountForVT = totalAmount;
					logger.log(Level.SEVERE,
							"assessableAmountForVT::::::::::::"
									+ assessableAmountForVT);
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(salesLineItem
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(salesLineItem
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountForVT);
					double chargePayableVT = (assessableAmountForVT * salesLineItem
							.getVatTax().getPercentage()) / 100;
					productOtherCharges
							.setChargePayable(chargePayableVT);
					logger.log(Level.SEVERE,
							"chargePayableST::::::::::::"
									+ chargePayableVT);
					productOtherChargesList
							.add(productOtherCharges);

					// For ST
					double assessableAmountForST = totalAmount
							+ chargePayableVT;

					logger.log(Level.SEVERE,
							"assessableAmountForST::::::::::::"
									+ assessableAmountForST);
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(salesLineItem
									.getServiceTax().getTaxConfigName());
					productOtherChargesST
							.setChargePercent(salesLineItem
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountForST);
					double chargePayableST = assessableAmountForST
							* salesLineItem.getServiceTax()
									.getPercentage() / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					netPayable = assessableAmountForST
							+ chargePayableST;
					totalAmtFix=totalAmtFix+totalAmount;
					netPayFix=netPayFix+netPayable;
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);
				} else if (salesLineItem.getServiceTax()
						.isInclusive()
						&& !(salesLineItem.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Exclusive and Service Tax Inclusive::::::::::::");
					// ServiceTax=Inclusive && VatTax=Exclusive....
					// Here we are adding VAT TAX first because it
					// is
					// exclusive, and calculating NetPayable value.
					// The
					// we will do reverse calculation
					double netPay = (salesLineItem.getPrice() * salesLineItem
							.getVatTax().getPercentage()) / 100;
					netPayable = netPay;

					double assessableAmountST = (netPayable * 100)
							/ (100 + salesLineItem
									.getServiceTax()
									.getPercentage());
					double assessableAmountVT = (assessableAmountST * 100)
							/ (100 + salesLineItem.getVatTax()
									.getPercentage());

					totalAmount = assessableAmountVT;

					// Now we will calculate VAT TAX
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(salesLineItem
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(salesLineItem
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountVT);
					productOtherCharges
							.setChargePayable((assessableAmountVT * salesLineItem
									.getVatTax().getPercentage()) / 100);
					productOtherChargesList
							.add(productOtherCharges);

					// First we are calculating Service Tax of the
					// product to find assessable amount to show in
					// product
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(salesLineItem
									.getServiceTax().getTaxConfigName());
					productOtherChargesST
							.setChargePercent(salesLineItem
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountST);
					productOtherChargesST
							.setChargePayable((assessableAmountST * salesLineItem
									.getServiceTax()
									.getPercentage()) / 100);
					productOtherChargesList
							.add(productOtherChargesST);
					totalAmtFix=totalAmtFix+totalAmount;
					netPayFix=netPayFix+netPayable;
				
				} else if (!(salesLineItem.getServiceTax()
						.isInclusive())
						&& (salesLineItem.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Inclusive and Service Tax Exclusive ::::::::::::");
					// ServiceTax=Exclusive && VatTax=Inclusive....
					double assesableAmountST = salesLineItem
							.getPrice();
					double assesableAmountVT = (assesableAmountST * 100)
							/ (100 + salesLineItem
									.getServiceTax()
									.getPercentage());
					totalAmount = assesableAmountVT;
					netPayable=assesableAmountST;
					
					netPayFix=netPayFix+netPayable;
					
					totalAmtFix=totalAmtFix+totalAmount;
				
					// Calculation of VAT TAX
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(salesLineItem
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(salesLineItem
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assesableAmountVT);
					double chargePayable = (assesableAmountVT * salesLineItem
							.getVatTax().getPercentage()) / 100;
					productOtherCharges
							.setChargePayable(chargePayable);
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayable);
					productOtherChargesList
							.add(productOtherCharges);
					// Calculation for Service Tax
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(salesLineItem
									.getServiceTax().getTaxConfigName());
					productOtherChargesST
							.setChargePercent(salesLineItem
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assesableAmountST);
					double chargePayableST = (assesableAmountST * salesLineItem
							.getServiceTax().getPercentage()) / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					logger.log(Level.SEVERE,
							"chargePayableST::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);
				}

			} else {
				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax Not Present::::::::::::");
				totalAmount = salesLineItem.getPrice();
				netPayable = totalAmount;
				totalAmtFix=totalAmtFix+totalAmount;
				netPayFix=netPayFix+netPayable;
			}
		
		}
		
		return productOtherChargesList;
	}

}
