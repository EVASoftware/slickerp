package com.slicktechnologies.server.printing;

import java.net.URL;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;


/**
 * Responsible for Printing the {@link Printable}
 */
public abstract class Printer 
{
	
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12;
	private  Document document;
	
	Printable object;
	
	/**
	 * Zero Arg Constructor initalizes the fonts.
	 */
	
	public Printer()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
	
		
	}
	
	
	
	
	/**
	 * Implementation of Method is Faulty
	 */
	public void printCompanyLogo()
	{
		String logoUrl=object.getCompanyLogo();
		if(logoUrl.equals(""))
			return;
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+logoUrl));
			image2.scaleAbsolute(60f,60f);
			document.add(image2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		
		
	   }  	
	}
	
	public void printCompanyInfo()
	{
		this.printComapnyName();
		this.printComapnyContact();
		this.printTaxInfo();
	}
	
	public void printComapnyName() 
	{
		String stringcompanyName=object.getCompanyName(); 
		Phrase companyName= new Phrase(stringcompanyName,font16boldul);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     try {
			document.add(p);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void printComapnyContact()
	{
		Contact contact=object.getCompanyContact();
		Address adress=contact.getAddress();
		Phrase adressline1= new Phrase(adress.getAddrLine1(),font12);
		Phrase adressline2=null;
		if(adress.getAddrLine2()!=null)
			adressline2= new Phrase(adress.getAddrLine2(),font12);
		Phrase landmark=null;
		Phrase locality=null;
		if(adress.getLandmark()!=null&&adress.getLocality().equals("")==false)
		{
			
			String landmarks=adress.getLandmark()+" , ";
			locality= new Phrase(landmarks+adress.getLocality()+" , "+adress.getCity()+" , "
				      +adress.getPin(),font12);
		}
		else
		{
			locality= new Phrase(adress.getLocality()+" , "+adress.getCity()+" , "
				      +adress.getPin(),font12);
		}
		Paragraph adressPragraph=new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if(adressline2!=null)
		{
				adressPragraph.add(adressline2);
				adressPragraph.add(Chunk.NEWLINE);
		}
		
		
		
		adressPragraph.add(locality);
		
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);
		
		
		// Phrase for phone,landline ,fax and email
		
		Phrase titlecell=new Phrase("Mob :",font8bold);
		Phrase titleTele = new Phrase("Tele :",font8bold);
		Phrase titleemail= new Phrase("Email :",font8bold);
		Phrase titlefax= new Phrase("Fax :",font8bold);
		
		Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
		Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
		String servicetax="               ";
		String vatttax="            ";
		Phrase servicetaxphrase=null;
		Phrase vattaxphrase=null;
		
		if(servicetax!=null&&servicetax.trim().equals("")==false)
			servicetaxphrase=new Phrase(servicetax,font8);
		
		if(vatttax!=null&&vatttax.trim().equals("")==false)
			vattaxphrase=new Phrase(vatttax,font8);
		
		
	
		
		
		// cell number logic
		String stringcell1=contact.getCellNo1()+"";
		String stringcell2=null;
		Phrase mob=null;
		if(contact.getCellNo2()!=null&&contact.getCellNo2()!=0)
			stringcell2=contact.getCellNo2()+"";
		if(stringcell2!=null)
		    mob=new Phrase(stringcell1+" / "+stringcell2,font8);
		else
			 mob=new Phrase(stringcell1,font8);
		
		
		//LANDLINE LOGIC
		Phrase landline=null;
		if(contact.getLandline()!=null&&contact.getLandline()!=0)
			landline=new Phrase(contact.getLandline()+"",font8);
		
		
		
		
		// fax logic
		Phrase fax=null;
		if(contact.getFaxNo()!=null && contact.getFaxNo()!=0)
			fax=new Phrase(contact.getFaxNo()+"",font8);
		// email logic
		Phrase email= new Phrase(contact.getEmail(),font8);
		
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("            "));
		
		
		if(landline!=null)
		{
			
			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("            "));
		}
		
		
		
		if(fax!=null)
		{
			
			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}
		
		
		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk("            "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		
		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public void printTaxInfo()
	{
		
	}
	
	public void printDocumentName()
	{
		
	}
	
	public void printCustomerInfo()
	{
		
	}
	
	public void printDocumentInfo()
	{
		
	}
	
	public void printFooter()
	{
		
	}
	
	public abstract void printBusinessLogic();




	public Document getDocument() {
		return document;
	}




	public void setDocument(Document document) {
		this.document = document;
	}
	
	public void cratePdf()
	{
		this.printComapnyName();
		this.printComapnyContact();
	}
}

