package com.slicktechnologies.server.printing;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

@SuppressWarnings("serial")
public class PrinterServlet extends HttpServlet {

 @Override
 protected void doGet(HttpServletRequest request,
   HttpServletResponse response) throws ServletException, IOException {
  
	 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
  

  try {

   Printer cont=new ContractPrinter();
   
   Document document = cont.getDocument();
   PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
   document.open();
   cont.cratePdf();
   document.close();
  } 
  catch (DocumentException e) {
	  e.printStackTrace();
  }
 
   }

}
