package com.slicktechnologies.server.printing;

import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;

/**
 * Any thing which can be Printed  will implement this interface.
 * @author Ajay
 *
 */
public interface Printable 
{
	
	/**
	 * Provides Company Logo
	 */
	public String getCompanyLogo();
	/**
	 * Provides company Info
	 */
	public String getCompanyName();
	
	/**
	 * Get Company Adresss
	 */
	
	public Contact getCompanyContact();
	
	
	
	
	

	/**
	 * provides Customer Info
	 */
	void getCustomerInfo();
	/**
	 * provides Document Name
	 */
	void getDocumentName();
	/**
	 * provides Document Info
	 */
	void getDocumentInfo();
	/**
	 * provides business logic
	 */
	void getBuisnessLogic();

}
