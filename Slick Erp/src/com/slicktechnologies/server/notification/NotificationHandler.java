package com.slicktechnologies.server.notification;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.notification.NotificationService;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.helperlayer.User;

public class NotificationHandler  extends RemoteServiceServlet implements NotificationService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 301451152727741358L;
	Logger logger=Logger.getLogger("NotificationHandler.class");
	
	@Override
	public ArrayList<Integer> sendNotification(long companyId, String employeeName,
			String toApplication,String messageData) {
		String regId = null,applicationName = null,message = null;
		applicationName=toApplication;
		message=messageData;
		ArrayList<Integer> intList=new ArrayList<Integer>();
		User user=ofy().load().type(User.class).filter("companyId",companyId).filter("employeeName", employeeName.trim()).first().now();
		if(user==null){
			intList.add(3);
			return intList;
		}
		RegisterDevice registerDevice=ofy().load().type(RegisterDevice.class).filter("companyId", companyId).filter("applicationName", applicationName.trim()).filter("userName", user.getUserName()).first().now();
		if(registerDevice==null){
			intList.add(2);
			return intList;
		}
		regId=registerDevice.getRegId();
		String data=sendCentralisedNotification(regId,applicationName,message);
		if(data.trim().equalsIgnoreCase("Successfull")){
			intList.add(1);
		}else{
			intList.add(0);
		}
		
		return intList;
	}

	private String sendCentralisedNotification(String regId,
			String applicationName, String message) {
		// TODO Auto-generated method stub
		final String URL="http://my.essevaerp.appspot.com/slick_erp/pushNotificationsHandler?regId="+regId+"&applicationName="+applicationName+"&message="+message;
		String data="";
		
		URL url = null;
		try {
			url = new URL(URL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        //Log.d("Test", "From Get Post Method" + getPostData(values));
//        try {
//			writer.write();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
//			e.printStackTrace();
//		}
        try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        BufferedReader br = new BufferedReader(
                new InputStreamReader(is));
        String temp;

        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp;
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"Data::::::::::"+data);
		
		return data;
	}

}
