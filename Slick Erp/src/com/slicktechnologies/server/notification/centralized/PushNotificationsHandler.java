package com.slicktechnologies.server.notification.centralized;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class PushNotificationsHandler extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6114689778807100367L;
	Logger logger = Logger.getLogger("PushNotificationsHandler.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");

		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", splitUrl[0]).first().now();
		if (company == null) {
			company = ofy().load().type(Company.class)
					.filter("accessUrl", "127").first().now();
		}
		String deviceName=req.getParameter("regId");
		String applicationName=req.getParameter("applicationName");
		String message=req.getParameter("message");
		logger.log(Level.SEVERE,"deviceName-"+deviceName);
		logger.log(Level.SEVERE,"applicationName-"+applicationName);
		logger.log(Level.SEVERE,"message-"+message);
		
		sendPushNotification(company.getCompanyId(), applicationName, message, deviceName, company);
		resp.getWriter().print("Successfull");
	}
	
	private void sendPushNotification(long companyId, String applicationName,
			String message, String deviceName, Company company) {
//		CustomerUser customerUser=ofy().load().type(CustomerUser.class).filter("companyId",companyId).filter("cinfo.fullName",service.getPersonInfo().getFullName()).first().now(); 
		
//		String deviceName = deviceName;device.getRegId().trim();
		logger.log(Level.SEVERE,"deviceName"+deviceName);
		URL url = null;
		try {
			url = new URL("https://fcm.googleapis.com/fcm/send");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn.setDoOutput(true);
		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "key="
				+ company.getFcmServerKeyForKreto().trim());
		conn.setDoOutput(true);
		conn.setDoInput(true);

		try {
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = conn.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createJSONData(message, deviceName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;
		String data = "";
		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);
	}
	
	private String createJSONData(String message, String deviceName) {
		// TODO Auto-generated method stub
		org.json.simple.JSONObject jObj1 = new org.json.simple.JSONObject();
		jObj1.put("message", message);

		org.json.simple.JSONObject jObj = new org.json.simple.JSONObject();
		jObj.put("data", jObj1);
		jObj.put("to", deviceName);

		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON Data Sent:::::::" + jsonString);

		return jsonString;
	}


}
