package com.slicktechnologies.server;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.LogoutService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.sessionmanagement.SessionServiceImpl;
//import com.slicktechnologies.server.sessionmanagement.SessionServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.User;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class LogoutServiceImpl extends RemoteServiceServlet implements LogoutService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5749013434833830765L;

	Logger logger=Logger.getLogger("Logout Logger");
	
	@Override
	public User doLogout(Long companyId, String username, String remark,int sessionId) {
		
	//  rohan added session count to logout specific sessionsonly
	// Date : 14/3/2017
		
		LoggedIn loggedin  = ofy().load().type(LoggedIn.class).filter("companyId", companyId).filter("userName", username).filter("status", AppConstants.ACTIVE).filter("count", sessionId).first().now();

		if(loggedin!=null){
			loggedin.setStatus(AppConstants.INACTIVE);
			loggedin.setLogoutDate(DateUtility.getDateWithTimeZone("IST", new  Date()));
			loggedin.setLogoutTime(DateUtility.getTimeWithTimeZone("IST", new Date()));
			loggedin.setRemark(remark);
			ofy().save().entity(loggedin).now();
			
			for(int n=0;n<SessionServiceImpl.arrOfSession.size();n++)
			{
				if(SessionServiceImpl.arrOfSession.get(n).getUserName().trim().equals(username.trim())
						&& SessionServiceImpl.arrOfSession.get(n).getCompanyId()==companyId
						&& SessionServiceImpl.arrOfSession.get(n).getSessionId()==sessionId)
				{
					SessionServiceImpl.arrOfSession.remove(n);
				}
			}
			logger.log(Level.SEVERE,"on do logout="+SessionServiceImpl.arrOfSession.size());
			
			}
		
		return null;
	}

	@Override
	public User doLogoutonBrowserClose(Long companyId, String username,String remark,int sessionId) {
	
	//  rohan added session count to logout specific sessionsonly
	// Date : 14/3/2017
		
		LoggedIn loggedin  = ofy().load().type(LoggedIn.class).filter("companyId", companyId).filter("userName", username.trim()).filter("status", AppConstants.ACTIVE).filter("count", sessionId).first().now();

		if(loggedin!=null){
			loggedin.setStatus(AppConstants.INACTIVE);
			loggedin.setLogoutDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			loggedin.setLogoutTime(DateUtility.getTimeWithTimeZone("IST", new Date()));
			loggedin.setRemark(remark);
			ofy().save().entity(loggedin).now();
			
			for(int n=0;n<SessionServiceImpl.arrOfSession.size();n++)
			{
				if(SessionServiceImpl.arrOfSession.get(n).getUserName().trim().equals(username.trim())
						&& SessionServiceImpl.arrOfSession.get(n).getCompanyId()==companyId
						&& SessionServiceImpl.arrOfSession.get(n).getSessionId()==sessionId)
				{
					SessionServiceImpl.arrOfSession.remove(n);
				}
			}
			
			logger.log(Level.SEVERE,"doLogoutonBrowserClose="+SessionServiceImpl.arrOfSession.size());
			}
		
		return null;
	}

}
