package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.AmcAssuredRevenueReport;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.ServiceRevanueReportBean;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.sms.SMSDetails;

public class ContractServiceImplementor extends RemoteServiceServlet implements ContractService
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8606683866365273117L;
	Logger logger=Logger.getLogger("Services Creation On Approval");
	
	@Override
	public void changeStatus(Contract contract)
	{
		logger.log(Level.SEVERE,"Change Status Called");
	  if(contract!=null)
	  {
		  if(contract.getStatus().equals(Contract.APPROVED)){
			  logger.log(Level.SEVERE,"Making Services");
			  /* this condition added by vijay for when contract Rate checkbox is true then services will not created
			   * 
			   */
			  logger.log(Level.SEVERE," old contract value iscontractRate === "+contract.isContractRate());
			  if(contract.isContractRate()==false)  {  // vijay for contract rate
				  System.out.println("inside services");
				  makeServices(contract);
				  
				  
			  }
			  
//			  /*
//			   * Date:20/07/2018
//			   * Developer:Ashwini
//			   * Des:To enable only AMC(contract group)contract
//			   */
//			  if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableOnlyAMCContractForContractRenewal",contract.getCompanyId() ) ){
//					if(!contract.getGroup().equalsIgnoreCase("AMC")){
//						contract.setCustomerInterestFlag(true);
//					}
//				}
//				/*
//				 * end by Ashwni
//				 */
			  /**
			   * @author Vijay Date 12-11-2020
			   * Des :- Peccop custom development if contract group is One Time then it will marked as do not renew so it will
			   * not availble for Renewal above old code commeted added new code here as per new requirement
			   */
			  /**
			   * @author Vijay Date 05-03-2011
			   * Des :- As per Nitin Sir If contract group is One Time or Single then it should not come for renewal and renewal reminder
			   */
//			  if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "DisableOneTimeContractForContractRenewal",contract.getCompanyId() ) ){
					if(contract.getGroup()!=null && !contract.getGroup().equals("") && contract.getGroup().equalsIgnoreCase("One Time") || contract.getGroup().equalsIgnoreCase("Single")){
						contract.setCustomerInterestFlag(true);
						contract.setNonRenewalRemark("One Time Contract");
					}
//				}
			  
			  /**
			   * Date : 11-02-2017 By Anil
			   * moved this code form outside if block to inside because it is specifically used at the time of approval 
			   * its not required at the makeServices
			   */
			  changeCustomerStatus(contract);
			  changeQuotationStatus(contract);
			  ofy().save().entity(contract);
			  
			  /**
			   * End
			   */
			  
			   /**
				 * Date 28-02-2018 By vijay 
				 * for changing lead status 
				 */
				ChangeLeadStatus(contract);
				/**
				 * ends here
				 */
		  }
		  
		  if(contract.getStatus().equals(Contract.CANCELLED)){
			  cancelServices(contract);
		  }
		  //   old code 
//		  changeCustomerStatus(contract);
//		  changeQuotationStatus(contract);
//		  ofy().save().entity(contract);
				
		  System.out.println("Start Date After Save is "+contract.getStartDate());
		  System.out.println("End Date is After Save "+contract.getEndDate());
	  }
	}
	
	
	 public void ChangeLeadStatus(Contract con) {

	    	if(con.getLeadCount()!=0){
	    		Lead leadEntity = ofy().load().type(Lead.class).filter("companyId", con.getCompanyId()).filter("count", con.getLeadCount()).first().now();
	    		if(leadEntity!=null){
	    			leadEntity.setStatus(Quotation.QUOTATIONSUCESSFUL);
	    			ofy().save().entity(leadEntity);
	    			
	    			/**Added by Sheetal : 16-03-2022
	    			 * Des : For Sending SMS automatically on lead successful event**/
	    			 SMSDetails smsdetails = new SMSDetails();
	    			 smsdetails.setModel(leadEntity);
	    			 SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", leadEntity.getCompanyId()).filter("event", "Lead Successful").filter("status",true).first().now();
	    			 if(smsEntity!=null) {
	    			 smsdetails.setSmsTemplate(smsEntity);
	    			 smsdetails.setEntityName("Lead");
	    			 String mobileNo=Long.toString(leadEntity.getPersonInfo().getCellNumber());
	    			 smsdetails.setMobileNumber(mobileNo);
	    			 smsdetails.setModuleName(AppConstants.SERVICEMODULE);
	    			 smsdetails.setDocumentName(AppConstants.LEAD);
	    			 
	    			 SmsServiceImpl sendSms = new SmsServiceImpl();
	    			 String smsmsg = sendSms.validateAndSendSMS(smsdetails);
	    			 logger.log(Level.SEVERE," inside validateAndSendSMS lead successful=="+smsmsg);
	    			 }
	    			 /**end**/
	    		}
	    	}
		}
	 
	public void makeServices(Contract con)
	{
		logger.log(Level.SEVERE, "Service Creation Method");
		/**
		  * Date 21-09-2019 by Vijay for NBHC CCPM WMS To EVA Fumigation Services internal Customer
		  * Service Address must pick from contract only
		 */ 
		boolean warehouseaddressflag = false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "EnableWarehouseAddressasServiceAddress", con.getCompanyId())){
			warehouseaddressflag = true;
		}
		ArrayList<SuperModel> services = new ArrayList<SuperModel>();
		Customer cust = null;
		Address addr = null;
		CustomerBranchDetails custBrach;
		double totalservices = 0;
		totalservices = gettotalnumberofservices(con.getItems());
		/** 29-09-2017 sagar sore [] **/
//		double serviceValue = 0;
		
		/**
		 * Date : 04-09-2017 By ANIL 
		 * if service schedule list is empty then we check another entity ServiceSchedule 
		 * in which we store schedule services if service count is more than thousand. 
		 */
		 List<ServiceSchedule> schList=new ArrayList<>();
		 ofy().clear();
		 if(con.getServiceScheduleList().size()==0){
			schList=ofy().load().type(ServiceSchedule.class).filter("companyId", con.getCompanyId()).filter("documentType", "Contract").filter("documentId", con.getCount()).list();
			if(schList!=null){
				logger.log(Level.SEVERE,"FROM DB LIST SIZE : "+schList.size());
			}
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
		 }
		 /**
		  * End
		  */
		 

		 /**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  *  26-09-2018 for map bill for material
		  */
		 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", con.getCompanyId());
		 boolean billofMaterialActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", con.getCompanyId());
	
		/**
		 * Date 12-09-2017 added by vijay for for task queue services number
		 * genration first updated and below olc code commented
		 **/ 
		 int count=0;
		 
		 /**
		  * @author Anil
		  * @since 06-06-2020
		  * Premium tech : recalculating no. of services based on asset qty, no. of branch and no. of services
		  * also retriving asset id 
		  */
//		 int assetCount=0;
		 boolean complainTatFlag=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ComplainServiceWithTurnAroundTime", con.getCompanyId());
		 if(complainTatFlag){
			 totalservices=getTotalNoOfServices(con.getItems());
			 logger.log(Level.SEVERE,"COMP totalservices "+totalservices);
			 try{
				 generateAndUpdateAssetNum(con.getItems(),con.getCompanyId());
			 }catch(Exception e){
				 
			 }
		 }
		 
	 	/**
		 * @author Vijay Chougule Date 09-09-2020
		 * Des :- Adding Service duration with process config for LifeLine
		 */
		 boolean serviceDurationFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_ADDSERVICEDURATIONCOLUMN,con.getCompanyId());
		
		/**
	     * @author Vijay Chougule
	     * Des :- if Automatich Scheduling process Config is active then Service Project will create with Automatic
	     * Service Scheduling for Technician App material Map from BOM.
	     */
		 boolean automaticScheduling = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.AUTOMATICSCHEDULING, con.getCompanyId());
	   /**
		 * @author Vijay Chougule Date 24-09-2020
		 * Des :- For Free Material Services will Schedule by default for Technician App
		 */
//		 boolean freeMaterialFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL,con.getCompanyId());
		 boolean freeMaterialFlag = con.isStationedTechnician();

		 ServerAppUtility serverapputility = new ServerAppUtility();
		 
		 Complain complainEntity=null;
		if(con.getComplaintFlag()!=null&&con.getTicketNumber()!=-1 && con.getComplaintFlag()){
			complainEntity=ofy().load().type(Complain.class).filter("companyId", con.getCompanyId()).filter("count", con.getTicketNumber()).first().now();
		}

		 if (totalservices > 50) {

			long companyId = con.getCompanyId();
			NumberGeneration numbergen = null;
			boolean processconfigflag = checkprocessconfiguration(con);
			
			/**
			 *@author Anil , Date  : 18-10-2019 
			 */
			SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
			if (processconfigflag) {
				logger.log(Level.SEVERE, "In the Number Range process config true "+ con.getNumberRange());
				
				if (con.getNumberRange() != null && !con.getNumberRange().equals("")) {
//					logger.log(Level.SEVERE, "111111111111");
					String selectedNumberRange = con.getNumberRange();
					String prefixselectedNumberRange = "S_"+ selectedNumberRange;
//					logger.log(Level.SEVERE, "22222222222");
					count=(int) numGen.getLastUpdatedNumber(prefixselectedNumberRange, con.getCompanyId(), (long) totalservices);
//					numbergen = ofy().load().type(NumberGeneration.class)
//							.filter("companyId", con.getCompanyId())
//							.filter("processName",prefixselectedNumberRange)
//							.filter("status", true).first().now();
				} else {
//					logger.log(Level.SEVERE, "33333333333333");
					count=(int) numGen.getLastUpdatedNumber("Service", con.getCompanyId(), (long) totalservices);
//					numbergen = ofy().load().type(NumberGeneration.class)
//							.filter("companyId", companyId).filter("processName", "Service")
//							.filter("status", true).first().now();
				}
			} else {
//				logger.log(Level.SEVERE, "44444444444444");
				count=(int) numGen.getLastUpdatedNumber("Service", con.getCompanyId(), (long) totalservices);
//				numbergen = ofy().load().type(NumberGeneration.class)
//						.filter("companyId", companyId)
//						.filter("processName", "Service")
//						.filter("status", true).first().now();
			}

			/**************************
			 * ende here some change while saving services bellow for normal
			 * services & taskqueue services
			 *********************************/

			/**
			 * @author Anil ,Date : 18-10-2019
			 * saving number generation entity directly
			 */
//			long number = numbergen.getNumber();
//			logger.log(Level.SEVERE, "Last Number====" + number);
//			count = (int) number;
//			number = number + (int) totalservices;
//			numbergen.setNumber(number);
//			GenricServiceImpl genimpl = new GenricServiceImpl();
//			genimpl.save(numbergen);
//			ofy().save().entity(numbergen).now();
		}
		 
	   /*************** Ends here ***************************/ 	 
		/**
		 * 18-06-2018
		 * nidhi
		 * 
		 */
		HashSet<String> branchSet = new HashSet<String>();
		for (int i=0 ; i< con.getItems().size(); i++)
		{
			Set<Integer> ser =  con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet();
			if(ser!=null) {
			for(Integer seq : ser){
				ArrayList<BranchWiseScheduling> branch =  con.getItems().get(i).getCustomerBranchSchedulingInfo().get(seq);
				for(int k =0 ;k<branch.size();k++){
					if(!branch.get(k).getBranchName().equalsIgnoreCase("Service Address")
							&& !branch.get(k).getBranchName().equalsIgnoreCase("Select")  && branch.get(k).isCheck())
					{
						branchSet.add(branch.get(k).getBranchName());
					}
				}
				
		}
			}
		}
		
		List<CustomerBranchDetails> custBranchList = new ArrayList<CustomerBranchDetails>();
		ArrayList <String> branchList = new  ArrayList<String>();
		if(branchSet.size()>0){
			branchList.addAll(branchSet);
			custBranchList = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName IN",  branchList).filter("cinfo.count", con.getCinfo().getCount())
			   .filter("companyId",con.getCompanyId()).list();
		}
		
		HashMap<String, Address> addList = new HashMap<String, Address>();
		HashMap<String, String> costCenterList = new HashMap<String, String>(); //Ashwini Patil Date:22-03-2024 orion wants to map cost center from customer branch to service
	
		for(CustomerBranchDetails branchDt : custBranchList){
			addList.put(branchDt.getBusinessUnitName(), branchDt.getAddress());
			if(branchDt.getCostCenter()!=null)
				costCenterList.put(branchDt.getBusinessUnitName(), branchDt.getCostCenter());
		}
		/**
		 * end
		 */
		
		logger.log(Level.SEVERE, "total services="+totalservices);
		
		for(SalesLineItem item:con.getItems())
		{
			
			/**
			 * Date : 25-07-2017 By ANIL
			 * Added try catch block to avoid repetitive execution of same code as contract approval process 
			 * execute through task queue process which result in creating excess services.
			 */
			
			try{
				/** Date 12-09-2017 new code above added by vijay for for task queue services number genration first updated and below old code commented  ***/ 
			
			if(item.getPrduct() instanceof ServiceProduct)
		    {
			   if(con.getCinfo()!=null)
			   {
				   if(con.getCompanyId()!=null)	
				   {
					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
					   filter("companyId",con.getCompanyId()).first().now();
				   }
				   else
				   {
					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
							   first().now();  
				   }
			   }
			   
			   
			   //  rohan commented this code and added below code 
//					//A patch as Neither Customer nor Contract has Customer Address
//					if(cust!=null)
//					    addr=cust.getSecondaryAdress();
				
			   /***
			    * Date : 05-09-2017 By ANIL
			    * If in contract we didn't have schedule services then we can load schedule list from ServiceSchedule entity 
			    * as we are storing in separate entity if service size is greater that 1000.
			    */
			
			   if(con.getServiceScheduleList().size()==0){
				   
				   for(int k=0;k<schList.size();k++)
					{
						//  rohan comments this code for adding similar product in contract (pesto change)
						
						if(item.getProductSrNo()==schList.get(k).getSerSrNo()){
							
							/** Developed by : Rohan D Bhagde.
							 * Reason : added this code for adding selected customer branch address as service address.
							 * ie. map branch address in service address if branch is selected in service schedule pop up.
							 * 
							 *   date : 26/10/2016
							 */
							
							if(schList.get(k).getScheduleProBranch().equalsIgnoreCase("Service Address"))
							{
								if(cust!=null)
								    addr=cust.getSecondaryAdress();
							}
							else if(schList.get(k).getScheduleProBranch().equalsIgnoreCase("Select")){
								if(cust!=null)
								    addr=cust.getSecondaryAdress();
							}
							else
							{
								   if(schList.get(k).getScheduleProBranch()!=null)	
								   {/*
									   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",schList.get(k).getScheduleProBranch()).
									   filter("companyId",con.getCompanyId()).first().now();
								   }
								   else
								   {
									   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",schList.get(k).getScheduleProBranch()).
											   first().now();  
								  */ 
									   

									   /**
										 * 18-06-2018
										 * nidhi
										 * 
										 */
									   /*custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",schList.get(k).getScheduleProBranch()).
									   filter("companyId",con.getCompanyId()).first().now();
								   }
								   else
								   {
									   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",schList.get(k).getScheduleProBranch()).
											   first().now(); */
									  
									   addr = addList.get(schList.get(k).getScheduleProBranch());
									   /**
									    * end
									    */
								   
								   }
								   
//								   addr = custBrach.getAddress();
								   
							}
							
							/**
							 *  ends here 
							 */
							
								/**
								 * Date :18-10-2019 , Anil
								 */
//								int setcount = ++count;
								int setcount = count++;
								/**
								 * 
								 */
								
								int seviceindex = k+1;
								
								String refNo="";
								if(con.getRefNo()!= null && !con.getRefNo().equals(""))
								{
									refNo = con.getRefNo();
								}
								else
								{
									refNo = 0+"";
								}
								
								String refno2 ="";
								if(con.getRefNo2()!= null && !con.getRefNo2().equals(""))
								{
									refno2 = con.getRefNo2();
								}
								else
								{
									refno2 = 0+"";
								}
								
								String techName ="";
								if(con.getTechnicianName()!= null && !con.getTechnicianName().equals(""))
								{
									techName = con.getTechnicianName();
								}
								else
								{
									techName = 0+"";
								}
								
								
								String serviceRemark="";
								if(schList.get(k).getServiceRemark()!=null && !schList.get(k).getServiceRemark().equals("")){
									serviceRemark=schList.get(k).getServiceRemark();
								}else{
									serviceRemark=0+"";
								}
								
								System.out.println(" Service Remark =="+serviceRemark);
								
								/**29-09-2017 sagar sore [setting service value through taskqueue also added to serviceString]**/
								//serviceValue=item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
//								serviceValue=item.getTotalAmount()/item.getNumberOfServices();//Ashwini Patil
//								logger.log(Level.SEVERE, "serviceValue1="+serviceValue);
								/**
								 * Date : 06-10-2017 BY ANIL
								 */
								String serviceWiseBilling="NO";
								if(con.isServiceWiseBilling()){
									serviceWiseBilling="YES";
								}
								/**
								 * End
								 */
								/**
								 * Date : 13-11-2017 BY ANIL
								 * setting servicing branch as branch name in service 
								 * earlier it was contract's branch
								 */
								String servicingBranch="";
								if(schList.get(k).getServicingBranch()!=null
										&&!schList.get(k).getServicingBranch().equals("")){
									servicingBranch=schList.get(k).getServicingBranch();
								}else{
									servicingBranch=con.getBranch();
								}
								/**
								 * End
								 */
								/** date 17.02.2018 added by komal for area wise billing**/
								/**
								 * nidhi
								 * 22-05-2018
								 * for nbhc
								 */
								boolean priceStatus = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CustomerBranchAreaWiseBilling", con.getCompanyId());
								
								double perUnitPrice = 0;
								String costCenter = "na";
								double area = 0;
								String uom = "na";
								
								double tax = 0;
								double origPrice = 0;
								double serviceValue =0;
								
								tax = removeAllTaxes(item.getPrduct());
								//origPrice = item.getPrice() - tax;
								origPrice=item.getPrice(); //Ashwini Patil
								
								
								/*
								 * Ashwini Patil 
								 * Date:6-03-2024 
								 * Area from branch popup on contract product line is not getting mapped to quantity field in service and not getting printed on orion's service docket
								 */
								Set<Integer> ser =  item.getCustomerBranchSchedulingInfo().keySet();
								
								for(Integer seq : ser){
									ArrayList<BranchWiseScheduling> branch =  item.getCustomerBranchSchedulingInfo().get(seq);
									if(branch!=null) {
										for(BranchWiseScheduling binfo:branch){
											if(binfo.getBranchName().equalsIgnoreCase(schList.get(k).getScheduleProBranch())//(con.getServiceScheduleList().get(k).getScheduleProBranch())
													&& binfo.isCheck())
											{
												if(binfo.getArea()>0)
													area=binfo.getArea();
												
												if(costCenterList!=null&&costCenterList.containsKey(binfo.getBranchName()))
													costCenter=costCenterList.get(binfo.getBranchName());
											}
										}
									
									}
								}
								
								String serviceType="Periodic";
								if(con.getServicetype()!=null&&!con.getServicetype().equals("")){
									serviceType=con.getServicetype(); //Ashwini Patil Date:29-02-2024 for orion
								}
								
								
								if(con.isServiceWiseBilling() && priceStatus){
									
									if(!(item.getArea().equalsIgnoreCase("NA"))){
									logger.log(Level.SEVERE , "inside area");
									CustomerBranchDetails branches = ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId())	.filter("status", true).filter("cinfo.count", con.getCinfo().getCount()).filter("buisnessUnitName", con.getServiceScheduleList().get(k).getScheduleProBranch()).first().now();
									logger.log(Level.SEVERE , "branches :" + branches);
									if(branches != null){
										if(branches.getCostCenter() !=null){
											costCenter = branches.getCostCenter();
										}
									if(branches.getArea()!=0){
										logger.log(Level.SEVERE , "branches area:" + branches.getArea());
										
										serviceValue = Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice)));
										logger.log(Level.SEVERE , "serviceValue2="+serviceValue);
										perUnitPrice = origPrice;
										area = branches.getArea();
										if(branches.getUnitOfMeasurement()!=null){
											uom = branches.getUnitOfMeasurement();
										}
									  }
									}	
								}
							}
							/**
							 * end komal
							 */
								
								
//							/**
//							 * @author Anil @since 09-11-2021
//							 * Calculating per service value from contract
//							 * Raised by Nitin sir and Nithila for UDS water
//							 */
//							try{	
//								if(item.getArea()!=null&&!item.getArea().equals("")&&!item.getArea().equalsIgnoreCase("NA")){
//									double quantity=0;
//									try{
//										quantity=Double.parseDouble(item.getArea());
//									}catch(Exception e){
//										
//									}
//									
//									if(quantity!=0){
//										serviceValue=origPrice;
//										
//										logger.log(Level.SEVERE , "serviceValue3="+serviceValue);
//									}
//								}else{
////									serviceValue=origPrice/totalservices;
//									serviceValue=item.getTotalAmount()/item.getNumberOfServices();//Ashwini Patil
//									logger.log(Level.SEVERE , "serviceValue4="+serviceValue);
//								}
//							}catch(Exception e){
//								
//							}
							


							/**
							 * Date 04-07-2018 
							 * Developer :- Vijay
							 * Des :- Primises not setting to services
							 */
							String primises ="";
							if(item.getPremisesDetails()!=null && !item.getPremisesDetails().equals("") ){
								primises = item.getPremisesDetails();
							}else{
								primises = 0+"";
							}
							/**
							 * ends here
							 */
							
							/**
							 * nidhi
							 * 8-08-2018
							 * for map model and serial no
							 */

							String proSerialNo ="0", proModelNo = "0";
							
							if(item.getProModelNo() != null && !item.getProModelNo().equals("")){
								proModelNo = item.getProModelNo();
							}
						
							if(item.getProSerialNo() != null && !item.getProSerialNo().equals("")){
								proSerialNo = item.getProSerialNo();
							}
					
					
							/**
							 * nidhi
							 * 5-10-2018
							 */
							String jsonProDetails="NA";
							
							if(billofMaterialActive){
								Gson gson = new Gson();
//								if(con.getServiceScheduleList().get(k).getServiceProductList()!=null && con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0)
//									&& con.getServiceScheduleList().get(k).getServiceProductList().get(0).size()>0){
//									jsonProDetails = gson.toJson(con.getServiceScheduleList().get(k).getServiceProductList().get(0));
//								}
								/*** Date 03-04-2018 By Vijay Updated for create services properly ***/
								if(schList.get(k).getServiceProductList()!=null && schList.get(k).getServiceProductList().containsKey(0)
										&& schList.get(k).getServiceProductList().get(0).size()>0){
										jsonProDetails = gson.toJson(schList.get(k).getServiceProductList().get(0));
								}
							}
							
							/**
							 * @author Anil
							 * @since 06-06-2020
							 * setting additional details for premuim tech requirement
							 */
							String tierName="";
							String componentName="";
							String mfgNum="";
							Date mfgDate=null;
							Date replacementDate=null;
							int assetId=0;
							/**
							 * @author Anil
							 * @since 16-06-2020
							 * asset unit
							 */
							String assetUnit="";
							try{
								
								/**
								 * @author Anil
								 * @since 14-10-2020
								 * if scheduling list in contract is zero then we have to read it from 
								 * schList to update asset/component details
								 */
//								tierName=con.getServiceScheduleList().get(k).getTierName();
//								componentName=con.getServiceScheduleList().get(k).getComponentName();
//								mfgNum=con.getServiceScheduleList().get(k).getMfgNum();
//								mfgDate=con.getServiceScheduleList().get(k).getMfgDate();
//								replacementDate=con.getServiceScheduleList().get(k).getReplacementDate();
//								
//								assetId=getAssetId(item,con.getServiceScheduleList().get(k));
//								assetUnit=con.getServiceScheduleList().get(k).getAssetUnit();
								
								assetId=getAssetId(item,schList.get(k));
								assetUnit=schList.get(k).getAssetUnit();
								
								tierName=schList.get(k).getTierName();
								componentName=schList.get(k).getComponentName();
								mfgNum=schList.get(k).getMfgNum();
								mfgDate=schList.get(k).getMfgDate();
								replacementDate=schList.get(k).getReplacementDate();
								
								
								
							}catch(Exception e){
								
							}
							/**
							 * @author Vijay Date :- 07-04-2022
							 * Des :- As per Nitin sir new logic added for Service value
							 */
							serviceValue = serverapputility.getServiceValue(item,schList.get(k).getScheduleProBranch(),
									schList.get(k).getScheduleServiceNo(),con.getCompanyId(),con.isServiceWiseBilling());
							
							if(complainTatFlag){
								List<SalesLineItem> sitemLis=new ArrayList<SalesLineItem>();
								sitemLis.add(item);
								
								if(checkAssetQty(sitemLis)){
									serviceValue=item.getTotalAmount()/getTotalNoOfServices(sitemLis);
									logger.log(Level.SEVERE , "serviceValue5="+serviceValue);
								}

								
								
							}
							
							String ServiceDuration ="";
							if(serviceDurationFlag){
								ServiceDuration = schList.get(k).getServiceDuration()+"";
							}
							
							/**
							 * @author Priyanka @since 20-08-2021
							 * Des : ontract is created for customer branch of customer, then cell number, 
							 *       POC Name should be picked from customer branch master.for customer service list screen.
							 *       Requirement raised by Rahul
							 */
							String customerPoc=con.getCinfo().getPocName();
							String customerCell=con.getCinfo().getCellNumber()+"";
							if(!schList.get(k).getScheduleProBranch().equals("Service Address")){
								if(custBranchList!=null&&custBranchList.size()!=0){
									for(CustomerBranchDetails custBranch:custBranchList){
										if(custBranch.getBusinessUnitName().equals(schList.get(k).getScheduleProBranch())){
											if(custBranch.getPocName()!=null&&!custBranch.getPocName().equals("")){
												customerPoc=custBranch.getPocName();
											}
											if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
												customerCell=custBranch.getCellNumber1()+"";
											}
											break;
										}
									}
								}
							}
							
												
							String productFrequency ="";
							if(item.getProfrequency()!=null){
								productFrequency = item.getProfrequency();
							}
							
							
							String	serviceString= con.getCinfo().getCount()+"$"+con.getCinfo().getEmail()+"$"+con.getCinfo().getFullName()+"$"+customerPoc+"$"+customerCell
										+"$"+con.getCount()+"$"+servicingBranch+"$"+Service.SERVICESTATUSSCHEDULE+"$"+con.getStartDate()+"$"+con.getEndDate()+"$"+false+
										"$"+seviceindex+"$"+serviceType+"$"+schList.get(k).getScheduleServiceNo()+"$"+schList.get(k).getScheduleServiceDate()+
										"$"+addr.getAddrLine1()+"$"+addr.getAddrLine2()+"$"+addr.getCountry()+"$"+addr.getState()+"$"+addr.getCity()+"$"+addr.getLocality()+"$"+addr.getPin()+"$"+addr.getLandmark()+"$"+con.getScheduleServiceDay()+"$"+schList.get(k).getScheduleServiceTime()+
										"$"+schList.get(k).getScheduleProBranch()+"$"+schList.get(k).getScheduleServiceDay()+
										"$"+con.getCompanyId()+"$"+item.getPrduct().getComment()+"$"+item.getPrduct().getCommentdesc()+"$"+item.getPrduct().getCompanyId()+"$"+item.getPrduct().getCount()+"$"
										+item.getPrduct().getCreatedBy()+"$"+item.getPrduct().isDeleted()+"$"+item.getDuration()+"$"+item.getPrduct().getId()+"$"+item.getNumberOfServices()+"$"+item.getPrice()+"$"
										+item.getPrduct().getProductCategory()+"$"+item.getPrduct().getProductClassification()+"$"+item.getPrduct().getProductCode()+"$"
										+item.getPrduct().getProductGroup()+"$"+item.getPrduct().getProductName()+"$"+item.getPrduct().getProductType()+"$"+item.getPrduct().getRefNumber1()+"$"+item.getPrduct().getRefNumber2()+"$"
										+item.getPrduct().getServiceTax().isInclusive()+"$"+item.getPrduct().getServiceTax().getPercentage()+"$"+item.getPrduct().getServiceTax().getTaxConfigName()+"$"
										+item.getPrduct().getServiceTax().getTaxName()+"$"+item.getPrduct().getSpecifications()+"$"+item.getPrduct().isStatus()+"$"
										+item.getPrduct().getUnitOfMeasurement()+"$"+item.getPrduct().getUserId()+"$"+item.getPrduct().getVatTax().isInclusive()+"$"+item.getPrduct().getVatTax().getPercentage()+"$"
										+item.getPrduct().getVatTax().getTaxConfigName()+"$"+item.getPrduct().getVatTax().getTaxName()
										+"$"+setcount+"$"+con.getNumberRange()+"$"+false+"$"+refNo+"$"+refno2+"$"+serviceRemark
										+"$"+schList.get(k).getWeekNo()+"$"+techName+"$"+String.format("%.2f", serviceValue)+
										"$"+serviceWiseBilling+"$"+item.getProductSrNo()
										/**
										 nidhi 
										 date : 4-12-2017
										 set service type as process configration 
										 */+"$"+confiFlag +"$"+con.getCategory()/**
										 * end *//**  nidhi Date : 29-01-2018 */+"$"+con.getPocName()
										 /**added by komal 8.3.2018 **/+"$"+perUnitPrice+"$"+costCenter+"$"+area+"$"+uom
										 /** nidhi */+"$"+con.isRenewContractFlag()+"$"+primises+"$"+proModelNo+"$"+proSerialNo/**Nidhi for map serial no**/
										 +"$" + jsonProDetails /**nidhi 29-09-2018 for min list */
										 +"$"+tierName+"$"+componentName+"$"+mfgNum+"$"+mfgDate+"$"+replacementDate+"$"+assetId+"$"+assetUnit+"$"+ServiceDuration+"$"+automaticScheduling+"$"+freeMaterialFlag+"$"+con.getDescription()+"$"+productFrequency;

//								+item.getPrduct().getTermsAndConditions().getName()+"$"+item.getPrduct().getTermsAndConditions().isStatus()+"$"+item.getPrduct().getTermsAndConditions().getUrl()+"$"
							logger.log(Level.SEVERE , "serviceValue sending to taskQueue="+serviceValue);
								Queue queue = QueueFactory.getQueue("ContractServices-queue");
								queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractservicestaskqueue").param("servicekey", serviceString));
										
//					}
					
					}
			    }
			   }else{
				 
				for(int k=0;k<con.getServiceScheduleList().size();k++)
				{
					//  rohan comments this code for adding similar product in contract (pesto change)
					
					if(item.getProductSrNo()==con.getServiceScheduleList().get(k).getSerSrNo()){
						
						/** Developed by : Rohan D Bhagde.
						 * Reason : added this code for adding selected customer branch address as service address.
						 * ie. map branch address in service address if branch is selected in service schedule pop up.
						 * 
						 *   date : 26/10/2016
						 */
						
						if(con.getServiceScheduleList().get(k).getScheduleProBranch().equalsIgnoreCase("Service Address"))
						{
							if(cust!=null)
							    addr=cust.getSecondaryAdress();
						}
						else if(con.getServiceScheduleList().get(k).getScheduleProBranch().equalsIgnoreCase("Select")){
							if(cust!=null)
							    addr=cust.getSecondaryAdress();
						}
						else
						{
							   if(con.getServiceScheduleList().get(k).getScheduleProBranch()!=null)	
							   {/*
								   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",con.getServiceScheduleList().get(k).getScheduleProBranch()).
								   filter("companyId",con.getCompanyId()).first().now();
							   }
							   else
							   {
								   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",con.getServiceScheduleList().get(k).getScheduleProBranch()).
										   first().now();  
							   }
							   
							   addr = custBrach.getAddress();
							   */
								    /**
									 * 18-06-2018
									 * nidhi
									 * 
									 */
								   addr = addList.get(con.getServiceScheduleList().get(k).getScheduleProBranch());
								   /**
								    * end
								    */ 
							   }
						}
						
						/**
						 *  ends here 
						 */
						
						if(totalservices > 50){
							
							/***
							 * Date :18-10-2019 , Anil
							 */
//							int setcount = ++count;
							int setcount = count++;
							/**
							 * 
							 */
							
							int seviceindex = k+1;
							
							String refNo="";
							if(con.getRefNo()!= null && !con.getRefNo().equals(""))
							{
								refNo = con.getRefNo();
							}
							else
							{
								refNo = 0+"";
							}
							
							String refno2 ="";
							if(con.getRefNo2()!= null && !con.getRefNo2().equals(""))
							{
								refno2 = con.getRefNo2();
							}
							else
							{
								refno2 = 0+"";
							}
							
							String techName ="";
							if(con.getTechnicianName()!= null && !con.getTechnicianName().equals(""))
							{
								techName = con.getTechnicianName();
							}
							else
							{
								techName = 0+"";
							}
							
							
							String serviceRemark="";
							if(con.getServiceScheduleList().get(k).getServiceRemark()!=null && !con.getServiceScheduleList().get(k).getServiceRemark().equals("")){
								serviceRemark=con.getServiceScheduleList().get(k).getServiceRemark();
							}else{
								serviceRemark=0+"";
							}
							
							System.out.println(" Service Remark =="+serviceRemark);
							
							/**29-09-2017 sagar sore [setting service value through taskqueue also added to serviceString]**/
//							serviceValue=item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
//							serviceValue=item.getTotalAmount()/item.getNumberOfServices();//Ashwini Patil
//							logger.log(Level.SEVERE , "serviceValue6="+serviceValue);
							
							/**
							 * Date : 06-10-2017 BY ANIL
							 */
							String serviceWiseBilling="NO";
							if(con.isServiceWiseBilling()){
								serviceWiseBilling="YES";
							}
							/**
							 * End
							 */
							
							/**
							 * Date : 13-11-2017 BY ANIL
							 * setting servicing branch as branch name in service 
							 * earlier it was contract's branch
							 */
							String servicingBranch="";
							if(con.getServiceScheduleList().get(k).getServicingBranch()!=null
									&&!con.getServiceScheduleList().get(k).getServicingBranch().equals("")){
								servicingBranch=con.getServiceScheduleList().get(k).getServicingBranch();
							}else{
								servicingBranch=con.getBranch();
							}
							/**
							 * End
							 */
							/** date 17.02.2018 added by komal for area wise billing**/
							double perUnitPrice = 0;
							String costCenter = "na";
							double area = 0;
							String uom = "na";
							
							/**
							 * nidhi
							 * 22-05-2018
							 * for nbhc
							 */
							boolean priceStatus = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CustomerBranchAreaWiseBilling", con.getCompanyId());
							double tax = 0;
							double origPrice = 0;
							double serviceValue = 0;
							tax = removeAllTaxes(item.getPrduct());
//							origPrice = item.getPrice() - tax;
							origPrice = item.getPrice();//Ashwini Patil
							
							/*
							 * Ashwini Patil 
							 * Date:6-03-2024 
							 * Area from branch popup on contract product line is not getting mapped to quantity field in service and not getting printed on orion's service docket
							 */
							Set<Integer> ser =  item.getCustomerBranchSchedulingInfo().keySet();
							
							for(Integer seq : ser){
								ArrayList<BranchWiseScheduling> branch =  item.getCustomerBranchSchedulingInfo().get(seq);
								if(branch!=null) {

									for(BranchWiseScheduling binfo:branch){
										if(binfo.getBranchName().equalsIgnoreCase(con.getServiceScheduleList().get(k).getScheduleProBranch())
												&& binfo.isCheck())
										{
											if(binfo.getArea()>0)
												area=binfo.getArea();
											if(costCenterList!=null&&costCenterList.containsKey(binfo.getBranchName()))
												costCenter=costCenterList.get(binfo.getBranchName());
										}
									}
									
								}
							}
							
							String serviceType="Periodic";
							if(con.getServicetype()!=null&&!con.getServicetype().equals("")){
								serviceType=con.getServicetype(); //Ashwini Patil Date:29-02-2024 for orion
							}
							
							if(con.isServiceWiseBilling() && priceStatus){
								
								if(!(item.getArea().equalsIgnoreCase("NA"))){
								logger.log(Level.SEVERE , "inside area");
								CustomerBranchDetails branches = ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId())	.filter("status", true).filter("cinfo.count", con.getCinfo().getCount()).filter("buisnessUnitName", con.getServiceScheduleList().get(k).getScheduleProBranch()).first().now();
								logger.log(Level.SEVERE , "branches :" + branches);
								if(branches != null){
									if(branches.getCostCenter() !=null){
										costCenter = branches.getCostCenter();
									}
								if(branches.getArea()!=0){
									logger.log(Level.SEVERE , "branches area:" + branches.getArea());
									
									serviceValue = Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice)));
									logger.log(Level.SEVERE , "serviceValue7="+serviceValue);
									perUnitPrice = origPrice;
									area = branches.getArea();
									if(branches.getUnitOfMeasurement()!=null){
										uom = branches.getUnitOfMeasurement();
									}
								  }
								}	
							}
						}
						/**
						 * end komal
						 */
							
							
//								/**
//						 * @author Anil @since 09-11-2021
//						 * Calculating per service value from contract
//						 * Raised by Nitin sir and Nithila for UDS water
//						 */
//						try{	
//							if(item.getArea()!=null&&!item.getArea().equals("")&&!item.getArea().equalsIgnoreCase("NA")){
//								double quantity=0;
//								try{
//									quantity=Double.parseDouble(item.getArea());
//								}catch(Exception e){
//									
//								}
//								
//								if(quantity!=0){
//									serviceValue=origPrice;
//									logger.log(Level.SEVERE , "serviceValue8="+serviceValue);
//								}
//							}
//							else{
////								serviceValue=origPrice/totalservices;
//								serviceValue=item.getTotalAmount()/item.getNumberOfServices();//Ashwini Patil
//								logger.log(Level.SEVERE , "serviceValue9="+serviceValue);
//							}
//						}catch(Exception e){
//							
//						}
//							
						/**
						 * Date 04-07-2018 
						 * Developer :- Vijay
						 * Des :- Primises not setting to services
						 */
						String primises ="";
						if(item.getPremisesDetails()!=null && !item.getPremisesDetails().equals("") ){
							primises = item.getPremisesDetails();
						}else{
							primises = 0+"";
						}
						/**
						 * ends here
						 */
						/**
						 * nidhi
						 * 8-08-2018
						 * for map model and serial no
						 */

						String proSerialNo ="0", proModelNo = "0";
						
						if(item.getProModelNo() != null && !item.getProModelNo().equals("")){
							proModelNo = item.getProModelNo();
						}
					
						if(item.getProSerialNo() != null && !item.getProSerialNo().equals("")){
							proSerialNo = item.getProSerialNo();
						}
						String jsonProDetails="NA";
						
						if(billofMaterialActive){
							Gson gson = new Gson();
							if(con.getServiceScheduleList().get(k).getServiceProductList()!=null && con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0)
								&& con.getServiceScheduleList().get(k).getServiceProductList().get(0).size()>0){
								jsonProDetails = gson.toJson(con.getServiceScheduleList().get(k).getServiceProductList().get(0));
							}
							
						}
						
						/**
						 * @author Anil
						 * @since 06-06-2020
						 * setting additional details for premuim tech requirement
						 */
						String tierName="";
						String componentName="";
						String mfgNum="";
						Date mfgDate=null;
						Date replacementDate=null;
						int assetId=0;
						/**
						 * @author Anil
						 * @since 16-06-2020
						 * asset unit
						 */
						String assetUnit="";
						try{
							assetId=getAssetId(item,con.getServiceScheduleList().get(k));
							assetUnit=con.getServiceScheduleList().get(k).getAssetUnit();
							logger.log(Level.SEVERE , "assetId: " + assetId);
							tierName=con.getServiceScheduleList().get(k).getTierName();
							componentName=con.getServiceScheduleList().get(k).getComponentName();
							mfgNum=con.getServiceScheduleList().get(k).getMfgNum();
							mfgDate=con.getServiceScheduleList().get(k).getMfgDate();
							replacementDate=con.getServiceScheduleList().get(k).getReplacementDate();
							
						}catch(Exception e){
							
						}
						
						/**
						 * @author Vijay Date :- 07-04-2022
						 * Des :- As per Nitin sir new logic added for Service value
						 */
						serviceValue = serverapputility.getServiceValue(item,con.getServiceScheduleList().get(k).getScheduleProBranch(),
																con.getServiceScheduleList().get(k).getScheduleServiceNo(),con.getCompanyId(),con.isServiceWiseBilling());
						logger.log(Level.SEVERE , "serviceValue = "+serviceValue);
						/**
						 * ends here
						 */
						
						if(complainTatFlag){
							List<SalesLineItem> sitemLis=new ArrayList<SalesLineItem>();
							sitemLis.add(item);
							if(checkAssetQty(sitemLis)){
								serviceValue=item.getTotalAmount()/getTotalNoOfServices(sitemLis);
								logger.log(Level.SEVERE , "serviceValue5="+serviceValue);
							}
						}
						
						String ServiceDuration ="";
						if(serviceDurationFlag){
							ServiceDuration = con.getServiceScheduleList().get(k).getServiceDuration()+"";
						}
						
						/**
						 * @author Priyanka @since 20-08-2021
						 * Des : ontract is created for customer branch of customer, then cell number, 
						 *       POC Name should be picked from customer branch master.for customer service list screen.
						 *       Requirement raised by Rahul
						 */
												
						String customerPoc=con.getCinfo().getPocName();
						String customerCell=con.getCinfo().getCellNumber()+"";
						if(!con.getServiceScheduleList().get(k).getScheduleProBranch().equals("Service Address")){
							if(custBranchList!=null&&custBranchList.size()!=0){
								for(CustomerBranchDetails custBranch:custBranchList){
									if(custBranch.getBusinessUnitName().equals(con.getServiceScheduleList().get(k).getScheduleProBranch())){
										if(custBranch.getPocName()!=null&&!custBranch.getPocName().equals("")){
											
											customerPoc=custBranch.getPocName();
										}
										if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
											
											customerCell=custBranch.getCellNumber1()+"";
										}
										break;
									}
								}
							}
						}
						

						String productFrequency ="";
						if(item.getProfrequency()!=null){
							productFrequency = item.getProfrequency();
						}
							String serviceString="";
							serviceString= con.getCinfo().getCount()+"$"+con.getCinfo().getEmail()+"$"+con.getCinfo().getFullName()+"$"+customerPoc+"$"+customerCell
									+"$"+con.getCount()+"$"+servicingBranch+"$"+Service.SERVICESTATUSSCHEDULE+"$"+con.getStartDate()+"$"+con.getEndDate()+"$"+false+
									"$"+seviceindex+"$"+serviceType+"$"+con.getServiceScheduleList().get(k).getScheduleServiceNo()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceDate()+
									"$"+addr.getAddrLine1()+"$"+addr.getAddrLine2()+"$"+addr.getCountry()+"$"+addr.getState()+"$"+addr.getCity()+"$"+addr.getLocality()+"$"+addr.getPin()+"$"+addr.getLandmark()+"$"+con.getScheduleServiceDay()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceTime()+
									"$"+con.getServiceScheduleList().get(k).getScheduleProBranch()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceDay()+
									"$"+con.getCompanyId()+"$"+item.getPrduct().getComment()+"$"+item.getPrduct().getCommentdesc()+"$"+item.getPrduct().getCompanyId()+"$"+item.getPrduct().getCount()+"$"
									+item.getPrduct().getCreatedBy()+"$"+item.getPrduct().isDeleted()+"$"+item.getDuration()+"$"+item.getPrduct().getId()+"$"+item.getNumberOfServices()+"$"+item.getPrice()+"$"
									+item.getPrduct().getProductCategory()+"$"+item.getPrduct().getProductClassification()+"$"+item.getPrduct().getProductCode()+"$"
									+item.getPrduct().getProductGroup()+"$"+item.getPrduct().getProductName()+"$"+item.getPrduct().getProductType()+"$"+item.getPrduct().getRefNumber1()+"$"+item.getPrduct().getRefNumber2()+"$"
									+item.getPrduct().getServiceTax().isInclusive()+"$"+item.getPrduct().getServiceTax().getPercentage()+"$"+item.getPrduct().getServiceTax().getTaxConfigName()+"$"
									+item.getPrduct().getServiceTax().getTaxName()+"$"+item.getPrduct().getSpecifications()+"$"+item.getPrduct().isStatus()+"$"
									+item.getPrduct().getUnitOfMeasurement()+"$"+item.getPrduct().getUserId()+"$"+item.getPrduct().getVatTax().isInclusive()+"$"+item.getPrduct().getVatTax().getPercentage()+"$"
									+item.getPrduct().getVatTax().getTaxConfigName()+"$"+item.getPrduct().getVatTax().getTaxName()
									+"$"+setcount+"$"+con.getNumberRange()+"$"+false+"$"+refNo+"$"+refno2+"$"+serviceRemark
									+"$"+con.getServiceScheduleList().get(k).getWeekNo()+"$"+techName+"$"+String.format("%.2f", serviceValue)
									+"$"+serviceWiseBilling+"$"+item.getProductSrNo()
									/**
									 nidhi 
									 date : 4-12-2017
									 set service type as process configration 
									 */+"$"+confiFlag +"$"+con.getCategory()/**
									 * end *//**  nidhi Date : 29-01-2018 */+"$"+con.getPocName()
									 /**komal date: 8.3.2018 **/+"$"+perUnitPrice+"$"+costCenter+"$"+area+"$"+uom
									 /** nidhi */+"$"+con.isRenewContractFlag()+"$"+primises+"$"+proModelNo+"$"+proSerialNo/**Nidhi for map serial no**/
									 +"$" + jsonProDetails /**nidhi 29-09-2018 for min list */
									 +"$"+tierName+"$"+componentName+"$"+mfgNum+"$"+mfgDate+"$"+replacementDate+"$"+assetId+"$"+assetUnit+"$"+ServiceDuration+"$"+automaticScheduling+"$"+freeMaterialFlag+"$"+con.getDescription()+"$"+productFrequency;

//							+item.getPrduct().getTermsAndConditions().getName()+"$"+item.getPrduct().getTermsAndConditions().isStatus()+"$"+item.getPrduct().getTermsAndConditions().getUrl()+"$"
							
							logger.log(Level.SEVERE , "serviceValue11 for taskQueue="+serviceValue);
							Queue queue = QueueFactory.getQueue("ContractServices-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractservicestaskqueue").param("servicekey", serviceString));
									
				}
				else
				{
					Service temp=new Service();
					
					
					String customerPoc=con.getCinfo().getPocName();
					Long customerCell=con.getCinfo().getCellNumber();
					//System.out.println("Customer Branch contract to service else part Task Queue");
					if(!con.getServiceScheduleList().get(k).getScheduleProBranch().equals("Service Address")){
						if(custBranchList!=null&&custBranchList.size()!=0){
							//System.out.println("Customer Branch Size === "+custBranchList.size());
							for(CustomerBranchDetails custBranch:custBranchList){
								if(custBranch.getBusinessUnitName().equals(con.getServiceScheduleList().get(k).getScheduleProBranch())){
									if(custBranch.getPocName()!=null&&!custBranch.getPocName().equals("")){
										//System.out.println("Customer Branch Poc Name === "+custBranch.getPocName());
										customerPoc=custBranch.getPocName();
									}
									if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
										//System.out.println("Customer Branch Cell No === "+custBranch.getCellNumber1());
										customerCell=custBranch.getCellNumber1();
									}
									break;
								}
							}
						}
						temp.getPersonInfo().setCount(con.getCinfo().getCount());
						temp.getPersonInfo().setFullName(con.getCinfo().getFullName());
						temp.getPersonInfo().setPocName(customerPoc);
						temp.getPersonInfo().setCellNumber(customerCell);
						temp.getPersonInfo().setEmail(con.getCinfo().getEmail());
						
					}else{
					temp.setPersonInfo(con.getCinfo());
					}
					
					//temp.setPersonInfo(con.getCinfo());
					temp.setContractCount(con.getCount());
					temp.setProduct((ServiceProduct) item.getPrduct());
					
					/**
					 * Date : 13-11-2017 BY ANIL
					 * setting servicing branch as branch name in service 
					 * earlier it was contract's branch
					 */
					String servicingBranch="";
					if(con.getServiceScheduleList().get(k).getServicingBranch()!=null
							&&!con.getServiceScheduleList().get(k).getServicingBranch().equals("")){
						servicingBranch=con.getServiceScheduleList().get(k).getServicingBranch();
					}else{
						servicingBranch=con.getBranch();
					}
					/**
					 * End
					 */
					
					temp.setBranch(servicingBranch);
					
					
					temp.setStatus(Service.SERVICESTATUSSCHEDULE);
					/**
					 * Date 13 Feb 2017
					 * by Vijay
					 * for every service service engineer should be blank by default and select service engineer in service
					 */
//					temp.setEmployee(con.getEmployee());
					/**
					 * ends here 
					 */
					temp.setContractStartDate(con.getStartDate());
					temp.setContractEndDate(con.getEndDate());
					temp.setAdHocService(false);
					temp.setServiceIndexNo(k+1);
					
					/**
					 * @author Vijay Date - 27-07-2023 
					 * Des :- added for orion when they select service type on complain screen the same it should map in complaint service
					 */
					if(con.getComplaintFlag()!=null&&con.getTicketNumber()!=-1 && con.getComplaintFlag() && complainEntity!=null
							&& complainEntity.getServiceType()!=null && !complainEntity.getServiceType().equals("")){
						temp.setServiceType(complainEntity.getServiceType());
					}else if(con.getServicetype()!=null&&!con.getServicetype().equals("")){
						temp.setServiceType(con.getServicetype()); //Ashwini Patil Date:29-02-2024 for orion
					}
					else{
						temp.setServiceType("Periodic");
					}
					
					
					
					/*
					 * Ashwini Patil 
					 * Date:6-03-2024 
					 * Area from branch popup on contract product line is not getting mapped to quantity field in service and not getting printed on orion's service docket
					 */
					Set<Integer> ser =  item.getCustomerBranchSchedulingInfo().keySet();
					
					for(Integer seq : ser){
						ArrayList<BranchWiseScheduling> branch =  item.getCustomerBranchSchedulingInfo().get(seq);
						if(branch!=null) {
						for(BranchWiseScheduling binfo:branch){
							if(binfo.getBranchName().equalsIgnoreCase(con.getServiceScheduleList().get(k).getScheduleProBranch())
									&& binfo.isCheck())
							{
								if(binfo.getArea()>0)
									temp.setQuantity(binfo.getArea());
								if(costCenterList!=null&&costCenterList.containsKey(binfo.getBranchName()))
									temp.setCostCenter(costCenterList.get(binfo.getBranchName()));
							}
						}
						}
					}
					
					
					/**
					 * Date:24-01-2017 By Anil
					 * setting ref no.2 
					 */
					temp.setRefNo2(con.getRefNo2());
					/**
					 * End
					 */
					
					//   rohan added ref no in service for bpt  
					temp.setRefNo(con.getRefNo());					
					//    changes ends here 
//					ProductInfo prodInfo=new ProductInfo();
//					prodInfo.setProductCode(item.getProductCode());
//					prodInfo.setProductName(item.getProductName());
//					prodInfo.setProdID(item.getPrduct().getCount());
//					
//					if(prodInfo!=null){
//						temp.setProductInfoDetails(prodInfo);
//					}
					temp.setServiceSerialNo(con.getServiceScheduleList().get(k).getScheduleServiceNo());
					temp.setServiceDate(con.getServiceScheduleList().get(k).getScheduleServiceDate());
			    	temp.setAddress(addr);
//			    	dfghjkl
//			    	if(con.getScheduleServiceDay()!=null){
//						temp.setServiceDay(con.getScheduleServiceDay());
//					}
			    	/**above old code commented by Rohan
			    	 * Changed by Rohan for setting service day in services
			    	 */
			    	if(con.getServiceScheduleList().get(k).getScheduleServiceDay()!=null){
						temp.setServiceDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
					}
			    	/**
			    	 * Ends here
			    	 */
			    	
			    	
					temp.setServiceTime(con.getServiceScheduleList().get(k).getScheduleServiceTime());
					temp.setServiceBranch(con.getServiceScheduleList().get(k).getScheduleProBranch());
					temp.setServiceDateDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
			    	
					temp.setBranch(con.getServiceScheduleList().get(k).getServicingBranch());
					temp.setServicingTime(con.getServiceScheduleList().get(k).getTotalServicingTime());
				
					
					/**
					 * Added this for NBHC Only (By Rahul)
					 * 
					 */
					if(con.getStackDetailsList()!=null)
					temp.setStackDetailsList(con.getStackDetailsList());
					
					
					if(con.getNumberRange()!=null)
						temp.setNumberRange(con.getNumberRange());
					
					/**
					 * Date 07 Feb 2017
					 * vijay added this for in contract product level remark will set in service entity
					 */
					temp.setRemark(con.getServiceScheduleList().get(k).getServiceRemark());
					/*
					 * End here
					 */
					
					/**
					 * Date 02-04-2017 added by vijay for service week no of month
					 */
					temp.setServiceWeekNo(con.getServiceScheduleList().get(k).getWeekNo());
					/**
					 * ends here
					 */
					
					/**
					 * Date : 12-02-2017 added by rohan added this code for adding technician in services
					 */
					
					temp.setEmployee(con.getTechnicianName());
					/**
					 * ends here 
					 */
					
					
					
					/** 29-07-2017 sagar sore setting service value**/
//					temp.setServiceValue(Double.parseDouble(String.format("%.2f",(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())))));
//					logger.log(Level.SEVERE , "temp.setServiceValue="+Double.parseDouble(String.format("%.2f",(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())))));
					/**
					 * Date : 06-10-2017 BY ANIL
					 * setting service wise billing flag
					 */
					temp.setServiceWiseBilling(con.isServiceWiseBilling());
					temp.setServiceSrNo(item.getProductSrNo());
					/**
					 * End
					 */
					

					/**
					 *  nidhi
					 *  4-12-2017
					 *  process config
					 *    "Service" - "ContractCategoryAsServiceType"
					 */
					if(confiFlag){
						temp.setServiceType(con.getCategory());
						
					}
					/**
					 * end
					 */
					/**
					 *  nidhi
					 *  29-01-2018
					 *  for copy pocname in person info
					 */
					if(con.getPocName()!=null && !con.getPocName().trim().equals("")){
						temp.getPersonInfo().setPocName(con.getPocName());
					}
					/**
					 *  end
					 */
					/** date 17.02.2018 added by komal for area wise billing**/
					/**
					 * nidhi
					 * 22-05-2018
					 * for nbhc
					 */
					boolean priceStatus = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CustomerBranchAreaWiseBilling", con.getCompanyId());
					double tax = removeAllTaxes(item.getPrduct());
//					double origPrice = item.getPrice() - tax;
					double origPrice = item.getPrice();//Ashwini Patil

					
					if(con.isServiceWiseBilling() && priceStatus){
						if(!(item.getArea().equalsIgnoreCase("NA"))){
						logger.log(Level.SEVERE , "inside area");
						CustomerBranchDetails branches = ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId())	.filter("status", true).filter("cinfo.count", con.getCinfo().getCount()).filter("buisnessUnitName", con.getServiceScheduleList().get(k).getScheduleProBranch()).first().now();
						logger.log(Level.SEVERE , "branches :" + branches);
						if(branches != null){
							if(branches.getCostCenter() !=null){
								 temp.setCostCenter(branches.getCostCenter());
							}
						if(branches.getArea()!=0){
							logger.log(Level.SEVERE , "branches area:" + branches.getArea());
							
							
							temp.setServiceValue(Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice))));
							logger.log(Level.SEVERE ,"temp.setServiceValue2="+Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice))));
							temp.setQuantity(branches.getArea());
							if(branches.getUnitOfMeasurement()!=null){
								temp.setUom(branches.getUnitOfMeasurement());
							}
							temp.setUom(branches.getUnitOfMeasurement());
							temp.setPerUnitPrice(origPrice);
							}
						}	
					}
				}
				/**
				 * end komal
				 */
					
//				/**
//				 * @author Anil @since 09-11-2021
//				 * Calculating per service value from contract
//				 * Raised by Nitin sir and Nithila for UDS water
//				 */
//				try{	
//					if(item.getArea()!=null&&!item.getArea().equals("")&&!item.getArea().equalsIgnoreCase("NA")){
//						double quantity=0;
//						try{
//							quantity=Double.parseDouble(item.getArea());
//						}catch(Exception e){
//							
//						}
//						
//						if(quantity!=0){
//							temp.setServiceValue(origPrice);
//							logger.log(Level.SEVERE ,"temp.setServiceValue(origPrice)="+origPrice);
//							
//						}
//					}else{
////						temp.setServiceValue(origPrice/totalservices);
//						temp.setServiceValue(item.getTotalAmount()/item.getNumberOfServices());//Ashwini Patil
////						logger.log(Level.SEVERE ,"temp.setServiceValue(origPrice/totalservices)="+origPrice/totalservices);
//						logger.log(Level.SEVERE ,"temp.setServiceValue(item.getTotalAmount()/item.getNumberOfServices()=" + item.getTotalAmount()/item.getNumberOfServices());
//						
//					}
//				}catch(Exception e){
//					
//				}
				
					/**
					 * nidhi
					 */
					temp.setRenewContractFlag(con.isRenewContractFlag());
					
					/**
					 * Date 04-07-2018 
					 * Developer :- Vijay
					 * Des :- Primises not setting to services
					 */
					if(item.getPremisesDetails()!=null && !item.getPremisesDetails().equals("") ){
						temp.setPremises(item.getPremisesDetails());
					}
					/**
					 * ends here
					 */
					/**
					 * nidhi
					 * 8-08-2018
					 */
					if(item.getProModelNo() != null){
						temp.setProModelNo(item.getProModelNo());
					}
					if(item.getProSerialNo()!=null){
						temp.setProSerialNo(item.getProSerialNo());
					}
					/**
					 * end
					 */
					/**
					 * nidhi
					 *  *:*:*
					 *  for bill of material calculation 
					 */
					if(billofMaterialActive){
						if(con.getServiceScheduleList().get(k).getServiceProductList()!=null &&
								con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0) &&
								con.getServiceScheduleList().get(k).getServiceProductList().get(0).size()>0){
							temp.setUom(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getUnit());
							temp.setQuantity(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getQuantity());
							ArrayList<ServiceProductGroupList> serProList  = new ArrayList<ServiceProductGroupList>();
							for(int jj=0 ; jj<con.getServiceScheduleList().get(k).getServiceProductList().get(0).size();jj++){
								serProList.add(temp.getServiceProdDt(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(jj)));
							}
							
							temp.setServiceProductList(serProList);
						}
					}
					
					
					/**
					 * Date 01-08-2019 by Vijay for NBHC CCPM
					 * Fumigation Services for every Stack
					 * This code only executed from Web Services of NBHC Fumigation service
					 */
					 temp.setWareHouse(con.getServiceScheduleList().get(k).getWarehouseName());
					 temp.setShade(con.getServiceScheduleList().get(k).getShade());
					 temp.setCompartment(con.getServiceScheduleList().get(k).getCompartment());
					 temp.setStackNo(con.getServiceScheduleList().get(k).getStackNo());
					 temp.setStackQty(con.getServiceScheduleList().get(k).getStackQty());
					 if(con.getStartDate()!=null)
					 temp.setDepositeDate(con.getStartDate());
					 /**
					  * ends here
					  */
					 /**
					  * Date 21-09-2019 by Vijay
					  * Des :- NBHC WMS fumigation services services address not mapping from customer
					  * we are mapping from contract service address. when inward then storing in contract service address
					  */
					 if(warehouseaddressflag && con.getCinfo().getCount()==100003857){
						 temp.setAddress(con.getCustomerServiceAddress()); 
					 }
					 /**
					  * ends here
					  */
					 
					 
					 /**
					  * @author Anil
					  * @since 06-06-2020
					  * Premium Tech : updating asset and component details
					  */
					 try{
						 temp.setTierName(con.getServiceScheduleList().get(k).getTierName());
						 temp.setComponentName(con.getServiceScheduleList().get(k).getComponentName());
						 temp.setMfgNo(con.getServiceScheduleList().get(k).getMfgNum());
						 temp.setMfgDate(con.getServiceScheduleList().get(k).getMfgDate());
						 temp.setReplacementDate(con.getServiceScheduleList().get(k).getReplacementDate());
						 int assetId=getAssetId(item,con.getServiceScheduleList().get(k));
						 temp.setAssetId(assetId);
						 temp.setAssetUnit(con.getServiceScheduleList().get(k).getAssetUnit());
					 }catch(Exception e){
						 
					 }
					 
					/**
					 * @author Vijay Date :- 07-04-2022
					 * Des :- As per Nitin sir new logic added for Service value
					 */
					double newserviceValue = serverapputility.getServiceValue(item,con.getServiceScheduleList().get(k).getScheduleProBranch(),
															con.getServiceScheduleList().get(k).getScheduleServiceNo(),con.getCompanyId(),con.isServiceWiseBilling());
					temp.setServiceValue(newserviceValue);
					/**
					 * ends here
					 */
						
					 if(complainTatFlag){
						 	double serviceValue=0;
							List<SalesLineItem> sitemLis=new ArrayList<SalesLineItem>();
							sitemLis.add(item);
							
							if(checkAssetQty(sitemLis)){
								serviceValue=item.getTotalAmount()/getTotalNoOfServices(sitemLis);
								logger.log(Level.SEVERE , "serviceValue5="+serviceValue);
								temp.setServiceValue(serviceValue);
							}
					}
					 
					if(serviceDurationFlag){
						System.out.println("product name =="+con.getServiceScheduleList().get(k).getScheduleProdName());
						System.out.println("Service Duration ="+con.getServiceScheduleList().get(k).getServiceDuration());
						if(con.getServiceScheduleList().get(k).getServiceDuration()!=0){
							temp.setServiceDuration(con.getServiceScheduleList().get(k).getServiceDuration());
						}
					}
						
					
					/**
					 * @author Vijay Chougule Date 24-09-2020
					 * Des :- For Free Material Services will Schedule by default for Technician App for Pest Terminaters
					 */
					if(freeMaterialFlag){
						temp.setServiceScheduled(true);
					}
					
					
//					temp.setServiceValue(Double.parseDouble(String.format("%.2f",(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())))));
//					temp.setServiceValue(item.getTotalAmount()/item.getNumberOfServices());
				
					if(item.getProfrequency()!=null){
						temp.setProductFrequency(item.getProfrequency());
					}

					services.add(temp);
					
					temp.setCompanyId(con.getCompanyId());
					
					/**
					 * @author Anil
					 * @since 12-01-2021
					 * Setting contract description to service description
					 */
					temp.setDescription(con.getDescription());
					
					/**
					 * @author Anil
					 * @since 23-06-2020
					 * setting asset id,asset unit and mfg num in description
					 */
					String descHead="";
					String descVal="";
					if(temp.getAssetId()!=0){
						descHead="Asset Id";
						descVal=temp.getAssetId()+"";
					}
					if(temp.getAssetUnit()!=null&&!temp.getAssetUnit().equals("")){
						if(!descHead.equals("")){
							descHead=descHead+"/"+"Asset Unit";
							descVal=descVal+"/"+temp.getAssetUnit();
						}else{
							descHead="Asset Unit";
							descVal=temp.getAssetUnit();
						}
					}
					if(temp.getMfgNo()!=null&&!temp.getMfgNo().equals("")){
						if(!descHead.equals("")){
							descHead=descHead+"/"+"Mfg No.";
							descVal=descVal+"/"+temp.getMfgNo();
						}else{
							descHead="Mfg No.";
							descVal=temp.getMfgNo();
						}
					}
					String description="";
					if(!descHead.equals("")){
						description=descHead+" : "+descVal+" ";
					}
					
					if(!description.equals("")){
						description=description+" "+temp.getDescription();
					}else{
						description=temp.getDescription();
					}
					temp.setDescription(description);
					
					
					
					}
				}
		    }
		}	
				
				
			/**
			 *  nidhi
			 *  4-12-2017
			 *  process config
			 *    "Service" - "ContractCategoryAsServiceType"
			 */
			if(confiFlag){
				boolean status = ServerAppUtility.checkServiceTypeAvailableOrNot(con.getCategory(), con.getCompanyId(), con.getApproverName());
			}
			/**
			 *  end
			 */
//			GenricServiceImpl impl=new GenricServiceImpl();
//				if(services!=null&&services.size()!=0){
//					logger.log(Level.SEVERE,"Saving Services");
//				    impl.save(services);
//				    
//				    System.out.println("Saving Services");
//				}
//				logger.log(Level.SEVERE,"Services Creation Complete");
		}
//			if(totalservices>50){
//				numbergen.setNumber(count);
//		    	 GenricServiceImpl impl = new GenricServiceImpl();
//				  impl.save(numbergen);
//			}else{
				
			
			
			
				/**
				 * Date : 02-04-2018 By ANIL
				 * This save method is called at every loop of contracts product
				 * commented this and called same code after loop
				 */
			
//				GenricServiceImpl impl=new GenricServiceImpl();
//				if(services!=null&&services.size()!=0){
//				    impl.save(services);
//				}
				
				/**
				 * End
				 */
				
//			}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		/**
		 * Date  : 21-03-2018 BY ANIL
		 * NEW CODE FOR SAVING SERVICE AT ONCE
		 */
		GenricServiceImpl impl=new GenricServiceImpl();
		
		if(services!=null&&services.size()!=0){
		    impl.save(services);
		    
		    /**
		     * @author Vijay Chougule
		     * Des :- if Automatich Scheduling process Config is active then Service Project will create with Automatic
		     * Service Scheduling for Technician App.Material Mapping From BOM to project
		     */
		    if(automaticScheduling){
		    	for(SuperModel model : services){
			    	String companyId = model.getCompanyId()+"";
			    	String serviceId = model.getCount()+"";
			    	Service serviceEntity = (Service) model;
			    	if(serviceEntity.getServiceProductList()!=null && serviceEntity.getServiceProductList().size()!=0){
			    		Queue queue = QueueFactory.getQueue("ProjectCreationTaskQueue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/projectcreationtaskqueue").param("serviceId", serviceId)
															.param("companyId", companyId));
			    	}
			    	
						
			    }
		    }
		    /**
		     * ends here
		     */
		    
		}
		
		/**
		 * END
		 */
		/**
		 * nidhi
		 *  for map invoice on contract apporval
		 */
		boolean serviceMapping = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ServiceInvoiceMappingOnContractApproval", con.getCompanyId());
		logger.log(Level.SEVERE,"scontract -- " + serviceMapping +"  flag -- ");
		if(!con.isContractRate() && !con.isServiceWiseBilling() &&  serviceMapping){
			final Integer contractCount = con.getCount();
			Queue queue = QueueFactory.getQueue("ContractServicesInvoiceMapping-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractServiceInvoiceMapping")
					.param("contract", contractCount+"").param("companyId", con.getCompanyId()+"")
					.param("totalservices", totalservices+""));
		}
		/**
		 *  After creation of services Customer Project is auto created.
		 *  This method is called for creating customer project.
		 */
//			System.out.println("SizeFor Projects ==="+services.size());
//				createProject(con);
		
		
//		for(SalesLineItem item:con.getItems())
//		{
//			if(item.getPrduct() instanceof ServiceProduct)
//		    {
//				long noServices=(long) (item.getNumberOfServices()*item.getQty());
//				int noOfdays=item.getDuration();
//				System.out.println("Interval"+noOfdays/item.getNumberOfServices());
//				int interval= (int) (noOfdays/item.getNumberOfServices());
//				Date servicedate=item.getStartDate();
//				System.out.println("Start Date"+servicedate);
//				Date d=new Date(servicedate.getTime());
//				d=DateUtility.getDateWithTimeZone("IST", (Date) d);
//			    System.out.println("Interval is "+interval);
//			    System.out.println("Number of Services "+noServices);
//			  
//			   if(con.getCinfo()!=null)
//			   {
//				   if(con.getCompanyId()!=null)	
//				   {
//					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
//					   filter("companyId",con.getCompanyId()).first().now();
//				   }
//				   else
//				   {
//					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
//							   first().now();  
//				   }
//			   }
//					//A patch as Neither Customer nor Contract has Customer Address
//					if(cust!=null)
//					    addr=cust.getSecondaryAdress();
//					 
//					
//				
//				for(int k=0;k<noServices;k++)
//				{
//					Service temp=new Service();
//					
//					temp.setPersonInfo(con.getCinfo());
//					temp.setContractCount(con.getCount());
//					temp.setProduct((ServiceProduct) item.getPrduct());
//					temp.setBranch(con.getBranch());
//					temp.setStatus(Service.SERVICESTATUSSCHEDULE);
//					temp.setEmployee(con.getEmployee());
//					temp.setContractStartDate(con.getStartDate());
//					temp.setContractEndDate(con.getEndDate());
//					temp.setServiceSerialNo(k+1);
//					if(k==0){
//					temp.setServiceDate(d);
//					}
//					System.out.println("pppp"+noServices/item.getQty());
//					if(item.getQty()>1&&k%(noServices/item.getQty())==0){
//						System.out.println("K Value"+k);
//						d=null;
//						servicedate=item.getStartDate();
//						d=new Date(servicedate.getTime());
//					}
//					
//					if(k!=0){
//						temp.setServiceDate(d);
//					}
//					
//					Calendar c = Calendar.getInstance(); 
//			    	c.setTime(servicedate); 
//			    	c.add(Calendar.DATE, interval);
//			    	servicedate=c.getTime();
//			    	Date tempdate=new Date(servicedate.getTime());
//			    	d=DateUtility.getDateWithTimeZone("IST",tempdate);
//			    	
//			    	temp.setAddress(addr);
//					services.add(temp);
//					
//					temp.setCompanyId(con.getCompanyId());
//					
//					System.out.println("Created Service With Date "+temp.getServiceDate());
//				}
//				}
//				
//				GenricServiceImpl impl=new GenricServiceImpl();
//				if(services!=null&&services.size()!=0){
//				    impl.save(services);
//				}
//			}
//		
//		
//		/**
//		 *  After creation of services Customer Project is auto created.
//		 *  This method is called for creating customer project.
//		 */
//		
//				createProject(con);
	   }

	
	
	



	double totalservices = 0;
	private double gettotalnumberofservices(List<SalesLineItem> getproductitem) {
		
		for(int i=0;i<getproductitem.size();i++){
			
			totalservices = totalservices + (getproductitem.get(i).getQty()*getproductitem.get(i).getNumberOfServices());
		}
		
		return totalservices;
		
	}
	
	
	
	@OnSave
	@GwtIncompatible
	protected void changeCustomerStatus(Contract con) {
		
		if(con.getCinfo()!=null)
    	{
			List<Customer> customerList=null;
			if(con.getCompanyId()!=null)
			{
				customerList =	ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
				filter("companyId",con.getCompanyId())
				.list();
    	
			}
		else
			customerList =	ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).list();
	    
		if(customerList!=null&&customerList.size()!=0)
    	 {
    	
			if(con.getStatus().equals(Contract.APPROVED))
			{
				for(Customer cust:customerList)
				{
					cust.setStatus(Customer.CUSTOMERACTIVE);
				}
			}
			
			ofy().save().entities(customerList).now();
    	 }
		}
	}
	
    
	@OnSave
	@GwtIncompatible
	protected void changeQuotationStatus(Contract con)
	{
    	int quotationId=con.getQuotationCount();
    	List<Quotation> quotation=null;
    	if(quotationId==-1)
    		return;
    	if(con.getCompanyId()!=null)
    	{
    	        quotation =	ofy().load().type(Quotation.class).filter("count",quotationId).
    			filter("companyId",con.getCompanyId()).list();
    	}
    	
    	else
    		quotation =	ofy().load().type(Quotation.class).filter("count",quotationId).list();
    	    	
    	if(quotation==null)
    	  return;
    	if(quotation.size()==0)
    		return;
    	
    	if(con.getStatus().equals(Contract.APPROVED))
    	{
    	  for(Quotation temp:quotation)	
    	  {
    		  temp.setStatus(Quotation.QUOTATIONSUCESSFUL);
    	  }
    		
    	}
    	
    	else if(con.getStatus().equals(Contract.CONTRACTCANCEL))
    	{
    		for(Quotation temp:quotation)	
      	  {
      		  temp.setStatus(Quotation.QUOTATIONUNSUCESSFUL);
      	  }
    	}
    	
    	
    	ofy().save().entities(quotation).now();
    		
    }
    
	
   private void cancelServices(Contract con)
   {
	   List<Service>service;
	   if(con.getCompanyId()!=null)
   	{
		   service =ofy().load().type(Service.class).filter("contractCount",con.getCount()).
   			filter("companyId",con.getCompanyId()).list();
   	}
   	
   	else
   		service =	ofy().load().type(Service.class).filter("contractCount",con.getCount()).list();
	   
	   if(service!=null)
	   {
		   for(Service ser:service)
		   {
			   if(ser!=null)
			   {
				   ser.setStatus(Service.SERVICESTATUSCANCELLED);
			   }
		   }
	   }
	   if(service.size()!=0)
		   ofy().save().entities(service).now();
   }
   
   
//   protected void createProject(Contract con){
//	   System.out.println("Entered In project");
//	   ArrayList<SuperModel> projects=new ArrayList<SuperModel>();
//	   int servicesNo;
////	   for(SalesLineItem prod:con.getItems())
////	   {
////		   long noServices=(long) (prod.getNumberOfServices()*prod.getQty());
////		   servicesNo=(int) (prod.getNumberOfServices()*prod.getQty());
////		   for(int k=0;k<noServices;k++)
////			{
//////			   int serviceSrNo=k+1;
//////			   int pCount=prod.getPrduct().getCount();
////			   System.out.println("Index FIlter"+(servicesNo+(k+1)));
////			   int flagService=(servicesNo+(k+1));
////			   int serviceSrNo=con.getServiceScheduleList().get(k).getScheduleServiceNo();
////			   int pCount=con.getServiceScheduleList().get(k).getScheduleProdId();
////			   projects.add(getFieldsToCustomerProjectScreen(pCount,serviceSrNo,con,flagService));
////			}
////		   GenricServiceImpl impl=new GenricServiceImpl();
////			if(projects!=null&&projects.size()!=0){
////			    impl.save(projects);
////			    servicesNo=(int) (servicesNo+noServices);
////			}
////	   }
//	   
//		   for(int k=0;k<con.getServiceScheduleList().size();k++)
//			{
//			   int serviceSrNo=con.getServiceScheduleList().get(k).getScheduleServiceNo();
//			   int pCount=con.getServiceScheduleList().get(k).getScheduleProdId();
//			   projects.add(getFieldsToCustomerProjectScreen(pCount,serviceSrNo,con,k+1));
//			}
//		   GenricServiceImpl impl=new GenricServiceImpl();
//			if(projects!=null&&projects.size()!=0){
//			    impl.save(projects);
//			}
//   }
//
//
//   
//
//	private ServiceProject getFieldsToCustomerProjectScreen(int prodCount,int serviceSrNo,Contract con,int indexForProject) {
//		
//		
//		 Customer cust=null;
//		 Address addr=null;
//		 Employee empEntity=null;
//		 Service serEntity=null;
//		 BillOfMaterial bomEntity=null;
//			 if(con.getCinfo()!=null)
//			   {
//				   if(con.getCompanyId()!=null)	
//				   {
//					   
//					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
//					   filter("companyId",con.getCompanyId()).first().now();
//				   }
//				   else
//				   {
//					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
//							   first().now();  
//				   }
//			   }
//			
//		 if(cust!=null){
//			  addr=cust.getSecondaryAdress();
//		 }
//	
//		 if(con.getCompanyId()!=null){
//			 serEntity=ofy().load().type(Service.class).filter("contractCount",con.getCount()).
//					   	filter("product.count",prodCount).filter("companyId",con.getCompanyId()).filter("serviceIndexNo", indexForProject).first().now();
//		 }
//		 else{
//			 serEntity=ofy().load().type(Service.class).filter("contractCount",con.getCount()).
//					   	filter("product.count",prodCount).filter("serviceIndexNo", indexForProject).filter("serviceSerialNo",serviceSrNo).first().now();
//		 }
//		 
//		 if(con.getCompanyId()!=null){
//			 empEntity=ofy().load().type(Employee.class).filter("fullname",con.getEmployee().trim()).filter("companyId",con.getCompanyId()).first().now();
//		 }
//		 else{
//			 empEntity=ofy().load().type(Employee.class).filter("fullname",con.getEmployee().trim()).first().now();
//		 }
//		 
//		 if(con.getCompanyId()!=null){
//			 bomEntity=ofy().load().type(BillOfMaterial.class).filter("product_id", prodCount).
//						filter("companyId",con.getCompanyId()).filter("status",true).first().now();
//		 }
//		 else{
//			 bomEntity=ofy().load().type(BillOfMaterial.class).filter("product_id", prodCount).filter("status",true).first().now();
//		 }
//		 
//		 
//	
//		
//		ServiceProject cusproject=new ServiceProject();
//		
//		cusproject.setserviceId(serEntity.getCount());
//		cusproject.setCompanyId(con.getCompanyId());
//		cusproject.setserviceEngineer(serEntity.getEmployee());
//		cusproject.setServiceStatus(serEntity.getStatus());
//		cusproject.setserviceDate(serEntity.getServiceDate());
//		cusproject.setContractId(serEntity.getContractCount());
//		cusproject.setContractStartDate(serEntity.getContractStartDate());
//		cusproject.setContractEndDate(serEntity.getContractEndDate());
//		cusproject.setPersonInfo(serEntity.getPersonInfo());
//		cusproject.setPocName(serEntity.getEmployee());
//		cusproject.setPocName(empEntity.getFullname());
//		cusproject.setPocLandline(empEntity.getLandline());
//		cusproject.setPocCell(empEntity.getCellNumber1());
//		cusproject.setPocEmail(empEntity.getEmail());
//		cusproject.setProjectName("Project-"+serEntity.getCount());
//		cusproject.setProjectStatus("Created");
//		cusproject.setAddr(addr);
//		cusproject.setServiceSrNo(serEntity.getServiceSerialNo());
//		
//		if(bomEntity!=null){
//			List<CompanyAsset> companyAssetList=getToolsFromProductId(bomEntity);
//			if(companyAssetList!=null){
//				cusproject.setTooltable(companyAssetList);
//			}
//		}
//		
//		
//		if(empEntity!=null){
//			cusproject.setTechnicians(getTechiciansFromEmployeeInfo(empEntity));
//		}
//		
//		List<ProductGroupList> billProdList=getMaterialAndToolFromProductId(con,bomEntity,serEntity);
//		
//		if(billProdList.size()!=0){
//			cusproject.setProdDetailsList(billProdList);
//		}
//		
//		return cusproject;
//	}
//
//	private List<CompanyAsset> getToolsFromProductId(BillOfMaterial bomEntity) {
//		
//		if(bomEntity!=null){
//			return bomEntity.getToolItems();
//		}
//		else{
//			return null;
//		}
//	}
//
//		private List<ProductGroupList> getMaterialAndToolFromProductId(Contract con,BillOfMaterial bomEntity,Service serEntity) {
//			
//			int prodId=0;
//			List<ProductGroupList> prodDetailslist = new ArrayList<ProductGroupList>();
//			List<ProductGroupList> prodDetailslis = null;
//			
//			if(bomEntity!=null)
//			{
//				if(bomEntity.getBillProdItems().get(0).getProdGroupId()!=0){
//					prodId=bomEntity.getBillProdItems().get(0).getProdGroupId();
//				}
//			}
//			
//			if(prodId!=0)
//			{
//				 if(con.getCompanyId()!=null){
//					 prodDetailslis=ofy().load().type(ProductGroupList.class).filter("productGroupId", prodId).
//								filter("companyId",con.getCompanyId()).filter("status",true).list();
//				 }
//				 else{
//					 prodDetailslis=ofy().load().type(ProductGroupList.class).filter("productGroupId", prodId).filter("status",true).list();
//				 }
//			}
//			 
//			if(prodDetailslis!=null)
//			{
//				 if(prodDetailslis.size()!= 0)
//				 {
//					 for(int i=0;i<prodDetailslis.size();i++)
//					 {
//						 ProductGroupList prodDetailsListEntity=new ProductGroupList();
//						 
//						 prodDetailsListEntity.setProduct_id(prodDetailslis.get(i).getProduct_id());
//						 prodDetailsListEntity.setCode(prodDetailslis.get(i).getCode());
//						 prodDetailsListEntity.setName(prodDetailslis.get(i).getName());
//						 prodDetailsListEntity.setQuantity(prodDetailslis.get(i).getQuantity());
//						 prodDetailsListEntity.setUnit(prodDetailslis.get(i).getUnit());
//						 
//						 prodDetailslist.add(prodDetailsListEntity); 
//					 }
//				 }
//			}
//			
//			return prodDetailslist;
//		}
//
//
//	/**
//	 * This method is used for retrieving Technicians information for saving in ServiceProject Entity 
//	 */
//
//	private List<EmployeeInfo> getTechiciansFromEmployeeInfo(Employee empEntity) {
//		
//		ArrayList<EmployeeInfo> technicians=new ArrayList<EmployeeInfo>();
//		
//		EmployeeInfo empInfo=new EmployeeInfo();
//		empInfo.setFullName(empEntity.getFullName());
//		empInfo.setCellNumber(empEntity.getCellNumber1());
//		empInfo.setEmpCount(empEntity.getCount());
//		technicians.add(empInfo);
//		
//		return technicians;
//	}
//   
   
   private boolean checkprocessconfiguration(Contract con) {

		boolean flag = false;
		System.out.println("hi ===== ");
		ProcessName processname = ofy().load().type(ProcessName.class).filter("processName", "NumberRange").filter("status",true).filter("companyId", con.getCompanyId()).first().now();
		ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "NumberRange").filter("processList.processType", "NumberRangeProcess").filter("processList.status", true).filter("companyId", con.getCompanyId()).first().now();
		if(processname!=null){
			System.out.println("In process name");
			if(processconfiguration!=null){
				System.out.println("In Processconfig");
				flag=true;
			}
		}
		
		return flag;
	}

   
   
   
 //    rohan added this code for creating service  
@Override
public boolean createServicesFromContrat(Contract con) {
	

	logger.log(Level.SEVERE,"Service Creaiton Methd");
	
	List<Service> list = new ArrayList<Service>();
	
	list = ofy().load().type(Service.class).filter("contractCount",con.getCount()).filter("companyId", con.getCompanyId()).list();
	
	 /**
	  *  nidhi
	  *  Date : 4-12-2017
	  *  Check process configration for 
	  */
	 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", con.getCompanyId());
	 
	 /**
	  * end
	  */
	 /**
	  * nidhi
	  * 26-09-2018 for map bill for material
	  */
	 boolean billofMaterialActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", con.getCompanyId());
	
	if(list.size()!=0)
	{
		return false;
	}
	else
	{
	ArrayList<SuperModel> services=new ArrayList<SuperModel>();
	Customer cust=null;
	Address addr=null;
	
	//vijay
	
	 double totalservices=0;
	 
	 totalservices = gettotalnumberofservices(con.getItems());
	

		/**
		 * 18-06-2018
		 * nidhi
		 * 
		 */
		HashSet<String> branchSet = new HashSet<String>();
		for (int i=0 ; i< con.getItems().size(); i++)
		{
			
			Set<Integer> ser =  con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet();
			if(ser!=null) {
			for(Integer seq : ser){
				ArrayList<BranchWiseScheduling> branch =  con.getItems().get(i).getCustomerBranchSchedulingInfo().get(seq);
				for(int k =0 ;k<branch.size();k++){
					if(!branch.get(k).getBranchName().equalsIgnoreCase("Service Address")
							&& !branch.get(k).getBranchName().equalsIgnoreCase("Select")  && branch.get(k).isCheck())
					{
						branchSet.add(branch.get(k).getBranchName());
					}
				}
				
			}
			}
		}
		
		List<CustomerBranchDetails> custBranchList = new ArrayList<CustomerBranchDetails>();
		ArrayList <String> branchList = new  ArrayList<String>();
		if(branchSet.size()>0){
			branchList.addAll(branchSet);
			
			custBranchList = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName IN",  branchList).filter("cinfo.count", con.getCinfo().getCount())
			   .filter("companyId",con.getCompanyId()).list();
			
		}
		
		HashMap<String, Address> addList = new HashMap<String, Address>();
		
		for(CustomerBranchDetails branchDt : custBranchList){
			addList.put(branchDt.getBusinessUnitName(), branchDt.getAddress());
		}
		/**
		 * end
		 */
	for(SalesLineItem item:con.getItems())
	{
		
		
//		boolean 
		long companyId = con.getCompanyId();
		
		NumberGeneration numbergen = null;	
		
//		NumberGeneration numbergen = new NumberGeneration();
//		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
		  
		
			boolean processconfigflag = checkprocessconfiguration(con);
			
			if(processconfigflag){
				logger.log(Level.SEVERE,"In the process config true");

				System.out.println("In the process config true");
				List<NumberGeneration> numbergenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", con.getCompanyId()).list();
				if(con.getNumberRange()!=null  && !con.getNumberRange().equals("")){
					
					logger.log(Level.SEVERE,"111111111111");

					String selectedNumberRange = con.getNumberRange();
					String prefixselectedNumberRange = "S_"+selectedNumberRange;
					
					for(int i=0;i<numbergenrationList.size();i++){
						
						if(prefixselectedNumberRange.equals(numbergenrationList.get(i).getProcessName())){
							logger.log(Level.SEVERE,"22222222222");

							numbergen=ofy().load().type(NumberGeneration.class).filter("companyId",con.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
						}
					}
				}
				else{
					logger.log(Level.SEVERE,"33333333333333");

					numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
				}
			}
			else{
				logger.log(Level.SEVERE,"44444444444444");

				numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
			}
		
	    long number = numbergen.getNumber();
	    
	    logger.log(Level.SEVERE,"Last Number===="+number);
    	 int count = (int) number;
    	 
    	 // end vijay
		
		
		
		if(item.getPrduct() instanceof ServiceProduct)
	    {
		   if(con.getCinfo()!=null)
		   {
			   if(con.getCompanyId()!=null)	
			   {
				   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
				   filter("companyId",con.getCompanyId()).first().now();
			   }
			   else
			   {
				   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
						   first().now();  
			   }
		   }
//				if(cust!=null)
//				    addr=cust.getSecondaryAdress();
			
				
			
				/**
				 *  nidhi 
				 *  30-03-2018
				 *  add for create service if more than 
				 */
				/**
				 * Date : 04-09-2017 By ANIL 
				 * if service schedule list is empty then we check another entity ServiceSchedule 
				 * in which we store schedule services if service count is more than thousand. 
				 */
				
					 List<ServiceSchedule> schList=new ArrayList<>();
					 ofy().clear();
					 if(con.getServiceScheduleList().size()==0){
						 
						schList=ofy().load().type(ServiceSchedule.class).filter("companyId", con.getCompanyId()).filter("documentType", "Contract").filter("documentId", con.getCount()).list();
						if(schList!=null){
							logger.log(Level.SEVERE,"FROM DB LIST SIZE : "+schList.size());
						}
						ObjectifyService.reset();
					    ofy().consistency(Consistency.STRONG);
					    ofy().clear();
					    
					    logger.log(Level.SEVERE,"get loaded service list for more than 10000 service --" + schList.size()  );
						 con.getServiceScheduleList().addAll(schList);
						 
					 }
					 
				
				
				 /**
				  * End
				  */
				 /**
				  * end
				  */
				
				
				
			for(int k=0;k<con.getServiceScheduleList().size();k++)
			{
				double serviceValue = 0;
				logger.log(Level.SEVERE,"Rohan123"+item.getProductSrNo()+"--"+con.getServiceScheduleList().get(k).getSerSrNo());
				if(item.getProductSrNo()==con.getServiceScheduleList().get(k).getSerSrNo()){
					logger.log(Level.SEVERE,"Rohan123"+item.getProductSrNo()+"=="+con.getServiceScheduleList().get(k).getSerSrNo());
					
					/**
					 * nidhi
					 * 18-06-2018
					 */
					if(con.getServiceScheduleList().get(k).getScheduleProBranch().equalsIgnoreCase("Service Address"))
					{
						if(cust!=null)
						    addr=cust.getSecondaryAdress();
					}
					else if(con.getServiceScheduleList().get(k).getScheduleProBranch().equalsIgnoreCase("Select")){
						if(cust!=null)
						    addr=cust.getSecondaryAdress();
					}
					else
					{
						   if(con.getServiceScheduleList().get(k).getScheduleProBranch()!=null)	
						   {
							   /*custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",schList.get(k).getScheduleProBranch()).
							   filter("companyId",con.getCompanyId()).first().now();
						   }
						   else
						   {
							   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",schList.get(k).getScheduleProBranch()).
									   first().now(); */
							   /**
								 * 18-06-2018
								 * nidhi
								 * 
								 */
							   addr = addList.get(con.getServiceScheduleList().get(k).getScheduleProBranch());
							   /**
							    * end
							    */
						   }
						   
//						   addr = custBrach.getAddress();
						   
					}
					
					/**
					 *  ends here 
					 */
					
					
					if(totalservices > 50){
						
						logger.log(Level.SEVERE,"WITH TASK QUEUE SEVICES");

						int setcount = ++count;
						
						System.out.println("ref no value "+con.getRefNo());
						int seviceindex = k+1;
						String refNo="0";
						if(con.getRefNo()!= null && !con.getRefNo().equals(""))
						{
							refNo = con.getRefNo();
						}
						String refno2 ="";
						if(con.getRefNo2()!= null && !con.getRefNo2().equals(""))
						{
							refno2 = con.getRefNo2();
						}
						else
						{
							refno2 = 0+"";
						}
						
						String techName ="";
						if(con.getTechnicianName()!= null && !con.getTechnicianName().equals(""))
						{
							techName = con.getTechnicianName();
						}
						else
						{
							techName = 0+"";
						}
						
						
						String serviceRemark="";
						if(con.getServiceScheduleList().get(k).getServiceRemark()!=null && !con.getServiceScheduleList().get(k).getServiceRemark().equals("")){
							serviceRemark=con.getServiceScheduleList().get(k).getServiceRemark();
						}else{
							serviceRemark=0+"";
						}
						
						System.out.println(" Service Remark =="+serviceRemark);
						serviceValue=item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
						logger.log(Level.SEVERE,"serviceValue13="+serviceValue);
						/**
						 * Date : 06-10-2017 BY ANIL
						 */
						String serviceWiseBilling="NO";
						if(con.isServiceWiseBilling()){
							serviceWiseBilling="YES";
						}
						/**
						 * End
						 */
						/**
						 * Date : 13-11-2017 BY ANIL
						 * setting servicing branch as branch name in service 
						 * earlier it was contract's branch
						 */
						String servicingBranch="";
						if(con.getServiceScheduleList().get(k).getServicingBranch()!=null
								&&!con.getServiceScheduleList().get(k).getServicingBranch().equals("")){
							servicingBranch=con.getServiceScheduleList().get(k).getServicingBranch();
						}else{
							servicingBranch=con.getBranch();
						}
						/**
						 * End
						 */
						/** date 17.02.2018 added by komal for area wise billing**/
						double perUnitPrice = 0;
						String costCenter = "na";
						double area = 0;
						String uom = "na";
						/**
						 * nidhi
						 * 22-05-2018
						 * for nbhc
						 */
						boolean priceStatus = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CustomerBranchAreaWiseBilling", con.getCompanyId());
						
						if(con.isServiceWiseBilling() && priceStatus){
							double tax = 0;
							double origPrice = 0;
							if(!(item.getArea().equalsIgnoreCase("NA"))){
							logger.log(Level.SEVERE , "inside area");
							CustomerBranchDetails branches = ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId())	.filter("status", true).filter("cinfo.count", con.getCinfo().getCount()).filter("buisnessUnitName", con.getServiceScheduleList().get(k).getScheduleProBranch()).first().now();
							logger.log(Level.SEVERE , "branches :" + branches);
							if(branches != null){
								if(branches.getCostCenter() !=null){
									costCenter = branches.getCostCenter();
								}
							if(branches.getArea()!=0){
								logger.log(Level.SEVERE , "branches area:" + branches.getArea());
								tax = removeAllTaxes(item.getPrduct());
								origPrice = item.getPrice() - tax;
								serviceValue = Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice)));
								logger.log(Level.SEVERE,"serviceValue14="+serviceValue);
								perUnitPrice = origPrice;
								//temp.setServiceValue(Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice))));
								//temp.setPerUnitPrice(origPrice);
								area = branches.getArea();
								if(branches.getUnitOfMeasurement()!=null){
									uom = branches.getUnitOfMeasurement();
								}
								}
							}	
						}
					}
					/**
					 * end komal
					 */

						/**
						 * Date 04-07-2018 
						 * Developer :- Vijay
						 * Des :- Primises not setting to services
						 */
						String primises ="";
						if(item.getPremisesDetails()!=null && !item.getPremisesDetails().equals("") ){
							primises = item.getPremisesDetails();
						}else{
							primises = 0+"";
						}
						/**
						 * ends here
						 */
						/**
						 * nidhi
						 * 8-08-2018
						 * for map model and serial no
						 */

						String proSerialNo ="0", proModelNo = "0";
						
						if(item.getProModelNo() != null && !item.getProModelNo().equals("")){
							proModelNo = item.getProModelNo();
						}
					
						if(item.getProSerialNo() != null && !item.getProSerialNo().equals("")){
							proSerialNo = item.getProSerialNo();
						}
						

						/**
						 * nidhi
						 * 5-10-2018
						 */
						String jsonProDetails="NA";
						
						if(billofMaterialActive){
							Gson gson = new Gson();
							if(con.getServiceScheduleList().get(k).getServiceProductList()!=null && con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0)
								&& con.getServiceScheduleList().get(k).getServiceProductList().get(0).size()>0){
								jsonProDetails = gson.toJson(con.getServiceScheduleList().get(k).getServiceProductList().get(0));
							}
							
						}
						
						String customerPoc=con.getCinfo().getPocName();
						String customerCell=con.getCinfo().getCellNumber()+"";
						System.out.println("Customer Branch crete Service from contrcat st Task Queue");
						if(!con.getServiceScheduleList().get(k).getScheduleProBranch().equals("Service Address")){
							if(custBranchList!=null&&custBranchList.size()!=0){
								System.out.println("Customer Branch Size === "+custBranchList.size());
								for(CustomerBranchDetails custBranch:custBranchList){
									if(custBranch.getBusinessUnitName().equals(con.getServiceScheduleList().get(k).getScheduleProBranch())){
										if(custBranch.getPocName()!=null&&!custBranch.getPocName().equals("")){
											System.out.println("Customer Branch Poc Name === "+custBranch.getPocName());
											customerPoc=custBranch.getPocName();
										}
										if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
											System.out.println("Customer Branch Cell No === "+custBranch.getCellNumber1());
											customerCell=custBranch.getCellNumber1()+"";
										}
										break;
									}
								}
							}
						}
						
						
						
						
						String serviceString="";
						serviceString= con.getCinfo().getCount()+"$"+con.getCinfo().getEmail()+"$"+con.getCinfo().getFullName()+"$"+customerPoc+"$"+customerCell
								+"$"+con.getCount()+"$"+servicingBranch+"$"+Service.SERVICESTATUSSCHEDULE+"$"+con.getStartDate()+"$"+con.getEndDate()+"$"+false+
								"$"+seviceindex+"$"+"Periodic"+"$"+con.getServiceScheduleList().get(k).getScheduleServiceNo()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceDate()+
								"$"+addr.getAddrLine1()+"$"+addr.getAddrLine2()+"$"+addr.getCountry()+"$"+addr.getState()+"$"+addr.getCity()+"$"+addr.getLocality()+"$"+addr.getPin()+"$"+addr.getLandmark()+"$"+con.getScheduleServiceDay()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceTime()+
								"$"+con.getServiceScheduleList().get(k).getScheduleProBranch()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceDay()+
								"$"+con.getCompanyId()+"$"+item.getPrduct().getComment()+"$"+item.getPrduct().getCommentdesc()+"$"+item.getPrduct().getCompanyId()+"$"+item.getPrduct().getCount()+"$"
								+item.getPrduct().getCreatedBy()+"$"+item.getPrduct().isDeleted()+"$"+item.getDuration()+"$"+item.getPrduct().getId()+"$"+item.getNumberOfServices()+"$"+item.getPrice()+"$"
								+item.getPrduct().getProductCategory()+"$"+item.getPrduct().getProductClassification()+"$"+item.getPrduct().getProductCode()+"$"
								+item.getPrduct().getProductGroup()+"$"+item.getPrduct().getProductName()+"$"+item.getPrduct().getProductType()+"$"+item.getPrduct().getRefNumber1()+"$"+item.getPrduct().getRefNumber2()+"$"
								+item.getPrduct().getServiceTax().isInclusive()+"$"+item.getPrduct().getServiceTax().getPercentage()+"$"+item.getPrduct().getServiceTax().getTaxConfigName()+"$"
								+item.getPrduct().getServiceTax().getTaxName()+"$"+item.getPrduct().getSpecifications()+"$"+item.getPrduct().isStatus()+"$"
								+item.getPrduct().getUnitOfMeasurement()+"$"+item.getPrduct().getUserId()+"$"+item.getPrduct().getVatTax().isInclusive()+"$"+item.getPrduct().getVatTax().getPercentage()+"$"
								+item.getPrduct().getVatTax().getTaxConfigName()+"$"+item.getPrduct().getVatTax().getTaxName()
								+"$"+setcount+"$"+con.getNumberRange()+"$"+false+"$"+refNo+"$"+refno2+"$"+serviceRemark+"$"
								+con.getServiceScheduleList().get(k).getWeekNo()+"$"+techName+"$"+String.format("%.2f", serviceValue)+"$"
								+serviceWiseBilling+"$"+item.getProductSrNo()/**
								 nidhi 
								 date : 4-12-2017
								 set service type as process configration 
								 */+"$"+confiFlag +"$"+con.getCategory()/**
								 * end *//**  nidhi Date : 29-01-2018 */+"$"+con.getPocName()
								 /** komal date 8.3.2018 **/+"$"+perUnitPrice+"$"+costCenter+"$"+area+"$"+uom
								 /** nidhi */+"$"+con.isRenewContractFlag()+"$"+primises+"$"+proModelNo+"$"+proSerialNo/**Nidhi for map serial no**/
								 +"$" + jsonProDetails /**nidhi 29-09-2018 for min list */;

						logger.log(Level.SEVERE,"serviceValue14 for TaskQueue="+serviceValue);
						Queue queue = QueueFactory.getQueue("ContractServices-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractservicestaskqueue").param("servicekey", serviceString));
								
			}
			else
			{
				logger.log(Level.SEVERE,"FOR NORMAL Sevices");
				Service temp=new Service();
				
				String customerPoc=con.getCinfo().getPocName();
				Long customerCell=con.getCinfo().getCellNumber();
				System.out.println("Customer Branch contract to service else part Task Queue");
				if(!con.getServiceScheduleList().get(k).getScheduleProBranch().equals("Service Address")){
					if(custBranchList!=null&&custBranchList.size()!=0){
						System.out.println("Customer Branch Size === "+custBranchList.size());
						for(CustomerBranchDetails custBranch:custBranchList){
							if(custBranch.getBusinessUnitName().equals(con.getServiceScheduleList().get(k).getScheduleProBranch())){
								if(custBranch.getPocName()!=null&&!custBranch.getPocName().equals("")){
									System.out.println("Customer Branch Poc Name === "+custBranch.getPocName());
									customerPoc=custBranch.getPocName();
								}
								if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
									System.out.println("Customer Branch Cell No === "+custBranch.getCellNumber1());
									customerCell=custBranch.getCellNumber1();
								}
								break;
							}
						}
					}
					temp.getPersonInfo().setCount(con.getCinfo().getCount());
					temp.getPersonInfo().setFullName(con.getCinfo().getFullName());
					temp.getPersonInfo().setPocName(customerPoc);
					temp.getPersonInfo().setCellNumber(customerCell);
					temp.getPersonInfo().setEmail(con.getCinfo().getEmail());
					
				}else{
				temp.setPersonInfo(con.getCinfo());
				}
				
				temp.setContractCount(con.getCount());
				temp.setProduct((ServiceProduct) item.getPrduct());
				temp.setBranch(con.getBranch());
				temp.setStatus(Service.SERVICESTATUSSCHEDULE);
				temp.setEmployee(con.getEmployee());
				temp.setContractStartDate(con.getStartDate());
				temp.setContractEndDate(con.getEndDate());
				temp.setAdHocService(false);
				temp.setServiceIndexNo(k+1);
				if(con.getServicetype()!=null&&!con.getServicetype().equals("")){
					temp.setServiceType(con.getServicetype()); //Ashwini Patil Date:29-02-2024 for orion
				}else
				temp.setServiceType("Periodic");
				temp.setRefNo(con.getRefNo());					
				temp.setServiceSerialNo(con.getServiceScheduleList().get(k).getScheduleServiceNo());
				temp.setServiceDate(con.getServiceScheduleList().get(k).getScheduleServiceDate());
		    	temp.setAddress(addr);
		    	
//		    	if(con.getScheduleServiceDay()!=null){
//					temp.setServiceDay(con.getScheduleServiceDay());
//				}
		    	
		    	/**above old code commented by vijay
		    	 * Date 1 March 2017 changed by vijay for setting service day in services
		    	 */
		    	if(con.getServiceScheduleList().get(k).getScheduleServiceDay()!=null){
					temp.setServiceDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
				}
		    	/**
		    	 * Ends here
		    	 */
		    	
				temp.setServiceTime(con.getServiceScheduleList().get(k).getScheduleServiceTime());
				temp.setServiceBranch(con.getServiceScheduleList().get(k).getScheduleProBranch());
				temp.setServiceDateDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
		    	
				temp.setBranch(con.getServiceScheduleList().get(k).getServicingBranch());
				temp.setServicingTime(con.getServiceScheduleList().get(k).getTotalServicingTime());
				
				
				if(con.getNumberRange()!=null )
					temp.setNumberRange(con.getNumberRange());
				

				/**
				 * Added this for NBHC Only (By Rahul)
				 * 
				 */
				if(con.getStackDetailsList()!=null)
				temp.setStackDetailsList(con.getStackDetailsList());
				
				
				/**
				 * Date 07 Feb 2017
				 * vijay added this for in contract product level remark will set in service entity
				 */
				temp.setRemark(con.getServiceScheduleList().get(k).getServiceRemark());
				/*
				 * End here
				 */
				
				/**
				 * Date 02-04-2017 added by vijay for service week no of month
				 */
				temp.setServiceWeekNo(con.getServiceScheduleList().get(k).getWeekNo());
				/**
				 * ends here
				 */
				
				/**
				 * Date : 12-02-2017 added by rohan added this code for adding technician in services
				 */
				
				temp.setEmployee(con.getTechnicianName());
				/**
				 * ends here 
				 */
				
				/**
				 *  nidhi
				 *  29-01-2018
				 *  for copy pocname in person info
				 */
				if(con.getPocName()!=null && !con.getPocName().trim().equals("")){
					temp.getPersonInfo().setPocName(con.getPocName());
				}
				/**
				 *  end
				 */
				/** date 17.02.2018 added by komal for area wise billing**/
				/**
				 * nidhi
				 * 22-05-2018
				 * for nbhc
				 */
				boolean priceStatus = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "CustomerBranchAreaWiseBilling", con.getCompanyId());
				
				
				if(con.isServiceWiseBilling() && priceStatus){
					if(!(item.getArea().equalsIgnoreCase("NA"))){
					logger.log(Level.SEVERE , "inside area");
					CustomerBranchDetails branches = ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId())	.filter("status", true).filter("cinfo.count", con.getCinfo().getCount()).filter("buisnessUnitName", con.getServiceScheduleList().get(k).getScheduleProBranch()).first().now();
					logger.log(Level.SEVERE , "branches :" + branches);
					if(branches != null){
						if(branches.getCostCenter() !=null){
							temp.setCostCenter(branches.getCostCenter());
						}
					if(branches.getArea()!=0){
						logger.log(Level.SEVERE , "branches area:" + branches.getArea());
						double tax = removeAllTaxes(item.getPrduct());
						double origPrice = item.getPrice() - tax;						
						temp.setServiceValue(Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice))));
						logger.log(Level.SEVERE,"serviceValue15="+Double.parseDouble(String.format("%.2f",(branches.getArea()*origPrice))));
						temp.setPerUnitPrice(origPrice);
						temp.setQuantity(branches.getArea());
						if(branches.getUnitOfMeasurement()!=null){
							temp.setUom(branches.getUnitOfMeasurement());
						}
						temp.setUom(branches.getUnitOfMeasurement());
						}
					}	
				}
			}
			/**
			 * end komal
			 */
				
			/**
			 * Date 04-07-2018 
			 * Developer :- Vijay
			 * Des :- Primises not setting to services
			 */
			if(item.getPremisesDetails()!=null && !item.getPremisesDetails().equals("") ){
				temp.setPremises(item.getPremisesDetails());
			}
			/**
			 * ends here
			 */
			/**
			 * nidhi
			 * 8-08-2018
			 */
			if(item.getProModelNo() != null){
				temp.setProModelNo(item.getProModelNo());
			}
			if(item.getProSerialNo()!=null){
				temp.setProSerialNo(item.getProSerialNo());
			}
			/**
			 * end
			 */
			/**
			 * nidhi
			 *  *:*:*
			 *  for bill of material calculation 
			 */
			if(billofMaterialActive){
				if(con.getServiceScheduleList().get(k).getServiceProductList()!=null &&
						con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0) &&
						con.getServiceScheduleList().get(k).getServiceProductList().get(0).size()>0){
					temp.setUom(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getUnit());
					temp.setQuantity(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getQuantity());
					ArrayList<ServiceProductGroupList> serProList  = new ArrayList<ServiceProductGroupList>();
					for(int jj=0 ; jj<con.getServiceScheduleList().get(k).getServiceProductList().get(0).size();jj++){
						serProList.add(temp.getServiceProdDt(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(jj)));
					}
					
					temp.setServiceProductList(serProList);
				}
			}
				services.add(temp);
				
				temp.setCompanyId(con.getCompanyId());
				}
			}
	    }
			
	}
		logger.log(Level.SEVERE,"Services Creation Complete");
		if(totalservices>50){
			numbergen.setNumber(count);
	    	 GenricServiceImpl impl = new GenricServiceImpl();
			  impl.save(numbergen);
		}else{
			
			GenricServiceImpl impl=new GenricServiceImpl();
			if(services!=null&&services.size()!=0){
				logger.log(Level.SEVERE,"Saving Services");
			    impl.save(services);
			    System.out.println("Saving Services");
			}
			
		}
	}
	
	/**
	 * nidhi
	 *  for map service invoice
	 *  
	 */
	
	boolean serviceMapping = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "ServiceInvoiceMappingOnContractApproval", con.getCompanyId());
	
	if(!con.isContractRate() && !con.isServiceWiseBilling() &&  serviceMapping){
		final Integer contractCount = con.getCount();
		Queue queue = QueueFactory.getQueue("ContractServicesInvoiceMapping-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractServiceInvoiceMapping")
				.param("contract", contractCount+"").param("companyId", con.getCompanyId()+"")
				.param("totalservices", totalservices+""));
	}
	
	/**
	 *  After creation of services Customer Project is auto created.
	 *  This method is called for creating customer project.
	 */
	return true;
}
}

/**
 * Rohan added this method to create services for EVA as per EVA request on date 12-06-2017 
 */

@Override
public boolean createServicesAsPerEVA(Contract contract) {
	System.out.println("in side create services in EVA");
	List<Service> serviceList = new ArrayList<Service>();
	List<Service> serviceLis = new ArrayList<Service>();
	serviceList=ofy().load().type(Service.class).filter("contractCount",contract.getCount()).filter("companyId",contract.getCompanyId()).list();
	System.out.println("in side create services in EVA serviceList size"+serviceList.size());
	
	NumberGeneration number = ofy().load().type(NumberGeneration.class).filter("companyId",contract.getCompanyId()).filter("processName", "Service").first().now();
	int count =0;
	System.out.println("count before="+ Integer.parseInt(number.getNumber()+""));
	if(serviceList.size() < 10 ){
	int  serviceNo = 12-serviceList.size();
	if(serviceNo > 0){
		for (int i = 0; i < serviceNo; i++) {
			System.out.println("Add"+contract.getCount()+"-person info-"+contract.getStartDate()+"="+contract.getEndDate());
			count=Integer.parseInt(number.getNumber()+"")+(i+1);
			Service temp=new Service();
			
			temp.setPersonInfo(contract.getCinfo());
			temp.setContractCount(contract.getCount());
			temp.setBranch(contract.getBranch());
			temp.setStatus(Service.SERVICESTATUSSCHEDULE);
			temp.setEmployee(contract.getEmployee());
			temp.setContractStartDate(contract.getStartDate());
			temp.setContractEndDate(contract.getEndDate());
			temp.setAdHocService(false);
			temp.setServiceIndexNo(i+1);
			if(contract.getServicetype()!=null&&!contract.getServicetype().equals("")){
				temp.setServiceType(contract.getServicetype()); //Ashwini Patil Date:29-02-2024 for orion
			}else
				temp.setServiceType("Periodic");
			temp.setRefNo(0+"");					
			temp.setServiceSerialNo(1);
			temp.setServiceDate(new Date());
			temp.setServiceDay("Flexible");
			temp.setServiceTime("Flexible");
			temp.setServiceBranch("Vikhroli");
			temp.setServiceDateDay("");
			temp.setBranch(contract.getBranch());
			temp.setServicingTime(2.0);
			if(contract.getNumberRange()!=null )
				temp.setNumberRange(contract.getNumberRange());
			if(contract.getStackDetailsList()!=null)
			temp.setStackDetailsList(contract.getStackDetailsList());
			temp.setRemark("");
			temp.setServiceWeekNo(1);
			temp.setEmployee(contract.getTechnicianName());
			temp.setCompanyId(contract.getCompanyId());
			temp.setCount(count);
			serviceLis.add(temp);
		}
		
		
		ofy().save().entities(serviceLis).now();
		System.out.println("count after "+count);
		number.setNumber(Long.parseLong(count+""));
		ofy().save().entity(number).now();
	}

	
	List<Service> mySerrviceList=new ArrayList<Service>();
	mySerrviceList =ofy().load().type(Service.class).filter("contractCount",contract.getCount()).filter("companyId",contract.getCompanyId()).list();
	System.out.println("in side create services in EVA mySerrviceList size"+mySerrviceList.size());
	SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	System.out.println("Contract St Date "+contract.getStartDate());
	
	Comparator<Service> comp = new Comparator<Service>() {
		@Override
		public int compare(Service o1, Service o2) {
			Integer num1 = o1.getCount();
			Integer num2 = o2.getCount();
			
			return num1.compareTo(num2);
		}
	};
	Collections.sort(mySerrviceList,comp);
	
	
	for (int i=0;i<mySerrviceList.size();i++) {
		Calendar now = Calendar.getInstance();
	
		if(i==0){
			Date date= contract.getStartDate();
			 try {
				now.setTime(sdfDate.parse(sdfDate.format(date)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		 now.add(Calendar.DATE, 2);
    		 
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 1:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("Registration Form");
    		 mySerrviceList.get(i).setServiceSerialNo(1);
		}else if(i==1){
			Date date= contract.getStartDate();
			 now.setTime(date);
    		 now.add(Calendar.DATE, 5);
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 2:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("Production Link");
    		 mySerrviceList.get(i).setServiceSerialNo(2);
		}else if(i==2){
			Date date= contract.getStartDate();
			 now.setTime(date);
    		 now.add(Calendar.DATE, 7);
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 3:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("Invoice Raised");
    		 mySerrviceList.get(i).setServiceSerialNo(3);
		}else if(i==3){
			Date date= contract.getStartDate();
			 now.setTime(date);
    		 now.add(Calendar.DATE, 18);
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 4:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("1st Training");
    		 mySerrviceList.get(i).setServiceSerialNo(4);
		}else if(i==4){
			 Date date= contract.getStartDate();
			 now.setTime(date);
    		 now.add(Calendar.DATE, 30);
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 5:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("2nd Training");
    		 mySerrviceList.get(i).setServiceSerialNo(5);
		}else if(i==5){
			Date date= contract.getStartDate();
			 now.setTime(date);
    		 now.add(Calendar.DATE, 45);
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 6:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("Inventory");
    		 mySerrviceList.get(i).setServiceSerialNo(6);
		}else if(i==6){
			Date date= contract.getStartDate();
			 now.setTime(date);
    		 now.add(Calendar.DATE, 60);
    		 Date newdt= now.getTime();
    		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		 System.out.println("Service Date 7:"+newdt);
    		 mySerrviceList.get(i).getProduct().setProductName("Go Live - Service Scheduling");
    		 mySerrviceList.get(i).setServiceSerialNo(7);
		}else if(i==7){
			Date date= contract.getStartDate();
			 now.setTime(date);
   		 now.add(Calendar.DATE, 60);
   		 Date newdt= now.getTime();
   		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
   		 System.out.println("Service Date 8:"+newdt);
   		 mySerrviceList.get(i).getProduct().setProductName("Go Live - Invoice");
   		 mySerrviceList.get(i).setServiceSerialNo(8);
		}else if(i==8){
			Date date= contract.getStartDate();
			 now.setTime(date);
  		 now.add(Calendar.DATE, 60);
  		 Date newdt= now.getTime();
  		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
  		 System.out.println("Service Date 9:"+newdt);
  		 mySerrviceList.get(i).getProduct().setProductName("Go Live - Inventory");
  		 mySerrviceList.get(i).setServiceSerialNo(9);
		}
		
		else if(i==9){
			Date date= contract.getStartDate();
			 now.setTime(date);
		 now.add(Calendar.DATE, 60);
		 Date newdt= now.getTime();
		 try {
					mySerrviceList.get(i).setServiceDate(sdfDate.parse(sdfDate.format(newdt)));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 System.out.println("Service Date 10:"+newdt);
		 mySerrviceList.get(i).getProduct().setProductName("Go Live - PO-GRN");
		 mySerrviceList.get(i).setServiceSerialNo(10);
		}
		else if(i==10){
			Date date= contract.getEndDate();
			 now.setTime(date);
			 mySerrviceList.get(i).getProduct().setProductName("Renewal");
			 mySerrviceList.get(i).setServiceDate(date);
			 mySerrviceList.get(i).setServiceSerialNo(11);
		}
		else if(i==11){
			Date date= contract.getEndDate();
			 now.setTime(date);
			 mySerrviceList.get(i).getProduct().setProductName("Tally-SMS");
			 mySerrviceList.get(i).setServiceDate(date);
			 mySerrviceList.get(i).setServiceSerialNo(12);
		}
		ofy().save().entity(mySerrviceList.get(i));
	}
	return true;
	}else{
		return false;
	}
	
}



/*
 *  nidhi 
 *  1-07-2017
 * 	method for service revanue report genrator report 	
 */

@Override
	public String serviceRevanueReport(Long companyId, Date startDate,Date endDate,String branchName) {
	String status = "";
//	try {
		Date startDate1 = null;

		startDate1 = DateUtility.getDateWithTimeZone("IST",(Date) startDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate1);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.HOUR, 00);
		startDate = calendar.getTime();

		startDate1 = DateUtility.getDateWithTimeZone("IST", (Date) endDate);
		calendar = Calendar.getInstance();
		calendar.setTime(startDate1);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.HOUR, 23);
		endDate = calendar.getTime();

		logger.log(Level.SEVERE, "From Date : " + startDate);
		logger.log(Level.SEVERE, "To Date : " + endDate);
		List<Service> custServiceList = ofy().load().type(Service.class)
				.filter("companyId", companyId)
				.filter("serviceDate >=", startDate)
				.filter("serviceDate <=", endDate)
				.filter("branch ", branchName)
				.filter("status", Service.SERVICESTATUSCOMPLETED)
				.list();
		
		logger.log(Level.SEVERE,"Service List Size  :  - " + custServiceList.size());
		
		List<Integer> serList = new ArrayList<Integer>();
		HashSet<Integer> contList = new HashSet<Integer>();
		HashSet<String> techniList = new HashSet<String>();
		
			if(custServiceList.size()>0){
				
			for(Service ser : custServiceList){
				serList.add(ser.getCount());
			}
			
			List<ServiceProject> custProjectList = ofy().load()
					.type(ServiceProject.class).filter("companyId", companyId)
					.filter("serviceId IN",serList)
					.filter("projectStatus", "Completed").list();
			
			logger.log(Level.SEVERE,"Service Project List Size  :  - " + custProjectList.size());
			
			/**
			 * @author Anil,Date  : 25-02-2019
			 * due to duplicate service id ,and branch is not stored in projecr entity,on above query we were loading other services project also which causes as data mapping issue
			 * this methods remove project which were not against the selected services
			 */
			removeDuplicateServiceProject(custProjectList,custServiceList);
			
			logger.log(Level.SEVERE,"Service Project List Size after removing duplicate service project :  - " + custProjectList.size());
			
			
			HashMap<Integer, String> serProContract = new HashMap<Integer,String>();
			List<Integer> proServiceId = new ArrayList<Integer>();
			
			ArrayList<ServiceRevanueReportBean> serRevList = null;
			
			if (custProjectList.size() > 0) {
				for (ServiceProject pro : custProjectList) {
					proServiceId.add(pro.getserviceId());
					contList.add(pro.getContractId());
					serProContract.put(pro.getCount(),pro.getContractId() + "-"+pro.getserviceId() );
					for (EmployeeInfo info : pro.getTechnicians()) {
						techniList.add(info.getFullName());
					}
				}
//				logger.log(Level.SEVERE,"get project combo --" + serProContract.toString());
				
//				logger.log(Level.SEVERE,"get project combo --" + serProContract);
				logger.log(Level.SEVERE,"Unique Services : " + serList.size());
				logger.log(Level.SEVERE,"Unique Contracts : " + contList.size());

			
				logger.log(Level.SEVERE,"Service List Size :  - " + custServiceList.size());

				/**
				 * @author Anil,Date : 25-02-2019
				 * Added branch filter for loading contract
				 */
				List<Contract> contDetails = ofy().load().type(Contract.class)
						.filter("companyId", companyId)
						.filter("branch", branchName)
						.filter("count IN", new ArrayList<Integer>(contList))
						.list();
				logger.log(Level.SEVERE,"Contract List Size  :  - " + contDetails.size());

				serRevList = new ArrayList<ServiceRevanueReportBean>();
				ServiceRevanueReportBean servBean;
				
				for (ServiceProject project : custProjectList) {
					
					Contract contract=null;
					Service service=null;
					
					 contract=getContractDetails(contDetails,project.getContractId());
//					 logger.log(Level.SEVERE,"get contract count --" + contract.getCount() + " ser count -- " + project.getserviceId());
//					 logger.log(Level.SEVERE,"get pro contract count --" + project.getContractId() + " project count -- " + project.getCount());
					 /**
					  * @author Anil,Date : 28-02-2019
					  * earlier contract count is passed from contract,if contract not found it throws null pointer exception
					  */
//					 service=getServiceDetails(custServiceList,project.getserviceId(),contract.getCount());
					 service=getServiceDetails(custServiceList,project.getserviceId(),project.getContractId());
//					 if(service!=null)
					 {
						for (EmployeeInfo technician : project.getTechnicians()) {
							servBean = new ServiceRevanueReportBean();
							try{
							
							servBean.setProjectId(project.getCount());
							servBean.setProjectName(project.getProjectName());
							servBean.setTechnicianName(technician.getFullName());
							
							servBean.setServiceId(service.getCount());
							servBean.setContractCount(service.getContractCount());
							servBean.setServiceDate(service.getServiceDate());
							if(service.getServiceCompletionDate()!=null){
								servBean.setServiceCompletionDate(service.getServiceCompletionDate());
							}
							servBean.setContEndDate(service.getContractEndDate());
							servBean.setContStartDate(service.getContractStartDate());
							
							
							servBean.setCustomerId(service.getPersonInfo().getCount());
							servBean.setCustName(service.getPersonInfo().getFullName());
							servBean.setProdDetails(service.getProduct().getProductName());
							
							servBean.setContractDate(contract.getContractDate());
							servBean.setContCategory(contract.getCategory());
							servBean.setContType(contract.getType());
							servBean.setCompBranch(contract.getBranch());
							
							
							
							if (contract.isContractRate() == true) {
								List<String> statusList=new ArrayList<String>();
								statusList.add(BillingDocument.APPROVED);
								statusList.add("Invoiced");
								/**
								 * Date : 24-10-2017 BY ANIL
								 * If billing is in created or requested state then also we have to calculate the amount
								 */
								statusList.add(Invoice.CREATED);
								statusList.add(Invoice.REQUESTED);
								
								BillingDocument billdetails = ofy().load().type(BillingDocument.class)
										.filter("companyId", companyId)
										.filter("rateContractServiceId",service.getCount())
										.filter("status IN",statusList).first().now();
								if(billdetails!=null){
									servBean.setProdPrice(billdetails.getTotalBillingAmount()/ project.getTechnicians().size());
								}else{
									servBean.setRemark("Billing document not found.");
								}
							} else {
								for (SalesLineItem pro : contract.getItems()) {
									if(service.getProduct().getCount()==pro.getPrduct().getCount()){
//										servBean.setProdPrice((getTotalPriceOfService(pro)/ pro.getNumberOfServices())/ project.getTechnicians().size());
//										servBean.setProdPrice((getTotalPriceOfService(pro)/(pro.getNumberOfServices()*pro.getQty()))/ project.getTechnicians().size());
										servBean.setProdPrice(service.getServiceValue()/project.getTechnicians().size());
										break;
									}
								}
							}
								}catch(Exception e){
							logger.log(Level.SEVERE,"Exception" + e);
							e.printStackTrace();
							String msg="";
							if(contract==null){
								msg=msg+"Contract not found. ";
							}
							if(service==null){
								msg=msg+" Service / service project  not found";
							}
							e.printStackTrace();
							logger.log(Level.SEVERE," get error --" + e.getMessage());
							servBean.setRemark("Error Fetching Record."+msg);
							servBean.setContractCount(project.getContractId());
							servBean.setServiceId(project.getserviceId());
						}
						serRevList.add(servBean);
	///
						}
					}
					 
				}
				
				logger.log(Level.SEVERE,"Tech Rev List Size " + serRevList.size());
				
				Comparator<ServiceRevanueReportBean> compare = new Comparator<ServiceRevanueReportBean>() {
					@Override
					public int compare(ServiceRevanueReportBean o1,ServiceRevanueReportBean o2) {
						if(o1.getTechnicianName()!=null&&o2.getTechnicianName()!=null){
							return o1.getTechnicianName().compareTo(o2.getTechnicianName());
						}
						return 0;
					}
				};
				Collections.sort(serRevList, compare);
				
				CsvWriter.technicianList = serRevList;
				status = "Success";
				
				
				////////////////////////////////////////////////////////////////////
				
			} else {
				status = "No customer projects were found for selected duration.";
			}
			
		}
		
		
//	} catch (Exception e) {
//		e.printStackTrace();
//		logger.log(Level.SEVERE," get error --" + e.getMessage());
//		System.out.println(e.getMessage());
//	}
	return status;
	}


private void removeDuplicateServiceProject(List<ServiceProject> custProjectList,List<Service> custServiceList) {
	for(Service service:custServiceList){
		for(int i=0;i<custProjectList.size();i++){
			ServiceProject project=custProjectList.get(i);
			if(service.getCount()==project.getserviceId()){
				if(service.getContractCount()!=project.getContractId()){
					custProjectList.remove(i);
					i--;
				}
			}
		}
	}
}

public ServiceRevanueReportBean getInvoiceDetailList(List<Invoice> invlist,int contractID){
	ServiceRevanueReportBean serDetail = null;
	try {
		int invcount = 0;
		double total = 0;
		StringBuffer strInv = new StringBuffer();
		
		for(Invoice indetail : invlist){
			if(contractID == indetail.getContractCount())
			{	
				if(serDetail ==  null){
					serDetail = new ServiceRevanueReportBean();
					
				}
				total = total + indetail.getNetPayable();
				strInv.append(indetail.getCount() + "/");
				invcount++;
			}
		}
		//strInv.substring(0, strInv.length() - 1).toString();
		if(serDetail!=null){
			serDetail.setInvDetails(strInv.substring(0, strInv.length() - 1).toString());
			serDetail.setInvCont(invcount);
			serDetail.setActualAmt(total);
//			System.out.println("get invoice  : "+serDetail.getInvDetails() );
		}
		
	} catch (Exception e) {
		System.out.println(e.getMessage());
	}
	return serDetail;
}



/*
 * end
 */




	/*
	 *  10-07-2017
	 *   nidhi
	 *   	method for technician revenue report
	 *   
	 */
		@Override
		public String technicianRevanueReport(Long companyId, Date startDate,Date endDate,String branchName) {
			String status = "";
//			try {
				Date startDate1 = null;

				startDate1 = DateUtility.getDateWithTimeZone("IST",(Date) startDate);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(startDate1);
				calendar.set(Calendar.SECOND, 00);
				calendar.set(Calendar.MINUTE, 00);
				calendar.set(Calendar.HOUR, 00);
				startDate = calendar.getTime();

				startDate1 = DateUtility.getDateWithTimeZone("IST", (Date) endDate);
				calendar = Calendar.getInstance();
				calendar.setTime(startDate1);
				calendar.set(Calendar.SECOND, 59);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.HOUR, 23);
				endDate = calendar.getTime();

				logger.log(Level.SEVERE, "From Date : " + startDate);
				logger.log(Level.SEVERE, "To Date : " + endDate);
				List<Service> servDetails = ofy().load().type(Service.class)
						.filter("companyId", companyId)
						.filter("serviceDate >=", startDate)
						.filter("serviceDate <=", endDate)
						.filter("branch ", branchName)
						.filter("status", Service.SERVICESTATUSCOMPLETED)
						.list();
				List<Integer> serList = new ArrayList<Integer>();
				HashSet<Integer> contList = new HashSet<Integer>();
				HashSet<String> techniList = new HashSet<String>();
				
				HashMap<Integer, Integer> serContract = new HashMap<Integer,Integer>();
 				if(servDetails.size()>0){
					for(Service ser : servDetails){
						serList.add(ser.getCount());
						serContract.put(ser.getCount(),ser.getContractCount());
					}
					
					logger.log(Level.SEVERE,"get ser contract hash map --" + serContract.toString());
					logger.log(Level.SEVERE,"get ser combo --" + serContract);
					List<ServiceProject> custProDetails = ofy().load()
							.type(ServiceProject.class).filter("companyId", companyId)
							.filter("serviceId IN",serList)
							.filter("projectStatus", "Completed").list();
					
					List<ServiceProject> checkedSerPro = new ArrayList<ServiceProject>();

					for(Service ser : servDetails){
						for(ServiceProject serPr : custProDetails){
							if(ser.getCount() == serPr.getserviceId() && ser.getCount()== serPr.getserviceId()){
								
							}
						}
					}
					
					logger.log(Level.SEVERE,"Service Project List Size  :  - " + custProDetails.size());
					HashMap<Integer, String> serProContract = new HashMap<Integer,String>();
					List<Integer> proServiceId = new ArrayList<Integer>();
					
					ArrayList<ServiceRevanueReportBean> serRevList = null;
					if (custProDetails.size() > 0) {
						for (ServiceProject pro : custProDetails) {
							proServiceId.add(pro.getserviceId());
							contList.add(pro.getContractId());
							serProContract.put(pro.getCount(),pro.getContractId() + "-"+pro.getserviceId() );
							for (EmployeeInfo info : pro.getTechnicians()) {
								techniList.add(info.getFullName());
							}
						}
						logger.log(Level.SEVERE,"get project combo --" + serProContract.toString());
						
						logger.log(Level.SEVERE,"get project combo --" + serProContract);
						logger.log(Level.SEVERE,"Unique Services : " + serList.size());
						logger.log(Level.SEVERE,"Unique Contracts : " + contList.size());

					
						logger.log(Level.SEVERE,"Service List Size :  - " + servDetails.size());

						List<Contract> contDetails = ofy().load().type(Contract.class)
								.filter("companyId", companyId)
								.filter("count IN", new ArrayList<Integer>(contList))
								.list();
						logger.log(Level.SEVERE,"Contract List Size  :  - " + contDetails.size());

						serRevList = new ArrayList<ServiceRevanueReportBean>();
						ServiceRevanueReportBean servBean;
						
						for (ServiceProject project : custProDetails) {
							
							Contract contract=null;
							Service service=null;
							
							 contract=getContractDetails(contDetails,project.getContractId());
							 logger.log(Level.SEVERE,"get contract count --" + contract.getCount() + " ser count -- " + project.getserviceId());
							 logger.log(Level.SEVERE,"get pro contract count --" + project.getContractId() + " project count -- " + project.getCount());
							 service=getServiceDetails(servDetails,project.getserviceId(),contract.getCount());
							
//							 if(service!=null)
							 {
								for (EmployeeInfo technician : project.getTechnicians()) {
									servBean = new ServiceRevanueReportBean();
									try{
									
									servBean.setProjectId(project.getCount());
									servBean.setProjectName(project.getProjectName());
									servBean.setTechnicianName(technician.getFullName());
									
									servBean.setServiceId(service.getCount());
									servBean.setContractCount(service.getContractCount());
									servBean.setServiceDate(service.getServiceDate());
									if(service.getServiceCompletionDate()!=null){
										servBean.setServiceCompletionDate(service.getServiceCompletionDate());
									}
									servBean.setContEndDate(service.getContractEndDate());
									servBean.setContStartDate(service.getContractStartDate());
									
									
									servBean.setCustomerId(service.getPersonInfo().getCount());
									servBean.setCustName(service.getPersonInfo().getFullName());
									servBean.setProdDetails(service.getProduct().getProductName());
									
									servBean.setContractDate(contract.getContractDate());
									servBean.setContCategory(contract.getCategory());
									servBean.setContType(contract.getType());
									servBean.setCompBranch(contract.getBranch());
									
									
									
									if (contract.isContractRate() == true) {
										List<String> statusList=new ArrayList<String>();
										statusList.add(BillingDocument.APPROVED);
										statusList.add("Invoiced");
										/**
										 * Date : 24-10-2017 BY ANIL
										 * If billing is in created or requested state then also we have to calculate the amount
										 */
										statusList.add(Invoice.CREATED);
										statusList.add(Invoice.REQUESTED);
										
										BillingDocument billdetails = ofy().load().type(BillingDocument.class)
												.filter("companyId", companyId)
												.filter("rateContractServiceId",service.getCount())
												.filter("status IN",statusList).first().now();
										if(billdetails!=null){
											servBean.setProdPrice(billdetails.getTotalBillingAmount()/ project.getTechnicians().size());
										}else{
											servBean.setRemark("Billing document not found.");
										}
									} else {
										for (SalesLineItem pro : contract.getItems()) {
											if(service.getProduct().getCount()==pro.getPrduct().getCount()){
//												servBean.setProdPrice((getTotalPriceOfService(pro)/ pro.getNumberOfServices())/ project.getTechnicians().size());
//												servBean.setProdPrice((getTotalPriceOfService(pro)/(pro.getNumberOfServices()*pro.getQty()))/ project.getTechnicians().size());
												servBean.setProdPrice(service.getServiceValue()/project.getTechnicians().size());
												break;
											}
										}
									}
										}catch(Exception e){
									logger.log(Level.SEVERE,"Exception" + e);
									e.printStackTrace();
									String msg="";
									if(contract==null){
										msg=msg+"Contract not found. ";
									}
									if(service==null){
										msg=msg+" Service / service project  not found";
									}
									e.printStackTrace();
									logger.log(Level.SEVERE," get error --" + e.getMessage());
									servBean.setRemark("Error Fetching Record."+msg);
									servBean.setContractCount(project.getContractId());
									servBean.setServiceId(project.getserviceId());
								}
								serRevList.add(servBean);
			///
								}
							}
							 
						}
						
						logger.log(Level.SEVERE,"Tech Rev List Size " + serRevList.size());
						
						Comparator<ServiceRevanueReportBean> compare = new Comparator<ServiceRevanueReportBean>() {
							@Override
							public int compare(ServiceRevanueReportBean o1,ServiceRevanueReportBean o2) {
								if(o1.getTechnicianName()!=null&&o2.getTechnicianName()!=null){
									return o1.getTechnicianName().compareTo(o2.getTechnicianName());
								}
								return 0;
							}
						};
						Collections.sort(serRevList, compare);
						
						CsvWriter.technicianList = serRevList;
						status = "Success";
						
						
						////////////////////////////////////////////////////////////////////
						
					} else {
						status = "No customer projects were found for selected duration.";
					}
					
				}
				
				
//			} catch (Exception e) {
//				e.printStackTrace();
//				logger.log(Level.SEVERE," get error --" + e.getMessage());
//				System.out.println(e.getMessage());
//			}
			return status;
		}

private Service getServiceDetails(List<Service> servDetails,Integer getserviceId,int contractId) {
	for(Service object:servDetails){
		if(object.getCount()==getserviceId && object.getContractCount()==contractId){
			return object;
		}
	}
	return null;
}

private Contract getContractDetails(List<Contract> contDetails, int contractId) {
	for(Contract object:contDetails){
		if(object.getCount()==contractId ){
			return object;
		}
	}
	return null;
}

/**
 * Date:19-07-2017 By Anil
 * These methods are added to calculate total amount of any service given in contract.
 * we do this because we havnt stored the total amount.
 * @param object
 * @return
 */

public double getTotalPriceOfService(SalesLineItem object){
	
	double total=0;
		try {
			SuperProduct product = object.getPrduct();
			double tax = removeAllTaxes(product);
			double origPrice = object.getPrice() - tax;

			if (object.getArea().equals("") || object.getArea().equals("0")) {
				object.setArea("NA");
			}
			if ((object.getPercentageDiscount() == null && object.getPercentageDiscount() == 0)
					&& (object.getDiscountAmt() == 0)) {
				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
				} else {
					total = origPrice;
				}
			}else if ((object.getPercentageDiscount() != null)&& (object.getDiscountAmt() != 0)) {
				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
					total = total- (total * object.getPercentageDiscount() / 100);
					total = total - object.getDiscountAmt();
				} else {
					total = origPrice;
					total = total- (total * object.getPercentageDiscount() / 100);
					total = total - object.getDiscountAmt();
				}
			} else {
				if (object.getPercentageDiscount() != null|| object.getPercentageDiscount() != 0) {
					if (!object.getArea().equalsIgnoreCase("NA")) {
						double area = Double.parseDouble(object.getArea());
						total = origPrice * area;
						total = total- (total * object.getPercentageDiscount() / 100);
					} else {
						total = origPrice;
						total = total- (total * object.getPercentageDiscount() / 100);
					}
				} else {
					if (!object.getArea().equalsIgnoreCase("NA")) {
						double area = Double.parseDouble(object.getArea());
						total = origPrice * area;
						total = total - object.getDiscountAmt();
					} else {
						total = total - object.getDiscountAmt();
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	return total;
}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			if (entity instanceof ItemProduct) {
				ItemProduct prod = (ItemProduct) entity;
				if (prod.getServiceTax().getTaxPrintName() != null
						&& !prod.getServiceTax().getTaxPrintName().equals("")
						&& prod.getVatTax().getTaxPrintName() != null
						&& !prod.getVatTax().getTaxPrintName().equals("")) {

					double dot = service + vat;
					retrServ = (entity.getPrice() / (1 + dot / 100));
					retrServ = entity.getPrice() - retrServ;
				} else {
					double removeServiceTax = (entity.getPrice() / (1 + service / 100));
					retrServ = (removeServiceTax / (1 + vat / 100));
					retrServ = entity.getPrice() - retrServ;
				}
			}

			if (entity instanceof ServiceProduct) {
				ServiceProduct prod = (ServiceProduct) entity;
				if (prod.getServiceTax().getTaxPrintName() != null
						&& !prod.getServiceTax().getTaxPrintName().equals("")
						&& prod.getVatTax().getTaxPrintName() != null
						&& !prod.getVatTax().getTaxPrintName().equals("")) {
					double dot = service + vat;
					retrServ = (entity.getPrice() / (1 + dot / 100));
					retrServ = entity.getPrice() - retrServ;
				} else {
					double removeServiceTax = (entity.getPrice() / (1 + service / 100));
					retrServ = (removeServiceTax / (1 + vat / 100));
					retrServ = entity.getPrice() - retrServ;
				}
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}
	
	/*
	 *  end
	 */
	
	
	@Override
	public ArrayList<ServiceSchedule> getScheduleServiceSearchResult(Long companyId, String documnetType, int documentId) {
		logger.log(Level.SEVERE,"COM_ID "+companyId+" DOC_TYPE "+documnetType+" DOC_ID "+documentId);
		ArrayList<ServiceSchedule> list=new ArrayList<ServiceSchedule>();
		ofy().clear();
		List<ServiceSchedule> schList=ofy().load().type(ServiceSchedule.class).filter("companyId", companyId).filter("documentType", documnetType).filter("documentId", documentId).list();
		
		if(schList!=null){
			logger.log(Level.SEVERE,"SER_SCH LIST SIZE : "+schList.size());
			list.addAll(schList);
		}
		logger.log(Level.SEVERE,"LIST SIZE "+list.size());
		
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
	    ofy().clear();
	    
		return list;
	}

	@Override
	public void updateScheduleService(String operation,ArrayList<ServiceSchedule> serSchList) {
		ofy().clear();
		logger.log(Level.SEVERE,"LIST SIZE "+serSchList.size());
		if(operation.equalsIgnoreCase("Delete")){
			logger.log(Level.SEVERE,"DELETE OPERATION");
			ofy().delete().entities(serSchList).now();
		}else if(operation.equalsIgnoreCase("Save")){
			logger.log(Level.SEVERE,"SAVE OPERATION");
			ofy().save().entities(serSchList).now();
		}
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
	    ofy().clear();
	    logger.log(Level.SEVERE,"Updated Successfully!!!");
	    
	}

	@Override
	public void deleteSchServices(Long companyId) {
		ofy().clear();
		List<ServiceSchedule> schList=ofy().load().type(ServiceSchedule.class).filter("companyId", companyId).list();
		if(schList!=null){
			logger.log(Level.SEVERE,"SER_SCH LIST SIZE : "+schList.size());
			ofy().delete().entities(schList).now();
		}
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
	    ofy().clear();
		 logger.log(Level.SEVERE,"Delete Successfully!!!");
	}
	
	/*
	 *  end
	 */
	/** Date 15.03.2018 added by komal for amc assured revenue report **/
	@Override
	public ArrayList<AmcAssuredRevenueReport> getAmcAssuredRevenueReport(
			Long companyId, Date date, String branch) {
		// TODO Auto-generated method stub
		ArrayList<AmcAssuredRevenueReport> amcAssuredList = new ArrayList<AmcAssuredRevenueReport>();
		try {
			logger.log(Level.SEVERE,"get method service .. " + date);
			Date startDate1 = null;
			startDate1 = DateUtility.getDateWithTimeZone("IST",(Date) date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate1);
			calendar.set(Calendar.SECOND, 00);
			calendar.set(Calendar.MINUTE, 00);
			calendar.set(Calendar.HOUR, 00);
			date = calendar.getTime();
			int dateYear = calendar.get(Calendar.YEAR);
			int dateMonth = calendar.get(Calendar.MONTH)+1;
			
			Date startDate2 = null;
			startDate2 = DateUtility.getDateWithTimeZone("IST",(Date) new Date());
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(startDate2);
			calendar1.set(Calendar.DAY_OF_MONTH, 31);
			calendar1.set(Calendar.MONTH, 2);
			calendar1.set(Calendar.YEAR, dateYear+1);
			
			calendar1.set(Calendar.SECOND, 59);
			calendar1.set(Calendar.MINUTE, 59);
			calendar1.set(Calendar.HOUR, 23);
            Date date1 = calendar1.getTime();
            
//			logger.log(Level.SEVERE,"From Date : " + date);
//			List<Contract> contDetails = new ArrayList<Contract>();
//				contDetails = ofy().load().type(Contract.class)
//						.filter("companyId", companyId)
//						.filter("startDate >=", date)
//						.filter("branch", branch)
//						.filter("status", "Approved").list();
            /**
             * Date 13-06-2019 by vijay
             * Des :- above query is the wrong report must be based on active contracts so updated the query below
             */

			List<Contract> contDetails = new ArrayList<Contract>();
			contDetails = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("endDate >=", date)
					.filter("branch", branch)
					.filter("status", "Approved").list();
			
			HashSet<Integer> contractIdList = new HashSet<Integer>();
			/**
			 * nidhi
			 * 29-11-2018
			 */
			ArrayList<Contract> contractList  = new ArrayList<Contract>();
		   for(Contract c : contDetails){
			   if(!(c.isServiceWiseBilling()) && !(c.isContractRate())){
				   contractIdList.add(c.getCount());
				   contractList.add(c);
			   }   
		   }
				
			logger.log(Level.SEVERE,"cont details - " + contractList.size());
			
			Calendar annualCal = Calendar.getInstance();
		
			for(Contract contract : contractList){
				logger.log(Level.SEVERE,"cont details - " + contract.getCount());
				AmcAssuredRevenueReport amcAssuredRevenue = new AmcAssuredRevenueReport();
				amcAssuredRevenue.setCount(dateYear);
				amcAssuredRevenue.setBranch(contract.getBranch());
				amcAssuredRevenue.setCustName(contract.getCinfo().getFullName());
				amcAssuredRevenue.setContractCount(contract.getCount());
				if(contract.getType()!=null)
				amcAssuredRevenue.setContractType(contract.getType());
				amcAssuredRevenue.setContStartDate(contract.getStartDate());
				amcAssuredRevenue.setContEndDate(contract.getEndDate());
				amcAssuredRevenue.setCompanyId(contract.getCompanyId());
				amcAssuredRevenue.setContractValue(contract.getNetpayable());
				
				/*** Date 13-06-2019 by Vijay for NBHC CCPM selected Date month onwards report to display Data ****/
				amcAssuredRevenue.setMonth(dateMonth+"");
				
				Date startDate= DateUtility.getDateWithTimeZone("IST",(Date) contract.getStartDate());
				Date endDate= DateUtility.getDateWithTimeZone("IST",(Date) contract.getEndDate());
				HashMap<String , Double> map = new HashMap<String, Double>();
				int month  ;
				
				int diffMonth = getMonthDiff(contract.getStartDate(),contract.getEndDate());
				logger.log(Level.SEVERE,"get month diff " + diffMonth);
				double monthAmt =0;
				if(contract.getNetpayable()!=0 && diffMonth!=0){
					
					 monthAmt = contract.getNetpayable()/diffMonth;
					
//					 DecimalFormat dc = new DecimalFormat();
////					 dc.setMinimumFractionDigits(2);
//				        NumberFormat nf = NumberFormat.getInstance();
//				        nf.setMaximumFractionDigits(2);
//				        
//				        monthAmt = Double.parseDouble(dc.format(monthAmt));
				        
				        NumberFormat fmt = NumberFormat.getNumberInstance();
				        fmt.setMaximumFractionDigits(2);
				        fmt.setRoundingMode(RoundingMode.CEILING);

				        
				        logger.log(Level.SEVERE," before get amout -- "+fmt.format(monthAmt));
				        String value = fmt.format(monthAmt).replace(",", "");
				        monthAmt = Double.parseDouble(value);
				        logger.log(Level.SEVERE,"get amout -- "+value);

				}else{
					monthAmt = contract.getNetpayable();
				}
				
				breakLoop:
				do {
					month = getCalendar(startDate).get(Calendar.MONTH)+1;
					 annualCal.setTime(startDate);
					 annualCal.add(Calendar.MONTH, 1);
					
					startDate = annualCal.getTime();
					if(map.containsKey(month+"")){
						map.put(month+"", map.get(month+"") + monthAmt);
					}else{
						map.put(month+"", monthAmt);
					}
					if(startDate.after(endDate)){
						break breakLoop;
					}
				} while (startDate.before(date1));
				
				amcAssuredRevenue.setServiceValueMap(map);
				amcAssuredList.add(amcAssuredRevenue);
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		CsvWriter.amcAssuredRevenueReportList = amcAssuredList;
		return amcAssuredList;
	
	
	}
	
	int getMonthDiff(Date start,Date end){
		Calendar startCalendar = Calendar.getInstance();
		
		startCalendar.setTime(DateUtility.getDateWithTimeZone("IST",(Date) start));
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(DateUtility.getDateWithTimeZone("IST",(Date) end));

		int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int diffMonth = endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		int diffDay = endCalendar.get(Calendar.DAY_OF_MONTH) - startCalendar.get(Calendar.DAY_OF_MONTH);
		


        Calendar startcal = Calendar.getInstance();
        startcal.setTime(start);

        Calendar endcal = Calendar.getInstance();
        endcal.setTime(end);

          int monthsBetween = 0;
            int dateDiff = endcal.get(Calendar.DAY_OF_MONTH)-startcal.get(Calendar.DAY_OF_MONTH);      

            if(dateDiff<0) {
                int borrrow = endcal.getActualMaximum(Calendar.DAY_OF_MONTH);           
                 dateDiff = (endcal.get(Calendar.DAY_OF_MONTH)+borrrow)-startcal.get(Calendar.DAY_OF_MONTH);
                 monthsBetween--;

                 if(dateDiff>0){
                     monthsBetween++;
                 }
            }
            else {
                monthsBetween++;
            }      
            monthsBetween += endcal.get(Calendar.MONTH)-startcal.get(Calendar.MONTH);      
            monthsBetween  += (endcal.get(Calendar.YEAR)-startcal.get(Calendar.YEAR))*12;      
            return monthsBetween;
		
	}
	/** date 16.03.2018 added by komal **/
	 private Calendar getCalendar(Date date){
		 Date startDate1 = null;
			startDate1 = DateUtility.getDateWithTimeZone("IST",(Date) date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate1);
			calendar.set(Calendar.SECOND, 00);
			calendar.set(Calendar.MINUTE, 00);
			calendar.set(Calendar.HOUR, 00);
			return calendar; 
	 }
	 /** date 16.03.2018 added by komal **/
	 private ArrayList<Service> getServiceDetails(List<Contract> contractList , List<Service> servDetails,int contractId) {
		 ArrayList<Service> list = new ArrayList<Service>();
			for(Service object:servDetails){
				if(object.getContractCount()==contractId){
					if(object.getServiceValue()==0){
						double serviceValue = 0;
						Contract contract = null;
						for(Contract c : contractList){
							if(contractId == c.getCount()){
								contract = c;
								break;
							}
						}
								//ofy().load().type(Contract.class).filter("companyId", object.getCompanyId()).filter("count", contractId).first().now();
						for(SalesLineItem item : contract.getItems()){
							if(item.getProductCode().equalsIgnoreCase(object.getProduct().getProductCode())){
								if(item.getTotalAmount()==0){
									double qty = 1 , unit = 1 , totalAmount = 0;
									if(item.getQty()>0){
										qty = item.getQty();
									}
									if(item.getArea()!=null && item.getArea().trim().matches("[0-9.]*")){
										unit = Double.parseDouble(item.getArea().trim());
									}
									totalAmount = item.getPrice() * unit ;
									serviceValue = totalAmount  /(item.getNumberOfServices() * qty);
									logger.log(Level.SEVERE,"serviceValue16="+serviceValue);
									
								}else{
									serviceValue=item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
									logger.log(Level.SEVERE,"serviceValue17="+serviceValue);
								}
								break;
							}
						}
						object.setServiceValue(serviceValue);
					}
					list.add(object);
				}
			}
			if(list.size()!=0){
			Comparator<Service> comp = new Comparator<Service>(){

				@Override
				public int compare(Service e1, Service e2) {
					// TODO Auto-generated method stub
					 if(e1.getServiceDate() == null && e2.getServiceDate()==null){
					        return 0;
					    }
					    if(e1.getServiceDate() == null) return 1;
					    if(e2.getServiceDate() == null) return -1;
					    
					    return e1.getServiceDate().compareTo(e2.getServiceDate());
				}					
			};	
				Collections.sort(list, comp);
			}

			return list;
		}

	/** Date 30-05-2018 By vijay for updating services branch form customer branch servicing branch ***/

	@Override
	public boolean updateServices(Contract contract) {

		List<Service> servicelist = ofy().load().type(Service.class)
				.filter("companyId", contract.getCompanyId())
				.filter("contractCount", contract.getCount()).list();
		
		List<CustomerBranchDetails> customerbranchlist = ofy().load().type(CustomerBranchDetails.class).filter("companyId", contract.getCompanyId()).filter("cinfo.count", contract.getCinfo().getCount()).list();
		
		if(customerbranchlist.size()!=0 && servicelist.size()!=0){
			for(Service service : servicelist){
				for(CustomerBranchDetails customerBranch : customerbranchlist){
					if(!service.getServiceBranch().equals("Service Address")){
						if(service.getServiceBranch().equals(customerBranch.getBusinessUnitName())){
							service.setBranch(customerBranch.getBranch());
						}
					}
				}
			}
			 ofy().save().entities(servicelist);
		}

		return true;
	}


	@Override
	public ArrayList<RenewalResult> contractRenewalList(ArrayList<Filter> filter,
			ArrayList<Filter> filter1) {


		System.out.println("Inside CONTRACT  Renewal......");
		
		Logger logger = Logger.getLogger("Inside CONTRACT  Renewal......");
		
		
		ArrayList<RenewalResult> renewalList = new ArrayList<RenewalResult>();
		
		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Contract());
		if (filter.size() != 0) {
			querry.getFilters().addAll(filter);
		}

		GenricServiceImpl impl = new GenricServiceImpl();
		List<SuperModel> modelLis = impl.getSearchResult(querry);
		System.out.println("Result SIZE.. "+modelLis.size());
		
		logger.log(Level.SEVERE,"Renewal Result SIZE.. "+modelLis.size());
		
		for (SuperModel temp : modelLis) {
			
			
			
			RenewalResult result = new RenewalResult();
			Contract entity = (Contract) temp;
			
			logger.log(Level.SEVERE,"get entity flag -- "+entity.isRenewFlag() +" count --   " + entity.getRefContractCount()) ;
			if((!entity.isRenewFlag() && (entity.getRefContractCount()!=null && entity.getRefContractCount()<=0))
					|| (entity.isRenewFlag() && (entity.getRefContractCount()!=null || entity.getRefContractCount()>0))){
				logger.log(Level.SEVERE,"get in condition .. ");
				continue;
				
			}
			
			
			result.setContractId(entity.getCount());
			result.setContractDate(entity.getContractDate());
			result.setContractEndDate(entity.getEndDate());
			result.setBranch(entity.getBranch());
			result.setSalesPerson(entity.getEmployee());
				
			result.setCustomerId(entity.getCinfo().getCount());
			result.setCustomerName(entity.getCinfo().getFullName());
			result.setCustomerCell(entity.getCinfo().getCellNumber());
				
				
			for(int i=0;i<entity.getItems().size();i++){
				entity.getItems().get(i).setOldProductPrice(entity.getItems().get(i).getPrice());
			}
			result.setItems(entity.getItems());
			
			result.setYear("");
			result.setStatus(entity.getStatus());
			result.setDate(entity.getCreationDate());
			result.setRenewProcessed(entity.getRenewProcessed());
			
			/**
			 * Date 11 April 2017 added by vijay requirement from PCAAMB for contract amount and contract type
			 */
			result.setNetPayable(entity.getNetpayable());
			result.setContractType(entity.getType());
			/**
			 * nidhi 20-12-2018
			 */
			result.setRefContractCount(entity.getRefContractCount());
			result.setCompanyId(entity.getCompanyId());
			logger.log(Level.SEVERE,"get cont -- "  + result.getContractId() + " ref count -- " + result.getRefContractCount());
			renewalList.add(result);
			
		}
		
		logger.log(Level.SEVERE,"CONTRACT RENEWAL LIST SIZE FROM (CONTRACT) :: "+renewalList.size());
		System.out.println("CONTRACT RENEWAL LIST SIZE FROM (CONTRACT) :: "+renewalList.size());
		
		///////////////////////////////
		
	
		return renewalList;
	}


	@Override
	public ArrayList<RenewalResult> contractRenewalReeport(
			ArrayList<RenewalResult> contList) {

		HashSet<Integer> conList = new HashSet<Integer>();
		
		if(contList.size()>0){
			for(RenewalResult rn : contList){
				conList.add(rn.getRefContractCount());
			}
			logger.log(Level.SEVERE,"get cont List -- " + conList.size());
			logger.log(Level.SEVERE,"get con dyt -- "  + conList.toString());
		}
		
		if(conList.size()>0){
			ArrayList<Integer> con = new ArrayList<Integer>();
			
			con.addAll(conList);
			
			List<Contract> contDtList = ofy().load().type(Contract.class)
					.filter("companyId", contList.get(0).getCompanyId())
					.filter("count IN",conList).list();
			
			
			if(contDtList!=null && contDtList.size()>0){
				for(int i =0 ;i<contList.size();i++){
					for(Contract contract : contDtList){
						if(contList.get(i).getRefContractCount() == contract.getCount() && contract.getCustomerId() == contList.get(i).getCustomerId()){
							logger.log(Level.SEVERE,"get contract "+ contract.getCount());
							contList.get(i).setOldContractValue(contract.getNetpayable());
							double ince  = (contList.get(i).getNetPayable()*100)/contract.getNetpayable();
							DecimalFormat df = new DecimalFormat("#.00");
							if(ince!=0)
							ince = Double.parseDouble(df.format(ince));
							ince = ince - 100;
							/*if(contList.get(i).getNetPayable() < contract.getNetpayable() && ince!=0 ){
								ince = ince * -1;
							}*/
							contList.get(i).setAvgValue(ince);
							logger.log(Level.SEVERE,"get contract count 0-- " + contList.get(i).getContractId() + " ince -- " +ince);
						}
					}
					
				}
			}
		}
		
		CsvWriter.renewalContractDt.clear();
		CsvWriter.renewalContractDt.addAll(contList);
		return contList;
	}

	/*
	 *  end
	 */
	
	/**
	 * @author Anil
	 * @since 06-06-2020
	 * recalculating total service based on asset qty for premium tech  requirement
	 * @param items
	 * @return
	 */
	public double getTotalNoOfServices(List<SalesLineItem> items) {
		// TODO Auto-generated method stub
		int sum=0;
		boolean assetFlag=false;
		for(SalesLineItem item:items){
			assetFlag=false;
			ArrayList<BranchWiseScheduling> branchWiseSchedulingList=null;
			if(item.getCustomerBranchSchedulingInfo()!=null){
				branchWiseSchedulingList=item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
			}
			
			if(branchWiseSchedulingList!=null){
				for(BranchWiseScheduling branch:branchWiseSchedulingList){
					if(branch.isCheck()&&branch.getAssetQty()!=0){
						assetFlag=true;
						sum=(int) (sum+(item.getNumberOfServices()*branch.getAssetQty()));
					}
				}
				if(!assetFlag){
					sum=(int) (sum+(item.getNumberOfServices()*item.getQty()));
				}
			}
			else{
				//Normal calculation comes here
				sum=(int) (sum+(item.getNumberOfServices()*item.getQty()));
			}
		}
		return sum;
	}
	
	private void generateAndUpdateAssetNum(List<SalesLineItem> items,long companyId) {
		// TODO Auto-generated method stub
		int sum=0;
		for(SalesLineItem item:items){
			ArrayList<BranchWiseScheduling> branchWiseSchedulingList=null;
			if(item.getCustomerBranchSchedulingInfo()!=null){
				branchWiseSchedulingList=item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
			}
			
			if(branchWiseSchedulingList!=null){
				for(BranchWiseScheduling branch:branchWiseSchedulingList){
					if(branch.isCheck()&&branch.getAssetQty()!=0){
						sum=sum+branch.getAssetQty();
					}
				}
			}
		}
		int assetCount=0;
		try{
			SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
			assetCount=(int) numGen.getLastUpdatedNumber("AssetCount", companyId, (long) sum);
		}catch(Exception e){
			
		}
		logger.log(Level.SEVERE,"COMP totalservices assetCount "+assetCount);
		if(assetCount!=0){
			for(SalesLineItem item:items){
				ArrayList<BranchWiseScheduling> branchWiseSchedulingList=null;
				if(item.getCustomerBranchSchedulingInfo()!=null){
					branchWiseSchedulingList=item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
				}
				
				if(branchWiseSchedulingList!=null){
					for(BranchWiseScheduling branch:branchWiseSchedulingList){
						if(branch.isCheck()&&branch.getAssetQty()!=0){
							if(branch.getComponentList()!=null&&branch.getComponentList().size()!=0){
								for(ComponentDetails obj:branch.getComponentList()){
									obj.setAssetId(assetCount);
									assetCount++;
								}
							}
						}
					}
					item.getCustomerBranchSchedulingInfo().put(item.getProductSrNo(), branchWiseSchedulingList);
				}
			}
		}
	}
	
	private int getAssetId(SalesLineItem item, ServiceSchedule ss) {
		// TODO Auto-generated method stub
		ArrayList<BranchWiseScheduling> branchWiseSchedulingList=null;
		if(item.getCustomerBranchSchedulingInfo()!=null){
			branchWiseSchedulingList=item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
		}
		
		if(branchWiseSchedulingList!=null){
			for(BranchWiseScheduling branch:branchWiseSchedulingList){
				if(branch.isCheck()&&branch.getAssetQty()!=0){
					if(branch.getBranchName().equals(ss.getScheduleProBranch())){
						if(branch.getComponentList()!=null&&branch.getComponentList().size()!=0){
							for(ComponentDetails obj:branch.getComponentList()){
	//							if(obj.getComponentName()!=null&&ss.getComponentName()!=null
	//									&&obj.getComponentName().equals(ss.getComponentName())
	//									&&obj.getMfgNum().equals(ss.getMfgNum())){
	//								return obj.getAssetId();
	//							}else if(obj.getMfgNum().equals(ss.getMfgNum())){
	//								return obj.getAssetId();
	//							} 
								
								if(obj.getSrNo()==ss.getSrNo()){
									logger.log(Level.SEVERE,"8getAssetId "+obj.getAssetId());
									return obj.getAssetId();
								}
							}
						}
					}
				}
			}
		}
	
		return 0;
	}

	public boolean checkAssetQty(List<SalesLineItem> items) {
		// TODO Auto-generated method stub
		int sum=0;
		boolean assetQtyFlag = false;
		for(SalesLineItem item:items){
			ArrayList<BranchWiseScheduling> branchWiseSchedulingList=null;
			if(item.getCustomerBranchSchedulingInfo()!=null){
				branchWiseSchedulingList=item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
			}
			
			if(branchWiseSchedulingList!=null){
				for(BranchWiseScheduling branch:branchWiseSchedulingList){
					if(branch.isCheck()&&branch.getAssetQty()!=0){
						assetQtyFlag = true;
						sum=(int) (sum+(item.getNumberOfServices()*branch.getAssetQty()));
					}
				}
			}
		}
		return assetQtyFlag;
	}

}
