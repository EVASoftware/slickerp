package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.contract.UpdatePricesService;
import com.slicktechnologies.client.views.device.CreateSingleServiceService;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class CreateSingleServiceImpl extends RemoteServiceServlet implements
CreateSingleServiceService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8571763917722344787L;

	Logger logger = Logger.getLogger("CreateSingleServiceImpl.class");

	@Override
	public ArrayList<Integer> createSingleService(int serviceCount,
			Date previousDate, int serviceNo, int serviceSerialNo,
			int contractId, long companyId) {
		// TODO Auto-generated method stub
		ArrayList<Integer> intValue=new ArrayList<Integer>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		Contract contract = ofy().load().type(Contract.class)
				.filter("companyId", companyId).filter("count", contractId)
				.first().now();
		Service service = ofy().load().type(Service.class)
				.filter("companyId", companyId).filter("count", serviceCount)
				.first().now();
		int value=createService(contract, service, companyId, previousDate, serviceNo,
				serviceSerialNo);
		intValue.clear();
		intValue.add(value);
		
		return intValue;
	}

	
	private int createService(Contract contract, Service serviceEnt,
			long companyId, Date previousDate, int serviceNo,
			int serviceSerialNo) {
		try{
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class)
				.filter("companyId", companyId)
				.filter("processName", "Service").filter("status", true)
				.first().now();

		long number = ng.getNumber();
		int count = (int) number;
		logger.log(Level.SEVERE, "Count ::" + count);

		for (int i = 0; i < contract.getItems().size(); i++) {
			Service service = new Service();

			service.setContractCount(contract.getCount());
			service.setContractStartDate(contract.getStartDate());
			service.setContractEndDate(contract.getEndDate());
			// service.setWareHouse(wareHouse);
			// service.setStorageLocation(storageLocation);
			// service.setStorageBin(storageBin);

			// Customer Info
			PersonInfo person = new PersonInfo();
			person.setCount(contract.getCinfo().getCount());
			person.setCellNumber(contract.getCinfo().getCellNumber());
			person.setPocName(contract.getCinfo().getPocName());
			person.setFullName(contract.getCinfo().getFullName().trim());
			service.setPersonInfo(person);

			/**
			 * Harded Coded serial number as one because of contract. and there
			 * will be only one service created against one contract .
			 */
			service.setStackDetailsList(contract.getStackDetailsList());
			service.setServiceSerialNo(serviceSerialNo + 1);
			service.setEmployee(contract.getEmployee());
			service.setBranch(contract.getBranch());
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			Calendar c1 = Calendar.getInstance();
			try {
				c1.setTime(sdf.parse(sdf.format(previousDate)));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);
				
				e1.printStackTrace();
				return -2;
			}

			try {
				c1.add(Calendar.DATE, 45);
				Date date1 = c1.getTime();

				service.setServiceDate(sdf.parse(sdf.format(date1)));

				if (sdf.parse(sdf.format(date1)).after(
						sdf.parse(sdf.format(contract.getEndDate())))) {
					contract.setEndDate(sdf.parse(sdf.format(date1)));
					ofy().save().entity(contract);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -3;
			}
			service.setBranch(contract.getBranch());

			service.setAddress(serviceEnt.getAddress());
			service.setServiceTime("flexible");
			service.setServiceIndexNo(serviceNo + 1);
			service.setCompanyId(companyId);
			// Warehouse code / Shed Name / Multiple stack No

			service.setStatus(Service.SERVICESTATUSSCHEDULE);

			service.setCount(count + 1);

			ng.setNumber(count + 1);

			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(ng);
			ofy().save().entity(service).now();
		}

		 return 1;
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.SEVERE,"Unexpected Error"+e);
			return -1;
		}
		
	}

	/*
	 * Added by Ashwini
	 */
	
	public ArrayList<Customer>createSinglecustomer(int serviceCount,ArrayList custId,long companyId){
		 
		List<Customer> cutomerId =new ArrayList<Customer>();
		cutomerId.addAll(custId); 
		System.out.println("get list -- " + cutomerId.toArray().toString());
		ArrayList<Customer> intValue=new ArrayList<Customer>();
		
		List<Customer> customerlist = ofy().load().type(Customer.class)
				.filter("companyId", companyId)
				.filter("count IN", cutomerId ).list();
		logger.log(Level.SEVERE,"get cust -- " + customerlist);
		intValue.addAll(customerlist);
		return intValue;
	}
	
	/*
	 * 	End by Ashwini
	 */
}
