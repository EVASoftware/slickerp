package com.slicktechnologies.server;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;
import java.util.logging.Level;

import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.CNCBillAnnexure;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeTrackingDetails;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.OtherTaxCharges;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.Purchase;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.StackMaster;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.AssessmentReportUnit;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.DsReportDetails;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.WhatsNew;
import com.slicktechnologies.shared.common.android.EmployeeFingerDB;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.businessprocesslayer.Activity;
import com.slicktechnologies.shared.common.businessprocesslayer.CompanyProfileConfiguration;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.cnc.VersionDetails;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;
import com.slicktechnologies.shared.common.helperlayer.LicenseManagement;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.hrtaxstructure.EmployeeInvestment;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeCheckListType;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.Grade;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneEmi;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipAllocationProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.AssignedShift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.DateShift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.EmployeeShift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.ShiftPattern;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportConfig;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.IncomingInventory;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.InventoryEntries;
import com.slicktechnologies.shared.common.inventory.InventoryInHand;
import com.slicktechnologies.shared.common.inventory.InventoryProductDetail;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.OutgoingInventory;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.SerialNumberDeviation;
import com.slicktechnologies.shared.common.inventory.SerialNumberStockMaster;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.Transfer;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.UploadHistory;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;
import com.slicktechnologies.shared.common.relationalLayer.VendorRelation;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.Liaison;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonGroup;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.InspectionMethod;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.supportlayer.RaiseTicket;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class MyWarmup implements ServletContextListener{


	Logger logger = Logger.getLogger("NameOfYourLogger");


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		logger.log(Level.SEVERE, "After filter 11111111111111111111111" );
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ObjectifyService.register(CustomerBranchServiceLocation.class);
		ObjectifyService.register(ScreenMenuConfiguration.class);
		ObjectifyService.register(CNCBillAnnexure.class);
		ObjectifyService.register(EmployeeTrackingDetails.class);
		ObjectifyService.register(ExpensePolicies.class);
		ObjectifyService.register(Esic.class);
		ObjectifyService.register(ProvidentFund.class);
		ObjectifyService.register(Bonus.class);
		ObjectifyService.register(PaidLeave.class);
		/** date 08.03.2018 added by komal **/
		ObjectifyService.register(TechnicianWareHouseDetailsList.class);
		/** date 6.10.2018 added by komal **/
		ObjectifyService.register(LWF.class);
		ObjectifyService.register(UnitConversion.class);
		ObjectifyService.register(ProfessionalTax.class);
		ObjectifyService.register(EmployeeFingerDB.class);
		ObjectifyService.register(Attendance.class);
		ObjectifyService.register(CNC.class);
		ObjectifyService.register(VersionDetails.class);
		ObjectifyService.register(ProjectAllocation.class);
		/**
		 *  date : 25-01-2018
		 *  Nidhi
		 *  Cron Job configration entity register
		 */
		ObjectifyService.register(CronJobConfigration.class);
		/**
		 *  end
		 */
		/**
		 * Date : 01 Dec 2017
		 * Dev : Rahul Verma
		 * Description : RegisterDevice will store devices registered.
		 */
		ObjectifyService.register(RegisterDevice.class);
		/**
		   * Date : 28-08-2017 By ANIl
		   * For saving service schedule list in separate entity
		   */
		  ObjectifyService.register(ServiceSchedule.class);
		  /**
		   * End
		   */
	  ObjectifyService.register(LoneType.class);
		ObjectifyService.register(PaySlip.class);
		ObjectifyService.register(LoneEmi.class);
		ObjectifyService.register(LeaveBalance.class);
		ObjectifyService.register(LoggedIn.class);
		ObjectifyService.register(Service.class);
		ObjectifyService.register(Quotation.class);
		ObjectifyService.register(Approvals.class);
		ObjectifyService.register(Contract.class);
	  ObjectifyService.register(Config.class);
	  ObjectifyService.register(ConfigCategory.class);
	  ObjectifyService.register(Type.class);
	  ObjectifyService.register(Company.class);
	  ObjectifyService.register(Activity.class);
	  ObjectifyService.register(TimeReportConfig.class);
	  ObjectifyService.register(Branch.class);
	  ObjectifyService.register(BranchRelation.class);
	  ObjectifyService.register(Employee.class);
	  ObjectifyService.register(EmployeeInfo.class);
	  ObjectifyService.register(EmployeeRelation.class);
	  ObjectifyService.register(User.class);
	  ObjectifyService.register(ItemProduct.class);
	  ObjectifyService.register(SuperProduct.class);
	  ObjectifyService.register(ServiceProduct.class);
	  ObjectifyService.register(Vendor.class);
	  ObjectifyService.register(Expense.class);
	  ObjectifyService.register(Lead.class);
	  ObjectifyService.register(Customer.class);
	  ObjectifyService.register(VendorRelation.class);
	  ObjectifyService.register(CustomerRelation.class);
	  ObjectifyService.register(Category.class);
	  ObjectifyService.register(Purchase.class);
	  ObjectifyService.register(Calendar.class);
	  ObjectifyService.register(GLAccount.class);
	  ObjectifyService.register(SalesOrder.class);
	  ObjectifyService.register(EmployeeLeave.class);


	  ObjectifyService.register(Loan.class);
	  ObjectifyService.register(Country.class);
	  ObjectifyService.register(Grade.class);
	  ObjectifyService.register(LeaveType.class);


	  ObjectifyService.register(LeaveApplication.class);
	  ObjectifyService.register(Shift.class);
	  ObjectifyService.register(ShiftPattern.class);
	  ObjectifyService.register(Department.class);
	  
	  ObjectifyService.register(UserRole.class);
	  ObjectifyService.register(HrProject.class);
	  ObjectifyService.register(HolidayType.class);
	  ObjectifyService.register(EmployeeGroup.class);
	  ObjectifyService.register(LeaveGroup.class);
	 
	  ObjectifyService.register(CtcComponent.class);
	  ObjectifyService.register(CTC.class);
	  ObjectifyService.register(EmployeeOvertime.class);
	  ObjectifyService.register(AssignedShift.class);
	 
	  

	  ObjectifyService.register(CompanyAsset.class);
	  ObjectifyService.register(ToolGroup.class);
	  ObjectifyService.register(com.slicktechnologies.shared.common.servicerelated.ServiceProject.class);
	  ObjectifyService.register(ClientSideAsset.class);
	
	  ObjectifyService.register(EmployeeAttendance.class);
	  
	  ObjectifyService.register(LeaveAllocationProcess.class);
	  ObjectifyService.register(PaySlipAllocationProcess.class);
	  
	  ObjectifyService.register(LiaisonStep.class);
	  ObjectifyService.register(Liaison.class);
	  ObjectifyService.register(LiaisonGroup.class);
	  ObjectifyService.register(SalesQuotation.class);
	  ObjectifyService.register(PriceList.class);
	  ObjectifyService.register(RequsestForQuotation.class);
	  ObjectifyService.register(LetterOfIntent.class);
	  ObjectifyService.register(PettyCash.class);
	  ObjectifyService.register(PettyCashDeposits.class);
	  ObjectifyService.register(BillingDocument.class);
	  ObjectifyService.register(Invoice.class);
	  ObjectifyService.register(CustomerPayment.class);
	  ObjectifyService.register(OtherTaxCharges.class);
	  ObjectifyService.register(TaxesAndCharges.class);
	  ObjectifyService.register(DeliveryNote.class);
	  ObjectifyService.register(PurchaseOrder.class);
	  ObjectifyService.register(PurchaseRequisition.class);
	  ObjectifyService.register(GRN.class);
	  ObjectifyService.register(InventoryInHand.class);
	  ObjectifyService.register(Transfer.class);
	  ObjectifyService.register(IncomingInventory.class);
	  ObjectifyService.register(LocationRelation.class);
	  ObjectifyService.register(InventoryEntries.class);
	  ObjectifyService.register(WareHouse.class);
	  ObjectifyService.register(OutgoingInventory.class);
	  ObjectifyService.register(InventoryProductDetail.class);
	  ObjectifyService.register(StorageLocation.class);
	  ObjectifyService.register(Storagebin.class);
	  ObjectifyService.register(ProductInventoryView.class);
	  ObjectifyService.register(ProductInventoryViewDetails.class);
	  ObjectifyService.register(CompanyPayment.class);
	  ObjectifyService.register(VendorPayment.class);
	  ObjectifyService.register(TaxDetails.class);
	  ObjectifyService.register(State.class);
	  ObjectifyService.register(City.class);
	  ObjectifyService.register(Locality.class);
	  ObjectifyService.register(CheckListStep.class);
	  ObjectifyService.register(CheckListType.class);
	  ObjectifyService.register(ShippingMethod.class);
	  ObjectifyService.register(PackingMethod.class);
	  ObjectifyService.register(EmployeeShift.class);
	  ObjectifyService.register(DateShift.class);
	  ObjectifyService.register(TimeReport.class);
	  ObjectifyService.register(PaySlipRecord.class);
	  ObjectifyService.register(ProductGroupList.class);
	  ObjectifyService.register(ProductGroupDetails.class);
	  ObjectifyService.register(BillOfMaterial.class);
	  ObjectifyService.register(NumberGeneration.class);
	  ObjectifyService.register(AccountingInterface.class);
	  ObjectifyService.register(ContactPersonIdentification.class);
	  ObjectifyService.register(PhysicalInventoryMaintaince.class);
	  ObjectifyService.register(RaiseTicket.class);
	  ObjectifyService.register(MaterialRequestNote.class);
	  ObjectifyService.register(MaterialIssueNote.class);
	  ObjectifyService.register(ProductInventoryTransaction.class);
	  ObjectifyService.register(MaterialMovementNote.class);
	  ObjectifyService.register(MaterialMovementType.class);
	  ObjectifyService.register(ProcessName.class);
	  ObjectifyService.register(InteractionType.class);
	  ObjectifyService.register(ProcessConfiguration.class);
	  ObjectifyService.register(DocumentHistory.class);
	  ObjectifyService.register(InspectionMethod.class);
	  ObjectifyService.register(LicenseManagement.class);
	  ObjectifyService.register(CancelSummary.class);
	  ObjectifyService.register(Inspection.class);
	  ObjectifyService.register(BillOfProductMaterial.class);
	  ObjectifyService.register(VendorPriceListDetails.class);
	  ObjectifyService.register(WorkOrder.class);
	  ObjectifyService.register(CustomerBranchDetails.class);
	  ObjectifyService.register(SmsTemplate.class);
	  ObjectifyService.register(SmsConfiguration.class);
	  ObjectifyService.register(WhatsNew.class);
	  ObjectifyService.register(Fumigation.class);
	  ObjectifyService.register(IPAddressAuthorization.class);
	  ObjectifyService.register(MultilevelApproval.class);
	  ObjectifyService.register(SmsHistory.class);
	  ObjectifyService.register(CustomerTrainingDetails.class);
	  ObjectifyService.register(EmployeeAdditionalDetails.class);
	  ObjectifyService.register(Overtime.class);
	  ObjectifyService.register(CompanyPayrollRecord.class);
	  ObjectifyService.register(Declaration.class);
	  ObjectifyService.register(Complain.class);
	  ObjectifyService.register(ContractRenewal.class);
	  ObjectifyService.register(TeamManagement.class);
	  ObjectifyService.register(CustomerUser.class);
	  ObjectifyService.register(PettyCashTransaction.class);
	  ObjectifyService.register(MultipleExpenseMngt.class);
	  ObjectifyService.register(ServicePo.class);
	  ObjectifyService.register(RoleDefinition.class);
	  ObjectifyService.register(EmployeeLevel.class);
	  /** added by komal for hvac **/
	  ObjectifyService.register(CustomerLogDetails.class); 
	  
	  ObjectifyService.register(CTCTemplate.class);
	
	  ObjectifyService.register(AssessmentReportUnit.class);
	  ObjectifyService.register(EmployeeOvertime.class);
	  /** date 24.7.2018 added by komal for employee asset **/
	  ObjectifyService.register(EmployeeAsset.class);
	  /** added by komal for  Taxation **/
	  ObjectifyService.register(TaxSlab.class);
	  ObjectifyService.register(Investment.class);
	  ObjectifyService.register(EmployeeInvestment.class);
	  ObjectifyService.register(ArrearsDetails.class);
	  ObjectifyService.register(EmployeeCheckListType.class);
	  ObjectifyService.register(CNCVersion.class);
	  /** added by komal for vendor invoice entity **/
	  ObjectifyService.register(VendorInvoice.class);
	  ObjectifyService.register(UploadHistory.class);
	  ObjectifyService.register(VoluntaryPf.class);
	  ObjectifyService.register(VoluntaryPfHistory.class);
	  
          ObjectifyService.register(DsReport.class);
		  ObjectifyService.register(DsReportDetails.class);
		/** Date 27-06-2019 added by Vijay **/
		ObjectifyService.register(SerialNumberStockMaster.class);
		ObjectifyService.register(StackMaster.class);
		/** Date 21-09-2019 added by Vijay **/
		ObjectifyService.register(AssesmentReport.class);
		logger.log(Level.SEVERE, "After filter 222222222222222" );
		
		ObjectifyService.register(SerialNumberDeviation.class);
		ObjectifyService.register(FumigationServiceReport.class);
		ObjectifyService.register(CompanyProfileConfiguration.class);
		ObjectifyService.register(CreditNote.class);
		
		ObjectifyService.register(EmailTemplate.class);
		ObjectifyService.register(EmailTemplateConfiguration.class);
		ObjectifyService.register(HrProjectOvertime.class);
		ObjectifyService.register(EmployeeProjectAllocation.class);
		ObjectifyService.register(AuditObservations.class);
		ObjectifyService.register(TermsAndConditions.class);//Ashwini Patil Date:28-02-2023

		List<Company> compEntity = ofy().load().type(Company.class).list();
		logger.log(Level.SEVERE, "Company Size:====="+compEntity.size());
		
		logger.log(Level.SEVERE, "After filter 3333333333333333333" );
		
		List<Branch> Branchentity = ofy().load().type(Branch.class).list();
		logger.log(Level.SEVERE, "Branch Size:====="+Branchentity.size());
		
		logger.log(Level.SEVERE, "Done here =============================================" );
		
		List<ProcessConfiguration> ProcessConfigurationEntity = ofy().load().type(ProcessConfiguration.class).list();
		logger.log(Level.SEVERE, "ProcessConfiguration Size:====="+ProcessConfigurationEntity.size());
		
		logger.log(Level.SEVERE, "Done here $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" );
		
		List<ProcessName> ProcessNameEntity = ofy().load().type(ProcessName.class).list();
		logger.log(Level.SEVERE, "ProcessName Size:====="+ProcessNameEntity.size());
			
		logger.log(Level.SEVERE, "Done here ********************************" );

		ProcessName prosName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).first().now();
		ProcessConfiguration prosconfig = ofy().load().type(ProcessConfiguration.class).filter("processList.processName","CronJob").filter("processList.processType","ContractRenewalDailyEmail").filter("processList.status",true).first().now();					
		
		logger.log(Level.SEVERE,"after 3 filters for ProcessConfigurations");
		
		
		if(prosName!=null){
			logger.log(Level.SEVERE,"In the prossName");
		if(prosconfig!=null){
						
			logger.log(Level.SEVERE,"In the ProsConfifg !=null ");
			
		 }
		}
		
		logger.log(Level.SEVERE,"Warmup request done here $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		
	    logger.log(Level.SEVERE, "Done here@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" );    
	}
	
}
