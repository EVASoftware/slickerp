package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.services.RateContractServicesBillingService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.ratecontractproductpopup.ProductInfo;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class RateContractServicesBillingServiceImpl extends RemoteServiceServlet implements RateContractServicesBillingService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6121106704899303449L;


	Logger logger = Logger.getLogger("Name of logger");
	
	@Override
	public ArrayList<String> CreateServiceAndBilling(Contract con,ArrayList<ProductInfo> productlist,boolean validate) {
		
		ArrayList<String> returnlist = new ArrayList<String>();
		String areaExceedMsg="";
		/**
		 * nidhi Date : 4-12-2017 Check process configration for
		 */
		boolean confiFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service",
						"ContractCategoryAsServiceType", con.getCompanyId());
		boolean conficontractFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("copyconfigration",
						"ContractTypeAsPaymentProcess", con.getCompanyId());
		/**
		 * end
		 */
		
		/**
		 * *:*:* nidhi 26-09-2018 for map bill for material
		 */
		boolean billofMaterialActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial",
						"ApplyBillOfMaterial", con.getCompanyId());

		Customer cust = null;
		Address addr = null;

		logger.log(Level.SEVERE, "inside  rate contract === "+con.getItems().size()+" validateFlag="+validate);

//		System.out.println(" contract product list size==="+ con.getItems().size());

		/**
		 * @author Vijay Chougule Date 24-09-2020
		 * Des :- For Free Material Services will Schedule by default for Technician App
		 */
//		 boolean freeMaterialFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL,con.getCompanyId());
		 boolean freeMaterialFlag = con.isStationedTechnician();

		 
		 HashSet<String> branchSet = new HashSet<String>();
			for (int i=0 ; i< con.getItems().size(); i++)
			{
				Set<Integer> ser =  con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet();
				
				for(Integer seq : ser){
					ArrayList<BranchWiseScheduling> branch =  con.getItems().get(i).getCustomerBranchSchedulingInfo().get(seq);
					for(int k =0 ;k<branch.size();k++){
						if(!branch.get(k).getBranchName().equalsIgnoreCase("Service Address")
								&& !branch.get(k).getBranchName().equalsIgnoreCase("Select")  && branch.get(k).isCheck())
						{
							branchSet.add(branch.get(k).getBranchName());
						}
					}
					
			}
			}
			
			List<CustomerBranchDetails> custBranchList = new ArrayList<CustomerBranchDetails>();
			ArrayList <String> branchList = new  ArrayList<String>();
			if(branchSet.size()>0){
				branchList.addAll(branchSet);
				custBranchList = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName IN",  branchList).filter("cinfo.count", con.getCinfo().getCount())
				   .filter("companyId",con.getCompanyId()).list();
			}
						HashMap<String, String> costCenterList = new HashMap<String, String>(); //Ashwini Patil Date:22-03-2024 orion wants to map cost center from customer branch to service
			for(CustomerBranchDetails branchDt : custBranchList){
				if(branchDt.getCostCenter()!=null)
					costCenterList.put(branchDt.getBusinessUnitName(), branchDt.getCostCenter());
			}
		 
		 
		 
		 
		for (int k = 0; k < con.getItems().size(); k++) {

			for (int l = 0; l < productlist.size(); l++) {

				/**
				 * Date : 30-10-2017 BY ANIL Change comparison from product name
				 * to product code also checked sr no. comparison
				 */
				if (productlist.get(l).getProductCode().equals(con.getItems().get(k).getProductCode())
						&& productlist.get(l).getSrNo() == con.getItems().get(k).getProductSrNo()) {
					// here whichever product selected from popup those product
					// service will create and billing will create as per
					// payment terms of contract
					if (productlist.get(l).isSelectedProduct() == true) {
						Service temp = new Service();
						try{
							
						logger.log(Level.SEVERE, "Product Name ===== "+ productlist.get(l).getProductName());
						if (con.getCinfo() != null) {
							if (con.getCompanyId() != null) {
								cust = ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount())
										.filter("companyId", con.getCompanyId()).first().now();
							} else {
								cust = ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount())
										.first().now();
							}
						}
						if (cust != null)
							addr = cust.getSecondaryAdress();

						List<Service> servicelist = ofy().load().type(Service.class).filter("contractCount", con.getCount())
								.filter("product.productCode",productlist.get(l).getProductCode()).list();
						
						
						
						int p = servicelist.size();
						logger.log(Level.SEVERE, "no of existing services="+p);
//						// here setting tax name and tax percent to existing
//						// product from my popup service tax value
//						Tax tax = new Tax();
//						tax.setTaxName(productlist.get(l).getServiceTax().getTaxName());
//						tax.setPercentage(productlist.get(l).getServiceTax().getPercentage());
//						tax.setTaxConfigName(productlist.get(l).getServiceTax().getTaxConfigName());
//						
//						con.getItems().get(k).getPrduct().setServiceTax(tax);

//						Service temp = new Service();

						temp.setPersonInfo(con.getCinfo());
						temp.setContractCount(con.getCount());
						temp.setProduct((ServiceProduct) con.getItems().get(k).getPrduct());
						temp.setBranch(con.getBranch());
						temp.setStatus(Service.SERVICESTATUSSCHEDULE);
						
						/**
						 * @author Vijay Chougule Date 03-09-2020
						 * Des :- updated code if technician added at contract level then it will map from technician name
						 * and else of old code as sales person as Technician Name
						 */
						if(con.getTechnicianName()!=null && !con.getTechnicianName().equals("")){
							temp.setEmployee(con.getTechnicianName());
						}
						else{
							temp.setEmployee(con.getEmployee());
						}

						
						temp.setContractStartDate(con.getStartDate());
						temp.setContractEndDate(con.getEndDate());
						temp.setAdHocService(false);
						temp.setServiceIndexNo(p++);
						if(con.getServicetype()!=null&&!con.getServicetype().equals("")){
							temp.setServiceType(con.getServicetype()); //Ashwini Patil Date:29-02-2024 for orion
						}else
						temp.setServiceType("Periodic");
						temp.setRefNo(con.getRefNo());
						/**
						 * Date:24-01-2017 By Anil setting ref no.2
						 */
						temp.setRefNo2(con.getRefNo2());
						/**
						 * End
						 */
						temp.setServiceSerialNo(p++);
						temp.setServiceDate(productlist.get(l).getServiceDate());
						temp.setAddress(addr);

						if (con.getNumberRange() != null)
							temp.setNumberRange(con.getNumberRange());

						temp.setCompanyId(con.getCompanyId());

						// /** 29-09-2017 sagar sore [ As only one service is
						// getting created for rate contract whole cost assigned
						// to one service] **/
						// temp.setServiceValue(productlist.get(l).getPrice());
						/**
						 * Date 27-08-2019 By Vijay Des :- Service Value Area *
						 * price if area is not NA
						 */
						double sqftArea = 0;
						if (!productlist.get(l).getAreaprSqFeet().equalsIgnoreCase("NA")
								&& !productlist.get(l).getAreaprSqFeet().equalsIgnoreCase("")) {
							sqftArea = Double.parseDouble(productlist.get(l).getAreaprSqFeet());
							temp.setQuantity(sqftArea);//Ashwini Patil Date:20-10-2023
						} else {
							sqftArea = 1.0;
						}
						temp.setServiceValue(productlist.get(l).getPrice()* sqftArea);
						/**
						 * ends here
						 */
						
						/*
						 * Ashwini Patil 
						 * Date:18-03-2024
						 * Rate contract allows user to create as many number of services and bill.
						 * Orion requested to restrict this feature.
						 * System should not allow to create services for area more than are adefined in contract
						 */
						int totalContractQuantity=Integer.parseInt(con.getItems().get(k).getArea());
						int totalServicedArea=0;
						if(servicelist!=null) {
							for(Service s:servicelist) {
								totalServicedArea+=s.getQuantity();
							}
						}
						
						if((totalServicedArea+sqftArea)>totalContractQuantity) {
							areaExceedMsg+="AreaExceedMsgYour service quantity is exceeding. Find Following details: "+"\n";
							areaExceedMsg+="Product Name : "+con.getItems().get(k).getProductName()+"\n";
							areaExceedMsg+="Total Contractual quantity : "+totalContractQuantity+"\n";
							areaExceedMsg+="Services created for quantity : "+totalServicedArea+"\n";
							int exceedby=(int) ((totalServicedArea+sqftArea)-totalContractQuantity);
							areaExceedMsg+="Currently you are trying to create service for quantity "+sqftArea +" which is exceeding contractual quantity\n\n";
						}

						logger.log(Level.SEVERE, "totalContractQuantity="+totalContractQuantity+" totalServicedArea="+totalServicedArea+" sqftArea="+sqftArea);
						/**
						 * Date : 31-10-2017 BY ANIL making rateContractService
						 * flag true for rate contract
						 */

						temp.setRateContractService(true);
						/**
						 * End
						 */

						/**
						 * nidhi 4-12-2017 for service configration
						 */

						if (confiFlag) {
							temp.setServiceType(con.getCategory());
						}
						/**
						 * end
						 */

						/**
						 * Date 15-02-2018 By vijay for service id will genrate
						 * as per Number Range
						 **/
						if (con.getNumberRange() != null)
							temp.setNumberRange(con.getNumberRange());

						/**
						 * nidhi 29-01-2018 for copy pocname in person info
						 */
						if (con.getPocName() != null&& !con.getPocName().trim().equals("")) {
							temp.getPersonInfo().setPocName(con.getPocName());
						}
						/**
						 * end
						 */

						/**
						 * nidhi 2-08-2018 for premises
						 */
						String primises = "";
						if (con.getItems().get(k).getPremisesDetails() != null
								&& !con.getItems().get(k).getPremisesDetails().equals("")) {
							primises = con.getItems().get(k).getPremisesDetails();
						} 
						//commented by Ashwini Patil Date:17-03-2022 
//						else {  
//							primises = 0 + "";
//						}
						temp.setPremises(primises);
						/**
						 * ends here
						 */
						/**
						 * nidhi 8-08-2018 for map pro model
						 */
						if (con.getItems().get(k).getProModelNo() != null && !con.getItems().get(k).getProModelNo().equals("")) {
							temp.setProModelNo(con.getItems().get(k).getProModelNo());
						}

						if (con.getItems().get(k).getProSerialNo() != null && !con.getItems().get(k).getProSerialNo().equals("")) {
							temp.setProSerialNo(con.getItems().get(k).getProSerialNo());
						}

						/*
						 * end
						 */
						/**
						 * date 16.3.2019 added by komal to create BOM for rate
						 * contract
						 **/
						if (billofMaterialActive) {
							logger.log(Level.SEVERE, "rate contract BOM");
							if (con.getServiceScheduleList().get(k).getServiceProductList() != null
									&& con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0)
									&& con.getServiceScheduleList().get(k).getServiceProductList().get(0).size() > 0) {
								logger.log(Level.SEVERE, "rate contract BOM");
								temp.setUom(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getUnit());
								temp.setQuantity(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getQuantity());
								ArrayList<ServiceProductGroupList> serProList = new ArrayList<ServiceProductGroupList>();
								for (int jj = 0; jj < con.getServiceScheduleList().get(k).getServiceProductList().get(0).size(); jj++) {
									serProList.add(temp.getServiceProdDt(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(jj)));
								}
								logger.log(Level.SEVERE, "rate contract BOM");
								temp.setServiceProductList(serProList);
							}
						}
						/**
						 * @author Vijay Chougule
						 * Des :- Bug NBHC some ratecontract services created with WMSServiceflag true so here added WMSservice flag as false
						 */
						temp.setWmsServiceFlag(false);
						
						if(freeMaterialFlag){
							temp.setServiceScheduled(true);
						}
						temp.setServiceBranch("Service Address"); //Ashwini Patil Date:21-03-2021 Description: service branch was not getting set for rate contract service and due to which service location drop down was not getting value at Customer Support New screen
						
						
						Set<Integer> ser =  con.getItems().get(k).getCustomerBranchSchedulingInfo().keySet();
						
						for(Integer seq : ser){
							ArrayList<BranchWiseScheduling> branch =  con.getItems().get(k).getCustomerBranchSchedulingInfo().get(seq);
							for(BranchWiseScheduling binfo:branch){
								if(binfo.isCheck())
								{
									if(costCenterList!=null&&costCenterList.containsKey(binfo.getBranchName()))
										temp.setCostCenter(costCenterList.get(binfo.getBranchName()));								
									temp.setServiceBranch(binfo.getBranchName());
								}
							}
						}
						
						
						if(con.getItems().get(k).getProfrequency()!=null){
							temp.setProductFrequency(con.getItems().get(k).getProfrequency());
						}
						
						if(areaExceedMsg.equals("")||!validate) {
							GenricServiceImpl impl = new GenricServiceImpl();
							impl.save(temp);
							System.out.println(" services created successfully");
							logger.log(Level.SEVERE,"Here Services created successfully");							
						}
						
						 /**
					     * @author Vijay Chougule
					     * Des :- if Automatich Scheduling process Config is active then Service Project will create with Automatic
					     * Service Scheduling for Technician App.
					     */
						if(areaExceedMsg.equals("")||!validate) {
					    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.AUTOMATICSCHEDULING, temp.getCompanyId())){
					    	if(temp.getServiceProductList()!=null && temp.getServiceProductList().size()!=0){

					    		String companyId = temp.getCompanyId()+"";
						    	String serviceId = temp.getCount()+"";
						    	Queue queue = QueueFactory.getQueue("ProjectCreationTaskQueue-queue");
								queue.add(TaskOptions.Builder.withUrl("/slick_erp/projectcreationtaskqueue").param("serviceId", serviceId)
																	.param("companyId", companyId));
					    	}

									
					    }
						}
					    /**
					     * ends here
					     */
						
						
						}catch(Exception e){
							returnlist.add(3 + "");
						}
						if(areaExceedMsg.equals("")||!validate)
							returnlist.add(1 + "");

						/**
						 * Date 15 Feb 2017 added by vijay for service id should
						 * be copied in billing document
						 */
						int serviceId = temp.getCount();
						/**
						 * End Here
						 */
						/*** here service created SuccessFully ***************/

						/*******
						 * here now i am creating billing document as per
						 * payment terms for per service
						 **************/
						if(areaExceedMsg.equals("")||!validate) {
						for (int i = 0; i < con.getPaymentTermsList().size(); i++) {
							
							try{
							BillingDocument billingDocEntity = new BillingDocument();
							List<SalesOrderProductLineItem> salesProdLis = null;

							ArrayList<PaymentTerms> billingPayTerms = new ArrayList<PaymentTerms>();
							PaymentTerms paymentTerms = new PaymentTerms();

							Date conStartDate = null;
							DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
							dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							System.out.println("Contract Start Date"+ conStartDate);

							if (con.getCinfo() != null)
								billingDocEntity.setPersonInfo(con.getCinfo());
							if (con.getCount() != 0)
								billingDocEntity.setContractCount(con.getCount());
							if (con.getStartDate() != null)
								billingDocEntity.setContractStartDate(con.getStartDate());
							if (con.getEndDate() != null)
								billingDocEntity.setContractEndDate(con.getEndDate());
							if (con.getNetpayable() != 0)
								billingDocEntity.setTotalSalesAmount(productlist.get(l).getTotalAmount());
							System.out.println(" total sales amount"+ productlist.get(l).getTotalAmount());

							double price = productlist.get(l).getPrice();

							double area = 0;
							if (!productlist.get(l).getAreaprSqFeet().equalsIgnoreCase("NA")) {
								area = Double.parseDouble(productlist.get(l).getAreaprSqFeet());
							} else {
								area = 1.0;
							}
							// note : Rohan Bhagde added 1 more parameter as
							// area as it is consider in calculations
//							salesProdLis = retrieveSalesproduct(con.getPaymentTermsList().get(i).getPayTermPercent(),
//									con.getItems().get(k), price, productlist.get(l).getServiceTax(),
//									productlist.get(l).getServiceTaxEdit(),area, productlist.get(l).getProductQty());
							
							salesProdLis = retrieveSalesproduct(con.getPaymentTermsList().get(i).getPayTermPercent(),
									con.getItems().get(k), price, productlist.get(l).getServiceTax(),
									productlist.get(l).getServiceTaxEdit(),area, productlist.get(l).getProductQty(),productlist.get(l).getVatTax(),productlist.get(l).getVatTaxEdit());
							
							ArrayList<SalesOrderProductLineItem> salesOrdArr = new ArrayList<SalesOrderProductLineItem>();
							salesOrdArr.addAll(salesProdLis);
							System.out.println("Size here here"+ salesOrdArr.size());
							if (salesOrdArr.size() != 0)
								billingDocEntity.setSalesOrderProducts(salesOrdArr);
							if (con.getCompanyId() != null)
								billingDocEntity.setCompanyId(con.getCompanyId());

							billingDocEntity.setBillingDate(productlist.get(l).getServiceDate());
							billingDocEntity.setInvoiceDate(productlist.get(l).getServiceDate());
							billingDocEntity.setPaymentDate(productlist.get(l).getServiceDate());

							if (con.getPaymentMethod() != null)
								billingDocEntity.setPaymentMethod(con.getPaymentMethod());
							if (con.getApproverName() != null)
								billingDocEntity.setApproverName(con.getApproverName());
							if (con.getEmployee() != null)
								billingDocEntity.setEmployee(con.getEmployee());
							if (con.getBranch() != null)
								billingDocEntity.setBranch(con.getBranch());

							double percentage = con.getPaymentTermsList().get(i).getPayTermPercent();
							double basebillingwithservicetax = 0;
							double baseBillingAmount = (price * area * percentage) / 100;
							// Date 09-08-2017 vat and gst changes added by
							// vijay
							double basebillingwithVattax = 0;

							List<ContractCharges> billTaxLis = new ArrayList<ContractCharges>();

							if (productlist.get(l).getVatTax().getPercentage() != 0.0|| productlist.get(l).getVatTax().getPercentage() != 0) {

								ContractCharges taxDetails = new ContractCharges();
								if (productlist.get(l).getVatTax().getTaxPrintName() != null
										&& !productlist.get(l).getVatTax().getTaxPrintName().equals("")) {
//									System.out.println("Hi vijay for GST == "+ productlist.get(l).getVatTax().getTaxPrintName());
									taxDetails.setTaxChargeName(productlist.get(l).getVatTax().getTaxPrintName());
								} else {
									taxDetails.setTaxChargeName("VAT");
								}

								taxDetails.setTaxChargePercent(productlist.get(l).getVatTax().getPercentage());
								taxDetails.setTaxChargeAssesVal(baseBillingAmount);
								basebillingwithVattax = baseBillingAmount* productlist.get(l).getVatTax().getPercentage() / 100;
								System.out.println(" vat asses value ==== "+ basebillingwithVattax);
								taxDetails.setChargesBalanceAmt(basebillingwithVattax);
								billTaxLis.add(taxDetails);

							}

							if (productlist.get(l).getServiceTax().getPercentage() != 0.0
									|| productlist.get(l).getServiceTax().getPercentage() != 0) {

								ContractCharges taxDetails = new ContractCharges();
								System.out.println("HI Vijay =="+ productlist.get(l).getServiceTax().getTaxPrintName());
								if (productlist.get(l).getServiceTax().getTaxPrintName() != null
										&& !productlist.get(l).getServiceTax().getTaxPrintName().equals("")) {
									System.out.println("hiiiiii "+ productlist.get(l).getServiceTax().getTaxPrintName());
									taxDetails.setTaxChargeName(productlist.get(l).getServiceTax().getTaxPrintName());
								} else {
									taxDetails.setTaxChargeName("Service Tax");
								}
								taxDetails.setTaxChargePercent(productlist.get(l).getServiceTax().getPercentage());

								if (productlist.get(l).getVatTax().getTaxPrintName() == null
										|| productlist.get(l).getVatTax().getTaxPrintName().equals("")) {
									System.out.println("if vat is there base amount changing ");
									baseBillingAmount = baseBillingAmount+ basebillingwithVattax;
								}
								System.out.println("BASE Billing amount here ==="+ baseBillingAmount);
								taxDetails.setTaxChargeAssesVal(baseBillingAmount);

								basebillingwithservicetax = baseBillingAmount* productlist.get(l).getServiceTax().getPercentage() / 100;
								System.out.println(" asses value ==== "+ basebillingwithservicetax);
								taxDetails.setChargesBalanceAmt(basebillingwithservicetax);
								billTaxLis.add(taxDetails);

							}
							System.out.println("billTaxLis "+billTaxLis.size());
							billingDocEntity.setBillingTaxes(billTaxLis);

							if (con.getStartDate() != null) {
								billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST",con.getStartDate()));
							}
							if (con.getEndDate() != null) {
								billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST",con.getEndDate()));
							}

							double grossValue = baseBillingAmount+ basebillingwithservicetax+ basebillingwithVattax;

							billingDocEntity.setGrossValue(grossValue);

							double totalBillingAmount = baseBillingAmount+ basebillingwithservicetax+ basebillingwithVattax;
							System.out.println("Toatl billing amount without round ===  "+ totalBillingAmount);
							System.out.println("Toatl billing amount with round ===  "+ Math.round(totalBillingAmount));

							billingDocEntity.setTotalBillingAmount(totalBillingAmount);

							billingDocEntity.setOrderCreationDate(new Date());

							/**
							 * Date 03-09-2017 added by vijay for total amount
							 * before taxes
							 **/
							billingDocEntity.setTotalAmount(baseBillingAmount);
							/**
							 * Date 05-09-2017 added by vijay for total amount
							 * after discount amount before taxes
							 **/
							billingDocEntity.setFinalTotalAmt(baseBillingAmount);
							/**
							 * Date 05-09-2017 added by vijay for total amount
							 * after taxes and before other charges
							 **/
							billingDocEntity.setTotalAmtIncludingTax(totalBillingAmount);
							/***
							 * Date 06-09-2017 added by vijay for grand total
							 * before roundoff
							 *********/
							billingDocEntity.setGrandTotalAmount(totalBillingAmount);

							int payTrmsDays = con.getPaymentTermsList().get(i).getPayTermDays();
							double payTrmsPercent = con.getPaymentTermsList().get(i).getPayTermPercent();
							String payTrmsComment = con.getPaymentTermsList().get(i).getPayTermComment().trim();
							paymentTerms.setPayTermDays(payTrmsDays);
							paymentTerms.setPayTermPercent(payTrmsPercent);
							paymentTerms.setPayTermComment(payTrmsComment);
							billingPayTerms.add(paymentTerms);
							billingDocEntity.setArrPayTerms(billingPayTerms);
							billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
							billingDocEntity.setStatus(BillingDocument.CREATED);
							billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
							billingDocEntity.setOrderCformStatus("");
							billingDocEntity.setOrderCformPercent(-1);

							/**
							 * nidhi 24-07-2017 this field added to billing
							 * details for display service category as segment
							 * details
							 * 
							 */
							if (con.getSegment() != null) {
								billingDocEntity.setSegment(con.getSegment());
							}
							/**
							 * end
							 */

							/**
							 * nidhi 24-07-2017 this field added to billing
							 * details for display referance No details
							 * 
							 */
							if (con.getRefNo() != null) {
								billingDocEntity.setRefNumber(con.getRefNo());
							}
							/**
							 * end
							 */

							// vijay on 15 fev 2017
							billingDocEntity.setRateContractServiceId(serviceId);

							if (con.getNumberRange() != null)
								billingDocEntity.setNumberRange(con.getNumberRange());

							/**
							 * Date :24-10-2017 BY ANIL Copying contract
							 * reference no to billing reference as required by
							 * NBHC
							 */
							if (con.getRefNo() != null) {
								billingDocEntity.setRefNumber(con.getRefNo());
							}
							/**
							 * End
							 */
							/**
							 * nidhi Date : 4-12-2017 For copy configration
							 * details to bill
							 */
							if (confiFlag) {
								billingDocEntity.setBillingCategory(con.getCategory());
								billingDocEntity.setBillingType(con.getType());
								billingDocEntity.setBillingGroup(con.getGroup());
							}
							/**
							 * end
							 */
							/**
							 * nidhi Date : 29-01-2018 save poc name from
							 * multiple contact list
							 */
							if (con.getPocName() != null&& !con.getPocName().trim().equals("")) {
								billingDocEntity.getPersonInfo().setPocName(con.getPocName());
							}
							/**
							 * end
							 */
							
							/**
							 * @author Vijay 28-03-2022 for do not print service address flag updating to billing and invoice
							 */
							billingDocEntity.setDonotprintServiceAddress(con.isDonotprintServiceAddress());
							
							/**Sheetal:30-03-2022 for payment mode to updating to billing and invoice**/
							if(con.getPaymentModeName()!=null&&!con.getPaymentModeName().equals("")) {
							billingDocEntity.setPaymentMode(con.getPaymentModeName());
							}
							/**
							 * @author Vijay Date :- 25-01-2022
							 * Des :- to manage invoice pdf print on invoice
							 */
							if(con.getInvoicePdfNameToPrint()!=null){
								billingDocEntity.setInvoicePdfNameToPrint(con.getInvoicePdfNameToPrint());
							}
							/**
							 * ends here
							 */

							GenricServiceImpl impl2 = new GenricServiceImpl();
							ReturnFromServer reServer = impl2.save(billingDocEntity);

							logger.log(Level.SEVERE,"Here Billing created successfully");
							
							/**
							 * nidhi 11-10-2018 ||*
							 */
							if (reServer.count != 0) {
								temp.setBillingCount(reServer.count);
								ofy().save().entity(temp).now();
							}
							/**
							 * end
							 */
							
							}catch(Exception e){
								returnlist.add(4 + "");
							}

							returnlist.add(2 + "");
							
						}
						}
					}
				}
			}
		}

		if(!areaExceedMsg.equals(""))
			returnlist.add(areaExceedMsg);
		
		logger.log(Level.SEVERE,"areaExceedMsg="+areaExceedMsg);
		/**
		 * nidhi 4-12-2017 process config "Service" -
		 * "ContractCategoryAsServiceType"
		 */
		if (confiFlag) {
			boolean status = ServerAppUtility.checkServiceTypeAvailableOrNot(con.getCategory(), con.getCompanyId(),con.getApproverName());
		}

		if (confiFlag) {
			boolean updateFlag = false;
			updateFlag = ServerAppUtility.checkContractCategoryAvailableOrNot(con.getCategory(), con.getCompanyId(),con.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkContractTypeAvailableOrNot(con.getType(), con.getCategory(), con.getCompanyId(),con.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkServiceTypeAvailableOrNot(con.getGroup(), con.getCompanyId(), con.getApproverName(),46);
		}
		/**
		 * end
		 */

		return returnlist;
	}
	
	
	
	
	

	private List<SalesOrderProductLineItem> retrieveSalesproduct(double percentage, SalesLineItem salesLineItem, double price, Tax tax, String servicetaxedit, double area, double prodqty, Tax tax2, String tax2Name) {


			ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
			double baseBillingAmount=0;
			String prodDesc1="",prodDesc2="";
			
				SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", salesLineItem.getPrduct().getCount()).first().now();
				if(superProdEntity!=null){
					System.out.println("Superprod Not Null");
					if(superProdEntity.getComment()!=null){
						System.out.println("Desc 1 Not Null");
						prodDesc1=superProdEntity.getComment();
					}
					if(superProdEntity.getCommentdesc()!=null){
						System.out.println("Desc 2 Not Null");
						prodDesc2=superProdEntity.getCommentdesc();
					}
				}
				
				SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
				salesOrder.setProdId(salesLineItem.getPrduct().getCount());
				salesOrder.setProdCategory(salesLineItem.getProductCategory());
				salesOrder.setProdCode(salesLineItem.getProductCode());
				salesOrder.setProdName(salesLineItem.getProductName());
				salesOrder.setQuantity(prodqty);
				salesOrder.setOrderDuration(salesLineItem.getDuration());
				salesOrder.setOrderServices(salesLineItem.getNumberOfServices());
				/**Date 2-7-2020 by Amol raised by Ashwini**/
				salesOrder.setHsnCode(salesLineItem.getPrduct().getHsnNumber());
				
				salesOrder.setPrice(price);
				salesOrder.setServiceTaxEdit(servicetaxedit);
				salesOrder.setServiceTax(tax);
				salesOrder.setVatTaxEdit(tax2Name);
				salesOrder.setVatTax(tax2);
				
				salesOrder.setProdPercDiscount(0.0);
				salesOrder.setProdDesc1(prodDesc1);
				salesOrder.setProdDesc2(prodDesc2);
				if(salesLineItem.getUnitOfMeasurement()!=null){
					salesOrder.setUnitOfMeasurement(salesLineItem.getUnitOfMeasurement());
				}
				
				//   rohan bhagde added this code for setting area to table 
				salesOrder.setArea(salesLineItem.getArea());
				
				salesOrder.setDiscountAmt(0);
				salesOrder.setTotalAmount(price*area);
				
				System.out.println("percAmt = = "+price);
				System.out.println("percAmt with math round = = "+ Math.round(price));

				System.out.println("payTermPercent === "+percentage);
				
				if(percentage!=0){
					baseBillingAmount=(price*area*percentage)/100;
				}
			
				System.out.println(" withoud round ==== "+baseBillingAmount);
				System.out.println(" after round ==== "+Math.round(baseBillingAmount));
				
				salesOrder.setBaseBillingAmount(baseBillingAmount);
				salesOrder.setPaymentPercent(100.0);
				
				
				/**
				 * Date : 04-08-2017 By Anil
				 * Setting base bill amount to base payment amount
				 */
				salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
				/**
				 * End
				 */
				
				salesOrder.setPrduct(superProdEntity);
				//Date 14-05-2018 By vijay for product sr no
				salesOrder.setProductSrNumber(salesLineItem.getProductSrNo());
				

				/**
				 * Date 06/06/2018 By vijay
				 * for product warranty need to show in invoice
				 */
				if(salesLineItem.getWarrantyPeriod()!=0)
				salesOrder.setWarrantyPeriod(salesLineItem.getWarrantyPeriod());
				
				/**
				 * ends here
				 */

				/**
				 * nidhi
				 * 8-08-2018
				 * for map pro model
				 */
				if(salesLineItem.getProModelNo() !=null && !salesLineItem.getProModelNo().equals("") ){
					salesOrder.setProModelNo(salesLineItem.getProModelNo());
				}
				
				if(salesLineItem.getProSerialNo() !=null && !salesLineItem.getProSerialNo().equals("") ){
					salesOrder.setProSerialNo(salesLineItem.getProSerialNo());
				}
				
				/*
				 * end
				 */
				salesProdArr.add(salesOrder);
			return salesProdArr;
	}
	
	/**
	 * For updating existing employee names to UPPER CASE 
	 */
	@Override
	public ArrayList<String> updateEmployees(long companyId) {
		
		logger.log(Level.SEVERE," inside employee update method");
		
		ArrayList<String> returnlist = new ArrayList<String>();
		
		 List<Employee> employeelist =  ofy().load().type(Employee.class).filter("companyId", companyId).list();
		 
		 for(int i=0;i<employeelist.size();i++){
			 
			 String employeefullName = employeelist.get(i).getFullname();
			 employeelist.get(i).setFullname(employeefullName.trim().toUpperCase());
			 ofy().save().entity(employeelist.get(i));
			 logger.log(Level.SEVERE,"employee udpated");

		 }

		 
		logger.log(Level.SEVERE,"All employee udpated successfully");
		returnlist.add("1");
		
		logger.log(Level.SEVERE,"Here Started employee Info");
		
		List<EmployeeInfo> employeeInfolist =  ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).list();
		 
		 for(int i=0;i<employeeInfolist.size();i++){
			 
			 String employeefullName = employeeInfolist.get(i).getFullName();
			 employeeInfolist.get(i).setFullName(employeefullName.trim().toUpperCase());
			 ofy().save().entity(employeeInfolist.get(i));
			 logger.log(Level.SEVERE,"employee Info udpated name ="+employeefullName);

		 }

		logger.log(Level.SEVERE,"All employee Info udpated successfully");
		returnlist.add("2");
		
		return returnlist;
	}






	@Override
	public ArrayList<String> CreateServiceAndBillingForBranches(
			Contract con,ArrayList<ProductInfo> productlist,boolean validate) {
		logger.log(Level.SEVERE, "Ashwini in CreateServiceAndBillingForBranches validatefalg="+validate);
		
		ArrayList<String> returnlist = new ArrayList<String>();
		String areaExceedMsg="";
		boolean confiFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service",
						"ContractCategoryAsServiceType", con.getCompanyId());
		boolean conficontractFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("copyconfigration",
						"ContractTypeAsPaymentProcess", con.getCompanyId());
		
		boolean billofMaterialActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial",
						"ApplyBillOfMaterial", con.getCompanyId());

		Customer cust = null;
		Address addr = null;
		List<CustomerBranchDetails> customerBranches=null;

		logger.log(Level.SEVERE, "inside  rate contract === "+con.getItems().size());
		
		if (con.getCinfo() != null) {
			if (con.getCompanyId() != null) {
				cust = ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount())
						.filter("companyId", con.getCompanyId()).first().now();
			} else {
				cust = ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount())
						.first().now();
			}
			
			
		}
			if (cust != null){
			addr = cust.getSecondaryAdress();
			customerBranches=ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", con.getCompanyId()).filter("cinfo.count",cust.getCount()).list();
			
		}
		
		
//		System.out.println(" contract product list size==="+ con.getItems().size());

		
//		 boolean freeMaterialFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL,con.getCompanyId());
		 boolean freeMaterialFlag = con.isStationedTechnician();

		 
		for (int k = 0; k < con.getItems().size(); k++) {

			for (int l = 0; l < productlist.size(); l++) {

				
				if (productlist.get(l).getProductCode().equals(con.getItems().get(k).getProductCode())
						&& productlist.get(l).getSrNo() == con.getItems().get(k).getProductSrNo()) {
					
					
					
					if (productlist.get(l).isSelectedProduct() == true) {
						logger.log(Level.SEVERE, "in selectedProduct");
						boolean billCreated=false;
						ReturnFromServer reServer=null;
						ArrayList<BranchWiseScheduling> branchSchedulingList=new ArrayList<BranchWiseScheduling>();
						
						List<Service> servicelist = ofy().load().type(Service.class).filter("contractCount", con.getCount())
								.filter("product.productCode",productlist.get(l).getProductCode()).list();
						
						int p = servicelist.size();
						
						if(con.getItems().get(k).getCustomerBranchSchedulingInfo()!=null){
							branchSchedulingList =con.getItems().get(k).getCustomerBranchSchedulingInfo().get(con.getItems().get(k).getProductSrNo());
						}
						if(branchSchedulingList!=null&&branchSchedulingList.size()>1){
							logger.log(Level.SEVERE, "branchSchedulingList size="+branchSchedulingList.size());
							int noOfBranchesSelected=0;
							for(BranchWiseScheduling b:branchSchedulingList){
								if(b.isCheck())
									noOfBranchesSelected++;
							}
							for(int b=0;b<branchSchedulingList.size();b++){
								
								BranchWiseScheduling branch=branchSchedulingList.get(b);
								if(branch.isCheck()==true){
								logger.log(Level.SEVERE, "branchName for "+con.getItems().get(k).getPrduct().getProductName()+" ="+branch.getBranchName()+"Amount="+branch.getAmount());
								Service temp = new Service();
								try{
									
//								logger.log(Level.SEVERE, "Product Name ===== "+ productlist.get(l).getProductName());
								

								

//								// here setting tax name and tax percent to existing
//								// product from my popup service tax value
//								Tax tax = new Tax();
//								tax.setTaxName(productlist.get(l).getServiceTax().getTaxName());
//								tax.setPercentage(productlist.get(l).getServiceTax().getPercentage());
//								tax.setTaxConfigName(productlist.get(l).getServiceTax().getTaxConfigName());
//								
//								con.getItems().get(k).getPrduct().setServiceTax(tax);

//								Service temp = new Service();

								temp.setPersonInfo(con.getCinfo());
								temp.setContractCount(con.getCount());
								temp.setProduct((ServiceProduct) con.getItems().get(k).getPrduct());
								temp.setBranch(con.getBranch());								
								temp.setStatus(Service.SERVICESTATUSSCHEDULE);
								
								
								if(con.getTechnicianName()!=null && !con.getTechnicianName().equals("")){
									temp.setEmployee(con.getTechnicianName());
								}
								else{
									temp.setEmployee(con.getEmployee());
								}

								
								temp.setContractStartDate(con.getStartDate());
								temp.setContractEndDate(con.getEndDate());
								temp.setAdHocService(false);
								temp.setServiceIndexNo(p);
								if(con.getServicetype()!=null&&!con.getServicetype().equals("")){
									temp.setServiceType(con.getServicetype()); //Ashwini Patil Date:29-02-2024 for orion
								}else
								temp.setServiceType("Periodic");
								temp.setRefNo(con.getRefNo());
								
								temp.setRefNo2(con.getRefNo2());
								
								temp.setServiceSerialNo(p);
								temp.setServiceDate(productlist.get(l).getServiceDate());
								if(branch.getBranchName().equals("Service Address"))
									temp.setAddress(addr);
								else if(customerBranches!=null){
									for(CustomerBranchDetails details:customerBranches){
										if(details.getBusinessUnitName().equals(branch.getBranchName())){
											temp.setAddress(details.getAddress());
											if(details.getCostCenter()!=null)
												temp.setCostCenter(details.getCostCenter()); //Ashwini Patil Date:22-03-2024
										
										}
									}
								}else{
									temp.setAddress(addr);
								}
									

								if (con.getNumberRange() != null)
									temp.setNumberRange(con.getNumberRange());

								temp.setCompanyId(con.getCompanyId());

								
								double sqftArea = 0;
								if (!productlist.get(l).getAreaprSqFeet().equalsIgnoreCase("NA")
										&& !productlist.get(l).getAreaprSqFeet().equalsIgnoreCase("")) {
									sqftArea = Double.parseDouble(productlist.get(l).getAreaprSqFeet());
									temp.setQuantity(sqftArea);//Ashwini Patil Date:20-10-2023
								} else {
									sqftArea = 1.0;
								}
								System.out.println("productlist.get(l).getPrice()="+productlist.get(l).getPrice()+" sqftArea="+sqftArea);
								double totalprice=productlist.get(l).getPrice()* sqftArea;
								System.out.println("totalprice="+totalprice);
								if(branch.getAmount()>0)
									temp.setServiceValue(branch.getAmount());
								else
									temp.setServiceValue(totalprice/noOfBranchesSelected);
								

								/*
								 * Ashwini Patil 
								 * Date:18-03-2024
								 * Rate contract allows user to create as many number of services and bill.
								 * Orion requested to restrict this feature.
								 * System should not allow to create services for area more than are adefined in contract
								 */
								int totalContractQuantity=Integer.parseInt(con.getItems().get(k).getArea());
								int totalServicedArea=0;
								if(servicelist!=null) {
									for(Service s:servicelist) {
										totalServicedArea+=s.getQuantity();
									}
								}
								
								if((totalServicedArea+sqftArea)>totalContractQuantity) {
									areaExceedMsg+="AreaExceedMsgYour service quantity is exceeding. Find Following details: "+"\n";
									areaExceedMsg+="Product Name : "+con.getItems().get(k).getProductName()+"\n";
									areaExceedMsg+="Total Contractual quantity : "+totalContractQuantity+"\n";
									areaExceedMsg+="Services created for quantity : "+totalServicedArea+"\n";
									int exceedby=(int) ((totalServicedArea+sqftArea)-totalContractQuantity);
									areaExceedMsg+="Currently you are trying to create service for quantity "+sqftArea +" which is exceeding contractual quantity\n\n";
								}
								
								logger.log(Level.SEVERE, "totalContractQuantity="+totalContractQuantity+" totalServicedArea="+totalServicedArea+" sqftArea="+sqftArea);
								
								
								temp.setRateContractService(true);
								

								if (confiFlag) {
									temp.setServiceType(con.getCategory());
								}
								
								if (con.getNumberRange() != null)
									temp.setNumberRange(con.getNumberRange());

								
								if (con.getPocName() != null&& !con.getPocName().trim().equals("")) {
									temp.getPersonInfo().setPocName(con.getPocName());
								}
							
								String primises = "";
								if (con.getItems().get(k).getPremisesDetails() != null
										&& !con.getItems().get(k).getPremisesDetails().equals("")) {
									primises = con.getItems().get(k).getPremisesDetails();
								} 
								//commented by Ashwini Patil Date:17-03-2022 
//								else {  
//									primises = 0 + "";
//								}
								temp.setPremises(primises);
								
								if (con.getItems().get(k).getProModelNo() != null && !con.getItems().get(k).getProModelNo().equals("")) {
									temp.setProModelNo(con.getItems().get(k).getProModelNo());
								}

								if (con.getItems().get(k).getProSerialNo() != null && !con.getItems().get(k).getProSerialNo().equals("")) {
									temp.setProSerialNo(con.getItems().get(k).getProSerialNo());
								}

								if (billofMaterialActive) {
									logger.log(Level.SEVERE, "rate contract BOM");
									if (con.getServiceScheduleList().get(k).getServiceProductList() != null
											&& con.getServiceScheduleList().get(k).getServiceProductList().containsKey(0)
											&& con.getServiceScheduleList().get(k).getServiceProductList().get(0).size() > 0) {
										logger.log(Level.SEVERE, "rate contract BOM");
										temp.setUom(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getUnit());
										temp.setQuantity(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(0).getQuantity());
										ArrayList<ServiceProductGroupList> serProList = new ArrayList<ServiceProductGroupList>();
										for (int jj = 0; jj < con.getServiceScheduleList().get(k).getServiceProductList().get(0).size(); jj++) {
											serProList.add(temp.getServiceProdDt(con.getServiceScheduleList().get(k).getServiceProductList().get(0).get(jj)));
										}
										logger.log(Level.SEVERE, "rate contract BOM");
										temp.setServiceProductList(serProList);
									}
								}
							
								temp.setWmsServiceFlag(false);
								
								if(freeMaterialFlag){
									temp.setServiceScheduled(true);
								}
//								temp.setServiceBranch("Service Address"); //Ashwini Patil Date:21-03-2021 Description: service branch was not getting set for rate contract service and due to which service location drop down was not getting value at Customer Support New screen
								temp.setServiceBranch(branch.getBranchName());
								
								if(areaExceedMsg.equals("")||!validate) {
								GenricServiceImpl impl = new GenricServiceImpl();
								impl.save(temp);
								logger.log(Level.SEVERE,"Here Service created successfully");
								
								 
							    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.AUTOMATICSCHEDULING, temp.getCompanyId())){
							    	if(temp.getServiceProductList()!=null && temp.getServiceProductList().size()!=0){

							    		String companyId = temp.getCompanyId()+"";
								    	String serviceId = temp.getCount()+"";
								    	Queue queue = QueueFactory.getQueue("ProjectCreationTaskQueue-queue");
										queue.add(TaskOptions.Builder.withUrl("/slick_erp/projectcreationtaskqueue").param("serviceId", serviceId)
																			.param("companyId", companyId));
							    	}

											
							    }
								}
							    /**
							     * ends here
							     */
								
								
								}catch(Exception e){
									returnlist.add(3 + "");
								}
								if(areaExceedMsg.equals("")||!validate)
									returnlist.add(1 + "");

								
								int serviceId = temp.getCount();
								
								if(areaExceedMsg.equals("")||!validate) {
								if(!billCreated){
									System.out.println("in if(!billCreated)");
								for (int i = 0; i < con.getPaymentTermsList().size(); i++) {
									
									try{
									BillingDocument billingDocEntity = new BillingDocument();
									List<SalesOrderProductLineItem> salesProdLis = null;

									ArrayList<PaymentTerms> billingPayTerms = new ArrayList<PaymentTerms>();
									PaymentTerms paymentTerms = new PaymentTerms();

									Date conStartDate = null;
									DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
									dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
									TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//									System.out.println("Contract Start Date"+ conStartDate);

									if (con.getCinfo() != null)
										billingDocEntity.setPersonInfo(con.getCinfo());
									if (con.getCount() != 0)
										billingDocEntity.setContractCount(con.getCount());
									if (con.getStartDate() != null)
										billingDocEntity.setContractStartDate(con.getStartDate());
									if (con.getEndDate() != null)
										billingDocEntity.setContractEndDate(con.getEndDate());
									if (con.getNetpayable() != 0)
										billingDocEntity.setTotalSalesAmount(productlist.get(l).getTotalAmount());
//									System.out.println(" total sales amount"+ productlist.get(l).getTotalAmount());

									double price = productlist.get(l).getPrice();

									double area = 0;
									if (!productlist.get(l).getAreaprSqFeet().equalsIgnoreCase("NA")) {
										area = Double.parseDouble(productlist.get(l).getAreaprSqFeet());
									} else {
										area = 1.0;
									}
									
									salesProdLis = retrieveSalesproduct(con.getPaymentTermsList().get(i).getPayTermPercent(),
											con.getItems().get(k), price, productlist.get(l).getServiceTax(),
											productlist.get(l).getServiceTaxEdit(),area, productlist.get(l).getProductQty(),productlist.get(l).getVatTax(),productlist.get(l).getVatTaxEdit());
									
									ArrayList<SalesOrderProductLineItem> salesOrdArr = new ArrayList<SalesOrderProductLineItem>();
									salesOrdArr.addAll(salesProdLis);
									System.out.println("Size here here"+ salesOrdArr.size());
									if (salesOrdArr.size() != 0)
										billingDocEntity.setSalesOrderProducts(salesOrdArr);
									if (con.getCompanyId() != null)
										billingDocEntity.setCompanyId(con.getCompanyId());

									billingDocEntity.setBillingDate(productlist.get(l).getServiceDate());
									billingDocEntity.setInvoiceDate(productlist.get(l).getServiceDate());
									billingDocEntity.setPaymentDate(productlist.get(l).getServiceDate());

									if (con.getPaymentMethod() != null)
										billingDocEntity.setPaymentMethod(con.getPaymentMethod());
									if (con.getApproverName() != null)
										billingDocEntity.setApproverName(con.getApproverName());
									if (con.getEmployee() != null)
										billingDocEntity.setEmployee(con.getEmployee());
									if (con.getBranch() != null)
										billingDocEntity.setBranch(con.getBranch());

									double percentage = con.getPaymentTermsList().get(i).getPayTermPercent();
									double basebillingwithservicetax = 0;
									double baseBillingAmount = (price * area * percentage) / 100;
									
									double basebillingwithVattax = 0;

									List<ContractCharges> billTaxLis = new ArrayList<ContractCharges>();

									if (productlist.get(l).getVatTax().getPercentage() != 0.0|| productlist.get(l).getVatTax().getPercentage() != 0) {

										ContractCharges taxDetails = new ContractCharges();
										if (productlist.get(l).getVatTax().getTaxPrintName() != null
												&& !productlist.get(l).getVatTax().getTaxPrintName().equals("")) {
//											System.out.println("Hi vijay for GST == "+ productlist.get(l).getVatTax().getTaxPrintName());
											taxDetails.setTaxChargeName(productlist.get(l).getVatTax().getTaxPrintName());
										} else {
											taxDetails.setTaxChargeName("VAT");
										}

										taxDetails.setTaxChargePercent(productlist.get(l).getVatTax().getPercentage());
										taxDetails.setTaxChargeAssesVal(baseBillingAmount);
										basebillingwithVattax = baseBillingAmount* productlist.get(l).getVatTax().getPercentage() / 100;
//										System.out.println(" vat asses value ==== "+ basebillingwithVattax);
										taxDetails.setChargesBalanceAmt(basebillingwithVattax);
										billTaxLis.add(taxDetails);

									}

									if (productlist.get(l).getServiceTax().getPercentage() != 0.0
											|| productlist.get(l).getServiceTax().getPercentage() != 0) {

										ContractCharges taxDetails = new ContractCharges();
//										System.out.println("HI Vijay =="+ productlist.get(l).getServiceTax().getTaxPrintName());
										if (productlist.get(l).getServiceTax().getTaxPrintName() != null
												&& !productlist.get(l).getServiceTax().getTaxPrintName().equals("")) {
//											System.out.println("hiiiiii "+ productlist.get(l).getServiceTax().getTaxPrintName());
											taxDetails.setTaxChargeName(productlist.get(l).getServiceTax().getTaxPrintName());
										} else {
											taxDetails.setTaxChargeName("Service Tax");
										}
										taxDetails.setTaxChargePercent(productlist.get(l).getServiceTax().getPercentage());

										if (productlist.get(l).getVatTax().getTaxPrintName() == null
												|| productlist.get(l).getVatTax().getTaxPrintName().equals("")) {
//											System.out.println("if vat is there base amount changing ");
											baseBillingAmount = baseBillingAmount+ basebillingwithVattax;
										}
//										System.out.println("BASE Billing amount here ==="+ baseBillingAmount);
										taxDetails.setTaxChargeAssesVal(baseBillingAmount);

										basebillingwithservicetax = baseBillingAmount* productlist.get(l).getServiceTax().getPercentage() / 100;
//										System.out.println(" asses value ==== "+ basebillingwithservicetax);
										taxDetails.setChargesBalanceAmt(basebillingwithservicetax);
										billTaxLis.add(taxDetails);

									}
//									System.out.println("billTaxLis "+billTaxLis.size());
									billingDocEntity.setBillingTaxes(billTaxLis);

									if (con.getStartDate() != null) {
										billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST",con.getStartDate()));
									}
									if (con.getEndDate() != null) {
										billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST",con.getEndDate()));
									}

									double grossValue = baseBillingAmount+ basebillingwithservicetax+ basebillingwithVattax;

									billingDocEntity.setGrossValue(grossValue);

									double totalBillingAmount = baseBillingAmount+ basebillingwithservicetax+ basebillingwithVattax;
//									System.out.println("Toatl billing amount without round ===  "+ totalBillingAmount);
//									System.out.println("Toatl billing amount with round ===  "+ Math.round(totalBillingAmount));

									billingDocEntity.setTotalBillingAmount(totalBillingAmount);

									billingDocEntity.setOrderCreationDate(new Date());

									
									billingDocEntity.setTotalAmount(baseBillingAmount);
									
									billingDocEntity.setFinalTotalAmt(baseBillingAmount);
									
									billingDocEntity.setTotalAmtIncludingTax(totalBillingAmount);
									
									billingDocEntity.setGrandTotalAmount(totalBillingAmount);

									int payTrmsDays = con.getPaymentTermsList().get(i).getPayTermDays();
									double payTrmsPercent = con.getPaymentTermsList().get(i).getPayTermPercent();
									String payTrmsComment = con.getPaymentTermsList().get(i).getPayTermComment().trim();
									paymentTerms.setPayTermDays(payTrmsDays);
									paymentTerms.setPayTermPercent(payTrmsPercent);
									paymentTerms.setPayTermComment(payTrmsComment);
									billingPayTerms.add(paymentTerms);
									billingDocEntity.setArrPayTerms(billingPayTerms);
									billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
									billingDocEntity.setStatus(BillingDocument.CREATED);
									billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
									billingDocEntity.setOrderCformStatus("");
									billingDocEntity.setOrderCformPercent(-1);

									
									if (con.getSegment() != null) {
										billingDocEntity.setSegment(con.getSegment());
									}
									
									if (con.getRefNo() != null) {
										billingDocEntity.setRefNumber(con.getRefNo());
									}
									
									billingDocEntity.setRateContractServiceId(serviceId);

									if (con.getNumberRange() != null)
										billingDocEntity.setNumberRange(con.getNumberRange());

									
									if (con.getRefNo() != null) {
										billingDocEntity.setRefNumber(con.getRefNo());
									}
									
									if (confiFlag) {
										billingDocEntity.setBillingCategory(con.getCategory());
										billingDocEntity.setBillingType(con.getType());
										billingDocEntity.setBillingGroup(con.getGroup());
									}
									
									if (con.getPocName() != null&& !con.getPocName().trim().equals("")) {
										billingDocEntity.getPersonInfo().setPocName(con.getPocName());
									}
									
									billingDocEntity.setDonotprintServiceAddress(con.isDonotprintServiceAddress());
									
									if(con.getPaymentModeName()!=null&&!con.getPaymentModeName().equals("")) {
									billingDocEntity.setPaymentMode(con.getPaymentModeName());
									}
									
									/**
									 * @author Vijay Date :- 25-01-2022
									 * Des :- to manage invoice pdf print on invoice
									 */
									if(con.getInvoicePdfNameToPrint()!=null){
										billingDocEntity.setInvoicePdfNameToPrint(con.getInvoicePdfNameToPrint());
									}
									/**
									 * ends here
									 */

									GenricServiceImpl impl2 = new GenricServiceImpl();
									reServer = impl2.save(billingDocEntity);
									billCreated=true;
									logger.log(Level.SEVERE,"Here Billing created successfully");
									
									
									
									}catch(Exception e){
										returnlist.add(4 + "");
									}
									returnlist.add(2 + "");
									
								}
								}
								
								
								if (reServer.count != 0) {
									temp.setBillingCount(reServer.count);
									ofy().save().entity(temp).now();								
								}
								
								p++;
								}
							}
							}
						}else{
							logger.log(Level.SEVERE, "branchSchedulingList empty");
						}
						
						branchSchedulingList=null;
						
						
					}//selected true end
				}
			}
		}

		if(!areaExceedMsg.equals(""))
			returnlist.add(areaExceedMsg);
		
		logger.log(Level.SEVERE,"areaExceedMsg="+areaExceedMsg);
		if (confiFlag) {
			boolean status = ServerAppUtility.checkServiceTypeAvailableOrNot(con.getCategory(), con.getCompanyId(),con.getApproverName());
		}

		if (confiFlag) {
			boolean updateFlag = false;
			updateFlag = ServerAppUtility.checkContractCategoryAvailableOrNot(con.getCategory(), con.getCompanyId(),con.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkContractTypeAvailableOrNot(con.getType(), con.getCategory(), con.getCompanyId(),con.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkServiceTypeAvailableOrNot(con.getGroup(), con.getCompanyId(), con.getApproverName(),46);
		}
		/**
		 * end
		 */

		return returnlist;
	
	}
	
}
