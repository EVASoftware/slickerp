package com.slicktechnologies.server.email;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.sendgrid.Attachments;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.slicktechnologies.client.services.SendGridEmailService;

import org.apache.commons.codec.binary.Base64;

public class SendGridEmailServlet extends RemoteServiceServlet implements SendGridEmailService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -265041451925193673L;

	Logger logger = Logger.getLogger("SendGridEmail");
	
	@Override
	public void sendGridEmail(String toEmail) {
		logger.log(Level.SEVERE, "Entered In Common Method "+toEmail);
		final String sendgridApiKey = System.getenv("SENDGRID_API_KEY");
		final String sendgridSender = System.getenv("SENDGRID_SENDER");
		
		
		logger.log(Level.SEVERE, "API KEY updated : "+sendgridApiKey);
		logger.log(Level.SEVERE, "SENDER updated : "+sendgridSender);

		// [START gae_sendgrid]
		// Set content for request.
		Email to = new Email(toEmail);
		Email from = new Email(sendgridSender);
		
		String subject = "This is a test email";
		Content content = new Content("text/plain", "Example text body.");
		Mail mail = new Mail(from, subject, to, content);

		try {
			// Instantiates SendGrid client.
			SendGrid sendgrid = new SendGrid(sendgridApiKey);

			// Instantiate SendGrid request.
			Request request = new Request();
			
			// Set request configuration.
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());

			// Use the client to send the API request.
			Response response = sendgrid.api(request);
			
			if (response.getStatusCode() != 202) {
				logger.log(Level.SEVERE,String.format("An error occured: %s",response.getStatusCode()));
				return;
			}
//			// Print response.
			logger.log(Level.SEVERE,"Email sent.");
		} catch (IOException e) {
			try {
				throw new ServletException("SendGrid error", e);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		// [END gae_sendgrid]
	}
	
	
	/**
	 * @author Anil,Date : 09-02-2019
	 * @param fromEmail
	 * @param toList
	 * @param ccList
	 * @param bccList
	 * @param subject
	 * @param mailBody
	 * @param contentType-"text/plain" ,"text/html"
	 * @param attachmentInBytes
	 * @param attachmentName
	 * @param attachmentContentType-"application/pdf","image/png","application/vnd.ms-excel"
	 * @return
	 */
	public String sendMailWithSendGrid(String fromEmail,ArrayList<String> toList,ArrayList<String> ccList,
			ArrayList<String> bccList,String subject,String mailBody,String contentType,
			ByteArrayOutputStream attachmentInBytes,String attachmentName, String attachmentContentType,String displayNameForFromEmail){

		Mail mail = new Mail();
		
		String msg="";
		logger.log(Level.SEVERE, "INSIDE SEND SEND GRID EMAIL API ");
		final String sendgridApiKey = System.getenv("SENDGRID_API_KEY");
		final String sendgridSender = System.getenv("SENDGRID_SENDER");
		logger.log(Level.SEVERE, "API KEY updated : "+sendgridApiKey);
		logger.log(Level.SEVERE, "SENDER updated : "+sendgridSender);		
				
			
		// [START gae_sendgrid]
		// Set content for request.
		Email from =null;
		if(fromEmail!=null&&!fromEmail.equals("")){
			from = new Email(fromEmail);
			/**
			 * @author Anil , Date : 10-04-2020
			 * setting from email display name
			 */
			if(displayNameForFromEmail!=null&&!displayNameForFromEmail.equals("")){
				from.setName(displayNameForFromEmail);
			}
			
		}else{
			return "Please add email address.";
		}
		logger.log(Level.SEVERE, "FROM : "+fromEmail);
		mail.setFrom(from);
		
		Personalization personalization = new Personalization();
		
		/**
		 * @author Vijay Chougule
		 * Des :- added below code should not add duplicate email id in To cc and BCC
		 * so added to lower because hashset can be duplicate with case sensitive email id's
		 */
		ArrayList<String> toemaillist = new ArrayList<String>();
		if(toList!=null && toList.size()!=0){
			for(String emailId : toList) {
				toemaillist.add(emailId.toLowerCase());
			}
		}
		
		ArrayList<String> ccemaillist = new ArrayList<String>();
		if(ccList!=null&&ccList.size()!=0){
			for(String emailId : ccList) {
				ccemaillist.add(emailId.toLowerCase());
			}
		}
		
		ArrayList<String> bccemaillist = new ArrayList<String>();
		/**
		 * @author Anil @since 14-10-2021
		 * Sending copy to from mail address in order to maintain history of sent mail from send grid
		 * raised by Nitin sir and Harshan 
		 */
		bccemaillist.add(fromEmail);
		
		if(bccList!=null&&bccList.size()!=0){
			for(String emailId : bccList) {
				bccemaillist.add(emailId.toLowerCase());
			}
		}else{
			/**
			 * @author Anil @since 14-10-2021
			 * Sending copy to from mail address in order to maintain history of sent mail from send grid
			 * raised by Nitin sir and Harshan 
			 */
			bccList=new ArrayList<String>();
			bccList.add(fromEmail);
		}
		logger.log(Level.SEVERE, "bccemaillist : " + bccemaillist);
		logger.log(Level.SEVERE, "bccList : " + bccList);
		
		
		/**
		 * Date 04-12-2019 by Vijay
		 *  Des :- Sender id must be unique as per sendgrid to send email
		 *  if duplicate found then sendgrid response as error duplicate instance found
		 *  added hashset to for avoid duplicate email id 
		 */
		if(toemaillist!=null && toemaillist.size()!=0){
			HashSet<String> tohslist = new HashSet<String>(toemaillist);
				for(String toEmail:tohslist){
					logger.log(Level.SEVERE, "TO : "+toEmail);
					Email to = new Email();
				    to.setEmail(toEmail);
				    personalization.addTo(to);
				}
		}
		else{
			return "Please add to email address.";
		}
		
		try {

			if (ccemaillist != null && ccemaillist.size() != 0) {
				ccList.removeAll(toemaillist);
				/**
				 * @author Anil
				 * @since 14-10-2020 removing duplicate email ids from cc by comparing to list
				 */
				HashSet<String> cchslist = new HashSet<String>(ccList);
				logger.log(Level.SEVERE, "cc : " + ccList);
				for (String ccEmail : cchslist) {
					Email cc = new Email();
					cc.setEmail(ccEmail);
					personalization.addCc(cc);
				}
			}

		} catch (Exception e) {

		}
		
		try {
			if (bccemaillist != null && bccemaillist.size() != 0) {
				logger.log(Level.SEVERE, "bccemaillist : " + bccemaillist);
				
				if(toemaillist!=null&&toemaillist.size()!=0) {
					bccList.removeAll(toemaillist);
				}
				if(ccList!=null&&ccList.size()!=0) {
					bccList.removeAll(ccList);
				}
				/**
				 * @author Anil
				 * @since 14-10-2020 removing duplicate email ids from cc by comparing to list
				 */
				HashSet<String> bcchslist = new HashSet<String>(bccList);
				logger.log(Level.SEVERE, "bcc : " + bccList);
				for (String bccEmail : bcchslist) {
					Email bcc = new Email();
					bcc.setEmail(bccEmail);
					personalization.addBcc(bcc);
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "bcc exception: "+e.getStackTrace()  );
		}
		
		mail.addPersonalization(personalization);
		
		mail.setSubject(subject);
		
		Content content = new Content();
	    content.setType(contentType);
	    content.setValue(mailBody);
	    mail.addContent(content);
	    
	    
	    if(attachmentInBytes!=null&&attachmentName!=null&&attachmentContentType!=null){
	    	byte[] filedata =attachmentInBytes.toByteArray();
	    	Base64 x = new Base64();
			String dataString = x.encodeAsString(filedata);
			Attachments attachments = new Attachments();
			attachments.setContent(dataString);
			attachments.setType(attachmentContentType);// "application/pdf"
			attachments.setFilename(attachmentName);
			attachments.setDisposition("attachment");
			attachments.setContentId("Banner");
			mail.addAttachments(attachments);
	    	
	    }

		try {
			// Instantiates SendGrid client.
			SendGrid sendgrid = new SendGrid(sendgridApiKey);

			// Instantiate SendGrid request.
			Request request = new Request();
			
			// Set request configuration.
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());

			// Use the client to send the API request.
			Response response = sendgrid.api(request);
			
			if (response.getStatusCode() != 202) {
				logger.log(Level.SEVERE,String.format("An error occured: %s",response.getStatusCode()));
				return response.getStatusCode()+" "+response.getBody();
			}
			// Print response.
			
			msg="Email sent.";
		} catch (IOException e) {
			try {
				throw new ServletException("SendGrid error", e);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		// [END gae_sendgrid]
		logger.log(Level.SEVERE,msg);	
		return msg;
		
	}
	
	
	public String sendMailWithSendGrid(String fromEmail,ArrayList<String> toList,ArrayList<String> ccList,
			ArrayList<String> bccList,String subject,String mailBody,String contentType,
			ByteArrayOutputStream attachmentInBytes,String attachmentName, String attachmentContentType,
			ByteArrayOutputStream attachmentInBytes2,String attachmentName2, String attachmentContentType2,
			ByteArrayOutputStream attachmentInBytes3,String attachmentName3, String attachmentContentType3,
			ByteArrayOutputStream attachmentInBytes4,String attachmentName4, String attachmentContentType4,
			String displayNameForFromEmail){

		Mail mail = new Mail();
		
		String msg="";
		logger.log(Level.SEVERE, "INSIDE SEND SEND GRID EMAIL API ");
		final String sendgridApiKey = System.getenv("SENDGRID_API_KEY");
		final String sendgridSender = System.getenv("SENDGRID_SENDER");
		logger.log(Level.SEVERE, "API KEY updated : "+sendgridApiKey);
		logger.log(Level.SEVERE, "SENDER updated : "+sendgridSender);		
				
			
		// [START gae_sendgrid]
		// Set content for request.
		Email from =null;
		if(fromEmail!=null&&!fromEmail.equals("")){
			from = new Email(fromEmail);
			/**
			 * @author Anil , Date : 10-04-2020
			 * setting from email display name
			 */
			if(displayNameForFromEmail!=null&&!displayNameForFromEmail.equals("")){
				from.setName(displayNameForFromEmail);
			}
			
		}else{
			return "Please add email address.";
		}
		logger.log(Level.SEVERE, "FROM : "+fromEmail);
		mail.setFrom(from);
		
		Personalization personalization = new Personalization();
	  
		
		
		/**
		 * @author Vijay Chougule
		 * Des :- added below code should not add duplicate email id in To cc and BCC
		 * so added to lower because hashset can be duplicate with case sensitive email id's
		 */
		ArrayList<String> toemaillist = new ArrayList<String>();
		if(toList!=null && toList.size()!=0){
			for(String emailId : toList) {
				toemaillist.add(emailId.toLowerCase());
			}
		}
		
		ArrayList<String> ccemaillist = new ArrayList<String>();
		if(ccList!=null&&ccList.size()!=0){
			for(String emailId : ccList) {
				ccemaillist.add(emailId.toLowerCase());
			}
		}
		
		ArrayList<String> bccemaillist = new ArrayList<String>();
		/**
		 * @author Anil @since 14-10-2021
		 * Sending copy to from mail address in order to maintain history of sent mail from send grid
		 * raised by Nitin sir and Harshan 
		 */
		bccemaillist.add(fromEmail);
		
		if(bccList!=null&&bccList.size()!=0){
			for(String emailId : bccList) {
				bccemaillist.add(emailId.toLowerCase());
			}
		}else{
			/**
			 * @author Anil @since 14-10-2021
			 * Sending copy to from mail address in order to maintain history of sent mail from send grid
			 * raised by Nitin sir and Harshan 
			 */
			bccList=new ArrayList<String>();
			bccList.add(fromEmail);
		}
		logger.log(Level.SEVERE, "bccemaillist : " + bccemaillist);
		logger.log(Level.SEVERE, "bccList : " + bccList);
		
		
		/**
		 * Date 04-12-2019 by Vijay
		 *  Des :- Sender id must be unique as per sendgrid to send email
		 *  if duplicate found then sendgrid response as error duplicate instance found
		 *  added hashset to for avoid duplicate email id 
		 */
		if(toemaillist!=null && toemaillist.size()!=0){
			HashSet<String> tohslist = new HashSet<String>(toemaillist);
				for(String toEmail:tohslist){
					logger.log(Level.SEVERE, "TO : "+toEmail);
					Email to = new Email();
				    to.setEmail(toEmail);
				    personalization.addTo(to);
				}
		}
		else{
			return "Please add to email address.";
		}
		
		try {

			if (ccemaillist != null && ccemaillist.size() != 0) {
				ccList.removeAll(toemaillist);
				/**
				 * @author Anil
				 * @since 14-10-2020 removing duplicate email ids from cc by comparing to list
				 */
				HashSet<String> cchslist = new HashSet<String>(ccList);
				logger.log(Level.SEVERE, "cc : " + ccList);
				for (String ccEmail : cchslist) {
					Email cc = new Email();
					cc.setEmail(ccEmail);
					personalization.addCc(cc);
				}
			}

		} catch (Exception e) {

		}
		
		try {

			if (bccemaillist != null && bccemaillist.size() != 0) {
				logger.log(Level.SEVERE, "bccemaillist : " + bccemaillist);
				
				if(toemaillist!=null&&toemaillist.size()!=0) {
					bccList.removeAll(toemaillist);
				}
				if(ccList!=null&&ccList.size()!=0) {
					bccList.removeAll(ccList);
				}
				logger.log(Level.SEVERE, "bccList : " + bccList);
				/**
				 * @author Anil
				 * @since 14-10-2020 removing duplicate email ids from cc bycomparing to list
				 */
				
				HashSet<String> bcchslist = new HashSet<String>(bccList);
				logger.log(Level.SEVERE, "bcc : " + bccList);
				for (String bccEmail : bcchslist) {
					Email bcc = new Email();
					bcc.setEmail(bccEmail);
					personalization.addBcc(bcc);
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "bcc exception: "+e.getStackTrace() );
		}
		
		mail.addPersonalization(personalization);
		
		mail.setSubject(subject);
		
		Content content = new Content();
	    content.setType(contentType);
	    content.setValue(mailBody);
	    mail.addContent(content);
	    
	    
	    if(attachmentInBytes!=null&&attachmentName!=null&&attachmentContentType!=null){
	    	byte[] filedata =attachmentInBytes.toByteArray();
	    	Base64 x = new Base64();
			String dataString = x.encodeAsString(filedata);
			Attachments attachments = new Attachments();
			attachments.setContent(dataString);
			attachments.setType(attachmentContentType);// "application/pdf"
			attachments.setFilename(attachmentName);
			attachments.setDisposition("attachment");
			attachments.setContentId("Banner");
			mail.addAttachments(attachments);
	    	
	    }
	    
	    if(attachmentInBytes2!=null&&attachmentName2!=null&&attachmentContentType2!=null){
	    	byte[] filedata =attachmentInBytes2.toByteArray();
	    	Base64 x = new Base64();
			String dataString = x.encodeAsString(filedata);
			Attachments attachments = new Attachments();
			attachments.setContent(dataString);
			attachments.setType(attachmentContentType2);// "application/pdf"
			attachments.setFilename(attachmentName2);
			attachments.setDisposition("attachment");
			attachments.setContentId("Banner");
			mail.addAttachments(attachments);
	    	
	    }
	    
	    if(attachmentInBytes3!=null&&attachmentName3!=null&&attachmentContentType3!=null){
	    	byte[] filedata =attachmentInBytes3.toByteArray();
	    	Base64 x = new Base64();
			String dataString = x.encodeAsString(filedata);
			Attachments attachments = new Attachments();
			attachments.setContent(dataString);
			attachments.setType(attachmentContentType3);// "application/pdf"
			attachments.setFilename(attachmentName3);
			attachments.setDisposition("attachment");
			attachments.setContentId("Banner");
			mail.addAttachments(attachments);
	    	
	    }
	    
	    if(attachmentInBytes4!=null&&attachmentName4!=null&&attachmentContentType4!=null){
	    	byte[] filedata =attachmentInBytes4.toByteArray();
	    	Base64 x = new Base64();
			String dataString = x.encodeAsString(filedata);
			Attachments attachments = new Attachments();
			attachments.setContent(dataString);
			attachments.setType(attachmentContentType4);// "application/pdf"
			attachments.setFilename(attachmentName4);
			attachments.setDisposition("attachment");
			attachments.setContentId("Banner");
			mail.addAttachments(attachments);
	    	
	    }
	    

	       

		try {
			// Instantiates SendGrid client.
			SendGrid sendgrid = new SendGrid(sendgridApiKey);

			// Instantiate SendGrid request.
			Request request = new Request();
			
			// Set request configuration.
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());

			// Use the client to send the API request.
			Response response = sendgrid.api(request);
			
			if (response.getStatusCode() != 202) {
				logger.log(Level.SEVERE,String.format("An error occured: %s",response.getStatusCode()));
				return response.getStatusCode()+" "+response.getBody();
			}
			// Print response.
			
			msg="Email sent.";
		} catch (IOException e) {
			try {
				throw new ServletException("SendGrid error", e);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		// [END gae_sendgrid]
		logger.log(Level.SEVERE,msg);	
		return msg;
		
	}
	
	
	
}
