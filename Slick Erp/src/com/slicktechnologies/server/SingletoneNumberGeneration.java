package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class SingletoneNumberGeneration {

    private static final SingletoneNumberGeneration singletoneNumGenInstance=new SingletoneNumberGeneration();
    Logger logger = Logger.getLogger("Saving Logger");
    private SingletoneNumberGeneration() {
    }
    public static SingletoneNumberGeneration getSingletonInstance() {

//		if (null == singletoneNumGenInstance) {
//			synchronized (SingletoneNumberGeneration.class) {
//				if (null == singletoneNumGenInstance) {
//					singletoneNumGenInstance = new SingletoneNumberGeneration();
//				}
//			}
//		}
//		return singletoneNumGenInstance;
		
//        if (null == singletoneNumGenInstance) {
//            singletoneNumGenInstance = new SingletoneNumberGeneration();
//        }
        return singletoneNumGenInstance;
    }
    
    public long getLastUpdatedNumber(String processName,long companyId,long updateBy){
    	logger.log(Level.SEVERE,"Class name : "+processName +" Time - "+new Date());
    	long number = 0;
    	NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId",companyId)
				.filter("processName",processName).filter("status", true).first().now();
    	
    	/**
    	 * @author Vijay Date :- 19-10-2022
    	 * Des :- if defualt number generation not found then creating defult number generation to save the data
    	 * but if we select number range on screen then data will not save.
    	 */
    	if(processName!=null && !processName.equals("") && !processName.contains("_") && ng==null) {
    		logger.log(Level.SEVERE, "Number range not defined for "+processName);
    		
    		NumberGeneration ngEntity=new NumberGeneration();
			ngEntity.setCompanyId(companyId);
			ngEntity.setProcessName(processName);
			ngEntity.setNumber(100000000);
			ngEntity.setStatus(true);
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(ngEntity);
			ng = ngEntity;
    		logger.log(Level.SEVERE, "ng "+ng);
    	}
    	/**
    	 * ends here
    	 */
    	
    	if(ng!=null){

    		number=ng.getNumber()+1;
        	ng.setNumber(ng.getNumber()+updateBy);
        	ofy().save().entity(ng).now();
        	logger.log(Level.SEVERE,"Number genration number "+number);

    	}
    	else{
    		/**
    		 * @author Vijay Date :- 10-11-2020
    		 * Des :- if Inhouse Number generation not defined in the system then inhouse service will create with
    		 * normal (Service) series number generation
    		 */
    		if(processName.equals(AppConstants.INHOUSESERVICE)){
    			NumberGeneration numbGen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId)
    					.filter("processName","Service").filter("status", true).first().now();
    			number=numbGen.getNumber()+1;
    			numbGen.setNumber(numbGen.getNumber()+updateBy);
            	ofy().save().entity(numbGen).now();
    		}
    		/**
			 * @author Anil @since 26-02-2021
			 * if number generation is not defined then only document getting saved
			 * issue raised by Vaishnavi for Sarthak regarding cron job configuration
			 * so in that condition terminating the code by creating exception
			 */
    		else{
    			number=ng.getNumber()+1;
            	ng.setNumber(ng.getNumber()+updateBy);
            	ofy().save().entity(ng).now();
    		}
    	}
    	
    	return number;
    }
    
    
    
    
}
