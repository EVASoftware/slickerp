package com.slicktechnologies.server.deadstockservice;


import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;


public class DeadStockWebservice extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5306880616468413724L;

	double totalAmtFix = 0;
	double netPayFix = 0;
	public static String APPID = "";
	public static long AUTHCODE = 0l;
	Logger logger = Logger.getLogger("DeadStockWebservice.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		RegisterServiceImpl impl = new RegisterServiceImpl();
		impl.getClass();

		String jsonWareHouseString = null;
		String wareHouseDataString = null;;
		jsonWareHouseString = req.getParameter("data").trim();
		if(jsonWareHouseString!=null){
			if(jsonWareHouseString.trim().contains("hasand")){
				wareHouseDataString=jsonWareHouseString.trim().replace("hasand", "&");
			}else{
				wareHouseDataString=jsonWareHouseString.trim();
			}
		}
		logger.log(Level.SEVERE,"JSON DATA:::"+wareHouseDataString);
		String sendResponse = null;

		if (wareHouseDataString != null) {
			JSONObject obj = null;
			try {
				obj = new JSONObject(wareHouseDataString);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				sendResponse="ERROR in parsing JSON!!!!";
			}
			if (!obj.optString("actionTask").trim().equalsIgnoreCase("NA")
					&& obj.optString("actionTask").trim().length() > 0) {
			
			if(obj.optString("actionTask").trim().equalsIgnoreCase("wareHouseInterface")){
				sendResponse = createWareHouse(jsonWareHouseString);
//			}else if(obj.optString("actionTask").trim().equalsIgnoreCase("purchaseOrderInterface")){
//				sendResponse= createPurchaseOrder(jsonWareHouseString);
			}else{
				sendResponse="Unable to find actionTask = "+obj.optString("actionTask").trim(); 
			}
			}else{
				sendResponse="actionTask is mandatory!!!";
			}
		}
		
		resp.getWriter().println(sendResponse);

	}

	private String createPurchaseOrder(String jsonWareHouseString) {
		// TODO Auto-generated method stub
		JSONObject jObj = null;
		try {
			jObj = new JSONObject(jsonWareHouseString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long companyId = Long.parseLong(jObj.optString("authCode").trim());
		String url = jObj.optString("url").trim();
		String[] urlArray = url.split("\\.");
		String accessUrl = urlArray[0];
		String applicationId = urlArray[1];
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl.trim()).first().now();
		AUTHCODE = company.getCompanyId();
		APPID = company.getAccessUrl();

		if (companyId == AUTHCODE) {
			
			PurchaseOrder po=new PurchaseOrder();
			String prNumber;
			if (!jObj.optString("PRNumber").trim().equalsIgnoreCase("NA")
					&& jObj.optString("PRNumber").trim().length() > 0) {
				prNumber=jObj.optString("PRNumber").trim();
			}else{
				return "PRNumber is mandatory!!";
			}
			/*
			 * PR ref Details
			 */
			PurchaseRequisition pr=ofy().load().type(PurchaseRequisition.class).filter("companyId",companyId).filter("count", Integer.parseInt(prNumber)).first().now();
			if(pr!=null){
				po.setRefOrderNO(pr.getCount()+"");
				po.setPrDate(pr.getRefOrderDate());
				po.setResponseDate(pr.getResponseDate());
				po.setEmployee(pr.getEmployee());
			}else{
				return "PR not Found!!!!";
			}
			/*
			 * PR ref Details Done
			 */
			
			/*
			 * PO info
			 */
			if (!jObj.optString("poTitle").trim().equalsIgnoreCase("NA")
					&& jObj.optString("poTitle").trim().length() > 0) {
				po.setTitle(jObj.optString("poTitle").trim());
			}else{
				po.setTitle("");
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			if (!jObj.optString("poDate").trim().equalsIgnoreCase("NA")
					&& jObj.optString("poDate").trim().length() > 0) {
				try {
					po.setPODate(sdf.parse(jObj.optString("poDate").trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "ERROR:: Po Date Parsing Error -"+ jObj.optString("poDate").trim();
				}
			}else{
//				po.setTitle("");
			}
			if (!jObj.optString("exceptedDeliveryDate").trim().equalsIgnoreCase("NA")
					&& jObj.optString("exceptedDeliveryDate").trim().length() > 0) {
				try {
					po.setDeliveryDate(sdf.parse(jObj.optString("exceptedDeliveryDate").trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "ERROR parsing exceptedDeliveryDate";
				}
				
			}else{
//				po.setTitle("");
			}
			if (!jObj.optString("poCategory").trim().equalsIgnoreCase("NA")
					&& jObj.optString("poCategory").trim().length() > 0) {
				po.setCategory(jObj.optString("poCategory").trim());
			}else{
				po.setCategory("");
			}
			if (!jObj.optString("poType").trim().equalsIgnoreCase("NA")
					&& jObj.optString("poType").trim().length() > 0) {
				po.setType(jObj.optString("poType").trim());
			}else{
				po.setType("");
			}
			if (!jObj.optString("branch").trim().equalsIgnoreCase("NA")
					&& jObj.optString("branch").trim().length() > 0) {
				po.setBranch(jObj.optString("branch").trim());
			}else{
				po.setBranch("");
			}
			
			if (!jObj.optString("approverName").trim().equalsIgnoreCase("NA")
					&& jObj.optString("approverName").trim().length() > 0) {
				po.setApproverName(jObj.optString("approverName").trim());
				
			}else{
				po.setApproverName("");
			}
			if (!jObj.optString("approverRemark").trim().equalsIgnoreCase("NA")
					&& jObj.optString("approverRemark").trim().length() > 0) {
				po.setRemark(jObj.optString("approverRemark").trim());
				
			}else{
				po.setRemark("");
			}
			
			if (!jObj.optString("paymentMethod").trim().equalsIgnoreCase("NA")
					&& jObj.optString("paymentMethod").trim().length() > 0) {
				po.setPaymentMethod(jObj.optString("paymentMethod").trim());
			}else{
				po.setPaymentMethod("");
			}
			
			if (!jObj.optString("cForm").trim().equalsIgnoreCase("YES")) {
				po.setcForm(jObj.optString("cForm").trim());
				if (!jObj.optString("cstPercent").trim().equalsIgnoreCase("NA")
						&& jObj.optString("cstPercent").trim().length() > 0) {
					po.setCstpercent(Double.parseDouble(jObj.optString("cstPercent").trim()));
				}else{
					return "Cst Percent is mandatory";
				}
			}else if (!jObj.optString("cForm").trim().equalsIgnoreCase("NO")) {
				po.setcForm(jObj.optString("cForm").trim());
			}else{
				
				return "Please provide CForm value as Yes or No";
			}
			
			po.setStatus(PurchaseOrder.APPROVED);
			
			JSONArray jPayMentArray;
			try {
				jPayMentArray=jObj.getJSONArray("paymentDetails");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Error parsing Payment Details";
			}
			
			/*
			 * PO information ends here
			 */
			
			/*
			 * Payment Terms
			 */
			List<PaymentTerms> paymentTermsList=new ArrayList<PaymentTerms>();
			for (int i = 0; i < jPayMentArray.length(); i++) {
				JSONObject obj;
				try {
					obj=jPayMentArray.getJSONObject(i);
					PaymentTerms paymentTerms=new PaymentTerms();
					try{
					paymentTerms.setPayTermDays(Integer.parseInt(obj.optString("paymentDay")));
					}catch(Exception e){
						e.printStackTrace();
						return "Error parsing paymentDay";
					}
					try{
					paymentTerms.setPayTermPercent(Double.parseDouble(obj.optString("paymentPercent")));
					}catch(Exception e){
						e.printStackTrace();
						return "Error parsing paymentPercent";
					}
					paymentTerms.setPayTermComment(obj.optString("paymentComment"));
					paymentTermsList.add(paymentTerms);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "Error Parsing payment details"+i;
				}
				
				
			}
			po.setPaymentTermsList(paymentTermsList);
			/*
			 * End of Payment Terms
			 */
			
			/*
			 * Vendor Info
			 */
			JSONArray jVendorArray;
			try {
				jVendorArray=jObj.getJSONArray("vendorDetails");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "ERROR : Parsing Vendor Details";
			}
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "Vendor").filter("status", true)
					.first().now();
			long number = ng.getNumber();
			int count = (int) number;
			List<VendorDetails> vendorList=new ArrayList<VendorDetails>();
			
			for (int i = 0; i < jVendorArray.length(); i++) {
				JSONObject objVendor = null;
				try {
					objVendor = jVendorArray.getJSONObject(i);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "ERROR: Parsing Vendor Details of row "+i;					
				}
				
				Vendor vendor=ofy().load().type(Vendor.class).filter("companyId", companyId).filter("refNo", objVendor.optString("vendorRefNo").trim()).first().now();
				
				if(vendor!=null){
					VendorDetails vendorDetails=new VendorDetails();
					vendorDetails.setVendorId(vendor.getCount());
					vendorDetails.setVendorName(vendor.getVendorName());
					vendorDetails.setVendorPhone(vendor.getCellNumber1());
					vendorDetails.setPocName(vendor.getPointOfContact().getFullname());
					vendorDetails.setStatus(true);
					vendorList.add(vendorDetails);
//					vendorDetails.se
				}else{
					Object object=createNewVendor(objVendor,companyId,count);
					if(object instanceof Vendor){
						Vendor vendor1=(Vendor) object;
						VendorDetails vendorDetails=new VendorDetails();
						vendorDetails.setVendorId(vendor1.getCount());
						vendorDetails.setVendorName(vendor1.getVendorName());
						vendorDetails.setVendorPhone(vendor1.getCellNumber1());
						vendorDetails.setPocName(vendor1.getPointOfContact().getFullname());
						vendorDetails.setStatus(true);
						vendorList.add(vendorDetails);
					}else if(object instanceof String){
						return object.toString().trim();
					}else{
						return "Unable to create Vendor";
					}
					
				}
				
			}
			ng.setNumber(count);
			GenricServiceImpl vendOrImpl=new GenricServiceImpl();
			vendOrImpl.save(ng);
			
			po.setVendorDetails(vendorList);
			/*
			 * End of Vendor Info
			 */
			
			if (!jObj.optString("header").trim().equalsIgnoreCase("NA")
					&& jObj.optString("header").trim().length() > 0) {
				po.setHeader(jObj.optString("header").trim());
			}else{
				return "Header is Mandotory";
			}
			
			if (!jObj.optString("footer").trim().equalsIgnoreCase("NA")
					&& jObj.optString("footer").trim().length() > 0) {
				po.setFooter(jObj.optString("footer"));
			}else{
				return "Footer is Mandotory";
			}
			
			/*
			 * 
			 */
			/**
			 * Delivery Address
			 */
			if (!jObj.optString("isDeliveryAddressDiff").trim().equalsIgnoreCase("Yes")
					&& jObj.optString("isDeliveryAddressDiff").trim().length() > 0) {
				Address address=new Address();
				if (!jObj.optString("dAddressLine1").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dAddressLine1").trim().length() > 0) {
					address.setAddrLine1(jObj.optString("dAddressLine1").trim());
				}else{
					return "dAddressLine1 is Mandotory";
				}
				
				if (!jObj.optString("dAddressLine2").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dAddressLine2").trim().length() > 0) {
					address.setAddrLine2(jObj.optString("dAddressLine2").trim());
				}else{
					address.setAddrLine2("");
				}
				
				if (!jObj.optString("dLandMark").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dLandMark").trim().length() > 0) {
					address.setLandmark(jObj.optString("dLandMark").trim());
				}else{
					address.setLandmark("");
				}
				if (!jObj.optString("dCountry").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dCountry").trim().length() > 0) {
					address.setLandmark(jObj.optString("dCountry").trim());
				}else{
					return "dCountry is Mandotory";
				}
				if (!jObj.optString("dState").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dState").trim().length() > 0) {
					address.setState(jObj.optString("dState").trim());
				}else{
					return "dState is Mandotory";
				}
				if (!jObj.optString("dCity").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dCity").trim().length() > 0) {
					address.setCity(jObj.optString("dCity").trim());
				}else{
					return "dCity is Mandotory";
				}
				if (!jObj.optString("dLocality").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dLocality").trim().length() > 0) {
					address.setLocality(jObj.optString("dLocality").trim());
				}else{
					address.setLocality("");
				}
				
				if (!jObj.optString("dPin").trim().equalsIgnoreCase("NA")
						&& jObj.optString("dPin").trim().length() > 0) {
					try{
					address.setPin(Long.parseLong(jObj.optString("dPin").trim()));
					}catch(Exception e){
						return "ERROR in dPin"+e;
					}
				}else{
					return "dPin is Mandotory";
				}
			}else{
				return "isDeliveryAddressDiff is Mandotory";
			}
			/**
			 * 
			 */
			
			/**
			 * Product Details
			 */
			JSONArray productAraay = null;
			
			try {
				productAraay=jObj.getJSONArray("productDetails");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<ProductDetailsPO> prodDetailsList=new ArrayList<ProductDetailsPO>();
			List<ItemProduct> itemProductListPO=ofy().load().type(ItemProduct.class).filter("companyId", companyId).list();
			List<TaxDetails> vattaxList=ofy().load().type(TaxDetails.class).filter("companyId", companyId).filter("isVatTax", true).list();
			List<TaxDetails> sertaxList=ofy().load().type(TaxDetails.class).filter("companyId", companyId).filter("isServiceTax", true).list();
			List<WareHouse> warehouseList=ofy().load().type(WareHouse.class).filter("companyId", companyId).list();
			
			for (int i = 0; i < productAraay.length(); i++) {
				ProductDetailsPO productDetails=new ProductDetailsPO();
				JSONObject jproductObj = null;
				try {
					jproductObj=productAraay.getJSONObject(i);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try{
				productDetails.setProductID(Integer.parseInt(jproductObj.optString("prodID").trim()));
				}catch(Exception e){
					return "ERROR : prodID parsing error";
				}
				boolean productPresent=false;
				for (int j = 0; j < itemProductListPO.size(); j++) {
					if(itemProductListPO.get(j).getProductCode().trim().equalsIgnoreCase(jproductObj.optString("prodCode").trim())){
						productDetails.setPrduct(itemProductListPO.get(j));
						productDetails.setProductID(itemProductListPO.get(j).getCount());
						productDetails.setProductName(itemProductListPO.get(j).getProductName().trim());
						productDetails.setProductCode(itemProductListPO.get(j).getProductCode().trim());
						productDetails.setProductCategory(itemProductListPO.get(j).getProductCategory().trim());
						try{
						productDetails.setProdPrice(Double.parseDouble(jproductObj.optString("prodPrice").trim()));
						}catch(Exception e){
							e.printStackTrace();
							return "ERROR in prodPrice "+e;
						}
						productDetails.setUnitOfmeasurement(itemProductListPO.get(j).getUnitOfMeasurement().trim());
						double vatPercent;
						if(jproductObj.optString("vatTaxPercent").trim().length()>0){
							vatPercent=Double.parseDouble(jproductObj.optString("vatTaxPercent").trim());
						}else{
							vatPercent=0;
						}
						String vatInclusive=jproductObj.optString("vatInclusive").trim();
						double servicePercent;
						if(jproductObj.optString("serviceTaxPercent").trim().length()>0){
							servicePercent=Double.parseDouble(jproductObj.optString("serviceTaxPercent").trim());
						}else{
							servicePercent=0;
						}
						String serInclusive=jproductObj.optString("serInclusive").trim();
						boolean vatPresent=false;
						boolean serPresent=false;
						
						for (int k = 0; k < vattaxList.size(); k++) {
							if(vattaxList.get(k).getTaxChargePercent()==vatPercent){
								Tax tax=new Tax();
								tax.setTaxConfigName(vattaxList.get(k).getTaxChargeName().trim());
								tax.setPercentage(vattaxList.get(k).getTaxChargePercent());
								if(vatInclusive.trim().equalsIgnoreCase("Yes")){
									tax.setInclusive(true);
								}else{
									tax.setInclusive(false);
								}
								productDetails.setVatTax(tax);
								vatPresent=true;
							}
						}
						if(!vatPresent){
							return "Given VAT Percent does not exceeds";
						}

						for (int k = 0; k < sertaxList.size(); k++) {
							if(sertaxList.get(k).getTaxChargePercent()==servicePercent){
								Tax tax=new Tax();
								tax.setTaxConfigName(sertaxList.get(k).getTaxChargeName().trim());
								tax.setPercentage(sertaxList.get(k).getTaxChargePercent());
								if(serInclusive.trim().equalsIgnoreCase("Yes")){
									tax.setInclusive(true);
								}else{
									tax.setInclusive(false);
								}
								productDetails.setServiceTax(tax);
								serPresent=true;
							}
						}
						if(!serPresent){
							return "Given Service Percent does not exceeds";
						}
						try{
						productDetails.setProductQuantity(Double.parseDouble(jproductObj.optString("productQuantity").trim()));
						}catch(Exception e){
							return "Error in product quantity!!";
						}
						if(jproductObj.optString("percentDiscount").trim().length()>0&&jproductObj.optString("discountAmount").trim().length()==0){
							try{
							productDetails.setDiscount(Double.parseDouble(jproductObj.optString("percentDiscount").trim()));
							}catch(Exception e){
								return "Error in Discount Percent"+e;
							}
						}else if(jproductObj.optString("percentDiscount").trim().length()==0&&jproductObj.optString("discountAmount").trim().length()>0){
							try{
								productDetails.setDiscountAmt(Double.parseDouble(jproductObj.optString("discountAmount").trim()));
							}catch(Exception e){
								return "Error in Discount amount "+e;
							}
						}else{
							return "Please provide discount in percentage or Discount in amount. Both cannot be applicable";
						}
						try{
						productDetails.setTotal(Double.parseDouble(jproductObj.optString("totalAmount").trim()));
						}catch(Exception e){
							e.printStackTrace();
							return "ERROR in totalAmount";
						}
						try {
							productDetails.setProdDate(sdf.parse(jproductObj.optString("productDeliveryDate").trim()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return "ERROR in productDeliveryDate "+e;
						}
						boolean warehousePresent=false;
						for (int k = 0; k < warehouseList.size(); k++) {
							if(warehouseList.get(k).getWarehouseCode().trim().equalsIgnoreCase(jproductObj.optString("warehouseCode").trim())){
								productDetails.setWarehouseName(warehouseList.get(k).getBusinessUnitName().trim());
								po.setAdress(warehouseList.get(k).getAddress());
								warehousePresent=true;
							}
						}
						if(!warehousePresent){
							return "Warehouse Code does not exists!!";
						}
						try{
						productDetails.setAvailableStock(Double.parseDouble(jproductObj.optString("availableStock").trim()));
						}catch(Exception e){
							e.printStackTrace();
							return "Error in availableStock "+e;
						}
						productPresent=true;
					}
				}
				if(!productPresent){
					return "Product Code = "+jproductObj.optString("prodCode").trim()+" is not present";
				}
				
				
				prodDetailsList.add(productDetails);
			}
			
			po.setProductDetails(prodDetailsList);
			/**
			 * Taxes Table
			 * 
			 */
			po.setProductTaxes(calculateTaxTable(prodDetailsList));
			/**
			 * 
			 */
			po.setTotal(totalAmtFix);
			po.setTotalAmount(netPayFix);
			po.setNetpayble(netPayFix);
		}else{
			return "Authentication Failed";
		}
		
		
		
		
		
		return "PO created Successfully";
	}

	private List<ProductOtherCharges> calculateTaxTable(
			List<ProductDetailsPO> prodDetailsList) {
		List<ProductOtherCharges> productOtherChargesList = new ArrayList<ProductOtherCharges>();

		// vat tax part
		// productOtherCharges.setChargeName(serviceProductItem
		// .getVatTax().getTaxConfigName().trim());
		
		double totalAmount = 0;
		double netPayable = 0;
		double assessableAmount = 0;

		for (ProductDetailsPO productDetailsPO : prodDetailsList) {
			double quantity = productDetailsPO.getProductQuantity();
			logger.log(Level.SEVERE,
					"serviceProductItem.getVatTax().getTaxConfigName().trim()"
							+ productDetailsPO.getVatTax()
									.getTaxConfigName().trim());
			logger.log(Level.SEVERE,
					"serviceProductItem.getVatTax().getTaxName().trim()"
							+ productDetailsPO.getVatTax()
									.getTaxName().trim());

			if (((productDetailsPO.getVatTax() != null) && (productDetailsPO
					.getVatTax().isInclusive()))
					&& (productDetailsPO.getServiceTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Inclusive and Service Tax Not Present::::::::::::");
				// this is the value on which the tax will get
				// calculated
				// Formula : (Price-Price*VAT%) .....Because it is
				// inclusive
				assessableAmount = ((productDetailsPO.getProdPrice() * quantity) - ((productDetailsPO
						.getProdPrice() * quantity * productDetailsPO
						.getVatTax().getPercentage()) / 100));
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = assessableAmount;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(productDetailsPO
								.getVatTax().getTaxConfigName()
								.trim());
				productOtherCharges
						.setChargePercent(productDetailsPO
								.getVatTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* productDetailsPO.getVatTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				totalAmtFix = totalAmtFix + totalAmount;
				netPayable = productDetailsPO.getProdPrice();
				netPayFix = netPayFix + netPayable;
				productOtherChargesList.add(productOtherCharges);
			} else if (((productDetailsPO.getVatTax() != null) && !(productDetailsPO
					.getVatTax().isInclusive()))
					&& (productDetailsPO.getServiceTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Exclusive and Service Tax Not Present::::::::::::");
				assessableAmount = productDetailsPO.getProdPrice()
						* quantity;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = productDetailsPO.getProdPrice()
						* quantity;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(productDetailsPO
								.getVatTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(productDetailsPO
								.getVatTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* productDetailsPO.getVatTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = totalAmount + chargePayable;
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			} else if (((productDetailsPO.getServiceTax() != null) && (productDetailsPO
					.getServiceTax().isInclusive()))
					&& (productDetailsPO.getVatTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax inclusive::::::::::::");
				assessableAmount = ((productDetailsPO.getProdPrice() * quantity) - ((productDetailsPO
						.getProdPrice() * quantity * productDetailsPO
						.getServiceTax().getPercentage()) / 100));
				totalAmount = assessableAmount;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(productDetailsPO
								.getServiceTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(productDetailsPO
								.getServiceTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* productDetailsPO.getServiceTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = productDetailsPO.getProdPrice();
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			} else if (((productDetailsPO.getServiceTax() != null) && !(productDetailsPO
					.getServiceTax().isInclusive()))
					&& (productDetailsPO.getVatTax() == null)) {

				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax Exclusive::::::::::::");
				assessableAmount = productDetailsPO.getProdPrice()
						* quantity;
				logger.log(Level.SEVERE,
						"assessableAmount::::::::::::"
								+ assessableAmount);
				totalAmount = productDetailsPO.getProdPrice()
						* quantity;
				ProductOtherCharges productOtherCharges = new ProductOtherCharges();
				productOtherCharges
						.setChargeName(productDetailsPO
								.getServiceTax().getTaxConfigName());
				productOtherCharges
						.setChargePercent(productDetailsPO
								.getServiceTax().getPercentage());
				productOtherCharges
						.setAssessableAmount(assessableAmount);
				double chargePayable = assessableAmount
						* productDetailsPO.getServiceTax()
								.getPercentage() / 100;
				productOtherCharges.setChargePayable(chargePayable);
				logger.log(Level.SEVERE,
						"chargePayable::::::::::::" + chargePayable);
				netPayable = totalAmount + chargePayable;
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			} else if (productDetailsPO.getServiceTax() != null
					&& productDetailsPO.getVatTax() != null) {

				logger.log(Level.SEVERE,
						"Vat Tax Present  and Service Tax Present::::::::::::");

				if (productDetailsPO.getServiceTax()
						.isInclusive()
						&& productDetailsPO.getVatTax()
								.isInclusive()) {
					logger.log(Level.SEVERE,
							"Vat Tax Inclusive and Service Tax Inclusive::::::::::::");

					netPayable = productDetailsPO.getProdPrice()
							* quantity;
					// For Tax
					double assessableAmountForST = (netPayable * 100)
							/ (100 + productDetailsPO
									.getServiceTax()
									.getPercentage());
					logger.log(Level.SEVERE,
							"assessableAmountForST::::::::::::"
									+ assessableAmountForST);

					double assessableAmountForVT = (assessableAmountForST * 100)
							/ (100 + productDetailsPO.getVatTax()
									.getPercentage());
					logger.log(Level.SEVERE,
							"assessableAmountForVT::::::::::::"
									+ assessableAmountForVT);
					totalAmount = assessableAmountForVT;
					totalAmtFix = totalAmtFix + totalAmount;
					netPayFix = netPayFix + netPayable;

					// For VAT
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(productDetailsPO
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(productDetailsPO
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountForVT);
					double chargePayableVT = assessableAmountForVT
							* productDetailsPO.getVatTax()
									.getPercentage() / 100;
					productOtherCharges
							.setChargePayable(chargePayableVT);
					logger.log(Level.SEVERE,
							"chargePayableVT::::::::::::"
									+ chargePayableVT);
					productOtherChargesList
							.add(productOtherCharges);

					// For ST
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(productDetailsPO
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(productDetailsPO
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountForST);
					double chargePayableST = assessableAmountForST
							* productDetailsPO.getServiceTax()
									.getPercentage() / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);

				} else if (!(productDetailsPO.getServiceTax()
						.isInclusive())
						&& !(productDetailsPO.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Exclusive and Service Tax Exclusive::::::::::::");
					totalAmount = productDetailsPO.getProdPrice();
					double assessableAmountForVT = totalAmount;
					logger.log(Level.SEVERE,
							"assessableAmountForVT::::::::::::"
									+ assessableAmountForVT);
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(productDetailsPO
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(productDetailsPO
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountForVT);
					double chargePayableVT = (assessableAmountForVT * productDetailsPO
							.getVatTax().getPercentage()) / 100;
					productOtherCharges
							.setChargePayable(chargePayableVT);
					logger.log(Level.SEVERE,
							"chargePayableST::::::::::::"
									+ chargePayableVT);
					productOtherChargesList
							.add(productOtherCharges);

					// For ST
					double assessableAmountForST = totalAmount
							+ chargePayableVT;

					logger.log(Level.SEVERE,
							"assessableAmountForST::::::::::::"
									+ assessableAmountForST);
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(productDetailsPO
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(productDetailsPO
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountForST);
					double chargePayableST = assessableAmountForST
							* productDetailsPO.getServiceTax()
									.getPercentage() / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					netPayable = assessableAmountForST
							+ chargePayableST;
					totalAmtFix = totalAmtFix + totalAmount;
					netPayFix = netPayFix + netPayable;
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);
				} else if (productDetailsPO.getServiceTax()
						.isInclusive()
						&& !(productDetailsPO.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Exclusive and Service Tax Inclusive::::::::::::");
					// ServiceTax=Inclusive && VatTax=Exclusive....
					// Here we are adding VAT TAX first because it
					// is
					// exclusive, and calculating NetPayable value.
					// The
					// we will do reverse calculation
					double netPay = (productDetailsPO.getProdPrice() * productDetailsPO
							.getVatTax().getPercentage()) / 100;
					netPayable = netPay;

					double assessableAmountST = (netPayable * 100)
							/ (100 + productDetailsPO
									.getServiceTax()
									.getPercentage());
					double assessableAmountVT = (assessableAmountST * 100)
							/ (100 + productDetailsPO.getVatTax()
									.getPercentage());

					totalAmount = assessableAmountVT;

					// Now we will calculate VAT TAX
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(productDetailsPO
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(productDetailsPO
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assessableAmountVT);
					productOtherCharges
							.setChargePayable((assessableAmountVT * productDetailsPO
									.getVatTax().getPercentage()) / 100);
					productOtherChargesList
							.add(productOtherCharges);

					// First we are calculating Service Tax of the
					// product to find assessable amount to show in
					// product
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(productDetailsPO
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(productDetailsPO
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assessableAmountST);
					productOtherChargesST
							.setChargePayable((assessableAmountST * productDetailsPO
									.getServiceTax()
									.getPercentage()) / 100);
					productOtherChargesList
							.add(productOtherChargesST);
					totalAmtFix = totalAmtFix + totalAmount;
					netPayFix = netPayFix + netPayable;

				} else if (!(productDetailsPO.getServiceTax()
						.isInclusive())
						&& (productDetailsPO.getVatTax()
								.isInclusive())) {

					logger.log(Level.SEVERE,
							"Vat Tax Inclusive and Service Tax Exclusive ::::::::::::");
					// ServiceTax=Exclusive && VatTax=Inclusive....
					double assesableAmountST = productDetailsPO
							.getProdPrice();
					double assesableAmountVT = (assesableAmountST * 100)
							/ (100 + productDetailsPO
									.getServiceTax()
									.getPercentage());
					totalAmount = assesableAmountVT;
					netPayable = assesableAmountST;

					netPayFix = netPayFix + netPayable;

					totalAmtFix = totalAmtFix + totalAmount;

					// Calculation of VAT TAX
					ProductOtherCharges productOtherCharges = new ProductOtherCharges();
					productOtherCharges
							.setChargeName(productDetailsPO
									.getVatTax().getTaxConfigName());
					productOtherCharges
							.setChargePercent(productDetailsPO
									.getVatTax().getPercentage());
					productOtherCharges
							.setAssessableAmount(assesableAmountVT);
					double chargePayable = (assesableAmountVT * productDetailsPO
							.getVatTax().getPercentage()) / 100;
					productOtherCharges
							.setChargePayable(chargePayable);
					logger.log(Level.SEVERE,
							"chargePayable::::::::::::"
									+ chargePayable);
					productOtherChargesList
							.add(productOtherCharges);
					// Calculation for Service Tax
					ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
					productOtherChargesST
							.setChargeName(productDetailsPO
									.getServiceTax()
									.getTaxConfigName());
					productOtherChargesST
							.setChargePercent(productDetailsPO
									.getServiceTax()
									.getPercentage());
					productOtherChargesST
							.setAssessableAmount(assesableAmountST);
					double chargePayableST = (assesableAmountST * productDetailsPO
							.getServiceTax().getPercentage()) / 100;
					productOtherChargesST
							.setChargePayable(chargePayableST);
					logger.log(Level.SEVERE,
							"chargePayableST::::::::::::"
									+ chargePayableST);
					productOtherChargesList
							.add(productOtherChargesST);
				}

			} else {
				logger.log(Level.SEVERE,
						"Vat Tax Not Present and Service Tax Not Present::::::::::::");
				totalAmount = productDetailsPO.getProdPrice();
				netPayable = totalAmount;
				totalAmtFix = totalAmtFix + totalAmount;
				netPayFix = netPayFix + netPayable;
			}
		}
		return productOtherChargesList;
	}

	private Object createNewVendor(JSONObject objVendor, long companyId, int count) {
		// TODO Auto-generated method stub
		
		
		Vendor vendor=new Vendor();
		if(objVendor.optString("vendorName").trim()!=null&&objVendor.optString("vendorName").trim().length()>0){
		vendor.setVendorName(objVendor.optString("vendorName").trim());
		}else{
			return "Vendor name is mandatory!!";
		}
		if(objVendor.optString("pocFullName").trim()!=null&&objVendor.optString("pocFullName").trim().length()>0){
			Person person=new Person();
			person.setFullname(objVendor.optString("pocFullName").trim());
			vendor.setPointOfContact(person);
			}else{
				return "PocFullName is mandatory!!";
			}
		vendor.setVendorStatus(true);
		if(objVendor.optString("vendorEmail").trim()!=null&&objVendor.optString("vendorEmail").trim().length()>0){
			vendor.setEmail(objVendor.optString("vendorEmail").trim());
			}else{
				return "Vendor Email is mandatory!!";
			}
		if(objVendor.optString("vendorLandLine").trim()!=null&&objVendor.optString("vendorLandLine").trim().length()>0){
			vendor.setLandline(Long.parseLong(objVendor.optString("vendorLandLine").trim()));
			}else{
				
			}
		if(objVendor.optString("cellPhone1").trim()!=null&&objVendor.optString("cellPhone1").trim().length()>0){
			vendor.setCellNumber1(Long.parseLong(objVendor.optString("cellPhone1").trim()));
			}else{
				return "cellPhone1 is mandatory!!";
			}
		if(objVendor.optString("cellPhone2").trim()!=null&&objVendor.optString("cellPhone2").trim().length()>0){
			vendor.setCellNumber2(Long.parseLong(objVendor.optString("cellPhone2").trim()));
			}else{
//				return "Vendor name is mandatory!!";
			}
		if(objVendor.optString("faxNumber").trim()!=null&&objVendor.optString("faxNumber").trim().length()>0){
			vendor.setFaxNumber(Long.parseLong(objVendor.optString("faxNumber").trim()));
			}else{
//				return "Vendor name is mandatory!!";
			}
		if(objVendor.optString("vendorGroup").trim()!=null&&objVendor.optString("vendorGroup").trim().length()>0){
			vendor.setGroup(objVendor.optString("vendorGroup").trim());
			}else{
//				return "Vendor name is mandatory!!";
			}
		if(objVendor.optString("vendorCategory").trim()!=null&&objVendor.optString("vendorCategory").trim().length()>0){
			vendor.setCategory(objVendor.optString("vendorCategory").trim());
			}else{
//				return "Vendor name is mandatory!!";
			}
		if(objVendor.optString("vendorType").trim()!=null&&objVendor.optString("vendorType").trim().length()>0){
			vendor.setType(objVendor.optString("vendorType").trim());
			}else{
//				return "Vendor name is mandatory!!";
			}
//		if(objVendor.optString("vendorRefNum").trim()!=null&&objVendor.optString("vendorRefNum").trim().length()>0){
//			vendor.setRefNo(objVendor.optString("vendorRefNum").trim());
//			}else{
////				return "Vendor name is mandatory!!";
//				vendor.setRefNo("");
//			}
		Address address=new Address();
		if(objVendor.optString("addressLine1").trim()!=null&&objVendor.optString("addressLine1").trim().length()>0){
			address.setAddrLine1(objVendor.optString("addressLine1").trim());
			}else{
				return "Vendor addressLine1 is mandatory!!";
			}
		if(objVendor.optString("addressLine2").trim()!=null&&objVendor.optString("addressLine2").trim().length()>0){
			address.setAddrLine2(objVendor.optString("addressLine2").trim());
			}else{
//				return "Vendor addressLine1 is mandatory!!";
				address.setAddrLine2("");
			}
		if(objVendor.optString("landMark").trim()!=null&&objVendor.optString("landMark").trim().length()>0){
			address.setLandmark(objVendor.optString("landMark").trim());
			}else{
			address.setLandmark("");
			}
		if(objVendor.optString("country").trim()!=null&&objVendor.optString("country").trim().length()>0){
			address.setCountry(objVendor.optString("country").trim());
			}else{
				return "Vendor country is mandatory!!";
			}
		if(objVendor.optString("state").trim()!=null&&objVendor.optString("state").trim().length()>0){
			address.setState(objVendor.optString("state").trim());
			}else{
				return "Vendor state is mandatory!!";
			}
		if(objVendor.optString("city").trim()!=null&&objVendor.optString("city").trim().length()>0){
			address.setCity(objVendor.optString("city").trim());
			}else{
				return "Vendor city is mandatory!!";
			}
		if(objVendor.optString("locality").trim()!=null&&objVendor.optString("locality").trim().length()>0){
			address.setLocality(objVendor.optString("locality").trim());
			}else{
				address.setLocality("");
			}
		if(objVendor.optString("pin").trim()!=null&&objVendor.optString("pin").trim().length()>0){
			address.setPin(Long.parseLong(objVendor.optString("pin").trim()));
			}else{
				return "Vendor pin is mandatory!!";
			}
		
			vendor.setPrimaryAddress(address);
			
			if(objVendor.optString("saddressLine1").trim()!=null&&objVendor.optString("saddressLine1").trim().length()>0){
				address.setAddrLine1(objVendor.optString("saddressLine1").trim());
				}else{
					return "Vendor saddressLine1 is mandatory!!";
				}
			if(objVendor.optString("saddressLine2").trim()!=null&&objVendor.optString("saddressLine2").trim().length()>0){
				address.setAddrLine2(objVendor.optString("saddressLine2").trim());
				}else{
//					return "Vendor addressLine1 is mandatory!!";
					address.setAddrLine2("");
				}
			if(objVendor.optString("slandMark").trim()!=null&&objVendor.optString("slandMark").trim().length()>0){
				address.setLandmark(objVendor.optString("slandMark").trim());
				}else{
				address.setLandmark("");
				}
			if(objVendor.optString("scountry").trim()!=null&&objVendor.optString("scountry").trim().length()>0){
				address.setCountry(objVendor.optString("scountry").trim());
				}else{
					return "Vendor scountry is mandatory!!";
				}
			if(objVendor.optString("sstate").trim()!=null&&objVendor.optString("sstate").trim().length()>0){
				address.setState(objVendor.optString("sstate").trim());
				}else{
					return "Vendor sstate is mandatory!!";
				}
			if(objVendor.optString("scity").trim()!=null&&objVendor.optString("scity").trim().length()>0){
				address.setCity(objVendor.optString("scity").trim());
				}else{
					return "Vendor scity is mandatory!!";
				}
			if(objVendor.optString("slocality").trim()!=null&&objVendor.optString("slocality").trim().length()>0){
				address.setLocality(objVendor.optString("slocality").trim());
				}else{
					address.setLocality("");
				}
			if(objVendor.optString("spin").trim()!=null&&objVendor.optString("spin").trim().length()>0){
				address.setPin(Long.parseLong(objVendor.optString("spin").trim()));
				}else{
					return "Vendor spin is mandatory!!";
				}
			
				vendor.setSecondaryAddress(address);
				
				SocialInformation info=new SocialInformation();
				info.setFaceBookId(objVendor.optString("vendorFbId").trim());
				info.setGooglePlusId(objVendor.optString("vendorGoogleId").trim());
				info.setTwitterId(objVendor.optString("vendorTwitterId").trim());
				vendor.setSocialInfo(info);
			vendor.setCompanyId(companyId);
			vendor.setCount(++count);
//			GenricServiceImpl impl=new GenricServiceImpl();
//			impl.save(vendor);
		return vendor;
	}

	private String createWareHouse(String jsonWareHouseString) {
		boolean wareHouseStatus;
		// TODO Auto-generated method stub
		JSONObject jObj = null;
		try {
			jObj = new JSONObject(jsonWareHouseString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long companyId = Long.parseLong(jObj.optString("authCode").trim());
		String url = jObj.optString("url").trim();
		String[] urlArray = url.split("\\.");
		String accessUrl = urlArray[0];
		String applicationId = urlArray[1];
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl.trim()).first().now();
		AUTHCODE = company.getCompanyId();
		APPID = company.getAccessUrl();

		if (companyId == AUTHCODE) {
			String wareHouseName="",wareHouseCode="";
			
			
			if (!jObj.optString("wareHouseCode").trim().equalsIgnoreCase("NA")
					&& jObj.optString("wareHouseCode").trim().length() > 0) {
				wareHouseCode=jObj.optString("wareHouseCode").trim();
			}else{
				return "wareHouseCode is mandatory!!";
			}
			if (!jObj.optString("warehouseName").trim().equalsIgnoreCase("NA")
					&& jObj.optString("warehouseName").trim().length() > 0) {
				wareHouseName=jObj.optString("warehouseName").trim();
			}else{
				return "warehouse is mandatory!!";
			}
			
			
			if(warehouseValidationOfCode(wareHouseCode,AUTHCODE,jObj)){
				WareHouse wareHouse;
				if(jObj.optString("actionToBeTaken").trim().equalsIgnoreCase("creation")){
					wareHouse= new WareHouse();	
				}else if(jObj.optString("actionToBeTaken").trim().equalsIgnoreCase("updation")){
					wareHouse=ofy().load().type(WareHouse.class).filter("companyId", AUTHCODE).filter("warehouseCode", wareHouseCode.trim()).first().now();
				}else{
					wareHouse= new WareHouse();	
				}
				
				if(jObj.optString("actionToBeTaken").trim().equalsIgnoreCase("updation")){
					if(wareHouse==null){
//						return "No Such Warehouse Found!!";//Deepak Salve comment as per Client request
						/***04-01-2020 Deepak Salve added this code for create warehouse when warehouse is not present***/
						wareHouse= new WareHouse();
						/***End***/
					}
				}
			
			if (!jObj.optString("warehouseName").trim().equalsIgnoreCase("NA")
					&& jObj.optString("warehouseName").trim().length() > 0) {
				String warehouseName = jObj.optString("warehouseName").trim()+"-"+jObj.optString("wareHouseCode").trim();
//				wareHouse.setBusinessUnitName(jObj.optString("warehouseName")
//						.trim());
				wareHouse.setBusinessUnitName(warehouseName);
			} else {
				return "Warehouse Name is Mandatory";
			}

			if (!jObj.optString("wareHouseCode").trim().equalsIgnoreCase("NA")
					&& jObj.optString("wareHouseCode").trim().length() > 0) {
				wareHouse.setWarehouseCode(jObj.optString("wareHouseCode")
						.trim());
			} else {
//				wareHouse.setWarehouseCode(jObj.optString(""));
				return "Warehouse Code is Mandatory";
				
			}

			if (!jObj.optString("wareHouseType").trim().equalsIgnoreCase("NA")
					&& jObj.optString("wareHouseType").trim().length() > 0) {
				wareHouse.setWarehouseType(jObj.optString("wareHouseType")
						.trim());
			} else {
				wareHouse.setWarehouseType("");
			}
			if (!jObj.optString("parentWareHouse").trim()
					.equalsIgnoreCase("NA")
					&& jObj.optString("parentWareHouse").trim().length() > 0) {
				wareHouse.setParentWarehouse(jObj.optString("parentWareHouse")
						.trim());
			} else {
				wareHouse.setParentWarehouse("");
			}

			if (!jObj.optString("wareHouseEmail").trim().equalsIgnoreCase("NA")
					&& jObj.optString("wareHouseEmail").trim().length() > 0) {
				wareHouse.setEmail(jObj.optString("wareHouseEmail").trim());
			} else {
				wareHouse.setEmail("");
			}

			try {
				if (!jObj.optString("wareHouseLandline").trim()
						.equalsIgnoreCase("NA")
						&& jObj.optString("wareHouseLandline").trim().length() > 0) {
					wareHouse.setLandline(Long.parseLong(jObj.optString(
							"wareHouseLandline").trim()));
				} else {
					wareHouse.setLandline(0l);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Invalid Land Line Number";
			}

			try {
				if (!jObj.optString("cellNumber1").trim()
						.equalsIgnoreCase("NA")
						&& jObj.optString("cellNumber1").trim().length() > 0) {
					wareHouse.setCellNumber1(Long.parseLong(jObj.optString(
							"cellNumber1").trim()));
				} else {
					wareHouse.setCellNumber1(0l);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Invalid Cell Number 1";
			}

			try {
				if (!jObj.optString("cellNumber2").trim()
						.equalsIgnoreCase("NA")
						&& jObj.optString("cellNumber2").trim().length() > 0) {
					wareHouse.setCellNumber2(Long.parseLong(jObj.optString(
							"cellNumber2").trim()));
				} else {
					wareHouse.setCellNumber2(0l);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Invalid Cell Number 2";
			}

			try {
				if (!jObj.optString("faxNumber").trim().equalsIgnoreCase("NA")
						&& jObj.optString("faxNumber").trim().length() > 0) {
					wareHouse.setFaxNumber((Long.parseLong(jObj.optString(
							"faxNumber").trim())));
				} else {
					wareHouse.setFaxNumber(0l);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Invalid Fax Number";
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			try {
				if (!jObj.optString("expiryDate").trim().equalsIgnoreCase("NA")
						&& jObj.optString("expiryDate").trim().length() > 0) {
					wareHouse.setWarehouseExpiryDate(sdf.parse(jObj
							.optString("expiryDate")));
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Error in expiry Date";
			}
			if (!jObj.optString("status").trim().equalsIgnoreCase("NA")
					&& jObj.optString("status").trim().length() > 0) {
				if (jObj.optString("status").trim().equalsIgnoreCase("Active")) {
					wareHouse.setstatus(true);
					wareHouseStatus=true;
				} else if(jObj.optString("status").trim().equalsIgnoreCase("INACTIVE")){
					wareHouse.setstatus(false);
					wareHouseStatus=false;
				}else {
					return "Please provide status as ACTIVE or INACTIVE";
				}
			} else {
				return "Please provide status as ACTIVE or INACTIVE";
			}

			if (!jObj.optString("pocName").trim().equalsIgnoreCase("NA")
					&& jObj.optString("pocName").trim().length() > 0) {
				wareHouse.setPocName(jObj.optString("pocName").trim());
			} else {
				wareHouse.setPocName("");
			}
			try {
				if (!jObj.optString("pocLandLine").trim()
						.equalsIgnoreCase("NA")
						&& jObj.optString("pocLandLine").trim().length() > 0) {
					wareHouse.setPocLandline(Long.parseLong(jObj.optString(
							"pocLandLine").trim()));
				} else {
					wareHouse.setPocLandline(0l);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Invalid Poc Landline Number";
			}
			try {
				if (!jObj.optString("pocCell").trim().equalsIgnoreCase("NA")
						&& jObj.optString("pocCell").trim().length() > 0) {
					wareHouse.setPocCell(Long.parseLong(jObj.optString(
							"pocCell").trim()));
				} else {
					wareHouse.setPocCell(0l);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Invalid Poc Cell";
			}

			if (!jObj.optString("pocEmail").trim().equalsIgnoreCase("NA")
					&& jObj.optString("pocEmail").trim().length() > 0) {
				wareHouse.setPocEmail(jObj.optString("pocEmail").trim());
			} else {
				wareHouse.setPocEmail("");
			}
			Address address = null ;
			if (!jObj.optString("isCompanyAddressSame").trim()
					.equalsIgnoreCase("NA")
					&& jObj.optString("isCompanyAddressSame").trim().length() > 0) {
				if (jObj.optString("isCompanyAddressSame").trim()
						.equalsIgnoreCase("Yes")) {
					wareHouse.setAddress(company.getAddress());
					address=company.getAddress();
				} else {
					address= new Address();
					if (!jObj.optString("addressLine1").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("addressLine1").trim().length() > 0) {
					address.setAddrLine1(jObj.optString("addressLine1").trim()
							.toUpperCase());
					}else{
						return "Address Line 1 is Mandatory";
					}
					
					if (!jObj.optString("addressLine2").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("addressLine2").trim().length() > 0) {
					address.setAddrLine2(jObj.optString("addressLine2").trim()
					
							.toUpperCase());
					}else{
						address.setAddrLine2("");
					}
					
					if (!jObj.optString("landMark").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("landMark").trim().length() > 0) {
					address.setLandmark(jObj.optString("landMark").trim()
							.toUpperCase());
					}else{
						address.setLandmark("");
					}
					if (!jObj.optString("country").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("country").trim().length() > 0) {
					address.setCountry(jObj.optString("country").trim()
							.toUpperCase());
					}else{
						return "Country Name is Mandatory";
					}
					
					if (!jObj.optString("state").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("state").trim().length() > 0) {
					address.setState(jObj.optString("state").trim()
							.toUpperCase());
					}else{
						return "State Name is Mandatory";
					}
					if (!jObj.optString("city").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("city").trim().length() > 0) {
					address.setCity(jObj.optString("city").trim().toUpperCase());
					}else{
						return "City Name is Mandatory";
					}
					
					if (!jObj.optString("locality").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("locality").trim().length() > 0) {
					address.setLocality(jObj.optString("locality").trim()
							.toUpperCase());
					}else{
						address.setLocality("");
					}
					if (!jObj.optString("pin").trim()
							.equalsIgnoreCase("NA")
							&& jObj.optString("pin").trim().length() > 0) {
					try {
						address.setPin(Long.parseLong(jObj.optString("pin")
								.trim()));
						wareHouse.setAddress(address);
					} catch (Exception e) {
						e.printStackTrace();
						return "Invalid PIN";
					}
					}else{
						return "PIN is Mandatory";
					}

				}
			} else {
				return "Please provide isCompanyAddressSame as 'Yes' or 'No'";
			}

			JSONArray jArray = new JSONArray();
			try {
				jArray = jObj.getJSONArray("listOfBranch");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Error parsing listOfBranch";
			}
			List<BranchDetails> branchdetails = new ArrayList<BranchDetails>();

			for (int i = 0; i < jArray.length(); i++) {
				JSONObject jsonObj;
				try {
					jsonObj = jArray.getJSONObject(i);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return "Exception in list of branch obj" + e1;
				}
				Branch bQuery = ofy()
						.load()
						.type(Branch.class)
						.filter("companyId", companyId)
						.filter("buisnessUnitName",
								jsonObj.optString("branchName").trim()).first()
						.now();

				if (bQuery != null) {
					BranchDetails bDetails = new BranchDetails();

					bDetails.setBranchID(bQuery.getCount());

					bDetails.setBranchName(bQuery.getBusinessUnitName());
					bDetails.setBranchAdds(bQuery.getAddress().getCity());
					branchdetails.add(bDetails);
				} else {
					return jsonObj.optString("branchName").trim()
							+ " Branch not found!!";
			 	}

			}
			wareHouse.setBranchdetails(branchdetails);
			
			String response=createStorageLocationAndBin(jsonWareHouseString,address,wareHouseStatus);
			if(!response.trim().equalsIgnoreCase("Successfull")){
				return response;
			}
			NumberGeneration ng = new NumberGeneration();
			ng = ofy().load().type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "WareHouse").filter("status", true)
					.first().now();
			long number = ng.getNumber();
			int count = (int) number;
			ng.setNumber(count + 1);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(ng);
			wareHouse.setCompanyId(companyId);
			wareHouse.setCount(count + 1);
			ofy().save().entity(wareHouse);

			return "Successfull";
		}else{
			return wareHouseName+" already exist";
		}
		}else {
			return "AUTHENTICATION FAILED";
		}
		
	}

	private boolean warehouseValidationOfCode(String wareHouseCode,
			long aUTHCODE2, JSONObject jObj) {
		// TODO Auto-generated method stub
		WareHouse warehouse=ofy().load().type(WareHouse.class).filter("companyId", aUTHCODE2).filter("warehouseCode", wareHouseCode.trim()).first().now();
		if(warehouse!=null){
			if(jObj.optString("actionToBeTaken").trim().equalsIgnoreCase("updation")){
				return true;
			}else{
				return false;
			}
		}
		return true;
	}

	private boolean warehouseValidationOfName(String wareHouseName,
			long aUTHCODE2) {
		// TODO Auto-generated method stub
		WareHouse warehouse=ofy().load().type(WareHouse.class).filter("companyId", aUTHCODE2).filter("buisnessUnitName", wareHouseName.trim()).first().now();
		if(warehouse!=null){
			return false;
		}
		return true;
	}

	private String createStorageLocationAndBin(String jsonWareHouseString, Address address, boolean wareHouseStatus) {
		// TODO Auto-generated method stub
		JSONObject jObj = null;
		try {
			jObj = new JSONObject(jsonWareHouseString);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Parsing Data in creation of Loc & Bin";
			
		}
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class)
				.filter("companyId", AUTHCODE)
				.filter("processName", "StorageLocation").filter("status", true)
				.first().now();
		long number = ng.getNumber();
		int count = (int) number;
		StorageLocation storageLocation=new StorageLocation();
		if (!jObj.optString("warehouseName").trim().equalsIgnoreCase("NA")
				&& jObj.optString("warehouseName").trim().length() > 0) {
			/***19-12-2019 Deepak Salve added this code for map Storege Location with Warehouse ***/
			storageLocation.setWarehouseName(jObj.optString("warehouseName").trim()+"-"+jObj.optString("wareHouseCode").trim());
			storageLocation.setBusinessUnitName(jObj.optString("warehouseName").trim()+"-"+jObj.optString("wareHouseCode").trim()+" Location");
//			storageLocation.setBusinessUnitName(wlocation);
			/***End***/
		} else {
			return "Warehouse Name is Mandatory";
		}
		
		storageLocation.setstatus(wareHouseStatus);
		
		if (!jObj.optString("pocName").trim().equalsIgnoreCase("NA")
				&& jObj.optString("pocName").trim().length() > 0) {
			storageLocation.setPocName(jObj.optString("pocName").trim());
		} else {
			storageLocation.setPocName("");
		}
		try {
			if (!jObj.optString("pocLandLine").trim()
					.equalsIgnoreCase("NA")
					&& jObj.optString("pocLandLine").trim().length() > 0) {
				storageLocation.setPocLandline(Long.parseLong(jObj.optString(
						"pocLandLine").trim()));
			} else {
				storageLocation.setPocLandline(0l);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Invalid Poc Landline Number";
		}
		try {
			if (!jObj.optString("pocCell").trim().equalsIgnoreCase("NA")
					&& jObj.optString("pocCell").trim().length() > 0) {
				storageLocation.setPocCell(Long.parseLong(jObj.optString(
						"pocCell").trim()));
			} else {
				storageLocation.setPocCell(0l);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Invalid Poc Cell";
		}

		if (!jObj.optString("pocEmail").trim().equalsIgnoreCase("NA")
				&& jObj.optString("pocEmail").trim().length() > 0) {
			storageLocation.setPocEmail(jObj.optString("pocEmail").trim());
		} else {
			storageLocation.setPocEmail("");
		}

		storageLocation.setAddress(address);
		
		storageLocation.setCompanyId(AUTHCODE);
		
		storageLocation.setCount(count+1);
	
		NumberGeneration ng1 = new NumberGeneration();
		ng1 = ofy().load().type(NumberGeneration.class)
				.filter("companyId", AUTHCODE)
				.filter("processName", "Storagebin").filter("status", true)
				.first().now();
		long number1 = ng.getNumber();
		int count1 = (int) number1;
		
		Storagebin storageBin=new Storagebin();
		if (!jObj.optString("warehouseName").trim().equalsIgnoreCase("NA")
				&& jObj.optString("warehouseName").trim().length() > 0) {
			/***19-12-2019 Deepak Salve added this code for map Storege Bin with Warehouse and Location ***/
			storageBin.setWarehouseName(jObj.optString("warehouseName").trim()+"-"+jObj.optString("wareHouseCode").trim());
			storageBin.setStoragelocation(jObj.optString("warehouseName").trim()+"-"+jObj.optString("wareHouseCode").trim()+" Location");
			storageBin.setBinName(jObj.optString("warehouseName").trim()+"-"+jObj.optString("wareHouseCode").trim()+" Bin");
			/***End***/
			
			storageBin.setXcoordinate(1+"");
			storageBin.setYcoordinate(1+"");
			
			storageBin.setStatus(wareHouseStatus);
//			storageBin.setStoragelocation(jObj.optString("warehouseName").trim()+" Location");
//			storageBin.setWarehouseName(jObj.optString("warehouseName").trim());
			storageBin.setCompanyId(AUTHCODE);
			storageBin.setCount(count1+1);
			
		} else {
			return "Warehouse Name is Mandatory";
		}

		ng1.setNumber(count1 + 1);
		GenricServiceImpl impl1 = new GenricServiceImpl();
		impl1.save(ng1);
		ofy().save().entity(storageBin);
		
		
		ng.setNumber(count + 1);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);
		ofy().save().entity(storageLocation);
		
		

		
		return "Successfull";
	}


}
