package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalService;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
public class ContractRenewalServiceImpl extends RemoteServiceServlet implements ContractRenewalService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4857842990225479421L;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Logger logger = Logger.getLogger("Inside CONTRACT  Renewal......");


	@Override
	public ArrayList<RenewalResult> getRenewalStatus(ArrayList<Filter> filter,ArrayList<Filter> filter1) {
		System.out.println("Inside CONTRACT  Renewal......");
		
		Logger logger = Logger.getLogger("Inside CONTRACT  Renewal......");
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		ArrayList<RenewalResult> renewalList = new ArrayList<RenewalResult>();
		List<SuperModel> modelLis = new ArrayList<SuperModel>();
		
		/**
		 * @author Vijay Date :- 30-11-2022
		 * Des :- as per new requirement simplified the renewal Data filter to avoid index issue
		 * 
		 */
		int customerIdfilter=0;
		String statusfilter ="";
		String branchfilter="";
		Date fromDate = null, toDate=null;
		boolean customerwiseData = false;
		boolean customerInterestFlag= false;
		long companyId = 0;
		boolean fromDateTodateBranchAndStatus = false;
		boolean fromDateToDateStatus = false;
		boolean fromDateToDate = false;
		logger.log(Level.SEVERE, "filter1"+filter1);
		
		for(Filter filtr : filter){
			logger.log(Level.SEVERE, "Querystring"+filtr.getQuerryString());
			if(filtr.getQuerryString().equals("cinfo.count")){
				customerIdfilter = filtr.getIntValue();
				customerwiseData = true;
			}
			if(filtr.getQuerryString().equals("status")){
				statusfilter = filtr.getStringValue();
			}
			if(filtr.getQuerryString().equals("branch")){
				branchfilter = filtr.getStringValue();
			}
			if(filtr.getQuerryString().equals("endDate >=")){
				fromDate = filtr.getDateValue();
			}
			if(filtr.getQuerryString().equals("endDate <=")){
				toDate = filtr.getDateValue();
			}
			if(filtr.getQuerryString().equals("customerInterestFlag")){
				customerInterestFlag = filtr.isBooleanvalue();
			}
			if(filtr.getQuerryString().equals("companyId")){
				companyId = filtr.getLongValue();
			}
			
		}
		
		if(fromDate!=null && toDate!=null && branchfilter!=null && !branchfilter.equals("") && statusfilter!=null && !statusfilter.equals("")){
			fromDateTodateBranchAndStatus = true;
		}
		else if(fromDate!=null && toDate!=null && statusfilter!=null && !statusfilter.equals("")){
			fromDateToDateStatus = true;
		}
		else if(fromDateToDate){
			fromDateToDate = true;
		}
		
		
		List<Contract> contractlist = new ArrayList<Contract>();
		if(!customerwiseData && branchfilter!=null && !branchfilter.equals("")){
			contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("endDate >=", fromDate)
					.filter("endDate <=", toDate).filter("branch", branchfilter).list();
			logger.log(Level.SEVERE, "only from Date Todate wise contractlist size "+contractlist.size());
		}
		logger.log(Level.SEVERE, "fromDateTodateBranchAndStatus "+fromDateTodateBranchAndStatus);
		logger.log(Level.SEVERE, "fromDateToDateStatus "+fromDateToDateStatus);
		logger.log(Level.SEVERE, "fromDateToDate "+fromDateToDate);

		MyQuerry querry = new MyQuerry();
		querry.setQuerryObject(new Contract());
		if(customerwiseData){
			
			if(branchfilter!=null && !branchfilter.equals("")){
				contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", statusfilter)
						.filter("customerInterestFlag", customerInterestFlag).filter("cinfo.count", customerIdfilter)
						.filter("branch", branchfilter).list();
			}
			else{
				contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", statusfilter)
						.filter("customerInterestFlag", customerInterestFlag).filter("cinfo.count", customerIdfilter).list();
			}
		
			logger.log(Level.SEVERE, "customer wise contractlist size "+contractlist.size());
			
			if(contractlist!=null) {
			if(contractlist.size()>0){
				for(Contract contractEntity : contractlist){
					if(fromDateTodateBranchAndStatus){
						if((contractEntity.getEndDate().after(fromDate) && contractEntity.getEndDate().before(toDate)) || (contractEntity.getEndDate().equals(fromDate) || contractEntity.getEndDate().equals(toDate)) ){
							if(contractEntity.getBranch().equals(branchfilter))
								modelLis.add(contractEntity);
						}
					}
					else if(fromDateToDateStatus || fromDateToDate){
						if((contractEntity.getEndDate().after(fromDate) && contractEntity.getEndDate().before(toDate)) || (contractEntity.getEndDate().equals(fromDate) || contractEntity.getEndDate().equals(toDate)) ){
							modelLis.add(contractEntity);
						}
					}
					else{
						System.out.println("No filter only customer id filter");
						modelLis.add(contractEntity);
					}
					
				}
			}
		}
		}
		else if(fromDateTodateBranchAndStatus){
			
			contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("endDate >=", fromDate)
					.filter("endDate <=", toDate).filter("branch", branchfilter).list();
			logger.log(Level.SEVERE, "fromDateTodateBranchAndStatus wise contractlist size "+contractlist.size());

			if(contractlist!=null) {
			if(contractlist.size()>0){
				for(Contract contractEntity : contractlist){
					if(contractEntity.getStatus().equals(statusfilter) && !contractEntity.isCustomerInterestFlag() ){
						modelLis.add(contractEntity);
					}
				}
			}	
			}
		}
		else if(fromDateToDateStatus){
			contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("endDate >=", fromDate)
					.filter("endDate <=", toDate).list();
			logger.log(Level.SEVERE, "fromDateToDateStatus wise contractlist size "+contractlist.size());

			if(contractlist!=null) {
			if(contractlist.size()>0){
				for(Contract contractEntity : contractlist){
					if(contractEntity.getStatus().equals(statusfilter) && !contractEntity.isCustomerInterestFlag() ){
						modelLis.add(contractEntity);
					}
				}
				logger.log(Level.SEVERE, "fromDateToDateStatus wise contractlist size "+modelLis.size());

			}	
			}
		}
		else if(fromDateToDate){
			logger.log(Level.SEVERE, "fromDateToDate wise contractlist size ");
			if(contractlist!=null) {
			if(contractlist.size()>0){
				for(Contract contractEntity : contractlist){
					if(!contractEntity.isCustomerInterestFlag() && contractEntity.getStatus().equals(statusfilter)){
						modelLis.add(contractEntity);
					}
				}
				logger.log(Level.SEVERE, "fromDateToDate wise contractlist size "+modelLis.size());

			}	
			}
		}
		else{
			
			if (filter.size() != 0) {
				querry.getFilters().addAll(filter);
			}

			GenricServiceImpl impl = new GenricServiceImpl();
//			List<SuperModel> modelLis = impl.getSearchResult(querry);
			modelLis = impl.getSearchResult(querry);

			
		}
		
		System.out.println("Result SIZE.. "+modelLis.size());
		
		logger.log(Level.SEVERE,"Renewal Result SIZE.. "+modelLis.size());
		
		
		for (SuperModel temp : modelLis) {
			RenewalResult result = new RenewalResult();
			Contract entity = (Contract) temp;
			
			if(entity.getRenewProcessed().equalsIgnoreCase(AppConstants.NO)){
				
				Customer cust=ofy().load().type(Customer.class).filter("count",entity.getCinfo().getCount()).first().now();
				
				result.setContractId(entity.getCount());
				result.setContractDate(entity.getContractDate());
				result.setContractEndDate(entity.getEndDate());
				result.setBranch(entity.getBranch());
				result.setSalesPerson(entity.getEmployee());
					
				result.setCustomerId(entity.getCinfo().getCount());
				result.setCustomerName(entity.getCinfo().getFullName());
				if(entity.getCinfo()!=null&&entity.getCinfo().getCellNumber()!=null){
				result.setCustomerCell(entity.getCinfo().getCellNumber());
				}else{
					result.setCustomerCell(0);
				}
				if(cust!=null){
					logger.log(Level.SEVERE,"CUSTOMER   "+cust.getCount());
					result.setCity(cust.getSecondaryAdress().getCity());
					if(cust.getSecondaryAdress().getLocality()!=null){
						result.setLocality(cust.getSecondaryAdress().getLocality());
					}else{
						result.setLocality("");
					}
				}else{
					result.setCity("");
					result.setLocality("");
				}
					
				for(int i=0;i<entity.getItems().size();i++){
					entity.getItems().get(i).setOldProductPrice(entity.getItems().get(i).getPrice());
					
					
					/*
					 * Ashwini Patil
					 * Date:16-07-2024
					 * Ultra reported that when we renew contract, revise button popup shows product old price before discount.
					 * It should show product price after discount
					 */
					double totalprice=entity.getItems().get(i).getPrice();
					if(!entity.getItems().get(i).getArea().equalsIgnoreCase("NA")){
						totalprice=entity.getItems().get(i).getPrice()*Double.parseDouble(entity.getItems().get(i).getArea());						
					}
					double discAmt=0;
					double discPercentAmt=0;
					if(entity.getItems().get(i).getDiscountAmt()>0)
						discAmt=entity.getItems().get(i).getDiscountAmt();
					if(entity.getItems().get(i).getPercentageDiscount()>0)
						discPercentAmt=totalprice*entity.getItems().get(i).getPercentageDiscount()/100;				
					
					double totaldiscount = discAmt +discPercentAmt;
					logger.log(Level.SEVERE,"totaldiscount="+totaldiscount+" for product "+entity.getItems().get(i).getProductName());
					if(totaldiscount>0)
						entity.getItems().get(i).setOldProductPrice(totalprice-totaldiscount);
					
					logger.log(Level.SEVERE,"entity.getItems().get(i).getOldProductPrice="+entity.getItems().get(i).getOldProductPrice());
					
					Calendar cal3=Calendar.getInstance();
					cal3.setTime(entity.getEndDate());
					cal3.add(Calendar.DATE, +1);
					
					Date newcontractStartDate=null;
					
					try {
						newcontractStartDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
						newcontractStartDate=cal3.getTime();
					} catch (ParseException e) {
						e.printStackTrace();
					}
					entity.getItems().get(i).setStartDate(newcontractStartDate);
					
					System.out.println("newcontractStartDate "+newcontractStartDate);

					Calendar cal4=Calendar.getInstance();
					cal4.setTime(newcontractStartDate);
					cal4.add(Calendar.DATE, +(entity.getItems().get(i).getDuration()-1));//Ashwini Patil added -1
					
					Date newcontractEndDate=null;
					
					try {
						newcontractEndDate=dateFormat.parse(dateFormat.format(cal4.getTime()));
						newcontractEndDate=cal4.getTime();
					} catch (ParseException e) {
						e.printStackTrace();
					}
					entity.getItems().get(i).setEndDate(newcontractEndDate);
					System.out.println("newcontractEndDate "+newcontractEndDate);
				}
				result.setItems(entity.getItems());
				
				result.setYear("");
				result.setStatus(entity.getStatus());
				result.setDate(new Date());
				result.setRenewProcessed(entity.getRenewProcessed());
				
				/**
				 * Date 11 April 2017 added by vijay requirement from PCAAMB for contract amount and contract type
				 */
				result.setNetPayable(entity.getNetpayable());
				result.setContractType(entity.getType());
				result.setContractCategory(entity.getCategory());//16-1-2024 Ultra pest requirement to display category in renewal download
//				if(entity.getCategory()!=null)
//					logger.log(Level.SEVERE,"category set to "+entity.getCategory());
//				else
//					logger.log(Level.SEVERE,"category set to null");
				if(entity.getNonRenewalRemark()!=null)
				result.setNonRenewalRemark(entity.getNonRenewalRemark());
				
				if(entity.getStartDate()!=null)
				result.setContractStartDate(entity.getStartDate());
				
				renewalList.add(result);
				
			}
			
			
		}
		
		
		logger.log(Level.SEVERE,"CONTRACT RENEWAL LIST SIZE FROM (CONTRACT) :: "+renewalList.size());
		System.out.println("CONTRACT RENEWAL LIST SIZE FROM (CONTRACT) :: "+renewalList.size());
		
		///////////////////////////////
		
		MyQuerry querry1 = new MyQuerry();
		querry1.setQuerryObject(new ContractRenewal());
		
		List<SuperModel> modelLis1 = new ArrayList<SuperModel>();
		
		int cusIdfilter=0;
		String statusfiltr ="";
		String branchfiltr="";
		Date fromDatefiltr = null, toDatefiltr=null;
		boolean customerwiseDatafiltr = false;
		long companyIdfiltr = 0;
		boolean fromDateTodateBranchFiltr = false;
		boolean fromDateToDateStatusfiltr = false;
		boolean fromDateTodateBranchAndStatusfiltr = false;
		boolean fromDateTodatefiltr = false;

		for(Filter filtr : filter){
			if(filtr.getQuerryString().equals("customerId")){
				cusIdfilter = filtr.getIntValue();
				customerwiseDatafiltr = true;
			}
			if(filtr.getQuerryString().equals("status")){
				statusfiltr = filtr.getStringValue();
			}
			if(filtr.getQuerryString().equals("branch")){
				branchfiltr = filtr.getStringValue();
			}
			if(filtr.getQuerryString().equals("contractEndDate >=")){
				fromDatefiltr = filtr.getDateValue();
			}
			if(filtr.getQuerryString().equals("contractEndDate <=")){
				toDatefiltr = filtr.getDateValue();
			}
			if(filtr.getQuerryString().equals("companyId")){
				companyIdfiltr = filtr.getLongValue();
			}
			
		}
		
		if(fromDatefiltr!=null && toDatefiltr!=null && branchfiltr!=null && !branchfiltr.equals("") && statusfiltr!=null && !statusfiltr.equals("")){
			fromDateTodateBranchAndStatusfiltr = true;
		}
		else if(fromDatefiltr!=null && toDatefiltr!=null && statusfiltr!=null && !statusfiltr.equals("")){
			fromDateToDateStatusfiltr = true;
		}
		else if(fromDatefiltr!=null && toDatefiltr!=null && branchfiltr!=null && !branchfiltr.equals("")){
			fromDateTodateBranchFiltr = true;
		}
		else if(fromDatefiltr!=null && toDatefiltr!=null ){
			fromDateTodatefiltr = true;
		}
		
		List<ContractRenewal> contractRenewallist =null;
		if(!customerwiseDatafiltr){
			contractRenewallist = ofy().load().type(ContractRenewal.class).filter("companyId", companyIdfiltr).filter("contractEndDate >=", fromDatefiltr)
					.filter("contractEndDate <=", toDatefiltr).list();
			logger.log(Level.SEVERE, "from Date To Date wise contractRenewallist size "+contractRenewallist.size());

		}
		
		if(customerwiseDatafiltr){
			if(branchfiltr!=null && !branchfiltr.equals("")){
				contractRenewallist = ofy().load().type(ContractRenewal.class).filter("companyId", companyIdfiltr)
						.filter("customerId", cusIdfilter).filter("status", statusfiltr).filter("branch", branchfiltr).list();
			}
			else{
				contractRenewallist = ofy().load().type(ContractRenewal.class).filter("companyId", companyIdfiltr)
						.filter("customerId", cusIdfilter).filter("status", statusfiltr).list();
			}
			
			logger.log(Level.SEVERE, "customer wise contractRenewallist size "+contractRenewallist.size());
			if(contractRenewallist.size()>0){
				for(ContractRenewal contractRenewal : contractRenewallist){
					
					if(fromDateTodateBranchAndStatusfiltr){
						if((contractRenewal.getContractEndDate().after(fromDatefiltr) && contractRenewal.getContractEndDate().before(toDatefiltr)) || (contractRenewal.getContractEndDate().equals(fromDatefiltr) || contractRenewal.getContractEndDate().equals(toDatefiltr)) ){
							if(contractRenewal.getBranch().equals(branchfilter))
								modelLis.add(contractRenewal);
						}
					}
					else if(fromDateToDateStatusfiltr || fromDateTodatefiltr){
						if((contractRenewal.getContractEndDate().after(fromDatefiltr) && contractRenewal.getContractEndDate().before(toDatefiltr)) || (contractRenewal.getContractEndDate().equals(fromDatefiltr) || contractRenewal.getContractEndDate().equals(toDatefiltr)) ){
							modelLis.add(contractRenewal);
						}
					}
					else{
						modelLis.add(contractRenewal);
					}
					
				}
			}
		}
		else if(fromDateTodateBranchAndStatusfiltr){
			logger.log(Level.SEVERE, "fromDateTodateBranchAndStatusfiltr wise contractRenewallist ");

			if(contractRenewallist.size()>0){
				for(ContractRenewal contractRenewal : contractRenewallist){
					if(branchfiltr.equals(contractRenewal.getBranch()) && statusfiltr.equals(contractRenewal.getStatus())){
						modelLis.add(contractRenewal);
					}
				}
				logger.log(Level.SEVERE, "fromDateTodateBranchAndStatusfiltr wise contractRenewallist "+modelLis.size());

			}	
		}
		else if(fromDateTodateBranchFiltr){
			
			logger.log(Level.SEVERE, "fromDateTodateBranchFiltr wise contractRenewallist size ");

			if(contractRenewallist.size()>0){
				for(ContractRenewal contractRenewal : contractRenewallist){
					if(branchfiltr.equals(contractRenewal.getBranch()) && statusfiltr.equals(contractRenewal.getStatus())){
						modelLis.add(contractRenewal);
					}
				}
				logger.log(Level.SEVERE, "fromDateTodateBranchFiltr wise contractRenewallist size "+modelLis.size());
			}	
		}
		else if(fromDateToDateStatusfiltr){
			logger.log(Level.SEVERE, "fromDateToDateStatusfiltr wise contractRenewallist size ");
			if(contractRenewallist.size()>0){
				for(ContractRenewal contractRenewal : contractRenewallist){
					if(statusfiltr.equals(contractRenewal.getStatus())){
						modelLis.add(contractRenewal);
					}
				}
				logger.log(Level.SEVERE, "fromDateToDateStatusfiltr wise contractRenewallist size "+modelLis.size());
			}	
		}
		else if(fromDateTodatefiltr){
			logger.log(Level.SEVERE, "fromDateTodatefiltr wise contractRenewallist size ");
			if(contractRenewallist.size()>0){
				for(ContractRenewal contractRenewal : contractRenewallist){
					if(statusfiltr.equals(contractRenewal.getStatus())){
						modelLis.add(contractRenewal);
					}
				}
				logger.log(Level.SEVERE, "fromDateTodatefiltr wise contractRenewallist size "+modelLis.size());
			}	
		}
		else{
			logger.log(Level.SEVERE,"with old filer logic ");
			if (filter1.size() != 0) {
				querry1.getFilters().addAll(filter1);
			}

			GenricServiceImpl impl1 = new GenricServiceImpl();
			modelLis1 = impl1.getSearchResult(querry1);
		}

		logger.log(Level.SEVERE,"CONTRACT RENEWAL List Size "+modelLis1.size());

		
		
		if(modelLis1.size()!=0){
		for (SuperModel temp : modelLis1) {
			
			logger.log(Level.SEVERE,"CONTRACT RENEWAL NOT NULL (CONTRACT RENEWAL) :: ");
			RenewalResult result = new RenewalResult();
			ContractRenewal entity = (ContractRenewal) temp;
			
			
			
			result.setContractId(entity.getContractId());
			result.setContractDate(entity.getContractDate());
			result.setContractEndDate(entity.getContractEndDate());
			result.setBranch(entity.getBranch());
			result.setSalesPerson(entity.getSalesPerson());
				
			result.setCustomerId(entity.getCustomerId());
			result.setCustomerName(entity.getCustomerName());
			result.setCustomerCell(entity.getCustomerCell());
				
			result.setCity(entity.getCity());
			if(entity.getLocality()!=null){
				result.setLocality(entity.getLocality());
			}else{
				result.setLocality("");
			}
			
			result.setItems(entity.getItems());
			result.setYear("");
			result.setStatus(entity.getStatus());
			result.setDate(entity.getDate());
			result.setRenewProcessed(AppConstants.YES);
			result.setDescription(entity.getDescription());
			result.setRecordSelect(false);
			/**
			 * Date 11 April 2017 added by vijay requirement from PCAAMB for contract amount and contract type
			 */
			
			result.setNetPayable(entity.getNetPayable());
			result.setContractType(entity.getContractType());
			result.setContractCategory(entity.getContractCategory());//16-1-2024 Ultra pest requirement to display category in renewal download
			if(entity.getContractCategory()!=null)
				logger.log(Level.SEVERE,"category set to "+entity.getContractCategory());
			else
				logger.log(Level.SEVERE,"category set to null");
			/**
			 * ends here
			 */
			renewalList.add(result);
			
		}
		}
		System.out.println("CONTRACT RENEWAL LIST SIZE FROM (CONTRACT RENEWAL) :: "+renewalList.size());
		logger.log(Level.SEVERE,"CONTRACT RENEWAL LIST SIZE FROM (CONTRACT RENEWAL) :: "+renewalList.size());
		return renewalList;
	}

	@Override
	public ArrayList<RenewalResult> processContractRenewal(ArrayList<ContractRenewal> contractRenewalList) {
		Logger logger = Logger.getLogger("Inside Process CONTRACT  Renewal......");

		/**
		 * @author Vijay Date :- 24-10-2020
		 * Des :- Bug pecopp doouble entry created in contractRenewal Entity
		 *  so added checker if contract id is already processed then removing that contract id from contractRenewalList
		 *  so double entry will not create.
		 */
		HashSet<Integer> hscontractid = new HashSet<Integer>();
		for(ContractRenewal conRenewal : contractRenewalList){
			hscontractid.add(conRenewal.getContractId());
		}
		ArrayList<Integer> contractidlist = new ArrayList<Integer>(hscontractid);

		List<ContractRenewal> conrenewallist = ofy().load().type(ContractRenewal.class)
								.filter("contractId IN", contractidlist).filter("companyId", contractRenewalList.get(0).getCompanyId())
								.list();
		logger.log(Level.SEVERE,"contractrenewallist"+conrenewallist.size());

		ArrayList<ContractRenewal> updatedContractrenewallist = new ArrayList<ContractRenewal>();
		updatedContractrenewallist.addAll(contractRenewalList);
		
		if(conrenewallist.size()>0){
			for(ContractRenewal conrenewal : conrenewallist){
				for(ContractRenewal contractrenewal : updatedContractrenewallist){
					if(conrenewal.getContractId() == contractrenewal.getContractId()){
						contractRenewalList.remove(contractrenewal);
					}
				}
				
			}
		}
		/**
		 * ends here
		 */
		ArrayList<RenewalResult> processedrenewallist = new ArrayList<RenewalResult>();

		if(contractRenewalList.size()>0){
		
			/**
			 * Added by sheetal:16-12-2021 
			 * for contract renewal start date and end date
			 */
			for(ContractRenewal conrenewal : contractRenewalList){

				for(SalesLineItem lineItem : conrenewal.getItems()){

//Ashwini Patil Date:19-06-2022 Description: In getRenewalStatus() method already dates are getting revised. Below code is commented as it was again revising dates and that problem is reported by Ankita Pest and Pepcopp pest client.
//					Date previousEndDate = conrenewal.getItems().get(0).getEndDate();
//					Calendar cal=Calendar.getInstance();
//					cal.setTime(previousEndDate);
//					cal.add(Calendar.DATE, +1);
					
//					Date newcontractStartDate=null;
//					
//					try {
//						newcontractStartDate=dateFormat.parse(dateFormat.format(cal.getTime()));
//						
//						Calendar cal2=Calendar.getInstance();
//						cal2.setTime(newcontractStartDate);
//						cal2.add(Calendar.DATE, lineItem.getDuration());
//						
//						Date newcontractEndDate=null;
//						
//						try {
//							newcontractEndDate=dateFormat.parse(dateFormat.format(cal2.getTime()));
//							
//							lineItem.setStartDate(newcontractStartDate);							
//							lineItem.setEndDate(newcontractEndDate);
//							logger.log(Level.SEVERE,"newcontractStartDate set to "+newcontractStartDate);
//							logger.log(Level.SEVERE,"newcontractEndDate set to "+newcontractEndDate);
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
					
				}
			}
			
		/**Date :1/11/2017 By :Manisha
		 * Description :Uncomment the code to map the taxes from contract!!!!!
		 */	
		ofy().save().entities(contractRenewalList).now();
		
		/**end**/
		/*****************************Commented this**************/
		/**above code commented by vijay for below new logic
		 * Date 24 march 2017
		 * added by vijay for getting contract renewal product service tax & vat tax from master
		 */
		
//		for(int j=0;j<contractRenewalList.size();j++){
//			
//			for(int k=0;k<contractRenewalList.get(j).getItems().size();k++){
//				ServiceProduct serviceprod = ofy().load().type(ServiceProduct.class).filter("companyId", contractRenewalList.get(j).getCompanyId()).filter("count", contractRenewalList.get(j).getItems().get(k).getPrduct().getCount()).first().now();
//			System.out.println("Company id==="+contractRenewalList.get(j).getCompanyId());
//				if(serviceprod!=null){
//					contractRenewalList.get(j).getItems().get(k).setServiceTax(serviceprod.getServiceTax());
//					contractRenewalList.get(j).getItems().get(k).setVatTax(serviceprod.getVatTax());
//					System.out.println("Service prod serviec tax"+serviceprod.getServiceTax());
//					System.out.println("Service prod vat tax"+serviceprod.getVatTax());
//				}
//			}
//			ofy().save().entities(contractRenewalList).now();
//		}
		/**
		 * ends here
		 */
		
		
		/**
		 * Date 28-03-2017
		 * added by vijay for returning processed contract renewal list
		 */
		
//		ArrayList<RenewalResult> processedrenewallist = new ArrayList<RenewalResult>();
		for(int p=0;p<contractRenewalList.size();p++){
			
			RenewalResult renewalresult = new RenewalResult();
			
			renewalresult.setContractId(contractRenewalList.get(p).getContractId());
			renewalresult.setContractDate(contractRenewalList.get(p).getContractDate());
			renewalresult.setContractEndDate(contractRenewalList.get(p).getContractEndDate());
			renewalresult.setBranch(contractRenewalList.get(p).getBranch());
			renewalresult.setSalesPerson(contractRenewalList.get(p).getSalesPerson());
				
			renewalresult.setCustomerId(contractRenewalList.get(p).getCustomerId());
			renewalresult.setCustomerName(contractRenewalList.get(p).getCustomerName());
			renewalresult.setCustomerCell(contractRenewalList.get(p).getCustomerCell());
				
			renewalresult.setCity(contractRenewalList.get(p).getCity());
			if(contractRenewalList.get(p).getLocality()!=null){
				renewalresult.setLocality(contractRenewalList.get(p).getLocality());
			}else{
				renewalresult.setLocality("");
			}
			renewalresult.setItems(contractRenewalList.get(p).getItems());
			renewalresult.setYear("");
			renewalresult.setStatus(contractRenewalList.get(p).getStatus());
			renewalresult.setDate(contractRenewalList.get(p).getDate());
			renewalresult.setRenewProcessed(AppConstants.YES);
			renewalresult.setDescription(contractRenewalList.get(p).getDescription());
			renewalresult.setRecordSelect(false);
			
			/**
			 * Date 11 April 2017 added by vijay requirement from PCAAMB for contract amount and contract type
			 */
			renewalresult.setNetPayable(contractRenewalList.get(p).getNetPayable());
			renewalresult.setContractType(contractRenewalList.get(p).getContractType());
			renewalresult.setContractCategory(contractRenewalList.get(p).getContractCategory());//16-1-2024 Ultra pest requirement to display category in renewal download
			
			
			for(SalesLineItem item:renewalresult.getItems()) {
				item.setDiscountAmt(0); //Ashwini Patil Date:5-1-2024 As per Nitin sir we shall not copy discount at the time of renewal
			}
			
			processedrenewallist.add(renewalresult);
		}
		
		/**
		 * ends here
		 */
		
		for(int i=0;i<contractRenewalList.size();i++){
			Contract contract=ofy().load().type(Contract.class).filter("companyId", contractRenewalList.get(i).getCompanyId()).filter("count", contractRenewalList.get(i).getContractId()).first().now();
			
			if(contract!=null){
				System.out.println("CONTRACT NOT NULL ");
				contract.setRenewProcessed(AppConstants.YES);
				ofy().save().entity(contract).now();
			}
		}
		}
		return processedrenewallist;
	}

	@Override
	public ArrayList<ContractRenewal> setReminder(ArrayList<ContractRenewal> renewalList,String loggedinuser) {
		ArrayList<ContractRenewal> conRenewalList=new ArrayList<ContractRenewal>();
		System.out.println("Renewal Reminder List Size "+renewalList.size());
		for(int i=0;i<renewalList.size();i++){
			ContractRenewal conRenewal=ofy().load().type(ContractRenewal.class).filter("companyId", renewalList.get(i).getCompanyId()).filter("contractId", renewalList.get(i).getContractId()).first().now();
			
			if(conRenewal!=null){
				conRenewal.setContractEndDate(renewalList.get(i).getContractEndDate());
					conRenewal.setDescription(renewalList.get(i).getDescription());
				
				ofy().save().entity(conRenewal).now();
				conRenewalList.add(conRenewal);
				
				/*
				 * Ashwini Patil
				 * Date:8-05-2023
				 * Hi-Tech requirement - Update Contract end date in Contract screen too after clicking on Remind Me Later in Renewal 
				 */
				System.out.println("Ashwini conRenewal.getContractId()="+conRenewal.getContractId());
				Contract con=ofy().load().type(Contract.class).filter("companyId",renewalList.get(i).getCompanyId()).filter("count",conRenewal.getContractId()).first().now();
				if(con!=null){
					String desc=loggedinuser+" changed contract renewal date to "+dateFormat.format(renewalList.get(i).getContractEndDate())+" from "+dateFormat.format(con.getEndDate())+" (Remind Me Later) on Date "+new Date();
					con.setEndDate(renewalList.get(i).getContractEndDate());
					if(con.getDescription()!=null)
						con.setDescription(con.getDescription()+desc);
					else
						con.setDescription(desc);
					
					ofy().save().entity(con).now();
				}
			}
		}
		
		return conRenewalList;
	}

	@Override
	public ArrayList<ContractRenewal> setStatusDoNotRenew(ArrayList<ContractRenewal> renewalList) {
		ArrayList<ContractRenewal> conRenewalList=new ArrayList<ContractRenewal>();
		System.out.println("Renewal Status List Size "+renewalList.size());
		for(int i=0;i<renewalList.size();i++){
			ContractRenewal conRenewal=ofy().load().type(ContractRenewal.class).filter("companyId", renewalList.get(i).getCompanyId()).filter("contractId", renewalList.get(i).getContractId()).first().now();
			
			if(conRenewal!=null){
				conRenewal.setStatus(renewalList.get(i).getStatus());
				/**
				 * nidhi
				 * 9-06-2018
				 */
				conRenewal.setNonRenewalRemark(renewalList.get(i).getNonRenewalRemark());
				ofy().save().entity(conRenewal).now();
				conRenewalList.add(conRenewal);
			}
			
		//  ************update contract status in contract ********************
			Contract con=ofy().load().type(Contract.class).filter("companyId",renewalList.get(i).getCompanyId()).filter("count",conRenewalList.get(i).getContractId()).first().now();
		
			
			if(con!=null)
			{
				/**
				 * nidhi
				 * 9-06-2018
				 */
				con.setNonRenewalRemark(renewalList.get(i).getNonRenewalRemark());
				con.setRenewProcessed(AppConstants.YES);
				/** @author Vijay Date :- 04-12-2020
				 * Des :- when contract is marked as do not renew from contract renewal screen then updating its flag.
				 * if status is do not renew then updating its flag in contract
				 */
				if(renewalList.get(i).getStatus().trim().equals(AppConstants.DONOTRENEW)) {
					con.setCustomerInterestFlag(true);
				}
				/**
				 * ends here
				 */
				
				ofy().save().entity(con).now();
			}
			
			//***********************changes ends here ****************************
			
			
		}
		
		return conRenewalList;
	}

	@Override
	public ArrayList<ContractRenewal> setStatusNeverRenew(ArrayList<ContractRenewal> renewalList) {
		ArrayList<ContractRenewal> conRenewalList=new ArrayList<ContractRenewal>();
		System.out.println("Renewal Status never List Size "+renewalList.size());
		for(int i=0;i<renewalList.size();i++){
			ContractRenewal conRenewal=ofy().load().type(ContractRenewal.class).filter("companyId", renewalList.get(i).getCompanyId()).filter("contractId", renewalList.get(i).getContractId()).first().now();
			
			if(conRenewal!=null){
				System.out.println("CON REN NOT NULL");
				conRenewal.setStatus(renewalList.get(i).getStatus());
				
				ofy().save().entity(conRenewal).now();
				conRenewalList.add(conRenewal);
				
				
				Contract contract=ofy().load().type(Contract.class).filter("companyId", renewalList.get(i).getCompanyId()).filter("count", renewalList.get(i).getContractId()).first().now();
				
				if(contract!=null){
					System.out.println("CONTRACT NOT NULL ");
					contract.setCustomerInterestFlag(true);
					/**
					 * nidhi
					 * 9-06-2018
					 */
					contract.setNonRenewalRemark(renewalList.get(i).getNonRenewalRemark());
					ofy().save().entity(contract).now();
				}
			}
			
			
		}
		
		return conRenewalList;
	}

	@Override
	public void saveProductDetails(ContractRenewal contractRenewalList) {
		System.out.println("SERVER CONTRACT ID :: "+contractRenewalList.getContractId());
		System.out.println(" SERVER CONTRACT price :: "+contractRenewalList.getItems().get(0).getPrice());
		ContractRenewal conRenewal=ofy().load().type(ContractRenewal.class).filter("companyId", contractRenewalList.getCompanyId()).filter("contractId", contractRenewalList.getContractId()).first().now();
		
		if(conRenewal!=null){
			System.out.println("CONRACT RENEWAL NOT NULL !");
			conRenewal.setDescription(contractRenewalList.getDescription());
			conRenewal.setItems(contractRenewalList.getItems());
			
			logger.log(Level.SEVERE, "contractRenewalList.getNetPayable() "+contractRenewalList.getNetPayable());
			logger.log(Level.SEVERE, "conRenewal.getNetPayable() "+conRenewal.getNetPayable());

			if(contractRenewalList.getNetPayable() != conRenewal.getNetPayable()) {
				conRenewal.setPaymentGatewayUniqueId("");
			}
			logger.log(Level.SEVERE, "contractRenewalList.getSalesPerson()"+contractRenewalList.getSalesPerson());
			if(contractRenewalList.getSalesPerson()!=null)
			conRenewal.setSalesPerson(contractRenewalList.getSalesPerson());
			
			conRenewal.setDate(contractRenewalList.getDate());//Ashwini Patil Date:27-04-2023
			/****Date :1/11/2017 By :Manisha
			 * Description :To set the net payable amount.!!!!!
			 */
			conRenewal.setNetPayable(contractRenewalList.getNetPayable());
			/**end**/
			ofy().save().entity(conRenewal).now();
		}
	}
	
	
	/**
	 * @author Vijay Date :- 07-12-2020
	 * Des :- when Contract is renewed or do not renewd from Contract screen then creating its entry in contract renewal table
	 * so we can get the reports as per nitin sir
	 */
	@Override
	public String contractRenewalEntry(Contract contractEntity, String status,String description) {
		logger.log(Level.SEVERE,"in contractRenewalEntry");
		ContractRenewal contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("contractId", contractEntity.getCount())
												.first().now();
		if(contractRenewalEntity==null) {
			logger.log(Level.SEVERE,"contractRenewal entity does not exist");
			Customer cust=ofy().load().type(Customer.class).filter("count",contractEntity.getCinfo().getCount()).first().now();

			ContractRenewal contractrenewal=new ContractRenewal();
			
			contractrenewal.setContractId(contractEntity.getCount());
			contractrenewal.setContractDate(contractEntity.getContractDate());
			contractrenewal.setContractEndDate(contractEntity.getEndDate());
			contractrenewal.setBranch(contractEntity.getBranch());
			contractrenewal.setSalesPerson(contractEntity.getEmployee());
			
			contractrenewal.setCustomerId(contractEntity.getCustomerId());
			contractrenewal.setCustomerName(contractEntity.getCinfo().getFullName());
			contractrenewal.setCustomerCell(contractEntity.getCinfo().getCellNumber());
			contractrenewal.setItems(contractEntity.getItems());
			
			contractrenewal.setStatus(status);
			
			/**Manisha**/
			if(description!=null&&!description.equals(""))
				contractrenewal.setDescription(contractEntity.getDescription()+" "+description);
			else
				contractrenewal.setDescription(contractEntity.getDescription());
			/**End**/
			// Date 11 April 2017 added by vijay for PCAMB contract amount and type
			contractrenewal.setNetPayable(contractEntity.getNetpayable());
			if(contractEntity.getType()!=null)
				contractrenewal.setContractType(contractEntity.getType());
		
			if(cust!=null){
				contractrenewal.setCity(cust.getSecondaryAdress().getCity());
				if(cust.getSecondaryAdress().getLocality()!=null){
					contractrenewal.setLocality(cust.getSecondaryAdress().getLocality());
				}else{
					contractrenewal.setLocality("");
				}
			}else{
				contractrenewal.setCity("");
				contractrenewal.setLocality("");
			}
			
			contractrenewal.setCompanyId(contractEntity.getCompanyId());
			contractrenewal.setDate(new Date());
			/**
			 * @author Vijay
			 * Des :- if number generation does not not exist then double services getting created due to exception so added try catch to handle error
			 * and to avoid duplicate services
			 */
			try {
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(contractrenewal);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			contractEntity.setRenewProcessed(AppConstants.YES);
			ofy().save().entity(contractEntity);
		}
		/**
		 * @author Ashwini Patil
		 * @since 9-03-2022
		 * Pecopp reported issue that Renewal Screen showing renewed contracts in processed
		 * Here we are updating status of existing contractRenewal entity to renew so that it wont appear in processed list.
		 */
		else{
			logger.log(Level.SEVERE,"contractRenewal entity exist"+status);
			if(status.trim().equals(AppConstants.RENEWED)){
				
				contractRenewalEntity.setStatus(AppConstants.RENEWED);
				try {
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(contractRenewalEntity);
				} catch (Exception e) {
					logger.log(Level.SEVERE,"error while saving"+e.getMessage());
				}
			}
			
		}
		
		return "Updated Successfully";
	}

}
