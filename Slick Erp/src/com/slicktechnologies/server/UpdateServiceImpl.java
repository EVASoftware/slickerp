package com.slicktechnologies.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.cronjobimpl.ServiceFumigationDetailReportCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.ServiceFumigationValueDetailReportCronJobImpl;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.CommodityFumigationDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.ServiceInvoiceDetails;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class UpdateServiceImpl extends RemoteServiceServlet implements
		UpdateService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8206751028226226105L;

	Logger logger = Logger.getLogger("Document Cancellation Task Queue");
	private SimpleDateFormat fmt= new SimpleDateFormat("dd/MM/yyyy");

	ArrayList<ProductInventoryTransaction> recordsToUpdate = new ArrayList<ProductInventoryTransaction>();
	/** Added by komal for min and mmn number generation **/
	Long count = 0l;

	@Override
	public ArrayList<ProductInventoryTransaction> updateInventoryProductListStock(
			ArrayList<ProductInventoryTransaction> productInventoryList,
			MaterialIssueNote minEntity) {

		for (int j = 0; j < minEntity.getSubProductTablemin().size(); j++) {
			recordsToUpdate = new ArrayList<ProductInventoryTransaction>();

			recordsToUpdate = getProductTransactions(productInventoryList,
					minEntity.getSubProductTablemin().get(j)
							.getMaterialProductId());

			if (recordsToUpdate.size() == 0) {
				int productId = minEntity.getSubProductTablemin().get(j)
						.getMaterialProductId();
				String storageLocation = minEntity.getSubProductTablemin()
						.get(j).getMaterialProductStorageLocation();
				String storageBin = minEntity.getSubProductTablemin().get(j)
						.getMaterialProductStorageBin();
				String warehouse = minEntity.getSubProductTablemin().get(j)
						.getMaterialProductWarehouse();
				double productQty = minEntity.getSubProductTablemin().get(j)
						.getMaterialProductRequiredQuantity();
				Date date = minEntity.getCreationDate();
				long compId = minEntity.getCompanyId();
				int idCount = minEntity.getCount();
				String minTitle1 = minEntity.getMinTitle();
				System.out.println("Id :: " + productId);
				minTitle1 = minTitle1 + " Created through Stock Update";

				UpdateStock.setProductInventory(productId, productQty, idCount,
						date, AppConstants.MIN, minTitle1, warehouse,
						storageLocation, storageBin, AppConstants.SUBTRACT,
						compId, minEntity.getBranch(),minEntity.getServiceId()+"");
			} else if (recordsToUpdate.size() > 1) {
				cancelTransactionsAndUpdateStock(recordsToUpdate);
			}

			minEntity.setStatus("Approved");

			// starts here
			ArrayList<MaterialProduct> materialList = new ArrayList<MaterialProduct>();
			HashSet<Integer> minMaterialSet = new HashSet<Integer>();
			for (int i = 0; i < minEntity.getSubProductTablemin().size(); i++) {
				minMaterialSet.add(minEntity.getSubProductTablemin().get(i)
						.getMaterialProductId());
			}

			ArrayList<Integer> halist = new ArrayList<Integer>(minMaterialSet);

			for (Integer obj : halist) {
				ArrayList<MaterialProduct> list = new ArrayList<MaterialProduct>();
				list = getUniqueMaterialProductFromMIN(minEntity, obj);
				materialList.add(list.get(0));
			}

			minEntity.setSubProductTablemin(materialList);

			// ends here
			ofy().save().entity(minEntity).now();
		}

		return productInventoryList;
	}

	private ArrayList<MaterialProduct> getUniqueMaterialProductFromMIN(
			MaterialIssueNote minEntity, int prodId) {

		ArrayList<MaterialProduct> materialList = new ArrayList<MaterialProduct>();
		// ArrayList<Integer> halist = new ArrayList<Integer>();
		// halist.addAll(minMaterialSet);

		// for (int i = 0; i < halist.size(); i++) {

		for (int j = 0; j < minEntity.getSubProductTablemin().size(); j++) {

			if (prodId == minEntity.getSubProductTablemin().get(j)
					.getMaterialProductId()) {
				materialList.add(minEntity.getSubProductTablemin().get(j));
			}
		}
		// }

		return materialList;
	}

	private void cancelTransactionsAndUpdateStock(
			ArrayList<ProductInventoryTransaction> recordsToUpdate) {

		Comparator<ProductInventoryTransaction> compare = new Comparator<ProductInventoryTransaction>() {
			@Override
			public int compare(ProductInventoryTransaction o1,
					ProductInventoryTransaction o2) {
				Integer l1 = o1.getCount();
				Integer l2 = o2.getCount();
				return l1.compareTo(l2);
			}
		};
		Collections.sort(recordsToUpdate, compare);

		recordsToUpdate.remove(0);

		double sum = 0;
		// this is use to show transaction id's
		String remark = "Transaction Id: ";

		for (int i = 0; i < recordsToUpdate.size(); i++) {
			sum = sum + recordsToUpdate.get(i).getProductQty();
			remark = remark + " " + recordsToUpdate.get(i).getCount();

			// Previous transactions saving part with titles
			String previousTitle = recordsToUpdate.get(i).getDocumentTitle();
			recordsToUpdate.get(i).setDocumentTitle(
					"Cancelled Transaction " + previousTitle);
		}

		ofy().save().entities(recordsToUpdate).now();

		String desc = "Stock Reverse Against MIN-"
				+ recordsToUpdate.get(0).getDocumentId()
				+ ". With following transactions- " + remark;

		// UpdateStock.setProductInventory(recordsToUpdate.get(0).getProductId(),sum,
		// recordsToUpdate.get(0).getDocumentId(),new
		// Date(),AppConstants.MIN,desc,
		// recordsToUpdate.get(0).getWarehouseName(),
		// recordsToUpdate.get(0).getStorageLocation(),
		// recordsToUpdate.get(0).getStorageBin(),AppConstants.ADD,recordsToUpdate.get(0).getCompanyId());

		String typeducumentName = "UpdateStock" + "$"
				+ recordsToUpdate.get(0).getProductId() + "$" + sum + "$"
				+ recordsToUpdate.get(0).getDocumentId() + "$" + new Date()
				+ "$" + AppConstants.MIN + "$" + desc + "$"
				+ recordsToUpdate.get(0).getWarehouseName() + "$"
				+ recordsToUpdate.get(0).getStorageLocation() + "$"
				+ recordsToUpdate.get(0).getStorageBin() + "$"
				+ AppConstants.ADD + "$"
				+ recordsToUpdate.get(0).getCompanyId();

		Queue queue = QueueFactory
				.getQueue("UpdateProductTransactionListIndex-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/producttransactiontaskqueue").param(
				"ProductTransactionDetaiilsValueKey", typeducumentName));

	}

	private ArrayList<ProductInventoryTransaction> getProductTransactions(
			ArrayList<ProductInventoryTransaction> productInventoryList,
			int prodID) {

		ArrayList<ProductInventoryTransaction> productWiseTransactionLIst = new ArrayList<ProductInventoryTransaction>();
		for (int i = 0; i < productInventoryList.size(); i++) {
			if (productInventoryList.get(i).getProductId() == prodID) {
				productWiseTransactionLIst.add(productInventoryList.get(i));
			}
		}
		System.out.println("productWiseTransactionLIst size"
				+ productWiseTransactionLIst.size());
		return productWiseTransactionLIst;
	}

	@Override
	public void updateProductInventoryListIndex(Long companyId) {
		System.out.println("rohan companyId" + companyId);
		String typeducumentName = "UpdateIndex" + "$" + companyId;
		Queue queue = QueueFactory
				.getQueue("UpdateProductTransactionListIndex-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/producttransactiontaskqueue").param(
				"ProductTransactionDetaiilsValueKey", typeducumentName));
	}

	/**
	 * This is used for canceling MIN and reverse stock by creating transactions
	 * Date : 20/1/2017
	 */

	@Override
	public String cancelMIN(MaterialIssueNote min) {
		if (min != null) {
			List<MaterialMovementNote> mmnList = ofy().load()
					.type(MaterialMovementNote.class)
					.filter("companyId", min.getCompanyId())
					.filter("mmnMinId", min.getCount()).list();
			HashMap<Integer, Double> prodQtyMap = new HashMap<Integer, Double>();
			if (mmnList.size() != 0) {
				System.out.println("MMN " + mmnList.size());
				for (MaterialMovementNote mmn : mmnList) {
					if (mmn.getStatus().equals(MaterialIssueNote.CREATED)
							|| mmn.getStatus().equals(
									MaterialIssueNote.REQUESTED)) {
						return "MMN Id: "
								+ mmn.getCount()
								+ " is created against MIN. Please update stock through MMN and then cancel MIN.";
					}
					for (MaterialProduct prod : mmn.getSubProductTableMmn()) {
						if (prodQtyMap.size() == 0) {
							System.out.println("map zero");
							System.out
									.println(prod.getMaterialProductId()
											+ " "
											+ prod.getMaterialProductRequiredQuantity());
							prodQtyMap.put(prod.getMaterialProductId(),
									prod.getMaterialProductRequiredQuantity());
						} else {
							System.out.println(prod.getMaterialProductId());
							double sum = 0;
							if (prodQtyMap.containsKey(prod
									.getMaterialProductId())) {
								System.out
										.println("PRODUCT ALREADY EXIST..!!!");
								sum = prod.getMaterialProductRequiredQuantity()
										+ prodQtyMap.get(prod
												.getMaterialProductId());
								prodQtyMap
										.put(prod.getMaterialProductId(), sum);
							} else {
								System.out.println("exist else");
								prodQtyMap
										.put(prod.getMaterialProductId(),
												prod.getMaterialProductRequiredQuantity());
							}
						}
					}
				}

				System.out.println("MMN PRODUCT DETAILS:");
				for (Integer key : prodQtyMap.keySet()) {
					System.out.println("Product Id : " + key + " QTY : "
							+ prodQtyMap.get(key));
				}

				HashMap<Integer, Double> prodRemQtyMap = new HashMap<Integer, Double>();
				for (MaterialProduct prod : min.getSubProductTablemin()) {
					if (prodQtyMap.containsKey(prod.getMaterialProductId())) {
						if (prod.getMaterialProductRequiredQuantity() != prodQtyMap
								.get(prod.getMaterialProductId())) {
							double remQty = prod
									.getMaterialProductRequiredQuantity()
									- prodQtyMap.get(prod
											.getMaterialProductId());
							prodRemQtyMap.put(prod.getMaterialProductId(),
									remQty);
						}
					} else {
						prodRemQtyMap.put(prod.getMaterialProductId(),
								prod.getMaterialProductRequiredQuantity());
					}
				}

				System.out.println("REM PRODUCT DETAILS:");
				for (Integer key : prodRemQtyMap.keySet()) {
					System.out.println("Product Id : " + key + " QTY : "
							+ prodRemQtyMap.get(key));
				}

				if (prodRemQtyMap.size() != 0) {
					String message = "Please update the stock of following product through MMN \n";
					for (Integer key : prodRemQtyMap.keySet()) {
						message = message + "Product Id: " + key
								+ " Remaining Qty: " + prodRemQtyMap.get(key)
								+ "\n";
					}
					return message;
				}
				ofy().save().entity(min).now();
			} else {
				ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
				for (MaterialProduct product : min.getSubProductTablemin()) {
					InventoryTransactionLineItem item = new InventoryTransactionLineItem();
					item.setCompanyId(min.getCompanyId());
					item.setBranch(min.getBranch());
					item.setDocumentType(AppConstants.MIN);
					item.setDocumentId(min.getCount());
					item.setDocumentDate(min.getCreationDate());
					item.setDocumnetTitle(min.getMinTitle()
							+ " (Cancelled MIN)");
					if(min.getServiceId()>0)
						item.setServiceId(min.getServiceId()+"");
					item.setProdId(product.getMaterialProductId());
					item.setUpdateQty(product
							.getMaterialProductRequiredQuantity());
					item.setOperation(AppConstants.ADD);
					item.setWarehouseName(product.getMaterialProductWarehouse());
					item.setStorageLocation(product
							.getMaterialProductStorageLocation());
					item.setStorageBin(product.getMaterialProductStorageBin());
					itemList.add(item);
				}
				UpdateStock.setProductInventory(itemList);
				ofy().save().entity(min).now();
			}
			return "MIN Cancelled Successfully.";

		}
		return "Not getting the MIN Details.";

	}

	@Override
	public String cancelDocument(SuperModel model) {

		if (model instanceof MaterialMovementNote) {

			MaterialMovementNote mmn = (MaterialMovementNote) model;

			ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
			for (MaterialProduct product : mmn.getSubProductTableMmn()) {
				InventoryTransactionLineItem item = new InventoryTransactionLineItem();
				item.setCompanyId(mmn.getCompanyId());
				item.setBranch(mmn.getBranch());
				item.setDocumentType(AppConstants.MMN);
				item.setDocumentId(mmn.getCount());
				item.setDocumentDate(mmn.getCreationDate());
				item.setDocumnetTitle(mmn.getMmnTitle() + " (Cancelled MMN)");
				if(mmn.getServiceId()>0)
					item.setServiceId(mmn.getServiceId()+"");
				item.setProdId(product.getMaterialProductId());
				item.setUpdateQty(product.getMaterialProductRequiredQuantity());

				item.setWarehouseName(product.getMaterialProductWarehouse());
				item.setStorageLocation(product
						.getMaterialProductStorageLocation());
				item.setStorageBin(product.getMaterialProductStorageBin());

				String transaction = mmn.getTransDirection().trim();
				String operation = null;
				if (transaction.equals("IN")) {
					operation = AppConstants.SUBTRACT;
				} else if (transaction.equals("OUT")) {
					operation = AppConstants.ADD;
				} else if (transaction.equals("TRANSFEROUT")) {
					operation = AppConstants.ADD;
				} else if (transaction.equals("TRANSFERIN")) {
					operation = AppConstants.SUBTRACT;
					item.setWarehouseName(mmn.getTransToWareHouse());
					item.setStorageLocation(mmn.getTransToStorLoc());
					item.setStorageBin(mmn.getTransToStorBin());
				}
				item.setOperation(operation);
				itemList.add(item);
			}
			UpdateStock.setProductInventory(itemList);

			ofy().save().entity(mmn);

			return "MMN Cancelled Successfully.";

		}
		if (model instanceof GRN) {

			GRN grn = (GRN) model;

			ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
			for (GRNDetails product : grn.getInventoryProductItem()) {
				/** Date 15-04-2019 by Vijay for NBHC CCPM
				 * Des :- if GRN already cancelled by Partial GRN(specific product cancelled) then it will not cancelled
				 * by full cancellation (Normal cancellation) so added if condition 
				 * @author Anil , date : 05-07-2019
				 * checked partial cancellation flag for partially cancellation of GRN
				 * and added condition to cancel normal cancellation of GRN
				 */
				if(grn.isPartialCancellationFlag()&&!product.getStatus().equals(GRN.CANCELLED)){
					InventoryTransactionLineItem item = new InventoryTransactionLineItem();
					item.setCompanyId(grn.getCompanyId());
					item.setBranch(grn.getBranch());
					item.setDocumentType(AppConstants.GRN);
					item.setDocumentId(grn.getCount());
					item.setDocumentDate(grn.getCreationDate());
					item.setDocumnetTitle("" + " (Cancelled GRN)");
					item.setProdId(product.getProductID());
					item.setUpdateQty(product.getProductQuantity());
					item.setOperation(AppConstants.SUBTRACT);
					item.setWarehouseName(product.getWarehouseLocation());
					item.setStorageLocation(product.getStorageLoc());
					item.setStorageBin(product.getStorageBin());
					/**
					 * nidhi
					 * 23-08-2018
					 * for map serial number
					 */
					HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
						
					if(product.getProSerialNoDetails()!=null && product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
						prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
						for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
							 ProductSerialNoMapping element = new ProductSerialNoMapping();
							 element.setAvailableStatus(pr.isAvailableStatus());
							 element.setNewAddNo(pr.isNewAddNo());
							 element.setStatus(pr.isStatus());
							 element.setProSerialNo(pr.getProSerialNo());
							 prserdt.get(0).add(element);
						}
						for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
					      {
							ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
							if(!pr.isStatus()){
									itr.remove();
							}
					      }
					}
					item.setProSerialNoDetails(prserdt);
					itemList.add(item);
				}else{
					InventoryTransactionLineItem item = new InventoryTransactionLineItem();
					item.setCompanyId(grn.getCompanyId());
					item.setBranch(grn.getBranch());
					item.setDocumentType(AppConstants.GRN);
					item.setDocumentId(grn.getCount());
					item.setDocumentDate(grn.getCreationDate());
					item.setDocumnetTitle("" + " (Cancelled GRN)");
					
					item.setProdId(product.getProductID());
					item.setUpdateQty(product.getProductQuantity());
					item.setOperation(AppConstants.SUBTRACT);
					item.setWarehouseName(product.getWarehouseLocation());
					item.setStorageLocation(product.getStorageLoc());
					item.setStorageBin(product.getStorageBin());
					/**
					 * nidhi
					 * 23-08-2018
					 * for map serial number
					 */
					HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
						
					if(product.getProSerialNoDetails()!=null && product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
						prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
						for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
							 ProductSerialNoMapping element = new ProductSerialNoMapping();
							 element.setAvailableStatus(pr.isAvailableStatus());
							 element.setNewAddNo(pr.isNewAddNo());
							 element.setStatus(pr.isStatus());
							 element.setProSerialNo(pr.getProSerialNo());
							 prserdt.get(0).add(element);
						}
						for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
					      {
							ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
							if(!pr.isStatus()){
									itr.remove();
							}
					      }
					}
					item.setProSerialNoDetails(prserdt);
					itemList.add(item);
				}
			}
			if(itemList.size()!=0){ //Date 15-04-2019 by Vijay 
			UpdateStock.setProductInventory(itemList);
			}
			ofy().save().entity(grn);
			/**
			 * @author Vijay Chougule 27-12-2019
			 * Des :- NBHC CCPM BUG :- accounting Interface not activated but creating records in accounting interface
			 * So added process configuration of accounting interface
			 */
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "Accounting Interface", grn.getCompanyId())){
				/**
				 * Dev : Rahul Verma Dated : 28 Nov 2017 Description : Cancellation
				 * to create accouting Interface for GRN
				 */

				logger.log(Level.SEVERE,"grn.getInventoryProductItem().size()"+grn.getInventoryProductItem().size());
					for (int i = 0; i < grn.getInventoryProductItem().size(); i++) {
						/** Date 15-04-2019 by Vijay for NBHC CCPM
						 * Des :- if GRN already cancelled by Partial GRN(specific product cancelled) then it will not cancelled
						 * by full cancellation (Normal cancellation) so added if condition 
						 * @author Anil , date : 05-07-2019
						 * checked partial cancellation flag for partially cancellation of GRN
						 * and added condition to cancel normal cancellation of GRN
						 */
						if(grn.isPartialCancellationFlag()&&!grn.getInventoryProductItem().get(i).getStatus().equals(GRN.CANCELLED)){
							
							String unitofmeasurement = grn.getInventoryProductItem()
									.get(i).getUnitOfmeasurement();
							double productprice = grn.getInventoryProductItem().get(i)
									.getProdPrice();
							int prodId = grn.getInventoryProductItem().get(i)
									.getProductID();
							String productCode = grn.getInventoryProductItem().get(i)
									.getProductCode();
							String productName = grn.getInventoryProductItem().get(i)
									.getProductName();
							double productQuantity = grn.getInventoryProductItem()
									.get(i).getProductQuantity();
							double totalAmount = grn.getInventoryProductItem().get(i)
									.getProdPrice()
									* grn.getProductDetails().get(i)
											.getProductQuantity();
							String prodRefId = "";
							ProductDetailsPO po = grn.getInventoryProductItem().get(i);
							if (grn.getInventoryProductItem().get(i).getProductRefId()
									.trim() != null) {
								prodRefId = grn.getInventoryProductItem().get(i)
										.getProductRefId().trim();
							}
							String vName = grn.getVendorInfo().getFullName().trim();
							long vCell = grn.getVendorInfo().getCellNumber();
							int vId = grn.getVendorInfo().getCount();
							String vendorRefId = "";
							if (grn.getVendorReferenceId() != null
									&& grn.getVendorReferenceId().trim().length() > 0) {
								vendorRefId = grn.getVendorReferenceId();
							} else {
								vendorRefId = 0 + "";
							}
							String warehouseName = grn.getInventoryProductItem().get(i)
									.getWarehouseLocation().trim();
							logger.log(Level.SEVERE,"warehouseName" + warehouseName);
							WareHouse warehouse = ofy().load().type(WareHouse.class)
									.filter("companyId", grn.getCompanyId())
									.filter("buisnessUnitName", warehouseName).first()
									.now();
							logger.log(Level.SEVERE,"warehouse" + warehouse);
							String warehouseCode = "";
							if (warehouse.getWarehouseCode().trim() != null) {
								warehouseCode = warehouse.getWarehouseCode().trim();
							}
							/**
							 * Added by rahul Verma on 19 June 2017 Only for NBHC This
							 * will create accounting interface for cancellation
							 */
		
							UpdateAccountingInterface.updateTally(
									new Date(),// accountingInterfaceCreationDate
									grn.getEmployee(),// accountingInterfaceCreatedBy
									GRN.CANCELLED,
									AppConstants.STATUS,// Status
									grn.getRefNo2(),// remark /*** Date 15-04-2019 by Vijay for NBHC CCPM need Bill number to map in SAP **/
									"Inventory",// module
									"GRN",// documentType
									grn.getCount(),// docID
									grn.getTitle(),// DOCtile
									grn.getCreationDate(), // docDate
									AppConstants.DOCUMENTGL,// docGL
									grn.getPoNumber(),// ref doc no.1
									grn.getPoCreationDate(),// ref doc date.1
									AppConstants.REFDOCPO,// ref doc type 1
									grn.getRefNo(),// ref doc no 2
									grn.getGrnReferenceDate(),// ref doc date 2
									AppConstants.REFDOCPO,// ref doc type 2
									AppConstants.ACCOUNTTYPEAP,// accountType
									0,// custID
									"",// custName
									0,// custCell
									Integer.parseInt(vendorRefId.trim()),// vendor ID
									vName,// vendor NAme
									vCell,// vendorCell
									0,// empId
									" ",// empNAme
									grn.getBranch(),// branch
									grn.getEmployee(),// personResponsible
									"",// requestedBY
									grn.getApproverName(),// approvedBY
									"",// paymentmethod
									null,// paymentdate
									"",// cheqno.
									null,// cheqDate
									null,// bankName
									null,// bankAccount
									0,// transferReferenceNumber
									null,// transactionDate
									null,// contract start date
									null, // contract end date
									prodId,// prod ID
									productCode,// prod Code
									productName,// productNAme
									productQuantity,// prodQuantity
									null,// productDate
									0,// duration
									0,// services
									unitofmeasurement,// unit of measurement
									productprice,// productPrice
									0,// VATpercent
									0,// VATamt
									0,// VATglAccount
									0,// serviceTaxPercent
									0,// serviceTaxAmount
									0,// serviceTaxGLaccount
									"",// cform
									0,// cformpercent
									0,// cformamount
									0,// cformGlaccount
									0,// totalamouNT
									0,// NET PAPAYABLE
									"",// Contract Category
									0,// amount Recieved
									0.0,// base Amt for tdscal in pay by rohan
									0, // tds percentage by rohan
									0,// tds amount by rohan
									"", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0,
									"", 0, "", 0, "", 0, "", 0, "", 0, "", "", "", "",
									"", "", 0, grn.getCompanyId(), null, // billing from
																			// date
																			// (rohan)
									null, // billing to date (rohan)
									warehouseName, // Warehouse
									warehouseCode, // warehouseCode
									prodRefId, // ProductRefId
									AccountingInterface.OUTBOUNDDIRECTION, // Direction
									AppConstants.CCPM, // sourceSystem
									AppConstants.NBHC, // Destination System
									null,null,null

									);
					    }else{
					    	
					    	
					    	String unitofmeasurement = grn.getInventoryProductItem()
									.get(i).getUnitOfmeasurement();
							double productprice = grn.getInventoryProductItem().get(i)
									.getProdPrice();
							int prodId = grn.getInventoryProductItem().get(i)
									.getProductID();
							String productCode = grn.getInventoryProductItem().get(i)
									.getProductCode();
							String productName = grn.getInventoryProductItem().get(i)
									.getProductName();
							double productQuantity = grn.getInventoryProductItem()
									.get(i).getProductQuantity();
							double totalAmount = grn.getInventoryProductItem().get(i)
									.getProdPrice()
									* grn.getProductDetails().get(i)
											.getProductQuantity();
							String prodRefId = "";
							ProductDetailsPO po = grn.getInventoryProductItem().get(i);
							if (grn.getInventoryProductItem().get(i).getProductRefId()
									.trim() != null) {
								prodRefId = grn.getInventoryProductItem().get(i)
										.getProductRefId().trim();
							}
							String vName = grn.getVendorInfo().getFullName().trim();
							long vCell = grn.getVendorInfo().getCellNumber();
							int vId = grn.getVendorInfo().getCount();
							String vendorRefId = "";
							if (grn.getVendorReferenceId() != null
									&& grn.getVendorReferenceId().trim().length() > 0) {
								vendorRefId = grn.getVendorReferenceId();
							} else {
								vendorRefId = 0 + "";
							}
							String warehouseName = grn.getInventoryProductItem().get(i)
									.getWarehouseLocation().trim();
							logger.log(Level.SEVERE,"warehouseName" + warehouseName);
							WareHouse warehouse = ofy().load().type(WareHouse.class)
									.filter("companyId", grn.getCompanyId())
									.filter("buisnessUnitName", warehouseName).first()
									.now();
							logger.log(Level.SEVERE,"warehouse" + warehouse);
							String warehouseCode = "";
							if (warehouse.getWarehouseCode().trim() != null) {
								warehouseCode = warehouse.getWarehouseCode().trim();
							}
							/**
							 * Added by rahul Verma on 19 June 2017 Only for NBHC This
							 * will create accounting interface for cancellation
							 */
		
							UpdateAccountingInterface.updateTally(
									new Date(),// accountingInterfaceCreationDate
									grn.getEmployee(),// accountingInterfaceCreatedBy
									GRN.CANCELLED,
									AppConstants.STATUS,// Status
									grn.getRefNo2(),// remark /*** Date 15-04-2019 by Vijay for NBHC CCPM need Bill number to map in SAP **/
									"Inventory",// module
									"GRN",// documentType
									grn.getCount(),// docID
									grn.getTitle(),// DOCtile
									grn.getCreationDate(), // docDate
									AppConstants.DOCUMENTGL,// docGL
									grn.getPoNumber(),// ref doc no.1
									grn.getPoCreationDate(),// ref doc date.1
									AppConstants.REFDOCPO,// ref doc type 1
									grn.getRefNo(),// ref doc no 2
									grn.getGrnReferenceDate(),// ref doc date 2
									AppConstants.REFDOCPO,// ref doc type 2
									AppConstants.ACCOUNTTYPEAP,// accountType
									0,// custID
									"",// custName
									0,// custCell
									Integer.parseInt(vendorRefId.trim()),// vendor ID
									vName,// vendor NAme
									vCell,// vendorCell
									0,// empId
									" ",// empNAme
									grn.getBranch(),// branch
									grn.getEmployee(),// personResponsible
									"",// requestedBY
									grn.getApproverName(),// approvedBY
									"",// paymentmethod
									null,// paymentdate
									"",// cheqno.
									null,// cheqDate
									null,// bankName
									null,// bankAccount
									0,// transferReferenceNumber
									null,// transactionDate
									null,// contract start date
									null, // contract end date
									prodId,// prod ID
									productCode,// prod Code
									productName,// productNAme
									productQuantity,// prodQuantity
									null,// productDate
									0,// duration
									0,// services
									unitofmeasurement,// unit of measurement
									productprice,// productPrice
									0,// VATpercent
									0,// VATamt
									0,// VATglAccount
									0,// serviceTaxPercent
									0,// serviceTaxAmount
									0,// serviceTaxGLaccount
									"",// cform
									0,// cformpercent
									0,// cformamount
									0,// cformGlaccount
									0,// totalamouNT
									0,// NET PAPAYABLE
									"",// Contract Category
									0,// amount Recieved
									0.0,// base Amt for tdscal in pay by rohan
									0, // tds percentage by rohan
									0,// tds amount by rohan
									"", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0,
									"", 0, "", 0, "", 0, "", 0, "", 0, "", "", "", "",
									"", "", 0, grn.getCompanyId(), null, // billing from
																			// date
																			// (rohan)
									null, // billing to date (rohan)
									warehouseName, // Warehouse
									warehouseCode, // warehouseCode
									prodRefId, // ProductRefId
									AccountingInterface.OUTBOUNDDIRECTION, // Direction
									AppConstants.CCPM, // sourceSystem
									AppConstants.NBHC, // Destination System
									null,null,null

									);
					    	
					    	
					    	
					    	
					    	
					    }
					}

				/**
				 * ENDS for Rahul Verma
				 */
			}
			
			return "GRN Cancelled Successfully.";

		}
		return null;
	}

	@Override
	public void updatePaymentDetailsRecords(Long companyId) {

		// List<CustomerPayment> paymentList = new ArrayList<CustomerPayment>();
		// List<CustomerPayment> paymentlis = new ArrayList<CustomerPayment>();
		//
		// paymentList =
		// ofy().load().type(CustomerPayment.class).filter("companyId",
		// companyId).list();
		//
		// for (int i = 0; i < paymentList.size(); i++) {
		//
		// CustomerPayment payment = new CustomerPayment();
		// payment.setCompanyId(paymentList.get(i).getCompanyId());
		//
		// //
		// payment.setTdsPercentage(Double.parseDouble(paymentList.get(i).getTdsPercentage()+""));
		// payment.setTdsPercentage(0.0);
		//
		// paymentlis.add(payment);
		// }
		//
		// ofy().save().entities(paymentlis).now();
	}

	/**
	 * rohan has create these methods for Accounting interface Date : 23/2/2017
	 */

	@Override
	public ArrayList<AccountingInterface> getAccountingInterfaceDataSynched(
			ArrayList<AccountingInterface> acclist, String status, String syncBy) {

		logger.log(Level.SEVERE,"hi form impl");
		ArrayList<AccountingInterface> updatedList = new ArrayList<AccountingInterface>();
		/** date 16.4.2018 added by komal to add  unique id for documents which are synched together**/
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		Date date = new Date();
		Calendar cal=Calendar.getInstance();
		String uniqueId = syncBy+"-"+fmt.format(date);
		/***date 28.7.2018 added by komal for new date format **/
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
		/**end**/
		System.out.println("in server side lis size before "
				+ updatedList.size());
		for (AccountingInterface accountingInterface : acclist) {
			 //komal
			accountingInterface.setDocumentUniqueId(uniqueId);
			/***date 28.7.2018 added by komal for new date format **/
			accountingInterface.setDateofSynch(currentDate);
			accountingInterface.setStatus(status);
			accountingInterface.setSynchedBy(syncBy);

			updatedList.add(accountingInterface);
		}

		System.out.println("in server side list after " + updatedList.size());

		ofy().save().entities(updatedList).now();
		return updatedList;
	}

	@Override
	public void updateSynchDataWithSynchDate(
			ArrayList<AccountingInterface> acclist) {

		List<AccountingInterface> updatedList = new ArrayList<AccountingInterface>();
		for (AccountingInterface accountingInterface : acclist) {

			accountingInterface.setDateofSynch(new Date());
			updatedList.add(accountingInterface);
		}

		ofy().save().entities(updatedList).now();
	}

	@Override
	public void cancelDeleteFromTallyRecord(
			ArrayList<AccountingInterface> acclist) {

		List<AccountingInterface> updatedList = new ArrayList<AccountingInterface>();
		for (AccountingInterface accountingInterface : acclist) {

			accountingInterface.setStatus(AccountingInterface.CANCELLED);
			updatedList.add(accountingInterface);
		}

		ofy().save().entities(updatedList).now();
	}

	@Override
	public void reactOnCancelContract(Contract con, String remark,
			String loginUser) {
		
		logger.log(Level.SEVERE, "Company Id " + con.getCompanyId());
		System.out.println("rohan companyId" + con.getCompanyId());
		String typeducumentName = "CancelContract" + "$" + con.getCompanyId()
				+ "$" + con.getCount() + "$" + remark + "$" + loginUser +"$" + con.getStatus();
		logger.log(Level.SEVERE, "Company Id " + typeducumentName);
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/documentCancellationQueue").param(
				"taskKeyAndValue", typeducumentName));
	}

	@Override
	public void depositepaymentAmtIntoPettyCash(int paymentdocId,
			long companyId, String personResponible, Date depositDate, String savetime, double payreceived,String pettyCashName) {

		ProcessName processname = ofy().load().type(ProcessName.class)
				.filter("processName", "CustomerPayment")
				.filter("status", true).filter("companyId", companyId).first()
				.now();
		ProcessConfiguration processconfiguration = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("processList.processName", "CustomerPayment")
				.filter("processList.processType", "PaymentAddIntoPettyCash")
				.filter("processList.status", true)
				.filter("companyId", companyId).first().now();

		if (processname != null && processconfiguration != null) {

			GenricServiceImpl genimpl = new GenricServiceImpl();

			/**
			 * Date 24-10-2018 By Vijay for Pettycash current balance issue added below querry
			 */
//			PettyCash pettybal = pettyCashbal;
			PettyCash pettybal = ofy().load().type(PettyCash.class).filter("pettyCashName", pettyCashName).first().now();
			/**
			 * ends here
			 */
			 
			double currentBalance = pettybal.getPettyCashBalance()
					+ payreceived;

			/************ vijay for transaction list ***********************/
			double openingstock = pettybal.getPettyCashBalance();

			// Set deposit info to petty cash deposits entity
			PettyCashDeposits pettyDeposits = new PettyCashDeposits();
			pettyDeposits.setPettyCashName(pettybal.getPettyCashName());
			if (personResponible != null) {
				pettyDeposits.setEmployee(personResponible);
			}
			pettyDeposits.setDepositAmount(payreceived);
			pettyDeposits.setDepositDate(depositDate);
			pettyDeposits.setDepositTime(savetime);
			pettyDeposits.setCompanyId(companyId);
			pettyDeposits.setPaymentdocId(paymentdocId);

			// Set pettycash balance value
			pettybal.setPettyCashBalance(currentBalance);
			genimpl.save(pettybal);
			genimpl.save(pettyDeposits);

			/**************************** vijay for transaction list *******************/
			System.out.println("hi vijay ====");
			PettyCashTransaction pettycash = new PettyCashTransaction();
			pettycash.setOpeningStock(openingstock);
			pettycash.setTransactionAmount(payreceived);
			pettycash.setClosingStock(currentBalance);
			pettycash.setDocumentId(paymentdocId);
			pettycash.setDocumentName("Payment Document Cash Deposit");
			pettycash.setTransactionDate(depositDate);
			pettycash.setAppoverName("");
			if (pettybal.getEmployee() != null) {
				pettycash.setPersonResponsible(pettybal.getEmployee());
			}
			pettycash.setAction("+");
			pettycash.setDocumentTitle(pettybal.getPettyCashName());
			pettycash.setPettyCashName(pettybal.getPettyCashName());
			pettycash.setTransactionTime(savetime);
			pettycash.setCompanyId(companyId);

			genimpl.save(pettycash);

		}

	}

	@Override
	public String cancelServices(ArrayList<Service> list , String remark , String loggedInUser) {
		logger.log(Level.SEVERE, "Services list size"+list.size());
		if (list.size() != 0 && list.size() <= 100) {
			cancelAllServices(list, remark, loggedInUser);
			logger.log(Level.SEVERE, "Services cancelled successfully.");
			return "Services cancelled successfully.";
		} else if(list.size() != 0 && list.size() > 100){
			long companyId = 0;
			List<Integer> serviceIdList = new ArrayList<Integer>();
			for(Service ser : list){
				serviceIdList.add(ser.getCount());
				companyId = ser.getCompanyId();
			}
			Gson gson = new Gson();
			String str = gson.toJson(serviceIdList);

			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "ServiceMarkCancel").param("Remark", remark)
					.param("user", loggedInUser).param("serviceId", str).param("companyId", companyId+""));
			return "Services cancellation process started. It may take few minutes";
		}else {
			return "No service is selected for cancellation.";
		}
		
		

	}

	@Override
	public String cancelDocument(String taskName, Long companyId) {

		if (taskName.equals("PartialServiceCancellation")) {
			List<Contract> cancelContractList = ofy().load()
					.type(Contract.class).filter("companyId", companyId)
					.filter("status", Contract.CANCELLED).list();
			logger.log(Level.SEVERE, "CANCEL CONTRACT LIST SIZE :: "
					+ cancelContractList.size());
			for (Contract cancelContract : cancelContractList) {
				logger = Logger.getLogger("CONTRACT ID ---- "
						+ cancelContract.getCount());
				String taskName1 = taskName + "$" + companyId + "$"
						+ cancelContract.getCount();

				Queue queue = QueueFactory
						.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl(
						"/slick_erp/documentCancellationQueue").param(
						"taskKeyAndValue", taskName1));
			}

			return "Successfull.";

		} else {
			taskName = taskName + "$" + companyId;
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", taskName));
			return "Successfull.";
		}
	}

	@Override
	public void reactOnCancelExpenseMngt(MultipleExpenseMngt model,
			String remark, String loginUser) {

		PettyCash petty = ofy().load().type(PettyCash.class)
				.filter("pettyCashName", model.getPettyCashName()).first()
				.now();
		if (petty != null) {
			
			double expAmt=model.getTotalAmount();
			
			//Ashwini Patil Date:19-10-2022 Added below logic as entire total amount was getting returned even if there is previous return history. 
			if(model.getReturnExpenseHistory().size()!=0){
				
				double returnamt = 0;
				for(int i=0;i<model.getReturnExpenseHistory().size();i++){
					
					returnamt += model.getReturnExpenseHistory().get(i).getExpenseReturnAmt();
				}
				expAmt = expAmt - returnamt ;				 
			}	
					
			
			// int expAmount=(int) expAmt;
			double mainBalance = petty.getPettyCashBalance();

			double deductAmount = mainBalance + expAmt;
			petty.setPettyCashBalance(deductAmount);
			ofy().save().entities(petty).now();

			createExpenseTransactionList(model, mainBalance, expAmt);

			/**
			 * @author Anil
			 * @since 05-01-2021
			 * While cancelling expenses , expense was not getting updated
			 * raised by  Vaishnavi
			 */
			ofy().save().entity(model).now();

		}
	}

	private void createExpenseTransactionList(MultipleExpenseMngt model,
			double mainBalance, double expAmt) {

		PettyCashTransaction pettycash = new PettyCashTransaction();
		pettycash.setOpeningStock(mainBalance);
		pettycash.setTransactionAmount(expAmt);
		pettycash.setClosingStock(mainBalance + expAmt);
		pettycash.setDocumentId(model.getCount());
		pettycash.setDocumentName("Expense Management");
		Date saveDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy h:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String datestring = sdf.format(saveDate);
		Logger logger = Logger.getLogger("NameOfYourLogger");
		logger.log(Level.SEVERE, " Date $$$$$$  === with IST" + datestring);
		String[] dateString = datestring.split(" ");
		String time = dateString[1];
		logger.log(Level.SEVERE, " Date ==" + dateString[0]);
		logger.log(Level.SEVERE, " Date time ==" + dateString[1]);
		pettycash.setTransactionTime(time);
		pettycash.setTransactionAmount(expAmt);
		try {
			pettycash.setTransactionDate(sdf.parse(datestring));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE, " date parse exception  ==" + e);
		}
		pettycash.setAppoverName(model.getApproverName());
		pettycash.setPersonResponsible(model.getEmployee());
		pettycash.setAction("+");
		pettycash.setDocumentTitle(model.getPettyCashName());
		pettycash.setCompanyId(model.getCompanyId());

		pettycash.setPettyCashName(model.getPettyCashName());

		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(pettycash);
	}

	@Override
	public String saveMultiplePaymentDetails(
			List<CustomerPayment> selectedPaymentList, String accntNumber,
			String bankName, String branchName, String chequeNumber,
			Date chequeDt, Date transferDt, String refNumber, String issueBy,
			String paymentMethod) {

		for (int i = 0; i < selectedPaymentList.size(); i++) {

			selectedPaymentList.get(i).setBankAccNo(accntNumber);
			selectedPaymentList.get(i).setBankName(bankName);
			selectedPaymentList.get(i).setBankBranch(branchName);
			selectedPaymentList.get(i).setChequeNo(chequeNumber);
			selectedPaymentList.get(i).setChequeDate(chequeDt);
			selectedPaymentList.get(i).setAmountTransferDate(transferDt);
			selectedPaymentList.get(i).setReferenceNo(refNumber);
			selectedPaymentList.get(i).setChequeIssuedBy(issueBy);

			selectedPaymentList.get(i).setPaymentReceived(
					selectedPaymentList.get(i).getPaymentAmt());
			selectedPaymentList.get(i).setStatus("Closed");
			selectedPaymentList.get(i).setPaymentMethod(paymentMethod);
		}

		ofy().save().entities(selectedPaymentList);

		return "save";
	}

	@Override
	public String updateStateCode(Long companyId) {

		DemoConfigurationImpl demoImpl = new DemoConfigurationImpl();
		List<State> stateList = ofy().load().type(State.class)
				.filter("companyId", companyId).list();
		for (State obj : stateList) {
			if (demoImpl.getStateCode(obj.getStateName().trim()) != null) {
				obj.setStateCode(demoImpl.getStateCode(obj.getStateName()
						.trim()));
			}
		}
		if (stateList.size() != 0) {
			ofy().save().entities(stateList).now();
		}
		return null;
	}

	@Override
	public String updateCustomer(Long companyId, String taskName1) {

		String taskName = "UpdateCustomerStatus" + "$" + companyId;
		Queue queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/documentCancellationQueue").param(
				"taskKeyAndValue", taskName));

		// List<String> statusList=new ArrayList<String>();
		// statusList.add("Created");
		// statusList.add("Prospect");
		// if(taskName.equals("UpdateCustomerStatus")){
		// List<Customer>
		// customerList=ofy().load().type(Customer.class).filter("companyId",
		// companyId).filter("businessProcess.status IN", statusList).list();
		// for(Customer object:customerList){
		// object.setStatus("Active");
		// }
		//
		// if(customerList.size()!=0){
		// ofy().save().entities(customerList);
		// }
		// }
		return "Data Updated Successfully";
	}

	/** date 01/11/2017 added by komal for setting CancelContract list **/
	public CancelContract setDataInCancelContract(int count, String branch,
			String cName, String status, String dType) {

		CancelContract canContract = new CancelContract();
		canContract.setDocumnetId(count);
		canContract.setBranch(branch);
		canContract.setBpName(cName);
		canContract.setDocumentStatus(status);
		canContract.setDocumnetName(dType);
		if (canContract.getDocumentStatus().equalsIgnoreCase(Contract.APPROVED)
				|| canContract.getDocumentStatus().equalsIgnoreCase(
						ServiceProject.COMPLETED)
				|| canContract.getDocumentStatus().equalsIgnoreCase(
						BillingDocument.BILLINGINVOICED)
				|| canContract.getDocumentStatus().equalsIgnoreCase(
						BillingDocument.PAYMENTCLOSED)) {

			if (canContract.getDocumnetName().equalsIgnoreCase(
					AppConstants.BILLINGDETAILS)
					&& canContract.getDocumentStatus().equalsIgnoreCase(
							BillingDocument.APPROVED)) {
				canContract.setRecordSelect(true);
			} else if (dType.equalsIgnoreCase("Tax Invoice")
					&& canContract.getDocumentStatus().equalsIgnoreCase(
							Invoice.APPROVED)) {
				canContract.setRecordSelect(false);
				canContract.setDocumnetName(AppConstants.PROCESSCONFIGINV);
			} else if (dType.equalsIgnoreCase("Proforma Invoice")
					&& canContract.getDocumentStatus().equalsIgnoreCase(
							BillingDocument.BILLINGINVOICED)) {
				canContract.setRecordSelect(false);
				canContract.setDocumnetName(AppConstants.PROCESSCONFIGINV);
			} else if (dType.equalsIgnoreCase("Proforma Invoice")
					|| dType.equalsIgnoreCase("Tax Invoice")) {
				canContract.setDocumnetName(AppConstants.PROCESSCONFIGINV);
				canContract.setRecordSelect(true);
			} else {

				canContract.setRecordSelect(false);
			}
		} else {
			if (dType.equalsIgnoreCase("Proforma Invoice")
					|| dType.equalsIgnoreCase("Tax Invoice")) {
				canContract.setDocumnetName(AppConstants.PROCESSCONFIGINV);
			}
			canContract.setRecordSelect(true);
		}

		return canContract;

	}

	@Override
	public void saveContractCancelData(Contract con, String remark,
			String loginUser, ArrayList<CancelContract> cancelContractList) {

		logger.log(Level.SEVERE, "Company Id " + con.getCompanyId());
		System.out.println("companyId" + con.getCompanyId());

		ArrayList<Integer> billingIdList = new ArrayList<Integer>();
		ArrayList<Integer> paymentIdList = new ArrayList<Integer>();
		ArrayList<Integer> invoiceIdList = new ArrayList<Integer>();
		ArrayList<Integer> serviceIdList = new ArrayList<Integer>();
		ArrayList<Integer> projectIdList = new ArrayList<Integer>();
		ArrayList<Integer> minIdList = new ArrayList<Integer>();
		ArrayList<Integer> mmnIdList = new ArrayList<Integer>();
		ArrayList<CancelContract> minmmnList = new ArrayList<CancelContract>();
		ArrayList<Integer> accInterfaceIdList = new ArrayList<Integer>();
		String documentType;
		String typeducumentName;
		Integer contractId = 0;
		Queue queue;
		for (CancelContract c : cancelContractList) {
			documentType = c.getDocumnetName();
			switch (documentType) {
			case AppConstants.CONTRACT :
				contractId = c.getDocumnetId();
				break;
			case AppConstants.BILLINGDETAILS:
				billingIdList.add(c.getDocumnetId());
				break;
			case AppConstants.PROCESSCONFIGINV:
				invoiceIdList.add(c.getDocumnetId());
				break;
			case AppConstants.PAYMENT:
				paymentIdList.add(c.getDocumnetId());
				break;
			case AppConstants.SERVICETYPECUST:
				serviceIdList.add(c.getDocumnetId());
				break;
			case "Project":
				projectIdList.add(c.getDocumnetId());
				break;
			case AppConstants.MIN:
				minIdList.add(c.getDocumnetId());
				CancelContract c1 = new CancelContract();
				c1.setDocumnetId(c.getDocumnetId());
				c1.setDocumnetName(AppConstants.MIN);
				minmmnList.add(c1);
				break;
			case AppConstants.MMN:
				mmnIdList.add(c.getDocumnetId());
				CancelContract c2 = new CancelContract();
				c2.setDocumnetId(c.getDocumnetId());
				c2.setDocumnetName(AppConstants.MMN);
				minmmnList.add(c2);
				break;
			case AppConstants.PROCESSTYPEACCINTERFACE:
				accInterfaceIdList.add(c.getDocumnetId());
				break;
			}
		}

		typeducumentName = "ClearList" + "$" + con.getCompanyId();
		logger.log(Level.SEVERE, "Company Id " + typeducumentName);
		queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/documentCancellationQueue").param(
				"taskKeyAndValue", typeducumentName));

		List<MaterialIssueNote> minList = new ArrayList<MaterialIssueNote>();
		List<MaterialMovementNote> mmnList = new ArrayList<MaterialMovementNote>();
//		List<MaterialIssueNote> getMinList = new ArrayList<MaterialIssueNote>();
//		List<MaterialMovementNote> getMmnList = new ArrayList<MaterialMovementNote>();
		Long count1 = 0l;
		Long mmnCount = 0l;
		NumberGeneration numberGeneration = ofy().load()
				.type(NumberGeneration.class)
				.filter("companyId", con.getCompanyId())
				.filter("processName", "ProductInventoryTransaction").first()
				.now();
		count = numberGeneration.getNumber();
		count1 = numberGeneration.getNumber();
		if (minIdList.size() > 0) {
			minList = ofy().load().type(MaterialIssueNote.class)
					.filter("companyId", con.getCompanyId())
					.filter("count IN", minIdList)
					.filter("status", MaterialIssueNote.APPROVED).list();
			if (minList.size() > 0) {
				count = minTransactionDetails(con.getCompanyId(), minList);
			}
		}
		if (mmnIdList.size() > 0) {
			mmnList = ofy().load().type(MaterialMovementNote.class)
					.filter("companyId", con.getCompanyId())
					.filter("count IN", mmnIdList)
					.filter("status", MaterialMovementNote.APPROVED).list();
			if (mmnList.size() > 0) {
				mmnCount = this.mmnTransactionDetails(con.getCompanyId(), mmnList);
			}
		}
		numberGeneration.setNumber(count + mmnCount);
		ofy().save().entity(numberGeneration);
		Gson gson = new Gson();
		queue = QueueFactory.getQueue("documentCancellation-queue");
		if (minList.size() > 0) {
			String str = gson.toJson(minList);
			typeducumentName = "InventoryTransaction" + "$"
					+ con.getCompanyId() + "$" + str + "$" + AppConstants.MIN;
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/documentCancellationQueue")
					.param("taskKeyAndValue", typeducumentName)
					.param("count", count1.toString()));
		}
		if (mmnList.size() > 0) {
			String str1 = gson.toJson(mmnList);
			String typeducumentName1 = "InventoryTransaction" + "$"
					+ con.getCompanyId() + "$" + str1 + "$" + AppConstants.MMN;
			;
			logger.log(Level.SEVERE, "Company Id " + typeducumentName1);
			queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/documentCancellationQueue")
					.param("taskKeyAndValue", typeducumentName1)
					.param("count", count.toString()));
		}

		for (Integer billingId : billingIdList) {
			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ billingId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.BILLINGDETAILS + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		for (Integer invoiceId : invoiceIdList) {
			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ invoiceId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.PROCESSCONFIGINV + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		for (Integer paymentId : paymentIdList) {
			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ paymentId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.PAYMENT + "$" + cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		
		String str = gson.toJson(serviceIdList);
			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ 0 + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.SERVICETYPECUST + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName).param("serviceId", str));
		
		for (Integer projectId : projectIdList) {
			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ projectId + "$" + remark + "$" + loginUser + "$"
					+ "Project" + "$" + cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}
		for (CancelContract cancelContract : minmmnList) {

			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ cancelContract.getDocumnetId() + "$" + remark + "$"
					+ loginUser + "$" + cancelContract.getDocumnetName() + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));

		}

		for (Integer accInterfaceId : accInterfaceIdList) {
			typeducumentName = "SelectedCancelContract" + "$"
					+ con.getCompanyId() + "$" + con.getCount() + "$"
					+ accInterfaceId + "$" + remark + "$" + loginUser + "$"
					+ AppConstants.PROCESSTYPEACCINTERFACE + "$"
					+ cancelContractList.size();
			logger.log(Level.SEVERE, "Company Id " + typeducumentName);
			queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl(
					"/slick_erp/documentCancellationQueue").param(
					"taskKeyAndValue", typeducumentName));
		}

		
		//documentType = "Contract";
		int documentId = 0;
		//Nidhi
//		9-06-2018
//		if(!(con.getStatus().equalsIgnoreCase(Contract.CANCELLED))){
		if(contractId !=0){
		typeducumentName = "SelectedCancelContract" + "$" + con.getCompanyId()
				+ "$" + con.getCount() + "$" + documentId + "$" + remark + "$"
				+ loginUser + "$" + AppConstants.CONTRACT + "$"
				+ cancelContractList.size();
		logger.log(Level.SEVERE, "Company Id " + typeducumentName);
		queue = QueueFactory.getQueue("documentCancellation-queue");
		queue.add(TaskOptions.Builder.withUrl(
				"/slick_erp/documentCancellationQueue").param(
				"taskKeyAndValue", typeducumentName));
		 }
	}
	
	
	
	@Override
	public ArrayList<CancelContract> getRecordsForContractCancel(Long companyId, int orderId, boolean contractStatus,boolean serviceFlag,String callFrom) {

		List<AccountingInterface> accInterfaceList = new ArrayList<AccountingInterface>();
		ArrayList<CancelContract> cancelContractList = new ArrayList<CancelContract>();
		List<BillingDocument> billingList = ofy().load()
				.type(BillingDocument.class).filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPEFORSERVICE).list();
		System.out.println("billing" + billingList.size() + "");
		if (billingList.size() != 0) {
			for (BillingDocument bDoc : billingList) {
				if (!(bDoc.getStatus().equals(BillingDocument.CANCELLED)) ) {
					if(contractStatus && (bDoc.getStatus().equals(BillingDocument.APPROVED) || bDoc.getStatus().equals(BillingDocument.BILLINGINVOICED))){
						
					}else{
						cancelContractList.add(setDataInCancelContract(
								bDoc.getCount(), bDoc.getBranch(), bDoc.getName(),
								bDoc.getStatus(), AppConstants.BILLINGDETAILS));
					}
					
				}
			}
		}
		List<Invoice> invoiceList = ofy().load().type(Invoice.class)
				.filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).list();
		System.out.println("billing" + invoiceList.size() + "");
		if (invoiceList.size() != 0) {
			for (Invoice invoice : invoiceList) {
				
				/*
				 * Ashwini Patil
				 * Date:5-12-2024
				 * Pecopp reported that when they cancel contract, all open documents gets cancelled and approved documents stays as it is. but accounting interface entries are getting cancelled.
				 * So invoices shows in approved state but respective accounting interface entries shows in cancelled state.
				 * this getRecordsForContractCancel method is getting used by 2 programs so handling this by callfrom parameter.
				 * so that change will not affect other programs flow.
				 * if callFrom is equal to cancelOpenStatusDocuments then do not cancel accounting interface entries
				 */
				
				if (invoice.getStatus().equals(Invoice.APPROVED)&&!callFrom.equals("cancelOpenStatusDocuments")) {
					List<AccountingInterface> acc = ofy()
							.load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", invoice.getCount())
							.filter("documentType",
									AppConstants.PROCESSCONFIGINV).list();
					if (acc != null)
						accInterfaceList.addAll(acc);
				}
				if (!(invoice.getStatus().equalsIgnoreCase(Invoice.CANCELLED))) {
					if(contractStatus && (invoice.getStatus().equals(Invoice.APPROVED) || invoice.getStatus().equals(Invoice.PROFORMAINVOICE) || invoice.getStatus().equals(BillingDocument.BILLINGINVOICED))){
						
					}else{
					cancelContractList.add(setDataInCancelContract(
							invoice.getCount(), invoice.getBranch(),
							invoice.getName(), invoice.getStatus(),
							invoice.getInvoiceType()));
					}
				}
			}
		}
		List<CustomerPayment> paymentList = ofy().load()
				.type(CustomerPayment.class).filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).list();
		System.out.println("billing" + paymentList.size() + "");
		if (paymentList.size() != 0) {
			for (CustomerPayment payment : paymentList) {
				if (payment.getStatus().equals(CustomerPayment.CLOSED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", payment.getCount())
							.filter("documentType", AppConstants.PAYMENT)
							.first().now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(payment.getStatus().equals(CustomerPayment.CANCELLED))) {
					if(contractStatus && (payment.getStatus().equals(CustomerPayment.CLOSED))){
						
					}else{
					cancelContractList.add(setDataInCancelContract(
							payment.getCount(), payment.getBranch(),
							payment.getName(), payment.getStatus(),
							AppConstants.PAYMENT));
					}
					}
			}
		}

		/**
		 * @author Anil , Date : 11-12-2019
		 * for NBHC we are not loading service ,we will delete/cancel it sepratly 
		 */
		if(serviceFlag){
			List<Service> serviceList = ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("contractCount", orderId).list();
			System.out.println("billing" + serviceList.size() + "");
			if (serviceList.size() != 0) {
				for (Service service : serviceList) {
					if (!(service.getStatus().equals(Service.CANCELLED))) {
						if(contractStatus && (service.getStatus().equals(Service.SERVICESTATUSCOMPLETED))){
							
						}else{
							cancelContractList.add(setDataInCancelContract(	
								service.getCount(), service.getBranch(),
								service.getCustomerName(), service.getStatus(),
								AppConstants.SERVICETYPECUST));
						}
					}
				}
			}
		}
		List<ServiceProject> projectList = ofy().load()
				.type(ServiceProject.class).filter("companyId", companyId)
				.filter("contractId", orderId).list();
		System.out.println("billing" + projectList.size() + "");
		if (projectList.size() != 0) {
			for (ServiceProject project : projectList) {
				if (!(project.getProjectStatus()
						.equals(ServiceProject.CANCELLED))) {
					if(contractStatus && (project.getProjectStatus().equals(ServiceProject.COMPLETED))){
						
					}else{
						cancelContractList.add(setDataInCancelContract(
							project.getCount(), "", project.getCustomerName(),
							project.getProjectStatus(), "Project"));
					}

					}
			}
		}
		List<MaterialIssueNote> minList = ofy().load()
				.type(MaterialIssueNote.class).filter("companyId", companyId)
				.filter("minSoId", orderId).list();
		System.out.println("billing" + minList.size() + "");
		if (minList.size() != 0) {
			for (MaterialIssueNote min : minList) {
				if (min.getStatus().equals(MaterialIssueNote.APPROVED) && !contractStatus) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", min.getCount())
							.filter("documentType", AppConstants.MIN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(min.getStatus().equals(MaterialIssueNote.CANCELLED))) {
					if(contractStatus && min.getStatus().equals(MaterialIssueNote.APPROVED)){
						
					}else{
						cancelContractList.add(setDataInCancelContract(min
								.getCount(), min.getBranch(), min.getCinfo()
								.getFullName(), min.getStatus(), AppConstants.MIN));
					}
					
				}
			}
		}
		List<MaterialMovementNote> mmnList = ofy().load()
				.type(MaterialMovementNote.class)
				.filter("companyId", companyId).filter("orderID", orderId + "")
				.list();
		System.out.println("billing" + mmnList.size() + "");
		if (mmnList.size() != 0) {
			for (MaterialMovementNote mmn : mmnList) {
				if (mmn.getStatus().equals(MaterialMovementNote.APPROVED) && !contractStatus) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", mmn.getCount())
							.filter("documentType", AppConstants.MMN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(mmn.getStatus().equals(MaterialMovementNote.CANCELLED))) {
					if(contractStatus && mmn.getStatus().equals(MaterialMovementNote.APPROVED)){
						
					}else{
						cancelContractList.add(setDataInCancelContract(
								mmn.getCount(), mmn.getBranch(), "",
								mmn.getStatus(), AppConstants.MMN));
					}
					
				}
			}
		}
		
		List<AccountingInterface> acntsList = ofy().load()
				.type(AccountingInterface.class).filter("companyId", companyId)
				.filter("documentID", orderId)
				.filter("documentType", "Contract").list();
		acntsList.addAll(accInterfaceList);
		System.out.println("billing" + acntsList.size() + "");
		if (acntsList.size() != 0) {
			for (AccountingInterface accInterface : acntsList) {
				if (!(accInterface.getDocumentStatus()
						.equals(AccountingInterface.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							accInterface.getCount(), accInterface.getBranch(),
							accInterface.getCustomerName(),
							accInterface.getDocumentStatus(),
							AppConstants.PROCESSTYPEACCINTERFACE));
				}
			}

		}
		System.out.println("list" + cancelContractList.size() + "");
		accInterfaceList.clear();
		return cancelContractList;
	}

/*	@Override
	public ArrayList<CancelContract> getRecordsForContractCancel(
			Long companyId, int orderId, boolean contractStatus) {

		List<AccountingInterface> accInterfaceList = new ArrayList<AccountingInterface>();
		ArrayList<CancelContract> cancelContractList = new ArrayList<CancelContract>();
		List<BillingDocument> billingList = ofy().load()
				.type(BillingDocument.class).filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPEFORSERVICE).list();
		System.out.println("billing" + billingList.size() + "");
		if (billingList.size() != 0) {
			for (BillingDocument bDoc : billingList) {
				if (!(bDoc.getStatus().equals(BillingDocument.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							bDoc.getCount(), bDoc.getBranch(), bDoc.getName(),
							bDoc.getStatus(), AppConstants.BILLINGDETAILS));
				}
			}
		}
		List<Invoice> invoiceList = ofy().load().type(Invoice.class)
				.filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).list();
		System.out.println("billing" + invoiceList.size() + "");
		if (invoiceList.size() != 0) {
			for (Invoice invoice : invoiceList) {
				if (invoice.getStatus().equals(Invoice.APPROVED)) {
					AccountingInterface acc = ofy()
							.load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", invoice.getCount())
							.filter("documentType",
									AppConstants.PROCESSCONFIGINV).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(invoice.getStatus().equalsIgnoreCase(Invoice.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							invoice.getCount(), invoice.getBranch(),
							invoice.getName(), invoice.getStatus(),
							invoice.getInvoiceType()));
				}
			}
		}
		List<CustomerPayment> paymentList = ofy().load()
				.type(CustomerPayment.class).filter("companyId", companyId)
				.filter("contractCount", orderId)
				.filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).list();
		System.out.println("billing" + paymentList.size() + "");
		if (paymentList.size() != 0) {
			for (CustomerPayment payment : paymentList) {
				if (payment.getStatus().equals(CustomerPayment.CLOSED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", payment.getCount())
							.filter("documentType", AppConstants.PAYMENT)
							.first().now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(payment.getStatus().equals(CustomerPayment.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							payment.getCount(), payment.getBranch(),
							payment.getName(), payment.getStatus(),
							AppConstants.PAYMENT));
				}
			}
		}

		List<Service> serviceList = ofy().load().type(Service.class)
				.filter("companyId", companyId)
				.filter("contractCount", orderId).list();
		System.out.println("billing" + serviceList.size() + "");
		if (serviceList.size() != 0) {
			for (Service service : serviceList) {
				if (!(service.getStatus().equals(Service.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							service.getCount(), service.getBranch(),
							service.getCustomerName(), service.getStatus(),
							AppConstants.SERVICETYPECUST));
				}
			}
		}
		List<ServiceProject> projectList = ofy().load()
				.type(ServiceProject.class).filter("companyId", companyId)
				.filter("contractId", orderId).list();
		System.out.println("billing" + projectList.size() + "");
		if (projectList.size() != 0) {
			for (ServiceProject project : projectList) {
				if (!(project.getProjectStatus()
						.equals(ServiceProject.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							project.getCount(), "", project.getCustomerName(),
							project.getProjectStatus(), "Project"));
				}
			}
		}
		List<MaterialIssueNote> minList = ofy().load()
				.type(MaterialIssueNote.class).filter("companyId", companyId)
				.filter("minSoId", orderId).list();
		System.out.println("billing" + minList.size() + "");
		if (minList.size() != 0) {
			for (MaterialIssueNote min : minList) {
				if (min.getStatus().equals(MaterialIssueNote.APPROVED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", min.getCount())
							.filter("documentType", AppConstants.MIN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(min.getStatus().equals(MaterialIssueNote.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(min
							.getCount(), min.getBranch(), min.getCinfo()
							.getFullName(), min.getStatus(), AppConstants.MIN));
				}
			}
		}
		List<MaterialMovementNote> mmnList = ofy().load()
				.type(MaterialMovementNote.class)
				.filter("companyId", companyId).filter("orderID", orderId + "")
				.list();
		System.out.println("billing" + mmnList.size() + "");
		if (mmnList.size() != 0) {
			for (MaterialMovementNote mmn : mmnList) {
				if (mmn.getStatus().equals(MaterialMovementNote.APPROVED)) {
					AccountingInterface acc = ofy().load()
							.type(AccountingInterface.class)
							.filter("companyId", companyId)
							.filter("documentID", mmn.getCount())
							.filter("documentType", AppConstants.MMN).first()
							.now();
					if (acc != null)
						accInterfaceList.add(acc);
				}
				if (!(mmn.getStatus().equals(MaterialMovementNote.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							mmn.getCount(), mmn.getBranch(), "",
							mmn.getStatus(), AppConstants.MMN));
				}
			}
		}
		List<AccountingInterface> acntsList = ofy().load()
				.type(AccountingInterface.class).filter("companyId", companyId)
				.filter("documentID", orderId)
				.filter("documentType", "Contract").list();
		acntsList.addAll(accInterfaceList);
		System.out.println("billing" + acntsList.size() + "");
		if (acntsList.size() != 0) {
			for (AccountingInterface accInterface : acntsList) {
				if (!(accInterface.getDocumentStatus()
						.equals(AccountingInterface.CANCELLED))) {
					cancelContractList.add(setDataInCancelContract(
							accInterface.getCount(), accInterface.getBranch(),
							accInterface.getCustomerName(),
							accInterface.getDocumentStatus(),
							AppConstants.PROCESSTYPEACCINTERFACE));
				}
			}

		}
		System.out.println("list" + cancelContractList.size() + "");
		accInterfaceList.clear();
		return cancelContractList;
	}*/

	/** date 24/11/2017 added by komal for min and mmn transactions **/
	public Long minTransactionDetails(Long companyId,
			List<MaterialIssueNote> minList) {
		HashSet<Integer> hset = new HashSet<Integer>();
		HashMap<String, Double> minMap = new HashMap<String, Double>();
		HashMap<String, Double> openingStockMap = new HashMap<String, Double>();
		HashMap<String, Double> closingStockMap = new HashMap<String, Double>();
		for (MaterialIssueNote min : minList) {
			for (MaterialProduct product : min.getSubProductTablemin()) {
				hset.add(product.getMaterialProductId());
				String key = product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim();
				if (minMap != null
						&& minMap.containsKey(product.getMaterialProductId()
								+ "-"
								+ product.getMaterialProductWarehouse()
										.toUpperCase().trim()
								+ "-"
								+ product.getMaterialProductStorageLocation()
										.toUpperCase().trim()
								+ "-"
								+ product.getMaterialProductStorageBin()
										.toUpperCase().trim())) {
					minMap.put(
							product.getMaterialProductId()
									+ "-"
									+ product.getMaterialProductWarehouse()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getMaterialProductStorageLocation()
											.toUpperCase().trim()
									+ "-"
									+ product.getMaterialProductStorageBin()
											.toUpperCase().trim(),
							minMap.get(product.getMaterialProductId()
									+ "-"
									+ product.getMaterialProductWarehouse()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getMaterialProductStorageLocation()
											.toUpperCase().trim()
									+ "-"
									+ product.getMaterialProductStorageBin()
											.toUpperCase().trim())
									+ product
											.getMaterialProductRequiredQuantity());
				} else {
					minMap.put(product.getMaterialProductId()
							+ "-"
							+ product.getMaterialProductWarehouse()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageLocation()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageBin()
									.toUpperCase().trim(),
							product.getMaterialProductRequiredQuantity());
				}
			}
		}

		List<Integer> prodIdList = new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList=null;
		List<ProductInventoryViewDetails> prodInvViewDetailsList=null;
		if(prodIdList!=null&&prodIdList.size()>0) {
			prodInvViewList= ofy().load()
				.type(ProductInventoryView.class)
				.filter("companyId", companyId)
				.filter("productinfo.prodID IN", prodIdList).list();
			prodInvViewDetailsList = ofy().load()
				.type(ProductInventoryViewDetails.class)
				.filter("companyId", companyId)
				.filter("prodid IN", prodIdList).list();
		}
		if(prodInvViewList!=null) {
		for (ProductInventoryView productView : prodInvViewList) {
			for (ProductInventoryViewDetails pvDetails : productView
					.getDetails()) {
				double openingStock = 0;
				double closingStock = 0;
				String key1 = pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim();
				if (minMap.containsKey(pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim())) {
					openingStock = pvDetails.getAvailableqty();
					openingStockMap.put(pvDetails.getProdid()
							+ "-"
							+ pvDetails.getWarehousename().toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation().toUpperCase()
									.trim() + "-"
							+ pvDetails.getStoragebin().toUpperCase().trim(),
							openingStock);
					closingStock = openingStock
							+ minMap.get(pvDetails.getProdid()
									+ "-"
									+ pvDetails.getWarehousename()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragelocation()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragebin().toUpperCase()
											.trim());
					pvDetails.setAvailableqty(closingStock);

				}
			}
		}
		}
		if(prodInvViewDetailsList!=null) {
		for (ProductInventoryViewDetails pvDetails : prodInvViewDetailsList) {
			double openingStock = 0;
			double closingStock = 0;
			String key = pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim();
			if (minMap.containsKey(pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim())) {
				openingStock = pvDetails.getAvailableqty();
				closingStock = openingStock
						+ minMap.get(pvDetails.getProdid()
								+ "-"
								+ pvDetails.getWarehousename().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragelocation().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragebin().toUpperCase()
										.trim());
				pvDetails.setAvailableqty(closingStock);
			}
		}
		}
		if(prodInvViewList!=null)
		ofy().save().entities(prodInvViewList);
		if(prodInvViewDetailsList!=null)
		ofy().save().entities(prodInvViewDetailsList);
		

		for (MaterialIssueNote min : minList) {
			for (MaterialProduct product : min.getSubProductTablemin()) {
				double stock = 0;
				String key = product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim();
				if (openingStockMap.containsKey(product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim())) {
					stock = openingStockMap.get(product.getMaterialProductId()
							+ "-"
							+ product.getMaterialProductWarehouse()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageLocation()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageBin()
									.toUpperCase().trim());
					product.setMaterialProductAvailableQuantity(stock);
					openingStockMap
							.put(product.getMaterialProductId()
									+ "-"
									+ product.getMaterialProductWarehouse()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getMaterialProductStorageLocation()
											.toUpperCase().trim()
									+ "-"
									+ product.getMaterialProductStorageBin()
											.toUpperCase().trim(),
									openingStockMap.get(product
											.getMaterialProductId()
											+ "-"
											+ product
													.getMaterialProductWarehouse()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getMaterialProductStorageLocation()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getMaterialProductStorageBin()
													.toUpperCase().trim())
											+ product
													.getMaterialProductRequiredQuantity());

				}
				count++;
			}
		}
		return count;
	}

	public Long mmnTransactionDetails(Long companyId,
			List<MaterialMovementNote> mmnList) {

		HashSet<Integer> hset = new HashSet<Integer>();
		HashMap<String, Double> mmnMap = new HashMap<String, Double>();
		HashMap<String, Double> openingStockMap = new HashMap<String, Double>();
		HashMap<String, Double> closingStockMap = new HashMap<String, Double>();
        Long count = 0l;
		for (MaterialMovementNote mmn : mmnList) {
			for (MaterialProduct product : mmn.getSubProductTableMmn()) {
				hset.add(product.getMaterialProductId());
				String key = product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim();
				double value = 0;
				String transaction = mmn.getTransDirection().trim();
				if (mmnMap.containsKey(product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim())) {
					value = mmnMap.get(product.getMaterialProductId()
							+ "-"
							+ product.getMaterialProductWarehouse()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageLocation()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageBin()
									.toUpperCase().trim());
				}

				if (transaction.equals("IN")) {
					value = value
							- product.getMaterialProductRequiredQuantity();
				} else if (transaction.equals("OUT")) {
					value = value
							+ product.getMaterialProductRequiredQuantity();
				} else if (transaction.equals("TRANSFEROUT")) {
					value = value
							+ product.getMaterialProductRequiredQuantity();
				} else if (transaction.equals("TRANSFERIN")) {
					value = value
							- product.getMaterialProductRequiredQuantity();
				}
				if (mmnMap != null
						&& mmnMap.containsKey(product.getMaterialProductId()
								+ "-"
								+ product.getMaterialProductWarehouse()
										.toUpperCase().trim()
								+ "-"
								+ product.getMaterialProductStorageLocation()
										.toUpperCase().trim()
								+ "-"
								+ product.getMaterialProductStorageBin()
										.toUpperCase().trim())) {
					mmnMap.put(product.getMaterialProductId()
							+ "-"
							+ product.getMaterialProductWarehouse()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageLocation()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageBin()
									.toUpperCase().trim(), value);
				} else {
					mmnMap.put(product.getMaterialProductId()
							+ "-"
							+ product.getMaterialProductWarehouse()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageLocation()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageBin()
									.toUpperCase().trim(), value);
				}
			}
		}

		List<Integer> prodIdList = new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList = ofy().load()
				.type(ProductInventoryView.class)
				.filter("companyId", companyId)
				.filter("productinfo.prodID IN", prodIdList).list();
		List<ProductInventoryViewDetails> prodInvViewDetailsList = ofy().load()
				.type(ProductInventoryViewDetails.class)
				.filter("companyId", companyId)
				.filter("prodid IN", prodIdList).list();

		for (ProductInventoryView productView : prodInvViewList) {
			for (ProductInventoryViewDetails pvDetails : productView
					.getDetails()) {
				double openingStock = 0;
				double closingStock = 0;
				String key1 = pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim();
				if (mmnMap.containsKey(pvDetails.getProdid() + "-"
						+ pvDetails.getWarehousename().toUpperCase().trim()
						+ "-"
						+ pvDetails.getStoragelocation().toUpperCase().trim()
						+ "-" + pvDetails.getStoragebin().toUpperCase().trim())) {
					openingStock = pvDetails.getAvailableqty();
					openingStockMap.put(pvDetails.getProdid()
							+ "-"
							+ pvDetails.getWarehousename().toUpperCase().trim()
							+ "-"
							+ pvDetails.getStoragelocation().toUpperCase()
									.trim() + "-"
							+ pvDetails.getStoragebin().toUpperCase().trim(),
							openingStock);
					closingStock = openingStock
							+ mmnMap.get(pvDetails.getProdid()
									+ "-"
									+ pvDetails.getWarehousename()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragelocation()
											.toUpperCase().trim()
									+ "-"
									+ pvDetails.getStoragebin().toUpperCase()
											.trim());
					pvDetails.setAvailableqty(closingStock);
				}
			}
		}

		for (ProductInventoryViewDetails pvDetails : prodInvViewDetailsList) {
			double openingStock = 0;
			double closingStock = 0;
			String key = pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim();
			if (mmnMap.containsKey(pvDetails.getProdid() + "-"
					+ pvDetails.getWarehousename().toUpperCase().trim() + "-"
					+ pvDetails.getStoragelocation().toUpperCase().trim() + "-"
					+ pvDetails.getStoragebin().toUpperCase().trim())) {
				openingStock = pvDetails.getAvailableqty();
				closingStock = openingStock
						+ mmnMap.get(pvDetails.getProdid()
								+ "-"
								+ pvDetails.getWarehousename().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragelocation().toUpperCase()
										.trim()
								+ "-"
								+ pvDetails.getStoragebin().toUpperCase()
										.trim());
				pvDetails.setAvailableqty(closingStock);
			}
		}
		ofy().save().entities(prodInvViewList);
		ofy().save().entities(prodInvViewDetailsList);

		for (MaterialMovementNote mmn : mmnList) {
			for (MaterialProduct product : mmn.getSubProductTableMmn()) {
				double stock = 0;
				String key = product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim();
				if (openingStockMap.containsKey(product.getMaterialProductId()
						+ "-"
						+ product.getMaterialProductWarehouse().toUpperCase()
								.trim()
						+ "-"
						+ product.getMaterialProductStorageLocation()
								.toUpperCase().trim()
						+ "-"
						+ product.getMaterialProductStorageBin().toUpperCase()
								.trim())) {
					stock = openingStockMap.get(product.getMaterialProductId()
							+ "-"
							+ product.getMaterialProductWarehouse()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageLocation()
									.toUpperCase().trim()
							+ "-"
							+ product.getMaterialProductStorageBin()
									.toUpperCase().trim());
					product.setMaterialProductAvailableQuantity(stock);
					double value = 0;
					String transaction = mmn.getTransDirection().trim();
					if (transaction.equals("IN")) {
						value = value
								- product.getMaterialProductRequiredQuantity();
					} else if (transaction.equals("OUT")) {
						value = value
								+ product.getMaterialProductRequiredQuantity();
					} else if (transaction.equals("TRANSFEROUT")) {
						value = value
								+ product.getMaterialProductRequiredQuantity();
					} else if (transaction.equals("TRANSFERIN")) {
						value = value
								- product.getMaterialProductRequiredQuantity();
					}

					openingStockMap
							.put(product.getMaterialProductId()
									+ "-"
									+ product.getMaterialProductWarehouse()
											.toUpperCase().trim()
									+ "-"
									+ product
											.getMaterialProductStorageLocation()
											.toUpperCase().trim()
									+ "-"
									+ product.getMaterialProductStorageBin()
											.toUpperCase().trim(),
									openingStockMap.get(product
											.getMaterialProductId()
											+ "-"
											+ product
													.getMaterialProductWarehouse()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getMaterialProductStorageLocation()
													.toUpperCase().trim()
											+ "-"
											+ product
													.getMaterialProductStorageBin()
													.toUpperCase().trim())
											+ value);
				}
				count++;
			}
		}

		return count;
	}

	/**
	 *  nidhi
	 *  10-11-2017
	 *  method add for commodity fumigation details report.  for nbhc
	 */
	@Override
	public void dailyFumigationReportDetails(Long companyId,Date todayparse) {

		try {
			
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			String  modifiedDate=sdf.format(new Date());
			
			logger.log(Level.SEVERE ," today date --   " +modifiedDate);
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("IST")); 
			 // this takes current date
		    c.set(Calendar.DAY_OF_MONTH, 1);
		    c.set(Calendar.HOUR_OF_DAY, 0);  
	        c.set(Calendar.MINUTE, 0);  
	        c.set(Calendar.SECOND, 0);  
	        c.set(Calendar.MILLISECOND, 0);  
		    System.out.println(c.getTime()); 
		    
		    Date firstDay = c.getTime();
		    
		    Date today = sdf.parse(sdf.format(new Date()));
		    
		    if(todayparse!=null){
		    	today = todayparse;
		    }
		    
		    today = DateUtility.getDateWithTimeZone("IST", today);
			Calendar cal=Calendar.getInstance();
			cal.setTime(today);
			cal.add(Calendar.DATE, 0);
			
			today=sdf.parse(sdf.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			today=cal.getTime();
		    
			logger.log(Level.SEVERE ,"get date -- " + firstDay + " today date -- " + today);
			
		    ArrayList<String> proCode = new ArrayList<String>();
		    proCode.add("STK-01");
		    proCode.add("SLM-01");
		    proCode.add("AFC-10");
		    proCode.add("CON-02");
		    proCode.add("AFC-02");
		    proCode.add("BNF-01");
		    proCode.add("CON-01");
		    
		    List<CommodityFumigationDetails> dailyReportDetail = new ArrayList<CommodityFumigationDetails>();
		    
		    List<Branch> branchList = ofy().load().type(Branch.class).filter("companyId", companyId).list();
		    
		    List<ServiceProduct> serProList = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productCode IN", proCode).list();
		    
		    TreeMap<String,String> proName = new TreeMap<String,String>();
		    
		    for(ServiceProduct serPro : serProList){
		    	proName.put(serPro.getProductName(),serPro.getProductCode());
		    	logger.log(Level.SEVERE,"Pro name --" +serPro.getProductName() + "Pro code --" + serPro.getProductCode());
		    }
		    
			List<Service> serDetailList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=", firstDay)
					.filter("serviceDate <=", today)
					.filter("product.productCode IN", proCode)
					.filter("status", Service.SERVICESTATUSCOMPLETED).list();
			
			
			logger.log(Level.SEVERE ," branch list -- " + branchList + " service list -- " +serDetailList.size());
			
			ArrayList<String> branchName = new ArrayList<String>();
			
			/**
			 * Date 03-09-2018 By Vijay
			 * Des :- NBHC CCPM Date wise Daily Summary Report in detail
			 * above old code commented
			 */
			Comparator<Service> servicelistComparator = new Comparator<Service>() {
				
				@Override
				public int compare(Service s1, Service s2) {
					Date serviceDate = s1.getServiceDate();
					Date serviceDate2 = s2.getServiceDate();
					return serviceDate.compareTo(serviceDate2);
				}
			};
			Collections.sort(serDetailList, servicelistComparator);
			
			HashSet<Date> serviceDatelist = new HashSet<Date>();
			for(Service service : serDetailList){
				serviceDatelist.add(service.getServiceDate());
			}
			
//			for(Branch branch : branchList){
//				for(String proDetail : proName.keySet()){
//					
//					CommodityFumigationDetails details = getServiceProductWise(serDetailList, proDetail, branch.getBusinessUnitName() ,today );
////							logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//							if(details.getProdCode().equals("")){
//								details.setProdCode(proName.get(proDetail));
//							}
//							details.setDate(today);
//							dailyReportDetail.add(details);
//							logger.log(Level.SEVERE," after method pro name -- "+ details.getProName() + " pro code --" + details.getProdCode());
//				}
//				branchName.add(branch.getBusinessUnitName());
//			}
			
			
			for(Branch branch : branchList){
				
				for(Date dateWiseServiceDate : serviceDatelist){
				for(String proDetail : proName.keySet()){
//				CommodityFumigationDetails details = getServiceProductWise(serDetailList, proDetail, branch ,Duration,dateWiseServiceDate,monthWise );
				
					CommodityFumigationDetails commFumiDetails = null;
					try {
						String proCoded = "";
						double dayWise = 0;
						double monthWise = 0;
						for(Service ser : serDetailList){
						if(ser.getServiceDate().equals(dateWiseServiceDate) && proDetail.equals(ser.getProductName())){
							dayWise += ser.getQuantity();
							monthWise = monthWise + ser.getQuantity();
							proCoded = ser.getProduct().getProductCode();
							}
						}
						if(dayWise!=0){
						commFumiDetails = new CommodityFumigationDetails();
						commFumiDetails.setBranchName(branch.getBusinessUnitName());
						commFumiDetails.setProdCode(proCoded);
						commFumiDetails.setDayWiseTotal(dayWise);
						commFumiDetails.setMonthWiseTotal(monthWise);
						commFumiDetails.setProName(proDetail);
						commFumiDetails.setSummaryDate(dateWiseServiceDate);
						commFumiDetails.setDate(today);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("getServiceProductWise method get error -- " + e.getMessage());
						logger.log(Level.SEVERE,"getServiceProductWise method get error -- " + e.getMessage());
					}
					
					if(commFumiDetails!=null){
					dailyReportDetail.add(commFumiDetails);
					}
				}
				}
				branchName.add(branch.getBusinessUnitName());
			}
			
			/**
			 * ends here
			 */

			logger.log(Level.SEVERE," dail report list size -- " + dailyReportDetail.size());
			
			Comparator<String> branchComparator = new Comparator<String>() {
				public int compare(String s1, String s2) {
				
				String branch1 = s1;
				String branch2 = s2;
				
				//ascending order
				return branch1.compareTo(branch2);
				}
				
				};
				Collections.sort(branchName, branchComparator);
				
				Comparator<CommodityFumigationDetails> serviceDateComparator = new Comparator<CommodityFumigationDetails>() {
					public int compare(CommodityFumigationDetails s1, CommodityFumigationDetails s2) {
					
					String branch1 = s1.getBranchName();
					String branch2 = s2.getBranchName();
					
					//ascending order
					return branch1.compareTo(branch2);
					}
					
					};
					Collections.sort(dailyReportDetail, serviceDateComparator);
				
				
				ServiceFumigationDetailReportCronJobImpl.commodityFumigation.clear();
				ServiceFumigationDetailReportCronJobImpl.branchList.clear();
				ServiceFumigationDetailReportCronJobImpl.proName.clear();
				
				ServiceFumigationDetailReportCronJobImpl.commodityFumigation = dailyReportDetail;
				ServiceFumigationDetailReportCronJobImpl.branchList =branchName; 
				ServiceFumigationDetailReportCronJobImpl.proName = proName;
				
				XlsxWriter.commodityFumigation.clear();
				XlsxWriter.branchList.clear();
				XlsxWriter.proName.clear();
				
				XlsxWriter.commodityFumigation.addAll(dailyReportDetail);
				XlsxWriter.branchList.addAll(branchName);
				XlsxWriter.proName = proName;
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" dailyFumigationReportDetails method pro -- "+e.getMessage());
			logger.log(Level.SEVERE, " dailyFumigationReportDetails method details -- " + e.getMessage() );
		}
		
	}
	
	
	CommodityFumigationDetails getServiceProductWise(List<Service> serDetailList,String proName,String brnach,Date todayDate){
		CommodityFumigationDetails commFumiDetails = new CommodityFumigationDetails();
		ArrayList<Service> serList = new ArrayList<Service>();
		try {
			logger.log(Level.SEVERE," proName - " + proName + "  serDetailList --  " + serDetailList.size()  + " branch -  " + brnach );
			String proCode = "";
			double monthWise = 0 , dayWise = 0;
			for(Service ser : serDetailList){
				logger.log(Level.SEVERE,"ser id- " + ser.getCount() +" ser pro name - " + ser.getProductName()+" proName - " + proName +" ser bramch --" + ser.getBranch() + " branch -  " + brnach + " daywise - " + DateUtility.getDateWithTimeZone("IST", todayDate) +" service date -- "+DateUtility.getDateWithTimeZone("IST", ser.getServiceDate()) + " \n mothwise  - " +monthWise + " qty -- "+ser.getQuantity());
				if(ser.getProduct().getProductName().trim().equalsIgnoreCase(proName.trim()) && ser.getBranch().trim().equalsIgnoreCase(brnach.trim())){
						if(DateUtility.getDateWithTimeZone("IST", ser.getServiceDate()).equals(DateUtility.getDateWithTimeZone("IST", todayDate))){
							dayWise = dayWise + ser.getQuantity();
						}
						monthWise = monthWise + ser.getQuantity();
						proCode = ser.getProduct().getProductCode();
				}
			}
			if(dayWise > 0){
				logger.log(Level.SEVERE," after loop proName - " + proName + "  procode --  " + proCode  + " branch -  " + brnach + " daywise - " + dayWise + " mothwise  - " +monthWise);
			}
			
				commFumiDetails.setBranchName(brnach);
				commFumiDetails.setProdCode(proCode);
				commFumiDetails.setDayWiseTotal(dayWise);
				commFumiDetails.setMonthWiseTotal(monthWise);
				commFumiDetails.setProName(proName);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("getServiceProductWise method get error -- " + e.getMessage());
			logger.log(Level.SEVERE,"getServiceProductWise method get error -- " + e.getMessage());
		}
		
		return commFumiDetails;
	}

	@Override
	public void dailyFumigationValueReportDetails(Long companyId, Date todayparse) {
		
		try {
				
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				String  modifiedDate=sdf.format(new Date());
				
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				logger.log(Level.SEVERE ," today date --   " +modifiedDate);
				Calendar c = Calendar.getInstance(TimeZone.getTimeZone("IST")); 
				 // this takes current date
			    c.set(Calendar.DAY_OF_MONTH, 1);
			    c.set(Calendar.HOUR_OF_DAY, 0);  
		        c.set(Calendar.MINUTE, 0);  
		        c.set(Calendar.SECOND, 0);  
		        c.set(Calendar.MILLISECOND, 0);  
			    System.out.println(c.getTime()); 
			    
			    Date firstDay = c.getTime();
			    
			    Date today = sdf.parse(sdf.format(new Date()));
			    
			    if(todayparse!=null){
			    	today = todayparse;
			    }
			    today = DateUtility.getDateWithTimeZone("IST", today);
				Calendar cal=Calendar.getInstance();
				cal.setTime(today);
				cal.add(Calendar.DATE, 0);
				
				today=sdf.parse(sdf.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,999);
				today=cal.getTime();
			    
						    
				ArrayList<CommodityFumigationDetails> dailyReportDetail = new ArrayList<CommodityFumigationDetails>();
			    
			    ArrayList<CommodityFumigationDetails> dailyCancledReportDetail = new ArrayList<CommodityFumigationDetails>();
			    
			    ArrayList<CommodityFumigationDetails> scheduleServiceReportDetail = new ArrayList<CommodityFumigationDetails>();
			    
			    List<Branch> branchList = ofy().load().type(Branch.class).filter("companyId", companyId).list();
			    
			    ArrayList<String> status = new ArrayList<String>();
			    
			    status.add(Service.SERVICESTATUSCANCELLED);
			    status.add(Service.SERVICESTATUSCOMPLETED);
			    status.add(Service.SERVICESTATUSSCHEDULE);
			    status.add(Service.SERVICESTATUSRESCHEDULE);
				List<Service> serDetailList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("serviceDate >=", firstDay)
						.filter("serviceDate <=", today)
						.filter("status IN", status).list();
				
				List<Service> completeServiceList = new ArrayList<Service>();
				List<Service> cancledServiceList = new ArrayList<Service>();
				List<Service> scheduleServiceList = new ArrayList<Service>();
				
				ArrayList<Integer> scheduleRateServiceId = new ArrayList<Integer>();
				ArrayList<Integer> completeRateServiceId = new ArrayList<Integer>();
				ArrayList<Integer> cancledRateServiceId = new ArrayList<Integer>();
				
				for(Service ser : serDetailList){
					if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED) && !ser.isRateContractService()){
						completeServiceList.add(ser);
					}else if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED) && ser.isRateContractService()){
						completeRateServiceId.add(ser.getCount());
					}else if(ser.getStatus().equals(Service.SERVICESTATUSCANCELLED) && !ser.isRateContractService()){
						cancledServiceList.add(ser);
					}else if(ser.getStatus().equals(Service.SERVICESTATUSCANCELLED) && ser.isRateContractService()){
						cancledRateServiceId.add(ser.getCount());
					}else if((ser.getStatus().equals(Service.SERVICESTATUSSCHEDULE)|| ser.getStatus().equals(Service.SERVICESTATUSRESCHEDULE) ) && ser.isRateContractService()){
						scheduleRateServiceId.add(ser.getCount());
					}else if((ser.getStatus().equals(Service.SERVICESTATUSSCHEDULE)|| ser.getStatus().equals(Service.SERVICESTATUSRESCHEDULE) ) && !ser.isRateContractService()){
						scheduleServiceList.add(ser);
					}
				}
				
				ArrayList<Integer> billingId = new ArrayList<Integer>();
				if(completeRateServiceId.size()>0){
					billingId.addAll(completeRateServiceId);
				}
				if(cancledRateServiceId.size()>0){
					billingId.addAll(cancledRateServiceId);
				}
				if(scheduleRateServiceId.size()>0){
					billingId.addAll(scheduleRateServiceId);
				}
				List<BillingDocument> billingList = new ArrayList<BillingDocument>();
				if(billingId.size()>0){
					billingList =  ofy().load().type(BillingDocument.class).filter("companyId", companyId)
							.filter("rateContractServiceId IN", billingId).list();
					
				}
				
				List<BillingDocument> completeServiceBillingList = new ArrayList<BillingDocument>();
				List<BillingDocument> cancledServiceBillingList = new ArrayList<BillingDocument>();
				List<BillingDocument> scheduleServiceBillingList = new ArrayList<BillingDocument>();
				if(billingList.size()>0){
					
					for(BillingDocument bill : billingList){
						if(completeRateServiceId.contains(bill.getRateContractServiceId())){
							completeServiceBillingList.add(bill);
							continue;
						}else if(cancledRateServiceId.contains(bill.getRateContractServiceId())){
							cancledServiceBillingList.add(bill);
							continue;
						}else if(scheduleRateServiceId.contains(bill.getRateContractServiceId())){
							scheduleServiceBillingList.add(bill);
							continue;
						}
					}
					
				}
				
				
				logger.log(Level.SEVERE ," branch list -- " + branchList + " service list -- " +serDetailList.size());
				
				ArrayList<String> branchName = new ArrayList<String>();
				
				
				
				for(Branch branch : branchList){
						
//						CommodityFumigationDetails details = getServiceValueReport(completeServiceList, branch.getBusinessUnitName() ,today ,completeServiceBillingList);
//								logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//								details.setDate(today);
//								dailyReportDetail.add(details);
//							branchName.add(branch.getBusinessUnitName());
//					
//					CommodityFumigationDetails cancledetails = getServiceValueReport(cancledServiceList, branch.getBusinessUnitName() ,today ,cancledServiceBillingList);
//							logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//							cancledetails.setDate(today);
//							dailyCancledReportDetail.add(cancledetails);
//							
//					CommodityFumigationDetails scheduledetails = getServiceValueReport(scheduleServiceList, branch.getBusinessUnitName() ,today ,scheduleServiceBillingList);
//							logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//							cancledetails.setDate(today);
//							scheduleServiceReportDetail.add(scheduledetails);
					
					/**
					 * Date 02-09-2018 By Vijay
					 * Des :- NBHC CCPM Service Revenue Report Updated. above old code commented
					 * report must be in detail on date wise summary
					 */
					Comparator<Service> servicelistComparator = new Comparator<Service>() {
						
						@Override
						public int compare(Service s1, Service s2) {
							Date serviceDate = s1.getServiceDate();
							Date serviceDate2 = s2.getServiceDate();
							return serviceDate.compareTo(serviceDate2);
						}
					};
					Collections.sort(serDetailList, servicelistComparator);
					System.out.println("Service list "+serDetailList.size());
					HashSet<Date> serviceDatelist = new HashSet<Date>();
					for(Service service : serDetailList){
						String strDate = fmt.format(service.getServiceDate());
						serviceDatelist.add(fmt.parse(strDate));
					}
					
					if(completeServiceBillingList.size()!=0){
						for(BillingDocument billDoc : completeServiceBillingList){
							String strDate = fmt.format(billDoc.getBillingDate());
							serviceDatelist.add(fmt.parse(strDate));
						}
					}
					if(scheduleServiceBillingList.size()!=0){
						for(BillingDocument billDoc : scheduleServiceBillingList){
							String strDate = fmt.format(billDoc.getBillingDate());
							serviceDatelist.add(fmt.parse(strDate));
						}
					}
					if(cancledServiceList.size()!=0){
						for(BillingDocument billDoc : cancledServiceBillingList){
							String strDate = fmt.format(billDoc.getBillingDate());
							serviceDatelist.add(fmt.parse(strDate));
						}
					}
					
					for(Date serviceDate : serviceDatelist){

						ArrayList<CommodityFumigationDetails> details = getServiceValueReport(completeServiceList, branch.getBusinessUnitName() ,today ,completeServiceBillingList,"Completed",serviceDate);
						if(details!=null)
						dailyReportDetail.addAll(details);
					
								
						ArrayList<CommodityFumigationDetails> scheduledetails = getServiceValueReport(scheduleServiceList, branch.getBusinessUnitName() ,today ,scheduleServiceBillingList,"Scheduled",serviceDate);
						if(scheduledetails!=null)
						dailyReportDetail.addAll(scheduledetails);
						
						ArrayList<CommodityFumigationDetails> cancledetails = getServiceValueReport(cancledServiceList, branch.getBusinessUnitName() ,today ,cancledServiceBillingList,"Cancelled",serviceDate);
						if(cancledetails!=null)
						dailyReportDetail.addAll(cancledetails);
					}
					
					branchName.add(branch.getBusinessUnitName());
					/**
					 * ends here
					 */
				}
				
				logger.log(Level.SEVERE," dail report list size -- " + dailyReportDetail.size() +"  -->  "+dailyCancledReportDetail.size());
				
				Comparator<String> branchComparator = new Comparator<String>() {
					public int compare(String s1, String s2) {
					
					String branch1 = s1;
					String branch2 = s2;
					
					//ascending order
					return branch1.compareTo(branch2);
					}
					
					};
					Collections.sort(branchName, branchComparator);
					
					Comparator<CommodityFumigationDetails> serviceDateComparator = new Comparator<CommodityFumigationDetails>() {
						public int compare(CommodityFumigationDetails s1, CommodityFumigationDetails s2) {
						
						String branch1 = s1.getBranchName();
						String branch2 = s2.getBranchName();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(dailyReportDetail, serviceDateComparator);
					
					
					ServiceFumigationValueDetailReportCronJobImpl.commodityServiceValue.clear();
					ServiceFumigationValueDetailReportCronJobImpl.serviceBranchList.clear();
					ServiceFumigationValueDetailReportCronJobImpl.commodityCancledServiceValue.clear();
					ServiceFumigationValueDetailReportCronJobImpl.commodityscheduleServiceValue.clear();
					
					ServiceFumigationValueDetailReportCronJobImpl.commodityCancledServiceValue =  dailyCancledReportDetail;
					ServiceFumigationValueDetailReportCronJobImpl.commodityServiceValue = dailyReportDetail;
					ServiceFumigationValueDetailReportCronJobImpl.serviceBranchList =branchName; 
					ServiceFumigationValueDetailReportCronJobImpl.commodityscheduleServiceValue = scheduleServiceReportDetail;
					
					XlsxWriter.commodityServiceValue.clear();
					XlsxWriter.serviceBranchList.clear();
					XlsxWriter.commodityCancledServiceValue.clear();
					XlsxWriter.commodityscheduleServiceValue.clear();
					
					XlsxWriter.commodityCancledServiceValue.addAll(dailyCancledReportDetail);
					XlsxWriter.commodityServiceValue.addAll(dailyReportDetail);
					XlsxWriter.serviceBranchList.addAll(branchName);
					XlsxWriter.commodityscheduleServiceValue.addAll(scheduleServiceReportDetail);
				
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE,"Get error in dail fumigation value Report -- "+e.getMessage());
		}
		
	}
	
//	CommodityFumigationDetails getServiceValueReport(List<Service> serDetailList,String brnach,Date todayDate,List<BillingDocument> completeServiceBillingList){
//		CommodityFumigationDetails commFumiDetails = new CommodityFumigationDetails();
//		ArrayList<Service> serList = new ArrayList<Service>();
//		try {
//			String proCode = "";
//			double monthWise = 0 , dayWise = 0;
//			for(Service ser : serDetailList){
//				
//				if(ser.getBranch().trim().equalsIgnoreCase(brnach.trim())){
//						if(DateUtility.getDateWithTimeZone("IST", ser.getServiceDate()).equals(DateUtility.getDateWithTimeZone("IST", todayDate))){
//							dayWise = dayWise + ser.getServiceValue();
//						}
//						monthWise = monthWise + ser.getServiceValue();
//						proCode = ser.getProduct().getProductCode();
//				}
//			}
//			
//			
//			for(BillingDocument bill : completeServiceBillingList){
//				
//				if(bill.getBranch().trim().equalsIgnoreCase(brnach.trim()) ){
//						if(DateUtility.getDateWithTimeZone("IST", bill.getBillingDate()).equals(DateUtility.getDateWithTimeZone("IST", todayDate))){
//							dayWise = dayWise + bill.getTotalBillingAmount();
//						}
//						monthWise = monthWise + bill.getTotalBillingAmount();
//				}
//			}
//			logger.log(Level.SEVERE,"   procode --  " + proCode  + " branch -  " + brnach + " daywise - " + dayWise + " mothwise  - " +monthWise);
//				commFumiDetails.setBranchName(brnach);
//				commFumiDetails.setProdCode(proCode);
//				commFumiDetails.setDayWiseTotal(dayWise);
//				commFumiDetails.setMonthWiseTotal(monthWise);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println("getServiceValueReport method get error -- " + e.getMessage());
//			logger.log(Level.SEVERE,"getServiceValueReport method get error -- " + e.getMessage());
//		}
//		
//		return commFumiDetails;
//	}
	
	/***
	 * nidhi
	 *  method for calculate service revenue 
	 *  for from to to date duration 
	 *  same method for cron job also
	 */
@Override	
public void dailyFumigationValueReportDetails(Long companyId, Date todayparse,Date duration, List<String> branchList) {
		
		try {
				
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				String  modifiedDate=sdf.format(new Date());
				
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				logger.log(Level.SEVERE ," today date --   " +modifiedDate);
				Calendar c = Calendar.getInstance(TimeZone.getTimeZone("IST")); 
				 // this takes current date
//			    c.set(Calendar.DAY_OF_MONTH, 1);
			    c.set(Calendar.HOUR_OF_DAY, 0);  
		        c.set(Calendar.MINUTE, 0);  
		        c.set(Calendar.SECOND, 0);  
		        c.set(Calendar.MILLISECOND, 0);  

			    System.out.println(c.getTime()); 
			    
			    Date firstDay = c.getTime();
			    
			    Date today = null;
			    
			    if(todayparse!=null){
			        c.setTime(todayparse);
			    	today = todayparse;
			    }else{
			    	  c.setTime(duration);
			    	c.set(Calendar.DAY_OF_MONTH, 1);
			    	today = c.getTime();
			    }
//			    today = DateUtility.getDateWithTimeZone("IST", today);
				Calendar cal=Calendar.getInstance();
				cal.setTime(today);
//				cal.add(Calendar.DATE, today./);
				
						    
				 Date toEndday = sdf.parse(sdf.format(new Date()));
				    
				    if(duration!=null){
				    	toEndday = duration;
				    }
				    toEndday = DateUtility.getDateWithTimeZone("IST", toEndday);
//					Calendar cal=Calendar.getInstance();
					cal.setTime(toEndday);
//					cal.add(Calendar.DATE, today./);
					
				Date durtoday=sdf.parse(sdf.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
					cal.set(Calendar.MILLISECOND,99);
					durtoday=cal.getTime();
				ArrayList<CommodityFumigationDetails> dailyReportDetail = new ArrayList<CommodityFumigationDetails>();
			    
			    ArrayList<CommodityFumigationDetails> dailyCancledReportDetail = new ArrayList<CommodityFumigationDetails>();
			    
			    ArrayList<CommodityFumigationDetails> scheduleServiceReportDetail = new ArrayList<CommodityFumigationDetails>();
			    
//			    List<Branch> branchList = ofy().load().type(Branch.class).filter("companyId", companyId).list();
			    
			    ArrayList<String> status = new ArrayList<String>();
			    
			    status.add(Service.SERVICESTATUSCANCELLED);
			    status.add(Service.SERVICESTATUSCOMPLETED);
			    status.add(Service.SERVICESTATUSSCHEDULE);
			    status.add(Service.SERVICESTATUSRESCHEDULE);
			    logger.log(Level.SEVERE,"get first date -" + today +" last date --" + durtoday);
			    List<Service> serDetailList = new ArrayList<Service>();
			     

			    {
			    	 serDetailList = ofy().load().type(Service.class).filter("companyId", companyId)
							.filter("serviceDate >=", today)
							.filter("serviceDate <=", durtoday).filter("branch IN", branchList)
							.filter("status IN", status).list();
			    }
				
				logger.log(Level.SEVERE," ser list size --- " +serDetailList.size());
				List<Service> completeServiceList = new ArrayList<Service>();
				List<Service> cancledServiceList = new ArrayList<Service>();
				List<Service> scheduleServiceList = new ArrayList<Service>();
				List<Service> contractServiceList = new ArrayList<Service>();
				
				ArrayList<Integer> scheduleRateServiceId = new ArrayList<Integer>();
				ArrayList<Integer> completeRateServiceId = new ArrayList<Integer>();
				ArrayList<Integer> cancledRateServiceId = new ArrayList<Integer>();
				ArrayList<Integer> contractSerList = new ArrayList<Integer>();
				
				HashSet<Integer> contractSerSet = new HashSet<Integer>();
				HashMap<Integer, Integer> serContractSet = new HashMap<Integer,Integer>();
				
				int seCount = 1;
				for(Service ser : serDetailList){
					logger.log(Level.SEVERE,"ser no -- " + seCount + " ser cpint-- " + ser.getCount() + " ser value== " + ser.getServiceValue() +"ser contract count --" + ser.getContractCount() + " rate contract --" +ser.getStatus());
					if(ser.getStatus().equals(Service.SERVICESTATUSCANCELLED) || ser.getServiceValue() == 0.0){
						contractSerSet.add(ser.getContractCount());
						serContractSet.put(ser.getCount(), ser.getContractCount());
						contractServiceList.add(ser);
						continue;
					}
					else  if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED) && !ser.isRateContractService()  ){
						completeServiceList.add(ser);
						continue;
					}else if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED) && ser.isRateContractService()){
						completeRateServiceId.add(ser.getCount());
						continue;
					}else if(ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED) && ser.isServiceWiseBilling()){
						completeRateServiceId.add(ser.getCount());
						continue;
					}else if(ser.getStatus().equals(Service.SERVICESTATUSCANCELLED) && (ser.isRateContractService() || ser.isServiceWiseBilling())){
						contractSerSet.add(ser.getContractCount());
						serContractSet.put(ser.getCount(), ser.getContractCount());
						continue;
//						cancledRateServiceId.add(ser.getCount());
					}else if((ser.getStatus().equals(Service.SERVICESTATUSSCHEDULE)|| ser.getStatus().equals(Service.SERVICESTATUSRESCHEDULE) ) && ser.isRateContractService()){
						scheduleRateServiceId.add(ser.getCount());
						continue;
					}else if((ser.getStatus().equals(Service.SERVICESTATUSSCHEDULE)|| ser.getStatus().equals(Service.SERVICESTATUSRESCHEDULE) ) && !ser.isRateContractService()){
						scheduleServiceList.add(ser);
						continue;
					}
				}
				logger.log(Level.SEVERE,"schedule serv list 0- " + scheduleServiceList.size());
				List<Contract> contList = new ArrayList<Contract>();
				logger.log(Level.SEVERE,"get contract count ser -- "+ contractSerSet.size() + " contract -- "+ contractSerSet.toString());
				if(contractSerSet.size()>0){
					contractSerList.addAll(contractSerSet);
					
					contList =  ofy().load().type(Contract.class).filter("companyId", companyId)
							.filter("count IN", contractSerList).filter("status", Contract.APPROVED).list();
				
				}
				logger.log(Level.SEVERE,"contract count --" +contList.size());
				for(Contract contDt : contList){
					for(Service ser : contractServiceList){
						logger.log(Level.SEVERE, " service count --  " + ser.getCount() + " ser status == " + ser.getStatus());
						if(ser.getContractCount() == contDt.getCount()){
							if(contDt.isContractRate()){
								if(ser.getStatus().equalsIgnoreCase(Service.CANCELLED)){
									cancledRateServiceId.add(ser.getCount());
								}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE)){
									scheduleRateServiceId.add(ser.getCount());
								}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
									completeRateServiceId.add(ser.getCount());
								}
							}else if(contDt.isServiceWiseBilling()){
								if(ser.getStatus().equalsIgnoreCase(Service.CANCELLED)){
									cancledRateServiceId.add(ser.getCount());
								}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE)){
									scheduleRateServiceId.add(ser.getCount());
								}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
									completeRateServiceId.add(ser.getCount());
								}
							}else if(contDt.isBranchWiseBilling()){

								for(SalesLineItem salesItem : contDt.getItems()){
									if(salesItem.getProductCode().equalsIgnoreCase(ser.getProduct().getProductCode())){
//										item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())
										double qty = 1;
										if(salesItem.getQty()>0){
											qty = salesItem.getQty();
										}
										Double value = ((salesItem.getTotalAmount()/salesItem.getNumberOfServices())*qty);
										ser.setServiceValue(value);
										logger.log(Level.SEVERE," contract value -- " + value + " service count --  " + ser.getCount() + " ser status == " + ser.getStatus());	
										if(ser.getStatus().equalsIgnoreCase(Service.CANCELLED)){
//											cancledRateServiceId.add(ser.getCount());
											cancledServiceList.add(ser);
										}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) || ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE)){
											scheduleServiceList.add(ser);
										}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
											completeServiceList.add(ser);
										}
										break;
									}
								}
							}else{
								for(SalesLineItem salesItem : contDt.getItems()){
									if(salesItem.getProductCode().equalsIgnoreCase(ser.getProduct().getProductCode())){
//										item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())
										double qty = 1,unit = 1, totalAmt = 0;
										if(salesItem.getQty()>0){
											qty = salesItem.getQty();
										}
										if(salesItem.getArea()!= null && salesItem.getArea().trim().matches("[0-9.]*")){
											unit = Double.parseDouble(salesItem.getArea().trim());
										}
										totalAmt = salesItem.getTotalAmount();
										if(salesItem.getTotalAmount() == 0 ){
											totalAmt = salesItem.getPrice() * unit;
										}
										Double value = ((totalAmt/(salesItem.getNumberOfServices() * qty )));
										ser.setServiceValue(value);
											logger.log(Level.SEVERE," get serv for 0 contract" + ser.getCount() + " value --"  +ser.getServiceValue());
										if(ser.getStatus().equalsIgnoreCase(Service.CANCELLED)){
//											cancledRateServiceId.add(ser.getCount());
											cancledServiceList.add(ser);
										}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE)){
											scheduleServiceList.add(ser);
										}else if(ser.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
											completeServiceList.add(ser);
										}
										break;
									}
								}
							}
							
						}
					}
				}
				
				

				ArrayList<Integer> billingId = new ArrayList<Integer>();
				if(completeRateServiceId.size()>0){
					billingId.addAll(completeRateServiceId);
				}
				if(cancledRateServiceId.size()>0){
					billingId.addAll(cancledRateServiceId);
				}
				if(scheduleRateServiceId.size()>0){
					billingId.addAll(scheduleRateServiceId);
				}
				List<BillingDocument> billingList = new ArrayList<BillingDocument>();
				logger.log(Level.SEVERE," Billling ser coung- -" + billingId.size());
				if(billingId.size()>0){
					billingList =  ofy().load().type(BillingDocument.class).filter("companyId", companyId)
							.filter("rateContractServiceId IN", billingId).list();
					
				}
				logger.log(Level.SEVERE," billing list - " + billingList);
				List<BillingDocument> completeServiceBillingList = new ArrayList<BillingDocument>();
				List<BillingDocument> cancledServiceBillingList = new ArrayList<BillingDocument>();
				List<BillingDocument> scheduleServiceBillingList = new ArrayList<BillingDocument>();
				if(billingList.size()>0){
					
					for(BillingDocument bill : billingList){
						if(completeRateServiceId.contains(bill.getRateContractServiceId())){
							completeServiceBillingList.add(bill);
							continue;
						}else if(cancledRateServiceId.contains(bill.getRateContractServiceId())){
							cancledServiceBillingList.add(bill);
							continue;
						}else if(scheduleRateServiceId.contains(bill.getRateContractServiceId())){
							scheduleServiceBillingList.add(bill);
							continue;
						}
					}
					
				}
				
				
				logger.log(Level.SEVERE ," branch list -- " + branchList + " service list -- " +serDetailList.size());
				
				ArrayList<String> branchName = new ArrayList<String>();
				
				
				for(String branch : branchList){
						
//						CommodityFumigationDetails details = getServiceValueReport(completeServiceList, branch ,durtoday ,completeServiceBillingList);
////								logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//								details.setDate(durtoday);
//								dailyReportDetail.add(details);
//							branchName.add(branch);
//					
//					CommodityFumigationDetails cancledetails = getServiceValueReport(cancledServiceList, branch ,durtoday ,cancledServiceBillingList);
////							logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//							cancledetails.setDate(durtoday);
//							dailyCancledReportDetail.add(cancledetails);
//							
//					CommodityFumigationDetails scheduledetails = getServiceValueReport(scheduleServiceList, branch ,durtoday ,scheduleServiceBillingList);
////							logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//							scheduledetails.setDate(durtoday);
//							scheduleServiceReportDetail.add(scheduledetails);
					
					
					/**
					 * Date 02-09-2018 By Vijay
					 * Des :- NBHC CCPM Service Revenue Report Updated. above old code commented
					 * report must be in detail on date wise summary
					 */
					Comparator<Service> servicelistComparator = new Comparator<Service>() {
						
						@Override
						public int compare(Service s1, Service s2) {
							Date serviceDate = s1.getServiceDate();
							Date serviceDate2 = s2.getServiceDate();
							return serviceDate.compareTo(serviceDate2);
						}
					};
					Collections.sort(serDetailList, servicelistComparator);
					
					HashSet<Date> serviceDatelist = new HashSet<Date>();
					for(Service service : serDetailList){
						serviceDatelist.add(service.getServiceDate());
					}
					System.out.println("serviceDatelist =="+serviceDatelist.size());
					
					
					if(completeServiceBillingList.size()!=0){
						for(BillingDocument billDoc : completeServiceBillingList){
							serviceDatelist.add(billDoc.getBillingDate());
						}
					}
					if(scheduleServiceBillingList.size()!=0){
						for(BillingDocument billDoc : scheduleServiceBillingList){
							serviceDatelist.add(billDoc.getBillingDate());
						}
					}
					if(cancledServiceList.size()!=0){
						for(BillingDocument billDoc : cancledServiceBillingList){
							serviceDatelist.add(billDoc.getBillingDate());
						}
					}
					
					for(Date serviceDate : serviceDatelist){

						ArrayList<CommodityFumigationDetails> details = getServiceValueReport(completeServiceList, branch ,durtoday ,completeServiceBillingList,"Completed",serviceDate);
						if(details!=null)
						dailyReportDetail.addAll(details);
					
								
						ArrayList<CommodityFumigationDetails> scheduledetails = getServiceValueReport(scheduleServiceList, branch ,durtoday ,scheduleServiceBillingList,"Scheduled",serviceDate);
						if(scheduledetails!=null)
						dailyReportDetail.addAll(scheduledetails);
						
						ArrayList<CommodityFumigationDetails> cancledetails = getServiceValueReport(cancledServiceList, branch ,durtoday ,cancledServiceBillingList,"Cancelled",serviceDate);
						if(cancledetails!=null)
						dailyReportDetail.addAll(cancledetails);
					}
					
					branchName.add(branch);
					/**
					 * ends here
					 */
				}
				
				logger.log(Level.SEVERE," dail report list size -- " + dailyReportDetail.size() +"  -->  "+dailyCancledReportDetail.size() + " schec"  +scheduleServiceReportDetail.size());
				
				Comparator<String> branchComparator = new Comparator<String>() {
					public int compare(String s1, String s2) {
					
					String branch1 = s1;
					String branch2 = s2;
					
					//ascending order
					return branch1.compareTo(branch2);
					}
					
					};
					Collections.sort(branchName, branchComparator);
					
					Comparator<CommodityFumigationDetails> serviceDateComparator = new Comparator<CommodityFumigationDetails>() {
						public int compare(CommodityFumigationDetails s1, CommodityFumigationDetails s2) {
						
						String branch1 = s1.getBranchName();
						String branch2 = s2.getBranchName();
						
						//ascending order
						return branch1.compareTo(branch2);
						}
						
						};
						Collections.sort(dailyReportDetail, serviceDateComparator);
					
					
					ServiceFumigationValueDetailReportCronJobImpl.commodityServiceValue.clear();
					ServiceFumigationValueDetailReportCronJobImpl.serviceBranchList.clear();
					ServiceFumigationValueDetailReportCronJobImpl.commodityCancledServiceValue.clear();
					ServiceFumigationValueDetailReportCronJobImpl.commodityscheduleServiceValue.clear();
					
					ServiceFumigationValueDetailReportCronJobImpl.commodityCancledServiceValue =  dailyCancledReportDetail;
					ServiceFumigationValueDetailReportCronJobImpl.commodityServiceValue = dailyReportDetail;
					ServiceFumigationValueDetailReportCronJobImpl.serviceBranchList =branchName; 
					ServiceFumigationValueDetailReportCronJobImpl.commodityscheduleServiceValue = scheduleServiceReportDetail;
					
					XlsxWriter.commodityServiceValue.clear();
					XlsxWriter.serviceBranchList.clear();
					XlsxWriter.commodityCancledServiceValue.clear();
					XlsxWriter.commodityscheduleServiceValue.clear();
					
					XlsxWriter.commodityCancledServiceValue.addAll(dailyCancledReportDetail);
					XlsxWriter.commodityServiceValue.addAll(dailyReportDetail);
					XlsxWriter.serviceBranchList.addAll(branchName);
					XlsxWriter.commodityscheduleServiceValue.addAll(scheduleServiceReportDetail);
				
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE,"Get error in dail fumigation value Report -- "+e.getMessage());
		}
		
	}

/**
 * Date 02-03-2018 By Vijay
 * Des :- below code old code only having the summary 
 * I have created new method for Date wise summary Report in detail
 */
//CommodityFumigationDetails getServiceValueReport(List<Service> serDetailList,String brnach,Date todayDate,List<BillingDocument> completeServiceBillingList){
//	CommodityFumigationDetails commFumiDetails = new CommodityFumigationDetails();
//	ArrayList<Service> serList = new ArrayList<Service>();
//	try {
//		String proCode = "";
//		double monthWise = 0 , dayWise = 0;
//		for(Service ser : serDetailList){
//			
//			if(ser.getBranch().trim().equalsIgnoreCase(brnach.trim())){
//					if(DateUtility.getDateWithTimeZone("IST", ser.getServiceDate()).equals(DateUtility.getDateWithTimeZone("IST", todayDate))){
//						dayWise = dayWise + ser.getServiceValue();
//					}
//					monthWise = monthWise + ser.getServiceValue();
//					proCode = ser.getProduct().getProductCode();
//			}
//		}
//		
//		
//		for(BillingDocument bill : completeServiceBillingList){
//			
//			if(bill.getBranch().trim().equalsIgnoreCase(brnach.trim()) ){
//					if(DateUtility.getDateWithTimeZone("IST", bill.getBillingDate()).equals(DateUtility.getDateWithTimeZone("IST", todayDate))){
//						dayWise = dayWise + bill.getTotalBillingAmount();
//					}
//					monthWise = monthWise + bill.getTotalBillingAmount();
//			}
//		}
//		logger.log(Level.SEVERE,"   procode --  " + proCode  + " branch -  " + brnach + " daywise - " + dayWise + " mothwise  - " +monthWise);
//			commFumiDetails.setBranchName(brnach);
//			commFumiDetails.setProdCode(proCode);
//			commFumiDetails.setDayWiseTotal(dayWise);
//			commFumiDetails.setMonthWiseTotal(monthWise);
//		
//	} catch (Exception e) {
//		e.printStackTrace();
//		System.out.println("getServiceValueReport method get error -- " + e.getMessage());
//		logger.log(Level.SEVERE,"getServiceValueReport method get error -- " + e.getMessage());
//	}
//	
//	return commFumiDetails;
//}
/**
 * ends here
 */

/***
 * nidhi
 *  method for calculate fumigation service 
 *  for from to to date duration 
 *  same method for cron job also
 */

@Override
public void dailyFumigationReportDetails(Long companyId,Date todayparse,Date Duration,List<String> branchList) {

	try {
		
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String  modifiedDate=sdf.format(new Date());
		
		logger.log(Level.SEVERE ," today date --   " +modifiedDate);
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("IST")); 
		 // this takes current date
		
	    c.set(Calendar.HOUR_OF_DAY, 0);  
        c.set(Calendar.MINUTE, 0);  
        c.set(Calendar.SECOND, 0);  
        c.set(Calendar.MILLISECOND, 0);  
        
	    System.out.println(c.getTime()); 
	    
	    Date firstDay = c.getTime();
	    
	    Date today = sdf.parse(sdf.format(new Date()));
	    
	    if(todayparse!=null){
	    	c.setTime(todayparse);
	    	todayparse = c.getTime();
	    }else{
	    	c.setTime(Duration);
	    	c.set(Calendar.DAY_OF_MONTH, 1);
	    	
	    	todayparse = c.getTime();
	    }
	    
	    today = DateUtility.getDateWithTimeZone("IST", today);
		Calendar cal=Calendar.getInstance();
		
		today=sdf.parse(sdf.format(cal.getTime()));
		cal.set(Calendar.HOUR_OF_DAY,23);
		cal.set(Calendar.MINUTE,59);
		cal.set(Calendar.SECOND,59);
		cal.set(Calendar.MILLISECOND,999);
		cal.setTime(Duration);
		Duration=cal.getTime();
	    
		logger.log(Level.SEVERE ,"get date -- " + Duration + " today date -- " + todayparse);
		/**
		 * Date 12-10-2018 By Vijay
		 * Des :- as per vaishali ma Fumigation report no need of AFC-10,AFC-02,BNF-01
		 */
	    ArrayList<String> proCode = new ArrayList<String>();
	    proCode.add("STK-01");
	    proCode.add("SLM-01");
//	    proCode.add("AFC-10");
	    proCode.add("CON-02");
//	    proCode.add("AFC-02");
//	    proCode.add("BNF-01");
	    proCode.add("CON-01");
	    
	    List<CommodityFumigationDetails> dailyReportDetail = new ArrayList<CommodityFumigationDetails>();
	    
//	    List<Branch> branchList = ofy().load().type(Branch.class).filter("companyId", companyId).list();
	    
	    List<ServiceProduct> serProList = ofy().load().type(ServiceProduct.class).filter("companyId", companyId)
	    		.filter("productCode IN", proCode).list();
	    
	    TreeMap<String,String> proName = new TreeMap<String,String>();
	    
	    for(ServiceProduct serPro : serProList){
	    	proName.put(serPro.getProductName(),serPro.getProductCode());
	    	logger.log(Level.SEVERE,"Pro name --" +serPro.getProductName() + "Pro code --" + serPro.getProductCode());
	    }
	    
	    List<Service> serDetailList = new ArrayList<Service>();
//	    if(todayparse!=null)
	    {
	    	 serDetailList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=", todayparse)
					.filter("serviceDate <=", Duration).filter("branch IN", branchList)
					.filter("product.productCode IN", proCode)
					.filter("status", Service.SERVICESTATUSCOMPLETED).list();
	    }
//	    else{
//	    	 serDetailList = ofy().load().type(Service.class).filter("companyId", companyId)
//						.filter("serviceDate <=", Duration).filter("branch IN", branchList)
//						.filter("product.productCode IN", proCode)
//						.filter("status", Service.SERVICESTATUSCOMPLETED).list();
//	    }
	    
		
		
		
		logger.log(Level.SEVERE ," branch list -- " + branchList + " service list -- " +serDetailList.size());
		
		ArrayList<String> branchName = new ArrayList<String>();
		
//		for(String branch : branchList){
//			for(String proDetail : proName.keySet()){
//				
//				CommodityFumigationDetails details = getServiceProductWise(serDetailList, proDetail, branch ,Duration );
////						logger.log(Level.SEVERE," pro name -- "+ details.getProName());
//						if(details.getProdCode().equals("")){
//							details.setProdCode(proName.get(proDetail));
//						}
//						details.setDate(Duration);
//						dailyReportDetail.add(details);
//						logger.log(Level.SEVERE," after method pro name -- "+ details.getProName() + " pro code --" + details.getProdCode());
//			}
//			branchName.add(branch);
//		}
		
		/**
		 * Date 03-09-2018 By Vijay
		 * Des :- NBHC CCPM Date wise Daily Summary Report in detail
		 * above old code commented
		 */
		Comparator<Service> servicelistComparator = new Comparator<Service>() {
			
			@Override
			public int compare(Service s1, Service s2) {
				Date serviceDate = s1.getServiceDate();
				Date serviceDate2 = s2.getServiceDate();
				return serviceDate.compareTo(serviceDate2);
			}
		};
		Collections.sort(serDetailList, servicelistComparator);
		
		HashSet<Date> serviceDatelist = new HashSet<Date>();
		for(Service service : serDetailList){
			serviceDatelist.add(service.getServiceDate());
		}
		
		
		for(String branch : branchList){
			
			for(Date dateWiseServiceDate : serviceDatelist){
			for(String proDetail : proName.keySet()){
//			CommodityFumigationDetails details = getServiceProductWise(serDetailList, proDetail, branch ,Duration,dateWiseServiceDate,monthWise );
			
				CommodityFumigationDetails commFumiDetails = null;
				try {
					String proCoded = "";
					double dayWise = 0;
					double monthWise = 0;
					for(Service ser : serDetailList){
					if(ser.getServiceDate().equals(dateWiseServiceDate) && proDetail.equals(ser.getProductName())){
						dayWise += ser.getQuantity();
						monthWise = monthWise + ser.getQuantity();
						proCoded = ser.getProduct().getProductCode();
						}
					}
					if(dayWise!=0){
					commFumiDetails = new CommodityFumigationDetails();
					commFumiDetails.setBranchName(branch);
					commFumiDetails.setProdCode(proCoded);
					commFumiDetails.setDayWiseTotal(dayWise);
					commFumiDetails.setMonthWiseTotal(monthWise);
					commFumiDetails.setProName(proDetail);
					commFumiDetails.setSummaryDate(dateWiseServiceDate);
					commFumiDetails.setDate(Duration);
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("getServiceProductWise method get error -- " + e.getMessage());
					logger.log(Level.SEVERE,"getServiceProductWise method get error -- " + e.getMessage());
				}
				
				if(commFumiDetails!=null){
				dailyReportDetail.add(commFumiDetails);
				}
			}
			}
			branchName.add(branch);
		}
		logger.log(Level.SEVERE," dail report list size -- " + dailyReportDetail.size());
		
		/**
		 * ends here
		 */
		
		logger.log(Level.SEVERE," dail report list size -- " + dailyReportDetail.size());
		
		Comparator<String> branchComparator = new Comparator<String>() {
			public int compare(String s1, String s2) {
			
			String branch1 = s1;
			String branch2 = s2;
			
			//ascending order
			return branch1.compareTo(branch2);
			}
			
			};
			Collections.sort(branchName, branchComparator);
			
			Comparator<CommodityFumigationDetails> serviceDateComparator = new Comparator<CommodityFumigationDetails>() {
				public int compare(CommodityFumigationDetails s1, CommodityFumigationDetails s2) {
				
				String branch1 = s1.getBranchName();
				String branch2 = s2.getBranchName();
				
				//ascending order
				return branch1.compareTo(branch2);
				}
				
				};
				Collections.sort(dailyReportDetail, serviceDateComparator);
				
				/**
				 * Date 03-09-2018 By Vijay
				 * Des :- NBHC CCPM Date wise Daily Summary Report
				 */
			Comparator<CommodityFumigationDetails> serDateComparator = new  Comparator<CommodityFumigationDetails>() {
				
				@Override
				public int compare(CommodityFumigationDetails s1,
						CommodityFumigationDetails s2) {
					
					Date serviceDate = s1.getSummaryDate();
					Date serviceDate2 = s2.getSummaryDate();
					
					return serviceDate.compareTo(serviceDate2);
				}
			};
			Collections.sort(dailyReportDetail,serDateComparator);
			/**
			 * ends here
			 */
			
			ServiceFumigationDetailReportCronJobImpl.commodityFumigation.clear();
			ServiceFumigationDetailReportCronJobImpl.branchList.clear();
			ServiceFumigationDetailReportCronJobImpl.proName.clear();
			
			ServiceFumigationDetailReportCronJobImpl.commodityFumigation = dailyReportDetail;
			ServiceFumigationDetailReportCronJobImpl.branchList =branchName; 
			ServiceFumigationDetailReportCronJobImpl.proName = proName;
			
			XlsxWriter.commodityFumigation.clear();
			XlsxWriter.branchList.clear();
			XlsxWriter.proName.clear();
			
			XlsxWriter.commodityFumigation.addAll(dailyReportDetail);
			XlsxWriter.branchList.addAll(branchName);
			XlsxWriter.proName = proName;
		
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(" dailyFumigationReportDetails method pro -- "+e.getMessage());
		logger.log(Level.SEVERE, " dailyFumigationReportDetails method details -- " + e.getMessage() );
	}
	
}

/**
 * Date 02-09-2018 By Vijay
 * Des :- NBHC CCPM Service Revenue Report on daily basis summary Report
 * @param serviceDate 
 * @param status 
 */
ArrayList<CommodityFumigationDetails>  getServiceValueReport(List<Service> serDetailList,String brnach,Date todayDate,List<BillingDocument> completeServiceBillingList, String serviceStatus, Date serviceDate){
	
	ArrayList<CommodityFumigationDetails> DailyWiseSummaryReport = null;
	
	try {
		String proCode = "";
		
		double monthWise = 0 , dayWise = 0, lastDaySmum = 0; 
		String strDate = fmt.format(serviceDate);
		Date serDate = fmt.parse(strDate);
		for(Service ser : serDetailList){
			
			if(ser.getBranch().trim().equalsIgnoreCase(brnach.trim())){
				if(serviceDate.equals(ser.getServiceDate())){
					dayWise += ser.getServiceValue();
					monthWise += ser.getServiceValue();
				}
				
			}
		}
		if(dayWise!=0){
		 DailyWiseSummaryReport =  new ArrayList<CommodityFumigationDetails>();
		CommodityFumigationDetails commFumiDetails = new CommodityFumigationDetails();
		commFumiDetails.setBranchName(brnach);
		commFumiDetails.setProdCode(proCode);
		commFumiDetails.setDayWiseTotal(dayWise);
		commFumiDetails.setMonthWiseTotal(monthWise);
		commFumiDetails.setDate(todayDate);
		commFumiDetails.setSummaryDate(serDate);
		commFumiDetails.setStatus(serviceStatus);
		DailyWiseSummaryReport.add(commFumiDetails);
		}
		
		System.out.println("completeServiceBillingList ==="+completeServiceBillingList.size());
		if(completeServiceBillingList.size()!=0){
			for(BillingDocument bill : completeServiceBillingList){
				if(serviceDate.equals(bill.getBillingDate())){
				if(bill.getBranch().trim().equalsIgnoreCase(brnach.trim()) ){
					
					dayWise += bill.getTotalBillingAmount();
					monthWise += bill.getTotalBillingAmount();
				}
				}
			}

			if(dayWise!=0){
				DailyWiseSummaryReport =  new ArrayList<CommodityFumigationDetails>();
				CommodityFumigationDetails commodityFumiDetails  = new CommodityFumigationDetails();
				commodityFumiDetails.setBranchName(brnach);
				commodityFumiDetails.setProdCode(proCode);
				commodityFumiDetails.setDayWiseTotal(dayWise);
				commodityFumiDetails.setMonthWiseTotal(monthWise);
				commodityFumiDetails.setDate(todayDate);
				commodityFumiDetails.setSummaryDate(serDate);
				commodityFumiDetails.setStatus(serviceStatus);

				DailyWiseSummaryReport.add(commodityFumiDetails);
			}
			
		}
		
		

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("getServiceValueReport method get error -- " + e.getMessage());
		logger.log(Level.SEVERE,"getServiceValueReport method get error -- " + e.getMessage());
	}
	
	return DailyWiseSummaryReport;
	}
/**
 * ends here
 */

@Override
public Integer serviceInvoiceIdMapping(Long companyId, Date todayparse,
		Date duration, List<String> branchList) {


	try {
	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	String  modifiedDate=sdf.format(new Date());
	
	logger.log(Level.SEVERE ," today date --   " +modifiedDate);
	Calendar c = Calendar.getInstance(TimeZone.getTimeZone("IST")); 
	 // this takes current date
	
    c.set(Calendar.HOUR_OF_DAY, 0);  
    c.set(Calendar.MINUTE, 0);  
    c.set(Calendar.SECOND, 0);  
    c.set(Calendar.MILLISECOND, 0);  
    
    System.out.println(c.getTime()); 
    
    Date firstDay = c.getTime();
    
    Date today = sdf.parse(sdf.format(new Date()));
    
    if(todayparse!=null){
    	c.setTime(todayparse);
    	todayparse = c.getTime();
    }else{
    	c.setTime(duration);
    	c.set(Calendar.DAY_OF_MONTH, 1);
    	
    	todayparse = c.getTime();
    }
    
    today = DateUtility.getDateWithTimeZone("IST", today);
	Calendar cal=Calendar.getInstance();
	
	
		today=sdf.parse(sdf.format(cal.getTime()));

	cal.set(Calendar.HOUR_OF_DAY,23);
	cal.set(Calendar.MINUTE,59);
	cal.set(Calendar.SECOND,59);
	cal.set(Calendar.MILLISECOND,999);
	cal.setTime(duration);
	duration=cal.getTime();
    
	logger.log(Level.SEVERE ,"get date -- " + duration + " today date -- " + todayparse);
    
    
    List<Service> serDetailList = new ArrayList<Service>();
//    if(todayparse!=null)
    {
    	 serDetailList = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("serviceDate >=", todayparse).filter("serviceDate <=", duration)
				.filter("branch IN", branchList).filter("status", Service.SERVICESTATUSCOMPLETED).list();
    }
    
    
    if(serDetailList.size()>0){
    	
    	List<ServiceInvoiceDetails> serDetail = new ArrayList<ServiceInvoiceDetails>();
    	
    	
    	HashMap<Integer,ServiceInvoiceDetails> serList = new HashMap<Integer,ServiceInvoiceDetails>();
    	
    	HashSet<Integer> billList = new HashSet<Integer>();
    	HashSet<Integer> invList = new HashSet<Integer>();
    	HashSet<Integer> serRateList = new HashSet<Integer>();
    	
    	for(Service ser : serDetailList){
    		ServiceInvoiceDetails serDt = new ServiceInvoiceDetails();
    		serDt.setServiceID(ser.getCount());
    		serDt.setSerDate(ser.getServiceDate());
    		serDt.setBillId(ser.getBillingCount());
    		serDt.setInvId(ser.getInvoiceId());
    		serDt.setCustId(ser.getCustomerId());
    		serDt.setCustName(ser.getCustomerName());
    		serDt.setContStartDate(ser.getContractStartDate());
    		serDt.setContractId(ser.getContractCount());
    		serDt.setInvDate(ser.getInvoiceDate());
    		serDt.setServiceStatus(ser.getStatus());
    		if(ser.isRateContractService() || ser.isServiceWiseBilling()){
    			serRateList.add(ser.getCount());
    		}else{
    			if(serDt.getBillId()!=0){
        			billList.add(serDt.getBillId());
        		}
        		
        		if(serDt.getInvId()!=0){
        			invList.add(ser.getInvoiceId());
        		}
    		}
    			
        		
    		System.out.println("get ser list -- " + ser.getCount());
        		serList.put(ser.getCount(),serDt );
    	}

    	System.out.println(" inv ser list  -- "  + invList.toString());
    	if(billList.size()>0){
    		
    		ArrayList<Integer> billLt =  new ArrayList<Integer>();
    		billLt.addAll(billList);
    		
    		List<BillingDocument> billDtList = ofy().load().type(BillingDocument.class).filter("companyId", companyId)
    				.filter("count IN", billLt).list();
    		
    		if(billDtList.size()>0){
    			for(BillingDocument billDt : billDtList){
    				if(billDt.getRateContractServiceId()!=0){
    					serList.get(billDt.getRateContractServiceId()).setBillDate(billDt.getBillingDate());
    					serList.get(billDt.getRateContractServiceId()).setBillStatus(billDt.getStatus());
    					if(billDt.getInvoiceCount()!=0){
    						invList.add(billDt.getInvoiceCount());
    						serList.get(billDt.getRateContractServiceId()).setInvId(billDt.getInvoiceCount());
    					}
    				}else{
    					if(billDt.getServiceId()!=null){
    					for(Integer ser : billDt.getServiceId()){
    						if(serList.containsKey(ser)){
    							serList.get(ser).setBillDate(billDt.getBillingDate());
    							serList.get(ser).setBillStatus(billDt.getStatus());
    							if(billDt.getInvoiceCount()!=0){
    								serList.get(ser).setInvId(billDt.getInvoiceCount());
    							}
    						}
    						
    					}
    					}
    				}
    				
    				if(billDt.getInvoiceCount()!=0){
						invList.add(billDt.getInvoiceCount());
					}
    			}
    		}
    		
    	}
    	
    	
    	if(serRateList.size()>0){
    		
    		ArrayList<Integer> billLt =  new ArrayList<Integer>();
    		billLt.addAll(serRateList);
    		
    		List<BillingDocument> billDtList = ofy().load().type(BillingDocument.class).filter("companyId", companyId)
    				.filter("rateContractServiceId IN", billLt).list();
    		
    		if(billDtList.size()>0){
    			for(BillingDocument billDt : billDtList){
    				if(billDt.getRateContractServiceId()!=0){
    					serList.get(billDt.getRateContractServiceId()).setBillDate(billDt.getBillingDate());
    					serList.get(billDt.getRateContractServiceId()).setBillStatus(billDt.getStatus());
    					
    					if(billDt.getInvoiceCount()!=0){
    						serList.get(billDt.getRateContractServiceId()).setInvId(billDt.getInvoiceCount());
    					}
    				}else{
    					if(billDt.getServiceId()!=null)
    					for(Integer ser : billDt.getServiceId()){
    						if(serList.containsKey(ser)){
    							serList.get(ser).setBillDate(billDt.getBillingDate());
    							serList.get(ser).setBillStatus(billDt.getStatus());
    							if(billDt.getInvoiceCount()!=0){
    								serList.get(ser).setInvId(billDt.getInvoiceCount());
    							}
    						}
    						
    					}
    				}
    				if(billDt.getInvoiceCount()!=0){
						invList.add(billDt.getInvoiceCount());
					}
    			}
    		}
    		
    	}
    	
    	if(invList.size()>0){
    		
    		ArrayList<Integer> billLt =  new ArrayList<Integer>();
    		billLt.addAll(invList);
    		
    		List<Invoice> billDtList = ofy().load().type(Invoice.class).filter("companyId", companyId)
    				.filter("count IN", billLt).list();
    		
	    		if(billDtList.size()>0){
	    			for(Invoice billDt : billDtList){
	    				if(billDt.getRateContractServiceId()!=0){
	    					serList.get(billDt.getRateContractServiceId()).setInvDate(billDt.getInvoiceDate());
	    					serList.get(billDt.getRateContractServiceId()).setInvStatus(billDt.getStatus());
	    					serList.get(billDt.getRateContractServiceId()).setSapInvId(billDt.getSapInvoiceId());
	    				}else{
	    					if(billDt.getServiceId()!=null)
	    					for(Integer ser : billDt.getServiceId()){
	    						if(serList.containsKey(ser)){
	    							serList.get(ser).setInvDate(billDt.getInvoiceDate());
			    					serList.get(ser).setInvStatus(billDt.getStatus());
			    					serList.get(ser).setSapInvId(billDt.getSapInvoiceId());
	    						}
	    						
	    					}
	    				}
	    			}
	    		}
    		
    	}
    	
    	CsvWriter.serInvoiceDt.clear();
    	CsvWriter.serInvoiceDt = serList;
    }
	
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return 1;
}
  /**
   * Date 13-04-2019 by Vijay for NBHC CCPM Partial GRN Cancellation
   */

@Override
public String partialGRNCancellation(SuperModel model) {
	 
	if (model instanceof GRN) {

		GRN grn = (GRN) model;

		ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
		for (GRNDetails product : grn.getInventoryProductItem()) {
			System.out.println("product.getStatus()=="+product.getStatus());
			if(product.getStatus()!=null && !product.getStatus().equals("") && product.getStatus().equals(GRN.CANCELLED)){
			InventoryTransactionLineItem item = new InventoryTransactionLineItem();
			item.setCompanyId(grn.getCompanyId());
			item.setBranch(grn.getBranch());
			item.setDocumentType(AppConstants.GRN);
			item.setDocumentId(grn.getCount());
			item.setDocumentDate(grn.getCreationDate());
			item.setDocumnetTitle("" + " (Cancelled GRN)");
			item.setProdId(product.getProductID());
			item.setUpdateQty(product.getProductQuantity());
			item.setOperation(AppConstants.SUBTRACT);
			item.setWarehouseName(product.getWarehouseLocation());
			item.setStorageLocation(product.getStorageLoc());
			item.setStorageBin(product.getStorageBin());
			/**
			 * nidhi
			 * 23-08-2018
			 * for map serial number
			 */
				HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				
			if(product.getProSerialNoDetails()!=null && product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
				prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
				for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
					 ProductSerialNoMapping element = new ProductSerialNoMapping();
					 element.setAvailableStatus(pr.isAvailableStatus());
					 element.setNewAddNo(pr.isNewAddNo());
					 element.setStatus(pr.isStatus());
					 element.setProSerialNo(pr.getProSerialNo());
					 prserdt.get(0).add(element);
				}
				for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
			      {
					ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
					if(!pr.isStatus()){
							itr.remove();
					}
			      }
			}
			item.setProSerialNoDetails(prserdt);
			itemList.add(item);
		 }
		}
		UpdateStock.setProductInventory(itemList);
		ofy().save().entity(grn);
		
		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", grn.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGGRN)
					.filter("companyId", grn.getCompanyId())
					.filter("configStatus", true).first().now();
		}
		int flag = 0;

		if (processConfig != null) {
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {

			if (grn.getCount() != 0) {

		logger.log(Level.SEVERE,"grn.getInventoryProductItem().size()"+grn.getInventoryProductItem().size());
			for (int i = 0; i < grn.getInventoryProductItem().size(); i++) {
				if(grn.getInventoryProductItem().get(i).getStatus()!=null && !grn.getInventoryProductItem().get(i).getStatus().equals("") && grn.getInventoryProductItem().get(i).getStatus().equals(GRN.CANCELLED)){
					
				String unitofmeasurement = grn.getInventoryProductItem()
						.get(i).getUnitOfmeasurement();
				double productprice = grn.getInventoryProductItem().get(i)
						.getProdPrice();
				int prodId = grn.getInventoryProductItem().get(i)
						.getProductID();
				String productCode = grn.getInventoryProductItem().get(i)
						.getProductCode();
				String productName = grn.getInventoryProductItem().get(i)
						.getProductName();
				double productQuantity = grn.getInventoryProductItem()
						.get(i).getProductQuantity();
				double totalAmount = grn.getInventoryProductItem().get(i)
						.getProdPrice()
						* grn.getProductDetails().get(i)
								.getProductQuantity();
				String prodRefId = "";
				ProductDetailsPO po = grn.getInventoryProductItem().get(i);
				if (grn.getInventoryProductItem().get(i).getProductRefId()
						.trim() != null) {
					prodRefId = grn.getInventoryProductItem().get(i)
							.getProductRefId().trim();
				}
				String vName = grn.getVendorInfo().getFullName().trim();
				long vCell = grn.getVendorInfo().getCellNumber();
				int vId = grn.getVendorInfo().getCount();
				String vendorRefId = "";
				if (grn.getVendorReferenceId() != null
						&& grn.getVendorReferenceId().trim().length() > 0) {
					vendorRefId = grn.getVendorReferenceId();
				} else {
					vendorRefId = 0 + "";
				}
				String warehouseName = grn.getInventoryProductItem().get(i)
						.getWarehouseLocation().trim();
				logger.log(Level.SEVERE,"warehouseName" + warehouseName);
				WareHouse warehouse = ofy().load().type(WareHouse.class)
						.filter("companyId", grn.getCompanyId())
						.filter("buisnessUnitName", warehouseName).first()
						.now();
				logger.log(Level.SEVERE,"warehouse" + warehouse);
				String warehouseCode = "";
				if (warehouse.getWarehouseCode().trim() != null) {
					warehouseCode = warehouse.getWarehouseCode().trim();
				}
				/**
				 * Added by rahul Verma on 19 June 2017 Only for NBHC This
				 * will create accounting interface for cancellation
				 */

				UpdateAccountingInterface.updateTally(
						new Date(),// accountingInterfaceCreationDate
						grn.getEmployee(),// accountingInterfaceCreatedBy
						GRN.CANCELLED,
						AppConstants.STATUS,// Status
						grn.getRefNo2(),// remark /**** Date 15-04-2019 by Vijay for NBHC CCPM SAP bill no Mapping ***/
						"Inventory",// module
						"GRN",// documentType
						grn.getCount(),// docID
						grn.getTitle(),// DOCtile
						grn.getCreationDate(), // docDate
						AppConstants.DOCUMENTGL,// docGL
						grn.getPoNumber(),// ref doc no.1
						grn.getPoCreationDate(),// ref doc date.1
						AppConstants.REFDOCPO,// ref doc type 1
						grn.getRefNo(),// ref doc no 2
						grn.getGrnReferenceDate(),// ref doc date 2
						AppConstants.REFDOCPO,// ref doc type 2
						AppConstants.ACCOUNTTYPEAP,// accountType
						0,// custID
						"",// custName
						0,// custCell
						Integer.parseInt(vendorRefId.trim()),// vendor ID
						vName,// vendor NAme
						vCell,// vendorCell
						0,// empId
						" ",// empNAme
						grn.getBranch(),// branch
						grn.getEmployee(),// personResponsible
						"",// requestedBY
						grn.getApproverName(),// approvedBY
						"",// paymentmethod
						null,// paymentdate
						"",// cheqno.
						null,// cheqDate
						null,// bankName
						null,// bankAccount
						0,// transferReferenceNumber
						null,// transactionDate
						null,// contract start date
						null, // contract end date
						prodId,// prod ID
						productCode,// prod Code
						productName,// productNAme
						productQuantity,// prodQuantity
						null,// productDate
						0,// duration
						0,// services
						unitofmeasurement,// unit of measurement
						productprice,// productPrice
						0,// VATpercent
						0,// VATamt
						0,// VATglAccount
						0,// serviceTaxPercent
						0,// serviceTaxAmount
						0,// serviceTaxGLaccount
						"",// cform
						0,// cformpercent
						0,// cformamount
						0,// cformGlaccount
						0,// totalamouNT
						0,// NET PAPAYABLE
						"",// Contract Category
						0,// amount Recieved
						0.0,// base Amt for tdscal in pay by rohan
						0, // tds percentage by rohan
						0,// tds amount by rohan
						"", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0,
						"", 0, "", 0, "", 0, "", 0, "", 0, "", "", "", "",
						"", "", 0, grn.getCompanyId(), null, // billing from
																// date
																// (rohan)
						null, // billing to date (rohan)
						warehouseName, // Warehouse
						warehouseCode, // warehouseCode
						prodRefId, // ProductRefId
						AccountingInterface.OUTBOUNDDIRECTION, // Direction
						AppConstants.CCPM, // sourceSystem
						AppConstants.NBHC , // Destination System
						null,null,null
						);
			 }
			}

			}
		/**
		 * ENDS for Rahul Verma
		 */
		return "GRN Cancelled Successfully.";
	}
	}
	return null;
	}
 public void cancelAllServices(ArrayList<Service> list , String remark , String loggedInUser){
	
		for(Service ser:list){
			ser.setReasonForChange("Service ID ="+ser.getCount()+" "+"Service status ="+ser.getStatus().trim()+"\n"
				+"has been cancelled by "+loggedInUser+" with remark "+"\n"
				+"Remark ="+remark+" "+"Cancellation Date ="+fmt.format(new Date()));
			ser.setStatus(Service.SERVICESTATUSCANCELLED);
			ser.setRecordSelect(false);
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCancellationRemark", ser.getCompanyId())){
//				ser.setRemark(remark);
//			}
			if(remark!=null){
				ser.setRemark(remark);
			}
			
			ser.setCancellationDate(new Date());
			ofy().save().entity(ser);
		}
 }

//Ashwini Patil Date:14-06-2024 for ultra pest- salesperson should get updated in bills, invoice and payment
@Override
public String updateSalesPerson(Long companyId, int contractCount, String salesPerson) {
	logger.log(Level.SEVERE,"updateSalesPerson called");
	String result="";
	List<BillingDocument>	billinglist=ofy().load().type(BillingDocument.class).filter("companyId", companyId)
			.filter("contractCount", contractCount).list();
	
	List<Invoice>	invoicelist=ofy().load().type(Invoice.class).filter("companyId", companyId)
			.filter("contractCount", contractCount).list();
	
	List<CustomerPayment> paymentList =ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
			.filter("contractCount", contractCount).list();

	if(billinglist!=null&&billinglist.size()>0) {
		
		for(BillingDocument obj:billinglist) {
			obj.setEmployee(salesPerson);
		}
		ofy().save().entities(billinglist);		
		logger.log(Level.SEVERE,billinglist.size()+" bills updated");
	}
	if(invoicelist!=null&&invoicelist.size()>0) {
		for(Invoice obj:invoicelist) {
			obj.setEmployee(salesPerson);
		}
		ofy().save().entities(invoicelist);
		logger.log(Level.SEVERE,invoicelist.size()+" invoices updated");
	}
	if(paymentList!=null&&paymentList.size()>0) {
		for(CustomerPayment obj:paymentList) {
			obj.setEmployee(salesPerson);
		}
		ofy().save().entities(paymentList);
		logger.log(Level.SEVERE,paymentList.size()+" payments updated");
	}
	return "Sales Person Updated in all documents such as Bills, Invoices and Payments";
}

@Override
public String createExtraServicesForCustomerBranches(ArrayList<BranchWiseScheduling> custBranchList, Contract con,
		String loggedinuser) {
		String msg="";
		
		if(con!=null) {

		List<Service> serviceList = ofy().load().type(Service.class)
				.filter("companyId", con.getCompanyId())
				.filter("contractCount", con.getCount()).list();
		
		
		ArrayList<Service> servicesToBeDeleted=new ArrayList<Service>();
		
		if(serviceList==null||serviceList.size()==0)
			return "No services found for this contract.";
		
		logger.log(Level.SEVERE,"serviceList size="+serviceList.size());
		List<CustomerBranchDetails> customerBranchList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId()).filter("cinfo.count", con.getCinfo().getCount()).list();

		HashMap<String, CustomerBranchDetails> customerBranchMap =new HashMap<String, CustomerBranchDetails>();
		if(customerBranchList==null) {
				return "Not able to search customer branches of this customer";
		}else {			
			for(CustomerBranchDetails branch:customerBranchList) {
				customerBranchMap.put(branch.getBusinessUnitName(), branch);
			}
			logger.log(Level.SEVERE,"customerBranchList size="+customerBranchList.size()+" customerBranchMap size="+customerBranchMap.size());
		}
		//items.productSrNo
		//serviceSrNo
		
		ArrayList<String> branchnames=new ArrayList<String>();
		for(SalesLineItem item:con.getItems()) {
			ArrayList<BranchWiseScheduling> branchlist =  item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
			ArrayList<Service> itemwiseservicelist=new ArrayList<Service>();
			boolean isServiceAddressSelected=false;
			List<CustomerBranchDetails> itemWiseCustomerBranchList=new ArrayList<CustomerBranchDetails>();
			for(Service s:serviceList) {
				if(item.getProductSrNo()==s.getServiceSrNo())
					itemwiseservicelist.add(s);
			}
			
			for(BranchWiseScheduling binfo: branchlist) {
				if(binfo.isCheck()) {
					if(!binfo.getBranchName().equals("Service Address")&&customerBranchMap.get(binfo.getBranchName())!=null)
						itemWiseCustomerBranchList.add(customerBranchMap.get(binfo.getBranchName()));
					if(binfo.getBranchName().equals("Service Address"))
						isServiceAddressSelected=true;
				}					
			}
			logger.log(Level.SEVERE,"Product name="+item.getProductName()+" productsrno="+item.getProductSrNo()+" itemwiseservicelist size="+itemwiseservicelist.size()+" itemWiseCustomerBranchList size="+itemWiseCustomerBranchList.size());
			
			if(itemwiseservicelist.size()>0) {

				int count=0;
				NumberGeneration numbergen = null;
				int totalservices=itemWiseCustomerBranchList.size()*itemwiseservicelist.size();
				
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
				
					if (con.getNumberRange() != null && !con.getNumberRange().equals("")) {
						String selectedNumberRange = con.getNumberRange();
						String prefixselectedNumberRange = "S_"+ selectedNumberRange;
						count=(int) numGen.getLastUpdatedNumber(prefixselectedNumberRange, con.getCompanyId(), (long) totalservices);

					} else {
						count=(int) numGen.getLastUpdatedNumber("Service", con.getCompanyId(), (long) totalservices);

					}
				
				logger.log(Level.SEVERE,"count="+count);
							
				
				ArrayList<Service> newServicesList=new ArrayList<Service>();
//				logger.log(Level.SEVERE,"serviceList size="+serviceList.size());
				
				for(Service s:itemwiseservicelist) {
					double serviceValue=0;
					if(isServiceAddressSelected)
						serviceValue=s.getServiceValue()/(itemWiseCustomerBranchList.size()+1);
					else
						serviceValue=s.getServiceValue()/(itemWiseCustomerBranchList.size());
					
					for(CustomerBranchDetails branch:itemWiseCustomerBranchList) {					
						Service newService=Myclone(s,count);
						newService.setCount(count);
						newService.setServiceValue(serviceValue);
						newService.setServiceBranch(branch.getBusinessUnitName());
						newService.setBranch(branch.getBranch());
						Address address=new Address();
						if(branch.getAddress().getAddrLine1()!=null)
							address.setAddrLine1(branch.getAddress().getAddrLine1());
						
						if(branch.getAddress().getAddrLine2()!=null)
							address.setAddrLine2(branch.getAddress().getAddrLine2());
						
						if(branch.getAddress().getLocality()!=null)
							address.setLocality(branch.getAddress().getLocality());
						
						if(branch.getAddress().getCity()!=null)
							address.setCity(branch.getAddress().getCity());
						
						if(branch.getAddress().getState()!=null)
							address.setState(branch.getAddress().getState());
						
						if(branch.getAddress().getCountry()!=null)
							address.setCountry(branch.getAddress().getCountry());
						
						if(branch.getAddress().getPin()>0)
							address.setPin(branch.getAddress().getPin());
						if(branch.getCostCenter()!=null&&!branch.getCostCenter().equals(""))
							newService.setCostCenter(branch.getCostCenter());
						if(branch.getArea()>0)
							newService.setQuantity(branch.getArea());
						if(branch.getUnitOfMeasurement()!=null&&!branch.getUnitOfMeasurement().equals(""))
							newService.setUom(branch.getUnitOfMeasurement());
						
						
						
						newService.setAddress(address);
						newServicesList.add(newService);
						count++;
					}
					if(isServiceAddressSelected) {
						s.setServiceValue(serviceValue);
						newServicesList.add(s);
					}else {
						servicesToBeDeleted.add(s);
					}
						
				}
				logger.log(Level.SEVERE,"newServicesList size="+newServicesList.size());
				logger.log(Level.SEVERE,"servicesToBeDeleted size="+servicesToBeDeleted.size());
				ofy().save().entities(newServicesList);
				msg+="Created "+(itemWiseCustomerBranchList.size()*itemwiseservicelist.size())+" services for product "+item.getProductName()+". \n";

			}// end of if(itemwiseservicelist.size()>0) 
			
		} //end of con.getItems() for loop
		if(servicesToBeDeleted.size()>0) {
			ofy().delete().entities(servicesToBeDeleted);
			logger.log(Level.SEVERE,"Services for service address deleted");
		}
			
	}
		if(con.getUpdationLog()!=null)
			con.setUpdationLog(con.getUpdationLog()+" User "+loggedinuser+ " executed add customer branches program on "+new Date()+".");
		else
			con.setUpdationLog(" User "+loggedinuser+ " executed add customer branches program on "+new Date()+".");
		
	ofy().save().entity(con);
	return msg;
}


private Service Myclone(Service s,int count) {
	Service temp=new Service();
//	temp.id=id;
	temp.setCount(count);
	temp.setCompanyId(s.getCompanyId());
//	temp.setDeleted(isDeleted());
	temp.setStatus(s.getStatus());
	temp.setEmployee(s.getEmployee());
	temp.setBranch(s.getBranch());
//	temp.setCreationTime(new Date().getTime()+"");
//	temp.approvalTime=approvalTime;
	temp.setCreationDate(new Date());
	temp.setGroup(s.getGroup());
	temp.setCategory(s.getCategory());
	temp.setType(s.getType());
	temp.setServiceDate(s.getServiceDate());
	temp.setReasonForChange(s.getReasonForChange());
	temp.setAddress(s.getAddress());
	temp.setProduct((ServiceProduct)s.getProduct());
	temp.setContractCount(s.getContractCount());
	temp.setPersonInfo(s.getPersonInfo());
	temp.setComment(s.getComment());
	temp.setArray(s.getArray());
	temp.setUptestReport(s.getUptestReport());
	temp.setUpDeviceDocument(s.getUpDeviceDocument());
	temp.setListHistory(s.getListHistory());
	
	temp.setServiceCompletionDate(s.getServiceCompletionDate());
	
	temp.setServiceDay(s.getServiceDay());
	temp.setServiceDateDay(s.getServiceDateDay());
	temp.setServiceTime(s.getServiceTime());
	temp.setServiceBranch(s.getServiceBranch());
	temp.setServiceCompleteDuration(s.getServiceCompleteDuration());
	temp.setServiceCompleteRemark(s.getServiceCompleteRemark());
	
	if(s.getCatchtrapList()!=null&&s.getCatchtrapList().size()>0)
		temp.setCatchtrapList(s.getCatchtrapList());
	if(s.getPestTreatmentList()!=null&&s.getPestTreatmentList().size()>0)
		temp.setPestTreatmentList(s.getPestTreatmentList());
	if(s.getCheckList()!=null&&s.getCheckList().size()>0)
		temp.setCheckList(s.getCheckList());
	temp.setServiceImage1(s.getServiceImage1());
	temp.setServiceImage2(s.getServiceImage2());
	temp.setServiceImage3(s.getServiceImage3());
	temp.setServiceImage4(s.getServiceImage4());
	temp.setServiceImage5(s.getServiceImage5());
	temp.setServiceImage6(s.getServiceImage6());
	temp.setServiceImage7(s.getServiceImage7());
	temp.setServiceImage8(s.getServiceImage8());
	temp.setServiceImage9(s.getServiceImage9());
	temp.setServiceImage10(s.getServiceImage10());
	
	temp.setTicketNumber(s.getTicketNumber());
	
	temp.setCustomerFeedback(s.getCustomerFeedback());
	
	temp.setServicingTime(s.getServicingTime()); 
	
	temp.setTeam(s.getTeam());
	temp.setScheduled(s.getScheduled());
	temp.setReason(s.getReason());
	temp.setSpecificServFlag(s.getSpecificServFlag());
	
//	temp.recordSelect = recordSelect;
	temp.setNumberRange(s.getNumberRange());
	
	temp.setTrackServiceTabledetails(s.getTrackServiceTabledetails()); 
	temp.setRefNo(s.getRefNo());
	temp.setRemark(s.getRemark());
	temp.setTechnicianRemark(s.getTechnicianRemark());
	temp.setPremises(s.getPremises());
	
	temp.setFromTime(s.getFromTime()); 
	temp.setToTime(s.getToTime());

	temp.setWareHouse(s.getWareHouse());

	temp.setStorageLocation(s.getStorageLocation());
	temp.setStorageBin(s.getStorageBin());

	temp.setStackDetailsList(s.getStackDetailsList());
	temp.setRefNo2(s.getRefNo2()); 
	temp.setServiceWeekNo(s.getServiceWeekNo());

	temp.setQuantity(s.getQuantity());
	temp.setUom(s.getUom());

	temp.setServiceValue(s.getServiceValue());
	
	temp.setServiceWiseBilling(s.isServiceWiseBilling());
	temp.setServiceSrNo(s.getServiceSrNo());
	temp.setSchedulingNumber(s.getSchedulingNumber());
	
//	temp.projectId = projectId;
	temp.setInvoiceId(s.getInvoiceId());
	temp.setInvoiceDate(s.getInvoiceDate());
	temp.setRateContractService(s.isRateContractService());
	temp.setAssessmentServiceFlag(s.isAssessmentServiceFlag());
	
//	temp.techPlanOtherInfo = techPlanOtherInfo;
	temp.setCustomerSignature(s.getCustomerSignature()); 
	temp.setCustomerSignName(s.getCustomerSignName()); 
	temp.setCustomerSignNumber(s.getCustomerSignNumber());
	temp.setDescription(s.getDescription());
	
	temp.setTechnicians(s.getTechnicians()); 
	temp.setStartLatnLong(s.getStartLatnLong());
	temp.setReportLatnLong(s.getReportLatnLong());
	temp.setCompleteLatnLong(s.getCompleteLatnLong());
	temp.setPerUnitPrice(s.getPerUnitPrice());
	temp.setCostCenter(s.getCostCenter()); 

	temp.setServiceWiseLatLong(s.getServiceWiseLatLong());
	
	temp.setRenewContractFlag(s.isRenewContractFlag());
	
//	temp.isCallwiseService= isCallwiseService ;
	temp.setProModelNo(s.getProModelNo());
	temp.setProSerialNo(s.getProSerialNo());
	temp.setServiceProductList(s.getServiceProductList());

	
	temp.setContractStartDate(s.getContractStartDate()); 	
	temp.setContractEndDate(s.getContractEndDate());
	temp.setServiceSerialNo(s.getServiceSerialNo());
	temp.setServiceIndexNo(s.getServiceIndexNo());
	
	temp.setServiceType(s.getServiceType());
	temp.setAdHocService(s.isAdHocService());

	temp.setBillingCount(s.getBillingCount());
	
	temp.setServiceNumber(s.getServiceNumber());
	temp.setHours(s.getHours());
	temp.setMinutes(s.getMinutes());
	temp.setAmpm(s.getAmpm());
	
	temp.setProjectId(s.getProjectId());
	return temp;
}

@Override
public String updateFindingEntity(Long companyId) {
	String result="Success";
	try {
	List<Service> serviceList = ofy().load().type(Service.class)
			.filter("companyId",companyId)
			.filter("status", Service.SERVICESTATUSCOMPLETED).list();
	TechnicianAppOperation opr=new TechnicianAppOperation();
	if(serviceList!=null) {
		logger.log(Level.SEVERE,"in updateFindingEntity serviceList size="+serviceList.size());
		
		for(Service s:serviceList) {
			if(s.getCatchtrapList()!=null&&s.getCatchtrapList().size()>0) {
				opr.updateFindingsEntity(s, s.getCatchtrapList());
			}
		}
		logger.log(Level.SEVERE,"all services processed and finding entries created");
		
	}
	}catch(Exception e) {
		e.printStackTrace();
		return "failed";
	}
	return result;
	}
}
