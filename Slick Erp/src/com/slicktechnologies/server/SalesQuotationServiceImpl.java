package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.salesquotation.SalesQuotationService;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesQuotationServiceImpl extends RemoteServiceServlet implements SalesQuotationService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5187296516341239880L;

	@Override
	public void changeStatus(SalesQuotation q) {
		if(q!=null)
		{
			changeCustomerStatus(q);
			ChangeLeadStatus(q);
		}
	}
	
    private void changeCustomerStatus(SalesQuotation q)
    {
    	List<Customer>cust = null;
    	System.out.println("Called Customer Status Changed From Quotation");
    	System.out.println("Quotation Status Value "+q.getStatus());
    	
    	if(q.getCinfo()!=null)
    	{
    	   if(q.getStatus().equals(SalesQuotation.APPROVED))
    	   {
    		  if(q.getCompanyId()!=null)
    		   cust =	ofy().load().type(Customer.class).filter("count",q.getCinfo().getCount()).
    				   filter("companyId",q.getCompanyId()).list();
    		  else
    			  cust =ofy().load().type(Customer.class).filter("count",q.getCinfo().getCount()).list(); 
    	      if(cust!=null)
    	      {
    	    	  System.out.println("Size is "+cust.size());
    	      for(Customer c:cust)
    	          {
    	    	  	if(!(c.getStatus().equals(Customer.CUSTOMERACTIVE)))
    	    	  	{
    	    	  		System.out.println("Changing Status of Customer --"+c.getName());
    	    	  		c.setStatus(Customer.CUSTOMERPROSPECT);
    	    	  		System.out.println("Changed Status To --"+c.getStatus());
    	    	  		
    	    	  	}
    				
    	    	  }	
    	      }
    	   }
    	   
    	   if(q.getStatus().equals(SalesQuotation.SALESQUOTATIONUNSUCESSFUL))
    	   {
    		   if(q.getCompanyId()!=null)
    		    cust =ofy().load().type(Customer.class).filter("count",q.getCinfo().getCount())
    		    		.filter("companyId",q.getCompanyId()).list();
    		   else
    			   cust =ofy().load().type(Customer.class).filter("count",q.getCinfo().getCount()).list();  
    		   
    	       if(cust!=null)
    	      for(Customer c:cust)
    	          {
    	    	  	if(!(c.getStatus().equals(Customer.CUSTOMERACTIVE)))
    	    	  	{
    	    	  		System.out.println("Changing Status of Customer --"+c.getName());
    	    	  		c.setStatus(Customer.CUSTOMERCREATED);
    	    	  		System.out.println("Changed Status To --"+c.getStatus());
    	    	  		
    	    	  	}
    				
    	    	  	 }
    	      }
    	   if(cust!=null&&cust.size()!=0)
    	       ofy().save().entities(cust).now();
    	} 
    	
    	ofy().save().entity(q).now();
    	
     }
    
    private void ChangeLeadStatus(SalesQuotation q) {

    	if(q.getLeadCount()!=0){
    		Lead leadEntity = ofy().load().type(Lead.class).filter("companyId", q.getCompanyId()).filter("count", q.getLeadCount()).first().now();
    		if(leadEntity!=null){
    			leadEntity.setStatus("Quotation Sent");
    			ofy().save().entity(leadEntity);
    		}
    	}
	}


}
