package com.slicktechnologies.server.materialconsumption;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jetty.servlets.QoSFilter;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.MaterialConsumptionService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.materialconsumptionreport.MaterialProductReport;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.inventory.ExpenseCost;
import com.slicktechnologies.shared.common.inventory.LabourCost;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class MaterialConsumptionImpl extends RemoteServiceServlet implements MaterialConsumptionService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1824047653699175175L;
	
	DecimalFormat decimalformat = new DecimalFormat("0.00");
	
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	Logger logger=Logger.getLogger("Consumption Logger");
	
	public static ArrayList<String> emailIdList = new ArrayList<String>();

	@Override
	public ArrayList<MaterialConsumptionReport> retrieveMinList(long companyId,int contractCount) {
	
		List<MaterialIssueNote> minEntityLis=ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("minSoId", contractCount).list();
		
		
		
		logger.log(Level.SEVERE,"Size Of Loading MIN List"+minEntityLis.size());
		ArrayList<Integer> prodCountLis=new ArrayList<Integer>();
		ArrayList<MaterialProductReport> prodReportLis=new ArrayList<MaterialProductReport>();
		ArrayList<MaterialConsumptionReport> consumptionMinReport=new ArrayList<MaterialConsumptionReport>();
		ArrayList<MaterialConsumptionReport> retrunMinReport=new ArrayList<MaterialConsumptionReport>();
		HashSet<Integer> contractId = new HashSet<Integer>();
		for(int i=0;i<minEntityLis.size();i++)
		{
			if(minEntityLis.get(i).getStatus().trim().equals(MaterialIssueNote.APPROVED))
			{
				for(int j=0;j<minEntityLis.get(i).getSubProductTablemin().size();j++)
				{
					MaterialConsumptionReport report=new MaterialConsumptionReport();
					report.setContractId(minEntityLis.get(i).getMinSoId());
					if(minEntityLis.get(i).getMinSoId()!=0){
						contractId.add(minEntityLis.get(i).getMinSoId());
					}
					report.setCustId(minEntityLis.get(i).getCinfo().getCount());
					report.setCustName(minEntityLis.get(i).getCinfo().getFullName());
					report.setCustCellNo(minEntityLis.get(i).getCinfo().getCellNumber()+"");
					report.setServiceId(minEntityLis.get(i).getServiceId());
					report.setDocID(minEntityLis.get(i).getCount());
					report.setProdId(minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductId());
					report.setProdName(minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductName());
					report.setProdCode(minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductCode());
					report.setProdUOM(minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductUOM());
					report.setQuantity(minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductRequiredQuantity());
					report.setBranch(minEntityLis.get(i).getBranch());
//					logger.log(Level.SEVERE,"qTY"+minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductRequiredQuantity());
					consumptionMinReport.add(report);
					prodCountLis.add(minEntityLis.get(i).getSubProductTablemin().get(j).getMaterialProductId());
				}
			}
		}
		logger.log(Level.SEVERE,"Consumption Report List Size=="+consumptionMinReport.size());
		logger.log(Level.SEVERE,"Prod List Size=="+prodCountLis.size());
		List<Integer> uniqueProdIds=new ArrayList<Integer>(new HashSet<Integer>(prodCountLis));
		logger.log(Level.SEVERE,"Unique Prod List Size=="+uniqueProdIds.size());
		
		for(int k=0;k<uniqueProdIds.size();k++)
		{
			SuperProduct prodEntity=ofy().load().type(SuperProduct.class).filter("count", uniqueProdIds.get(k)).filter("companyId", companyId).first().now();
			MaterialProductReport prodReport=new MaterialProductReport();
			prodReport.setProdId(uniqueProdIds.get(k));
			prodReport.setProdPrice(prodEntity.getPrice());
			prodReportLis.add(prodReport);
		}
		
		List<Contract> conList =null;
		if(contractId.size()>0){
			ArrayList<Integer> contractC = new ArrayList<Integer>();
			contractC.addAll(contractId);
			
			conList = ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("count IN", contractC).list();
			
		}
		
		
		
		for(int l=0;l<consumptionMinReport.size();l++)
		{
			MaterialConsumptionReport report=new MaterialConsumptionReport();
			report.setContractId(consumptionMinReport.get(l).getContractId());
			report.setServiceId(consumptionMinReport.get(l).getServiceId());
			report.setDocID(consumptionMinReport.get(l).getDocID());
			report.setDocType("MIN");
			report.setProdId(consumptionMinReport.get(l).getProdId());
			report.setProdName(consumptionMinReport.get(l).getProdName());
			report.setProdCode(consumptionMinReport.get(l).getProdCode());
			report.setProdUOM(consumptionMinReport.get(l).getProdUOM());
			report.setQuantity(consumptionMinReport.get(l).getQuantity());
			report.setBranch(consumptionMinReport.get(1).getBranch());
			double prodPriceVal=findProductCount(consumptionMinReport.get(l).getProdId(),prodReportLis);
			report.setPrice(prodPriceVal);
			report.setCustId(consumptionMinReport.get(l).getCustId());
			report.setCustName(consumptionMinReport.get(l).getCustName());
			report.setCustCellNo(consumptionMinReport.get(l).getCustCellNo());
			if(conList!=null && conList.size()>0){
				for(Contract con : conList){
					if(con.getCount() == consumptionMinReport.get(l).getContractId())
					{
						report.setContAmount(con.getNetpayable());
						break;
					}
				}
			}
			retrunMinReport.add(report);
		}
		return retrunMinReport;
	}
	
	
	public double findProductCount(int pid,List<MaterialProductReport> lisProducts)
	{
		double prodPrice=0;
		for(int m=0;m<lisProducts.size();m++)
		{
			if(lisProducts.get(m).getProdId()==pid)
			{
				prodPrice=lisProducts.get(m).getProdPrice();
			}
		}
		return prodPrice;
	}

	@Override
	public ArrayList<MaterialConsumptionReport> retrieveMmnList(long companyId,int contractCount) {
		
		Logger logger=Logger.getLogger("Consumption Logger");
		List<MaterialMovementNote> mmnEntityLis=ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("orderID", contractCount+"").list();
		logger.log(Level.SEVERE,"Size Of Loading MMN List"+mmnEntityLis.size());
		ArrayList<Integer> prodCountLis=new ArrayList<Integer>();
		ArrayList<MaterialProductReport> prodReportLis=new ArrayList<MaterialProductReport>();
		ArrayList<MaterialConsumptionReport> consumptionMmnReport=new ArrayList<MaterialConsumptionReport>();
		ArrayList<MaterialConsumptionReport> retrunMmnReport=new ArrayList<MaterialConsumptionReport>();
		
		
		
		for(int i=0;i<mmnEntityLis.size();i++)
		{
			if(mmnEntityLis.get(i).getStatus().trim().equals(MaterialIssueNote.APPROVED))
			{
				for(int j=0;j<mmnEntityLis.get(i).getSubProductTableMmn().size();j++)
				{
					MaterialConsumptionReport report=new MaterialConsumptionReport();
					report.setContractId(Integer.parseInt(mmnEntityLis.get(i).getOrderID()));
					report.setServiceId(mmnEntityLis.get(i).getServiceId());
					report.setDocID(mmnEntityLis.get(i).getCount());
					report.setProdId(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductId());
					report.setProdName(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductName());
					report.setProdCode(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductCode());
					report.setProdUOM(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductUOM());
					report.setQuantity(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity());
					logger.log(Level.SEVERE,"Qty Mmn"+mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity());
					report.setBranch(mmnEntityLis.get(1).getBranch());
					consumptionMmnReport.add(report);
					prodCountLis.add(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductId());
				}
			}
		}
		logger.log(Level.SEVERE,"Consumption Report List Size=="+consumptionMmnReport.size());
		logger.log(Level.SEVERE,"Prod List Size=="+prodCountLis.size());
		List<Integer> uniqueProdIds=new ArrayList<Integer>(new HashSet<Integer>(prodCountLis));
		logger.log(Level.SEVERE,"Unique Prod List Size=="+uniqueProdIds.size());
		
		for(int k=0;k<uniqueProdIds.size();k++)
		{
			SuperProduct prodEntity=ofy().load().type(SuperProduct.class).filter("count", uniqueProdIds.get(k)).filter("companyId", companyId).first().now();
			MaterialProductReport prodReport=new MaterialProductReport();
			prodReport.setProdId(uniqueProdIds.get(k));
			prodReport.setProdPrice(prodEntity.getPrice());
			prodReportLis.add(prodReport);
		}
		
		
		for(int l=0;l<consumptionMmnReport.size();l++)
		{
			MaterialConsumptionReport report=new MaterialConsumptionReport();
			report.setContractId(consumptionMmnReport.get(l).getContractId());
			report.setServiceId(consumptionMmnReport.get(l).getServiceId());
			report.setDocID(consumptionMmnReport.get(l).getDocID());
			report.setDocType("MMN");
			report.setProdId(consumptionMmnReport.get(l).getProdId());
			report.setProdName(consumptionMmnReport.get(l).getProdName());
			report.setProdCode(consumptionMmnReport.get(l).getProdCode());
			report.setProdUOM(consumptionMmnReport.get(l).getProdUOM());
			report.setQuantity(consumptionMmnReport.get(l).getQuantity());
			double prodPriceVal=findProductCount(consumptionMmnReport.get(l).getProdId(),prodReportLis);
			logger.log(Level.SEVERE,"Product Price Min"+prodPriceVal);
			report.setBranch(consumptionMmnReport.get(1).getBranch());
			report.setPrice(prodPriceVal);
			retrunMmnReport.add(report);
		}
		return retrunMmnReport;
		
	}


@Override
public ArrayList<LabourCost> retrieveLabourCost(long companyId,String contractOrCustCount, String type, Date fromDate, Date toDate) {

	logger.log(Level.SEVERE,"Inside the impl labour cost , TYPE-"+type+" CONTRACT/CUSTOMER ID-"+contractOrCustCount);

	ArrayList<LabourCost> labourCostList = new ArrayList<LabourCost>();	
	List<Service> serviceList = new ArrayList<Service>();
	HashMap<Integer, Double> contractPrice = new HashMap<Integer, Double>();
	if (type.equalsIgnoreCase("Contract")) {
		
		serviceList = ofy().load().type(Service.class)
				.filter("companyId", companyId).filter("contractCount",
						Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
	
	}else if (type.equalsIgnoreCase("branch")) {
		logger.log(Level.SEVERE, "Branch name labour -- " + contractOrCustCount + "  today --  " 
				+DateUtility.getDateWithTimeZone("IST", fromDate) 
				+" \n to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));

		List<Integer> intContractLis= new ArrayList<Integer>();
		List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
				filter("status", Contract.APPROVED).
				filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
				filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
		// added all contracts in integer list 
		logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
		List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
				filter("branch", contractOrCustCount).filter("status", Contract.APPROVED).
				filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
				filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
		// added all contracts in integer list 
		logger.log(Level.SEVERE,"contract -- for branch " + contractLis.size());
		for (Contract contract : contractLis) {
			intContractLis.add(contract.getCount());
			contractPrice.put(contract.getCount(), contract.getNetpayable());
		}
		
		logger.log(Level.SEVERE,"Branch contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
		if(intContractLis.size()>0){
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
		}
		
		logger.log(Level.SEVERE,"segment contract -- " + serviceList.size() +" branch no == " +contractOrCustCount);
		
	
	}else if (type.equalsIgnoreCase("segment")) {
		
		List<Integer> intContractLis= new ArrayList<Integer>();
		List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
				filter("status", Contract.APPROVED).
				filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
				filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
		// added all contracts in integer list 
		logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
		
		logger.log(Level.SEVERE, "segment labour name -- " + contractOrCustCount + "  today --  " 
				+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
		List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
				filter("segment", contractOrCustCount).filter("status", Contract.APPROVED).
				filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
				filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
		// added all contracts in integer list 
		logger.log(Level.SEVERE,"contract -- " + contractLis.size());
		
		for (Contract contract : contractLis) {
			intContractLis.add(contract.getCount());
			contractPrice.put(contract.getCount(), contract.getNetpayable());
		}
		
		logger.log(Level.SEVERE,"segment contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
		if(intContractLis.size()>0){
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
		}
		
	
	}   else{
		
		serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
	
	}

	logger.log(Level.SEVERE,"SERVICE LIST SIZE : "+serviceList.size());

	if(serviceList.size()!=0){
		
		for(Service service:serviceList){
//		for(int i=0;i<serviceList.size();i++){
			List<ServiceProject> projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", service.getContractCount()).filter("serviceId", service.getCount()).list();
			logger.log(Level.SEVERE,"PROJECT LIST SIZE : "+projectList.size());
			for(ServiceProject project:projectList){
//			for(int j=0;j<projectList.size();j++){
				for(EmployeeInfo empInfo:project.getTechnicians()){
//				for(int k=0;k<project.getTechnicians().size();k++){
					String empName = empInfo.getFullName().trim();
					
					Employee emp = ofy().load().type(Employee.class).filter("companyId", project.getCompanyId()).filter("fullname", empName).first().now();
					
					Double serviceduration = (double) service.getServiceCompleteDuration();
					Double servicecompletioninhours =  serviceduration/60;
					
					double empMonthlySalary =emp.getSalaryAmt();
					double empMonthlyWorkingHrs =emp.getMonthlyWorkingHrs();
					double empHourSalary = empMonthlySalary/empMonthlyWorkingHrs;
					
					Double labourCost = empHourSalary*servicecompletioninhours;
					
					LabourCost labourcost = new LabourCost();
					labourcost.setServiceId(service.getCount());
					labourcost.setEmployeeId(emp.getCount());
					labourcost.setEmployeeName(emp.getFullName());
					labourcost.setEmployeeCell(emp.getCellNumber1()+"");
					labourcost.setServiceHours(decimalformat.format(servicecompletioninhours));
					labourcost.setEmpHourSalary(decimalformat.format(empHourSalary));
					labourcost.setLabourCost(Math.round(labourCost));
					labourcost.setContractId(service.getContractCount());
					labourcost.setCustId(service.getCustomerId());
					labourcost.setCustCellNo(service.getCellNumber()+"");
					labourcost.setCustName(service.getCustomerName());
					labourcost.setBranch(service.getBranch());
					if(contractPrice.size()>0){
						if(contractPrice.containsKey(service.getContractCount())){
							labourcost.setContTotal(contractPrice.get(service.getContractCount()));
						}
					}
					labourCostList.add(labourcost);
				}
			}
		}
	}
	return labourCostList;
}

	private int daysInMonth(String payRollPeriod,String timeZone)
	{
		SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance(); 
		isoFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
		logger.log(Level.SEVERE," :::Salary period DIM :::  "+payRollPeriod);
		
		try {
			Date d=isoFormat.parse(payRollPeriod);
			c.setTime(d);
			c.add(Calendar.DATE, 1);
			d=DateUtility.getDateWithTimeZone("IST", c.getTime());
			
			Date startDate=new Date(d.getTime());
			Date endDate=new Date(d.getTime());
			startDate=DateUtility.getStartDateofMonth(d);
			logger.log(Level.SEVERE," :::START DATE DIM :::  "+startDate);
			logger.log(Level.SEVERE," :::Total Days DIM :::  "+c.getActualMaximum(Calendar.DAY_OF_MONTH));
			return c.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		catch(Exception e)
		{
		}
		return 0;
	}
	
	
	@Override
	public ArrayList<ExpenseCost> retrieveExpenseCost(long companyId,String contractid,String type, Date fromDate, Date toDate) {
		System.out.println("contract id "+contractid+"==type"+type);
		ArrayList<ExpenseCost> expenseCostList = new  ArrayList<ExpenseCost>();
		List<Service> serviceList= new ArrayList<Service>();
		
//		if(type.equalsIgnoreCase("Customer")){
//			 serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count",Integer.parseInt(contractid)).list();
//		}else{
//			 serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", Integer.parseInt(contractid)).list();
//		}
		HashMap<Integer, Double> contractPrice = new HashMap<Integer, Double>();
		HashMap<Integer, Double> contractSerPrice = new HashMap<Integer, Double>();
		if (type.equalsIgnoreCase("Contract")) {
			
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).
					filter("contractCount", Integer.parseInt(contractid)).filter("status", "Completed").list();
		
		}else if (type.equalsIgnoreCase("branch")) {
			ArrayList<Integer> intContractLis = new ArrayList<Integer>();
			logger.log(Level.SEVERE, "Branch name -- " + contractid + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
		/*	serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("branch", contractid).
					filter("contractEndDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("contractEndDate <=", DateUtility.getDateWithTimeZone("IST", toDate))
			.filter("status", "Completed").list();*/
		
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("branch", contractid).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
				contractPrice.put(contract.getCount(), contract.getNetpayable());
			}
			
			logger.log(Level.SEVERE,"Branch contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
		}else if (type.equalsIgnoreCase("segment")) {
			logger.log(Level.SEVERE, "Branch name seg-- " + contractid + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) 
					+" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
			List<String> intContractLis= new ArrayList<String>();
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("segment", contractid).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate))
					.list();
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
				contractPrice.put(contract.getCount(), contract.getNetpayable());
			}
			if(intContractLis.size()!=0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
		
		}   else{
			
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("personInfo.count", Integer.parseInt(contractid)).filter("status", "Completed").list();
		
		}
		
		logger.log(Level.SEVERE," :::servicelist size :::  "+serviceList.size());
		if(serviceList.size()!=0)
		{
			List<Integer> serviceCountLis= new ArrayList<Integer>();
			for (int i = 0; i < serviceList.size(); i++) {
				serviceCountLis.add(serviceList.get(i).getCount());
				contractSerPrice.put(serviceList.get(i).getCount(), contractPrice.get(serviceList.get(i).getContractCount()));
			}
			if(serviceCountLis.size()==0){
				return expenseCostList;
			}
			
																																	    
			List<MultipleExpenseMngt> expenseList = ofy().load().type(MultipleExpenseMngt.class).filter("companyId", companyId)
					.filter("serviceId IN", serviceCountLis).filter("status", "Approved").list();
			logger.log(Level.SEVERE," :::expenseList size :::  "+expenseList.size());
			if(expenseList.size()!=0){
				
				for(int i =0; i<expenseList.size();i++){
					
					ExpenseCost expensecost = new ExpenseCost();
					
//					expensecost.setContractId(contractid);
					expensecost.setServiceId(expenseList.get(i).getServiceId());
					expensecost.setExpenseId(expenseList.get(i).getCount());
//					expensecost.setExpenseDate(fmt.format(expenseList.get(i).getExpenseDate()));
					expensecost.setApprovarName(expenseList.get(i).getApproverName());
					expensecost.setExpenseAmt(expenseList.get(i).getTotalAmount());
					expensecost.setExpenseCategory(expenseList.get(i).getCategory());
					expensecost.setExpenseType(expenseList.get(i).getType());
					expensecost.setEmpployeeName(expenseList.get(i).getEmployee());
					if(contractPrice.size()>0){
						if(contractSerPrice.containsKey(expenseList.get(i).getServiceId())){
							expensecost.setContCost(contractSerPrice.get(expensecost.getServiceId()));
						}
					}
					expenseCostList.add(expensecost);
				}
			}
		}
		logger.log(Level.SEVERE," :::expensecostlist size :::  "+expenseCostList.size());
		
		return expenseCostList;
	}


	@Override
	public ArrayList<String> retrieveAdministrativeCost(long companyId,String contractdate) {
		
		ArrayList<String> adminCostWithCalendar= new ArrayList<String>();
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Date contDate = null ;
		Date contractDate=null;
		
		double administrativeCost=0;
		
		/*// here i am setting time 00 00 00 to contract date
		try {
			contDate = fmt.parse(contractdate);
			logger.log(Level.SEVERE," before contract Date ==="+contDate);
			Calendar cal4=Calendar.getInstance();
			cal4.setTime(contDate);
			cal4.add(Calendar.DATE, 0);
			contractDate=fmt.parse(fmt.format(cal4.getTime()));
			cal4.set(Calendar.HOUR_OF_DAY,0);
			cal4.set(Calendar.MINUTE,00);
			cal4.set(Calendar.SECOND,00);
			cal4.set(Calendar.MILLISECOND,000);
			contractDate=cal4.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"contractDate with time 00:00:00"+contractDate);
		
		List<FinancialCalender> finYearList =  ofy().load().type(FinancialCalender.class).filter("companyId", companyId).list();
		
		logger.log(Level.SEVERE,"financialyearlist size"+finYearList.size());
		
		
		for(int i=0;i<finYearList.size();i++){

			logger.log(Level.SEVERE," Financial Start Date Year  =="+finYearList.get(i).getStartDate());
			logger.log(Level.SEVERE," Financial End Date Year  =="+finYearList.get(i).getEndDate());

			if(contDate.after(finYearList.get(i).getStartDate()) && contDate.before(finYearList.get(i).getEndDate()) 
					|| contractDate.equals(finYearList.get(i).getEndDate())|| contractDate.equals(finYearList.get(i).getStartDate())){
				logger.log(Level.SEVERE," Financial Year Calendar Name  =="+finYearList.get(i).getCalendarName());

				double totalPaymentAmt=0;
				double totalAdminExpAmt=0;
				System.out.println(" financial date=="+finYearList.get(i).getStartDate());
				
				if(finYearList.get(i).getStartDate()!=null)
				{	
					List<CustomerPayment> paymentList = ofy().load().type(CustomerPayment.class)
							.filter("paymentDate >=", finYearList.get(i).getStartDate())
							.filter("paymentDate <=", finYearList.get(i).getEndDate()).filter("companyId", companyId).list();
					System.out.println(" paymnet list size =="+paymentList.size());	
					logger.log(Level.SEVERE," paymnet list size  =="+paymentList.size());
					List<CustomerPayment> custPayList = new ArrayList<CustomerPayment>();
					
					for(int j=0;j<paymentList.size();j++){
						if(!paymentList.get(j).getStatus().equals("Cancelled")){
							custPayList.add(paymentList.get(j));
						}
					}
					
					System.out.println(" new payment list size =="+custPayList.size());
					logger.log(Level.SEVERE," payment list without cancelled payment list size"+custPayList.size());
		
					for(int k=0;k<custPayList.size();k++){
						totalPaymentAmt += custPayList.get(k).getPaymentAmt();
					}
					logger.log(Level.SEVERE,"Total Payment amount"+totalPaymentAmt);

					List<Expense> expenselist = ofy().load().type(Expense.class).filter("companyId", companyId)
		.filter("category", "Administrative Cost").filter("status", "Approved")
		.filter("expenseDate >=", finYearList.get(i).getStartDate())
		.filter("expenseDate <=",finYearList.get(i).getEndDate()).list();
					logger.log(Level.SEVERE," Administrative Expense list size"+expenselist.size());
					for(int l=0;l<expenselist.size();l++){
						totalAdminExpAmt +=expenselist.get(l).getAmount();
					}
					System.out.println("Total paymnet amt =="+totalPaymentAmt);
					System.out.println(" Total expense amt ==="+totalAdminExpAmt);
					logger.log(Level.SEVERE," Total expense amt ==="+totalAdminExpAmt);

					administrativeCost =totalAdminExpAmt/totalPaymentAmt;
					logger.log(Level.SEVERE," Administrative cost final ==="+administrativeCost);

					System.out.println(" Administrative cost final ==="+administrativeCost);
					adminCostWithCalendar.add(administrativeCost+"");
					adminCostWithCalendar.add(finYearList.get(i).getCalendarName());
			 }
			}
		}*/
		return adminCostWithCalendar;
	}
	
	
	


	@Override
	public ArrayList<MaterialConsumptionReport> retriveMaterialDetails(long companyId, String conOrCustCount, String typeOfPnL, Date fromDate, Date toDate) {
		
		List<Integer> intContractLis= new ArrayList<Integer>();
		ArrayList<Integer> prodCountLis=new ArrayList<Integer>();
		ArrayList<MaterialProductReport> prodReportLis=new ArrayList<MaterialProductReport>();
		ArrayList<MaterialConsumptionReport> consumptionMinReport=new ArrayList<MaterialConsumptionReport>();
		ArrayList<MaterialConsumptionReport> retrunMinReport=new ArrayList<MaterialConsumptionReport>();
		HashSet<Integer> contractId = new HashSet<Integer>();
		if(typeOfPnL.equalsIgnoreCase("Customer")){
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("cinfo.count",Integer.parseInt(conOrCustCount)).list();
				
			if(contractLis!=null){
				// added all contracts in integer list 
				for (Contract contract : contractLis) {
					intContractLis.add(contract.getCount());
				}
			}
				
			
		} else if (typeOfPnL.equalsIgnoreCase("branch")) {
			logger.log(Level.SEVERE, "remat branch labour name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("status",  Contract.APPROVED)
					.filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
			logger.log(Level.SEVERE,"get con list --"+contractLis1.size());
			
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("status",  Contract.APPROVED).
					filter("branch",conOrCustCount)
					.filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
			if(contractLis!=null){
				// added all contracts in integer list 
				for (Contract contract : contractLis) {
					intContractLis.add(contract.getCount());
				}
			}
		}	else if (typeOfPnL.equalsIgnoreCase("segment")) {
			
			logger.log(Level.SEVERE, "segment labour name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
			

			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("segment",conOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			logger.log(Level.SEVERE,"contractLis -- list -- " + contractLis.size());
			if(contractLis!=null){
				// added all contracts in integer list 
				for (Contract contract : contractLis) {
					intContractLis.add(contract.getCount());
				}
			}
		} else{
		
			intContractLis.add(Integer.parseInt(conOrCustCount));
		}
		
		if(intContractLis.size()==0){
			return retrunMinReport;
		}
		
		/**
		 * this is MIN Loading part  
		 */
		List<MaterialIssueNote> minEntityLis=ofy().load().type(MaterialIssueNote.class)
				.filter("companyId", companyId).filter("minSoId IN", intContractLis).list();
		logger.log(Level.SEVERE,"Size Of Loading MIN List"+minEntityLis.size());
		
		
		
		for (MaterialIssueNote materialIssueNote : minEntityLis) {
			
			if(materialIssueNote.getStatus().trim().equalsIgnoreCase(MaterialIssueNote.APPROVED)){
				
				for (int i = 0; i < materialIssueNote.getSubProductTablemin().size(); i++) {
					
					MaterialConsumptionReport report=new MaterialConsumptionReport();
					report.setContractId(materialIssueNote.getMinSoId());
					if(materialIssueNote.getMinSoId()!=0){
						contractId.add(materialIssueNote.getMinSoId());
					}
					report.setServiceId(materialIssueNote.getServiceId());
					report.setDocID(materialIssueNote.getCount());
					report.setCustId(materialIssueNote.getCinfo().getCount());
					report.setCustName(materialIssueNote.getCinfo().getFullName());
					report.setCustCellNo(materialIssueNote.getCinfo().getCellNumber()+"");
					report.setProdId(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductId());
					report.setProdName(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductName());
					report.setProdCode(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductCode());
					report.setProdUOM(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductUOM());
					report.setQuantity(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
					report.setBranch(materialIssueNote.getBranch());
//					for(Contract contract:con){
//						if(materialIssueNote.getMinSoId() == contract.getCount()){
//							
//							report.setContGroup(contract.getGroup());
//						}
//					}
					report.setContGroup(materialIssueNote.getGroup());
					report.setContType(materialIssueNote.getType());
					
					/*
					 * end by Ashwini
					 */
					logger.log(Level.SEVERE,"qTY"+materialIssueNote.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
					consumptionMinReport.add(report);
					prodCountLis.add(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductId());
				}
			}
		}
		
		logger.log(Level.SEVERE,"Consumption Report List MIN Size=="+consumptionMinReport.size());
		logger.log(Level.SEVERE,"Prod List MIN Size=="+prodCountLis.size());
		List<Integer> uniqueProdIds=new ArrayList<Integer>(new HashSet<Integer>(prodCountLis));
		logger.log(Level.SEVERE,"Unique Prod List MIN Size=="+uniqueProdIds.size());
		
		if(uniqueProdIds.size()==0){
			return retrunMinReport;
		}
		List<SuperProduct>  prodEntity=new ArrayList<SuperProduct>();
		if(uniqueProdIds.size()!=0){
			prodEntity=ofy().load().type(SuperProduct.class).filter("count IN", uniqueProdIds).filter("companyId", companyId).list();
		}
		for(int k=0;k<uniqueProdIds.size();k++)
		{
			MaterialProductReport prodReport=new MaterialProductReport();
			prodReport.setProdId(uniqueProdIds.get(k));
			for (int i = 0; i < prodEntity.size(); i++) {
				if(uniqueProdIds.get(k)==prodEntity.get(i).getCount()){
					prodReport.setProdPrice(prodEntity.get(i).getPrice());
				}
			}
			prodReportLis.add(prodReport);
		}
		List<Contract> conList =null;
		if(contractId.size()>0){
			ArrayList<Integer> contractC = new ArrayList<Integer>();
			contractC.addAll(contractId);
			
			conList = ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("count IN", contractC).list();
			
		}
		
		
		for(int l=0;l<consumptionMinReport.size();l++)
		{
			MaterialConsumptionReport report=new MaterialConsumptionReport();
			report.setContractId(consumptionMinReport.get(l).getContractId());
			report.setServiceId(consumptionMinReport.get(l).getServiceId());
			report.setDocID(consumptionMinReport.get(l).getDocID());
			report.setDocType("MIN");
			report.setProdId(consumptionMinReport.get(l).getProdId());
			report.setProdName(consumptionMinReport.get(l).getProdName());
			report.setProdCode(consumptionMinReport.get(l).getProdCode());
			report.setProdUOM(consumptionMinReport.get(l).getProdUOM());
			report.setQuantity(consumptionMinReport.get(l).getQuantity());
			report.setBranch(consumptionMinReport.get(l).getBranch());
			double prodPriceVal=findProductCount(consumptionMinReport.get(l).getProdId(),prodReportLis);
			logger.log(Level.SEVERE,"Product Price Min"+prodPriceVal);
			logger.log(Level.SEVERE,"Product contract id-- "+report.getContractId());
			report.setPrice(prodPriceVal);
			report.setCustId(consumptionMinReport.get(l).getCustId());
			report.setCustName(consumptionMinReport.get(l).getCustName());
			report.setCustCellNo(consumptionMinReport.get(l).getCustCellNo());
			if(conList!=null && conList.size()>0){
				for(Contract con : conList){
					if(con.getCount() == consumptionMinReport.get(l).getContractId())
					{
						report.setContAmount(con.getNetpayable());
						
						/*
						 * added by Ashwini
						 */
						report.setContGroup(con.getGroup());
						report.setContType(con.getType());
						break;
					}
				}
			}
			retrunMinReport.add(report);
		}
		return retrunMinReport;
	}


	@Override
	public ArrayList<MaterialConsumptionReport> retriveMaterialMMNDetails(long companyId, String conOrCustCount, String typeOfPnL,Date fromDate, Date  toDate) {

		List<String> intContractLis= new ArrayList<String>();
		
		ArrayList<Integer> prodCountLis=new ArrayList<Integer>();
		ArrayList<MaterialProductReport> prodReportLis=new ArrayList<MaterialProductReport>();
		ArrayList<MaterialConsumptionReport> consumptionMmnReport=new ArrayList<MaterialConsumptionReport>();
		ArrayList<MaterialConsumptionReport> retrunMmnReport=new ArrayList<MaterialConsumptionReport>();
		
		System.out.println("contract counttttt:::"+Integer.parseInt(conOrCustCount));
		if(typeOfPnL.equalsIgnoreCase("Customer")){
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).filter("cinfo.count", Integer.parseInt(conOrCustCount)).list();
			
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
			}
			
		} else if(typeOfPnL.equalsIgnoreCase("branch")){
			logger.log(Level.SEVERE, "Mmn branch name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).filter("branch", conOrCustCount).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
			}
			
		} else if(typeOfPnL.equalsIgnoreCase("segment")){
			logger.log(Level.SEVERE, "Mmn segment name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
//			
//			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
//					filter("segment", conOrCustCount).list();
//			
//			// added all contracts in integer list 
//			for (Contract contract : contractLis) {
//				intContractLis.add(contract.getCount()+"");
//			}
			

			logger.log(Level.SEVERE, "Branch name seg-- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) 
					+" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
		intContractLis= new ArrayList<String>();
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).filter("segment", conOrCustCount).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).filter("status", "Approved").list();
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
			}
			
		}else{
		
			intContractLis.add(conOrCustCount+"");
		}
		
		if(intContractLis.size()==0){
			return retrunMmnReport;
		}
		
		Logger logger=Logger.getLogger("Consumption Logger");
		logger.log(Level.SEVERE,"Size Of Contract MMN List"+intContractLis.size());
		List<MaterialMovementNote> mmnEntityLis=ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("orderID IN", intContractLis).list();
		logger.log(Level.SEVERE,"Size Of Loading MMN List"+mmnEntityLis.size());
		
		
		
		
		
		for(int i=0;i<mmnEntityLis.size();i++)
		{
			if(mmnEntityLis.get(i).getStatus().trim().equals(MaterialIssueNote.APPROVED))
			{
				for(int j=0;j<mmnEntityLis.get(i).getSubProductTableMmn().size();j++)
				{
					MaterialConsumptionReport report=new MaterialConsumptionReport();
					report.setContractId(Integer.parseInt(mmnEntityLis.get(i).getOrderID()));
					report.setServiceId(mmnEntityLis.get(i).getServiceId());
					report.setDocID(mmnEntityLis.get(i).getCount());
					report.setProdId(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductId());
					report.setProdName(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductName());
					report.setProdCode(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductCode());
					report.setProdUOM(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductUOM());
					report.setQuantity(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity());
					logger.log(Level.SEVERE,"Qty Mmn"+mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity());
					consumptionMmnReport.add(report);
					prodCountLis.add(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductId());
				}
			}
		}
		logger.log(Level.SEVERE,"Consumption Report List MMN Size=="+consumptionMmnReport.size());
		logger.log(Level.SEVERE,"Prod List MMN Size=="+prodCountLis.size());
		List<Integer> uniqueProdIds=new ArrayList<Integer>(new HashSet<Integer>(prodCountLis));
		logger.log(Level.SEVERE,"Unique Prod List MMN Size=="+uniqueProdIds.size());
		List<SuperProduct>  prodEntity = null;
		if(uniqueProdIds.size()!=0){
			prodEntity=ofy().load().type(SuperProduct.class).filter("count IN", uniqueProdIds).filter("companyId", companyId).list();
		}
		
	for(int k=0;k<uniqueProdIds.size();k++)
		{
			MaterialProductReport prodReport=new MaterialProductReport();
			prodReport.setProdId(uniqueProdIds.get(k));
			for (int i = 0; i < prodEntity.size(); i++) {
				if(uniqueProdIds.get(k)==prodEntity.get(i).getCount()){
					prodReport.setProdPrice(prodEntity.get(i).getPrice());
				}
			}
			prodReportLis.add(prodReport);
		}
		
		
		for(int l=0;l<consumptionMmnReport.size();l++)
		{
			MaterialConsumptionReport report=new MaterialConsumptionReport();
			report.setContractId(consumptionMmnReport.get(l).getContractId());
			report.setServiceId(consumptionMmnReport.get(l).getServiceId());
			report.setDocID(consumptionMmnReport.get(l).getDocID());
			/*
			 * Added by Ashwini
			 */
			
			report.setContGroup(consumptionMmnReport.get(l).getContGroup());
			report.setContType(consumptionMmnReport.get(l).getContType());
			
			/*
			 * end by Ashwini
			 */
			report.setDocType("MMN");
			report.setProdId(consumptionMmnReport.get(l).getProdId());
			report.setProdName(consumptionMmnReport.get(l).getProdName());
			report.setProdCode(consumptionMmnReport.get(l).getProdCode());
			report.setProdUOM(consumptionMmnReport.get(l).getProdUOM());
			report.setQuantity(consumptionMmnReport.get(l).getQuantity());
			double prodPriceVal=findProductCount(consumptionMmnReport.get(l).getProdId(),prodReportLis);
			logger.log(Level.SEVERE,"Product Price MMN"+prodPriceVal);
			report.setPrice(prodPriceVal);
			retrunMmnReport.add(report);
		}
		return retrunMmnReport;
		
	}


	@Override
	public ArrayList<String> retrieveAdministrativeCost(long companyId,
			String contractOrCustCount, String type, Date fromDate, Date toDate) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside the impl labour cost , TYPE-"+type+" CONTRACT/CUSTOMER ID-"+contractOrCustCount);

		ArrayList<LabourCost> labourCostList = new ArrayList<LabourCost>();	
		List<Service> serviceList = new ArrayList<Service>();
		
		if (type.equalsIgnoreCase("Contract")) {
			
			serviceList = ofy().load().type(Service.class)
					.filter("companyId", companyId).filter("contractCount",
							Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
		
		}else if (type.equalsIgnoreCase("branch")) {
			logger.log(Level.SEVERE, "Branch name labour -- " + contractOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) 
					+" \n to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));

			List<Integer> intContractLis= new ArrayList<Integer>();
			
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("branch", contractOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
			}
			
			logger.log(Level.SEVERE,"Branch contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
			logger.log(Level.SEVERE,"segment contract -- " + serviceList.size() +" branch no == " +contractOrCustCount);
			
		
		}else if (type.equalsIgnoreCase("segment")) {
			
			List<Integer> intContractLis= new ArrayList<Integer>();
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			
			logger.log(Level.SEVERE, "segment labour name -- " + contractOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("segment", contractOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
			}
			
			logger.log(Level.SEVERE,"segment contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
		
		}   else{
			
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
		
		}

		logger.log(Level.SEVERE,"SERVICE LIST SIZE : "+serviceList.size());

		if(serviceList.size()!=0){
			
			for(Service service:serviceList){
//			for(int i=0;i<serviceList.size();i++){
				List<ServiceProject> projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", service.getContractCount()).filter("serviceId", service.getCount()).list();
				logger.log(Level.SEVERE,"PROJECT LIST SIZE : "+projectList.size());
				for(ServiceProject project:projectList){
//				for(int j=0;j<projectList.size();j++){
					for(EmployeeInfo empInfo:project.getTechnicians()){
//					for(int k=0;k<project.getTechnicians().size();k++){
						String empName = empInfo.getFullName().trim();
						
						Employee emp = ofy().load().type(Employee.class).filter("companyId", project.getCompanyId()).filter("fullname", empName).first().now();
						
						Double serviceduration = (double) service.getServiceCompleteDuration();
						Double servicecompletioninhours =  serviceduration/60;
						
						double empMonthlySalary =emp.getSalaryAmt();
						double empMonthlyWorkingHrs =emp.getMonthlyWorkingHrs();
						double empHourSalary = empMonthlySalary/empMonthlyWorkingHrs;
						
						Double labourCost = empHourSalary*servicecompletioninhours;
						
						LabourCost labourcost = new LabourCost();
						labourcost.setServiceId(service.getCount());
						labourcost.setEmployeeId(emp.getCount());
						labourcost.setEmployeeName(emp.getFullName());
						labourcost.setEmployeeCell(emp.getCellNumber1()+"");
						labourcost.setServiceHours(decimalformat.format(servicecompletioninhours));
						labourcost.setEmpHourSalary(decimalformat.format(empHourSalary));
						labourcost.setLabourCost(Math.round(labourCost));
						labourcost.setContractId(service.getContractCount());
						labourCostList.add(labourcost);
					}
				}
			}
		}
		return null;
	}


	@Override
	public ArrayList<MaterialConsumptionReport> retriveMaterialDetailsBaseOnBOM(
			long companyId, String conOrCustCount, String typeOfPnL,
			Date fromDate, Date toDate) {
		
		List<Integer> intContractLis= new ArrayList<Integer>();
		ArrayList<Integer> prodCountLis=new ArrayList<Integer>();
		ArrayList<MaterialProductReport> prodReportLis=new ArrayList<MaterialProductReport>();
		ArrayList<MaterialConsumptionReport> consumptionMinReport=new ArrayList<MaterialConsumptionReport>();
		ArrayList<MaterialConsumptionReport> retrunMinReport=new ArrayList<MaterialConsumptionReport>();
		HashSet<Integer> contractId = new HashSet<Integer>();
		HashSet<Integer> quotationId = new HashSet<Integer>();
		if(typeOfPnL.equalsIgnoreCase("Customer")){
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("status", Contract.APPROVED)
					.filter("cinfo.count",Integer.parseInt(conOrCustCount)).list();
				
			if(contractLis!=null){
				// added all contracts in integer list 
				for (Contract contract : contractLis) {
					intContractLis.add(contract.getCount());
					if( contract.getQuotationCount()!=0)
					quotationId.add(contract.getQuotationCount());
				}
			}
				
			
		} else if (typeOfPnL.equalsIgnoreCase("branch")) {
			logger.log(Level.SEVERE, "remat branch labour name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
		/*	List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("status",  Contract.APPROVED)
					.filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
			logger.log(Level.SEVERE,"get con list --"+contractLis1.size());*/
			
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("status",  Contract.APPROVED).
					filter("branch",conOrCustCount)
					.filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
			if(contractLis!=null){
				// added all contracts in integer list 
				for (Contract contract : contractLis) {
					intContractLis.add(contract.getCount());
					if( contract.getQuotationCount()!=0)
						quotationId.add(contract.getQuotationCount());
				}
			}
		}	else if (typeOfPnL.equalsIgnoreCase("segment")) {
			
			logger.log(Level.SEVERE, "segment labour name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
			

			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId)
					.filter("segment",conOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			logger.log(Level.SEVERE,"contractLis -- list -- " + contractLis.size());
			if(contractLis!=null){
				// added all contracts in integer list 
				for (Contract contract : contractLis) {
					intContractLis.add(contract.getCount());
					if( contract.getQuotationCount()!=0)
						quotationId.add(contract.getQuotationCount());
				}
			}
		} else{
		
			intContractLis.add(Integer.parseInt(conOrCustCount));
		}
		
		if(intContractLis.size()==0){
			return retrunMinReport;
		}
		
		/**
		 * this is MIN Loading part  
		 */
		List<MaterialIssueNote> minEntityLis=ofy().load().type(MaterialIssueNote.class)
				.filter("companyId", companyId).filter("minSoId IN", intContractLis).list();
		logger.log(Level.SEVERE,"Size Of Loading MIN List"+minEntityLis.size());
		
		
		
		for (MaterialIssueNote materialIssueNote : minEntityLis) {
			
			if(materialIssueNote.getStatus().trim().equalsIgnoreCase(MaterialIssueNote.APPROVED)){
				
				for (int i = 0; i < materialIssueNote.getSubProductTablemin().size(); i++) {
					
					MaterialConsumptionReport report=new MaterialConsumptionReport();
					report.setContractId(materialIssueNote.getMinSoId());
					if(materialIssueNote.getMinSoId()!=0){
						contractId.add(materialIssueNote.getMinSoId());
					}
					report.setServiceId(materialIssueNote.getServiceId());
					report.setDocID(materialIssueNote.getCount());
					report.setCustId(materialIssueNote.getCinfo().getCount());
					report.setCustName(materialIssueNote.getCinfo().getFullName());
					report.setCustCellNo(materialIssueNote.getCinfo().getCellNumber()+"");
					report.setProdId(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductId());
					report.setProdName(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductName());
					report.setProdCode(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductCode());
					report.setProdUOM(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductUOM());
					report.setQuantity(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
					report.setBranch(materialIssueNote.getBranch());
					/*
					 * added by Ashwini
					 */
					report.setContGroup(materialIssueNote.getGroup());
					report.setContType(materialIssueNote.getType());
					/*
					 * end by Ashwini
					 */
					logger.log(Level.SEVERE,"qTY"+materialIssueNote.getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
					consumptionMinReport.add(report);
					prodCountLis.add(materialIssueNote.getSubProductTablemin().get(i).getMaterialProductId());
				}
			}
		}
		
		logger.log(Level.SEVERE,"Consumption Report List MIN Size=="+consumptionMinReport.size());
		logger.log(Level.SEVERE,"Prod List MIN Size=="+prodCountLis.size());
		List<Integer> uniqueProdIds=new ArrayList<Integer>(new HashSet<Integer>(prodCountLis));
		logger.log(Level.SEVERE,"Unique Prod List MIN Size=="+uniqueProdIds.size());
		
		if(uniqueProdIds.size()==0){
			return retrunMinReport;
		}
		List<SuperProduct>  prodEntity=new ArrayList<SuperProduct>();
		if(uniqueProdIds.size()!=0){
			prodEntity=ofy().load().type(SuperProduct.class).filter("count IN", uniqueProdIds).filter("companyId", companyId).list();
		}
		for(int k=0;k<uniqueProdIds.size();k++)
		{
			MaterialProductReport prodReport=new MaterialProductReport();
			prodReport.setProdId(uniqueProdIds.get(k));
			for (int i = 0; i < prodEntity.size(); i++) {
				if(uniqueProdIds.get(k)==prodEntity.get(i).getCount()){
					prodReport.setProdPrice(prodEntity.get(i).getPrice());
				}
			}
			prodReportLis.add(prodReport);
		}
		List<Contract> conList =null;
		if(contractId.size()>0){
			ArrayList<Integer> contractC = new ArrayList<Integer>();
			contractC.addAll(contractId);
			
			conList = ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("count IN", contractC).list();
			
		}
		/**
		 * nidhi |*)
		 */
		HashMap<Integer, String> getContNetPayable = new HashMap<Integer, String>();
		double	plannedValue =0;
		for(int l=0;l<consumptionMinReport.size();l++)
		{
			MaterialConsumptionReport report=new MaterialConsumptionReport();
			report.setContractId(consumptionMinReport.get(l).getContractId());
			report.setServiceId(consumptionMinReport.get(l).getServiceId());
			report.setDocID(consumptionMinReport.get(l).getDocID());
			report.setDocType("MIN");
			report.setProdId(consumptionMinReport.get(l).getProdId());
			report.setProdName(consumptionMinReport.get(l).getProdName());
			report.setProdCode(consumptionMinReport.get(l).getProdCode());
			report.setProdUOM(consumptionMinReport.get(l).getProdUOM());
			report.setQuantity(consumptionMinReport.get(l).getQuantity());
			report.setBranch(consumptionMinReport.get(l).getBranch());
			
			double prodPriceVal=findProductCount(consumptionMinReport.get(l).getProdId(),prodReportLis);
			logger.log(Level.SEVERE,"Product Price Min"+prodPriceVal);
			logger.log(Level.SEVERE,"Product contract id-- "+report.getContractId());
			report.setPrice(prodPriceVal);
			report.setCustId(consumptionMinReport.get(l).getCustId());
			report.setCustName(consumptionMinReport.get(l).getCustName());
			report.setCustCellNo(consumptionMinReport.get(l).getCustCellNo());
			if(getContNetPayable.containsKey(consumptionMinReport.get(l).getContractId())){
				//getContNetPayable.get(consumptionMinReport.get(l).getContractId())
				
				
				if(getContNetPayable.get(consumptionMinReport.get(l).getContractId()).contains("-")){
					List<String> amt  = Arrays.asList(getContNetPayable.get(consumptionMinReport.get(l).getContractId()).split("-"));
					report.setContAmount(Double.parseDouble(amt.get(0).trim()));
					report.setTargetRevenue(Double.parseDouble(amt.get(1).trim()));
				}else{
					
				}
				
 				
			}else{
				
				if(conList!=null && conList.size()>0){
					for(Contract con : conList){
						if(con.getCount() == consumptionMinReport.get(l).getContractId())
						{
							/*
							 * added by Ashwini
							 */
							
							report.setContGroup(con.getGroup());
							report.setContType(con.getType());
							/*
							 * end by Ashwini
							 */
							report.setContAmount(con.getNetpayable());
							if(typeOfPnL.equalsIgnoreCase("Customer")){
									plannedValue = 	getPlannedPaymentAmt(new Date(),new Date(),con);
								
							}else{
									plannedValue = getPlannedPaymentAmt(DateUtility.getDateWithTimeZone("IST", fromDate),DateUtility.getDateWithTimeZone("IST", toDate),con);
							}
							getContNetPayable.put(con.getCount(), con.getNetpayable() + "-" + plannedValue);
							report.setTargetRevenue(plannedValue);
//							getContNetPayable.put(consumptionMinReport.get(l).getContractId(), con.getNetpayable());
							break;
						}
					}
				}	
			}
			
			retrunMinReport.add(report);
		}
		if(retrunMinReport.size()>0)
		{
			retrunMinReport.get(0).setQuotationId(new ArrayList<Integer>(quotationId));
			
		}
		return retrunMinReport;
	}


	@Override
	public ArrayList<MaterialConsumptionReport> retriveMaterialMMNDetailsBaseOnBOM(
			long companyId, String conOrCustCount, String typeOfPnL,
			Date fromDate, Date toDate) {

		List<String> intContractLis= new ArrayList<String>();
		
		ArrayList<Integer> prodCountLis=new ArrayList<Integer>();
		ArrayList<MaterialProductReport> prodReportLis=new ArrayList<MaterialProductReport>();
		ArrayList<MaterialConsumptionReport> consumptionMmnReport=new ArrayList<MaterialConsumptionReport>();
		ArrayList<MaterialConsumptionReport> retrunMmnReport=new ArrayList<MaterialConsumptionReport>();
		
		if(typeOfPnL.equalsIgnoreCase("Customer")){
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).filter("cinfo.count", Integer.parseInt(conOrCustCount)).list();
			
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
			}
			
		} else if(typeOfPnL.equalsIgnoreCase("branch")){
			logger.log(Level.SEVERE, "Mmn branch name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).filter("branch", conOrCustCount).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
			}
			
		} else if(typeOfPnL.equalsIgnoreCase("segment")){
			logger.log(Level.SEVERE, "Mmn segment name -- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
//			
//			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
//					filter("segment", conOrCustCount).list();
//			
//			// added all contracts in integer list 
//			for (Contract contract : contractLis) {
//				intContractLis.add(contract.getCount()+"");
//			}
			

			logger.log(Level.SEVERE, "Branch name seg-- " + conOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) 
					+" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			
		intContractLis= new ArrayList<String>();
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).filter("segment", conOrCustCount).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).filter("status", "Approved").list();
			// added all contracts in integer list 
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount()+"");
			}
			
		}else{
		
			intContractLis.add(conOrCustCount+"");
		}
		
		if(intContractLis.size()==0){
			return retrunMmnReport;
		}
		
		Logger logger=Logger.getLogger("Consumption Logger");
		logger.log(Level.SEVERE,"Size Of Contract MMN List"+intContractLis.size());
		List<MaterialMovementNote> mmnEntityLis=ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("orderID IN", intContractLis).list();
		logger.log(Level.SEVERE,"Size Of Loading MMN List"+mmnEntityLis.size());
		
		
		
		
		
		for(int i=0;i<mmnEntityLis.size();i++)
		{
			if(mmnEntityLis.get(i).getStatus().trim().equals(MaterialIssueNote.APPROVED))
			{
				for(int j=0;j<mmnEntityLis.get(i).getSubProductTableMmn().size();j++)
				{
					MaterialConsumptionReport report=new MaterialConsumptionReport();
					report.setContractId(Integer.parseInt(mmnEntityLis.get(i).getOrderID()));
					report.setServiceId(mmnEntityLis.get(i).getServiceId());
					report.setDocID(mmnEntityLis.get(i).getCount());
					report.setProdId(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductId());
					report.setProdName(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductName());
					report.setProdCode(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductCode());
					report.setProdUOM(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductUOM());
					report.setQuantity(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity());
					logger.log(Level.SEVERE,"Qty Mmn"+mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity());
					consumptionMmnReport.add(report);
					prodCountLis.add(mmnEntityLis.get(i).getSubProductTableMmn().get(j).getMaterialProductId());
				}
			}
		}
		logger.log(Level.SEVERE,"Consumption Report List MMN Size=="+consumptionMmnReport.size());
		logger.log(Level.SEVERE,"Prod List MMN Size=="+prodCountLis.size());
		List<Integer> uniqueProdIds=new ArrayList<Integer>(new HashSet<Integer>(prodCountLis));
		logger.log(Level.SEVERE,"Unique Prod List MMN Size=="+uniqueProdIds.size());
		List<SuperProduct>  prodEntity = null;
		if(uniqueProdIds.size()!=0){
			prodEntity=ofy().load().type(SuperProduct.class).filter("count IN", uniqueProdIds).filter("companyId", companyId).list();
		}
		
	for(int k=0;k<uniqueProdIds.size();k++)
		{
			MaterialProductReport prodReport=new MaterialProductReport();
			prodReport.setProdId(uniqueProdIds.get(k));
			for (int i = 0; i < prodEntity.size(); i++) {
				if(uniqueProdIds.get(k)==prodEntity.get(i).getCount()){
					prodReport.setProdPrice(prodEntity.get(i).getPrice());
				}
			}
			prodReportLis.add(prodReport);
		}
		
		
		for(int l=0;l<consumptionMmnReport.size();l++)
		{
			MaterialConsumptionReport report=new MaterialConsumptionReport();
			report.setContractId(consumptionMmnReport.get(l).getContractId());
			report.setServiceId(consumptionMmnReport.get(l).getServiceId());
			report.setDocID(consumptionMmnReport.get(l).getDocID());
			report.setDocType("MMN");
			report.setProdId(consumptionMmnReport.get(l).getProdId());
			report.setProdName(consumptionMmnReport.get(l).getProdName());
			report.setProdCode(consumptionMmnReport.get(l).getProdCode());
			report.setProdUOM(consumptionMmnReport.get(l).getProdUOM());
			report.setQuantity(consumptionMmnReport.get(l).getQuantity());
			double prodPriceVal=findProductCount(consumptionMmnReport.get(l).getProdId(),prodReportLis);
			logger.log(Level.SEVERE,"Product Price MMN"+prodPriceVal);
			report.setPrice(prodPriceVal);
			retrunMmnReport.add(report);
		}
		return retrunMmnReport;
		
	}


	@Override
	public ArrayList<String> retrieveAdministrativeCostBaseOnBOM(
			long companyId, String contractOrCustCount, String type,
			Date fromDate, Date toDate) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside the impl labour cost , TYPE-"+type+" CONTRACT/CUSTOMER ID-"+contractOrCustCount);

		ArrayList<LabourCost> labourCostList = new ArrayList<LabourCost>();	
		List<Service> serviceList = new ArrayList<Service>();
		
		if (type.equalsIgnoreCase("Contract")) {
			
			serviceList = ofy().load().type(Service.class)
					.filter("companyId", companyId).filter("contractCount",
							Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
		
		}else if (type.equalsIgnoreCase("branch")) {
			logger.log(Level.SEVERE, "Branch name labour -- " + contractOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) 
					+" \n to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));

			List<Integer> intContractLis= new ArrayList<Integer>();
			
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("branch", contractOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
			}
			
			logger.log(Level.SEVERE,"Branch contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
			logger.log(Level.SEVERE,"segment contract -- " + serviceList.size() +" branch no == " +contractOrCustCount);
			
		
		}else if (type.equalsIgnoreCase("segment")) {
			
			List<Integer> intContractLis= new ArrayList<Integer>();
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			
			logger.log(Level.SEVERE, "segment labour name -- " + contractOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("segment", contractOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
			}
			
			logger.log(Level.SEVERE,"segment contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
		
		}   else{
			
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
		
		}

		logger.log(Level.SEVERE,"SERVICE LIST SIZE : "+serviceList.size());

		if(serviceList.size()!=0){
			
			for(Service service:serviceList){
//			for(int i=0;i<serviceList.size();i++){
				List<ServiceProject> projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", service.getContractCount()).filter("serviceId", service.getCount()).list();
				logger.log(Level.SEVERE,"PROJECT LIST SIZE : "+projectList.size());
				for(ServiceProject project:projectList){
//				for(int j=0;j<projectList.size();j++){
					for(EmployeeInfo empInfo:project.getTechnicians()){
//					for(int k=0;k<project.getTechnicians().size();k++){
						String empName = empInfo.getFullName().trim();
						
						Employee emp = ofy().load().type(Employee.class).filter("companyId", project.getCompanyId()).filter("fullname", empName).first().now();
						
						Double serviceduration = (double) service.getServiceCompleteDuration();
						Double servicecompletioninhours =  serviceduration/60;
						
						double empMonthlySalary =emp.getSalaryAmt();
						double empMonthlyWorkingHrs =emp.getMonthlyWorkingHrs();
						double empHourSalary = empMonthlySalary/empMonthlyWorkingHrs;
						
						Double labourCost = empHourSalary*servicecompletioninhours;
						
						LabourCost labourcost = new LabourCost();
						labourcost.setServiceId(service.getCount());
						labourcost.setEmployeeId(emp.getCount());
						labourcost.setEmployeeName(emp.getFullName());
						labourcost.setEmployeeCell(emp.getCellNumber1()+"");
						labourcost.setServiceHours(decimalformat.format(servicecompletioninhours));
						labourcost.setEmpHourSalary(decimalformat.format(empHourSalary));
						labourcost.setLabourCost(Math.round(labourCost));
						labourcost.setContractId(service.getContractCount());
						labourCostList.add(labourcost);
					}
				}
			}
		}
		return null;
	}


	/*@Override
	public ArrayList<LabourCost> retrieveAdministrativeCost(long companyId,String contractOrCustCount, String type, Date fromDate, Date toDate) {

		logger.log(Level.SEVERE,"Inside the impl labour cost , TYPE-"+type+" CONTRACT/CUSTOMER ID-"+contractOrCustCount);

		ArrayList<LabourCost> labourCostList = new ArrayList<LabourCost>();	
		List<Service> serviceList = new ArrayList<Service>();
		
		if (type.equalsIgnoreCase("Contract")) {
			
			serviceList = ofy().load().type(Service.class)
					.filter("companyId", companyId).filter("contractCount",
							Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
		
		}else if (type.equalsIgnoreCase("branch")) {
			logger.log(Level.SEVERE, "Branch name labour -- " + contractOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) 
					+" \n to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));

			List<Integer> intContractLis= new ArrayList<Integer>();
			
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("branch", contractOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
			}
			
			logger.log(Level.SEVERE,"Branch contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
			logger.log(Level.SEVERE,"segment contract -- " + serviceList.size() +" branch no == " +contractOrCustCount);
			
		
		}else if (type.equalsIgnoreCase("segment")) {
			
			List<Integer> intContractLis= new ArrayList<Integer>();
			List<Contract> contractLis1=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- for branch " + contractLis1.size());
			
			logger.log(Level.SEVERE, "segment labour name -- " + contractOrCustCount + "  today --  " 
					+DateUtility.getDateWithTimeZone("IST", fromDate) +" to date -- "+DateUtility.getDateWithTimeZone("IST", toDate));
			List<Contract> contractLis=ofy().load().type(Contract.class).filter("companyId", companyId).
					filter("segment", contractOrCustCount).filter("status", Contract.APPROVED).
					filter("endDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("endDate <=", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			// added all contracts in integer list 
			logger.log(Level.SEVERE,"contract -- " + contractLis.size());
			for (Contract contract : contractLis) {
				intContractLis.add(contract.getCount());
			}
			
			logger.log(Level.SEVERE,"segment contract -- " + contractLis.size() +" intContractLis == " +intContractLis.size());
			if(intContractLis.size()>0){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("contractCount IN", intContractLis).filter("status", "Completed").list();
			}
			
		
		}   else{
			
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", Integer.parseInt(contractOrCustCount)).filter("status", "Completed").list();
		
		}

		logger.log(Level.SEVERE,"SERVICE LIST SIZE : "+serviceList.size());

		if(serviceList.size()!=0){
			
			for(Service service:serviceList){
//			for(int i=0;i<serviceList.size();i++){
				List<ServiceProject> projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractId", service.getContractCount()).filter("serviceId", service.getCount()).list();
				logger.log(Level.SEVERE,"PROJECT LIST SIZE : "+projectList.size());
				for(ServiceProject project:projectList){
//				for(int j=0;j<projectList.size();j++){
					for(EmployeeInfo empInfo:project.getTechnicians()){
//					for(int k=0;k<project.getTechnicians().size();k++){
						String empName = empInfo.getFullName().trim();
						
						Employee emp = ofy().load().type(Employee.class).filter("companyId", project.getCompanyId()).filter("fullname", empName).first().now();
						
						Double serviceduration = (double) service.getServiceCompleteDuration();
						Double servicecompletioninhours =  serviceduration/60;
						
						double empMonthlySalary =emp.getSalaryAmt();
						double empMonthlyWorkingHrs =emp.getMonthlyWorkingHrs();
						double empHourSalary = empMonthlySalary/empMonthlyWorkingHrs;
						
						Double labourCost = empHourSalary*servicecompletioninhours;
						
						LabourCost labourcost = new LabourCost();
						labourcost.setServiceId(service.getCount());
						labourcost.setEmployeeId(emp.getCount());
						labourcost.setEmployeeName(emp.getFullName());
						labourcost.setEmployeeCell(emp.getCellNumber1()+"");
						labourcost.setServiceHours(decimalformat.format(servicecompletioninhours));
						labourcost.setEmpHourSalary(decimalformat.format(empHourSalary));
						labourcost.setLabourCost(Math.round(labourCost));
						
						labourCostList.add(labourcost);
					}
				}
			}
		}
		return labourCostList;
	}*/

	
	double getPlannedPaymentAmt(Date startDate,Date endDate, Contract con){
		
		Logger billingLogger=Logger.getLogger("Billing Logger");
		billingLogger.log(Level.SEVERE,"BIlling Process Started for Creation");
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
		
		// below count variable is added by vijay for billing period
	    int count=0;
	    
	    double totalPlannAmt = 0;
	    
		for(int i=0;i<con.getPaymentTermsList().size();i++)
		{
			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			PaymentTerms paymentTerms=new PaymentTerms();
			Date conStartDate=null;
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			System.out.println("Contract Start Date"+conStartDate);
			
	/***********************Invoice Date******************************/
			Calendar calInvoiceDate = Calendar.getInstance();
			if(con.getContractDate()!=null){
				Date serConDate=DateUtility.getDateWithTimeZone("IST", con.getContractDate());
				calInvoiceDate.setTime(serConDate);
			}
			else{
				conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
				calInvoiceDate.setTime(conStartDate);
			}
			salesProdLis=con.retrieveSalesProducts(con.getPaymentTermsList().get(i).getPayTermPercent());
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			calInvoiceDate.add(Calendar.DATE, con.getPaymentTermsList().get(i).getPayTermDays());
			Date calculatedInvoiceDate=null;
			try {
				calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
				calInvoiceDate.set(Calendar.MINUTE,59);
				calInvoiceDate.set(Calendar.SECOND,59);
				calInvoiceDate.set(Calendar.MILLISECOND,999);
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
				if(currentDate.after(calculatedInvoiceDate))
				{
					calculatedInvoiceDate=currentDate;
				}
				
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			Date calBillingDate=null;
			Calendar calBilling=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
			
			calBilling.add(Calendar.DATE, -2);
			try {
				calBilling.set(Calendar.HOUR_OF_DAY,23);
				calBilling.set(Calendar.MINUTE,59);
				calBilling.set(Calendar.SECOND,59);
				calBilling.set(Calendar.MILLISECOND,999);
				calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
				
				if(currentDate.after(calBillingDate)){
					calBillingDate=currentDate;
				}
				
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***************************************************************************************/
			
			/********************************Payment Date******************************************/
			
			Date calPaymentDate=null;
			int creditVal=0;
			if(con.getCreditPeriod()!=null){
				creditVal=con.getCreditPeriod();
			}
			
			Calendar calPayment=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calPayment.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate));
			}
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
			
			calPayment.add(Calendar.DATE, creditVal);
			try {
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				calPayment.set(Calendar.HOUR_OF_DAY,23);
				calPayment.set(Calendar.MINUTE,59);
				calPayment.set(Calendar.SECOND,59);
				calPayment.set(Calendar.MILLISECOND,999);
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			if(con.getNetpayable()!=0)
				billingDocEntity.setTotalSalesAmount(con.getNetpayable());
			if(con.getPaymentTermsList().get(i).getPaymentDate()!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", con.getPaymentTermsList().get(i).getPaymentDate()));
				billingLogger.log(Level.SEVERE,"Bill-Payment Date Not Null : "+billingDocEntity.getBillingDate());
			
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", con.getPaymentTermsList().get(i).getPaymentDate()));
				billingLogger.log(Level.SEVERE,"Invoice-Payment Date Not Null : "+billingDocEntity.getInvoiceDate());
				
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", con.getPaymentTermsList().get(i).getPaymentDate()));
				billingLogger.log(Level.SEVERE,"Payment-Payment Date Not Null : "+billingDocEntity.getPaymentDate());
			
			}else{
				if(calBillingDate!=null){
					billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
					billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
				}
				
				if(calculatedInvoiceDate!=null){
					billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
					billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
				}
				if(calPaymentDate!=null){
					billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
					billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
				}
			}
			
			
			ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
			if(con.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				billTaxLis=con.listForBillingTaxes(con.getPaymentTermsList().get(i).getPayTermPercent());
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
				billTaxArrROHAN.addAll(billTaxLis);
			}

			
	//***********************changes made by rohan for saving gross value *************	
			double grossValue=con.getTotalAmount();
			
			
			billingDocEntity.setGrossValue(grossValue);
			
	//********************************changes ends here *******************************		
			
			
			double totBillAmt=con.getTotalBillAmt(salesProdLis);
			
			double taxAmt= con.getTotalFromTaxTable(billTaxArrROHAN);
			
			double totalBillingAmount = totBillAmt+taxAmt;
			billingDocEntity.setTotalBillingAmount(totalBillingAmount);
			
			
			int payTrmsDays=con.getPaymentTermsList().get(i).getPayTermDays();
			double payTrmsPercent=con.getPaymentTermsList().get(i).getPayTermPercent();
			String payTrmsComment=con.getPaymentTermsList().get(i).getPayTermComment().trim();
			
			
			paymentTerms.setPayTermDays(payTrmsDays);
			paymentTerms.setPayTermPercent(payTrmsPercent);
			paymentTerms.setPayTermComment(payTrmsComment);
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			
			count++;
			
			
			/** Date 03-09-2017 added by vijay for total amount before taxes**/
			billingDocEntity.setTotalAmount(totBillAmt);
			/** Date 05-09-2017 added by vijay for Discount amount**/
			double discountamt = 0;
			if(con.getDiscountAmt()!=0){
				discountamt = con.getDiscountAmt()/con.getPaymentTermsList().size();
				billingDocEntity.setDiscountAmt(discountamt);
			}
			/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
			double totalfinalAmt=totBillAmt-discountamt;
			billingDocEntity.setFinalTotalAmt(totalfinalAmt);
			 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
			double totalamtincludingtax=totalfinalAmt+taxAmt;
			billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
			 /** Date 05-09-2017 added by vijay for roundoff amount **/
			double roundoff=0;
			if(con.getRoundOffAmt()!=0){
				roundoff=con.getRoundOffAmt()/con.getPaymentTermsList().size();
				billingDocEntity.setRoundOffAmt(roundoff);
			}
			
			/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
			billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
			
			/*** Date 10-10-2017 added by vijay for total billing amount *********/
			billingDocEntity.setTotalBillingAmount(totalamtincludingtax+roundoff);
			
			if(billingDocEntity.getBillingDate().before(endDate)|| billingDocEntity.getBillingDate().equals(endDate)){
				totalPlannAmt = totalPlannAmt+billingDocEntity.getTotalBillingAmount();
			}else{
				break;
			}
		}
		
		logger.log(Level.SEVERE,"get total amt 0--- " + totalPlannAmt + " contract count -- " + con.getCount());
		
		return totalPlannAmt;
	}


	@Override
	public ArrayList<MaterialConsumptionReport> retriveServicePlannedRevanueAndBillingRevanue(
			ArrayList<MaterialConsumptionReport> ServiceContractDetails,
			HashSet<Integer> contract, HashSet<Integer> serSet,
			HashMap<Integer, HashSet<Integer>> contSer,long companyID) {
		HashMap<Integer, Double> billingAmt = new HashMap<Integer, Double>();
		List<Contract> conList = new ArrayList<Contract>();
		List<Quotation> quoListDt = new ArrayList<Quotation>();
		if(contract!=null && contract.size()>0){
			
			ArrayList<Integer> contList = new ArrayList<Integer>();
			for(Integer con : contract){
				contList.add(con);
			}
			
			
			List<BillingDocument> conDtList = ofy().load().type(BillingDocument.class).filter("companyId", companyID)
					.filter("contractCount IN", contList).filter("status", BillingDocument.BILLINGINVOICED).list();
			
			
			double billTotal = 0;
			if(conDtList!=null && conDtList.size()>0){
					for(BillingDocument billDt : conDtList){
						if(billingAmt.containsKey(billDt.getContractCount())){
							billTotal = billDt.getTotalBillingAmount() + billingAmt.get(billDt.getContractCount());
							billingAmt.put(billDt.getContractCount(), billTotal);
						}else{
							billingAmt.put(billDt.getContractCount(), billDt.getTotalBillingAmount());
						}
					}
				
			}
			
			if(serSet !=null && serSet.size()>0){
				ArrayList<Integer> quoList = new ArrayList<Integer>();

				for(Integer quo : serSet){
					quoList.add(quo);
				}
				
				conList = 	ofy().load().type(Contract.class).filter("companyId", companyID)
						.filter("quotationCount IN", quoList).filter("status", Contract.APPROVED).list();
				
				quoListDt = ofy().load().type(Quotation.class).filter("companyId", companyID)
				.filter("count IN", quoList).list();
			}
			
		
		}
		
		for(Integer cnt : billingAmt.keySet()){
			logger.log(Level.SEVERE,"get count - "+ cnt + " bill amt -- " + billingAmt.get(cnt));
		}
		MaterialConsumptionReport materilDt = new MaterialConsumptionReport();
		materilDt.setBillingAmtDt(billingAmt);
		materilDt.getConList().addAll(conList);
		materilDt.getQuoListDt().addAll(quoListDt);
		materilDt.getQuotationId().addAll(serSet);
		materilDt.getContDtList().addAll(contract);
		ArrayList<MaterialConsumptionReport> reportArr = new ArrayList<MaterialConsumptionReport>();
		reportArr.add(materilDt);
		
		
		return reportArr;
		
	}


	@Override
	public ArrayList<MaterialConsumptionReport> retrivelabourPlannedRevanueAndBillingRevanue(
			ArrayList<MaterialConsumptionReport> SerContDetails,
			HashSet<Integer> contract, HashSet<Integer> serSet,
			HashMap<Integer, HashSet<Integer>> contSer, long companyID) {
		
		if(SerContDetails.get(0).getQuoListDt()==null || SerContDetails.get(0).getQuoListDt().size()==0) {
			return null;
		}
		ArrayList<MaterialConsumptionReport> matDt = new ArrayList<MaterialConsumptionReport>();
		
		List<Service> serDt = ofy().load().type(Service.class).filter("companyId", companyID)
				.filter("contractCount IN", SerContDetails.get(0).getContDtList())
				.filter("status", Service.SERVICESTATUSCOMPLETED).list();
		
		
		HashMap<Integer, Integer> serCount = new HashMap<Integer, Integer>(); // contract count for completed service
		HashMap<String, Integer> serNameCount = new HashMap<String, Integer>(); // total service as per product
	
		if(serDt.size()>0){
			int cnt =0;
			for(Service ser : serDt){
				if(serCount.containsKey(ser.getContractCount())){
					cnt = serCount.get(ser.getContractCount())+1;
					serCount.put(ser.getContractCount(), cnt);
					
					serNameCount.put(ser.getContractCount()+"-"+ser.getProductName(), cnt);
				}else if(serNameCount.containsKey(ser.getContractCount()+"-"+ser.getProductName())){

					cnt = serNameCount.get(ser.getContractCount()+"-"+ser.getProductName())+1;
					
					serNameCount.put(ser.getContractCount()+"-"+ser.getProductName(), cnt);
				
				}else{
					serCount.put(ser.getContractCount(), 1);
					serNameCount.put(ser.getContractCount()+"-"+ser.getProductName(), 1);
				}
			}
			
		}

		HashMap<String, Integer> serNameDt  = new HashMap<String, Integer>();
		HashMap<String, Double> serCost = new HashMap<String, Double>();
		HashMap<Integer, Double> contPlannedCost = new HashMap<Integer, Double>();
 		if(SerContDetails.get(0).getQuoListDt()!=null && SerContDetails.get(0).getQuoListDt().size()>0){
			for(Integer serName : serCount.keySet()){
				for(int j =0 ;j<SerContDetails.get(0).getQuoListDt().size();j++){
					if(SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList()==null 
							|| SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().size()==0){
						continue;
					}
					serNameDt  = new HashMap<String, Integer>();
					serCost = new HashMap<String, Double>();
					
					if(SerContDetails.get(0).getQuoListDt().get(j).getContractCount() == serName){
						int numberofSer = 0;
						double costTotal = 0.00 , finalCost= 0.0;
						for(int i=0;i<SerContDetails.get(0).getQuoListDt().get(j).getItems().size();i++){
		
							if(serNameCount.containsKey(serName+"-"
									+SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getProductName())){
							if(serNameDt.containsKey(SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getProductName())){
								numberofSer = SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getNumberOfServices();

								numberofSer  =  numberofSer + serNameDt.get(SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getProductName());
								
								serNameDt.put(SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getProductName(), numberofSer);
							}else{
								numberofSer = SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getNumberOfServices();
								serNameDt.put(SerContDetails.get(0).getQuoListDt().get(j).getItems().get(i).getProductName(), numberofSer);
							}
							}
						}
						
						
						for(int jj=0;jj<SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().size();jj++){
							if(serCost.containsKey(SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().get(jj).getServiceName())){
								costTotal = serCost.get(SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().get(jj).getServiceName()) 
										+ SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().get(jj).getTotalCost();
								serCost.put(SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().get(jj).getServiceName(),
										costTotal);
							}else{
								
								serCost.put(SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().get(jj).getServiceName(), 
										SerContDetails.get(0).getQuoListDt().get(j).getManPowCostList().get(jj).getTotalCost());
							}
						}
						
						logger.log(Level.SEVERE,"get sercost --- " + serCost.keySet().toString() );
						
						if(contPlannedCost.containsKey(serName)){
							logger.log(Level.SEVERE,"get contract cost -- again " + serName);
						}else{
							
							Double cost = 0.0;
							
							for(String serProdDt : serCost.keySet()){
								cost = cost + (serCost.get(serProdDt) / serNameDt.get(serProdDt) );
								logger.log(Level.SEVERE,"get service cost0  --  "+ cost + " servvice Name 00 "+ serProdDt );
							}
							
							
							contPlannedCost.put(serName, cost);
						}
						
						
					}
				}
			}
		
		}
		
		matDt.get(0).setPlannedQuoCost(contPlannedCost);
		return matDt;
	}

	/** DATE 8.4.2019 added by komal for new contract p and l **/
	@Override
	public ArrayList<MaterialConsumptionReport> retrieveServiceDetails(long companyId,String contractid,String type, Date fromDate, Date toDate ,ArrayList<String> emailList) {
		
		System.out.println("contract id "+contractid+"==type"+type);
		ArrayList<MaterialConsumptionReport>  materialConsumptionReportList = null;
        if(emailList != null && emailList.size() > 0){	
        	emailIdList.clear();
        	emailIdList.addAll(emailList);
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue"); 
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
						.param("OperationName", "BOMReport")
						.param("companyId",companyId+"").param("contractId",contractid+"")
						.param("type",type).param("fromDate",fromDate+"").param("toDate",toDate+""));
				
        }else{
        	materialConsumptionReportList = retrieveData(companyId,contractid,type,fromDate,toDate);
        }
        return materialConsumptionReportList;
	}
	public ArrayList<MaterialConsumptionReport> retrieveData(long companyId,String contractid,String type, Date fromDate, Date toDate){
		
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		System.out.println("contract id "+contractid+"==type"+type);
		
		List<Service> serviceList= new ArrayList<Service>();
		ArrayList<Integer> serviceIdList= new ArrayList<Integer>();
		
		Set<Integer> contractIdSet= new HashSet<Integer>();
		List<ServiceProject> serviceProjectList= new ArrayList<ServiceProject>();
		Map<Integer , ServiceProject> serviceProjectMap = new HashMap<Integer , ServiceProject>();
		ArrayList<MaterialConsumptionReport> materialConsumptionReportList = new ArrayList<MaterialConsumptionReport>();
		Map<Integer , MaterialConsumptionReport> materialConsumptionReportMap = new HashMap<Integer , MaterialConsumptionReport>();
		Map<Integer , ArrayList<Service>> contractWiseServiceMap = new HashMap<Integer , ArrayList<Service>>();
		Map<String ,Double> empHourlyRateMap = new HashMap<String , Double>();
		Set<String> employeeSet= new HashSet<String>();
		Set<Integer> productIdSet= new HashSet<Integer>();
		Map<Integer ,Double> productMap = new HashMap<Integer , Double>();
		
		if (type.equalsIgnoreCase("Contract")) {
			
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).
					filter("contractCount", Integer.parseInt(contractid)).list();
		
		}else if (type.equalsIgnoreCase("branch")) {
			DateUtility.addDaysToDate(toDate, 1);
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("branch", contractid).filter("serviceDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
						filter("serviceDate <", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			
		}else if(type.equalsIgnoreCase("Date")){
			DateUtility.addDaysToDate(toDate, 1);
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
					filter("serviceDate <", DateUtility.getDateWithTimeZone("IST", toDate)).list();
		}
		else{
			if(fromDate == null && toDate == null){
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("personInfo.count", Integer.parseInt(contractid)).list();
			}else{
				DateUtility.addDaysToDate(toDate, 1);
				serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("personInfo.count", Integer.parseInt(contractid)).filter("serviceDate >=", DateUtility.getDateWithTimeZone("IST", fromDate)).
						filter("serviceDate <", DateUtility.getDateWithTimeZone("IST", toDate)).list();
			}
		
		}
		
		logger.log(Level.SEVERE," :::servicelist size :::  "+serviceList.size());
		ArrayList<Service> serList = null;
		for(Service service : serviceList){
			serviceIdList.add(service.getCount());
			contractIdSet.add(service.getContractCount());
			employeeSet.add(service.getEmployee());
			if(contractWiseServiceMap.containsKey(service.getContractCount())){
				serList = contractWiseServiceMap.get(service.getContractCount());
				serList.add(service);
				contractWiseServiceMap.put(service.getContractCount(), serList);
			}else{
				serList = new ArrayList<Service>();
				serList.add(service);
				contractWiseServiceMap.put(service.getContractCount(), serList);
			}
		}
		if(serviceIdList.size() > 0){
			serviceProjectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("serviceId IN", serviceIdList).list();
		}
		if(serviceProjectList!= null && serviceProjectList.size() >0){
			for(ServiceProject serProject : serviceProjectList){
				for(EmployeeInfo info : serProject.getTechnicians()){
					employeeSet.add(info.getFullName());
				}
				for(ProductGroupList list : serProject.getProdDetailsList()){
					productIdSet.add(list.getProduct_id());
				}
				serviceProjectMap.put(serProject.getserviceId(), serProject);
			}
		}
		
		if(productIdSet.size() > 0){
			ArrayList<Integer> productIdList= new ArrayList<Integer>(productIdSet);
			List<SuperProduct> productList  = ofy().load().type(SuperProduct.class).filter("companyId", companyId)
					.filter("count IN",productIdList).list();
			for(SuperProduct product : productList){
				if(product.getPurchasePrice() != 0){
					productMap.put(product.getCount(), product.getPurchasePrice());
				}else{
					productMap.put(product.getCount(), product.getPrice());
				}
			}
		}
		
		List<Integer> contractIdList= new ArrayList<Integer>(contractIdSet);
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		List<Quotation> quotationList = ofy().load().type(Quotation.class).filter("companyId", companyId)
					.filter("contractCount IN",contractIdList).list();
		Map<Integer,Quotation> contractQuotationMap = new HashMap<Integer,Quotation>();
		for(Quotation quot : quotationList){
			contractQuotationMap.put(quot.getContractCount(), quot);
		}
 		//getManPowCostList();
		List<Employee> empList = new ArrayList<Employee>();
		if(employeeSet.size() >0){
			ArrayList<String> employeeNameList= new ArrayList<String>();
			employeeNameList.addAll(employeeSet);
 			empList = ofy().load().type(Employee.class).filter("companyId",companyId).filter("fullname IN", employeeNameList).list();
		}
		logger.log(Level.SEVERE , "employee list :" + empList.size());
 		for(Employee emp : empList){
			double empMonthlySalary =emp.getSalaryAmt();
			double empMonthlyWorkingHrs =emp.getMonthlyWorkingHrs();
			double empHourSalary = empMonthlySalary/empMonthlyWorkingHrs;
			empHourlyRateMap.put(emp.getFullName() , empHourSalary);
		}
		
		 for(Map.Entry<Integer, ArrayList<Service>> map:contractWiseServiceMap.entrySet()){
			 ArrayList<Service> allServiceList = map.getValue();
			 MaterialConsumptionReport materialConsumptionReport = new MaterialConsumptionReport();
			 		 
			 Service ser = allServiceList.get(0);
			 if(ser.getPersonInfo() != null){
				 materialConsumptionReport.setCustId(ser.getPersonInfo().getCount()); // customer id
			 } 
			 if(ser.getPersonInfo() != null && ser.getPersonInfo().getFullName() != null){
				 materialConsumptionReport.setCustName(ser.getPersonInfo().getFullName()); //customer name
			 }else{
				 materialConsumptionReport.setCustName("");
			 }//
			 materialConsumptionReport.setContractId(ser.getContractCount()); // order id
			 
			 if(ser.getContractGroup() != null){
				 materialConsumptionReport.setContGroup(ser.getContractGroup());   // contract group
			 }else{
				 materialConsumptionReport.setContGroup("");  
			 }
			 if(ser.getContractType() != null){
				 materialConsumptionReport.setContType(ser.getContractType());     // contract type
			 }else{
				 materialConsumptionReport.setContType("");
			 }
			 if(ser.getBranch() != null){
				 materialConsumptionReport.setBranch(ser.getBranch());           //branch
			 }else{
				 materialConsumptionReport.setBranch("");
			 }
			 if(ser.getRefNo() != null){
				 materialConsumptionReport.setReferenceNumber(ser.getRefNo());
			 }else{
				 materialConsumptionReport.setReferenceNumber("");
			 }
			materialConsumptionReport.setContAmount(ser.getContractAmount());  // contract amount
			double totalTargetRevenue = 0;
			double totalServiceValue = 0;
			double totalMaterialCostPlanned = 0;
			double totalMaterialCostActual = 0;
			double totalOperatorCostPlanned = 0;
			double totalOperatorCostActual = 0;
			List<String> employeeList = null;
			for(Service service : allServiceList){	
				totalTargetRevenue += service.getServiceValue();
				if(service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)){
					totalServiceValue += service.getServiceValue();
				}
				if(service.getServiceProductList() != null){
					for(ServiceProductGroupList groupList :service.getServiceProductList()){
						totalMaterialCostPlanned += (groupList.getPrice());
					}
				}
				if(serviceProjectMap.containsKey(service.getCount())){
					ServiceProject proj  = serviceProjectMap.get(service.getCount());
					for(ProductGroupList group : proj.getProdDetailsList()){
						if(group.getReturnQuantity()!=0){
							totalMaterialCostActual += (group.getReturnQuantity() * productMap.get(group.getProduct_id()));
						}else{
							totalMaterialCostActual += (group.getQuantity() *  productMap.get(group.getProduct_id()));
						}
						
					}
					for(EmployeeInfo info : proj.getTechnicians()){
						double salary =0 ;
						if(empHourlyRateMap.containsKey(info.getFullName())){
							salary = empHourlyRateMap.get(info.getFullName());
						}
						
						totalOperatorCostActual += (salary * (service.getServiceCompleteDuration()/60.0));
						if(service.getServicingTime() != null){
							totalOperatorCostPlanned += (salary * service.getServicingTime());
						}else{
							totalOperatorCostPlanned += 0;
						}
						
					}
					
				}
				if(contractQuotationMap.containsKey(ser.getContractCount())){
					Quotation quot = contractQuotationMap.get(ser.getContractCount());
					if(quot.getManPowCostList().size() >0){
						//totalOperatorCostPlanned = 0;
						for(ManPowerCostStructure manPower : quot.getManPowCostList()){
							if(service.getProduct().getProductName().equalsIgnoreCase(manPower.getServiceName())){
								totalOperatorCostPlanned += manPower.getTotalCost();
							}
						}
						//totalOperatorCostPlanned = totalOperatorCostPlanned;
					}
				}
			}
			
			materialConsumptionReport.setTargetRevenue(totalTargetRevenue);
			materialConsumptionReport.setTotal(totalServiceValue);
			materialConsumptionReport.setPlannedLabourCost(totalOperatorCostPlanned);
			materialConsumptionReport.setPlannedminCost(totalMaterialCostPlanned);
			materialConsumptionReport.setActualLabourCost(totalOperatorCostActual);
			materialConsumptionReport.setActualMinCost(totalMaterialCostActual);
			materialConsumptionReportList.add(materialConsumptionReport);
		}
		
		return materialConsumptionReportList;
	
	}
	
}
