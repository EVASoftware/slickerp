package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.services.AutoInvoiceService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

/**
 * 
 * @author Vijay Chougule
 * Des :- NBHC CCPM Double invoices was generated so created new RPC for NBHC CCPM
 * 
 * Note :- Please do not add any other Operation in the RPC
 *
 */
public class AutoInvoiceServiceImpl extends RemoteServiceServlet implements AutoInvoiceService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7378211618454994802L;
	
	Logger logger = Logger.getLogger("Name of logger");


	@Override
	public String createTaxInvoice(BillingDocument model,Invoice invoice, String loggedInUser,boolean autoinvoiceflag, ArrayList<Integer> billIdList, ArrayList<Integer> billOrderIdList) {
		
		logger.log(Level.SEVERE,"Inside Invoice creation method "+DateUtility.getDateWithTimeZone("IST", new Date()));
		String response="";
		if(model.getInvoiceCount()!=0&&model.getStatus().equals("Invoiced")){
			response="Invoice already created.";
			return response;
		}
		
		response  = createNewInvoice(model,invoice,loggedInUser,autoinvoiceflag,billIdList,billOrderIdList);
		logger.log(Level.SEVERE,"RESPONSE : "+response);
		return response;
			
	}
	
	private String createNewInvoice(BillingDocument model, Invoice inventity, String userName, boolean autoinvoiceflag, ArrayList<Integer> billIdList,
			ArrayList<Integer> billOrderIdList) {

		logger.log(Level.SEVERE, "Auto Invoice creating invoice");
		ReturnFromServer object=null;
		try{
			GenricServiceImpl impl=new GenricServiceImpl();
			object=impl.save(inventity);
			logger.log(Level.SEVERE," Invoice Id : "+object.count);
			logger.log(Level.SEVERE, "Auto Invoice Created");
		}catch(Exception e){
			/**
			 * @author niles , Date : 31-03-2020
			 * Added logger to identify
			 */
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception "+e.getMessage()+" - "+e.toString());
			return "Invoice creation failed. Please contact support.";
		}

		try{
			
			ArrayList<Integer> bilIdList=new ArrayList<Integer>();
			ArrayList<Integer> bilOrderIdList=new ArrayList<Integer>();
			for(int i=0;i<model.getArrayBillingDocument().size();i++){
				bilIdList.add(model.getArrayBillingDocument().get(i).getBillId());
				bilOrderIdList.add(model.getArrayBillingDocument().get(i).getOrderId());
			}
			logger.log(Level.SEVERE, "billIdList =="+billIdList);
			logger.log(Level.SEVERE, "billOrderIdList=="+billOrderIdList);

			List<BillingDocument> billingList = ofy().load().type(BillingDocument.class)
					.filter("companyId", model.getCompanyId()).filter("contractCount IN", bilOrderIdList)
					.filter("count IN", bilIdList).filter("typeOfOrder", model.getTypeOfOrder()).list();
			
//			List<BillingDocument> billingList = ofy().load().type(BillingDocument.class)
//					.filter("companyId", model.getCompanyId()).filter("contractCount IN", billOrderIdList)
//					.filter("count IN", billIdList).filter("typeOfOrder", model.getTypeOfOrder()).list();
			
			logger.log(Level.SEVERE, "billingList size"+billingList.size());
	
			if (billingList != null && billIdList.size() != 0) {
				logger.log(Level.SEVERE, "Billing List Size : " + billingList.size());
				for (BillingDocument bill : billingList) {
					// for(Integer orderId:billOrderIdList){
					// if(bill.getContractCount()==orderId){
					bill.setStatus(BillingDocument.BILLINGINVOICED);
					bill.setInvoiceCount(object.count);
					// }
					// }
				}
				ofy().save().entities(billingList).now();
				logger.log(Level.SEVERE, "Billing Details Saved sucessfully");
			}
		}catch(Exception e){
			return "Billing update failed. Please contact support.";
		}
		
		/**
		 * @author Anil , Date :
		 */
		try{
			DocumentUploadServiceImpl doc=new DocumentUploadServiceImpl();
			String response=doc.sendApprovalRequest(inventity, userName, object.count);
			
			if(response.equals("success")){
				return object.count+"-"+response;
			}else{
				return response;
			}
			
		}catch(Exception e){
			return "Unable to send approval requests. Please contact support.";
		}
		
//		return object.count+"-";
	}


	@Override
	public String updateServiceBranchinServies(int contractId, long companyId) {
		
		try {
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "UpdateServicingBranchInServices").param("companyId", companyId+"").param("contractId", contractId+""));
			
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception payment Date follow up update =="+e.getMessage());
			return "Failed";
		}
		
		return "ServiceBranch Updation Process Started";
	}

}
