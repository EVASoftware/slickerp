package com.slicktechnologies.server.democonfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.DemoConfigService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.server.CompanyRegistrationServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.taskqueue.DocumentTaskQueue;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.ScreenName;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.config.configurations.ConfigTypes;

public class DemoConfigurationImpl  extends RemoteServiceServlet implements DemoConfigService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6368104386069712551L;
Logger logger=Logger.getLogger("Name Of logger");
	
/**
 * Date : 01-07-2017 BY ANIL
 */

public HashMap<String,String> stateMap=new HashMap<String,String>();

public DemoConfigurationImpl() {
	stateMap.put("Jammu and Kashmir","01");
	stateMap.put("Himachal Pradesh","02");
	stateMap.put("Punjab","03");
	stateMap.put("Chandigarh","04");
	stateMap.put("Uttarakhand","05");
	stateMap.put("Haryana","06");
	stateMap.put("Delhi","07");
	stateMap.put("Rajasthan","08");
	stateMap.put("Uttar Pradesh","09");
	stateMap.put("Bihar","10");
	
	stateMap.put("Sikkim","11");
	stateMap.put("Arunachal Pradesh","12");
	stateMap.put("Nagaland","13");
	stateMap.put("Manipur","14");
	stateMap.put("Mizoram","15");
	stateMap.put("Tripura","16");
	stateMap.put("Meghalaya","17");
	stateMap.put("Assam","18");
	stateMap.put("West Bengal","19");
	stateMap.put("Jharkhand","20");
	
	stateMap.put("Orissa","21");
	stateMap.put("Chhattisgarh","22");
	stateMap.put("Madhya Pradesh","23");
	stateMap.put("Gujarat","24");
	stateMap.put("Daman and Diu","25");
	stateMap.put("Dadra and Nagar Haveli","26");
	stateMap.put("Maharashtra","27");
	stateMap.put("Andhra Pradesh","28");
	stateMap.put("Karnataka","29");
	stateMap.put("Goa","30");
	
	stateMap.put("Lakshadweep","31");
	stateMap.put("Kerala","32");
	stateMap.put("Tamil Nadu","33");
	stateMap.put("Puducherry","34");
	stateMap.put("Andaman and Nicobar Islands","35");
	
}

public String getStateCode(String stateName){
	if(stateMap!=null){
		return stateMap.get(stateName);
	}
	return null;
}

/**
 * ENd
 */


	/**
	 * 
	 * 
	 * 
	 * 
	 * 
	 * setPestControlCategories
	 * 
	 * 
	 */
	/*******************************************Config***********************************************/
	
	@Override
	public Integer saveDemoConfig(Long companyId,String clientType) {
		if(clientType.trim().equals("Pest Control")){
			setPestControlConfigs(companyId);
		}
//		if(clientType.equals("Other")){
//			setConfigs(companyId);
//		}
		
		return 1;
	}
	
	/****************************************Config Category********************************************/
	
	@Override
	public Integer saveDemoConfigCategory(Long companyId,String clientType) {
		if(clientType.trim().equals("Pest Control")){
			setPestControlCategories(companyId);
//			setPestControlType(companyId);
		}
//		else{
//			setConfigCategory(companyId);
//		}
		
		return 1;
	}
	
	
	
	/*******************************************Other(Emp,Cust,Branch)***************************************/
	
	@Override
	public Integer saveDemoOtherDetails(Long companyId,String clientType) {
		setOtherDetails(companyId);
		return 1;
	}
	
	/*******************************************Asset Management****************************************/
	
	
	@Override
	public Integer saveDemoAssetDetails(Long companyId,String clientType) {
		
		setAssetDetails(companyId);
		return 1;
	}

	/*****************************************Products************************************************/
	
	@Override
	public Integer saveDemoProductsDetails(Long companyId,String clientType) {
		setProductDetails(companyId);
		return 1;
	}

	/***********************************************Inventory*********************************************/
	
	@Override
	public Integer saveDemoInventoryDetails(Long companyId,String clientType) {
		/**
		 * below is commented by vijay on 28 feb 2017
		 */
//		setInventoryDetails(companyId);
		/**
		 * ends here
		 */
		return 1;
	}
	
	/*****************************************Default Config********************************************/
	
	@Override
	public Integer saveDefaultConfigs(Long companyId, String clientType) {
		setDefaultConfigs(companyId);
		return 1;
	}
	

	
	
	/***************************************Data Save Common Method*****************************************/
	
	public void saveConfig(SuperModel model){
		
		GenricServiceImpl impl =new GenricServiceImpl();
		impl.saveDemoConfigData(model);
	}
	
	/********************************************End Of Common Method*-**************************************/
	
	
	public void setOtherDetails(Long companyId) {
		
		for(int i=0;i<otherList().size();i++){
//			if(otherList().get(i).trim().equals("COUNTRY")){
//				Country country = getCountryData("India","EN",companyId);
//				saveConfig(country);
//			}
			
			
//			if(otherList().get(i).trim().equals("STATE")){
//				State state = getStateData("Maharashtra", "India", companyId);
//				saveConfig(state);
//				State state1 = getStateData("Gujarat","India",companyId);
//				saveConfig(state1);
//				State state2 = getStateData("Rajasthan","India", companyId);
//				saveConfig(state2);
//				State state3 = getStateData("Madhya Pradesh","India", companyId);
//				saveConfig(state3);
//				State state4 = getStateData("Uttar Pradesh","India", companyId);
//				saveConfig(state4);
//				State state5 = getStateData("Jammu and Kashmir","India", companyId);
//				saveConfig(state5);
//				State state6 = getStateData("Karnataka","India", companyId);
//				saveConfig(state6);
//				State state7 = getStateData("Andhra Pradesh","India", companyId);
//				saveConfig(state7);
//				State state8 = getStateData("Odisha","India", companyId);
//				saveConfig(state8);
//				State state9 = getStateData("Chhattisgarh","India", companyId);
//				saveConfig(state9);
//				State state10 = getStateData("Tamil Nadu","India", companyId);
//				saveConfig(state10);
//				State state11 = getStateData("Telangana","India", companyId);
//				saveConfig(state11);
//				State state12 = getStateData("Bihar","India", companyId);
//				saveConfig(state12);
//				State state13 = getStateData("West Bengal","India", companyId);
//				saveConfig(state13);
//				State state14 = getStateData("Arunachal Pradesh","India", companyId);
//				saveConfig(state14);
//				State state15 = getStateData("Jharkhand","India", companyId);
//				saveConfig(state15);
//				State state16 = getStateData("Assam","India", companyId);
//				saveConfig(state16);
//				State state17 = getStateData("Himachal Pradesh","India", companyId);
//				saveConfig(state17);
//				State state18 = getStateData("Uttarakhand","India", companyId);
//				saveConfig(state18);
//				State state19 = getStateData("Punjab","India", companyId);
//				saveConfig(state19);
//				State state20 = getStateData("Haryana","India", companyId);
//				saveConfig(state20);
//				State state21 = getStateData("Kerala","India", companyId);
//				saveConfig(state21);
//				State state22 = getStateData("Meghalaya","India", companyId);
//				saveConfig(state22);
//				State state23 = getStateData("Manipur","India", companyId);
//				saveConfig(state23);
//				State state24 = getStateData("Delhi","India", companyId);
//				saveConfig(state24);
//			
//			}
			
			
//			if(otherList().get(i).trim().equals("CITY")){
//				City city = getCityData("Mumbai", "Maharashtra", companyId);
//				saveConfig(city); 
//				City city1 = getCityData("Pune", "Maharashtra", companyId);
//				saveConfig(city1); 
//				
//				City city2 = getCityData("Jodhpur", "Rajasthan", companyId);
//				saveConfig(city2); 
//				
//				City city3 = getCityData("Jaipur", "Rajasthan", companyId);
//				saveConfig(city3); 
//				 
//			}
//			if(otherList().get(i).trim().equals("LOCALITY")){
//				Locality loc = getLocalityData("Dadar", "Mumbai", companyId);
//				saveConfig(loc);
//				
//				Locality loc1 = getLocalityData("Ghatkopar", "Mumbai", companyId);
//				saveConfig(loc1);
//				
//			}
			
//			if(otherList().get(i).trim().equals("BRANCH")){
//				Branch branch = getBranchData("Dadar", companyId);
//				saveConfig(branch);
//				
//				Branch branch1 = getBranchData("Andheri", companyId);
//				saveConfig(branch1);
//				
//				Branch branch2 = getBranchData("Navi Mumbai", companyId);
//				saveConfig(branch2);
//			}
			
//			if(otherList().get(i).trim().equals("EMPLOYEE")){
//				Employee emp = getEmployeeData("Vedant Kamble", companyId);
//				saveConfig(emp);
//				
//				Employee emp1 = getEmployeeData("Rohan Bhagde", companyId);
//				saveConfig(emp1);
//				
//				Employee emp2 = getEmployeeData("Rahul Verma", companyId);
//				saveConfig(emp2);
//							
//			}
//			
//			if(otherList().get(i).trim().equals("CUSTOMER")){
//				Customer custEntity = getCustomerData(companyId,"Commercial");
//				saveConfig(custEntity);
//				
//				Customer custEntity1 = getCustomerData(companyId,"Residancial");
//				saveConfig(custEntity1);
//			}
//			
		
//			if(otherList().get(i).trim().equals("DEPARTMENT")){
//				Department deptEntity = getDepartmentData(companyId,"Service");
//				saveConfig(deptEntity);
//				
//				Department deptEntity1 = getDepartmentData(companyId,"Admin");
//				saveConfig(deptEntity1);
//				
//				Department deptEntity2 = getDepartmentData(companyId,"Sales");
//				saveConfig(deptEntity2);
//				
//				Department deptEntity3 = getDepartmentData(companyId,"Back Office");
//				saveConfig(deptEntity3);
//				
//				Department deptEntity4 = getDepartmentData(companyId,"Accounts");
//				saveConfig(deptEntity4);
//				
//				Department deptEntity5 = getDepartmentData(companyId,"HR");
//				saveConfig(deptEntity5);
//			}
		}
	}

	public Country getCountryData(String country1,String lang,long companyId){
		
		Country country = new Country();
		country.setCountryName(country1);
		country.setLanguage(lang);
		country.setCompanyId(companyId);
		return country;
	}
	
	public State getStateData(String state,String country,long companyId){
		
		State st = new State();
		st.setStateName(state);
		st.setCountry(country);
		st.setStatus(true);
		st.setCompanyId(companyId);
		return st;
	}
	
	public City getCityData(String city,String state,long companyId){
		
		City ct = new City();
		ct.setCityName(city);
		ct.setState(state);
		ct.setStatus(true);
		ct.setCompanyId(companyId);
		return ct;
	}
	
	public Locality getLocalityData(String loc,String city,long companyId){
		
		Locality lt = new Locality();
		lt.setLocality(loc);
		lt.setCityName(city);
		lt.setStatus(true);
		lt.setCompanyId(companyId);
		return lt;
	}
	
//	public Branch getBranchData(String branch1,long companyId)
//	{
//		Branch branch = new Branch();
//		
//		branch.setBusinessUnitName(branch1);
//		branch.setEmail("evasoftwaresolutionsdev@gmail.com");
//		branch.setstatus(true);
//		branch.setCellNumber1(981234567);
//		branch.setPocName("Kiran Kadwadkar");
//		branch.setPocCell(981234567);
//		
//		Address address=new Address();
//		address.setAddrLine1("Address Line 1");
//		address.setCountry("India");
//		address.setState("Maharashtra");
//		address.setCity("Mumbai");
//		address.setPin(476288);
//		
//		branch.setAddress(address);
//		branch.setCompanyId(companyId);
//		return branch;
//	}

	public Employee getEmployeeData(String fullName,long companyId){
		
		Employee emp = new Employee();
		emp.setCountry("India");
//		emp.setFirstName(empName);
//		emp.setLastName(lname);
		emp.setFullname(fullName);
		emp.setBranchName("Dadar");
		emp.setRoleName("Head Person");
		emp.setDesignation("Service Manager");
		emp.setStatus(true);
		emp.setEmail("evasoftwaresolutionserp@gmail.com");
		emp.setCellNumber1(9876546985L);
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		
		emp.setAddress(address);
		emp.setCompanyId(companyId);
		return emp;
	}
	
	public Customer getCustomerData(long companyId,String type){
		
		if(type.equalsIgnoreCase("Commercial"))
		{
			Customer cust = new Customer();
			cust.setCompany(true);
			cust.setCompanyName("Sharma & Sons");
			cust.setDob(new Date());
			cust.setFullname("Suraj Sharma");
			cust.setEmail("evasoftwaresolutionserp@gmail.com");
			cust.setCellNumber1(9876543210L);
			cust.setBranch("Dadar");
			cust.setCategory("COMMERCIAL");
			cust.setEmployee("Vedant Kamble");
			cust.setType("OFFICES");
			cust.setGroup("Register Billing");
			cust.setCustomerLevel("Silver");
			cust.setCustomerPriority("MEDIUM");
			Address address=new Address();
			address.setAddrLine1("Vikhroli");
			address.setCountry("India");
			address.setState("Maharashtra");
			address.setCity("Mumbai");
			address.setPin(476288);
			cust.setAdress(address);
			cust.setSecondaryAdress(address);
			cust.setCompanyId(companyId);
			return cust;
		}
		else
		{
			Customer cust = new Customer();
			cust.setCompany(false);
			cust.setCompanyName("");
			cust.setDob(new Date());
			cust.setFullname("Suraj Sharma");
			cust.setEmail("evasoftwaresolutionserp@gmail.com");
			cust.setCellNumber1(9876543210L);
			cust.setBranch("Dadar");
			cust.setCategory("RESIDENCIAL");
			cust.setEmployee("Rohan Bhagde");
			cust.setType("1 BHK FLAT");
			cust.setGroup("Register Billing");
			cust.setCustomerLevel("Silver");
			cust.setCustomerPriority("MEDIUM");
			Address address=new Address();
			address.setAddrLine1("Vikhroli");
			address.setCountry("India");
			address.setState("Maharashtra");
			address.setCity("Mumbai");
			address.setPin(476288);
			cust.setAdress(address);
			cust.setSecondaryAdress(address);
			cust.setCompanyId(companyId);
			return cust;
		}
	}

	
	public Department getDepartmentData(long companyId,String deptName){
		
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setDeptName("Service");
		dept.setCellNo1(9513202569L);
		dept.setHodName("Vedant Kamble");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		return dept;
	}

	
	
	/*****************************************Set Configs****************************************/
	
//	public void setConfigs(Long companyId)
//	{
//		for(int i=0;i<configList().size();i++){
//			if(configList().get(i).trim().equals("CUSTOMERGROUP")||
//					configList().get(i).trim().equals("LEADGROUP")||
//					configList().get(i).trim().equals("SALESQUOTATIONGROUP")||
//					configList().get(i).trim().equals("SALESORDERGROUP")||
//					configList().get(i).trim().equals("DELIVERYNOTEGROUP")||
//					configList().get(i).trim().equals("QUOTATIONGROUP")||
//					configList().get(i).trim().equals("CONTRACTGROUP")||
//					configList().get(i).trim().equals("VENDORGROUP")||
//					configList().get(i).trim().equals("PRGROUP")||
//					configList().get(i).trim().equals("GRNGROUP")){
//				Screen scr = commonForAllConfigs(configList().get(i).trim());
//				Config config = getConfigData(ConfigTypes.getConfigType(scr), "Demo Group 1", companyId);
//				saveConfig(config);
//			}
//			
//			
//			if(configList().get(i).trim().equals("CUSTOMERLEVEL")){
//				
//				for(int j =0;j<2;j++){
//				if(j==0){	
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Gold",companyId);
//				saveConfig(config);
//				}
//				
//				if(j==1){	
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Platinum",companyId);
//				saveConfig(config);
//				}
//				if(j==2){	
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Silver",companyId);
//				saveConfig(config);
//				}
//			}
//			}
//			
//			/***************************PRIORITY***************************/
//			
//			if(configList().get(i).trim().equals("CUSTOMERPRIRORITY")||
//					configList().get(i).trim().equals("LEADPRIORITY")||
//					configList().get(i).trim().equals("SALESQUOTATIONPRIORITY")||
//					configList().get(i).trim().equals("QUOTATIONPRIORITY")){
//				
//				Screen scr = commonForAllConfigs(configList().get(i).trim());
//				
//					for(int k=0;k<1;k++){
//					Config config = getConfigData(ConfigTypes.getConfigType(scr),"LOW", companyId);
//					saveConfig(config);
//					}
//					for(int l=0;l<1;l++){
//					Config config1 = getConfigData(ConfigTypes.getConfigType(scr),"MEDIUM", companyId);
//					saveConfig(config1);
//					}
//					for(int m=0;m<1;m++){
//					Config config2 = getConfigData(ConfigTypes.getConfigType(scr),"HIGH", companyId);
//					saveConfig(config2);
//					}
//			}
//			
//			
//			if(configList().get(i).trim().equals("LEADSTATUS")){
//			
//			for(int n=0;n<1;n++){
//				Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Created",companyId);
//				saveConfig(config3);
//				}
//			for(int o=0;o<1;o++){
//				Config config4 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Approved", companyId);
//				saveConfig(config4);
//				}
//			for(int p=0;p<1;p++){
//				Config config5 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Requested", companyId);
//				saveConfig(config5);
//				}
//			for(int q=0;q<1;q++){
//				Config config6 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Successful", companyId);
//				saveConfig(config6);
//				}
//			for(int r=0;r<1;r++){
//				Config config7 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"UnSuccessful", companyId);
//				saveConfig(config7);
//				}
//			for(int s=0;s<1;s++){
//				Config config8 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Rejected",companyId);
//				saveConfig(config8);
//				}
//			}
//		}
//		}
	
//	public void setConfigCategory(Long companyId){
//		
//		for(int i=0;i<categoryList().size();i++){
//			if(categoryList().get(i).trim().equals("CUSTOMERCATEGORY")||
//					categoryList().get(i).trim().equals("LEADCATEGORY")||
//					categoryList().get(i).trim().equals("SALESQUOTATIONCATEGORY")||
//					categoryList().get(i).trim().equals("SALESORDERCATEGORY")||
//					categoryList().get(i).trim().equals("DELIVERYNOTECATEGORY")||
//					categoryList().get(i).trim().equals("QUOTATIONCATEGORY")||
//					categoryList().get(i).trim().equals("CONTRACTCATEGORY")||
//					categoryList().get(i).trim().equals("VENDORCATEGORY")||
//					categoryList().get(i).trim().equals("PRCATEGORY")||
//					categoryList().get(i).trim().equals("RFQCATEGORY")||
//					categoryList().get(i).trim().equals("LOICATEGORY")||
//					categoryList().get(i).trim().equals("POCATEGORY")||
//					categoryList().get(i).trim().equals("GRNCATEGORY")||
//					categoryList().get(i).trim().equals("MRNCATEGORY")||
//					categoryList().get(i).trim().equals("MINCATEGORY")||
//					categoryList().get(i).trim().equals("MMNCATEGORY")){
//				
//				Screen scr1 = commonForAllCategory(categoryList().get(i).trim());
//				ConfigCategory configCategory = getConfigCategoryData(CategoryTypes.getCategoryInternalType(scr1),"Demo Category 1","CC"+10000000+i,companyId);
//				saveConfig(configCategory);
//			}
//		}
//		
//		for(int i=0;i<typeList().size();i++){
////			String catcode="";
//			
//			if(typeList().get(i).trim().equals("CUSTOMERTYPE")||
//					typeList().get(i).trim().equals("LEADTYPE")||
//					typeList().get(i).trim().equals("SALESQUOTATIONTYPE")||
//					typeList().get(i).trim().equals("SALESORDERTYPE")||
//					typeList().get(i).trim().equals("DELIVERYNOTETYPE")||
//					typeList().get(i).trim().equals("QUOTATIONTYPE")||
//					typeList().get(i).trim().equals("CONTRACTTYPE")||
//					typeList().get(i).trim().equals("VENDORTYPE")||
//					typeList().get(i).trim().equals("PRTYPE")||
//					typeList().get(i).trim().equals("RFQTYPE")||
//					typeList().get(i).trim().equals("LOITYPE")||
//					typeList().get(i).trim().equals("POTYPE")||
//					typeList().get(i).trim().equals("GRNTYPE")||
//					typeList().get(i).trim().equals("MRNTYPE")||
//					typeList().get(i).trim().equals("MINTYPE")||
//					typeList().get(i).trim().equals("MMNTYPE")
//					){
//				
//				
//				Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//				
//				Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Demo Category 1", "Demo Type 1", "TT"+10000000+i,"CC"+10000000+i, companyId);
//				saveConfig(types);
//			}
//		}
//	}
	
	

	public Config getConfigData(int value,String S1,long companyId){
			
			Config config = new Config();
			config.setName(S1);
			config.setStatus(true);
			config.setCompanyId(companyId);
			config.setType(value);
			return config;
		}
	

	public static ConfigCategory getConfigCategoryData(int value,String S1,String S2,long companyId){
		
		
		ConfigCategory configCategory = new ConfigCategory();
		configCategory.setInternalType(value);
		configCategory.setCategoryName(S1);
		configCategory.setCategoryCode(S2);
		configCategory.setCompanyId(companyId);
		configCategory.setStatus(true);
		return configCategory;
	}


	public static Type getTypesData(int value,String S1,String S2,String S3,String S4,long companyId){
		
		Type types = new Type();
		types.setInternalType(value);
		types.setCategoryName(S1);
		types.setTypeName(S2);
		types.setTypeCode(S3);
		types.setCatCode(S4);
//		types.setCategoryKey(key);
		types.setCompanyId(companyId);
		types.setStatus(true);
		return types;
	}
		
	public void setAssetDetails(Long companyId)
	{
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "CompanyAsset").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
    	int compAssCount = (int) lastNumber;
    	
    	ArrayList<CompanyAsset> companyAssetList=new ArrayList<CompanyAsset>();
    	CompanyAsset asset1=getCompanyAssetData(companyId, ++compAssCount, "Mask", "Tool Category 1", "Brand", "Demo Model", "100000000", "Demo vendor");
    	CompanyAsset asset2=getCompanyAssetData(companyId, ++compAssCount, "Brush", "Tool Category 1", "Brand", "Demo Model", "100000001", "Demo vendor");
    	CompanyAsset asset3=getCompanyAssetData(companyId, ++compAssCount, "Rod", "Tool Category 1", "Brand", "Demo Model", "100000002", "Demo vendor");
    	companyAssetList.add(asset1);
    	companyAssetList.add(asset2);
    	companyAssetList.add(asset3);
    	
    	ofy().save().entities(companyAssetList);
    	numbergen.setNumber(compAssCount);
    	ofy().save().entity(numbergen);
    	
//		companyAssetScreen(companyId);
//		companyAssetScreen1(companyId);
//		companyAssetScreen2(companyId);
		clientSideAsset(companyId);
		toolSet(companyId);
	}
	
	
	public CompanyAsset getCompanyAssetData(Long companyId,int count,String assetName,String categoryName,String brand,String modelNo,String srNo,String purchaseFrom){
		CompanyAsset companyAsset=new CompanyAsset();
		companyAsset.setName(assetName);
		companyAsset.setCategory(categoryName);
		companyAsset.setBrand(brand);
		companyAsset.setModelNo(modelNo);
		companyAsset.setSrNo(srNo);
		companyAsset.setPurchasedFrom(purchaseFrom);
		companyAsset.setCompanyId(companyId);
		companyAsset.setCount(count);
		return companyAsset;
	}
	
	private void companyAssetScreen(Long companyId)
	{
		CompanyAsset companyAsset=new CompanyAsset();
		companyAsset.setName("Mask");
		companyAsset.setCategory("Tool Category 1");
		companyAsset.setBrand("Brand");
		companyAsset.setModelNo("Demo Model");
		companyAsset.setSrNo("100000000");
		companyAsset.setPurchasedFrom("Employee 1");
		companyAsset.setCompanyId(companyId);
		saveConfig(companyAsset);
		
	}
	
	private void companyAssetScreen1(Long companyId)
	{
		CompanyAsset companyAsset=new CompanyAsset();
		companyAsset.setName("Rod");
		companyAsset.setCategory("Tool Category 1");
		companyAsset.setBrand("Brand");
		companyAsset.setPurchasedFrom("Employee 1");
		companyAsset.setCompanyId(companyId);
		saveConfig(companyAsset);
		
	}
	
	private void companyAssetScreen2(Long companyId)
	{
		CompanyAsset companyAsset=new CompanyAsset();
		companyAsset.setName("Brush");
		companyAsset.setCategory("Tool Category 1");
		companyAsset.setBrand("Brand");
		companyAsset.setCompanyId(companyId);
		saveConfig(companyAsset);
	}
	
	private void clientSideAsset(Long companyId)
	{
		ClientSideAsset clientAsset=new ClientSideAsset();
		
		PersonInfo personInfo=new PersonInfo();
		personInfo.setFullName("ROHIT SHARMA");
		personInfo.setCellNumber(9876543210L);
		personInfo.setCount(100000001);
		clientAsset.setPersonInfo(personInfo);
		clientAsset.setName("Demo Asset 1");
		clientAsset.setCategory("Asset Category 1");
		clientAsset.setBrand("Brand");
		clientAsset.setSrNo("100000000");
		clientAsset.setPurchasedFrom("Employee 1");
		clientAsset.setCompanyId(companyId);
		saveConfig(clientAsset);
	}
	
	
	private void toolSet(Long companyId)
	{
		ToolGroup toolGroup=new ToolGroup();
		
		toolGroup.setCompanyId(companyId);
		toolGroup.setName("Tool Set 1");
		toolGroup.setCode("DemoCode");
		ArrayList<CompanyAsset> arrayOfAsset=new ArrayList<CompanyAsset>();
		CompanyAsset listTool=new CompanyAsset();
		listTool.setName("Demo Asset 1");
		listTool.setBrand("Brand");
		listTool.setModelNo("Demo Model");
		listTool.setSrNo("100000000");
		arrayOfAsset.add(listTool);
		toolGroup.setArraylistTool(arrayOfAsset);
		
		saveConfig(toolGroup);
	}
	
	
	public void setProductDetails(Long companyId)
	{
		/**
		 * Product Category
		 */
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Category").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
    	int catCount = (int) lastNumber;
    	
		ArrayList<Category>productCategoryList=new ArrayList<Category>(); 
		Category itemCat1 = getItemProductCategoryData(companyId,"IP001","Chemicals",++catCount);
		Category servCat1 = getServiceProductCategoryData(companyId,"SP001","Service Category",++catCount);
		productCategoryList.add(itemCat1);
		productCategoryList.add(servCat1);
		ofy().save().entities(productCategoryList);
		numbergen.setNumber(catCount);
		ofy().save().entity(numbergen);
		
		/**
		 * GLAccount
		 */
		NumberGeneration numbergen1 = null;	
		numbergen1 = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "GLAccount").filter("status", true).first().now();
		long lastNumber1 = numbergen1.getNumber();
    	int glCount = (int) lastNumber1;
    	
		ArrayList<GLAccount>glAccountList=new ArrayList<GLAccount>(); /*
		GLAccount vat1=getGLAccountData(companyId, "Tax Group", "VAT", 16987,++glCount);
		GLAccount st1=getGLAccountData(companyId, "Tax Group", "Service Tax", 16927,++glCount);
		GLAccount st2=getGLAccountData(companyId, "Tax Group", "CST", 16950,++glCount);
		
		*nidhi
		* 30-03-2018
		* for set cgst and sgst on creating new demo link
		*/

		GLAccount vat1=getGLAccountData(companyId, "Tax Group", "CGST", 16987,++glCount);
		GLAccount st1=getGLAccountData(companyId, "Tax Group", "SGST", 16927,++glCount);
		GLAccount st2=getGLAccountData(companyId, "Tax Group", "IGST", 16950,++glCount);
		GLAccount st3=getGLAccountData(companyId, "Tax Group", "NA", 16950,++glCount);
		/**
		 * end
		 */

		glAccountList.add(vat1);
		glAccountList.add(st1);
		glAccountList.add(st2);
		ofy().save().entities(glAccountList);
		numbergen1.setNumber(glCount);
		ofy().save().entity(numbergen1);
		
		/**
		 * Tax Details
		 */
		NumberGeneration numbergen2 = null;	
		numbergen2 = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "GLAccount").filter("status", true).first().now();
		long lastNumber2 = numbergen2.getNumber();
    	int txCount = (int) lastNumber2;
    	
		ArrayList<TaxDetails>taxDetailList=new ArrayList<TaxDetails>(); 
	/*	TaxDetails st15=getTaxDetailsData(companyId, "SER 15", 15.0, "Service Tax",true,false,false,++txCount);
		TaxDetails stNA=getTaxDetailsData(companyId, "NA", 0.0, "Service Tax",true,false,false,++txCount);
		TaxDetails vat6=getTaxDetailsData(companyId, "VAT 6", 6.0, "VAT",false,true,false,++txCount);
		TaxDetails vatNA=getTaxDetailsData(companyId, "NA", 0.0, "VAT",false,true,false,++txCount);
		TaxDetails cst2=getTaxDetailsData(companyId, "CST 2", 2.0, "CST",false,false,true,++txCount);*/
		/**
		 *  nidhi
		 *  30-03-2018
		 *  for remove service tax to sgst and cgst
		 */
		TaxDetails st15=getTaxDetailsData(companyId, "SGST@9", 9.0, "SGST",true,false,false,++txCount);
		TaxDetails stNA=getTaxDetailsData(companyId, "NA", 0.0, "NA",true,false,false,++txCount);
		TaxDetails vat6=getTaxDetailsData(companyId, "CGST@9", 9.0, "CGST",false,true,false,++txCount);
//		TaxDetails vatNA=getTaxDetailsData(companyId, "NA", 0.0, "CGST",false,true,false,++txCount);
		TaxDetails cst2=getTaxDetailsData(companyId, "IGST@18", 18.0, "IGST",true,false,false,++txCount);
		/**
		 *  end
		 */
		taxDetailList.add(st15);
		taxDetailList.add(stNA);
		taxDetailList.add(vat6);
//		taxDetailList.add(vatNA);
		taxDetailList.add(cst2);
		ofy().save().entities(taxDetailList);
		numbergen2.setNumber(txCount);
		ofy().save().entity(numbergen2);
		
		/**
		 * This method creates item and service product through task queue
		 */
		getProductDetailstaskqueue(companyId);
		
	}
	
	
	/**
	 * Date : 27-01-2017 By Anil
	 * Updated method: made method parameterized
	 */
	
	private Category getServiceProductCategoryData(Long companyId,String categoryCode,String categoryName,int count)
	{
		Category catEntity=new Category();
		catEntity.setCatCode(categoryCode);
		catEntity.setCatName(categoryName);
		catEntity.setStatus(true);
		catEntity.setCompanyId(companyId);
		catEntity.setServiceCategory(true);
		catEntity.setSellCategory(false);
		catEntity.setCount(count);
		
		return catEntity;
	}
	/**
	 * Date : 27-01-2017 By Anil
	 * Updated method: made method parameterized
	 */
	private Category getItemProductCategoryData(Long companyId,String categoryCode,String categoryName,int count)
	{
		Category catEntity=new Category();
		catEntity.setCatCode(categoryCode);
		catEntity.setCatName(categoryName);
		catEntity.setStatus(true);
		catEntity.setCompanyId(companyId);
		catEntity.setServiceCategory(false);
		catEntity.setSellCategory(true);
		catEntity.setCount(count);
		
		return catEntity;
	}
	/**
	 * Date : 27-01-2017 By Anil
	 * Updated method: made method parameterized
	 */
	
	private GLAccount getGLAccountData(Long companyId,String glAccountGroup,String glAccountName,int glAccountNo,int count)
	{
		GLAccount glData=new GLAccount();
		glData.setCompanyId(companyId);
		glData.setGlAccountGroup(glAccountGroup);
		glData.setGlAccountName(glAccountName);
		glData.setGlAccountNo(glAccountNo);
		glData.setStatus("Active");
		glData.setCount(count);
		
		return glData;
	}
	
	/**
	 * Date : 27-01-2017 By Anil
	 * Updated method: made method parameterized
	 */
	
	private TaxDetails getTaxDetailsData(Long companyId,String taxName,double taxPercent,String taxGroup,boolean isSerTax,boolean isVat,boolean isCst,int count)
	{
		TaxDetails taxEntity=new TaxDetails();
		taxEntity.setCompanyId(companyId);
		taxEntity.setTaxChargeStatus(true);
		taxEntity.setTaxChargeName(taxName);
		taxEntity.setTaxChargePercent(taxPercent);
		taxEntity.setGlAccountName(taxGroup);
		taxEntity.setServiceTax(isSerTax);
		taxEntity.setVatTax(isVat);
		taxEntity.setCentralTax(isCst);
		taxEntity.setCount(count);
		
		return taxEntity;
	}
	
	
	private TaxDetails getSerTaxDetailsInfo(Long companyId)
	{
		TaxDetails taxEntity=new TaxDetails();
		taxEntity.setCompanyId(companyId);
		taxEntity.setTaxChargeStatus(true);
		taxEntity.setTaxChargeName("SER 15");
		taxEntity.setTaxChargePercent(15.0);
		taxEntity.setGlAccountName("Tax Group");
		taxEntity.setServiceTax(true);
		
		return taxEntity;
	}
	
	private TaxDetails getSerTaxDetailsInfo1(Long companyId)
	{
		TaxDetails taxEntity=new TaxDetails();
		taxEntity.setCompanyId(companyId);
		taxEntity.setTaxChargeStatus(true);
		taxEntity.setTaxChargeName("NA");
		taxEntity.setTaxChargePercent(0.0);
		taxEntity.setGlAccountName("Tax Group");
		taxEntity.setServiceTax(true);
		
		return taxEntity;
	}
	
	private TaxDetails getVatTaxDetailsInfo(Long companyId)
	{
		TaxDetails taxEntity=new TaxDetails();
		taxEntity.setCompanyId(companyId);
		taxEntity.setTaxChargeStatus(true);
		taxEntity.setTaxChargeName("VAT 6");
		taxEntity.setTaxChargePercent(6);
		taxEntity.setGlAccountName("Tax Group");
		taxEntity.setVatTax(true);
		
		return taxEntity;
	}

	private TaxDetails getVatTaxDetailsInfo1(Long companyId)
	{
		TaxDetails taxEntity=new TaxDetails();
		taxEntity.setCompanyId(companyId);
		taxEntity.setTaxChargeStatus(true);
		taxEntity.setTaxChargeName("N.A");
		taxEntity.setTaxChargePercent(0.0);
		taxEntity.setGlAccountName("Tax Group");
		taxEntity.setVatTax(true);
		
		return taxEntity;
	}
	
	private TaxDetails getCstTaxDetailsInfo(Long companyId)
	{
		TaxDetails taxEntity=new TaxDetails();
		taxEntity.setCompanyId(companyId);
		taxEntity.setTaxChargeStatus(true);
		taxEntity.setTaxChargeName("CST 2");
		taxEntity.setTaxChargePercent(2.0);
		taxEntity.setGlAccountName("Tax Group");
		taxEntity.setCentralTax(true);
		
		return taxEntity;
	}

	private ItemProduct getItemProductData(Long companyId)
	{
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("IPDemo 1");
		itemProd.setProductCategory("Item Category");
		itemProd.setProductName("Chemical Injection");
		itemProd.setPrice(1500);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		return itemProd;
	}
	
	private ItemProduct getItemProductData1(Long companyId)
	{
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("IPDemo 2");
		itemProd.setProductCategory("Item Category");
		itemProd.setProductName("Spray");
		itemProd.setPrice(2000);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		Tax vatTax=new Tax();
		vatTax.setTaxConfigName("VAT 12.5");
		vatTax.setPercentage(12.5);
		itemProd.setVatTax(vatTax);
		itemProd.setStatus(true);
		
		return itemProd;
	}
	
	private ServiceProduct getServiceProductData(Long companyId)
	{
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(4);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("SPDemo");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("Bed Bug Treatment");
		serviceProd.setPrice(2000);
		Tax serTax=new Tax();
		serTax.setPercentage(15.0);
		serTax.setTaxConfigName("SER 15");
		serviceProd.setServiceTax(serTax);
		
		return serviceProd;
	}
	
	private GLAccount getVatGLAccountData(Long companyId)
	{
		GLAccount glData=new GLAccount();
		glData.setCompanyId(companyId);
		glData.setGlAccountGroup("Tax Group");
		glData.setGlAccountName("VAT");
		glData.setGlAccountNo(16987);
		glData.setStatus("Active");
		
		return glData;
	}
	
	private GLAccount getSTGLAccountData(Long companyId)
	{
		GLAccount glData=new GLAccount();
		glData.setCompanyId(companyId);
		glData.setGlAccountGroup("Tax Group");
		glData.setGlAccountName("Service Tax");
		glData.setGlAccountNo(16927);
		glData.setStatus("Active");
		
		return glData;
	}
	
//	public void setInventoryDetails(Long companyId)
//	{
//		int count =0;
//		for(int i=0;i<inventoryListData().size();i++)
//		{
//			
//			if(inventoryListData().get(i).trim().equals("TRANSACTIONTYPE")){
//				if(count==0){
//					
//					GenricServiceImpl genimpl = new GenricServiceImpl();
//					
//					MaterialMovementType returnType = getReturnTransactionTypeData(companyId);
//					genimpl.save(returnType);
////					saveConfig(returnType);
//					
//					MaterialMovementType scrapType = getScrapTransactionTypeData(companyId);
////					saveConfig(scrapType);
//					genimpl.save(scrapType);
//					
//					MaterialMovementType tinType = getTinTransactionTypeData(companyId);
////					saveConfig(tinType);
//					genimpl.save(tinType);
//					
//					MaterialMovementType toutType = getToutTransactionTypeData(companyId);
////					saveConfig(toutType);
//					genimpl.save(toutType);
//				}
//				count++;
//				
//			}
//
//		}
//	}
	
	
	public void setDefaultConfigs(Long companyId)
	{
		for(int x=0;x<defaultConfigsListData().size();x++)
		{
			if(defaultConfigsListData().get(x).trim().equals("MODULENAME"))
			{
				/* Date:- 10-09-2016
				 * Release :- 30 sep 2016
				 * here i am saving module name with task queue so first i took last number generation of configCategory
				 * then i called task queue then again the last  number updated in number generation.
				 * and bellow old code commented by me because old code saving document name normal saving procedure 
				 */
				
				NumberGeneration numbergen = null;	
				numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "ConfigCategory").filter("status", true).first().now();
				long lastNumber = numbergen.getNumber();
				logger.log(Level.SEVERE,"Last Number===="+lastNumber);
		    	 int count = (int) lastNumber;
				
				
				for(int j=0;j<moduleNames().size();j++)
				{
					// old code commented by vijay
//					ConfigCategory configCategory = getConfigCategoryData(CategoryTypes.getCategoryInternalType(Screen.MODULENAME),moduleNames().get(j).trim(),"CC"+10000000+j,companyId);
//					saveConfig(configCategory);
					
					
					int setcount = ++count;
					
					String configCategoryinstring = getConfigCategoryDatainString(CategoryTypes.getCategoryInternalType(Screen.MODULENAME),moduleNames().get(j).trim(),"CC"+10000000+j,companyId,setcount);
					System.out.println(" vijay  string value"+configCategoryinstring);
					
					
					Queue queue = QueueFactory.getQueue("ModuleName-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/modulenametaskqueue").param("moduleNamekey", configCategoryinstring));
				}
				
				// vijay
				  numbergen.setNumber(count);
		    	  GenricServiceImpl impl = new GenricServiceImpl();
				  impl.save(numbergen);
			}
			
			if(defaultConfigsListData().get(x).trim().equals("DOCUMENTNAME"))
			{
				
				/* Date:- 10-09-2016
				 * Release :- 30 sep 2016
				 * here i am saving document name with task queue so first i took last number generation of Type
				 * then i called task queue then again  the last  number updated in number generation.
				 * and bellow old code commented by me because old code saving document name normal saving procedure 
				 */
				
				NumberGeneration numbergen = null;	
				numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Type").filter("status", true).first().now();
				long lastNumber = numbergen.getNumber();
				logger.log(Level.SEVERE,"Last Number===="+lastNumber);
		    	 int count = (int) lastNumber;
				
				for(int j=0;j<moduleNames().size();j++)
				{
					if(moduleNames().get(j).trim().equals("Sales"))
					{
						for(int g=0;g<salesDocumentNames().size();g++)
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Sales", salesDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Sales", salesDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId,setcount);

							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals(AppConstants.SALESCONFIG)) {
						for (int g=0; g < salesConfigDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.SALESCONFIG,salesConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.SALESCONFIG,salesConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					
					if(moduleNames().get(j).trim().equals("Service"))
					{
						for(int g=0;g<serviceDocumentNames().size();g++)
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Service", serviceDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Service", serviceDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals(AppConstants.SERVICECONFIG)) {
						for (int g = 0; g < serviceConfigDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.SERVICECONFIG,serviceConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.SERVICECONFIG,serviceConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));	
						}
					}
					
					if(moduleNames().get(j).trim().equals("Purchase"))
					{
						for(int g=0;g<purchaseDocumentNames().size();g++)
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Purchase", purchaseDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Purchase", purchaseDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals(AppConstants.PURCHASECONFIG)) {
						for (int g = 0; g < purchaseConfigDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.PURCHASECONFIG,purchaseConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.PURCHASECONFIG,purchaseConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if(moduleNames().get(j).trim().equals("Inventory"))
					{
						for(int g=0;g<inventoryDocumentNames().size();g++)
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Inventory", inventoryDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Inventory", inventoryDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
							
						}
					}
					
					if (moduleNames().get(j).trim().equals(AppConstants.INVENTORYCONFIG)) {
						System.out.println("in side module name "+moduleNames().get(j).trim());
						
						for (int g = 0; g < inventoryConfigDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.INVENTORYCONFIG,inventoryConfigDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000 + j, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.INVENTORYCONFIG,inventoryConfigDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000 + j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals("HR Admin")) 
					{
						for (int g = 0; g < hrAdminDocumentNames().size(); g++) 
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"HR Admin", hrAdminDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+ 10000000 + j, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"HR Admin", hrAdminDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+ 10000000 + j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}

					if (moduleNames().get(j).trim().equals(AppConstants.HRCONFIG)) 
					{
						for (int g = 0; g < hrAdminConfigDocumentNames().size(); g++) 
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.HRCONFIG,hrAdminConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.HRCONFIG,hrAdminConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
							
						}
					}
					
					if(moduleNames().get(j).trim().equals("Product"))
					{
						for(int g=0;g<productDocumentNames().size();g++)
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Product", productDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Product", productDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals(AppConstants.PRODUCTCONFIG)) 
					{
						for (int g = 0; g < productConfigDocumentNames().size(); g++) 
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.PRODUCTCONFIG,productConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.PRODUCTCONFIG,productConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals("Asset Management")) {
						for (int g = 0; g < assetManagementDocumentNames().size(); g++) 
						{		// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Asset Management",assetManagementDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000 + j, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Asset Management",assetManagementDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000 + j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}

					if (moduleNames().get(j).equals(AppConstants.ASSETMANAGEMENTCONFIG)) {
						for (int g = 0; g < assetManagementConfigDocumentNames().size(); g++) 
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.ASSETMANAGEMENTCONFIG,assetManagementConfigDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000+j,companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.ASSETMANAGEMENTCONFIG,assetManagementConfigDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000+j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
							
						}
					}

					if(moduleNames().get(j).trim().equals("Accounts"))
					{
						for(int g=0;g<accountsDocumentNames().size();g++)
						{
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Accounts", accountsDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME), "Accounts", accountsDocumentNames().get(g).trim(), "TT"+10000000+g,"CC"+10000000+j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim()
							.equals(AppConstants.ACCOUNTCONFIG)) {
						for (int g = 0; g < accountsConfigDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.ACCOUNTCONFIG,
//									accountsConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),AppConstants.ACCOUNTCONFIG,accountsConfigDocumentNames().get(g).trim(),"TT" + 10000000 + g, "CC" + 10000000 + j,companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals("Settings")) {
						for (int g = 0; g < settingDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(
//									CategoryTypes
//											.getCategoryInternalType(Screen.DOCUMENTNAME),
//									"Settings", settingDocumentNames().get(g)
//											.trim(), "TT" + 10000000 + g, "CC"
//											+ 10000000 + j, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Settings", settingDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+ 10000000 + j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));							
							
						}
					}

					if (moduleNames().get(j).trim().equals("Approval")) {
						for (int g = 0; g < approvalDocumentNames().size(); g++) {
							// old code commented by vijay
//							Type types = getTypesData(
//							CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Approval", approvalDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+ 10000000 + j, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Approval", approvalDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+ 10000000 + j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
					
					if (moduleNames().get(j).trim().equals("Employee Self Service")){
						for (int g = 0; g < employeeSelfServiceDocumentNames().size(); g++) 
						{		// old code commented by vijay
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Employee Self Service",employeeSelfServiceDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000+j, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							String ducumentNamesData = getTypeDatafortaskqueue(CategoryTypes.getCategoryInternalType(Screen.DOCUMENTNAME),"Employee Self Service",employeeSelfServiceDocumentNames().get(g).trim(), "TT" + 10000000 + g, "CC"+10000000+j, companyId,setcount);
							
							Queue queue = QueueFactory.getQueue("DocumentName-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentametaskqueue").param("documentNamekey", ducumentNamesData));
						}
					}
				}
				
				 numbergen.setNumber(count);
		    	 GenricServiceImpl impl = new GenricServiceImpl();
				 impl.save(numbergen);
			}
		}
	}
	

	private WareHouse getWarehouseData(Long companyId)
	{
		WareHouse warehouseEntity=new WareHouse();
		warehouseEntity.setBusinessUnitName("Mumbai");
		warehouseEntity.setEmail("evasoftwaresolutionserp@gmail.com");
		warehouseEntity.setstatus(true);
		warehouseEntity.setPocName("ROHIT SHARMA");
		warehouseEntity.setPocCell(9876543210L);
		
		Address address=new Address();
		address.setAddrLine1("Address Line 1");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(400083);
		warehouseEntity.setAddress(address);
		warehouseEntity.setCompanyId(companyId);
		
		return warehouseEntity;
	}
	
	private StorageLocation getStorageLocationData(Long companyId)
	{
		StorageLocation locationEntity=new StorageLocation();
		
		locationEntity.setCompanyId(companyId);
		locationEntity.setWarehouseName("Mumbai");
		locationEntity.setBusinessUnitName("Mumbai LOC");
		locationEntity.setstatus(true);
		locationEntity.setEmail("evasoftwaresolutionsdev@gmail.com");
		locationEntity.setCellNumber1(9876543210L);
		locationEntity.setPocName("ROHIT SHARMA");
		locationEntity.setPocCell(9876543210L);
		
		Address address=new Address();
		address.setAddrLine1("Address Line 1");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(400083);
		locationEntity.setAddress(address);
		
		return locationEntity;
	}
	
	private Storagebin getStorageBinData(Long companyId)
	{
		Storagebin binEntity=new Storagebin();
		
		binEntity.setCompanyId(companyId);
		binEntity.setBinName("Mumbai BIN");
		binEntity.setXcoordinate("20");
		binEntity.setYcoordinate("25");
		binEntity.setStatus(true);
		binEntity.setStoragelocation("Mumbai LOC");
		
		return binEntity;
	}
	
	private MaterialMovementType getReturnTransactionTypeData(Long companyId)
	{
		MaterialMovementType transactionType=new MaterialMovementType();

		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("Return");
		transactionType.setMmtType("IN");
		
		return transactionType;
	}
	
	private MaterialMovementType getScrapTransactionTypeData(Long companyId)
	{
		MaterialMovementType transactionType=new MaterialMovementType();

		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("Scrap");
		transactionType.setMmtType("OUT");
		
		return transactionType;
	}
	
	private MaterialMovementType getTinTransactionTypeData(Long companyId)
	{
		MaterialMovementType transactionType=new MaterialMovementType();

		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("TransferIN");
		transactionType.setMmtType("TRANSFERIN");
		
		return transactionType;
	}
	
	private MaterialMovementType getToutTransactionTypeData(Long companyId)
	{
		MaterialMovementType transactionType=new MaterialMovementType();

		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("TransferOUT");
		transactionType.setMmtType("TRANSFEROUT");
		
		return transactionType;
	}
	
	
	/******************************If Client Type is Pest Control*******************************************/
	
	/**********************************************Pest Control Configs*************************************/
	
	public void setPestControlConfigs(Long companyId)
	{
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Config").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	 int count = (int) lastNumber;
		
		for(int i=0;i<configList().size();i++){
			
			
			if(configList().get(i).trim().equals("WORKORDERGROUP")|| configList().get(i).trim().equals("INSPECTIONGROUP"))
			{
				for(int j=0;j<3;j++)
				{
					Screen scr = commonForAllConfigs(configList().get(i).trim());
					if(j==0){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Internal", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
						
					}
					if(j==1){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "External", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==2){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "3rd Party ", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
				}
				
			}
			
			
			
			if(configList().get(i).trim().equals("MMNDIRECTION"))
			{
				
				Screen scr = commonForAllConfigs(configList().get(i).trim());
				for(int j=0;j<3;j++)
				{
					
					if(j==0){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "IN", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
						
					}
					if(j==1){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "OUT", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==2){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "TRANSFERIN", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					
					if(j==3){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "TRANSFEROUT", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
				}
				
			}
			
			
			if(configList().get(i).trim().equals("EXPENSEGROUP"))
			{
				
				Screen scr = commonForAllConfigs(configList().get(i).trim());
				for(int j=0;j<3;j++)
				{
					
					if(j==0){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Daily", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
						
					}
					if(j==1){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Monthly", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==2){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Weekly", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					
					if(j==3){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Yearly", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
				}
				
			}
			
			
			
				if(configList().get(i).trim().equals("PRODUCTGROUP"))
				{
					
					Screen scr = commonForAllConfigs(configList().get(i).trim());
					for(int j=0;j<3;j++)
					{
						
						if(j==0){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Powder", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
							
						}
						if(j==1){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Gel", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
						if(j==2){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Glue pads", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
						
						if(j==3){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Chemical", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
					}
					
				}
				
				if(configList().get(i).trim().equals("PRODUCTCLASSIFICATION"))
				{
					for(int j=0;j<2;j++)
					{
						Screen scr = commonForAllConfigs(configList().get(i).trim());
						if(j==0){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Use for Spraying", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
							
						}
						if(j==1){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Use for Fogging", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
						
					}
					
				}
				
			
				if(configList().get(i).trim().equals("PRODUCTTYPE"))
				{
					for(int j=0;j<4;j++)
					{
						Screen scr = commonForAllConfigs(configList().get(i).trim());
						if(j==0){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Trap Treatment", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
							
						}
						if(j==1){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Spraying", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
						if(j==2){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Fogging", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
						if(j==3){
//							Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//							saveConfig(types);
							
							
							int setcount = ++count;
							
							String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Gel Application", companyId,setcount);
							logger.log(Level.SEVERE,"vijay  string value"+configInstring);
							
							Queue queue = QueueFactory.getQueue("ConfigValue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
							
						}
					}
					
				}
				
			
			if(configList().get(i).trim().equals("UNITOFMEASUREMENT"))
			{
				for(int j=0;j<7;j++)
				{
					Screen scr = commonForAllConfigs(configList().get(i).trim());
					if(j==0){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Ltr", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
						
					}
					if(j==1){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Ml", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==2){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Nos", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==3){
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Gram", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==4){
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "KG", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==5){
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Sq ft", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==6){
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Box", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
				}
				
			}
			
			
			
			if(configList().get(i).trim().equals("COMMUNICATIONTYPE"))
			{
				for(int j=0;j<3;j++)
				{
					Screen scr = commonForAllConfigs(configList().get(i).trim());
					if(j==0){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Verbal", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
						
					}
					if(j==1){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Non Verbal", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					if(j==2){
//						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
//						saveConfig(types);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "SMS", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
				}
				
			}
			
			
			if(configList().get(i).trim().equals("COMMUNICATIONCATEGORY"))
			{				
				Screen scr = commonForAllConfigs(configList().get(i).trim());
				for(int j=0;j<3;j++)
				{
					if(j==0){
//						ConfigCategory configCategory = getConfigCategoryData(CategoryTypes.getCategoryInternalType(scr1),"Distributor","CC"+10000000+i,companyId);
//						saveConfig(configCategory);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Internal", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
				if(j==1){
					
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "With Customer", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
					
				if(j==2){
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "External", companyId,setcount);
						logger.log(Level.SEVERE,"vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
				}
			}
			
			
			
			
			if(configList().get(i).trim().equals("CUSTOMERGROUP")||configList().get(i).trim().equals("INVOICEGROUP")||configList().get(i).trim().equals("BILLINGGROUP")){
				
				Screen scr = commonForAllConfigs(configList().get(i).trim());
				for(int j=0;j<9;j++){
					if(j==0){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Single", companyId);
//						saveConfig(config);

					
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Single", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
					else if(j==1){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Monthly", companyId);
//						saveConfig(config);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Monthly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else if(j==2){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Quarterly", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Quarterly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
					}
					else if(j==3){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Four Monthly", companyId);
//						saveConfig(config);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Four Monthly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
						
						
					}
					else if(j==4){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Half Yearly ", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Half Yearly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else if(j==5){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Yearly", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Yearly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else if(j==6){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "3 Yearly", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "3 Yearly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else if(j==7){
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "5 Yearly", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "5 Yearly", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else{
//						Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERGROUP), "Sample Service Non Billing", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Sample Service Non Billing", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
				}
			}
			
			if(configList().get(i).trim().equals("LEADGROUP")||configList().get(i).trim().equals("QUOTATIONGROUP")||configList().get(i).trim().equals("CONTRACTGROUP")){
				Screen scr = commonForAllConfigs(configList().get(i).trim());
				for(int j=0;j<2;j++){
					if(j==0){
//						Config config = getConfigData(ConfigTypes.getConfigType(scr), "Campaign", companyId);
//						saveConfig(config);
	
						
							int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Campaign", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else{
//						Config config = getConfigData(ConfigTypes.getConfigType(scr), "Enquiry", companyId);
//						saveConfig(config);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Enquiry", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
				}
			}
			
			if(configList().get(i).trim().equals("SALESQUOTATIONGROUP")||configList().get(i).trim().equals("SALESORDERGROUP")||configList().get(i).trim().equals("DELIVERYNOTEGROUP")){
				for(int j=0;j<2;j++){
					Screen scr = commonForAllConfigs(configList().get(i).trim());
					if(j==0){
//						Config config = getConfigData(ConfigTypes.getConfigType(scr), "ERP", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "ERP", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else{
//						Config config = getConfigData(ConfigTypes.getConfigType(scr), "ECommerce", companyId);
//						saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "ECommerce", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
				}
			}
			
			
			
//			if(configList().get(i).trim().equals("WORKORDERGROUP")){
//				for(int j=0;j<3;j++){
////					Screen scr = commonForAllConfigs(configList().get(i).trim());
//					if(j==0){
////						Config config = getConfigData(ConfigTypes.getConfigType(scr), "ERP", companyId);
////						saveConfig(config);
//						
//						int setcount = ++count;
//						
//						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.WORKORDERGROUP), "Project", companyId,setcount);
//						System.out.println(" vijay  string value"+configInstring);
//						
//						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
//					}
//					if(j==1){
////						Config config = getConfigData(ConfigTypes.getConfigType(scr), "ECommerce", companyId);
////						saveConfig(config);
//						
//						int setcount = ++count;
//						
//						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.WORKORDERGROUP), "Exhibition", companyId,setcount);
//						System.out.println(" vijay  string value"+configInstring);
//						
//						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
//					}
//					if(j==2){
////						Config config = getConfigData(ConfigTypes.getConfigType(scr), "ECommerce", companyId);
////						saveConfig(config);
//						
//						int setcount = ++count;
//						
//						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.WORKORDERGROUP), "Maintenance", companyId,setcount);
//						System.out.println(" vijay  string value"+configInstring);
//						
//						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
//					}
//				}
//			}
			
			
			if(configList().get(i).trim().equals("VENDORGROUP")||configList().get(i).trim().equals("GRNGROUP")||configList().get(i).trim().equals("PRGROUP")){
				for(int j=0;j<2;j++){
					Screen scr = commonForAllConfigs(configList().get(i).trim());
					if(j==0){
//						Config config = getConfigData(ConfigTypes.getConfigType(scr), "Registered", companyId);
//						saveConfig(config);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Registered", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					else{
//						Config config = getConfigData(ConfigTypes.getConfigType(scr), "Unregistered", companyId);
//						saveConfig(config);
						
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Unregistered", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
				}
			}
			
			
			
			
		if(configList().get(i).trim().equals("TDSPERCENTAGE")){
				
//				for(int j =0;j<10;j++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.APPROVALLEVEL),(j+1)+"",companyId);
//				
//				saveConfig(config);
//			 }
				
				for(int j =0; j < 3; j++){
					if(j==0){	
//					Config config = getConfigData(ConfigTypes.getConfigType(Screen.APPROVALLEVEL),"Gold",companyId);
//					saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.TDSPERCENTAGE), "1", companyId,setcount);
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					
					if(j==1){	
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.TDSPERCENTAGE), "2", companyId,setcount);
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					if(j==2){	
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.TDSPERCENTAGE), "3", companyId,setcount);
						System.out.println(" Rohan string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
				}	
				
			}
			
			
			
		
			
		if(configList().get(i).trim().equals("CURRENCY")){
			
//			for(int j =0;j<10;j++){
//			Config config = getConfigData(ConfigTypes.getConfigType(Screen.APPROVALLEVEL),(j+1)+"",companyId);
//			
//			saveConfig(config);
//		 }
			
			for(int j =0; j < 3; j++){
				if(j==0){	
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.APPROVALLEVEL),"Gold",companyId);
//				saveConfig(config);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CURRENCY), "RS", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				if(j==1){	
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Platinum",companyId);
//				saveConfig(config);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CURRENCY), "USD", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}	
			
		}
		
			
			if(configList().get(i).trim().equals("CUSTOMERLEVEL")){
				
//				for(int j =0;j<10;j++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),(j+1)+"",companyId);
//				
//				saveConfig(config);
//			 }
				for(int j =0; j < 3; j++){
					if(j==0){	
//					Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Gold",companyId);
//					saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL), "Gold", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					
					if(j==1){	
//					Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Platinum",companyId);
//					saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL), "Platinum", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					if(j==2){	
//					Config config = getConfigData(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL),"Silver",companyId);
//					saveConfig(config);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CUSTOMERLEVEL), "Silver", companyId,setcount);
						System.out.println(" Rohan string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
				}	
				
			}
			
			/***************************PRIORITY***************************/
			
			if(configList().get(i).trim().equals("CUSTOMERPRIRORITY")||
					configList().get(i).trim().equals("LEADPRIORITY")||
					configList().get(i).trim().equals("SALESQUOTATIONPRIORITY")||
					configList().get(i).trim().equals("QUOTATIONPRIORITY")||
					configList().get(i).trim().equals("COMPLAINPRIORITY")||
					configList().get(i).trim().equals("INTERACTIONPRIORITY")){
				
				Screen scr = commonForAllConfigs(configList().get(i).trim());
				
					for(int k=0;k<1;k++){
//					Config config = getConfigData(ConfigTypes.getConfigType(scr),"Low", companyId);
//					saveConfig(config);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Low", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
					for(int l=0;l<1;l++){
//					Config config1 = getConfigData(ConfigTypes.getConfigType(scr),"Medium", companyId);
//					saveConfig(config1);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "Medium", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					for(int m=0;m<1;m++){
//					Config config2 = getConfigData(ConfigTypes.getConfigType(scr),"High", companyId);
//					saveConfig(config2);
						
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), "High", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
			}
			
			if(configList().get(i).trim().equals("LEADSTATUS")){
			
			for(int n=0;n<1;n++){
//				Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Created",companyId);
//				saveConfig(config3);

				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.LEADSTATUS), "Created", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
			}
			
			for(int o=0;o<1;o++){
//				Config config4 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Approved", companyId);
//				saveConfig(config4);

				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.LEADSTATUS), "Approved", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				
				}
//			for(int p=0;p<1;p++){
//				Config config5 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Requested", companyId);
//				saveConfig(config5);
//				}
			for(int q=0;q<1;q++){
//				Config config6 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Successful", companyId);
//				saveConfig(config6);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.LEADSTATUS), "Successful", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				
				}
			for(int r=0;r<1;r++){
//				Config config7 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"UnSuccessful", companyId);
//				saveConfig(config7);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.LEADSTATUS), "UnSuccessful", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				
				}
//			for(int s=0;s<1;s++){
//				Config config8 = getConfigData(ConfigTypes.getConfigType(Screen.LEADSTATUS),"Rejected",companyId);
//				saveConfig(config8);
//				}
			for(int t=0;t<1;t++){

				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.LEADSTATUS), "Quotation Created", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				
				}
			for(int v=0;v<1;v++){

				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.LEADSTATUS), "Quotation Sent", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				
				}
		
			}
			
			
			if(configList().get(i).trim().equals("COMPLAINSTATUS")){
				
				for(int n=0;n<1;n++){
//					Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS),"Created",companyId);
//					saveConfig(config3);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS), "Created", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
				for(int o=0;o<1;o++){
//					Config config4 = getConfigData(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS),"Completed", companyId);
//					saveConfig(config4);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS), "Completed", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
				}
			
			if(configList().get(i).trim().equals("GROUPTYPE")){
				
				for(int n=0;n<1;n++){
//					Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS),"Created",companyId);
//					saveConfig(config3);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.GROUPTYPE), "Sales", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
				
				for(int n=0;n<1;n++){
//					Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS),"Created",companyId);
//					saveConfig(config3);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.GROUPTYPE), "Service", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
				
				for(int n=0;n<1;n++){
//					Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.COMPLAINSTATUS),"Created",companyId);
//					saveConfig(config3);
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.GROUPTYPE), "Payments", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					
					}
			}
			
			
			if(configList().get(i).trim().equals("EMPLOYEEROLE")){
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.EMPLOYEEROLE),"Head Person",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.EMPLOYEEROLE), "Head Person", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config1 = getConfigData(ConfigTypes.getConfigType(Screen.EMPLOYEEROLE),"Manager",companyId);
//				saveConfig(config1);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.EMPLOYEEROLE), "Manager", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			if(configList().get(i).trim().equals("EMPLOYEEDESIGNATION")){
			
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.EMPLOYEEDESIGNATION),"CEO",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.EMPLOYEEDESIGNATION), "CEO", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config1 = getConfigData(ConfigTypes.getConfigType(Screen.EMPLOYEEDESIGNATION),"Service Manager",companyId);
//				saveConfig(config1);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.EMPLOYEEDESIGNATION), "Service Manager", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			if(configList().get(i).trim().equals("EMPLOYEETYPE")){
				
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.EMPLOYEETYPE),"Permanent",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.EMPLOYEETYPE), "Permanent", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			if(configList().get(i).trim().equals("ARTICLETYPE")){
			
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.ARTICLETYPE),"VAT",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ARTICLETYPE), "VAT", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config2 = getConfigData(ConfigTypes.getConfigType(Screen.ARTICLETYPE),"PAN No",companyId);
//				saveConfig(config2);
				
					int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ARTICLETYPE), "PAN No", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.ARTICLETYPE),"Service Tax",companyId);
//				saveConfig(config3);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ARTICLETYPE), "Service Tax", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}			
				
		     }
			
			if(configList().get(i).trim().equals("SERVICETYPE")){
				
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.SERVICETYPE),"Periodic",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SERVICETYPE), "Periodic", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config2 = getConfigData(ConfigTypes.getConfigType(Screen.SERVICETYPE),"Complain",companyId);
//				saveConfig(config2);
				
					int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SERVICETYPE), "Complain", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config3 = getConfigData(ConfigTypes.getConfigType(Screen.SERVICETYPE),"Free Service",companyId);
//				saveConfig(config3);
			
					int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SERVICETYPE), "Free Service", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config4 = getConfigData(ConfigTypes.getConfigType(Screen.SERVICETYPE),"Re Service",companyId);
//				saveConfig(config4);
				
					int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SERVICETYPE), "Re Service", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
			}
			
			if(configList().get(i).trim().equals("PAYMENTMETHODS")){
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.PAYMENTMETHODS),"Cash",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PAYMENTMETHODS), "Cash", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config2 = getConfigData(ConfigTypes.getConfigType(Screen.PAYMENTMETHODS),"Cheque",companyId);
//				saveConfig(config2);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PAYMENTMETHODS), "Cheque", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
			}
			
			if(configList().get(i).trim().equals("COMPANYTYPE")){
				for(int n=0;n<1;n++){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.COMPANYTYPE),"General",companyId);
//				saveConfig(config);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.COMPANYTYPE), "General", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
//				Config config1 = getConfigData(ConfigTypes.getConfigType(Screen.COMPANYTYPE),"Pest Control",companyId);
//				saveConfig(config1);
				
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.COMPANYTYPE), "Pest Control", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
			}
			
			if(configList().get(i).trim().equals("FUMIGATIONDECLAREDPOE")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.FUMIGATIONDECLAREDPOE), "India", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.FUMIGATIONDECLAREDPOE), "Australia", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("DOSAGERATEOFFUMIGANT")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.DOSAGERATEOFFUMIGANT), "24", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.DOSAGERATEOFFUMIGANT), "32", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.DOSAGERATEOFFUMIGANT), "48", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			
			if(configList().get(i).trim().equals("DURATIONOFFUMIGATION")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.DURATIONOFFUMIGATION), "24", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.DURATIONOFFUMIGATION), "48", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.DURATIONOFFUMIGATION), "72", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			
			if(configList().get(i).trim().equals("MINIMUMAIRTEMP")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.MINIMUMAIRTEMP), "20", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.MINIMUMAIRTEMP), "23", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.MINIMUMAIRTEMP), "25", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("QUANTITYDECLAREUNIT")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.QUANTITYDECLAREUNIT), "Boxes", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.QUANTITYDECLAREUNIT), "Pallets", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.QUANTITYDECLAREUNIT), "Pkgs", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			
			if(configList().get(i).trim().equals("PLACEOFFUMIGATION")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PLACEOFFUMIGATION), "Mumbai", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PLACEOFFUMIGATION), "Gujrat", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			
			if(configList().get(i).trim().equals("COUNTRYOFLOADING")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.COUNTRYOFLOADING), "Mumbai-India", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.COUNTRYOFLOADING), "Nava Sheva", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("APPROVALLEVEL")){
				for(int n=0;n<3;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.APPROVALLEVEL), n+1+"", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
			}
			
						
			if(configList().get(i).trim().equals("NUMBERRANGE")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.NUMBERRANGE), "Billing", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.NUMBERRANGE), "NonBilling", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			
			if(configList().get(i).trim().equals("CATCHTRAPPESTNAME")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CATCHTRAPPESTNAME), "Rodent", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CATCHTRAPPESTNAME), "Mosquito", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("PERSONTYPE")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PERSONTYPE), "Customer", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PERSONTYPE), "Employee", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PERSONTYPE), "Vendor", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("CONTACTROLE")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CONTACTROLE), "Manager", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CONTACTROLE), "Director", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CONTACTROLE), "Technician", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CONTACTROLE), "Accountant", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("SPECIFICREASONFORRESCHEDULING")){
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SPECIFICREASONFORRESCHEDULING), "Customer Not Available", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SPECIFICREASONFORRESCHEDULING), "Customer Address Change", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SPECIFICREASONFORRESCHEDULING), "Service Engg Not Available", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
				
				for(int n=0;n<1;n++){
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.SPECIFICREASONFORRESCHEDULING), "Customer Request", companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			
			if(configList().get(i).trim().equals("PROCESSTYPE"))
			{
				for(int h=0;h<processTypeData().size();h++)
				{
//					Config config = getConfigData(ConfigTypes.getConfigType(Screen.PROCESSTYPE), processTypeData().get(h).trim(), companyId);
//					saveConfig(config);
					
					
					int setcount = ++count;
					
					String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.PROCESSTYPE), processTypeData().get(h).trim(), companyId,setcount);
					System.out.println(" vijay  string value"+configInstring);
					
					Queue queue = QueueFactory.getQueue("ConfigValue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
				}
			}
			
			
			if(configList().get(i).trim().equals("GLACCOUNTGROUP")){
//				Config config = getConfigData(ConfigTypes.getConfigType(Screen.GLACCOUNTGROUP), "Tax Group", companyId);
//				saveConfig(config);
				
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.GLACCOUNTGROUP), "Tax Group", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
			}
			
			if(configList().get(i).trim().equals("ASSETCATEGORY")){
//			Config config = getConfigData(ConfigTypes.getConfigType(Screen.ASSETCATEGORY), "Tool Category 1", companyId);
//			saveConfig(config);
				
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ASSETCATEGORY), "Tax Group", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
			}
			
			if(configList().get(i).trim().equals("CLIENTSIDEASSETCATEGORY")){
//			Config config1 = getConfigData(ConfigTypes.getConfigType(Screen.CLIENTSIDEASSETCATEGORY), "Asset Category 1", companyId);
//			saveConfig(config1);
				
				int setcount = ++count;
				
				String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CLIENTSIDEASSETCATEGORY), "Asset Category 1", companyId,setcount);
				System.out.println(" vijay  string value"+configInstring);
				
				Queue queue = QueueFactory.getQueue("ConfigValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
			}
			
			if(configList().get(i).trim().equals("ASSETARTICLETYPE")){
//				Config config1 = getConfigData(ConfigTypes.getConfigType(Screen.CLIENTSIDEASSETCATEGORY), "Asset Category 1", companyId);
//				saveConfig(config1);
					for (int j = 0; j < 3; j++) {
						
					if(j==0){
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ASSETARTICLETYPE), "Manufacturer's licence No", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					if(j==1)
					{
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ASSETARTICLETYPE), "Warranty ", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					if(j==2)
					{
						int setcount = ++count;
						
						String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.ASSETARTICLETYPE), "Guarantee", companyId,setcount);
						System.out.println(" vijay  string value"+configInstring);
						
						Queue queue = QueueFactory.getQueue("ConfigValue-queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
					}
					}
					
					}
		}
		
		 numbergen.setNumber(count);
   	     GenricServiceImpl impl = new GenricServiceImpl();
		 impl.save(numbergen);
	}
	
	
	public void setPestControlCategories(Long companyId) {

		NumberGeneration numbergen = null;
		numbergen = ofy().load().type(NumberGeneration.class)
				.filter("companyId", companyId)
				.filter("processName", "ConfigCategory").filter("status", true)
				.first().now();
		
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE, "Last Number====" + lastNumber);
		int count = (int) lastNumber;


		NumberGeneration numbergenration = null;
		numbergenration = ofy().load().type(NumberGeneration.class)
				.filter("companyId", companyId)
				.filter("processName", "Type").filter("status", true)
				.first().now();
		long lastNumberGen = numbergenration.getNumber();
		logger.log(Level.SEVERE, "Last Number====" + lastNumberGen);
		int typeCount = (int) lastNumberGen;

		/***************/
		for (int i = 0; i < categoryList().size(); i++) {
			
			Screen scr1 = commonForAllCategory(categoryList().get(i).trim());
			
			/**
			 * 
			 * 
			 * 
			 * 
			 *         1
			 * 
			 * 
			 * 
			 * 
			 * 
			 */	
			if (categoryList().get(i).trim().equals("GRNCATEGORY")
					|| categoryList().get(i).trim().equals("MRNCATEGORY")
					|| categoryList().get(i).trim().equals("MINCATEGORY")
					|| categoryList().get(i).trim().equals("MMNCATEGORY")) {

				

				int setcount = ++count;
				String configInstring = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Chemicals", "CC" + setcount, companyId, setcount,"Blank");
				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", configInstring));


				int setTypecount1 = ++typeCount;
				String typeInstring1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Chemicals", "Spray","TT"+ setTypecount1,"CC" + setcount, companyId,setTypecount1);
				Queue typequeue1 = QueueFactory.getQueue("TypeValue-queue");
				typequeue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring1));

				int setTypecount2 = ++typeCount;
				String typeInstring2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Chemicals", "Injection", "TT"+ setTypecount2,"CC" + setcount, companyId,setTypecount2);
				Queue typequeue2 = QueueFactory.getQueue("TypeValue-queue");
				typequeue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring2));
				
			}
			
			if(categoryList().get(i).trim().equals("MRNCATEGORY")){
				/**
				 * Category : Internal Use
				 */
				int setInternalcount = ++count;
				String internalCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Internal Use", "CC" + setInternalcount, companyId, setInternalcount,"Blank");
				Queue internalCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				internalCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", internalCatString));
				
				//Types
				int setTypecount1 = ++typeCount;
				String typeInstring1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal Use", "For Production","TT"+ setTypecount1,"CC" + setInternalcount, companyId,setTypecount1);
				Queue typequeue1 = QueueFactory.getQueue("TypeValue-queue");
				typequeue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring1));
				
				int setTypecount2 = ++typeCount;
				String typeInstring2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal Use", "Returnable","TT"+ setTypecount2,"CC" + setInternalcount, companyId,setTypecount2);
				Queue typequeue2 = QueueFactory.getQueue("TypeValue-queue");
				typequeue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring2));
				
				int setTypecount3 = ++typeCount;
				String typeInstring3 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal Use", "Non Returnable","TT"+ setTypecount3,"CC" + setInternalcount, companyId,setTypecount3);
				Queue typequeue3 = QueueFactory.getQueue("TypeValue-queue");
				typequeue3.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring3));
				
				/**
				 * Category : External User
				 */
				int setExternalCount = ++count;
				String externalCatString1 = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"External Use", "CC" + setExternalCount, companyId, setExternalCount,"Blank");
				Queue externalCatQueue1 = QueueFactory.getQueue("CategoryValue-queue");
				externalCatQueue1.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", externalCatString1));
				
				//Types
				int setTypecount4 = ++typeCount;
				String typeInstring4 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External Use", "For Production","TT"+ setTypecount4,"CC" + setExternalCount, companyId,setTypecount4);
				Queue typequeue4 = QueueFactory.getQueue("TypeValue-queue");
				typequeue4.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring4));
				
				int setTypecount5 = ++typeCount;
				String typeInstring5 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External Use", "Returnable","TT"+ setTypecount5,"CC" + setExternalCount, companyId,setTypecount5);
				Queue typequeue5 = QueueFactory.getQueue("TypeValue-queue");
				typequeue5.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring5));
				
				int setTypecount6 = ++typeCount;
				String typeInstring6 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External Use", "Non Returnable","TT"+ setTypecount6,"CC" + setExternalCount, companyId,setTypecount6);
				Queue typequeue6 = QueueFactory.getQueue("TypeValue-queue");
				typequeue6.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring6));
				
				/**
				 * Category : Consumption
				 */
				int setConsumptionCount = ++count;
				String consumptionCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Consumption", "CC" + setConsumptionCount, companyId, setConsumptionCount,"Blank");
				Queue consumptionCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				consumptionCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", consumptionCatString));
				
				//Types
				int setTypecount7 = ++typeCount;
				String typeInstring7 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Consumption", "For Production","TT"+ setTypecount7,"CC" + setConsumptionCount, companyId,setTypecount7);
				Queue typequeue7 = QueueFactory.getQueue("TypeValue-queue");
				typequeue7.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring7));
				
				int setTypecount8 = ++typeCount;
				String typeInstring8 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Consumption", "Returnable","TT"+ setTypecount8,"CC" + setConsumptionCount, companyId,setTypecount8);
				Queue typequeue8 = QueueFactory.getQueue("TypeValue-queue");
				typequeue8.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring8));
				
				int setTypecount9 = ++typeCount;
				String typeInstring9 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Consumption", "Non Returnable","TT"+ setTypecount9,"CC" + setConsumptionCount, companyId,setTypecount9);
				Queue typequeue9 = QueueFactory.getQueue("TypeValue-queue");
				typequeue9.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring9));
				
				/**
				 * Category : Within Branch
				 */
				int setWbCount = ++count;
				String wbCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Within Branch", "CC" + setWbCount, companyId, setWbCount,"Blank");
				Queue wbCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				wbCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", wbCatString));
				
				//Types
				int setTypecount10 = ++typeCount;
				String typeInstring10 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Within Branch", "For Production","TT"+ setTypecount10,"CC" + setWbCount, companyId,setTypecount10);
				Queue typequeue10 = QueueFactory.getQueue("TypeValue-queue");
				typequeue10.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring10));
				
				int setTypecount11 = ++typeCount;
				String typeInstring11 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Within Branch", "Returnable","TT"+ setTypecount11,"CC" + setWbCount, companyId,setTypecount11);
				Queue typequeue11 = QueueFactory.getQueue("TypeValue-queue");
				typequeue11.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring11));
				
				int setTypecount12 = ++typeCount;
				String typeInstring12 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Within Branch", "Non Returnable","TT"+ setTypecount12,"CC" + setWbCount, companyId,setTypecount12);
				Queue typequeue12 = QueueFactory.getQueue("TypeValue-queue");
				typequeue12.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring12));
				
				/**
				 * Category : Across Branch
				 */
				int setAcBcount = ++count;
				String acBlCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Across Branch", "CC" + setAcBcount, companyId, setAcBcount,"Blank");
				Queue acBCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				acBCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", acBlCatString));
				
				//Types
				int setTypecount13 = ++typeCount;
				String typeInstring13 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Across Branch", "For Production","TT"+ setTypecount13,"CC" + setAcBcount, companyId,setTypecount13);
				Queue typequeue13 = QueueFactory.getQueue("TypeValue-queue");
				typequeue13.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring13));
				
				int setTypecount14 = ++typeCount;
				String typeInstring14 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Across Branch", "Returnable","TT"+ setTypecount14,"CC" + setAcBcount, companyId,setTypecount14);
				Queue typequeue14 = QueueFactory.getQueue("TypeValue-queue");
				typequeue14.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring14));
				
				int setTypecount15 = ++typeCount;
				String typeInstring15 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Across Branch", "Non Returnable","TT"+ setTypecount15,"CC" + setAcBcount, companyId,setTypecount15);
				Queue typequeue15 = QueueFactory.getQueue("TypeValue-queue");
				typequeue15.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring15));
				
				/**
				 * Category : Finished
				 */
				int setFinishedcount = ++count;
				String finishedCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Finished", "CC" + setFinishedcount, companyId, setFinishedcount,"Blank");
				Queue finishedCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				finishedCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", finishedCatString));
				
				//Types
				int setTypecount16 = ++typeCount;
				String typeInstring16 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Finished", "For Production","TT"+ setTypecount16,"CC" + setFinishedcount, companyId,setTypecount16);
				Queue typequeue16 = QueueFactory.getQueue("TypeValue-queue");
				typequeue16.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring16));
				
				int setTypecount17 = ++typeCount;
				String typeInstring17 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Finished", "Returnable","TT"+ setTypecount17,"CC" + setFinishedcount, companyId,setTypecount17);
				Queue typequeue17 = QueueFactory.getQueue("TypeValue-queue");
				typequeue17.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring17));
				
				int setTypecount18 = ++typeCount;
				String typeInstring18 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Finished", "Non Returnable","TT"+ setTypecount18,"CC" + setFinishedcount, companyId,setTypecount18);
				Queue typequeue18 = QueueFactory.getQueue("TypeValue-queue");
				typequeue18.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring18));
				
				/**
				 * Category : Semi Finished
				 */
				int setSemiFinishedCount = ++count;
				String semiFinishedCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Semi Finished", "CC" + setSemiFinishedCount, companyId, setSemiFinishedCount,"Blank");
				Queue semiFinishedCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				semiFinishedCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", semiFinishedCatString));
				
				//Types
				int setTypecount19 = ++typeCount;
				String typeInstring19 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Semi Finished", "For Production","TT"+ setTypecount19,"CC" + setSemiFinishedCount, companyId,setTypecount19);
				Queue typequeue19 = QueueFactory.getQueue("TypeValue-queue");
				typequeue19.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring19));
				
				int setTypecount20 = ++typeCount;
				String typeInstring20 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Semi Finished", "Returnable","TT"+ setTypecount20,"CC" + setSemiFinishedCount, companyId,setTypecount20);
				Queue typequeue20 = QueueFactory.getQueue("TypeValue-queue");
				typequeue20.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring20));
				
				int setTypecount21 = ++typeCount;
				String typeInstring21 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Semi Finished", "Non Returnable","TT"+ setTypecount21,"CC" + setSemiFinishedCount, companyId,setTypecount21);
				Queue typequeue21 = QueueFactory.getQueue("TypeValue-queue");
				typequeue21.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring21));
				
				/**
				 * Category : Raw Material
				 */
				int setRmCount = ++count;
				String rmCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Raw Material", "CC" + setRmCount, companyId, setRmCount,"Blank");
				Queue rmCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				rmCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", rmCatString));
				
				//Types
				int setTypecount22 = ++typeCount;
				String typeInstring22 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Raw Material", "For Production","TT"+ setTypecount22,"CC" + setRmCount, companyId,setTypecount22);
				Queue typequeue22 = QueueFactory.getQueue("TypeValue-queue");
				typequeue22.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring22));
				
				int setTypecount23 = ++typeCount;
				String typeInstring23 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Raw Material", "Returnable","TT"+ setTypecount23,"CC" + setRmCount, companyId,setTypecount23);
				Queue typequeue23 = QueueFactory.getQueue("TypeValue-queue");
				typequeue23.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring23));
				
				int setTypecount24 = ++typeCount;
				String typeInstring24 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Raw Material", "Non Returnable","TT"+ setTypecount24,"CC" + setRmCount, companyId,setTypecount24);
				Queue typequeue24 = QueueFactory.getQueue("TypeValue-queue");
				typequeue24.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring24));
			}
			if(categoryList().get(i).trim().equals("MMNCATEGORY")){
				/**
				 * Category : Scrap
				 */
				int setScrapCount = ++count;
				String scrapCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Scrap", "CC" + setScrapCount, companyId, setScrapCount,"Blank");
				Queue scrapCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				scrapCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", scrapCatString));
				
				//Types
				int setTypecount1 = ++typeCount;
				String typeInstring1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scrap", "Internal","TT"+ setTypecount1,"CC" + setScrapCount, companyId,setTypecount1);
				Queue typequeue1 = QueueFactory.getQueue("TypeValue-queue");
				typequeue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring1));
				
				int setTypecount2 = ++typeCount;
				String typeInstring2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scrap", "External","TT"+ setTypecount2,"CC" + setScrapCount, companyId,setTypecount2);
				Queue typequeue2 = QueueFactory.getQueue("TypeValue-queue");
				typequeue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring2));
				
				int setTypecount3 = ++typeCount;
				String typeInstring3 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scrap", "On Request","TT"+ setTypecount3,"CC" + setScrapCount, companyId,setTypecount3);
				Queue typequeue3 = QueueFactory.getQueue("TypeValue-queue");
				typequeue3.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring3));
				
				/**
				 * Category : Expired
				 */
				int setExpiredCount = ++count;
				String expiredCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Expired", "CC" + setExpiredCount, companyId, setExpiredCount,"Blank");
				Queue expiredCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				expiredCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", expiredCatString));
				
				//Types
				int setTypecount4 = ++typeCount;
				String typeInstring4 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Expired", "Internal","TT"+ setTypecount4,"CC" + setExpiredCount, companyId,setTypecount4);
				Queue typequeue4 = QueueFactory.getQueue("TypeValue-queue");
				typequeue4.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring4));
				
				int setTypecount5 = ++typeCount;
				String typeInstring5 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Expired", "External","TT"+ setTypecount5,"CC" + setExpiredCount, companyId,setTypecount5);
				Queue typequeue5 = QueueFactory.getQueue("TypeValue-queue");
				typequeue5.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring5));
				
				int setTypecount6 = ++typeCount;
				String typeInstring6 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Expired", "On Request","TT"+ setTypecount6,"CC" + setExpiredCount, companyId,setTypecount6);
				Queue typequeue6 = QueueFactory.getQueue("TypeValue-queue");
				typequeue6.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring6));
				
				/**
				 * Category : Return
				 */
				int setReturnCount = ++count;
				String returnCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Return", "CC" + setReturnCount, companyId, setReturnCount,"Blank");
				Queue returnCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				returnCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", returnCatString));
				
				//Types
				int setTypecount7 = ++typeCount;
				String typeInstring7 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Return", "Internal","TT"+ setTypecount7,"CC" + setReturnCount, companyId,setTypecount7);
				Queue typequeue7 = QueueFactory.getQueue("TypeValue-queue");
				typequeue7.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring7));
				
				int setTypecount8 = ++typeCount;
				String typeInstring8 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Return", "External","TT"+ setTypecount8,"CC" + setReturnCount, companyId,setTypecount8);
				Queue typequeue8 = QueueFactory.getQueue("TypeValue-queue");
				typequeue8.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring8));
				
				int setTypecount9 = ++typeCount;
				String typeInstring9 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Return", "On Request","TT"+ setTypecount9,"CC" + setReturnCount, companyId,setTypecount9);
				Queue typequeue9 = QueueFactory.getQueue("TypeValue-queue");
				typequeue9.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring9));
				
				/**
				 * Category : Damage
				 */
				int setDamageCount = ++count;
				String damageCatString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),"Damage", "CC" + setDamageCount, companyId, setDamageCount,"Blank");
				Queue damageCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				damageCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", damageCatString));
				
				//Types
				int setTypecount10 = ++typeCount;
				String typeInstring10 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Damage", "Internal","TT"+ setTypecount10,"CC" + setDamageCount, companyId,setTypecount10);
				Queue typequeue10 = QueueFactory.getQueue("TypeValue-queue");
				typequeue10.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring10));
				
				int setTypecount11 = ++typeCount;
				String typeInstring11 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Damage", "External","TT"+ setTypecount11,"CC" + setDamageCount, companyId,setTypecount11);
				Queue typequeue11 = QueueFactory.getQueue("TypeValue-queue");
				typequeue11.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring11));
				
				int setTypecount12 = ++typeCount;
				String typeInstring12 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Damage", "On Request","TT"+ setTypecount12,"CC" + setDamageCount, companyId,setTypecount12);
				Queue typequeue12 = QueueFactory.getQueue("TypeValue-queue");
				typequeue12.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring12));
				
				
			}

			/**
			 * 
			 * 
			 * 
			 * 
			 *         2
			 * 
			 * 
			 * 
			 * 
			 * 
			 */	

			if (categoryList().get(i).trim().equals("CUSTOMERCATEGORY")
					|| categoryList().get(i).trim().equals("LEADCATEGORY")
					|| categoryList().get(i).trim().equals("QUOTATIONCATEGORY")
					|| categoryList().get(i).trim().equals("CONTRACTCATEGORY")
					|| categoryList().get(i).trim().equals("BILLINGCATEGORY")
					|| categoryList().get(i).trim().equals("INVOICECATEGORY")) {
				

				/**
				 * Category: RESIDENTIAL & Its Type
				 */
				int setResCount = ++count;
				String resCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"RESIDENTIAL", "CC" + setResCount, companyId,setResCount, "Blank");
				Queue resCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				resCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", resCatInString));

				//Types
				int setResTypecount1 = ++typeCount;
				String resTypeInString1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"RESIDENTIAL", "1 BHK FLAT", "TT"+ setResTypecount1, "CC" + setResCount,companyId, setResTypecount1);
				Queue resTypequeue1 = QueueFactory.getQueue("TypeValue-queue");
				resTypequeue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", resTypeInString1));
				
				int setResTypecount2 = ++typeCount;
				String resTypeInString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"RESIDENTIAL", "2 BHK FLAT", "TT"+ setResTypecount2, "CC" + setResCount,companyId, setResTypecount2);
				Queue resTypequeue2 = QueueFactory.getQueue("TypeValue-queue");
				resTypequeue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", resTypeInString2));

				int setResTypecount3 = ++typeCount;
				String resTypeInString3 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"RESIDENTIAL", "3 BHK FLAT", "TT"+ setResTypecount3, "CC" + setResCount,companyId, setResTypecount3);
				Queue resTypequeue3 = QueueFactory.getQueue("TypeValue-queue");
				resTypequeue3.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", resTypeInString3));

				
				/**
				 * Category: COMMERCIAL & Its Type
				 */

				int setCommCount = ++count;
				String commCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"COMMERCIAL", "CC" + setCommCount, companyId,setCommCount, "Blank");
				Queue commCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				commCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", commCatInString));

				//Types	
				int setCommTypecount = ++typeCount;
				String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"COMMERCIAL", "BANK", "TT" + setCommTypecount,"CC" + setCommCount, companyId, setCommTypecount);
				Queue commTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				commTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));

				/**
				 * Category: INDUSTRIAL & Its Type
				 */

				int setIndusCount = ++count;
				String indusCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"INDUSTRIAL", "CC" + setIndusCount, companyId,setIndusCount, "Blank");
				Queue indusCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				indusCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", indusCatInString));
				
				//Types

				/**
				 * Category: SOCIETY & Its Type
				 */
				
				int setSocCount = ++count;
				String socCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"SOCIETY", "CC" + setSocCount, companyId,setSocCount, "Blank");
				Queue socCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				socCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", socCatInString));
				
				//Types

				/**
				 * Category: CONSTRUCTION & Its Type
				 */
				
				int setConsCount = ++count;
				String consCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"CONSTRUCTION", "CC" + setConsCount, companyId,setConsCount, "Blank");
				Queue consCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				consCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", consCatInString));

				//Types
				
				/**
				 * Category: OTHERS & Its Type
				 */
				
				int setOthCount = ++count;
				String configInstring = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"OTHERS", "CC" + setOthCount, companyId, setOthCount,"Blank");
				Queue othCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				othCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", configInstring));
				
				//Types

			}
			/**
			 * 
			 * 
			 * 
			 * 
			 *          3
			 * 
			 * 
			 * 
			 * 
			 * 
			 */			

			
			if ( categoryList().get(i).trim().equals("PRCATEGORY")
					|| categoryList().get(i).trim().equals("RFQCATEGORY")
					|| categoryList().get(i).trim().equals("LOICATEGORY")
					|| categoryList().get(i).trim().equals("POCATEGORY")) {
				
					
					/**
					 * Category: Chemicals & Its Type
					 */
					
					int setChemCount = ++count;
					String chemCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
							"Chemicals", "CC" + setChemCount, companyId,setChemCount, "Blank");
					Queue chemCatQueue = QueueFactory.getQueue("CategoryValue-queue");
					chemCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", chemCatInString));

					//Types
					int setChemTypeCount = ++typeCount;
					String chemTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
							"Chemicals", "Liquid", "TT"+ setChemTypeCount, "CC"+ setChemCount, companyId,setChemCount);
					Queue chemTypeQueue = QueueFactory.getQueue("TypeValue-queue");
					chemTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", chemTypeInString));

					/**
					 * Category: Solutions & Its Type
					 */
					int setSolcount = ++count;
					String solCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
							"Solutions", "CC" + setSolcount, companyId,setSolcount, "Blank");
					Queue solCatQueue = QueueFactory.getQueue("CategoryValue-queue");
					solCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", solCatInString));
					
					//Types
					int setSolTypeCount = ++typeCount;
					String solTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
							"Solutions", "Paste", "TT" + setSolTypeCount,"CC" + setSolcount, companyId, setSolTypeCount);
					Queue taskqueue = QueueFactory.getQueue("TypeValue-queue");
					taskqueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", solTypeInString));

			}
			
			if(categoryList().get(i).trim().equals("VENDORCATEGORY")){
				/**
				 * Category: Retailer & Its Type
				 */
				
				int setChemCount = ++count;
				String chemCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Chemicals", "CC" + setChemCount, companyId,setChemCount, "Blank");
				Queue chemCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				chemCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", chemCatInString));

				//vijay change its count
				//Types
				int setChemTypeCount = ++typeCount;
				String chemTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Chemicals", "Liquid", "TT"+ setChemTypeCount, "CC"+ setChemCount, companyId,setChemTypeCount);
				Queue chemTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				chemTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", chemTypeInString));

				/**
				 * Category: Solutions & Its Type
				 */
				int setSolcount = ++count;
				String solCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Solutions", "CC" + setSolcount, companyId,setSolcount, "Blank");
				Queue solCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				solCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", solCatInString));
				
				//Types
				int setSolTypeCount = ++typeCount;
				String solTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Solutions", "Paste", "TT" + setSolTypeCount,"CC" + setSolcount, companyId, setSolTypeCount);
				Queue taskqueue = QueueFactory.getQueue("TypeValue-queue");
				taskqueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", solTypeInString));
				
				
				int setRetcount = ++count;
				String solRetInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Retailer", "CC" + setRetcount, companyId,setRetcount, "Blank");
				Queue solRetQueue = QueueFactory.getQueue("CategoryValue-queue");
				solRetQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", solRetInString));
				
				//Types
				int setTypeCount1 = ++typeCount;
				String setTypeString1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Retailer", "Local", "TT" + setTypeCount1,"CC" + setRetcount, companyId, setTypeCount1);
				Queue taskqueue1 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString1));
				
				int setTypeCount2 = ++typeCount;
				String setTypeString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Retailer", "Regional", "TT" + setTypeCount2,"CC" + setRetcount, companyId, setTypeCount2);
				Queue taskqueue2 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString2));
				
				int setTypeCount3 = ++typeCount;
				String setTypeString3 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Retailer", "National", "TT" + setTypeCount3,"CC" + setRetcount, companyId, setTypeCount3);
				Queue taskqueue3 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue3.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString3));
				
				int setTypeCount4 = ++typeCount;
				String setTypeString4 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Retailer", "Herbal", "TT" + setTypeCount4,"CC" + setRetcount, companyId, setTypeCount4);
				Queue taskqueue4 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue4.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString4));
				
				int setTypeCount5 = ++typeCount;
				String setTypeString5 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Retailer", "Non Herbal", "TT" + setTypeCount5,"CC" + setRetcount, companyId, setTypeCount5);
				Queue taskqueue5 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue5.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString5));
				
				
				
				/**
				 * Category: Manufacturer & Its Type
				 */
				int setManCount = ++count;
				String solManInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manufacturer", "CC" + setManCount, companyId,setManCount, "Blank");
				Queue solManQueue = QueueFactory.getQueue("CategoryValue-queue");
				solManQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", solManInString));
				
				//Types
				int setTypeCount10 = ++typeCount;
				String setTypeString10 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manufacturer", "Local", "TT" + setTypeCount10,"CC" + setManCount, companyId, setTypeCount10);
				Queue taskqueue10 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue10.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString10));
				
				int setTypeCount11 = ++typeCount;
				String setTypeString11 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manufacturer", "Regional", "TT" + setTypeCount11,"CC" + setManCount, companyId, setTypeCount11);
				Queue taskqueue11 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue11.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString11));
				
				int setTypeCount12 = ++typeCount;
				String setTypeString12 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manufacturer", "National", "TT" + setTypeCount12,"CC" + setManCount, companyId, setTypeCount12);
				Queue taskqueue12 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue12.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString12));
				
				int setTypeCount13 = ++typeCount;
				String setTypeString13 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manufacturer", "Herbal", "TT" + setTypeCount13,"CC" + setManCount, companyId, setTypeCount13);
				Queue taskqueue13 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue13.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString13));
				
				int setTypeCount14 = ++typeCount;
				String setTypeString14 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manufacturer", "Non Herbal", "TT" + setTypeCount14,"CC" + setManCount, companyId, setTypeCount14);
				Queue taskqueue14 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue14.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString14));
				
				/**
				 * Category: Wholesaler & Its Type
				 */
				int setWsCount = ++count;
				String solWsInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Wholesaler", "CC" + setWsCount, companyId,setWsCount, "Blank");
				Queue solWsQueue = QueueFactory.getQueue("CategoryValue-queue");
				solWsQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", solWsInString));
				
				//Types
				int setTypeCount6 = ++typeCount;
				String setTypeString6 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Wholesaler", "Local", "TT" + setTypeCount6,"CC" + setWsCount, companyId, setTypeCount6);
				Queue taskqueue6 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue6.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString6));
				
				int setTypeCount7 = ++typeCount;
				String setTypeString7 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Wholesaler", "Regional", "TT" + setTypeCount7,"CC" + setWsCount, companyId, setTypeCount7);
				Queue taskqueue7 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue7.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString7));
				
				int setTypeCount8 = ++typeCount;
				String setTypeString8 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Wholesaler", "National", "TT" + setTypeCount8,"CC" + setWsCount, companyId, setTypeCount8);
				Queue taskqueue8 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue8.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString8));
				
				int setTypeCount9 = ++typeCount;
				String setTypeString9 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Wholesaler", "Herbal", "TT" + setTypeCount9,"CC" + setWsCount, companyId, setTypeCount9);
				Queue taskqueue9 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue9.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString9));
				
				int setTypeCount15 = ++typeCount;
				String setTypeString15 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Wholesaler", "Non Herbal", "TT" + setTypeCount15,"CC" + setWsCount, companyId, setTypeCount15);
				Queue taskqueue15 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue15.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString15));
				
				/**
				 * Category: Distributor & Its Type
				 */
				int setDisCount = ++count;
				String solDisInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "CC" + setDisCount, companyId,setDisCount, "Blank");
				Queue solDisQueue = QueueFactory.getQueue("CategoryValue-queue");
				solDisQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", solDisInString));
				
				//Types
				int setTypeCount16 = ++typeCount;
				String setTypeString16 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "Local", "TT" + setTypeCount16,"CC" + setDisCount, companyId, setTypeCount16);
				Queue taskqueue16 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue16.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString16));
				
				int setTypeCount17 = ++typeCount;
				String setTypeString17 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "Regional", "TT" + setTypeCount17,"CC" + setDisCount, companyId, setTypeCount17);
				Queue taskqueue17 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue17.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString17));
				
				int setTypeCount18 = ++typeCount;
				String setTypeString18 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "National", "TT" + setTypeCount18,"CC" + setDisCount, companyId, setTypeCount18);
				Queue taskqueue18 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue18.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString18));
				
				int setTypeCount19 = ++typeCount;
				String setTypeString19 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "Herbal", "TT" + setTypeCount19,"CC" + setDisCount, companyId, setTypeCount19);
				Queue taskqueue19 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue19.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString19));
				
				int setTypeCount20 = ++typeCount;
				String setTypeString20 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "Non Herbal", "TT" + setTypeCount20,"CC" + setDisCount, companyId, setTypeCount20);
				Queue taskqueue20 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue20.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString20));
			}
			if(categoryList().get(i).trim().equals("PRCATEGORY")){
				/**
				 * Category: Contract & Its Type
				 */
				int setContCount = ++count;
				String setCatContInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Contracts", "CC" + setContCount, companyId,setContCount, "Blank");
				Queue contCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				contCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatContInString));
				
				//Types
				int setTypeCount1 = ++typeCount;
				String setTypeString1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Contracts", "High Value", "TT" + setTypeCount1,"CC" + setContCount, companyId, setTypeCount1);
				Queue taskqueue1 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString1));
				
				int setTypeCount2 = ++typeCount;
				String setTypeString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Contracts", "Low Value", "TT" + setTypeCount2,"CC" + setContCount, companyId, setTypeCount2);
				Queue taskqueue2 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString2));
				
				int setTypeCount3 = ++typeCount;
				String setTypeString3 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Contracts", "Raw Materail", "TT" + setTypeCount3,"CC" + setContCount, companyId, setTypeCount3);
				Queue taskqueue3 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue3.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString3));
				
				int setTypeCount4 = ++typeCount;
				String setTypeString4 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Contracts", "Chemical", "TT" + setTypeCount4,"CC" + setContCount, companyId, setTypeCount4);
				Queue taskqueue4 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue4.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString4));
				
				int setTypeCount5 = ++typeCount;
				String setTypeString5 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Contracts", "Solutions", "TT" + setTypeCount5,"CC" + setContCount, companyId, setTypeCount5);
				Queue taskqueue5 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue5.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString5));
				
				/**
				 * Category: Distributed Contracts & Its Type
				 */
				int setDsCount = ++count;
				String setCatDsInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributed Contracts", "CC" + setDsCount, companyId,setDsCount, "Blank");
				Queue dsCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				dsCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatDsInString));
				
				//Types
				int setTypeCount6 = ++typeCount;
				String setTypeString6 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributed Contracts", "High Value", "TT" + setTypeCount6,"CC" + setDsCount, companyId, setTypeCount6);
				Queue taskqueue6 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue6.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString6));
				
				int setTypeCount7 = ++typeCount;
				String setTypeString7 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributed Contracts", "Low Value", "TT" + setTypeCount7,"CC" + setDsCount, companyId, setTypeCount7);
				Queue taskqueue7 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue7.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString7));
				
				int setTypeCount8 = ++typeCount;
				String setTypeString8 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributed Contracts", "Raw Materail", "TT" + setTypeCount8,"CC" + setDsCount, companyId, setTypeCount8);
				Queue taskqueue8 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue8.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString8));
				
				int setTypeCount9 = ++typeCount;
				String setTypeString9 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributed Contracts", "Chemical", "TT" + setTypeCount9,"CC" + setDsCount, companyId, setTypeCount9);
				Queue taskqueue9 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue9.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString9));
				
				int setTypeCount10 = ++typeCount;
				String setTypeString10 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributed Contracts", "Solutions", "TT" + setTypeCount10,"CC" + setDsCount, companyId, setTypeCount10);
				Queue taskqueue10 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue10.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString10));
				
				/**
				 * Category: Scheduling Agreement & Its Type
				 */
				int setSaCount = ++count;
				String setCatSaInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scheduling Agreement", "CC" + setSaCount, companyId,setSaCount, "Blank");
				Queue saCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				saCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatSaInString));
				
				//Types
				int setTypeCount11 = ++typeCount;
				String setTypeString11 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scheduling Agreement", "High Value", "TT" + setTypeCount11,"CC" + setSaCount, companyId, setTypeCount11);
				Queue taskqueue11 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue11.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString11));
				
				int setTypeCount12 = ++typeCount;
				String setTypeString12 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scheduling Agreement", "Low Value", "TT" + setTypeCount12,"CC" + setSaCount, companyId, setTypeCount12);
				Queue taskqueue12 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue12.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString12));
				
				int setTypeCount13 = ++typeCount;
				String setTypeString13 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scheduling Agreement", "Raw Materail", "TT" + setTypeCount13,"CC" + setSaCount, companyId, setTypeCount13);
				Queue taskqueue13 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue13.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString13));
				
				int setTypeCount14 = ++typeCount;
				String setTypeString14 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scheduling Agreement", "Chemical", "TT" + setTypeCount14,"CC" + setSaCount, companyId, setTypeCount14);
				Queue taskqueue14 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue14.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString14));
				
				int setTypeCount15 = ++typeCount;
				String setTypeString15 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Scheduling Agreement", "Solutions", "TT" + setTypeCount15,"CC" + setSaCount, companyId, setTypeCount15);
				Queue taskqueue15 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue15.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString15));
				
				/**
				 * Category: Outline Purchase Agreement & Its Type
				 */
				int setOlpaCount = ++count;
				String setCatOlpaInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Outline Purchase Agreement", "CC" + setOlpaCount, companyId,setOlpaCount, "Blank");
				Queue olpaCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				olpaCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatOlpaInString));
				
				//Types
				int setTypeCount16 = ++typeCount;
				String setTypeString16 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Outline Purchase Agreement", "High Value", "TT" + setTypeCount16,"CC" + setOlpaCount, companyId, setTypeCount16);
				Queue taskqueue16 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue16.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString16));
				
				int setTypeCount17 = ++typeCount;
				String setTypeString17 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Outline Purchase Agreement", "Low Value", "TT" + setTypeCount17,"CC" + setOlpaCount, companyId, setTypeCount17);
				Queue taskqueue17 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue17.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString17));
				
				int setTypeCount18 = ++typeCount;
				String setTypeString18 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Outline Purchase Agreement", "Raw Materail", "TT" + setTypeCount18,"CC" + setOlpaCount, companyId, setTypeCount18);
				Queue taskqueue18 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue18.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString18));
				
				int setTypeCount19 = ++typeCount;
				String setTypeString19 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Outline Purchase Agreement", "Chemical", "TT" + setTypeCount19,"CC" + setOlpaCount, companyId, setTypeCount19);
				Queue taskqueue19 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue19.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString19));
				
				int setTypeCount20 = ++typeCount;
				String setTypeString20 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Outline Purchase Agreement", "Solutions", "TT" + setTypeCount20,"CC" + setOlpaCount, companyId, setTypeCount20);
				Queue taskqueue20 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue20.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString20));
				
				/**
				 * Category: Periodic & Its Type
				 */
				int setPrCount = ++count;
				String setCatPrInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Periodic", "CC" + setPrCount, companyId,setPrCount, "Blank");
				Queue prCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				prCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatPrInString));
				
				//Types
				int setTypeCount21 = ++typeCount;
				String setTypeString21 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Periodic", "High Value", "TT" + setTypeCount21,"CC" + setPrCount, companyId, setTypeCount21);
				Queue taskqueue21 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue21.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString21));
				
				int setTypeCount22 = ++typeCount;
				String setTypeString22 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Periodic", "Low Value", "TT" + setTypeCount22,"CC" + setPrCount, companyId, setTypeCount22);
				Queue taskqueue22 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue22.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString22));
				
				int setTypeCount23 = ++typeCount;
				String setTypeString23 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Periodic", "Raw Materail", "TT" + setTypeCount23,"CC" + setPrCount, companyId, setTypeCount23);
				Queue taskqueue23 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue23.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString23));
				
				int setTypeCount24 = ++typeCount;
				String setTypeString24 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Periodic", "Chemical", "TT" + setTypeCount24,"CC" + setPrCount, companyId, setTypeCount24);
				Queue taskqueue24 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue24.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString24));
				
				int setTypeCount25 = ++typeCount;
				String setTypeString25 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Periodic", "Solutions", "TT" + setTypeCount25,"CC" + setPrCount, companyId, setTypeCount25);
				Queue taskqueue25 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue25.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString25));
				
				/**
				 * Category: Planned & Its Type
				 */
				int setPlanCount = ++count;
				String setCatPlanInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Planned", "CC" + setPlanCount, companyId,setPlanCount, "Blank");
				Queue planCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				planCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatPlanInString));
				
				//Types
				int setTypeCount26 = ++typeCount;
				String setTypeString26 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Planned", "High Value", "TT" + setTypeCount26,"CC" + setPlanCount, companyId, setTypeCount26);
				Queue taskqueue26 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue26.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString26));
				
				int setTypeCount27 = ++typeCount;
				String setTypeString27 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Planned", "Low Value", "TT" + setTypeCount27,"CC" + setPlanCount, companyId, setTypeCount27);
				Queue taskqueue27 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue27.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString27));
				
				int setTypeCount28 = ++typeCount;
				String setTypeString28 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Planned", "Raw Materail", "TT" + setTypeCount28,"CC" + setPlanCount, companyId, setTypeCount28);
				Queue taskqueue28 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue28.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString28));
				
				int setTypeCount29 = ++typeCount;
				String setTypeString29 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Planned", "Chemical", "TT" + setTypeCount29,"CC" + setPlanCount, companyId, setTypeCount29);
				Queue taskqueue29 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue29.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString29));
				
				int setTypeCount30 = ++typeCount;
				String setTypeString30 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Planned", "Solutions", "TT" + setTypeCount30,"CC" + setPlanCount, companyId, setTypeCount30);
				Queue taskqueue30 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue30.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString30));
				
				/**
				 * Category: Unplanned & Its Type
				 */
				int setUpCount = ++count;
				String setCatUpInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Unplanned", "CC" + setUpCount, companyId,setUpCount, "Blank");
				Queue upCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				upCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatUpInString));
				
				//Types
				int setTypeCount31 = ++typeCount;
				String setTypeString31 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Unplanned", "High Value", "TT" + setTypeCount31,"CC" + setUpCount, companyId, setTypeCount31);
				Queue taskqueue31 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue31.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString31));
				
				int setTypeCount32 = ++typeCount;
				String setTypeString32 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Unplanned", "Low Value", "TT" + setTypeCount32,"CC" + setUpCount, companyId, setTypeCount32);
				Queue taskqueue32 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue32.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString32));
				
				int setTypeCount33 = ++typeCount;
				String setTypeString33 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Unplanned", "Raw Materail", "TT" + setTypeCount33,"CC" + setUpCount, companyId, setTypeCount33);
				Queue taskqueue33 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue33.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString33));
				
				//vijay changed number
				int setTypeCount34= ++typeCount;
				String setTypeString34 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Unplanned", "Chemical", "TT" + setTypeCount34,"CC" + setUpCount, companyId, setTypeCount34);
				Queue taskqueue34 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue34.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString34));
				
				int setTypeCount35 = ++typeCount;
				String setTypeString35 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Unplanned", "Solutions", "TT" + setTypeCount35,"CC" + setUpCount, companyId, setTypeCount35);
				Queue taskqueue35 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue35.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString35));
			}
			if(categoryList().get(i).trim().equals("RFQCATEGORY")){
				/**
				 * Category: Public Sector & Its Type
				 */
				//vijay chnaged number
				int setPsCount = ++count;
				String setCatPsInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Public Sector", "CC" + setPsCount, companyId,setPsCount, "Blank");
				Queue psCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				psCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatPsInString));
				
				//Types
				int setTypeCount1 = ++typeCount;
				String setTypeString1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Public Sector", "High Value", "TT" + setTypeCount1,"CC" + setPsCount, companyId, setTypeCount1);
				Queue taskqueue1 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString1));
				
				int setTypeCount2 = ++typeCount;
				String setTypeString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Public Sector", "Low Value", "TT" + setTypeCount2,"CC" + setPsCount, companyId, setTypeCount2);
				Queue taskqueue2 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString2));
				
				int setTypeCount3 = ++typeCount;
				String setTypeString3 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Public Sector", "Urgent", "TT" + setTypeCount3,"CC" + setPsCount, companyId, setTypeCount3);
				Queue taskqueue3 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue3.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString3));
				
				/**
				 * Category: Private Sector & Its Type
				 */
				// vijay changed here
				int setPrsCount = ++count;
				String setCatPrsInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Private Sector", "CC" + setPrsCount, companyId,setPrsCount, "Blank");
				Queue prsCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				prsCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatPrsInString));
				
				//Types
				int setTypeCount4 = ++typeCount;
				String setTypeString4 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Private Sector", "High Value", "TT" + setTypeCount4,"CC" + setPrsCount, companyId, setTypeCount4);
				Queue taskqueue4 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue4.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString4));
				
				int setTypeCount5 = ++typeCount;
				String setTypeString5 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Private Sector", "Low Value", "TT" + setTypeCount5,"CC" + setPrsCount, companyId, setTypeCount5);
				Queue taskqueue5 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue5.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString5));
				
				int setTypeCount6 = ++typeCount;
				String setTypeString6 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Private Sector", "Urgent", "TT" + setTypeCount6,"CC" + setPrsCount, companyId, setTypeCount6);
				Queue taskqueue6 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue6.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString6));
				
				/**
				 * Category: Internal & Its Type
				 */
				int setInCount = ++count;
				String setCatInInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal", "CC" + setInCount, companyId,setInCount, "Blank");
				Queue inCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				inCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatInInString));
				
				//Types
				int setTypeCount7 = ++typeCount;
				String setTypeString7 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal", "High Value", "TT" + setTypeCount7,"CC" + setInCount, companyId, setTypeCount7);
				Queue taskqueue7 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue7.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString7));
				
				int setTypeCount8 = ++typeCount;
				String setTypeString8 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal", "Low Value", "TT" + setTypeCount8,"CC" + setInCount, companyId, setTypeCount8);
				Queue taskqueue8 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue8.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString8));
				
				int setTypeCount9 = ++typeCount;
				String setTypeString9 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Internal", "Urgent", "TT" + setTypeCount9,"CC" + setInCount, companyId, setTypeCount9);
				Queue taskqueue9 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue9.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString9));
				
				/**
				 * Category: External & Its Type
				 */
				int setExCount = ++count;
				String setCatExInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External", "CC" + setExCount, companyId,setExCount, "Blank");
				Queue exCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				exCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", setCatExInString));
				
				//Types
				int setTypeCount10 = ++typeCount;
				String setTypeString10 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External", "High Value", "TT" + setTypeCount10,"CC" + setExCount, companyId, setTypeCount10);
				Queue taskqueue10 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue10.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString10));
				
				int setTypeCount11 = ++typeCount;
				String setTypeString11 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External", "Low Value", "TT" + setTypeCount11,"CC" + setExCount, companyId, setTypeCount11);
				Queue taskqueue11 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue11.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString11));
				
				int setTypeCount12 = ++typeCount;
				String setTypeString12 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"External", "Urgent", "TT" + setTypeCount12,"CC" + setExCount, companyId, setTypeCount12);
				Queue taskqueue12 = QueueFactory.getQueue("TypeValue-queue");
				taskqueue12.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", setTypeString12));
			}
			/**
			 * 
			 * 
			 * 
			 * 
			 *          4
			 * 
			 * 
			 * 
			 * 
			 * 
			 */			

			if (categoryList().get(i).trim().equals("SALESQUOTATIONCATEGORY")
					|| categoryList().get(i).trim().equals("SALESORDERCATEGORY")
					|| categoryList().get(i).trim().equals("DELIVERYNOTECATEGORY")) {
				
				
				/**
				 * Category: Distributor & Its Type
				 */
				int setDisCount = ++count;
				String disCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "CC" + setDisCount, companyId,setDisCount, "Blank");
				Queue disCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				disCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", disCatInString));
				
				//Types
				int setDisTypecount1 = ++typeCount;
				String disTypeInString1 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Distributor", "New", "TT" + setDisTypecount1, "CC"+ setDisCount, companyId, setDisTypecount1);
				Queue disTypeQueue1 = QueueFactory.getQueue("TypeValue-queue");
				disTypeQueue1.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", disTypeInString1));

				int setDisTypecount2 = ++typeCount;
				String disTypeInString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE),
						"Distributor", "Priority","TT" + setDisTypecount2, "CC" + setDisCount,companyId, setDisTypecount2);
				Queue disTypeQueue2 = QueueFactory.getQueue("TypeValue-queue");
				disTypeQueue2.add(TaskOptions.Builder.withUrl("/slick_erp/leaddatataskqueue").param("typeValuekey", disTypeInString2));

				/**
				 * Category: Reseller & Its Type
				 */
				int setReseCount = ++count;
				String reseCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Reseller", "CC" + setReseCount, companyId,setReseCount, "Blank");
				Queue reseCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				reseCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", reseCatInString));
				
				//Types
				int setReseTypeCount1 = ++typeCount;
				String reseTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Reseller", "Existing", "TT" + setReseTypeCount1,"CC" + setReseCount, companyId, setReseTypeCount1);
				Queue typequeue = QueueFactory.getQueue("TypeValue-queue");
				typequeue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", reseTypeInString));

				int setReseTypeCount2 = ++typeCount;
				String reseTypeInString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE),
						"Reseller", "Non Priority", "TT"+ setReseTypeCount2, "CC" + setReseCount,companyId, setReseTypeCount2);
				Queue typequeue2 = QueueFactory.getQueue("TypeValue-queue");
				typequeue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", reseTypeInString2));


			}
			/**
			 * 
			 * 
			 * 
			 * 
			 *          5
			 * 
			 * 
			 * 
			 * 
			 * 
			 */		
			if (categoryList().get(i).trim().equals("ADDPAYMENTTERMS")) {
				
				/**
				 * Category: Monthly
				 */
				int setMonthlycount = ++count;
				String monthlyCatnString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Monthly", "CC" + setMonthlycount, companyId,setMonthlycount, "30");
				Queue monthCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				monthCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", monthlyCatnString));

				/**
				 * Category: Yearly
				 */
				int setYearlyCount = ++count;
				String yearlyCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Yearly", "CC" + setYearlyCount, companyId, setYearlyCount,"365");
				Queue yearlyCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				yearlyCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", yearlyCatInString));
			
				/**
				 * Category: Quarterly
				 */

				int setQuarterlyCount = ++count;
				String quarterlyCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Quarterly", "CC" + setQuarterlyCount, companyId,setQuarterlyCount, "90");
				Queue quarterlyCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				quarterlyCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", quarterlyCatInString));
			
				/**
				 * Category: Half Yearly
				 */

				int setHfCount = ++count;
				String hfCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Half Yearly", "CC" + setHfCount, companyId,setHfCount, "180");
				Queue hfCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				hfCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", hfCatInString));
			}
			/**
			 * 
			 * 
			 * 
			 * 
			 *          6
			 * 
			 * 
			 * 
			 * 
			 * 
			 */		
			if (categoryList().get(i).trim().equals("EXPENSECATEGORY")) {

				/**
				 * Category: Transport and its Type
				 */
				
				int setTranCount = ++count;
				String transCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Transport", "CC" + setTranCount, companyId,setTranCount, "Blank");
				Queue transCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				transCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", transCatInString));
				
				//Types
				
				int setTransTypeCount = ++typeCount;
				String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.EXPENSETYPE),
						"Transport", "Petrol", "TT" + setTransTypeCount,"CC" + setTranCount, companyId, setTransTypeCount);
				Queue transTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				transTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));

				
				/**
				 * Category: Other and its Type
				 */
				int setOtherCount = ++count;
				String otherCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Other", "CC" + setOtherCount + i, companyId,setOtherCount, "Blank");
				Queue otherCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				otherCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", otherCatInString));
				
				//Types

				
				/**
				 * Category: Administrative Cost and its Type
				 */

				int setAdmCount = ++count;
				String admCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Administrative Cost", "CC" + setAdmCount,companyId, setAdmCount, "Blank");
				Queue admCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				admCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", admCatInString));
				
				//Types
				
				int admTypeCount = ++typeCount;
				String admTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.EXPENSETYPE),
						"Administrative Cost", "Water Bottels", "TT"+ admTypeCount, "CC" + setAdmCount,companyId, admTypeCount);
				Queue admTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				admTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", admTypeInString));

			}
			/**
			 * 
			 * 
			 * 
			 * 
			 *          7
			 * 
			 * 
			 * 
			 * 
			 * 
			 */		
			if (categoryList().get(i).trim().equals("DECLARATION")) {
				/**
				 * Category: DECLARATION
				 */
				int setDecCount = ++count;
				String decCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.DECLARATION),
						"DECLARATION","CC" + setDecCount,companyId,setDecCount,
						"By signing below,I the AFAS accredited fumigator responsible, declare that these details are true and correct and the fumigation has been carried out in accordance with all the requirements in the AQIS Methyl Bromide Fumigation Standard.");
				Queue decCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				decCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", decCatInString));
			}

			/**
			 * 
			 * 
			 * 
			 * 
			 *          8
			 * 
			 * 
			 * 
			 * 
			 * 
			 */		

			if (categoryList().get(i).trim().equals("INSPECTIONCATEGORY")) {
						
				/**
				 * Category: Manual and its Type
				 */

				int setManualCount = ++count;
				String manualCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Manual", "CC" + setManualCount, companyId, setManualCount,"Blank");
				Queue manualCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				manualCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", manualCatInString));

				//Types
				int setManualTypeCount = ++typeCount;
				String manualTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.INSPECTIONTYPE),
						"Manual", "On Site", "TT" + setManualTypeCount, "CC"+ setManualCount, companyId, setManualTypeCount);
				Queue manualTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				manualTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", manualTypeInString));

				
				/**
				 * Category: Random Inspection and its Type
				 */

				int setRandCount = ++count;
				String randCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Random Inspection", "CC" + setRandCount,companyId, setRandCount, "Blank");
				Queue randCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				randCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", randCatInString));
				
				//Types
				int setRandTypeCount = ++typeCount;
				String randTypeInString = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.INSPECTIONTYPE),
						"Random Inspection", "On Site", "TT"+ setRandTypeCount, "CC" + setRandCount,companyId, setRandTypeCount);
				Queue randTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				randTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", randTypeInString));
				
				/**
				 * Category: Conatiner Loading Inspection and its Type
				 */
				
				int setContCount = ++count;
				String contCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Conatiner Loading Inspection","CC" + setContCount, companyId, setContCount, "Blank");
				Queue contCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				contCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", contCatInString));
				
				//Types
				
			
				/**
				 * Category: Automatic and its Type
				 */
				int setAutCount = ++count;
				String autoCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Automatic", "CC" + setAutCount, companyId,setAutCount, "Blank");
				Queue autoCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				autoCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", autoCatInString));
				
				//Types
			}
			/**
			 * 
			 * 
			 * 
			 * 
			 *          9
			 * 
			 * 
			 * 
			 * 
			 * 
			 */		
			if (categoryList().get(i).trim().equals("WORKORDERCATEGORY")) {
						
				/**
				 * Category: Pest Control and its Type
				 */

				int setPcCount = ++count;
				String pcCatInString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Pest Control", "CC" + setPcCount, companyId,setPcCount, "Blank");
				Queue pcCatQueue = QueueFactory.getQueue("CategoryValue-queue");
				pcCatQueue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", pcCatInString));

				//Types
				int pcTypeCount = ++typeCount;
				String pcTypeIString = getTypeDataForTaskQueue(
						CategoryTypes.getCategoryInternalType(Screen.WORKORDERTYPE),
						"Pest Control", "Rework", "TT" + pcTypeCount,"CC" + setPcCount, companyId, pcTypeCount);
				Queue pcTypeQueue = QueueFactory.getQueue("TypeValue-queue");
				pcTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", pcTypeIString));

				int pcTypeCount1 = ++typeCount;
				String pcTypeInString2 = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.WORKORDERTYPE),
						"Pest Control", "Installation", "TT"+ pcTypeCount1, "CC" + setPcCount,companyId, pcTypeCount1);
				Queue pcTypeQueue2 = QueueFactory.getQueue("TypeValue-queue");
				pcTypeQueue2.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", pcTypeInString2));

				
				/**
				 * Category: Other and its Type
				 */

				int setcount = ++count;
				String configInstring = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr1),
						"Other", "CC" + setcount, companyId, setcount,"Blank");
				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", configInstring));
				
				//Types
				int typecount = ++typeCount;
				String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.WORKORDERTYPE),
						"Other", "Installation", "TT" + typecount, "CC"+ setcount, companyId, typecount);
				Queue typequeue = QueueFactory.getQueue("TypeValue-queue");
				typequeue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));

			}

		}
		numbergen.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);

		numbergenration.setNumber(typeCount);
		GenricServiceImpl impl2 = new GenricServiceImpl();
		impl2.save(numbergenration);

	}
		
		
	
	public void setPestControlType(Long companyId)
	{
		
//		NumberGeneration numbergen = null;	
//		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "ConfigCategory").filter("status", true).first().now();
//		long lastNumber = numbergen.getNumber();
//		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
//    	 int count = (int) lastNumber;
//		
//		for(int i=0;i<typeList().size();i++){
			
//			if(typeList().get(i).trim().equals("INSPECTIONTYPE"))
//			{
//				for(int j=0;j<2;j++)
//				{
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Distributor", "Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.INSPECTIONTYPE), "Manual", "On Site", "TT"+setcount,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else{
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.INSPECTIONTYPE), "Random Inspection", "On Site", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
//			}
			
		
			
//			if(typeList().get(i).trim().equals("EXPENSETYPE"))
//			{
//				for(int j=0;j<2;j++)
//				{
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Distributor", "Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.EXPENSETYPE), "Transport", "Petrol", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					if(j==1){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.EXPENSETYPE), "Administrative Cost", "Water Bottels", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					
////					if(j==2){
//////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
//////						saveConfig(types);
////						
////						
////						int setcount = ++count;
////						
////						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.EXPENSETYPE), "Administrative Cost", "Water Bottels", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
////						System.out.println(" vijay  string value"+typeInstring);
////						
////						Queue queue = QueueFactory.getQueue("TypeValue-queue");
////						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
////					}
//				}
//			}
			
			
//			if(typeList().get(i).trim().equals("WORKORDERTYPE"))
//			{
//				for(int j=0;j<3;j++)
//				{
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Distributor", "Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.WORKORDERTYPE), "Pest Control", "Rework", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					if(j==1){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.WORKORDERTYPE), "Pest Control", "Installation", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					
//					if(j==2){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.WORKORDERTYPE), "Other", "Installation", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
//			}
			
//			if(typeList().get(i).trim().equals("GRNTYPE")||
//					typeList().get(i).trim().equals("MRNTYPE")||
//					typeList().get(i).trim().equals("MINTYPE")||
//					typeList().get(i).trim().equals("MMNTYPE")
//					){
//				
//				
//				for(int j=0;j<2;j++)
//				{
//					Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Spray", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Spray", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else{
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Injection", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Injection", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
//			}
			
			
			
//			if(typeList().get(i).trim().equals("CUSTOMERTYPE")||
//					typeList().get(i).trim().equals("LEADTYPE")||
//					typeList().get(i).trim().equals("QUOTATIONTYPE")||
//					typeList().get(i).trim().equals("CONTRACTTYPE")){
//				
//				for(int j=0;j<5;j++)
//				{
//					Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "1 BHK FLAT", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "1 BHK FLAT", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else if(j==1){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "2 BHK FLAT", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "2 BHK FLAT", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else if(j==2){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "3 BHK FLAT", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "3 BHK FLAT", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else if(j==3){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "COMMERCIAL", "BANK", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "COMMERCIAL", "BANK", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else{
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "COMMERCIAL", "OFFICES", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "COMMERCIAL" , "OFFICES", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
			}
			
//			if(typeList().get(i).trim().equals("SALESQUOTATIONTYPE")||typeList().get(i).trim().equals("SALESORDERTYPE"))
//			{
//				for(int j=0;j<2;j++)
//				{
//					Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Distributor", "New", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "Distributor", "New", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else{
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Reseller", "Existing", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "Reseller", "Existing", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
//			}
			
//			if(typeList().get(i).trim().equals("DELIVERYNOTETYPE"))
//			{
//				for(int j=0;j<2;j++)
//				{
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Distributor", "Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Distributor", "Priority", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/leaddatataskqueue").param("typeValuekey", typeInstring));
//					}
//					else{
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(Screen.DELIVERYNOTETYPE), "Reseller", "Non Priority", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
//			}
			
//			if(typeList().get(i).trim().equals("VENDORTYPE")||typeList().get(i).trim().equals("PRTYPE")||
//					typeList().get(i).trim().equals("RFQTYPE")||
//					typeList().get(i).trim().equals("LOITYPE")||
//					typeList().get(i).trim().equals("POTYPE"))
//			{
//				for(int j=0;j<2;j++)
//				{
//					Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					else{
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				}
//			}
			
			
//			if(typeList().get(i).trim().equals("INVOICECONFIGTYPE")||typeList().get(i).trim().equals("BILLINGTYPE"))
//			{
//				for(int j=0;j<2;j++)
//				{
//					Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "COMMERCIAL", "NEW", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					if(j==1){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "COMMERCIAL", "OLD", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				
//				}
//				
//				
//				for(int j=0;j<3;j++)
//				{
//					Screen scr2 = commonForAllTypes(typeList().get(i).trim());
//					if(j==0){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Chemicals", "Liquid", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "NEW", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//					if(j==1){
////						Type types = getTypesData(CategoryTypes.getCategoryInternalType(scr2), "Solutions", "Paste", "TT"+10000000+i,"CC"+10000000+i, companyId);
////						saveConfig(types);
//						
//						int setcount = ++count;
//						
//						String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(scr2), "RESIDENCIAL", "OLD", "TT"+10000000+i,"CC"+10000000+i, companyId,setcount);
//						System.out.println(" vijay  string value"+typeInstring);
//						
//						Queue queue = QueueFactory.getQueue("TypeValue-queue");
//						queue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
//					}
//				
//				}
//			}
//			
//		}
	
//		 numbergen.setNumber(count);
//   	     GenricServiceImpl impl = new GenricServiceImpl();
//		 impl.save(numbergen);
//	}
	
	
	
	/******************************ArrayLists & Screen Names Identification*******************************/
	
/***************************************** Other ArrayList *********************************/
	
	public ArrayList<String> otherList()
	{
		ArrayList<String> otherArray=new ArrayList<String>();
		
		otherArray.add("COUNTRY");
		otherArray.add("STATE");
		otherArray.add("CITY");
		otherArray.add("LOCALITY");
		otherArray.add("ARTICLETYPE");
		otherArray.add("SERVICETYPE");
		otherArray.add("PAYMENTMETHODS");
		otherArray.add("BRANCH");
		otherArray.add("EMPLOYEE");
		otherArray.add("CUSTOMER");
		otherArray.add("EMPLOYEEROLE");
		otherArray.add("EMPLOYEEDESIGNATION");
		otherArray.add("EMPLOYEETYPE");
		otherArray.add("DEPARTMENT");
		otherArray.add("COMPANYTYPE");
		
		return otherArray;
	}
	
	/***************************************** Config ArrayList *********************************/
	
	public ArrayList<String> configList()
	{
		ArrayList<String> configArray=new ArrayList<String>();
		
		configArray.add("CUSTOMERGROUP");
		configArray.add("LEADGROUP");
		configArray.add("SALESQUOTATIONGROUP");
		configArray.add("SALESORDERGROUP");
		configArray.add("DELIVERYNOTEGROUP");
		configArray.add("QUOTATIONGROUP");
		configArray.add("CONTRACTGROUP");
		configArray.add("PRGROUP");
		configArray.add("VENDORGROUP");
		configArray.add("GRNGROUP");
		configArray.add("CUSTOMERPRIRORITY");
		configArray.add("LEADPRIORITY");
		configArray.add("SALESQUOTATIONPRIORITY");
		configArray.add("QUOTATIONPRIORITY");
		//  rohan added this code for modifications Date : 21/11/2016
		configArray.add("CUSTOMERLEVEL");
		configArray.add("LEADSTATUS");
		configArray.add("COMPLAINSTATUS");
		configArray.add("COMPLAINPRIORITY");
		configArray.add("INTERACTIONPRIORITY");
		configArray.add("GROUPTYPE");
		configArray.add("GLACCOUNTGROUP");
		configArray.add("EMPLOYEEROLE");
		configArray.add("WORKORDERGROUP");
		configArray.add("EMPLOYEEDESIGNATION");
		configArray.add("EMPLOYEETYPE");
		configArray.add("ARTICLETYPE");
		configArray.add("SERVICETYPE");
		configArray.add("PAYMENTMETHODS");
		configArray.add("COMPANYTYPE");
		configArray.add("FUMIGATIONDECLAREDPOE");
		configArray.add("DOSAGERATEOFFUMIGANT");
		configArray.add("DURATIONOFFUMIGATION");
		configArray.add("MINIMUMAIRTEMP");
		configArray.add("QUANTITYDECLAREUNIT");
		configArray.add("PLACEOFFUMIGATION");
		configArray.add("COUNTRYOFLOADING");
		configArray.add("DECLARATION");
		configArray.add("NUMBERRANGE");
		configArray.add("CATCHTRAPPESTNAME");
		
		configArray.add("PERSONTYPE");
		configArray.add("PROCESSTYPE");
		configArray.add("ASSETCATEGORY");
		configArray.add("CLIENTSIDEASSETCATEGORY");
		configArray.add("APPROVALLEVEL");
		configArray.add("CURRENCY");
		configArray.add("TDSPERCENTAGE");
		configArray.add("CONTACTROLE");
		configArray.add("BILLINGGROUP");
		configArray.add("INVOICEGROUP");
		configArray.add("SPECIFICREASONFORRESCHEDULING");
		configArray.add("COMMUNICATIONTYPE");
		configArray.add("COMMUNICATIONCATEGORY");
		
		
		configArray.add("INSPECTIONGROUP");
		configArray.add("ASSETARTICLETYPE");
		configArray.add("UNITOFMEASUREMENT");
		configArray.add("PRODUCTGROUP");
		configArray.add("PRODUCTTYPE");
		configArray.add("PRODUCTCLASSIFICATION");
		configArray.add("EXPENSEGROUP");
		configArray.add("MMNDIRECTION");
		
		
		return configArray;
	}
	
	/***************************************** Category ArrayList *********************************/
	
	public static ArrayList<String> categoryList(){
		
		ArrayList<String> categoryArray = new ArrayList<String>();
		
		categoryArray.add("CUSTOMERCATEGORY");
		categoryArray.add("LEADCATEGORY");
		categoryArray.add("SALESQUOTATIONCATEGORY");
		categoryArray.add("SALESORDERCATEGORY");
		categoryArray.add("DELIVERYNOTECATEGORY");
		categoryArray.add("QUOTATIONCATEGORY");
		categoryArray.add("CONTRACTCATEGORY");
		categoryArray.add("VENDORCATEGORY");
		categoryArray.add("PRCATEGORY");
		categoryArray.add("RFQCATEGORY");
		categoryArray.add("LOICATEGORY");
		categoryArray.add("POCATEGORY");
		categoryArray.add("GRNCATEGORY");
		categoryArray.add("MRNCATEGORY");
		categoryArray.add("MINCATEGORY");
		categoryArray.add("MMNCATEGORY");
		categoryArray.add("ADDPAYMENTTERMS");
		categoryArray.add("DECLARATION");
		categoryArray.add("BILLINGCATEGORY");
		categoryArray.add("INVOICECATEGORY");
		categoryArray.add("COMMUNICATIONCATEGORY");
		categoryArray.add("INSPECTIONCATEGORY");
		categoryArray.add("WORKORDERCATEGORY");
		categoryArray.add("EXPENSECATEGORY");
		
		return categoryArray;
	}
	
	/***************************************** Type ArrayList *********************************/
	
	public static ArrayList<String> typeList(){
		
		ArrayList<String> typeArray = new ArrayList<String>();
		
		typeArray.add("CUSTOMERTYPE");
		typeArray.add("LEADTYPE");
		typeArray.add("SALESQUOTATIONTYPE");
		typeArray.add("SALESORDERTYPE");
		typeArray.add("DELIVERYNOTETYPE");
		typeArray.add("QUOTATIONTYPE");
		typeArray.add("CONTRACTTYPE");
		typeArray.add("VENDORTYPE");
		typeArray.add("PRTYPE");
		typeArray.add("RFQTYPE");
		typeArray.add("LOITYPE");
		typeArray.add("POTYPE");
		typeArray.add("GRNTYPE");
		typeArray.add("MRNTYPE");
		typeArray.add("MINTYPE");
		typeArray.add("MMNTYPE");
		
		typeArray.add("INVOICECONFIGTYPE");
		typeArray.add("BILLINGTYPE");
		typeArray.add("COMMUNICATIONTYPE");
		typeArray.add("INSPECTIONTYPE");
		typeArray.add("WORKORDERTYPE");
		typeArray.add("EXPENSETYPE");
		
		
		return typeArray;
	}
	
   /*****************************************Product ArrayList******************************************/
	
	public ArrayList<String> productListData()
	{
		ArrayList<String> productArray=new ArrayList<String>();
		
		productArray.add("ITEMCATEGORY");
		productArray.add("SERVICECATEGORY");
		productArray.add("UOM");
//		productArray.add("PRODGROUP");
		productArray.add("PRODTYPE");
		productArray.add("PRODCLASSIFICATION");
//		productArray.add("GLACCOUNTGROUP");
		productArray.add("GLACCOUNT");
		productArray.add("TAXDETAILS");
		productArray.add("PRODCLASSIFICATION");
		productArray.add("ITEMPRODUCT");
		productArray.add("SERVICEPRODUCT");
		
		return productArray;
	}
	
	/*******************************************Inventory ArrayList**************************************/
	
	public ArrayList<String> inventoryListData()
	{
		ArrayList<String> inventoryArray=new ArrayList<String>();
		
		inventoryArray.add("WAREHOUSE");
		inventoryArray.add("STORAGELOCATION");
		inventoryArray.add("STORAGEBIN");
//		inventoryArray.add("DIRECTION");
		inventoryArray.add("ENTITYTYPE");
		inventoryArray.add("TRANSACTIONTYPE");
		
		return inventoryArray;
	}
	
	/****************************************Default Configs*********************************************/
	
	public ArrayList<String> defaultConfigsListData()
	{
		ArrayList<String> defaultConfigArray=new ArrayList<String>();
		defaultConfigArray.add("PROCESSTYPE");
		defaultConfigArray.add("MODULENAME");
		defaultConfigArray.add("DOCUMENTNAME");
		
		return defaultConfigArray;	
	}
	
	/****************************************************************************************************/
	/****************************************************************************************************/
	
	public Screen commonForAllConfigs(String typeOfConfig){
		
		Screen scr = null;
		if(typeOfConfig.equals("CUSTOMERGROUP")){
			scr = Screen.CUSTOMERGROUP;
		}
		if(typeOfConfig.equals("LEADGROUP")){
			scr = Screen.LEADGROUP;
		}
		if(typeOfConfig.equals("SALESQUOTATIONGROUP")){
			scr = Screen.SALESQUOTATIONGROUP;
		}
		if(typeOfConfig.equals("SALESORDERGROUP")){
			scr = Screen.SALESORDERGROUP;
		}
		if(typeOfConfig.equals("DELIVERYNOTEGROUP")){
			scr = Screen.DELIVERYNOTEGROUP;
		}
		if(typeOfConfig.equals("QUOTATIONGROUP")){
			scr = Screen.QUOTATIONGROUP;
		}
		if(typeOfConfig.equals("CONTRACTGROUP")){
			scr = Screen.CONTRACTGROUP;
		}
		if(typeOfConfig.equals("VENDORGROUP")){
			scr = Screen.VENDORGROUP;
		}
		if(typeOfConfig.equals("PRGROUP")){
			scr = Screen.PRGROUP;
		}
		if(typeOfConfig.equals("GRNGROUP")){
			scr = Screen.GRNGROUP;
		}
		
		if(typeOfConfig.equals("CUSTOMERPRIRORITY")){
			scr = Screen.CUSTOMERPRIORITY;
		}
		
		if(typeOfConfig.equals("LEADPRIORITY")){
			scr = Screen.LEADPRIORITY;
		}
		
		if(typeOfConfig.equals("SALESQUOTATIONPRIORITY")){
			scr = Screen.SALESQUOTATIONPRIORITY;
		}
		if(typeOfConfig.equals("QUOTATIONPRIORITY")){
			scr = Screen.QUOTATIONPRIORITY;
		}
		
		if(typeOfConfig.equals("COMPLAINPRIORITY")){
			scr = Screen.COMPLAINPRIORITY;
		}
		
		if(typeOfConfig.equals("INTERACTIONPRIORITY")){
			scr = Screen.INTERACTIONPRIORITY;
		}
		
		if(typeOfConfig.equals("INVOICEGROUP")){
			scr = Screen.INVOICEGROUP;
		}
		if(typeOfConfig.equals("BILLINGGROUP")){
			scr = Screen.BILLINGGROUP;
		}
		
		if(typeOfConfig.equals("INSPECTIONGROUP")){
			scr = Screen.INSPECTIONGROUP;
		}
		
		if(typeOfConfig.equals("WORKORDERGROUP")){
			scr = Screen.WORKORDERGROUP;
		}
		
		if(typeOfConfig.equals("UNITOFMEASUREMENT")){
			scr = Screen.UNITOFMEASUREMENT;
		}
		if(typeOfConfig.equals("PRODUCTGROUP")){
			scr = Screen.PRODUCTGROUP;
		}
		if(typeOfConfig.equals("PRODUCTTYPE")){
			scr = Screen.PRODUCTTYPE;
		}
		if(typeOfConfig.equals("PRODUCTCLASSIFICATION")){
			scr = Screen.PRODUCTCLASSIFICATION;
		}
		if(typeOfConfig.equals("COMMUNICATIONTYPE")){
			scr = Screen.COMMUNICATIONTYPE;
		}
		
		if(typeOfConfig.equals("COMMUNICATIONCATEGORY")){
			scr = Screen.COMMUNICATIONCATEGORY;
		}
		if(typeOfConfig.equals("EXPENSEGROUP")){
			scr = Screen.EXPENSEGROUP;
		}
		
		if(typeOfConfig.equals("MMNDIRECTION")){
			scr = Screen.MMNDIRECTION;
		}
		
		
		
		return scr;
	}
	
	
	
	public static Screen commonForAllCategory(String typeOfCategory){
		Screen scr1 = null;
		
		if(typeOfCategory.equals("CUSTOMERCATEGORY")){
			scr1 = Screen.CUSTOMERCATEGORY;
		}
		if(typeOfCategory.equals("LEADCATEGORY")){
			scr1 = Screen.LEADCATEGORY;
		}
		if(typeOfCategory.equals("SALESQUOTATIONCATEGORY")){
			scr1 = Screen.SALESQUOTATIONCATEGORY;
		}
		
		if(typeOfCategory.equals("SALESORDERCATEGORY")){
			scr1 = Screen.SALESORDERCATEGORY;
		}
		if(typeOfCategory.equals("DELIVERYNOTECATEGORY")){
			scr1 = Screen.DELIVERYNOTECATEGORY;
		}
		if(typeOfCategory.equals("QUOTATIONCATEGORY")){
			scr1 = Screen.QUOTATIONCATEGORY;
		}
		if(typeOfCategory.equals("CONTRACTCATEGORY")){
			scr1 = Screen.CONTRACTCATEGORY;
		}
		if(typeOfCategory.equals("VENDORCATEGORY")){
			scr1 = Screen.VENDORCATEGORY;
		}
		if(typeOfCategory.equals("PRCATEGORY")){
			scr1 = Screen.PRCATEGORY;
		}
		if(typeOfCategory.equals("RFQCATEGORY")){
			scr1 = Screen.RFQCATEGORY;
		}
		if(typeOfCategory.equals("LOICATEGORY")){
			scr1 = Screen.LOICATEGORY;
		}
		if(typeOfCategory.equals("POCATEGORY")){
			scr1 = Screen.PURCHASEORDERCATEGORY;
		}
		if(typeOfCategory.equals("GRNCATEGORY")){
			scr1 = Screen.GRNCATEGORY;
		}
		if(typeOfCategory.equals("MRNCATEGORY")){
			scr1 = Screen.MRNCATEGORY;
		}
		if(typeOfCategory.equals("MINCATEGORY")){
			scr1 = Screen.MINCATEGORY;
		}
		if(typeOfCategory.equals("MMNCATEGORY")){
			scr1 = Screen.MMNCATEGORY;
		}
		if(typeOfCategory.equals("ADDPAYMENTTERMS")){
			scr1 = Screen.ADDPAYMENTTERMS;
		}
		
		if(typeOfCategory.equals("BILLINGCATEGORY")){
			scr1 = Screen.BILLINGCATEGORY;
		}
		if(typeOfCategory.equals("BILLINGCATEGORY")){
			scr1 = Screen.BILLINGCATEGORY;
		}
		
		if(typeOfCategory.equals("INVOICECATEGORY")){
			scr1 = Screen.INVOICECATEGORY;
		}
		
//		if(typeOfCategory.equals("COMMUNICATIONCATEGORY")){
//			scr1 = Screen.COMMUNICATIONCATEGORY;
//		}
		
		if(typeOfCategory.equals("INSPECTIONCATEGORY")){
			scr1 = Screen.INSPECTIONCATEGORY;
		}
		
		if(typeOfCategory.equals("WORKORDERCATEGORY")){
			scr1 = Screen.WORKORDERCATEGORY;
		}
		
		if(typeOfCategory.equals("EXPENSECATEGORY")){
			scr1 = Screen.EXPENSECATEGORY;
		}
		
		return scr1;
	}
	
	
	public static Screen commonForAllTypes(String typeOfTypes){
		Screen scr2 = null;
		
		if(typeOfTypes.equals("CUSTOMERTYPE")){
			scr2 = Screen.CUSTOMERTYPE;
		}
		if(typeOfTypes.equals("LEADTYPE")){
			scr2 = Screen.LEADTYPE;
		}
		if(typeOfTypes.equals("SALESQUOTATIONTYPE")){
			scr2 = Screen.SALESQUOTATIONTYPE;
		}
		if(typeOfTypes.equals("SALESORDERTYPE")){
			scr2 = Screen.SALESORDERTYPE;
		}
		if(typeOfTypes.equals("DELIVERYNOTETYPE")){
			scr2 = Screen.DELIVERYNOTETYPE;
		}
		if(typeOfTypes.equals("QUOTATIONTYPE")){
			scr2 = Screen.QUOTATIONTYPE;
		}
		if(typeOfTypes.equals("CONTRACTTYPE")){
			scr2 = Screen.CONTRACTTYPE;
		}
		if(typeOfTypes.equals("VENDORTYPE")){
			scr2 = Screen.VENDORTYPE;
		}
		if(typeOfTypes.equals("PRTYPE")){
			scr2 = Screen.PRTYPE;
		}
		if(typeOfTypes.equals("RFQTYPE")){
			scr2 = Screen.RFQTYPE;
		}
		if(typeOfTypes.equals("LOITYPE")){
			scr2 = Screen.LOITYPE;
		}
		if(typeOfTypes.equals("POTYPE")){
			scr2 = Screen.PURCHASEORDERTYPE;
		}
		if(typeOfTypes.equals("GRNTYPE")){
			scr2 = Screen.GRNTYPE;
		}
		if(typeOfTypes.equals("MRNTYPE")){
			scr2 = Screen.MRNTYPE;
		}
		if(typeOfTypes.equals("MINTYPE")){
			scr2 = Screen.MINTYPE;
		}
		if(typeOfTypes.equals("MMNTYPE")){
			scr2 = Screen.MMNTYPE;
		}
		
		if(typeOfTypes.equals("INVOICECONFIGTYPE")){
			scr2 = Screen.INVOICECONFIGTYPE;
		}
		if(typeOfTypes.equals("BILLINGTYPE")){
			scr2 = Screen.BILLINGTYPE;
		}
		
		
		if(typeOfTypes.equals("WORKORDERTYPE")){
			scr2 = Screen.BILLINGTYPE;
		}
		if(typeOfTypes.equals("INSPECTIONTYPE")){
			scr2 = Screen.BILLINGTYPE;
		}
		
		return scr2;
	}

	/********************************************Default Configs**************************************/

	public ArrayList<String> moduleNames()
	{
		ArrayList<String> modulesArray=new ArrayList<String>();
		
		modulesArray.add("Sales");
		modulesArray.add(AppConstants.SALESCONFIG);
		modulesArray.add("Service");
		modulesArray.add(AppConstants.SERVICECONFIG);
		modulesArray.add("Purchase");
		modulesArray.add(AppConstants.PURCHASECONFIG);
		modulesArray.add("Inventory");
		modulesArray.add(AppConstants.INVENTORYCONFIG);
		modulesArray.add("HR Admin");
		modulesArray.add(AppConstants.HRCONFIG);
		modulesArray.add("Employee Self Service");
		modulesArray.add("Product");
		modulesArray.add(AppConstants.PRODUCTCONFIG);
		modulesArray.add("Asset Management");
		modulesArray.add(AppConstants.ASSETMANAGEMENTCONFIG);
		modulesArray.add("Accounts");
		modulesArray.add(AppConstants.ACCOUNTCONFIG);
		modulesArray.add("Settings");
		modulesArray.add("Approval");
		
		
		return modulesArray;
	}
	
	public ArrayList<String> salesDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		
		documentArray.add("Dashboard");
		documentArray.add("Customer");
		documentArray.add("Campaign Management");
		documentArray.add("Lead");
		documentArray.add("Quotation");
		documentArray.add("Sales Order");
		documentArray.add("Quick Sales Order");
		documentArray.add("Delivery Note");
		documentArray.add("Work Order");
		documentArray.add("Interaction To Do List");
		documentArray.add("Interaction To Do");
		documentArray.add("Contact Person List");
		documentArray.add("Contact Person Details");
		documentArray.add("Security Deposit List");
		
		
		return documentArray;
	}
	
	
	public ArrayList<String> salesConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Customer Group");
		documentArray.add("Customer Category");
		documentArray.add("Customer Type");
		documentArray.add("Customer Level");
		documentArray.add("Customer Priority");
		
		documentArray.add("Lead Status");
		documentArray.add("Lead Group");
		documentArray.add("Lead Category");
		documentArray.add("Lead Type");
		documentArray.add("Lead Priority");
		
		documentArray.add("Sales Quotation Group");
		documentArray.add("Sales Quotation Category");
		documentArray.add("Sales Quotation Type");
		documentArray.add("Sales Quotation Priority");
		
		documentArray.add("Sales Order Group");
		documentArray.add("Sales Order Category");
		documentArray.add("Sales Order Type");
		
		documentArray.add("Delivery Note Group");
		documentArray.add("Delivery Note Category");
		documentArray.add("Delivery Note Type");
		
		documentArray.add("Work Order Group");
		documentArray.add("Work Order Category");
		documentArray.add("Work Order Type");
		
		documentArray.add("Checklist Step");
		documentArray.add("Checklist Type");
		
		documentArray.add("Shipping Method");
		documentArray.add("Packing Method");
		documentArray.add("Entity Type");
		documentArray.add("Interaction Group");
		documentArray.add("Communication Category");
		documentArray.add("Communication Type");
		documentArray.add("Interaction Priority");
		documentArray.add("Contact Role");
		return documentArray;
	}
	
	
	
	public ArrayList<String> serviceDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Dashboard");
		documentArray.add("Complain Dashboard");
		documentArray.add("Customer"); 
		documentArray.add("Customer List");
		documentArray.add("Customer Help Desk");
		documentArray.add("Campaign Management");
		documentArray.add("Customer Branch");
		documentArray.add("Lead");
		documentArray.add("Quotation");
		documentArray.add("Contract");
		documentArray.add("Quick Contract");
		
		documentArray.add("Customer Service List");
		documentArray.add("Customer Service");
		documentArray.add("Customer Project");
		documentArray.add("Technician Dashboard");
		//   rohan added this used for (sales person details (vijay))
		documentArray.add("Sales Person Dashboard");
		documentArray.add("Sales Person Targets");
		documentArray.add("Sales Person Targets Performance");
		
		
		
		documentArray.add("Fumigation");
		documentArray.add("Fumigation(Aus)");
		documentArray.add("Liasion Steps");
		documentArray.add("Liasion Group");
		documentArray.add("Liasion");
		documentArray.add("Customer Support");
		documentArray.add("Contract Renewal");
		documentArray.add("Scheduling And Routing");
		documentArray.add("Team Management");
		
		documentArray.add("Interaction To Do List");
		documentArray.add("Interaction To Do");
		documentArray.add("Contact Person List");
		documentArray.add("Contact Person Details");
		documentArray.add("Security Deposit List");
		
		return documentArray;
	}
	
	
	public ArrayList<String> serviceConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Customer Group");
		documentArray.add("Customer Category");
		documentArray.add("Customer Type");
		documentArray.add("Customer Level");
		documentArray.add("Customer Priority");
		
		documentArray.add("Lead Status");
		documentArray.add("Lead Group");
		documentArray.add("Lead Category");
		documentArray.add("Lead Type");
		documentArray.add("Lead Priority");
		
		documentArray.add("Quotation Group");
		documentArray.add("Quotation Category");
		documentArray.add("Quotation Type");
		documentArray.add("Quotation Priority");
		
		documentArray.add("Contract Group");
		documentArray.add("Contract Category");
		documentArray.add("Contract Type");
		
		
		documentArray.add("Service Type");
		documentArray.add("Document Type");
		documentArray.add("Specific Reason");
		documentArray.add("Bill Of Material");
		
		documentArray.add("Entity Type");
		documentArray.add("Interaction Group");
		documentArray.add("Communication Category");
		documentArray.add("Communication Type");
		documentArray.add("Interaction Priority");
		documentArray.add("Contact Role");
		
		documentArray.add("Declare point of Entry");
		documentArray.add("Dosage rate of fumigant");
		documentArray.add("Duration of fumigation");
		documentArray.add("Minimum air temperature");
		documentArray.add("Quantity Declare Unit");
		documentArray.add("Place of Fumigation");
		documentArray.add("Port and Country of Loading");
		documentArray.add("Fumigation Declaration");
		documentArray.add("Complain Priority");
		documentArray.add("Complain Status");
		documentArray.add("Number Range");
		documentArray.add("Catch Pest Name");
		
		return documentArray;
	}
	
	
	public ArrayList<String> purchaseDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Dashboard");
		documentArray.add("Vendor");
		documentArray.add("Purchase Requisition");
		documentArray.add("RFQ");
		documentArray.add("Letter Of Intent(LOI)");
		
		documentArray.add("Purchase Order");
		documentArray.add("Quick Purchase Order");

		documentArray.add("Vendor Product Pricelist");
		documentArray.add("Vendor Product Price");
	
		documentArray.add("Interaction To Do List");
		documentArray.add("Interaction To Do");
		documentArray.add("Contact Person List");
		documentArray.add("Contact Person Details");
		
//		documentArray.add("Security Deposit List");
		
		documentArray.add("Service PO");
		documentArray.add("Purchase Requisition List");
		
		return documentArray;
	}
	
	public ArrayList<String> purchaseConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Vendor Group");
		documentArray.add("Vendor Category");
		documentArray.add("Vendor Type");
		
		documentArray.add("PR Group");
		documentArray.add("PR Category");
		documentArray.add("PR Type");
		
		documentArray.add("RFQ Category");
		documentArray.add("RFQ Type");
		
		documentArray.add("LOI Category");
		documentArray.add("LOI Type");
		
		documentArray.add("PO Type");
		documentArray.add("PO Category");
		
		documentArray.add("Entity Type");
		documentArray.add("Interaction Group");
		documentArray.add("Communication Category");
		documentArray.add("Communication Type");
		documentArray.add("Interaction Priority");
		documentArray.add("Contact Role");
		
		documentArray.add("Service PO Category");
		documentArray.add("Service PO Type");
		
		return documentArray;
	}
	
	
	public ArrayList<String> productDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Service Product");
		documentArray.add("Item Product");
		documentArray.add("Product Group List");
		documentArray.add("Product Group Details");
		documentArray.add("Bill Of Material");
		
		return documentArray;
	}
	
	public ArrayList<String> productConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Category");
		documentArray.add("Unit Of Measurement");
		documentArray.add("Product Group");
		documentArray.add("Product Type");
		documentArray.add("Product Classification");
		documentArray.add("Employee Level");
		
		
		return documentArray;
	}
	
	
	public ArrayList<String> accountsDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Dashboard (AR)");
		documentArray.add("Dashboard (AP)");
		
		documentArray.add("Expense Management");
		documentArray.add("Petty Cash Transaction");
		documentArray.add("Petty Cash Transaction List");
		documentArray.add("Billing List");
		documentArray.add("Billing Details");
		documentArray.add("Invoice List");
		documentArray.add("Invoice Details");
		documentArray.add("Payment List");
		documentArray.add("Payment Details");
		documentArray.add("Accounting Interface");
		documentArray.add("Financial Calendar");
		
		return documentArray;
	}
	
	public ArrayList<String> accountsConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Expense Group");
		documentArray.add("Expense Category");
		documentArray.add("Expense Type");
		
		documentArray.add("GLAccount");
		documentArray.add("GLAccount Group");
		documentArray.add("Payment Method");
		documentArray.add("Other Charges");
		documentArray.add("Petty Cash");
		documentArray.add("Company");
		documentArray.add("Company Type");
		documentArray.add("Billing Group");
		documentArray.add("Billing Category");
		documentArray.add("Billing Type");
		documentArray.add("Invoice Group");
		documentArray.add("Invoice Category");
		documentArray.add("Invoice Type");
		documentArray.add("Tax Details");
		documentArray.add("Payment Mode");
		documentArray.add("Vendor Payment Mode");
		documentArray.add("TDS Tax Details");
		documentArray.add("Payment Terms Details");
		
		return documentArray;
	}
	
	public ArrayList<String> inventoryDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Dashboard");
		documentArray.add("Inventory Transaction List");
		documentArray.add("Product Inventory View");
		documentArray.add("Product Inventory List");
		documentArray.add("Stock Alert");
		documentArray.add("GRN List");
		documentArray.add("GRN");
		documentArray.add("Inspection List");
		documentArray.add("Inspection");
		documentArray.add("Material Request Note");
		documentArray.add("Material Issue Note");
		documentArray.add("Material Movement Note");
		documentArray.add("Physical Inventory");
		documentArray.add("Material Consumption Report");
		documentArray.add("Interaction To Do List");
		documentArray.add("Interaction To Do");
		
		
		return documentArray;
	}
	
	public ArrayList<String> inventoryConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Warehouse");
		documentArray.add("Storage Location");
		documentArray.add("Storage Bin");
		documentArray.add("GRN Group");
		documentArray.add("GRN Category");
		documentArray.add("GRN Type");
		documentArray.add("Inspection Group");
		documentArray.add("Inspection Category");
		documentArray.add("Inspection Type");
		documentArray.add("MRN Category");
		documentArray.add("MRN Type");
		documentArray.add("MIN Category");
		documentArray.add("MIN Type");
		documentArray.add("MMN Category");
		documentArray.add("MMN Type");
		documentArray.add("MMN Direction");
		documentArray.add("MMN Transaction Type");
		documentArray.add("Inspection Method");
		documentArray.add("Entity Type");
		documentArray.add("Interaction Group");
		documentArray.add("Communication Category");
		documentArray.add("Communication Type");
		documentArray.add("Interaction Priority");
		
		documentArray.add("Warehouse Type");
		
		return documentArray;
	}
	
	
	public ArrayList<String> assetManagementDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Dashboard");
		documentArray.add("Company Asset");
		documentArray.add("Client Asset");
		documentArray.add("Tool Set");
		
		return documentArray;
	}
	
	public ArrayList<String> assetManagementConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Tool Category");
		documentArray.add("Client Asset Category");
		
		return documentArray;
	}
	
	
	public ArrayList<String> employeeSelfServiceDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Leave Application");
		documentArray.add("Advance Request");
		documentArray.add("Employee Attendance");
		documentArray.add("Time Report");
//		documentArray.add("Human Resource");
		documentArray.add("CTC");
		documentArray.add("Leave Balance");
		
		return documentArray;
	}
	
	public ArrayList<String> approvalConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Approval");
		return documentArray;
	}
	
	
	public ArrayList<String> approvalDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Approval");
		return documentArray;
	}
	
	
	public ArrayList<String> settingDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Branch");
		documentArray.add("Employee");
		documentArray.add("User");
		documentArray.add("Customer User");
		documentArray.add("License Info");
		documentArray.add("Currency");
		documentArray.add("Country");
		documentArray.add("State");
		documentArray.add("City");
		documentArray.add("Locality");
		documentArray.add("Module Name");
		documentArray.add("Document Name");
		documentArray.add("Number Generation");
		documentArray.add("Process Name");
		
		documentArray.add("Process Configuration");
		documentArray.add("Process Type");
		documentArray.add("Article Type");
		documentArray.add("SMS History");
		documentArray.add("SMS Template");
		documentArray.add("SMS Configuration");
		documentArray.add("Multilevel Approval");
		documentArray.add("Approval Level");
		documentArray.add("IP Address Authorization");
		documentArray.add("LoggedIn History");
		documentArray.add("Document History");
		
		documentArray.add("User Authorization");
		documentArray.add("Data Migration");
		documentArray.add("Data Migration Task Queue");
		documentArray.add("Role Definition");  // 
		documentArray.add("Android Tips");
		documentArray.add("App Registration History");
		
		return documentArray;
	}
	
	
	
	public ArrayList<String> hrAdminDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Employee");
		documentArray.add("Employee Additional Details");
		documentArray.add("Leave Application");
		documentArray.add("Advance Request");
		documentArray.add("Upload Attendance");
		documentArray.add("Employee Attendance");
		documentArray.add("Time Report");
		documentArray.add("CTC");
		documentArray.add("Leave Balance");
//		documentArray.add("Assign Shift");
		documentArray.add("Process Payroll");
		documentArray.add("Salary Slip");
		documentArray.add("Company's Contribution");
		documentArray.add("Interaction To Do List");
		documentArray.add("Interaction To Do");
		documentArray.add("Contact Person List");
		documentArray.add("Contact Person Details");
		
		return documentArray;
	}
	
	
	public ArrayList<String> hrAdminConfigDocumentNames()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Employee Role");
		documentArray.add("Employee Designation");
		documentArray.add("Employee Type");
		documentArray.add("Department");
		documentArray.add("Employee Family Relation");
		documentArray.add("Address Type");
		documentArray.add("Shift");
		documentArray.add("Shift Pattern");
		documentArray.add("Shift Category");
		documentArray.add("Pattern Category");
		documentArray.add("Project");
		documentArray.add("Holiday Type");
		documentArray.add("Calendar");
		documentArray.add("Leave Type");
		documentArray.add("Leave Group");
		documentArray.add("Overtime");
		documentArray.add("Calendar & Leave Allocation");
		documentArray.add("Advance Type");
		documentArray.add("CTC Component");
		documentArray.add("Deductions");
		documentArray.add("Time Reporting Period");
		documentArray.add("Entity Type");
		documentArray.add("Interaction Group");
		documentArray.add("Communication Category");
		
		documentArray.add("Communication Type");
		documentArray.add("Interaction Priority");
		documentArray.add("Contact Role");
		
		return documentArray;
	}
	
	public ArrayList<String> hrAdminStatutoryDeductionNames() {
		ArrayList<String> documentArray=new ArrayList<String>();
		documentArray.add("Provident Fund");
		documentArray.add("Esic");
		documentArray.add("Professional Tax");
		documentArray.add("LWF");
		return documentArray;
	}
	
	public ArrayList<String> processTypeData()
	{
		ArrayList<String> documentArray=new ArrayList<String>();
		
		documentArray.add("Accounting Interface");
		documentArray.add("ApprovalDailyEmail");
		documentArray.add("BRANCHNONMANDATORY");
		documentArray.add("BranchLevelRestriction");
		documentArray.add("CONTRACTRENEWALLETTERFORPESTOINDIA");
		documentArray.add("CompanyAsLetterHead");
		documentArray.add("ComplainFormForRealGuard");
		documentArray.add("Contract Service Data");
		documentArray.add("ContractRenewalDailyEmail");
		documentArray.add("DisclaimerText");
		documentArray.add("EmployeeApprovalProcess");
		documentArray.add("HIDEPRODUCTINFO");
		documentArray.add("HygeiaPestManagement");
		documentArray.add("INVOICEANDREFDOCCUSTMIZATION");
		documentArray.add("IPAuthorization");
		documentArray.add("InstallationPdf");
		documentArray.add("InvoiceARDailyEmail");
		documentArray.add("IsInvoiceGroupMandatory");
		documentArray.add("JobCompletionCertificate");
		documentArray.add("JobReportcumReceiptForRealGuard");
		documentArray.add("LeadDailyEmail");
		documentArray.add("MakeMaterialIssueMandatory");
		documentArray.add("NBHCQuotation");
		documentArray.add("NumberRangeProcess");
		documentArray.add("PartialPaymentOneInvoiceAndPaymentAsPerrecievedAndBalanceAmt");
		documentArray.add("PaymentARDailyEmail");
		documentArray.add("PepcoppQuotation");
		documentArray.add("PestleQuotations");
		documentArray.add("PestoIndiaQuotations");
		documentArray.add("PrintCompanyNameAsLogo");
		documentArray.add("PrintCustomQuotation");
		documentArray.add("Quotation Service Data");
		documentArray.add("QuotationAsLetter");
		documentArray.add("REDDYSPESTCONTROLQUOTATIONS");
		documentArray.add("RInvoiceChangesAccounting Interface");
		documentArray.add("Security Deposit");
		documentArray.add("ServiceInvoicePayTerm");
		documentArray.add("ServiceQuotationDailyEmail");
		documentArray.add("Serviceandbillingaddresssameprintone");
		documentArray.add("UniversalPestCustomization");
		documentArray.add("UserWiseIPAuthorization");
		documentArray.add("VCareCustomization");
		documentArray.add("SalesPersonRestriction");
		documentArray.add("NewCustomerButtonSetEnableFromLead");
		documentArray.add("printDescOnFirstPageFlagForContractAndQuotation");
		documentArray.add("PrintMultipleCompanyNamesFromInvoiceGroup");
		// These configurations are added by anil for NBHC Deadstock
		documentArray.add("ADDPOPURENGGINPR");
		documentArray.add("PRHEADASDEFAULTPURENGG");
		documentArray.add("VENDORINSTRUCTIONONPOPDF");
		documentArray.add("PRAPPROVALMAILTOPURENGG");
		documentArray.add("CODEAUTOGENERATE");
		documentArray.add("BRANCHNONMANDETORY");
		documentArray.add("HIDEPRODUCTINFO");
		
		
		documentArray.add("LoadEmployeeRoleWise");
		documentArray.add("PurchaseEngineerPRPO");
		
		documentArray.add("POASSIGNANDAPPROVALMAILTOPRPURENGG");
		
		
		
		return documentArray;
	}

	public ArrayList<String> classesList()
	{
		ArrayList<String> classessArray=new ArrayList<String>();
		
		classessArray.add("LoneType");
		classessArray.add("PaySlip");
		classessArray.add("LoneEmi");
		classessArray.add("LeaveBalance");
		classessArray.add("LoggedIn");
		classessArray.add("Service");
		classessArray.add("Quotation");
		classessArray.add("Approvals");
		classessArray.add("Contract");
		classessArray.add("Config");
		classessArray.add("ConfigCategory");
		classessArray.add("Type");
		classessArray.add("Company");
		classessArray.add("Activity");
		classessArray.add("TimeReportConfig");
		classessArray.add("Branch");
		classessArray.add("BranchRelation");
		classessArray.add("Employee");
		classessArray.add("EmployeeInfo");
		classessArray.add("EmployeeRelation");
		classessArray.add("User");
		classessArray.add("ItemProduct");
		classessArray.add("SuperProduct");
		classessArray.add("ServiceProduct");
		classessArray.add("Vendor");
		classessArray.add("Expense");
		classessArray.add("Lead");
		classessArray.add("Customer");
		classessArray.add("VendorRelation");
		classessArray.add("CustomerRelation");
		classessArray.add("Category");
		classessArray.add("Purchase");
		classessArray.add("Calendar");
		classessArray.add("GLAccount");
		classessArray.add("SalesOrder");
		classessArray.add("EmployeeLeave");
		classessArray.add("Loan");
		classessArray.add("Country");
		classessArray.add("Grade");
		classessArray.add("LeaveType");
		classessArray.add("LeaveApplication");
		classessArray.add("Shift");
		classessArray.add("ShiftPattern");
		classessArray.add("Department");
		classessArray.add("UserRole");
		classessArray.add("HrProject");
		classessArray.add("HolidayType");
		classessArray.add("EmployeeGroup");
		classessArray.add("LeaveGroup");
		classessArray.add("CtcComponent");
		classessArray.add("CTC");
		classessArray.add("AssignedShift");
		classessArray.add("CompanyAsset");
		classessArray.add("ToolGroup");
		classessArray.add("ServiceProject");
		classessArray.add("ClientSideAsset");
		classessArray.add("EmployeeAttendance");
		classessArray.add("LeaveAllocationProcess");
		classessArray.add("PaySlipAllocationProcess");
		classessArray.add("LiaisonStep");
		classessArray.add("Liaison");
		classessArray.add("LiaisonGroup");
		classessArray.add("SalesQuotation");
		classessArray.add("PriceList");
		classessArray.add("RequsestForQuotation");
		classessArray.add("LetterOfIntent");
		classessArray.add("PettyCash");
		classessArray.add("PettyCashDeposits");
		classessArray.add("BillingDocument");
		classessArray.add("Invoice");
		classessArray.add("CustomerPayment");
		classessArray.add("OtherTaxCharges");
		classessArray.add("TaxesAndCharges");
		classessArray.add("DeliveryNote");
		classessArray.add("PurchaseOrder");
		classessArray.add("PurchaseRequisition");
		classessArray.add("GRN");
		classessArray.add("InventoryInHand");
		classessArray.add("Transfer");
		classessArray.add("IncomingInventory");
		classessArray.add("LocationRelation");
		classessArray.add("InventoryEntries");
		classessArray.add("WareHouse");
		classessArray.add("OutgoingInventory");
		classessArray.add("InventoryProductDetail");
		classessArray.add("StorageLocation");
		classessArray.add("Storagebin");
		classessArray.add("ProductInventoryView");
		classessArray.add("ProductInventoryViewDetails");
		classessArray.add("CompanyPayment");
		classessArray.add("VendorPayment");
		classessArray.add("TaxDetails");
		classessArray.add("State");
		classessArray.add("City");
		classessArray.add("Locality");
		classessArray.add("CheckListStep");
		classessArray.add("CheckListType");
		classessArray.add("ShippingMethod");
		classessArray.add("PackingMethod");
		classessArray.add("EmployeeShift");
		classessArray.add("DateShift");
		classessArray.add("TimeReport");
		classessArray.add("PaySlipRecord");
		classessArray.add("ProductGroupList");
		classessArray.add("ProductGroupDetails");
		classessArray.add("BillOfMaterial");
		classessArray.add("NumberGeneration");
		classessArray.add("AccountingInterface");
		classessArray.add("ContactPersonIdentification");
		classessArray.add("PhysicalInventoryMaintaince");
		classessArray.add("RaiseTicket");
		classessArray.add("MaterialRequestNote");
		classessArray.add("MaterialIssueNote");
		classessArray.add("ProductInventoryTransaction");
		classessArray.add("MaterialMovementNote");
		classessArray.add("MaterialMovementType");
		classessArray.add("ProcessName");
		classessArray.add("InteractionType");
		classessArray.add("ProcessConfiguration");
		classessArray.add("DocumentHistory");
		classessArray.add("InspectionMethod");
		classessArray.add("LicenseManagement");
		classessArray.add("CancelSummary");
		classessArray.add("Inspection");
		classessArray.add("BillOfProductMaterial");
		classessArray.add("VendorPriceListDetails");
		classessArray.add("WorkOrder");
		classessArray.add("CustomerBranchDetails");
		classessArray.add("SmsConfiguration");
		classessArray.add("SmsTemplate");
		classessArray.add("IPAddressAuthorization");
		classessArray.add("MultilevelApproval");
		classessArray.add("SmsHistory");
		classessArray.add("Fumigation");
		classessArray.add("CustomerTrainingDetails");
		classessArray.add("EmployeeAdditionalDetails");
		classessArray.add("Overtime");
		classessArray.add("CompanyPayrollRecord");
		classessArray.add("Declaration");
		classessArray.add("Complain");
		classessArray.add("ContractRenewal");
		classessArray.add("TeamManagement");
		classessArray.add("CustomerUser");
		classessArray.add("WhatsNew");
		classessArray.add("PettyCashTransaction");
		classessArray.add("SalesPersonTargets");
		classessArray.add("FinancialCalender");
		classessArray.add("AssesmentReport");
		classessArray.add("CronJob");	
		classessArray.add("MulipleExpenseMngt");	
		
		classessArray.add("C_Billing");	
		classessArray.add("S_Billing");	
		classessArray.add("B_Billing");	
		classessArray.add("I_Billing");	
		classessArray.add("P_Billing");	
		classessArray.add("C_NonBilling");	
		classessArray.add("S_NonBilling");	
		classessArray.add("B_NonBilling");	
		classessArray.add("I_NonBilling");
		classessArray.add("P_NonBilling");	
		classessArray.add("P_Invoice");	
		classessArray.add("ServicePo");	
		classessArray.add("RoleDefinition");	
		classessArray.add("EmployeeLevel");	
		
		
	  
		return classessArray;
	}

/**
 * This method is used for taking all the config values in string format and sent to task queue
 * Date : 10 -09-2016
 * Release date : 30 sept 2016
 */
	private String getConfigCategoryDatainString(int categoryInternalType,String categoryName, String categoryCode, Long companyId, int count) {
		
		String configcategoryinstring = categoryInternalType+"$"+categoryName+"$"+categoryCode+"$"+companyId+"$"+count;
		return configcategoryinstring;
		
	}
	/**
	 * ends here 
	 */
	
	
	
	
	/**
	 * This method is used for taking all the Type values (Document names) in string format and sent to task queue
	 * Date : 10 -09-2016
	 * Release date : 30 sept 2016
	 */
	
	private String getTypeDatafortaskqueue(int categoryInternalType,String categoryName, String typeName, String typeCode3, String catCode,
	Long companyId, int setcount)
	{
			 String typeducumentName = new String();
			 typeducumentName = categoryInternalType+"$"+categoryName+"$"+typeName+"$"+typeCode3+"$"+catCode+"$"+companyId+"$"+setcount;
			 
			return typeducumentName;
	}
	/**
	 * ends here 
	 */
	
	
	/**
	 * Developed by : Rohan bhagde
	 * Date : 22/11/2016
	 * Reason : This is used for taking all config value in string format and send to task queue 
	 * @param companyId 
	 * @param name 
	 * @param type 
	 * @param setcount 
	 */
	
	private String getConfigDataForTaskQueue(int type, String name, Long companyId, int setcount)
	{
		 String configName = new String();
		 configName = type +"$" +name +"$"+companyId+"$"+setcount;
		 return configName;
		
	}
	
	/**
	 * ends here 
	 */
	
	/**
	 * Developed by : Rohan bhagde
	 * Date : 22/11/2016
	 * Reason : This is used for taking all Category value in string format and send to task queue 
	 * @param companyId 
	 * @param name 
	 * @param type 
	 * @param setcount 
	 */
	
	private String getCategoryDataForTaskQueue(int internalType,String categoryName,String categoryCode,long companyId,int setcount,String desc)
	{
		 String categoryNameValue = new String();
		 categoryNameValue = internalType +"$" +categoryName+"$"+categoryCode +"$"+companyId+"$"+setcount+"$"+desc;
		 return categoryNameValue;
		
	}
	
	/**
	 * ends here 
	 */
	
	/**
	 * Developed by : Rohan bhagde
	 * Date : 22/11/2016
	 * Reason : This is used for taking all Type value in string format and send to task queue 
	 * @param companyId 
	 * @param name 
	 * @param type 
	 * @param setcount 
	 */
	
	private String getTypeDataForTaskQueue(int internalType,String categoryName, String typeName,String typeCode,String categoryCode, Long companyId, int setcount)
	{
		 String typeNameValue = new String();
		 typeNameValue = internalType +"$" +categoryName +"$"+typeName+"$"+typeCode+"$"+categoryCode+"$"+companyId+"$"+setcount;
		 
		 return typeNameValue;
		
	}
	
	/**
	 * ends here 
	 */
	
	
//	/**
//	 * This method is used for taking all the Type values (Document names) in string format and sent to task queue
//	 * Date : 10 -09-2016
//	 * Release date : 30 sept 2016
//	 * @param long1 
//	 */
//	
	public void getLeadDatafortaskqueue(Long companyId)
	{
		
			    String typeducumentName = new String();
			    typeducumentName = companyId+"";
			    logger.log(Level.SEVERE,"typeducumentName===="+typeducumentName);
			 
				Queue queue = QueueFactory.getQueue("LeadValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/leadvaluetaskqueue").param("LeadDataValuekey", typeducumentName));
			 
	}
	/**
	 * ends here 
	 */
//	
//	
//	/**
//	 * Rohan added this code for jSondata to send for task queue for saving lead data 
//	 * Date : 10/12/2016
//	 * @param count 
//	 *   
//	 */
//	private String getleadJStr(Long companyId, int count) {
//		
//		SimpleDateFormat spf = new SimpleDateFormat("dd MMM yyyy");
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//		spf.setTimeZone(TimeZone.getTimeZone("IST"));
//		JSONObject leadJObj = null;
//				try
//		{
//		leadJObj=new JSONObject();
//		leadJObj.put("approverName"," ");
//		leadJObj.put("approvalDate"," ");
//		leadJObj.put("branch","Andheri");
//		leadJObj.put("category","RESIDENCIAL");
//		leadJObj.put("companyId",companyId);
//		leadJObj.put("count",count);
//		leadJObj.put("createdBy","Vedant Kamble");
//		leadJObj.put("creationDate",spf.format(new Date()));
//		leadJObj.put("description","This is lead for Residencial Client");
//		leadJObj.put("employee","Rohan Bhagde");
//		leadJObj.put("group","Enquiry");
//		
//		//   product details
//		JSONArray jArray = new JSONArray();
//		for (int i = 0; i < 1; i++) {
//			JSONObject jproductDetails = new JSONObject();
//			jproductDetails.put("ProductSrNo", "1");
//			jproductDetails.put("ProductId", "500000001");
//			jproductDetails.put("ProductCategory", "Service Category");
//			jproductDetails.put("ProductName", "Bed Bug Treatment");
//			jproductDetails.put("ProductCode", "SPDemo");
//		    jproductDetails.put("ProductQty", "1");
//			jproductDetails.put("ProductNoOfService", "4");
//			jproductDetails.put("ProductUnit", "Ml");
//			jproductDetails.put("ProductDuration", "365");
//			jproductDetails.put("ProductPrice", "2000");
//			jproductDetails.put("ProductArea", "NA");	
//			jArray.add(jproductDetails);
//		} 
//		//
//		leadJObj.put("productDetails", jArray);
//		leadJObj.put("personInfo.fullName", "Suraj Sharma");
//		leadJObj.put("personInfo.email", "evasoftwaresolutionserp@gmail.com");
//		leadJObj.put("personInfo.count", "100000001");
//		leadJObj.put("personInfo.cellNumber", "9876543210");
//		leadJObj.put("personInfo.poc", "9876543210");
//		
//		leadJObj.put("priority","Medium");
//		leadJObj.put("remark","");
//		leadJObj.put("status","Created");
//		leadJObj.put("title","Enquiry for Bed Bug");
//		leadJObj.put("type","1 BHK FLAT");
//		leadJObj.put("userId","admin123");
//		}
//		catch(Exception e)
//		{
//			logger.log(Level.SEVERE, "JSON Error "+e);
//		} 
//		
//		String jsonString = leadJObj.toJSONString().replaceAll("\\\\", "");
//		logger.log(Level.SEVERE, "Lead JSON Data"+jsonString);
//		
//		return jsonString;
//	}

	/**
	 * ends here 
	 */
	
	
	
	
	/**
	 * This method is used for taking all the Service Product Details by task queue
	 * Date : 12 -09-2016
	 * Release date : 30 sept 2016
	 * @param long1 
	 */
	
	public void getProductDetailstaskqueue(Long companyId)
	{
	    String typeducumentName = new String();
	    typeducumentName=companyId+"";
	    logger.log(Level.SEVERE,"typeducumentName===="+typeducumentName);
	 
		Queue queue = QueueFactory.getQueue("ProductData-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/productdatataskqueue").param("ProductDetaiilsValueKey", typeducumentName));
	}
	/**
	 * ends here 
	 */
	
	
	/**
	 * This method is used for taking all the Item Product Details by task queue
	 * Date : 12 -09-2016
	 * Release date : 30 sept 2016
	 * @param long1 
	 */
	
	public void getItemProductDetailstaskqueue(Long companyId)
	{
	    String typeducumentName = new String();
	    typeducumentName=companyId+"";
	    logger.log(Level.SEVERE,"typeducumentName===="+typeducumentName);
	 
		Queue queue = QueueFactory.getQueue("ProductData-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/productdatataskqueue").param("ProductDetaiilsValueKey", typeducumentName));
	}
	/**
	 * ends here 
	 */


	@Override
	public String createselectedConfiguration(long companyId, ArrayList<String> selectedScreenNamelist) {

		for(String str : selectedScreenNamelist){
			if(str.equals("Customer")){
				ArrayList<String> customerConfigs = getcustomerConfig();
				for(String config : customerConfigs){
					if(config.trim().equals("Customer Group")){
						createconfigs(companyId,Screen.CUSTOMERGROUP);
					}
					else if(config.trim().equals("Customer Priority")){
						createconfigs(companyId,Screen.CUSTOMERPRIORITY);
					}
					else if(config.trim().equals("Customer Level")){
						createconfigs(companyId,Screen.CUSTOMERLEVEL);
					}
				}
				createCategory(companyId,Screen.CUSTOMERCATEGORY);
			}
			else if(str.equals("Lead")){
				ArrayList<String> leadConfigs = getleadConfigs();
				for(String config : leadConfigs){
					if(config.trim().equals("Lead Status")){
						createconfigs(companyId,Screen.LEADSTATUS);
					}
					else if(config.trim().equals("Lead Group")){
						createconfigs(companyId,Screen.LEADGROUP);
					}
					else if(config.trim().equals("Lead Priority")){
						createconfigs(companyId,Screen.LEADPRIORITY);
					}
				}
				createCategory(companyId,Screen.LEADCATEGORY);
			}
			else if(str.equals("Quotation")){
				ArrayList<String> quoationConfigs = getquotationConfigs();
				for(String config : quoationConfigs){
					if(config.trim().equals("Quotation Group")){
						createconfigs(companyId,Screen.QUOTATIONGROUP);
					}
					else if(config.trim().equals("Quotation Priority")){
						createconfigs(companyId,Screen.QUOTATIONPRIORITY);
					}
				}
				createCategory(companyId,Screen.QUOTATIONCATEGORY);
			}
			else if(str.equals("Contract")){
				ArrayList<String> quoationConfigs = getcontractGroups();
				for(String config : quoationConfigs){
					if(config.trim().equals("Contract Group")){
						createconfigs(companyId,Screen.CONTRACTGROUP);
					}
//					else if(config.trim().equals("Do Not Renew Remark")){
//						createconfigs(companyId,Screen.DONOTRENEWREMARK);
//					}
					else if(config.trim().equals("Do Not Renew Remark")){
						createconfigs(companyId,Screen.DONOTRENEW);
					}
					else if(config.trim().equals("Cancellation Remark")){
						createconfigs(companyId,Screen.CANCELLATIONREMARK);
					}
					else if(config.trim().equals("Entity Type")){
						createconfigs(companyId,Screen.PERSONTYPE);
					}
				}
				createCategory(companyId,Screen.CONTRACTCATEGORY);
			}
			else if(str.equals("Service")){
				ArrayList<String> quoationConfigs = getServiceConfigs();
				for(String config : quoationConfigs){
					if(config.trim().equals("Service Type")){
						createconfigs(companyId,Screen.SERVICETYPE);
					}
					else if(config.trim().equals("Specific Reason")){
						createconfigs(companyId,Screen.SPECIFICREASONFORRESCHEDULING);
					}
					else if(config.trim().equals("Complain Priority")){
						createconfigs(companyId,Screen.COMPLAINPRIORITY);
					}
					else if(config.trim().equals("Complain Status")){
						createconfigs(companyId,Screen.COMPLAINSTATUS);
					}
					else if(config.trim().equals("Catch Pest Name")){
						createconfigs(companyId,Screen.CATCHTRAPPESTNAME);
					}
					else if(config.trim().equals("Number Range")){
						createconfigs(companyId,Screen.NUMBERRANGE);
					}
				}
				createCategory(companyId,Screen.TICKETCATEGORY);
			}
			else if(str.equals("MMN")){
				ArrayList<String> quoationConfigs = getMMNConfiguration();
				for(String config : quoationConfigs){
					if(config.trim().equals("MMN Direction")){
						createconfigs(companyId,Screen.MMNDIRECTION);
					}
					else if(config.trim().equals("MMN Transaction Type")){
						createconfigs(companyId,Screen.MMNTRANSACTIONTYPE);
					}
				}
			}
			else if(str.equals("Product Configuration")){
				ArrayList<String> quoationConfigs = getproductConfiguration();
				for(String config : quoationConfigs){
					if(config.trim().equals("Unit of Measurement")){
						createconfigs(companyId,Screen.UNITOFMEASUREMENT);
					}
				}
			}
			else if(str.equals("Process Configuration")){
				ArrayList<String> quoationConfigs = getprocessConfigurationByScreen();
				for(String strName : quoationConfigs){
					createProcessConfiguration(companyId,strName);

				}
			}
		}
		
		return "Configuration Creation Process Started Successfully.It will take some time to update.";
	}

	private ArrayList<String> getprocessConfigurationByScreen() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Contract");
		list.add("Quotation");
		list.add("Employee");
		list.add("Company");
		list.add("Contract Renewal");
		list.add("Billing Document");
		list.add("Invoice");

		return list;
	}

	private void createProcessConfiguration(long companyId,	String strName) {
		ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId)
				.filter("processName", strName).first().now();
		if(processconfiguration==null){
			ProcessConfiguration processconfig = new ProcessConfiguration();
			processconfig.setCompanyId(companyId);
			processconfig.setProcessName(strName);
			processconfig.setConfigStatus(true);
			ArrayList<ProcessTypeDetails> list = new ArrayList<ProcessTypeDetails>();
			if(strName.trim().equals("Contract")){
				List<String> processtypelist = getcontractProcessTypeList(); 
				list.addAll(getlist(processtypelist,strName));
			}
			else if(strName.equals("Quotation") || strName.equals("Billing Document") || strName.equals("Invoice") ){
				List<String> processtypelist = getSelfApprovalProcessTypeList(); 
				list.addAll(getlist(processtypelist,strName));
			}
			else if(strName.trim().equals("Employee")){
				List<String> processtypelist = getEmployeeProcessTypeList(); 
				list.addAll(getlist(processtypelist,strName));
			}
			else if(strName.trim().equals("Contract Renewal")){
				List<String> processtypelist = getContractRenewalProcessTypeList(); 
				list.addAll(getlist(processtypelist,strName));
			}
			else if(strName.trim().equals("Company")){
				List<String> processtypelist = getcompanyProcessTypeList(); 
				list.addAll(getlist(processtypelist,strName));
			}
			processconfig.setProcessList(list);
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(processconfig);
		}
		else{
			ArrayList<ProcessTypeDetails> existinglist = new ArrayList<ProcessTypeDetails>();
			if(strName.trim().equals("Contract")){
				List<String> processtypelist = getcontractProcessTypeList(); 
				for(ProcessTypeDetails processType :  processconfiguration.getProcessList()){
					if(processtypelist.contains(processType)){
						processtypelist.remove(processType.getProcessType());
					}
				}
				if(processtypelist.size()!=0){
					existinglist.addAll(getlist(processtypelist,strName));
					processconfiguration.setProcessList(existinglist);
				}
			}
			else if(strName.equals("Quotation") || strName.equals("Billing Document") || strName.equals("Invoice") ){
				List<String> processtypelist = getSelfApprovalProcessTypeList(); 
				for(ProcessTypeDetails processType :  processconfiguration.getProcessList()){
					if(processtypelist.contains(processType)){
						processtypelist.remove(processType.getProcessType());
					}
				}
				if(processtypelist.size()!=0){
					existinglist.addAll(getlist(processtypelist,strName));
					processconfiguration.setProcessList(existinglist);
				}
			
			}
			else if(strName.trim().equals("Employee")){
				List<String> processtypelist = getEmployeeProcessTypeList(); 
				for(ProcessTypeDetails processType :  processconfiguration.getProcessList()){
					if(processtypelist.contains(processType)){
						processtypelist.remove(processType.getProcessType());
					}
				}
				if(processtypelist.size()!=0){
					existinglist.addAll(getlist(processtypelist,strName));
					processconfiguration.setProcessList(existinglist);
				}
			}
			else if(strName.trim().equals("Contract Renewal")){
				List<String> processtypelist = getContractRenewalProcessTypeList(); 
				for(ProcessTypeDetails processType :  processconfiguration.getProcessList()){
					if(processtypelist.contains(processType)){
						processtypelist.remove(processType.getProcessType());
					}
				}
				if(processtypelist.size()!=0){
					existinglist.addAll(getlist(processtypelist,strName));
					processconfiguration.setProcessList(existinglist);
				}
			}
			else if(strName.trim().equals("Company")){
				List<String> processtypelist = getcompanyProcessTypeList(); 
				for(ProcessTypeDetails processType :  processconfiguration.getProcessList()){
					if(processtypelist.contains(processType)){
						processtypelist.remove(processType.getProcessType());
					}
				}
				if(processtypelist.size()!=0){
					existinglist.addAll(getlist(processtypelist,strName));
					processconfiguration.setProcessList(existinglist);
				}
			}

			ofy().save().entity(processconfiguration).now();
		}
		

	}

	private List<String> getcompanyProcessTypeList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Companyasletterhead");
		return list;
	}

	private List<String> getContractRenewalProcessTypeList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("ContractRenewalSMS");
		return list;
	}

	private List<String> getEmployeeProcessTypeList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("EMAILNOTMANDATORY");
		list.add("EnableEmployeeCellNoNonMandatory");
		list.add("EnableEmployeeName");
		list.add("EnableEmployeeDateOfBirthNonMandatory");
		
		return list;
	}

	private List<String> getSelfApprovalProcessTypeList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("SELFAPPROVAL");
		return list;	
	}

	private ArrayList<ProcessTypeDetails> getlist(List<String> processtypelist, String strName) {
		ArrayList<ProcessTypeDetails> list = new ArrayList<ProcessTypeDetails>();
		for(String processtypeName : processtypelist){
			ProcessTypeDetails processtypedetails = new ProcessTypeDetails();
			processtypedetails.setProcessName(strName);
			processtypedetails.setProcessType(processtypeName);
			processtypedetails.setStatus(true);
			list.add(processtypedetails);
		}
		return list;
	}

	private List<String> getcontractProcessTypeList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("SELFAPPROVAL");
		list.add("CancelOpenDocumentsDirectly");
		list.add("ContractRenewalDailyEmail");
		list.add("ENABLESHOWCONTRACTRENEWFORRENEWEDCONTRACTS");
		list.add("EnableCancellationRemark");
		return list;	
	}

	private ArrayList<String> getproductConfiguration() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Unit of Measurement");
		return list;	
	}

	private ArrayList<String> getMMNConfiguration() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("MMN Direction");
		list.add("MMN Transaction Type");
		return list;
	}

	private ArrayList<String> getserviceType() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Periodic");
		list.add("Complaint");
		list.add("Trial Paid Service");
		list.add("Complementary Service");  
		list.add("Follow UP Service");
		return list;
	}

	private ArrayList<String> getcontractGroups() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Contract Group");
		list.add("Do Not Renew Remark");
		list.add("Cancellation Remark");
		list.add("Entity Type");
		return list;
	}

	private ArrayList<String> getquotationConfigs() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Quotation Group");
		list.add("Quotation Priority");
		return list;
	}

	private ArrayList<String> getleadConfigs() {
		ArrayList<String> list = new ArrayList<String>();
			list.add("Lead Status");
			list.add("Lead Group");
			list.add("Lead Priority");
			return list;
	}
	

	private ArrayList<String> getServiceConfigs() {
		ArrayList<String> list = new ArrayList<String>();
			list.add("Service Type");
			list.add("Specific Reason");
			list.add("Complain Priority");
			list.add("Complain Status");
			list.add("Catch Pest Name");
			list.add("Number Range");
			return list;
	}

	private void createCategory(long companyId, Screen screenName) {

		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "ConfigCategory").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	int count = (int) lastNumber;
    	
    	
    	NumberGeneration numbergenration = null;
		numbergenration = ofy().load().type(NumberGeneration.class)
				.filter("companyId", companyId)
				.filter("processName", "Type").filter("status", true)
				.first().now();
		long lastNumberGen = numbergenration.getNumber();
		logger.log(Level.SEVERE, "Last Number====" + lastNumberGen);
		int typeCount = (int) lastNumberGen;
		
		
    	if(screenName==Screen.CUSTOMERCATEGORY){
    		List<String> customerCategory = getcustomerCategory();
    		List<ConfigCategory> configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", 1).list();

    			if(configEntity.size()!=0){
    				for(int i=0;i<configEntity.size();i++){
	    	    			if(customerCategory.contains(configEntity.get(i).getCategoryName().trim())){
	    	    				customerCategory.remove(configEntity.get(i).getCategoryName().trim());
	    	    			}
	    	    	}
    			}
    			for(String custCategory : customerCategory){
            		int setcount = ++count;
    	    		String paymentTermsString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    	    				custCategory, "CC" + setcount, companyId,setcount, "");
    				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
    				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", paymentTermsString));
        		
    				ArrayList<String> typeList = getCustomerCategoryTypeList(custCategory);
    				
    				for(String typeName : typeList){
    					int setTypecount = ++typeCount;
    					String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    							custCategory, typeName, "TT" + setTypecount,"CC" + setcount, companyId, setTypecount);
    					Queue commTypeQueue = QueueFactory.getQueue("TypeValue-queue");
    					commTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
    				}
    				
        		}
    		
    	}
    	else if(screenName==Screen.LEADCATEGORY){
    		List<String> Categorylist = getleadCategory();
    		List<ConfigCategory> configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", 2).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(Categorylist.contains(configEntity.get(i).getCategoryName().trim())){
    	    				Categorylist.remove(configEntity.get(i).getCategoryName().trim());
    	    			}
    	    	}
    		}	
    		
			for(String custCategory : Categorylist){
        		int setcount = ++count;
	    		String paymentTermsString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
	    				custCategory, "CC" + setcount, companyId,setcount, "");
				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", paymentTermsString));
    		
				ArrayList<String> typeList = getleadCategoryTypeList(custCategory);
				
				for(String typeName : typeList){
					int setTypecount = ++typeCount;
					String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
							custCategory, typeName, "TT" + setTypecount,"CC" + setcount, companyId, setTypecount);
					Queue commTypeQueue = QueueFactory.getQueue("TypeValue-queue");
					commTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
				}
				
    		}
    	}
    	else if(screenName==Screen.QUOTATIONCATEGORY){

    		List<String> QuotationList = getQuotCategory();
    		List<ConfigCategory> configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", 3).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(QuotationList.contains(configEntity.get(i).getCategoryName().trim())){
    	    				QuotationList.remove(configEntity.get(i).getCategoryName().trim());
    	    			}
    	    	}
    		}	
    			for(String custCategory : QuotationList){
            		int setcount = ++count;
    	    		String paymentTermsString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    	    				custCategory, "CC" + setcount, companyId,setcount, "");
    				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
    				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", paymentTermsString));
        		
    				ArrayList<String> typeList = getleadCategoryTypeList(custCategory);
    				
    				for(String typeName : typeList){
    					int setTypecount = ++typeCount;
    					String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    							custCategory, typeName, "TT" + setTypecount,"CC" + setcount, companyId, setTypecount);
    					Queue commTypeQueue = QueueFactory.getQueue("TypeValue-queue");
    					commTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
    				}
    				
        		}
    	
    	}
    	
    	else if(screenName==Screen.TICKETCATEGORY){   //(21),

    		List<String> QuotationList = getTicketCategory();
    		List<ConfigCategory> configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", 21).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(QuotationList.contains(configEntity.get(i).getCategoryName().trim())){
    	    				QuotationList.remove(configEntity.get(i).getCategoryName().trim());
    	    			}
    	    	}
    		}	
    			for(String custCategory : QuotationList){
            		int setcount = ++count;
    	    		String paymentTermsString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    	    				custCategory, "CC" + setcount, companyId,setcount, "");
    				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
    				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", paymentTermsString));
        		
    				ArrayList<String> typeList = getleadCategoryTypeList(custCategory);
    				
    				for(String typeName : typeList){
    					int setTypecount = ++typeCount;
    					String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    							custCategory, typeName, "TT" + setTypecount,"CC" + setcount, companyId, setTypecount);
    					Queue commTypeQueue = QueueFactory.getQueue("TypeValue-queue");
    					commTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
    				}
    				
        		}
    	
    	}
    	else if(screenName==Screen.CONTRACTCATEGORY){

    		List<String> Categorylist = getleadCategory();
    		List<ConfigCategory> configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", 4).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(Categorylist.contains(configEntity.get(i).getCategoryName().trim())){
    	    				Categorylist.remove(configEntity.get(i).getCategoryName().trim());
    	    			}
    	    	}
    		}	
    			for(String custCategory : Categorylist){
            		int setcount = ++count;
    	    		String paymentTermsString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    	    				custCategory, "CC" + setcount, companyId,setcount, "");
    				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
    				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", paymentTermsString));
        		
    				ArrayList<String> typeList = getleadCategoryTypeList(custCategory);
    				
    				for(String typeName : typeList){
    					int setTypecount = ++typeCount;
    					String typeInstring = getTypeDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
    							custCategory, typeName, "TT" + setTypecount,"CC" + setcount, companyId, setTypecount);
    					Queue commTypeQueue = QueueFactory.getQueue("TypeValue-queue");
    					commTypeQueue.add(TaskOptions.Builder.withUrl("/slick_erp/typevaluetaskqueue").param("typeValuekey", typeInstring));
    				}
    				
        		}
    	
    	}
    	
    	numbergenration.setNumber(typeCount);
  		GenricServiceImpl impl1 = new GenricServiceImpl();
  		impl1.save(numbergenration);
  		
    	numbergen.setNumber(count);
  		GenricServiceImpl impl = new GenricServiceImpl();
  		impl.save(numbergen);
	}

   
	private List<String> getTicketCategory() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("New request");
		list.add("Repetitive");
		list.add("Complaint");
		//list.add("Product Sale");
		return list;
	}

	private List<String> getQuotCategory() {
		
		ArrayList<String> list = new ArrayList<String>();
		list.add("Residential");
		list.add("Commercial");
		//list.add("Society");
		//list.add("Product Sale");
		return list;
	}

	private ArrayList<String> getleadCategoryTypeList(String custCategory) {
		ArrayList<String> list = new ArrayList<String>();
		if(custCategory.trim().equals("Residential")){
			list.add("Flat");
			list.add("Bunglow");
			list.add("Villa");
			list.add("Row House");
			list.add("Club House");

		}
		else if(custCategory.trim().equals("Commercial")){
			list.add("Hotel");
			list.add("Hospital");
			list.add("Restuarant");
			list.add("Office");
			list.add("Warehouse");
		}
//		else if(custCategory.trim().equals("Society")){
//			list.add("Common area");
//			list.add("Community hall");
//			list.add("Parking area");
//			list.add("Club House");
//		}
//		else if(custCategory.trim().equals("Product Sale")){
//			list.add("Glue Traps");
//			list.add("Rodent Station");
//			list.add("Fly Catcher Machine");
//		}
		return list;
	}

	private List<String> getleadCategory() {
		ArrayList<String> list = new ArrayList<String>();
			list.add("Residential");
			list.add("Commercial");
			//list.add("Society");
			//list.add("Product Sale");
			return list;
	}

	private ArrayList<String> getCustomerCategoryTypeList(String custCategory) {
		ArrayList<String> list = new ArrayList<String>();
		if(custCategory.trim().equals("Residential")){
			list.add("Flat");
			list.add("Bunglow");
			list.add("Villa");
			list.add("Row House");
			list.add("Club House");

		}
		else if(custCategory.trim().equals("Commercial")){
			list.add("Hotel");
			list.add("Hospital");
			list.add("Restuarant");
			list.add("Office");
			list.add("Warehouse");
		}
//		else if(custCategory.trim().equals("Society")){
//			list.add("Common area");
//			list.add("Community hall");
//			list.add("Parking area");
//			list.add("Club House");
//		}
//		else if(custCategory.trim().equals("Government")){
//			list.add("Govt office");
//			list.add("Quarters");
//			list.add("Railway stations");
//		}
//		else if(custCategory.trim().equals("Industrial")){
//			list.add("Plant");
//			list.add("Manufacturing Plant");
//			list.add("Processing Plan");
//			list.add("Production Unit");
//		}
		return list;
	}

	private ArrayList<String> getcustomerCategory() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Residential");
		list.add("Commercial");
		//list.add("Society");
		//list.add("Government");
		//list.add("Industrial");
		return list;
	}

	private void createconfigs(long companyId, Screen screenName) {

		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Config").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	int count = (int) lastNumber;
		
	    	if(screenName.equals(Screen.CUSTOMERGROUP)){
				List<String> customerGrouplist = getCustomerGroup();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 29).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(customerGrouplist.contains(configEntity.get(i).getName().trim())){
			    				customerGrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : customerGrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	
	    	}
	    	else if(screenName.equals(Screen.CUSTOMERPRIORITY)){
				List<String> customerGrouplist = getcustomerPriority();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 9).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(customerGrouplist.contains(configEntity.get(i).getName().trim())){
			    				customerGrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : customerGrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	
	    	else if(screenName.equals(Screen.CUSTOMERLEVEL)){
				List<String> customerLevellist = getcustomerLevelList();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 3).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(customerLevellist.contains(configEntity.get(i).getName().trim())){
			    				customerLevellist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : customerLevellist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}

	    	else if(screenName.equals(Screen.LEADSTATUS)){
				List<String> leadtstatuslist = getleadstatuslist();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 6).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtstatuslist.contains(configEntity.get(i).getName().trim())){
			    				leadtstatuslist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtstatuslist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.LEADGROUP)){
				List<String> leadtgrouplist = getleadgroup();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 32).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	
	    	}
	    	else if(screenName.equals(Screen.LEADPRIORITY)){
				List<String> leadtgrouplist = getleadPrioriry();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 7).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	
	    	}
	    	else if(screenName.equals(Screen.LEADUNSUCCESFULREASONS)){
				List<String> leadtstatlist = getleadUnsuccessfullReasons();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 109).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtstatlist.contains(configEntity.get(i).getName().trim())){
			    				leadtstatlist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtstatlist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	
	    	}
	    	else if(screenName.equals(Screen.QUOTATIONGROUP)){
				List<String> quotgrouplist = getquotationGroup();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 30).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(quotgrouplist.contains(configEntity.get(i).getName().trim())){
			    				quotgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : quotgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.QUOTATIONPRIORITY)){
				List<String> quotprioritylist = getleadPrioriry();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 10).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(quotprioritylist.contains(configEntity.get(i).getName().trim())){
			    				quotprioritylist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : quotprioritylist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.QUOTATIONTYPE)){
				List<String> quottypelist = getquotType();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 52).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(quottypelist.contains(configEntity.get(i).getName().trim())){
			    				quottypelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : quottypelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.CONTRACTGROUP)){
				List<String> congrouplist = getcontractGroup();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 31).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(congrouplist.contains(configEntity.get(i).getName().trim())){
			    				congrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : congrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.DONOTRENEW)){
				List<String> leadtgrouplist = getleadPrioriry();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 115).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}	
	    	else if(screenName.equals(Screen.CANCELLATIONREMARK)){
				List<String> leadtgrouplist = getCancellationRemarkList();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 116).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	// Regular, Complaint,Trial, Complementory

	    	
	    	else if(screenName.equals(Screen.SERVICETYPE)){   
				List<String> servicetypelist = getserviceType();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 78).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(servicetypelist.contains(configEntity.get(i).getName().trim())){
			    				servicetypelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : servicetypelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.COMPLAINPRIORITY)){   
				List<String> servicetypelist = getComplainPriority();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 21).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(servicetypelist.contains(configEntity.get(i).getName().trim())){
			    				servicetypelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : servicetypelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.SPECIFICREASONFORRESCHEDULING)){   
				List<String> servicetypelist = getspecificResaonRescheduleList();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 89).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(servicetypelist.contains(configEntity.get(i).getName().trim())){
			    				servicetypelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : servicetypelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    
	    	else if(screenName.equals(Screen.DONOTRENEW)){   
				List<String> donotrenewlist = getDoNotRenewList();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 115).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(donotrenewlist.contains(configEntity.get(i).getName().trim())){
			    				donotrenewlist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : donotrenewlist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.CATCHTRAPPESTNAME)){   
				List<String> pestnamelist = getpestNameList();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 92).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(pestnamelist.contains(configEntity.get(i).getName().trim())){
			    				pestnamelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : pestnamelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	
	    	else if(screenName.equals(Screen.COMPLAINSTATUS)){   
				List<String> pestnamelist = getComplainStatus();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 23).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(pestnamelist.contains(configEntity.get(i).getName().trim())){
			    				pestnamelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : pestnamelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    
	    	else if(screenName.equals(Screen.NUMBERRANGE)){   
				List<String> pestnamelist = getNumberRangeList();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 91).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(pestnamelist.contains(configEntity.get(i).getName().trim())){
			    				pestnamelist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : pestnamelist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	
	    	else if(screenName.equals(Screen.MMNDIRECTION)){
				List<String> leadtgrouplist = getmmnDirectionlist();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 56).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.MMNTRANSACTIONTYPE)){
				List<String> leadtgrouplist = getMMNTransactionType();
				List<MaterialMovementType> configEntity = ofy().load().type(MaterialMovementType.class).filter("companyId", companyId)
						.list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getMmtType().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getMmtType().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
					MaterialMovementType mmntype = new MaterialMovementType();
					mmntype.setCompanyId(companyId);
					if(strname.trim().equals("Scrap") || strname.trim().equals("Write off") ){
						mmntype.setMmtName(strname);
						mmntype.setMmtType("OUT");
					}
					else{
						mmntype.setMmtName(strname);
						if(strname.equalsIgnoreCase("TRANSFEROUT")){
							mmntype.setMmtType("TRANSFEROUT");
						}
						else{
							mmntype.setMmtType("TransferIN");
						}
					}
					
					mmntype.setMmtStatus(true);
					GenricServiceImpl impl = new GenricServiceImpl();
			  		impl.save(mmntype);
				}
	    	}
	    	else if(screenName.equals(Screen.PERSONTYPE)){
				List<String> leadtgrouplist = getEntityTypelist();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 65).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	else if(screenName.equals(Screen.UNITOFMEASUREMENT)){
				List<String> leadtgrouplist = getUnitOfMeasurement();
				List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
						.filter("type", 47).list();
		
				if(configEntity.size()!=0){
					for(int i=0;i<configEntity.size();i++){
			    			if(leadtgrouplist.contains(configEntity.get(i).getName().trim())){
			    				leadtgrouplist.remove(configEntity.get(i).getName().trim());
			    			}
			    	}
				}
				for(String strname : leadtgrouplist){
		    		int setcount = ++count;
		    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(screenName), strname, companyId,setcount);
		    		
		    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
		    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
		    	}
	    	}
	    	
	    	numbergen.setNumber(count);
	  		GenricServiceImpl impl = new GenricServiceImpl();
	  		impl.save(numbergen);
	
	}
	

	private List<String> getComplainPriority() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("High");    
		list.add("Medium");
		list.add("Low");

		return list;
	}
	
	private List<String> getComplainStatus() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Created");    
		list.add("Completed");
	
		return list;
	}
	
	private List<String> getNumberRangeList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Billing");    
		list.add("Non-Billing");
	
		return list;
	}

	private List<String> getCancellationRemarkList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Client Renewal is pending");     
		list.add("Contract re-punched with changes/correction");
		list.add("Customer did not want to avail service");
		list.add("Operator could not travel due to long distance/out of city COVID -19");     
		list.add("Order lost - Contract cancelled by customer due to poor service");
		list.add("Order lost - Permanent Business Closure");
		list.add("Order lost - to competitor with low rate");     
		list.add("Services not provided due to operator absent");
		list.add("Site in-charge / customer not available on service location");
		list.add("Stopped service due to non-payment");     
		list.add("Temporary Business Closure");
		list.add("Others");
		return list;

	}

	private List<String> getcustomerLevelList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Gold");    
		list.add("Silver");
		list.add("Platinum");
		//list.add("Rats");
		return list;
	}

	 
	private List<String> getpestNameList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Rat(s)");
		list.add("Cockroach(es)");
		list.add("Lizard(s)");
		list.add("Mosquito(es)");
		list.add("Snake(s)");
		list.add("Flies");
		return list;
	}

 
	private List<String> getDoNotRenewList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Stopped due to unsatisfactory service");
		list.add("Lost to competitor with lesser rate");
		list.add("Service No longer required by customer");
		list.add("service stopped due to non-payment");
		list.add("Others -As & When Required");
		list.add("Customer relocated");
		return list;
	}

 
	private List<String> getspecificResaonRescheduleList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("As per client reqeust");
		list.add("Contract Cancelled");
		list.add("Due to emergency COVID-19");
		list.add("Premises found Closed");
		list.add("Schedule Issue");
		return list;
	}

	private List<String> getcontractGroup() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("AMC");
		list.add("One Time");
		list.add("Single");
		list.add("Monthly");
		return list;
	}

	private List<String> getquotType() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("AMC");
		list.add("One time");
		list.add("Short term");
		return list;
	}

	private List<String> getmmnDirectionlist() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("OUT");
		list.add("IN");
		list.add("TRANSFEROUT");
		list.add("TRANSFERIN");
		return list;
	}

	private List<String> getUnitOfMeasurement() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("KG");
		list.add("Gram");
		list.add("Nos");
		list.add("ML");
		return list;
	}

	private List<String> getEntityTypelist() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Employee");
		list.add("Customer");
		list.add("Vendor");
		return list;
	}

	private List<String> getMMNTransactionType() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Scrap");
		list.add("Write off");
		list.add("TRANSFEROUT");
		list.add("TransferIN");
		return list;
	}

	private List<String> getquotationGroup() {   //"With Inspection 	Without Inspection"

		ArrayList<String> list = new ArrayList<String>();
		list.add("With Inspection");
		list.add("Without Inspection");  //Revised
		list.add("Revised");
		return list;
	}

	private List<String> getleadUnsuccessfullReasons() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Price too high");
		list.add("Lost to other company");
		list.add("Unable to meet customer requirement");
		list.add("Cancelled by customer");
		list.add("If any Others");
		return list;
	}

	private List<String> getleadPrioriry() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("High");
		list.add("Low");
		list.add("Medium");
		return list;
	}

	private List<String> getleadgroup() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Website");
		list.add("Reference from Employee");
		list.add("Reference from Customer");
		list.add("Cold Call");
		list.add("Digital Marketing");
		list.add("Networking Group");
		return list;
	}

	private List<String> getleadstatuslist() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Open");
		list.add("Successful");
		list.add("Unsuccessful");
		return list;
	}

	private List<String> getcustomerPriority() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("High");
		list.add("Low");
		list.add("Medium");
		return list;
	}

	private List<String> getCustomerGroup() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Reference");  // Reference, Marketting, Website
		list.add("Marketing");
		list.add("Website");
		return list;
	}

	private ArrayList<String> getcustomerConfig() {
		ArrayList<String> customerConfiglist = new ArrayList<String>();
		customerConfiglist.add("Customer Group");
		customerConfiglist.add("Customer Priority");  //Customer Level
		customerConfiglist.add("Customer Level");
		

		return customerConfiglist;
	}

	@Override
	public String createDefaultConfiguration(long companyId) {

		CompanyRegistrationServiceImpl companyreg = new CompanyRegistrationServiceImpl();
		String accessurl = "my";
		Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(comp!=null) {
			accessurl = comp.getAccessUrl();
		}
		companyreg.createModuleNamesAndDocumentNames(companyId,accessurl);
		companyreg.createSettingModuleConfiguration(companyId);
		
		List<String> processNamelist = companyreg.getProcessNamelist();
		List<ProcessName> processNameEntitylist = ofy().load().type(ProcessName.class).filter("companyId", companyId).list();

		if(processNameEntitylist.size()!=0){
			for(int i=0;i<processNameEntitylist.size();i++){
	    			if(processNamelist.contains(processNameEntitylist.get(i).getProcessName().trim())){
	    				processNamelist.remove(processNameEntitylist.get(i).getProcessName().trim());
	    			}
	    	}
			if(processNamelist.size()!=0){
				ArrayList<ProcessName> processNameList=new ArrayList<ProcessName>();
				for(int i=0;i<processNamelist.size();i++)
				{
					ProcessName configEntity=new ProcessName();
					configEntity.setCompanyId(companyId);
					configEntity.setProcessName(processNamelist.get(i));
					configEntity.setStatus(true);
					configEntity.setCount(i+1);
					processNameList.add(configEntity);
				}
				ofy().save().entities(processNameList).now();
			}
		}
		
		

		List<String> processtypelist = companyreg.getprocessTypelist();
		List<Config> processTypeEntitylist = ofy().load().type(Config.class).filter("type", 73).filter("companyId", companyId).list();
		if(processTypeEntitylist.size()!=0){
			for(int i=0;i<processTypeEntitylist.size();i++){
	    			if(processtypelist.contains(processTypeEntitylist.get(i).getName().trim())){
	    				processtypelist.remove(processTypeEntitylist.get(i).getName().trim());
	    			}
	    	}
			if(processtypelist.size()!=0){
				ArrayList<Config> processNameList1=new ArrayList<Config>();
				for(int i=0;i<processtypelist.size();i++)
				{
					Config configEntity1 = new Config();
					configEntity1.setCompanyId(companyId);
					configEntity1.setType(73);
					configEntity1.setName(processtypelist.get(i));
					configEntity1.setStatus(true);
					configEntity1.setCount(i + 1);
					processNameList1.add(configEntity1);
				}
				ofy().save().entities(processNameList1).now();
			}
		}
		
		
		companyreg.createNumberGeneration(companyId);
		
		return "Configuration Creation Process Started Successfully.It will take some time to update.";
	}

	@Override
	public String createScreenMenuData(long companyId) {
		
		DocumentTaskQueue dtq = new DocumentTaskQueue();
		dtq.createMenuScreenConfiguration(companyId);
		
		return  "Screen Menu Configuration Started Successfully.It will take some time to update.";
	}

	@Override
	public String createUserRoleData(long companyId) {
		
		try {
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "Define User Role").param("companyId", companyId+""));
			
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception Defining user role =="+e.getMessage());
		}
		return "User Role Creation Started Successfully.It will take some time to update.";
	}

	@Override
	public String deleteAllScreenConfiguration(long companyId) {
		
		DemoConfigurationImpl demoimpl = new DemoConfigurationImpl();
		ArrayList<String> modulelist = new ArrayList<String>();
		modulelist.addAll(demoimpl.moduleNames());
		
		List<ScreenMenuConfiguration> screenMenuEntityList = ofy().load().type(ScreenMenuConfiguration.class).filter("companyId", companyId).list();
	     if (screenMenuEntityList!=null&&screenMenuEntityList.size()!=0) {
	    	 for (int i = 0; i < screenMenuEntityList.size(); i++) {
	    		 if (modulelist.contains(screenMenuEntityList.get(i).getModuleName().trim())){
	    			 modulelist.remove(screenMenuEntityList.get(i).getModuleName().trim());
	    		 }
			} 	 
	    	 
		}

		ofy().delete().entities(screenMenuEntityList).now();

			
		
		return "Screen Menu Configuration Data Deleted Successfully..!!";
	}

	@Override
	public String copyConfiguration(long companyId, String fromLink,String toLink, ArrayList<ScreenName> entityList) {
		String referenceLink=toLink+"/slick_erp/copyConfigurationService";
		logger.log(Level.SEVERE,"referenceLink URL : "+referenceLink);
		
		URL url = null;
		try {
			url = new URL("https://"+referenceLink);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
			return "Please add valid url. "+e.getMessage();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
			return "Error occured in establishing connection. "+e.getMessage();
		}
		
		Gson gson = new Gson();
		String entity = gson.toJson(entityList);
		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "copyConfiguration")
				.param("companyId", companyId+"")
				.param("fromLink", fromLink )
				.param("toLink", toLink )
				.param("entityList", entity));
		
		
		return "Process has been started, please check after some time.";
	}

	
}
