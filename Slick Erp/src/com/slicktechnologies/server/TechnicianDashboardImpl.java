package com.slicktechnologies.server;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.TechnicianDashboardService;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.complain.Complain;
import static com.googlecode.objectify.ObjectifyService.ofy;

public class TechnicianDashboardImpl extends RemoteServiceServlet implements TechnicianDashboardService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7921712315651862348L;


	@Override
	public Integer getReWorkdetails(long companyId, ArrayList<Service> servicelist) {

		int totalcomplaints=0;
		for(int i=0;i<servicelist.size();i++){
			
			List<Complain> complainlist = ofy().load().type(Complain.class).filter("companyId", companyId).filter("existingServiceId", servicelist.get(i).getCount()).list();
			totalcomplaints+=complainlist.size();
		}
		
		return totalcomplaints;
	}

}
