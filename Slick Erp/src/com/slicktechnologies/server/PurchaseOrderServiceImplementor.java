package com.slicktechnologies.server;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderService;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class PurchaseOrderServiceImplementor extends RemoteServiceServlet implements PurchaseOrderService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1458855982749497366L;

	
	@Override
	public void approvalTransaction(PurchaseOrder purchaseorder) {
		
		if(purchaseorder!=null&&purchaseorder.getStatus().equals(ConcreteBusinessProcess.APPROVED))
		{
			createPurchaseBillingDocuments(purchaseorder);
			saveTaxesAndCharges(purchaseorder);
		}
		
	}
	
	@GwtIncompatible
	protected void createPurchaseBillingDocuments(PurchaseOrder poEntity)
	{
		List<VendorDetails> personInfo=poEntity.getVendorDetails();
		PersonInfo personInfoEntity=new PersonInfo();
		for(int i=0;i<personInfo.size();i++)
		{
			if(personInfo.get(i).getStatus()==true)
			{
				personInfoEntity.setCount(personInfo.get(i).getVendorId());
				personInfoEntity.setFullName(personInfo.get(i).getVendorName());
				personInfoEntity.setCellNumber(personInfo.get(i).getVendorPhone());
			}
		}
		
		Logger billingLogger=Logger.getLogger("Billing Logger");
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
		billingLogger.log(Level.SEVERE,"Current Date "+currentDate);

		for(int i=0;i<poEntity.getPaymentTermsList().size();i++)
		{
			ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			PaymentTerms paymentTerms=new PaymentTerms();
			Date conStartDate=null;
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			
			
			
			/***********************Invoice Date******************************/
			billingLogger.log(Level.SEVERE,"SO Date As per IST"+DateUtility.getDateWithTimeZone("IST",poEntity.getPODate()));
			billingLogger.log(Level.SEVERE,"SO Date As Per Saved"+poEntity.getPODate());
			
			
			Calendar calInvoiceDate = Calendar.getInstance();
			if(poEntity.getPODate()!=null){
				Date salesDate=DateUtility.getDateWithTimeZone("IST", poEntity.getPODate());
				calInvoiceDate.setTime(salesDate);
			}
			else{
				conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
				calInvoiceDate.setTime(conStartDate);
			}
			
			
			calInvoiceDate.add(Calendar.DATE, poEntity.getPaymentTermsList().get(i).getPayTermDays());
			Date calculatedInvoiceDate=null;
			try {
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
				calInvoiceDate.set(Calendar.MINUTE,59);
				calInvoiceDate.set(Calendar.SECOND,59);
				calInvoiceDate.set(Calendar.MILLISECOND,999);
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
				
				
				if(currentDate.after(calculatedInvoiceDate))
				{
					calculatedInvoiceDate=currentDate;
				}
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***********************************************************************************/
			
			/*********************************Billing Date************************************/
			
			Date calBillingDate=null;
			Calendar calBilling=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
			calBilling.add(Calendar.DATE, -2);
			try {
				calBilling.set(Calendar.HOUR_OF_DAY,23);
				calBilling.set(Calendar.MINUTE,59);
				calBilling.set(Calendar.SECOND,59);
				calBilling.set(Calendar.MILLISECOND,999);
				calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
				
				if(currentDate.after(calBillingDate)){
					calBillingDate=currentDate;
				}
				
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***************************************************************************************/
			
			/********************************Payment Date******************************************/
			
			Date calPaymentDate=null;
			int creditVal=0;
			if(poEntity.getCreditDays()!=null){
				creditVal=poEntity.getCreditDays();
			}
			
			Calendar calPayment=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calPayment.setTime(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
			
			try {
				calPayment.set(Calendar.HOUR_OF_DAY,23);
				calPayment.set(Calendar.MINUTE,59);
				calPayment.set(Calendar.SECOND,59);
				calPayment.set(Calendar.MILLISECOND,999);
				calPayment.add(Calendar.DATE, creditVal);
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***********************************************************************************/
			
			
				billingDocEntity.setPersonInfo(personInfoEntity);
				billingDocEntity.setContractCount(poEntity.getCount());
		
			if(poEntity.getNetpayble()!=0){
				billingDocEntity.setTotalSalesAmount(poEntity.getNetpayble());
			}
			
			salesProdLis=this.retrieveSalesProducts(poEntity.getPaymentTermsList().get(i).getPayTermPercent(),poEntity.getProductDetails(),poEntity.getCompanyId());
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			if(salesOrdArr.size()!=0)
				billingDocEntity.setSalesOrderProducts(salesOrdArr);
			if(poEntity.getCompanyId()!=null)
				billingDocEntity.setCompanyId(poEntity.getCompanyId());
			if(calBillingDate!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST",calBillingDate));
				billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
			}
			
			if(calculatedInvoiceDate!=null){
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
			}
			if(calPaymentDate!=null){
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
				billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
			}
			
			
			if(poEntity.getPaymentMethod()!=null)
				billingDocEntity.setPaymentMethod(poEntity.getPaymentMethod());
			if(poEntity.getApproverName()!=null)
				billingDocEntity.setApproverName(poEntity.getApproverName());
			if(poEntity.getEmployee()!=null)
				billingDocEntity.setEmployee(poEntity.getEmployee());
			if(poEntity.getBranch()!=null)
				billingDocEntity.setBranch(poEntity.getBranch());
			if(poEntity.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				//billTaxLis=this.listForBillingTaxes(salesProdLis);
				billTaxLis=this.listForBillingTaxes(poEntity.getPaymentTermsList().get(i).getPayTermPercent(),poEntity.getProductTaxes());
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
			}
			
			billingDocEntity.setOrderCreationDate(poEntity.getCreationDate());

			double totBillAmt=this.getTotalBillAmt(salesProdLis);
			billingDocEntity.setTotalBillingAmount(totBillAmt);
			
			int payTrmsDays=poEntity.getPaymentTermsList().get(i).getPayTermDays();
			double payTrmsPercent=poEntity.getPaymentTermsList().get(i).getPayTermPercent();
			String payTrmsComment=poEntity.getPaymentTermsList().get(i).getPayTermComment().trim();
			
			
			if(poEntity.getcForm()!=null){
				billingDocEntity.setOrderCformStatus(poEntity.getcForm());
			}
			else{
				billingDocEntity.setOrderCformStatus("");
			}
			
			if(poEntity.getCstpercent()!=0){
				billingDocEntity.setOrderCformPercent(poEntity.getCstpercent());
			}
			else{
				billingDocEntity.setOrderCformPercent(-1);
			}
			
	//***********************changes made by rohan for saving gross value *************	
			double grossValue=poEntity.getTotalAmount();
			
			
			billingDocEntity.setGrossValue(grossValue);
			
	//********************************changes ends here *******************************		
			paymentTerms.setPayTermDays(payTrmsDays);
			paymentTerms.setPayTermPercent(payTrmsPercent);
			paymentTerms.setPayTermComment(payTrmsComment);
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPEPURCHASE);
			billingDocEntity.setStatus(BillingDocument.CREATED);
			billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAP);
			
			 /** Date 09-06-2018 added by vijay for roundoff amount **/
			double roundoff=0;
			if(poEntity.getRoundOffAmount()!=0){
				roundoff=poEntity.getRoundOffAmount()/poEntity.getPaymentTermsList().size();
				billingDocEntity.setRoundOffAmt(roundoff);
			}
			/**
			 * ends here
			 */
			
			arrbilling.add(billingDocEntity);
			/******************************************************************************/
			GenricServiceImpl impl=new GenricServiceImpl();
			if(arrbilling.size()!=0){
				impl.save(arrbilling);
			}
		}
	}
	
	@GwtIncompatible
	protected void saveTaxesAndCharges(PurchaseOrder porder)
	{
		if(porder.getCount()!=0){
			final GenricServiceImpl genimpl=new GenricServiceImpl();
			System.out.println("kasbdsdadads");
			TaxesAndCharges taxChargeEntity=new TaxesAndCharges();
			List<ContractCharges> listaxcharge=this.saveCharges(porder.getProductCharges());
			ArrayList<ContractCharges> arrtaxchrg=new ArrayList<ContractCharges>();
			arrtaxchrg.addAll(listaxcharge);
			taxChargeEntity.setContractId(porder.getCount());
			taxChargeEntity.setIdentifyOrder(AppConstants.BILLINGPURCHASEFLOW.trim());
			taxChargeEntity.setTaxesChargesList(arrtaxchrg);
			taxChargeEntity.setCompanyId(porder.getCompanyId());
			
			genimpl.save(taxChargeEntity);
		}
	}
	
	private List<ContractCharges> saveCharges(List<ProductOtherCharges> chargesList)
	{
		ArrayList<ContractCharges> arrCharge=new ArrayList<ContractCharges>();
		double calcBalAmt=0;
		for(int i=0;i<chargesList.size();i++)
		{
			ContractCharges chargeDetails=new ContractCharges();
			chargeDetails.setTaxChargeName(chargesList.get(i).getChargeName());
			chargeDetails.setTaxChargePercent(chargesList.get(i).getChargePercent());
			chargeDetails.setTaxChargeAbsVal(chargesList.get(i).getChargeAbsValue());
			chargeDetails.setTaxChargeAssesVal(chargesList.get(i).getAssessableAmount());
			if(chargesList.get(i).getChargeAbsValue()!=0){
				calcBalAmt=chargesList.get(i).getChargeAbsValue();
			}
			if(chargesList.get(i).getChargePercent()!=0){
				calcBalAmt=chargesList.get(i).getChargePercent()*chargesList.get(i).getAssessableAmount()/100;
			}
			chargeDetails.setChargesBalanceAmt(calcBalAmt);
			arrCharge.add(chargeDetails);
		}
		return arrCharge;
		
	}
	
	@GwtIncompatible
	private List<SalesOrderProductLineItem> retrieveSalesProducts(double payTermPercent,List<ProductDetailsPO> purchaseProd,long companyId)
	{
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<purchaseProd.size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", purchaseProd.get(i).getProductID()).filter("companyId",companyId).first().now();
			
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
//			SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProdId(purchaseProd.get(i).getProductID());
			salesOrder.setProdCategory(purchaseProd.get(i).getProductCategory());
			salesOrder.setProdCode(purchaseProd.get(i).getProductCode());
			salesOrder.setProdName(purchaseProd.get(i).getProductName());
			salesOrder.setQuantity(purchaseProd.get(i).getProductQuantity());
			salesOrder.setOrderDuration(0);
			salesOrder.setOrderServices(0);
			totalTax=this.removeTaxAmt(purchaseProd.get(i).getPrduct());
			prodPrice=purchaseProd.get(i).getProdPrice()-totalTax;
			salesOrder.setPrice(prodPrice);
			
			Tax vatTax=new Tax();
			vatTax.setPercentage(purchaseProd.get(i).getVat());
			
			Tax servTax=new Tax();
			servTax.setPercentage(purchaseProd.get(i).getTax());
			
			salesOrder.setVatTax(vatTax);
			salesOrder.setServiceTax(servTax);
			salesOrder.setProdPercDiscount(purchaseProd.get(i).getDiscount());
			salesOrder.setDiscountAmt(purchaseProd.get(i).getDiscountAmt());
			salesOrder.setUnitOfMeasurement(purchaseProd.get(i).getUnitOfmeasurement());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			
			
		//  Vijay added this code for setting HSN Code Date : 03-07-2017
			if(purchaseProd.get(i).getPrduct().getHsnNumber()!=null && !purchaseProd.get(i).getPrduct().getHsnNumber().equals(""))
			salesOrder.setHsnCode(purchaseProd.get(i).getPrduct().getHsnNumber());
			
					
//			if(purchaseProd.get(i).getDiscount()!=0){
//				percAmt=prodPrice-(prodPrice*purchaseProd.get(i).getDiscount()/100);
//				percAmt=percAmt*purchaseProd.get(i).getProductQuantity();
//			}
//			if(purchaseProd.get(i).getDiscount()==0){
//				percAmt=prodPrice*purchaseProd.get(i).getProductQuantity();
//			}
			
/***************** new code area calculation and discount on total *******************/
			
			if((purchaseProd.get(i).getDiscount()==null && purchaseProd.get(i).getDiscount()==0) && (purchaseProd.get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				percAmt=prodPrice;
			}
			
			else if((purchaseProd.get(i).getDiscount()!=null)&& (purchaseProd.get(i).getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				
				percAmt=prodPrice-(prodPrice*purchaseProd.get(i).getDiscount()/100);
				percAmt=percAmt-(purchaseProd.get(i).getDiscountAmt());
				percAmt=percAmt*purchaseProd.get(i).getProductQuantity();
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					if(purchaseProd.get(i).getDiscount()!=null && purchaseProd.get(i).getDiscount()!=0){
						System.out.println("inside getPercentageDiscount oneof the null condition");
						percAmt=prodPrice-(prodPrice*purchaseProd.get(i).getDiscount()/100);
						
					}
					else 
					{
						System.out.println("inside getDiscountAmt oneof the null condition");
						percAmt=prodPrice-(purchaseProd.get(i).getDiscountAmt());
					}
					percAmt=percAmt*purchaseProd.get(i).getProductQuantity();
			}
			
			
			salesOrder.setTotalAmount(percAmt);
			
			if(payTermPercent!=0){
				baseBillingAmount=(percAmt*payTermPercent)/100;
			}
			System.out.println("Base Billing Amount Through Payment Terms"+baseBillingAmount);
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			/**
			 * Date : 05-08-2017 By ANIL
			 */
			salesOrder.setBasePaymentAmount(baseBillingAmount);
			salesOrder.setIndexVal(i+1);
			/////////////////////////////////////////////////////////////////////////////////
			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
	}
	
	private double getTotalBillAmt(List<SalesOrderProductLineItem> lisForTotalBill)
	{
		double saveTotalBillAmt=0;
		for(int i=0;i<lisForTotalBill.size();i++)
		{
			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getBaseBillingAmount();
		}
		
		System.out.println("Save Total Billing Amount"+saveTotalBillAmt);
		return saveTotalBillAmt;
	}
	
	
	private List<ContractCharges> listForBillingTaxes(double percPay,List<ProductOtherCharges> taxesListPO){
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
		double assessValue=0;
		for(int i=0;i<taxesListPO.size();i++){
			ContractCharges taxDetails=new ContractCharges();
			taxDetails.setTaxChargeName(taxesListPO.get(i).getChargeName());
			taxDetails.setTaxChargePercent(taxesListPO.get(i).getChargePercent());
			assessValue=taxesListPO.get(i).getAssessableAmount()*percPay/100;
			taxDetails.setTaxChargeAssesVal(assessValue);
			
			taxDetails.setIdentifyTaxCharge(taxesListPO.get(i).getIndexCheck());
			arrBillTax.add(taxDetails);
		}
		return arrBillTax;
	}
	
	
	
	public double removeTaxAmt(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			/**
			 * Date : 03-07-2017 By vijay 
			 * Checking GST printable name 
			 */
			if(entity.getServiceTax().getTaxPrintName()!=null && ! entity.getServiceTax().getTaxPrintName().equals("")
					   && entity.getVatTax().getTaxPrintName()!=null && ! entity.getVatTax().getTaxPrintName().equals(""))
			{
				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
			}
			else
			{
				double removeServiceTax=(entity.getPrice()/(1+service/100));
				retrServ=(removeServiceTax/(1+vat/100));
				retrServ=entity.getPrice()-retrServ;
//				double taxPerc=service+vat;
//				retrServ=entity.getPrice()/(1+taxPerc/100);
//				retrServ=entity.getPrice()-retrServ;
				
			}
		}
		tax=retrVat+retrServ;
		System.out.println("Shared Tax Return"+tax);
		return tax;
	}
	
	
	/************************************* SERVICE PO BILLING GENERATION ************************************/
	
	/**
	 * This method Creates billing for service po
	 * Date : 18-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	public void creatingBillingOfServicePo(ServicePo purchaseorder ){

		if(purchaseorder!=null&&purchaseorder.getStatus().equals(ConcreteBusinessProcess.APPROVED))
		{
			createServicePoBillingDocuments(purchaseorder);
			saveTaxesAndCharges(purchaseorder);
		}
	}
	
	protected void saveTaxesAndCharges(ServicePo porder)
	{
		if(porder.getCount()!=0){
			final GenricServiceImpl genimpl=new GenricServiceImpl();
			System.out.println("kasbdsdadads");
			TaxesAndCharges taxChargeEntity=new TaxesAndCharges();
			List<ContractCharges> listaxcharge=this.saveCharges(porder.getProductCharges());
			ArrayList<ContractCharges> arrtaxchrg=new ArrayList<ContractCharges>();
			arrtaxchrg.addAll(listaxcharge);
			taxChargeEntity.setContractId(porder.getCount());
			taxChargeEntity.setIdentifyOrder(AppConstants.BILLINGPURCHASEFLOW.trim());
			taxChargeEntity.setTaxesChargesList(arrtaxchrg);
			taxChargeEntity.setCompanyId(porder.getCompanyId());
			
			genimpl.save(taxChargeEntity);
		}
	}
	
	
	protected void createServicePoBillingDocuments(ServicePo poEntity)
	{	
		PersonInfo personInfoEntity=poEntity.getVinfo();
		
		Logger billingLogger=Logger.getLogger("Billing Logger");
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
		billingLogger.log(Level.SEVERE,"Current Date "+currentDate);

		int count=0;
		for(int i=0;i<poEntity.getPaymentTermsList().size();i++)
		{
			ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			PaymentTerms paymentTerms=new PaymentTerms();
			Date conStartDate=null;
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			
			
			
			/***********************Invoice Date******************************/
			billingLogger.log(Level.SEVERE,"SO Date As per IST"+DateUtility.getDateWithTimeZone("IST",poEntity.getServicePoDate()));
			billingLogger.log(Level.SEVERE,"SO Date As Per Saved"+poEntity.getServicePoDate());
			
			
			Calendar calInvoiceDate = Calendar.getInstance();
			if(poEntity.getServicePoDate()!=null){
				Date salesDate=DateUtility.getDateWithTimeZone("IST", poEntity.getServicePoDate());
				calInvoiceDate.setTime(salesDate);
			}
			else{
				conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
				calInvoiceDate.setTime(conStartDate);
			}
			
			
			calInvoiceDate.add(Calendar.DATE, poEntity.getPaymentTermsList().get(i).getPayTermDays());
			Date calculatedInvoiceDate=null;
			try {
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
				calInvoiceDate.set(Calendar.MINUTE,59);
				calInvoiceDate.set(Calendar.SECOND,59);
				calInvoiceDate.set(Calendar.MILLISECOND,999);
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
				
				
				if(currentDate.after(calculatedInvoiceDate))
				{
					calculatedInvoiceDate=currentDate;
				}
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*********************************Billing Date************************************/
			
			Date calBillingDate=null;
			Calendar calBilling=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
			calBilling.add(Calendar.DATE, -2);
			try {
				calBilling.set(Calendar.HOUR_OF_DAY,23);
				calBilling.set(Calendar.MINUTE,59);
				calBilling.set(Calendar.SECOND,59);
				calBilling.set(Calendar.MILLISECOND,999);
				calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
				
				if(currentDate.after(calBillingDate)){
					calBillingDate=currentDate;
				}
				
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/********************************Payment Date******************************************/
			Date calPaymentDate=null;
			int creditVal=0;
			if(poEntity.getCreditDays()!=0){
				creditVal=poEntity.getCreditDays();
			}
			
			Calendar calPayment=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calPayment.setTime(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
			
			try {
				calPayment.set(Calendar.HOUR_OF_DAY,23);
				calPayment.set(Calendar.MINUTE,59);
				calPayment.set(Calendar.SECOND,59);
				calPayment.set(Calendar.MILLISECOND,999);
				calPayment.add(Calendar.DATE, creditVal);
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			billingDocEntity.setPersonInfo(personInfoEntity);
			billingDocEntity.setContractCount(poEntity.getCount());
		
			if(poEntity.getNetPayable()!=0){
				billingDocEntity.setTotalSalesAmount(poEntity.getNetPayable());
			}
			
			salesProdLis=this.retrieveServiceProducts(poEntity.getPaymentTermsList().get(i).getPayTermPercent(),poEntity.getItems(),poEntity.getCompanyId());
			
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			if(salesOrdArr.size()!=0)
				billingDocEntity.setSalesOrderProducts(salesOrdArr);
			if(poEntity.getCompanyId()!=null)
				billingDocEntity.setCompanyId(poEntity.getCompanyId());
			if(calBillingDate!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST",calBillingDate));
				billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
			}
			if(calculatedInvoiceDate!=null){
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
			}
			if(calPaymentDate!=null){
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
				billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
			}
			
			if(poEntity.getPaymentMethod()!=null)
				billingDocEntity.setPaymentMethod(poEntity.getPaymentMethod());
			if(poEntity.getApproverName()!=null)
				billingDocEntity.setApproverName(poEntity.getApproverName());
			if(poEntity.getEmployee()!=null)
				billingDocEntity.setEmployee(poEntity.getEmployee());
			if(poEntity.getBranch()!=null)
				billingDocEntity.setBranch(poEntity.getBranch());
			if(poEntity.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				//billTaxLis=this.listForBillingTaxes(salesProdLis);
				billTaxLis=this.listForBillingTaxes(poEntity.getPaymentTermsList().get(i).getPayTermPercent(),poEntity.getProductTaxes());
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
			}
			double taxAmt= this.getTotalFromTaxTable(billingDocEntity.getBillingTaxes());
			billingDocEntity.setOrderCreationDate(poEntity.getCreationDate());

			double totBillAmt=this.getTotalBillAmt(salesProdLis);
			billingDocEntity.setTotalBillingAmount(totBillAmt);
			
			//////////////////////////////////////////////
			billingDocEntity.setTotalAmount(totBillAmt);
			billingDocEntity.setFinalTotalAmt(totBillAmt);
			
			double totalamtincludingtax=totBillAmt+taxAmt;
			billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
			
			billingDocEntity.setGrandTotalAmount(totBillAmt);
			
			
			//////////////////////////////////////////////
			
			int payTrmsDays=poEntity.getPaymentTermsList().get(i).getPayTermDays();
			double payTrmsPercent=poEntity.getPaymentTermsList().get(i).getPayTermPercent();
			String payTrmsComment=poEntity.getPaymentTermsList().get(i).getPayTermComment().trim();
			
			
			billingDocEntity.setOrderCformStatus("");
			billingDocEntity.setOrderCformPercent(-1);
			double grossValue=poEntity.getTotalAmount2();
			billingDocEntity.setGrossValue(grossValue);
			
			paymentTerms.setPayTermDays(payTrmsDays);
			paymentTerms.setPayTermPercent(payTrmsPercent);
			paymentTerms.setPayTermComment(payTrmsComment);
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICEPO);
			billingDocEntity.setStatus(BillingDocument.CREATED);
			billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAP);
			
			/**
			 * 
			 */
			
			Calendar calEndDate=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calPayment.setTime(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
			}
			int noOfDays=getMaxNumberOfDays(poEntity.getItems());
			Date endDate=null;
			try {
				calEndDate.set(Calendar.HOUR_OF_DAY,23);
				calEndDate.set(Calendar.MINUTE,59);
				calEndDate.set(Calendar.SECOND,59);
				calEndDate.set(Calendar.MILLISECOND,999);
				calEndDate.add(Calendar.DATE, creditVal);
				endDate=dateFormat.parse(dateFormat.format(calEndDate.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated END DATE"+endDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			
			count++;
			
			if(poEntity.getPaymentTermsList().size()==1){
				billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
				billingDocEntity.setBillingPeroidToDate(endDate);
			}else{
				billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
				if(count!=poEntity.getPaymentTermsList().size()){
					Calendar calInvoiceDate2 = Calendar.getInstance();
					if(poEntity.getServicePoDate()!=null){
						Date serConDate=DateUtility.getDateWithTimeZone("IST", poEntity.getServicePoDate());
						calInvoiceDate2.setTime(serConDate);
					}
					else{
						conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
						calInvoiceDate2.setTime(conStartDate);
					}
					
					calInvoiceDate2.add(Calendar.DATE, poEntity.getPaymentTermsList().get(i+1).getPayTermDays());
					Date calculatedInvoiceDate2=null;
					try {
						calInvoiceDate2.set(Calendar.HOUR_OF_DAY,23);
						calInvoiceDate2.set(Calendar.MINUTE,59);
						calInvoiceDate2.set(Calendar.SECOND,59);
						calInvoiceDate2.set(Calendar.MILLISECOND,999);
						calculatedInvoiceDate2=dateFormat.parse(dateFormat.format(calInvoiceDate2.getTime()));
						billingLogger.log(Level.SEVERE,"Calculated Invoice Date =="+calculatedInvoiceDate2);
						if(currentDate.after(calculatedInvoiceDate2))
						{
							calculatedInvoiceDate2=currentDate;
						}
						
						
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					/***********************************************************************************/
					
					
					Date calBillingDate2=null;
					Calendar calBilling2=Calendar.getInstance();
					if(calculatedInvoiceDate2!=null){
						calBilling2.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate2 ));
					}
					
					billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing=="+calculatedInvoiceDate2);
					
					calBilling2.add(Calendar.DATE, -3);
					try {
						calBilling2.set(Calendar.HOUR_OF_DAY,23);
						calBilling2.set(Calendar.MINUTE,59);
						calBilling2.set(Calendar.SECOND,59);
						calBilling2.set(Calendar.MILLISECOND,999);
						calBillingDate2=dateFormat.parse(dateFormat.format(calBilling2.getTime()));
						
						if(currentDate.after(calBillingDate2)){
							calBillingDate2=currentDate;
						}
						
						billingLogger.log(Level.SEVERE,"Calculated Billing=="+calBillingDate2);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				
					billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST", calBillingDate2));
					
					System.out.println("To billing peroid Date"+DateUtility.getDateWithTimeZone("IST", calBillingDate2));

				}else{
					billingDocEntity.setBillingPeroidToDate(endDate);
				}
				
			}
			
			/**
			 * 
			 */
			
			arrbilling.add(billingDocEntity);
			/******************************************************************************/
			GenricServiceImpl impl=new GenricServiceImpl();
			if(arrbilling.size()!=0){
				impl.save(arrbilling);
			}
		}
	}
	
	private int getMaxNumberOfDays(ArrayList<SalesLineItem> items) {
		// TODO Auto-generated method stub
		int maxDays=0;
		for(SalesLineItem item:items){
			if(maxDays==0){
				maxDays=item.getDuration();
			}
			
			if(maxDays<item.getDuration()){
				maxDays=item.getDuration();
			}
		}
		return maxDays;
	}

	public double getTotalFromTaxTable(List<ContractCharges> list){
		double totalCalcAmt=0;
		for(int i=0;i<list.size();i++){
			totalCalcAmt+=list.get(i).getTaxChargeAssesVal()*list.get(i).getTaxChargePercent()/100;
		}
		return totalCalcAmt;
	}
	
	private List<SalesOrderProductLineItem> retrieveServiceProducts(double payTermPercent,List<SalesLineItem> purchaseProd,long companyId)
	{
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<purchaseProd.size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", purchaseProd.get(i).getPrduct().getCount()).filter("companyId",companyId).first().now();
			
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
//			SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProdId(purchaseProd.get(i).getPrduct().getCount());
			salesOrder.setProdCategory(purchaseProd.get(i).getProductCategory());
			salesOrder.setProdCode(purchaseProd.get(i).getProductCode());
			salesOrder.setProdName(purchaseProd.get(i).getProductName());
			salesOrder.setQuantity(purchaseProd.get(i).getQty());
			salesOrder.setOrderDuration(0);
			salesOrder.setOrderServices(0);
			totalTax=this.removeTaxAmt(purchaseProd.get(i).getPrduct());
			prodPrice=purchaseProd.get(i).getPrice()-totalTax;
			salesOrder.setPrice(prodPrice);
			
			Tax vatTax=purchaseProd.get(i).getVatTax();
			
			Tax servTax=purchaseProd.get(i).getServiceTax();
			
			salesOrder.setVatTax(vatTax);
			salesOrder.setServiceTax(servTax);
			salesOrder.setProdPercDiscount(purchaseProd.get(i).getPercentageDiscount());
			salesOrder.setUnitOfMeasurement(purchaseProd.get(i).getUnitOfMeasurement());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			salesOrder.setHsnCode(purchaseProd.get(i).getPrduct().getHsnNumber());
			salesOrder.setDiscountAmt(purchaseProd.get(i).getDiscountAmt());
			
			/**
			if(purchaseProd.get(i).getPercentageDiscount()!=0){
				percAmt=prodPrice-(prodPrice*purchaseProd.get(i).getPercentageDiscount()/100);
				percAmt=percAmt*purchaseProd.get(i).getQty();
			}
			if(purchaseProd.get(i).getPercentageDiscount()==0){
				percAmt=prodPrice*purchaseProd.get(i).getQty();
			}
			salesOrder.setTotalAmount(percAmt);
			
			if(payTermPercent!=0){
				baseBillingAmount=(percAmt*payTermPercent)/100;
			}
			System.out.println("Base Billing Amount Through Payment Terms"+baseBillingAmount);
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			salesOrder.setIndexVal(i+1);
			**/
			
			if((purchaseProd.get(i).getPercentageDiscount()==null 
					&&purchaseProd.get(i).getPercentageDiscount()==0) 
					&& (purchaseProd.get(i).getDiscountAmt()==0) ){
				if(!purchaseProd.get(i).getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(purchaseProd.get(i).getArea());
					percAmt=prodPrice*squareArea;
				}else{
					percAmt=prodPrice*purchaseProd.get(i).getQty();
				}
				
			}
			
			else if((purchaseProd.get(i).getPercentageDiscount()!=null)
					&& (purchaseProd.get(i).getDiscountAmt()!=0)){
				if(!purchaseProd.get(i).getArea().equalsIgnoreCase("NA")){
					double squareArea = Double.parseDouble(purchaseProd.get(i).getArea());
					percAmt=prodPrice*squareArea;
					percAmt= percAmt-(percAmt*purchaseProd.get(i).getPercentageDiscount()/100);
					percAmt=percAmt-purchaseProd.get(i).getDiscountAmt();
				}else{
					percAmt=prodPrice*purchaseProd.get(i).getQty();
					percAmt=percAmt-(percAmt*purchaseProd.get(i).getPercentageDiscount()/100);
					percAmt=percAmt-purchaseProd.get(i).getDiscountAmt();
				}
			}else{
				if(purchaseProd.get(i).getPercentageDiscount()!=null && purchaseProd.get(i).getPercentageDiscount()!=0){
					if(!purchaseProd.get(i).getArea().equalsIgnoreCase("NA")){
						Double squareArea = Double.parseDouble(purchaseProd.get(i).getArea());
						percAmt = prodPrice*squareArea;
						percAmt=percAmt-(percAmt*purchaseProd.get(i).getPercentageDiscount()/100);
					}else{
						percAmt=prodPrice*purchaseProd.get(i).getQty();
						percAmt=percAmt-(percAmt*purchaseProd.get(i).getPercentageDiscount()/100);
					}
				}else{
					if(!purchaseProd.get(i).getArea().equals("NA")){
						Double squareArea = Double.parseDouble(purchaseProd.get(i).getArea());
						percAmt = prodPrice*squareArea;
						percAmt=percAmt-purchaseProd.get(i).getDiscountAmt();
					}else{
						percAmt=prodPrice*purchaseProd.get(i).getQty();
						percAmt=percAmt-purchaseProd.get(i).getDiscountAmt();
					}
				}
			}
			salesOrder.setTotalAmount(percAmt);
			if(payTermPercent!=0){
				baseBillingAmount=(percAmt*payTermPercent)/100;
			}
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			salesOrder.setBasePaymentAmount(baseBillingAmount);
			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
	}

	/************************************** SERVICE PO BILLING END ******************************************/
	
	
	/************************************** SERVICE PO BILLING END ******************************************/
	
	
	public void createAsset(GRN grn) {
		System.out.println("CREATE ASSET METHOD");
		Logger logger = Logger.getLogger("ASSET CREATION FROM UPLOADED EXCEL");
		BlobstoreService blobstoreService= BlobstoreServiceFactory.getBlobstoreService();
		 
		String urlData="";
		String fileName="";
		if(grn.getAssetExcelUplod()!=null&&grn.getAssetExcelUplod().getUrl()!=null){
			urlData=grn.getAssetExcelUplod().getUrl();
			logger.log(Level.SEVERE,"Doc URL :::: "+urlData);
			
			fileName=grn.getAssetExcelUplod().getName();
			logger.log(Level.SEVERE,"Document Name :::: "+fileName);
			
			String[] res=urlData.split("blob-key=",0);
			logger.log(Level.SEVERE,"Splitted Url :::: "+res);
			
			logger.log(Level.SEVERE,"BLOB ARRAY SIZE  "+res.length);
			logger.log(Level.SEVERE,"ARRAY 0 "+res[0]);
			
			String blob =res[1].trim();
			logger.log(Level.SEVERE,"Splitted Url Assigned to string  :::: "+blob);
			
			BlobKey blobKey=null;
			try{
				blobKey = new BlobKey(blob);
			}
			catch(IllegalArgumentException e){
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"BlobKey  :::: "+blobKey);
			DataMigrationImpl.blobkey=blobKey;
		}
		
		DataMigrationImpl impl=new DataMigrationImpl();
		impl.savedRecordsDeatils(grn.getCompanyId(), "Company Asset", true,LoginPresenter.loggedInUser);		
	}

}
