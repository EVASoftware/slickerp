package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.services.DataMigrationTaskQueueService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.android.hr.LoadAttendanceFromEmployee;
import com.slicktechnologies.server.outboundwebservice.IntegrateSyncServiceImpl;
import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.attendance.AttandanceDetails;
import com.slicktechnologies.shared.common.attendance.AttandanceError;
import com.slicktechnologies.shared.common.attendance.AttandanceInfo;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.EmployeeLeaveBalance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveValidityInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.AvailedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;


public class DataMigrationTaskQueueImpl extends RemoteServiceServlet implements DataMigrationTaskQueueService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5933636488811468105L;


	Logger logger = Logger.getLogger("Name of logger");
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	
	static BlobKey blobkey;


	@Override
	public ArrayList<Integer> saveRecords(long companyId, String entityName) {
		
		  ArrayList<String> exceldatalist = new ArrayList<String>();
		  
			ArrayList<Integer> intlist = new ArrayList<Integer>();


			/******************* Reading Excel Data & adding in list *****************************/
			try
			{
				 logger.log(Level.SEVERE,"Hiiiiiiiiiiiiiiiiiiiii");
				 
				long i;
				
		    
			    Workbook wb = null;
			    try {
					wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				} catch (EncryptedDocumentException e) {
					e.printStackTrace();
				} catch (InvalidFormatException e) {
					e.printStackTrace();
				} 
			    Sheet sheet = wb.getSheetAt(0);
			    
		    
			    
			 // Get iterator to all the rows in current sheet 
			    Iterator<Row> rowIterator = sheet.iterator();
			    
			    
			    // Traversing over each row of XLSX file
			    while (rowIterator.hasNext()) {
			    	Row row = rowIterator.next();
			    // For each row, iterate through each columns 
			    	Iterator<Cell> cellIterator = row.cellIterator(); 
			    	while (cellIterator.hasNext()) { 
			    		Cell cell = cellIterator.next();
			    		switch (cell.getCellType()) {
			    		case Cell.CELL_TYPE_STRING: 
		                    exceldatalist.add(cell.getStringCellValue());
			    			break; 
			    		case Cell.CELL_TYPE_NUMERIC: 
			    			if(DateUtil.isCellDateFormatted(cell)){
			    				exceldatalist.add(fmt.format(cell.getDateCellValue()));
			    			}else{
//			    				i=(long) cell.getNumericCellValue();
//			    				exceldatalist.add(i+"");
//			    				System.out.println("Value:"+i);
			    				
			    				/**
			    				 * @author Anil , Date : 08-03-2019
			    				 * for storing double values
			    				 */
			    				DecimalFormat decimalFormat = new DecimalFormat("#########.##");
								int value1 = 0 , value2 = 0;
								String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
								if(array.length > 0){
								//	value1 = Integer.parseInt(array[0]);
								}
								if(array.length > 1){
									value2 = Integer.parseInt(array[1]);
								}
								if(value2 == 0){
									i = (long) cell.getNumericCellValue();
									exceldatalist.add(i + "");
								}else{
									exceldatalist.add(cell.getNumericCellValue() + "");
								}
			    				/**
			    				 * 
			    				 */
			    			}
			    		   break; 
			    		 case Cell.CELL_TYPE_BOOLEAN: 
			    			 
			    			break;
			    			default : 
			    			}
			    		} 
			    	System.out.println("");

			    }

			    
			    System.out.println("Total size:"+exceldatalist.size());
				 logger.log(Level.SEVERE,"FileString List Size:"+exceldatalist.size());

			    wb.close();
			    
			} catch (FileNotFoundException e) {
			    e.printStackTrace();
			} catch (IOException e) {
			    e.printStackTrace();
			}
			
			/******************************* end of excel data reading and adding in list *******************/
			
			/******************************* Methods for inserting records in Entity     ********************/
			if(entityName.equals("Customer")){
				
//				customersSave(companyId, exceldatalist);
				
				if (exceldatalist.get(0).trim().equalsIgnoreCase("Company")
						&& exceldatalist.get(1).trim()
								.equalsIgnoreCase("Company Name")
						&& exceldatalist
								.get(2)
								.trim()
								.equalsIgnoreCase(
										"D.O.B/Establishment Date(mm/dd/yyyy)")
						&& exceldatalist.get(3).equalsIgnoreCase("salutation")
						&& exceldatalist.get(4).trim()
								.equalsIgnoreCase("Full Name")
						&& exceldatalist.get(5).trim().equalsIgnoreCase("Email")
						&& exceldatalist.get(6).equalsIgnoreCase("Landline No.")
						&& exceldatalist.get(7).equalsIgnoreCase("Cell No.1")
						&& exceldatalist.get(8).equalsIgnoreCase("Cell No.2")
						&& exceldatalist.get(9).equalsIgnoreCase("Fax No.")
						&& exceldatalist.get(10).equalsIgnoreCase("Branch")
						&& exceldatalist.get(11).equalsIgnoreCase("Sales Person")
						&& exceldatalist.get(12).equalsIgnoreCase("Customer Group")
						&& exceldatalist.get(13).equalsIgnoreCase(
								"Customer Category")
						&& exceldatalist.get(14).equalsIgnoreCase("Customer Type")
						&& exceldatalist.get(15).equalsIgnoreCase(
								"Customer Priority")
						&& exceldatalist.get(16).equalsIgnoreCase("customer level")
						&& exceldatalist.get(17).equalsIgnoreCase("Reference No.1")
						&& exceldatalist.get(18).equalsIgnoreCase("Reference No.2")
						&& exceldatalist.get(19).equalsIgnoreCase("website")
						&& exceldatalist.get(20).equalsIgnoreCase("Description")
						&& exceldatalist.get(21).equalsIgnoreCase("google plus id")
						&& exceldatalist.get(22).equalsIgnoreCase("facebook id")
						&& exceldatalist.get(23).equalsIgnoreCase("twitter id")
						&& exceldatalist.get(24).equalsIgnoreCase("address line1")
						&& exceldatalist.get(25).equalsIgnoreCase("address line2")
						&& exceldatalist.get(26).equalsIgnoreCase("landmark")
						&& exceldatalist.get(27).equalsIgnoreCase("country")
						&& exceldatalist.get(28).equalsIgnoreCase("state")
						&& exceldatalist.get(29).equalsIgnoreCase("city")
						&& exceldatalist.get(30).equalsIgnoreCase("locality")
						&& exceldatalist.get(31).equalsIgnoreCase("pin")
						&& exceldatalist.get(32).equalsIgnoreCase("address line1")
						&& exceldatalist.get(33).equalsIgnoreCase("address line2")
						&& exceldatalist.get(34).equalsIgnoreCase("landmark")
						&& exceldatalist.get(35).equalsIgnoreCase("country")
						&& exceldatalist.get(36).equalsIgnoreCase("state")
						&& exceldatalist.get(37).equalsIgnoreCase("city")
						&& exceldatalist.get(38).equalsIgnoreCase("locality")
						&& exceldatalist.get(39).equalsIgnoreCase("pin")
						&& exceldatalist.get(40).equalsIgnoreCase("GSTIN Number")) {
					intlist = customersSave(companyId, exceldatalist,entityName);
				} else {
					intlist.add(-1);
				}

			}
			else if(entityName.equals("Locality")){
				if (exceldatalist.get(0).equalsIgnoreCase("City")
						&& exceldatalist.get(1).equalsIgnoreCase("Locality Name")
						&& exceldatalist.get(2).equalsIgnoreCase("Pin Code")
						&& exceldatalist.get(3).equalsIgnoreCase("Status")) {
					intlist = localitysave(companyId, exceldatalist,entityName);
				}else{
					intlist.add(-1);
				}
			}else if(entityName.equals("TechnicianWarehouseDetails")){
				/** date 25/04/2019 added by komal to upload technician warehouse details of nbhc **/
				if (exceldatalist.get(0).trim().equalsIgnoreCase("Employee ID")
						&& exceldatalist.get(1).trim().equalsIgnoreCase("Employee Name")
						&& exceldatalist.get(2).equalsIgnoreCase("User ID")
						&& exceldatalist.get(3).equalsIgnoreCase("Password")
						&& exceldatalist
								.get(4)
								.trim()
								.equalsIgnoreCase(
										"Monthly Salary")
						&& exceldatalist.get(5).equalsIgnoreCase("Monthly Working Hours")
						&& exceldatalist.get(6).trim()
								.equalsIgnoreCase("Cell Number") 
						&& exceldatalist.get(7).trim()
						.equalsIgnoreCase("Branch")
						&& exceldatalist.get(8).trim().equalsIgnoreCase("Parent Warehouse")
						&& exceldatalist.get(9).equalsIgnoreCase("Parent Storage Location")
						&& exceldatalist.get(10).equalsIgnoreCase("Parent Storage Bin")){
					intlist = technicianWarehouseDetailsSave(companyId, exceldatalist,entityName);
				} else {
					intlist.add(-1);
				}
			}
		
		return intlist;
	}

	

	private ArrayList<Integer> localitysave(long companyId,
			ArrayList<String> exceldatalist, String entityName) {
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		logger.log(Level.SEVERE,"Hi Locality save");
			
			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", entityName).param("companyId", companyId+""));
			
			logger.log(Level.SEVERE,"Hi Task queue calling for locality save");

		return integerlist;
	}

	private ArrayList<Integer> customersSave(long companyId, ArrayList<String> exceldatalist, String entityName) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Hi Customer save Task queue calling ");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", entityName).param("companyId", companyId+""));
		
		logger.log(Level.SEVERE,"Hi Task queue cazlling save");
		integerlist.add(1);
		
		
		return integerlist;
		
	}
	
	@Override
	public ArrayList<AttandanceInfo> saveAttandance(long companyId, String entityName) {
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		ArrayList<AttandanceInfo> attandanceInfoList=new ArrayList<AttandanceInfo>();
		
		ArrayList<String> exceldatalist = new ArrayList<String>();
		ArrayList<Integer> intlist = new ArrayList<Integer>();
		try {
			logger.log(Level.SEVERE, "Hiiiiiiiiiiiiiiiiiiiii");
			long i;
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);

			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();

			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {
							//i = (long) cell.getNumericCellValue();
							//exceldatalist.add(i + "");
							
								//	i = (long) cell.getNumericCellValue();
								//	logger.log(Level.SEVERE, "Value : "+i);
//									logger.log(Level.SEVERE, "Value : "+cell.getNumericCellValue());
								//	exceldatalist.add(cell.getNumericCellValue() + "");
									DecimalFormat decimalFormat = new DecimalFormat("#########.##");
									int value1 = 0 , value2 = 0;
									String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
									if(array.length > 0){
										value1 = Integer.parseInt(array[0]);
									}
									if(array.length > 1){
										value2 = Integer.parseInt(array[1]);
									}
									if(value2 == 0){
										i = (long) cell.getNumericCellValue();
										exceldatalist.add(i + "");
									}else{
										exceldatalist.add(cell.getNumericCellValue() + "");
									}
								
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						break;
					default:
					}
				}
			}

			logger.log(Level.SEVERE,"FileString List Size:" + exceldatalist.size());
			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if(entityName.equals("Employee Attendance")){
			/**
			 * @author Anil , Date : 08-05-2018
			 * adding single Ot and double ot column in attendance upload excel
			 * Process name : Attendance
			 * Process Type : AddSingleAndDoubleOtInAttendanceUploadTemplate
			 * raised by Sonu for Charlene Hospitality
			 */
			boolean otDotFlag=false;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "AddSingleAndDoubleOtInAttendanceUploadTemplate", companyId)){
				otDotFlag=true;
			}
			
			/**
			 * @author Anil
			 * @since 26-08-2020
			 * EnableSiteLocation
			 * for sun facility raised by rahul
			 */
			boolean siteLocation=false;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "EnableSiteLocation", companyId)){
				siteLocation=true;
			}
			
			/** date 27.6.2018 added by komal for employee attendance **/
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Month")) {
				int month = 0 , year = 0;
				if(!exceldatalist.get(1).trim().equalsIgnoreCase("") && !exceldatalist.get(1).trim().equalsIgnoreCase("NA")){
					System.out.println("month :" +exceldatalist.get(1).trim());
//					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					Calendar c = Calendar.getInstance();
					try {
						c.setTime(sdf.parse(exceldatalist.get(1).trim()));
						c.set(Calendar.HOUR, 0);
						c.set(Calendar.MINUTE, 0);
						c.set(Calendar.SECOND, 0);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					month = c.get(Calendar.MONTH);
					year = c.get(Calendar.YEAR);
					if (!siteLocation&&exceldatalist.get(2).trim().equalsIgnoreCase("EMP ID")
							&& exceldatalist.get(3).trim().equalsIgnoreCase("EMPLOYEE NAME")
							&& exceldatalist.get(4).trim().equalsIgnoreCase("DESIGNATION")
							&& exceldatalist.get(5).trim().equalsIgnoreCase("PROJECT NAME")
//							&& exceldatalist.get(6).trim().equalsIgnoreCase("OVERTIME")
							&& exceldatalist.get(6).trim().equalsIgnoreCase("SHIFT")){
						
						if (exceldatalist.get(7).trim().equalsIgnoreCase("1")
								&& exceldatalist.get(8).trim().equalsIgnoreCase("2")
								&& exceldatalist.get(9).trim().equalsIgnoreCase("3")
								&& exceldatalist.get(10).trim().equalsIgnoreCase("4")
								&& exceldatalist.get(11).trim().equalsIgnoreCase("5")
								&& exceldatalist.get(12).trim().equalsIgnoreCase("6")
								&& exceldatalist.get(13).trim().equalsIgnoreCase("7")
								&& exceldatalist.get(14).trim().equalsIgnoreCase("8")
								&& exceldatalist.get(15).trim().equalsIgnoreCase("9")
								&& exceldatalist.get(16).trim().equalsIgnoreCase("10")
								&& exceldatalist.get(17).trim().equalsIgnoreCase("11")
								&& exceldatalist.get(18).trim().equalsIgnoreCase("12")
								&& exceldatalist.get(19).trim().equalsIgnoreCase("13")
								&& exceldatalist.get(20).trim().equalsIgnoreCase("14")
								&& exceldatalist.get(21).trim().equalsIgnoreCase("15")
								&& exceldatalist.get(22).trim().equalsIgnoreCase("16")
								&& exceldatalist.get(23).trim().equalsIgnoreCase("17")
								&& exceldatalist.get(24).trim().equalsIgnoreCase("18")
								&& exceldatalist.get(25).trim().equalsIgnoreCase("19")
								&& exceldatalist.get(26).trim().equalsIgnoreCase("20")
								&& exceldatalist.get(27).trim().equalsIgnoreCase("21")
								&& exceldatalist.get(28).trim().equalsIgnoreCase("22")
								&& exceldatalist.get(29).trim().equalsIgnoreCase("23")
								&& exceldatalist.get(30).trim().equalsIgnoreCase("24")
								&& exceldatalist.get(31).trim().equalsIgnoreCase("25")
								&& exceldatalist.get(32).trim().equalsIgnoreCase("26")
								&& exceldatalist.get(33).trim().equalsIgnoreCase("27")
								&& exceldatalist.get(34).trim().equalsIgnoreCase("28")
								&& exceldatalist.get(35).trim().equalsIgnoreCase("29")
								&& exceldatalist.get(36).trim().equalsIgnoreCase("30")
								&& exceldatalist.get(37).trim().equalsIgnoreCase("31")) {
							
							if(otDotFlag){
								if(!exceldatalist.get(38).trim().equalsIgnoreCase("Single OT")
										|| !exceldatalist.get(39).trim().equalsIgnoreCase("Double OT")){
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}else{
								if(exceldatalist.get(38).trim().equalsIgnoreCase("Single OT")
										|| exceldatalist.get(39).trim().equalsIgnoreCase("Double OT")){
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}
							
							try {
								attandanceInfoList = employeeAttendanceSave(companyId, exceldatalist,entityName, month, year,otDotFlag,0,null,null,siteLocation);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}else{
							/**
							 * @author Anil
							 * @since 27-07-2020
							 * If static days were not matching then we will check those days with 
							 * dynamic generated days
							 */
							Date startDate=c.getTime();
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(new Date(startDate.getTime()));
							calendar.add(Calendar.MONTH, 1);
							calendar.add(Calendar.DATE, -1);
							Date endDate=calendar.getTime();
							logger.log(Level.SEVERE,"startDate "+startDate);
							logger.log(Level.SEVERE,"endDate " + endDate);
							
							int noOfDays=DateUtility.getNumberOfDaysBetweenDates(endDate, startDate)+1;
							logger.log(Level.SEVERE,"noOfDays " + noOfDays);
							int daysArray[]=new int[noOfDays];
							
							Date date =new Date(startDate.getTime());
							for(int i=0;i<noOfDays;i++){
								daysArray[i]=date.getDate();
								date=DateUtility.addDaysToDate(date, 1);
							}
							String daysArrStr="";
							for(int i=0;i<daysArray.length;i++){
								daysArrStr=daysArrStr+daysArray[i]+" ";
							}
							logger.log(Level.SEVERE,"daysArray " + daysArrStr);
							
							int index=7;
							for(int i=0;i<daysArray.length;i++){
								if(exceldatalist.get(index).trim().equalsIgnoreCase(daysArray[i]+"")){
									index++;
								}else{
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}
//							index=index-1;
							logger.log(Level.SEVERE,"index " + index);
							
							if(otDotFlag){
								logger.log(Level.SEVERE,"OT FLAG TRUE "+index);
								try{
									logger.log(Level.SEVERE,"OT "+exceldatalist.get(index).trim());
									logger.log(Level.SEVERE,"DOT "+exceldatalist.get(index+1).trim());
									if(!exceldatalist.get((index)).trim().equalsIgnoreCase("Single OT")
											|| !exceldatalist.get((index+1)).trim().equalsIgnoreCase("Double OT")){
										AttandanceInfo info=new AttandanceInfo();
										info.setErrorCode(-1);
										attandanceInfoList.add(info);
										return attandanceInfoList;
									}
								}catch(Exception e){
									
								}
								index=index+2;
								
							}else{
								logger.log(Level.SEVERE,"OT FLAG FALSE ");
								try{
									if(exceldatalist.get(index).trim().equalsIgnoreCase("Single OT")
											|| exceldatalist.get(index+1).trim().equalsIgnoreCase("Double OT")){
										AttandanceInfo info=new AttandanceInfo();
										info.setErrorCode(-1);
										attandanceInfoList.add(info);
										return attandanceInfoList;
									}
								}catch(Exception e){
									logger.log(Level.SEVERE,"OT/DOT "+e.getMessage());
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}
							
							logger.log(Level.SEVERE,"final index " + index);
							
							try {
								attandanceInfoList = employeeAttendanceSave(companyId, exceldatalist,entityName, month, year,otDotFlag,index,startDate,endDate,siteLocation);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
						}
						
					}else if (siteLocation&&exceldatalist.get(2).trim().equalsIgnoreCase("EMP ID")
							&& exceldatalist.get(3).trim().equalsIgnoreCase("EMPLOYEE NAME")
							&& exceldatalist.get(4).trim().equalsIgnoreCase("DESIGNATION")
							&& exceldatalist.get(5).trim().equalsIgnoreCase("PROJECT NAME")
							&& exceldatalist.get(6).trim().equalsIgnoreCase("SITE LOCATION")
							&& exceldatalist.get(7).trim().equalsIgnoreCase("SHIFT")){
						
						if (exceldatalist.get(8).trim().equalsIgnoreCase("1")
								&& exceldatalist.get(9).trim().equalsIgnoreCase("2")
								&& exceldatalist.get(10).trim().equalsIgnoreCase("3")
								&& exceldatalist.get(11).trim().equalsIgnoreCase("4")
								&& exceldatalist.get(12).trim().equalsIgnoreCase("5")
								&& exceldatalist.get(13).trim().equalsIgnoreCase("6")
								&& exceldatalist.get(14).trim().equalsIgnoreCase("7")
								&& exceldatalist.get(15).trim().equalsIgnoreCase("8")
								&& exceldatalist.get(16).trim().equalsIgnoreCase("9")
								&& exceldatalist.get(17).trim().equalsIgnoreCase("10")
								&& exceldatalist.get(18).trim().equalsIgnoreCase("11")
								&& exceldatalist.get(19).trim().equalsIgnoreCase("12")
								&& exceldatalist.get(20).trim().equalsIgnoreCase("13")
								&& exceldatalist.get(21).trim().equalsIgnoreCase("14")
								&& exceldatalist.get(22).trim().equalsIgnoreCase("15")
								&& exceldatalist.get(23).trim().equalsIgnoreCase("16")
								&& exceldatalist.get(24).trim().equalsIgnoreCase("17")
								&& exceldatalist.get(25).trim().equalsIgnoreCase("18")
								&& exceldatalist.get(26).trim().equalsIgnoreCase("19")
								&& exceldatalist.get(27).trim().equalsIgnoreCase("20")
								&& exceldatalist.get(28).trim().equalsIgnoreCase("21")
								&& exceldatalist.get(29).trim().equalsIgnoreCase("22")
								&& exceldatalist.get(30).trim().equalsIgnoreCase("23")
								&& exceldatalist.get(31).trim().equalsIgnoreCase("24")
								&& exceldatalist.get(32).trim().equalsIgnoreCase("25")
								&& exceldatalist.get(33).trim().equalsIgnoreCase("26")
								&& exceldatalist.get(34).trim().equalsIgnoreCase("27")
								&& exceldatalist.get(35).trim().equalsIgnoreCase("28")
								&& exceldatalist.get(36).trim().equalsIgnoreCase("29")
								&& exceldatalist.get(37).trim().equalsIgnoreCase("30")
								&& exceldatalist.get(38).trim().equalsIgnoreCase("31")) {
							
							if(otDotFlag){
								if(!exceldatalist.get(39).trim().equalsIgnoreCase("Single OT")
										|| !exceldatalist.get(40).trim().equalsIgnoreCase("Double OT")){
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}else{
								if(exceldatalist.get(39).trim().equalsIgnoreCase("Single OT")
										|| exceldatalist.get(40).trim().equalsIgnoreCase("Double OT")){
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}
							
							try {
								attandanceInfoList = employeeAttendanceSave(companyId, exceldatalist,entityName, month, year,otDotFlag,0,null,null,siteLocation);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}else{
							/**
							 * @author Anil
							 * @since 27-07-2020
							 * If static days were not matching then we will check those days with 
							 * dynamic generated days
							 */
							Date startDate=c.getTime();
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(new Date(startDate.getTime()));
							calendar.add(Calendar.MONTH, 1);
							calendar.add(Calendar.DATE, -1);
							Date endDate=calendar.getTime();
							logger.log(Level.SEVERE,"startDate "+startDate);
							logger.log(Level.SEVERE,"endDate " + endDate);
							
							int noOfDays=DateUtility.getNumberOfDaysBetweenDates(endDate, startDate)+1;
							logger.log(Level.SEVERE,"noOfDays " + noOfDays);
							int daysArray[]=new int[noOfDays];
							
							Date date =new Date(startDate.getTime());
							for(int i=0;i<noOfDays;i++){
								daysArray[i]=date.getDate();
								date=DateUtility.addDaysToDate(date, 1);
							}
							String daysArrStr="";
							for(int i=0;i<daysArray.length;i++){
								daysArrStr=daysArrStr+daysArray[i]+" ";
							}
							logger.log(Level.SEVERE,"daysArray " + daysArrStr);
							
							int index=8;
							for(int i=0;i<daysArray.length;i++){
								if(exceldatalist.get(index).trim().equalsIgnoreCase(daysArray[i]+"")){
									index++;
								}else{
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}
//							index=index-1;
							logger.log(Level.SEVERE,"index " + index);
							
							if(otDotFlag){
								logger.log(Level.SEVERE,"OT FLAG TRUE "+index);
								try{
									logger.log(Level.SEVERE,"OT "+exceldatalist.get(index).trim());
									logger.log(Level.SEVERE,"DOT "+exceldatalist.get(index+1).trim());
									if(!exceldatalist.get((index)).trim().equalsIgnoreCase("Single OT")
											|| !exceldatalist.get((index+1)).trim().equalsIgnoreCase("Double OT")){
										AttandanceInfo info=new AttandanceInfo();
										info.setErrorCode(-1);
										attandanceInfoList.add(info);
										return attandanceInfoList;
									}
								}catch(Exception e){
									
								}
								index=index+2;
								
							}else{
								logger.log(Level.SEVERE,"OT FLAG FALSE ");
								try{
									if(exceldatalist.get(index).trim().equalsIgnoreCase("Single OT")
											|| exceldatalist.get(index+1).trim().equalsIgnoreCase("Double OT")){
										AttandanceInfo info=new AttandanceInfo();
										info.setErrorCode(-1);
										attandanceInfoList.add(info);
										return attandanceInfoList;
									}
								}catch(Exception e){
									logger.log(Level.SEVERE,"OT/DOT "+e.getMessage());
									AttandanceInfo info=new AttandanceInfo();
									info.setErrorCode(-1);
									attandanceInfoList.add(info);
									return attandanceInfoList;
								}
							}
							
							logger.log(Level.SEVERE,"final index " + index);
							
							try {
								attandanceInfoList = employeeAttendanceSave(companyId, exceldatalist,entityName, month, year,otDotFlag,index,startDate,endDate,siteLocation);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
						}
						
					}else{
						AttandanceInfo info=new AttandanceInfo();
						info.setErrorCode(-1);
						attandanceInfoList.add(info);
//						intlist.add(-1);//invalid excel
					}
				}else{
					AttandanceInfo info=new AttandanceInfo();
					info.setErrorCode(-2);
					attandanceInfoList.add(info);
//					intlist.add(-2);// date format error
				}
			}else{
				AttandanceInfo info=new AttandanceInfo();
				info.setErrorCode(-1);
				attandanceInfoList.add(info);
//				intlist.add(-1);//invalid excel
				
			}
		}
		
		
		
		return attandanceInfoList;
	}
	/**
	 * @author Anil , Date : 08-05-2019
	 * added boolean parameter otDotFlag which checks whether to add two more column in attendance upload template
	 * @param siteLocation 
	 */
	private ArrayList<AttandanceInfo> employeeAttendanceSave(long companyId,ArrayList<String> exceldatalist,String entityName,int month,int year, boolean otDotFlag,int lastUpdatedIndex,Date startDate, Date endDateOfMonth, boolean siteLocation) throws ParseException{
		/**
		 * @author Anil , Date : 09-05-2019
		 */
		int endLimit =36;
		int startLimit=38;
		if(otDotFlag){
			endLimit=38;
			startLimit=40;
		}
		
		/**
		 * @author Anil
		 * @since 26-08-2020
		 */
		if(siteLocation){
			endLimit=37;
			startLimit=39;
			if(otDotFlag){
				endLimit=39;
				startLimit=41;
			}
		}
		
		logger.log(Level.SEVERE,"OT/DOT Flag : "+otDotFlag+" - "+endLimit+" - "+startLimit+" siteLocation "+siteLocation);
		
		logger.log(Level.SEVERE,"lastUpdatedIndex : "+lastUpdatedIndex);
		if(lastUpdatedIndex!=0){
			if(startLimit>lastUpdatedIndex){
				startLimit=lastUpdatedIndex;
				endLimit=lastUpdatedIndex-2;
			}
			logger.log(Level.SEVERE,"New end and start limit : "+endLimit+" - "+startLimit);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat monthSdf = new SimpleDateFormat("MMM-yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		monthSdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		int mon = month;
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mon);
		c.set(Calendar.YEAR, year);
		c.set(Calendar.DAY_OF_MONTH , 1);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
//		Date startDate = c.getTime();
		if(startDate==null){
			startDate = c.getTime();
		}
//		Date endDateOfMonth=DateUtility.getEndDateofMonth(startDate);
		if(endDateOfMonth==null){
			endDateOfMonth=DateUtility.getEndDateofMonth(startDate);
		}
		int noOfDays=31;
		int otCol=30;
		noOfDays=DateUtility.getNumberOfDaysBetweenDates(endDateOfMonth, startDate)+1;
		if(lastUpdatedIndex!=0){
			otCol=noOfDays-1;
		}
		logger.log(Level.SEVERE,"noOfDays : "+noOfDays+" OT COL : "+otCol);
		
		ArrayList<AttandanceInfo> attandInfoList=new ArrayList<AttandanceInfo>();
		
		logger.log(Level.SEVERE,"END DATE OF THE MONTH : "+endDateOfMonth);
		loop:
		for (int p = startLimit; p < exceldatalist.size(); p += endLimit) {
			logger.log(Level.SEVERE,"LOOP  "+p+" NEXT "+(p+endLimit));
			AttandanceInfo attendInfo=new AttandanceInfo();
			ArrayList<AttandanceDetails> attList=new ArrayList<AttandanceDetails>();
			
			logger.log(Level.SEVERE,"date :  " + c.getTime() + "  Strt date :"+ startDate);
			c.setTime(startDate);

			attendInfo.setCompanyId(companyId);
			attendInfo.setMonth(month+"");
			attendInfo.setYear(year+"");
			attendInfo.setMonthYear(monthSdf.format(startDate));
			
			if(lastUpdatedIndex!=0){
				attendInfo.setMonthYear(monthSdf.format(endDateOfMonth));
			}
			attendInfo.setStartDate(startDate);
			attendInfo.setEndDate(endDateOfMonth);
			attendInfo.setStartLimit(startLimit);
			attendInfo.setEndLimit(endLimit);
			attendInfo.setNoOfDays(noOfDays);
			attendInfo.setLastIndexUpdated(lastUpdatedIndex);
			
			if (exceldatalist.get(p).trim().equalsIgnoreCase("na")) {
				attendInfo.setEmpId(-1);				
				
			} else {
				attendInfo.setEmpId(Integer.parseInt(exceldatalist.get(p).trim()));
			}
			
			if (exceldatalist.get(p+1).trim().equalsIgnoreCase("na")) {
				attendInfo.setEmpName(exceldatalist.get(p+1).trim());
			} else {
				attendInfo.setEmpName(exceldatalist.get(p+1).trim());
			}
			
			if (exceldatalist.get(p+2).trim().equalsIgnoreCase("na")) {
				attendInfo.setEmpDesignation(exceldatalist.get(p+2).trim());				
				
			} else {
				attendInfo.setEmpDesignation(exceldatalist.get(p+2).trim());
			}
			
			if (exceldatalist.get(p+3).trim().equalsIgnoreCase("na")) {
				attendInfo.setProjName(exceldatalist.get(p+3).trim());
			} else {
				attendInfo.setProjName(exceldatalist.get(p+3).trim());
			}
			
			int index=5;
			if(siteLocation){
				index=6;
				if (exceldatalist.get(p+4).trim().equalsIgnoreCase("na")) {
					attendInfo.setSiteLocation(exceldatalist.get(p+4).trim());
				} else {
					attendInfo.setSiteLocation(exceldatalist.get(p+4).trim());
				}
				logger.log(Level.SEVERE,"SITE LOCATION "+attendInfo.getSiteLocation()+" / "+exceldatalist.get(p+4).trim());
				
				if (exceldatalist.get(p+5).trim().equalsIgnoreCase("na")) {
					attendInfo.setShiftName(exceldatalist.get(p+5).trim());
				} else {
					attendInfo.setShiftName(exceldatalist.get(p+5).trim());
				}
			}else{
				if (exceldatalist.get(p+4).trim().equalsIgnoreCase("na")) {
					attendInfo.setShiftName(exceldatalist.get(p+4).trim());
				} else {
					attendInfo.setShiftName(exceldatalist.get(p+4).trim());
				}
			}
			
			
			for(int i=0;i<noOfDays;i++){
				AttandanceDetails attnDet=new AttandanceDetails();
				if(exceldatalist.get(p+index).trim().equalsIgnoreCase("end")){
					logger.log(Level.SEVERE,"COL NO. "+p+index);
					attendInfo.setAttandList(attList);
					attandInfoList.add(attendInfo);
					logger.log(Level.SEVERE,"VALUE OF P = "+p+" OT COL VAL = "+otCol+" LOOP No = "+i);
					p=p-(otCol-i);
					logger.log(Level.SEVERE,"VALUE OF P ::  "+p);
					int index1 = p + 5 + 31;
					if(lastUpdatedIndex!=0){
						index1 = p + 5 + noOfDays;
					}
					logger.log(Level.SEVERE,"VALUE OF index1 ::  "+index1);
					if(otDotFlag){
						updateOtAndDotDetails(exceldatalist, attendInfo, index1);
					}
					continue loop;
				}
				
				if(i!=0){
					c.add(Calendar.DATE, 1);
				}
				if (exceldatalist.get(p+index).trim().equalsIgnoreCase("na")) {
					attnDet.setDate(sdf.parse(sdf.format(c.getTime())));
					attnDet.setValue(exceldatalist.get(p+index).trim());
					attList.add(attnDet);
				} else {
//					AttandanceDetails attnDet=new AttandanceDetails();
					attnDet.setDate(sdf.parse(sdf.format(c.getTime())));
					attnDet.setValue(exceldatalist.get(p+index).trim());
					attList.add(attnDet);
				}
				index++;
				logger.log(Level.SEVERE,"date :  "+attnDet.getDate()+" Value "+attnDet.getValue() );
			}
			
			/**
			 * @author Anil , Date : 09-05-2019
			 * capturing ot and dot details
			 */
			if(otDotFlag){
				updateOtAndDotDetails(exceldatalist,attendInfo,p+index);
			}
			
			attendInfo.setAttandList(attList);
			attandInfoList.add(attendInfo);
		}
			
			
			
		return attandInfoList;
	}

	private void updateOtAndDotDetails(ArrayList<String> exceldatalist,AttandanceInfo attandInfo, int index) {
		// TODO Auto-generated method stub
		attandInfo.setSingleOt(exceldatalist.get(index).trim());
		attandInfo.setDoubleOt(exceldatalist.get(index+1).trim());
		attandInfo.setOtDotFlag(true);
		
		logger.log(Level.SEVERE,"Single OT "+attandInfo.getSingleOt()+" Double OT  "+attandInfo.getDoubleOt());
	}

	public String removeLastChar(String str) {
	    return str.substring(0, str.length() - 1);
	}

	/**
	 * Employee id validation
	 * Calendar Assign validation
	 * valid data validation
	 * Weekly off and holiday validation
	 * Leave and overtime validation
	 * 
	 * Put hours for present(9)
	 * L-Leave
	 * A-Absent
	 */
	@Override
	public ArrayList<AttandanceInfo> validateAttandance(ArrayList<AttandanceInfo> attandanceList) {
		
		/**Added by sheetal:10-01-2022
		   Des: If Number generation for attendance is not register then show 
		   this as error while uploading attendance**/    
		
		ArrayList<AttandanceError>errorList=new ArrayList<AttandanceError>();
		NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId", attandanceList.get(0).getCompanyId())
				.filter("processName", "Attendance").filter("status", true)
				.first().now();
		if(ng==null){
			    AttandanceError error=new AttandanceError();
				error.setEmpName("");
				error.setProjectName("");
				error.setRemark("Number generation for attendance is not configured!");
				attandanceList.get(0).setErrorCode(-1);
				errorList.add(error);
				CsvWriter.attendanceErrorList=errorList;
				return attandanceList;
				}
		/**end**/
	   
		/**
		 * Date : 15-12-2018 BY ANIL
		 * for clearing all loaded data from appengine cache memory
		 */
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		/**
		 * End
		 */
		/**
		 * @author Anil , Date :04-05-2019
		 * Validating employee attendance overlapping
		 * if user is marking attendance for same day but for different project
		 */
		ArrayList<String> empIdListAsPerdate=new ArrayList<String>();
		/**
		 * @author Anil,Date :24-01-2019
		 * requirement is instead of putting working hours ,wanted to put label as 'P','HD'
		 * For Orion By Sonu
		 */
		boolean labelAsInputFlag=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "AttendanceLabelAsInput", attandanceList.get(0).getCompanyId())){
			labelAsInputFlag=true;
		}
		logger.log(Level.SEVERE,"AttendanceLabelAsInput :: "+labelAsInputFlag+" OT/DOT FLAG "+attandanceList.get(0).isOtDotFlag());
		
		
		/**
		 * @author Anil, Date : 01-03-2019
		 * if this flag is true then for those employee we can not upload attendance through upload programme
		 */
		boolean isFingerRegistered=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "RestrictUploadProcessForRegisteredEmployee", attandanceList.get(0).getCompanyId())){
			isFingerRegistered=true;
		}
		logger.log(Level.SEVERE,"isFingerRegistered :: "+isFingerRegistered);
		
		/**
		 * @author Anil, Date : 10-05-2019
		 * Allow to upload multiple site attendance of same employee
		 * Process Name : Attendance
		 * Process Type : AllowToUploadMultipleSiteAttendance
		 */
		boolean isMultipleSiteAttendance=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "AllowToUploadMultipleSiteAttendance", attandanceList.get(0).getCompanyId())){
			isMultipleSiteAttendance=true;
		}
		logger.log(Level.SEVERE,"isMultipleSiteAttendance :: "+isMultipleSiteAttendance);
		
		
		/**
		 * @author Anil
		 * @since 26-08-2020
		 */
		boolean siteLocation=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "EnableSiteLocation", attandanceList.get(0).getCompanyId())){
			siteLocation=true;
		}
		logger.log(Level.SEVERE,"siteLocation :: "+siteLocation);
		
		SimpleDateFormat fmt= new SimpleDateFormat("dd/MM/yyyy");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		attandanceList.get(0).setErrorCode(0);
		
//		logger.log(Level.SEVERE,"READING EMP IDs.....");
		
		HashSet<Integer> hsEmpId=new HashSet<Integer>();
		HashSet<String> hsProj=new HashSet<String>();
		
		
		for(AttandanceInfo info:attandanceList){
			hsEmpId.add(info.getEmpId());
			hsProj.add(info.getProjName().trim());
		}
		ArrayList<Integer> empList=new ArrayList<Integer>(hsEmpId);
		ArrayList<String> projList=new ArrayList<String>(hsProj);
		logger.log(Level.SEVERE,"TOTAL NO. OF EMP : "+empList.size());
		logger.log(Level.SEVERE,"NO. OF Proj : "+projList.size());
		
//		/**
//		 * Date : 18-08-2018 BY ANIL
//		 * Retrieving existing attendance , leave and ot info for overriding it.
//		 */
//		List<Attendance> existingAttendanceList=getExistingAttendance(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList);
//		
		/**
		 * @author Anil
		 * @since 26-11-2020
		 * Removing attendance overriding concept to avoid attendance upload failure
		 * raised by Rahul Tiwari for Sunrise
		 */
//		List<EmployeeLeave> approvedLeaveList=getApprovedLeaveList(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList,attandanceList.get(0).getStartDate(),attandanceList.get(0).getEndDate());
//		List<EmployeeOvertime> approvedOtList=getApprovedOtList(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList,attandanceList.get(0).getStartDate(),attandanceList.get(0).getEndDate());
//		
//		List<Attendance> attenListToBeRemoved=new ArrayList<Attendance>();
//		List<EmployeeLeave> leaveListTobeRemoved=new ArrayList<EmployeeLeave>();
//		List<EmployeeOvertime> otListToBeRemoved=new ArrayList<EmployeeOvertime>();
		
		
		
		List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
		if(empList.size()!=0){
			empInfoList = ofy().load().type(EmployeeInfo.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("empCount IN", empList).list();
		}
		logger.log(Level.SEVERE,"EMPLOYEE INFO LIST SIZE : "+empInfoList.size());
		
		/**
		 * 
		 */
		List<Employee> employeeList = new ArrayList<Employee>();
		if(isFingerRegistered){
			if(empList.size()!=0){
				employeeList = ofy().load().type(Employee.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("count IN", empList).list();
			}
		}
		logger.log(Level.SEVERE,"EMPLOYEE LIST SIZE : "+employeeList.size());
		
		
		List<LeaveBalance>empLeaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("empInfo.empCount IN", empList).list();
		logger.log(Level.SEVERE,"EMPLOYEE LEAVE LIST SIZE : "+empLeaveBalList.size());
		
		List<HrProject>projectList=ofy().load().type(HrProject.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("projectName IN", projList).filter("status", true).list();
		
		logger.log(Level.SEVERE,"PROJECT SIZE :: "+projectList.size());
		
		ArrayList<CustomerBranchDetails> siteLocationList=new ArrayList<CustomerBranchDetails>();
		if(siteLocation){
			HashSet<String> stateHs=new HashSet<String>();
			HashSet<Integer> custIdHs=new HashSet<Integer>();
			for(HrProject project:projectList){
				if(project.getPersoninfo()!=null&&project.getPersoninfo().getCount()!=0){
					custIdHs.add(project.getPersoninfo().getCount());
				}
				if(project.getState()!=null&&!project.getState().equals("")){
					stateHs.add(project.getState());
				}
			}
			if(custIdHs.size()>0){
				List<CustomerBranchDetails> customerBranchList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("cinfo.count IN", custIdHs).list();
				if(customerBranchList!=null){
					logger.log(Level.SEVERE,"customerBranchList SIZE :: "+customerBranchList.size());
					for(String state:stateHs){
						for(CustomerBranchDetails obj:customerBranchList){
							if(obj.getAddress()!=null&&obj.getAddress().getState().equals(state)){
								siteLocationList.add(obj);
							}
						}
					}
				}
			}
		}
		logger.log(Level.SEVERE,"siteLocationList SIZE :: "+siteLocationList.size());
		
		/**
		 * @author Anil
		 * @since  27-07-2020
		 */
		String payrollCycleError=null;
		Integer payrollStartDay=null;
		if(projectList!=null){
			HashMap<Integer, ArrayList<String>> payrollCycleMap=new HashMap<Integer, ArrayList<String>>();
			for(HrProject project:projectList){
				payrollStartDay=project.getPayrollStartDay();
				if(project.getPayrollStartDay()==null||project.getPayrollStartDay()==0){
					payrollStartDay=1;
				}
				boolean updateFlag=true;
				if(payrollCycleMap!=null&&payrollCycleMap.size()!=0){
					if(payrollCycleMap.containsKey(payrollStartDay)){
						updateFlag=false;
						payrollCycleMap.get(payrollStartDay).add(project.getProjectName());
					}
				}
				if(updateFlag){
					ArrayList<String> projectNameHs=new ArrayList<String>();
					projectNameHs.add(project.getProjectName());
					payrollCycleMap.put(payrollStartDay, projectNameHs);
				}
			}
			logger.log(Level.SEVERE,"payrollCycleMap " + payrollCycleMap.size() + " payrollStartDay" + payrollStartDay);
			if(payrollCycleMap.size()>1){
				payrollCycleError="";
				for (Map.Entry<Integer, ArrayList<String>> entry : payrollCycleMap.entrySet()){  
					logger.log(Level.SEVERE,"Key = " + entry.getKey() + ", Value = " + entry.getValue()); 
					
					for(String obj:entry.getValue()){
						payrollCycleError=payrollCycleError+"For project "+obj+" payroll start day is "+entry.getKey()+".  ";
					}
//					payrollCycleError=removeLastChar(payrollCycleError);
//					payrollCycleError=payrollCycleError+" payroll start day is "+entry.getKey()+"\n";
				}
			}
			
			if(payrollCycleError!=null){
				AttandanceError error=new AttandanceError();
//				error.setEmpId("");
				error.setEmpName("");
				error.setProjectName("");
				error.setRemark(payrollCycleError);
				attandanceList.get(0).setErrorCode(-1);
				errorList.add(error);
				CsvWriter.attendanceErrorList=errorList;
				return attandanceList;
			}
		}
		
		/**
		 * Date : 15-12-2018 BY ANIL
		 * for clearing all loaded data from appengine cache memory
		 */
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		/**
		 * End
		 */
		
		/**
		 * @author Anil,Date : 23-01-2019 
		 * earlier we are comparing calendar expire validation with current date, 
		 * it should the attendance date only
		 */
		Date date=attandanceList.get(0).getAttandList().get(0).getDate();
		
		int day=date.getDate();
		logger.log(Level.SEVERE,"attendance first day " + day);
		if(payrollStartDay!=day){
			AttandanceError error=new AttandanceError();
//			error.setEmpId("");
			error.setEmpName("");
			error.setProjectName("");
			error.setRemark("Entered Payroll cycle and attendance start day doesn't match.");
			attandanceList.get(0).setErrorCode(-1);
			errorList.add(error);
			CsvWriter.attendanceErrorList=errorList;
			return attandanceList;
		}
		
		
		int count=0;
		for(AttandanceInfo info:attandanceList){
			/**
			 * Date : 23-07-2018 BY ANIL
			 */
			boolean calendarFlag=false;

			double totalNoOfDays = 0;
			double totalNoOfPresentDays = 0;
			double totalNoWeeklyOff=0;
			double totalNoOfHoliday=0;
			double totalNoOfAbsentDays=0;
			double totalExtraHours=0;
			/**
			 * date :16-12-2018 By ANIL
			 * Changed the variable name form totalLeaveHours to totalLeave
			 */
			double totalLeave=0;
			
//			double woAsLeave=0;
			double halfDayLeave=0;
			
			double woOt=0;
			double hOt=0;
			double nOt=0;
			double nhOt=0;
			double phOt=0;
			
			ArrayList<String> existingAttendanceListDtWise=new ArrayList<String>();
			AttandanceError error=new AttandanceError();
			EmployeeInfo empInfo=null;
			HashMap<String , Double> leaveTotalMap = new HashMap<String , Double>();
			if(info.getEmpId()==-1){
				error.setEmpId(info.getEmpId());
				error.setEmpName(info.getEmpName());
				error.setProjectName(info.getProjName());
				error.setRemark("Employee id is invalid.");
//				errorList.add(error);
				attandanceList.get(0).setErrorCode(-1);
			}else {
				empInfo=validateEmpId(info.getEmpId(), empInfoList);
				if(empInfo!=null){
					if(empInfo.getLeaveCalendar()!=null&&!empInfo.isAllocatable(date)){
//					if(empInfo.getLeaveCalendar()!=null){
						if(!info.getEmpName().equals(empInfo.getFullName())){
							error.setEmpId(info.getEmpId());
							error.setEmpName(info.getEmpName());
							error.setProjectName(info.getProjName());
							error.setRemark("Employee name is not matched in DB.");
//							errorList.add(error);
							attandanceList.get(0).setErrorCode(-1);
						}
						
					}else{
						calendarFlag=true;
						error.setEmpId(info.getEmpId());
						error.setEmpName(info.getEmpName());
						error.setProjectName(info.getProjName());
						error.setRemark("Calendar is not assigned to employee or it is expired.");
//						errorList.add(error);
						attandanceList.get(0).setErrorCode(-1);
//						break;
					}
					
					logger.log(Level.SEVERE,"EMP ID :: "+empInfo.getEmpCount()+" EMP NAME : "+empInfo.getFullName()+" PROJ "+info.getProjName()+" SITE "+info.getSiteLocation());
					
//					/**
//					 * @author Anil
//					 * @since 26-11-2020
//					 * If attendance is already uploaded then return
//					 */
//					boolean overrideFlag=false;
//					for(Attendance attendance:existingAttendanceList){
//						if(info.getEmpId()==attendance.getEmpId()){
//							error.setEmpId(info.getEmpId());
//							error.setEmpName(info.getEmpName());
//							error.setProjectName(info.getProjName());
//							error.setRemark("Attendance has been already uploaded.");
//							errorList.add(error);
//							attandanceList.get(0).setErrorCode(-1);
//							overrideFlag=true;
//							break;
//						}
//					}
//					
//					if(overrideFlag){
//						continue;
//					}
					
					
//					/**
//					 * @author Anil
//					 * @since 29-01-2021
//					 * Commenting above logic of validating existing attendance
//					 * As per the new logic of user is trying to upload attendance of same date which is uploaded then only we restrict to upload
//					 * earlier if any day attendance was found then he to delete and re upload attendance
//					 * Raised by Rahul For Sunrise
//					 */
//					existingAttendanceListDtWise=new ArrayList<String>();
//					try {
//						for(Attendance attendance:existingAttendanceList){
//							if(info.getEmpId()==attendance.getEmpId()){
//								existingAttendanceListDtWise.add(info.getEmpId()+""+fmt.format(attendance.getAttendanceDate()));
//							}
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
					
					
					/**
					 * @author Anil
					 * @since 28-08-2020
					 */
					HrProject project=getProjectName(info.getProjName(), projectList);
					if(project==null){
						error.setEmpId(info.getEmpId());
						error.setEmpName(info.getEmpName());
						error.setProjectName(info.getProjName());
						error.setRemark("No project found. "+empInfo.getProjectName());
						errorList.add(error);
						attandanceList.get(0).setErrorCode(-1);
						continue;
					}
					/**
					 * Date : 23-11-2018 By ANIL
					 * Validating Project Name
					 */
					if(!empInfo.getProjectName().trim().equals(info.getProjName().trim())){
						/**
						 * @author Anil , Date : 10-05-2019
						 */
						if(isMultipleSiteAttendance){
//							if(!siteLocation&&project!=null&&project.getOtList()!=null&&project.getOtList().size()!=0){
//								boolean validProject=false;
//								for(Overtime ot:project.getOtList()){
//									if(ot.getEmpId()==info.getEmpId()){
//										validProject=true;
//										break;
//									}
//								}
//								if(!validProject){
//									error.setEmpId(info.getEmpId());
//									error.setEmpName(info.getEmpName());
//									error.setProjectName(info.getProjName());
//									error.setRemark("Project name not matched. "+empInfo.getProjectName());
//			//						errorList.add(error);
//									attandanceList.get(0).setErrorCode(-1);
//								}else{
//									empInfo.setProjectName(info.getProjName());
//								}
//							}else 
							
							/**
							 * @author Anil @since 13-04-2021
							 * Commenting below condition of pre validating reliever OT
							 * Raised by Rahul TIwari for Sasha
							 */
							
//							if(empInfo.isReliever()&&project!=null&&project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//								boolean validProject=false;
//								for(Overtime ot:project.getRelieversOtList()){
//									if(ot.getEmpDesignation().equals(info.getEmpDesignation())){
//										validProject=true;
//										break;
//									}
//								}
//								if(!validProject){
//									error.setEmpId(info.getEmpId());
//									error.setEmpName(info.getEmpName());
//									error.setProjectName(info.getProjName());
//									error.setRemark("Reliever Ot details not entered for designation- "+info.getEmpDesignation()+" in project "+info.getProjName());
//									attandanceList.get(0).setErrorCode(-1);
//								}else{
//									empInfo.setProjectName(info.getProjName());
//								}
//							}else if(empInfo.isReliever()&&project!=null&&(project.getRelieversOtList()==null||project.getRelieversOtList().size()==0)){
//									error.setEmpId(info.getEmpId());
//									error.setEmpName(info.getEmpName());
//									error.setProjectName(info.getProjName());
//									error.setRemark("Reliever Ot details not found in project. "+info.getProjName());
//									attandanceList.get(0).setErrorCode(-1);
//							
//							}
//							else
							
							logger.log(Level.SEVERE, "Overtime data loading");
							/**
							 * @author Vijay Date :- 08-09-2022
							 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
							 */
							ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
							CommonServiceImpl commonservice = new CommonServiceImpl();
							overtimelist = commonservice.getHRProjectOvertimelist(project);
							/**
							 * ends here
							 */
							logger.log(Level.SEVERE, "Overtime data loading done here");

							
//							if(project!=null&&project.getOtList()!=null&&project.getOtList().size()!=0){
							if(project!=null&&overtimelist!=null&&overtimelist.size()!=0){
								boolean validProject=false;
								for(Overtime ot:overtimelist){
									if(ot.getEmpId()==info.getEmpId()){
										validProject=true;
										break;
									}
								}
								if(!validProject){
									/**
									 * @author Anil @since 13-04-2021
									 * This validation should not be checked for reliever employee
									 */
									if(!empInfo.isReliever()){
										error.setEmpId(info.getEmpId());
										error.setEmpName(info.getEmpName());
										error.setProjectName(info.getProjName());
										error.setRemark("Project name not matched. "+empInfo.getProjectName());
				//						errorList.add(error);
										attandanceList.get(0).setErrorCode(-1);
									}
								}else{
									empInfo.setProjectName(info.getProjName());
								}
							}else{
								/**
								 * @author Anil @since 13-04-2021
								 * This validation should not be checked for reliever employee
								 */
								if(!empInfo.isReliever()){
									error.setEmpId(info.getEmpId());
									error.setEmpName(info.getEmpName());
									error.setProjectName(info.getProjName());
									error.setRemark("Project name not matched. "+empInfo.getProjectName());
									attandanceList.get(0).setErrorCode(-1);
								}
							}
						}else{
							/**
							 * @author Anil @since 13-04-2021
							 * This validation should not be checked for reliever employee
							 */
							if(!empInfo.isReliever()){
								error.setEmpId(info.getEmpId());
								error.setEmpName(info.getEmpName());
								error.setProjectName(info.getProjName());
								error.setRemark("Project name not matched. "+empInfo.getProjectName());
								attandanceList.get(0).setErrorCode(-1);
							}
						}
					}
					
					/**
					 * @author Anil
					 * @since 26-08-2020
					 * Validating site location
					 */
					if(siteLocation){
						if(info.getSiteLocation()!=null&&!info.getSiteLocation().equals("")&&!info.getSiteLocation().equalsIgnoreCase("NA")){
							
							if(project.getState()==null||project.getState().equals("")){
								error.setEmpId(info.getEmpId());
								error.setEmpName(info.getEmpName());
								error.setProjectName(info.getProjName());
								error.setRemark("No state details found in project. ");
								attandanceList.get(0).setErrorCode(-1);
								errorList.add(error);
								continue;
							}
							if(siteLocationList.size()==0){
								error.setEmpId(info.getEmpId());
								error.setEmpName(info.getEmpName());
								error.setProjectName(info.getProjName());
								error.setRemark("Site location not found. ");
								attandanceList.get(0).setErrorCode(-1);
								errorList.add(error);
								continue;
							}
							int custId=0;
							if(project.getPersoninfo()!=null){
								custId=project.getPersoninfo().getCount();
							}
							
							if(!validateSiteLocation(siteLocationList,info.getSiteLocation(),project.getState(),custId)){
								error.setEmpId(info.getEmpId());
								error.setEmpName(info.getEmpName());
								error.setProjectName(info.getProjName());
								error.setRemark("Site location not found. "+info.getSiteLocation()+" / "+project.getState());
								attandanceList.get(0).setErrorCode(-1);
							}
//							if(project!=null&&project.getOtList()!=null&&project.getOtList().size()!=0){
//								boolean validProject=false;
//								for(Overtime ot:project.getOtList()){
//									if(ot.getEmpId()==info.getEmpId()&&ot.getSiteLocation()!=null&&ot.getSiteLocation().equals(info.getSiteLocation())){
//										validProject=true;
//										break;
//									}
//								}
//								if(!validProject){
//									error.setEmpId(info.getEmpId());
//									error.setEmpName(info.getEmpName());
//									error.setProjectName(info.getProjName());
//									error.setRemark("Site location not found. "+info.getSiteLocation());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}else{
//								error.setEmpId(info.getEmpId());
//								error.setEmpName(info.getEmpName());
//								error.setProjectName(info.getProjName());
//								error.setRemark("Site location not found. "+empInfo.getProjectName()+"/"+info.getSiteLocation());
//								attandanceList.get(0).setErrorCode(-1);
//							}
							
					 }else{
						error.setEmpId(info.getEmpId());
						error.setEmpName(info.getEmpName());
						error.setProjectName(info.getProjName());
						error.setRemark("Please add site location. "+empInfo.getProjectName());
						attandanceList.get(0).setErrorCode(-1);
					 }
				}
					
				}else{
					calendarFlag=true;
					error.setEmpId(info.getEmpId());
					error.setEmpName(info.getEmpName());
					error.setProjectName(info.getProjName());
					error.setRemark("Employee id is not matched in DB or Inactive.");
//					errorList.add(error);
					attandanceList.get(0).setErrorCode(-1);
				}
				
				/**
				 * @author Anil ,Date : 01-03-2019
				 */
				if(isFingerRegistered&&isEmployeeFingerRegistered(info.getEmpId(), employeeList)){
					error.setEmpId(info.getEmpId());
					error.setEmpName(info.getEmpName());
					error.setProjectName(info.getProjName());
					error.setRemark(error.getRemark()+" Can not upload attendance as employee is registered to mark attendance through app.");
//					errorList.add(error);
					attandanceList.get(0).setErrorCode(-1);
				}
			}
			
		
			error.setEmpId(info.getEmpId());
			error.setEmpName(info.getEmpName());
			error.setProjectName(info.getProjName());
			
			error.setTotalNoOfDays(totalNoOfDays);
			error.setTotalNoOfPresentDays(totalNoOfPresentDays);
			error.setTotalNoOfWO(totalNoWeeklyOff);
			error.setTotalNoOfHoliday(totalNoOfHoliday);
			error.setTotalNoOfAbsentDays(totalNoOfAbsentDays);
			
			if(empInfo!=null&&!calendarFlag){
			double leaveDays=totalLeave;
			double extraDays=totalExtraHours/empInfo.getLeaveCalendar().getWorkingHours();
			error.setTotalNoOfLeave(leaveDays);
			error.setTotalNoOfExtraDays(extraDays);
			}
			errorList.add(error);
			
			logger.log(Level.SEVERE, "loop ending"+ ++count);

		}
		
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		
		CsvWriter.attendanceErrorList=errorList;
		logger.log(Level.SEVERE, "first RPC end here");
		return attandanceList;
		
//			Comparator<AttandanceDetails> attandanceComparator = new  Comparator<AttandanceDetails>() {
//				@Override
//				public int compare(AttandanceDetails o1, AttandanceDetails o2) {
//					return o1.getDate().compareTo(o2.getDate());
//				}
//			};
//			Collections.sort(info.getAttandList(),attandanceComparator);
//			
//			LeaveBalance leaveBalance=null;
//			if(empLeaveBalList!=null&&empLeaveBalList.size()!=0){
//				leaveBalance=getEmpLeaveBalance(info.getEmpId(),empLeaveBalList);
//			}
//			
//			String msg="";
//			for(AttandanceDetails attDet:info.getAttandList()){
//				/**
//				 * @author Anil , Date : 07-05-2019
//				 * If attendance is marked as OP then we will not consider that attendance for update
//				 * raised by Nitin Sir,Sonu
//				 */
//				if(attDet.getValue().equalsIgnoreCase("OP")){
//					logger.log(Level.SEVERE,"AttendanceLabelAsInput  :: "+attDet.getValue());
//					continue;
//				}
//				
//				
//				/**
//				 * @author Anil
//				 */
//				
//				if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("P")){
//					logger.log(Level.SEVERE,"AttendanceLabelAsInput P :: "+empInfo.getLeaveCalendar().getWorkingHours());
//					attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()+"");
//				}else if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("HD")){
//					logger.log(Level.SEVERE,"AttendanceLabelAsInput HD :: "+empInfo.getLeaveCalendar().getWorkingHours()/2);
//					attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()/2+"");
//				}
//				
//				/**
//				 * 
//				 */
//				if(!calendarFlag){
//				totalNoOfDays++;
//				
//				if(empInfo!=null){
//					if(empInfo.getDateOfJoining()!=null){
//						empInfo.getDateOfJoining().setHours(0);
//						empInfo.getDateOfJoining().setMinutes(0);
//						empInfo.getDateOfJoining().setSeconds(0);
//					}
//					if(empInfo.getLastWorkingDate()!=null){
//						empInfo.getLastWorkingDate().setHours(0);
//						empInfo.getLastWorkingDate().setMinutes(0);
//						empInfo.getLastWorkingDate().setSeconds(0);
//					}
//				}
//				
//				if(attDet.getDate().before(empInfo.getDateOfJoining())
//						&&!attDet.getValue().trim().equalsIgnoreCase("NA")){
//					System.out.println("ATT DT : "+attDet.getDate()+" JOINING DT : "+empInfo.getDateOfJoining()+" VALUE : "+attDet.getValue());
//					msg=msg+" / Attendance should not be before date of joining "+fmt.format(empInfo.getDateOfJoining());
//					attandanceList.get(0).setErrorCode(-1);
//					break;
//				}
//				
//				if(empInfo.getLastWorkingDate()!=null&&attDet.getDate().after(empInfo.getLastWorkingDate())
//						&&!attDet.getValue().equalsIgnoreCase("NA")){
//					System.out.println("ATT DT : "+attDet.getDate()+" LAST WRKING DT : "+empInfo.getLastWorkingDate()+" VALUE : "+attDet.getValue());
//					msg=msg+" / Attendance should not be after last date of working "+fmt.format(empInfo.getLastWorkingDate());
//					attandanceList.get(0).setErrorCode(-1);
//					break;
//				}
//				
//				if(attDet.getValue().trim().equalsIgnoreCase("END")){
//					break ;
//				}
//				
////				if(!calendarFlag){
//					try{
//						
//						
//						double workedHrs=Double.parseDouble(attDet.getValue().trim());
//						WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
//						boolean isWoff = isWeeklyOff(woff,attDet.getDate());
//						boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//						
//						/**
//						 * @author Anil
//						 * @since 13-07-2020
//						 */
//						String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
//						String holidayLabel="H";
//						if(holidayType!=null){
//							logger.log(Level.SEVERE ,attDet.getDate()+ " holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//							if(holidayType.equalsIgnoreCase("National Holiday")){
//								holidayLabel="NH";
//							}else if(holidayType.equalsIgnoreCase("Public Holiday")){
//								holidayLabel="PH";
//							}
//						}
//						if(workedHrs==empInfo.getLeaveCalendar().getWorkingHours()){
//							totalNoOfPresentDays++;
//							
//							if(isWoff){
//								woOt=woOt+workedHrs;
//								totalExtraHours+=workedHrs;
//							}else if(isHday){
//								hOt=hOt+workedHrs;
//								/**
//								 * Date : 17-08-2018 By ANIL
//								 * OT ERROR
//								 */
//								totalExtraHours+=workedHrs;
//								if(holidayLabel.equals("NH")){
//									nhOt=nhOt+workedHrs;
//								}else if(holidayLabel.equals("PH")){
//									phOt=phOt+workedHrs;
//								}
//								logger.log(Level.SEVERE , "--holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//							}
//						}else if(workedHrs>empInfo.getLeaveCalendar().getWorkingHours()){
//							double extraHrs=workedHrs-empInfo.getLeaveCalendar().getWorkingHours();
//							totalExtraHours=totalExtraHours+extraHrs;
//							totalNoOfPresentDays++;
//							
//							WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
//							boolean isWo = isWeeklyOff(wo,attDet.getDate());
//							boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//							if(isWo){
//								woOt=woOt+extraHrs;
//							}else if(isHoliday){
//								hOt=hOt+extraHrs;
//							}else{
//								nOt=nOt+extraHrs;
//							}
//						}else if(workedHrs<empInfo.getLeaveCalendar().getWorkingHours()){
//							double leavaeHrs= empInfo.getLeaveCalendar().getWorkingHours()-workedHrs;
////							halfDayLeave=halfDayLeave+leavaeHrs;
//							totalNoOfPresentDays++;
//							
//							if(isWoff){
//								woOt=woOt+leavaeHrs;
//								/**
//								 * Date : 17-08-2018 By ANIL
//								 * OT ERROR
//								 */
//								totalExtraHours+=workedHrs;
//							}else if(isHday){
//								hOt=hOt+leavaeHrs;
//								/**
//								 * Date : 17-08-2018 By ANIL
//								 * OT ERROR
//								 */
//								totalExtraHours+=workedHrs;
//								
//								if(holidayType.equals("NH")){
//									nhOt=nhOt+workedHrs;
//								}else if(holidayType.equals("PH")){
//									phOt=phOt+workedHrs;
//								}
//							}
//							/**
//							 * @author Anil , Date : 25-06-2019
//							 * will update half day leave if and only if its not on weekly off and holiday
//							 */
//							else{
//								halfDayLeave=halfDayLeave+leavaeHrs;
//							}
//						}
//						
//						
//						
//						/**
//						 * @author Anil, Date :04-05-2019
//						 */
//						String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//						if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//							if(empIdListAsPerdate.contains(empId_Date_Key)){
//								msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//								attandanceList.get(0).setErrorCode(-1);
//							}
//							empIdListAsPerdate.add(empId_Date_Key);
//						}else{
//							empIdListAsPerdate.add(empId_Date_Key);
//						}
//						
//						/**
//						 * @author Anil @since 29-01-2021
//						 */
//						if(existingAttendanceListDtWise.size()!=0){
//							if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
////								error.setEmpId(info.getEmpId());
////								error.setEmpName(info.getEmpName());
////								error.setProjectName(info.getProjName());
////								error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
////								errorList.add(error);
////								attandanceList.get(0).setErrorCode(-1);
//								
//								msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//						
//						
//						
//					}catch(NumberFormatException exception){
//						
//						if(attDet.getValue().trim().equalsIgnoreCase("WO")||attDet.getValue().trim().equalsIgnoreCase("W/O")){
//							WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
//							boolean isWo = isWeeklyOff(wo,attDet.getDate());
//							/**
//							 * @author Anil , Date : 20-08-2019
//							 */
//							boolean isWoDefinedInCal=isWeeklyOffDefined(wo);
//							System.out.println("WO : "+isWo+" Defined : "+isWoDefinedInCal+" Dt : "+attDet.getDate());
//							if(!isWo){
////								woAsLeave++;
//								if(isWoDefinedInCal){
//									msg=msg+" / Weekly off day is not matched. "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							totalNoWeeklyOff++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//						}else if(attDet.getValue().trim().equalsIgnoreCase("H")){
//							boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//							if(!isHoliday){
//								msg=msg+" / Holiday date "+attDet.getDate()+" is not matched in Employee Calendar. ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//							totalNoOfHoliday++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//							String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
//							String holidayLabel="H";
//							if(holidayType!=null){
//								logger.log(Level.SEVERE ,attDet.getDate()+ "holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//								if(holidayType.equalsIgnoreCase("National Holiday")){
//									holidayLabel="NH";
//								}else if(holidayType.equalsIgnoreCase("Public Holiday")){
//									holidayLabel="PH";
//								}
//							}
//							
//						}else if(attDet.getValue().trim().equalsIgnoreCase("L")){
//							totalLeave++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//						}else if(attDet.getValue().trim().equalsIgnoreCase("A")){
//							totalNoOfAbsentDays++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//						}else if(attDet.getValue().trim().equalsIgnoreCase("NA")){
//							if(empInfo.getLastWorkingDate()!=null){
//								if(attDet.getDate().after(empInfo.getDateOfJoining())
//										&&attDet.getDate().before(empInfo.getLastWorkingDate())){
//									msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								
//							}else{
//								if(attDet.getDate().after(empInfo.getDateOfJoining())||attDet.getDate().equals(empInfo.getDateOfJoining())){
//									msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							
////							if(empInfo.getLastWorkingDate()!=null&&!attDet.getDate().before(empInfo.getLastWorkingDate())){
////								msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
////								attandanceList.get(0).setErrorCode(-1);
////							}
//							
//						}
//						/**
//						 * @author Anil
//						 * @since 10-07-2020
//						 * Adding condition for public holiday keyword
//						 */
//						else if(attDet.getValue().trim().contains("PH")&&validPH_OT(attDet.getValue())){
//							
//							WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
//							boolean isWoff = isWeeklyOff(woff,attDet.getDate());
//							boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//							
//							if(isWoff){
//								msg=msg+" / Can not mark attendance as PH on weekly off. "+attDet.getDate()+". ";
//								attandanceList.get(0).setErrorCode(-1);
//							}else if(isHday){
//								msg=msg+" / Can not mark attendance as PH on holiday. "+attDet.getDate()+". ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//							
//							totalNoOfHoliday++;
//							double phOtt=getTotalPhOtHours(attDet.getValue());
//							totalExtraHours=totalExtraHours+phOtt;
//							phOt+=phOtt;
//							
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//						}else{
//							/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/
//							double value = 1;
//							if(leaveTotalMap.containsKey(attDet.getValue().trim())){
//								value = leaveTotalMap.get(attDet.getValue().trim());
//								leaveTotalMap.put(attDet.getValue().trim(), value + 1);
//							}else{
//								leaveTotalMap.put(attDet.getValue().trim(), value);
//							}
//								String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//								if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//									if(empIdListAsPerdate.contains(empId_Date_Key)){
//										msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//										attandanceList.get(0).setErrorCode(-1);
//									}
//									empIdListAsPerdate.add(empId_Date_Key);
//								}else{
//									empIdListAsPerdate.add(empId_Date_Key);
//								}
////							msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
////							attandanceList.get(0).setErrorCode(-1);
//						}
//						
//						
//						/**
//						 * @author Anil @since 29-01-2021
//						 */
//						if(!attDet.getValue().trim().equalsIgnoreCase("NA")&&existingAttendanceListDtWise.size()!=0){
//							if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
////								error.setEmpId(info.getEmpId());
////								error.setEmpName(info.getEmpName());
////								error.setProjectName(info.getProjName());
////								error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
////								errorList.add(error);
////								attandanceList.get(0).setErrorCode(-1);
//								
//								msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//					}
//					
//					/**
//					 * Date : 18-08-2018 By ANIl
//					 * @author Anil
//					 * @since 26-11-2020
//					 * removing overriding functionality
//					 */
////					Attendance attendance=getExistingAttendance(info.getEmpId(),existingAttendanceList,attDet.getDate());
////					if(attendance!=null){
////						attenListToBeRemoved.add(attendance);
////					}
////					
////					EmployeeLeave empLeave=getExistingLeave(info.getEmpId(),approvedLeaveList,attDet.getDate());
////					if(empLeave!=null){
////						leaveListTobeRemoved.add(empLeave);
////						updateLeaveBalance(leaveBalance,empLeave,empInfo);
////					}
////					
////					EmployeeOvertime empOt=getExistingOt(info.getEmpId(),approvedOtList,attDet.getDate());
////					if(empOt!=null){
////						otListToBeRemoved.add(empOt);
////					}
//				
//			}
//			}
//			
//			
//			if(leaveBalance!=null){
//				
//				/**
//				 * @author Anil , Date : 20-08-2019
//				 * We will not maintain WO as leave, will just mark attendance as weekly off
//				 * Suggested by Nitin Sir
//				 */
//				//WO as Leave condition
////				if(woAsLeave>0){
////					boolean woFlag=false;
////					for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
////						if(leave.getShortName().equalsIgnoreCase("WO")||leave.getShortName().equalsIgnoreCase("W/O")){
////							woFlag=true;
////							if(woAsLeave>leave.getBalance()){
////								msg=msg+"/ Total WO leave balance is  "+leave.getBalance()+" only and requested WO leave is "+woAsLeave;
////								attandanceList.get(0).setErrorCode(-1);
////							}
////							break;
////						}
////					}
////					if(!woFlag){
////						msg=msg+"/ No WO type leave is assigned to employee.";
////						attandanceList.get(0).setErrorCode(-1);
////					}
////				}
//				
//				/**
//				 * End
//				 */
//				
//				//Total Paid Leave Condition
//				if(totalLeave>0){
//					boolean leaveFlag=false;
//					double afterDeductionLeaveBalance=0;
//					for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
////						if(leave.getPaidLeave()==true&&
////								!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O")){
//						if(leave.getPaidLeave()==true&&leave.getShortName().contains("PL")){
//							leaveFlag=true;
//							if(totalLeave>leave.getBalance()){
//								msg=msg+"/ Total paid leave balance is  "+leave.getBalance()+" only and requested leave is "+totalLeave;
//								attandanceList.get(0).setErrorCode(-1);
//							}else{
//								afterDeductionLeaveBalance=leave.getBalance()-totalLeave;
//							}
//							break;
//						}
//					}
//					if(!leaveFlag){
//						msg=msg+"/ No paid type leave is assigned to employee.";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//					
//					//Early leave/Half day leave condition
//					if(halfDayLeave>0){
//						halfDayLeave=halfDayLeave/empInfo.getLeaveCalendar().getWorkingHours();
//						if(afterDeductionLeaveBalance>0){
//							if(halfDayLeave>afterDeductionLeaveBalance){
//								msg=msg+"/"+(halfDayLeave-afterDeductionLeaveBalance)+"  half day leave will be considered as non paid leave as no paid leave is balanced.";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}else{
//							msg=msg+"/ All half day leave will be considered as non paid leave as no paid leave is balanced.";
//							attandanceList.get(0).setErrorCode(-1);
//						}
//					}
//				}
//				/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/ 
//				if(leaveTotalMap.size() > 0){
//					
//							for(Map.Entry<String, Double> entry : leaveTotalMap.entrySet()){
//								boolean leaveFlag=false;
//								double afterDeductionLeaveBalance = 0;
//								logger.log(Level.SEVERE , "leave type key :"+  entry.getKey());
//								for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//									
//									if((leave.getShortName().equalsIgnoreCase(entry.getKey()) && leave.getPaidLeave()==true)
//											&&
//											(!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O"))){
//										logger.log(Level.SEVERE , "leave type key :"+  entry.getValue());
//										leaveFlag=true;
//										if(entry.getValue()>leave.getBalance()){
//											msg=msg+"/ Total "+ leave.getName()  +" leave balance is  "+leave.getBalance()+" only and requested leave is "+entry.getValue();
//											attandanceList.get(0).setErrorCode(-1);
//										}
//										break;
//									}
//								}
//								if(!leaveFlag){
//									msg=msg+"/ No "+ entry.getKey() +" type leave is assigned to employee.";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//						}
//					
//				}
//				/**
//				 * end komal
//				 */
//			}else{
//				msg=msg+"/ No leave balance is defined against employee. ";
//				attandanceList.get(0).setErrorCode(-1);
//			}
//			
//			
//			if(totalExtraHours>0){
//				if(projectList!=null&&projectList.size()!=0){
//					HrProject project=getProjectName(info.getProjName(),projectList);
//					if(project!=null){
//						ArrayList<Overtime>otList=new ArrayList<Overtime>();
//						/**
//						 * @author Anil
//						 * @since 28-08-2020
//						 * Relievers OT
//						 */
//						String designation=null;
//						
//						/**
//						 * @author Anil @since 13-04-2021
//						 * Updated logic of Reliever ot validation 
//						 * Raised by Rahul Tiwari for sasha
//						 */
//						if(empInfo.isReliever()){
//							if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
////								otList=project.getRelieversOtList();
//								otList.addAll(project.getRelieversOtList());
//								designation=info.getEmpDesignation();
//							}
//						}
//						
//						/**
//						 * @author Vijay Date :- 08-09-2022
//						 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//						 */
//						ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//						CommonServiceImpl commonservice = new CommonServiceImpl();
//						overtimelist = commonservice.getHRProjectOvertimelist(project);
//						/**
//						 * ends here
//						 */
////						if(project.getOtList()!=null&&project.getOtList().size()!=0){
//////							otList=project.getOtList();
////							otList.addAll(project.getOtList());
////						}
//						if(overtimelist!=null&&overtimelist.size()!=0){
//							otList.addAll(overtimelist);
//						}
//						
//						if(otList.size()!=0){
//							logger.log(Level.SEVERE ,"woOt: "+ woOt+" hOt "+hOt+" nOt "+nOt+" phOt "+phOt+" nhOt "+nhOt);
//							logger.log(Level.SEVERE ,"Designation : "+designation+" Reliever "+empInfo.isReliever());
//							if(woOt>0){
//								Overtime ot=getOvertimeName(otList,"WO",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"WOT "+ot.getName());
//								}else{
//									msg=msg+"/ Weekly off OT is not added in project for employee "+info.getEmpName()+". ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							if(hOt>0){
//								Overtime ot=getOvertimeName(otList,"H",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"HOT "+ot.getName());
//									/**
//									 * Date : 27-08-2018 By ANIL
//									 */
//									if(ot.isLeave()==true){
//										boolean leaveFlag=false;
//										for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//											if(leave.getName().equals(ot.getLeaveName())){
//												leaveFlag=true;
//												break;
//											}
//										}
//										if(!leaveFlag){
//											msg=msg+"/ No Leave - "+ot.getLeaveName()+" is assigned to employee.";
//											attandanceList.get(0).setErrorCode(-1);
//										}
//									}
//									/**
//									 * End
//									 */
//								}else{
//									msg=msg+"/ Holiday OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							if(nOt>0){
//								Overtime ot=getOvertimeName(otList,"N",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"NOT "+ot.getName());
//								}else{
//									msg=msg+"/ OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							
//							if(phOt>0){
//								Overtime ot=getOvertimeName(otList,"PH",info.getEmpId(),designation);
//								logger.log(Level.SEVERE,"phOt :: "+ot);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"PHOT "+ot.getName());
//								}else{
//									msg=msg+"/ PH OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							
//							if(nhOt>0){
//								Overtime ot=getOvertimeName(otList,"NH",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"NHOT "+ot.getName());
//								}else{
//									msg=msg+"/ NH OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//						}else{
//							msg=msg+"/ Please add OT details in Project. ";
//							attandanceList.get(0).setErrorCode(-1);
//						}
//					}else{
//						msg=msg+"/ Project name is not matched in DB. ";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//					
//				}else{
//					msg=msg+"/ Project name is not matched in DB. ";
//					attandanceList.get(0).setErrorCode(-1);
//				}
//			}
//			
//			
//
//			
//			
//			
//			/**
//			 * @author Anil , Date : 09-05-2019
//			 * validating ot and dot details
//			 */
//			if(info.isOtDotFlag()==true){
//				HrProject project=getProjectName(info.getProjName(),projectList);
//				ArrayList<Overtime>otList=new ArrayList<Overtime>();
////				if(project!=null&&project.getOtList()!=null){
////					otList=project.getOtList();
////				}
//				
//				/**
//				 * @author Anil
//				 * @since 28-08-2020
//				 * Relievers OT
//				 */
//				String designation=null;
//				/**
//				 * @author Anil @since 13-04-2021
//				 * Updated logic of Reliever ot validation 
//				 * Raised by Rahul Tiwari for sasha
//				 */
//				
////				if(empInfo.isReliever()){
////					if(project!=null&&project.getRelieversOtList()!=null){
////						otList=project.getRelieversOtList();
////						designation=info.getEmpDesignation();
////					}
////				}else{
////					if(project!=null&&project.getOtList()!=null){
////						otList=project.getOtList();
////					}
////				}
//				
//				if(empInfo.isReliever()){
//					if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
////						otList=project.getRelieversOtList();
//						otList.addAll(project.getRelieversOtList());
//						designation=info.getEmpDesignation();
//					}
//				}
//				
////				if(project.getOtList()!=null&&project.getOtList().size()!=0){
//////					otList=project.getOtList();
////					otList.addAll(project.getOtList());
////				}
//				/**
//				 * @author Vijay Date :- 08-09-2022
//				 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//				 */
//				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//				CommonServiceImpl commonservice = new CommonServiceImpl();
//				overtimelist = commonservice.getHRProjectOvertimelist(project);
//				
//				if(overtimelist!=null&&overtimelist.size()!=0){
//					otList.addAll(overtimelist);
//				}
//				
//				/**
//				 * ends here
//				 */
//				
//				
//				
//				logger.log(Level.SEVERE,"OT?DOT :: ");
//				double singleOt=0;
//				double doubleOt=0;
//				if(!info.getSingleOt().equalsIgnoreCase("NA")){
//					try{
//						singleOt=Double.parseDouble(info.getSingleOt());
//						if(singleOt!=0){
//							Overtime ot=getOvertimeName(otList,"OT",info.getEmpId(),designation);
//							if(ot==null){
//								msg=msg+"/ Single OT is not added in Project. ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//					}catch(Exception e){
//						msg=msg+"/ Please add proper value against single ot column. ";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//				}
//				
//				if(!info.getDoubleOt().equalsIgnoreCase("NA")){
//					try{
//						doubleOt=Double.parseDouble(info.getDoubleOt());
//						if(doubleOt!=0){
//							Overtime ot=getOvertimeName(otList,"DOT",info.getEmpId(),designation);
//							if(ot==null){
//								msg=msg+"/ Double OT is not added in Project. ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//					}catch(Exception e){
//						msg=msg+"/ Please add proper value against double ot column. ";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//				}
//				
//				logger.log(Level.SEVERE,"SL "+singleOt+" DL "+doubleOt);
//			}
//			
//			
//			
//			
//			
//			
//			
//		
//			
//			
//			error.setEmpId(info.getEmpId());
//			error.setEmpName(info.getEmpName());
//			error.setProjectName(info.getProjName());
//			
//			error.setTotalNoOfDays(totalNoOfDays);
//			error.setTotalNoOfPresentDays(totalNoOfPresentDays);
//			error.setTotalNoOfWO(totalNoWeeklyOff);
//			error.setTotalNoOfHoliday(totalNoOfHoliday);
//			error.setTotalNoOfAbsentDays(totalNoOfAbsentDays);
//			
//			if(empInfo!=null&&!calendarFlag){
//			double leaveDays=totalLeave;
//			double extraDays=totalExtraHours/empInfo.getLeaveCalendar().getWorkingHours();
//			error.setTotalNoOfLeave(leaveDays);
//			error.setTotalNoOfExtraDays(extraDays);
//			}
//			
//			
//			
//			msg=msg+" / "+error.getRemark();
//			error.setRemark(msg);
//			errorList.add(error);
//			
//			
//			
//			
//		}
//		
//		ObjectifyService.reset();
//		ofy().consistency(Consistency.STRONG);
//		ofy().clear();
//		
//		/**
//		 * @author Anil
//		 * @since 26-11-2020
//		 * commenting overriding functionality
//		 */
////		if(attandanceList.get(0).getErrorCode()!=-1){
////			updateLeaveBalAndRemoveOldRecords(attenListToBeRemoved,leaveListTobeRemoved,otListToBeRemoved,empInfoList);
////		}
//		
//		/**
//		 * @author Vijay Date :- 13-12-2022
//		 * Des :- commented code not required this validation
//		 */
//		int attendanceNotUploadedCounter=0;
//		try{
//			/**
//			 * Date 16-3-2019 added by Amol for the employees whoes attendanced are
//			 * not marked , error message should be dislpay in Excel sheet as
//			 * a"Attendance Not Found."
//			 **/
//			
//			String msg = "";
//			AttandanceError error = new AttandanceError();
//			for (HrProject hrProject : projectList) {
//				HashSet<Integer>hsId=new HashSet<Integer>();
//				
//				/**
//				 * @author Vijay Date :- 08-09-2022
//				 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//				 */
//				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//				CommonServiceImpl commonservice = new CommonServiceImpl();
//				overtimelist = commonservice.getHRProjectOvertimelist(hrProject);
//				/**
//				 * ends here
//				 */
////				for (Overtime ot : hrProject.getOtList()) {
//				for (Overtime ot : overtimelist) {
//				
//					if(hsId.size()!=0){
//						if(hsId.contains(ot.getEmpId())){
//							continue;
//						}
//					}
//					hsId.add(ot.getEmpId());
//					
//					boolean isEmpPresent = CheckEmpAttendance(ot.getEmpId(),attandanceList);
//					if (!isEmpPresent) {
//						error = new AttandanceError();
//						msg = "/Attendance Not Found.";
//						error.setEmpId(ot.getEmpId());
//						error.setEmpName(ot.getEmpName());
//						error.setProjectName(hrProject.getProjectName());
//						error.setRemark(msg);
//						errorList.add(error);
//						attendanceNotUploadedCounter++;
//					}
//				}
//			}
//		}catch(Exception e){
//			
//		}
//		CsvWriter.attendanceErrorList=errorList;
//		
//		if(attendanceNotUploadedCounter!=0){
//			String comment=attendanceNotUploadedCounter+" employees attendance not uploaded!";
//			logger.log(Level.SEVERE,"EMPLOYEE ATTENDANCE NOT UPLOADED");
//			System.out.println("EMPLOYEE ATTENDANCE NOT UPLOADED");
//			attandanceList.get(0).setComment(comment);
//		}
//		
//		CsvWriter.attendanceErrorList=errorList;
//
//		return attandanceList;
	}



	private boolean validateSiteLocation(ArrayList<CustomerBranchDetails> siteLocationList,String siteLocation, String state,int custId) {
		for(CustomerBranchDetails obj:siteLocationList){
			if(obj.getBusinessUnitName().equals(siteLocation)&&obj.getAddress()!=null&&obj.getAddress().getState().equals(state)&&obj.getCinfo().getCount()==custId){
				return true;
			}
		}
		return false;
	}



	public double getTotalPhOtHours(String value) {
		try{
			String ph_ot = value.replaceAll("\\s", "");
			if(ph_ot.equalsIgnoreCase("PH")){
				return 0;
			}else{
				if(ph_ot.contains("-")){
					String[] phArray = ph_ot.split("-");
					String ph=phArray[0];
					String ot=phArray[1];
					if(ph.equalsIgnoreCase("PH")){
						try{
							int otNum=Integer.parseInt(ot);
							return otNum;
						}catch(Exception e){
							
						}
					}
				}
			}
		}catch(Exception e){
			
		}
		return 0;
	}



	public boolean validPH_OT(String value) {
		try{
			String ph_ot = value.replaceAll("\\s", "");
			if(ph_ot.equalsIgnoreCase("PH")){
				return true;
			}else{
				if(ph_ot.contains("-")){
					String[] phArray = ph_ot.split("-");
					String ph=phArray[0];
					String ot=phArray[1];
					if(ph.equalsIgnoreCase("PH")){
						try{
							int otNum=Integer.parseInt(ot);
							return true;
						}catch(Exception e){
							
						}
					}
				}
			}
		}catch(Exception e){
			
		}
		return false;
	}



	private boolean isEmployeeFingerRegistered(int empId,List<Employee> employeeList) {
		// TODO Auto-generated method stub
		for(Employee emp:employeeList){
			if(emp.getCount()==empId){
				return emp.isFingerDataPresent();
			}
		}
		return false;
	}


	private boolean CheckEmpAttendance(int empId,ArrayList<AttandanceInfo> attandanceList) {
		for (AttandanceInfo info : attandanceList) {
			if (empId == info.getEmpId()) {
				return true;
			}
		}
		
		/**
		 * 
		 */
		
		Attendance attendance=ofy().load().type(Attendance.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("month", attandanceList.get(0).getMonthYear()).filter("empId",empId).first().now();
		if(attendance!=null){
			return true;
		}
		
		/**
		 * 
		 */
		
		return false;
	}
	
	
	
	

	public void updateLeaveBalAndRemoveOldRecords(
			List<Attendance> attenListToBeRemoved,
			List<EmployeeLeave> leaveListTobeRemoved,
			List<EmployeeOvertime> otListToBeRemoved,
			List<EmployeeInfo> empInfoList) {
			// TODO Auto-generated method stub
			
			HashSet<Integer> hsEmpId=new HashSet<Integer>();
			for(EmployeeLeave leave:leaveListTobeRemoved){
				hsEmpId.add(leave.getEmplId());
			}
			for(EmployeeOvertime ot:otListToBeRemoved){
				hsEmpId.add(ot.getEmplId());
			}
			ArrayList<Integer> empList=new ArrayList<Integer>(hsEmpId);
			List<LeaveBalance>empLeaveBalList=new ArrayList<LeaveBalance>();
			if(empList.size()!=0){
				empLeaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", empInfoList.get(0).getCompanyId()).filter("empInfo.empCount IN", empList).list();
			}
			
			if(empLeaveBalList!=null){
				logger.log(Level.SEVERE,"UPDATE EMPLOYEE LEAVE LIST SIZE : "+empLeaveBalList.size());
				
				/**
				 * @author Anil
				 * @since 08-12-2020
				 * Added leave balance update code in try block to avoid interruption of program
				 * As we are not maintaining unpaid leaves so no need to reveres it again
				 * issue found for sasha - raised by rahul
				 */
				try{
					for(EmployeeLeave empLeave:leaveListTobeRemoved){
						LeaveBalance leaveBal=getEmpLeaveBalance(empLeave.getEmplId(), empLeaveBalList);
						EmployeeInfo info=validateEmpId(empLeave.getEmplId(), empInfoList);
						if(leaveBal!=null){
							try{
								for(AllocatedLeaves leave:leaveBal.getAllocatedLeaves()){
									if(leave.getName().equals(empLeave.getName())){
										double leaveDays=empLeave.getLeaveHrs()/info.getLeaveCalendar().getWorkingHours();
										/**
										 * @author Anil , Date : 26-07-2019
										 * avoidin negative stock availed
										 */
										if((leave.getAvailedDays()-leaveDays)>=0){
											leave.setBalance(leave.getBalance()+leaveDays);
											/**
											 * Date : 16-12-2018 By Anil
											 */
											leave.setAvailedDays(leave.getAvailedDays()-leaveDays);
										}
									}
								}
							}catch(Exception e){
								
							}
						}
					}
				}catch(Exception e){
					
				}
				
				
				for(EmployeeOvertime eot:otListToBeRemoved){
					LeaveBalance leaveBal=getEmpLeaveBalance(eot.getEmplId(), empLeaveBalList);
					if(eot.getOvertime().isLeave()==true){
						double earnedleave=0;
						double overtimeHrs=eot.getOtHrs();
						overtimeHrs=overtimeHrs*eot.getOvertime().getLeaveMultiplier();
						EmployeeInfo info=validateEmpId(eot.getEmplId(), empInfoList);
						logger.log(Level.SEVERE, "Inside comp off");
						earnedleave=overtimeHrs/info.getLeaveCalendar().getWorkingHours();
//						earnedleave=overtimeHrs/eot.getEmployee().getLeaveCalendar().getWorkingHours();
	//					System.out.println("earned Leave ::: "+earnedleave);
						
						if(leaveBal!=null){
							for(int i=0;i<leaveBal.getLeaveGroup().getAllocatedLeaves().size();i++){
								if(eot.getOvertime().getLeaveName().trim().equals(leaveBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim())){
									
									double balEarned=leaveBal.getLeaveGroup().getAllocatedLeaves().get(i).getEarned();
									balEarned=balEarned+earnedleave;
									leaveBal.getLeaveGroup().getAllocatedLeaves().get(i).setEarned(balEarned);
									
									double balLeave=leaveBal.getLeaveGroup().getAllocatedLeaves().get(i).getBalance();
									balLeave=balLeave-earnedleave;
									leaveBal.getLeaveGroup().getAllocatedLeaves().get(i).setBalance(balLeave);
									
									LeaveValidityInfo leaveInfo=new LeaveValidityInfo();
									leaveInfo.setLeaveName(eot.getOvertime().getLeaveName());
									leaveInfo.setLeaveCount(earnedleave);
									Date validDate=new Date(eot.getOtDate().getTime());
									Date validDate1=DateUtility.addDaysToDate(validDate, leaveBal.getLeaveGroup().getAllocatedLeaves().get(i).getNo_of_days_to_avail());
									leaveInfo.setValidUntil(validDate1);
									if(leaveBal.getLeaveValidityList()!=null){
										leaveBal.getLeaveValidityList().add(leaveInfo);
									}else{
										List<LeaveValidityInfo> leaveLis=new ArrayList<LeaveValidityInfo>();
										leaveLis.add(leaveInfo);
										leaveBal.setLeaveValidityList(leaveLis);
									}
								}
							}
						}
					}
				}
			}
			
//			if(empLeaveBalList!=null&&empLeaveBalList.size()!=0){
//				ofy().save().entities(empLeaveBalList).now();
//			}
			try{
				ofy().save().entities(empLeaveBalList).now();
			}catch(Exception e){
				
			}
		
//			logger.log(Level.SEVERE, "ATT SIZE : "+attenListToBeRemoved.size()+" LEAVE SIZE : "+leaveListTobeRemoved.size()+" OT SIZE : "+otListToBeRemoved.size());
//			if(attenListToBeRemoved.size()!=0){
//				ofy().delete().entities(attenListToBeRemoved).now();
//			}
			try{
				ofy().delete().entities(attenListToBeRemoved).now();
			}catch(Exception e){
				
			}
			
//			if(leaveListTobeRemoved.size()!=0){
//				ofy().delete().entities(leaveListTobeRemoved).now();
//			}
			try{
				ofy().delete().entities(leaveListTobeRemoved).now();
			}catch(Exception e){
				
			}
			
//			if(otListToBeRemoved.size()!=0){
//				ofy().delete().entities(otListToBeRemoved).now();
//			}
			try{
				ofy().delete().entities(otListToBeRemoved).now();
			}catch(Exception e){
				
			}
			
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
		
	}

	private void updateLeaveBalance(LeaveBalance leaveBalance,
			EmployeeLeave empLeave, EmployeeInfo empInfo) {
		// TODO Auto-generated method stub
		for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
			if(leave.getName().equals(empLeave.getName())){
				double leaveDays=empLeave.getLeaveHrs()/empInfo.getLeaveCalendar().getWorkingHours();
				leave.setBalance(leave.getBalance()+leaveDays);
				leave.setAvailedDays(leave.getAvailedDays()-leaveDays);//Ashwini Patil Date:19-05-2022
			}
		}	
	}

	private EmployeeOvertime getExistingOt(int empId,
			List<EmployeeOvertime> approvedOtList, Date date) {
		// TODO Auto-generated method stub
		for(EmployeeOvertime ot:approvedOtList){
			if(empId==ot.getEmplId()&&date.equals(ot.getOtDate())){
				return ot;
			}
		}
		return null;
	}

	private EmployeeLeave getExistingLeave(int empId,
			List<EmployeeLeave> approvedLeaveList, Date date) {
		// TODO Auto-generated method stub
		for(EmployeeLeave empLeave:approvedLeaveList){
			if(empId==empLeave.getEmplId()&&date.equals(empLeave.getLeaveDate())){
				return empLeave;
			}
		}
		return null;
	}

	private Attendance getExistingAttendance(int empId,
			List<Attendance> existingAttendanceList, Date date) {
		// TODO Auto-generated method stub
		for(Attendance attendance:existingAttendanceList){
			if(empId==attendance.getEmpId()&&date.equals(attendance.getAttendanceDate())){
				return attendance;
			}
		}
		
		return null;
	}

	private List<EmployeeOvertime> getApprovedOtList(long companyId, String monthYear,
			ArrayList<Integer> empList, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat isoFormat = new SimpleDateFormat("MMM-yyyy");
		isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		Calendar c = Calendar.getInstance(); 
//		Date startDate=null;
//		Date endDate=null;
		if(startDate==null||endDate==null){
			try {
				Date d=isoFormat.parse(monthYear);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				startDate=new Date(d.getTime());
				endDate=new Date(d.getTime());
				
				startDate=DateUtility.getStartDateofMonth(d);
				endDate=DateUtility.getEndDateofMonth(d);
				
				logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
				logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
			}catch(Exception e){
				
			}
		}
		logger.log(Level.SEVERE,"START DATE ::: "+startDate);
		logger.log(Level.SEVERE,"END DATE ::: "+endDate);
		
		List<EmployeeOvertime> otList=ofy().load().type(EmployeeOvertime.class).filter("companyId",companyId).filter("emplId IN", empList).filter("otDate >=",startDate).filter("otDate <=",endDate).list();
		if(otList!=null&&otList.size()!=0){
			logger.log(Level.SEVERE,"otList "+otList.size());
//			ofy().delete().entities(otList).now();
		}
		return otList;
	}

	private List<EmployeeLeave> getApprovedLeaveList(long companyId, String monthYear,
			ArrayList<Integer> empList, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat isoFormat = new SimpleDateFormat("MMM-yyyy");
		isoFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		Calendar c = Calendar.getInstance(); 
//		Date startDate=null;
//		Date endDate=null;
		if(startDate==null||endDate==null){
			try {
				Date d=isoFormat.parse(monthYear);
				c.setTime(d);
				c.add(Calendar.DATE, 1);
				
				d=DateUtility.getDateWithTimeZone("IST", c.getTime());
				
				startDate=new Date(d.getTime());
				endDate=new Date(d.getTime());
				
				startDate=DateUtility.getStartDateofMonth(d);
				endDate=DateUtility.getEndDateofMonth(d);
				
				logger.log(Level.SEVERE,"START DATE --- ::: "+startDate);
				logger.log(Level.SEVERE,"END DATE  --- ::: "+endDate);
			}catch(Exception e){
				
			}
		}
		logger.log(Level.SEVERE,"START DATE  ::: "+startDate);
		logger.log(Level.SEVERE,"END DATE  ::: "+endDate);
		
		List<EmployeeLeave> leaves=ofy().load().type(EmployeeLeave.class).filter("companyId",companyId).filter("emplId IN", empList).filter("leaveDate >=",startDate).filter("leaveDate <=",endDate).list();
		if(leaves!=null&&leaves.size()!=0){
			logger.log(Level.SEVERE,"leaves "+leaves.size());
//			ofy().delete().entities(leaves).now();
		}
		return leaves;
	}

	private List<Attendance> getExistingAttendance(long companyId, String monthYear,
			ArrayList<Integer> empList, ArrayList<String> projList) {
		// TODO Auto-generated method stub
		
//		List<Attendance> attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("empId IN", empList).filter("month", monthYear).list();
	
		List<Attendance> attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId)
				.filter("projectName IN", projList).filter("month", monthYear).list();

		logger.log(Level.SEVERE,"attendanceList == "+attendanceList.size());
		/**
		 * @author Anil, Date : 07-12-2019
		 * Commented log as printing/calling size method of list causes concurrent exception
		 */
//		if(attendanceList!=null&&attendanceList.size()!=0){
//			logger.log(Level.SEVERE,"attendanceList "+attendanceList.size());
//			ofy().delete().entities(attendanceList).now();
//		}
		
		return attendanceList;
	}

	/**
	 * @author Anil
	 * @since 07-09-2020
	 * Added designation in case of reliever employee to get OT details
	 * @param designation
	 * @return
	 */
	private Overtime getOvertimeName(ArrayList<Overtime> otList, String otType, int empId,String designation) {
		for(Overtime ot:otList){
			
			if(otType.equalsIgnoreCase("WO")&&ot.isWeeklyOff()==true&&ot.getEmpId()==empId){
				return ot;
			}
			if(otType.equalsIgnoreCase("N")&&ot.isNormalDays()==true&&ot.getEmpId()==empId){
				return ot;
			}
			if((otType.equalsIgnoreCase("H")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))){
				return ot;
			}
			
			if((otType.equalsIgnoreCase("PH")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
				return ot;
			}
			
			if((otType.equalsIgnoreCase("NH")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
				return ot;
			}
			
			/**
			 * @author Anil, Date :09-05-2019
			 */
			if((otType.equalsIgnoreCase("OT")&&ot.getEmpId()==empId)&&(ot.getShortName().equalsIgnoreCase("OT"))){
				return ot;
			}
			if((otType.equalsIgnoreCase("DOT")&&ot.getEmpId()==empId)&&(ot.getShortName().equalsIgnoreCase("DOT"))){
				return ot;
			}
		}
		
		for(Overtime ot:otList){
			if(ot.getEmpId()==0&&designation!=null){
				if(otType.equalsIgnoreCase("WO")&&ot.isWeeklyOff()==true&&ot.getEmpDesignation().equals(designation)){
					return ot;
				}
				if(otType.equalsIgnoreCase("N")&&ot.isNormalDays()==true&&ot.getEmpDesignation().equals(designation)){
					return ot;
				}
				if((otType.equalsIgnoreCase("H")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))){
					return ot;
				}
				
				if((otType.equalsIgnoreCase("PH")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
					return ot;
				}
				
				if((otType.equalsIgnoreCase("NH")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
					return ot;
				}
				
				if((otType.equalsIgnoreCase("OT")&&ot.getEmpDesignation().equals(designation))&&(ot.getShortName().equalsIgnoreCase("OT"))){
					return ot;
				}
				if((otType.equalsIgnoreCase("DOT")&&ot.getEmpDesignation().equals(designation))&&(ot.getShortName().equalsIgnoreCase("DOT"))){
					return ot;
				}
			}
		}
		
		
		return null;
	}

	private HrProject getProjectName(String projName,List<HrProject> projectList) {
		for(HrProject proj:projectList){
			if(proj.getProjectName().equals(projName)){
				return proj;
			}
		}
		return null;
	}

	private LeaveBalance getEmpLeaveBalance(int empId,List<LeaveBalance> empLeaveBalList) {
		// TODO Auto-generated method stub
		for(LeaveBalance balance:empLeaveBalList){
			if(empId==balance.getEmpCount()){
				return balance;
			}
		}
		return null;
	}

	private EmployeeInfo validateEmpId(int empId, List<EmployeeInfo> empInfoList) {
		for(EmployeeInfo info:empInfoList){
			/**
			 * Date : 27-08-2018 By ANIL
			 * Also checking whether employee is active or not
			 */
//			if(empId==info.getEmpCount()){
			if(empId==info.getEmpCount()&&info.isStatus()==true){
				return info;
			}
		}
		return null;
	}

	public boolean isWeeklyOff(WeekleyOff wo,Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		c.setTimeZone(TimeZone.getTimeZone("IST"));
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
//		logger.log(Level.SEVERE,"DATE : "+date+"DAY OF WEEK : "+dayOfWeek);
		if (dayOfWeek==1)
			return wo.isSUNDAY();
		if (dayOfWeek==2)
			return wo.isMONDAY();
		if (dayOfWeek==3)
			return wo.isTUESDAY();
		if (dayOfWeek==4)
			return wo.isWEDNESDAY();
		if (dayOfWeek==5)
			return wo.isTHRUSDAY();
		if (dayOfWeek==6)
			return wo.isFRIDAY();
		if (dayOfWeek==7)
			return wo.isSATAURDAY();
		return false;
	}
	
	/**
	 * @author Anil , Date : 20-08-2019
	 * this method checks whether weekly off is defined at calendar level or not
	 */
	public boolean isWeeklyOffDefined(WeekleyOff wo){
		
		if(wo.isSUNDAY()||wo.isMONDAY()||wo.isTUESDAY()||wo.isWEDNESDAY()||wo.isTHRUSDAY()||wo.isFRIDAY()||wo.isSATAURDAY()){
			return true;
		}
		return false;
	}
	

	@Override
	public ArrayList<AttandanceInfo> updateAttandance(ArrayList<AttandanceInfo> attandanceList) {

		logger.log(Level.SEVERE,"Hi Attendance save Task queue calling ");
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", "Employee Attendance").param("companyId", attandanceList.get(0).getCompanyId()+"").param("month", attandanceList.get(0).getMonth()).param("year",attandanceList.get(0).getYear())
				.param("startDate", fmt.format(attandanceList.get(0).getStartDate())).param("endDate", fmt.format(attandanceList.get(0).getEndDate()))
				.param("startLimit", attandanceList.get(0).getStartLimit()+"").param("endLimit", attandanceList.get(0).getEndLimit()+"")
				.param("noOfDays", attandanceList.get(0).getNoOfDays()+"").param("lastUpdatedIndex", attandanceList.get(0).getLastIndexUpdated()+""));
		logger.log(Level.SEVERE,"Hi Task queue cazlling save");
		
		return null;
	}
	
	private String isHoliday(List<EmployeeInfo> empInfoList , JSONArray jsonArray){
		
		DocumentUploadTaskQueue taskQueue = new DocumentUploadTaskQueue();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String actionLabel = "";
		JSONArray array = new JSONArray();
		JSONObject jObject = null;
		int count = 0;
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject object = null;
			Date date = null;
			int empId = 0;
			try{
					object = jsonArray.getJSONObject(i);
					jObject = new JSONObject();
					actionLabel = object.optString("actionLabel").trim();
					date = sdf.parse(object.optString("attendanceDate")
							.trim());
					logger.log(Level.SEVERE , "Attendance date in holiday method :"+ date+"emp id :" +object.optString("empId").trim() + "date :"+object.optString("attendanceDate").trim());
					
					empId = Integer.parseInt(object.optString("empId").trim());
			}catch(Exception e){
				logger.log(Level.SEVERE, "error: " + e);
			}
			logger.log(Level.SEVERE , "Attendance date in holiday method :"+ date+"emp id :" + empId);
			EmployeeInfo empInfo = taskQueue.getEmployeeInfo(empId, empInfoList);
			logger.log(Level.SEVERE , "Emp Info:"+ empInfo);
			boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(date);
			try{
			if(isHoliday && actionLabel.equals(AppConstants.H)){
				jObject.put(empId+"", "Successful");
			}else if(!isHoliday && actionLabel.equals(AppConstants.H)){
				jObject.put(empId+"", "Holiday is not assigned for this date.");
				count = count + 1;
			}else{
				jObject.put(empId+"", "Successful");
			}
			}catch(Exception e){
				logger.log(Level.SEVERE , "json error:"+ e);
			}
			array.put(jObject);
		}
		if(count >0){
			String arr = array.toString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE ,"json array : "+ arr);
			return arr;
		}
		return null;
	}
	
	/** date 10.8.2018 added by komal for attendance save from android**/
	public String employeeAttendance(long companyId, JSONArray jsonArray,
			String status) {
		Attendance attendace = null;
		DocumentUploadTaskQueue taskQueue = new DocumentUploadTaskQueue();

			//logger.log(Level.SEVERE, "updateAttendance companyId: " + companyId+ " status: " + status);
		//	logger.log(Level.SEVERE, "updateAttendance jsonArray: " + jsonArray);
			ArrayList<Attendance> attArrayList = new ArrayList<Attendance>();
			/** date 22.8.2018 added by komal for ot in hr project **/
			ArrayList<String> projList = new ArrayList<String>();
			List<HrProject> projectList = new ArrayList<HrProject>();
			List<Integer> empList = new ArrayList<Integer>();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String loadProjectName = ""  , loadShiftName = "";
			Date loadAttendanceDate = null;
			String updatedEmployee = "";
			
			/**
			 * @author Anil @since 25-01-2021
			 * checking for Both in time and out time
			 */
			boolean bothInAndOutTimePresentFlag=false,bothInAndOutTimeAbsentFlag=false,onlyInTime=false;
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject object = null;
				try{
					object = jsonArray.getJSONObject(i);
				}catch(Exception e){
					//logger.log(Level.SEVERE, "error: " + e);
				}
				
				
				try { //added on 24-07-2023
				String projectName = object.optString("projectName").trim();
				
				updatedEmployee = object.optString("updatedEmployeeName");
						
				projList.add(projectName);
				int empId = Integer.parseInt(object.optString("empId").trim());
				empList.add(empId);
				logger.log(Level.SEVERE,"employye added to emplist="+empId);
				
				/**
				 * @author Anil @since 25-01-2021
				 * Here checking for both in time and out time status
				 * for loading project list and employee leave list
				 * required only when both in time and out time is present for calculating OT if any
				 * required when both in time and out time is null for marking leave or absent
				 */
				try {
					String inTime="",outTime="";
					inTime = object.optString("inTime").trim();
					outTime = object.optString("outTime").trim();
					
					if(inTime!=null&&outTime!=null&&!inTime.equals("")&&!outTime.equals("")){
						bothInAndOutTimePresentFlag=true;
					}
					if((inTime==null&&outTime==null)||(inTime.equals("")&&outTime.equals(""))){
						bothInAndOutTimeAbsentFlag=true;
					}
					if((inTime!=null&&!inTime.equals(""))&&(outTime==null||outTime.equals(""))){
						onlyInTime=true;
						logger.log(Level.SEVERE,"Only in time");
					}
					
				} catch (Exception e) {
					//logger.log(Level.SEVERE, "Error:attendancedate: " + e);
				}
				
				
			}catch(Exception ex) {
				ex.printStackTrace();
				}
			}
			logger.log(Level.SEVERE, "bothInAndOutTimePresentFlag: " + bothInAndOutTimePresentFlag+" | bothInAndOutTimeAbsentFlag: "+bothInAndOutTimeAbsentFlag);
			
			/**
			 * @author Anil @since 25-01-2021
			 * Here project list is loaded every time of attendance marking whether it is required or not
			 * loading data when attendance status is approved or in time 
			 * and out time is not null
			 */
			if(bothInAndOutTimePresentFlag){
				projectList=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName IN", projList).list();
			}
			/**
			 * end komal
			 */
		//	logger.log(Level.SEVERE, "Unique Project List size: " + projList.size());
		//	logger.log(Level.SEVERE, "Project List size: " + projectList.size());
		//	logger.log(Level.SEVERE, "Unique Emp size: " + empList.size());
			List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
			
			if(empList.size()!=0){
				empInfoList = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empList).list();
			}
//			logger.log(Level.SEVERE, "Employee Info size: " + empInfoList.size());
//			String holidayValidation = isHoliday(empInfoList , jsonArray);
//			if(holidayValidation !=null){
//				return holidayValidation;
//			}
			
			/**
			 * @author Anil @since 25-01-2021
			 * Loading leave balance list if both in time and out time is null or not null
			 */
			List<LeaveBalance>empLeaveBalList=new ArrayList<LeaveBalance>();
			if(bothInAndOutTimePresentFlag||bothInAndOutTimeAbsentFlag||onlyInTime){//Ashwini Patil
				empLeaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount IN", empList).list();
			}
			
//			logger.log(Level.SEVERE, "leave Bal size: " + empLeaveBalList.size());
			
			for (int i = 0; i < jsonArray.length(); i++) {
				
				try { //added on 24-07-2023
				/*
				 * Name: Apeksha Gunjal Date: 16/07/2018 Note: If attendance is
				 * already approved then no one should changed in it.
				 */
				boolean isAttendanceAlreadyApproved = false;

				attendace = new Attendance();

				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
				
				JSONObject object = null;
				try{
					object = jsonArray.getJSONObject(i);
				}catch(Exception e){
					//logger.log(Level.SEVERE, "error: " + e);
				}
				String count = object.optString("count").trim();
				String createdBy = object.optString("createdBy").trim();
				String markInAddress="",markOutAddress="";
				try{
					markInAddress=object.optString("markInAddress").trim();
				}catch(Exception e){
					//e.printStackTrace();
					markInAddress="";
				}
				try{
					markOutAddress=object.optString("markOutAddress").trim();
				}catch(Exception e){
					//e.printStackTrace();
					markOutAddress="";
				}
				
				/*
				 * Name: Apeksha Gunjal Date: 16/07/2018 Note: Add Approver
				 * name.
				 */
				String approverName = "";
				try {
					if (status.trim().equalsIgnoreCase("Approved")) {
						approverName = object.optString("approverName").trim();
					}
				} catch (Exception e) {
					e.printStackTrace();
					//logger.log(Level.SEVERE, "Error: " + e);
				}
				String updatedEmpRole = "";
				try {
					updatedEmpRole = object.optString("updatedEmployeeRole").trim();
				} catch (Exception e) {
					//e.printStackTrace();
					//logger.log(Level.SEVERE, "Error: " + e);
				}
				String updatedEmpName = "";
				try {
					updatedEmpName = object.optString("updatedEmployeeName").trim();
				} catch (Exception e) {
					//e.printStackTrace();
					//logger.log(Level.SEVERE, "Error: " + e);
				}
			
				String attendanceDate = "", overtimeType = "", inTime = "", outTime = "", empId = "", actionLabel = "", shift = "", month = "", targetInTime = "", targetOutTime = "";
				try {
					attendanceDate = object.optString("attendanceDate").trim();
					overtimeType = object.optString("overtimeType").trim();
					inTime = object.optString("inTime").trim();
					outTime = object.optString("outTime").trim();
				} catch (Exception e) {
					//logger.log(Level.SEVERE, "Error:attendancedate: " + e);
				}
					
				try {
					empId = object.optString("empId").trim();
					actionLabel = object.optString("actionLabel").trim();
					shift = object.optString("shift").trim();
				} catch (Exception e) {
					//logger.log(Level.SEVERE, "Error:empId: " + e);
				}
				String projectName = object.optString("projectName").trim();
				loadProjectName = projectName;
				loadShiftName = shift;
				try {
					loadAttendanceDate = sdf.parse(attendanceDate);
				} catch (ParseException e2) {
					// TODO Auto-generated catch block
					//e2.printStackTrace();
				}
				try {
					month = object.optString("month").trim();
					targetInTime = object.optString("targetInTime").trim();
					targetOutTime = object.optString("targetOutTime")
							.trim();
				} catch (Exception e) {
					//logger.log(Level.SEVERE, "Error:month: " + e);
				}
				
				/**
				 * @author Anil @since 25-01-2021
				 * commented below code not found usefull
				 */
//				if(projectName!=null && !projectName.equals("")) {
//				HrProject hrproject=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", projectName).first().now();
//					if(hrproject!=null) {
//						hrproject.setSynchedBy(createdBy);
//						ofy().save().entities(hrproject);
//					}
//				}
				
				
				String otStatus = object.optString("OTStatus").trim();
				String requestedHours = object.optString("requestedHours").trim();
				String supervisorName = object.optString("supervisorName").trim();
					
				double calWorkingHours = 0.0;
				EmployeeInfo empInfo = null;
				Overtime otObj = null;
				int empCount = 0;
				try {
					//logger.log(Level.SEVERE, ": companyId: " + companyId+" empCount: " + empId);
					empCount = Integer.parseInt(empId);
					//logger.log(Level.SEVERE, ": companyId: " + companyId+" empCount: " + empCount);
					empInfo =taskQueue.getEmployeeInfo(empCount, empInfoList);
					
					//logger.log(Level.SEVERE,": empInfo: " + empInfo.getBranch());

					com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal = null;
					if (empInfo != null) {
						//logger.log(Level.SEVERE,": empInfo: " + empInfo.getLeaveCalendar());
						if (!empInfo.getLeaveCalendar().equals("")) {
						//	logger.log(Level.SEVERE, ": empInfo:wrkng "+ empInfo.getLeaveCalendar().getWorkingHours());
							calWorkingHours = empInfo.getLeaveCalendar().getWorkingHours();
							System.out.println("Working hours :"+ calWorkingHours);
							cal = empInfo.getLeaveCalendar();
						}
					}
				} catch (Exception e) {
					//logger.log(Level.SEVERE, "Error: empInfo: " + e);
				}
				/***03-01-2020 Deepak Salve added this code for clear Data***/
				ObjectifyService.reset();
				ofy().consistency(Consistency.STRONG);
				ofy().clear();
				/***End***/
				logger.log(Level.SEVERE, "CompnayId: " + companyId);
				logger.log(Level.SEVERE, "attendanceDate: " + attendanceDate);
				logger.log(Level.SEVERE, "EmployeeId: " + empCount);
				logger.log(Level.SEVERE, "Project Name: " + projectName);

				try{
					attendace = ofy().load().type(Attendance.class)
							.filter("companyId", companyId)
							.filter("attendanceDate", sdf.parse(attendanceDate))
							.filter("empId", empCount)//by Apeksha Gunjal on 14/08/2018 15r:58:-change project name to empId..wrong filter
							.filter("projectName", projectName).first().now();
					//logger.log(Level.SEVERE, "attendance: " + attendace);
				}catch (Exception e) {
					//e.printStackTrace();
				//	logger.log(Level.SEVERE, "Error: " + e);
				}
				if (attendace == null) {
					attendace = new Attendance();
					//by Apeksha Gunjal... commented as told by Komal
					attendace.setCount(0);
				}
				attendace.setUpdatedEmployeeName(updatedEmpName);
				attendace.setUpdatedEmployeeRole(updatedEmpRole);
				//logger.log(Level.SEVERE,"empcount to get leave balance="+empCount);
				//logger.log(Level.SEVERE,"empLeaveBalList size="+empLeaveBalList.size());
				LeaveBalance leaveBalance =  taskQueue.getLeaveBalance(empCount, empLeaveBalList);
				
					
				try {
					if (attendace.getStatus().trim().equalsIgnoreCase("Approved")) {
						isAttendanceAlreadyApproved = true;
					}else{
						long id = attendace.getId();
						int attCount = attendace.getCount();
						String attendanceInAddress=attendace.getMarkInAddress();
						logger.log(Level.SEVERE, "Attendance MarkinAddress : "+attendace.getMarkInAddress());
						attendace = new Attendance();
						attendace.setMarkInAddress(attendanceInAddress);
						attendace.setCount(attCount);
						attendace.setId(id);
					}
				} catch (Exception e) {
					//e.printStackTrace();
					//logger.log(Level.SEVERE, "Error: " + e);
				}
				
				Date date1 = null;
				Date date2 = null;
				/*
				 * Name: Apeksha Gunjal
				 * Date: 15/08/2018 @ 11:30
				 * Note: Handling following conditions:
				 * 		1.If manager and supervisor send attendance at same time then save manager's attendance
				 * 		2.If supervisor send latest in time out time then save it, and vice versa for manager
				 * 		for avoiding unnecessary overriding doing this changes.
				 */
				SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
				try {
					if (inTime != null && !inTime.equals("")) {
						//InTime is present...
						Date serverInTime = null;
						try{
							serverInTime = serverTimeSdf.parse(object.optString("serverInTime").trim());
						}catch(Exception e){
							//logger.log(Level.SEVERE, "Error: intime : " + e);
						}
						if(serverInTime != null){
							try{
								if(attendace.getServerInTime() != null){
									Date attServerInTime = serverTimeSdf.parse(attendace.getServerInTime().trim());
									if(serverInTime.after(attServerInTime)){
										attendace.setServerInTime(serverTimeSdf.format(serverInTime));
										date1 = format.parse(inTime);
										attendace.setInTime(date1);
									}else if (serverInTime == attServerInTime){
										boolean isManager = object.optBoolean("isManager");
										if(isManager){
											attendace.setServerInTime(serverTimeSdf.format(serverInTime));
											date1 = format.parse(inTime);
											attendace.setInTime(date1);
										}//if supervisor n manager mark attendance at same time then keep manager's attendance
									}//server in time is before then don't set fields
								}else{
									attendace.setServerInTime(serverTimeSdf.format(serverInTime));
									date1 = format.parse(inTime);
									attendace.setInTime(date1);
								}
							}catch(Exception e){
								//logger.log(Level.SEVERE, "Error: intime : " + e);
								//setting server in time
								try{
									attendace.setServerInTime(serverTimeSdf.format(serverInTime));
									date1 = format.parse(inTime);
									attendace.setInTime(date1);
								}catch(Exception e1){
									//logger.log(Level.SEVERE, "Error: intime : " + e1);
								}
							}
						}else{
							try {
								date1 = format.parse(inTime);
								attendace.setInTime(date1);
								attendace.setServerInTime(serverTimeSdf.format(new Date()));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								//e.printStackTrace();
							}
						}
						
					}
				}catch(Exception e){
					//logger.log(Level.SEVERE, "Error: intime : " + e);
				}
				
				try{
					//logger.log(Level.SEVERE, "outTime: " + outTime);
					if (outTime != null && !outTime.equals("")) {

						//InTime is present...
						Date serverOutTime = null;
						try{
							serverOutTime = serverTimeSdf.parse(object.optString("serverOutTime").trim());
						}catch(Exception e){
							//logger.log(Level.SEVERE, "Error: outtime : " + e);
						}
						if(serverOutTime != null){
							try{
								if(attendace.getServerOutTime() != null){
									Date attServerOutTime = serverTimeSdf.parse(attendace.getServerOutTime().trim());
									if(serverOutTime.after(attServerOutTime)){
										attendace.setServerOutTime(serverTimeSdf.format(serverOutTime));
										date2 = format.parse(outTime);
										attendace.setOutTime(date2);
									}else if (serverOutTime == attServerOutTime){
										boolean isManager = object.optBoolean("isManager");
										if(isManager){
											attendace.setServerOutTime(serverTimeSdf.format(serverOutTime));
											date2 = format.parse(outTime);
											attendace.setOutTime(date2);
										}//if supervisor n manager mark attendance at same time then keep manager's attendance
									}//server out time is before then don't set fields
								}else{
									attendace.setServerOutTime(serverTimeSdf.format(serverOutTime));
									date2 = format.parse(outTime);
									attendace.setOutTime(date2);
								}
							}catch(Exception e){
								//logger.log(Level.SEVERE, "Error: outtime : " + e);
								//setting server in time
								try{
									attendace.setServerOutTime(serverTimeSdf.format(serverOutTime));
									date2 = format.parse(outTime);
									attendace.setOutTime(date2);
								}catch(Exception e1){
									//logger.log(Level.SEVERE, "Error: outtime : " + e1);
								}
							}
						}else{
							try {
								date2 = format.parse(outTime);
								attendace.setOutTime(date2);
								attendace.setServerOutTime(serverTimeSdf.format(new Date()));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								//e.printStackTrace();
							}
						}
						
					}
				} catch (Exception e) {
					//logger.log(Level.SEVERE, "Error: outime: " + e);
				}
				
				/*
				 * End 15/08/2018 changes by Apeksha Gunjal.
				 */
				HrProject project = new HrProject();
				long difference = 0;
				double hours = 0.0;
//					try {
					if (inTime != null && !inTime.equals("")
							&& outTime != null && !outTime.equals("")) {
						
						try {
							if(date1 == null)
								date1 = format.parse(inTime);
							if(date2 == null)
								date2 = format.parse(outTime);
							difference = date2.getTime() - date1.getTime();
							hours = Math.round((difference / (60 * 60.0 * 1000)) * 100) / 100.0;

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							//logger.log(Level.SEVERE , "Error difference : ");
							//e.printStackTrace();
						}
						
						/** date 22.8.2018 added by komal**/
						ArrayList<Overtime> otList = new ArrayList<Overtime>();// outtime
						for(HrProject proj : projectList){
							if(projectName.equals(proj.getProjectName())){
								project = proj;
								
								/**
								 * @author Vijay Date :- 08-09-2022
								 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
								 */
								ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
								CommonServiceImpl commonservice = new CommonServiceImpl();
								overtimelist = commonservice.getHRProjectOvertimelist(project);
								/**
								 * ends here
								 */
//								for(Overtime ot : proj.getOtList()){
								for(Overtime ot : overtimelist){
									otList.add(ot);
								}
							}
							
						}/** end komal**/
						
						/** date 29.10.2018 added by komal**/
						Overtime ot = null;
						if(actionLabel.equals(AppConstants.H)){
							ot=getOvertimeName(otList,"H", empCount,null);
						}else if(actionLabel.equalsIgnoreCase(AppConstants.WO)){
							ot=getOvertimeName(otList,"WO", empCount,null);
						}else{
							ot=getOvertimeName(otList,"N", empCount,null);
						}
						if(ot != null && ot.getName() != null && !ot.getName().equals("")){
							overtimeType = ot.getName();
						}
					}

					attendace.setWorkingHours(calWorkingHours);
					attendace.setTotalWorkedHours(hours);
					if(createdBy!=null && !createdBy.equals("")) {
						attendace.setCreatedBy(createdBy);
					}
					attendace.setActionLabel(actionLabel);

					/*
					 * Name: Apeksha Gunjal Date: 15/07/2018 Note: Send status
					 * from Android app. Created for Supervisor / other and
					 * Approved for Manager & admin
					 */
					//logger.log(Level.SEVERE, "Status: " + status);
					attendace.setStatus(status);// ConcreteBusinessProcess.APPROVED
					attendace.setProjectName(projectName);
					try{
					/**
					 * Rahul Verma added on 05th Oct 2018
					 */
						
					/***02-01-2020 Deepak Salve added this code for set MarkIn & Markout address***/	
					if(markInAddress!=null&&!markInAddress.trim().equals("")){	
					attendace.setMarkInAddress(markInAddress);
					logger.log(Level.SEVERE, "Inside Mark In Address ");
					}
					if(markOutAddress!=null&&!markOutAddress.trim().equals("")){
					attendace.setMarkOutAddress(markOutAddress);
					logger.log(Level.SEVERE, "Inside Mark Out Address ");
					}
					/***End***/
					
					/**
					 * Ends
					 */
					}catch(Exception e){
						e.printStackTrace();
					}
					try {
						attendace.setAttendanceDate(sdf.parse(attendanceDate));
						/*
						 * Name: Apeksha Gunjal Date: 15/07/2018 Note: Set
						 * approval date when status is approved
						 */
						if (status.trim().equalsIgnoreCase("Approved")) {
							attendace.setApprovalDate(new Date());
						}
						attendace.setApproverName(approverName);
						attendace.setCreationDate(format.parse(format.format(new Date())));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						//logger.log(Level.SEVERE, "Error: " + e);
					}
//					try{
						attendace.setEmpId(Integer.parseInt(empId));
						attendace.setEmployeeName(empInfo.getFullName());
						attendace.setEmpDesignation(empInfo.getDesignation());
						attendace.setBranch(empInfo.getBranch());
						attendace.setMonth(month);
						attendace.setCompanyId(companyId);
						attendace.setShift(shift);
					//	attendace.setActionLabel(actionLabel);
//					} catch (Exception e) {
//						logger.log(Level.SEVERE, "Error: data setting: " + e);
//					}
					//logger.log(Level.SEVERE, "Shift Name" + shift);
						
					/**
					 * @author Anil @since 25-01-2021
					 * loading shift only when both in time and out time is present
					 * for calculating grace period if any given
					 */
					Shift shiftObject = null;
//					try{
						if (!shift.equalsIgnoreCase("")&&bothInAndOutTimePresentFlag) {
							shiftObject	 = ofy().load().type(Shift.class).filter("companyId", companyId).filter("shiftName", shift).first().now();
						}
//					} catch (Exception e) {
//						logger.log(Level.SEVERE, "Error: shiftObject: " + e);
//					}
					//	logger.log(Level.SEVERE, "Shift " + shiftObject);
					double intimeGrace =0 , outtimeGrace = 0, halfHours =0;
//					try{
						if(shiftObject!=null){
							intimeGrace = shiftObject.getIntimeGracePeriod() / 60;//hours
							outtimeGrace = shiftObject.getOuttimeGracePeriod() / 60;//hours
						}
//					} catch (Exception e) {
//						logger.log(Level.SEVERE, "Error: grace time: " + e);
//					}
					/** date 22.3.2019 added by komal to mark wekly off , holiday or absent when intime or outtime is null**/
				//	logger.log(Level.SEVERE , "action label :" +actionLabel);
					if(status.trim().equalsIgnoreCase(AppConstants.APPROVED) && (actionLabel.equalsIgnoreCase("") || actionLabel == null)){
					//	logger.log(Level.SEVERE , "action label :" +actionLabel);
						if (inTime == null || inTime.equals("")
								|| outTime == null || outTime.equals("")) {
					//		logger.log(Level.SEVERE , "action label :" +actionLabel);
							
						if(!actionLabel.equals(AppConstants.A) && !actionLabel.equals(AppConstants.H) &&
								!actionLabel.equals(AppConstants.WO) && !actionLabel.equals(AppConstants.P) && 
								!actionLabel.equals(AppConstants.HF)){
								logger.log(Level.SEVERE, "Intime or outtime null...direct absent.");
								try {
									boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(sdf.parse(attendanceDate));
									boolean isWeeklyOff = false;
									if(isHoliday){
										actionLabel = AppConstants.H;
									}else if(!isHoliday){
										isWeeklyOff = empInfo.getLeaveCalendar().isWeekleyOff(sdf.parse(attendanceDate));
										if(isWeeklyOff){
											actionLabel = AppConstants.WO;
										}else{
											actionLabel = AppConstants.A;
										}
									}
							      
								} catch (ParseException e) {
									// TODO Auto-generated catch block
								//	logger.log(Level.SEVERE, "Error : intime or outtime null...direct absent.");
								//	e.printStackTrace();
							}
						}
					}
						
					}
					attendace.setActionLabel(actionLabel);
				//	logger.log(Level.SEVERE , "action label :" +actionLabel);
						if (actionLabel.equalsIgnoreCase(AppConstants.WO)) {
							// checkWeekOff(cal);
							attendace.setTotalWorkedHours(0.0);
							attendace.setWorkedHours(0.0);
						//	attendace.setLeaveHours(calWorkingHours);
							attendace.setLeave(false);
						//	attendace.setLeaveType(AppConstants.WO);
							attendace.setWeeklyOff(true);
						} else if (actionLabel.equalsIgnoreCase(AppConstants.HF)) {
							// checkWeekOff(cal);
							attendace.setTotalWorkedHours(calWorkingHours / 2.0);
							attendace.setWorkedHours(calWorkingHours / 2.0);
							attendace.setLeaveHours(calWorkingHours / 2.0);
							attendace.setLeave(true);
							//attendace.setLeaveType("PL");
							AllocatedLeaves leaves=null;
							leaves=taskQueue.getLeaveDetails("Paid Leave", 0.5,leaveBalance);
							// attendace.setHalfDay(true);
							if(leaves!=null){
								attendace.setLeaveType(leaves.getName());
							}else{
								leaves=taskQueue.getLeaveDetails("Unpaid Leave", 0.5,leaveBalance);
								attendace.setLeaveType(leaves.getName());
							}
							// }

						}else if(actionLabel.equalsIgnoreCase(AppConstants.H) ) {
							
							String holidayName="";
							for(Holiday holiday:empInfo.getLeaveCalendar().getHoliday()){
								if(holiday.getHolidaydate().equals(attendace.getAttendanceDate())){
									holidayName=holiday.getHolidayname();
									break;
								}
							}
							
							attendace.setTotalWorkedHours(0.0);
							attendace.setWorkedHours(0.0);
							attendace.setLeaveHours(calWorkingHours);
							attendace.setLeave(true);
							attendace.setLeaveType(holidayName);
							attendace.setHoliday(true);
							attendace.setActionLabel(AppConstants.H);
						}
						/** date 02.01.2018 added by komal for actgionLabel A **/
						else if (actionLabel.equalsIgnoreCase(AppConstants.A)) {
							//logger.log(Level.SEVERE ,"in actionLabel.equalsIgnoreCase(AppConstants.A)");
							attendace.setTotalWorkedHours(0.0);
							attendace.setWorkedHours(0.0);
							attendace.setLeaveHours(calWorkingHours);
							attendace.setLeave(true);
							//attendace.setLeaveType("PL");
							AllocatedLeaves leaves=null;
							leaves=taskQueue.getLeaveDetails("Paid Leave",1.0,leaveBalance);
							// attendace.setHalfDay(true);
							if(leaves!=null){
								attendace.setLeaveType(leaves.getName());
							}else{
								leaves=taskQueue.getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
								attendace.setLeaveType(leaves.getName());
							}
						}


					
					Double ot = 0.0;
					Double hf = 0.0;
					/** date 21.8.2018 added by komal **/
					if (inTime != null && !inTime.equals("")
							&& outTime != null && !outTime.equals("")) {

					try {
						if (hours > 0) {
							if (hours >= calWorkingHours ) {// - outtimeGrace
								ot = hours - calWorkingHours;
								// projectHoursList.add(hours);
								// for(String s : set){
								attendace.setPresent(true);
								attendace.setTotalWorkedHours(hours);
								attendace.setWorkedHours(calWorkingHours);
								attendace.setActionLabel(AppConstants.P);
								//logger.log(Level.SEVERE, "isPresent: " + true+ " totalWorkedHours: " + hours+ " calWorkedHours: " + calWorkingHours);
							//need to check if ot is assigned to employee in project
								
								//Ashwini Patil Date:16-05-2021 Fetching employee OT from Project
								ArrayList<Overtime>otlist=new ArrayList<Overtime>();
								int employeeId=Integer.parseInt(empId);
								
								/**
								 * @author Vijay Date :- 08-09-2022
								 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
								 */
								ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
								CommonServiceImpl commonservice = new CommonServiceImpl();
								overtimelist = commonservice.getHRProjectOvertimelist(project);
								/**
								 * ends here
								 */
								if(project!=null&&overtimelist!=null){
									logger.log(Level.SEVERE,"in if(project!=null&&project.getOtList()!=null");
									for(Overtime o:overtimelist){
										if(o.getEmpId()==employeeId){
											otlist.add(o);
										}
									}
								}
								logger.log(Level.SEVERE,"otlist size="+otlist.size());
								if(otlist.size()>0) {
									for(Overtime o:otlist) {
										if(!o.getName().equals("")) {	
											logger.log(Level.SEVERE,"OT name from project="+o.getName()+" OvertimeType from request="+overtimeType);
											if (ot != 0 && o.getName().equals(overtimeType)) {
												double otValue = getOTHours(ot , project.getMiOtMinutes());
												logger.log(Level.SEVERE, "ot value :" + otValue);
												if(otValue > 0){
													attendace.setApprovedOTHOURS(Double.parseDouble(requestedHours));
													attendace.setOvertime(true);
													attendace.setOvertimeHours(otValue);
													attendace.setOtStatus(otStatus);
													attendace.setProjectNameForOT(attendace.getProjectName());
													attendace.setOvertimeType(overtimeType);
												}
											}
										}
									}
								}
								//below if block commented by ashwini patil 
//								if(!overtimeType.equals("")){
//								if (ot != 0) {
//								//	double otValue = Double.parseDouble(requestedHours);
//									double otValue = getOTHours(ot , project.getMiOtMinutes());
////									if(otStatus.equalsIgnoreCase("Approved")){							
////										if(ot > otValue){
////											ot = otValue;
////										}
//									logger.log(Level.SEVERE, "ot value :" + otValue);
//									if(otValue > 0){
//										attendace.setApprovedOTHOURS(Double.parseDouble(requestedHours));
//										attendace.setOvertime(true);
//										attendace.setOvertimeHours(otValue);
//										attendace.setOtStatus(otStatus);
//										attendace.setProjectNameForOT(attendace.getProjectName());
//										attendace.setOvertimeType(overtimeType);
//									}							
//									
//									
//								}
//									
//								}
							} else if (hours < calWorkingHours) {
								/** date 9.1.2019 added by komal to save data accounding to half da rules**/
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "EnableStandardAttendance",companyId)){
								//	logger.log(Level.SEVERE, "shift =="+shift);
								//	logger.log(Level.SEVERE,"hours ============"+hours);
									double hoursWithGraceCalculation = 0;
									if(shift!=null){
										hoursWithGraceCalculation = calWorkingHours - (intimeGrace + outtimeGrace);
									}
								//	logger.log(Level.SEVERE,"calWorkingHours =="+ calWorkingHours);
								//	logger.log(Level.SEVERE,"hours =="+hoursWithGraceCalculation);
									if(hours >= hoursWithGraceCalculation){
										attendace.setPresent(true);
										attendace.setTotalWorkedHours(hoursWithGraceCalculation);
										attendace.setWorkedHours(hoursWithGraceCalculation);
										attendace.setActionLabel(AppConstants.P);	
									}
									else{
										double halfDayHours = hoursWithGraceCalculation/2;
								//		logger.log(Level.SEVERE,"hours =="+hours);
								//		logger.log(Level.SEVERE,"halfDayHours =="+halfDayHours);
										if(hours>=halfDayHours){
											attendace.setTotalWorkedHours(hours);
											attendace.setWorkedHours(hours);
											AllocatedLeaves leaves=null;
										
											leaves=taskQueue.getLeaveDetails("Paid Leave",halfDayHours/calWorkingHours,leaveBalance);
											// attendace.setHalfDay(true);
											if(leaves!=null){
												attendace.setLeaveType(leaves.getName());
											}else{
												leaves=taskQueue.getLeaveDetails("Unpaid Leave",halfDayHours/calWorkingHours,leaveBalance);
												attendace.setLeaveType(leaves.getName());
											}
											attendace.setLeaveHours(calWorkingHours/2);
											attendace.setLeave(true);
											
											attendace.setActionLabel(AppConstants.HF);
											attendace.setPresent(true);
											
										}else{
											AllocatedLeaves leaves=null;
											leaves=taskQueue.getLeaveDetails("Paid Leave",1.0,leaveBalance);
											if(leaves!=null){
												attendace.setLeaveType(leaves.getName());
											}else{
												leaves=taskQueue.getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
												attendace.setLeaveType(leaves.getName());
											}
											
											attendace.setTotalWorkedHours(0.0);
											attendace.setWorkedHours(0.0);
											attendace.setLeaveHours(calWorkingHours);
											attendace.setLeave(true);
											attendace.setActionLabel(AppConstants.A);
										}
									}
								}else{
								hf = calWorkingHours - hours;
								halfHours = calWorkingHours / 2;
								if(hours < halfHours) { //- intimeGrace
									attendace.setActionLabel(AppConstants.A);
									attendace.setLeave(true);
									//attendace.setLeaveType("UnPaid Leave");
									attendace.setLeaveHours(calWorkingHours);
									AllocatedLeaves leaves=null;
									leaves=taskQueue.getLeaveDetails("Paid Leave",1.0,leaveBalance);
									// attendace.setHalfDay(true);
									if(leaves!=null){
										attendace.setLeaveType(leaves.getName());
									}else{
										leaves=taskQueue.getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
										attendace.setLeaveType(leaves.getName());
									}

									
								}else{
								attendace.setTotalWorkedHours(hours);
								attendace.setWorkedHours(hours);
								// attendace.setHalfDay(true);
								attendace.setLeaveHours(hf);
								attendace.setLeave(true);
							//	attendace.setLeaveType("PL");

								attendace.setActionLabel(AppConstants.HF);
								attendace.setPresent(true);
								AllocatedLeaves leaves=null;
								leaves=taskQueue.getLeaveDetails("Paid Leave",hf/calWorkingHours,leaveBalance);
								// attendace.setHalfDay(true);
								if(leaves!=null){
									attendace.setLeaveType(leaves.getName());
								}else{
									leaves=taskQueue.getLeaveDetails("Unpaid Leave",hf/calWorkingHours,leaveBalance);
									attendace.setLeaveType(leaves.getName());
								}

								}
							}
							}
						}else
						/**
						 * Updated By: Viraj
						 * Date: 27-02-2019
						 * Description: To set attendence when in-time and out-time is same
						 */
						if(hours == 0) {
							attendace.setActionLabel(AppConstants.A);
							attendace.setLeave(true);
							attendace.setLeaveHours(calWorkingHours);
							//attendace.setLeaveType("UnPaid Leave");
							AllocatedLeaves leaves=null;
							leaves=taskQueue.getLeaveDetails("Paid Leave",1.0,leaveBalance);
							// attendace.setHalfDay(true);
							if(leaves!=null){
								attendace.setLeaveType(leaves.getName());
							}else{
								leaves=taskQueue.getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
								attendace.setLeaveType(leaves.getName());
							}
						}
						/** Ends **/

					} catch (Exception e) {
						//logger.log(Level.SEVERE, "Error: hours: " + e);
					}
					
						if (!shift.equalsIgnoreCase("")) {
							
							
							if (!targetInTime.equalsIgnoreCase("")) {
								try {
									Date targetInTime1 = timeFormat.parse(targetInTime);
									attendace.setTargetInTime(targetInTime1);
									Date inTime1 = null;
									if (date1 != null) {
										inTime1 = timeFormat.parse(timeFormat.format(date1));
										if (inTime1.after(targetInTime1)) {
											attendace.setLateMarkInTime(true);
										}else{
											attendace.setLateMarkInTime(false);
										}
										if (inTime1.before(targetInTime1)) {
											attendace.setEarlyMarkInTime(true);
										}else{
											attendace.setEarlyMarkInTime(false);
										}
									}

								} catch (ParseException e) {
									// Invalid date was entered
									//logger.log(Level.SEVERE,"updateAttendance error: " + e);
								}
							}
							if (!targetOutTime.equalsIgnoreCase("")) {
								try {
									Date targetOutTime1 = timeFormat.parse(targetOutTime);
									attendace.setTargetOutTime(targetOutTime1);
									Date outTime1 = null;
									if (date2 != null) {
										outTime1 = timeFormat.parse(timeFormat.format(date2));
										if (outTime1.after(targetOutTime1)) {
											attendace.setLateMarkOutTime(true);
										}else{
											attendace.setLateMarkOutTime(false);
										}
										if (outTime1.before(targetOutTime1)) {
											attendace.setEarlyMarkOutTime(true);
										}else{
											attendace.setEarlyMarkOutTime(false);
										}
									}

								} catch (ParseException e) {
									// Invalid date was entered
									//logger.log(Level.SEVERE,
									//		"updateAttendance error: " + e);
								}
							}
						}
					}
//					} catch (Exception e) {
//						logger.log(Level.SEVERE, "Error: shift: " + e);
//					}
					/*
					 * Name: Apeksha Gunjal Date: 16/07/2018 Note: If attendance
					 * is already approved then no one can changed in it.
					 */
//					try {
						if (!isAttendanceAlreadyApproved) {
							attArrayList.add(attendace);
						} else {
							//logger.log(Level.SEVERE,"AttendanceAlreadyApproved that's why not added to list.."+ empId);
						}
						
			}catch(Exception e) {
				e.printStackTrace();
			}
			}
			Map<String , Attendance> map = new HashMap<String , Attendance>();
			for(Attendance att : attArrayList){
					map.put(att.getProjectName()+"-"+att.getEmpId(), att);
			}
			ArrayList<Attendance> attList = new ArrayList<Attendance>();
			for(Map.Entry<String, Attendance> map1:map.entrySet()){
				attList.add(map1.getValue());
			}
			
			String str = dbUpdate(attList, companyId);
//		} catch (Exception e) {
//			logger.log(Level.SEVERE, "updateAttendance error: " + e);
		//	logger.log(Level.SEVERE, "updateAttendance attendance data: " + attList.toString());
//			return "Error : " + e;
//		}
			if(updatedEmployee.equals("AutoSubmit")){				
				return str;
			}else{
				Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				LoadAttendanceFromEmployee load = new LoadAttendanceFromEmployee();
				JSONObject json = load.getAttendanceString(loadProjectName, loadAttendanceDate, loadShiftName, company,"", "");
				String jsonString=json.toString().replaceAll("\\\\", "");
				String res="";
				if(bothInAndOutTimePresentFlag) {
					logger.log(Level.SEVERE,"before calling createAttendanceInZoho");
					Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
					if(comp!=null&&comp.getZohoClientID()!=null&&!comp.getZohoClientID().equals("")&&comp.getZohoClientSecret()!=null&&!comp.getZohoClientSecret().equals("")&&comp.getZohoPermissionsCode()!=null&&!comp.getZohoPermissionsCode().equals("")&&comp.getZohoOrganisationID()!=null&&!comp.getZohoOrganisationID().equals("")) {
						logger.log(Level.SEVERE,"zoho credentials present");
						
						IntegrateSyncServiceImpl sync=new IntegrateSyncServiceImpl();
						for (int i = 0; i < jsonArray.length(); i++) {
							try {
							JSONObject object = jsonArray.getJSONObject(i);
							String inTime = object.optString("inTime").trim()+":00";
							String outTime = object.optString("outTime").trim()+":00";
							String empid=object.optString("empId").trim();
							res=sync.createAttendanceInZoho(comp.getCompanyId(),inTime,outTime,empid);
							}catch(Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				logger.log(Level.SEVERE,"res="+res);
				
				return jsonString;
			}
	}



	/** date 10.8.2018 added by komal for OT request **/
	public String saveRequestedOTDetails(long companyId , JSONObject object){
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
			String empId = object.optString("empId").trim();
			String projectName = object.optString("projectName").trim();
			String attendanceDate = object.optString("attendanceDate").trim();
			String otStatus = object.optString("OTStatus").trim();
			String requestedHours = object.optString("requestedHours").trim();
			String supervisorName = object.optString("supervisorName").trim();
			Date date = null;
			try {
				date = sdf.parse(attendanceDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int id = Integer.parseInt(empId);
			Attendance attendance = ofy().load().type(Attendance.class)
					.filter("companyId", companyId)
					.filter("attendanceDate", date)
					.filter("empId", id)
					.filter("projectName", projectName).first().now();
			if(attendance == null){
				attendance= new Attendance();
				attendance.setProjectName(projectName);
				attendance.setCompanyId(companyId);
				attendance.setEmpId(id);
				attendance.setAttendanceDate(date);
			}
			attendance.setOtStatus(otStatus);
			attendance.setRequestedOTHours(Double.parseDouble(requestedHours));
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(attendance);
			return "Successful";
		}catch(Exception e){
			logger.log(Level.SEVERE, "RequestOTHours error: "+e);
			return "Failed";
		}
	}
	/** end komal**/
	
	public String dbUpdate(ArrayList<Attendance> attArrayList , long companyId){
		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class)
				.filter("companyId", companyId)
				.filter("processName", "Attendance").filter("status", true)
				.first().now();

		logger.log(Level.SEVERE, "updateAttendance ng: " + ng.getNumber());
		long number = ng.getNumber();
		int count = (int) number;
		ServerAppUtility utility = new ServerAppUtility();
		for(Attendance attendace : attArrayList){
			/**
			 * @author Anil
			 * @since 04-02-2022
			 * If any of one of attendance is not proper then rest all attendance was also not getting properly updated
			 * like if some one has issue with ot or leave
			 * thats why handled exception here
			 */
			try{
				if(attendace.getCount() == 0){
					attendace.setCount(++count);
				}
				/* Name: Apeksha Gunjal 
				 * Date: 15/07/2018 
				 * Note: if status is approved then deduct leave
				 */
				//logger.log(Level.SEVERE, "updateAttendance status: " + attendace.getStatus());
				if(attendace.getStatus().trim().equalsIgnoreCase("Approved")){
					utility.ApproveLeave(attendace);
				}else{
					//Later it will change.
				}
			}catch(Exception e){
				attendace.setStatus("Created");
			}
		}		
		ofy().save().entities(attArrayList).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		System.out
				.println("Data saved successfully=======================================");
		logger.log(Level.SEVERE,
				"Data saved successfully=======================================");
		return "success";
	}
	



	@Override
	public Attendance updateAndOverrideAttendance(Attendance attendance) {
		
		try {
		if(attendance!=null){
			//logger.log(Level.SEVERE, "In updateAndOverrideAttendance attendance label="+attendance.getActionLabel());
			
			/**
			 * @author Anil,Date : 06-02-2018
			 * If attendance is not created then we have to create attendance first then will do the approval part
			 */
			if(attendance.getCount()==0){
				//logger.log(Level.SEVERE, "In if(attendance.getCount()==0)");
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(attendance);
			}
			//logger.log(Level.SEVERE, "leave selected="+attendance.getLeaveType());
			EmployeeLeave empLeave=ofy().load().type(EmployeeLeave.class).filter("companyId",attendance.getCompanyId()).filter("emplId", attendance.getEmpId()).filter("leaveDate",attendance.getAttendanceDate()).first().now();
			EmployeeOvertime overtime=ofy().load().type(EmployeeOvertime.class).filter("companyId",attendance.getCompanyId()).filter("emplId", attendance.getEmpId()).filter("otDate",attendance.getAttendanceDate()).first().now();
			EmployeeInfo empInfo=ofy().load().type(EmployeeInfo.class).filter("companyId", attendance.getCompanyId()).filter("empCount", attendance.getEmpId()).first().now();
			LeaveBalance leaveBalance=null;
			if(empLeave!=null){
				//logger.log(Level.SEVERE, "Employee Leave Not Null...and leave name is  "+empLeave.getName());
				leaveBalance=ofy().load().type(LeaveBalance.class).filter("companyId", attendance.getCompanyId()).filter("empInfo.empCount", attendance.getEmpId()).first().now();
				updateLeaveBalance(leaveBalance,empLeave,empInfo);
			}
			
			if(empLeave!=null){
				ofy().delete().entity(empLeave).now();
			}
			if(overtime!=null){
				logger.log(Level.SEVERE, "Overtime Not Null...");
				ofy().delete().entity(overtime).now();
			}
			
			if(leaveBalance!=null){
				logger.log(Level.SEVERE, "Employee Leave Balance Not Null...");
				ofy().save().entity(leaveBalance).now();
			}
			//logger.log(Level.SEVERE, "overtime hours="+attendance.getOvertimeHours());
			//logger.log(Level.SEVERE, "before saving attendance label="+attendance.getActionLabel());
			ofy().save().entity(attendance).now();
			
			ServerAppUtility utility=new ServerAppUtility();
			utility.ApproveLeave(attendance);
			
			
		}
		
		
		return attendance;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return attendance;
	}
	
	

	@Override
	public ArrayList<EmployeeLeaveBalance> getEmployeeLeaveDetails(long companyId) {
//	public HashMap<Integer, ArrayList<EmployeeLeaveBalance>> getEmployeeLeaveDetails(long companyId) {
		
		ArrayList<String> exceldatalist = new ArrayList<String>();
		try {
			long i;
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {
							i = (long) cell.getNumericCellValue();
							exceldatalist.add(i + "");
						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						break;
					default:
					}
				}
			}
			logger.log(Level.SEVERE,"FileString List Size:" + exceldatalist.size());
			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayList<EmployeeLeaveBalance> list=new ArrayList<EmployeeLeaveBalance>();	
		HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveBalMap=new HashMap<Integer, ArrayList<EmployeeLeaveBalance>>();
		if (exceldatalist.get(0).trim().equalsIgnoreCase("Emp Id")
				&& exceldatalist.get(1).trim().equalsIgnoreCase("Emp Name")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Leave Name")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("Leave Shortname")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("Opening Balance")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("Availed Days")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("Earned Days")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("Balance Days")) {
			
			try {
//				empLeaveBalMap=getEmpLeaveDetails(companyId,exceldatalist);
				list=getEmpLeaveDetails(companyId,exceldatalist);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
//			empLeaveBalMap.put(-1, new ArrayList<EmployeeLeaveBalance>());
			EmployeeLeaveBalance bal=new EmployeeLeaveBalance(-1, "", "", "", 0, 0, 0, 0, 0);
			list.add(bal);
		}
//		return empLeaveBalMap;
		return list;
	}
	
	
	private ArrayList<EmployeeLeaveBalance>getEmpLeaveDetails(long companyId,ArrayList<String> exceldatalist) {
//	private HashMap<Integer, ArrayList<EmployeeLeaveBalance>> getEmpLeaveDetails(long companyId,ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		
		HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveBalMap=new HashMap<Integer, ArrayList<EmployeeLeaveBalance>>();
		ArrayList<EmployeeLeaveBalance> empLeaveBalanceList=new ArrayList<EmployeeLeaveBalance>();
		for(int i=8;i<exceldatalist.size();i+=8){
			int empId = 0;
			String empName="";
			String leaveName="";
			String leaveShortName="";
			double openingBalance=0;
			double availedDays=0;
			double earnedDays=0;
			double balanceDays=0;
			
			if(!exceldatalist.get(i).equalsIgnoreCase("NA")){
				empId=Integer.parseInt(exceldatalist.get(i).trim());
			}
			if(!exceldatalist.get(i+1).equalsIgnoreCase("NA")){
				empName=exceldatalist.get(i+1).trim();
			}
			if(!exceldatalist.get(i+2).equalsIgnoreCase("NA")){
				leaveName=exceldatalist.get(i+2).trim();
			}
			if(!exceldatalist.get(i+3).equalsIgnoreCase("NA")){
				leaveShortName=exceldatalist.get(i+3).trim();
			}
			if(!exceldatalist.get(i+4).equalsIgnoreCase("NA")){
				openingBalance=Double.parseDouble(exceldatalist.get(i+4).trim());
			}
			if(!exceldatalist.get(i+5).equalsIgnoreCase("NA")){
				availedDays=Double.parseDouble(exceldatalist.get(i+5).trim());
			}
			if(!exceldatalist.get(i+6).equalsIgnoreCase("NA")){
				earnedDays=Double.parseDouble(exceldatalist.get(i+6).trim());
			}
			if(!exceldatalist.get(i+7).equalsIgnoreCase("NA")){
				balanceDays=Double.parseDouble(exceldatalist.get(i+7).trim());
			}
			
			EmployeeLeaveBalance empLeaveBal=new EmployeeLeaveBalance(empId, empName, leaveName, leaveShortName, openingBalance, availedDays, earnedDays, balanceDays,companyId);
			empLeaveBalanceList.add(empLeaveBal);
		}
		
//		if(empLeaveBalanceList.size()!=0){
//			for(EmployeeLeaveBalance bal:empLeaveBalanceList){
//				if(empLeaveBalMap!=null&&empLeaveBalMap.size()!=0){
//					if(empLeaveBalMap.get(bal.getEmpId())!=null){
//						empLeaveBalMap.get(bal.getEmpId()).add(bal);
//					}else{
//						ArrayList<EmployeeLeaveBalance>balList=new ArrayList<EmployeeLeaveBalance>();
//						balList.add(bal);
//						empLeaveBalMap.put(bal.getEmpId(), balList);
//					}
//					
//				}else{
//					ArrayList<EmployeeLeaveBalance>balList=new ArrayList<EmployeeLeaveBalance>();
//					balList.add(bal);
//					empLeaveBalMap.put(bal.getEmpId(), balList);
//				}
//			}
//		}
//		
//		logger.log(Level.SEVERE, "Map size : "+empLeaveBalMap.size());
		
//		return empLeaveBalMap;
		return empLeaveBalanceList;
		
	}

	public HashMap<Integer, ArrayList<EmployeeLeaveBalance>> convertListToMap(ArrayList<EmployeeLeaveBalance> empLeaveBalanceList){
		HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveBalMap=new HashMap<Integer, ArrayList<EmployeeLeaveBalance>>();
		if(empLeaveBalanceList.size()!=0){
			for(EmployeeLeaveBalance bal:empLeaveBalanceList){
				if(empLeaveBalMap!=null&&empLeaveBalMap.size()!=0){
					if(empLeaveBalMap.get(bal.getEmpId())!=null){
						empLeaveBalMap.get(bal.getEmpId()).add(bal);
					}else{
						ArrayList<EmployeeLeaveBalance>balList=new ArrayList<EmployeeLeaveBalance>();
						balList.add(bal);
						empLeaveBalMap.put(bal.getEmpId(), balList);
					}
					
				}else{
					ArrayList<EmployeeLeaveBalance>balList=new ArrayList<EmployeeLeaveBalance>();
					balList.add(bal);
					empLeaveBalMap.put(bal.getEmpId(), balList);
				}
			}
		}
		
		logger.log(Level.SEVERE, "Map size : "+empLeaveBalMap.size());
		return empLeaveBalMap;
	}


	@Override
//	public HashMap<Integer, ArrayList<EmployeeLeaveBalance>> validateEmployeeLeaveDetails(
//			HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveMap) {
	public ArrayList<EmployeeLeaveBalance> validateEmployeeLeaveDetails(
			ArrayList<EmployeeLeaveBalance> empLeaveBalanceList) {
		// TODO Auto-generated method stub
		
		HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveMap=convertListToMap(empLeaveBalanceList);
		ArrayList<AttandanceError>errorList=new ArrayList<AttandanceError>();
		ArrayList<EmployeeLeaveBalance> errorBalList=new ArrayList<EmployeeLeaveBalance>();
		long companyId=0;
		if(empLeaveMap!=null&&empLeaveMap.size()!=0){
			
			 Set<Integer> keys = empLeaveMap.keySet();
			 if(keys!=null&&keys.size()!=0){
				 for (Integer empId : keys){ 
					 ArrayList<EmployeeLeaveBalance> empLeaveList = empLeaveMap.get(empId); 
					 companyId=empLeaveList.get(0).getCompanyId();
					 System.out.println("Key = " + empId + ", Value = " + companyId); 
					 break;
			     } 
				 ArrayList<Integer> empIdList=new ArrayList<Integer>(keys);
				 List<LeaveBalance> leaveBalList=new ArrayList<LeaveBalance>();
				 if(empIdList.size()!=0){
					 leaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount IN", empIdList).list();
				 }
				 
				 logger.log(Level.SEVERE, "Validate Map size : "+empLeaveMap.size());
				 logger.log(Level.SEVERE, "Leave Balance : "+leaveBalList.size());
				 
				 for(Map.Entry<Integer, ArrayList<EmployeeLeaveBalance>> map:empLeaveMap.entrySet()){
					 ArrayList<EmployeeLeaveBalance> empLeaveList = map.getValue();
					 LeaveBalance balance=getEmpLeaveBalance(map.getKey(), leaveBalList);
					 
					 if(balance==null){
						 AttandanceError error=new AttandanceError();
						 error.setEmpId(map.getKey());
						 error.setEmpName(empLeaveList.get(0).getEmpName());
						 error.setRemark("Leave Balance not found.");
//						 if(empLeaveMap.get(-1)==null){
//							 empLeaveMap.put(-1, new ArrayList<EmployeeLeaveBalance>());
//						 }
						 if(errorBalList.size()==0){
							 EmployeeLeaveBalance bal=new EmployeeLeaveBalance(-1, "", "", "", 0, 0, 0, 0, 0);
							 errorBalList.add(bal);
						 }
						 errorList.add(error);
					 }else{
						for(EmployeeLeaveBalance empBal:empLeaveList){
							AttandanceError error=new AttandanceError();
							error.setEmpId(map.getKey());
							error.setEmpName(empLeaveList.get(0).getEmpName());
							
							if(!balance.getEmpInfo().getFullName().equals(empBal.getEmpName())){
								error.setRemark("Employee name is not matched. "+empBal.getEmpName());
								if(errorBalList.size()==0){
									EmployeeLeaveBalance bal=new EmployeeLeaveBalance(-1, "", "", "", 0, 0, 0, 0, 0);
									errorBalList.add(bal);
								}
								errorList.add(error);
							}
							
							boolean leaveFoundFlag=false;
							for(AllocatedLeaves leaves:balance.getAllocatedLeaves()){
								if(empBal.getLeaveName().equals(leaves.getName())){
									leaveFoundFlag=true;
								}
							}
							if(!leaveFoundFlag){
								error.setRemark(empBal.getLeaveName() +" is not added in employee leave list.");
//								if(empLeaveMap.get(-1)==null){
//									 empLeaveMap.put(-1, new ArrayList<EmployeeLeaveBalance>());
//								}
								 if(errorBalList.size()==0){
									 EmployeeLeaveBalance bal=new EmployeeLeaveBalance(-1, "", "", "", 0, 0, 0, 0, 0);
									 errorBalList.add(bal);
								 }
								 errorList.add(error);
							}
						}
					 }
				 }
				 
				 
			 }
		}
		logger.log(Level.SEVERE, "WITH EROOR Map size : "+empLeaveMap.size());
		
		if(errorBalList.size()!=0){
			CsvWriter.attendanceErrorList=errorList;
			return errorBalList;
		}else{
			return empLeaveBalanceList;
		}
		
//		return empLeaveMap;
	}



	@Override
//	public String updateLeaveBalance(HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveMap) {
	public String updateLeaveBalance(ArrayList<EmployeeLeaveBalance> empLeaveBalanceList) {
		// TODO Auto-generated method stub
		HashMap<Integer, ArrayList<EmployeeLeaveBalance>> empLeaveMap=convertListToMap(empLeaveBalanceList);
		String msg=null;
		long companyId=0;
		if(empLeaveMap!=null&&empLeaveMap.size()!=0){
			
			logger.log(Level.SEVERE, "MAP : "+empLeaveMap.size());
			 Set<Integer> keys = empLeaveMap.keySet();
			 if(keys!=null&&keys.size()!=0){
				 for (Integer empId : keys){ 
					 ArrayList<EmployeeLeaveBalance> empLeaveList = empLeaveMap.get(empId); 
					 companyId=empLeaveList.get(0).getCompanyId();
					 System.out.println("Key = " + empId + ", Value = " + companyId); 
					 break;
			     } 
				 ArrayList<Integer> empIdList=new ArrayList<Integer>(keys);
				 List<LeaveBalance> leaveBalList=new ArrayList<LeaveBalance>();
				 logger.log(Level.SEVERE, "EMP SIZE : "+empIdList.size());
				 if(empIdList.size()!=0){
					 leaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount IN", empIdList).list();
				 }
				 logger.log(Level.SEVERE, "EMP LEAVE BALANCE LIS SIZE  : "+leaveBalList.size());
				 for(Map.Entry<Integer, ArrayList<EmployeeLeaveBalance>> map:empLeaveMap.entrySet()){
					ArrayList<EmployeeLeaveBalance> empLeaveList = map.getValue();
					LeaveBalance balance=getEmpLeaveBalance(map.getKey(), leaveBalList);
					 
					for(EmployeeLeaveBalance empBal:empLeaveList){
						AttandanceError error=new AttandanceError();
						error.setEmpId(map.getKey());
						error.setEmpName(empLeaveList.get(0).getEmpName());
						for(AllocatedLeaves leaves:balance.getAllocatedLeaves()){
							if(empBal.getLeaveName().equals(leaves.getName())){
								leaves.setShortName(empBal.getLeaveShortName());
								/**
								 * @author Anil , Date : 08-03-2019
								 * As per updated logic ,setting opening and closing leave balance 
								 */
//								leaves.setMaxDays((int) empBal.getOpeningBalance());
								leaves.setAvailedDays(empBal.getAvailedDays());
								leaves.setEarned(empBal.getEarnedDays());
//								leaves.setBalance(empBal.getBalanceDays());
								
								leaves.setBalance(empBal.getBalanceDays());
								leaves.setOpeningBalance(empBal.getOpeningBalance());
								
								/**
								 * End
								 */
								
								balance.setRemark("Updated through upload programme on "+new Date());
							}
						}
					}
				 }
				 
				 if(leaveBalList.size()!=0){
					 ofy().save().entities(leaveBalList).now();
				 }
				 
				 
			 }
		}
		logger.log(Level.SEVERE, "DONE ");
		
		return msg;
	}
	private ArrayList<Integer> technicianWarehouseDetailsSave(long companyId, ArrayList<String> exceldatalist, String entityName) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Hi technician warehouse details save Task queue calling ");
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		ArrayList<String> hsStorageBin = new ArrayList<String>();
		for (int i = 11; i < exceldatalist.size(); i += 11) {
			hsStorageBin.add(exceldatalist.get(i+2).trim());
		}
		ArrayList<String> arrayStorageBin = new ArrayList<String>(hsStorageBin);
		List<Storagebin> storageBinlist = ofy().load().type(Storagebin.class).filter("companyId", companyId)
											.filter("binName IN", arrayStorageBin).list();
		
		String branch = "" , parentWarehouse = "" , parentLocation = "" , parentBin = "";
		for (int i = 11; i < exceldatalist.size(); i += 11) {
			
			if (exceldatalist.get(i+7).equalsIgnoreCase("na")) {
				branch = "";
				integerlist.add(-4);
				return integerlist;
			} else {
				branch = exceldatalist.get(i+7);
				Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", branch).first().now();
				if(branchEntity == null){
					integerlist.add(-4);
					return integerlist;
				}
				
				
			}
			if (exceldatalist.get(i + 8).equalsIgnoreCase("na")) {
				parentWarehouse = "";
				integerlist.add(-5);
				return integerlist;
			} else {
				parentWarehouse = exceldatalist.get(i + 8);
				WareHouse warehouseEntity = ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("buisnessUnitName", parentWarehouse).first().now();
				if(warehouseEntity == null){
					integerlist.add(-5);
					return integerlist;
				}
			}
			if (exceldatalist.get(i + 9).equalsIgnoreCase("na")) {
				parentLocation = "";
				integerlist.add(-6);
				return integerlist;
			} else {
				parentLocation = exceldatalist.get(i + 9);
				StorageLocation storageLocationEntity = ofy().load().type(StorageLocation.class).filter("companyId", companyId).filter("buisnessUnitName", parentLocation).first().now();
				if(storageLocationEntity == null){
					integerlist.add(-6);
					return integerlist;
				}
			}

			if (exceldatalist.get(i + 10).equalsIgnoreCase("na")) {
				integerlist.add(-6);
				return integerlist;
			} else {
				parentBin = exceldatalist.get(i + 10);
				Storagebin storageBinEntity = ofy().load().type(Storagebin.class).filter("companyId", companyId).filter("binName", parentBin).first().now();
				if(storageBinEntity == null){
					integerlist.add(-7);
					return integerlist;
				}
			}
			
			/**
			 * @author Vijay
			 * Des :- added validation duplicate bin should dot create so it will first check storage exist then it will give validation msg storage bin already exist
			 * for new employee creation
			 */
			if(!exceldatalist.get(i + 2).equalsIgnoreCase("na") && (exceldatalist.get(i).equalsIgnoreCase("0") || exceldatalist.get(i).equalsIgnoreCase("0.0") )){

				if(!parentWarehouse.equals("") && !parentLocation.equals("") && !parentBin.equals("")){
					boolean duplicateStorageBinFlag = false;
					for(Storagebin storageBin : storageBinlist){
						if(storageBin.getWarehouseName().trim().equals(parentWarehouse.trim())
								&& storageBin.getStoragelocation().trim().equals(parentLocation.trim()) 
								&& storageBin.getBinName().trim().equals(exceldatalist.get(i + 2).trim())){
							duplicateStorageBinFlag = true;
							break;
						}
					}
					if(duplicateStorageBinFlag){
						integerlist.add(-8);
						return integerlist;
					}
				}
			}
			
			break;
		}
		
		Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentUploadTaskQueue").param("EntityName", entityName).param("companyId", companyId+""));
		
		logger.log(Level.SEVERE,"Hi Task queue cazlling save");
		integerlist.add(1);
		
		
		return integerlist;
		
	}
	public double getOTHours(double otHours , double projectOTMinutes){
		double ot = 0;
		logger.log(Level.SEVERE, "ot value :" + otHours);
		logger.log(Level.SEVERE, "ot value :" + projectOTMinutes);
		if((otHours*60) >= projectOTMinutes){
			ot = Math.ceil(otHours);
			return ot;
		}		
		return ot;
	}



	@Override
	public ArrayList<String> readStockUpdateExcel(String entityNameStockUpdate,
			Long companyId,String blobURL) {
		
		// TODO Auto-generated method stub

		ArrayList<String> strvalidationlist = new ArrayList<String>();
		if (entityNameStockUpdate.trim().equals("Stock Update")) {
			
			BlobKey blobkeyValue=new BlobKey(blobURL);
			
//			ArrayList<String> exceldatalist = readExcelAndGetArraylist(blobkey);
			
			ArrayList<String> exceldatalist= readExcelAndGetArraylist(blobkeyValue);
			
			System.out.println("Array List Size After Excel Read :- "+exceldatalist.size());
			
			if (exceldatalist == null || exceldatalist.size() == 0) {
				strvalidationlist
						.add("Unable to read excel data please try again!");
				return strvalidationlist;
			}
			if (exceldatalist.get(0).trim().equalsIgnoreCase("Product ID")
					&& exceldatalist.get(1).trim().equalsIgnoreCase("Product Name")
					&& exceldatalist.get(2).trim()
							.equalsIgnoreCase("Warehouse")
					&& exceldatalist.get(3)
							.equalsIgnoreCase("Storage Location")
					&& exceldatalist.get(4).trim()
							.equalsIgnoreCase("Storage Bin")
					&& exceldatalist.get(5).trim()
							.equalsIgnoreCase("Warehouse Type")
					&& exceldatalist.get(6).equalsIgnoreCase("#Min Qty")
					&& exceldatalist.get(7).equalsIgnoreCase("#Max Qty")
					&& exceldatalist.get(8).equalsIgnoreCase("#Normal level")
					// &&
					// exceldatalist.get(9).equalsIgnoreCase("#Reorder level")
					// && exceldatalist.get(10).equalsIgnoreCase("#Reorder Qty")
					&& exceldatalist.get(9).equalsIgnoreCase("#Available Qty")
					&& exceldatalist.get(10).equalsIgnoreCase(
							"#Per Monthly Avg Consumption")
					&& exceldatalist.get(11).equalsIgnoreCase("#Lead Days")
					// &&
					// exceldatalist.get(14).equalsIgnoreCase("Safety Stock Level")
					&& exceldatalist.get(12).equalsIgnoreCase("#Min Order Qty")
					&& exceldatalist.get(13).equalsIgnoreCase(
							"#Forecast/ Demand")) {
				System.out.println("Excel Header are Correct");
				exceldatalist.add(blobkeyValue+"");
				return exceldatalist;

			} else {
				strvalidationlist.add("Please Upload Stock Update Excel uploaded Excel not from Stock update Please check Header");
				return strvalidationlist;
			}
		} else {
			strvalidationlist.add("Invalid Stock update operation");
			return strvalidationlist;
		}
	}



	private ArrayList<String> readExcelAndGetArraylist(BlobKey blobkeyValue) {


		ArrayList<String> exceldatalist = new ArrayList<String>();
		System.out.println(" Excel Reading Start ");
		try {
			
			logger.log(Level.SEVERE, "Blobkey value" + blobkey);

			long i;
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell
									.getDateCellValue()));
						} else {

							// i = (long) cell.getNumericCellValue();
							// logger.log(Level.SEVERE, "Value : "+i);
//							logger.log(Level.SEVERE,
//									"Value : " + cell.getNumericCellValue());
							// exceldatalist.add(cell.getNumericCellValue() +
							// "");
							DecimalFormat decimalFormat = new DecimalFormat(
									"#########.##");
							int value1 = 0, value2 = 0;
							String[] array = decimalFormat.format(
									cell.getNumericCellValue()).split("\\.");
							if (array.length > 0) {
								// value1 = Integer.parseInt(array[0]);
							}
							if (array.length > 1) {
								value2 = Integer.parseInt(array[1]);
							}
							if (value2 == 0) {
								i = (long) cell.getNumericCellValue();
								exceldatalist.add(i + "");
							} else {
								exceldatalist.add(cell.getNumericCellValue()
										+ "");
							}

						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:
					}
				}
			}

			System.out.println("Excel Read from WB :- Size is " + exceldatalist.size());
			logger.log(Level.SEVERE,"Excel Read from WB :- Size is " + exceldatalist.size());
			wb.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(" Excel size After Reading file " + exceldatalist);
		return exceldatalist;
		
	}



	@Override
	public ArrayList<String> reactOnStockUpdateValidate(
			ArrayList<String> stockupdateExcel, Long companyId) {
		
		
		// TODO Auto-generated method stub
		System.out.println(" Inside Validation Method and Excel size is :- "+stockupdateExcel.size());
		System.out.println(" stockupdateExcel.size() - 1 :- "+(stockupdateExcel.size() - 1));
		String blobkey = stockupdateExcel.get(stockupdateExcel.size()-1);
		System.out.println(" Blobkey 1 Value " + blobkey);
		stockupdateExcel.remove(stockupdateExcel.size()-1);
		
		

		// ArrayList<ProductInventoryViewDetails> prodInventoryViewList=new
		// ArrayList<ProductInventoryViewDetails>();
		ArrayList<String> errorList = new ArrayList<String>();
		// errorList.add("");
		HashSet<Integer> hProdInventoryID = new HashSet<Integer>();
		HashSet<Integer> prodname_id = new HashSet<Integer>();
		HashSet<String> hWarehouseName = new HashSet<String>();
		HashSet<String> hStorageLoc = new HashSet<String>();
		HashSet<String> hStorageBin = new HashSet<String>();
		HashSet<String> hWarehouseType = new HashSet<String>();

		for (int i = 14; i < stockupdateExcel.size(); i += 14) {
			hProdInventoryID.add(Integer.parseInt(stockupdateExcel.get(i)));
			prodname_id.add(Integer.parseInt(stockupdateExcel.get(i)));
			hWarehouseName.add(stockupdateExcel.get(i + 2));
			hStorageLoc.add(stockupdateExcel.get(i + 3));
			hStorageBin.add(stockupdateExcel.get(i + 4));
			hWarehouseType.add(stockupdateExcel.get(i + 5));
		}

		// ArrayList<Integer> prodInventoryIDList=new
		// ArrayList<Integer>(hProdInventoryID);
		ArrayList<Integer> prodNameList = new ArrayList<Integer>(prodname_id);
		System.out.println(" ProdNameList Value " + prodNameList + " and Size "+ prodNameList.size());
		ArrayList<String> warehouseNameList = new ArrayList<String>(hWarehouseName);
		System.out.println(" warehouseNameList Value " + warehouseNameList+ " and Size " + warehouseNameList.size());
		ArrayList<String> storageLocList = new ArrayList<String>(hStorageLoc);
		System.out.println(" storageLocList Value " + storageLocList+ " and Size " + storageLocList.size());
		ArrayList<String> storageBinList = new ArrayList<String>(hStorageBin);
		System.out.println(" storageBinList Value " + storageBinList+ " and Size " + storageBinList.size());
		// ArrayList<String> warehouseTypeList=new
		// ArrayList<String>(hWarehouseType);

		// List<ProductInventoryView>
		// prodInventoryID=ofy().load().type(ProductInventoryView.class).filter("compnayId",
		// companyId).filter("prodid IN", prodInventoryIDList).list();
		// List<ProductInventoryView>
		// prodId_Name=ofy().load().type(ProductInventoryView.class).filter("compnayId",
		// companyId).filter("prodID IN", prodNameList).list();
		// System.out.println(" Product ID "+prodId_Name.size());

		List<ProductInventoryView> prodId_Name = ofy().load()
				.type(ProductInventoryView.class)
				.filter("companyId", companyId)
				.filter("productinfo.prodID IN", prodNameList).list();
		System.out.println(" ProductInventoryView size " + prodId_Name.size());
		// List<WareHouse>
		// warehouseName=ofy().load().type(WareHouse.class).filter("compnayId",
		// companyId).filter("buisnessUnitName IN", warehouseNameList).list();
		List<WareHouse> warehouseName = ofy().load().type(WareHouse.class)
				.filter("companyId", companyId)
				.filter("buisnessUnitName IN", warehouseNameList).list();
		System.out.println(" Warehouse Name " + warehouseName
				+ " Warehouse size " + warehouseName.size());
		// List<StorageLocation>
		// storageLoc=ofy().load().type(StorageLocation.class).filter("compnayId",
		// companyId).filter("buisnessUnitName IN", storageLocList).list();
		List<StorageLocation> storageLoc = ofy().load()
				.type(StorageLocation.class).filter("companyId", companyId)
				.filter("buisnessUnitName IN", storageLocList).list();
		System.out.println(" Storage Location " + storageLoc
				+ " Storage Location size " + storageLoc.size());
		List<Storagebin> storageBin = ofy().load().type(Storagebin.class)
				.filter("companyId", companyId)
				.filter("binName IN", storageBinList).list();
		System.out.println(" Storage Bin  " + storageBin + " Storage Bin Size "
				+ storageBin.size());

		ArrayList<ProductInventoryViewDetails> newExcelList = new ArrayList<ProductInventoryViewDetails>();

		for (int i = 14; i < stockupdateExcel.size(); i += 14) {

			try {

				ProductInventoryViewDetails prodInventoryViewEntity = new ProductInventoryViewDetails();

				if (stockupdateExcel.get(i).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i).equals(" ")) {
					// prodInventoryViewEntity.setProdid(Integer.parseInt(stockupdateExcel.get(i)));
					errorList.add("Product id should not be NA/blank.");
					return errorList;
				} else {
					int idget = 0;
					idget = Integer.parseInt(stockupdateExcel.get(i));
					int id = getProd_ID(idget, prodId_Name);
					if (id == 0) {
						errorList.add(" Invalid Product ID "
								+ stockupdateExcel.get(i) + " Excel Location "
								+ i);
						return errorList;
					} else {
						prodInventoryViewEntity.setProdid(id);
					}
				}

				if (stockupdateExcel.get(i + 1).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 1).equals(" ")) {
					errorList.add("Product Name should not be NA/blank.");
					return errorList;
				} else {

					String prod_Name = getProd_Name(
							stockupdateExcel.get(i + 1), prodId_Name);

					if (prod_Name.trim().equalsIgnoreCase("")) {
						int no = i + 1;
						errorList.add(" Invalid Product Name "
								+ stockupdateExcel.get(i + 1)
								+ " Excel Location " + no);
						return errorList;
					} else {
						prodInventoryViewEntity.setProdname(prod_Name);
					}

				}
				if (stockupdateExcel.get(i + 2).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 2).equals(" ")) {
					errorList.add("Warehouse should not be NA/blank.");
					return errorList;
				} else {

					String warehouse_Name = getWarehouseName(
							stockupdateExcel.get(i + 2), warehouseName);
					if (warehouse_Name.equals("")) {
						int no = i + 2;
						errorList.add(" Invalid Warehouse Name '"
								+ stockupdateExcel.get(i + 2)
								+ "' Excel Location " + no);
						return errorList;
					} else {
						prodInventoryViewEntity
								.setWarehousename(warehouse_Name);
					}
				}

				if (stockupdateExcel.get(i + 3).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 3).equals(" ")) {
					errorList.add("Storage Location should not be NA/blank.");
					return errorList;
				} else {
					String location = getStoreLocation(
							stockupdateExcel.get(i + 3), storageLoc);
					if (location.equals("")) {
						int no = i + 3;
						errorList.add(" Invalid Storage Location '"
								+ stockupdateExcel.get(i + 3)
								+ "' Excel Location " + no);
						return errorList;
					} else {
						prodInventoryViewEntity.setStoragelocation(location);
					}
				}

				if (stockupdateExcel.get(i + 4).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 4).equals(" ")) {
					errorList.add("Storage Bin should not be NA/blank.");
					return errorList;
				} else {

					String storeBin = getStorage_Bin(
							stockupdateExcel.get(i + 4), storageBin);

					if (storeBin.equals("")) {
						int no = i + 4;
						errorList.add(" Invalid Storage Bin '"
								+ stockupdateExcel.get(i + 4)
								+ "' Excel Location " + no);
						return errorList;
					} else {
						prodInventoryViewEntity.setStoragelocation(storeBin);
					}
				}

				if (stockupdateExcel.get(i + 5).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 5).isEmpty()) {
					errorList.add("Warehouse Type should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i));
					return errorList;
				}
				// else{
				// String
				// wtype=getWarehouse_Type(stockupdateExcel.get(i+5),warehouseName);
				// if(wtype.equals("")){
				// errorList.add(" Invalid Warehouse Type '"+stockupdateExcel.get(i+5)+"' Excel Location "+i+5);
				// return errorList;
				// }else {
				// prodInventoryViewEntity.setWarehouseType(wtype);
				// }
				//
				// }

				if (stockupdateExcel.get(i + 6).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 6).isEmpty()) {
					errorList.add("Min Quantity should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i));
					return errorList;
				} else {

					prodInventoryViewEntity.setMinOrderQty(Double
							.parseDouble(stockupdateExcel.get(i + 6)));
				}

				if (stockupdateExcel.get(i + 7).trim().equalsIgnoreCase("NA")) {
					errorList.add("Max Quantity should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i + 6));
					return errorList;
				} else {

					prodInventoryViewEntity.setMaxqty(Double
							.parseDouble(stockupdateExcel.get(i + 7)));
				}

				if (stockupdateExcel.get(i + 8).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 8).equals(" ")) {
					errorList.add("Normal Level should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i + 8));
					return errorList;
				} else {

					prodInventoryViewEntity.setNormallevel(Double
							.parseDouble(stockupdateExcel.get(i + 8)));
				}

				if (stockupdateExcel.get(i + 9).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 9).equals(" ")) {
					errorList.add("Available Stock should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i + 9));
					return errorList;
				} else {

					prodInventoryViewEntity.setAvailableqty(Double
							.parseDouble(stockupdateExcel.get(i + 9)));
				}

				if (stockupdateExcel.get(i + 10).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 10).equals(" ")) {
					errorList
							.add(" Per Monthly Avg Consumption should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i + 10));
					return errorList;
				} else {

					prodInventoryViewEntity.setAvgConsumption(Double
							.parseDouble(stockupdateExcel.get(i + 10)));
				}

				if (stockupdateExcel.get(i + 11).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 11).equalsIgnoreCase(" ")) {
					errorList.add(" Lead Days should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i + 11));
					return errorList;
				} else {

					prodInventoryViewEntity.setLeadDays(Integer
							.parseInt(stockupdateExcel.get(i + 11)));
				}

				if (stockupdateExcel.get(i + 12).trim().equalsIgnoreCase("NA")) {
					errorList
							.add(" Min Order Quantity should not be NA/blank.");
					System.out.println(" I Value " + i + " "
							+ stockupdateExcel.get(i + 12));
					return errorList;
				} else {

					prodInventoryViewEntity.setMinOrderQty(Double
							.parseDouble(stockupdateExcel.get(i + 12)));
				}

				if (stockupdateExcel.get(i + 13).trim().equalsIgnoreCase("NA")
						|| stockupdateExcel.get(i + 13).equals(" ")) {
					errorList.add(" Forcas Demand should not be NA/blank.");
					return errorList;
				} else {
					prodInventoryViewEntity.setForecastDemand(Double
							.parseDouble(stockupdateExcel.get(i + 13)));
				}
				if (prodInventoryViewEntity != null) {
					newExcelList.add(prodInventoryViewEntity);
				}
			} catch (Exception e) {
				System.out.println(" Error " + e);
			}
		}
		System.out.println(" Return value print Here" + errorList);
		System.out.println(" New List Size " + newExcelList.size()
				+ " Excel List Size " + stockupdateExcel.size());
		if (errorList.size() == 0) {
			errorList.add("Success");
			errorList.add(blobkey);
			return errorList;
		}

		System.out.println(" Return value print Here" + errorList);
		return errorList;
		
		
		
	}



	private String getStorage_Bin(String stringName, List<Storagebin> storageBin) {
		// TODO Auto-generated method stub
		String name = "";
		for (Storagebin sbin : storageBin) {
			if (stringName.trim().equalsIgnoreCase(sbin.getBinName())) {
				name = sbin.getBinName();
				return name;
			}
		}
		System.out
				.println(" I am Printing Bin Name " + name + " " + stringName);
		return name;
	}



	private String getStoreLocation(String stringName,
			List<StorageLocation> storageLoc) {
		// TODO Auto-generated method stub
		String name = "";
		for (StorageLocation location : storageLoc) {
			if (stringName.trim().equalsIgnoreCase(
					location.getBusinessUnitName())) {
				name = location.getBusinessUnitName();
				return name;
			}
		}
		System.out.println(" I am Printing Location Name " + name + " "
				+ stringName);
		return name;
	}



	private String getWarehouseName(String stringName,
			List<WareHouse> warehouseName) {
		// TODO Auto-generated method stub
		String name = "";
		for (WareHouse name1 : warehouseName) {
			if (stringName.trim().equalsIgnoreCase(name1.getBusinessUnitName())) {
				name = name1.getBusinessUnitName();
				System.out.println(" I am Printing Warehouse Name " + name);
				return name;
			}
		}
		System.out.println(" I am Printing Warehouse Name " + name + " "+ stringName);
		return name;
	}



	private String getProd_Name(String stringName,
			List<ProductInventoryView> prodName) {
		String name = "";
		for (ProductInventoryView name1 : prodName) {
			for (ProductInventoryViewDetails details : name1.getDetails()) {
				if (stringName.trim().equalsIgnoreCase(details.getProdname())) {
					name = details.getProdname();
					return name;
				}
			}
		}
		System.out.println(" I am Printing Product Name " + name + " "
				+ stringName);
		return name;
	}



	private int getProd_ID(int excelID, List<ProductInventoryView> prodId_Name) {
		// TODO Auto-generated method stub
		int prod_id = 0;
		for (ProductInventoryView prodID : prodId_Name) {
			for (ProductInventoryViewDetails detailsID : prodID.getDetails()) {
				System.out
						.println("Item Product Code " + detailsID.getProdid());
				if (excelID == detailsID.getProdid()) {
					return prod_id = detailsID.getProdid();
				}
			}
		}
		return prod_id;
	}



	@Override
	public String stockUpdateExcelUpload(String blobkey, Long companyId) {
		// TODO Auto-generated method stub
		try {
			System.out.println(" Inside stockUpdateExcelUpload Method ");

			Queue queue = QueueFactory.getQueue("DocumentUploadTaskQueue-queue");
			queue.add(TaskOptions.Builder
					.withUrl("/slick_erp/DocumentUploadTaskQueue")
					.param("EntityName", "Stock Update")
					.param("companyId", companyId + "")
					.param("stockUpdateBlobKey", blobkey));
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception ==" + e.getMessage());
		}
		return "Stock Update Upload process started";
	}



	@Override
	public ArrayList<SuperModel> validateAttandancepart2(ArrayList<AttandanceInfo> attandanceList) {
		
		ArrayList<SuperModel> alldatalist = new ArrayList<SuperModel>();
		try {
			
		HashSet<Integer> hsEmpId=new HashSet<Integer>();
		HashSet<String> hsProj=new HashSet<String>();
		
		
		for(AttandanceInfo info:attandanceList){
			hsEmpId.add(info.getEmpId());
			hsProj.add(info.getProjName().trim());
		}
		ArrayList<Integer> empList=new ArrayList<Integer>(hsEmpId);
		ArrayList<String> projList=new ArrayList<String>(hsProj);
		logger.log(Level.SEVERE,"TOTAL NO. OF EMP : "+empList.size());
		logger.log(Level.SEVERE,"NO. OF Proj : "+projList.size());
		
		List<LeaveBalance>empLeaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("empInfo.empCount IN", empList).list();
		logger.log(Level.SEVERE,"EMPLOYEE LEAVE LIST SIZE : "+empLeaveBalList.size());
		if(empLeaveBalList!=null && empLeaveBalList.size()>0) {
			for(LeaveBalance leaveBalance : empLeaveBalList) {
//				leaveBalance.setDocumentName("LeaveBalance");
			}
			alldatalist.addAll(empLeaveBalList);
		}
		
		List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
		if(empList.size()!=0){
			empInfoList = ofy().load().type(EmployeeInfo.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("empCount IN", empList).list();
		}
		logger.log(Level.SEVERE,"EMPLOYEE INFO LIST SIZE : "+empInfoList.size());
		
		if(empInfoList!=null && empInfoList.size()>0) {
			for(EmployeeInfo employeeinfo : empInfoList) {
//				employeeinfo.setDocumentName("EmployeeInfo");
			}
			alldatalist.addAll(empInfoList);
		}
		
		
		
		/**
		 * @author Anil , Date :04-05-2019
		 * Validating employee attendance overlapping
		 * if user is marking attendance for same day but for different project
		 */
		ArrayList<String> empIdListAsPerdate=new ArrayList<String>();

		
		List<HrProject>projectList=ofy().load().type(HrProject.class).filter("companyId", attandanceList.get(0).getCompanyId()).filter("projectName IN", projList).filter("status", true).list();
		logger.log(Level.SEVERE,"PROJECT SIZE :: "+projectList.size());
		
//		ArrayList<Integer> projectidlist = new ArrayList<Integer>();
		
		if(projectList!=null && projectList.size()>0) {
			for(HrProject hrProject : projectList) {
//				hrProject.setDocumentName("HrProject");
//				projectidlist.add(hrProject.getCount());
			}
			alldatalist.addAll(projectList);
		}
		

//		/**
//		 * Date : 18-08-2018 BY ANIL
//		 * Retrieving existing attendance , leave and ot info for overriding it.
//		 */
//		List<Attendance> existingAttendanceList=getExistingAttendance(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList);
//		
//		if(existingAttendanceList!=null && existingAttendanceList.size()>0) {
//			for(Attendance existingattendance : existingAttendanceList) {
//				existingattendance.setDocumentName("ExistingAttendance");
//			}
//			alldatalist.addAll(existingAttendanceList);
//		}
//		
//		if(projectidlist!=null && projectidlist.size()!=0) {
//			CommonServiceImpl commnservice = new CommonServiceImpl();
//			 ArrayList<HrProjectOvertime> HrProjectOvertimelist = commnservice.getHrProjectOvertimelist(projectidlist, attandanceList.get(0).getCompanyId());
//			 if(HrProjectOvertimelist!=null && HrProjectOvertimelist.size()>0) {
//					for(HrProjectOvertime hrprojectovertime : HrProjectOvertimelist) {
//						hrprojectovertime.setDocumentName("HrProjectOvertime");
//					}
//					alldatalist.addAll(HrProjectOvertimelist);
//			}
//		}
		
		
		} catch (Exception e) {
			e.printStackTrace();
			
			Attendance attendance = new Attendance();
//			attendance.setDocumentName("ERROR");
			alldatalist.add(attendance);
		}
		
		return  alldatalist;
		
//		for(AttandanceInfo info:attandanceList){
//			/**
//			 * Date : 23-07-2018 BY ANIL
//			 */
//			boolean calendarFlag=false;
//
//			double totalNoOfDays = 0;
//			double totalNoOfPresentDays = 0;
//			double totalNoWeeklyOff=0;
//			double totalNoOfHoliday=0;
//			double totalNoOfAbsentDays=0;
//			double totalExtraHours=0;
//			/**
//			 * date :16-12-2018 By ANIL
//			 * Changed the variable name form totalLeaveHours to totalLeave
//			 */
//			double totalLeave=0;
//			
////			double woAsLeave=0;
//			double halfDayLeave=0;
//			
//			double woOt=0;
//			double hOt=0;
//			double nOt=0;
//			double nhOt=0;
//			double phOt=0;
//			
//			AttandanceError error=new AttandanceError();
//			HashMap<String , Double> leaveTotalMap = new HashMap<String , Double>();
//			
//			
//			Comparator<AttandanceDetails> attandanceComparator = new  Comparator<AttandanceDetails>() {
//				@Override
//				public int compare(AttandanceDetails o1, AttandanceDetails o2) {
//					return o1.getDate().compareTo(o2.getDate());
//				}
//			};
//			Collections.sort(info.getAttandList(),attandanceComparator);
//			
//			LeaveBalance leaveBalance=null;
//			if(empLeaveBalList!=null&&empLeaveBalList.size()!=0){
//				leaveBalance=getEmpLeaveBalance(info.getEmpId(),empLeaveBalList);
//			}
//			
//			EmployeeInfo empInfo=validateEmpId(info.getEmpId(), empInfoList);
//			
//
//			/**
//			 * @author Anil
//			 * @since 29-01-2021
//			 * Commenting above logic of validating existing attendance
//			 * As per the new logic of user is trying to upload attendance of same date which is uploaded then only we restrict to upload
//			 * earlier if any day attendance was found then he to delete and re upload attendance
//			 * Raised by Rahul For Sunrise
//			 */
////			ArrayList<String> existingAttendanceListDtWise=new ArrayList<String>();
////			try {
////			Label:	for(Attendance attend:existingAttendanceList){
////						if(info.getEmpId()==attend.getEmpId()){
////							existingAttendanceListDtWise.add(info.getEmpId()+""+fmt.format(attend.getAttendanceDate()));
////						}
////					}
////			} catch (Exception e) {
////				e.printStackTrace();
////			}
//			ArrayList<String> existingAttendanceListDtWise = getDatewiseAttendance(existingAttendanceList,info);
//
//
//			String msg="";
//			for(AttandanceDetails attDet:info.getAttandList()){
//				/**
//				 * @author Anil , Date : 07-05-2019
//				 * If attendance is marked as OP then we will not consider that attendance for update
//				 * raised by Nitin Sir,Sonu
//				 */
//				if(attDet.getValue().equalsIgnoreCase("OP")){
//					logger.log(Level.SEVERE,"AttendanceLabelAsInput  :: "+attDet.getValue());
//					continue;
//				}
//				
//				
//				/**
//				 * @author Anil
//				 */
//				
//				if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("P")){
////					logger.log(Level.SEVERE,"AttendanceLabelAsInput P :: "+empInfo.getLeaveCalendar().getWorkingHours());
//					attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()+"");
//				}else if(labelAsInputFlag&&attDet.getValue().equalsIgnoreCase("HD")){
////					logger.log(Level.SEVERE,"AttendanceLabelAsInput HD :: "+empInfo.getLeaveCalendar().getWorkingHours()/2);
//					attDet.setValue(empInfo.getLeaveCalendar().getWorkingHours()/2+"");
//				}
//				
//				/**
//				 * 
//				 */
//				if(!calendarFlag){
//				totalNoOfDays++;
//				
//				if(empInfo!=null){
//					if(empInfo.getDateOfJoining()!=null){
//						empInfo.getDateOfJoining().setHours(0);
//						empInfo.getDateOfJoining().setMinutes(0);
//						empInfo.getDateOfJoining().setSeconds(0);
//					}
//					if(empInfo.getLastWorkingDate()!=null){
//						empInfo.getLastWorkingDate().setHours(0);
//						empInfo.getLastWorkingDate().setMinutes(0);
//						empInfo.getLastWorkingDate().setSeconds(0);
//					}
//				}
//				
//				if(attDet.getDate().before(empInfo.getDateOfJoining())
//						&&!attDet.getValue().trim().equalsIgnoreCase("NA")){
//					System.out.println("ATT DT : "+attDet.getDate()+" JOINING DT : "+empInfo.getDateOfJoining()+" VALUE : "+attDet.getValue());
//					msg=msg+" / Attendance should not be before date of joining "+fmt.format(empInfo.getDateOfJoining());
//					attandanceList.get(0).setErrorCode(-1);
//					break;
//				}
//				
//				if(empInfo.getLastWorkingDate()!=null&&attDet.getDate().after(empInfo.getLastWorkingDate())
//						&&!attDet.getValue().equalsIgnoreCase("NA")){
//					System.out.println("ATT DT : "+attDet.getDate()+" LAST WRKING DT : "+empInfo.getLastWorkingDate()+" VALUE : "+attDet.getValue());
//					msg=msg+" / Attendance should not be after last date of working "+fmt.format(empInfo.getLastWorkingDate());
//					attandanceList.get(0).setErrorCode(-1);
//					break;
//				}
//				
//				if(attDet.getValue().trim().equalsIgnoreCase("END")){
//					break ;
//				}
//				
////				if(!calendarFlag){
//					try{
//						
//						
//						double workedHrs=Double.parseDouble(attDet.getValue().trim());
//						WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
//						boolean isWoff = isWeeklyOff(woff,attDet.getDate());
//						boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//						
//						/**
//						 * @author Anil
//						 * @since 13-07-2020
//						 */
//						String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
//						String holidayLabel="H";
//						if(holidayType!=null){
//							logger.log(Level.SEVERE ,attDet.getDate()+ " holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//							if(holidayType.equalsIgnoreCase("National Holiday")){
//								holidayLabel="NH";
//							}else if(holidayType.equalsIgnoreCase("Public Holiday")){
//								holidayLabel="PH";
//							}
//						}
//						if(workedHrs==empInfo.getLeaveCalendar().getWorkingHours()){
//							totalNoOfPresentDays++;
//							
//							if(isWoff){
//								woOt=woOt+workedHrs;
//								totalExtraHours+=workedHrs;
//							}else if(isHday){
//								hOt=hOt+workedHrs;
//								/**
//								 * Date : 17-08-2018 By ANIL
//								 * OT ERROR
//								 */
//								totalExtraHours+=workedHrs;
//								if(holidayLabel.equals("NH")){
//									nhOt=nhOt+workedHrs;
//								}else if(holidayLabel.equals("PH")){
//									phOt=phOt+workedHrs;
//								}
//								logger.log(Level.SEVERE , "--holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//							}
//						}else if(workedHrs>empInfo.getLeaveCalendar().getWorkingHours()){
//							double extraHrs=workedHrs-empInfo.getLeaveCalendar().getWorkingHours();
//							totalExtraHours=totalExtraHours+extraHrs;
//							totalNoOfPresentDays++;
//							
//							WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
//							boolean isWo = isWeeklyOff(wo,attDet.getDate());
//							boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//							if(isWo){
//								woOt=woOt+extraHrs;
//							}else if(isHoliday){
//								hOt=hOt+extraHrs;
//							}else{
//								nOt=nOt+extraHrs;
//							}
//						}else if(workedHrs<empInfo.getLeaveCalendar().getWorkingHours()){
//							double leavaeHrs= empInfo.getLeaveCalendar().getWorkingHours()-workedHrs;
////							halfDayLeave=halfDayLeave+leavaeHrs;
//							totalNoOfPresentDays++;
//							
//							if(isWoff){
//								woOt=woOt+leavaeHrs;
//								/**
//								 * Date : 17-08-2018 By ANIL
//								 * OT ERROR
//								 */
//								totalExtraHours+=workedHrs;
//							}else if(isHday){
//								hOt=hOt+leavaeHrs;
//								/**
//								 * Date : 17-08-2018 By ANIL
//								 * OT ERROR
//								 */
//								totalExtraHours+=workedHrs;
//								
//								if(holidayType.equals("NH")){
//									nhOt=nhOt+workedHrs;
//								}else if(holidayType.equals("PH")){
//									phOt=phOt+workedHrs;
//								}
//							}
//							/**
//							 * @author Anil , Date : 25-06-2019
//							 * will update half day leave if and only if its not on weekly off and holiday
//							 */
//							else{
//								halfDayLeave=halfDayLeave+leavaeHrs;
//							}
//						}
//						
//						
//						
//						/**
//						 * @author Anil, Date :04-05-2019
//						 */
//						String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//						if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//							if(empIdListAsPerdate.contains(empId_Date_Key)){
//								msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//								attandanceList.get(0).setErrorCode(-1);
//							}
//							empIdListAsPerdate.add(empId_Date_Key);
//						}else{
//							empIdListAsPerdate.add(empId_Date_Key);
//						}
//						
//						/**
//						 * @author Anil @since 29-01-2021
//						 */
//						if(existingAttendanceListDtWise.size()!=0){
//							if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
////								error.setEmpId(info.getEmpId());
////								error.setEmpName(info.getEmpName());
////								error.setProjectName(info.getProjName());
////								error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
////								errorList.add(error);
////								attandanceList.get(0).setErrorCode(-1);
//								
//								msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//						
//						
//						
//					}catch(NumberFormatException exception){
//						
//						if(attDet.getValue().trim().equalsIgnoreCase("WO")||attDet.getValue().trim().equalsIgnoreCase("W/O")){
//							WeekleyOff wo = empInfo.getLeaveCalendar().getWeeklyoff();
//							boolean isWo = isWeeklyOff(wo,attDet.getDate());
//							/**
//							 * @author Anil , Date : 20-08-2019
//							 */
//							boolean isWoDefinedInCal=isWeeklyOffDefined(wo);
//							System.out.println("WO : "+isWo+" Defined : "+isWoDefinedInCal+" Dt : "+attDet.getDate());
//							if(!isWo){
////								woAsLeave++;
//								if(isWoDefinedInCal){
//									msg=msg+" / Weekly off day is not matched. "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							totalNoWeeklyOff++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//						}else if(attDet.getValue().trim().equalsIgnoreCase("H")){
//							boolean isHoliday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//							if(!isHoliday){
//								msg=msg+" / Holiday date "+attDet.getDate()+" is not matched in Employee Calendar. ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//							totalNoOfHoliday++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//							String holidayType=empInfo.getLeaveCalendar().getHolidayType(attDet.getDate());
//							String holidayLabel="H";
//							if(holidayType!=null){
//								logger.log(Level.SEVERE ,attDet.getDate()+ "holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
//								if(holidayType.equalsIgnoreCase("National Holiday")){
//									holidayLabel="NH";
//								}else if(holidayType.equalsIgnoreCase("Public Holiday")){
//									holidayLabel="PH";
//								}
//							}
//							
//						}else if(attDet.getValue().trim().equalsIgnoreCase("L")){
//							totalLeave++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//						}else if(attDet.getValue().trim().equalsIgnoreCase("A")){
//							totalNoOfAbsentDays++;
//							
//							/**
//							 * @author Anil, Date :04-05-2019
//							 */
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//						}else if(attDet.getValue().trim().equalsIgnoreCase("NA")){
//							if(empInfo.getLastWorkingDate()!=null){
//								if(attDet.getDate().after(empInfo.getDateOfJoining())
//										&&attDet.getDate().before(empInfo.getLastWorkingDate())){
//									msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								
//							}else{
//								if(attDet.getDate().after(empInfo.getDateOfJoining())||attDet.getDate().equals(empInfo.getDateOfJoining())){
//									msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							
////							if(empInfo.getLastWorkingDate()!=null&&!attDet.getDate().before(empInfo.getLastWorkingDate())){
////								msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
////								attandanceList.get(0).setErrorCode(-1);
////							}
//							
//						}
//						/**
//						 * @author Anil
//						 * @since 10-07-2020
//						 * Adding condition for public holiday keyword
//						 */
//						else if(attDet.getValue().trim().contains("PH")&&validPH_OT(attDet.getValue())){
//							
//							WeekleyOff woff = empInfo.getLeaveCalendar().getWeeklyoff();
//							boolean isWoff = isWeeklyOff(woff,attDet.getDate());
//							boolean isHday = empInfo.getLeaveCalendar().isHolidayDate(attDet.getDate());
//							
//							if(isWoff){
//								msg=msg+" / Can not mark attendance as PH on weekly off. "+attDet.getDate()+". ";
//								attandanceList.get(0).setErrorCode(-1);
//							}else if(isHday){
//								msg=msg+" / Can not mark attendance as PH on holiday. "+attDet.getDate()+". ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//							
//							totalNoOfHoliday++;
//							double phOtt=getTotalPhOtHours(attDet.getValue());
//							totalExtraHours=totalExtraHours+phOtt;
//							phOt+=phOtt;
//							
//							String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//							if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//								if(empIdListAsPerdate.contains(empId_Date_Key)){
//									msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//									attandanceList.get(0).setErrorCode(-1);
//								}
//								empIdListAsPerdate.add(empId_Date_Key);
//							}else{
//								empIdListAsPerdate.add(empId_Date_Key);
//							}
//							
//						}else{
//							/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/
//							double value = 1;
//							if(leaveTotalMap.containsKey(attDet.getValue().trim())){
//								value = leaveTotalMap.get(attDet.getValue().trim());
//								leaveTotalMap.put(attDet.getValue().trim(), value + 1);
//							}else{
//								leaveTotalMap.put(attDet.getValue().trim(), value);
//							}
//								String empId_Date_Key=fmt.format(attDet.getDate())+info.getEmpId();
//								if(empIdListAsPerdate!=null&&empIdListAsPerdate.size()!=0){
//									if(empIdListAsPerdate.contains(empId_Date_Key)){
//										msg=msg+" / Attendance is overlapping on "+fmt.format(attDet.getDate());
//										attandanceList.get(0).setErrorCode(-1);
//									}
//									empIdListAsPerdate.add(empId_Date_Key);
//								}else{
//									empIdListAsPerdate.add(empId_Date_Key);
//								}
////							msg=msg+" / Please add proper value against date "+attDet.getDate()+". ";
////							attandanceList.get(0).setErrorCode(-1);
//						}
//						
//						
//						/**
//						 * @author Anil @since 29-01-2021
//						 */
//						if(!attDet.getValue().trim().equalsIgnoreCase("NA")&&existingAttendanceListDtWise.size()!=0){
//							if(existingAttendanceListDtWise.contains(empInfo.getEmpCount()+""+fmt.format(attDet.getDate()))){
////								error.setEmpId(info.getEmpId());
////								error.setEmpName(info.getEmpName());
////								error.setProjectName(info.getProjName());
////								error.setRemark("Attendance is already uploaded on "+fmt.format(attDet.getDate()));
////								errorList.add(error);
////								attandanceList.get(0).setErrorCode(-1);
//								
//								msg=msg+"Attendance is already uploaded on "+fmt.format(attDet.getDate());
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//					}
//					
//					/**
//					 * Date : 18-08-2018 By ANIl
//					 * @author Anil
//					 * @since 26-11-2020
//					 * removing overriding functionality
//					 */
////					Attendance attendance=getExistingAttendance(info.getEmpId(),existingAttendanceList,attDet.getDate());
////					if(attendance!=null){
////						attenListToBeRemoved.add(attendance);
////					}
////					
////					EmployeeLeave empLeave=getExistingLeave(info.getEmpId(),approvedLeaveList,attDet.getDate());
////					if(empLeave!=null){
////						leaveListTobeRemoved.add(empLeave);
////						updateLeaveBalance(leaveBalance,empLeave,empInfo);
////					}
////					
////					EmployeeOvertime empOt=getExistingOt(info.getEmpId(),approvedOtList,attDet.getDate());
////					if(empOt!=null){
////						otListToBeRemoved.add(empOt);
////					}
//				
//			}
//			}
//			
//			
//			if(leaveBalance!=null){
//				
//				/**
//				 * @author Anil , Date : 20-08-2019
//				 * We will not maintain WO as leave, will just mark attendance as weekly off
//				 * Suggested by Nitin Sir
//				 */
//				//WO as Leave condition
////				if(woAsLeave>0){
////					boolean woFlag=false;
////					for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
////						if(leave.getShortName().equalsIgnoreCase("WO")||leave.getShortName().equalsIgnoreCase("W/O")){
////							woFlag=true;
////							if(woAsLeave>leave.getBalance()){
////								msg=msg+"/ Total WO leave balance is  "+leave.getBalance()+" only and requested WO leave is "+woAsLeave;
////								attandanceList.get(0).setErrorCode(-1);
////							}
////							break;
////						}
////					}
////					if(!woFlag){
////						msg=msg+"/ No WO type leave is assigned to employee.";
////						attandanceList.get(0).setErrorCode(-1);
////					}
////				}
//				
//				/**
//				 * End
//				 */
//				
//				//Total Paid Leave Condition
//				if(totalLeave>0){
//					boolean leaveFlag=false;
//					double afterDeductionLeaveBalance=0;
//					for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
////						if(leave.getPaidLeave()==true&&
////								!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O")){
//						if(leave.getPaidLeave()==true&&leave.getShortName().contains("PL")){
//							leaveFlag=true;
//							if(totalLeave>leave.getBalance()){
//								msg=msg+"/ Total paid leave balance is  "+leave.getBalance()+" only and requested leave is "+totalLeave;
//								attandanceList.get(0).setErrorCode(-1);
//							}else{
//								afterDeductionLeaveBalance=leave.getBalance()-totalLeave;
//							}
//							break;
//						}
//					}
//					if(!leaveFlag){
//						msg=msg+"/ No paid type leave is assigned to employee.";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//					
//					//Early leave/Half day leave condition
//					if(halfDayLeave>0){
//						halfDayLeave=halfDayLeave/empInfo.getLeaveCalendar().getWorkingHours();
//						if(afterDeductionLeaveBalance>0){
//							if(halfDayLeave>afterDeductionLeaveBalance){
//								msg=msg+"/"+(halfDayLeave-afterDeductionLeaveBalance)+"  half day leave will be considered as non paid leave as no paid leave is balanced.";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}else{
//							msg=msg+"/ All half day leave will be considered as non paid leave as no paid leave is balanced.";
//							attandanceList.get(0).setErrorCode(-1);
//						}
//					}
//				}
//				/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/ 
//				if(leaveTotalMap.size() > 0){
//					
//							for(Map.Entry<String, Double> entry : leaveTotalMap.entrySet()){
//								boolean leaveFlag=false;
//								double afterDeductionLeaveBalance = 0;
//								logger.log(Level.SEVERE , "leave type key :"+  entry.getKey());
//								for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//									
//									if((leave.getShortName().equalsIgnoreCase(entry.getKey()) && leave.getPaidLeave()==true)
//											&&
//											(!leave.getShortName().equalsIgnoreCase("WO")||!leave.getShortName().equalsIgnoreCase("W/O"))){
//										logger.log(Level.SEVERE , "leave type key :"+  entry.getValue());
//										leaveFlag=true;
//										if(entry.getValue()>leave.getBalance()){
//											msg=msg+"/ Total "+ leave.getName()  +" leave balance is  "+leave.getBalance()+" only and requested leave is "+entry.getValue();
//											attandanceList.get(0).setErrorCode(-1);
//										}
//										break;
//									}
//								}
//								if(!leaveFlag){
//									msg=msg+"/ No "+ entry.getKey() +" type leave is assigned to employee.";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//						}
//					
//				}
//				/**
//				 * end komal
//				 */
//			}else{
//				msg=msg+"/ No leave balance is defined against employee. ";
//				attandanceList.get(0).setErrorCode(-1);
//			}
//			
//			
//			if(totalExtraHours>0){
//				if(projectList!=null&&projectList.size()!=0){
//					HrProject project=getProjectName(info.getProjName(),projectList);
//					if(project!=null){
//						ArrayList<Overtime>otList=new ArrayList<Overtime>();
//						/**
//						 * @author Anil
//						 * @since 28-08-2020
//						 * Relievers OT
//						 */
//						String designation=null;
//						
//						/**
//						 * @author Anil @since 13-04-2021
//						 * Updated logic of Reliever ot validation 
//						 * Raised by Rahul Tiwari for sasha
//						 */
//						if(empInfo.isReliever()){
//							if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
////								otList=project.getRelieversOtList();
//								otList.addAll(project.getRelieversOtList());
//								designation=info.getEmpDesignation();
//							}
//						}
//						
//						/**
//						 * @author Vijay Date :- 08-09-2022
//						 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//						 */
//						ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//						CommonServiceImpl commonservice = new CommonServiceImpl();
//						overtimelist = commonservice.getHRProjectOvertimelist(project);
//						/**
//						 * ends here
//						 */
////						if(project.getOtList()!=null&&project.getOtList().size()!=0){
//////							otList=project.getOtList();
////							otList.addAll(project.getOtList());
////						}
//						if(overtimelist!=null&&overtimelist.size()!=0){
//							otList.addAll(overtimelist);
//						}
//						
//						if(otList.size()!=0){
//							logger.log(Level.SEVERE ,"woOt: "+ woOt+" hOt "+hOt+" nOt "+nOt+" phOt "+phOt+" nhOt "+nhOt);
//							logger.log(Level.SEVERE ,"Designation : "+designation+" Reliever "+empInfo.isReliever());
//							if(woOt>0){
//								Overtime ot=getOvertimeName(otList,"WO",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"WOT "+ot.getName());
//								}else{
//									msg=msg+"/ Weekly off OT is not added in project for employee "+info.getEmpName()+". ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							if(hOt>0){
//								Overtime ot=getOvertimeName(otList,"H",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"HOT "+ot.getName());
//									/**
//									 * Date : 27-08-2018 By ANIL
//									 */
//									if(ot.isLeave()==true){
//										boolean leaveFlag=false;
//										for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
//											if(leave.getName().equals(ot.getLeaveName())){
//												leaveFlag=true;
//												break;
//											}
//										}
//										if(!leaveFlag){
//											msg=msg+"/ No Leave - "+ot.getLeaveName()+" is assigned to employee.";
//											attandanceList.get(0).setErrorCode(-1);
//										}
//									}
//									/**
//									 * End
//									 */
//								}else{
//									msg=msg+"/ Holiday OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							if(nOt>0){
//								Overtime ot=getOvertimeName(otList,"N",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"NOT "+ot.getName());
//								}else{
//									msg=msg+"/ OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							
//							if(phOt>0){
//								Overtime ot=getOvertimeName(otList,"PH",info.getEmpId(),designation);
//								logger.log(Level.SEVERE,"phOt :: "+ot);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"PHOT "+ot.getName());
//								}else{
//									msg=msg+"/ PH OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//							
//							if(nhOt>0){
//								Overtime ot=getOvertimeName(otList,"NH",info.getEmpId(),designation);
//								if(ot!=null){
//									logger.log(Level.SEVERE ,"NHOT "+ot.getName());
//								}else{
//									msg=msg+"/ NH OT is not added in Project. ";
//									attandanceList.get(0).setErrorCode(-1);
//								}
//							}
//						}else{
//							msg=msg+"/ Please add OT details in Project. ";
//							attandanceList.get(0).setErrorCode(-1);
//						}
//					}else{
//						msg=msg+"/ Project name is not matched in DB. ";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//					
//				}else{
//					msg=msg+"/ Project name is not matched in DB. ";
//					attandanceList.get(0).setErrorCode(-1);
//				}
//			}
//			
//			
//
//			
//			
//			
//			/**
//			 * @author Anil , Date : 09-05-2019
//			 * validating ot and dot details
//			 */
//			if(info.isOtDotFlag()==true){
//				HrProject project=getProjectName(info.getProjName(),projectList);
//				ArrayList<Overtime>otList=new ArrayList<Overtime>();
////				if(project!=null&&project.getOtList()!=null){
////					otList=project.getOtList();
////				}
//				
//				/**
//				 * @author Anil
//				 * @since 28-08-2020
//				 * Relievers OT
//				 */
//				String designation=null;
//				/**
//				 * @author Anil @since 13-04-2021
//				 * Updated logic of Reliever ot validation 
//				 * Raised by Rahul Tiwari for sasha
//				 */
//				
////				if(empInfo.isReliever()){
////					if(project!=null&&project.getRelieversOtList()!=null){
////						otList=project.getRelieversOtList();
////						designation=info.getEmpDesignation();
////					}
////				}else{
////					if(project!=null&&project.getOtList()!=null){
////						otList=project.getOtList();
////					}
////				}
//				
//				if(empInfo.isReliever()){
//					if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
////						otList=project.getRelieversOtList();
//						otList.addAll(project.getRelieversOtList());
//						designation=info.getEmpDesignation();
//					}
//				}
//				
////				if(project.getOtList()!=null&&project.getOtList().size()!=0){
//////					otList=project.getOtList();
////					otList.addAll(project.getOtList());
////				}
//				/**
//				 * @author Vijay Date :- 08-09-2022
//				 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
//				 */
//				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
//				CommonServiceImpl commonservice = new CommonServiceImpl();
//				overtimelist = commonservice.getHRProjectOvertimelist(project);
//				
//				if(overtimelist!=null&&overtimelist.size()!=0){
//					otList.addAll(overtimelist);
//				}
//				
//				/**
//				 * ends here
//				 */
//				
//				
//				
//				logger.log(Level.SEVERE,"OT?DOT :: ");
//				double singleOt=0;
//				double doubleOt=0;
//				if(!info.getSingleOt().equalsIgnoreCase("NA")){
//					try{
//						singleOt=Double.parseDouble(info.getSingleOt());
//						if(singleOt!=0){
//							Overtime ot=getOvertimeName(otList,"OT",info.getEmpId(),designation);
//							if(ot==null){
//								msg=msg+"/ Single OT is not added in Project. ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//					}catch(Exception e){
//						msg=msg+"/ Please add proper value against single ot column. ";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//				}
//				
//				if(!info.getDoubleOt().equalsIgnoreCase("NA")){
//					try{
//						doubleOt=Double.parseDouble(info.getDoubleOt());
//						if(doubleOt!=0){
//							Overtime ot=getOvertimeName(otList,"DOT",info.getEmpId(),designation);
//							if(ot==null){
//								msg=msg+"/ Double OT is not added in Project. ";
//								attandanceList.get(0).setErrorCode(-1);
//							}
//						}
//					}catch(Exception e){
//						msg=msg+"/ Please add proper value against double ot column. ";
//						attandanceList.get(0).setErrorCode(-1);
//					}
//				}
//				
//				logger.log(Level.SEVERE,"SL "+singleOt+" DL "+doubleOt);
//			}
//			
//			
//			
//			
//			logger.log(Level.SEVERE,"msg"+msg);
//
//			if(msg!=null && !msg.trim().equals("")) {
//				
//				error.setEmpId(info.getEmpId());
//				error.setEmpName(info.getEmpName());
//				error.setProjectName(info.getProjName());
//				
//				error.setTotalNoOfDays(totalNoOfDays);
//				error.setTotalNoOfPresentDays(totalNoOfPresentDays);
//				error.setTotalNoOfWO(totalNoWeeklyOff);
//				error.setTotalNoOfHoliday(totalNoOfHoliday);
//				error.setTotalNoOfAbsentDays(totalNoOfAbsentDays);
//				
//				if(empInfo!=null&&!calendarFlag){
//				double leaveDays=totalLeave;
//				double extraDays=totalExtraHours/empInfo.getLeaveCalendar().getWorkingHours();
//				error.setTotalNoOfLeave(leaveDays);
//				error.setTotalNoOfExtraDays(extraDays);
//				}
//				
//				error.setRemark(msg);
//				errorList.add(error);
//				
//				attandanceList.get(0).setErrorCode(-1);
//
//			}
//				
//		}		
//		
//		ObjectifyService.reset();
//		ofy().consistency(Consistency.STRONG);
//		ofy().clear();
//		
//		CsvWriter.attendanceErrorList=errorList;
//
//		return attandanceList;
	}



	private ArrayList<String> getDatewiseAttendance(List<Attendance> existingAttendanceList, AttandanceInfo info) {
		ArrayList<String> list =new ArrayList<String>();

		try {
			for(Attendance attend:existingAttendanceList){
						if(info.getEmpId()==attend.getEmpId()){
							list.add(info.getEmpId()+""+fmt.format(attend.getAttendanceDate()));
						}
					}
			} catch (Exception e) {
			}
		
		return list;
	}



	@Override
	public ArrayList<SuperModel> validateAttandancepart3(ArrayList<AttandanceInfo> attandanceList,
			ArrayList<Integer> empList, ArrayList<Integer> projectidlist) {
		ArrayList<SuperModel> alldatalist = new ArrayList<SuperModel>();

		try {
			
			logger.log(Level.SEVERE, "attandanceList.get(0).getMonthYear()"+attandanceList.get(0).getMonthYear());
			logger.log(Level.SEVERE, "empList size "+empList.size());
			
			HashSet<String> hsProj=new HashSet<String>();
			
			
			for(AttandanceInfo info:attandanceList){
				hsProj.add(info.getProjName().trim());
			}
			ArrayList<String> projList=new ArrayList<String>(hsProj);
			logger.log(Level.SEVERE,"NO. OF Proj : "+projList.size());
			
			/**
			 * Date : 18-08-2018 BY ANIL
			 * Retrieving existing attendance , leave and ot info for overriding it.
			 */
//			List<Attendance> existingAttendanceList=getExistingAttendance(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList);
			List<Attendance> existingAttendanceList=getExistingAttendance(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList,projList);
			logger.log(Level.SEVERE,"existingAttendanceList"+existingAttendanceList.size());

			if(existingAttendanceList!=null && existingAttendanceList.size()>0) {
				for(Attendance existingattendance : existingAttendanceList) {
//					existingattendance.setDocumentName("ExistingAttendance");
				}
				alldatalist.addAll(existingAttendanceList);
			}
			logger.log(Level.SEVERE, "existingattendance size"+existingAttendanceList.size());
			logger.log(Level.SEVERE, "projectidlist size"+projectidlist.size());

			if(projectidlist!=null && projectidlist.size()!=0) {
				CommonServiceImpl commnservice = new CommonServiceImpl();
				 ArrayList<HrProjectOvertime> HrProjectOvertimelist = commnservice.getHrProjectOvertimelist(projectidlist, attandanceList.get(0).getCompanyId());
					logger.log(Level.SEVERE, "HrProjectOvertimelist "+HrProjectOvertimelist.size());
				 if(HrProjectOvertimelist!=null && HrProjectOvertimelist.size()>0) {
						for(HrProjectOvertime hrprojectovertime : HrProjectOvertimelist) {
//							hrprojectovertime.setDocumentName("HrProjectOvertime");
						}
						alldatalist.addAll(HrProjectOvertimelist);
				}
			}
			
			logger.log(Level.SEVERE, "All Data loaded here");

		} catch (Exception e) {
			e.printStackTrace();
			Attendance attendance = new Attendance();
//			attendance.setDocumentName("ERROR");
			alldatalist.add(attendance);
		}
		
		
		return alldatalist;
	}



	@Override
	public ArrayList<LeaveBalance> loadLeaveBalanceEntity(ArrayList<Integer> empList , long companyId) {
		
		try {
			
			List<LeaveBalance>empLeaveBalList=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount IN", empList).list();
			logger.log(Level.SEVERE,"EMPLOYEE LEAVE LIST SIZE : "+empLeaveBalList.size());
			
			if(empLeaveBalList.size()>0){
				ArrayList<LeaveBalance> list = new ArrayList<LeaveBalance>();
				list.addAll(empLeaveBalList);
				return list;
			}
			
		} catch (Exception e) {
		}
		
		return null;
		
	}



	@Override
	public ArrayList<EmployeeInfo> loadEmployeeInfoEntity(ArrayList<Integer> empList , long companyId) {
		
		try {
			
			List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
			if(empList.size()!=0){
				empInfoList = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empList).list();
				logger.log(Level.SEVERE,"EMPLOYEE INFO LIST SIZE : "+empInfoList.size());

				ArrayList<EmployeeInfo> list = new ArrayList<EmployeeInfo>();
				list.addAll(empInfoList);
				return list;
			}
			
		} catch (Exception e) {
		}
		
		return null;
	}





	@Override
	public ArrayList<HrProjectOvertime> loadHRprojectOvertimeEntity(
			ArrayList<AttandanceInfo> attandanceList,
			ArrayList<Integer> empList, ArrayList<Integer> projectidlist) {

		try {
			logger.log(Level.SEVERE, "projectidlist "+projectidlist);
			logger.log(Level.SEVERE, "empList "+empList);
			logger.log(Level.SEVERE, "attandanceList "+attandanceList);

			CommonServiceImpl commnservice = new CommonServiceImpl();
			 ArrayList<HrProjectOvertime> HrProjectOvertimelist = commnservice.getHrProjectOvertimelist(projectidlist, attandanceList.get(0).getCompanyId());
				logger.log(Level.SEVERE, "HrProjectOvertimelist "+HrProjectOvertimelist.size());
			 if(HrProjectOvertimelist!=null && HrProjectOvertimelist.size()>0) {
				 
				 ArrayList<HrProjectOvertime> list = new ArrayList<HrProjectOvertime>();
				 list.addAll(HrProjectOvertimelist);
				 return list;
			 }
		} catch (Exception e) {
		}
		
		
		return null;
	}




	@Override
	public ArrayList<Attendance> loadExistingAttendance(
			ArrayList<AttandanceInfo> attandanceList,
			ArrayList<Integer> empList,ArrayList<String> projList) {

		try {
			
			logger.log(Level.SEVERE, "attandanceList.get(0).getMonthYear()"+attandanceList.get(0).getMonthYear());
			logger.log(Level.SEVERE, "empList size "+empList.size());
			
//			HashSet<String> hsProj=new HashSet<String>();
//			
//			
//			for(AttandanceInfo info:attandanceList){
//				hsProj.add(info.getProjName().trim());
//			}
//			ArrayList<String> projList=new ArrayList<String>(hsProj);
//			logger.log(Level.SEVERE,"NO. OF Proj : "+projList.size());
			
			List<Attendance> existingAttendanceList=getExistingAttendance(attandanceList.get(0).getCompanyId(),attandanceList.get(0).getMonthYear(),empList,projList);
			logger.log(Level.SEVERE,"existingAttendanceList"+existingAttendanceList.size());

			
			if(existingAttendanceList!=null && existingAttendanceList.size()>0) {
				ArrayList<Attendance> list = new ArrayList<Attendance>();
				list.addAll(existingAttendanceList);
				
				return list;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return null;
	}



	@Override
	public ArrayList<HrProject> loadHRprojectEntity(ArrayList<String> projList,
			long companyId) {
		
		try {
			
			List<HrProject>projectList=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName IN", projList).filter("status", true).list();
			logger.log(Level.SEVERE,"PROJECT SIZE :: "+projectList.size());
			
			
			if(projectList!=null && projectList.size()>0) {
				ArrayList<HrProject> list = new ArrayList<HrProject>();
				list.addAll(projectList);
				return list;
			}
			
		} catch (Exception e) {
		}
		
		return null;
	}
}
