package com.slicktechnologies.server.cronjobinteration;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.slicktechnologies.server.cronjobimpl.DeliveryNoteCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.GoodsReceiptNoteCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.ServiceFumigationDetailReportCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.ServiceFumigationValueDetailReportCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.ServiceQuotationCronJobImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class GenericCronJobServiceDetailsImpl extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2721092474988163265L;
	Logger logger = Logger.getLogger("GenericCronJobimpl.class");
	long companyId=0;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		

		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		

		String urlcalled=req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlcalled:::::"+urlcalled);
		
		String url1=urlcalled.replace("http://", "");
		logger.log(Level.SEVERE,"url1:::::"+url1);
		String[] splitUrl=url1.split("\\.");
		logger.log(Level.SEVERE,"splitUrl[0]:::::"+splitUrl[0]);
		
		Company company = new Company();
		List<Company> compnayDetails=ofy().load().type(Company.class).list();
		
		
		for(int d = 0 ; d < compnayDetails.size() ; d++){
			company = compnayDetails.get(d);
			companyId=company.getCompanyId();
		
		
		
		List<ConfigCategory> cronJobList = ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("internalType", 29).list();

		
		
		logger.log(Level.SEVERE,"cron job list -- " + cronJobList.size() + " resp.isCommitted(); -- " + resp.isCommitted());
		ArrayList<String> cronList = new ArrayList<String>();
		for(ConfigCategory con : cronJobList){
			if(con.getStatus()){
				cronList.add(con.getCategoryName());
				logger.log(Level.SEVERE,"cron job list -- " + con.getCategoryName());
			}
			
		}
	
		if(cronList.size()>0){
			List<CronJobConfigration> cronConfigList = ofy().load().type(CronJobConfigration.class).filter("companyId", companyId)
					.filter("cronJobsName IN",cronList).filter("configStatus", true).list();
			
			
			logger.log(Level.SEVERE,"Cron list -- "+ cronConfigList.size());
			
			for(CronJobConfigration cron : cronConfigList){
			
//				CustomerServiceCronJobImpl
				List<CronJobConfigrationDetails> cronJobDetailList = new ArrayList<CronJobConfigrationDetails>();
				logger.log(Level.SEVERE," cron list llooop --- "+ cron.getCronJobsName());
				if(cron.getCronJobsName().equalsIgnoreCase("ServiceQuotationCronJobImpl")){
					HashSet<String> employeeRole = new HashSet<String>();
					for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
						
						if(cronDetails.getCronJobName().equalsIgnoreCase("ServiceQuotationCronJobImpl") && cronDetails.isStatus()){
							
							/*
							 * Commented by Ashwini
							 */
							
//							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//						
//							logger.log(Level.SEVERE," current date -- " + currentDate  +" created date --" + cronDetails.getCreatedDate());
//						       long difference = currentDate.getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
						     
							 /*
						      * Added by Ashwini
						      */
						    /*   logger.log(Level.SEVERE," Starting  Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = cronDetails.getStartingDate().getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
						    */
						       /*
						        * End by Ashwini
						        */
							
							/**
							 * nidhi
							 * 31-07-2018 for correction logic
							 */
							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//							
						       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
						        
							/**
							 * end
							 */
							
							logger.log(Level.SEVERE,"get diff -- "+difference);
						       float daysBetween = (difference / (1000*60*60*24));
							
						       int  diffDays = (int) daysBetween;
						       
						       if(cronDetails.getFrequencyDay()!=0 ){
						    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
						    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
						    		   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
						    	   }
						       }else if(cronDetails.getFrequencyDay()==0){
						    	   cronJobDetailList.add(cronDetails);
						    	   employeeRole.add(cronDetails.getEmployeeRole());
						       }
						
						}
					}
					
					if(cronJobDetailList.size()>0){
						Gson gson =  new Gson();
						String cronDetail = gson.toJson(cronJobDetailList);
						logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
						ServiceQuotationCronJobImpl contReport = new ServiceQuotationCronJobImpl();
						contReport.ServiceQuotationList(cronJobDetailList,employeeRole);
					}
				}
			
				if(cron.getCronJobsName().equalsIgnoreCase("DeliveryNoteCronJobImpl")){
					HashSet<String> employeeRole = new HashSet<String>();
					for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
						
						if(cronDetails.getCronJobName().equalsIgnoreCase("DeliveryNoteCronJobImpl") && cronDetails.isStatus()){
							
							/*
							 * Commented by Ashwini
							 */
//							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//						
//							logger.log(Level.SEVERE," current date -- " + currentDate  +" created date --" + cronDetails.getCreatedDate());
//						       long difference = currentDate.getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
						      
							 /*
						      * Added by Ashwini
						      */
						    /*   logger.log(Level.SEVERE," Starting  Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = cronDetails.getStartingDate().getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
							 */
							
							/**
							 * nidhi
							 * 31-07-2018 for correction logic
							 */
							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//							
						       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
						        
							/**
							 * end
							 */ 
							/*
						        * End by Ashwini
						        */
							logger.log(Level.SEVERE,"get diff -- "+difference);
						       float daysBetween = (difference / (1000*60*60*24));
							
						       int  diffDays = (int) daysBetween;
						       
						       if(cronDetails.getFrequencyDay()!=0 ){
						    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
						    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
						    		   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
						    	   }
						       }else if(cronDetails.getFrequencyDay()==0){
						    	   cronJobDetailList.add(cronDetails);
						    	   employeeRole.add(cronDetails.getEmployeeRole());
						       }
						
						}
					}
					
					if(cronJobDetailList.size()>0){
						Gson gson =  new Gson();
						String cronDetail = gson.toJson(cronJobDetailList);
						logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
						DeliveryNoteCronJobImpl contReport = new DeliveryNoteCronJobImpl();
						contReport.deliveryNote(cronJobDetailList,employeeRole);
					}
				}else if(cron.getCronJobsName().equalsIgnoreCase("GoodsReceiptNoteCronJobImpl")){
					HashSet<String> employeeRole = new HashSet<String>();
					for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
						
						if(cronDetails.getCronJobName().equalsIgnoreCase("GoodsReceiptNoteCronJobImpl") && cronDetails.isStatus()){
							/*
							 * Commented by Ashwini
							 */
//							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//						
//							logger.log(Level.SEVERE," current date -- " + currentDate  +" created date --" + cronDetails.getCreatedDate());
//						       long difference = currentDate.getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
						       
							 /*
						      * Added by Ashwini
						      */
						     /*  logger.log(Level.SEVERE," Starting  Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = cronDetails.getStartingDate().getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
							*/
						    
							/*
						        * End by Ashwini
						        */
							
							/**
							 * nidhi
							 * 31-07-2018 for correction logic
							 */
							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//							
						       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
						        
							/**
							 * end
							 */
							logger.log(Level.SEVERE,"get diff -- "+difference);
						       float daysBetween = (difference / (1000*60*60*24));
							
						       int  diffDays = (int) daysBetween;
						       
						       if(cronDetails.getFrequencyDay()!=0 ){
						    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
						    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
						    		   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
						    	   }
						       }else if(cronDetails.getFrequencyDay()==0){
						    	   cronJobDetailList.add(cronDetails);
						    	   employeeRole.add(cronDetails.getEmployeeRole());
						       }
						
						}
					}
					
					if(cronJobDetailList.size()>0){
						Gson gson =  new Gson();
						String cronDetail = gson.toJson(cronJobDetailList);
						logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
						GoodsReceiptNoteCronJobImpl contReport = new GoodsReceiptNoteCronJobImpl();
						contReport.GoodsRecievedNote(cronJobDetailList,employeeRole);
					}
				}else if(cron.getCronJobsName().trim().equalsIgnoreCase("CommodityFumigationReportCronJob")){
					for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
						
						if(cronDetails.getCronJobName().equalsIgnoreCase("CommodityFumigationReportCronJob") && cronDetails.isStatus()){
							/*
							 * Commented by Ashwini
							 */
//							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//						
//							logger.log(Level.SEVERE," current date -- " + currentDate  +" created date --" + cronDetails.getCreatedDate());
//						       long difference = currentDate.getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
						      
							 /*
						      * Added by Ashwini
						      */
						  /*     logger.log(Level.SEVERE," Starting  Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = cronDetails.getStartingDate().getTime() - DateUtility.getDateWithTimeZone("IST",cronDetails.getCreatedDate()).getTime();
							*/
						       /*
						        * End by Ashwini
						        */
							
							/**
							 * nidhi
							 * 31-07-2018 for correction logic
							 */
							Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//							
						       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
						       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
						        
							/**
							 * end
							 */
							logger.log(Level.SEVERE,"get diff -- "+difference +" cron freq-- "+cronDetails.getFrequencyDay());
						       float daysBetween = (difference / (1000*60*60*24));
							
						       int  diffDays = (int) daysBetween;
						       
						       if(cronDetails.getFrequencyDay()!=0 ){
						    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
						    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
						    		   cronJobDetailList.add(cronDetails);
						    	   }
						       }else if(cronDetails.getFrequencyDay()==0){
						    	   cronJobDetailList.add(cronDetails);
						       }
						
						}
					}
					
					if(cronJobDetailList.size()>0){
						Gson gson =  new Gson();
						String cronDetail = gson.toJson(cronJobDetailList);
						logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
						ServiceFumigationDetailReportCronJobImpl contReport = new ServiceFumigationDetailReportCronJobImpl();
						contReport.getServiceFumigation(cronDetail);
					}
				}
			}
		}
		
		}
		
	
	}

}
