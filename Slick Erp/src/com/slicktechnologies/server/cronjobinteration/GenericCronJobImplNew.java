package com.slicktechnologies.server.cronjobinteration;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.slicktechnologies.server.cronjobimpl.ComplaintServiceCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.ContractRenewalCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.CustomerARPaymentDueEmailToClientCronJob;
import com.slicktechnologies.server.cronjobimpl.CustomerPaymentARCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.CustomerServiceCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.EmployeeEarningComponentRenewalCronJob;
import com.slicktechnologies.server.cronjobimpl.ServiceFeedbackCronJobImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class GenericCronJobImplNew extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6925162115484611284L;

	Logger logger = Logger.getLogger("GenericCronJobImplNew.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		List<Company> companylist = ofy().load().type(Company.class).list();

		for(Company company : companylist){
			
			List<ConfigCategory> cronJobList = ofy().load().type(ConfigCategory.class).filter("companyId", company.getCompanyId()).filter("internalType", 29).list();
			logger.log(Level.SEVERE,"cron job list -- " + cronJobList.size() + " resp.isCommitted(); -- " + resp.isCommitted());
			ArrayList<String> cronList = new ArrayList<String>();
			for(ConfigCategory con : cronJobList){
				if(con.getStatus()){
					cronList.add(con.getCategoryName());
					logger.log(Level.SEVERE,"cron job list -- " + con.getCategoryName());
				}
				
			}
			
			if(cronList.size()>0){
				List<CronJobConfigration> cronConfigList = ofy().load().type(CronJobConfigration.class).filter("companyId", company.getCompanyId())
						.filter("cronJobsName IN",cronList).filter("configStatus", true).list();
				
				
				logger.log(Level.SEVERE,"Cron list -- "+ cronConfigList.size());
				
				for(CronJobConfigration cron : cronConfigList){
				
					List<CronJobConfigrationDetails> cronJobDetailList = new ArrayList<CronJobConfigrationDetails>();
					logger.log(Level.SEVERE," cron list llooop --- "+ cron.getCronJobsName());
					if(cron.getCronJobsName().equalsIgnoreCase("ComplaintServiceCronJobImpl")){

						HashSet<String> employeeRole = new HashSet<String>();
						for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
							
							if(cronDetails.getCronJobName().equalsIgnoreCase("ComplaintServiceCronJobImpl") && cronDetails.isStatus()){
								   Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
							       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
							       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
							        
								logger.log(Level.SEVERE,"get diff -- "+difference);
							       float daysBetween = (difference / (1000*60*60*24));
								
							       int  diffDays = (int) daysBetween;
							       
							       if(cronDetails.getFrequencyDay()!=0 ){
							    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
							    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
							    		   cronJobDetailList.add(cronDetails);
							    		   employeeRole.add(cronDetails.getEmployeeRole());
							    	   }
							       }else if(cronDetails.getFrequencyDay()==0){
							    	   cronJobDetailList.add(cronDetails);
							    	   employeeRole.add(cronDetails.getEmployeeRole());
							       }
							
							}
						}
						
						if(cronJobDetailList.size()>0){
							Gson gson =  new Gson();
							String cronDetail = gson.toJson(cronJobDetailList);
							logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
							ComplaintServiceCronJobImpl contReport = new ComplaintServiceCronJobImpl();
							contReport.servicecontactlist(cronJobDetailList,employeeRole);
						}
					
					}	
					else if(cron.getCronJobsName().equalsIgnoreCase("EmployeeEarningComponentRenewalCronJob")){

							HashSet<String> employeeRole = new HashSet<String>();
							for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
								
								if(cronDetails.getCronJobName().equalsIgnoreCase("EmployeeEarningComponentRenewalCronJob") && cronDetails.isStatus()){
									   Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
								       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
								       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
								        
									logger.log(Level.SEVERE,"get diff -- "+difference);
								       float daysBetween = (difference / (1000*60*60*24));
									
								       int  diffDays = (int) daysBetween;
								       
								       if(cronDetails.getFrequencyDay()!=0 ){
								    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
								    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
								    		   cronJobDetailList.add(cronDetails);
								    		   employeeRole.add(cronDetails.getEmployeeRole());
								    	   }
								       }else if(cronDetails.getFrequencyDay()==0){
								    	   cronJobDetailList.add(cronDetails);
								    	   employeeRole.add(cronDetails.getEmployeeRole());
								       }
								
								}
							}
							
							if(cronJobDetailList.size()>0){
								Gson gson =  new Gson();
								String cronDetail = gson.toJson(cronJobDetailList);
								logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
								EmployeeEarningComponentRenewalCronJob contReport = new EmployeeEarningComponentRenewalCronJob();
								contReport.loadDuecomponent(cronJobDetailList,employeeRole);
							}
						
					}
					else if(cron.getCronJobsName().equalsIgnoreCase("ServiceFeedbackCronJobImpl")){
						HashSet<String> employeeRole = new HashSet<String>();

						for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
							
							if(cronDetails.getCronJobName().equalsIgnoreCase("ServiceFeedbackCronJobImpl") && cronDetails.isStatus()){
								   Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
							       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
							       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
							        
								logger.log(Level.SEVERE,"get diff -- "+difference);
							       float daysBetween = (difference / (1000*60*60*24));
								
							       int  diffDays = (int) daysBetween;
							       
							       if(cronDetails.getFrequencyDay()!=0 ){
							    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
							    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
							    		   cronJobDetailList.add(cronDetails);
							    		   employeeRole.add(cronDetails.getEmployeeRole());
							    	   }
							       }else if(cronDetails.getFrequencyDay()==0){
							    	   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
							       }
							
							}
						}
						
						if(cronJobDetailList.size()>0){
							Gson gson =  new Gson();
							String cronDetail = gson.toJson(cronJobDetailList);
							logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
							ServiceFeedbackCronJobImpl contReport = new ServiceFeedbackCronJobImpl();
							contReport.servicelist(cronJobDetailList,employeeRole);
						}
					
					}
					/**
					 * @author Ashwini Patil
					 * @since 23-04-2022
					 * Payment Due reminder cron job
					 */
					else if(cron.getCronJobsName().equalsIgnoreCase("CustomerARPaymentDueEmailToClientCronJob")){
						
						HashSet<String> employeeRole = new HashSet<String>();

						for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
							
							if(cronDetails.getCronJobName().equalsIgnoreCase("CustomerARPaymentDueEmailToClientCronJob") && cronDetails.isStatus()){
						
								/**
								 * nidhi
								 * 31-07-2018 for correction logic
								 */
								Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//								
							       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
							       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
							        
								/**
								 * end
								 */
							       
								logger.log(Level.SEVERE,"get diff -- "+difference);
							       float daysBetween = (difference / (1000*60*60*24));
								
							       int  diffDays = (int) daysBetween;
							       
							      
							       if(cronDetails.getFrequencyDay()!=0 ){
							    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
							    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
							    		   cronJobDetailList.add(cronDetails);
							    		   employeeRole.add(cronDetails.getEmployeeRole());
							    	   }
							       }else if(cronDetails.getFrequencyDay()==0){
							    	   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
							       }
							
							
							}
						}
						
						if(cronJobDetailList.size()>0){
							Gson gson =  new Gson();
							String cronDetail = gson.toJson(cronJobDetailList);
							logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
							CustomerARPaymentDueEmailToClientCronJob paymentDueReport = new CustomerARPaymentDueEmailToClientCronJob();
							paymentDueReport.customerPaymentARlistForCronJob(cronDetail,employeeRole ); 
						}
					}
					/**
					 * @author Sheetal  
					 * @since 02-05-2022
					 * contract renewal reminder to client cron job
					 */
					else if(cron.getCronJobsName().equalsIgnoreCase("ContractRenewalCronJobImpl")){
						HashSet<String> employeeRole = new HashSet<String>();

		                  for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
						    if(cronDetails.getCronJobName().equalsIgnoreCase("ContractRenewalCronJobImpl") && cronDetails.isStatus()){
																
						    	/**
								 * nidhi
								 * 31-07-2018 for correction logic
								 */
								Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//								
							       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
							       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
							        
								/**
								 * end
								 */
							       
								logger.log(Level.SEVERE,"get diff -- "+difference);
							       float daysBetween = (difference / (1000*60*60*24));
								
							       int  diffDays = (int) daysBetween;
							       
							      
							       if(cronDetails.getFrequencyDay()!=0 ){
							    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
							    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
							    		   cronJobDetailList.add(cronDetails);
							    		   employeeRole.add(cronDetails.getEmployeeRole());
							    	   }
							       }else if(cronDetails.getFrequencyDay()==0){
							    	   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
							       }
							
							
							}
						}
						
														
		                  if(cronJobDetailList.size()>0){
								Gson gson =  new Gson();
								String cronDetail = gson.toJson(cronJobDetailList);
								logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
							ContractRenewalCronJobImpl contReport = new ContractRenewalCronJobImpl();
						   contReport.contractRenewal(cronDetail,req,employeeRole);
						}
					}
				}
		
			}
		}	
	}
}
