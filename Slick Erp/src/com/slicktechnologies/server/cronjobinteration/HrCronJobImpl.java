package com.slicktechnologies.server.cronjobinteration;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


















import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class HrCronJobImpl extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8052848670268303038L;
	Logger logger = Logger.getLogger("HR CRON JOB LOGGER");
	long companyId=0;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
		String calledUrl=req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlcalled:::::"+calledUrl);
		String url=calledUrl.replace("http://", "");
		logger.log(Level.SEVERE,"url1:::::"+url);
		String[] splitUrl=url.split("\\.");
		logger.log(Level.SEVERE,"splitUrl[0]:::::"+splitUrl[0]);
		
		List<Company> compnayDetails=ofy().load().type(Company.class).list();
		Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
		for(Company company:compnayDetails){
			companyId=company.getCompanyId();
			ArrayList<String> hrCronJobNameList = new ArrayList<String>();
			hrCronJobNameList.add("EmployeeBirthdayReminder");
			hrCronJobNameList.add("EmployeeRetirementReminder");
			hrCronJobNameList.add("EmployeeProjectAllocationReminder");
			/**
			 * @author Anil ,Date : 25-03-2019
			 */
			hrCronJobNameList.add("MissingAttendanceCronJob");
			
	/***Date 14-3-2019 
			 * @author Amol 
			 * added cron job for Not approved Employee and KYC pending Employee
			 */
			hrCronJobNameList.add("KYCPendingReminder");
			/**
			 * @author Anil , Date : 25-05-2019
			 */
			hrCronJobNameList.add("EmployeeFnfReminder");
			
			List<CronJobConfigration> hrCronJobConfigurationList = ofy().load().type(CronJobConfigration.class).filter("companyId", companyId)
					.filter("cronJobsName IN",hrCronJobNameList).filter("configStatus", true).list();
			
			logger.log(Level.SEVERE,"CRON JOB CONFIGURATION LIST SIZE :"+ hrCronJobConfigurationList.size());
			if(hrCronJobConfigurationList!=null){
				List<CronJobConfigrationDetails> cronJobDetailsList = new ArrayList<CronJobConfigrationDetails>();
				for(CronJobConfigration cronJob : hrCronJobConfigurationList){
//					logger.log(Level.SEVERE,"CRON JOB NAME : "+ cronJob.getCronJobsName());
					
						
					for(CronJobConfigrationDetails cronJobDetails :cronJob.getCronJobsProcessList()){
//						logger.log(Level.SEVERE,"CRON JOB STARTING DATE : " + cronJobDetails.getStartingDate()  +"CRON JOB CREATION DATE : " + cronJobDetails.getCreatedDate());
						long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronJobDetails.getStartingDate()).getTime();
						float daysBetween = (difference / (1000*60*60*24));
						int  diffInDays = (int) daysBetween;
						logger.log(Level.SEVERE,"DIFFERENCE IN CURRENT DATE AND STARTING DATE : "+diffInDays);
						
						if(cronJobDetails.isStatus()){
							if(cronJobDetails.getFrequencyDay()!=0 ){
								logger.log(Level.SEVERE, "FREQUENCY DIFFERENCE : " + diffInDays % cronJobDetails.getFrequencyDay());
								if( diffInDays % cronJobDetails.getFrequencyDay() == 0){
									cronJobDetailsList.add(cronJobDetails);
								}
							}else if(cronJobDetails.getFrequencyDay()==0){
								cronJobDetailsList.add(cronJobDetails);
							}
						}
					}
				}
				
				for(CronJobConfigrationDetails cronJob:cronJobDetailsList){
					sendReminder(cronJob,company,currentDate);
				}
				
				
				
			}
		}
	}

	private void sendReminder(CronJobConfigrationDetails cronJob,Company company,Date currentDate) {
		
		logger.log(Level.SEVERE,"CRON JOB NAME : "+ cronJob.getCronJobName());
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		int currentYear=0;
		try{
			currentYear=Integer.parseInt(yearFormat.format(currentDate));
		}catch(Exception e){
			
		}
		logger.log(Level.SEVERE, "CURRENT YEAR" + currentYear);
		
		int diff = 0;
		if (cronJob.isOverdueStatus() && cronJob.getOverdueDays() > 0) {
			diff = cronJob.getOverdueDays() * -1;
		} else {
			diff = cronJob.getInterval();
		}
		
		logger.log(Level.SEVERE, "INTERVAL/FOLLOW UP " + diff);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, diff);
		Date previousDate = calendar.getTime();
		String todayDateinString = sdf.format(previousDate);

		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(previousDate);

		Date plusDaysDate = null;

		try {
			if(cronJob.isOverdueStatus()==false){
				plusDaysDate = sdf.parse(sdf.format(cal3.getTime()));
			}else{
				plusDaysDate = sdf.parse(sdf.format(new Date()));
			}
			cal3.setTime(plusDaysDate);
			cal3.set(Calendar.HOUR_OF_DAY, 23);
			cal3.set(Calendar.MINUTE, 59);
			cal3.set(Calendar.SECOND, 59);
			cal3.set(Calendar.MILLISECOND, 999);
			plusDaysDate = cal3.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Date minusDaysDate = null;

		try {
			if(cronJob.isOverdueStatus()==false){
				minusDaysDate = sdf.parse(sdf.format(new Date()));
			}else{
				minusDaysDate = sdf.parse(sdf.format(previousDate));
			}
			calendar.setTime(minusDaysDate);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			minusDaysDate = calendar.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
//		if (cronJob.isOverdueStatus()) {
//			try {
//				calendar.add(Calendar.DATE, cronJob.getOverdueDays());
//				minusDaysDate = sdf.parse(sdf.format(new Date()));
//				calendar.set(Calendar.HOUR_OF_DAY,0);
//				calendar.set(Calendar.MINUTE, 0);
//				calendar.set(Calendar.SECOND, 0);
//				calendar.set(Calendar.MILLISECOND,0);
//				minusDaysDate = calendar.getTime();
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
//		} 

		logger.log(Level.SEVERE, " minum date -- " + minusDaysDate+ " max days --" + plusDaysDate);
		List<Employee> employeeRoleList = new ArrayList<Employee>();
		employeeRoleList = ofy().load().type(Employee.class).filter("companyId", companyId)
				.filter("roleName",cronJob.getEmployeeRole()).list();
		
		logger.log(Level.SEVERE,"ROLE NAME" + cronJob.getEmployeeRole() + " "+ "ROLE WISE EMP LIST : "+ employeeRoleList.size());

		label:
		for (Employee employee : employeeRoleList) {
			ArrayList<String> toEmailList = new ArrayList<String>();
			toEmailList.add(employee.getEmail().trim());
			List<String> branchList = new ArrayList<String>();
			for (int i = 0; i < employee.getEmpBranchList().size(); i++) {
				branchList.add(employee.getEmpBranchList().get(i).getBranchName());
			}
			logger.log(Level.SEVERE,"toEmailList size::: " + toEmailList.size()+ " Branch List SIze : "+ branchList.size());

			/**
			 * @author Anil,Date : 25-03-2019
			 * Missing attendance cron job
			 */
			if(cronJob.getCronJobName().equalsIgnoreCase("MissingAttendanceCronJob")){
				sendMissingAttendanceCronJob(cronJob,company,currentDate,employee,branchList,toEmailList);
				continue label;
			}
			
			List<Employee> employeeList = new ArrayList<Employee>();
			
			if (branchList.size() != 0) {
				if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeBirthdayReminder")){
					employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
							.filter("branchName IN", branchList).filter("status", true).list();
					
				}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeRetirementReminder")){
					employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
							.filter("dateOfRetirement >=",minusDaysDate).filter("dateOfRetirement <=",plusDaysDate)
							.filter("branchName IN", branchList).filter("status", true).list();
					
				}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeProjectAllocationReminder")){
					ArrayList<String> projectNameList=new ArrayList<String>();
					projectNameList.add("");
					employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
							.filter("joinedAt >=",minusDaysDate).filter("joinedAt <=",plusDaysDate)
							.filter("branchName IN", branchList).filter("projectName IN", projectNameList)
							.filter("status", true).list();
				}
				/***Date 5-3-2019 added by Amol ****/
				else if(cronJob.getCronJobName().equalsIgnoreCase("KYCPendingReminder")){
					employeeList = ofy().load().type(Employee.class).filter("companyId",companyId).filter("branchName IN", branchList).list();
				}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeFnfReminder")){
					employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
							.filter("lastWorkingDate >=",minusDaysDate).filter("lastWorkingDate <=",plusDaysDate)
							.filter("branchName IN", branchList).filter("fnfStatus", "Initiated").list();
				}
			}
			
			ArrayList<Employee> selectedEmpList=new ArrayList<Employee>();
			
			
			if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeBirthdayReminder")){
				for(Employee emp:employeeList){
					calendar.add(Calendar.DATE, cronJob.getOverdueDays());
					Date empDob = null;
					if(emp.getDob()!=null){
						try {						
							empDob = sdf.parse(sdf.format(emp.getDob()));
//							logger.log(Level.SEVERE, "BEFORE EMP DOB ::"+ empDob);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						calendar.setTime(empDob);
						calendar.set(Calendar.HOUR_OF_DAY,0);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.MILLISECOND,0);
						calendar.set(Calendar.YEAR, currentYear);
						empDob=calendar.getTime();
//						logger.log(Level.SEVERE, "EMP DOB ::"+ empDob);
						if(empDob.after(minusDaysDate)&&empDob.before(plusDaysDate)){
							selectedEmpList.add(emp);
						}
					
					}
				}
			}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeRetirementReminder")){
				if(employeeList.size()!=0){
					selectedEmpList.addAll(employeeList);
				}
			}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeProjectAllocationReminder")){
				if(employeeList.size()!=0){
					selectedEmpList.addAll(employeeList);
				}
			}else if(cronJob.getCronJobName().equalsIgnoreCase("KYCPendingReminder")){
				for(Employee emp : employeeList){
					
					if(emp.isResigned()==false&&emp.isAbsconded()==false&&emp.isTerminated()==false){
				if(emp.isStatus()==false||emp.getAadharNumber()==0
						||emp.getPhoto()==null||emp.getPhoto().getUrl().equals("")
						||emp.getUploadAddDet()==null||emp.getUploadAddDet().getUrl().equals("")){
					        selectedEmpList.add(emp);
					}
					}
				
				}
			}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeFnfReminder")){
				if(employeeList.size()!=0){
					selectedEmpList.addAll(employeeList);
				}
			}
			
			logger.log(Level.SEVERE, "EMP List Size ::"+ selectedEmpList.size());

			/**
			 * Email Header
			 */
			ArrayList<String> tbl_header = new ArrayList<String>();
			if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeBirthdayReminder")){
				tbl_header.add("Date of Birth");
			}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeRetirementReminder")){
				tbl_header.add("Date of Retirement");
			}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeProjectAllocationReminder")){
				tbl_header.add("Date of Joining");
			}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeFnfReminder")){
				tbl_header.add("Last date of working");
			}
			
			tbl_header.add("Employee Id");
			tbl_header.add("Employee Name");
			tbl_header.add("Employee Cell");
			tbl_header.add("Email");
			tbl_header.add("Branch");
			tbl_header.add("Department");
			tbl_header.add("Employee Type");
			tbl_header.add("Designation");
			tbl_header.add("Project");
			
			

			ArrayList<String> tbl1 = new ArrayList<String>();
			int count = 0;
			for (Employee obj : selectedEmpList) {
				
				if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeBirthdayReminder")){
					tbl1.add(sdf.format(obj.getDob()));
				}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeRetirementReminder")){
					tbl1.add(sdf.format(obj.getDateOfRetirement()));
				}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeProjectAllocationReminder")){
					tbl1.add(sdf.format(obj.getJoinedAt()));
				}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeFnfReminder")){
					tbl1.add(sdf.format(obj.getLastWorkingDate()));
				}
				
				tbl1.add(obj.getCount() + "");
				tbl1.add(obj.getFullName());
				tbl1.add(obj.getCellNumber1()+"");
				tbl1.add(obj.getEmail());
				tbl1.add(obj.getBranchName());
				tbl1.add(obj.getDepartMent());
				tbl1.add(obj.getEmployeeType());
				tbl1.add(obj.getDesignation());
				tbl1.add(obj.getProjectName());
				
				count = count + 1;
			}
			
			/**
			 * Date 14-3-2019 
			 * @author Amol
			 * added cronjob for Not approved Employee  and KYC Pending Employee
			 */
			if(cronJob.getCronJobName().equalsIgnoreCase("KYCPendingReminder")){
				tbl_header = new ArrayList<String>();
				tbl1 = new ArrayList<String>();
				tbl_header=getKycPendingHeaders();
				tbl1=getKycPendingValues(selectedEmpList);
			}
			
			/**
			 * 
			 */
			
			
			

			/**
			 * Email Parts ends here
			 */
			Email cronEmail = new Email();
			String emailSubject ="HR Reminder";
			try {
				if (selectedEmpList.size() != 0) {
					String footer = "";
					if (cronJob.getFooter().trim() != "") {
						footer = cronJob.getFooter();
					}
					
					if (!cronJob.getSubject().trim().equals("")&& cronJob.getSubject().trim().length() > 0) {
						emailSubject = cronJob.getSubject();
					}

					String emailMailBody = "";
					if (!cronJob.getMailBody().trim().equals("")&& cronJob.getMailBody().trim().length() > 0) {
						emailMailBody = cronJob.getMailBody();
					}

//					if (cronJob.isOverdueStatus()) {
//						footer = footer+ "<br>"+ "<Br>#This mail contains last 3 months data. For more records please contact Email- support@evasoftwaresolutions.com";
//					}
					logger.log(Level.SEVERE, "subject -- "+ emailSubject);
					cronEmail.cronSendEmail(toEmailList,emailSubject, emailMailBody, company,
							tbl_header, tbl1, null, null, null,null, "", footer);
				} else {
					
					if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeBirthdayReminder")){
						emailSubject="No upcoming birthday. ";
					}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeRetirementReminder")){
						emailSubject="No retirement record found. ";
					}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeProjectAllocationReminder")){
						emailSubject="No project allocation pending. ";
					}else if(cronJob.getCronJobName().equalsIgnoreCase("KYCPendingReminder")){
						emailSubject="No KYC Is Pending .";
					}else if(cronJob.getCronJobName().equalsIgnoreCase("EmployeeFnfReminder")){
						emailSubject="No FNF pending. ";
					}
					/**
					 * If no Birthday reminder found
					 */
					if(toEmailList!=null&&toEmailList.size()>0)
						cronEmail.cronSendEmail(toEmailList,emailSubject + " "+ todayDateinString,
							emailSubject, company, null,null, null, null, null, null);
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE,"Error in cronEmail method:::" + e);
			}
		}
			
	}
	private ArrayList<String> getKycPendingValues(ArrayList<Employee> selectedEmpList) {
		// TODO Auto-generated method stub
		ArrayList<String> tbl1 = new ArrayList<String>();
		int count = 0;
		for (Employee obj : selectedEmpList) {
				String comment="";
				String status="";
			    tbl1.add(obj.getCount() + "");
				tbl1.add(obj.getFullName());
				tbl1.add(obj.getDesignation());
				tbl1.add(obj.getBranchName());
				tbl1.add(obj.getProjectName());
				tbl1.add(obj.getAadharNumber()+"");
				
				if(obj.isStatus()==true){
					status="Approved";
				}else{
					status="Not Approved";
				}
				tbl1.add(status);
				if(obj.getPhoto()==null){
					comment=comment+"Photo not uploaded."+"\n";
				}
				if(obj.getUploadAddDet()==null){
					comment=comment+"KYC doc not uploaded.";
				}
				tbl1.add(comment);
				count = count + 1;
		}
		return tbl1;
	}
private ArrayList<String> getKycPendingHeaders() {
		 
		ArrayList<String> tbl_header = new ArrayList<String>();
	    tbl_header.add("Employee ID");
		tbl_header.add("Employee Name");
		tbl_header.add("Designation");
		tbl_header.add("Branch");
		tbl_header.add("Project");
		tbl_header.add("Aadhar Number");
		tbl_header.add("Status");
		tbl_header.add("Comment");
		
		
		return tbl_header;
	}
	private void sendMissingAttendanceCronJob(CronJobConfigrationDetails cronJob, Company company,
			Date currentDate, Employee employee, List<String> branchList, ArrayList<String> toEmailList) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"START OF MISSING ATTENDANCE CRON JOB ######################");
		try{
			HashMap<String,TreeMap<Date,ArrayList<MissingAttendanceDetails>>> projWiseMissingAttendanceMap=new HashMap<String, TreeMap<Date,ArrayList<MissingAttendanceDetails>>>();
			
			SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			companyId=company.getCompanyId();
			
			logger.log(Level.SEVERE,"Inside missing attendance cron Job : "+currentDate+" :: " + employee.getFullName());
			String month=sdf.format(currentDate);
			Date startDateOfMonth=DateUtility.getStartDateofMonth(currentDate);
			Date endDateOfMonth=DateUtility.getEndDateofMonth(currentDate);
			logger.log(Level.SEVERE,"MONTH : "+month+" :: Start date " + startDateOfMonth+" :: end date "+endDateOfMonth);
			
			List<HrProject> projectList=null;
			logger.log(Level.SEVERE,"Branch list : "+branchList);
			if(branchList!=null&&branchList.size()!=0){
				projectList=ofy().load().type(HrProject.class).filter("companyId", company.getCompanyId()).filter("branch IN", branchList).filter("status", true).list();
			}else{
				logger.log(Level.SEVERE,"No Branch is assigned.");
			}
			
			if(projectList!=null&&projectList.size()!=0){
				
				for(HrProject project:projectList){
					
					/**
					 * @author Vijay Date :- 08-09-2022
					 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
					 */
					ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
					CommonServiceImpl commonservice = new CommonServiceImpl();
					overtimelist = commonservice.getHRProjectOvertimelist(project);
					/**
					 * ends here
					 */
//					if(project.getOtList()!=null&&project.getOtList().size()!=0){
					if(overtimelist!=null&&overtimelist.size()!=0){
						TreeMap<Date,ArrayList<MissingAttendanceDetails>> dateAttendanceMap=new TreeMap<Date,ArrayList<MissingAttendanceDetails>>();
						HashSet<Integer> empIdHs=new HashSet<Integer>();
						for(Overtime ot:overtimelist){
							empIdHs.add(ot.getEmpId());
						}
						logger.log(Level.SEVERE,"Project : "+project.getProjectName()+" Total no. of employee : "+empIdHs.size());
						ArrayList<Integer>empIdList=new ArrayList<Integer>(empIdHs);
						dateAttendanceMap=getProjectWiseMissedAttendance(empIdList,month,companyId,startDateOfMonth,endDateOfMonth,currentDate,project.getProjectName(),project.getBranch(),project);
						
						if(dateAttendanceMap!=null&&dateAttendanceMap.size()!=0){
							if(projWiseMissingAttendanceMap!=null&&projWiseMissingAttendanceMap.size()!=0){
								if(projWiseMissingAttendanceMap.containsKey(project.getProjectName())){
	//								projWiseMissingAttendanceMap.get(project.getProjectName()).put(arg0, arg1)
								}else{
									projWiseMissingAttendanceMap.put(project.getProjectName(), dateAttendanceMap);
								}
							}else{
								projWiseMissingAttendanceMap.put(project.getProjectName(), dateAttendanceMap);
							}
						}
					}
				}
				
				logger.log(Level.SEVERE,"Project Map size : "+projWiseMissingAttendanceMap.size());
				
			}else{
				logger.log(Level.SEVERE,"No Project found for selected branch .");
			}
			
			String emailSubject ="Missing Attendance ";
			String todayDateinString = sdf1.format(currentDate);
			
			if (!cronJob.getSubject().trim().equals("")&& cronJob.getSubject().trim().length() > 0) {
				emailSubject = cronJob.getSubject();
			}
			String emailMailBody = "";
			if (!cronJob.getMailBody().trim().equals("")&& cronJob.getMailBody().trim().length() > 0) {
				emailMailBody = cronJob.getMailBody();
			}
			if(projWiseMissingAttendanceMap.size()!=0){
				/**
				 * Send Mail logic
				 */
				Email cronEmail = new Email();
				cronEmail.cronJobEmailForMissingAttendance(toEmailList, emailSubject+" "+todayDateinString, emailMailBody, company, projWiseMissingAttendanceMap);
				
			}else{
				/**
				 * No missing attendance found.
				 */
				Email cronEmail = new Email();
				cronEmail.cronJobEmailForMissingAttendance(toEmailList, emailSubject+" "+todayDateinString, "No missing attendance found.", company, null);
				
			}
			
			
		}catch(Exception e){
			logger.log(Level.SEVERE,"Exception Occured "+e.toString());
		}
		logger.log(Level.SEVERE,"###################### END OF MISSING ATTENDANCE CRON JOB");
		
	}
	
	private TreeMap<Date, ArrayList<MissingAttendanceDetails>> getProjectWiseMissedAttendance(ArrayList<Integer> empIdList,
			String month, long companyId, Date startDateOfMonth,Date endDateOfMonth, Date currentDate, String projectName, String branch,HrProject project) {
		
		List<Attendance> attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("projectName", projectName).filter("month", month).filter("empId IN", empIdList).list();
		
		if(attendanceList!=null&&attendanceList.size()!=0){
			logger.log(Level.SEVERE,"Attendance List Size : "+attendanceList.size());
//			return null;
		}else{
			logger.log(Level.SEVERE,"No attendance found.");
		}
		
		Date toDate=DateUtility.getDateWithTimeZone("IST", currentDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(toDate);
		cal.add(Calendar.DATE, -1);
		toDate = cal.getTime();
		
		logger.log(Level.SEVERE,"Before From Date : "+startDateOfMonth+" To Date : "+toDate);
		
		TreeMap<Date,ArrayList<MissingAttendanceDetails>> dateAttendanceMap=new TreeMap<Date,ArrayList<MissingAttendanceDetails>>();
		while(startDateOfMonth.equals(toDate)||startDateOfMonth.before(toDate)){
			for(Integer empId:empIdList){
				MissingAttendanceDetails missingAttendance=getMissingAttendance(empId,startDateOfMonth,attendanceList,month,project);
				if(missingAttendance!=null){
					if(dateAttendanceMap!=null&&dateAttendanceMap.size()!=0){
						if(dateAttendanceMap.containsKey(startDateOfMonth)){
							dateAttendanceMap.get(startDateOfMonth).add(missingAttendance);
						}else{
							ArrayList<MissingAttendanceDetails> list=new ArrayList<HrCronJobImpl.MissingAttendanceDetails>();
							list.add(missingAttendance);
							dateAttendanceMap.put(startDateOfMonth, list);
						}
					}else{
						ArrayList<MissingAttendanceDetails> list=new ArrayList<HrCronJobImpl.MissingAttendanceDetails>();
						list.add(missingAttendance);
						dateAttendanceMap.put(startDateOfMonth, list);
					}
				}
			}
			
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(startDateOfMonth);
			cal1.add(Calendar.DATE, 1);
			startDateOfMonth = cal1.getTime();
		}
		logger.log(Level.SEVERE,"After From Date : "+startDateOfMonth+" To Date : "+toDate+" MAP SIZE : "+dateAttendanceMap.size());
		
		return dateAttendanceMap;
		
	}

	private MissingAttendanceDetails getMissingAttendance(Integer empId, Date startDateOfMonth, List<Attendance> attendanceList, String month,HrProject project) {
		boolean attendanceFoundFlag=false;
		Overtime ot=null;
		
		/**
		 * @author Vijay Date :- 08-09-2022
		 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
		 */
		ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
		CommonServiceImpl commonservice = new CommonServiceImpl();
		overtimelist = commonservice.getHRProjectOvertimelist(project);
		/**
		 * ends here
		 */
//		for(Overtime obj:project.getOtList()){
		for(Overtime obj:overtimelist){
			if(obj.getEmpId()==empId){
				ot=obj;
				break;
			}
		}
		
		for(Attendance attendance:attendanceList ){
			if(empId==attendance.getEmpId()&&startDateOfMonth.equals(attendance.getAttendanceDate())
					&&attendance.getProjectName().equals(project.getProjectName())){
				attendanceFoundFlag=true;
				if(!attendance.getStatus().equals("Approved")){
					MissingAttendanceDetails obj=new MissingAttendanceDetails(project.getProjectName(), project.getBranch(), month, project.getBranchManager(), project.getManager(), project.getSupervisor(), empId, ot.getEmpName(), 0, ot.getEmpDesignation(), startDateOfMonth, "Attendance not approved/submitted.");
					return obj;
				}
			}
		}
		if(attendanceFoundFlag==false){
			MissingAttendanceDetails obj=new MissingAttendanceDetails(project.getProjectName(), project.getBranch(), month, project.getBranchManager(), project.getManager(), project.getSupervisor(), empId, ot.getEmpName(), 0, ot.getEmpDesignation(), startDateOfMonth, "Attendance not marked.");
			return obj;
		}
		
		return null;
	}

	/**
	 * 
	 * Project Name
	 * Branch
	 * Month
	 * Branch Manager
	 * Manager
	 * Supervisor
	 * 
	 * 
	 * Sr. No.
	 * Emp Id
	 * Emp Name
	 * Emp Cell
	 * Designation
	 * Attendance Date
	 * Remark
	 */
	
	public class MissingAttendanceDetails{
		String projectName;
		String branchName;
		String month;
		String branchManager;
		String manager;
		String supervisor;
		int empId;
		String empName;
		long empCell;
		String designation;
		Date attendanceDate;
		String remark;
		
		public MissingAttendanceDetails(String projectName,String branchName,String month,String branchManager,String manager,String supervisor,
				int empId,String empName,long empCell,String designation,Date attendanceDate,String remark) {
			// TODO Auto-generated constructor stub
			this.projectName=projectName;
			this.branchName=branchName;
			this.month=month;
			this.branchManager=branchManager;
			this.manager=manager;
			this.supervisor=supervisor;
			this.empId=empId;
			this.empName=empName;
			this.empCell=empCell;
			this.designation=designation;
			this.attendanceDate=attendanceDate;
			this.remark=remark;
		}

		public String getProjectName() {
			return projectName;
		}

		public void setProjectName(String projectName) {
			this.projectName = projectName;
		}

		public String getBranchName() {
			return branchName;
		}

		public void setBranchName(String branchName) {
			this.branchName = branchName;
		}

		public String getMonth() {
			return month;
		}

		public void setMonth(String month) {
			this.month = month;
		}

		public String getBranchManager() {
			return branchManager;
		}

		public void setBranchManager(String branchManager) {
			this.branchManager = branchManager;
		}

		public String getManager() {
			return manager;
		}

		public void setManager(String manager) {
			this.manager = manager;
		}

		public String getSupervisor() {
			return supervisor;
		}

		public void setSupervisor(String supervisor) {
			this.supervisor = supervisor;
		}

		public int getEmpId() {
			return empId;
		}

		public void setEmpId(int empId) {
			this.empId = empId;
		}

		public String getEmpName() {
			return empName;
		}

		public void setEmpName(String empName) {
			this.empName = empName;
		}

		public long getEmpCell() {
			return empCell;
		}

		public void setEmpCell(long empCell) {
			this.empCell = empCell;
		}

		public String getDesignation() {
			return designation;
		}

		public void setDesignation(String designation) {
			this.designation = designation;
		}

		public Date getAttendanceDate() {
			return attendanceDate;
		}

		public void setAttendanceDate(Date attendanceDate) {
			this.attendanceDate = attendanceDate;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}
		
		
		
		
		
	}

}