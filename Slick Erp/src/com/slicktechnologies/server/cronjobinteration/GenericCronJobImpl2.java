package com.slicktechnologies.server.cronjobinteration;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.cronjobimpl.ComplaintServiceCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.ContractRenewalCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.CustomerARPaymentDueEmailToClientCronJob;
import com.slicktechnologies.server.cronjobimpl.CustomerPaymentARCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.CustomerServiceCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.EVAERPUsageReportCronJobImpl;
import com.slicktechnologies.server.cronjobimpl.EmployeeEarningComponentRenewalCronJob;
import com.slicktechnologies.server.cronjobimpl.OpenAuditRemindersCronJob;
import com.slicktechnologies.server.cronjobimpl.ServiceFeedbackCronJobImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class GenericCronJobImpl2 extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6925162115484611284L;

	Logger logger = Logger.getLogger("GenericCronJobImpl2.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.log(Level.SEVERE,"GenericCronJobImpl2 do get method"); 
		List<Company> companylist = ofy().load().type(Company.class).list();

		for(Company company : companylist){
			
			List<ConfigCategory> cronJobList = ofy().load().type(ConfigCategory.class).filter("companyId", company.getCompanyId()).filter("internalType", 29).list();
			logger.log(Level.SEVERE,"cron job list -- " + cronJobList.size() + " resp.isCommitted(); -- " + resp.isCommitted());
			ArrayList<String> cronList = new ArrayList<String>();
			for(ConfigCategory con : cronJobList){
				if(con.getStatus()){
					cronList.add(con.getCategoryName());
					logger.log(Level.SEVERE,"cron job list -- " + con.getCategoryName());
				}
				
			}
			
			if(cronList.size()>0){
				List<CronJobConfigration> cronConfigList = ofy().load().type(CronJobConfigration.class).filter("companyId", company.getCompanyId())
						.filter("cronJobsName IN",cronList).filter("configStatus", true).list();
				
				
				logger.log(Level.SEVERE,"Cron list -- "+ cronConfigList.size());
				
				for(CronJobConfigration cron : cronConfigList){
				
					List<CronJobConfigrationDetails> cronJobDetailList = new ArrayList<CronJobConfigrationDetails>();
					logger.log(Level.SEVERE," cron list llooop --- "+ cron.getCronJobsName());
						
					
					/**
					 * @author Ashwini Patil
					 * @since 8-02-2023
					 * Open Audits Reminder cron job
					 */
					if(cron.getCronJobsName().equalsIgnoreCase("OpenAuditsReminder")){
						
						HashSet<String> employeeRole = new HashSet<String>();

						for(CronJobConfigrationDetails cronDetails :cron.getCronJobsProcessList()){
							
							if(cronDetails.getCronJobName().equalsIgnoreCase("OpenAuditsReminder") && cronDetails.isStatus()){
						
								
								Date currentDate = DateUtility.getDateWithTimeZone("IST", new Date());
//								
							       logger.log(Level.SEVERE," Starting Date -- " + cronDetails.getStartingDate()  +" created date --" + cronDetails.getCreatedDate());
							       long difference = currentDate.getTime()  - DateUtility.getDateWithTimeZone("IST",cronDetails.getStartingDate()).getTime();
							        
								/**
								 * end
								 */
							       
								logger.log(Level.SEVERE,"get diff -- "+difference);
							       float daysBetween = (difference / (1000*60*60*24));
								
							       int  diffDays = (int) daysBetween;
							       
							      
							       if(cronDetails.getFrequencyDay()!=0 ){
							    	   logger.log(Level.SEVERE, "get diff -- " + diffDays % cronDetails.getFrequencyDay());
							    	   if( diffDays % cronDetails.getFrequencyDay() == 0){
							    		   cronJobDetailList.add(cronDetails);
							    		   employeeRole.add(cronDetails.getEmployeeRole());
							    	   }
							       }else if(cronDetails.getFrequencyDay()==0){
							    	   cronJobDetailList.add(cronDetails);
						    		   employeeRole.add(cronDetails.getEmployeeRole());
							       }
							
							
							}
						}
						
						if(cronJobDetailList.size()>0){
							Gson gson =  new Gson();
							String cronDetail = gson.toJson(cronJobDetailList);
							logger.log(Level.SEVERE,"Ser req string --"+cronDetail);
							OpenAuditRemindersCronJob audit = new OpenAuditRemindersCronJob();
							audit.auditlistForCronJob(cronDetail,employeeRole ); 
						}
					}
					
		
			}
		}	
	}
}
}
	

