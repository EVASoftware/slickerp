package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dev.util.collect.HashSet;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.config.configurations.ConfigTabels;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.services.CompanyRegistrationService;
import com.slicktechnologies.client.services.DemoConfigService;
import com.slicktechnologies.client.services.DemoConfigServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.EmployeeTrackingDetails;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.coststructure.EmployeeLevel;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.UserRole;

public class CompanyRegistrationServiceImpl extends RemoteServiceServlet implements CompanyRegistrationService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8028459064515281201L;

	DemoConfigurationImpl demoImpl =new DemoConfigurationImpl();
	
	Logger logger = Logger.getLogger("NameOfYourLogger");
	
	@Override
	public ReturnFromServer saveCompany(Company company) {
		//Set the count of Company

		if(company.getCount()==0)
	    {
	    	int count=ofy().load().type(company.getClass()).count();
	    	company.setCount(count+1);
	    	
	    }
		//Check weather company id is null , then only save it for first time.
		if(company.getId()==null)
		{
			//To check weather acess url already exists or not
			int count=ofy().load().type(Company.class).filter("accessUrl", company.getAccessUrl()).count();
			if(count!=0)
				return null;
			//To check weather company name already exists or not
			int countCompany=ofy().load().type(Company.class).filter("buisnessUnitName", company.getBusinessUnitName()).count();
			if(countCompany!=0)
				return null;
			//Save the company it is being saved for first time set its id
			ofy().save().entity(company).now();
			company.setCompanyId(company.getId());
			
			//  rohan added this code for setting starting and end month and date 
			// which 
			company.setStartdate("1");
			company.setStartMonth("APR");
			company.setEnddate("31");
			company.setEndMonth("MAR");
			ofy().save().entity(company).now();
			
			
			/**
			 * Updated By Anil Date :27-01-2017
			 * Rearranged
			 */
			
			//Set a default role to user
			UserRole role=new UserRole();
			role.setRoleName("ADMIN");
			role.setRoleStatus(true);
			role.setCompanyId(company.getCompanyId());
			
			//save role
			ofy().save().entity(role).now();
			
			//Create a default user
			User user=new User();
			String temp=company.getBusinessUnitName().trim().replaceAll("\\s","");
			temp=temp.toLowerCase();
			user.setUserName("admin123");
			
			/**  rohan added this code for by default encrypted passward
			 * Date : 06-04-2017 
			 */
			
//			String  originalPassword = "pass123";
//	        String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt());
//	        System.out.println("Hashcode:"+generatedSecuredPasswordHash);
//	        user.setPassword(generatedSecuredPasswordHash);
			
			//    ends here 
			
			user.setPassword("pass123");
			user.setRole(role);
			user.setCompanyId(company.getId());
			user.setStatus(true);
			
			//save user
			ofy().save().entity(user).now();
			
			/**
			 * End
			 */
			
			
			
//			ArrayList<String> classessArray=new ArrayList<String>();
//			
//			classessArray.add("LoneType");
//			classessArray.add("PaySlip");
//			classessArray.add("LoneEmi");
//			classessArray.add("LeaveBalance");
//			classessArray.add("LoggedIn");
//			classessArray.add("Service");
//			classessArray.add("Quotation");
//			classessArray.add("Approvals");
//			classessArray.add("Contract");
//			classessArray.add("Config");
//			classessArray.add("ConfigCategory");
//			classessArray.add("Type");
//			classessArray.add("Company");
//			classessArray.add("Activity");
//			classessArray.add("TimeReportConfig");
//			classessArray.add("Branch");
//			classessArray.add("BranchRelation");
//			classessArray.add("Employee");
//			classessArray.add("EmployeeInfo");
//			classessArray.add("EmployeeRelation");
//			classessArray.add("User");
//			classessArray.add("ItemProduct");
//			classessArray.add("SuperProduct");
//			classessArray.add("ServiceProduct");
//			classessArray.add("Vendor");
//			classessArray.add("Expense");
//			classessArray.add("Lead");
//			classessArray.add("Customer");
//			classessArray.add("VendorRelation");
//			classessArray.add("CustomerRelation");
//			classessArray.add("Category");
//			classessArray.add("Purchase");
//			classessArray.add("Calendar");
//			classessArray.add("GLAccount");
//			classessArray.add("SalesOrder");
//			classessArray.add("EmployeeLeave");
//			classessArray.add("Loan");
//			classessArray.add("Country");
//			classessArray.add("Grade");
//			classessArray.add("LeaveType");
//			classessArray.add("LeaveApplication");
//			classessArray.add("Shift");
//			classessArray.add("ShiftPattern");
//			classessArray.add("Department");
//			classessArray.add("UserRole");
//			classessArray.add("HrProject");
//			classessArray.add("HolidayType");
//			classessArray.add("EmployeeGroup");
//			classessArray.add("LeaveGroup");
//			classessArray.add("CtcComponent");
//			classessArray.add("CTC");
//			classessArray.add("AssignedShift");
//			classessArray.add("CompanyAsset");
//			classessArray.add("ToolGroup");
//			classessArray.add("ServiceProject");
//			classessArray.add("ClientSideAsset");
//			classessArray.add("EmployeeAttendance");
//			classessArray.add("LeaveAllocationProcess");
//			classessArray.add("PaySlipAllocationProcess");
//			classessArray.add("LiaisonStep");
//			classessArray.add("Liaison");
//			classessArray.add("LiaisonGroup");
//			classessArray.add("SalesQuotation");
//			classessArray.add("PriceList");
//			classessArray.add("RequsestForQuotation");
//			classessArray.add("LetterOfIntent");
//			classessArray.add("PettyCash");
//			classessArray.add("PettyCashDeposits");
//			classessArray.add("BillingDocument");
//			classessArray.add("Invoice");
//			classessArray.add("CustomerPayment");
//			classessArray.add("OtherTaxCharges");
//			classessArray.add("TaxesAndCharges");
//			classessArray.add("DeliveryNote");
//			classessArray.add("PurchaseOrder");
//			classessArray.add("PurchaseRequisition");
//			classessArray.add("GRN");
//			classessArray.add("InventoryInHand");
//			classessArray.add("Transfer");
//			classessArray.add("IncomingInventory");
//			classessArray.add("LocationRelation");
//			classessArray.add("InventoryEntries");
//			classessArray.add("WareHouse");
//			classessArray.add("OutgoingInventory");
//			classessArray.add("InventoryProductDetail");
//			classessArray.add("StorageLocation");
//			classessArray.add("Storagebin");
//			classessArray.add("ProductInventoryView");
//			classessArray.add("ProductInventoryViewDetails");
//			classessArray.add("CompanyPayment");
//			classessArray.add("VendorPayment");
//			classessArray.add("TaxDetails");
//			classessArray.add("State");
//			classessArray.add("City");
//			classessArray.add("Locality");
//			classessArray.add("CheckListStep");
//			classessArray.add("CheckListType");
//			classessArray.add("ShippingMethod");
//			classessArray.add("PackingMethod");
//			classessArray.add("EmployeeShift");
//			classessArray.add("DateShift");
//			classessArray.add("TimeReport");
//			classessArray.add("PaySlipRecord");
//			classessArray.add("ProductGroupList");
//			classessArray.add("ProductGroupDetails");
//			classessArray.add("BillOfMaterial");
//			classessArray.add("NumberGeneration");
//			classessArray.add("AccountingInterface");
//			classessArray.add("ContactPersonIdentification");
//			classessArray.add("PhysicalInventoryMaintaince");
//			classessArray.add("RaiseTicket");
//			classessArray.add("MaterialRequestNote");
//			classessArray.add("MaterialIssueNote");
//			classessArray.add("ProductInventoryTransaction");
//			classessArray.add("MaterialMovementNote");
//			classessArray.add("MaterialMovementType");
//			classessArray.add("ProcessName");
//			classessArray.add("InteractionType");
//			classessArray.add("ProcessConfiguration");
//			classessArray.add("DocumentHistory");
//			classessArray.add("");
//			classessArray.add("LicenseManagement");
//			classessArray.add("CancelSummary");
//			classessArray.add("Inspection");
//			classessArray.add("BillOfProductMaterial");
//			classessArray.add("VendorPriceListDetails");
//			classessArray.add("WorkOrder");
//			classessArray.add("CustomerBranchDetails");
//			classessArray.add("SmsConfiguration");
//			classessArray.add("SmsTemplate");
//			classessArray.add("IPAddressAuthorization");
//			classessArray.add("MultilevelApproval");
//			classessArray.add("SmsHistory");
//			classessArray.add("Fumigation");
//			classessArray.add("CustomerTrainingDetails");
//			classessArray.add("EmployeeAdditionalDetails");
//			classessArray.add("Overtime");
//			classessArray.add("CompanyPayrollRecord");
//			classessArray.add("Declaration");
//			classessArray.add("Complain");
//			classessArray.add("ContractRenewal");
//			classessArray.add("TeamManagement");
//			classessArray.add("CustomerUser");
//			classessArray.add("WhatsNew");
//			classessArray.add("PettyCashTransaction");
//			classessArray.add("SalesPersonTargets");
//			classessArray.add("FinancialCalender");
//			classessArray.add("AssesmentReport");
//			classessArray.add("CronJob");	
//			classessArray.add("MultipleExpenseMngt");	
//			
//			classessArray.add("C_Billing");	
//			classessArray.add("S_Billing");	
//			classessArray.add("B_Billing");	
//			classessArray.add("I_Billing");	
//			classessArray.add("P_Billing");	
//			classessArray.add("C_NonBilling");	
//			classessArray.add("S_NonBilling");	
//			classessArray.add("B_NonBilling");	
//			classessArray.add("I_NonBilling");
//			classessArray.add("P_NonBilling");	
//			classessArray.add("P_Invoice");	
//			classessArray.add("ServicePo");	
//			classessArray.add("RoleDefinition");
//			classessArray.add("EmployeeLevel");
//			classessArray.add("CTCTemplate");
//			classessArray.add("RegisterDevice");
//			classessArray.add("UnitConversion");			
//			/** date 24.7.2018 added by komal for employee asset **/
//			classessArray.add("EmployeeAsset");
//			classessArray.add("EmployeeInvestment");
//			classessArray.add("TaxSlab");
//			classessArray.add("Investment");
//			classessArray.add("ArrearsDetails");
//			classessArray.add("EmployeeCheckListType");
//			classessArray.add("CNCVersion");
//			/** Date : 23-08-2018 BY ANIL **/
//			classessArray.add("ProfessionalTax");
//			
//			
//			classessArray.add("Attendance");
//			classessArray.add("EmployeeFingerDB");
//			classessArray.add("ProjectAllocation");
//			/** added by komal for vendor invoice entity **/
//			classessArray.add("VendorInvoice");
//			/** date 6.10.2018 added by komal **/
//			classessArray.add("LWF");
//			classessArray.add("VoluntaryPf");
//			classessArray.add("VoluntaryPfHistory");
//			/** date 08.03.2018 added by komal **/
//			classessArray.add("TechnicianWareHouseDetailsList");
//			/**
//			 * @author Anil , Date : 19-06-2019
//			 */
//			classessArray.add("PaidLeave");
//			classessArray.add("Bonus");
//			classessArray.add("ProvidentFund");
//			classessArray.add("Esic");
//			classessArray.add("ExpensePolicies");
//			classessArray.add("EmployeeTrackingDetails");
//			/**
//			 * Date 25-5-2018 by jayshree 
//			 */
			
			ArrayList<String> classessArray = new ArrayList<String>();
			classessArray = getProcessNamelist();
			
			/**
			 * Date : 27-01-2017 By Anil
			 * Updated Process Name Save code
			 * instead of saving records in loop ,saved records in once by adding records in list
			 */
			ArrayList<ProcessName> processNameList=new ArrayList<ProcessName>();
			for(int i=0;i<classessArray.size();i++)
			{
				ProcessName configEntity=new ProcessName();
				configEntity.setCompanyId(company.getCompanyId());
				configEntity.setProcessName(classessArray.get(i));
				configEntity.setStatus(true);
				configEntity.setCount(i+1);
				processNameList.add(configEntity);
			}
			ofy().save().entities(processNameList).now();
			
			/**
			 * End
			 */

			/**
			 * @author Abhinav Bihade
			 * @since 06/01/2020 As per Weekly Meeting Discussion Sonu Porel
			 *        Raised Question, In the presence of Nitin Sir,Anil pal and
			 *        Other People, 'Generate all process type at the time of
			 *        link creation'
			 */
			ArrayList<String> processTypeArray = new ArrayList<String>();
			processTypeArray = getprocessTypelist();
			
//			processTypeArray.add("MakeEmailMandatory");
//			processTypeArray.add("MakeGoupMandatory");
//			processTypeArray.add("DisableCellNumberValidation");
//			processTypeArray.add("MakeRefNumber1Mandatory");
//			processTypeArray.add("MakeCustomerLevelMandatory");
//			processTypeArray.add("MapCustomerCategoryAndTypeaAsaDocumentCategoryAndType");
//			processTypeArray.add("HideCustomerGroup");
//			processTypeArray.add("MakeCategoryAndTypeDisable");
//			processTypeArray.add("ChangeCompanyNameAndFullNameLable");
//			processTypeArray.add("MakeCellNumberNonMandatory");
//			processTypeArray.add("LeadProjectCoordinator");
//			processTypeArray.add("MakeCustomerPopUpEmailMandatory");
//			processTypeArray.add("MakeCustomerPopUpCompanyNameMandatory");
//			processTypeArray.add("MakeCustomerPopUpPhoneNumberMandatory");
//			processTypeArray.add("EnableFreezeLead");
//			processTypeArray.add("MakePincodeMandatory");
//			processTypeArray.add("CommunicationLogColumn");
//			processTypeArray.add("MapCustomerTypeToLeadCategory");
//			processTypeArray.add("MakePriceColumnDisable");
//			processTypeArray.add("ChangeProductComposite");
//			processTypeArray.add("SelfApproval");
//			processTypeArray.add("Quotation Service Data");
//			processTypeArray.add("printProductPremisesInPdf");
//			processTypeArray.add("GSTQuotationPDF");
//			processTypeArray.add("QuotationPdf");
//			processTypeArray.add("MakeLeadIDMandatory");
//			processTypeArray.add("MakeGroupMandatory");
//			processTypeArray.add("MakeTypeMandatory");
//			processTypeArray.add("MakeCatagoryMandatory");
//			processTypeArray.add("PrintAttnInPdf");
//			processTypeArray.add("HeaderWithOnlyComapanyDetail");
//			processTypeArray.add("QuotationAsLetter");
//			processTypeArray.add("DoNotProductServicesTable");
//			processTypeArray.add("DoNotProductDescription");
//			processTypeArray.add("PestoIndiaQuotations");
//			processTypeArray.add("CompanyAsLetterHead");
//			processTypeArray.add("PrintProductDiscriptionOnPdf");
//			processTypeArray.add("OnlyForPecopp");
//			processTypeArray.add("MakeQuotationDateDisable");
//			processTypeArray.add("MakeColumnDisable");
//			processTypeArray.add("PrintCustomQuotation");
//			processTypeArray.add("CreateContractAutoOnMarkSuccessfull");
//			processTypeArray.add("ContractServiceData");
//			processTypeArray.add("OnlyForFriendsPestControl");
//			processTypeArray.add("OnlyForNBHC");
//			processTypeArray.add("ConsolitePrice");
//			processTypeArray.add("HideOrShowCreateServicesButton");
//			processTypeArray.add("ContractReport");
//			processTypeArray.add("PrintProductPrimiseInPdf");
//			processTypeArray.add("EnableServiceBranch");
//			processTypeArray.add("printDESConFirstPPageFlaagForContractAndQuotation.");
//			processTypeArray.add("MakeNumberRangeMandatory");
//			processTypeArray.add("EnableOnlyAMCContractForContractRenewal");
//			processTypeArray.add("DisableAreaWiseBillingForAmc");
//			processTypeArray.add("ContractPaymentNonEditable");
//			processTypeArray.add("ServiceInvoiceMappingOnContractApproval");
//			processTypeArray.add("SalesPersonRestriction");
//			processTypeArray.add("ContractStartDateValidation");
//			processTypeArray.add("HideBranchWiseBillingOption");
//			processTypeArray.add("headerWithOnlycompanyDetail");
//			processTypeArray.add("EnableDoNotPrintGSTNumber");
//			processTypeArray.add("ContractDownloadNew");
//			processTypeArray.add("HideGstinNo");
//			processTypeArray.add("MakeLeadAndQuotationIdMandatory");
//			processTypeArray.add("OnlyForEVA");
//			processTypeArray.add("EnableRenewalContractTypeDefault");
//			processTypeArray.add("AddPoDetails");
//			processTypeArray.add("MakeContractDateDisable");
//			processTypeArray.add("EnableWarehouseAddressasServiceAddress");
//			processTypeArray.add("HideTechnicianNameAndContractPeriod");
//			processTypeArray.add("EnableShowContractRenewForRenewedContracts");
//			processTypeArray.add("DeleteExtraServiceProductWise");
//			processTypeArray.add("showCancelButtonForRenew");
//			processTypeArray.add("UPDATEADDRESS");
//			processTypeArray.add("HideRateAndDiscountColumn");
//			processTypeArray.add("DisableDefaultContractLoading");
//			processTypeArray.add("PartialPaymentOneInvoiceAndPaymentAsPerrecievedAndBalanceAmt");
//			processTypeArray.add("MakeServiceCompletionMandatoryForAMCContract");
//			processTypeArray.add("ServiceWiseBillingApprovedStatus");
//			processTypeArray.add("EnableAutoInvoice");
//			processTypeArray.add("BranchAsCompany");
//			processTypeArray.add("BranchLevelRestriction");
//			processTypeArray.add("FumigationDetails");
//			processTypeArray.add("MakeRefNumberAsCostCenter");
//			processTypeArray.add("MakeRefNumber2AsProfitCenter");
//			processTypeArray.add("Accounting Interface");
//			processTypeArray.add("ServicesScheduleList");
//			processTypeArray.add("OnlyForOrionPestSolutions");
//			processTypeArray.add("DeliveryNoteDailyEmail");
//			processTypeArray.add("PCAMBCustomization");
//			processTypeArray.add("DontShowZeroPayableAmtProduct");
//			processTypeArray.add("EnableInvoiceEditableForAllRoles");
//			processTypeArray.add("ActiveBranchEmailId");
//			processTypeArray.add("HideGSTINNumber");
//			processTypeArray.add("REFRENCEORDERNUMBER");
//			processTypeArray.add("ShowServiceAddress");
//			processTypeArray.add("ShowContractDuration");
//			processTypeArray.add("EnableRevenueLossReport");
//			processTypeArray.add("MakeAdressToUppercase");
//			processTypeArray.add("PrintPoDetailsOnInvoice");
//			processTypeArray.add("IPCInvoice");
//			processTypeArray.add("CustomerBranchNameInInvoiceServiceAddress");
//			processTypeArray.add("UserWiseIPAuthorization");
//			processTypeArray.add("LeadSaveEmail");
//			processTypeArray.add("CustomerTypeForNbhc");
//			processTypeArray.add("POPDFV1");
//			processTypeArray.add("OnlyForPepcopp");
//			processTypeArray.add("DoNotMaterialRequiredValidation");
//			processTypeArray.add("DeliveryNoteFromWorkOrder");
//			processTypeArray.add("ConfirmationMailToDepartment");
//			processTypeArray.add("MakeProjectNameEnable");
//			processTypeArray.add("CncEarningColumn");
//			processTypeArray.add("EnableCancellationRemark");
//			processTypeArray.add("EnableStackDetailsMandatory");
//			processTypeArray.add("EnableValidateAssesmentReport");
//			processTypeArray.add("HidePlanRescheduleFumigationColoumn");
//			processTypeArray.add("HideFumigationButton");
//			processTypeArray.add("RESTRICTBYDEFAULTLOADINGOFLIST");
//			processTypeArray.add("MakeServicesDirecctlyFromComlain");
//			processTypeArray.add("FumigationCertificate");
//			processTypeArray.add("OnlyForOrion");
//			processTypeArray.add("enableBillableNonMandatory");
//			processTypeArray.add("MakeTicketCategoryMandatory");
//			processTypeArray.add("EnableComplainStatusHistory");
//			processTypeArray.add("EnablePlannedStatusMandatory");
//			processTypeArray.add("EnableComplaintDueDate");
//			processTypeArray.add("EnableTicketDateRestriction");
//			processTypeArray.add("EnableCreateAssementForComplaintService");
//			processTypeArray.add("EnableCompleteComplaintService");
//			processTypeArray.add("OldFormatRenewalLetter");
//			processTypeArray.add("ShowOldContractDetails");
//			processTypeArray.add("ProcessedContractRenewalReport");
//			processTypeArray.add("UnprocessedContractRenewalReport");
//			processTypeArray.add("HIDEBANKDETAILS");
//			processTypeArray.add("EnableQuotationMandatoryForContractRenwal");
//			processTypeArray.add("NewTallyInterfaceDownload");
//			processTypeArray.add("AccountingInterfaceReport");
//			processTypeArray.add("EnableShowButtonChangeStatusToCreated");
//			processTypeArray.add("ShowCumulativeTaxAmount");
//			processTypeArray.add("OnlyForAirotech");
//			processTypeArray.add("PoDateEnableTrue");
//			processTypeArray.add("PRApprovalEmailToPOPurEngg");
//			processTypeArray.add("AddApprovalStatusInPR");
//			processTypeArray.add("AllowMultipleEmployeeInExpense");
//			processTypeArray.add("ApproveTransferINMMNDirectly");
//			processTypeArray.add("Matarial Issue Note");
//			processTypeArray.add("EmployeeApprovaProcess");
//			processTypeArray.add("EMPLOYEEAPPROVALPROCESS");
//			processTypeArray.add("UploadCompositeTracking");
//			processTypeArray.add("DisableJoiningDate");
//			processTypeArray.add("LwfAsPerEmployeeGroup");
//			processTypeArray.add("PrintCtcAlongWithEmployeeBiodata");
//			processTypeArray.add("EnableEmployeeDateOfBirthNonMandatory");
//			processTypeArray.add("EnableEmployeeCellNoNonMandatory");
//			processTypeArray.add("BackDatedJoiningDateNotAllowed");
//			processTypeArray.add("MakeReportToMandatory");
//			processTypeArray.add("EMAILNOTMANDATORY");
//			processTypeArray.add("EnableClosePRBasedOnGRNQty");
//			processTypeArray.add("IPAuthorization");
//			processTypeArray.add("Contract RenewalSMS");
//			processTypeArray.add("ApprovalDailyEmail");
//			processTypeArray.add("GRNDailyEmail");
//			processTypeArray.add("InvoiceAPDailyEmail");
//			processTypeArray.add("InvoiceARDailyEmail");
//			processTypeArray.add("LeadDailyEmail");
//			processTypeArray.add("PaymentAPDailyEmail");
//			processTypeArray.add("PaymentARDailyEmail");
//			processTypeArray.add("PhysicalInventoryDailyEmail");
//			processTypeArray.add("SalesQuotationDailyEmail");
//			processTypeArray.add("ServiceDailyMail");
//			processTypeArray.add("ServiceQuotationDailyEmail");
//			processTypeArray.add("ContractRenewalSMS");
//			processTypeArray.add("CustomerPaymentARSMS");
//			processTypeArray.add("CustomerServiceSms");
//			processTypeArray.add("ContractRenewalWithoutProcessedCustomerEmail");
//			processTypeArray.add("ReorderLevelEmail");
//			processTypeArray.add("WarehouseExpiryEmail");
//			processTypeArray.add("EVALicenseReminder");
//			processTypeArray.add("AutoMultilevelApproval");
//			processTypeArray.add("ContractReportGenerationCronJobImpl");
//			processTypeArray.add("DueReminderCronJob");
//			processTypeArray.add("UnsuccessfulQuotationcronjob");
//			processTypeArray.add("PaymentARDueDailyEmailToClient");
//			processTypeArray.add("PaymentARTDSCertificateDueEmailToClient");
//			processTypeArray.add("CustomerPaymentARCronJobImpl");
//			processTypeArray.add("UpdateButton");
//			processTypeArray.add("WarehouseExpiredDontLoad");
//			processTypeArray.add("EnableLoadExpiryWarehouseStockInHand");
//			processTypeArray.add("PaymentAddIntoPettyCash");
//			processTypeArray.add("PrintPaySlipOnSamePage");
//			processTypeArray.add("PaymentDateMondatory");
//			processTypeArray.add("UpdateDataTDSFlagIndex");
//			processTypeArray.add("NewEntryNotAllowed");
//			processTypeArray.add("VENDORINFONOTMANDATORY");
//			processTypeArray.add("MONTHWISENUMBERGENERATION");
//			processTypeArray.add("BhashSMSAPI");
//			processTypeArray.add("EnableSMSAPIWEBTACTIC");
//			processTypeArray.add("DontLoadAllLocalityAndCity");
//			processTypeArray.add("HideOrShowModelNoAndSerialNoDetails");
//			processTypeArray.add("PrintModelNoAndSerialNo");
//			processTypeArray.add("NumberRangeProcess");
//			processTypeArray.add("EnableServicePdfWithoutAddressAndCellNumber");
//			processTypeArray.add("ServiceBillingDetailMapingProcess");
//			processTypeArray.add("ServiceInvoiceMappingOnCompletion");
//			processTypeArray.add("CopyReferenceNoFromCustomerBranch");
//			processTypeArray.add("PrintServiceVoucher");
//			processTypeArray.add("EnableServiceWiseBillingServiceListBulkComplete");
//			processTypeArray.add("TECHNICIANWISEMATERIALPOPUP");
//			processTypeArray.add("DATETECHNICIANWISESERVICEPOPUP");
//			processTypeArray.add("MINBASEDONCONSUMEDQUANTITY");
//			processTypeArray.add("SendSRCopyOnServiceCompletion");
//			processTypeArray.add("MarkServiceCompletedFromApp");
//			processTypeArray.add("ServiceStatusAsPlanned");
//			processTypeArray.add("OnlyForOrdientVentures");
//			processTypeArray.add("EnableSSL");
//			processTypeArray.add("SiddhiServices");
//			processTypeArray.add("ShowAttendanceLabel");
//			processTypeArray.add("AttendanceLabelAsInput");
//			processTypeArray.add("AllowToUploadMultipleSiteAttendance");
//			processTypeArray.add("AddSingleAndDoubleOtInAttendanceUploadTemplate");
//			processTypeArray.add("ShowWorkingHours");
//			processTypeArray.add("TotalSummaryAtBottom");
//			processTypeArray.add("AutoCalculatePtAndLwf");
//			processTypeArray.add("SiteWisePayrollForReliever");
//			processTypeArray.add("DoNotLoadPtAndLwf");
//			processTypeArray.add("AutoCalculateHR");
//			processTypeArray.add("IncludeOtInGrossEarning");
//			processTypeArray.add("Form9");
//			processTypeArray.add("Form11");
//			processTypeArray.add("FormA");
//			processTypeArray.add("Formc");
//			processTypeArray.add("Form2aReivised");
//			processTypeArray.add("Form6A");
//			processTypeArray.add("Form3A");
//			processTypeArray.add("FormD");
//			processTypeArray.add("ShowEsicNo");
//			processTypeArray.add("CustomerCorrespondanceName");
//			processTypeArray.add("ShowPfNumberAndEsicNumberAsCompany");
//			processTypeArray.add("HideDepartmentName");
//			processTypeArray.add("OtherEarningComponent");
//			processTypeArray.add("EmployeeDesignationFromProject");
//			processTypeArray.add("UPDATEPRODUCTCODE");
//			processTypeArray.add("customerNameValidate");
//			processTypeArray.add("customerPhoneNoValidate");
//			processTypeArray.add("verticalPdfWithLimitedColumns");
//			processTypeArray.add("ProductDescriptionBelowTheProduct");
//			processTypeArray.add("RestrictUploadProcessForRegisteredEmployee");
//			processTypeArray.add("LoadHrTemplate");
//			processTypeArray.add("EnableEmployeeName");
//			processTypeArray.add("EnableUpdateStockMatserWithTransaction");
//			processTypeArray.add("ValidateMMN");
//			processTypeArray.add("EnableLockSealStartEndSerialMandatory");
//			processTypeArray.add("EnableLoadAllClusterInTransferTo");
//			processTypeArray.add("EnableLoadAllWarehousesGlobally");
//			processTypeArray.add("MakeShiftMandatory");
//			processTypeArray.add("CustomerBranchDownload");
//			processTypeArray.add("FranchiseInformation");
//			processTypeArray.add("Report");
//			processTypeArray.add("EnableAutomaticPOCreation");
//			processTypeArray.add("HIDEHR");
//			processTypeArray.add("SHOWWARRANTYINFO");
//			processTypeArray.add("EnableStoreSerialNumberInDetails");
//			processTypeArray.add("EnableCloseMRNBasedOnMMN");
//			processTypeArray.add("EnableUploadGRNMMNForLockSeal");
//			processTypeArray.add("EnableShowSerialNumber");
//			processTypeArray.add("EnableStockUpdation");
//			
//			/**
//			 * Pedio Process Types
//			 */
//			
//			processTypeArray.add("MAKESTARRATINGMANDITORY");
//			processTypeArray.add("MAKETECHNICIANREMARKMANDITORY");
//			processTypeArray.add("MAKECUSTOMERNAMEMANDITORY");
//			processTypeArray.add("MAKECUSTOMERREMARKMANDITORY");
//			processTypeArray.add("RETURNQTYVIEW");
//			processTypeArray.add("HIDESERVICEFINDING");
//			processTypeArray.add("HIDEMATERIALS");
//			processTypeArray.add("SHOWEXPENSE");
//			processTypeArray.add("DIRECTCOMPLETE");
//			processTypeArray.add("TECHNICIANATTENDANCE");
//			processTypeArray.add("SITEFINDING");
//			processTypeArray.add("PASSBOOKNAYARA");
//			processTypeArray.add("HIDETOOLS");
//			processTypeArray.add("REJECTSERVICE");
//			processTypeArray.add("HIDESRS");
//			processTypeArray.add("LIVETRACKING");
//			processTypeArray.add("FCMNOTIFICATION");
//			processTypeArray.add("ENABLEFCMNOTIFICATION");
//			processTypeArray.add("SENDSRCOPYONSERVICECOMPLETION");
//			processTypeArray.add("DATETECHNICIANWISESERVICEPOPUP");
//			processTypeArray.add("SENDOTPFORSERVICECOMPLETION");
//			processTypeArray.add("MINBASEDONCONSUMEDQUANTITY");
//			processTypeArray.add("MAKEMATERIALMANDATORY");
//			processTypeArray.add("SINGLECONTRACTSRCOPY");
//			processTypeArray.add("LoadOperatorDropDown");
//			processTypeArray.add("NOTECHNICIANWISEMMN");
//			processTypeArray.add("MARKSERVICECOMPLETEDFROMAPP");
//			processTypeArray.add("MarkServiceScheduledStatusTrue");
//			processTypeArray.add("WarehouseDetailsMandatory");
//			processTypeArray.add("UpdateProjectMaterial");
//			processTypeArray.add("updateProjectOnTechnicianChange");
//			processTypeArray.add("MakeServiceEditableAfterCompletion");
//			processTypeArray.add("TECHNICIANWISEMATERIALPOPUP");
//			processTypeArray.add("EnableFcmNotification");
//			processTypeArray.add("ENABLESERVICESCHEDULENOTIFICATION");



			ArrayList<Config> processNameList1 = new ArrayList<Config>();
			for (int i = 0; i < processTypeArray.size(); i++) {
						
				Config configEntity1 = new Config();
				configEntity1.setCompanyId(company.getCompanyId());
				configEntity1.setType(73);
				configEntity1.setName(processTypeArray.get(i));
				configEntity1.setStatus(true);
				configEntity1.setCount(i + 1);
				processNameList1.add(configEntity1);
			}
			ofy().save().entities(processNameList1).now();

			/**
			 * Date : 27-01-2017 By Anil
			 * Updated Number Generation Save code
			 * instead of saving records in loop ,saved records in once by adding records in list
			 */
			ArrayList<NumberGeneration> numberGenerationList=new ArrayList<NumberGeneration>();
			for(int i=0;i<classessArray.size();i++)
			{
				NumberGeneration ngEntity=new NumberGeneration();
				ngEntity.setCompanyId(company.getCompanyId());
				ngEntity.setProcessName(classessArray.get(i));
				ngEntity.setCount(i+1);

				if(classessArray.get(i).trim().equals("ItemProduct")){
					ngEntity.setNumber(100000000);
				}
				else if(classessArray.get(i).trim().equals("ServiceProduct")){
					ngEntity.setNumber(500000000);
				}
				else if(classessArray.get(i).trim().equals("Quotation")){
					ngEntity.setNumber(500000000);
				}
				else if(classessArray.get(i).trim().equals("Contract")){
					ngEntity.setNumber(500000000);
				}
				//   rohan added this code billing non billing condition
				else if(classessArray.get(i).trim().equals("C_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("S_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("B_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("I_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("P_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("C_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("S_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("B_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("I_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("P_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("P_Invoice")){
					ngEntity.setNumber(400000000);
				}
				else{
					ngEntity.setNumber(100000000);
				}
				ngEntity.setStatus(true);
				
				numberGenerationList.add(ngEntity);
			}
			
			ofy().save().entities(numberGenerationList).now();
			
			/**
			 * End
			 */
			
			/**
			 * @author Vijay Date 30-11-2020 
			 * Des :- to create module name and document Name
			 */
			if(company.getStatus().equalsIgnoreCase("Active")){
				createModuleNamesAndDocumentNames(company.getCompanyId(),company.getAccessUrl());
				createSettingModuleConfiguration(company.getCompanyId());
			}
			
			if(company.getStatus().equalsIgnoreCase("Demo"))
			{
				/**
				 * Developed by : Rohan Bhagde.
				 * Date : 22/11/2016
				 * Reason : This code is used for generating demo configurations automatically when new link is created 
				 * so that there is no need of manual clicking demo configurations and it will . 
				 */
					
				//  this is for product details 
				demoImpl.setProductDetails(company.getCompanyId());
			
				//  this task queue only contains configs value
				demoImpl.setPestControlConfigs(company.getCompanyId());
				
				// this task queue only for categories values
				//need to update
				demoImpl.setPestControlCategories(company.getCompanyId());
				
				// this task queue only for categories values
				demoImpl.setPestControlType(company.getCompanyId());
				
				//   this task queue contains Modules and document names  
				demoImpl.setDefaultConfigs(company.getCompanyId());	
				
				/**
				 * below is commented by vijay because for asset we are creating its item products as per disscution with anil Date 28 feb 2017
				 */
				//   this is for asset details 
//				demoImpl.setAssetDetails(company.getCompanyId());
				
				/**
				 * commented by vijay
				 */
				// this is for inventory detaisl
//				demoImpl.setInventoryDetails(company.getCompanyId());
				
				
				//  this is used for creating lead data
				demoImpl.getLeadDatafortaskqueue(company.getCompanyId());
				/**
				 * ends here 
				 */
			}
			/**
			 * Updated By: Viraj
			 * Date: 26-03-2019
			 * Description: To create a vendor of brand if Franchise Type is masterFranchise or Franchise
			 */
//			if(company.getStatus().equalsIgnoreCase("Active")) {
				
				if(company.getFranchiseType().equalsIgnoreCase("Franchise")) {
					if(company.getBrandUrl() != null && !company.getBrandUrl().equals("")) {
						logger.log(Level.SEVERE,"brandUrl before method call:: "+company.getBrandUrl());
						loadCompanyDetails(company.getBrandUrl(),company.getId());
					}
					if(company.getMasterUrl() != null) {
						loadCompanyDetails(company.getMasterUrl(),company.getId());
					}
				} else {
					if(company.getBrandUrl() != null && !company.getBrandUrl().equals("")) {
						logger.log(Level.SEVERE,"brandUrl before method call:: "+company.getBrandUrl());
						loadCompanyDetails(company.getBrandUrl(),company.getId());
					}
				}
				
//			}
			/** Ends **/
		}
		else{
			ofy().save().entity(company).now();
		}
		
		ReturnFromServer server = new ReturnFromServer();
		server.id = company.getId();
		server.count = company.getCount();
		// sendEmail(company);
		return server;
	    
	}
	
	

	


	






	/**
	 * Updated By: Viraj
	 * Date: 26-03-2019
	 * Description: To create a vendor of brand if Franchise Type is masterFranchise or Franchise
	 * @param companyId 
	 */
	  private void loadCompanyDetails(String brandUrl, Long companyId) {
				
		final String CLIENTURL = brandUrl+AppConstants.FRANCHISETYPELIST.BRAND_URL;
		logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
		
		String data="";
		URL url = null;
		try {
			url = new URL(CLIENTURL);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 2");
        con.setDoInput(true);
        con.setDoOutput(true);

        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 3");
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 4");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
			writer.write(getDataTosend(brandUrl));
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 5");
        
        
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		 logger.log(Level.SEVERE,"STAGE 6");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String temp;
        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp+"\n"+"\n";
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"STAGE 7");
        logger.log(Level.SEVERE,"Data::::::::::"+data);
		
        Gson gson = new Gson();
		JSONObject object = null;
		try {
			object = new JSONObject(data.trim());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Company compList=new Company();
		
		compList.setId(Long.parseLong(object.optString("comp_id").trim()));
		compList.setBusinessUnitName(object.optString("comp_name").trim());
		compList.setPocName(object.optString("comp_pocname").trim());
		compList.setEmail(object.optString("comp_email").trim());
		compList.setCellNumber1(Long.parseLong(object.optString("comp_no").trim()));
		compList.getAddress().setAddrLine1(object.optString("comp_addLine1").trim());
		compList.getAddress().setCity(object.optString("comp_city").trim());
		compList.getAddress().setState(object.optString("comp_state").trim());
		compList.getAddress().setCountry(object.optString("comp_country").trim());
		compList.setCompanyURL(object.optString("comp_url").trim());
		logger.log(Level.SEVERE,"Company Name::::::::::"+compList.getBusinessUnitName());
		logger.log(Level.SEVERE,"Company POCname::::::::::"+compList.getPocName());
		
		createVendor(compList,companyId);
	}
	private String getDataTosend(String brandUrl) {
		StringBuilder strB=new StringBuilder();
		strB.append("jsonString");
		strB.append("=");
		strB.append(brandUrl);
		logger.log(Level.SEVERE,"Brand Url::::::::::"+strB);
		return strB.toString();
	}
	private void createVendor(Company comp, Long companyId) {
		Vendor vendor = new Vendor();
		logger.log(Level.SEVERE,"Company POCname::::::::::"+comp.getPocName());
		
		if(comp != null) {
			
			vendor.setCompanyId(companyId);
			vendor.setVendorName(comp.getBusinessUnitName());
			vendor.setfullName(comp.getPocName());
			vendor.setEmail(comp.getEmail());
			vendor.setCellNumber1(comp.getCellNumber1());
			vendor.setPrimaryAddress(comp.getAddress());
			vendor.setSecondaryAddress(comp.getAddress());
			vendor.setVendorRefNo(comp.getCompanyURL());
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(vendor);
		}
		
	}
	/** Ends **/  
	
	  
	  
	  
	public void sendEmail(Company c) {
	 	 logger.log(Level.SEVERE,"Inside send Email method :::");		 
	 	 
		Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String name=c.getPocName();
        name=name.replace("null", "");
        String userid="admin123";
        userid=userid.replaceAll("\\s","");
        userid=userid.trim();
        userid=userid.toLowerCase();
       
        String password="pass123";
        password=password.replaceAll("\\s","");
        password=password.toLowerCase();
        password=password.trim();
        
        //Calculation of End date
        int planDurationinDay=c.getNoOfWeeks()*7;
        Date startDate=c.getLicenseStartDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate); // Now use today date.
        calendar.add(Calendar.DATE, planDurationinDay); // Adding 5 days
        String planEnd = sdf.format(calendar.getTime());
        String start=sdf.format(startDate);
        String CompnayMessage="";
        if(c.getStatus().equals(Company.DEMO))
        CompnayMessage="Thanks for asking evaluation copy. We are confident that this will be "
            		+ "enormous value addition to your business.<br><br><br>";
        if(c.getStatus().equals(Company.ACTIVE))
        	CompnayMessage="Thanks for choosing EVA Software Solutions to manage your business operations"+
                     "We are confident that this will be enormous value addition to your business.<br><br><br>";
       

        String msgBody = ""
        		+ "Dear "+name+","+"<br>"
        		+ "Greetings from EVA Software Solution!"+"<br>"
        		+CompnayMessage
        		+ "<b>Your account details are :</b>"+"<br><br><br>"
        		+"User ID :&nbsp;&nbsp; "+userid+"<br>"
        		+"Password :&nbsp;&nbsp; "+password+"<br>"
        		+"Account status  :&nbsp;&nbsp; "+c.getStatus()+"<br>"
        		+ "License Start Date :&nbsp;"+start+"<br>"
        		+ "License End Date :&nbsp;&nbsp;"+planEnd+"<br>"
        		+"No. of Users :&nbsp;"+c.getNoOfUser()+"<br><br><br>"
        		+"Link to access your EVA ERP / CRM : "+c.getAccessUrl()+".evasoftwaresolutions.appspot.com"+"<br><br><br>"
        		+ "For any queries, please contact us at the touch points mentioned below and we will be happy to assist you.Or"
        		+ " Call us on "+"+91919619390370 "+","+"+918452933313"+","
        		+ " email us at "+"support@evasoftwaresolutions.com"+"."+"<br><br><br> "
        		+"For any online assistance required please refer user guides at  link"
        		+"<a href=www.evasoftwaresolutions.com/download.html> User Guide </a><br><br><br>"
        		+ " We value your association with us and assure you of our best services.<br><br><br>"
        		+ "<b>"+"EVA Software Solutions"+"</b><br><br>"
                +"# 1 , Monginis Cake Shop<br>"
                +"<b>Station Road, Hariyalli Village</b> <br>"
                +"<b>Tagore Nagar</b> <br>"
                +"<b>Vikhroli East</b> <br>"
                +"<b>Mumbai 400083</b> <br>"
                +"<b>email - sales@evasofotwaresolutions.com </b> <br>"
                +"<b>website - www.evasoftwaresolutions.com</b> <br>"
                +"<b>Phone - +91 9619390370 / +91 8452933313</b> <br>"
                ;
                
        

        try {
            Message msg = new MimeMessage(session);
            String website=c.getWebsite();
            if(website!=null || website.trim().equals("")==false)
            msg.setFrom(new InternetAddress("evasoftwaresolutionsdev@gmail.com","Eva Software Solution"));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress(c.getEmail(),name));
           msg.setSubject("New Company Registration");
            Multipart mp=new MimeMultipart();
	          
	          MimeBodyPart htmlpart=new MimeBodyPart();
	          htmlpart.setContent(msgBody,"text/html");
	          mp.addBodyPart(htmlpart);
	          msg.setContent(mp);
         Transport.send(msg);
         
    	 logger.log(Level.SEVERE,"Email send successful :::");		 

        } catch (Exception e) {
        	e.printStackTrace();
            // ...
        
	}
		
	}
	
	
	@Override
	public ArrayList<SuperModel> getSearchResult(MyQuerry quer) {
		// Clear the session
		ArrayList<SuperModel> list = new ArrayList<SuperModel>();
		try {
			ofy().clear();
			Query<? extends SuperModel> query = null;
			// Get the querry
			query = ofy().load().type(quer.getQuerryObject().getClass());
			Logger logger = Logger.getLogger("NameOfYourLogger");
			// Get the
			if (quer.getFilters() != null) {
				for (int i = 0; i < quer.getFilters().size(); i++) {
					String conditionField = quer.getFilters().get(i)
							.getQuerryString();

					Object value = quer.getFilters().get(i).geFilterValue();

					if (value instanceof Date) {

						logger.log(Level.SEVERE, "Condition Field "
								+ conditionField);
						logger.log(Level.SEVERE, "Condition Value " + value);
						logger.log(Level.SEVERE,
								"Size of Querry " + query.count());
						GWT.log("Condition Field " + conditionField);
						GWT.log("Condition Value " + value);
						logger.log(
								Level.SEVERE,
								"Condition Value before parsed "
										+ value.toString());
						Date condDate = GenricServiceImpl
								.parseDate((Date) value);
						// Very dangerous patch learn internationalization and
						// recode
						Calendar c = Calendar.getInstance();
						c.setTime(condDate);
						c.add(Calendar.DATE, 1);
						condDate = c.getTime();
						condDate = GenricServiceImpl.parseDate((Date) condDate);
						logger.log(Level.SEVERE, "Condition Value When parsed "
								+ condDate);
						logger.log(Level.SEVERE, "Condition Field When parsed "
								+ conditionField);
						query = query.filter(conditionField, condDate);
					} else {

						logger.log(Level.SEVERE, "Condition Field "
								+ conditionField);
						logger.log(Level.SEVERE, "Condition Value " + value);
						if (conditionField.equals("companyId") == false)
							query = query.filter(conditionField, value);
						logger.log(Level.SEVERE,
								"Size of Querry " + query.count());

					}
				}

			}
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			logger.log(Level.SEVERE, "Size of Querry " + query.count());
			logger.log(Level.SEVERE, "Type Of"
					+ quer.getQuerryObject().getClass());

			ofy().clear();
			for (SuperModel i : query)
				list.add(i);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	

	/**
	  * Date 28 feb 2017
	  * added by vijay
	  * for creating configs on click in company presenter create configs button
	  */
	
	
	@Override
	public ArrayList<String> createConfigsData(long compnayId) {
		
		ArrayList<String> stringlist = new ArrayList<String>();
			
		Company company = ofy().load().type(Company.class).filter("companyId", compnayId).first().now();
		
		if(company.getCompanyType().equalsIgnoreCase("Pest Control")){
			
			String validationMsg ="";
			
			if(company.getStatus().equalsIgnoreCase("Demo"))
			{
				List<SuperProduct> productlist = ofy().load().type(SuperProduct.class).filter("companyId", compnayId).list();
				if(productlist.size()==0){
					//  this is for product details 
					demoImpl.setProductDetails(compnayId);
					validationMsg = validationMsg+"Products are created"+ "\n";
				}else{
					validationMsg = validationMsg+"Can not create products already some products created"+"\n";
				}
			
				List<Config> configlist = ofy().load().type(Config.class).filter("companyId", compnayId).list();
				if(configlist.size()==0){
				//  this task queue only contains configs value
					demoImpl.setPestControlConfigs(compnayId);
					validationMsg = validationMsg+"Configs created"+"\n";
				}else{
					validationMsg = validationMsg+"Configs can not create already some configs created"+"\n";
				}
				
				List<ConfigCategory> ConfigCategorylist = ofy().load().type(ConfigCategory.class).filter("companyId", compnayId).list();

				if(ConfigCategorylist.size()==0){
					// this task queue only for categories values
					demoImpl.setPestControlCategories(compnayId);
					validationMsg = validationMsg+"Category & Type created"+"\n";

				}else{
					validationMsg = validationMsg+"Category can not create already some Category created"+"\n";
				}
				
				List<ConfigCategory> modulelist = ofy().load().type(ConfigCategory.class).filter("companyId", compnayId).filter("internalType", 13).list();
				logger.log(Level.SEVERE,"Module name list size "+modulelist.size());
				if(modulelist.size()==0){
					//   this task queue contains Modules and document names  
						demoImpl.setDefaultConfigs(compnayId);	
						validationMsg = validationMsg+"Module & Document Name created"+"\n";
				}else{
					validationMsg = validationMsg+"Module can not create already some Module created"+"\n";
				}
				
				
				List<Lead> leadlist = ofy().load().type(Lead.class).filter("companyId", compnayId).list();
				if(leadlist.size()==0){
				//  this is used for creating lead data
					demoImpl.getLeadDatafortaskqueue(compnayId);
					validationMsg = validationMsg+"Lead created"+"\n";
				}else{
					validationMsg = validationMsg+"lead can not create already some lead created"+"\n";

				}
				
				
			}
			
			if(company.getStatus().equalsIgnoreCase("Active"))
			{
					
				List<SuperProduct> productlist = ofy().load().type(SuperProduct.class).filter("companyId", compnayId).list();
				if(productlist.size()==0){
					//  this is for product details 
					demoImpl.setProductDetails(compnayId);
					validationMsg = validationMsg+"Products are created"+ "\n";
				}else{
					validationMsg = validationMsg+"Can not create products already some products created"+"\n";
				}
			
				List<Config> configlist = ofy().load().type(Config.class).filter("companyId", compnayId).list();
				if(configlist.size()<=3){
				//  this task queue only contains configs value
					demoImpl.setPestControlConfigs(compnayId);
					validationMsg = validationMsg+"Configs created"+"\n";
				}else{
					validationMsg = validationMsg+"Configs can not create already some configs created"+"\n";
				}
				
				List<ConfigCategory> ConfigCategorylist = ofy().load().type(ConfigCategory.class).filter("companyId", compnayId).list();

				if(ConfigCategorylist.size()==0){
					// this task queue only for categories values
					demoImpl.setPestControlCategories(compnayId);
					validationMsg = validationMsg+"Category & Type created"+"\n";

				}else{
					validationMsg = validationMsg+"Category can not create already some Category created"+"\n";
				}
				
				List<ConfigCategory> modulelist = ofy().load().type(ConfigCategory.class).filter("companyId", compnayId).filter("internalType", 13).list();
				logger.log(Level.SEVERE,"Module name list size "+modulelist.size());
				if(modulelist.size()==0){
					//   this task queue contains Modules and document names  
						demoImpl.setDefaultConfigs(compnayId);	
						validationMsg = validationMsg+"Module & Document Name created"+"\n";
				}else{
					validationMsg = validationMsg+"Module can not create already some Module created"+"\n";
				}
				
				
			}
			
			stringlist.add(validationMsg);
		}
		
		
		return stringlist;
	}
	

	
	public void createModuleNamesAndDocumentNames(Long companyId, String accessUrl) {
		// TODO Auto-generated method stub
		try {
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "Update Module And Document Name").param("companyId", companyId+"")
					.param("accessUrl", accessUrl));
			
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception Creating module name document name task queue =="+e.getMessage());
		}
	}
	
	public void createSettingModuleConfiguration(Long companyId) {
		try {
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "Default Configuration").param("companyId", companyId+""));
			
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Exception Creating module name document name task queue =="+e.getMessage());
		}
	}
	
	public ArrayList<String> getProcessNamelist() {
		
		ArrayList<String> classessArray=new ArrayList<String>();
		
		classessArray.add("LoneType");
		classessArray.add("PaySlip");
		classessArray.add("LoneEmi");
		classessArray.add("LeaveBalance");
		classessArray.add("LoggedIn");
		classessArray.add("Service");
		classessArray.add("Quotation");
		classessArray.add("Approvals");
		classessArray.add("Contract");
		classessArray.add("Config");
		classessArray.add("ConfigCategory");
		classessArray.add("Type");
		classessArray.add("Company");
		classessArray.add("Activity");
		classessArray.add("TimeReportConfig");
		classessArray.add("Branch");
		classessArray.add("BranchRelation");
		classessArray.add("Employee");
		classessArray.add("EmployeeInfo");
		classessArray.add("EmployeeRelation");
		classessArray.add("User");
		classessArray.add("ItemProduct");
		classessArray.add("SuperProduct");
		classessArray.add("ServiceProduct");
		classessArray.add("Vendor");
		classessArray.add("Expense");
		classessArray.add("Lead");
		classessArray.add("Customer");
		classessArray.add("VendorRelation");
		classessArray.add("CustomerRelation");
		classessArray.add("Category");
		classessArray.add("Purchase");
		classessArray.add("Calendar");
		classessArray.add("GLAccount");
		classessArray.add("SalesOrder");
		classessArray.add("EmployeeLeave");
		classessArray.add("Loan");
		classessArray.add("Country");
		classessArray.add("Grade");
		classessArray.add("LeaveType");
		classessArray.add("LeaveApplication");
		classessArray.add("Shift");
		classessArray.add("ShiftPattern");
		classessArray.add("Department");
		classessArray.add("UserRole");
		classessArray.add("HrProject");
		classessArray.add("HolidayType");
		classessArray.add("EmployeeGroup");
		classessArray.add("LeaveGroup");
		classessArray.add("CtcComponent");
		classessArray.add("CTC");
		classessArray.add("AssignedShift");
		classessArray.add("CompanyAsset");
		classessArray.add("ToolGroup");
		classessArray.add("ServiceProject");
		classessArray.add("ClientSideAsset");
		classessArray.add("EmployeeAttendance");
		classessArray.add("LeaveAllocationProcess");
		classessArray.add("PaySlipAllocationProcess");
		classessArray.add("LiaisonStep");
		classessArray.add("Liaison");
		classessArray.add("LiaisonGroup");
		classessArray.add("SalesQuotation");
		classessArray.add("PriceList");
		classessArray.add("RequsestForQuotation");
		classessArray.add("LetterOfIntent");
		classessArray.add("PettyCash");
		classessArray.add("PettyCashDeposits");
		classessArray.add("BillingDocument");
		classessArray.add("Invoice");
		classessArray.add("CustomerPayment");
		classessArray.add("OtherTaxCharges");
		classessArray.add("TaxesAndCharges");
		classessArray.add("DeliveryNote");
		classessArray.add("PurchaseOrder");
		classessArray.add("PurchaseRequisition");
		classessArray.add("GRN");
		classessArray.add("InventoryInHand");
		classessArray.add("Transfer");
		classessArray.add("IncomingInventory");
		classessArray.add("LocationRelation");
		classessArray.add("InventoryEntries");
		classessArray.add("WareHouse");
		classessArray.add("OutgoingInventory");
		classessArray.add("InventoryProductDetail");
		classessArray.add("StorageLocation");
		classessArray.add("Storagebin");
		classessArray.add("ProductInventoryView");
		classessArray.add("ProductInventoryViewDetails");
		classessArray.add("CompanyPayment");
		classessArray.add("VendorPayment");
		classessArray.add("TaxDetails");
		classessArray.add("State");
		classessArray.add("City");
		classessArray.add("Locality");
		classessArray.add("CheckListStep");
		classessArray.add("CheckListType");
		classessArray.add("ShippingMethod");
		classessArray.add("PackingMethod");
		classessArray.add("EmployeeShift");
		classessArray.add("DateShift");
		classessArray.add("TimeReport");
		classessArray.add("PaySlipRecord");
		classessArray.add("ProductGroupList");
		classessArray.add("ProductGroupDetails");
		classessArray.add("BillOfMaterial");
		classessArray.add("NumberGeneration");
		classessArray.add("AccountingInterface");
		classessArray.add("ContactPersonIdentification");
		classessArray.add("PhysicalInventoryMaintaince");
		classessArray.add("RaiseTicket");
		classessArray.add("MaterialRequestNote");
		classessArray.add("MaterialIssueNote");
		classessArray.add("ProductInventoryTransaction");
		classessArray.add("MaterialMovementNote");
		classessArray.add("MaterialMovementType");
		classessArray.add("ProcessName");
		classessArray.add("InteractionType");
		classessArray.add("ProcessConfiguration");
		classessArray.add("DocumentHistory");
		classessArray.add("");
		classessArray.add("LicenseManagement");
		classessArray.add("CancelSummary");
		classessArray.add("Inspection");
		classessArray.add("BillOfProductMaterial");
		classessArray.add("VendorPriceListDetails");
		classessArray.add("WorkOrder");
		classessArray.add("CustomerBranchDetails");
		classessArray.add("SmsConfiguration");
		classessArray.add("SmsTemplate");
		classessArray.add("IPAddressAuthorization");
		classessArray.add("MultilevelApproval");
		classessArray.add("SmsHistory");
		classessArray.add("Fumigation");
		classessArray.add("CustomerTrainingDetails");
		classessArray.add("EmployeeAdditionalDetails");
		classessArray.add("Overtime");
		classessArray.add("CompanyPayrollRecord");
		classessArray.add("Declaration");
		classessArray.add("Complain");
		classessArray.add("ContractRenewal");
		classessArray.add("TeamManagement");
		classessArray.add("CustomerUser");
		classessArray.add("WhatsNew");
		classessArray.add("PettyCashTransaction");
		classessArray.add("SalesPersonTargets");
		classessArray.add("FinancialCalender");
		classessArray.add("AssesmentReport");
		classessArray.add("CronJob");	
		classessArray.add("MultipleExpenseMngt");	
		
		classessArray.add("C_Billing");	
		classessArray.add("S_Billing");	
		classessArray.add("B_Billing");	
		classessArray.add("I_Billing");	
		classessArray.add("P_Billing");	
		classessArray.add("C_NonBilling");	
		classessArray.add("S_NonBilling");	
		classessArray.add("B_NonBilling");	
		classessArray.add("I_NonBilling");
		classessArray.add("P_NonBilling");	
		classessArray.add("P_Invoice");	
		classessArray.add("ServicePo");	
		classessArray.add("RoleDefinition");
		classessArray.add("EmployeeLevel");
		classessArray.add("CTCTemplate");
		classessArray.add("RegisterDevice");
		classessArray.add("UnitConversion");			
		/** date 24.7.2018 added by komal for employee asset **/
		classessArray.add("EmployeeAsset");
		classessArray.add("EmployeeInvestment");
		classessArray.add("TaxSlab");
		classessArray.add("Investment");
		classessArray.add("ArrearsDetails");
		classessArray.add("EmployeeCheckListType");
		classessArray.add("CNCVersion");
		/** Date : 23-08-2018 BY ANIL **/
		classessArray.add("ProfessionalTax");
		
		
		classessArray.add("Attendance");
		classessArray.add("EmployeeFingerDB");
		classessArray.add("ProjectAllocation");
		/** added by komal for vendor invoice entity **/
		classessArray.add("VendorInvoice");
		/** date 6.10.2018 added by komal **/
		classessArray.add("LWF");
		classessArray.add("VoluntaryPf");
		classessArray.add("VoluntaryPfHistory");
		/** date 08.03.2018 added by komal **/
		classessArray.add("TechnicianWareHouseDetailsList");
		/**
		 * @author Anil , Date : 19-06-2019
		 */
		classessArray.add("PaidLeave");
		classessArray.add("Bonus");
		classessArray.add("ProvidentFund");
		classessArray.add("Esic");
		classessArray.add("ExpensePolicies");
		classessArray.add("EmployeeTrackingDetails");
		/**
		 *  Date : 10/02/2021 By Priyanka.
		 */
		classessArray.add("CronJobConfigration");
		classessArray.add("AndroidTips");
		classessArray.add("AppRegistrationHistory");
		classessArray.add("ScreenMenuConfiguration");

		/**
		 * Date 25-5-2018 by jayshree 
		 */
		return classessArray;
	}
	
	public ArrayList<String> getprocessTypelist() {
		ArrayList<String> processTypeArray = new ArrayList<String>();
		processTypeArray.add("MakeEmailMandatory");
		processTypeArray.add("MakeGoupMandatory");
		processTypeArray.add("DisableCellNumberValidation");
		processTypeArray.add("MakeRefNumber1Mandatory");
		processTypeArray.add("MakeCustomerLevelMandatory");
		processTypeArray.add("MapCustomerCategoryAndTypeaAsaDocumentCategoryAndType");
		processTypeArray.add("HideCustomerGroup");
		processTypeArray.add("MakeCategoryAndTypeDisable");
		processTypeArray.add("ChangeCompanyNameAndFullNameLable");
		processTypeArray.add("MakeCellNumberNonMandatory");
		processTypeArray.add("LeadProjectCoordinator");
		processTypeArray.add("MakeCustomerPopUpEmailMandatory");
		processTypeArray.add("MakeCustomerPopUpCompanyNameMandatory");
		processTypeArray.add("MakeCustomerPopUpPhoneNumberMandatory");
		processTypeArray.add("EnableFreezeLead");
		processTypeArray.add("MakePincodeMandatory");
		processTypeArray.add("CommunicationLogColumn");
		processTypeArray.add("MapCustomerTypeToLeadCategory");
		processTypeArray.add("MakePriceColumnDisable");
		processTypeArray.add("ChangeProductComposite");
		processTypeArray.add("SelfApproval");
		processTypeArray.add("Quotation Service Data");
		processTypeArray.add("printProductPremisesInPdf");
		processTypeArray.add("GSTQuotationPDF");
		processTypeArray.add("QuotationPdf");
		processTypeArray.add("MakeLeadIDMandatory");
		processTypeArray.add("MakeGroupMandatory");
		processTypeArray.add("MakeTypeMandatory");
		processTypeArray.add("MakeCatagoryMandatory");
		processTypeArray.add("PrintAttnInPdf");
		processTypeArray.add("HeaderWithOnlyComapanyDetail");
		processTypeArray.add("QuotationAsLetter");
		processTypeArray.add("DoNotProductServicesTable");
		processTypeArray.add("DoNotProductDescription");
		processTypeArray.add("PestoIndiaQuotations");
		processTypeArray.add("CompanyAsLetterHead");
		processTypeArray.add("PrintProductDiscriptionOnPdf");
		processTypeArray.add("OnlyForPecopp");
		processTypeArray.add("MakeQuotationDateDisable");
		processTypeArray.add("MakeColumnDisable");
		processTypeArray.add("PrintCustomQuotation");
		processTypeArray.add("CreateContractAutoOnMarkSuccessfull");
		processTypeArray.add("ContractServiceData");
		processTypeArray.add("OnlyForFriendsPestControl");
		processTypeArray.add("OnlyForNBHC");
		processTypeArray.add("ConsolitePrice");
		processTypeArray.add("HideOrShowCreateServicesButton");
		processTypeArray.add("ContractReport");
		processTypeArray.add("PrintProductPrimiseInPdf");
		processTypeArray.add("EnableServiceBranch");
		processTypeArray.add("printDESConFirstPPageFlaagForContractAndQuotation.");
		processTypeArray.add("MakeNumberRangeMandatory");
		processTypeArray.add("EnableOnlyAMCContractForContractRenewal");
		processTypeArray.add("DisableAreaWiseBillingForAmc");
		processTypeArray.add("ContractPaymentNonEditable");
		processTypeArray.add("ServiceInvoiceMappingOnContractApproval");
		processTypeArray.add("SalesPersonRestriction");
		processTypeArray.add("ContractStartDateValidation");
		processTypeArray.add("HideBranchWiseBillingOption");
		processTypeArray.add("headerWithOnlycompanyDetail");
		processTypeArray.add("EnableDoNotPrintGSTNumber");
		processTypeArray.add("ContractDownloadNew");
		processTypeArray.add("HideGstinNo");
		processTypeArray.add("MakeLeadAndQuotationIdMandatory");
		processTypeArray.add("OnlyForEVA");
		processTypeArray.add("EnableRenewalContractTypeDefault");
		processTypeArray.add("AddPoDetails");
		processTypeArray.add("MakeContractDateDisable");
		processTypeArray.add("EnableWarehouseAddressasServiceAddress");
		processTypeArray.add("HideTechnicianNameAndContractPeriod");
		processTypeArray.add("EnableShowContractRenewForRenewedContracts");
		processTypeArray.add("DeleteExtraServiceProductWise");
		processTypeArray.add("showCancelButtonForRenew");
		processTypeArray.add("UPDATEADDRESS");
		processTypeArray.add("HideRateAndDiscountColumn");
		processTypeArray.add("DisableDefaultContractLoading");
		processTypeArray.add("PartialPaymentOneInvoiceAndPaymentAsPerrecievedAndBalanceAmt");
		processTypeArray.add("MakeServiceCompletionMandatoryForAMCContract");
		processTypeArray.add("ServiceWiseBillingApprovedStatus");
		processTypeArray.add("EnableAutoInvoice");
		processTypeArray.add("BranchAsCompany");
		processTypeArray.add("BranchLevelRestriction");
		processTypeArray.add("FumigationDetails");
		processTypeArray.add("MakeRefNumberAsCostCenter");
		processTypeArray.add("MakeRefNumber2AsProfitCenter");
		processTypeArray.add("Accounting Interface");
		processTypeArray.add("ServicesScheduleList");
		processTypeArray.add("OnlyForOrionPestSolutions");
		processTypeArray.add("DeliveryNoteDailyEmail");
		processTypeArray.add("PCAMBCustomization");
		processTypeArray.add("DontShowZeroPayableAmtProduct");
		processTypeArray.add("EnableInvoiceEditableForAllRoles");
		processTypeArray.add("ActiveBranchEmailId");
		processTypeArray.add("HideGSTINNumber");
		processTypeArray.add("REFRENCEORDERNUMBER");
		processTypeArray.add("ShowServiceAddress");
		processTypeArray.add("ShowContractDuration");
		processTypeArray.add("EnableRevenueLossReport");
		processTypeArray.add("MakeAdressToUppercase");
		processTypeArray.add("PrintPoDetailsOnInvoice");
		processTypeArray.add("IPCInvoice");
		processTypeArray.add("CustomerBranchNameInInvoiceServiceAddress");
		processTypeArray.add("UserWiseIPAuthorization");
		processTypeArray.add("LeadSaveEmail");
		processTypeArray.add("CustomerTypeForNbhc");
		processTypeArray.add("POPDFV1");
		processTypeArray.add("OnlyForPepcopp");
		processTypeArray.add("DoNotMaterialRequiredValidation");
		processTypeArray.add("DeliveryNoteFromWorkOrder");
		processTypeArray.add("ConfirmationMailToDepartment");
		processTypeArray.add("MakeProjectNameEnable");
		processTypeArray.add("CncEarningColumn");
		processTypeArray.add("EnableCancellationRemark");
		processTypeArray.add("EnableStackDetailsMandatory");
		processTypeArray.add("EnableValidateAssesmentReport");
		processTypeArray.add("HidePlanRescheduleFumigationColoumn");
		processTypeArray.add("HideFumigationButton");
		processTypeArray.add("RESTRICTBYDEFAULTLOADINGOFLIST");
		processTypeArray.add("MakeServicesDirecctlyFromComlain");
		processTypeArray.add("FumigationCertificate");
		processTypeArray.add("OnlyForOrion");
		processTypeArray.add("enableBillableNonMandatory");
		processTypeArray.add("MakeTicketCategoryMandatory");
		processTypeArray.add("EnableComplainStatusHistory");
		processTypeArray.add("EnablePlannedStatusMandatory");
		processTypeArray.add("EnableComplaintDueDate");
		processTypeArray.add("EnableTicketDateRestriction");
		processTypeArray.add("EnableCreateAssementForComplaintService");
		processTypeArray.add("EnableCompleteComplaintService");
		processTypeArray.add("OldFormatRenewalLetter");
		processTypeArray.add("ShowOldContractDetails");
		processTypeArray.add("ProcessedContractRenewalReport");
		processTypeArray.add("UnprocessedContractRenewalReport");
		processTypeArray.add("HIDEBANKDETAILS");
		processTypeArray.add("EnableQuotationMandatoryForContractRenwal");
		processTypeArray.add("NewTallyInterfaceDownload");
		processTypeArray.add("AccountingInterfaceReport");
		processTypeArray.add("EnableShowButtonChangeStatusToCreated");
		processTypeArray.add("ShowCumulativeTaxAmount");
		processTypeArray.add("OnlyForAirotech");
		processTypeArray.add("PoDateEnableTrue");
		processTypeArray.add("PRApprovalEmailToPOPurEngg");
		processTypeArray.add("AddApprovalStatusInPR");
		processTypeArray.add("AllowMultipleEmployeeInExpense");
		processTypeArray.add("ApproveTransferINMMNDirectly");
		processTypeArray.add("Matarial Issue Note");
		processTypeArray.add("EmployeeApprovaProcess");
		processTypeArray.add("EMPLOYEEAPPROVALPROCESS");
		processTypeArray.add("UploadCompositeTracking");
		processTypeArray.add("DisableJoiningDate");
		processTypeArray.add("LwfAsPerEmployeeGroup");
		processTypeArray.add("PrintCtcAlongWithEmployeeBiodata");
		processTypeArray.add("EnableEmployeeDateOfBirthNonMandatory");
		processTypeArray.add("EnableEmployeeCellNoNonMandatory");
		processTypeArray.add("BackDatedJoiningDateNotAllowed");
		processTypeArray.add("MakeReportToMandatory");
		processTypeArray.add("EMAILNOTMANDATORY");
		processTypeArray.add("EnableClosePRBasedOnGRNQty");
		processTypeArray.add("IPAuthorization");
		processTypeArray.add("Contract RenewalSMS");
		processTypeArray.add("ApprovalDailyEmail");
		processTypeArray.add("GRNDailyEmail");
		processTypeArray.add("InvoiceAPDailyEmail");
		processTypeArray.add("InvoiceARDailyEmail");
		processTypeArray.add("LeadDailyEmail");
		processTypeArray.add("PaymentAPDailyEmail");
		processTypeArray.add("PaymentARDailyEmail");
		processTypeArray.add("PhysicalInventoryDailyEmail");
		processTypeArray.add("SalesQuotationDailyEmail");
		processTypeArray.add("ServiceDailyMail");
		processTypeArray.add("ServiceQuotationDailyEmail");
		processTypeArray.add("ContractRenewalSMS");
		processTypeArray.add("CustomerPaymentARSMS");
		processTypeArray.add("CustomerServiceSms");
		processTypeArray.add("ContractRenewalWithoutProcessedCustomerEmail");
		processTypeArray.add("ReorderLevelEmail");
		processTypeArray.add("WarehouseExpiryEmail");
		processTypeArray.add("EVALicenseReminder");
		processTypeArray.add("AutoMultilevelApproval");
		processTypeArray.add("ContractReportGenerationCronJobImpl");
		processTypeArray.add("DueReminderCronJob");
		processTypeArray.add("UnsuccessfulQuotationcronjob");
		processTypeArray.add("PaymentARDueDailyEmailToClient");
		processTypeArray.add("PaymentARTDSCertificateDueEmailToClient");
		processTypeArray.add("CustomerPaymentARCronJobImpl");
		processTypeArray.add("UpdateButton");
		processTypeArray.add("WarehouseExpiredDontLoad");
		processTypeArray.add("EnableLoadExpiryWarehouseStockInHand");
		processTypeArray.add("PaymentAddIntoPettyCash");
		processTypeArray.add("PrintPaySlipOnSamePage");
		processTypeArray.add("PaymentDateMondatory");
		processTypeArray.add("UpdateDataTDSFlagIndex");
		processTypeArray.add("NewEntryNotAllowed");
		processTypeArray.add("VENDORINFONOTMANDATORY");
		processTypeArray.add("MONTHWISENUMBERGENERATION");
		processTypeArray.add("BhashSMSAPI");
		processTypeArray.add("EnableSMSAPIWEBTACTIC");
		processTypeArray.add("DontLoadAllLocalityAndCity");
		processTypeArray.add("HideOrShowModelNoAndSerialNoDetails");
		processTypeArray.add("PrintModelNoAndSerialNo");
		processTypeArray.add("NumberRangeProcess");
		processTypeArray.add("EnableServicePdfWithoutAddressAndCellNumber");
		processTypeArray.add("ServiceBillingDetailMapingProcess");
		processTypeArray.add("ServiceInvoiceMappingOnCompletion");
		processTypeArray.add("CopyReferenceNoFromCustomerBranch");
		processTypeArray.add("PrintServiceVoucher");
		processTypeArray.add("EnableServiceWiseBillingServiceListBulkComplete");
		processTypeArray.add("TECHNICIANWISEMATERIALPOPUP");
		processTypeArray.add("DATETECHNICIANWISESERVICEPOPUP");
		processTypeArray.add("MINBASEDONCONSUMEDQUANTITY");
		processTypeArray.add("SendSRCopyOnServiceCompletion");
		processTypeArray.add("MarkServiceCompletedFromApp");
		processTypeArray.add("ServiceStatusAsPlanned");
		processTypeArray.add("OnlyForOrdientVentures");
		processTypeArray.add("EnableSSL");
		processTypeArray.add("SiddhiServices");
		processTypeArray.add("ShowAttendanceLabel");
		processTypeArray.add("AttendanceLabelAsInput");
		processTypeArray.add("AllowToUploadMultipleSiteAttendance");
		processTypeArray.add("AddSingleAndDoubleOtInAttendanceUploadTemplate");
		processTypeArray.add("ShowWorkingHours");
		processTypeArray.add("TotalSummaryAtBottom");
		processTypeArray.add("AutoCalculatePtAndLwf");
		processTypeArray.add("SiteWisePayrollForReliever");
		processTypeArray.add("DoNotLoadPtAndLwf");
		processTypeArray.add("AutoCalculateHR");
		processTypeArray.add("IncludeOtInGrossEarning");
		processTypeArray.add("Form9");
		processTypeArray.add("Form11");
		processTypeArray.add("FormA");
		processTypeArray.add("Formc");
		processTypeArray.add("Form2aReivised");
		processTypeArray.add("Form6A");
		processTypeArray.add("Form3A");
		processTypeArray.add("FormD");
		processTypeArray.add("ShowEsicNo");
		processTypeArray.add("CustomerCorrespondanceName");
		processTypeArray.add("ShowPfNumberAndEsicNumberAsCompany");
		processTypeArray.add("HideDepartmentName");
		processTypeArray.add("OtherEarningComponent");
		processTypeArray.add("EmployeeDesignationFromProject");
		processTypeArray.add("UPDATEPRODUCTCODE");
		processTypeArray.add("customerNameValidate");
		processTypeArray.add("customerPhoneNoValidate");
		processTypeArray.add("verticalPdfWithLimitedColumns");
		processTypeArray.add("ProductDescriptionBelowTheProduct");
		processTypeArray.add("RestrictUploadProcessForRegisteredEmployee");
		processTypeArray.add("LoadHrTemplate");
		processTypeArray.add("EnableEmployeeName");
		processTypeArray.add("EnableUpdateStockMatserWithTransaction");
		processTypeArray.add("ValidateMMN");
		processTypeArray.add("EnableLockSealStartEndSerialMandatory");
		processTypeArray.add("EnableLoadAllClusterInTransferTo");
		processTypeArray.add("EnableLoadAllWarehousesGlobally");
		processTypeArray.add("MakeShiftMandatory");
		processTypeArray.add("CustomerBranchDownload");
		processTypeArray.add("FranchiseInformation");
		processTypeArray.add("Report");
		processTypeArray.add("EnableAutomaticPOCreation");
		processTypeArray.add("HIDEHR");
		processTypeArray.add("SHOWWARRANTYINFO");
		processTypeArray.add("EnableStoreSerialNumberInDetails");
		processTypeArray.add("EnableCloseMRNBasedOnMMN");
		processTypeArray.add("EnableUploadGRNMMNForLockSeal");
		processTypeArray.add("EnableShowSerialNumber");
		processTypeArray.add("EnableStockUpdation");
		
		/**
		 * Pedio Process Types
		 */
		
		processTypeArray.add("MAKESTARRATINGMANDITORY");
		processTypeArray.add("MAKETECHNICIANREMARKMANDITORY");
		processTypeArray.add("MAKECUSTOMERNAMEMANDITORY");
		processTypeArray.add("MAKECUSTOMERREMARKMANDITORY");
		processTypeArray.add("RETURNQTYVIEW");
		processTypeArray.add("HIDESERVICEFINDING");
		processTypeArray.add("HIDEMATERIALS");
		processTypeArray.add("SHOWEXPENSE");
		processTypeArray.add("DIRECTCOMPLETE");
		processTypeArray.add("TECHNICIANATTENDANCE");
		processTypeArray.add("SITEFINDING");
		processTypeArray.add("PASSBOOKNAYARA");
		processTypeArray.add("HIDETOOLS");
		processTypeArray.add("REJECTSERVICE");
		processTypeArray.add("HIDESRS");
		processTypeArray.add("LIVETRACKING");
		processTypeArray.add("FCMNOTIFICATION");
		processTypeArray.add("ENABLEFCMNOTIFICATION");
		processTypeArray.add("SENDSRCOPYONSERVICECOMPLETION");
		processTypeArray.add("DATETECHNICIANWISESERVICEPOPUP");
		processTypeArray.add("SENDOTPFORSERVICECOMPLETION");
		processTypeArray.add("MINBASEDONCONSUMEDQUANTITY");
		processTypeArray.add("MAKEMATERIALMANDATORY");
		processTypeArray.add("SINGLECONTRACTSRCOPY");
		processTypeArray.add("LoadOperatorDropDown");
		processTypeArray.add("NOTECHNICIANWISEMMN");
		processTypeArray.add("MARKSERVICECOMPLETEDFROMAPP");
		processTypeArray.add("MarkServiceScheduledStatusTrue");
		processTypeArray.add("WarehouseDetailsMandatory");
		processTypeArray.add("UpdateProjectMaterial");
		processTypeArray.add("updateProjectOnTechnicianChange");
		processTypeArray.add("MakeServiceEditableAfterCompletion");
		processTypeArray.add("TECHNICIANWISEMATERIALPOPUP");
		processTypeArray.add("EnableFcmNotification");
		processTypeArray.add("ENABLESERVICESCHEDULENOTIFICATION");
		processTypeArray.add("ENABLEMENUBAR");
		
		return processTypeArray;
	}

	public void createNumberGeneration(long companyId){
		
		ArrayList<String> classessArray = new ArrayList<String>();
		classessArray = getProcessNamelist();
		
		List<NumberGeneration> numbergenerationEntitylist = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).list();

		
		for(int i=0;i<numbergenerationEntitylist.size();i++){
			if(classessArray.contains(numbergenerationEntitylist.get(i).getProcessName().trim())){
				classessArray.remove(numbergenerationEntitylist.get(i).getProcessName().trim());
			}
		}
		if(classessArray.size()!=0){
			ArrayList<NumberGeneration> numberGenerationList=new ArrayList<NumberGeneration>();
			for(int i=0;i<classessArray.size();i++)
			{
				NumberGeneration ngEntity=new NumberGeneration();
				ngEntity.setCompanyId(companyId);
				ngEntity.setProcessName(classessArray.get(i));
				ngEntity.setCount(i+1);

				if(classessArray.get(i).trim().equals("ItemProduct")){
					ngEntity.setNumber(100000000);
				}
				else if(classessArray.get(i).trim().equals("ServiceProduct")){
					ngEntity.setNumber(500000000);
				}
				else if(classessArray.get(i).trim().equals("Quotation")){
					ngEntity.setNumber(500000000);
				}
				else if(classessArray.get(i).trim().equals("Contract")){
					ngEntity.setNumber(500000000);
				}
				//   rohan added this code billing non billing condition
				else if(classessArray.get(i).trim().equals("C_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("S_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("B_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("I_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("P_Billing")){
					ngEntity.setNumber(200000000);
				}
				else if(classessArray.get(i).trim().equals("C_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("S_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("B_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("I_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("P_NonBilling")){
					ngEntity.setNumber(300000000);
				}
				else if(classessArray.get(i).trim().equals("P_Invoice")){
					ngEntity.setNumber(400000000);
				}
				else{
					ngEntity.setNumber(100000000);
				}
				ngEntity.setStatus(true);
				
				numberGenerationList.add(ngEntity);
			}
			
			ofy().save().entities(numberGenerationList).now();
		}
	

	}



	@Override
	public String resetAppid(long companyId, String appId, String companyName, String loggedinuser, int otp,String approverCellNo) {
		logger.log(Level.SEVERE,"resetAppid called companyid="+companyId);
		
		final String CLIENTURL="http://my."+appId+".appspot.com/slick_erp/ResetSystemAPI";
		logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
		String msg="";
		String data="";
		URL url = null;
		try {
			url = new URL(CLIENTURL);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
			return msg="Please add proper client url.";
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(60000); 
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
			return msg="Error occured in establishing connection.";
		}
		logger.log(Level.SEVERE,"STAGE 2");
        con.setDoInput(true);
        con.setDoOutput(true);
//        con.setRequestProperty("Content-Type","application/json; charset=UTF-8");
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
			return msg="Protocol error.";
		}
        logger.log(Level.SEVERE,"STAGE 3");
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 4");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
			writer.write("companyId="+companyId+"&appId="+appId+"&companyName="+companyName+"&loggedinuser="+loggedinuser+"&otp="+otp+"&approverCellNo="+approverCellNo);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 5");
        
        
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		 logger.log(Level.SEVERE,"STAGE 6");
        BufferedReader br = new BufferedReader(new InputStreamReader(is)); 
        String temp;
        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp+"\n"+"\n";
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"STAGE 7");
        
        logger.log(Level.SEVERE,"Data::::::::::"+data);
		
		return msg=data;
		
	}
}
