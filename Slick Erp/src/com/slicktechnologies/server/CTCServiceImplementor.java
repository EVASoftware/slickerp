package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.views.humanresource.ctc.CTCService;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class CTCServiceImplementor extends RemoteServiceServlet implements CTCService 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 626324244163594478L;

	@Override
	public ReturnFromServer save(CTC ctc) {
		ReturnFromServer server;
		 CTC temp;
		if(ctc.getCount()==0)
	    {
			int count;
			long finalNo=0;			
			//New Data is being saved please check for existing calendars
			if(ctc.getCompanyId()!=null)
			{
				temp=ofy().load().type(ctc.getClass()).filter("companyId",ctc.getCompanyId()).filter("calendarName",ctc.getCalendar()).filter("empid",ctc.getEmpid()).first().now();
			}
			else
			{
			 temp=ofy().load().type(ctc.getClass()).filter("calendarName",ctc.getCalendar()).filter("empid",ctc.getEmpid()).first().now();
			}
			if(temp==null)
			{
//				if(ctc.getCompanyId()!=null){
//					  count=ofy().load().type(ctc.getClass()).filter("companyId",ctc.getCompanyId()).count();
//				}
//				else{
//					 count=ofy().load().type(ctc.getClass()).count();
//				}
//				ctc.setCount(count+1);
				
				/**
				 * Adding Number Generation Logic To CTC Entity
				 */
				
				NumberGeneration ng;
				if(ctc.getCompanyId()==null){
					ng=ofy().load().type(NumberGeneration.class).filter("processName", "CTC").filter("status", true).first().now();
				}
				else{
					ng=ofy().load().type(NumberGeneration.class).filter("companyId",ctc.getCompanyId()).filter("processName", "CTC").filter("status", true).first().now();
				}
				
				ng.setNumber(ng.getNumber()+1);
				GenricServiceImpl implSave=new GenricServiceImpl();
				implSave.save(ng);
				finalNo=ng.getNumber();
				int countForCTC=(int) finalNo;
				ctc.setCount(countForCTC);
			}
			else
				return null;
			
	  }
		  ofy().save().entity(ctc).now();
		 server = new ReturnFromServer();
		 server.id=ctc.getId();
		 server.count=ctc.getCount();
	   
		
	    return server;
	  
	   
	   
	   
	}

}
