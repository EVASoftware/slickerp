package com.slicktechnologies.server.addhocdownload;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.slicktechnologies.client.services.XlsxService;

public class CreateXLXSServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1344414370755446424L;

	public static final int FUMIGATIONREPORT=1,SERVICEVALUEREPORT=2,COSTSHEETREPORT=3/** date 08.08.2018 added by komal for cost sheet download **/,
			BOMSERVICELIST=4,CONTRACTPLREPORT=5,CONTRACTSERVICEPANDLREPORT=6,PAYROLLSHEETREPORT=7,GPSHEET=8, ORACLEREPORT=9,
			LEAD=10,QUOTATION=11,CONTRACT=12,SERVICELIST=13,BILLINGLIST=14,INVOICELIST=15,PAYMNETLIST=16,CUSTOMER=17,EMPLOYEE=18,VENDOR=19;
	
	HttpServletResponse responseref;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	
	Logger logger = Logger.getLogger("XlsxWriter");

	XSSFWorkbook   workbook = new XSSFWorkbook();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		try {
			  workbook = new XSSFWorkbook();
			resp.setContentType("application/vnd.ms-excel");
		
		     String reqtype=req.getParameter("type");
		     responseref = resp;
		     System.out.println("Type is "+reqtype);
		     
		     int type=Integer.parseInt(reqtype);
		     
		     getWorkBook(type);
		     
	         
	         ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
	         workbook.write(outByteStream);
	         byte [] outArray = outByteStream.toByteArray();
	       
//	         logger.log(Level.SEVERE,"outArray  -  size -- "  + outArray.length);
	         OutputStream out = resp.getOutputStream();
	         resp.getOutputStream();
	         out.write(outArray);
	         out.flush();
	         out.close();
				
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		// TODO Auto-generated method stub
	}
	
	public void getWorkBook(int type){
//		 CsvWriter csv=new CsvWriter();
		XlsxWriter xlsx = new XlsxWriter();
		 Date d=new Date();
		 String dateString = fmt.format(d);
	
		switch(type)
		{
			case FUMIGATIONREPORT:
				     responseref.setHeader("Content-Disposition", "Attachment;filename=Commudity fumigation Report "+dateString+".xlsx");
				     xlsx.getFumigationReportDetails(workbook);
				     
				     break;
			
			case SERVICEVALUEREPORT:
			     responseref.setHeader("Content-Disposition", "Attachment;filename=Service Value Report "+dateString+".xlsx");
			     xlsx.getServiceValueReportDetails(workbook);
			     
			     break;
			     /** date 08.08.2018 added by komal for cost sheet download **/
			case COSTSHEETREPORT:
			    responseref.setHeader("Content-Disposition", "Attachment;filename=Cost Sheet " + dateString + ".xlsx");
			    xlsx.getCostSheetReport(workbook);
			    break;
			case BOMSERVICELIST:
			     responseref.setHeader("Content-Disposition", "Attachment;filename=Bom Service Material Report "+dateString+".xlsx");
			     xlsx.getBOMServiceReportDetails(workbook);
			     
			     break;	
			case  CONTRACTPLREPORT:
				logger.log(Level.SEVERE,"get contract p&L -- "+XlsxWriter.contractProfitLossReportList.size());
				responseref.setHeader("Content-Disposition", "Attachment;filename=Contract P & L Report "+dateString+".xlsx");
				xlsx.getContractPAndLReport(workbook);
				break;
			case CONTRACTSERVICEPANDLREPORT:
				logger.log(Level.SEVERE,"get contract p&L -- "+XlsxWriter.contractProfitLossReportList.size());
				responseref.setHeader("Content-Disposition", "Attachment;filename=Contract P & L Report "+dateString+".xlsx");
				xlsx.getContractServicePAndLReport(workbook);
				break;
				
			case PAYROLLSHEETREPORT:
				/**31-1-2019 added by Amol for Payroll Sheet Download***/
				responseref.setHeader("Content-Disposition", "Attachment;filename=Payroll  Sheet " + dateString + ".xlsx");
			    xlsx.getPayrollSheetReport(workbook);
			    break;
			    
			case GPSHEET:
				responseref.setHeader("Content-Disposition", "Attachment;filename=GP Sheet " + dateString + ".xlsx");
			    xlsx.getGPSheetReport(workbook);
			    break;
			    
			/**@author Sheetal : 28-04-2022
			 * Des : Creating Oracle Report in excel, requirement by Envocare**/   
			case ORACLEREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CRM Tally Report " + dateString + ".xlsx");
			    xlsx.getCRMTallyReport(workbook);
			    break;
			    
			case LEAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Lead Report " + dateString + ".xlsx");
			    xlsx.getLeadReport(workbook);
			    break;
			    
			case QUOTATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Quotation Report " + dateString + ".xlsx");
			    xlsx.getQuotationReport(workbook);
			    break;		
			    
			case CONTRACT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Contract Report " + dateString + ".xlsx");
			    xlsx.getContractReport(workbook);
			    break;	
			   
			case SERVICELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service List Report " + dateString + ".xlsx");
			    xlsx.getServiceListReport(workbook);
			    break;
			    
			case BILLINGLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Billing List Report " + dateString + ".xlsx");
			    xlsx.getBillingReport(workbook);
			    break;	
			    
			case INVOICELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Invoice List Report " + dateString + ".xlsx");
			    xlsx.getInvoiceReport(workbook);
			    break;	 
			    
			case PAYMNETLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Payment List Report " + dateString + ".xlsx");
			    xlsx.getPaymentReport(workbook);
			    break;	 
			    
			case CUSTOMER:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Customer Report " + dateString + ".xlsx");
			    xlsx.getCustomerReport(workbook);
			    break;	
			    
			case EMPLOYEE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Employee Report " + dateString + ".xlsx");
			    xlsx.getEmployeeReport(workbook);
			    break;	
			    
			case VENDOR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Vendor Report " + dateString + ".xlsx");
			    xlsx.getVendorReport(workbook);
			    break;	
		}
	}
	
}
