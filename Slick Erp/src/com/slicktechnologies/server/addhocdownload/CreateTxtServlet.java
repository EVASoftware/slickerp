package com.slicktechnologies.server.addhocdownload;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreateTxtServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6082947783355379505L;
	public static final int PFUPLOAD=1;
	
	HttpServletResponse responseref;
	PrintWriter pw;

	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		
		response.setContentType("text/plain");
		responseref = response;

		pw = response.getWriter();
		String reqtype = request.getParameter("type");
		System.out.println("Type is " + reqtype);
		int type = Integer.parseInt(reqtype);
		
		getTxt(type, request);

		pw.flush();
		pw.close();
	}
	
	public void getTxt(int type, HttpServletRequest request) throws IOException{
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		TxtWriter txt=new TxtWriter();
		Date d=new Date();
		String dateString = fmt.format(d);
		
		switch(type){
			case PFUPLOAD:
	            responseref.setHeader("Content-Disposition", "Attachment;filename=PF Report "+dateString+".txt");
	            txt.getPFlist(pw);
				break;
		}
	}

}
