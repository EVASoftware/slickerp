package com.slicktechnologies.server.addhocdownload;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.DocumentException;
import com.slicktechnologies.client.services.XlsxService;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.EquipmentRentalTable;
import com.slicktechnologies.server.addhocprinting.PdfUpdated;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.CommodityFumigationDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.OtherServices;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class XlsxWriter extends RemoteServiceServlet implements XlsxService{
	double  permonth=0;
	double  mcpermonth=0;
	double  inspermonth=0;
	DecimalFormat df1=new DecimalFormat("0");
	double Qty=0;
	double totalcost=0;
	double rentalon=0;
	double interstonmachinery=0;
	double maintanince=0;
	double hKcharges=0;
	double totaltotalcost=0;
	double totalValueIncludingManagementFees=0;
	
	/***Date 25-2-2019 added by Amol for adding CCA option in cost sheet*******/
    boolean hideColumn=false;
	/**
	 * 
	 */
    StaffingDetails staff;
	   EquipmentRental equipmentrental1;
	private static final long serialVersionUID = -2405431807677973034L;
	
	 Row row1;
	Logger logger = Logger.getLogger("XlsxWriter");
	
	XSSFCellStyle style2,boldStyle ;
	public static ArrayList<CommodityFumigationDetails>  commodityFumigation = new ArrayList<CommodityFumigationDetails>();
	public static TreeMap<String,String> proName = new TreeMap<String,String>();
	public  static ArrayList<String> branchList = new ArrayList<String>();
	
	
	
	/**
	 *  nidhi
	 *  16-11-2017
	 *  for service Value Report 
	 */
	public static ArrayList<CommodityFumigationDetails>  commodityServiceValue = new ArrayList<CommodityFumigationDetails>();
	public  static ArrayList<String> serviceBranchList = new ArrayList<String>();
	public static ArrayList<CommodityFumigationDetails>  commodityCancledServiceValue = new ArrayList<CommodityFumigationDetails>();
	public static ArrayList<CommodityFumigationDetails>  commodityscheduleServiceValue = new ArrayList<CommodityFumigationDetails>();
	/**
	 *  end schedule
	 */
	
	/** date 08.08.2018 added by komal for cost sheet **/
	public static CNC cnc = new CNC(); 
	/*****31-1-2019 added by amol for payrollsheet****/
	public static CNC cnc1 =new CNC();
	private SimpleDateFormat fmt= new SimpleDateFormat("dd/MM/yyyy");
	int rowCount = 0,columnCount = 0 ;
	/**
	 * nidhi
	 * 1-10-2018
	 * @param workbook
	 */
	public static ArrayList<Service> bomServiceList = new ArrayList<Service>();
	/**
	 * nidhi
	 * 1-10-2018
	 * @param workbook
	 */
	public static ArrayList<MaterialConsumptionReport> contractProfitLossReportList = new ArrayList<MaterialConsumptionReport>();
	public static String date="";
	public static String segment="";
	public static long companyId1 = 0 ;
	public static Date FromDate;
	public static Date ToDate;
	
	/** date 8.4.2019 added by komal for service wise contract p and l**/
	public static String branch = "";
	public static ArrayList<Service> contractPAndLReportList = new ArrayList<Service>();
	
	/**
	 * @author Anil , Date : 12-10-2019
	 * For GP sheet
	 */
	double totalPayExcludingManagementFees;
	double totalPayWithoutRelieverFormula;
	double totalManagementFees;
	double totalOfActualEquipmentCost;
	double totalEquipmentCost;
	double totalEquipmentRepair;
	double totalOtherServices;
	
	DecimalFormat decimalformat = new DecimalFormat("0.00");

	
	public static ArrayList<String> leadlist = new ArrayList<String>();
	static int leadColumnCount=0;
	
	public static ArrayList<String> quotationlist = new ArrayList<String>();
	static int quotationColumnCount=0;
	
	public static ArrayList<String> contractlist = new ArrayList<String>();
	static int contractColumnCount=0;
	
	public static ArrayList<String> servicelist = new ArrayList<String>();
	static int serviceColumnCount=0;
	
	public static ArrayList<String> Billinglist = new ArrayList<String>();
	static int billingColumnCount=0;
	
	public static ArrayList<String> invoicelist = new ArrayList<String>();
	static int invoiceColumnCount=0;
	
	public static ArrayList<String> summaryPaymentlist = new ArrayList<String>();
	static int summaryPaymentColumnCount=0;
	
	public static ArrayList<String> paymentlist = new ArrayList<String>();
	static int paymentColumnCount=0;
	
	public static ArrayList<String> customerlist = new ArrayList<String>();
	static int customerColumnCount=0;
	
	public static ArrayList<String> employeelist = new ArrayList<String>();
	static int employeeColumnCount=0;
	
	public static ArrayList<String> vendorlist = new ArrayList<String>();
	static int vendorColumnCount=0;
	
	public void getFumigationReportDetails(XSSFWorkbook  workbook){
		try {
			
			if(workbook.getNumberOfSheets()>0){
				for(int i= 0 ;i<= workbook.getNumberOfSheets() ;i++){
					workbook.removeSheetAt(i);
				}
			}
			  XSSFSheet sheet = workbook.createSheet("Service Fumigation Report" + new Date());
			
			  
			  List<CommodityFumigationDetails> commodityFumigationList = commodityFumigation;
				List<String> brnahcList =   branchList;
				int srno = 0;
				
				HashMap<String, Double> proTotal = new HashMap<String, Double>();
			  
			    HashMap<String, Double> prodayTotal = new HashMap<String, Double>();
			    
			    
			    
			    for(String pro : proName.keySet()){
			    	proTotal.put(pro, 0.0);
			    	prodayTotal.put(pro, 0.0);
			    }
			    
			    logger.log(Level.SEVERE,"pro name -- " + proName.toString());
			    
			    Cell cell;
			    Row row;
			    
			    /**
				 * Date 16-10-2018 By Vijay
				 * Des :- if there is no data then show messge of No Data found on excel
				 */
			    if(commodityFumigationList.size()==0){
				    getMessageNoDataFound(sheet,sheet.createRow(rowCount),workbook);
				    return;
			    }
			    
			    getServiceFumigationReportHeader(sheet,sheet.createRow(rowCount), proName,commodityFumigationList.get(0).getDate(),workbook);
			    
			    /**
		    	 * Date 08-09-2018 By Vijay
		    	 * Des :- for NBHC CCPM  Service Date hashset for avoid duplicate entry for proper calculation with date wise
		    	 */
		    		HashSet<Date>serviceDateList = new HashSet<Date>(); 
		    		for(CommodityFumigationDetails commodity : commodityFumigationList){
		    			serviceDateList.add(commodity.getSummaryDate());
		    		}

		    		List<Date> listServiceDate = new ArrayList<Date>(serviceDateList);
		    		Collections.sort(listServiceDate);
		    		
		    		/**
		    		 * ends here			    		
		    		 */
		    		
		    		for (String tobj : brnahcList) {
			    	
			    		for(Date serviceDate :listServiceDate){
						
			    			row = sheet.createRow(++rowCount);
					    	  
					    	   columnCount = 0;
					    	   
					    	   cell = row.createCell(columnCount);
				              
				               cell.setCellValue((int) ++srno);
				               
				               cell = row.createCell(++columnCount);
				               
				               cell.setCellValue((String) tobj);
				               
				               cell = row.createCell(++columnCount);
				               
				               cell.setCellValue((String) fmt.format(serviceDate));
				               for(String pro : proName.keySet()){
				            	   
				            	boolean getFlag = false;
				            for(CommodityFumigationDetails commList : commodityFumigationList){
								System.out.println("Product Name =="+pro);
								if(commList.getProName().equals(pro) && tobj.equals(commList.getBranchName()) && serviceDate.equals(commList.getSummaryDate())){
									
									double total = proTotal.get(pro);
									total = total + commList.getMonthWiseTotal();
									
									proTotal.put(pro, total);
									
									cell = row.createCell(++columnCount);
									cell.setCellValue(total);

									
									cell = row.createCell(++columnCount);
									cell.setCellValue((double) commList.getDayWiseTotal());
									
									total = prodayTotal.get(pro);
									total = total + commList.getDayWiseTotal();
									
									prodayTotal.put(pro, total);
									getFlag = true;
									logger.log(Level.SEVERE," pro - " + pro + " branch -- "+ tobj + " day wise -  " +commList.getDayWiseTotal());
							
								}
								
							}
						
							if(!getFlag){
								{
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
								}
							}
					   }
			        }
						
				}
					
			    
			    style2 = workbook.createCellStyle();
//			      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
//			      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			      
			      XSSFFont font = workbook.createFont();
			      font.setFontHeightInPoints((short) 11);
			      font.setFontName("Times New Roman");
			      font.setBold(true);;
			      
			      style2.setFont(font);
			    
			    columnCount=0;
			    row = sheet.createRow(++rowCount);
			    /**
		    	 * Date 08-09-2018 By Vijay
		    	 * Des :- Date wise report updated
		    	 */
			    
//				String chStr = getChart(0);
//				String chStr1 = getChart(1);
//				
//				 String rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
//				sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
			    /**
			     * ends here
			     */
			    
			    ++columnCount;
				cell = row.createCell(columnCount);
				cell.setCellStyle(style2);
				cell.setCellValue((String) "Total");
				 
				++columnCount;
				for(String str : proName.keySet()){
					
					
						
					 cell = row.createCell(++columnCount);
					 cell.setCellStyle(style2);
					 cell.setCellValue((double) proTotal.get(str));
					 
					 cell = row.createCell(++columnCount);
					 cell.setCellStyle(style2);
					 cell.setCellValue((double) prodayTotal.get(str));
				}
					
			  
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, " get error in fumigation print --"+e.getMessage());
		}
		
	}
	

	private void getServiceFumigationReportHeader( XSSFSheet sheet, Row row, TreeMap<String,String> proName,Date date,XSSFWorkbook  workbook) {
		
		
		int	columnCount = 0;
		Cell cell;
		
		 style2 = workbook.createCellStyle();
	      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	      
	      XSSFFont font = workbook.createFont();
	      font.setFontHeightInPoints((short) 11);
	      font.setFontName("Times New Roman");
	      font.setBold(true);;
	      
	      style2.setFont(font);
	      
	
		cell = row.createCell(columnCount);
		cell.setCellValue((String) "Ser No.");
		cell.setCellStyle(style2);
		
		
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Branch");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Service Date");
		cell.setCellStyle(style2);
		
		
		int count = 3;
		int rowChart = 1;
		logger.log(Level.SEVERE ," pro key set-- "+ proName.keySet());
		
		
		for(String str : proName.keySet()){
			
			String chStr = getChart(count);
			String chStr1 = getChart(++count);
			sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+rowChart+":"+chStr1+""+rowChart));
			cell = row.createCell(++columnCount);
			cell.setCellStyle(style2);
			cell.setCellValue((String) str + " ("+proName.get(str) + ")");
			count++;
			++columnCount;
		}
		
		row = sheet.createRow(++rowCount);
		columnCount=0;
		sheet.addMergedRegion(CellRangeAddress.valueOf("C1:C2"));
		sheet.addMergedRegion(CellRangeAddress.valueOf("B1:B2"));
		sheet.addMergedRegion(CellRangeAddress.valueOf("A1:A2"));
		++columnCount;
		++columnCount;
		for(String str : proName.keySet()){
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Month to date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) fmt.format(date));
			cell.setCellStyle(style2);
			
		}
		
	}
	


//AFC-10	AFC-10	CON-02	CON-02	AFC-02	AFC-02	BNF-01	BNF-01	CON-01	CON-01

	

	public String getChart(int i){
		int counter=59;
		String result="";
//        for(int x=0; x<=counter; x++)
//        {   
            int quotient, remainder;
            
            quotient=i;

            while (quotient >= 0)
            {
                remainder = quotient % 26;
                result = (char)(remainder + 65) + result;
                quotient = (int)Math.floor(quotient/26) - 1;
            }
            System.out.print(result+ " ");
//        }
        return result;
	}

	       
	
	
	

	public void getServiceValueReportDetails(XSSFWorkbook  workbook){
		try {
			
			if(workbook.getNumberOfSheets()>0){
				for(int i= 0 ;i<= workbook.getNumberOfSheets() ;i++){
					workbook.removeSheetAt(i);
				}
			}
			  XSSFSheet sheet = workbook.createSheet("Service Fumigation Report" + new Date());
			
			  
			  List<CommodityFumigationDetails> commodityFumigationList = commodityServiceValue;
			  List<CommodityFumigationDetails> commodityCancledService = commodityCancledServiceValue;
			  List<CommodityFumigationDetails> commodityScheduleService = commodityscheduleServiceValue;
			  
			  
				List<String> brnahcList =   serviceBranchList;
				int srno = 0;
				
			
			      
			    Cell cell;
			    Row row;
			    /**
				 * Date 16-10-2018 By Vijay
				 * Des :- if there is no data then show messge of No Data found on excel
				 */
			    if(commodityFumigationList.size()==0){
				    getMessageNoDataFound(sheet,sheet.createRow(rowCount),workbook);
				    return;
			    }
			    
			    DecimalFormat df = new DecimalFormat("#.##");
			   /**
		       * Updated By: Viraj
		       * Date: 13-06-2019
		       * Description: To Show From Date and To Date
		       */
			    rowCount = 0;
		    	row = sheet.createRow(rowCount);
		        cell = row.createCell(columnCount);
				cell.setCellValue((String) "From Date:"+ fmt.format(FromDate));
				cell.setCellStyle(style2);
			      
				cell = row.createCell(++columnCount);
				cell.setCellValue((String) "To Date: "+ fmt.format(ToDate));
				cell.setCellStyle(style2);
				rowCount++;
				/** Ends **/
			    getServiceValueReportHeader(sheet,sheet.createRow(rowCount),commodityFumigationList.get(0).getDate(),workbook);
			 
				style2 = workbook.createCellStyle();
//			      style2.setAlignment(XSSFCellStyle.);
//			      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			      
			      XSSFFont font = workbook.createFont();
			      font.setFontHeightInPoints((short) 11);
			      font.setFontName("Times New Roman");
			      font.setBold(true);;
			      
			      style2.setFont(font);
			    
			    double total = 0 ,todayToatal = 0,cancleTotal = 0 ,todayCancleToatal = 0,scheduleTotal = 0 ,todayScheduleToatal = 0;
			   double finalTotalfrMonth = 0, finalTotalfrDay = 0, finalAmount = 0;
			   
			   /**
			     * Date 03-09-2018 By Vijay 
			     * DES :- NBHC CCPM Date wise report below old code commented and updated new code
			     */
			   LinkedHashMap<String, Double> totalPro = new LinkedHashMap<String, Double>();
				totalPro.put("Completed", 0.0);
				totalPro.put("Scheduled", 0.0);
				totalPro.put("Cancelled", 0.0);
				
				List<String> serviceStatuslist = new ArrayList<String>();
				serviceStatuslist.add("Completed");
				serviceStatuslist.add("Scheduled");
				serviceStatuslist.add("Cancelled");
				
					
				
				/**
			     * Date 03-09-2018 By Vijay 
			     * DES :- NBHC CCPM Date wise report below and above old code commented and updated new code
			     */
		    	
			
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
	           	{
	        		if(commodityFumigationList.size()!=0){
	    			Comparator<CommodityFumigationDetails> servicelistComparator = new Comparator<CommodityFumigationDetails>() {
	    				
	    				@Override
	    				public int compare(CommodityFumigationDetails s1, CommodityFumigationDetails s2) {
	    					// TODO Auto-generated method stub
	    					Date serviceDate = s1.getSummaryDate();
	    					Date serviceDate2 = s2.getSummaryDate();
	    					return serviceDate.compareTo(serviceDate2);
	    				}
	    			};
	    			Collections.sort(commodityFumigationList,servicelistComparator);
	    		  }
	        		double finalTotayForDay = 0;
	        		
					logger.log(Level.SEVERE,"commodityFumigationList == =="+commodityFumigationList.size());
					
	        		TreeSet<Date> serviceDatelist = new TreeSet<Date>();
					for(CommodityFumigationDetails service : commodityFumigationList){
//						serviceDatelist.add(service.getSummaryDate());
						String strDate = fmt.format(service.getSummaryDate());
						serviceDatelist.add(fmt.parse(strDate));
					}
					
					logger.log(Level.SEVERE,"serviceDatelist == && -- "+serviceDatelist.size());
					 for (String tobj : brnahcList) {
					    	finalTotalfrMonth = 0;
					    	finalTotalfrDay = 0;
					
					for(Date serviceDate : serviceDatelist){
						logger.log(Level.SEVERE,"Service Date =="+serviceDate);
							row = sheet.createRow(++rowCount);
						   columnCount = 0;
					    	   
				    	   cell = row.createCell(columnCount);
			              
			               cell.setCellValue((int) ++srno);
			               
			               cell = row.createCell(++columnCount);
			               
			               cell.setCellValue((String) tobj);
			               
						    cell = row.createCell(++columnCount);
			                cell.setCellValue((String) fmt.format(serviceDate));
			                
		                double totalforDay = 0;
					for(int i=0;i<serviceStatuslist.size();i++){
						boolean flag= false;
						double satusWiseDayTotal = 0;
					for(CommodityFumigationDetails commList : commodityFumigationList){
						  	System.out.println(commList.getSummaryDate().after(serviceDate) && commList.getSummaryDate().before(serviceDate) );
							if( tobj.equals(commList.getBranchName()) && commList.getSummaryDate().equals(serviceDate) && commList.getStatus().equals(serviceStatuslist.get(i)) ){
								
								flag = true;
								
								double DayTotal = totalPro.get(serviceStatuslist.get(i));
								DayTotal = DayTotal + commList.getMonthWiseTotal();
								
								totalPro.put(serviceStatuslist.get(i), DayTotal);
								
								if(commList.getStatus().equals("Cancelled")){
									totalforDay =totalforDay - commList.getDayWiseTotal();
								}else{
									totalforDay +=commList.getDayWiseTotal();
								}
								satusWiseDayTotal += commList.getDayWiseTotal();
								logger.log(Level.SEVERE," todayToatal - " + todayToatal + " branch -- "+ tobj + " total -  " +total);
							}
							
						}
					
					if(flag){
						double DayTotal = totalPro.get(serviceStatuslist.get(i));
						cell = row.createCell(++columnCount);
						cell.setCellValue( df.format(DayTotal));

						cell = row.createCell(++columnCount);
						cell.setCellValue(  df.format(satusWiseDayTotal));
					}
					
					if(!flag){
						cell = row.createCell(++columnCount);
						cell.setCellValue("");
						 
						cell = row.createCell(++columnCount);
						cell.setCellValue("");
					}
					
					}
					
					cell = row.createCell(++columnCount);
					cell.setCellStyle(style2);
					cell.setCellValue(df.format(totalforDay));
					
					System.out.println("finalTotalfrDay =="+finalTotalfrDay);
					
					finalTotayForDay +=totalforDay;
					
					finalAmount = finalTotayForDay;
					
	           	}
					
				}
			}
	           	
			    columnCount=0;
			    row = sheet.createRow(++rowCount);
			    
			    /**
			     * Date 03-09-2018 By Vijay 
			     * DES :- NBHC CCPM Date wise report below old code commented
			     */
			    
				
				/**
			     * ends here
			     */
			    ++columnCount;
			    ++columnCount;
			    
				cell = row.createCell(columnCount);
				cell.setCellStyle(style2);
				cell.setCellValue((String) "Total");
				
				
				
				
				/**
			     * Date 03-09-2018 By Vijay 
			     * DES :- NBHC CCPM Date wise report below old code commented
			     * for total caculation
			     */
				 for(String str : totalPro.keySet()){
					 cell = row.createCell(++columnCount);
					 cell.setCellStyle(style2);
					 cell.setCellValue(df.format((double) totalPro.get(str)));

					 cell = row.createCell(++columnCount);
					 cell.setCellStyle(style2);
					 cell.setCellValue(df.format((double) totalPro.get(str)));
				 }
				 /**
				  * ends here
				  */
				 
				 	cell = row.createCell(++columnCount);
					cell.setCellStyle(style2);
					cell.setCellValue(df.format(finalAmount));
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, " get error in fumigation print --"+e.getMessage());
		}
		
	}
	
	
	private void getServiceValueReportHeader( XSSFSheet sheet, Row row,Date date,XSSFWorkbook  workbook) {
		
		logger.log(Level.SEVERE, "getServiceValueReportHeader --"+" FROM DATE : "+FromDate+" TO DATE : "+ToDate);
		int	columnCount = 0;
		Cell cell;
		
		 style2 = workbook.createCellStyle();
	      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	      
	      XSSFFont font = workbook.createFont();
	      font.setFontHeightInPoints((short) 11);
	      font.setFontName("Times New Roman");
	      font.setBold(true);
	      
	      style2.setFont(font);
		
		cell = row.createCell(columnCount);
		cell.setCellValue((String) "Ser No.");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Branch");
		cell.setCellStyle(style2);
		
		
		/**
		 * Date 03-09-2018 By Vijay
		 * Des :- NBHC CCPM Date wise report added and updated columns code accordingly
		 */
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Date");
		cell.setCellStyle(style2);
		

		sheet.addMergedRegion(CellRangeAddress.valueOf("D2:E2"));		//Updated by:Viraj Date:13-06-2019 Description: To set excel file header columns
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Completed Service Revenue");
		cell.setCellStyle(style2);
		
		++columnCount;
		
		
		
		sheet.addMergedRegion(CellRangeAddress.valueOf("F2:G2"));		//Updated by:Viraj Date:13-06-2019 Description: To set excel file header columns
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Schedule Service Revenue");
		cell.setCellStyle(style2);
		
		++columnCount;
		
		sheet.addMergedRegion(CellRangeAddress.valueOf("H2:I2"));		//Updated by:Viraj Date:13-06-2019 Description: To set excel file header columns
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Cancelled Service Revenue");
		cell.setCellStyle(style2);
		
		++columnCount;
//		sheet.addMergedRegion(CellRangeAddress.valueOf("I1:J1"));
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Total Service Revenue");
		cell.setCellStyle(style2);
		
		
		
		row = sheet.createRow(++rowCount);
		columnCount=0;
		sheet.addMergedRegion(CellRangeAddress.valueOf("B2:B3"));		//Updated by:Viraj Date:13-06-2019 Description: To set excel file header columns
		sheet.addMergedRegion(CellRangeAddress.valueOf("A2:A3"));		//Updated by:Viraj Date:13-06-2019 Description: To set excel file header columns
		sheet.addMergedRegion(CellRangeAddress.valueOf("C2:C3"));		//Updated by:Viraj Date:13-06-2019 Description: To set excel file header columns
		
//		++columnCount;
		
		int count = 2;
		int rowChart = 1;
		logger.log(Level.SEVERE ," pro key set-- "+ proName.keySet());
		
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Month to date");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) fmt.format(date));
		cell.setCellStyle(style2);
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Month to date");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) fmt.format(date));
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Month to date");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) fmt.format(date));
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Month to date");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) fmt.format(date));
		cell.setCellStyle(style2);
		
	}

	
	public ArrayList<String> getFumigationReportDetailsList(XSSFWorkbook  workbook){
		ArrayList<String> deatilsList = new ArrayList<String>();
		try {
			
//			  XSSFSheet sheet = workbook.createSheet("Service Fumigation Report" + new Date());
			
			  
			  List<CommodityFumigationDetails> commodityFumigationList = commodityFumigation;
				List<String> brnahcList =   branchList;
				int srno = 0;
				
				HashMap<String, Double> proTotal = new HashMap<String, Double>();
			  
			    HashMap<String, Double> prodayTotal = new HashMap<String, Double>();
			    
			    
			    
			    for(String pro : proName.keySet()){
			    	proTotal.put(pro, 0.0);
			    	prodayTotal.put(pro, 0.0);
			    }
			    
			    
			    deatilsList.add("##");
			    deatilsList.add("##");
				
				for(String str : proName.keySet()){
					
					deatilsList.add(fmt.format(commodityFumigationList.get(0).getDate())+"");
					deatilsList.add("Month to date"+"");
				}
			    logger.log(Level.SEVERE,"pro name -- " + proName.toString());
			    
			    Cell cell;
			    Row row;
			    
			    
			    
			    for (String tobj : brnahcList) {
//			    	   row = sheet.createRow(++rowCount);
			    	  
			    	   columnCount = 0;
			    	   
		               
		               deatilsList.add(++srno +"");
		               deatilsList.add(tobj);
		           	logger.log(Level.SEVERE ," pro key set-- "+ proName.keySet());
						for(String pro : proName.keySet()){
							
							for(CommodityFumigationDetails commList : commodityFumigationList){
									if(commList.getProName().equals(pro) && tobj.equals(commList.getBranchName())){
										 
										
										deatilsList.add(commList.getDayWiseTotal()+"");
										deatilsList.add(commList.getMonthWiseTotal()+"");
										
										double total = proTotal.get(pro);
										total = total + commList.getMonthWiseTotal();
										
										proTotal.put(pro, total);
										
										total = prodayTotal.get(pro);
										total = total + commList.getDayWiseTotal();
										
										prodayTotal.put(pro, total);
										logger.log(Level.SEVERE," pro - " + pro + " branch -- "+ tobj + " day wise -  " +commList.getDayWiseTotal());
								
									}
								}
						}
				}
					
			    columnCount=0;
//			    row = sheet.createRow(++rowCount);
				
				String chStr = getChart(0);
				String chStr1 = getChart(1);
				
//				 String rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
//				sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
//				cell = row.createCell(columnCount);
//				cell.setCellStyle(style2);
//				cell.setCellValue((String) "Total");
//				 
				deatilsList.add("Total :");
				deatilsList.add("$#");
				
				++columnCount;
				for(String str : proName.keySet()){
					
					
					deatilsList.add( prodayTotal.get(str)+"");
					deatilsList.add(proTotal.get(str)+"");
					 
				}
					
			  
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, " get error in fumigation print --"+e.getMessage());
		}
		return deatilsList;
	}
	
	public ArrayList<String> getServiceFumigationReportHeader(Date date,XSSFWorkbook  workbook) {
		ArrayList<String> detailList = new ArrayList<String>();
		
		int	columnCount = 0;
		Cell cell;
		
		detailList.add("Ser No.");
		detailList.add("$##");
		detailList.add("Brach");
		detailList.add("$##");

		int count = 2;
		int rowChart = 1;
		logger.log(Level.SEVERE ," pro key set-- "+ proName.keySet());
		
		
		for(String str : proName.keySet()){
			
			detailList.add(str + " ("+proName.get(str) + ")");
			detailList.add("$#");
			
		}
		
		
		return detailList;
	}
	
	/**
	 *  nidhi
	 *  16-11-2017
	 *  service value report
	 */
	
	public ArrayList<String> getServiceValueReportDetailsList(XSSFWorkbook  workbook){
		ArrayList<String> detailList = new ArrayList<String>();
		try {
			
			if(workbook.getNumberOfSheets()>0){
				for(int i= 0 ;i<= workbook.getNumberOfSheets() ;i++){
					workbook.removeSheetAt(i);
				}
			}
			  XSSFSheet sheet = workbook.createSheet("Service Fumigation Report" + new Date());
			
			  
			  List<CommodityFumigationDetails> commodityFumigationList = commodityServiceValue;
			  List<CommodityFumigationDetails> commodityCancledService = commodityCancledServiceValue;
			  List<CommodityFumigationDetails> commodityScheduleService = commodityscheduleServiceValue;
			  
			  
				List<String> brnahcList =   serviceBranchList;
				int srno = 0;
				
			    
			    Cell cell;
			    Row row;
			    detailList.add("##");
			    detailList.add("##");
			    detailList.add("Month to date");
				detailList.add(""+fmt.format(commodityFumigationList.get(0).getDate()));
				detailList.add("Month to date");
				detailList.add(""+fmt.format(commodityFumigationList.get(0).getDate()));
				detailList.add("Month to date");
				detailList.add(""+fmt.format(commodityFumigationList.get(0).getDate()));
			    
			    double total = 0 ,todayToatal = 0,cancleTotal = 0 ,todayCancleToatal = 0,scheduleTotal = 0 ,todayScheduleToatal = 0;
			    
			    for (String tobj : brnahcList) {
//			    	   row = sheet.createRow(++rowCount);
			    	  
			    	   columnCount = 0;
			    	   
			    	   detailList.add(++srno+"");
			    	   detailList.add(tobj);
		           	logger.log(Level.SEVERE ," pro key set-- "+ proName.keySet());
					{
							
							for(CommodityFumigationDetails commList : commodityFumigationList){
									if( tobj.equals(commList.getBranchName())){
										 
//										cell = row.createCell(++columnCount);
//										cell.setCellValue((double) commList.getMonthWiseTotal());
//										 
//										cell = row.createCell(++columnCount);
//										cell.setCellValue((double) commList.getDayWiseTotal());
										
										detailList.add(commList.getMonthWiseTotal()+"");
										detailList.add(commList.getDayWiseTotal()+"");
										
										total = total + commList.getMonthWiseTotal();
										
										
										todayToatal = todayToatal + commList.getDayWiseTotal();
										
										logger.log(Level.SEVERE," todayToatal - " + todayToatal + " branch -- "+ tobj + " total -  " +total);
								
									}
								}
							for(CommodityFumigationDetails commList : commodityCancledService){
								if( tobj.equals(commList.getBranchName())){
									 
//									cell = row.createCell(++columnCount);
//									cell.setCellValue((double) commList.getMonthWiseTotal());
//									 
//									cell = row.createCell(++columnCount);
//									cell.setCellValue((double) commList.getDayWiseTotal());
									
									detailList.add(commList.getMonthWiseTotal()+"");
									detailList.add(commList.getDayWiseTotal()+"");
									
									
									
									
									cancleTotal = cancleTotal + commList.getMonthWiseTotal();
									
									
									todayCancleToatal = todayCancleToatal + commList.getDayWiseTotal();
									
									logger.log(Level.SEVERE," todayToatal - " + todayToatal + " branch -- "+ tobj + " total -  " +total);
							
								}
							}
							
							for(CommodityFumigationDetails commList : commodityScheduleService){
								if( tobj.equals(commList.getBranchName())){
									 
									
									detailList.add(commList.getMonthWiseTotal()+"");
									detailList.add(commList.getDayWiseTotal()+"");
									
									
									scheduleTotal = scheduleTotal + commList.getMonthWiseTotal();
									
									
									todayScheduleToatal = todayScheduleToatal + commList.getDayWiseTotal();
									
									logger.log(Level.SEVERE," todayToatal - " + todayToatal + " branch -- "+ tobj + " total -  " +total);
							
								}
							}
						}
				}
					
			    columnCount=0;
			    row = sheet.createRow(++rowCount);
			    
				String chStr = getChart(0);
				String chStr1 = getChart(1);
				
				
				
				
				detailList.add("Total");
				detailList.add("$#");
				++columnCount;
			
				
				detailList.add(total+"");
				detailList.add(todayToatal+"");
				detailList.add(cancleTotal+"");
				detailList.add(todayCancleToatal+"");
				detailList.add(scheduleTotal+"");
				detailList.add(todayScheduleToatal+"");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, " get error in fumigation print --"+e.getMessage());
		}
		return detailList;
	}
	
	
public ArrayList<String> getServiceValueReportHeaderList( Date date,XSSFWorkbook  workbook) {
		
	ArrayList<String> detailList = new ArrayList<String>();
		
		detailList.add("Ser No.");
		detailList.add("$##");
		detailList.add("Branch");
		detailList.add("$##");//for merge row
		detailList.add("Completed Service Revenue");
		detailList.add("$#"); // for merge cell
		detailList.add("Cancelled Service Revenue");
		detailList.add("$#");
		detailList.add("Schedule Service Revenue");
		detailList.add("$#");
//		detailList.add("");
		int count = 2;
		int rowChart = 1;
		logger.log(Level.SEVERE ," pro key set-- "+ proName.keySet());
		
		
		
		
		
		return detailList;
	}
	
	@Override
	public void setServiceFumigatioList(ArrayList<CommodityFumigationDetails> qarray) {
		// TODO Auto-generated method stub
		this.commodityFumigation = qarray;
		
	}

	@Override
	public void setServiceValueList(ArrayList<CommodityFumigationDetails> qarray) {
		// TODO Auto-generated method stub
		this.commodityServiceValue = qarray;
		
	}

	public void getCostSheetReport(XSSFWorkbook  workbook){
		
		
		 /**
		  * @author Anil , Date : 16-08-2019
		  * Setting company logo to xlsx
		  */
		 Company company=ofy().load().type(Company.class).filter("companyId", cnc.getCompanyId()).first().now();
		 byte[] bytes=null;
		 int colNum=2;
		 if(company!=null&&company.getLogo()!=null&&company.getLogo().getUrl()!=null&&!company.getLogo().getUrl().equals("")){
			 bytes=getBolbDataToByteArray(company.getLogo());
			 if(bytes!=null){
				 colNum=1;
			 }
		 }
		
		
		/**
		 * @author Anil,Date : 29-01-2019
		 * Total's summary at bottom of its component
		 * For Orion raised By Sonu
		 */
		boolean totalSummaryAtBottomFlag=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "TotalSummaryAtBottom", cnc.getCompanyId())){
			totalSummaryAtBottomFlag=true;
		}
		
		try {
			int size = cnc.getSaffingDetailsList().size();
			if(workbook.getNumberOfSheets()>0){
				for(int i= 0 ;i<= workbook.getNumberOfSheets() ;i++){
					workbook.removeSheetAt(i);
			}
			}
			
			
			  XSSFSheet sheet = workbook.createSheet("Cost Sheet Report");
			  Row row = sheet.createRow(rowCount);
				int	columnCount = 0;
				Cell cell;
				
				XSSFCellStyle	style42 = workbook.createCellStyle();
				style42.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				style42.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			      
			     XSSFFont font33 = workbook.createFont();
			     font33.setFontHeightInPoints((short) 18);
//			     font.setFontName("Times New Roman");
			     font33.setFontName("Calibri");
			     font33.setBold(true);;
			      
			     style42.setFont(font33);
			     style42.setBorderTop(CellStyle.BORDER_THICK);
			     style42.setBorderBottom(CellStyle.BORDER_THICK);
			     style42.setBorderLeft(CellStyle.BORDER_THICK);
			     if(bytes!=null){
			    	 style42.setBorderRight(CellStyle.BORDER_NONE);
			     }else{
			    	 style42.setBorderRight(CellStyle.BORDER_THICK); 
			     }
			     String chStr = getChart(0);
			     String chStr1 = getChart(colNum + cnc.getSaffingDetailsList().size());
				 sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+1+":"+chStr1+""+1));
				 cell = row.createCell(columnCount);
				 
				 System.out.println("COUNT COL : "+columnCount);
				 /**
				  * Date : 17-12-2018 BY ANIL
				  * Cost sheet header should  be Project Name instead of client name
				  */
				 String costSheetHeader="";
				 if(cnc.getProjectName()!=null&&!cnc.getProjectName().equals("")){
					 costSheetHeader="Cost Sheet : "+ cnc.getProjectName();
				 }else{
					 costSheetHeader="Cost Sheet : "+ cnc.getPersonInfo().getFullName();
				 }
//				 cell.setCellValue((String) "Sasha Cost Sheet : "+ cnc.getPersonInfo().getFullName()+" Cost Sheet");
				 cell.setCellValue(costSheetHeader);
				 cell.setCellStyle(style42);
				
				 /**
				  * @author Anil , Date : 16-08-2019
				  * Setting company logo to xlsx
				  */
				 if(bytes!=null){
					 int rowCount1=0;
					 int colCount= 2 + cnc.getSaffingDetailsList().size();
					 XSSFCellStyle	style421 = workbook.createCellStyle();
					 style421.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					 style421.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					 style421.setFont(font33);
					 style421.setBorderTop(CellStyle.BORDER_THICK);
					 style421.setBorderBottom(CellStyle.BORDER_THICK);
					 style421.setBorderRight(CellStyle.BORDER_THICK);
					 style421.setBorderLeft(CellStyle.BORDER_NONE);
					 cell = row.createCell(colCount);
					 cell.setCellStyle(style421);
					 addCompanyLogoToCell(workbook, sheet, bytes,cell,rowCount1,colCount);
				 }
				 
				 /**
				  * 
				  */
				 
				 for(int j= 1;j<= (colNum+cnc.getSaffingDetailsList().size());j++){
						try{
							cell = row.createCell(j);
							cell.setCellValue("");
						}catch(Exception e){
							cell = row.getCell(j);
						}		
						cell.setCellStyle(style42);
				 }
				 /*********25-1-2019 added by amol for housekeeping equipment charges sheet*******/
				 
				 XSSFSheet sheet2= workbook.createSheet("Housekeeping Equipment Charges");
				 getHouseKeepingEquipmentCharges(sheet2,workbook,bytes);
//				 getCostSheetHeader(sheet,sheet.createRow(++rowCount),workbook);
				 getCostSheetHeader(sheet,sheet.createRow(rowCount),workbook);
				 getCostSheetVerticalHeader(sheet, rowCount, workbook,totalSummaryAtBottomFlag);
				 getCostSheetValues(sheet, rowCount,  workbook,totalSummaryAtBottomFlag);
			  

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
		logger.log(Level.SEVERE, " get error in cost sheet print --"+e.getMessage());
	}
		}
	
   


private void getHouseKeepingEquipmentCharges(XSSFSheet sheet2,XSSFWorkbook workbook, byte[] bytes) {
		try {
			int size = cnc.getSaffingDetailsList().size();
			int maxCol=10;
			if(bytes!=null){
				maxCol=9;
			}
	        Row row = sheet2.createRow(rowCount);
				int	columnCount = 0;
				Cell cell;
				
				 style2 = workbook.createCellStyle();
			     style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			     style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			      
			     XSSFFont font = workbook.createFont();
			     font.setFontHeightInPoints((short) 18);
//			     font.setFontName("Times New Roman");
			     font.setFontName("Calibri");
			     font.setBold(true);
			      
			     
			     style2.setBorderTop(CellStyle.BORDER_THICK);
			     style2.setBorderBottom(CellStyle.BORDER_THICK);
			     style2.setBorderLeft(CellStyle.BORDER_THICK);
			     style2.setBorderRight(CellStyle.BORDER_THICK);
			     if(bytes!=null){
			    	 style2.setBorderRight(CellStyle.BORDER_NONE);
			     }else{
			    	 style2.setBorderRight(CellStyle.BORDER_THICK); 
			     }
			     style2.setFont(font);
			     font.setBold(true);
			     String chStr = getChart(0);
			     String chStr1 = getChart(maxCol);
				 sheet2.addMergedRegion(CellRangeAddress.valueOf(chStr+""+1+":"+chStr1+""+1));
				 cell = row.createCell(columnCount);
			
				 String costSheetHeader="";
				 if(cnc.getProjectName()!=null&&!cnc.getBranch().equals("")){
					 costSheetHeader= cnc.getProjectName() + "  " +cnc.getBranch()+" -Housekeeping Equipment Charges";
				 }else{
					 costSheetHeader= cnc.getProjectName() +"  " +cnc.getBranch()+" -Housekeeping Equipment Charges";
				 }
//				
				 cell.setCellValue(costSheetHeader);
				 cell.setCellStyle(style2);
				 
				 /**
				  * @author Anil , Date : 16-08-2019
				  * Setting company logo to xlsx
				  */
				 if(bytes!=null){
					 int rowCount1=0;
					 int colCount= 10;
					 XSSFCellStyle	style421 = workbook.createCellStyle();
					 style421.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					 style421.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					 style421.setFont(font);
					 style421.setBorderTop(CellStyle.BORDER_THICK);
					 style421.setBorderBottom(CellStyle.BORDER_THICK);
					 style421.setBorderRight(CellStyle.BORDER_THICK);
					 style421.setBorderLeft(CellStyle.BORDER_NONE);
					 cell = row.createCell(colCount);
					 cell.setCellStyle(style421);
					 addCompanyLogoToCell(workbook, sheet2, bytes,cell,rowCount1,colCount);
				 }
				 
				 /**
				  * 
				  */
						
				   for(int j= 1;j<= maxCol;j++){
						try{
							cell = row.createCell(j);
							cell.setCellValue("");
						}catch(Exception e){
							cell = row.getCell(j);
						}		
						cell.setCellStyle(style2);
					}
				 
				 
//				 blankCell(sheet2,workbook);
				 getHouseKeepingEquipmentHeader(sheet2,sheet2.createRow(++rowCount),workbook);
				 getgetHouseKeepingEquipmentValues(sheet2, rowCount,  workbook);
				 footerTable(sheet2,workbook);
	     } catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
		logger.log(Level.SEVERE, " get error in cost sheet print --"+e.getMessage());
	}
		}


 private void footerTable(XSSFSheet sheet2, XSSFWorkbook workbook) {
	 int rowCount=cnc.getEquipmentRentalList().size()+6;
System.out.println("My rowcount :" + rowCount);
	try {
			
			int size = cnc.getEquipmentRentalList().size()+6;
	        Row row = sheet2.createRow(rowCount);
				int	columnCount = 0;
				Cell cell;
				
				 style2 = workbook.createCellStyle();
			     style2.setAlignment(XSSFCellStyle.ALIGN_LEFT);
			     style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
//			     XSSFCellStyle style = workbook.createCellStyle();
			     style2.setBorderTop(CellStyle.BORDER_THICK);
			     style2.setBorderBottom(CellStyle.BORDER_THICK);
			     style2.setBorderLeft(CellStyle.BORDER_THICK);
			     style2.setBorderRight(CellStyle.BORDER_THICK);
			     
			      
			     XSSFFont font = workbook.createFont();
			     font.setFontHeightInPoints((short) 18);
//			     font.setFontName("Times New Roman");
			     /**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
					
			     font.setFontName("Calibri");
			     font.setBold(true);
			      
			     style2.setFont(font);
			     
			     
			     XSSFFont font44 = workbook.createFont();
			     font44.setFontHeightInPoints((short) 18);
			     font44.setFontName("Calibri");
			     font44.setBold(true);
			     
			     XSSFCellStyle	 style11 = workbook.createCellStyle();
			     style11.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
			     style11.setBorderTop(CellStyle.BORDER_THICK);
			     style11.setBorderTop(CellStyle.BORDER_THICK);
			     style11.setBorderBottom(CellStyle.BORDER_THICK);
			     style11.setBorderLeft(CellStyle.BORDER_THICK);
			     style11.setBorderRight(CellStyle.BORDER_THICK);
			     
			     
			     
			     
                  style11.setFont(font44);
			     
                  
                 XSSFFont font45 = workbook.createFont();
 			     font45.setFontHeightInPoints((short) 18);
 			     font45.setFontName("Calibri");
 			     font45.setBold(true);
 			     
			     XSSFCellStyle	 style12 = workbook.createCellStyle();
			     style12.setBorderBottom(CellStyle.BORDER_THICK);
			     style12.setBorderTop(CellStyle.BORDER_THICK);
			     style12.setFont(font45);
			     
//			     XSSFFont font45 = workbook.createFont();
//			     font45.setFontHeightInPoints((short) 18);
//			     font45.setFontName("Calibri");
//			     font45.setBold(true);
//			     
			     
//			     style12.setFont(font45);
			     
			     
			     
			     String chStr = getChart(0);
			     String chStr1 = getChart(10);
				 sheet2.addMergedRegion(CellRangeAddress.valueOf(chStr+""+size+":"+chStr1+""+size));
				 /**
				  * 
				  */
				 cell = row.createCell(columnCount);
				 String amol="";
				 cell.setCellValue(amol);
				 cell.setCellStyle(style12);
				 /**
				  * 
				  */
				 int size2 =  size + 2;
				 
//			    row = sheet2.createRow(rowCount);
//				 sheet2.addMergedRegion(CellRangeAddress.valueOf(chStr+""+size2+":"+chStr1+""+size2));
				 sheet2.addMergedRegion(new CellRangeAddress(cnc.getEquipmentRentalList().size()+6,cnc.getEquipmentRentalList().size()+6,0,9));
				 System.out.println("equipmentcharges"+cnc.getEquipmentRentalList().size());
				
		//		 row = sheet2.createRow(rowCount++);
				 columnCount=0;
			     cell = row.createCell(columnCount);
				 String costSheetHeader = "";
				 costSheetHeader = "Total Housekeeping Equipment Charges";
				 cell.setCellValue(costSheetHeader);
				 cell.setCellStyle(style12);
				 
				 
				     for(int j= 1;j<= 9;j++){
						try{
							cell = row.createCell(j);
							cell.setCellValue("");
						}catch(Exception e){
							cell = row.getCell(j);
						}		
						cell.setCellStyle(style12);
					}

				 
				 cell = row.createCell(columnCount+10);
				 double costSheettotal ;
				 costSheettotal = hKcharges;
				 cell.setCellValue(costSheettotal);
				 cell.setCellStyle(style11);
				 
//				 row = sheet2.createRow(rowCount+1);
//				 cell=row.createCell(columnCount);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//				 
//				 cell=row.createCell(columnCount++);
//				 cell.setCellValue("");
//				 cell.setCellStyle(style11);
//						
			
	     } catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
		logger.log(Level.SEVERE, " get error in cost sheet print --"+e.getMessage());
	}
		}


private void getgetHouseKeepingEquipmentValues(XSSFSheet sheet2, int rowCount2,
		XSSFWorkbook workbook) {
//	ArrayList<String> list = getAllVerticalHeader();
	int columnCount = 0;
	Cell cell;	
	 XSSFCellStyle style1 = workbook.createCellStyle();
     style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
     style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
   
     XSSFCellStyle style = workbook.createCellStyle();
     style.setAlignment(XSSFCellStyle.ALIGN_LEFT);
     style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
   
     
//     font1.setFontName("Times New Roman");
    
    
//     style1.setBorderTop(CellStyle.BORDER_THIN);
//     style1.setBorderBottom(CellStyle.BORDER_THIN);
//     style1.setBorderLeft(CellStyle.BORDER_THIN);
//     style1.setBorderRight(CellStyle.BORDER_THIN);
     XSSFFont font22 = workbook.createFont();
     font22.setFontHeightInPoints((short) 12);
     font22.setFontName("Calibri");
     style1.setFont(font22);
     style.setFont(font22); 
    	 
    	 
     XSSFCellStyle style3 = workbook.createCellStyle();
     style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
     style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
//     CellStyle backgroundStyle = workbook.createCellStyle();
     style3.setBorderRight(CellStyle.BORDER_THICK);
     
	 XSSFFont font44 = workbook.createFont();
	 font44.setFontHeightInPoints((short) 12);
//	 font.setFontName("Times New Roman");
	 font44.setFontName("Calibri");
//	 font.setBold(true);
	 style3.setFont(font44); 
	 
//	 style3.setBorderTop(CellStyle.BORDER_THIN);
//     style3.setBorderBottom(CellStyle.BORDER_THIN);
//     style3.setBorderLeft(CellStyle.BORDER_THIN);
//     style3.setBorderRight(CellStyle.BORDER_THIN);
     
    
     int rowCount=2;
     Row row;
     int finalRowCont=0;
   
     int column = cnc.getEquipmentRentalList().size();
     System.out.println(cnc.getEquipmentRentalList().size()+"equipmentlist");
//     EquipmentRental equipmentrental1;
//    
     XSSFCellStyle	 style8 = workbook.createCellStyle();
      style8.setAlignment(XSSFCellStyle.ALIGN_CENTER);
//	     style8.setFont(font); 
     
      XSSFCellStyle	 style7 = workbook.createCellStyle();
       style7.setBorderLeft(CellStyle.BORDER_THICK);
	     style7.setAlignment(XSSFCellStyle.ALIGN_LEFT);
	     
	     
	     XSSFFont font77 = workbook.createFont();
	     font77.setFontHeightInPoints((short) 12);
//		 font.setFontName("Times New Roman");
	     font77.setFontName("Calibri");
	     style7.setFont(font77);
	     
//     font.setFontName("Times New Roman");
//     font.setBold(true);
//     style2.setFont(font); 
     double interestommachinery = 0;
     double maintanancepercentagepermonth=0;
     for(EquipmentRental equipmentrental:cnc.getEquipmentRentalList()){
    	   interestommachinery= equipmentrental.getMachineryInterest();
    	  maintanancepercentagepermonth=equipmentrental.getMaintanancePercentage();
     }
     
     interestommachinery= cnc.getInterestOnMachinary();
	  maintanancepercentagepermonth=cnc.getMaintainanceCharges();
     
     columnCount = 0;
     row = sheet2.createRow(rowCount++) ;    	
     
 	cell = row.createCell(columnCount);
	cell.setCellValue((String) "");
	cell.setCellStyle(style7);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)" " );
	cell.setCellStyle(style8);
	
 
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"");
	cell.setCellStyle(style8);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue("");
	cell.setCellStyle(style8);
   
	
	cell = row.createCell(++columnCount);
	cell.setCellValue("" );
	cell.setCellStyle(style8);
	
	 
	cell = row.createCell(++columnCount);
	cell.setCellValue("");
	cell.setCellStyle(style8);
	
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String) "");
	cell.setCellStyle(style8);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue( "");
	cell.setCellStyle(style8);
	
	
	 
	cell = row.createCell(++columnCount);
	cell.setCellValue(interestommachinery+"%");
	cell.setCellStyle(style8);

	 
	
    cell = row.createCell(++columnCount);
	cell.setCellValue(maintanancepercentagepermonth+"%");
	cell.setCellStyle(style8);

	 
	cell = row.createCell(++columnCount);
	cell.setCellValue("");
	cell.setCellStyle(style3);
	
 
     
     for(EquipmentRental equipmentrental:cnc.getEquipmentRentalList()){
    	
    		 columnCount = 0;
    		  XSSFCellStyle	 style5 = workbook.createCellStyle();
    		     style5.setAlignment(XSSFCellStyle.ALIGN_LEFT);
    		     style5.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
//    		     font.setFontName("Times New Roman");
//    		     font.setBold(true);
//    		     XSSFCellStyle	 style7 = workbook.createCellStyle();
    		     style7.setBorderLeft(CellStyle.BORDER_THICK);
    		     style7.setAlignment(XSSFCellStyle.ALIGN_LEFT);
//    		     style7.setFont(font); 
//    		     style5.setFont(font); 
    		     XSSFCellStyle	 style9 = workbook.createCellStyle();
    		     style9.setAlignment(XSSFCellStyle.ALIGN_LEFT);
//    		     style9.setFont(font); 
    		  double totalAmount=0;
    		  double  totalCost=0;
    			 Qty+=equipmentrental.getQty();
    			 totalcost+=totalCost;
    			 rentalon+=Double.parseDouble(equipmentrental.getRentalEquipmentPerMonth());
    			 interstonmachinery+=Double.parseDouble(equipmentrental.getInterestOnMachineryPerMonth());
    			maintanince+=Double.parseDouble(equipmentrental.getMaintananceAndInsurancePerMonth());
    		
			 row = sheet2.createRow(rowCount++) ;    	
			    System.out.println("Rowcount"+rowCount);
    		cell = row.createCell(columnCount);
    		System.out.println(columnCount+"columncount");
    		
  
    		
    		/**Date 21-8-2019 by Amol**/
    		String machineName=equipmentrental.getMachine();
//    		  int length=machineName.length();
//    		  for(int i=length-1;i>=0;i--){
//    			 rev=rev+machineName.charAt(i);
//    			 if(rev.equals("/")){
//    				 
//    			 }
//    		  }
    		String updatedName="";
    		String str[]=machineName.split("/");
    		for(int i=0;i<str.length-1;i++){
    			if(i==0){
    				updatedName=updatedName+str[i];
    			}else{
    				updatedName=updatedName+"/"+str[i];
    			}
    			
    		}
    		  
    		
    		cell.setCellValue(updatedName);
    		cell.setCellStyle(style7);
    		style9.setAlignment(XSSFCellStyle.ALIGN_LEFT);
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue((String)equipmentrental.getBrand() );
    		cell.setCellStyle(style);
//    		style1.setAlignment(XSSFCellStyle.ALIGN_LEFT);
    	 
    	
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue((String)equipmentrental.getModelNumber());
    		cell.setCellStyle(style1);
    		
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue(equipmentrental.getMarketPrice()+"");
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    		 
    		cell = row.createCell(++columnCount);
    		cell.setCellValue(df1.format(equipmentrental.getQty())+"" );
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    	
    		totalCost=(equipmentrental.getMarketPrice()*equipmentrental.getQty());
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue(df1.format(totalCost)+"");
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue((String) equipmentrental.getNoOFMonths());
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue((String) equipmentrental.getRentalEquipmentPerMonth());
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue((String)equipmentrental.getInterestOnMachineryPerMonth() );
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    		
    		cell = row.createCell(++columnCount);
    		cell.setCellValue((String)equipmentrental.getMaintananceAndInsurancePerMonth() );
    		cell.setCellStyle(style1);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);

    		 
    	String	epmonth=equipmentrental.getRentalEquipmentPerMonth();
    	String    mpermonth=equipmentrental.getInterestOnMachineryPerMonth();
    	String   inpermonth=equipmentrental.getMaintananceAndInsurancePerMonth();
    		
    	   
    	   double permonth=Double.parseDouble(epmonth);
    		double mPpermonth=Double.parseDouble(mpermonth);
    		double iNpermonth=Double.parseDouble(inpermonth);
    		
    		
    		totalAmount=permonth+mPpermonth+iNpermonth;
    		 
    		cell = row.createCell(++columnCount);
    		cell.setCellValue(df1.format(totalAmount)+"");
    		cell.setCellStyle(style3);
    		style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
    		 
    		 hKcharges+=permonth+mPpermonth+iNpermonth;;
    		 totaltotalcost+= (equipmentrental.getMarketPrice()*equipmentrental.getQty());
    		 
    	   		 
    		 
    		 
    		 
    	  		 
    		 
}
     /**
      * 4-3-2019
      */
     XSSFCellStyle	 style2 = workbook.createCellStyle();
//     font = workbook.createFont();
//     style2.setFont(font); 
     row = sheet2.createRow(rowCount++) ;  
     System.out.println("Rowcount1"+rowCount);
     columnCount=0;
 	cell = row.createCell(columnCount);
	cell.setCellValue((String) "");
	cell.setCellStyle(style7);
     
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style2);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style3);
     
		    
     XSSFCellStyle	 style4 = workbook.createCellStyle();
     style4.setAlignment(XSSFCellStyle.ALIGN_LEFT);
     style4.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
      style4.setBorderBottom(CellStyle.BORDER_THICK);
      XSSFFont  font = workbook.createFont();
      font.setFontName("Calibri");
      font.setBold(true);
      style4.setFont(font); 
      
       XSSFCellStyle style9 = workbook.createCellStyle();
       style9.setBorderLeft(CellStyle.BORDER_THICK);
       style9.setBorderBottom(CellStyle.BORDER_THICK);
       style9.setAlignment(XSSFCellStyle.ALIGN_LEFT);
       style9.setFont(font); 
     
       XSSFCellStyle style11 = workbook.createCellStyle();
       style11.setBorderRight(CellStyle.BORDER_THICK);
       style11.setBorderBottom(CellStyle.BORDER_THICK);
       style11.setAlignment(XSSFCellStyle.ALIGN_LEFT);
       style11.setFont(font); 
     
 	columnCount = 0;
     row = sheet2.createRow(rowCount++) ;    	
     System.out.println("Rowcount2"+rowCount);
 	cell = row.createCell(columnCount);
	cell.setCellValue((String) "");
	cell.setCellStyle(style9);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"" );
	cell.setCellStyle(style4);
	
 
	cell = row.createCell(++columnCount);
	cell.setCellValue((String)"");
	cell.setCellStyle(style4);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue("Total");
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue(df1.format(Qty)+"" );
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	 
	cell = row.createCell(++columnCount);
	cell.setCellValue(df1.format(totaltotalcost)+"");
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue((String) "");
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	cell = row.createCell(++columnCount);
	cell.setCellValue(df1.format(rentalon)+ "");
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	 
	cell = row.createCell(++columnCount);
	cell.setCellValue(df1.format(interstonmachinery)+"" );
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	
	cell = row.createCell(++columnCount);
	cell.setCellValue(df1.format(maintanince)+"");
	cell.setCellStyle(style4);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	 
	cell = row.createCell(++columnCount);
	cell.setCellValue(df1.format(hKcharges)+"");
	cell.setCellStyle(style11);
//	cell.setCellSt										yle(backgroundStyle);
	style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
     
	 System.out.println(rowCount+"Rowcount");
     
//	

//	 footerTable(sheet2,workbook);
        	 
}


private void getHouseKeepingEquipmentHeader(XSSFSheet sheet2,
		Row row, XSSFWorkbook workbook) {

 	int columnCount = 0;
 	Cell cell;
 	 CellStyle backgroundStyle = workbook.createCellStyle();
      backgroundStyle.setBorderRight(CellStyle.BORDER_THICK);
      XSSFCellStyle style44 = workbook.createCellStyle();
 	style44.setAlignment(XSSFCellStyle.ALIGN_CENTER);
 	style44.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

 	XSSFFont font5 = workbook.createFont();
 	font5.setFontHeightInPoints((short) 12);
// 	font.setFontName("Times New Roman");
 	/**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
	
 	font5.setFontName("Calibri");
 	font5.setBold(true);
 	style44.setFont(font5);  
 	
 	style44.setBorderTop(CellStyle.BORDER_THICK);
 	style44.setBorderBottom(CellStyle.BORDER_THIN);
 	style44.setBorderLeft(CellStyle.BORDER_THIN);
 	style44.setBorderRight(CellStyle.BORDER_THIN);
     
//     XSSFFont font2 = workbook.createFont();
//     font2.setFontHeightInPoints((short) 12);
     
 	XSSFCellStyle style27 = workbook.createCellStyle();
 	style27.setBorderTop(CellStyle.BORDER_THICK);
 	style27.setBorderBottom(CellStyle.BORDER_THIN);
 	style27.setBorderLeft(CellStyle.BORDER_THIN);
 	style27.setBorderRight(CellStyle.BORDER_THICK);
 	style27.setAlignment(XSSFCellStyle.ALIGN_CENTER);
 	style27.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
 	XSSFFont	font2 =workbook.createFont();
 	font2.setFontHeightInPoints((short) 12);
// 	font.setFontName("Times New Roman");
 	/**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
	
 	font2.setFontName("Calibri");
 	font2.setBold(true);
 	style27.setFont(font2);  
 	
 	XSSFCellStyle style28 = workbook.createCellStyle();
 	style28.setBorderTop(CellStyle.BORDER_THICK);
 	style28.setBorderBottom(CellStyle.BORDER_THIN);
 	style28.setBorderLeft(CellStyle.BORDER_THICK);
 	style28.setBorderRight(CellStyle.BORDER_THIN);
 	
 	style28.setAlignment(XSSFCellStyle.ALIGN_CENTER);
 	style28.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
 	XSSFFont font88 = workbook.createFont();
 	font88.setFontHeightInPoints((short) 12);
// 	font.setFontName("Times New Roman");
 	/**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
 	font88.setFontName("Calibri");
 	font88.setBold(true);
 	style28.setFont(font88);  
 	

 	cell = row.createCell(columnCount);
 	cell.setCellValue((String) "Equipment");
 	cell.setCellStyle(style28);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Brand");
 	cell.setCellStyle(style44);

 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Model No.");
 	cell.setCellStyle(style44);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Per Unit Cost");
 	cell.setCellStyle(style44);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Qty");
 	cell.setCellStyle(style44);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Total Cost");
 	cell.setCellStyle(style44);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) " ");
 	cell.setCellStyle(style44);
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Rental on Equipment Per Month ");
 	cell.setCellStyle(style44);
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Interest on Equipment Per Month");
 	cell.setCellStyle(style44);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "Maintenance & Insurance Per Month");
 	cell.setCellStyle(style44);
 	
 	cell = row.createCell(++columnCount);
 	cell.setCellValue((String) "HK Equipment Charges");
 	cell.setCellStyle(style27);
 	
 	
// 	cell.setCellStyle(backgroundStyle);
 	
// 	 style2 = workbook.createCellStyle();
//      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
//      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
//       
 // 	
 	
 	
 }


private void getCostSheetHeader( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {

		int columnCount = 0;
		Cell cell;

		style2 = workbook.createCellStyle();
		style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

		XSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
//		font.setFontName("Times New Roman");
		/**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
		
		font.setFontName("Calibri");
		font.setBold(true);
		style2.setFont(font);  
		
		style2.setBorderTop(CellStyle.BORDER_THICK);
	    style2.setBorderBottom(CellStyle.BORDER_THICK);
	    style2.setBorderLeft(CellStyle.BORDER_THICK);
	    style2.setBorderRight(CellStyle.BORDER_THICK);
	
		cell = row.createCell(columnCount);
		cell.setCellValue((String) "Sr No.");
		cell.setCellStyle(style2);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Description");
		cell.setCellStyle(style2);
		
		for(StaffingDetails staff : cnc.getSaffingDetailsList()){
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) staff.getDesignation());
			cell.setCellStyle(style2);
		}
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Total");
		cell.setCellStyle(style2);
		
	}
private void getCostSheetVerticalHeader( XSSFSheet sheet, int rowCount,XSSFWorkbook  workbook, boolean totalSummaryAtBottomFlag){
	ArrayList<String> list = getAllVerticalHeader(totalSummaryAtBottomFlag);
	int columnCount = 1;
	Cell cell;	
	 style2 = workbook.createCellStyle();
     style2.setAlignment(XSSFCellStyle.ALIGN_LEFT);
     style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
    
     XSSFFont font = workbook.createFont();
     font.setFontHeightInPoints((short) 12);
//     font.setFontName("Times New Roman");
     /**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
		
	font.setFontName("Calibri");
     font.setBold(true);;
     
     style2.setFont(font);
     style2.setBorderTop(CellStyle.BORDER_THICK);
     style2.setBorderBottom(CellStyle.BORDER_THICK);
     style2.setBorderLeft(CellStyle.BORDER_THICK);
     style2.setBorderRight(CellStyle.BORDER_THICK);
     
     XSSFCellStyle style1 = workbook.createCellStyle();
     style1.setAlignment(XSSFCellStyle.ALIGN_LEFT);
     style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
     XSSFFont font1 = workbook.createFont();
     font1.setFontHeightInPoints((short) 12);
//     font1.setFontName("Times New Roman");
     /**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
		
     font1.setFontName("Calibri");
     style1.setFont(font1);
     
     style1.setBorderTop(CellStyle.BORDER_THIN);
     style1.setBorderBottom(CellStyle.BORDER_THIN);
     style1.setBorderLeft(CellStyle.BORDER_THICK);
     style1.setBorderRight(CellStyle.BORDER_THICK);
   // style1.setWrapText(true);
     Row row;
     XSSFCellStyle style4 = workbook.createCellStyle();
     style4.setFont(font);
	for(String s : list){
		row = sheet.createRow(++rowCount);
		if(s.contains("//")){
			String array[] = s.split("//");
			cell = row.createCell(columnCount);
			cell.setCellValue((String) array[1]);
			cell.setCellStyle(style2);
			
			cell = row.createCell(columnCount-1);
			cell.setCellValue((String) array[0]);
			cell.setCellStyle(style2);

		}else if(s.contains("#")){
			String array[] = s.split("#");
			cell = row.createCell(columnCount);
			cell.setCellValue((String) array[1]);
			cell.setCellStyle(style1);
			
			cell = row.createCell(columnCount-1);
			cell.setCellValue((String) array[0]);
			 XSSFCellStyle style3 = workbook.createCellStyle();
		     style3.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		     style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		     style3.setFont(font);
		     style3.setBorderTop(CellStyle.BORDER_THIN);
		     style3.setBorderBottom(CellStyle.BORDER_THIN);
		     style3.setBorderLeft(CellStyle.BORDER_THICK);
		     style3.setBorderRight(CellStyle.BORDER_THICK);
			if(array[0].contains("c")){
				cell.setCellStyle(style1);
			}else{
				cell.setCellStyle(style3);
			}

		}else if(s.contains("/")){
			cell = row.createCell(columnCount);
			cell.setCellValue((String) s.substring(1, s.length()));
			cell.setCellStyle(style2);
			
			cell = row.createCell(columnCount-1);
			cell.setCellValue("");
			cell.setCellStyle(style2);
		}else{
			cell = row.createCell(columnCount);
			cell.setCellValue((String) s);
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount-1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
		}
	}
	XSSFCellStyle style3 = workbook.createCellStyle();
    style3.setAlignment(XSSFCellStyle.ALIGN_LEFT);
   // style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
    XSSFFont font3 = workbook.createFont();
    font3.setFontHeightInPoints((short) 12);
//    font3.setFontName("Times New Roman");
    font3.setFontName("Calibri");
    style3.setFont(font3);

    style3.setWrapText(true);
    style3.setBorderTop(CellStyle.BORDER_THIN);
    style3.setBorderBottom(CellStyle.BORDER_THIN);
    style3.setBorderLeft(CellStyle.BORDER_THIN);
    style3.setBorderRight(CellStyle.BORDER_THIN);
	String value = cnc.getTermsAndConditions().trim();
	String [] list1 = value.split("\n");
	for(String s : list1){
		if(s.length() >=3){
		row = sheet.createRow(++rowCount);
			cell = row.createCell(columnCount);
			
			String str = s.substring(2 , s.length());
			
			if(str.contains("&Totalcostinword")){
				str = str.replace("&Totalcostinword", PdfUpdated.convert(cnc.getNetPayableOFCNC())+"");
			}
			if(str.contains("&Totalcost")){
				str = str.replace("&Totalcost", roundOff(cnc.getNetPayableOFCNC()));
			}
			cell.setCellValue((String) str);
			
			cell.setCellStyle(style3);
	
			String chStr = getChart(1);
		    String chStr1 = getChart(2 + cnc.getSaffingDetailsList().size());
			sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+(1+rowCount)+":"+chStr1+""+(1+rowCount)));
			cell = row.createCell(columnCount-1);
			cell.setCellValue((String) "*");
			cell.setCellStyle(style3);
			
			for(int j= 2;j<= (2+cnc.getSaffingDetailsList().size());j++){
				try{
					cell = row.createCell(j);
					cell.setCellValue("");
				}catch(Exception e){
					cell = row.getCell(j);
				}		
				cell.setCellStyle(style3);
			}

			}
		}
}


private void getCostSheetValues( XSSFSheet sheet, int rowc,XSSFWorkbook  workbook, boolean totalSummaryAtBottomFlag){
//	ArrayList<String> list = getAllVerticalHeader();
	int columnCount = 2;
	Cell cell;	
	XSSFCellStyle style1 = workbook.createCellStyle();
     style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
     style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
     XSSFFont font1 = workbook.createFont();
     font1.setFontHeightInPoints((short) 12);
//     font1.setFontName("Times New Roman");
     font1.setFontName("Calibri");
     
     style1.setFont(font1);
    // style1.setWrapText(true);
     style1.setBorderTop(CellStyle.BORDER_THIN);
     style1.setBorderBottom(CellStyle.BORDER_THIN);
     style1.setBorderLeft(CellStyle.BORDER_THICK);
     style1.setBorderRight(CellStyle.BORDER_THICK);
     
     XSSFCellStyle style3 = workbook.createCellStyle();
     style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
     style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

	 XSSFFont font = workbook.createFont();
	 font.setFontHeightInPoints((short) 12);
//	 font.setFontName("Times New Roman");
	 font.setFontName("Calibri");
	 font.setBold(true);
	 style3.setFont(font); 
	 
	 style3.setBorderTop(CellStyle.BORDER_THICK);
     style3.setBorderBottom(CellStyle.BORDER_THICK);
     style3.setBorderLeft(CellStyle.BORDER_THICK);
     style3.setBorderRight(CellStyle.BORDER_THICK);
	 
     Row row;
     int finalRowCont=0;
     int totalEmployee = 0;
     int column = 2+cnc.getSaffingDetailsList().size();
     for(StaffingDetails staff : cnc.getSaffingDetailsList()){
    	 	int rowCount = rowc;
    	 	/**
    	 	 * @author Anil,Date : 29-01-2019
    	 	 */
    	 	if(!totalSummaryAtBottomFlag){
	    	 	row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOff(staff.getGrossSalary()));
				cell.setCellStyle(style3);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
    	 	}
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getBasic()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue (Double.parseDouble(staff.getDa())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getHra())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getConveyance())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			/** date 21.1.2019 added by komal to print B & P in excel sheet**/
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getBandP())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getMedical())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
				/***Date 20-2-2019 added by amol for adding CCA option****/
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CncEarningColumn",cnc.getCompanyId())){
				hideColumn=true;
			}
			
			if(hideColumn){
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue((String) (staff.getCityCompensatoryAllowance()+""));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			}
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getWashingAllowance()));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getOthers())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			if(hideColumn){
//			row = sheet.getRow(++rowCount);
//			cell = row.createCell(columnCount);
//			cell.setCellValue(roundOffValue(staff.getLeaveSalary()));
//			cell.setCellStyle(style1);	
//			cell = row.createCell(columnCount+1);
//			cell.setCellValue("");
//			cell.setCellStyle(style1);
			
			
//			row = sheet.getRow(++rowCount);
//			cell = row.createCell(columnCount);
//			cell.setCellValue(roundOff(staff.getGrossSalary()+staff.getLeaveSalary()));
//			cell.setCellStyle(style3);
//			cell = row.createCell(columnCount+1);
//			cell.setCellValue("");
//			cell.setCellStyle(style1);
			}
			
			/**
    	 	 * @author Anil,Date : 29-01-2019
    	 	 */
			if(totalSummaryAtBottomFlag){
	    	 	row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOff(staff.getGrossSalary()));
				cell.setCellStyle(style3);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
    	 	}
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			/**
    	 	 * @author Anil,Date : 29-01-2019
    	 	 */
			if(!totalSummaryAtBottomFlag){
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOff(Double.parseDouble(staff.getSatutoryDeductions())));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			}
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSdPF())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSdESIC())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue (Double.parseDouble(staff.getSdPT())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSdLWF())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			/**Date 6-12-2019 by AMol added this fields**/
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getPlEsic()));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getBonusEsic()));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			
			
			
			
			
			
			/**
    	 	 * @author Anil,Date : 29-01-2019
    	 	 */
			if(totalSummaryAtBottomFlag){
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOff(Double.parseDouble(staff.getSatutoryDeductions())));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			}
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
//			++rowCount;

			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(staff.getNetSalary()));
			cell.setCellStyle(style3);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getcLeave())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			/** date 21.01.2019 added by komal to add c bonus in cost sheet **/
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getcBonus())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getTravellingAllowance()));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getTakeHome())));
			cell.setCellStyle(style3);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
		//	++rowCount;
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			/**
    	 	 * @author Anil,Date : 29-01-2019
    	 	 */
			if(!totalSummaryAtBottomFlag){
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOff(Double.parseDouble(staff.getSatutoryPayments())));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			}
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpPF())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpESIC())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpEDLI())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpMWF())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpBonus())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
		//	++rowCount;
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getPaidLeaves())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			if(hideColumn){
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(((staff.getGratuity())));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
			
			
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOffValue((staff.getAdministrativecost())));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOffValue((staff.getSupervision())));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOffValue((staff.getTrainingCharges())));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
			    }
			/**DAte 6-12-2019 by Amol**/
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue((staff.getcPlEsic())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue((staff.getcBonusEsic())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			
			
			
			
			/**
    	 	 * @author Anil,Date : 29-01-2019
    	 	 */
			if(totalSummaryAtBottomFlag){
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOff(Double.parseDouble(staff.getSatutoryPayments())));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			}
			
	//		++rowCount;
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getTotal())));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getEmployementOverHeadCost())));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getTravellingAllowanceOverhead()));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getAdditionalAllowance()));
			cell.setCellStyle(style1);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			if(hideColumn){
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(roundOffValue(staff.getUniformCost()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
			}
			
			
			
			
			
			
			
		//	++rowCount;
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getTotalOfPaidAndOverhead())));
			cell.setCellStyle(style3);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
//			row = sheet.getRow(++rowCount);
//			cell = row.createCell(columnCount);
//			cell.setCellValue((String) (staff.getManagementFees()+""));
//			cell.setCellStyle(style1);
//			if(columnCount == column-1){
//				cell = row.createCell(column);
//				cell.setCellValue((String) (cnc.getManagementFees()+""));
//				cell.setCellStyle(style1);
//
//			}
			double value1=0 , value2=0;
			if(staff.getTotalOfPaidAndOverhead()!=null){
				value1 = Double.parseDouble(staff.getTotalOfPaidAndOverhead());
			}
			if(staff.getManagementFees()!=null){
				try{
					value2 = Math.round((Double.parseDouble(staff.getManagementFees()) / staff.getNoOfStaff())*100.0)/100.0;
				}catch(Exception e){
					value2 = 0;
				}
			}
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(value2));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

		//	double value = Math.round((value1+ value2)*100.0)/100.0 ;///
			/** date 23.1.19 changed by komal **/
			double value = Math.round((value1+ value2)) ;
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(value));
			cell.setCellStyle(style3);	
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
					
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(staff.getNoOfStaff()));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount+1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			totalEmployee = totalEmployee + staff.getNoOfStaff();
			if(columnCount == column-1){
				cell = row.createCell(column);
				cell.setCellValue(roundOff(totalEmployee));
				cell.setCellStyle(style3);

			}

            double totalIncludingManagementFees=(Double.parseDouble(staff.getTotalIncludingManagementFees()));
            
            totalValueIncludingManagementFees+=totalIncludingManagementFees;
            
			row = sheet.getRow(++rowCount);
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getTotalIncludingManagementFees())));
			cell.setCellStyle(style3);	
			cell = row.createCell(columnCount+1);
			/**Date 31-7-2019  by Amol**/
			cell.setCellValue(totalValueIncludingManagementFees);
			cell.setCellStyle(style3);
			
//			if(columnCount == column-1){
//				cell = row.createCell(column);
//				cell.setCellValue((String) (cnc.getNetPayableOFStaffing()+""));
//				cell.setCellStyle(style1);
//
//			}
			

		finalRowCont = rowCount;
	    columnCount++; 
     }
 	row = sheet.getRow(++finalRowCont);
 	String chStr = getChart(2);
    String chStr1 = getChart(2+cnc.getSaffingDetailsList().size()-1);
	sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+(finalRowCont+1)+":"+chStr1+""+(finalRowCont+1)));
	System.out.println("val" + chStr+""+finalRowCont+":"+chStr1+""+finalRowCont);
	cell = row.createCell(column-1);
	cell.setCellValue((String) (""));//cnc.getNetPayableOFConsumables()+
	cell.setCellStyle(style1);
	cell = row.createCell(column);
	
	/** date 23/8/2018 added by komal**/
	if(cnc.getNetPayableOFConsumables()!=0){
		cell.setCellValue(roundOff(cnc.getNetPayableOFConsumables()));
	}else{
		cell.setCellValue("At Actuals");
	}
	cell.setCellStyle(style1);

	
	row = sheet.getRow(++finalRowCont);
 	String chStr2 = getChart(2);
    String chStr3 = getChart(2+cnc.getSaffingDetailsList().size()-1);
	sheet.addMergedRegion(CellRangeAddress.valueOf(chStr2+""+(finalRowCont+1)+":"+chStr3+""+(finalRowCont+1)));
	cell = row.createCell(column-1);
	cell.setCellValue((String) (""));//cnc.getNetPayableOFEquipment()+
	cell.setCellStyle(style1);
	cell = row.createCell(column);
	cell.setCellValue(roundOff(cnc.getNetPayableOFEquipment()));
	cell.setCellStyle(style1);
	
	for(int j= 2;j< (2+cnc.getSaffingDetailsList().size());j++){
		try{
			cell = row.createCell(j);
			cell.setCellValue("");
		}catch(Exception e){
			cell = row.getCell(j);
		}		
		cell.setCellStyle(style1);
	}

//	/** date 25.8.2018 added by komal **/
//	row = sheet.getRow(++finalRowCont);
// 	String chStr6 = getChart(2);
//    String chStr7 = getChart(2+cnc.getSaffingDetailsList().size()-1);
//	sheet.addMergedRegion(CellRangeAddress.valueOf(chStr6+""+(finalRowCont+1)+":"+chStr7+""+(finalRowCont+1)));
//	cell = row.createCell(column-1);
//	cell.setCellValue((String) (""));//cnc.getNetPayableOFEquipment()+
//	cell.setCellStyle(style1);
//	cell = row.createCell(column);
//	cell.setCellValue((String) (cnc.getManagementFeesValue()+""));
//	cell.setCellStyle(style1);

	if(cnc.getOtherServicesList().size()>0){
		int i =0;
		String s[] = new String[cnc.getOtherServicesList().size()*2];
		for(OtherServices os : cnc.getOtherServicesList()){
			row = sheet.getRow(++finalRowCont);
			s[i] = getChart(2);
		    s[i+1] = getChart(2+cnc.getSaffingDetailsList().size()-1);
			sheet.addMergedRegion(CellRangeAddress.valueOf(s[i]+""+(finalRowCont+1)+":"+s[i+1]+""+(finalRowCont+1)));
			cell = row.createCell(column-1);
			cell.setCellValue((String) (""));//cnc.getNetPayableOFEquipment()+
			cell.setCellStyle(style1);
			cell = row.createCell(column);
			cell.setCellValue(roundOffValue(os.getChargableRates()));
			cell.setCellStyle(style1);
			for(int j= 2;j< (2+cnc.getSaffingDetailsList().size());j++){
				try{
					cell = row.createCell(j);
					cell.setCellValue("");
				}catch(Exception e){
					cell = row.getCell(j);
				}		
				cell.setCellStyle(style1);
			}
			i+= 2;
		}
	}
	row = sheet.getRow(++finalRowCont);
 	String chStr4 = getChart(2);
    String chStr5 = getChart(2+cnc.getSaffingDetailsList().size()-1);
	sheet.addMergedRegion(CellRangeAddress.valueOf(chStr4+""+(finalRowCont+1)+":"+chStr5+""+(finalRowCont+1)));
	cell = row.createCell(column-1);
	cell.setCellValue((String) (""));//cnc.getNetPayableOFCNC()+
	cell.setCellStyle(style1);
	cell = row.createCell(column);
	cell.setCellValue((String) (roundOff(cnc.getNetPayableOFCNC())));
	cell.setCellStyle(style3);
	for(int j= 2;j< (2+cnc.getSaffingDetailsList().size());j++){
		try{
			cell = row.createCell(j);
			cell.setCellValue("");
		}catch(Exception e){
			cell = row.getCell(j);
		}		
		cell.setCellStyle(style3);
	}
	
	
	if(hideColumn){
		columnCount=2;
 for(StaffingDetails staff : cnc.getSaffingDetailsList()){
	 
	 XSSFCellStyle style5 = workbook.createCellStyle();
	 style5.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	 style5.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
     XSSFFont font2 = workbook.createFont();
     font2.setFontHeightInPoints((short) 12);
     font2.setFontName("Times New Roman");
     style5.setFont(font1);
    // style1.setWrapText(true);
     style5.setBorderTop(CellStyle.BORDER_THICK);
     style5.setBorderBottom(CellStyle.BORDER_THICK);
     style5.setBorderLeft(CellStyle.BORDER_THICK);
     style5.setBorderRight(CellStyle.BORDER_THICK);
	 int rowCount = finalRowCont;
     
	row = sheet.getRow(++finalRowCont);
	cell = row.createCell(columnCount);
	cell.setCellValue(staff.getPayableOTRates());
	cell.setCellStyle(style5);	
	logger.log(Level.SEVERE,"OTPAYABLE RATES"+staff.getPayableOTRates() +" "+finalRowCont);
	cell = row.createCell(columnCount+1);
	cell.setCellValue("");
	cell.setCellStyle(style5);
	
	row = sheet.getRow(++finalRowCont);
	cell = row.createCell(columnCount);
	cell.setCellValue(getEsicRoundOff(staff.getEsicCost()));
	cell.setCellStyle(style5);	
	logger.log(Level.SEVERE,"ESIC COST"+staff.getEsicCost()+" "+finalRowCont);
	cell = row.createCell(columnCount+1);
	cell.setCellValue("");
	cell.setCellStyle(style5);
	
	row = sheet.getRow(++finalRowCont);
	cell = row.createCell(columnCount);
	cell.setCellValue(getEsicRoundOff(staff.getChargableOTRates()));
	cell.setCellStyle(style5);	
	logger.log(Level.SEVERE,"CHARGABLE OT"+staff.getChargableOTRates()+" "+finalRowCont);
	cell = row.createCell(columnCount+1);
	cell.setCellValue("");
	cell.setCellStyle(style5);
	
	columnCount++;
	finalRowCont = rowCount;
   }
	}
	

	}

 public double getEsicRoundOff(double amount){
	DecimalFormat df = new DecimalFormat("0");
	double esicAmount=0;
	esicAmount=Double.parseDouble(df.format(amount));
	
	if(esicAmount<amount){
		esicAmount++;
	}
	return esicAmount;
  }


private ArrayList<String> getAllVerticalHeader(boolean totalSummaryAtBottomFlag){
	/** DATE 12.10.2018 added by komal**/
	boolean flag = false;
	Branch branch = ofy().load().type(Branch.class).filter("companyId" , cnc.getCompanyId()).filter("buisnessUnitName", cnc.getBranch()).first().now();
	if(branch != null){
		if(branch.getAddress().getState() !=null && !branch.getAddress().getState().equals("")){
			if(branch.getAddress().getState().equalsIgnoreCase("Maharashtra")){
				flag = true;
			}
		}
	}
	ArrayList<String> list= new ArrayList<String>();
	/**
	 * @author Anil,Date : 29-01-2019
	 * when flag is false-total summary should come at top else it comes in bottom
	 */
	if(!totalSummaryAtBottomFlag){
		list.add("A//Monthly Payments");
	}
	
	list.add("Basic");
	list.add("D.A.");
	list.add("House Rent Allowance");
	list.add("Conveyance");
	/** date 21.1.2019 added by komal to print B & P in excel sheet**/
	list.add("B&P");
	list.add("Medical");
	/***Date 20-2-2019 added by amol for adding CCA option****/
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "CncEarningColumn",cnc.getCompanyId())){
		hideColumn=true;
	}
	if(hideColumn){
	list.add("City Compensatory Allowance (CCA)");
	}
	list.add("Washing Allowances");
	list.add("Other Allowances");
//	if(hideColumn){
//	list.add("Leave Salary");
//	}
	if(totalSummaryAtBottomFlag){
		list.add("A//Monthly Payments");
	}
	list.add("");
	
	if(!totalSummaryAtBottomFlag){
		list.add("/Statutory Deductions");
	}
	list.add("PF");
	list.add("ESIC");
	list.add("PT");
	/** DATE 12.10.2018 added by komal**/
	if(flag){
		list.add("MWF");
	}else{
		list.add("LWF");
	}
	/**Date 6-12-2019 by Amol**/
	list.add("PL Esic");
	list.add("Bonus Esic");
//	list.add("Other Allowances");
	if(totalSummaryAtBottomFlag){
		list.add("/Statutory Deductions");
	}
	
	list.add("");
	list.add("/Net Salary");
	list.add("C Leaves");
	/** date 21.01.2019 added by komal to add c bonus in cost sheet **/
	list.add("C Bonus");
	list.add("Travelling Allowance");
	list.add("/Take Home");
	list.add("");
	
	if(!totalSummaryAtBottomFlag){
		list.add("B//Statutory Payments");
	}
	
	list.add("PF");
	list.add("ESIC");
	list.add("EDLI");
	
	/** DATE 12.10.2018 added by komal**/
	if(flag){
		list.add("CMWF");
	}else{
		list.add("CLWF");
	}
	list.add("Bonus");
	list.add("P Leave");
	if(hideColumn){
		list.add("Gratuity");
		
		list.add("Administrative Cost");
		list.add("Supervision Charges");
		list.add("Training Charges");
	}
	
	list.add("CPL Esic");
	list.add("CBonus Esic");
	
	
	if(totalSummaryAtBottomFlag){
		list.add("B//Statutory Payments");
	}
	list.add("");
	list.add("C//Total (A+B)");
	list.add("c1#Employee Overhead Cost"); 
	list.add("c2#Travelling Allowance");
	list.add("c3#Additional Allowance");
	if(hideColumn){
	list.add("c4#Uniform Cost");
	}
	list.add("");
	if(hideColumn){
	list.add("D//Total (C+c1+c2+c3+c4)");
	}else{
	list.add("D//Total (C+c1+c2+c3)");
	}
	list.add("E//Management Fees");
	list.add("F//Total (D+E)");
	list.add("G//No. of Employees Required");
	list.add("H//Sum of (FXG)");
	list.add("I#Housekeeping Consumables");
	list.add("J#Housekeeping Equipment Charges");
//	list.add("K#Management fees");
	String str = "(H+I+J";
	int i =0;
	if(cnc.getOtherServicesList().size()>0){
		for(OtherServices os : cnc.getOtherServicesList()){
			char first = (char)(75+i);
			list.add(first+"#"+os.getOtherService());
			str += "+"+first;
			i++;
		}
	}
	list.add((char)(75+i)+"//Total "+str+")");
	if(hideColumn){
		list.add("L//OT");
//		list.add("M//ESIC @4.75%");
		list.add("M//ESIC");
		list.add("N//Chargable OT");
	}

	return list;
}

@Override
public void setCostSheetData(CNC cnc) {
	// TODO Auto-generated method stub
	this.cnc = cnc;
}
/** date 4.9.2018 added by komal**/
public String roundOff(double value){
	DecimalFormat df = new DecimalFormat("0");
	String amount=0+"";
	amount=df.format(value);
	System.out.println("Whole Amount :" + amount );
	return amount+"";
}

/** date 29.1.2019 added by komal**/
public String roundOffValue(double value){
	DecimalFormat df = new DecimalFormat("0.00");
	String amount=0+"";
	amount=df.format(value);
	System.out.println("Amount :" + amount );
	return amount+"";
}

@Override
public void setBomServiceList(ArrayList<Service> serList) {
	// TODO Auto-generated method stub
	
}
public void getBOMServiceReportDetails(XSSFWorkbook  workbook){
		
		if(workbook.getNumberOfSheets()>0){
			for(int i= 0 ;i<= workbook.getNumberOfSheets() ;i++){
				workbook.removeSheetAt(i);
			}
		}
		  XSSFSheet sheet = workbook.createSheet("Bom Service Material Report" + new Date());
		
		  
		  List<Service> serList = bomServiceList;
			
		    
		    
		    logger.log(Level.SEVERE,"serList -- " + serList.size());
		    
		    Cell cell;
		    Row row;
		    
		    
		    getBOMServiceListReportHeader(sheet,sheet.createRow(rowCount), workbook);
	               
						
						for(Service ser : serList){
//								i(commList.getProName().equals(pro) && tobj.equals(commList.getBranchName())){
							  columnCount = 0;
							  row = sheet.createRow(++rowCount);
				               
							cell = row.createCell(columnCount);
							cell.setCellValue(ser.getBranch());
							
							cell = row.createCell(++columnCount);
							cell.setCellValue(ser.getContractCount()+"");
							
							cell = row.createCell(++columnCount);
							cell.setCellValue((String) fmt.format(ser.getContractStartDate()));
							
							cell = row.createCell(++columnCount);
							cell.setCellValue((String) fmt.format(ser.getContractEndDate()));
							
							
							
							cell = row.createCell(++columnCount);
							cell.setCellValue((String) ser.getCustomerName());
							

							cell = row.createCell(++columnCount);
							cell.setCellValue( ser.getCustomerId()+"");
					
							cell = row.createCell(++columnCount);
							cell.setCellValue( ser.getCount()+"");
					
							
							cell = row.createCell(++columnCount);
							if(ser.getServiceDate()!=null){
								
							cell.setCellValue((String) fmt.format(ser.getServiceDate()));
							}else{
								cell.setCellValue("");
							}
							
							cell = row.createCell(++columnCount);
							cell.setCellValue( ser.getProductName()+"");
					
							cell = row.createCell(++columnCount);
							cell.setCellValue( ser.getQuantity()+"");
					
							cell = row.createCell(++columnCount);
							cell.setCellValue( ser.getUom()+"");
					
							cell = row.createCell(++columnCount);
							cell.setCellValue( ser.getStatus()+"");
							
							if(ser.getServiceProductList()!=null && ser.getServiceProductList().size()>0){
								for(int j = 0 ;j<ser.getServiceProductList().size();j++){
								
									if(j!=0){
										  columnCount = 0;
										  row = sheet.createRow(++rowCount);
							               
										cell = row.createCell(columnCount);
										cell.setCellValue(ser.getBranch());
										
										cell = row.createCell(++columnCount);
										cell.setCellValue(ser.getContractCount()+"");
										
										cell = row.createCell(++columnCount);
										if(ser.getContractStartDate()!= null) {
											cell.setCellValue((String) fmt.format(ser.getContractStartDate()));
										}else {
											cell.setCellValue("");
										}
										
										
										cell = row.createCell(++columnCount);
										if(ser.getContractEndDate()!=null) {
											cell.setCellValue((String) fmt.format(ser.getContractEndDate()));
										}else {
											cell.setCellValue("");
										}
										
										
										
										
										cell = row.createCell(++columnCount);
										cell.setCellValue((String) ser.getCustomerName());
										

										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getCustomerId()+"");
								
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getCount()+"");
								
										
										cell = row.createCell(++columnCount);
										if(ser.getServiceDate()!=null){
											
										cell.setCellValue((String) fmt.format(ser.getServiceDate()));
										}else{
											cell.setCellValue("");
										}
										
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getProductName()+"");
								
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getQuantity()+"");
								
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getUom()+"");
								
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getStatus()+"");
									}
							
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getName()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getQuantity()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getUnit()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getPrice()+"");
									
									/**
									 * 
									 */
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getIssueName()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getIssueQuantity()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getIssueUnit()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getIssuePrice()+"");
									
									/** **/
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getActulName()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getActulQuantity()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getActulUnit()+"");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue( ser.getServiceProductList().get(j).getActualPrice()+"");
									
									if(ser.getServiceProductList().get(j).getActulQuantity() != 0){
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getServiceProductList().get(j).getActulName()+"");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue(ser.getServiceProductList().get(j).getQuantity() - ser.getServiceProductList().get(j).getActulQuantity()+"");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getServiceProductList().get(j).getActulUnit()+"");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue(ser.getServiceProductList().get(j).getPrice() - ser.getServiceProductList().get(j).getActualPrice()+"");
										
									}else{
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
									}
									
									if(ser.getServiceProductList().get(j).getIssueQuantity() != 0){
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getServiceProductList().get(j).getActulName()+"");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue(ser.getServiceProductList().get(j).getIssueQuantity() - ser.getServiceProductList().get(j).getActulQuantity()+"");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue( ser.getServiceProductList().get(j).getActulUnit()+"");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue(ser.getServiceProductList().get(j).getIssuePrice() - ser.getServiceProductList().get(j).getActualPrice()+"");
										
									}else{
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
										
									}
									
									if(ser.getServiceCompleteDuration()==0){
										cell = row.createCell(++columnCount);
										cell.setCellValue("");
									}else{
										cell = row.createCell(++columnCount);
										cell.setCellValue(""+ser.getServiceCompleteDuration());
									}
								}	
							}else{

								
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								/** **/
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
								cell = row.createCell(++columnCount);
								cell.setCellValue("");
								
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
								
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
									
								
								if(ser.getServiceCompleteDuration()==0){
									cell = row.createCell(++columnCount);
									cell.setCellValue("");
								}else{
									cell = row.createCell(++columnCount);
									cell.setCellValue(""+ser.getServiceCompleteDuration());
								}
							
							}
							
						}
				
		    
	
}

private void getBOMServiceListReportHeader( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {
	int	columnCount = 0;
	Cell cell;
	
	 style2 = workbook.createCellStyle();
      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
      
      XSSFFont font = workbook.createFont();
      font.setFontHeightInPoints((short) 11);
      font.setFontName("Times New Roman");
      font.setBold(true);;
      
      style2.setFont(font);
      
      columnCount=0;
	    row = sheet.createRow(++rowCount);
		
		String chStr = getChart(0);
		String chStr1 = getChart(11);
		
		 String rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
		sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
		cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		cell.setCellValue((String) "Service Details");
      
		columnCount = columnCount+12;
		 chStr = getChart(12);
		 chStr1 = getChart(15);
		
		rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
		sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
		cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		cell.setCellValue((String) "Planned Material");
		columnCount = columnCount+4;
		 chStr = getChart(16);
		 chStr1 = getChart(19);
			
		rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
		sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
		cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		cell.setCellValue((String) "Issued Material");
		
		columnCount = columnCount+4;
		chStr = getChart(20);
		 chStr1 = getChart(23);
				
		rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
		sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
		cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		cell.setCellValue((String) "Actual Material");
		columnCount = columnCount+4;
		chStr = getChart(24);
		 chStr1 = getChart(27);
				
		rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
		sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
		cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		cell.setCellValue((String) "Planned - Actual Difference");
		
		chStr = getChart(28);
		 chStr1 = getChart(31);
		 columnCount = columnCount+4;
		 
		rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
		sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
		cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		cell.setCellValue((String) "Issued - Actual Difference");
		
		columnCount = columnCount+4;
		
		columnCount=0;
		row = sheet.createRow(++rowCount);
		
	for(int i =0 ; i<getBomListHeader().size();i++){
		cell = row.createCell(columnCount);
		cell.setCellValue((String) getBomListHeader().get(i));
		cell.setCellStyle(style2);
		++columnCount;
		
	}
	
}

	public ArrayList<String> getBomListHeader(){
		ArrayList<String> header = new ArrayList<String>();
		
		header.add("Branch");	
		header.add("Contract ID");
		header.add("Contract Start Date");	
		header.add("Contract End Date");	
		header.add("Customer Name");	
		header.add("Customer ID");	
		header.add("Service ID"); 	
		header.add("Service Date");	
		header.add("Service Product");
		header.add("Area/Quantity");
		header.add("UoM");
		header.add("Status"); 	
		
		header.add("Planned Material");
		header.add("Quantity");
		header.add("UoM");
		header.add("Cost");
		
		header.add("Issued Material");	
		header.add("Quantity");	
		header.add("UoM");	
		header.add("Cost"); 	
		
		header.add("Actual Material"); 	
		header.add("Quantity");	
		header.add("UoM");
		header.add("Cost"); 	
		
		header.add("Material");
		header.add("Quantity");	
		header.add("UoM");	
		header.add("Cost"); 	
		
		header.add("Material");
		header.add("Quantity");	
		header.add("UoM");
		header.add("Cost");
		header.add("Service Hour");

		return header;
	}

	/**
	 * Date 16-10-2018 By Vijay
	 * Des :- if there is no data then show messge of No Data found on excel
	 */
	private void getMessageNoDataFound(XSSFSheet sheet, Row row, XSSFWorkbook workbook) {
		
		int	columnCount = 0;
		Cell cell;
		
		 style2 = workbook.createCellStyle();
	      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	      
	      XSSFFont font = workbook.createFont();
	      font.setFontHeightInPoints((short) 11);
	      font.setFontName("Times New Roman");
	      font.setBold(true);;
	      
	      style2.setFont(font);
	      
	
		cell = row.createCell(columnCount);
		cell.setCellValue((String) "No Data Found");
		cell.setCellStyle(style2);
	}
	/**
	 * ends here
	 */
	
	
	
	/*
	 * @author Ashwini
	 * Date:02-02-2019
	 * Desc:contract p & NL sheet for nbhc
	 */
	
	
	public void getContractPAndLReport(XSSFWorkbook workbook) {
		
		logger.log(Level.SEVERE, "GETTING MCR SIZE --" + contractProfitLossReportList.size()+" FROM DATE : "+FromDate+" TO DATE : "+ToDate+" SEGMENT : "+segment+" COMPANY ID : "+companyId1);
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		logger.log(Level.SEVERE, "get here for excel download -- " + contractProfitLossReportList.size());
		logger.log(Level.SEVERE, "sheet size -- " + workbook.getNumberOfSheets());

		if (workbook.getNumberOfSheets() > 0) {
			for (int i = 0; i <= workbook.getNumberOfSheets(); i++) {
				workbook.removeSheetAt(i);
			}
		}
		XSSFSheet sheet = workbook.createSheet("Contract PNL ");

		logger.log(Level.SEVERE, "Contract PNL  -- " + contractProfitLossReportList.size());
		List<MaterialConsumptionReport> serList = contractProfitLossReportList;

		/*
		 * calculation of total revenue
		 */

		System.out.println("fromdate:" + FromDate);
		System.out.println("Todate:" + ToDate);
		List<Integer> contractIdList = new ArrayList<Integer>();
		for (MaterialConsumptionReport billcount : serList) {
			contractIdList.add(billcount.getContractId());
//			System.out.println("MaterialConsumptionReport count::" + billcount.getContractId());
		}
		System.out.println("countlist:::" + contractIdList.size());
		logger.log(Level.SEVERE, "countlist size ::" + contractIdList.size()+"  "+contractIdList);
		
		List<BillingDocument> billinglist;
		
		if (FromDate != null && ToDate != null) {

			billinglist = ofy().load().type(BillingDocument.class).filter("companyId", companyId1)
					.filter("billingDocumentInfo.billingDate >=", DateUtility.getDateWithTimeZone("IST", FromDate))
					.filter("billingDocumentInfo.billingDate <=", DateUtility.getDateWithTimeZone("IST", ToDate))
					.filter("contractCount IN", contractIdList).list();
			System.out.println("billinglist.size inside if" + billinglist.size());

		} else {
			billinglist = ofy().load().type(BillingDocument.class).filter("companyId", companyId1)
					.filter("contractCount IN", contractIdList).list();

			System.out.println("Date:" + date);
			System.out.println("billinglist.size" + billinglist.size());
		}

		logger.log(Level.SEVERE, "billinglist.size ::: " + billinglist.size());

		System.out.println("company id::::" + companyId1);

		HashMap<Integer, Double> billmap = new HashMap<Integer, Double>();

		double billtotal = 0;
		if (billinglist != null && billinglist.size() > 0) {
			for (BillingDocument billing : billinglist) {
				List<SalesOrderProductLineItem> productList = billing.getSalesOrderProducts();
				double targetRevnue = 0;

				for (SalesOrderProductLineItem product : productList) {
					targetRevnue = targetRevnue + product.getBaseBillingAmount();
					System.out.println("product base amount ::" + targetRevnue);
					if (billmap != null && billmap.containsKey(billing.getContractCount())) {
						billtotal = targetRevnue + billmap.get(billing.getContractCount());
						billmap.put(billing.getContractCount(), billtotal);
						System.out.println("billtotal inside if ::" + billtotal);
					} else {

						billmap.put(billing.getContractCount(), targetRevnue);
						System.out.println("billing total inside else::::" + targetRevnue);
					}
				}
				System.out.println("billing contract Count::::" + billing.getContractCount());

			}
		}

		/*
		 * calculation of billing amount
		 */
		List<Invoice> invoicelist = null;
		
		try{
			if (FromDate != null && ToDate != null) {
				invoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId1)
						.filter("contractCount IN", contractIdList)
						.filter("invoiceDate >=", DateUtility.getDateWithTimeZone("IST", FromDate))
						.filter("invoiceDate <= ", DateUtility.getDateWithTimeZone("IST", ToDate))
						.list();
				System.out.println("invoice list size inside if ::::" + invoicelist.size());
			} else {
				invoicelist = ofy().load().type(Invoice.class).filter("contractCount IN", contractIdList)
						.filter("companyId", companyId1).list();
				System.out.println("invoice list size::::" + invoicelist.size());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		logger.log(Level.SEVERE, "invoice list size::::" + invoicelist.size());

		HashMap<Integer, Double> invoicemap = new HashMap<Integer, Double>();
		Double invoiceTotal;
		if (invoicelist != null && invoicelist.size() > 0) {
			for (Invoice invoice : invoicelist) {
				if (invoicemap != null && invoicemap.containsKey(invoice.getContractCount())) {
					invoiceTotal = invoice.getInvoiceAmount() + invoicemap.get(invoice.getInvoiceAmount());
					System.out.println("invoice amount:::" + invoiceTotal);
					invoicemap.put(invoice.getContractCount(), invoiceTotal);
				} else {
					invoicemap.put(invoice.getContractCount(), invoice.getInvoiceAmount());
					System.out.println("invoice amount123:::" + invoice.getInvoiceAmount());
				}

			}
		}

		/*
		 * calculating material cost planned
		 */
		List<Contract> contract;
		if (FromDate != null && ToDate != null) {
			System.out.println("FromDate1:" + FromDate);
			System.out.println("ToDate1::" + ToDate);
			contract = ofy().load().type(Contract.class).filter("companyId", companyId1)
					.filter("contractDate >=", DateUtility.getDateWithTimeZone("IST", FromDate))
					.filter("contractDate <=", DateUtility.getDateWithTimeZone("IST", ToDate))
					.filter("count IN", contractIdList).list();

			System.out.println("contract Sizeinside if ::" + contract.size());
		} else {
			contract = ofy().load().type(Contract.class).filter("companyId", companyId1).filter("count IN", contractIdList)
					.list();
			System.out.println("contract Size::" + contract.size());
		}

		logger.log(Level.SEVERE, "contract Size::" + contract.size());
		List<ServiceSchedule> schedulelist = null;
		List<ManPowerCostStructure> manPowerlist = null;

		List<Quotation> qoutation;
		if (FromDate != null && ToDate != null) {
			qoutation = ofy().load().type(Quotation.class).filter("companyId", companyId1)
					.filter("quotationDate >=", DateUtility.getDateWithTimeZone("IST", FromDate))
					.filter("quotationDate <=", DateUtility.getDateWithTimeZone("IST", ToDate))
					.filter("contractCount IN", contractIdList).list();

			System.out.println("qoutation list inside if :::" + qoutation.size());
		} else {
			qoutation = ofy().load().type(Quotation.class).filter("contractCount IN", contractIdList).list();
			System.out.println("qoutation list:::" + qoutation.size());
		}

		logger.log(Level.SEVERE, "qoutation list:::" + qoutation.size());

		/*
		 * calculating Material Cost Actual
		 */
		List<MaterialIssueNote> materiallist;
		if (FromDate != null && ToDate != null) {
			materiallist = ofy().load().type(MaterialIssueNote.class)
					.filter("minDate >=", DateUtility.getDateWithTimeZone("IST", FromDate))
					.filter("minDate <=", DateUtility.getDateWithTimeZone("IST", ToDate))
					.filter("minSoId IN", contractIdList).list();
			System.out.println("materiallist list inside if :::" + materiallist.size());
		} else {

			materiallist = ofy().load().type(MaterialIssueNote.class).filter("minSoId IN", contractIdList).list();
			System.out.println("material issue note size::" + materiallist.size());
		}

		logger.log(Level.SEVERE, "material issue note size::" + materiallist.size());

		// product = ofy().load().type(SuperProduct.class).first().now();
		List<SuperProduct> product = ofy().load().type(SuperProduct.class).list();
		System.out.println("product size::" + product.size());

		/*
		 * calculating Operator Cost Actual
		 */

		Double duration = 0.0;
		Double min = 60.0;
		logger.log(Level.SEVERE, "fromdate service::" + FromDate);
		logger.log(Level.SEVERE, "Todate service::" + ToDate);

		List<Service> servicelist = null;
		
		try{
			if (FromDate != null && ToDate != null) {
				logger.log(Level.SEVERE, "companyId INSIDE SERVICELIST::::" + companyId1);
				servicelist = ofy().load().type(Service.class).filter("companyId", companyId1)
						.filter("contractCount IN", contractIdList)
						.filter("serviceDate >=", DateUtility.getDateWithTimeZone("IST", FromDate))
						.filter("serviceDate <=", DateUtility.getDateWithTimeZone("IST", ToDate))
						.list();
	
			} else {
				servicelist = ofy().load().type(Service.class).filter("companyId", companyId1)
						.filter("contractCount IN", contractIdList).list();
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "servicelist size ::" + servicelist.size());

		System.out.println("servicelist size ::" + servicelist.size());

		logger.log(Level.SEVERE, "serList -- " + serList.size());

		Cell cell;
		Row row;

		getContractProfitReportHeader(sheet, sheet.createRow(rowCount), workbook);

		for (MaterialConsumptionReport ser : serList) {

			double targetRevenue = 0;
			double billingAmount = 0;
			double materialCostPlanned = 0;
			Double operatorCostActual = 0.0;
			double operatorCostPlanned = 0;
			double totalAmount = 0;
			double materialActualcost =
					0;
			double totalActualCost = 0;
			double targetOperatingProfit = 0;
			float totalOperatingProfitper;
			double actualOperatingProfit = 0;
			double actualOperatingProfitper;
			double materialCostRevenueper;
			double operatorCostRevenueper;
			double varinOperatingProfit = 0;
			double varinMaterialCost = 0;
			double VarinMaterialCostper;
			double VarinOperatorCost = 0;
			double VarinOperatorCostper;
			// i(commList.getProName().equals(pro) &&
			// tobj.equals(commList.getBranchName())){
			columnCount = 0;
			row = sheet.createRow(++rowCount);

			cell = row.createCell(columnCount);
			cell.setCellValue(ser.getCustId() + "");

			cell = row.createCell(++columnCount);
			cell.setCellValue((String) ser.getCustName());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContractId() + "");
			System.out.println("contract id of p&nl::" + ser.getContractId());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContGroup());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContType());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getBranch() + "");

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContAmount() + "");

			cell = row.createCell(++columnCount);
			if (billmap.get(ser.getContractId()) != null) {
				targetRevenue = billmap.get(ser.getContractId());
			} else {
				targetRevenue = 0;
			}

			cell.setCellValue(targetRevenue);
			System.out.println("billvalue:::" + billmap.get(ser.getContractId()));

			cell = row.createCell(++columnCount);
			if (invoicemap.get(ser.getContractId()) != null) {
				billingAmount = invoicemap.get(ser.getContractId());
			} else {
				billingAmount = 0;
			}

			cell.setCellValue(billingAmount);
			System.out.println("invoice value :::::::" + invoicemap.get(ser.getContractId()));

			/*
			 * Material Cost Planned (linked to BOM)
			 */

			for (Contract con : contract) {
				System.out.println("contract count :::" + con.getCount());
				System.out.println("seriallist count::" + ser.getContractId());
				if (con.getCount() == ser.getContractId()) {
					System.out.println("inside if");
					schedulelist = con.getServiceScheduleList();
					System.out.println("schedulelist size :::" + schedulelist.size());

					for (ServiceSchedule schedule : schedulelist) {
						System.out.println(
								"schedule getServiceProductList :::" + schedule.getServiceProductList().get(0).size());

						HashMap<Integer, ArrayList<ProductGroupList>> materialmap = null;
						materialmap = schedule.getServiceProductList();
						//

						for (ArrayList<ProductGroupList> materialprod : materialmap.values()) {
							List<ProductGroupList> materialprod1 = materialprod;
							System.out.println("materialprod1:::" + materialprod);

							for (ProductGroupList materialpro : materialprod1) {
								if (materialpro.getPrice() != 0) {
									materialCostPlanned = materialCostPlanned + materialpro.getPrice();
								} else {
									materialCostPlanned = 0;
								}

							}

						}

						System.out.println("Total price = " + materialCostPlanned);
					}
				}

			}

			cell = row.createCell(++columnCount);
			cell.setCellValue(materialCostPlanned);

			/*
			 * material cost actual
			 */

			for (MaterialIssueNote materialactual : materiallist) {
				System.out.println("materialactual contractid::" + materialactual.getMinSoId());
				if (materialactual.getMinSoId() == ser.getContractId()) {
					System.out.println("inside material cost actual if ::");
					List<MaterialProduct> MaterialProductTable = materialactual.getSubProductTablemin();
					System.out.println("MaterialProductTable size::" + MaterialProductTable.size());
					for (MaterialProduct materilProduct : MaterialProductTable) {
						double qty = materilProduct.getMaterialProductRequiredQuantity();
						System.out.println("qty::" + qty);

						for (SuperProduct minproduct : product) {
							System.out.println("product.getProductName()::" + minproduct.getProductName());
							System.out.println("(materilProduct.getMaterialProductName():::"
									+ materilProduct.getMaterialProductName());
							if (materilProduct.getMaterialProductName().equals(minproduct.getProductName())) {
								System.out.println("inside product list:::");
								materialActualcost = materialActualcost + (minproduct.getPrice() * qty);
								System.out.println("minproduct price:" + minproduct.getPrice());
								System.out.println("actual cost ::" + materialActualcost);
							}
						}

					}
				}

			}

			cell = row.createCell(++columnCount);
			cell.setCellValue(materialActualcost);
			//
			/*
			 * Operator Cost Planned
			 */
			cell = row.createCell(++columnCount);
			double manpower1 = 0;
			for (Quotation quo : qoutation) {
				if (quo.getContractCount() == ser.getContractId()) {
					List<ManPowerCostStructure> manPowCostList = quo.getManPowCostList();
					for (ManPowerCostStructure manPowerCost : manPowCostList) {
						if (manPowerCost.getTotalCost() != 0) {
							manpower1 = manpower1 + manPowerCost.getTotalCost();
						} else {
							manpower1 = 0;
						}

					}
				}

				operatorCostPlanned = manpower1;
				cell.setCellValue(operatorCostPlanned);

				System.out.println("operatorCostPlanned++" + operatorCostPlanned);

			}
			System.out.println("operator cost planned ::" + operatorCostPlanned);

			// /*
			// * Operator Cost Actual
			// */
			//
			for (Quotation qo : qoutation) {

				if (qo.getContractCount() == ser.getContractId()) {
					manPowerlist = qo.getManPowCostList();
					System.out.println("manPowerlist size::" + manPowerlist.size());
					for (ManPowerCostStructure manpower : manPowerlist) {
						for (Service service : servicelist) {
							System.out.println("contract count:::" + service.getContractCount());
							if (service.getContractCount() == ser.getContractId()) {
								System.out.println("inside servicelist if::");
								System.out.println("service.getServiceCompleteDuration() ::"
										+ service.getServiceCompleteDuration());

								if (service.getServiceCompleteDuration() != 0) {
									duration = (service.getServiceCompleteDuration()) / min;
								} else {
									duration = 0.0;
								}

								System.out.println("duration:::" + duration);
								operatorCostActual = operatorCostActual + (manpower.getManCostPerHour() * duration);

							}

							System.out.println("Operator Cost Actual:: " + operatorCostActual);

						}

					}
				}

			}

			cell = row.createCell(++columnCount);
			cell.setCellValue(operatorCostActual);

			System.out.println("mapower cost is:::" + operatorCostActual);

			//
			// /*
			// *Total Actual Cost
			// */
			//
			totalActualCost = materialActualcost + operatorCostActual;
			System.out.println("totalActualCost:::" + totalActualCost);
			cell = row.createCell(++columnCount);
			cell.setCellValue(totalActualCost);

			/*
			 * Target Operating Profit
			 */

			targetOperatingProfit = targetRevenue - materialCostPlanned - operatorCostPlanned;
			cell = row.createCell(++columnCount);
			cell.setCellValue(targetOperatingProfit);
			System.out.println("totalOperatingProfit:::" + targetOperatingProfit);

			/*
			 * Target Operating Profit %
			 */

			totalOperatingProfitper = Math.round((targetOperatingProfit / targetRevenue) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(totalOperatingProfitper + "%");
			System.out.println("totalOperatingProfitper::" + totalOperatingProfitper);

			/*
			 * Actual Operating Profit
			 */

			actualOperatingProfit = billingAmount - totalActualCost;
			cell = row.createCell(++columnCount);
			cell.setCellValue(actualOperatingProfit);
			System.out.println("Actual Operating Profit:::" + actualOperatingProfit);

			/*
			 * Actual Operating Profit %
			 */

			actualOperatingProfitper = Math.round((actualOperatingProfit / billingAmount) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(actualOperatingProfitper + "%");

			System.out.println("Actual Operating Profit %:::" + actualOperatingProfitper);

			/*
			 * Material Cost as a % of Revenue
			 */
			materialCostRevenueper = Math.round((materialActualcost / billingAmount) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(materialCostRevenueper + "%");

			System.out.println("materialCostRevenueper %:::" + materialCostRevenueper);

			/*
			 * Operator Cost as a % of Revenue
			 */

			operatorCostRevenueper = Math.round((operatorCostActual / materialCostPlanned) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(operatorCostRevenueper + "%");

			System.out.println("operatorCostRevenueper" + operatorCostRevenueper);

			/*
			 * Variance in Operating Profit
			 */

			varinOperatingProfit = targetOperatingProfit - actualOperatingProfit;
			cell = row.createCell(++columnCount);
			cell.setCellValue(varinOperatingProfit);
			System.out.println("varinOperatingProfit:::" + varinOperatingProfit);

			/*
			 * Variance in Material Cost
			 */
			varinMaterialCost = materialCostPlanned - materialActualcost;
			cell = row.createCell(++columnCount);
			cell.setCellValue(varinMaterialCost);
			System.out.println("varinMaterialCost::" + varinMaterialCost);

			/*
			 * Variance in Material Cost %
			 */
			VarinMaterialCostper = Math.round((varinMaterialCost / materialCostPlanned) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(VarinMaterialCostper + "%");
			System.out.println("VarinMaterialCostper:::" + VarinMaterialCostper);

			/*
			 * Variance in Operator Cost
			 */
			VarinOperatorCost = operatorCostPlanned - operatorCostActual;
			cell = row.createCell(++columnCount);
			cell.setCellValue(VarinOperatorCost);

			System.out.println("VarinOperatorCost:::" + VarinOperatorCost);

			/*
			 * Variance in Operator Cost %
			 */

			VarinOperatorCostper = Math.round((VarinOperatorCost / operatorCostPlanned) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(VarinOperatorCostper + "%");

		}

	}
	
	private void getContractProfitReportHeader( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {
		int	columnCount = 0;
		Cell cell;
		
		 style2 = workbook.createCellStyle();
	      style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	      style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	      
	      XSSFFont font = workbook.createFont();
	      font.setFontHeightInPoints((short) 11);
	      font.setFontName("Times New Roman");
	      font.setBold(true);;
	      
	      style2.setFont(font);
	      
	      columnCount=0;
		    row = sheet.createRow(++rowCount);
			
			String chStr = getChart(0);
			String chStr1 = getChart(2);
			
			 String rowrang =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
			sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang));
			cell = row.createCell(columnCount);
			cell.setCellStyle(style2);
			cell.setCellValue((String) "CONTRACT PNL");
			
			/*
			 * commented by Ashwini
			 */
			
//			if(date!=null && date.trim().length()>0){
//				 row = sheet.createRow(++rowCount);
//					
//					
//					 String rowrang1 =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
//					sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang1));
//					cell = row.createCell(columnCount);
//					cell.setCellStyle(style2);
//					cell.setCellValue(date);
//			}
	      
			if(FromDate !=null && ToDate !=null){
				 row = sheet.createRow(++rowCount);
				String rowrang1 =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
				sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang1));
				cell = row.createCell(columnCount);
				cell.setCellStyle(style2);
				cell.setCellValue("Period :"+fmt.format(FromDate) + " To :" + fmt.format(ToDate));
			}
	      
			if(segment!=null && segment.trim().length()>0){
				 row = sheet.createRow(++rowCount);
					
					
					 String rowrang1 =  chStr+(1+rowCount) + ":" + chStr1+(1+rowCount);
					sheet.addMergedRegion(CellRangeAddress.valueOf(rowrang1));
					cell = row.createCell(columnCount);
					cell.setCellStyle(style2);
					cell.setCellValue(segment);
			}
			
			columnCount = columnCount+12;
			 chStr = getChart(12);
			 chStr1 = getChart(15);
			
			
			columnCount=0;
			row = sheet.createRow(++rowCount);
			
			ArrayList<String> headerList = getContractProfitListHeader();
			logger.log(Level.SEVERE,"headerList size -- " + headerList.size());
		for(int i =0 ; i<headerList.size();i++){
			cell = row.createCell(columnCount);
			cell.setCellValue((String) headerList.get(i));
			cell.setCellStyle(style2);
			++columnCount;
			
		}
		
	}

		public ArrayList<String> getContractProfitListHeader(){
			ArrayList<String> header = new ArrayList<String>();
			header.add("Customer ID");//1
			header.add("SAP Code");//1
			header.add("Customer Name");//2
			header.add("Contract ID	");//3
			header.add("Contract Group");//4
			header.add("Contract Type");//5
			header.add("Branch");//6
			header.add("Contract Amount");//7
			header.add("Target Revenue");//
			header.add("Service Value");//9
			header.add("Material Cost Planned");//10
			header.add("Material Cost Actual ");//11
			header.add("Operator Cost Planned");//12
			header.add("Operator Cost Actual");//13
			header.add("Total Actual Cost");//14
			header.add("Target Operating Profit");//15
			header.add("Target Operating Profit %");//16
			header.add("Actual Operating Profit");//17
			header.add("Actual Operating Profit %");//18
			header.add("Material Cost as a % of Revenue");//19
			header.add("Operator Cost as a % of Revenue");//20
			header.add("Variance in Operating Profit");//21
			header.add("Variance in Material Cost");//22
			header.add("Variance in Material Cost % ");//23
			header.add("Variance in Operator Cost");//24
			header.add("Variance in Operator Cost % ");//25

			return header;
		}


	@Override
	public void setContractPNLReport(ArrayList<MaterialConsumptionReport> reportList, Date fromdate, Date toDate,
			String segment1, long companyId) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "SETTING MCR SIZE --" + reportList.size()+" FROM DATE : "+fromdate+" TO DATE : "+toDate+" SEGMENT : "+segment1+" COMPANY ID : "+companyId);
		contractProfitLossReportList = reportList;
		FromDate = fromdate;
		ToDate = toDate;
		segment = segment1;
		companyId1 = companyId;
	}
	/** date 8.4.2019 added by komal for new contract p and l report **/
public void getContractServicePAndLReport(XSSFWorkbook workbook) {
		
		logger.log(Level.SEVERE, "GETTING MCR SIZE --" + contractProfitLossReportList.size()+" FROM DATE : "+FromDate+" TO DATE : "+ToDate+" SEGMENT : "+segment+" COMPANY ID : "+companyId1);
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		logger.log(Level.SEVERE, "get here for excel download -- " + contractProfitLossReportList.size());
		logger.log(Level.SEVERE, "sheet size -- " + workbook.getNumberOfSheets());

		if (workbook.getNumberOfSheets() > 0) {
			for (int i = 0; i <= workbook.getNumberOfSheets(); i++) {
				workbook.removeSheetAt(i);
			}
		}
		XSSFSheet sheet = workbook.createSheet("Contract PNL ");

		logger.log(Level.SEVERE, "Contract PNL  -- " + contractProfitLossReportList.size());
		List<MaterialConsumptionReport> serList = contractProfitLossReportList;


		Cell cell;
		Row row;

		getContractProfitReportHeader(sheet, sheet.createRow(rowCount), workbook);
		HashMap<Integer  , ArrayList<MaterialConsumptionReport>> reportMap = new HashMap<Integer  , ArrayList<MaterialConsumptionReport>>();
		ArrayList<MaterialConsumptionReport> reportList = new ArrayList<MaterialConsumptionReport>();
		for(MaterialConsumptionReport report : serList){
			if(reportMap.containsKey(report.getCustId())){
				reportList = reportMap.get(report.getCustId());
				reportList.add(report);
				reportMap.put(report.getCustId(), reportList);
			}else{
				reportList = new ArrayList<MaterialConsumptionReport>();
				reportList.add(report);
				reportMap.put(report.getCustId(), reportList);
			}
		}
		
	
		double totalTargetRevenue = 0;
		double totalbillingAmount = 0;
		double totalmaterialCostPlanned = 0;
		double totaloperatorCostActual = 0.0;
		double totaloperatorCostPlanned = 0;
		double totalServiceAmount = 0;
		double totalmaterialActualcost =0;
		double totalCost = 0;
		double totaltargetOperatingProfit = 0;
		double finalOperatingProfitper;
		double totalactualOperatingProfit = 0;
		double totalactualOperatingProfitper;
		double totalmaterialCostRevenueper;
		double totaloperatorCostRevenueper;
		double totalvarinOperatingProfit = 0;
		double totalvarinMaterialCost = 0;
		double totalVarinMaterialCostper;
		double totalVarinOperatorCost = 0;
		double totalVarinOperatorCostper;
		double totalContractAmount = 0;
		for(Map.Entry<Integer, ArrayList<MaterialConsumptionReport>> entry : reportMap.entrySet()){
			ArrayList<MaterialConsumptionReport> materialList = entry.getValue();
			logger.log(Level.SEVERE , "Customer id key:" + materialList.size());
			double customerTargetRevenue = 0;
			double customerbillingAmount = 0;
			double customermaterialCostPlanned = 0;
			double customeroperatorCostActual = 0.0;
			double customeroperatorCostPlanned = 0;
			double customerServiceAmount = 0;
			double customermaterialActualcost =0;
			double customerCost = 0;
			double customertargetOperatingProfit = 0;
			double customerOperatingProfitper;
			double customeractualOperatingProfit = 0;
			double customeractualOperatingProfitper;
			double customermaterialCostRevenueper;
			double customeroperatorCostRevenueper;
			double customervarinOperatingProfit = 0;
			double customervarinMaterialCost = 0;
			double customerVarinMaterialCostper;
			double customerVarinOperatorCost = 0;
			double customerVarinOperatorCostper;
			double customerContractAmount = 0;

		for (MaterialConsumptionReport ser : materialList) {

			double targetRevenue = 0;
			double billingAmount = 0;
			double materialCostPlanned = 0;
			double operatorCostActual = 0.0;
			double operatorCostPlanned = 0;
			double totalAmount = 0;
			double materialActualcost =0;
			double totalActualCost = 0;
			double targetOperatingProfit = 0;
			double totalOperatingProfitper;
			double actualOperatingProfit = 0;
			double actualOperatingProfitper;
			double materialCostRevenueper;
			double operatorCostRevenueper;
			double varinOperatingProfit = 0;
			double varinMaterialCost = 0;
			double VarinMaterialCostper;
			double VarinOperatorCost = 0;
			double VarinOperatorCostper=0;
			// i(commList.getProName().equals(pro) &&
			// tobj.equals(commList.getBranchName())){
			columnCount = 0;
			row = sheet.createRow(++rowCount);

			cell = row.createCell(columnCount);
			cell.setCellValue(ser.getCustId() + "");
			
			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getReferenceNumber() + "");

			cell = row.createCell(++columnCount);
			cell.setCellValue((String) ser.getCustName());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContractId() + "");
			System.out.println("contract id of p&nl::" + ser.getContractId());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContGroup());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContType());

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getBranch() + "");

			cell = row.createCell(++columnCount);
			cell.setCellValue(ser.getContAmount() + "");
			totalContractAmount += ser.getContAmount();
			customerContractAmount += ser.getContAmount();
			
			cell = row.createCell(++columnCount);
			targetRevenue = ser.getTargetRevenue();
			cell.setCellValue(targetRevenue);
			totalTargetRevenue += targetRevenue;
			customerTargetRevenue += targetRevenue;
//			System.out.println("billvalue:::" + billmap.get(ser.getContractId()));

			cell = row.createCell(++columnCount);
			billingAmount = ser.getTotal();
			cell.setCellValue(billingAmount);
			totalbillingAmount += billingAmount;
			customerbillingAmount += billingAmount;
//			System.out.println("invoice value :::::::" + invoicemap.get(ser.getContractId()));

			/*
			 * Material Cost Planned (linked to BOM)
			 */

			materialCostPlanned = ser.getPlannedminCost();
			cell = row.createCell(++columnCount);
			cell.setCellValue(materialCostPlanned);
			totalmaterialCostPlanned += materialCostPlanned;
			customermaterialCostPlanned += materialCostPlanned;
			/*
			 * material cost actual
			 */

			materialActualcost = ser.getActualMinCost();
			cell = row.createCell(++columnCount);
			cell.setCellValue(materialActualcost);
			totalmaterialActualcost += materialActualcost;
			customermaterialActualcost += materialActualcost;
			/*
			 * Operator Cost Planned
			 */
			operatorCostPlanned = ser.getPlannedLabourCost();
			cell = row.createCell(++columnCount);
			cell.setCellValue(operatorCostPlanned);
			totaloperatorCostPlanned += operatorCostPlanned;
			customeroperatorCostPlanned += operatorCostPlanned;
			System.out.println("operatorCostPlanned++" + operatorCostPlanned);

			 /*
			 * Operator Cost Actual
			 */
			
			operatorCostActual = ser.getActualLabourCost();
			cell = row.createCell(++columnCount);
			cell.setCellValue(operatorCostActual);
			totaloperatorCostActual += operatorCostActual;
			customeroperatorCostActual += operatorCostActual;
			System.out.println("mapower cost is:::" + operatorCostActual);

			//
			// /*
			// *Total Actual Cost
			// */
			//
			totalActualCost = materialActualcost + operatorCostActual;
			System.out.println("totalActualCost:::" + totalActualCost);
			cell = row.createCell(++columnCount);
			cell.setCellValue(totalActualCost);
			
			/*
			 * Target Operating Profit
			 */

			targetOperatingProfit = targetRevenue - materialCostPlanned - operatorCostPlanned;
			cell = row.createCell(++columnCount);
			cell.setCellValue(targetOperatingProfit);
			System.out.println("totalOperatingProfit:::" + targetOperatingProfit);

			/*
			 * Target Operating Profit %
			 */

			totalOperatingProfitper = Math.round((targetOperatingProfit / targetRevenue) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(totalOperatingProfitper + "%");
			System.out.println("totalOperatingProfitper::" + totalOperatingProfitper);

			/*
			 * Actual Operating Profit
			 */

			actualOperatingProfit = billingAmount - totalActualCost;
			cell = row.createCell(++columnCount);
			cell.setCellValue(actualOperatingProfit);
			System.out.println("Actual Operating Profit:::" + actualOperatingProfit);

			/*
			 * Actual Operating Profit %
			 */

			actualOperatingProfitper = Math.round((actualOperatingProfit / billingAmount) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(actualOperatingProfitper + "%");

			System.out.println("Actual Operating Profit %:::" + actualOperatingProfitper);

			/*
			 * Material Cost as a % of Revenue
			 */
			materialCostRevenueper = Math.round((materialActualcost / billingAmount) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(materialCostRevenueper + "%");

			System.out.println("materialCostRevenueper %:::" + materialCostRevenueper);

			/*
			 * Operator Cost as a % of Revenue
			 */

			operatorCostRevenueper = Math.round((operatorCostActual / billingAmount) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(operatorCostRevenueper + "%");

			System.out.println("operatorCostRevenueper" + operatorCostRevenueper);

			/*
			 * Variance in Operating Profit
			 */

			varinOperatingProfit = targetOperatingProfit - actualOperatingProfit;
			cell = row.createCell(++columnCount);
			cell.setCellValue(varinOperatingProfit);
			System.out.println("varinOperatingProfit:::" + varinOperatingProfit);

			/*
			 * Variance in Material Cost
			 */
			varinMaterialCost = materialCostPlanned - materialActualcost;
			cell = row.createCell(++columnCount);
			cell.setCellValue(varinMaterialCost);
			System.out.println("varinMaterialCost::" + varinMaterialCost);

			/*
			 * Variance in Material Cost %
			 */
			VarinMaterialCostper = Math.round((varinMaterialCost / materialCostPlanned) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(VarinMaterialCostper + "%");
			System.out.println("VarinMaterialCostper:::" + VarinMaterialCostper);

			/*
			 * Variance in Operator Cost
			 */
			VarinOperatorCost = operatorCostPlanned - operatorCostActual;
			cell = row.createCell(++columnCount);
			cell.setCellValue(VarinOperatorCost);

			System.out.println("VarinOperatorCost:::" + VarinOperatorCost);

			/*
			 * Variance in Operator Cost %
			 */

			VarinOperatorCostper = Math.round((VarinOperatorCost / operatorCostPlanned) * 100);
			cell = row.createCell(++columnCount);
			cell.setCellValue(VarinOperatorCostper + "%");

		}
		
		columnCount = 6;
	//	rowCount++;
		row = sheet.createRow(++rowCount);

		cell = row.createCell(columnCount);
		cell.setCellValue("Total");
//		
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerContractAmount + "");
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerTargetRevenue);

		cell = row.createCell(++columnCount);
		cell.setCellValue(customerbillingAmount);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(customermaterialCostPlanned);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(customermaterialActualcost);
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(customeroperatorCostPlanned);
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(customeroperatorCostActual);
		
	//	totalCost = totalmaterialActualcost + totaloperatorCostActual;
		customerCost = customermaterialActualcost + customeroperatorCostActual;
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerCost);
		
		customertargetOperatingProfit = customerTargetRevenue - customermaterialCostPlanned - customeroperatorCostPlanned;
		cell = row.createCell(++columnCount);
		cell.setCellValue(customertargetOperatingProfit);
		

		customerOperatingProfitper = Math.round((customertargetOperatingProfit / customerTargetRevenue) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerOperatingProfitper + "%");
		System.out.println("totalOperatingProfitper::" + customerOperatingProfitper);

		/*
		 * Actual Operating Profit
		 */

		customeractualOperatingProfit = customerbillingAmount - customerCost;
		cell = row.createCell(++columnCount);
		cell.setCellValue(customeractualOperatingProfit);
		System.out.println("Actual Operating Profit:::" + customeractualOperatingProfit);

		/*
		 * Actual Operating Profit %
		 */

		customeractualOperatingProfitper = Math.round((customeractualOperatingProfit / customerbillingAmount) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(customeractualOperatingProfitper + "%");

		System.out.println("Actual Operating Profit %:::" + customeractualOperatingProfitper);

		/*
		 * Material Cost as a % of Revenue
		 */
		customermaterialCostRevenueper = Math.round((customermaterialActualcost / customerbillingAmount) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(customermaterialCostRevenueper + "%");

		System.out.println("materialCostRevenueper %:::" + customermaterialCostRevenueper);

		/*
		 * Operator Cost as a % of Revenue
		 */

		customeroperatorCostRevenueper = Math.round((customeroperatorCostActual / customerbillingAmount) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(customeroperatorCostRevenueper + "%");

		System.out.println("operatorCostRevenueper" + customeroperatorCostRevenueper);

		/*
		 * Variance in Operating Profit
		 */

		customervarinOperatingProfit = customertargetOperatingProfit - customeractualOperatingProfit;
		cell = row.createCell(++columnCount);
		cell.setCellValue(customervarinOperatingProfit);
		System.out.println("varinOperatingProfit:::" + customervarinOperatingProfit);

		/*
		 * Variance in Material Cost
		 */
		customervarinMaterialCost = customermaterialCostPlanned - customermaterialActualcost;
		cell = row.createCell(++columnCount);
		cell.setCellValue(customervarinMaterialCost);
		System.out.println("varinMaterialCost::" + customervarinMaterialCost);

		/*
		 * Variance in Material Cost %
		 */
		customerVarinMaterialCostper = Math.round((customervarinMaterialCost / customermaterialCostPlanned) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerVarinMaterialCostper + "%");
		System.out.println("VarinMaterialCostper:::" + customerVarinMaterialCostper);

		/*
		 * Variance in Operator Cost
		 */
		customerVarinOperatorCost = customeroperatorCostPlanned - customeroperatorCostActual;
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerVarinOperatorCost);

		System.out.println("VarinOperatorCost:::" + customerVarinOperatorCost);

		/*
		 * Variance in Operator Cost %
		 */

		customerVarinOperatorCostper = Math.round((customerVarinOperatorCost / customeroperatorCostPlanned) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(customerVarinOperatorCostper + "%");
		}
		columnCount = 6;
		rowCount++;
		row = sheet.createRow(++rowCount);

		cell = row.createCell(columnCount);
		cell.setCellValue("Grand Total");
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalContractAmount + "");
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalTargetRevenue);

		cell = row.createCell(++columnCount);
		cell.setCellValue(totalbillingAmount);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalmaterialCostPlanned);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalmaterialActualcost);
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(totaloperatorCostPlanned);
		
		
		cell = row.createCell(++columnCount);
		cell.setCellValue(totaloperatorCostActual);
		
		totalCost = totalmaterialActualcost + totaloperatorCostActual;
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalCost);
		
		totaltargetOperatingProfit = totalTargetRevenue - totalmaterialCostPlanned - totaloperatorCostPlanned;
		cell = row.createCell(++columnCount);
		cell.setCellValue(totaltargetOperatingProfit);
		

		finalOperatingProfitper = Math.round((totaltargetOperatingProfit / totalTargetRevenue) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(finalOperatingProfitper + "%");
		System.out.println("totalOperatingProfitper::" + finalOperatingProfitper);

		/*
		 * Actual Operating Profit
		 */

		totalactualOperatingProfit = totalbillingAmount - totalCost;
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalactualOperatingProfit);
		System.out.println("Actual Operating Profit:::" + totalactualOperatingProfit);

		/*
		 * Actual Operating Profit %
		 */

		totalactualOperatingProfitper = Math.round((totalactualOperatingProfit / totalbillingAmount) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalactualOperatingProfitper + "%");

		System.out.println("Actual Operating Profit %:::" + totalactualOperatingProfitper);

		/*
		 * Material Cost as a % of Revenue
		 */
		totalmaterialCostRevenueper = Math.round((totalmaterialActualcost / totalbillingAmount) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalmaterialCostRevenueper + "%");

		System.out.println("materialCostRevenueper %:::" + totalmaterialCostRevenueper);

		/*
		 * Operator Cost as a % of Revenue
		 */

		totaloperatorCostRevenueper = Math.round((totaloperatorCostActual / totalbillingAmount) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(totaloperatorCostRevenueper + "%");

		System.out.println("operatorCostRevenueper" + totaloperatorCostRevenueper);

		/*
		 * Variance in Operating Profit
		 */

		totalvarinOperatingProfit = totaltargetOperatingProfit - totalactualOperatingProfit;
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalvarinOperatingProfit);
		System.out.println("varinOperatingProfit:::" + totalvarinOperatingProfit);

		/*
		 * Variance in Material Cost
		 */
		totalvarinMaterialCost = totalmaterialCostPlanned - totalmaterialActualcost;
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalvarinMaterialCost);
		System.out.println("varinMaterialCost::" + totalvarinMaterialCost);

		/*
		 * Variance in Material Cost %
		 */
		totalVarinMaterialCostper = Math.round((totalvarinMaterialCost / totalmaterialCostPlanned) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalVarinMaterialCostper + "%");
		System.out.println("VarinMaterialCostper:::" + totalVarinMaterialCostper);

		/*
		 * Variance in Operator Cost
		 */
		totalVarinOperatorCost = totaloperatorCostPlanned - totaloperatorCostActual;
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalVarinOperatorCost);

		System.out.println("VarinOperatorCost:::" + totalVarinOperatorCost);

		/*
		 * Variance in Operator Cost %
		 */

		totalVarinOperatorCostper = Math.round((totalVarinOperatorCost / totaloperatorCostPlanned) * 100);
		cell = row.createCell(++columnCount);
		cell.setCellValue(totalVarinOperatorCostper + "%");

	}

	/**
	 * Updated By: Viraj
	 * Date: 13-06-2019
	 * Description: To set from date and to date for displaying in download
	 */
	@Override
	public void setServiceFromToDate(Date fromDate, Date toDate) {
		logger.log(Level.SEVERE, "setServiceFromToDate --"+" FROM DATE : "+fromDate+" TO DATE : "+toDate);
		FromDate = fromDate;
		ToDate = toDate;
		
	}
	/** Ends **/

//@Override
//public void setContractServicePNLReport(ArrayList<MaterialConsumptionReport> reportList,
//		Date fromDate, Date toDate, String branchName, long companyId) {
//	// TODO Auto-generated method stub
//	logger.log(Level.SEVERE, "SETTING MCR SIZE --" + reportList.size()+" FROM DATE : "+fromDate+" TO DATE : "+toDate+" BRANCH : "+branchName+" COMPANY ID : "+companyId);
//	contractPAndLReportList = reportList;
//	FromDate = fromDate;
//	ToDate = toDate;
//	branch = branchName;
//	companyId1 = companyId;
//}
	
	public void getPayrollSheetReport(XSSFWorkbook  workbook){
		
		
		 Company company=ofy().load().type(Company.class).filter("companyId", cnc.getCompanyId()).first().now();
		 byte[] bytes=null;
		 int colNum=2;
		 if(company!=null&&company.getLogo()!=null&&company.getLogo().getUrl()!=null&&!company.getLogo().getUrl().equals("")){
			 bytes=getBolbDataToByteArray(company.getLogo());
			 if(bytes!=null){
				 colNum=1;
			 }
		 }
		
		
		

		try {
			int size = cnc.getSaffingDetailsList().size();
			if(workbook.getNumberOfSheets()>0){
				for(int i= 0 ;i<= workbook.getNumberOfSheets() ;i++){
					workbook.removeSheetAt(i);
			}
			}
			  XSSFSheet sheet = workbook.createSheet("Payroll Sheet Report");
			 
			   Row row = sheet.createRow(rowCount);
				int	columnCount = 0;
				Cell cell;
				
				XSSFCellStyle style43 = workbook.createCellStyle();
				style43.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				style43.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			      
			     XSSFFont font = workbook.createFont();
			     font.setFontHeightInPoints((short) 18);
//			     font.setFontName("Times New Roman");
			     font.setFontName("Calibri");
			     font.setBold(true);
			      
			     style43.setFont(font);
			     style43.setBorderTop(CellStyle.BORDER_THICK);
			     style43.setBorderBottom(CellStyle.BORDER_THICK);
			     style43.setBorderLeft(CellStyle.BORDER_THICK);
			     if(bytes!=null){
			    	 style43.setBorderRight(CellStyle.BORDER_NONE);
			     }else{
			    	 style43.setBorderRight(CellStyle.BORDER_THICK); 
			     }
//			     style43.setBorderRight(CellStyle.BORDER_THIN);
			     String chStr = getChart(0);
			     String chStr1 = getChart(colNum + cnc.getSaffingDetailsList().size());
				 sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+1+":"+chStr1+""+1));
				 cell = row.createCell(columnCount);
				
				 String payrollSheetHeader="";
				 if(cnc.getProjectName()!=null&&!cnc.getProjectName().equals("")){
					 payrollSheetHeader= cnc.getProjectName()+" - Payroll Sheet";
				 }else{
					 payrollSheetHeader= cnc.getProjectName()+"-  Payroll Sheet";
				 }
//				 cell.setCellValue((String) "Sasha Cost Sheet : "+ cnc.getPersonInfo().getFullName()+" Cost Sheet");
				 cell.setCellValue(payrollSheetHeader);
				 cell.setCellStyle(style43);
				
				 if(bytes!=null){
					 int rowCount1=0;
					 int colCount= 2 + cnc.getSaffingDetailsList().size();
					 XSSFCellStyle	style421 = workbook.createCellStyle();
					 style421.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					 style421.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					 style421.setFont(font);
					 style421.setBorderTop(CellStyle.BORDER_THICK);
					 style421.setBorderBottom(CellStyle.BORDER_THICK);
					 style421.setBorderRight(CellStyle.BORDER_THICK);
					 style421.setBorderLeft(CellStyle.BORDER_NONE);
					 cell = row.createCell(colCount);
					 cell.setCellStyle(style421);
					 addCompanyLogoToCell(workbook, sheet, bytes,cell,rowCount1,colCount);
				 }
				 
				 for(int j= 1;j<= (colNum+cnc.getSaffingDetailsList().size());j++){
						try{
							cell = row.createCell(j);
							cell.setCellValue("");
						}catch(Exception e){
							cell = row.getCell(j);
						}		
						cell.setCellStyle(style43);
					}
				 getPayrollSheetHeader(sheet,sheet.createRow(++rowCount),workbook);
				 getPayrollSheetVerticalHeader(sheet, rowCount, workbook);
				 getPayrollSheetValues(sheet, rowCount,  workbook);
			

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
		logger.log(Level.SEVERE, " get error in Payroll  sheet print --"+e.getMessage());
	}
		
		
	}
	private void getPayrollSheetHeader(XSSFSheet sheet, Row row,XSSFWorkbook  workbook){


		int columnCount = 0;
		Cell cell;

		XSSFCellStyle	style95 = workbook.createCellStyle();
		style95.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style95.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

		XSSFFont font10 = workbook.createFont();
		font10.setFontHeightInPoints((short) 12);
//		font.setFontName("Times New Roman");
		
		font10.setBold(true);
		style95.setFont(font10);  
		
		style95.setBorderTop(CellStyle.BORDER_THICK);
		style95.setBorderBottom(CellStyle.BORDER_THICK);
		style95.setBorderLeft(CellStyle.BORDER_THICK);
		style95.setBorderRight(CellStyle.BORDER_THICK);
	
		cell = row.createCell(columnCount);
		cell.setCellValue((String) "Sr No.");
		cell.setCellStyle(style95);
		
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Description");
		cell.setCellStyle(style95);
		
		for(StaffingDetails staff : cnc.getSaffingDetailsList()){
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) staff.getDesignation());
			cell.setCellStyle(style95);
		}
		cell = row.createCell(++columnCount);
		cell.setCellValue((String) "Total");
		cell.setCellStyle(style95);
		
	
	}
	private void getPayrollSheetVerticalHeader(XSSFSheet sheet, int rowCount,XSSFWorkbook  workbook) {

		ArrayList<String> list = getVerticalHeader();
		int columnCount = 1;
		Cell cell;	
		 style2 = workbook.createCellStyle();
	     style2.setAlignment(XSSFCellStyle.ALIGN_LEFT);
	     style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	    
	     XSSFFont font = workbook.createFont();
	     font.setFontHeightInPoints((short) 12);
//	     font.setFontName("Times New Roman");
	     font.setFontName("Calibri");
	     font.setBold(true);;
	     
	     style2.setFont(font);
	     style2.setBorderTop(CellStyle.BORDER_THICK);
	     style2.setBorderBottom(CellStyle.BORDER_THICK);
	     style2.setBorderLeft(CellStyle.BORDER_THICK);
	     style2.setBorderRight(CellStyle.BORDER_THICK);
	     
	     XSSFCellStyle style1 = workbook.createCellStyle();
	     style1.setAlignment(XSSFCellStyle.ALIGN_LEFT);
	     style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	     XSSFFont font1 = workbook.createFont();
	     font1.setFontHeightInPoints((short) 12);
//	     font1.setFontName("Times New Roman");
	     font1.setFontName("Calibri");
	     style1.setFont(font1);
	     
	     style1.setBorderTop(CellStyle.BORDER_THIN);
	     style1.setBorderBottom(CellStyle.BORDER_THIN);
	     style1.setBorderLeft(CellStyle.BORDER_THICK);
	     style1.setBorderRight(CellStyle.BORDER_THICK);
	   // style1.setWrapText(true);
	     Row row;
		for(String s : list){
			row = sheet.createRow(++rowCount);
			if(s.contains("//")){
				String array[] = s.split("//");
				cell = row.createCell(columnCount);
				cell.setCellValue((String) array[1]);
				cell.setCellStyle(style2);
				
				cell = row.createCell(columnCount-1);
				cell.setCellValue((String) array[0]);
				cell.setCellStyle(style2);

			}else if(s.contains("#")){
				String array[] = s.split("#");
				cell = row.createCell(columnCount);
				cell.setCellValue((String) array[1]);
				cell.setCellStyle(style1);
				
				cell = row.createCell(columnCount-1);
				cell.setCellValue((String) array[0]);
				if(array[0].contains("c")){
					cell.setCellStyle(style1);
				}else{
					cell.setCellStyle(style2);
				}

			}else if(s.contains("/")){
				cell = row.createCell(columnCount);
				cell.setCellValue((String) s.substring(1, s.length()));
				cell.setCellStyle(style2);
				
				cell = row.createCell(columnCount-1);
				cell.setCellValue("");
				cell.setCellStyle(style2);
			}else{
				cell = row.createCell(columnCount);
				cell.setCellValue((String) s);
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount-1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			}
		}
		XSSFCellStyle style3 = workbook.createCellStyle();
	    style3.setAlignment(XSSFCellStyle.ALIGN_LEFT);
	    style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	    XSSFFont font3 = workbook.createFont();
	    font3.setFontHeightInPoints((short) 12);
//	    font3.setFontName("Times New Roman");
	    font3.setFontName("Calibri");
	    style3.setFont(font3);
	    style3.setWrapText(true);
	    style3.setBorderTop(CellStyle.BORDER_THIN);
	    style3.setBorderBottom(CellStyle.BORDER_THIN);
	    style3.setBorderLeft(CellStyle.BORDER_THICK);
	    style3.setBorderRight(CellStyle.BORDER_THICK);
		String value = cnc.getTermsAndConditions().trim();
		String [] list1 = value.split("\n");
//		for(String s : list1){
//			if(s.length() >=3){
//			row = sheet.createRow(++rowCount);
//				cell = row.createCell(columnCount);
//				String str = s.substring(2 , s.length());
//				
//				if(str.contains("&Totalcostinword")){
//					str = str.replace("&Totalcostinword", PdfUpdated.convert(cnc.getNetPayableOFCNC())+"");
//				}
//				if(str.contains("&Totalcost")){
//					str = str.replace("&Totalcost", roundOff(cnc.getNetPayableOFCNC()));
//				}
//				cell.setCellValue((String) str);
//				cell.setCellStyle(style3);
//				
//				String chStr = getChart(1);
//			    String chStr1 = getChart(2 + cnc.getSaffingDetailsList().size());
//				sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+(1+rowCount)+":"+chStr1+""+(1+rowCount)));
//				cell = row.createCell(columnCount-1);
//				cell.setCellValue((String) "*");
//				cell.setCellStyle(style3);
//				
//				for(int j= 2;j<= (2+cnc.getSaffingDetailsList().size());j++){
//					try{
//						cell = row.createCell(j);
//						cell.setCellValue("");
//					}catch(Exception e){
//						cell = row.getCell(j);
//					}		
//					cell.setCellStyle(style3);
//				}
//
//				}
//			}

		
	}
	private void getPayrollSheetValues( XSSFSheet sheet, int rowc,XSSFWorkbook  workbook){

		ArrayList<String> list = getVerticalHeader();
		
		int columnCount = 2;
		Cell cell;	
		XSSFCellStyle style1 = workbook.createCellStyle();
	     style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	     style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	     XSSFFont font1 = workbook.createFont();
	     font1.setFontHeightInPoints((short) 12);
//	     font1.setFontName("Times New Roman");
	     font1.setFontName("Calibri");
	     
	     style1.setFont(font1);
	    // style1.setWrapText(true);
	     style1.setBorderTop(CellStyle.BORDER_THIN);
	     style1.setBorderBottom(CellStyle.BORDER_THIN);
	     style1.setBorderLeft(CellStyle.BORDER_THICK);
	     style1.setBorderRight(CellStyle.BORDER_THICK);
	     
	     XSSFCellStyle style3 = workbook.createCellStyle();
	     style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	     style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);

		 XSSFFont font = workbook.createFont();
		 font.setFontHeightInPoints((short) 12);
//		 font.setFontName("Times New Roman");
		 font.setFontName("Calibri");
		 font.setBold(true);
		 style3.setFont(font); 
		 
		 style3.setBorderTop(CellStyle.BORDER_THICK);
	     style3.setBorderBottom(CellStyle.BORDER_THICK);
	     style3.setBorderLeft(CellStyle.BORDER_THICK);
	     style3.setBorderRight(CellStyle.BORDER_THICK);
		 
	     Row row;
	     int finalRowCont=0;
	     int totalEmployee = 0;
	     int column = 2+cnc.getSaffingDetailsList().size();
	     System.out.println(cnc.getSaffingDetailsList().size()+"saffingdetailslist");
	     for(StaffingDetails staff : cnc.getSaffingDetailsList()){
	    	 	int rowCount = rowc;
	    	 	row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getGrossSalary()+""));
				cell.setCellStyle(style3);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getBasic()+""));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getDa()));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getHra()));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getConveyance()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getMedical()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getWashingAllowance()+""));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getOthers()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			//	++rowCount;
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSatutoryDeductions()));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSdPF()));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSdESIC()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSdPT()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSdLWF()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				/**DAte 6-12-2019 by Amol**/
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(staff.getPlEsic());
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(staff.getBonusEsic());
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				
				
				
				
				
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue("");
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
//				++rowCount;

				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getNetSalary()+""));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getcBonus()));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getcLeave()));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getTravellingAllowance()+""));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getTakeHome()+""));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			//	++rowCount;
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue("");
				cell.setCellStyle(style3);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				double cBonusPercent=staff.getcBonusPercentage();
				double bonusValue=0;
				bonusValue = ((staff.getBasic() + Double
						.parseDouble(staff.getDa())) * (cBonusPercent / 100));// +(1.75/30);
				
				double statuetoryTotalAmount=((Double.parseDouble(staff.getSpPF()))+(Double.parseDouble(staff.getSpESIC()))+(Double
										.parseDouble(staff.getSpEDLI()))+(Double
												.parseDouble(staff.getSpMWF()))+bonusValue+(staff.getcPlEsic())+(staff.getcBonusEsic()));
				
				
				
				
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(statuetoryTotalAmount);
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSpPF()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSpESIC()));
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSpEDLI()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getSpMWF()));
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
//				double paiddaysforBonus=Double.parseDouble(staff.getPaidLeaveDays().trim());
//				double cBonusPercent = 0;
//				cBonusPercent = staff.getcBonusPercentage();//Double.parseDouble(form.getTbcBonus().getValue().trim());
////				Console.log("cBonusPercent" + cBonusPercent);
//				
//				double bonusValue = 0;
//				if (staff.isBonusRelever()) {
//					bonusValue = ((staff.getBasic() + Double
//							.parseDouble(staff.getDa())) * (cBonusPercent / 100))
//							+ (((staff.getBasic() + Double
//									.parseDouble(staff.getDa())) * (cBonusPercent / 100)) * ((paiddaysforBonus/12) / 30));
//				} else {
//					bonusValue = ((staff.getBasic() + Double
//							.parseDouble(staff.getDa())) * (cBonusPercent / 100));// +(1.75/30);
//				}
////				Console.log("bonusValue" + bonusValue);
//				if(cBonusPercent!=0){
//					staff.setSpBonus((Math.round(bonusValue*100.0)/100.0) + "");
//				}
				
				
				
				
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(bonusValue);
				cell.setCellStyle(style1);	
				
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				/**Date 6-12-2019 by Amol**/
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(staff.getcPlEsic());
				cell.setCellStyle(style1);	
				
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue(staff.getcBonusEsic());
				cell.setCellStyle(style1);	
				
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
			//	++rowCount;
			
		//		++rowCount;
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				double grossPlusStatutoryPayment=(staff.getGrossSalary()+statuetoryTotalAmount);
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getTotal()));
				cell.setCellValue((String) (grossPlusStatutoryPayment+""));
				cell.setCellStyle(style3);
				
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style3);
				
				double paidLeavesValue = 0;
//				double paiddays=Double.parseDouble(staff.getPaidLeaveDays().trim());
//				double totalValue = Double.parseDouble(staff.getSpEDLI())
//						+ Double.parseDouble(staff.getSpESIC())
//						+ Double.parseDouble(staff.getSpMWF())
//						+ Double.parseDouble(staff.getSpPF());
//				totalValue = Math.round(totalValue*100.0)/100.0;
//				if (staff.isPaidLeaveRelever()) {
//					paidLeavesValue = ((staff.getGrossSalary() + (totalValue)) * ((paiddays/12)/30))
//															+
//							(((staff.getGrossSalary() + (totalValue)))*((paiddays/12)/30))*( (paiddays/12)/ 30);
//				} else {
//					paidLeavesValue = (staff.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
//				}
////				Console.log("paidLeavesValue" + paidLeavesValue);
//
//				staff.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
//				
				double paiddays=0;
				try{
					 paiddays=Double.parseDouble(staff.getPaidLeaveDays().trim());
				}catch(Exception  e){
					 paiddays=0;
				}
//				double paiddays=Double.parseDouble(staff.getPaidLeaveDays().trim());
				double totalValue = Double.parseDouble(staff.getSpEDLI())
						+ Double.parseDouble(staff.getSpESIC())
						+ Double.parseDouble(staff.getSpMWF())
						+ Double.parseDouble(staff.getSpPF());
				
				paidLeavesValue = (staff.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
				
				staff.setPaidLeaves((Math.round(paidLeavesValue*100.0)/100.0) + "");
				
				/**Date 18-6-2019
				 * as per discussion with Maradona Paidleave formula changes raised by Sonu
				 * @author AMOL 
				 * */
				
				double paidleave=((staff.getBasic()+Double.parseDouble(staff.getDa()))/30)*(Double.parseDouble(staff.getPaidLeaveDays())/12);
				
				
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
//				cell.setCellValue(paidLeavesValue);
				cell.setCellValue(paidleave);
				cell.setCellStyle(style1);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				
//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getEmployementOverHeadCost()+""));
//				cell.setCellStyle(style1);	
//				cell = row.createCell(columnCount+1);
//				cell.setCellValue("");
//				cell.setCellStyle(style3);
				
//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getTravellingAllowanceOverhead()+""));
//				cell.setCellStyle(style1);	
//				cell = row.createCell(columnCount+1);
//				cell.setCellValue("");
//				cell.setCellStyle(style3);
				
//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getAdditionalAllowance()+""));
//				cell.setCellStyle(style1);	
//				cell = row.createCell(columnCount+1);
//				cell.setCellValue("");
//				cell.setCellStyle(style3);
//			//	++rowCount;
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
				
				double cPlusC1=(grossPlusStatutoryPayment+paidleave);
				
				
				
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getTotalOfPaidAndOverhead()));
				cell.setCellValue((String) (cPlusC1+""));
				cell.setCellStyle(style3);	
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style3);
//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getManagementFees()+""));
//				cell.setCellStyle(style1);
//				if(columnCount == column-1){
//					cell = row.createCell(column);
//					cell.setCellValue((String) (cnc.getManagementFees()+""));
//					cell.setCellStyle(style1);
	//
//				}
//				double value1=0 , value2=0;
//				if(staff.getTotalOfPaidAndOverhead()!=null){
//					value1 = Double.parseDouble(staff.getTotalOfPaidAndOverhead());
//				}
//				if(staff.getManagementFees()!=null){
//					try{
//						value2 = Math.round((Double.parseDouble(staff.getManagementFees()) / staff.getNoOfStaff())*100.0)/100.0;
//					}catch(Exception e){
//						value2 = 0;
//					}
//				}
//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (value2+""));
//				cell.setCellStyle(style3);
//				cell = row.createCell(columnCount+1);
//				cell.setCellValue("");
//				cell.setCellStyle(style3);
//
//				double value = Math.round((value1+ value2)*100.0)/100.0 ;
//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (value+""));
//				cell.setCellStyle(style3);	
//				cell = row.createCell(columnCount+1);
//				cell.setCellValue("");
//				cell.setCellStyle(style3);
						
				row = sheet.getRow(++rowCount);
				cell = row.createCell(columnCount);
				cell.setCellValue((String) (staff.getNoOfStaff()+""));
				cell.setCellStyle(style3);
				cell = row.createCell(columnCount+1);
				cell.setCellValue("");
				cell.setCellStyle(style3);
				
				totalEmployee = totalEmployee + staff.getNoOfStaff();
				if(columnCount == column-1){
					cell = row.createCell(column);
					cell.setCellValue((String) (totalEmployee+""));
					cell.setCellStyle(style3);

				}


//				row = sheet.getRow(++rowCount);
//				cell = row.createCell(columnCount);
//				cell.setCellValue((String) (staff.getTotalIncludingManagementFees()));
//				cell.setCellStyle(style3);	
//				cell = row.createCell(columnCount+1);
//				cell.setCellValue("");
//				cell.setCellStyle(style3);
				
//				if(columnCount == column-1){
//					cell = row.createCell(column);
//					cell.setCellValue((String) (cnc.getNetPayableOFStaffing()+""));
//					cell.setCellStyle(style1);
	//
//				}
				

//			finalRowCont = rowCount;
//		    columnCount++; 
//	     }
//	 	row = sheet.getRow(++finalRowCont);
//	 	String chStr = getChart(2);
//	    String chStr1 = getChart(2+cnc.getSaffingDetailsList().size()-1);
//		sheet.addMergedRegion(CellRangeAddress.valueOf(chStr+""+(finalRowCont+1)+":"+chStr1+""+(finalRowCont+1)));
//		System.out.println("val" + chStr+""+finalRowCont+":"+chStr1+""+finalRowCont);
//		cell = row.createCell(column-1);
//		cell.setCellValue((String) (""));//cnc.getNetPayableOFConsumables()+
//		cell.setCellStyle(style3);
//		cell = row.createCell(column);
//		
//		/** date 23/8/2018 added by komal**/
//		if(cnc.getNetPayableOFConsumables()!=0){
//			cell.setCellValue((String) (cnc.getNetPayableOFConsumables()+""));
//		}else{
//			cell.setCellValue("At Actuals");
//		}
//		cell.setCellStyle(style1);

		
//		row = sheet.getRow(++finalRowCont);
//	 	String chStr2 = getChart(2);
//	    String chStr3 = getChart(2+cnc.getSaffingDetailsList().size()-1);
//		sheet.addMergedRegion(CellRangeAddress.valueOf(chStr2+""+(finalRowCont+1)+":"+chStr3+""+(finalRowCont+1)));
//		cell = row.createCell(column-1);
//		cell.setCellValue((String) (""));//cnc.getNetPayableOFEquipment()+
//		cell.setCellStyle(style1);
//		cell = row.createCell(column);
//		cell.setCellValue((String) (cnc.getNetPayableOFEquipment()+""));
//		cell.setCellStyle(style1);
//		
//		for(int j= 2;j< (2+cnc.getSaffingDetailsList().size());j++){
//			try{
//				cell = row.createCell(j);
//				cell.setCellValue("");
//			}catch(Exception e){
//				cell = row.getCell(j);
//			}		
//			cell.setCellStyle(style3);
//		}

//		/** date 25.8.2018 added by komal **/
//		row = sheet.getRow(++finalRowCont);
//	 	String chStr6 = getChart(2);
//	    String chStr7 = getChart(2+cnc.getSaffingDetailsList().size()-1);
//		sheet.addMergedRegion(CellRangeAddress.valueOf(chStr6+""+(finalRowCont+1)+":"+chStr7+""+(finalRowCont+1)));
//		cell = row.createCell(column-1);
//		cell.setCellValue((String) (""));//cnc.getNetPayableOFEquipment()+
//		cell.setCellStyle(style1);
//		cell = row.createCell(column);
//		cell.setCellValue((String) (cnc.getManagementFeesValue()+""));
//		cell.setCellStyle(style1);

//		if(cnc.getOtherServicesList().size()>0){
//			int i =0;
//			String s[] = new String[cnc.getOtherServicesList().size()*2];
//			for(OtherServices os : cnc.getOtherServicesList()){
//				row = sheet.getRow(++finalRowCont);
//				s[i] = getChart(2);
//			    s[i+1] = getChart(2+cnc.getSaffingDetailsList().size()-1);
//				sheet.addMergedRegion(CellRangeAddress.valueOf(s[i]+""+(finalRowCont+1)+":"+s[i+1]+""+(finalRowCont+1)));
//				cell = row.createCell(column-1);
//				cell.setCellValue((String) (""));//cnc.getNetPayableOFEquipment()+
//				cell.setCellStyle(style1);
//				cell = row.createCell(column);
//				cell.setCellValue((String) (os.getChargableRates()+""));
//				cell.setCellStyle(style1);
//				for(int j= 2;j< (2+cnc.getSaffingDetailsList().size());j++){
//					try{
//						cell = row.createCell(j);
//						cell.setCellValue("");
//					}catch(Exception e){
//						cell = row.getCell(j);
//					}		
//					cell.setCellStyle(style3);
//				}
//				i+= 2;
//			}
//		}
//		row = sheet.getRow(++finalRowCont);
//	 	String chStr4 = getChart(2);
//	    String chStr5 = getChart(2+cnc.getSaffingDetailsList().size()-1);
//		sheet.addMergedRegion(CellRangeAddress.valueOf(chStr4+""+(finalRowCont+1)+":"+chStr5+""+(finalRowCont+1)));
//		cell = row.createCell(column-1);
//		cell.setCellValue((String) (""));//cnc.getNetPayableOFCNC()+
//		cell.setCellStyle(style1);
//		cell = row.createCell(column);
//		cell.setCellValue((String) (roundOff(cnc.getNetPayableOFCNC())));
//		cell.setCellStyle(style3);
//		for(int j= 2;j< (2+cnc.getSaffingDetailsList().size());j++){
//			try{
//				cell = row.createCell(j);
//				cell.setCellValue("");
//			}catch(Exception e){
//				cell = row.getCell(j);
//			}		
//			cell.setCellStyle(style3);
//		}
				finalRowCont = rowCount;
			    columnCount++; 

	     }
	     
	     
	     
	    	  
	   



	}
	
	/****31-1-2019 added by amol for payroll sheet******/
	private ArrayList<String>getVerticalHeader(){

		 boolean flag = false;
		Branch branch = ofy().load().type(Branch.class).filter("companyId" , cnc.getCompanyId()).filter("buisnessUnitName", cnc.getBranch()).first().now();
		if(branch != null){
			if(branch.getAddress().getState() !=null && !branch.getAddress().getState().equals("")){
				if(branch.getAddress().getState().equalsIgnoreCase("Maharashtra")){
					flag = true;
				}
			}
		}
		ArrayList<String> list= new ArrayList<String>();
		list.add("A//Monthly Payments");
		list.add("Basic");
		list.add("D.A.");
		list.add("House Rent Allowance");
		list.add("Conveyance");
		list.add("Medical");
		list.add("Washing Allowances");
		list.add("Other Allowances");
		list.add("");
		list.add("/Statutory Deductions");
		list.add("PF");
		list.add("ESIC");
		list.add("PT");
		/** DATE 12.10.2018 added by komal**/
		if(flag){
			list.add("MWF");
		}else{
			list.add("LWF");
		}
		/**Date 6-12-2019 by AMol**/
		list.add("PL Esic");
		list.add("Bonus Esic");
		
		
//		list.add("Other Allowances");
		list.add("");
		list.add("/Net Salary");
		list.add("C Bonus");
		list.add("C Leaves");
		list.add("Travelling Allowance");
		list.add("/Take Home");
		list.add("");
		list.add("B//Statutory Payments");
		list.add("PF");
		list.add("ESIC");
		list.add("EDLI");
		/** DATE 12.10.2018 added by komal**/
		if(flag){
			list.add("CMWF");
		}else{
			list.add("CLWF");
		}
		list.add("Bonus");
		/**Date 6-12-2019 by Amol**/
		list.add("CPL Esic");
		list.add("CBonus Esic");
		list.add("");
		
		
		list.add("C//Total (A+B)");
		list.add("c1#P Leave");
		list.add("");
		list.add("D//Total (C+c1)");
		list.add("E//No. of Employees Required");
		
//		list.add("K#Management fees");
//		String str = "(H+I+J";
//		int i =1;
//		if(cnc.getOtherServicesList().size()>0){
//			for(OtherServices os : cnc.getOtherServicesList()){
//				char first = (char)(75+i);
//				list.add(first+"#"+os.getOtherService());
//				str += "+"+first;
//				i++;
//			}
//		}
//		list.add((char)(75+i)+"//Total "+str+")");

		return list;

		
		
	}


	@Override
	public void setPayrollSheetData(CNC cnc1) {
		// TODO Auto-generated method stub
		this.cnc1 = cnc1;
	}

	
	
	
public byte[] getBolbDataToByteArray(DocumentUpload uploadedData) {
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		String urlData = "";
		String fileName = "";
		byte[] arrayBytes = null;
		if (uploadedData != null&& uploadedData.getUrl() != null) {
			urlData = uploadedData.getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);
			fileName = uploadedData.getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);
			String[] res = urlData.split("blob-key=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);
			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "+ blob);
			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,							inxEnd);
					byteStream.write(bytes);
					if (bytes.length < 1024){
						flag = true;
					}
					inxStart = inxEnd + 1;
					inxEnd += 1025;
				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
		}
		arrayBytes=byteStream.toByteArray();
		logger.log(Level.SEVERE, " ARRAY BYTE : "+arrayBytes);
		return arrayBytes;
	}
	
	private void addCompanyLogoToCell(XSSFWorkbook workbook,XSSFSheet sheet, byte[] bytes, Cell cell, int rowCount, int colCount) throws IOException {
		// TODO Auto-generated method stub

		System.out.println("ROW : "+rowCount+" COL : "+colCount);
//		 Sheet sheet3 = workbook.createSheet("My Sample Excel");
		  
		 //FileInputStream obtains input bytes from the image file
//		   InputStream inputStream = new FileInputStream("/home/axel/Bilder/Wasserlilien.jpg");
//		   InputStream inputStream = new FileInputStream("images/NBHC_Header.jpg");
		   
		 //Get the contents of an InputStream as a byte[].
//		   byte[] bytes = IOUtils.toByteArray(inputStream);
		  
		 //Adds a picture to the workbook
		   int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
		  
		   //close the input stream
//		   inputStream.close();
		   //Returns an object that handles instantiating concrete classes
		   CreationHelper helper = workbook.getCreationHelper();
		   //Creates the top-level drawing patriarch.
		   Drawing drawing = sheet.createDrawingPatriarch();

		   //Create an anchor that is attached to the worksheet
		   ClientAnchor anchor = helper.createClientAnchor();

		   //create an anchor with upper left cell _and_ bottom right cell
//		   anchor.setCol1(1); //Column B
//		   anchor.setRow1(2); //Row 3
//		   anchor.setCol2(2); //Column C
//		   anchor.setRow2(3); //Row 4
		   
		   anchor.setCol1(colCount); //Column A
		   anchor.setRow1(rowCount); //Row 0
		   anchor.setCol2(colCount+1); //Column B
		   anchor.setRow2(rowCount+1); //Row 1

		   //Creates a picture
		   Picture pict = drawing.createPicture(anchor, pictureIdx);
		   
		   //Reset the image to the original size
//		   pict.resize(); //don't do that. Let the anchor resize the image!
		   
		   //Create the Cell B3
//		   cell = sheet3.createRow(2).createCell(1);
//		   cell = sheet.createRow(rowCount).createCell(colCount);

//		   set width to n character widths = count characters * 256
		   int widthUnits = 20*320;
		   sheet.setColumnWidth(colCount, widthUnits);

//		   set height to n points in twips = n * 20
		   short heightUnits = 40*20;
		   cell.getRow().setHeight(heightUnits);
//		 }
		///
	}


	public void getGPSheetReport(XSSFWorkbook workbook) {
		// TODO Auto-generated method stub
		try{
			totalPayExcludingManagementFees=0;
			totalPayWithoutRelieverFormula=0;
			totalManagementFees=0;
			totalOfActualEquipmentCost=0;
			rowCount=0;
			if (workbook.getNumberOfSheets() > 0) {
				for (int i = 0; i <= workbook.getNumberOfSheets(); i++) {
					workbook.removeSheetAt(i);
				}
			}
			XSSFSheet sheet = workbook.createSheet("GP Sheet");
			
			XSSFFont font8=setFontProperty(workbook, (short)8, "Calibri", false);
			XSSFFont font8bold=setFontProperty(workbook, (short)8, "Calibri", true);
			
			XSSFCellStyle s8_nb=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
			XSSFCellStyle s8B_nb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
			XSSFCellStyle s8B_tb_bb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, (short)-1, (short)-1);
			
			for(StaffingDetails staff:cnc.getSaffingDetailsList()){
				totalPayWithoutRelieverFormula+=staff.getActualPayementExcludingOverhead();
			}
			
			for(OtherServices othServies:cnc.getOtherServicesList()){
				totalOtherServices+=othServies.getVendorRates();
			}
			
			getGpSheetSummary(workbook,sheet);
			getCostSheetSummary(workbook,sheet);
			getEquipmentSheetSummary(workbook,sheet);
			rowCount=0;
			getGpSheetSummary(workbook,sheet);
			
			
			
		}catch(Exception e){
			
		}
		
	}

	public XSSFFont setFontProperty(XSSFWorkbook workbook,short fontHeight,String fontName,boolean isBold){
		 XSSFFont font = workbook.createFont();
	     font.setFontHeightInPoints(fontHeight);
	     font.setFontName(fontName);
	     font.setBold(isBold);
	     return font;
	}
		
	public XSSFCellStyle setCellStyleProperty(XSSFWorkbook workbook,XSSFFont font,short alignment,short verticalAlignment,short cellTopBorder,short cellBottomBorder,short cellLeftBorder,short cellRightBorder){
		XSSFCellStyle style = workbook.createCellStyle();
//		style.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
//		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		
		style.setAlignment(alignment);
		style.setVerticalAlignment(verticalAlignment);
		
//		XSSFFont font1 = workbook.createFont();
//		font1.setFontHeightInPoints((short) 12);
//		font1.setFontName("Calibri");
		
		style.setFont(font);
		
//		style.setWrapText(true);
//		style.setBorderTop(CellStyle.BORDER_THIN);
//		style.setBorderBottom(CellStyle.BORDER_THIN);
//		style.setBorderLeft(CellStyle.BORDER_THICK);
//		style.setBorderRight(CellStyle.BORDER_THICK);
		
		if(cellTopBorder!=-1){
			style.setBorderTop(cellTopBorder);
		}
		if(cellBottomBorder!=-1){
			style.setBorderBottom(cellBottomBorder);
		}
		if(cellLeftBorder!=-1){
			style.setBorderLeft(cellLeftBorder);
		}
		if(cellRightBorder!=-1){
			style.setBorderRight(cellRightBorder);
		}
		
		return style;
	}
	
	public Cell createCell(Row row,XSSFCellStyle style,int colNo,String colValue){
		Cell cell = row.createCell(colNo);
		cell.setCellValue(colValue);
		cell.setCellStyle(style);
		return cell;
	}
	
	public void getGpSheetSummary(XSSFWorkbook workbook, XSSFSheet sheet){
		double adminOhCost=0;
		double uniformCost=0;
		double grossProfit=0;
		double totalDirectCost=0;
		
		XSSFFont font8=setFontProperty(workbook, (short)12, "Calibri", false);
		XSSFFont font8bold=setFontProperty(workbook, (short)12, "Calibri", true);
		
		XSSFCellStyle s8_nb=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
		XSSFCellStyle s8B_nb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
		XSSFCellStyle s8B_tb_bb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, (short)-1, (short)-1);
		
		XSSFCellStyle s8_r_nb=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
		XSSFCellStyle s8B_r_nb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
		XSSFCellStyle s8B_r_tb_bb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, (short)-1, (short)-1);
		
		
		Row row = sheet.createRow(rowCount);//row1
		int	columnCount = 0;
		Cell cell;
		cell=createCell(row, s8_nb, columnCount, "");
		cell=createCell(row, s8B_nb, ++columnCount, cnc.getProjectName()+"");
		cell=createCell(row, s8_nb, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row2
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "Date Cost Sheet Prepared:");
		cell=createCell(row, s8B_tb_bb, ++columnCount, fmt.format(cnc.getCreationDate())+"");
		cell=createCell(row, s8B_tb_bb, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row3
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "All amounts are monthly, in INR");
		
		row = sheet.createRow(++rowCount);//row4
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "TOTAL SALES PER MONTH");
		cell=createCell(row, s8B_nb, ++columnCount, " ");
		cell=createCell(row, s8B_nb, ++columnCount, " ");
		cell=createCell(row, s8B_r_nb, ++columnCount, roundOff(cnc.getNetPayableOFCNC()));
		
		
		row = sheet.createRow(++rowCount);//row5
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "");
		cell=createCell(row, s8B_nb, ++columnCount, " ");
		cell=createCell(row, s8B_nb, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row6
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Wages(actually paid)");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, roundOff(totalPayWithoutRelieverFormula));
		
		/**Date 30-4-2020 by Amol**/
		row = sheet.createRow(++rowCount);//row6
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Other Services");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, roundOff(totalOtherServices));
		
		row = sheet.createRow(++rowCount);//row7
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Consumable");
		cell=createCell(row, s8_nb, ++columnCount, "");
		if(cnc.getNetPayableOFConsumables()!=0){
			cell=createCell(row, s8_r_nb, ++columnCount, roundOff(cnc.getNetPayableOFConsumables()));
		}else{
			cell=createCell(row, s8_nb, ++columnCount, "");
		}
		
		row = sheet.createRow(++rowCount);//row8
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Travelling Allowance");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row9
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Additional Hours");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, "");
		
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8B_r_nb, ++columnCount, "Rs.");
		cell=createCell(row, s8B_nb, ++columnCount, "UNIFORM INCLUDES");
		
		row = sheet.createRow(++rowCount);//row10
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Equipment Cost");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, roundOff(totalEquipmentCost));
		
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, "2400");
		cell=createCell(row, s8_nb, ++columnCount, "Shirt+Pant @ 800*3");
		
		row = sheet.createRow(++rowCount);//row11
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Equipment Repair");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, roundOff(totalEquipmentRepair));
		
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, "450");
		cell=createCell(row, s8_nb, ++columnCount, "Shoes");
		
		double noOfStaff=0;
		for(StaffingDetails obj:cnc.getSaffingDetailsList()){
			noOfStaff+=obj.getNoOfStaff();
		}
		adminOhCost=cnc.getAdminOverheadCost()*noOfStaff;
		uniformCost=(cnc.getApproxUniformCost()/12)*noOfStaff;
		row = sheet.createRow(++rowCount);//row12
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "OH Cost    (Admin Back Office Cost)");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, roundOff(adminOhCost));
		
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, "50");
		cell=createCell(row, s8_nb, ++columnCount, "Cap");
		
		row = sheet.createRow(++rowCount);//row13
		columnCount = 0;
		cell=createCell(row, s8_nb, columnCount, "Uniform    (Approx)");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, roundOff(uniformCost));
		
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_r_nb, ++columnCount, "300");
		cell=createCell(row, s8_nb, ++columnCount, "ID + Kit Bag");
		
		totalDirectCost=totalPayWithoutRelieverFormula+totalEquipmentCost+totalEquipmentRepair+adminOhCost+uniformCost+totalOtherServices;
		grossProfit=cnc.getNetPayableOFCNC()-totalDirectCost;
		
		row = sheet.createRow(++rowCount);//row14
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "TOTAL DIRECT COST");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		logger.log(Level.SEVERE, "total direct cost "+totalDirectCost);
		cell=createCell(row, s8B_r_nb, ++columnCount,roundOff(totalDirectCost));
		
		row = sheet.createRow(++rowCount);//row15
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		
		
		row = sheet.createRow(++rowCount);//row15
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "GROSS PROFIT");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8B_r_tb_bb, ++columnCount, roundOff(grossProfit));
		
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8_nb, ++columnCount, "");
		cell=createCell(row, s8B_r_nb, ++columnCount, roundOff((grossProfit/cnc.getNetPayableOFCNC())*100)+"%");
		
		row = sheet.createRow(++rowCount);//row15
	}
	
	public void getCostSheetSummary(XSSFWorkbook workbook, XSSFSheet sheet){
		Row row = sheet.createRow(++rowCount);
		getCostSheetHeader(sheet, sheet.createRow(rowCount), workbook);
//		getCostSheetVerticalHeader(sheet, rowCount, workbook,false);
		getCostSheetSummaryVerticalHeader(sheet, rowCount, workbook);
		getCostSheetSummaryVerticalValues(sheet, rowCount, workbook);
		rowCount=rowCount+getAllGPCostVerticalHeader().size()+1;
	}
	
	private void getEquipmentSheetSummary(XSSFWorkbook workbook, XSSFSheet sheet) {
		XSSFFont font8=setFontProperty(workbook, (short)12, "Calibri", false);
		XSSFFont font8bold=setFontProperty(workbook, (short)12, "Calibri", true);
		
		XSSFCellStyle s8B_nb=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, (short)-1, (short)-1, (short)-1, (short)-1);
		
		XSSFCellStyle s8B_c_tbtk_bbtk_lbtn_rbtk=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
		XSSFCellStyle s8B_c_tbtk_bbtk_lbtk_rbtn=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		XSSFCellStyle s8B_c_tbtk_bbtk_lbtn_rbtn=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN);
		
		XSSFCellStyle s8B_r_tbtk_bbtk_lbtn_rbtk=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
		
		XSSFCellStyle s8_l_tbtn_bbtn_lbtn_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_l_tbtn_bbtn_lbtk_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_l_tbtn_bbtn_lbtn_rbtk=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
		
		XSSFCellStyle s8B_l_tbtn_bbtn_lbtk_rbtn=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		
		
		XSSFCellStyle s8_c_tbtn_bbtn_lbtn_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_c_tbtn_bbtn_lbtk_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_c_tbtn_bbtn_lbtn_rbtk=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
		
		XSSFCellStyle s8B_c_tbtn_bbtn_lbtk_rbtn=setCellStyleProperty(workbook, font8bold, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		
		XSSFCellStyle s8_r_tbtn_bbtn_lbtn_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_r_tbtn_bbtn_lbtn_rbtk=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
		
		
		XSSFCellStyle s8_l_tbtn_bbtk_lbtn_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_l_tbtn_bbtk_lbtk_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_l_tbtn_bbtk_lbtn_rbtk=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
	
		XSSFCellStyle s8_r_tbtn_bbtk_lbtn_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_r_tbtn_bbtk_lbtk_rbtn=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN);
		XSSFCellStyle s8_r_tbtn_bbtk_lbtn_rbtk=setCellStyleProperty(workbook, font8, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK, CellStyle.BORDER_THIN, CellStyle.BORDER_THICK);
	
		
		Row row = sheet.createRow(++rowCount);//row1
		int	columnCount = 0;
		Cell cell;
		cell=createCell(row, s8B_nb, columnCount, "O");
		cell=createCell(row, s8B_nb, ++columnCount, "Equipment Rental");
		
		row = sheet.createRow(++rowCount);//row2
		columnCount = 0;
		cell=createCell(row, s8B_nb, columnCount, "");
		
		row = sheet.createRow(++rowCount);//row3
		columnCount = 1;  
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtk_rbtn, columnCount, "Equipment");
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtn_rbtn, ++columnCount, "Cost");
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtn_rbtn, ++columnCount, "Qty");
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtn_rbtk, ++columnCount, "Total");
		
		row = sheet.createRow(++rowCount);//row3a
		columnCount = 1;
		cell=createCell(row, s8_l_tbtn_bbtn_lbtk_rbtn, columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtk, ++columnCount, "");
		
		int actualEquipmentCost=0;
		
		for(EquipmentRental obj:cnc.getEquipmentRentalList()){
			row = sheet.createRow(++rowCount);//row4...
			columnCount = 1;
			double purPrice=0;
			if(obj.getPurchasePrice()==0){
				purPrice=obj.getMarketPrice();
			}else{
				purPrice=obj.getPurchasePrice();
			}
			double total=purPrice*obj.getQty();
			actualEquipmentCost+=total;
			cell=createCell(row, s8_l_tbtn_bbtn_lbtk_rbtn, columnCount, obj.getMachine());
			cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtn, ++columnCount, purPrice+"");
			cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, obj.getQty()+"");
			cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, total+"");
		}
		
		row = sheet.createRow(++rowCount);//row5
		columnCount = 1;
		cell=createCell(row, s8_l_tbtn_bbtn_lbtk_rbtn, columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtk, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row6
		columnCount = 1;
		cell=createCell(row, s8_l_tbtn_bbtn_lbtk_rbtn, columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtk, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row3
		columnCount = 1;
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtk_rbtn, columnCount, "");
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8B_c_tbtk_bbtk_lbtn_rbtn, ++columnCount, "Total");
		cell=createCell(row, s8B_r_tbtk_bbtk_lbtn_rbtk, ++columnCount, actualEquipmentCost+"");
		
		int noOfMonth=(int) cnc.getMonthlyFinancePlan();
		double monthlyAmt=actualEquipmentCost/noOfMonth;
		double intFinAmt=((actualEquipmentCost*cnc.getInterestOnFinAmt())/100)/12;
		double monthlyCostOfEquip=monthlyAmt+intFinAmt;
		double estimatedRepairAmt=((actualEquipmentCost*cnc.getEstimatedRepair())/100)/12;
		double totalMonthlyCost=monthlyCostOfEquip+estimatedRepairAmt;
		
		row = sheet.createRow(++rowCount);//row7
		columnCount = 1;
		cell=createCell(row, s8_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Monthly amounts - actual");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, "");
		
		row = sheet.createRow(++rowCount);//row8
		columnCount = 1;
		cell=createCell(row, s8_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Monthly finance plan");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, noOfMonth+"");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "months");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, Math.round(monthlyAmt)+"");
		
		row = sheet.createRow(++rowCount);//row9
		columnCount = 1;
		cell=createCell(row, s8_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Intrest on Financed Amount");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, cnc.getInterestOnFinAmt()+"");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, Math.round(intFinAmt)+"");
		
		row = sheet.createRow(++rowCount);//row10
		columnCount = 1;
		cell=createCell(row, s8B_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Monthly cost of equipment");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, Math.round(monthlyCostOfEquip)+"");
		
		row = sheet.createRow(++rowCount);//row11
		columnCount = 1;
		cell=createCell(row, s8_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Estimated repairs");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, cnc.getEstimatedRepair()+"");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, Math.round(estimatedRepairAmt)+"");
		
		totalEquipmentCost=monthlyCostOfEquip;
		totalEquipmentRepair=estimatedRepairAmt;
		totalOfActualEquipmentCost=Math.round(totalMonthlyCost);
		row = sheet.createRow(++rowCount);//row12
		columnCount = 1;
		cell=createCell(row, s8_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Total monthly cost - actual");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, Math.round(totalMonthlyCost)+"");
		
		
		row = sheet.createRow(++rowCount);//row13
		columnCount = 1;
		cell=createCell(row, s8B_c_tbtn_bbtn_lbtk_rbtn, columnCount, "Proposed charge per cost sheet");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_c_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount, Math.round(cnc.getTotalEquipmentAmount())+"");
		
		row = sheet.createRow(++rowCount);//row14
		columnCount = 1;
		cell=createCell(row, s8_l_tbtn_bbtn_lbtk_rbtn, columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtn_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtn_lbtn_rbtk, ++columnCount,Math.round(cnc.getTotalEquipmentAmount()-totalMonthlyCost)+"");
		
		row = sheet.createRow(++rowCount);//row15
		columnCount = 1;
		cell=createCell(row, s8_l_tbtn_bbtk_lbtk_rbtn, columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtk_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_l_tbtn_bbtk_lbtn_rbtn, ++columnCount, "");
		cell=createCell(row, s8_r_tbtn_bbtk_lbtn_rbtk, ++columnCount, Math.round(((cnc.getTotalEquipmentAmount()-totalMonthlyCost)/totalMonthlyCost)*100)+"%");
		
	}
	
	private ArrayList<String> getAllGPCostVerticalHeader() {

		boolean flag = false;
		Branch branch = ofy().load().type(Branch.class).filter("companyId" , cnc.getCompanyId()).filter("buisnessUnitName", cnc.getBranch()).first().now();
		if(branch != null){
			if(branch.getAddress().getState() !=null && !branch.getAddress().getState().equals("")){
				if(branch.getAddress().getState().equalsIgnoreCase("Maharashtra")){
					flag = true;
				}
			}
		}
		ArrayList<String> list= new ArrayList<String>();
		
		list.add("A//Monthly Payments");
		
		list.add("Basic");
		list.add("D.A.");
		list.add("House Rent Allowance");
		list.add("Conveyance");
		list.add("B&P");
		list.add("Medical");

		list.add("Washing Allowances");
		list.add("Other Allowances");
		
		list.add("");
		
		list.add("B//Statutory Payments");
		list.add("PF");
		list.add("ESIC");
		list.add("EDLI");
		if(flag){
			list.add("CMWF");
		}else{
			list.add("CLWF");
		}
		list.add("Bonus");
		list.add("P Leave");
		list.add("CPL Esic");
		list.add("CBonus Esic");	
		list.add("");
		
		list.add("C//Total (A+B)");
		list.add("c1#Employee Overhead Cost"); 
		list.add("c2#Travelling Allowance");
		list.add("c3#Additional Allowance");
		
		list.add("");
		
		list.add("D//Total (C+c1+c2+c3)");
		list.add("E// ");
		list.add("F//Total (D+E)");
		list.add("G//No. of Employees Required");
		list.add("H//Sum of (FXG)");
		//
		list.add("Bonus without reliever formula");
		list.add("Paid Leave without reliever formula");
		
		list.add(" //Excluding overheads-actual payment");
		list.add("");
		
		list.add("I//Additional Hours");
		list.add("J//SUM");
		
//		list.add("K//Housekeeping Consumables\nGarbage Bags\nFeminine Hygiene Bins\nWashroom Consumable(6 Caddies + 4 Buckets)\nTotal I+J\nManagement Fee");
		list.add("Housekeeping Consumables");
		list.add("Garbage Bags");
		list.add("Feminine Hygiene Bins");
		list.add("Washroom Consumable(6 Caddies + 4 Buckets)");
		list.add("Total I+J");
		list.add("K//Management Fee");
		
		list.add("L//TOTAL(J+K)");
		list.add("M//Equipment Rental");
		list.add("N//TOTAL SALES(MONTHLY)(J+K+L+M)");
		return list;
	}
	
	private void getCostSheetSummaryVerticalHeader( XSSFSheet sheet, int rowCount,XSSFWorkbook  workbook){
		ArrayList<String> list  = getAllGPCostVerticalHeader();
		
		int columnCount = 1;
		Cell cell;	
		 style2 = workbook.createCellStyle();
	     style2.setAlignment(XSSFCellStyle.ALIGN_LEFT);
	     style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	    
	     XSSFFont font = workbook.createFont();
	     font.setFontHeightInPoints((short) 12);
//	     font.setFontName("Times New Roman");
	     /**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
			
		font.setFontName("Calibri");
	     font.setBold(true);;
	     
	     style2.setFont(font);
	     style2.setBorderTop(CellStyle.BORDER_THICK);
	     style2.setBorderBottom(CellStyle.BORDER_THICK);
	     style2.setBorderLeft(CellStyle.BORDER_THICK);
	     style2.setBorderRight(CellStyle.BORDER_THICK);
	     
	     XSSFCellStyle style1 = workbook.createCellStyle();
	     style1.setAlignment(XSSFCellStyle.ALIGN_LEFT);
	     style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	     XSSFFont font1 = workbook.createFont();
	     font1.setFontHeightInPoints((short) 12);
//	     font1.setFontName("Times New Roman");
	     /**date 31-7-2019 by Amol Raised by Sasha font style change times new roman to calibri**/
			
	     font1.setFontName("Calibri");
	     style1.setFont(font1);
	     
	     style1.setBorderTop(CellStyle.BORDER_THIN);
	     style1.setBorderBottom(CellStyle.BORDER_THIN);
	     style1.setBorderLeft(CellStyle.BORDER_THICK);
	     style1.setBorderRight(CellStyle.BORDER_THICK);
	   // style1.setWrapText(true);
	     Row row;
	     XSSFCellStyle style4 = workbook.createCellStyle();
	     style4.setFont(font);
		for(String s : list){
			row = sheet.createRow(++rowCount);
			if(s.contains("//")){
				String array[] = s.split("//");
				cell = row.createCell(columnCount);
				cell.setCellValue((String) array[1]);
				cell.setCellStyle(style2);
				
				cell = row.createCell(columnCount-1);
				cell.setCellValue((String) array[0]);
				cell.setCellStyle(style2);

			}else if(s.contains("#")){
				String array[] = s.split("#");
				cell = row.createCell(columnCount);
				cell.setCellValue((String) array[1]);
				cell.setCellStyle(style1);
				
				cell = row.createCell(columnCount-1);
				cell.setCellValue((String) array[0]);
				 XSSFCellStyle style3 = workbook.createCellStyle();
			     style3.setAlignment(XSSFCellStyle.ALIGN_LEFT);
			     style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			     style3.setFont(font);
			     style3.setBorderTop(CellStyle.BORDER_THIN);
			     style3.setBorderBottom(CellStyle.BORDER_THIN);
			     style3.setBorderLeft(CellStyle.BORDER_THICK);
			     style3.setBorderRight(CellStyle.BORDER_THICK);
				if(array[0].contains("c")){
					cell.setCellStyle(style1);
				}else{
					cell.setCellStyle(style3);
				}

			}else if(s.contains("/")){
				cell = row.createCell(columnCount);
				cell.setCellValue((String) s.substring(1, s.length()));
				cell.setCellStyle(style2);
				
				cell = row.createCell(columnCount-1);
				cell.setCellValue("");
				cell.setCellStyle(style2);
			}else{
				cell = row.createCell(columnCount);
				cell.setCellValue((String) s);
				cell.setCellStyle(style1);
				cell = row.createCell(columnCount-1);
				cell.setCellValue("");
				cell.setCellStyle(style1);
			}
		}

	}

	private void getCostSheetSummaryVerticalValues(XSSFSheet sheet,int rowc, XSSFWorkbook workbook) {
		int columnCount = 2;
		Cell cell;
		
		XSSFCellStyle style1 = workbook.createCellStyle();
		style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		XSSFFont font1 = workbook.createFont();
		font1.setFontHeightInPoints((short) 12);
		font1.setFontName("Calibri");
		style1.setFont(font1);
		style1.setBorderTop(CellStyle.BORDER_THIN);
		style1.setBorderBottom(CellStyle.BORDER_THIN);
		style1.setBorderLeft(CellStyle.BORDER_THICK);
		style1.setBorderRight(CellStyle.BORDER_THICK);

		XSSFCellStyle style3 = workbook.createCellStyle();
		style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style3.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		XSSFFont font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setFontName("Calibri");
		font.setBold(true);
		style3.setFont(font);
		style3.setBorderTop(CellStyle.BORDER_THICK);
		style3.setBorderBottom(CellStyle.BORDER_THICK);
		style3.setBorderLeft(CellStyle.BORDER_THICK);
		style3.setBorderRight(CellStyle.BORDER_THICK);

		Row row;
		int finalRowCont = 0;
		int totalEmployee = 0;
		int column = 2 + cnc.getSaffingDetailsList().size();
		for (StaffingDetails staff : cnc.getSaffingDetailsList()) {
			int rowCount = rowc;
			
			row = sheet.getRow(++rowCount); //Row1
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(staff.getGrossSalary()));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row2
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getBasic()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row3
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getDa())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row4
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getHra())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row5
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getConveyance())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row6
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getBandP())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row7
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getMedical())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
				
			row = sheet.getRow(++rowCount);//Row8
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getWashingAllowance()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row9
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getOthers())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row10 BLANK
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row11 STATUTORY PAYMENTS
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getSatutoryPayments())));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row12
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpPF())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row13
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpESIC())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//Row14
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpEDLI())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);// ROW15
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpMWF())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW16
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getSpBonus())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW17
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getPaidLeaves())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			/**Date 6-12-2019 by amol**/
			row = sheet.getRow(++rowCount);//ROW17
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getcPlEsic()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW17
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getcBonusEsic()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			
			
			
			
			
			
			row = sheet.getRow(++rowCount);//ROW18 BLANK
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW19 TOTAL MONTHLY COST
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getTotal())));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW20 EMPLOYEE OVERHEADCOST
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(Double.parseDouble(staff.getEmployementOverHeadCost())));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW 21 TRAVELLING ALLOWANCE OVERHEAD
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getTravellingAllowanceOverhead()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW 22 ADDITIONAL ALLOWANCE
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOffValue(staff.getAdditionalAllowance()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW 23 BLANK
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW24 TOTAL OF OVERHEAD
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(Double.parseDouble(staff.getTotalOfPaidAndOverhead())));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			double value1 = 0, value2 = 0;
			if (staff.getTotalOfPaidAndOverhead() != null) {
				value1 = Double.parseDouble(staff.getTotalOfPaidAndOverhead());
			}
//			if (staff.getManagementFees() != null) {
//				try {
//					value2 = Math.round((Double.parseDouble(staff.getManagementFees()) / staff.getNoOfStaff()) * 100.0) / 100.0;
//				} catch (Exception e) {
//					value2 = 0;
//				}
//			}
			
			row = sheet.getRow(++rowCount);//ROW25 BLANK
			
			cell = row.createCell(columnCount);
//			cell.setCellValue(roundOff(value2));
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			double value = Math.round((value1 + value2));
			row = sheet.getRow(++rowCount); //ROW26
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(value));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			row = sheet.getRow(++rowCount);//ROW27 G No of Employee Required
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(staff.getNoOfStaff()));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);

			totalEmployee = totalEmployee + staff.getNoOfStaff();
			if (columnCount == column - 1) {
				cell = row.createCell(column);
				cell.setCellValue(roundOff(totalEmployee));
				cell.setCellStyle(style3);

			}

			double totalIncludingManagementFees = (Double.parseDouble(staff.getTotalOfPaidAndOverhead())*staff.getNoOfStaff());
			totalPayExcludingManagementFees += totalIncludingManagementFees;

			row = sheet.getRow(++rowCount);//ROW 28 H Total(F*G)
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(totalIncludingManagementFees));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue(totalPayExcludingManagementFees);
			cell.setCellStyle(style3);
			
			row = sheet.getRow(++rowCount);//ROW 29 bonus reliever formula
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(staff.getBonusWithoutReliever()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			
			/**date 27-4-2020 by Amol **/
			double paidleave=((staff.getBasic()+Double.parseDouble(staff.getDa()))/30)*(Double.parseDouble(staff.getPaidLeaveDays())/12);
			
			
			row = sheet.getRow(++rowCount);//ROW 30 paid leave reliever formula
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(paidleave));
//			cell.setCellValue(roundOff(staff.getPaidLeaveWithoutReliever()));
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW 31 excluding overhead -actual payment
			cell = row.createCell(columnCount);
			cell.setCellValue(roundOff(staff.getActualPayementExcludingOverhead()));
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			
//			totalPayWithoutRelieverFormula+=staff.getActualPayementExcludingOverhead();

			row = sheet.getRow(++rowCount);//ROW 32 BLANK
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			
			row = sheet.getRow(++rowCount);//ROW 32 ADDITIONAL HOURS
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			
			row = sheet.getRow(++rowCount);//ROW 33 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue(roundOff(totalPayExcludingManagementFees));
			cell.setCellStyle(style3);
			
			totalManagementFees+=Double.parseDouble(staff.getManagementFees());
			
			row = sheet.getRow(++rowCount);//ROW 33a SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			if (cnc.getNetPayableOFConsumables() != 0) {
				cell.setCellValue(roundOff(cnc.getNetPayableOFConsumables()));
			} else {
				cell.setCellValue("At Actuals");
			}
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW 34 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW 35 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW 36 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW 37 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue("");
			cell.setCellStyle(style1);
			
			row = sheet.getRow(++rowCount);//ROW 38 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue(roundOff(totalManagementFees));
			cell.setCellStyle(style3);
			
			
			double totalExcludingEquipment=0;
			if(cnc.getNetPayableOFConsumables()!=0){
				totalExcludingEquipment=totalPayExcludingManagementFees+totalManagementFees+cnc.getNetPayableOFConsumables();
			}else{
				totalExcludingEquipment=totalPayExcludingManagementFees+totalManagementFees;
			}
			
			row = sheet.getRow(++rowCount);//ROW 39 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue(roundOff(totalExcludingEquipment));
			cell.setCellStyle(style3);
			
			row = sheet.getRow(++rowCount);//ROW 40 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue(roundOff(cnc.getNetPayableOFEquipment()));
			cell.setCellStyle(style3);
			
			row = sheet.getRow(++rowCount);//ROW 41 SUM
			cell = row.createCell(columnCount);
			cell.setCellValue("");
			cell.setCellStyle(style3);
			cell = row.createCell(columnCount + 1);
			cell.setCellValue(roundOff(cnc.getNetPayableOFCNC()));
			cell.setCellStyle(style3);
			
			finalRowCont = rowCount;
			columnCount++;
		}
		
	}
    /**@author Sheetal : 28-04-2022 
     * Des : Created 4 sheets of CRM Tally Report in Oracle format, requirement by envocare
     **/
     
	public void getCRMTallyReport(XSSFWorkbook  workbook){
		try{
		XSSFSheet sheet1 = workbook.createSheet("RA_INTERFACE_LINES_ALL");
		XSSFSheet sheet2 = workbook.createSheet("RA_INTERFACE_DISTRIBUTIONS_ALL");
		XSSFSheet sheet3 = workbook.createSheet("RA_INTERFACE_SALESCREDITS_ALL");
		XSSFSheet sheet4 = workbook.createSheet("AR_INTERFACE_CONTS_ALL");
		
		ArrayList<Invoice> invoicelis=CsvWriter.invoicelis;
		List<Invoice> invlist = invoicelis;
		Cell cell;
	    Row row, row1;
		rowCount = 0;
    	row = sheet1.createRow(rowCount);
        cell = row.createCell(columnCount);
		cell.setCellStyle(style2);
		getRA_INTERFACE_LINES_ALL_Header(sheet1,row,workbook);
		getRA_INTERFACE_DISTRIBUTIONS_ALL_Header(sheet2,row,workbook);
		getRA_INTERFACE_SALESCREDITS_ALL(sheet3,row,workbook);
		getAR_INTERFACE_CONTS_ALL(sheet4,row,workbook);
		
		for (Invoice invoicelist : invlist) {
		
			int linenumber = 1;
			
               for (int d = 0; d < invoicelist.getSalesOrderProducts().size(); d++) {
            	   row1 = sheet1.createRow(++rowCount);
				   columnCount = 0;
			    	   
		    	   cell = row1.createCell(columnCount);
	               cell.setCellValue((String) "EPCSPL Business Unit");//1
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "Source - Import");//2
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "Invoice");//3
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) invoicelist.getPaymentTerms());//4//?
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) fmt.format(invoicelist.getInvoiceDate()));//5
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) fmt.format(invoicelist.getInvoiceDate()));//6
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//7
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//8
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//9
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//10
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//11
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//12
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//13
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//14
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//15
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//16
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//17
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//18
	               
	               Customer customer=ofy().load().type(Customer.class).filter("companyId",invoicelist.getCompanyId())
							 .filter("count", invoicelist.getPersonInfo().getCount()).first().now();
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) customer.getRefrNumber1());//19//
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) customer.getRefrNumber2());//20//
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//21
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) customer.getShipToCustAccNo());//22//
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) customer.getShipToCustSiteNo());//23//
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//24
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//25
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) invoicelist.getTransactionLineType());//26//?
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) invoicelist.getComment());//27
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "INR");//28
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "Corporate");//29
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//30
	           
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//31
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((Double) invoicelist.getSalesOrderProducts().get(d).getTotalAmount());//32//unit price*qty
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((Double) invoicelist.getSalesOrderProducts().get(d).getQuantity());//33//qty
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//34
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((Double) invoicelist.getSalesOrderProducts().get(d).getPrice());//35//unit price
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "");//36
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) "ENVOCARE_Invoice_Upload");//37
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((String) invoicelist.getInvRefNumber());//38
	               
	               cell = row1.createCell(++columnCount);
	               cell.setCellValue((int) linenumber);//39//
	               
	               int columnCountsheet1 = 52;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getTaxClassificationCode());//54//
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "11");//55
	               
	               columnCountsheet1 = 59;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "NOS");//61
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "NOS");//62
	               
	               columnCountsheet1 = 68;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "SALES_TRANSACTION");//70
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//71
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getSalesOrderProducts().get(d).getHsnCode());//72
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "SERVICES");//73
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//74
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//75
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "TAX INVOICE");//76
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "IN");//77
	               
	               HashSet<Integer> clientId=new HashSet<Integer>();
	       		
	       		for(Invoice invoice:invlist){
	       			if(invoice.getAccountType().equals("AR")){
	       				clientId.add(invoice.getPersonInfo().getCount());
	       			   }
	       			}
	       			List<Customer> customerList = null;
	               List<Integer> clientIdList=new ArrayList<Integer>(clientId);
	               if(clientIdList.size()!=0){
		       			customerList=ofy().load().type(Customer.class).filter("companyId", invlist.get(0).getCompanyId()).filter("count IN", clientIdList).list();
		       		}
	       		
	       	  Customer cust = null;
	       		cust=getCustomerDetails(invoicelist.getPersonInfo().getCount(), customerList);
	               String artValue = " ";
					if (cust != null && cust.getArticleTypeDetails() != null&& cust.getArticleTypeDetails().size() != 0) {
						ArrayList<ArticleType> articleTypeDetails = cust.getArticleTypeDetails();
						for (ArticleType art : articleTypeDetails) {
							if (art.getArticleTypeName().equalsIgnoreCase("gstIn")) {
								artValue = art.getArticleTypeValue();
							}
						}
					}
				  String branchGST="";
				  Branch branch = ofy().load().type(Branch.class).filter("companyId", invlist.get(0).getCompanyId()).filter("buisnessUnitName", invlist.get(0).getBranch()).first().now();
				  if(branch!=null){
					  if(branch.getGSTINNumber()!=null&&!branch.getGSTINNumber().equals("")){
						  branchGST = branch.getGSTINNumber();
					  }
				  }
	               columnCountsheet1 = 84;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) branchGST);//86 branch gst 
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) artValue);//87 customer gst
	               
	               columnCountsheet1 = 89;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getTaxExemptionCode());//91 dropdown tax exemption flag
	               
	               columnCountsheet1 = 105;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getPoNumber());//107 ref po no
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//108 
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//109
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//110
	               
	               String ProductCode="";
	               ServiceProduct ServiceProduct=ofy().load().type(ServiceProduct.class)
							 .filter("count", invoicelist.getSalesOrderProducts().get(d).getProdId()).first().now();
	               System.out.println("serviceproduct :" + ServiceProduct);
	               if(ServiceProduct!=null){
	            	    ProductCode=ServiceProduct.getRefNumber1();
	               }
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) ProductCode);//111//?
	               
	               columnCountsheet1 = 202;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getVendorName());//204// sub contractor-name
	               
	               columnCountsheet1 = 218;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getCreatedBy());//220//eva user name
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) "");//221//blank
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getRefNumber());//222 eva Reference number
	               
	               columnCountsheet1 = 230;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) fmt.format(invoicelist.getBillingPeroidFromDate()));//232 billing from date
	               
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) fmt.format(invoicelist.getBillingPeroidToDate()));//233 billing to date
	               
	               columnCountsheet1 = 285;
	               cell = row1.createCell(++columnCountsheet1);
	               cell.setCellValue((String) invoicelist.getDeclaration());//287 comments eva declaration feild
	              
	               linenumber++;         
			}
              
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, " get error in Oracle CRM tally report --"+e.getMessage());
		}
	}
	
		public Customer getCustomerDetails(int customerId,List<Customer> customerList){
   			if(customerList!=null){
   				for(Customer customer:customerList){
   					if(customer.getCount()==customerId){
   						return customer;
   					}
   				}
   			}
   			return null;
   		}
	
	private void getRA_INTERFACE_LINES_ALL_Header( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {
	
		    int columnCount = 0;
		    Cell cell;
		
		    style2 = workbook.createCellStyle();
	      
	        XSSFFont font = workbook.createFont();
	        font.setFontHeightInPoints((short) 11);
	        font.setFontName("Times New Roman");
	        font.setBold(true);
	      
	        style2.setFont(font);
	      
	        row = sheet.createRow(rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "Receivables AutoInvoice Interface Lines");
			cell.setCellStyle(style2);
			
			row = sheet.createRow(++rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "*Business Unit Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Transaction Batch Source Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Transaction Type Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Payment Terms");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Transaction Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Transaction Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Bill-to Customer Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Bill-to Customer Address Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Bill-to Customer Contact Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Ship-to Customer Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Ship-to Customer Address Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Ship-to Customer Contact Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Ship-to Customer Account Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Ship-to Customer Account Address Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Ship-to Customer Account Contact Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Sold-to Customer Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Sold-to Customer Account Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Bill-to Customer Account Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Bill-to Customer Site Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Bill-to Contact Party Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Ship-to Customer Account Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Ship-to Customer Site Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Ship-to Contact Party Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sold-to Customer Account Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Transaction Line Type");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Transaction Line Description");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Currency Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Currency Conversion Type");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Currency Conversion Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Currency Conversion Rate");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Transaction Line Amount");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Transaction Line Quantity");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Customer Ordered Quantity");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Unit Selling Price");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Unit Standard Price");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Primary Salesperson Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Classification Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Legal Entity Identifier");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounted Amount in Ledger Currency");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Order Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Order Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Actual Ship Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Warehouse Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Unit of Measure Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Unit of Measure Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoicing Rule Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Revenue Scheduling Rule Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Number of Revenue Periods");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Revenue Scheduling Rule Start Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Revenue Scheduling Rule End Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reason Code Meaning");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Last Period to Credit");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Transaction Business Category Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Product Fiscal Classification Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Product Category Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Product Type");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Intended Use Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Assessable Value");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Document Sub Type");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Default Taxation Country");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "User Defined Fiscal Classification");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Invoice Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Invoice Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Regime Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Status Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Rate Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Jurisdiction Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "First Party Registration Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Third Party Registration Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Final Discharge Location");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Taxable Amount");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Taxable Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Exemption Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Exemption Reason Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Exemption Reason Code Meaning");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Exemption Certificate Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Amount Includes Tax Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Precedence");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Method To Be Used For Lines With Revenue Scheduling Rules");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Method To Be Used For Transactions With Split Payment Terms");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reason Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Tax Rate");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "FOB Point");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Carrier");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Shipping Reference");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Order Line Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Order Source");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Order Revision Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Purchase Order Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Purchase Order Revision Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Purchase Order Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Agreement Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Memo Line Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Document Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original System Batch Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link-to Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reference Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Link To Parent Line Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receipt Method Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Printing Option");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Related Batch Source Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Related Transaction Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 16");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 17");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 18");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 19");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Inventory Item Segment 20");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Bill To Customer Bank Account Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Reset Transaction Date Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Payment Server Order Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Last Transaction on Debit Authorization");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Approval Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Address Verification Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Transaction Line Translated Description");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Consolidated Billing Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Promised Commitment Amount");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Payment Set Identifier");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Original Accounting Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoiced Line Accounting Level");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Override AutoAccounting Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Historical Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Deferral Exclusion Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Payment Attributes");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Billing Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Lines Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Invoice Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 16");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 17");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 18");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 19");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 20");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 21");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 22");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 23");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 24");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 25");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 26");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 27");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 28");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 29");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Segment 30");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Attribute Category");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 16");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 17");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 18");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 19");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Global Descriptive Flexfield Segment 20");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Comments");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Notes from Source");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Card Token Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Card Expiration Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "First Name of the Credit Card Holder");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Last Name of the Credit Card Holder");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Card Issuer Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Masked Credit Card Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Card Authorization Request Identifier");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Credit Card Voice Authorization Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Number Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Date Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Date Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Date Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Date Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Region Information Flexfield Date Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Number Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Number Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Number Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Number Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Number Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Date Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Date Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Date Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Date Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Receivables Transaction Line Region Information Flexfield Date Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Freight Charge");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Insurance Charge");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Packing Charge");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Miscellaneous Charge");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Commercial Discount");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Enforce Chronological Document Sequencing");
			cell.setCellStyle(style2);
		
	}
	
	private void getRA_INTERFACE_DISTRIBUTIONS_ALL_Header( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {
		    int columnCount = 0,rowCount=0;
		    Cell cell;
		
		    style2 = workbook.createCellStyle();
	      
	        XSSFFont font = workbook.createFont();
	        font.setFontHeightInPoints((short) 11);
	        font.setFontName("Times New Roman");
	        font.setBold(true);
	      
	        style2.setFont(font);
	        row = sheet.createRow(rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "Receivables AutoInvoice Interface Line Distributions");
			cell.setCellStyle(style2);
			
			row = sheet.createRow(++rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "*Business Unit Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Account Class");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Amount");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Percent");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounted Amount in Ledger Currency");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 16");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 17");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 18");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 19");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 20");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 21");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 22");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 23");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 24");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 25");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 26");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 27");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 28");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 29");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Accounting Flexfield Segment 30");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Comments");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 16");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 17");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 18");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 19");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 20");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 21");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 22");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 23");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 24");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 25");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 26");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 27");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 28");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 29");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interim Tax Segment 30");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Distributions Flexfield Segment 15");
			cell.setCellStyle(style2);	
			
			ArrayList<Invoice> invoicelis=CsvWriter.invoicelis;
			List<Invoice> invlist = invoicelis;
			
			for (Invoice invoicelist : invlist) {
				int linenumber=1;
			   for (int d = 0; d < invoicelist.getSalesOrderProducts().size(); d++) {
					 
	   			  for(int i=0;i<=1;i++){
	   				   Row row2=sheet.createRow(++rowCount);
	   			
	   				   columnCount = 0;
	   	               cell = row2.createCell(columnCount);
	   	               cell.setCellValue((String) "EPCSPL Business Unit");
	   	             
	   	               if(i==0){
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "REV");//
	   	               }else{
	   	            	   cell = row2.createCell(++columnCount);
	   		               cell.setCellValue((String) "REC");//
	   	               }
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((double) invoicelist.getSalesOrderProducts().get(d).getPrice());//
	   	 
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "100.00");
	   	            
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "ENVOCARE_Invoice_Upload");
	   	           
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) invoicelist.getInvRefNumber());
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((int) linenumber);//
	   	              
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	              
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	              
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	            
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	           
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	           
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	            
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	              
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	             
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	            
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "");
	   	              
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "337101");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "999");
	   	           
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "999999");
	   	           
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "999");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "999");
	   	              
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "999");
	   	           
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "99");
	   	         
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "9999");
	   	               
	   	               cell = row2.createCell(++columnCount);
	   	               cell.setCellValue((String) "999");
	   	           
	   			}
	   			 linenumber++;
			   }
			}		
	}
	
	private void getRA_INTERFACE_SALESCREDITS_ALL( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {
		    
		    int	columnCount = 0,rowCount=0;
		    Cell cell;
		
		    style2 = workbook.createCellStyle();
	      
	        XSSFFont font = workbook.createFont();
	        font.setFontHeightInPoints((short) 11);
	        font.setFontName("Times New Roman");
	        font.setBold(true);
	      
	        style2.setFont(font);
	        row = sheet.createRow(rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "Receivables AutoInvoice Interface Line Salescredits");
			cell.setCellStyle(style2);
			
			row = sheet.createRow(++rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "*Business Unit Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Salesperson Number");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Name of Sales Credit Type");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Credit Amount Split");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Sales Credit Percentage Split");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Interface Sales Credits Flexfield Segment 15");
			cell.setCellStyle(style2);
	}
	
    private void getAR_INTERFACE_CONTS_ALL( XSSFSheet sheet, Row row,XSSFWorkbook  workbook) {
    	
    	    int	columnCount = 0,rowCount=0;
		    Cell cell;
		
		    style2 = workbook.createCellStyle();
	      
	        XSSFFont font = workbook.createFont();
	        font.setFontHeightInPoints((short) 11);
	        font.setFontName("Times New Roman");
	        font.setBold(true);
	      
	        style2.setFont(font);
	        row = sheet.createRow(rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "Receivables AutoInvoice Interface Line Contingencies");
			cell.setCellStyle(style2);
			
			row = sheet.createRow(++rowCount);
	        cell = row.createCell(columnCount);
			cell.setCellValue((String) "*Business Unit Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Contingency Code");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "*Contingency Name");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Contingency Expiration Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Contingency Expiration Days");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Context");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Line Transactions Flexfield Segment 15");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Completed Flag");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Completed By");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Contingency Expiration Event Date");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Attribute Category");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 1");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 2");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 3");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 4");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 5");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 6");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 7");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 8");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 9");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 10");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 11");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 12");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 13");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 14");
			cell.setCellStyle(style2);
			
			cell = row.createCell(++columnCount);
			cell.setCellValue((String) "Descriptive Flexfield Segment 15");
			cell.setCellStyle(style2);	
			
	}
    
    /**
     * @author Vijay Date :- 13-09-2022
     * Des :- wrriten common method to write data iin excel format
     * input :- String list and Number of column for columnCount
     */
    public void createExcelData(ArrayList<String> list, int columnCount, XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"Inside createCTCWorkbook"+columnCount);
		logger.log(Level.SEVERE,"list size "+list.size());

		XSSFCellStyle boldStyle = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont boldFont = (XSSFFont) workbook.createFont();
//	    boldFont.setFontHeightInPoints((short) 14);
	    boldFont.setFontName("Times New Roman");
	    boldFont.setBold(true);
	    boldStyle.setFont(boldFont);
	    boldStyle.setBorderBottom(BorderStyle.THIN);
	    boldStyle.setBorderLeft(BorderStyle.THIN);
	    boldStyle.setBorderRight(BorderStyle.THIN);
	    boldStyle.setBorderTop(BorderStyle.THIN);
//	    boldStyle.setWrapText(true);
	   
	    XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont font = (XSSFFont) workbook.createFont();
//	    font.setFontHeightInPoints((short) 14);
	    font.setFontName("Times New Roman");
	    font.setBold(false);
	    style.setFont(font);
	    style.setBorderBottom(BorderStyle.THIN);
	    style.setBorderLeft(BorderStyle.THIN);
	    style.setBorderRight(BorderStyle.THIN);
	    style.setBorderTop(BorderStyle.THIN);
//	    style.setWrapText(true);
	    style.setAlignment(HorizontalAlignment.CENTER);
	    style.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    XSSFCellStyle datestyle = (XSSFCellStyle) workbook.createCellStyle();
//	    font.setFontHeightInPoints((short) 14);
	    font.setFontName("Times New Roman");
	    font.setBold(false);
	    datestyle.setFont(font);
	    datestyle.setBorderBottom(BorderStyle.THIN);
	    datestyle.setBorderLeft(BorderStyle.THIN);
	    datestyle.setBorderRight(BorderStyle.THIN);
	    datestyle.setBorderTop(BorderStyle.THIN);
//	    datestyle.setWrapText(true);
	    datestyle.setAlignment(HorizontalAlignment.CENTER);
	    datestyle.setVerticalAlignment(VerticalAlignment.CENTER);
		CreationHelper createHelper = workbook.getCreationHelper();
		datestyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy"));
	    
	    XSSFCellStyle leftStyle = (XSSFCellStyle) workbook.createCellStyle();
	    leftStyle.setFont(font);
	    leftStyle.setBorderBottom(BorderStyle.THIN);
	    leftStyle.setBorderLeft(BorderStyle.THIN);
	    leftStyle.setBorderRight(BorderStyle.THIN);
	    leftStyle.setBorderTop(BorderStyle.THIN);
	    leftStyle.setWrapText(true);
	    leftStyle.setAlignment(HorizontalAlignment.LEFT);
	    leftStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    
	    XSSFSheet sheet = (XSSFSheet) workbook.createSheet("Sheet1");
//	    sheet.setDefaultColumnWidth(15);
//	    sheet.setColumnWidth(0,2);
	    
	    Row row=sheet.createRow(0);
	    // Hedear Loop
	    for(int i=0;i<columnCount;i++){
		    createCell(row, i, list.get(i), boldStyle,workbook);
	    }
	    int rowCount=1;
	    System.out.println("columnCount == "+columnCount);
	    System.out.println("list size  == "+list.size());
	    	for(int i=columnCount; i<list.size(); i+=columnCount){
	    		row=sheet.createRow(rowCount);
	    			for(int j=0; j<columnCount; j++){
	    					createCell(row, j,list.get(i+j), style,workbook);
	    	    	}
		    	rowCount++;
		    }
	    
//	    }
		    
	}
	
	public void createCell(Row row, int column,String value,XSSFCellStyle style,XSSFWorkbook workbook) {
	    	Cell cell=row.createCell(column);
	    	cell.setCellStyle(style);	 
	    	
	    	XSSFFont font = (XSSFFont) workbook.createFont();
//		    font.setFontHeightInPoints((short) 14);
		    font.setFontName("Times New Roman");
		    font.setBold(false);
		    
		    XSSFCellStyle datestyle = (XSSFCellStyle) workbook.createCellStyle();
//		    font.setFontHeightInPoints((short) 14);
		    font.setFontName("Times New Roman");
		    font.setBold(false);
		    datestyle.setFont(font);
		    datestyle.setBorderBottom(BorderStyle.THIN);
		    datestyle.setBorderLeft(BorderStyle.THIN);
		    datestyle.setBorderRight(BorderStyle.THIN);
		    datestyle.setBorderTop(BorderStyle.THIN);
//		    datestyle.setWrapText(true);
		    datestyle.setAlignment(HorizontalAlignment.CENTER);
		    datestyle.setVerticalAlignment(VerticalAlignment.CENTER);
			CreationHelper createHelper = workbook.getCreationHelper();
			datestyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-mmm-yyyy"));
		    
			 
		    XSSFCellStyle linkstyle = (XSSFCellStyle) workbook.createCellStyle();
		    XSSFFont linkFont= (XSSFFont) workbook.createFont();
		    linkFont.setColor(HSSFColor.BLUE.index);
		    linkFont.setUnderline(XSSFFont.U_SINGLE);
		    
		    linkstyle.setFont(linkFont);
		    linkstyle.setBorderBottom(BorderStyle.THIN);
		    linkstyle.setBorderLeft(BorderStyle.THIN);
		    linkstyle.setBorderRight(BorderStyle.THIN);
		    linkstyle.setBorderTop(BorderStyle.THIN);
		    linkstyle.setAlignment(HorizontalAlignment.LEFT);
		    linkstyle.setVerticalAlignment(VerticalAlignment.CENTER);
		    
		    
	    	if(value!=null) {
	    		if(value.trim().matches("[0-9]+(\\.){0,1}[0-9]*")){
		    		if(isIntegerNumber(value)){
		    			int val = Integer.parseInt(value);
				    	cell.setCellValue((int) val);
		    		}
		    		else{
		    			double val = Double.parseDouble(value);
				    	cell.setCellValue((double) val);
		    		}
		    	}
	    		else if(value.trim().matches("[0-9]{2}(-){1}[0-9]{2}(-){1}\\d{4}")){
//	    			logger.log(Level.SEVERE,"value in dd-mm-yyyy="+value);
	    			int day=Integer.parseInt(value.substring(0, 2));
	    			int month=Integer.parseInt(value.substring(3, 5))-1;
			    	int year=Integer.parseInt(value.substring(6))-1900;
			    	System.out.println("year month day= "+year+" "+month+" "+day);
		    		Date d=new Date(year,month,day );
		    		cell.setCellValue(d);
		    		cell.setCellStyle(datestyle);	
		    		System.out.println("date set to "+d);
	    		}
		    	else if(value.trim().matches("[0-9]{2}(-){1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec){1}(-){1}\\d{4}")){
			    	int month=0;
//			    	logger.log(Level.SEVERE,"value in dd-mmm-yyyy="+value);
			    	switch(value.substring(3,6)){
			    	case "Jan":
			    		month=0;
			    		break;
			    	case "Feb":
			    		month=1;
			    		break;
			    	case "Mar":
			    		month=2;
			    		break;
			    	case "Apr":
			    		month=3;
			    		break;
			    	case "May":
			    		month=4;
			    		break;
			    	case "Jun":
			    		month=5;
			    		break;
			    	case "Jul":
			    		month=6;
			    		break;
			    	case "Aug":
			    		month=7;
			    		break;
			    	case "Sep":
			    		month=8;
			    		break;
			    	case "Oct":
			    		month=9;
			    		break;
			    	case "Nov":
			    		month=10;
			    		break;
			    	case "Dec":
			    		month=11;	
			    		break;
			    	}
			    	int day=Integer.parseInt(value.substring(0, 2));
			    	int year=Integer.parseInt(value.substring(7))-1900;
		    		Date d=new Date(year, month,day );
		    		cell.setCellValue(d);
//		    		logger.log(Level.SEVERE,"day="+day+"month="+month+"year"+year);
		    		cell.setCellStyle(datestyle);
		    	}
		    	else if(value.contains("https://")||value.contains("http://")){
		    		System.out.println("creating hyperlink");
		    		try {
		    			Hyperlink link = createHelper.createHyperlink(org.apache.poi.common.usermodel.Hyperlink.LINK_URL);
			    		link.setAddress(value);
			    		cell.setCellValue("Click here");
			    		cell.setHyperlink(link);
			    		cell.setCellStyle(linkstyle);
			    		
					} catch (Exception e) {
			    		cell.setCellValue(value);

					}
		    		
		    	}else{
		    		cell.setCellValue(value);
		    	}
	    	
	    	}else {
	    		cell.setCellValue("");
	    	}
	}
	/**
	 * ends here
	 */
	
	public static boolean isIntegerNumber(String str) {
	    try {
	        int v = Integer.parseInt(str);
	        return true;
	    } catch (NumberFormatException nfe) {
	    }
	    return false;
	}
	
	 /**
     * @author Vijay Date :- 13-09-2022
     * Des :- wrriten common method to write data iin excel format
     * input :- String list and Number of column for columnCount
     */
    public void createExcelData(ArrayList<String> list, int columnCount,ArrayList<String> list2, int columnCount2, XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"Inside createCTCWorkbook");
    	
		XSSFCellStyle boldStyle = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont boldFont = (XSSFFont) workbook.createFont();
//	    boldFont.setFontHeightInPoints((short) 14);
	    boldFont.setFontName("Times New Roman");
	    boldFont.setBold(true);
	    boldStyle.setFont(boldFont);
	    boldStyle.setBorderBottom(BorderStyle.THIN);
	    boldStyle.setBorderLeft(BorderStyle.THIN);
	    boldStyle.setBorderRight(BorderStyle.THIN);
	    boldStyle.setBorderTop(BorderStyle.THIN);
//	    boldStyle.setWrapText(true);
	   
	    XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont font = (XSSFFont) workbook.createFont();
//	    font.setFontHeightInPoints((short) 14);
	    font.setFontName("Times New Roman");
	    font.setBold(false);
	    style.setFont(font);
	    style.setBorderBottom(BorderStyle.THIN);
	    style.setBorderLeft(BorderStyle.THIN);
	    style.setBorderRight(BorderStyle.THIN);
	    style.setBorderTop(BorderStyle.THIN);
//	    style.setWrapText(true);
	    style.setAlignment(HorizontalAlignment.CENTER);
	    style.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    XSSFCellStyle leftStyle = (XSSFCellStyle) workbook.createCellStyle();
	    leftStyle.setFont(font);
	    leftStyle.setBorderBottom(BorderStyle.THIN);
	    leftStyle.setBorderLeft(BorderStyle.THIN);
	    leftStyle.setBorderRight(BorderStyle.THIN);
	    leftStyle.setBorderTop(BorderStyle.THIN);
	    leftStyle.setWrapText(true);
	    leftStyle.setAlignment(HorizontalAlignment.LEFT);
	    leftStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    
	    XSSFSheet sheet = (XSSFSheet) workbook.createSheet("Sheet1");
//	    sheet.setDefaultColumnWidth(15);
//	    sheet.setColumnWidth(0,2);
	    
	    Row row=sheet.createRow(0);
	    // Hedear Loop
	    for(int i=0;i<columnCount;i++){
		    createCell(row, i, list.get(i), boldStyle,workbook);
	    }
	    int rowCount=1;
	    System.out.println("columnCount == "+columnCount);
	    System.out.println("list size  == "+list.size());
	    for(int i=columnCount; i<list.size(); i+=columnCount){
    		row=sheet.createRow(rowCount);
    			for(int j=0; j<columnCount; j++){
        	    		createCell(row, j,list.get(i+j), style,workbook);
    	    	}
	    	rowCount++;
	    }
	    
	    
	    // Hedear Loop
	    for(int i=1;i<columnCount2;i++){
		    createCell(row, i, list2.get(i), boldStyle,workbook);
	    }
	    for(int i=columnCount2; i<list2.size(); i+=columnCount2){
    		row=sheet.createRow(rowCount);
    			for(int j=0; j<columnCount2; j++){
        	    		createCell(row, j,list2.get(i+j), style,workbook);
    	    	}
	    	rowCount++;
	    }
		    
	}
    

	
	public void getLeadReport(XSSFWorkbook workbook) {
		 logger.log(Level.SEVERE,"in getLeadReport");
		createExcelData(leadlist, leadColumnCount, workbook);
	}
	
	public void getQuotationReport(XSSFWorkbook workbook) {
		 logger.log(Level.SEVERE,"in getQuotationReport");
		createExcelData(quotationlist, quotationColumnCount, workbook);
	}


	public void getContractReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getContractReport");
		createExcelData(contractlist, contractColumnCount, workbook);
	}

	public void getServiceListReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getServiceListReport");
		createExcelData(servicelist, serviceColumnCount, workbook);
	}

	public void getBillingReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getBillingReport");
		createExcelData(Billinglist, billingColumnCount, workbook);
	}

	public void getInvoiceReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getInvoiceReport");
		createExcelData(invoicelist, invoiceColumnCount, workbook);
	}


	public void getPaymentReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getPaymentReport");
		createExcelData(summaryPaymentlist, summaryPaymentColumnCount, paymentlist, paymentColumnCount, workbook);
	}


	public void getCustomerReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getCustomerReport");
		createExcelData(customerlist, customerColumnCount, workbook);
	}


	public void getEmployeeReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getEmployeeReport");
		createExcelData(employeelist, employeeColumnCount, workbook);
	}


	public void getVendorReport(XSSFWorkbook workbook) {
		logger.log(Level.SEVERE,"in getVendorReport");
		createExcelData(vendorlist, vendorColumnCount, workbook);
	}
    
	
}
