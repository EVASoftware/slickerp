package com.slicktechnologies.server.addhocdownload;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.services.TxtService;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

@SuppressWarnings("serial")
public class TxtWriter extends RemoteServiceServlet implements TxtService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3924621461371627978L;
	public static ArrayList<PaySlip> paySlipList;
	
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1= new SimpleDateFormat("dd-MM-yyyy");
	private SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	
	DecimalFormat df = new DecimalFormat("0");
	
	HashMap<String, ArrayList<PaySlip>> statoturyReportMap;
	Logger logger = Logger.getLogger("NameOfYourLogger");
	
	public TxtWriter(){
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public PrintWriter getPFlist(PrintWriter writer) throws IOException {
		CsvWriter csv=new CsvWriter();
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Comparator<PaySlip> payslipComp=new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip arg0, PaySlip arg1) {
				// TODO Auto-generated method stub
				if(arg0.getUanNo()!=null&&arg1.getUanNo()!=null){
					return arg1.getUanNo().compareTo(arg0.getUanNo());
				}
				return 0;
			}
		};
		
		Collections.sort(paySlipList, payslipComp);
		PaySlip payslip=paySlipList.get(0);
		
		ArrayList<String> processNameList=new ArrayList<String>();
		processNameList.add("PfMaxFixedValue");
		processNameList.add("EsicForDirect");
		double pfMaxFixedValue=0;
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", payslip.getCompanyId()).filter("processName IN", processNameList).filter("configStatus", true).list();
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PfMaxFixedValue")&&ptDetails.isStatus()==true){
						pfMaxFixedValue=Double.parseDouble(ptDetails.getProcessType().trim());
					}
				}
			}
		}
		
		
		/**
		 * @author Anil , Date : 01-08-2019
		 */
		logger.log(Level.SEVERE, " statuteryReport SIZE : "+paySlipList.size());
		statoturyReportMap=new HashMap<String, ArrayList<PaySlip>>();
		for(PaySlip obj:paySlipList){
			String key=obj.getEmpid()+obj.getSalaryPeriod();
			if(statoturyReportMap!=null&&statoturyReportMap.size()!=0){
				if(statoturyReportMap.containsKey(key)){
					statoturyReportMap.get(key).add(obj);
				}else{
					ArrayList<PaySlip> list=new ArrayList<PaySlip>();
					list.add(obj);
					statoturyReportMap.put(key, list);
				}
			}else{
				ArrayList<PaySlip> list=new ArrayList<PaySlip>();
				list.add(obj);
				statoturyReportMap.put(key, list);
			}
		}
		logger.log(Level.SEVERE, " statuteryReport MAP SIZE : "+statoturyReportMap.size());
	
	int counter=0;
		
		for(Map.Entry<String, ArrayList<PaySlip>> entry:statoturyReportMap.entrySet()){
			ArrayList<PaySlip> paySlipList=entry.getValue();
			
			double grossWage=0;
			double pfWages=0;
			double empWages=0;
			
			for(PaySlip obj:paySlipList){
				pfWages=pfWages+csv.getPFwages(obj,true,false);
				empWages=empWages+csv.getPFwages(obj,false,true);
				grossWage=grossWage+obj.getGrossEarningWithoutOT();
			}
			PaySlip pay=paySlipList.get(0);
			
			counter++;
		
//		for (PaySlip pay : paySlipList) {
			if(pay.getUanNo()!=null){
				writer.append(pay.getUanNo()+"");
			}else{
				writer.append(" ");
			}
			writer.append("#~#");
			
			writer.append(pay.getEmployeeName() + "");
			writer.append("#~#");
			
			writer.append(df.format(pay.getGrossEarningWithoutOT())+ "");
			writer.append("#~#");
			
			/**
			 * @author Anil,Date : 01-07-2019
			 * picking pf wage amount from database 
			 * if not present then doing reverse calculation with the help of pf percent and pf amount
			 */
//			double pfWages=pay.getPFAmount()+pay.getPFArrearsAmount();
//			double pfWages=csv.getPFwages(pay, true, false);
//			if(pfMaxFixedValue!=0&&pfWages>pfMaxFixedValue){
//				pfWages=pfMaxFixedValue;
//			}
			writer.append(df.format(pfWages)+ "");
			writer.append("#~#");
			
			writer.append(df.format(pfWages)+ "");
			writer.append("#~#");
			
			writer.append(df.format(pfWages)+ "");
			writer.append("#~#");
			/**
			 * @author Anil,Date : 01-07-2019
			 * picking pf wage amount from database 
			 * if not present then doing reverse calculation with the help of pf percent and pf amount
			 */
//			double empWages=Math.round((pfWages*12)/100);
//			double empWages=csv.getPFwages(pay, false, true);
			writer.append(df.format(empWages)+ "");
			writer.append("#~#");
			
			double pensionCont=Math.round((pfWages*8.33)/100);
			writer.append(df.format(pensionCont)+ "");
			writer.append("#~#");
			
			double emprWages=Math.round((pfWages*3.67)/100);
			writer.append(df.format(emprWages)+ "");
			writer.append("#~#");
			
			writer.append(0+ "");
			writer.append("#~#");
			
			writer.append(0+ "");
			
			writer.append("\r\n");
		}
		return writer;
	}
	
	
	@Override
	public void setPaySliplist(ArrayList<PaySlip> paySlipList) {
		// TODO Auto-generated method stub
		this.paySlipList=paySlipList;
		
	}

}
