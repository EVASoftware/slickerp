package com.slicktechnologies.server.addhocdownload;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.utility.ServerAppUtility;

public class CreateCSVServlet extends HttpServlet {
	
	 
	private static final long serialVersionUID = 1L;
	


	public static final int CUSTOMER=1,QUOTATION=2,Contract=6,LEADS=3,EXPENSES=4,SERVICE=5,

			CALENDAR=7,LEAVEAPPLICATION=8,ADVANCE=9,COMPANY=20,EMPLOYEESHIFT=10,LEAVEBALANCE=11,TOOLSET=22,TOOL=23,CLIENTSIDEASSET=24
			,CTC=30,EMPLOYEE=40,VENDOR=41,PETTYCASHDEPOSITS=12,BILLINGLIST=14,INVOICELIST=15,PAYMENTLIST=16,COMPANYPAYMENTDETAILS=17,
			VENDORPRICELIST=42,REQUESTFORQUOTATION=43,LIAISON=44,LOI=45,PURCHASEORDER=46,PURCHASEREQUISITION=47,GRN=48,TIMEREPORT=54,SALESQUOTATION=60,SALESORDER=61,DELIVERYNOTE=62,
			INTERFACETALLYLIST=63,INTERACTIONTYPE=64,PRODUCTGROUPLIST=65,BILLOFMATERIAL=66,CONTACTPERSONIDENTIFICATION=67,MRN = 68,MIN=69,PRODUCTINVENTORYTRANSACTIONLIST=70,MMN=71,PIV=72,
			VENDORPAYMENTDETAILS=73,ITEMPRODUCT=74,SERVICEPRODUCT=75,APPROVAL=76,SALESSECURITYDEPOSIT=77,SERVICESECURITYDEPOSIT=78,
			DOCUMENTHISTORY=79,PRODUCTINVENTORYLIST=80,LOGGEDINHISTORY=81,CANCELSUMMARY=82,INSPECTION=83,BILLOFPRODUCTMATERIAL=84,VENDORPRICELISTDETAILS=85,WORKORDER=86,CUSTOMERBRANCHDETAILS=87,
			PHYSICALINVENTORY=88,STOCKALERT=89,MATERIALCONSUMPTIONREPORT=90,SMSHISTORY=91,FUMIGATION=92,EMPLOYEEADDITIONALDETAILS=93,FUMIGATIONAUSTRALIA=94,COMPLAIN=95,MATERIALINFO=96,
			CUSTHELPDESK=97,CUSTHELPDESKSERVICE=98,CUSTHELPDSKINVOICE=99,CUSTHELPDESKPAYMENT=100,CUSTHELPDESKLEAD=101,CUSTHELPDESKQUOTATION=102,
			SCHEDULEDSERVICES=103,UNSCHEDULEDSERVICES=104,PROCESSEDCONTRACTRENEWAL=105,UNPROCESSEDCONTRACTRENEWAL=106,SCHEDULINGGOOGLECALENDARFORMAT=107,PETTYCASHTRANSACTIONLIST=108,SALESPERSONPERFORMANCE=109, SALESPERSONTARGETACTUAL=110
			,COMPANYASSTLIST= 111,SERVICEPOLIST=112,ASSETFORMATDOWNLOAD=113,WAREHOUSELIST=114,STORAGELOCATIONLIST=115,STORAGEBINLIST=116,TECHNICIANDASHBOARD = 117,ASSESSMENTDETAILS=118,ROLEDEFINITION=119,MULTIPLEEXPENSE=120,USER=121,CLOSINGREPORT = 122, REVANUEREPORT = 123, TECHNICIANREPORT = 124,SALESPERSONSTAEGETS=125,
			CUSTOMERSUPPORT=126,CONTRACTREPORT=127, SALESREGISTER=128,SCHEDULINGLIST=129,/*Added by Nidhi*/MINERRORLIST=130,/*Added by Nidhi*/CONERRORLIST=131,UNPROCESSEDCONTRACTRENEWALREPORT=132,PROCESSEDCONTRACTRENEWALREPORT=133,CONTRACTNEW=134,ACCOUNTINGINGINTERFACENEW=135 , /** date 16.03.2018 komal **/ AMCASSUREDREVENUEREPORT=136,
			/**
			 *  nidhi
			 *  16-04-2018
			 *  for custom details list
			 */CUSTOMSERVICEDETAIL=137,CUSTOMGRNDETAIL=138,CUSTOMMINDETAIL=139,/** added by komal for hvac **/ CUSTOMERCALLLOG=140,
			 
					 /*** Date 22 June 2018 Giving 55 because already we are using 55 but didnt had any parameter for 55**/PAYSLIPREPORT=55,
			 /** date 9.5.2018 added by komal**/NEWTALLYINTERFACE=141,CUSTOMERBRANCHNEW=152,
			 /** date 9.7.2018 added by komal**/VENDORINVOICELIST=142,
			/**add by Jayshree**/STATUTERYPFREPORTPF=143,STATUTERYPFREPORTPT=144,STATUTERYPFREPORTEPIC=145,STATUTERYPFREPORTTDS=146,STATUTERYPFREPORTMWF=147,STATUTERYPFREPORTLWF=148,
			/** date 11.7.2018 added by komal**/ATTENDANCElIST=150,ATTENDANCEERROR=151,COMPLAINREPORT=153,SERVICEINVOICEMAPPING=154, 
			/** 22-11-2018  added by amol    * */DOWNLOADCNC=155,/**23-11-2018**added by amol**/DOWNLOADPROJECTALLOCATION=156,
            /**23-11-2018 added by amol**/   ASSETALLOCATIONDL=157,/**26-11-2018 added by amol**/ ADHOCDOWNLOAD=158, COMPANYCONTDL=159,
            /**30-11-2018 added by amol**/CTCTEMPLATEDL=160,PFHISTORYDL=161,VOLUNTRYPFHISTORY=162,

            /**1-12-2018 added by amol**/HRPROJECT=163,LEAVEBALANCEUPLOADERROR=164,RENEWCONTRACTREPORT = 165,UNALLOACATEDEMPLOYEE=166,EMPLOYEELEAVEHISTORY=167,
            /** Updated By: Viraj Date: 22-03-2019  **/ BILLEDUNBILLED=168,FORMT=169, BRANCHWISESPLITINVOICEREPORT = 170,STATUTERYREPORTFORMB=171,SALESREGISTERFORSL = 172,  /** Updated By: Viraj Date: 24-05-2019 Description: To generate csv file of sales register (Silver Lining) **/
            EMPLOYEEASSETERROR=173,FORMXIII=174,ATTENDANCEDOWNLOAD = 175,EMPLOYEEOVERTIMEHISTORY=176,LOCKSEALDEVIATIONREPORT=177,CUSTOMERBRANCHERROR=178,CUSTOMSERVICELOSSREVENUEDETAIL=179,APPDEREGISTOREHISTORY=180,INHOUSESERVICES=181,COMPLAINTWITHTAT=182
            ,PEDIOCONTRACT=183,VENDORPRODUCRPRICEUPLOADERROR=184,CTCTEMPLATEUPLOADERROR=185,UPDATEEMPLOYEEERROR=186,UPLOADASSETDETAILSERROR=187,UPDATECATEGORYERROR=188,UPDATETYPEERROR=189,PAYMENTUPLOADERROR=190,CITYERROR=191,LOCALITYERROR=192,TallyPaymentUploader=193,CustomerWithContractFileUpload=194,
            EMPLOYEEUPLOAD=195,SERVICEPRODUCTUPLOAD=196,ITEMPRODUCTUPLOADERROR=197,CTCALLOCATIONUPLOADERROR=198,CREDITNOTEUPOADERROR=199,CREDITNOTELIST=200,InHouseServiceUploadErrorList=201,INHOUSENOSTACKSERVICES=202,CRMTALLYREG=203,STOCKUPLOADERROR=204,CONSUMABLESERROR=205,
	         /**@sheetal for user upload**/USERUPLOAD=206,
	         /**@author Sheetal : 25-05-2022,for Oracle format of Envocare**/
	        		 ORACLESHEET1=207,ORACLESHEET2=208,ORACLESHEET3=209,ORACLESHEET4=210,
	   /**@author Sheetal : 02-06-2022 , for customer branch with service location errorlist***/
	CUSTOMERBRANCHWITHSERVICELOCATION=211,SERVICECREATIONUPLOAD=212,SMSTEMPLATELIST=213,ULTRATALLYEXPORT=214,SALARYSLIPREPORT2=215,SERVICECANCELLATIONERRPRLIST=216,ATTENDANCEREPORTDOWNLOADWORKEDDAYFORMAT=217,CUSTOMERBRANCHACTIVATEDEACTIVATEERRORFILE=218,
			downloadForScheduleOrCompleteViaExcel=219,ScheduleOrCompleteViaExcelErrorReport=220,UltimaTallyExport=221,ReverServiceCompletion=222;

	HttpServletResponse responseref;
	PrintWriter pw;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1= new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException 
	  {
	  
		/**
		 * @author Anil @since 12-06-2021
		 * Thai character was not getting download in proper format
		 * raised by Rahul Tiwari
		 */
//		response.setContentType("application/csv");
		
		response.setContentType("application/csv; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        
		 responseref=response;
		 
	     pw=response.getWriter(); 
	     String reqtype=request.getParameter("type");
	     System.out.println("Type is "+reqtype);
	     
	     int type=Integer.parseInt(reqtype);
//	     getcsv(type);
	     // Date 07-01-2017 added request parameter by vijay for new contract and contract renewal download as per filter
	     getcsv(type,request);
	     
	     pw.flush();
	     pw.close();
	  }
	
	public void getcsv(int type, HttpServletRequest request) throws IOException 
	{
		 CsvWriter csv=new CsvWriter();
		 Date d=new Date();
		 String dateString = fmt.format(d);
	
		switch(type)
		{
			case CUSTOMER:
				            responseref.setHeader("Content-Disposition", "Attachment;filename=Customer Report "+dateString+".csv"
				            		);
							csv.getCustomerCSV(pw);
							break;
							
							
			case QUOTATION:
				 responseref.setHeader("Content-Disposition", "Attachment;filename=Quotation Report "+dateString+".csv"
		            		);
							csv.getQuotationCSV(pw);
							break;
				
			case LEADS:
				 responseref.setHeader("Content-Disposition", "Attachment;filename=Lead Report "+dateString+".csv"
		            		);
							csv.getleadCSV(pw);
							break;
							
			case EXPENSES:
				 responseref.setHeader("Content-Disposition", "Attachment;filename=Expense Report "+dateString+".csv"
		            		);
							csv.getexpenseCSV(pw);
							break;
			case SERVICE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Report "+dateString+".csv"
	            		);
						csv.getServiceCSV(pw);
						break;
			case Contract:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Contract Report "+dateString+".csv"
	            		);
						csv.getContractCSV(pw);
						break;

			case COMPANY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Registered Company Report "+dateString+".csv"
	            		);
						csv.getCompanyCSV(pw);
						break;

			case ADVANCE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Advance Report "+dateString+".csv"
	            		);
						csv.getAdvaneCSV(pw);
						break;

			case LEAVEAPPLICATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Leave Application Report "+dateString+".csv"
	            		);
						csv.getLeaveApplicationCSV(pw);
						break;
						
			case EMPLOYEESHIFT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Leave Application Report "+dateString+".csv");
				csv.getEmployeeShiftCSV(pw);
				break;
			case CTC:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CTC Report "+dateString+".csv");
				csv.getCTC(pw);
				break;
			
			case LEAVEBALANCE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Leave Balance Report "+dateString+".csv");
				csv.getLeaveBalanceCSV(pw);
				break;
			case TOOLSET:
				responseref.setHeader("Content-Disposition", "Attachment;filename=ToolSet Report "+dateString+".csv");
				csv.getToolSetCSV(pw);
				break;
			case TOOL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Tool Report "+dateString+".csv");
				csv.getToolCSV(pw);
				break;
			case CLIENTSIDEASSET:
				responseref.setHeader("Content-Disposition", "Attachment;filename=ClientSide Asset Report "+dateString+".csv");
				csv.getClientsideAssetToolCSV(pw);
				break;
				
			case CALENDAR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Calendar "+dateString+".csv");
				csv.createCalendarCSV(pw);
				break;
				

			case EMPLOYEE:
							responseref.setHeader("Content-Disposition", "Attachment;filename=Employee Report "+dateString+".csv");
							csv.getEmployeeCSV(pw);
							break;
			case VENDOR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Vendor Report "+dateString+".csv");
				csv.createVendor(pw);
				break;
				
			case VENDORPRICELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Product List "+dateString+".csv");
				csv.getVendorProductCSV(pw);
				break;
				
			case REQUESTFORQUOTATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Request For Quotation "+dateString+".csv");
				csv.getRequestQuotationCSV(pw);
				break;

			case LIAISON:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Liaison "+dateString+".csv");
				csv.getLiaisonCSV(pw);
				break;
				
			case PETTYCASHDEPOSITS:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Petty Cash Deposits "+dateString+".csv");
				csv.getPettyCashCSV(pw);
				break;
	
			case BILLINGLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Billing List Report "+dateString+".csv");
//				csv.getBillingListCSV(pw);
				/*** Date 11-10-2018 By Vijay to seperate NBHC and Standard Download ****/
				csv.getBillingListReport(pw);
				break;
			
			case INVOICELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Invoice List Report "+dateString+".csv");
				//csv.getInvoiceListCSV(pw);
				/** date 07-02-2018 added by komal to seperate nbhc and normal invoice download **/
				csv.checkInvoiceReport(pw);
				break;
				
			case PAYMENTLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Payment List Report "+dateString+".csv");
				csv.getPaymentListCSV(pw);
				break;
				
			case SALESQUOTATION:
				 responseref.setHeader("Content-Disposition", "Attachment;filename=Quotation Report "+dateString+".csv");
							csv.getSalesQuotationCSV(pw);
							break;
			
			case SALESORDER:
				responseref.setHeader("Content-Disposition","Attachment;filename=Sales Order Report " + dateString+ ".csv");
				csv.getSalesOrderCSV(pw);
				break;	
				
			case DELIVERYNOTE:
				responseref.setHeader("Content-Disposition","Attachment;filename=Delivery Note Report " + dateString+ ".csv");
				csv.getDeliveryNoteCSV(pw);
				break;	
				
			case LOI:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Letter of Intent "+dateString+".csv");
				csv.getLOICSV(pw);
				break;
			case PURCHASEORDER:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Purchase Order "+dateString+".csv");
				csv.getPOCSV(pw);
				break;
			case PURCHASEREQUISITION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Purchase Requistion "+dateString+".csv");
				csv.getPRCSV(pw);
				break;
			case GRN:
				responseref.setHeader("Content-Disposition", "Attachment;filename=GRN Details"+dateString+".csv");
				csv.getGRNCSV(pw);
				break;
			case COMPANYPAYMENTDETAILS:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Company Payment Report "+dateString+".csv");
				csv.getCompanyPaymentDetailsCSV(pw);
				break;
			case INTERFACETALLYLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Interface Tally Report "+dateString+".csv");
				csv.getInterfaceTallyDetailsCSV(pw);
//				/** date 12-02-2018 added by komal for nbhc tally interface report download **/
//				csv.checkTallyInterfaceReport(pw);
				break;
			case INTERACTIONTYPE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Interaction Report "+dateString+".csv");
				csv.getInteractionCSV(pw);
				break;
			case PRODUCTGROUPLIST:
				responseref.setHeader("Content-Disposition", "Attchment;filename=Product Group List"+dateString+".csv");
				csv.getProductGrpListCSV(pw);
				break;	
			case BILLOFMATERIAL:
				responseref.setHeader("Content-Disposition", "Attchment;filename=Bill Of Material"+dateString+".csv");
				csv.getBillOfMaterialCSV(pw);
				break;	
			case CONTACTPERSONIDENTIFICATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Contact Person Report "+dateString+".csv");
				csv.getContactPersonTypeCSV(pw);
				break;
			case MRN:
				responseref.setHeader("Content-Disposition",
						"Attachment;filename=MRN Details" + dateString + ".csv");
				csv.getMaterialRequestNote(pw);
				break;
			case MIN:
				responseref.setHeader("Content-Disposition",
						"Attachment;filename=MIN Details" + dateString + ".csv");
				csv.getMaterialIssueNote(pw);
				break;
			case PRODUCTINVENTORYTRANSACTIONLIST:
				responseref.setHeader("Content-Disposition",
						"Attachment;filename=Product Inventory Transaction Details" + dateString + ".csv");
				csv.getProductInventoryTransaction(pw);
				break;
			case MMN:
				responseref.setHeader("Content-Disposition",
						"Attachment;filename=MMN Details" + dateString + ".csv");
				csv.getMaterialMovementNote(pw);
				break;
			case PIV:
				responseref.setHeader("Content-Disposition",
						"Attachment;filename=PIV Details" + dateString + ".csv");
				csv.getProductInventoryView(pw);
				break;
			case VENDORPAYMENTDETAILS:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Vendor Payment Mode Report "+dateString+".csv");
				csv.getVendorPaymentDetailsCSV(pw);
				break;
				
			case SERVICEPRODUCT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Product Report "+dateString+".csv");
				csv.getServiceProductCSV(pw);
				break;
				
			case ITEMPRODUCT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Item Product Report "+dateString+".csv");
				csv.getItemProductCSV(pw);
				break;
				
			case APPROVAL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Approval List"+dateString+".csv");
				csv.getApprovalListCSV(pw);
				break;		
				
			case SALESSECURITYDEPOSIT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Sales Security Deposit List"+dateString+".csv");
				csv.getSalesSecurityDepositCSV(pw);
				break;		

				
			case SERVICESECURITYDEPOSIT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Security Deposit List"+dateString+".csv");
				csv.getServiceSecurityDepositCSV(pw);
				break;	
				
			case DOCUMENTHISTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Document History List"+dateString+".csv");
				csv.getDocumentHistoryCSV(pw);
				break;	
				

			case PRODUCTINVENTORYLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Product Inventory View List"+dateString+".csv");
				csv.getProductInventoryListCSV(pw);
				break;	
				
			case LOGGEDINHISTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Logged In History Details"+dateString+".csv");
				csv.getLoggedInHistoryDetailsCSV(pw);
				break;

			case CANCELSUMMARY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Cancellation Summary"+dateString+".csv");
				csv.getCancelSummaryCSV(pw);
				break;
				
			case INSPECTION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Inspection Details"+dateString+".csv");
				csv.getInspectionDetails(pw);
				break;

			case BILLOFPRODUCTMATERIAL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Bill Of Material"+dateString+".csv");
				csv.getBillOfProductMaterialListDetails(pw);
				break;
				
			case VENDORPRICELISTDETAILS:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Vendor Price List Details"+dateString+".csv");
				csv.getVendorPriceListDetails(pw);
				break;
				
			case WORKORDER:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Work Order"+dateString+".csv");
				csv.getWorkOrderListDetails(pw);
				break;

			case TIMEREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Time Report"+dateString+".csv");
				csv.getTimeReportcsv(pw);
				break;

			case CUSTOMERBRANCHDETAILS:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Customer Branch Details"+dateString+".csv");
				csv.getCustomerBranchDetails(pw);
				break;
				
			case PHYSICALINVENTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Physical Inventory Report"+dateString+".csv");
				csv.getPhysicalInventoryListDetails(pw);
				break;
				
			case STOCKALERT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Stock Alert List"+dateString+".csv");
				csv.getStockAlertListDetails(pw);
				break;

			case MATERIALCONSUMPTIONREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Material Consumption Report"+dateString+".csv");
				csv.getAllInOneReport(pw);
				break;
				
			case SMSHISTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=SMS History list"+dateString+".csv");
				csv.getSMSHistoryCSV(pw);
				break;
				
			case FUMIGATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Fumigation Report"+dateString+".csv");
				csv.getFumigationReport(pw);
				break;
				
			case EMPLOYEEADDITIONALDETAILS:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Employee Additional Details"+dateString+".csv");
				csv.getEmpAddDerListDetails(pw);
				break;	
			case FUMIGATIONAUSTRALIA:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Fumigation Australia"+dateString+".csv");
				csv.getFumigationAusDetails(pw);
				break;
				
			case COMPLAIN:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Complains"+dateString+".csv");
				csv.getComplainDetails(pw);
				break;	
				
			case MATERIALINFO:
				responseref.setHeader("Content-Disposition", "Attachment;filename=MaterialInfo"+dateString+".csv");
				csv.getMaterialInfo(pw);
				break;	
				
				
			case CUSTHELPDESK:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CustomerHelpDesk(Contract)"+dateString+".csv");
				csv.getCustomerHeplDeskCSV(pw);
				break;		
		
			case CUSTHELPDESKSERVICE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CustomerHelpDesk(Service)"+dateString+".csv");
				csv.getCustomerHeplDeskServiceCSV(pw);
				break;	
				
			case CUSTHELPDSKINVOICE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CustomerHelpDesk(Invoice)"+dateString+".csv");
				csv.getCustomerHeplDeskInvoiceCSV(pw);
				break;	
				
			case CUSTHELPDESKPAYMENT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CustomerHelpDesk(Payment)"+dateString+".csv");
				csv.getCustomerHeplDeskPaymentCSV(pw);
				break;	
				
			case CUSTHELPDESKLEAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CustomerHelpDesk(Lead)"+dateString+".csv");
				csv.getCustomerHeplDeskLeadCSV(pw);
				break;	
				
			case CUSTHELPDESKQUOTATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CustomerHelpDesk(Quotation)"+dateString+".csv");
				csv.getCustomerHeplDeskQuotationCSV(pw);
				break;	
		
			case SCHEDULEDSERVICES:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Scheduled Services"+dateString+".csv");
				csv.getScheduledServicesDetails(pw);
				break;
				
			case UNSCHEDULEDSERVICES:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Unscheduled Services"+dateString+".csv");
				csv.getScheduledServicesDetails(pw);
				break;
				
				
			case PROCESSEDCONTRACTRENEWAL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Processed Contract Renewal"+dateString+".csv");
				csv.getContractRenewalsDetails(pw);
				break;
				
			case UNPROCESSEDCONTRACTRENEWAL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Unprocessed Contract Renewal"+dateString+".csv");
				csv.getContractRenewalsDetails(pw);
				break;
				
			case SCHEDULINGGOOGLECALENDARFORMAT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Scheduled Services In Google Calendar Format"+dateString+".csv");
				csv.getScheduledServicesInGoogleCaledarFormat(pw);
				break;	
				
			case PETTYCASHTRANSACTIONLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Pettycash TransactionList"+dateString+".csv");
				csv.getpettycashTransactionList(pw);
				break;	
				
			case SALESPERSONPERFORMANCE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=SalesPerson Performance"+dateString+".csv");
				csv.getSalesPersongPerformanceAllDetails(pw);
				break;
				
			case SALESPERSONTARGETACTUAL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=SalesPerson Target Performance"+dateString+".csv");
				csv.getSalesPersonTargetPerformanceAllDetails(pw);
				break;
				
			case COMPANYASSTLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Company Asset List"+dateString+".csv");
				csv.getCompanyAssetList(pw);
				break;
				
			case TECHNICIANDASHBOARD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Technician Information"+dateString+".csv");
				csv.getTechnicianAllDetails(pw);
				break;	
				
//			case ASSESSMENTDETAILS:
//				responseref.setHeader("Content-Disposition", "Attachment;filename=Assessment Report"+dateString+".csv");
//				csv.getassessmentDetails(pw);
//				break;	
				
				
			case SERVICEPOLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Serive Po List"+dateString+".csv");
				csv.getServicePoList(pw);
				break;
			case ASSETFORMATDOWNLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Asset Format "+dateString+".csv");
				csv.getAssetFormatList(pw);
				break;
			case WAREHOUSELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Warehouse "+dateString+".csv");
				csv.getWarehouseList(pw);
				break;
			case STORAGELOCATIONLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Storage Location "+dateString+".csv");
				csv.getStorageLocationList(pw);
				break;
			case STORAGEBINLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Storage Bin "+dateString+".csv");
				csv.getStorageBinList(pw);
				break;
				
			case ROLEDEFINITION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Role Definition "+dateString+".csv");
				csv.getRoleDefintionList(pw);
				break;
				
			case MULTIPLEEXPENSE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=MultiExpense "+dateString+".csv");
				csv.getMultiplExpenseList(pw);
				break;
				
			case USER:
				responseref.setHeader("Content-Disposition", "Attachment;filename=User "+dateString+".csv");
				csv.getUserList(pw);
				break;	
			
			case CLOSINGREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Closing Stock Report "+dateString+".csv");
				csv.getClosingStockReport(pw);
				break;
			case REVANUEREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Revenue Report "+dateString+".csv");
				csv.getRevanueServiceList(pw);
				break;
			case TECHNICIANREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Technician Revenue Report "+dateString+".csv");
				csv.getTechnicalServiceList(pw);
				break;
				/** Date 03/09/2017 By Jayshree to download 
				 * the sales person target report
				 */
			case SALESPERSONSTAEGETS:
				responseref.setHeader("Content-Disposition","Attachment;filename=Sales Persons Target Report"+dateString+".csv");
				csv.getSalesPersonTargetReport(pw);
				break;
				/**
				 * Date : 24-10-2017 BY ANIL
				 * New customer support download for NBHC
				 */
			case CUSTOMERSUPPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Complains"+dateString+".csv");
				csv.getNewComplainDetails(pw);
				break;	
				/**
				 * Date : 24-10-2017 BY ANIL
				 * New contract report download for NBHC
				 */
			case CONTRACTREPORT:
				responseref.setHeader("Content-Disposition","Attachment;filename=Contract Report " + dateString+ ".csv");
				
				/**
				 * Date 06-01-2017 added by vijay for contract download static list issue
				 * data will be download as per filters
				 */
				String reqQuerry = request.getParameter("querry");
			    String reqValue = request.getParameter("value");
				csv.getNewContractCSV(pw,reqQuerry,reqValue);
				
				break;
				/**
				 * Date : 28-10-2017 BY SAGAR
				 */
			case SALESREGISTER:
				responseref.setHeader("Content-Disposition","Attachment;filename=Sales Register " + dateString+ ".csv");
				csv.getSummaryPaymentListCSV(pw);
				break;
				//komal
			case SCHEDULINGLIST:
				responseref.setHeader("Content-Disposition","Attachment;filename=Scheduling " + dateString+ ".csv");
				pw.write(CsvWriter.schedulingStr);
				break;
				
			case MINERRORLIST:
				responseref.setHeader("Content-Disposition","Attachment;filename=MIN Upload Process " + dateString+ ".csv");
				csv.setErrorListDetails(pw);
				break;
			
			case CONERRORLIST:
				responseref.setHeader("Content-Disposition","Attachment;filename=Contract Upload Process " + dateString+ ".csv");
				csv.setErrorListDetails(pw);
				break;
				
			case UNPROCESSEDCONTRACTRENEWALREPORT:
				 String strreqQuerry = request.getParameter("querry");
			     String strreqValue = request.getParameter("value");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Unprocessed Contract Renewal"+dateString+".csv");
				csv.getNewContractRenewalsUnprocessedDetails(pw,strreqValue,strreqQuerry);
				break;
				
			case PROCESSEDCONTRACTRENEWALREPORT:
				 String strQuerry = request.getParameter("querry");
			     String strValue = request.getParameter("value");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Processed Contract Renewal"+dateString+".csv");
				csv.getNewContractRenewalsProcessedDetails(pw,strValue,strQuerry);
				break; 
				
			case CONTRACTNEW:
				 String stringQuerry = request.getParameter("querry");
			     String stringValue = request.getParameter("value");
				responseref.setHeader("Content-Disposition","Attachment;filename=Contract Data " + dateString+ ".csv");
				csv.getContractNewCSV(pw,stringValue,stringQuerry);
				break; 
				
			case ACCOUNTINGINGINTERFACENEW:
				 String querry = request.getParameter("querry");
			     String value = request.getParameter("value");
			     responseref.setHeader("Content-Disposition","Attachment;filename=Accounting Interface Data " + dateString+ ".csv");
				 csv.getAccountingInterfaceNewCSV(pw,value,querry);
				break;
				/** date 16.03.2018 added by komal 	for nbhc(amc assured revenue report)**/
			case AMCASSUREDREVENUEREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Amc Assured Revenue"+dateString+".csv");
				csv.getAMCAssuredRevenueReport(pw);
				break;
				
			case CUSTOMSERVICEDETAIL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Details List"+dateString+".csv");
				csv.getCustomeServiceDetailListCSV(pw);
				break;
			
			case CUSTOMSERVICELOSSREVENUEDETAIL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Reveneu Details List"+dateString+".csv");
				csv.getCustomeServiceRevenueLossDetailListCSV(pw);
				break;
				
				
				
			case CUSTOMGRNDETAIL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=GRN Details List"+dateString+".csv");
				csv.getCustomGrnListCSV(pw);
				break;
			
			case CUSTOMMINDETAIL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=MIN Details List"+dateString+".csv");
				csv.getCustomMaterialIssueNoteList(pw);
				break;
				//komal
			case CUSTOMERCALLLOG :
				responseref.setHeader("Content-Disposition","Attachment;filename=Customer Call Log " + dateString+ ".csv");
				csv.getCustomerLog(pw);
				break;
				/** date 19/03/2018 added by komal  for tally interface download **/
			case NEWTALLYINTERFACE:
				dateString = fmt1.format(d);
				responseref.setHeader("Content-Disposition","Attachment;filename=Tally Interface " + dateString+ ".csv");
				csv.getcNewTallyInterfaceCSV(pw);
				break;
				
			/**
			 * Date 05-07-2018 by Vijay
			 * Des :- Customer branch download for NBHC with process config
			 */	
			case CUSTOMERBRANCHNEW:
				dateString = fmt1.format(d);
				responseref.setHeader("Content-Disposition", "Attachment;filename=Customer Branch Details"+dateString+".csv");
				csv.getCustomerBranchDetailsNew(pw);
				break;
			/**
			 * ends here	
			 */
				
				/**Rahul Verma added this. To generate pay slip report*/
			case PAYSLIPREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Pay Slip Report"+dateString+".csv");
				csv.getPaySklipcsv(pw);
				break;
				
				/** DATE 9.7.2018 ADDED BY KOMAL FOR VENDOR INVOICE DOWNLOAD **/
			case VENDORINVOICELIST:
				responseref.setHeader("Content-Disposition","Attachment;filename=Vendor Invoice List " + dateString+ ".csv");
				csv.getVendorInvoiceData(pw);
				break;
				/**
				 * Date 10-7-2018 
				 * by jayshree
				 */
			case STATUTERYPFREPORTPF:
				System.out.println("inside report");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Statutory PF Report"+dateString+".csv");
				csv.getPFlist(pw);
				break;
				
			case STATUTERYPFREPORTPT:
				System.out.println("inside report");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Statutory PT Report"+dateString+".csv");
				csv.getPtlist(pw);
				break;
				
			case STATUTERYPFREPORTEPIC:
				System.out.println("inside report");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Statutory ESIC Report"+dateString+".csv");
				csv.getESIClist(pw);
				break;
				
			case STATUTERYPFREPORTTDS:
				System.out.println("inside report");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Statutory TDS Report"+dateString+".csv");
				csv.getTDSClist(pw);
				break;
				
			case STATUTERYPFREPORTMWF:
				System.out.println("inside report");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Statutory MWF Report"+dateString+".csv");
				csv.getMWFClist(pw);
				break;
				
			case STATUTERYPFREPORTLWF:
				System.out.println("inside report");
				responseref.setHeader("Content-Disposition", "Attachment;filename=Statutory LWF Report"+dateString+".csv");
				csv.getlWFClist(pw);
				break;
				
				//ENd
				/** date 11.7.2018 added by komal FOR ATTENDANCE DOWNLOAD**/
			case ATTENDANCElIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Attendance Report"+dateString+".csv");
				csv.getAttendanceReport(pw,1);
				break;
			case ATTENDANCEERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Attendance Error "+dateString+".csv");
				csv.getAttendanceError(pw);
				break;
				
			case COMPLAINREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Plan Report "+dateString+".csv");
				csv.getComplainReport(pw);
			break;
		
			case SERVICEINVOICEMAPPING:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Invoice Mapping Report "+dateString+".csv");
				csv.createServiceInvoiceMapping(pw);
				break;
				
					/** 22-11-2018 added by amol for download CNC **/
			case  DOWNLOADCNC:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CNC"+dateString+".csv");
				csv.getCNC(pw);
				break;
				
				
			case DOWNLOADPROJECTALLOCATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=projectallocation"+dateString+".csv");
				csv.getprojectallocation(pw);
				break;
				
			case ASSETALLOCATIONDL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=assetallocation"+dateString+".csv");
				csv.getassetallocation(pw);
				break;
				
			case ADHOCDOWNLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=adhocsettlement"+dateString+".csv");
				csv.getadhocsettlement(pw);
				break;
				
			case COMPANYCONTDL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=companycontribution"+dateString+".csv");
				csv.getcompanycontribution(pw);
				break;
				
			case CTCTEMPLATEDL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CTCtemplate"+dateString+".csv");
				csv.getctctemplate(pw);
				break;
			case PFHISTORYDL:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Voluntarypf"+dateString+".csv");
				csv.getvoluntrypf(pw);
				break;
			case VOLUNTRYPFHISTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=VoluntrypfHistory"+dateString+".csv");
				csv.getvoluntrypfhistory(pw);
				break;
				
			case HRPROJECT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=HRproject"+dateString+".csv");
				csv.gethrproject(pw);
				break;
				/**
				 * Date : 25-12-2018 By ANIL
				 */
			case LEAVEBALANCEUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=EmployeeLeaveBalance"+dateString+".csv");
				csv.getEmployeeLeaveBalance(pw);
				break;
			case RENEWCONTRACTREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=RenewalContractReport"+dateString+".csv");
				csv.getRenewalsContractDetails(pw);
				break;	
				
			case UNALLOACATEDEMPLOYEE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=UnaAllocatedEmployee"+dateString+".csv");
				csv.getUnallocatedEmployee(pw);
				break;
				
			case EMPLOYEELEAVEHISTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename=EmployeeLeaveHistory"+dateString+".csv");
				csv.getEmployeeLeaveHistory(pw);
				break;
			/**
			 * @author Anil ,Date : 01-04-2019
			 * Form T
			 */
			case FORMT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Form T"+dateString+".csv");
				csv.getForm_T_Details(pw);
				break;
				
			case BILLEDUNBILLED:
				responseref.setHeader("Content-Disposition", "Attachment;filename=billedUnbilled"+dateString+".csv");
				csv.getBilledUnbilled(pw);
				break;
				/**date 19.4.2019 added by komal to download branchwise split invoice report**/
			case BRANCHWISESPLITINVOICEREPORT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Invoice Report"+dateString+".csv");
				csv.getBranchwiseSplitInvoiceReportListCSV(pw);
				break;
				case STATUTERYREPORTFORMB:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Statutory FormB"+dateString+".csv");
				csv.getFormBDetails(pw);
				break;	
				
			case SALESREGISTERFORSL:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Sales Register"+dateString+".csv");
				csv.getSalesRegSL(pw);
				break;
			case EMPLOYEEASSETERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Employee Asset Upload Alert "+dateString+".csv");
				csv.getEmployeeAssetError(pw);
				break;
	case FORMXIII:
				responseref.setHeader("Content-Disposition", "Attachment;filename= FORM XIII"+dateString+".csv");
				csv.getFormXIII(pw);
				break;
				
		/** DATE 25/07/2019  ADDED BY komal to download attendance **/
			case ATTENDANCEDOWNLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Attendance List"+dateString+".csv");
				csv.getAttendanceList(pw);
				break;			
         	case EMPLOYEEOVERTIMEHISTORY:
					responseref.setHeader("Content-Disposition", "Attachment;filename= EmployeeOvertimeHistory"+dateString+".csv");
					csv.getEmployeeOvertimeHistory(pw);
					break;
			case LOCKSEALDEVIATIONREPORT:
         		responseref.setHeader("Content-Disposition", "Attachment;filename= Lock Seal Deviation"+dateString+".csv");
				csv.getLockSealDeviationReport(pw);
				break;		
				/** date 12/09/2019 added by komal to download customer branch error **/
			case CUSTOMERBRANCHERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename= Customer Branch Error"+dateString+".csv");
				csv.getCustomerBranchError(pw);
				break;
				
			case APPDEREGISTOREHISTORY:
				responseref.setHeader("Content-Disposition", "Attachment;filename= App Deregister History"+dateString+".csv");
				csv.getRegisteredDeviceDetails(pw);
				break;	
				
			case INHOUSESERVICES:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Details List"+dateString+".csv");
				csv.getInHouServiceDetailListCSV(pw);
				break;
			/**
			 * @author Anil
			 * @since 04-05-2020
			 * premium tech
			 */
			case COMPLAINTWITHTAT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Complains"+dateString+".csv");
				csv.getComplaintDetailsWithTat(pw);
				break;	
			/**
			 * @author Anil
			 * @since 12-06-2020
			 * pedio contract download
			 */
			case PEDIOCONTRACT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Contract Report "+dateString+".csv");
				csv.getPedioContractCSV(pw);
				break;
				
				
			case VENDORPRODUCRPRICEUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Vendor Price Upload Error Report "+dateString+".csv");
				csv.getVendorProductPriceUpload(pw);
				break;
				
			case CTCTEMPLATEUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CTC Template Upload Error Report "+dateString+".csv");
				csv.getCTCTemplateUpload(pw);
				break;
				
			case UPDATEEMPLOYEEERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Update Employee Upload Error Report "+dateString+".csv");
				csv.getEmployeeUpdateUpload(pw);
				break;
				
				
			case UPLOADASSETDETAILSERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Update Asset Details Upload Error Report "+dateString+".csv");
				csv.getAssetDetailsUpload(pw);
				break;
				
            	case UPDATECATEGORYERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Category Upload Error Report "+dateString+".csv");
				csv.getCategoryUploadError(pw);
				break;
			
			case UPDATETYPEERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Type Upload Error Report "+dateString+".csv");
				csv.getTypeUploadError(pw);
				break;
				
			case PAYMENTUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Payment Upload Error Report "+dateString+".csv");
				csv.getPaymentUploadError(pw);
				break;
				
	          case CITYERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=City Upload Error Report "+dateString+".csv");
				csv.getCityUploadError(pw);
				break;
				
			case LOCALITYERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Locality Upload Error Report "+dateString+".csv");
				csv.getLocalityUploadError(pw);
				break;	
			
			case TallyPaymentUploader:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Payment Upload Error Report "+dateString+".csv");
				csv.getTallyPaymentUploadError(pw);
				break;
				
			case CustomerWithContractFileUpload:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Customer With Service Details Upload Error Report "+dateString+".csv");
				csv.getCustomerWithContractUploadError(pw);
				break;
				
			case EMPLOYEEUPLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Employee Upload Error Report "+dateString+".csv");
				csv.getEmployeeUploadError(pw);
				break;
				
			case SERVICEPRODUCTUPLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Product Upload Error Report "+dateString+".csv");
				csv.getServiceProductUploadError(pw);
				break;	
				
			case ITEMPRODUCTUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Item Product Upload Error Report "+dateString+".csv");
				csv.getItemProductUploadError(pw);
				break;		
				
			case CTCALLOCATIONUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=CTC Allocation Upload Error Report "+dateString+".csv");
				csv.getCTCAllocationUploadError(pw);
				break;	
				
			case CREDITNOTEUPOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Credit Note Upload Error Report "+dateString+".csv");
				csv.getCreditNoteUploadError(pw);
				break;	
				
			case CREDITNOTELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Credit Note List "+dateString+".csv");
				csv.getCreditNoteList(pw);
				break;	
				
			case InHouseServiceUploadErrorList:
				responseref.setHeader("Content-Disposition", "Attachment;filename=InHouse Service Upload List "+dateString+".csv");
				csv.getInhouseServicecUploadError(pw);
				break;
				
			case INHOUSENOSTACKSERVICES:
				responseref.setHeader("Content-Disposition", "Attachment;filename=InHouse Service Upload List "+dateString+".csv");
				csv.getInhouseNoStackServiceError(pw);
				break;
				
			case CRMTALLYREG:
				responseref.setHeader("Content-Disposition","Attachment;filename=CRM Tally Report " + dateString+ ".csv");
				csv.checkCrmInvoiceReport(pw);
				break;
				
			case STOCKUPLOADERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Stock Upload Error Report "+dateString+".csv");
				csv.getStockUploadError(pw);
				break;
			case CONSUMABLESERROR:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Upload Error Report "+dateString+".csv");
				csv.getUploadError(pw);
				break;
			 /**@Sheetal : 31-01-2022
			     For User Upload***/
			case USERUPLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=User Upload Error Report "+dateString+".csv");
				csv.getUserUploadErrorlist(pw);
				break;
				
			case ORACLESHEET1:
				responseref.setHeader("Content-Disposition", "Attachment;filename=RA_INTERFACE_LINES_ALL"+dateString+".csv");
				csv.getRA_INTERFACE_LINES_ALL(pw);
				break;
				
			case ORACLESHEET2:
				responseref.setHeader("Content-Disposition", "Attachment;filename=RA_INTERFACE_DISTRIBUTIONS_ALL"+dateString+".csv");
				csv.getRA_INTERFACE_DISTRIBUTIONS_ALL(pw);
				break;
			    
				
			case ORACLESHEET3:
				responseref.setHeader("Content-Disposition", "Attachment;filename=RA_INTERFACE_SALESCREDITS_ALL"+dateString+".csv");
				csv.getRA_INTERFACE_SALESCREDITS_ALL(pw);
				break;
				
			case ORACLESHEET4:
				responseref.setHeader("Content-Disposition", "Attachment;filename=AR_INTERFACE_CONTS_ALL"+dateString+".csv");
				csv.getAR_INTERFACE_CONTS_ALL(pw);
				break;
				
			case CUSTOMERBRANCHWITHSERVICELOCATION:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Customer Branch upload with location and area Error Report"+dateString+".csv");
				csv.getCustomerbranchUploadWithLocationError(pw);
				break;
				
			case SERVICECREATIONUPLOAD:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service Creation Upload Error Report"+dateString+".csv");
				csv.getServiceCreationUploadError(pw);
				break;	
				
			case SMSTEMPLATELIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Communication Template Report"+dateString+".csv");
				csv.getSMSTemaplateReport(pw);
				break;	
			case ULTRATALLYEXPORT:
				dateString = fmt1.format(d);
				responseref.setHeader("Content-Disposition","Attachment;filename=Tally Interface " + dateString+ ".csv");
				csv.getUltraTallyInterfaceCSV(pw);
				break;
				
			case SALARYSLIPREPORT2:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Pay Slip Report"+dateString+".csv");
				csv.getsalarySlipReport(pw);
				break;
				
			case ATTENDANCEREPORTDOWNLOADWORKEDDAYFORMAT:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Attendance Report"+dateString+".csv");
				csv.getAttendanceReport(pw,2);
				break;
				
			case SERVICECANCELLATIONERRPRLIST:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Service cancellation Error Report"+dateString+".csv");
				csv.getServiceCancellationErrorReport(pw);
				break;
				
			case CUSTOMERBRANCHACTIVATEDEACTIVATEERRORFILE:
				responseref.setHeader("Content-Disposition", "Attachment;filename=Customer branch active/deactivate error report"+dateString+".csv");
				csv.getCustomerBranchActivateDeactivateErrorReport(pw);
				break;
				
			case downloadForScheduleOrCompleteViaExcel:
				responseref.setHeader("Content-Disposition", "Attachment;filename=ScheduleOrCompleteViaExcel upload file"+dateString+".csv");
				csv.downloadFileForScheduleOrCompleteViaExcel(pw);
				break;
				
			case ScheduleOrCompleteViaExcelErrorReport:
				responseref.setHeader("Content-Disposition", "Attachment;filename=ScheduleOrCompleteViaExcel error file"+dateString+".csv");
				csv.getScheduleOrCompleteViaExcelErrorReport(pw);
				break;
			case UltimaTallyExport:
				responseref.setHeader("Content-Disposition","Attachment;filename=Tally Export " + dateString+ ".csv");
				csv.getUltimaTallyExport(pw);
				break;
			case ReverServiceCompletion:
				responseref.setHeader("Content-Disposition","Attachment;filename=Reverse Service Completion " + dateString+ ".csv");
				csv.getReverseServiceCompletionError(pw);
				break;
				
		}
							
	}

	
}


