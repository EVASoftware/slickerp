package com.slicktechnologies.server;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import com.google.appengine.api.taskqueue.QueueFailureException;
import com.google.gwt.dev.shell.log.SwingTreeLogger.LogEvent;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
public class customerSaveTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8043172200454609097L;

	
	Logger logger = Logger.getLogger("Name of logger");
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	
	public static List<Customer> customerlist;
	public static int finalcount;
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		
		 logger.log(Level.SEVERE,"Welcome to taskqueue");

		
//		 byte[] bytes2 = request.getp
         String key = request.getParameter("key");
        // Do something with key.
         System.out.println("KEY:"+key);
        
         String[] customerstring = key.split("[$]");
         System.out.println("SIZE:"+customerstring.length);
        
         int excelcount = Integer.parseInt(customerstring[43]);
	     logger.log(Level.SEVERE,"Excelxount:::-----"+excelcount);

         Customer customer = new Customer();
        
   		 logger.log(Level.SEVERE,"First Name ::::::::::::"+customerstring[3]);
   		 logger.log(Level.SEVERE,"Middle Name ::::::::::::"+customerstring[4]);
   		 logger.log(Level.SEVERE,"Last Name ::::::::::::"+customerstring[5]);

   		 

   		try{
   			
        System.out.println("first Name"+customerstring[0]);
        
        if(customerstring[0].equalsIgnoreCase("NO")){
    		customer.setCompany(false);
    	}
    	else{
    		customer.setCompany(true);
    	}
    	
    	if(customerstring[1].equalsIgnoreCase("na")){
    		customer.setCompanyName("");
    	}
    	else{
    		customer.setCompanyName(customerstring[1]);
    	}
    	
    	try {
    		
    		if(customerstring[2].equalsIgnoreCase("NA")){
    			customer.setDob(fmt.parse(""));
    		}else{
				customer.setDob(fmt.parse(customerstring[2]));
    		}
    		} catch (ParseException e) {
    			e.printStackTrace();
    		}
    	
    	if(customerstring[3].equalsIgnoreCase("NA")){
    		customer.setFirstName("");

    	}else{
    		customer.setFirstName(customerstring[3]);
    	}
    	if(customerstring[4].equalsIgnoreCase("NA")){
	    	customer.setMiddleName("");

    	}else{
    	customer.setMiddleName(customerstring[4]);
    	}
    	if(customerstring[5].equalsIgnoreCase("na")){
	    	customer.setLastName("");
    	}else{
    	customer.setLastName(customerstring[5]);
    	}
    	if(customerstring[6].equalsIgnoreCase("na")){
	    	customer.setEmail("");

    	}else{
    		customer.setEmail(customerstring[6]);
    	}
    	if(customerstring[7].equalsIgnoreCase("na")){
	    	customer.setLandLine(0l);
    	}else{
	    	customer.setLandLine(Long.parseLong(customerstring[7]));
    	}
    	if(customerstring[8].equalsIgnoreCase("NA")){
	    	customer.setCellNumber1(0l);
    	}else{
	    	customer.setCellNumber1(Long.parseLong(customerstring[8]));
    	}
    	if(customerstring[9].equalsIgnoreCase("na")){
	    	customer.setCellNumber2(0l);
    	}else{
    		customer.setCellNumber2(Long.parseLong(customerstring[9]));
    	}
    	if(customerstring[10].equalsIgnoreCase("na")){
	    	customer.setFaxNumber(0l);
    	}else{
	    	customer.setFaxNumber(Long.parseLong(customerstring[10]));
    	}
    	if(customerstring[11].equalsIgnoreCase("na")){
	    	customer.setBranch("");
    	}else{
	    	customer.setBranch(customerstring[11]);
    	}
    	if(customerstring[12].equalsIgnoreCase("na")){
	    	customer.setEmployee("");
    	}else{
	    	customer.setEmployee(customerstring[12]);
    	}
    	if(customerstring[13].equalsIgnoreCase("na")){
	    	customer.setGroup("");
    	}else{
	    	customer.setGroup(customerstring[13]);
    	}
    	if(customerstring[14].equalsIgnoreCase("na")){
	    	customer.setCategory("");
    	}else{
    		customer.setCategory(customerstring[14]);
    	}
    	if(customerstring[15].equalsIgnoreCase("na")){
	    	customer.setType("");
    	}else{
	    	customer.setType(customerstring[15]);
    	}
    	if(customerstring[16].equalsIgnoreCase("na")){
	    	customer.setCustomerPriority("");
    	}else{
	    	customer.setCustomerPriority(customerstring[16]);
    	}
    	if(customerstring[17].equalsIgnoreCase("na")){
	    	customer.setCustomerLevel("");
    	}else{
	    	customer.setCustomerLevel(customerstring[17]);
    	}
    	if(customerstring[18].equalsIgnoreCase("na")){
	    	customer.setRefrNumber1("");
    	}
    	else{
	    	customer.setRefrNumber1(customerstring[18]);
    	}
    	if(customerstring[19].equalsIgnoreCase("na")){
	    	customer.setRefrNumber2("");
    	}else{
	    	customer.setRefrNumber2(customerstring[19]);
    	}
    	if(customerstring[20].equalsIgnoreCase("na")){
	    	customer.setWebsite("");
    	}else{
	    	customer.setWebsite(customerstring[20]);
    	}
    	if(customerstring[21].equalsIgnoreCase("NA")){
	    	customer.setDescription("");
    	}else{	
    		customer.setDescription(customerstring[21]);
    	}	
    	
    	SocialInformation socinfo=new SocialInformation();
    	
    	if(customerstring[22].equalsIgnoreCase("NA")){
    		socinfo.setGooglePlusId("");
    	}else{
    	socinfo.setGooglePlusId(customerstring[22]);
    	}
    	if(customerstring[23].equalsIgnoreCase("na")){
	    	socinfo.setFaceBookId("");
    	}else{
	    	socinfo.setFaceBookId(customerstring[23]);
    	}
    	if(customerstring[24].equalsIgnoreCase("na")){
	    	socinfo.setTwitterId("");
    	}else{
	    	socinfo.setTwitterId(customerstring[24]);
    	}
    	customer.setSocialInfo(socinfo);
    	
    	
    	Address address = new Address();
    	if(customerstring[25].equalsIgnoreCase("na")){
	    	address.setAddrLine1("");
    	}else{
	    	address.setAddrLine1(customerstring[25]);
    	}
    	if(customerstring[26].equalsIgnoreCase("na")){
	    	address.setAddrLine2("");
    	}else{
	    	address.setAddrLine2(customerstring[26]);
    	}
    	if(customerstring[27].equalsIgnoreCase("na")){
	    	address.setLandmark("");
    	}else{
	    	address.setLandmark(customerstring[27]);
    	}
    	if(customerstring[28].equalsIgnoreCase("na")){
	    	address.setCountry("");
    	}else{
	    	address.setCountry(customerstring[28]);
    	}
    	if(customerstring[29].equalsIgnoreCase("na")){
	    	address.setState("");
    	}
    	else{
	    	address.setState(customerstring[29]);
    	}
    	if(customerstring[30].equalsIgnoreCase("na")){
	    	address.setCity("");
    	}else{
	    	address.setCity(customerstring[30]);
    	}
    	if(customerstring[31].equalsIgnoreCase("na")){
	    	address.setLocality("");
    	}else{
	    	address.setLocality(customerstring[31]);
    	}
    	if(customerstring[32].equalsIgnoreCase("na")){
	    	address.setPin(0l);
    	}else{
	    	address.setPin(Long.parseLong(customerstring[32]));
    	}
    	customer.setAdress(address);
    	
    	Address address2 = new Address();
    	if(customerstring[33].equalsIgnoreCase("na")){
	    	address2.setAddrLine1("");
    	}else{
	    	address2.setAddrLine1(customerstring[33]);
    	}
    	if(customerstring[34].equalsIgnoreCase("na")){
	    	address2.setAddrLine2("");
    	}else {
	    	address2.setAddrLine2(customerstring[34]);
		}
    	if(customerstring[35].equalsIgnoreCase("na")){
	    	address2.setLandmark("");
    	}else{
	    	address2.setLandmark(customerstring[35]);
    	}
    	if(customerstring[36].equalsIgnoreCase("na")){
	    	address2.setCountry("");
    	}else{
	    	address2.setCountry(customerstring[36]);
    	}
    	if(customerstring[37].equalsIgnoreCase("na")){
	    	address2.setState("");
    	}else{
	    	address2.setState(customerstring[37]);
    	}
    	if(customerstring[38].equalsIgnoreCase("na")){
	    	address2.setCity("");
    	}else{
	    	address2.setCity(customerstring[38]);
    	}
    	if(customerstring[39].equalsIgnoreCase("")){
	    	address2.setLocality("");
    	}else{
	    	address2.setLocality(customerstring[39]);
    	}
    	if(customerstring[40].equalsIgnoreCase("na")){
	    	address2.setPin(0l);
    	}else{
	    	address2.setPin(Long.parseLong(customerstring[40]));
    	}
    	customer.setSecondaryAdress(address2);
    	
    	System.out.println("HI"+customerstring[40]);
    	
    	customer.setCompanyId(Long.parseLong(customerstring[41]));
    	
    	long companyid = Long.parseLong(customerstring[41]);    	
    	  
    	
    	customer.setCount(Integer.parseInt(customerstring[42]));
    	System.out.println("Count:::::::::----"+Integer.parseInt(customerstring[42]));
    	 
    	 ofy().save().entity(customer).now(); 
    	
		  logger.log(Level.SEVERE,"Data Saved successfully");


    	}
    	catch(Exception e){
  		  logger.log(Level.SEVERE,"Failed------------");
  		  
  		  Customer customerforlist = new Customer();

    		
    		 logger.log(Level.SEVERE,"First Name ::::::::::::"+customerstring[3]);
       		 logger.log(Level.SEVERE,"Middle Name ::::::::::::"+customerstring[4]);
       		 logger.log(Level.SEVERE,"Last Name ::::::::::::"+customerstring[5]);
    		
//       		 Customer cust = new Customer();
       		customerforlist.setFirstName(customerstring[3]);
       		customerforlist.setMiddleName(customerstring[4]);
       		customerforlist.setLastName(customerstring[5]);
       		logger.log(Level.SEVERE,"Before Adding to list Data is=="+customerforlist);
       		
       		
       			customerlist.add(customerforlist);
       			
       		for(int i=0;i<customerlist.size();i++){
           		logger.log(Level.SEVERE,"Data is=="+customerlist.get(i).getFirstName()+""+customerlist.get(i).getLastName());
       		}
    		
    	}
   		++finalcount;
   		logger.log(Level.SEVERE,"Final Count==============="+finalcount);
   		
   		if(excelcount==finalcount){
   				Email cronEmail = new Email();
   				
				ArrayList<String> toEmailList=new ArrayList<String>();
   				ArrayList<String> tbl_header = new ArrayList<String>();
   				
   				tbl_header.add("Serial No");
   				tbl_header.add("First Name");
   				tbl_header.add("Middle Name");
   				tbl_header.add("Last Name");
   				
   				ArrayList<String> tbl1=new ArrayList<String>();

   				
   				long companyId = Long.parseLong(customerstring[41]);
   				Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();



   			logger.log(Level.SEVERE,"DONE 55");
   			for(int j=0;j<customerlist.size();j++){
   				logger.log(Level.SEVERE,"Error Code 001 ==== "+customerlist.get(j).getFirstName()+""+customerlist.get(j).getMiddleName()+""+customerlist.get(j).getLastName());
   			
   			
   			    toEmailList.add(comp.getPocEmail());
   				
   				tbl1.add((j+1)+"");
   	   	 		tbl1.add(customerlist.get(j).getFirstName());
   	   	 		tbl1.add(customerlist.get(j).getMiddleName());
   	   	 		tbl1.add(customerlist.get(j).getLastName());
   			
   			}
   			
   			Date todaysDate = new Date();
   		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
   		 String todayDateString = df.format(todaysDate);
   		 System.out.println("String in dd/MM/yyyy format is: " + todayDateString);
			
//   	 	  customerUploadEmail
			cronEmail.customerUploadEmail(toEmailList, "Error List Upload Customer"+" "+todayDateString, null, comp, tbl_header, tbl1, null, null, null, null);

			logger.log(Level.SEVERE,"Email sent"); 
   			
   		}
   		

    }

	
}
