package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.account.expensemanagment.ExpenseManagmentService;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class ExpenseManagmentServiceImpl extends RemoteServiceServlet implements ExpenseManagmentService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2778660236001933063L;

	@Override
	public void pettycashBalanceDeductAndUpdatePettcashTransaction(String pettycashName ,Expense model) {
		
		deductBalanceFromPettycash(pettycashName,model);
	}

	private void deductBalanceFromPettycash(String pettycashName, Expense model) {
		
		if(pettycashName!=null && model != null)
		{
			PettyCash petty=ofy().load().type(PettyCash.class).filter("pettyCashName",pettycashName).first().now();
			if(petty!=null)
			{
				double expAmt=model.getAmount();
//				int expAmount=(int) expAmt;
				double mainBalance=petty.getPettyCashBalance();
				
				// vijay has taken for petty cash tran list( ballance)
				
				double balance = petty.getPettyCashBalance();
				double deductAmount=mainBalance-expAmt;
				petty.setPettyCashBalance(deductAmount);
				ofy().save().entities(petty).now();
				
				/*********************** vijay code for petty cash transaction list *************/
				  SaveExpenseInTransactionList(pettycashName,balance,expAmt,model);
			}
		}
	}

	private void SaveExpenseInTransactionList(String pettycashname, double mainBalance, double expAmt,Expense model) {
		
		/**************************** vijay for transaction list *******************/
		System.out.println("hi vijay ====");
		System.out.println("main balance = "+mainBalance);
		System.out.println("expnse amt == "+expAmt);
		PettyCashTransaction pettycash = new PettyCashTransaction();
		pettycash.setOpeningStock(mainBalance);
		pettycash.setTransactionAmount(expAmt);
		pettycash.setClosingStock(mainBalance-expAmt );
		pettycash.setDocumentId(model.getCount());
		pettycash.setDocumentName("Expense Management");
		pettycash.setPersonResponsible(model.getEmployee());
		pettycash.setAction("-");
		Date saveDate = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy h:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String datestring =  sdf.format(saveDate);
		Logger logger = Logger.getLogger("NameOfYourLogger");
		logger.log(Level.SEVERE," Date $$$$$$  === with IST"+datestring);
		String[] dateString=datestring.split(" ");
		String time=dateString[1];
		logger.log(Level.SEVERE," Date =="+dateString[0]);
		logger.log(Level.SEVERE," Date time =="+dateString[1]);
		pettycash.setTransactionTime(time);
		pettycash.setTransactionAmount(expAmt);
		try {
			pettycash.setTransactionDate(sdf.parse(datestring));
		} catch (ParseException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE," date parse exception  =="+e);
		}
		pettycash.setAppoverName(pettycashname);
		pettycash.setDocumentTitle(pettycashname);
		pettycash.setCompanyId(model.getCompanyId());
		pettycash.setPettyCashName(pettycashname);
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(pettycash);
		System.out.println("petty cash transaction list saved successfully");
		/************************* vijay *******************************/
	}

	@Override
	public void migrateDataFromExpenseMngt(Long companyId) {
		
		
    	int count=0;
		List<Expense> expenseList =  new ArrayList<Expense>();
		List<MultipleExpenseMngt> saveRecords = new ArrayList<MultipleExpenseMngt>();
		
		expenseList = ofy().load().type(Expense.class).filter("companyId",companyId).list();
		System.out.println("expenseList size "+expenseList.size());
		
		
		Comparator<Expense> compare = new Comparator<Expense>() {
			
			@Override
			public int compare(Expense o1, Expense o2) {
				
				Integer count1= o1.getCount();
				Integer count2 =o2.getCount();
				
				return count1.compareTo(count2);
			}
		};
		
		Collections.sort(expenseList, compare);
		
		
		
		ArrayList<ExpenseManagement> expensMngt = null;
		
		for (int i = 0; i < expenseList.size(); i++) 
		{
			expensMngt = new ArrayList<ExpenseManagement>();
			
			
			
			ExpenseManagement expmngt = new ExpenseManagement();
			expmngt.setInvoiceNumber(expenseList.get(i).getInvoiceNumber());
			expmngt.setExpenseGroup(expenseList.get(i).getGroup());
			expmngt.setExpenseCategory(expenseList.get(i).getCategory());
			expmngt.setExpenseType(expenseList.get(i).getType());
			expmngt.setExpenseDate(expenseList.get(i).getExpenseDate());
			expmngt.setEmployee(expenseList.get(i).getEmployee());
			expmngt.setAmount(expenseList.get(i).getAmount());
			expmngt.setCurrency(expenseList.get(i).getCurrency());
			expmngt.setPaymentMethod(expenseList.get(i).getPaymentMethod());
			expmngt.setVendor(expenseList.get(i).getVendor());
			expensMngt.add(expmngt);
			
			
			MultipleExpenseMngt expensemngt = new MultipleExpenseMngt();
			expensemngt.setExpenseList(expensMngt);
			expensemngt.setDescription(expenseList.get(i).getDescription());
			expensemngt.setPettyCashName(expenseList.get(i).getPettyCashName());
			expensemngt.setApproverName(expenseList.get(i).getApproverName());
			expensemngt.setBranch(expenseList.get(i).getBranch());
			expensemngt.setStatus(expenseList.get(i).getStatus());
			expensemngt.setExpTypeAsPettyCash(expenseList.get(i).getExpTypeAsPettyCash());
			expensemngt.setRemark(expenseList.get(i).getRemark());
			expensemngt.setTotalAmount(expenseList.get(i).getAmount());
			expensemngt.setAccount(expenseList.get(i).getAccount());
			expensemngt.setCreatedBy(expenseList.get(i).getCreatedBy());
			expensemngt.setCreationDate(expenseList.get(i).getCreationDate());
			expensemngt.setCount(expenseList.get(i).getCount());
			expensemngt.setCompanyId(expenseList.get(i).getCompanyId());
			
			saveRecords.add(expensemngt);
			
			
			
			if(i== expenseList.size()-1)
			{
				count = i;
			}
			
		}
	
		ofy().save().entities(saveRecords);
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "MulipleExpenseMngt").filter("status", true).first().now();
		numbergen.setCount(count);
		ofy().save().entity(numbergen).now();
	
	}

}
