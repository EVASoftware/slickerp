package com.slicktechnologies.server.outboundwebservice;
import static com.googlecode.objectify.ObjectifyService.ofy;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.labs.repackaged.com.google.common.collect.Multiset.Entry;
import com.google.gson.Gson;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.accountinginterface.IntegrateSyncService;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.BranchUsernamePassword;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;


import okhttp3.*;


public class IntegrateSyncServiceImpl extends RemoteServiceServlet implements IntegrateSyncService{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3904048349281625325L;

	Logger logger = Logger.getLogger("IntegrateSyncServiceImpl.class");
	@Override
	public String callInterFace(String docType, AccountingInterface entityAI) {
		// TODO Auto-generated method stub
		try{
			String response="";
//			if(docType.trim().equalsIgnoreCase("Contract")){
//				response=callNBHCSalesOrder(entityAI);
//			}else 
			if(docType.trim().equalsIgnoreCase("Invoice")){
				response=callNBHCINVOICE(entityAI);
			} else if (docType.trim().equalsIgnoreCase("GRN")) {
//				if (!entityAI.getStatus().trim()
//						.equals(AccountingInterface.CANCELLED)) {
//					response = callNBHCGRN(entityAI);
//				} else {
//					response = callNBHCGRNCancellationAPI(entityAI);
//				}
				/**** Date 31-04-2019 by Vijay for GRN interface updated with document status above old commented ************/
				if (!entityAI.getDocumentStatus().trim()
						.equals(AccountingInterface.CANCELLED)) {
					response = callNBHCGRN(entityAI);
				} else {
					response = callNBHCGRNCancellationAPI(entityAI);
				}
				
			} else if(docType.trim().equalsIgnoreCase("MMN")){
				response = callNBHCMMN(entityAI);
			}else{
				response="Interface of Document Type "+docType+" is not Available";
			}
			
			return response;
		}catch(Exception e){
			e.printStackTrace();
			return "Error::::::::: "+e;
		}
		
	}

	private String callNBHCMMN(AccountingInterface entityAI) {
		logger.log(Level.SEVERE, "This is where i will call invoice URL");
		final String MMNURL = AppConstants.NBHCMMNOUTBOUNDINTEGRATE;
		String data = "";

		URL url = null;
		try {
			url = new URL(MMNURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createMMNJSONObject(entityAI));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);

		return data;
	}

	private String createMMNJSONObject(AccountingInterface entityAI) {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		JSONObject jObj = new JSONObject();
		jObj.put("EVA_MMNID", entityAI.getDocumentID());
//		String[] warehouses=entityAI.getWareHouseCode().trim().split("\\$");
//		String fromWarehouse=warehouses[0];
//		String toWarehouse=warehouses[1];
		
		/**** Date 02-04-2019 by Vijay for NBHC ccpm MMN must send storage location for towarehouse to SAP to read ***/
		String[] warehouses=entityAI.getWareHouse().trim().split("\\$");
		String fromWarehouse=warehouses[0];
		String strtoWarehouse=warehouses[1];
		StorageLocation storageLocation = ofy().load().type(StorageLocation.class).filter("companyId", entityAI.getCompanyId())
											.filter("warehouseName", strtoWarehouse).first().now();
		
		StorageLocation storageLocation2 = ofy().load().type(StorageLocation.class).filter("companyId", entityAI.getCompanyId())
				.filter("warehouseName", fromWarehouse).first().now();
		String strFromWarehouse = "";
		if(storageLocation2!=null){
			strFromWarehouse = storageLocation2.getBusinessUnitName().trim();
		}
		logger.log(Level.SEVERE, "strFromWarehouse"+strFromWarehouse);

		String toWarehouseNameasStorageLocation = "";
		
		if(storageLocation!=null){
			toWarehouseNameasStorageLocation = storageLocation.getBusinessUnitName().trim();
		}
		logger.log(Level.SEVERE, "toWarehouseNameasStorageLocation"+toWarehouseNameasStorageLocation);
		jObj.put("EVA_MMNTitle", entityAI.getDocumentTitle()); // SAP PO
																		// ID is
																		// Stored
																		// in
																		// Reference
																		// Number
																		// 2
		
		jObj.put("EVA_MMNDate", sdf.format(entityAI.getDocumentDate()).trim()); // SAP P	
		String branch=entityAI.getBranch().trim();
//		if(branch.trim().equalsIgnoreCase("banglore")){
//			branch="Bangalore";
//		}
		jObj.put("EVA_Branch", branch);
		jObj.put("EVA_PersonResponsible", entityAI.getRequestedBy());
		jObj.put("EVA_ApproverName", entityAI.getApprovedBy());
		jObj.put("EVA_Status", entityAI.getDocumentStatus());
//		jObj.put("EVA_ToWarehouse",toWarehouse);
		jObj.put("EVA_ToWarehouse",toWarehouseNameasStorageLocation);

		
		jObj.put("EVA_Comment", branch);
		jObj.put("ClientCode", entityAI.getCustomerRefId());
		
		JSONArray jsonitemArray=new JSONArray();
		
		List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", entityAI.getCompanyId()).filter("documentType","MMN").filter("documentID", entityAI.getDocumentID()).list();
		for (int i = 0; i < accountingInterfaceList.size(); i++) {
			String[] itemWarehouses=accountingInterfaceList.get(i).getWareHouseCode().trim().split("\\$");
			String itemfromWarehouse=warehouses[0];
			String itemtoWarehouse=warehouses[1];
			
			JSONObject obj=new JSONObject();
			obj.put("EVA_MMNID", entityAI.getDocumentID());
			obj.put("EVA_MaterialCode", accountingInterfaceList.get(i).getProductCode());
			obj.put("EVA_MovQty", accountingInterfaceList.get(i).getQuantity());
			obj.put("EVA_UOM", accountingInterfaceList.get(i).getUnitOfMeasurement());
//			obj.put("EVA_FrmWareHouse",itemfromWarehouse );
			obj.put("EVA_FrmWareHouse",strFromWarehouse );

			obj.put("EVA_Remarks",entityAI.getDocumentRemark());
			obj.put("CreatedBy",entityAI.getCreatedBy());
			jsonitemArray.put(obj);
		}
		jObj.put("liDTOTransferPostingDetail",jsonitemArray);
		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON GRN DATA:::" + jsonString);
		return jsonString;
	}

	private String callNBHCGRN(AccountingInterface entityAI) {
		logger.log(Level.SEVERE, "This is where i will call invoice URL");
		final String GRNURL = AppConstants.NBHCGRNOUTBOUNDINTEGRATE;
		String data = "";

		URL url = null;
		try {
			url = new URL(GRNURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createGRNJSONObject(entityAI));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);

		return data;
	}

	
	private String createGRNJSONObject(AccountingInterface entityAI) {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		JSONObject jObj = new JSONObject();
		jObj.put("DocumentNo", entityAI.getDocumentID());
		jObj.put("ReferenceId", entityAI.getReferenceDocumentNo2()); // SAP PO
																		// ID is
																		// Stored
																		// in
																		// Reference
																		// Number
																		// 2
		String branch=entityAI.getBranch().trim();
//		if(branch.trim().equalsIgnoreCase("banglore")){
//			branch="Bangalore";
//		}
		jObj.put("Branch", branch);
		jObj.put("GoodsRecipient", null);
		jObj.put("DocType", null);
		jObj.put("ReferenceDate",
				sdf.format(entityAI.getReferenceDocumentDate2()));
		jObj.put("GRN_Date", sdf.format(entityAI.getDocumentDate()).trim());
//		jObj.put("Delivery_Note", "test"); 
		/*** Date 06-06-2019 by Vijay for SAP delivery note mapping for bill no ***/
		jObj.put("Delivery_Note", entityAI.getConractCategory()); 

		JSONArray jArray = new JSONArray();
		ArrayList<String> statusList = new ArrayList<String>();
		statusList.add(AccountingInterface.TALLYCREATED);
		statusList.add(AccountingInterface.FAILED);
		List<AccountingInterface> accountingInterfaceList = ofy().load()
				.type(AccountingInterface.class)
				.filter("companyId", entityAI.getCompanyId())
				.filter("documentID", entityAI.getDocumentID())
				.filter("status IN", statusList).filter("documentType", "GRN")
				.filter("direction", AccountingInterface.OUTBOUNDDIRECTION)
				.filter("sourceSystem", AppConstants.CCPM)
				.filter("destinationSystem", AppConstants.NBHC).list();
		logger.log(Level.SEVERE, "accountingInterfaceList size:::::::"
				+ accountingInterfaceList.size());
		for (AccountingInterface accountingInterface : accountingInterfaceList) {
			JSONObject jSonObj = new JSONObject();
			jSonObj.put("POItemNumber", accountingInterface.getProductRefId());
			jSonObj.put("Quantity", accountingInterface.getQuantity());
			jArray.put(jSonObj);
		}
		jObj.put("liGrnItem", jArray);
//		jObj.put("ReferenceId2", entityAI.getRemark()); //by vijay Bill no to map in SAP 
		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON GRN DATA:::" + jsonString);
		return jsonString;
	}
	
	private String callNBHCGRNCancellationAPI(AccountingInterface entityAI) {
		logger.log(Level.SEVERE, "This is where i will call invoice URL");
		final String GRNCancellationURL = AppConstants.NBHCGRNCANCELLATION;
		String data = "";

		URL url = null;
		try {
			url = new URL(GRNCancellationURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
			writer.write(createGRNCancelledObject(entityAI));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
			}
			logger.log(Level.SEVERE, "data data::::::::::" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);

		return data;
	}
	
	private String createGRNCancelledObject(AccountingInterface entityAI) {
		// TODO Auto-generated method stub
		JSONObject jObj = new JSONObject();
		jObj.put("Type", "GRN");
		jObj.put("DocumentNo", entityAI.getDocumentID());
		
		/*** Date 07-06-2019 by Vijay
		 * Des :- partial and full cancellation based on POItemnumber NBHC CCPM
		 */
		JSONArray jArray = new JSONArray();
		ArrayList<String> statusList = new ArrayList<String>();
		statusList.add(AccountingInterface.TALLYCREATED);
		statusList.add(AccountingInterface.FAILED);
		List<AccountingInterface> accountingInterfaceList = ofy().load()
				.type(AccountingInterface.class)
				.filter("companyId", entityAI.getCompanyId())
				.filter("documentID", entityAI.getDocumentID())
				.filter("status IN", statusList).filter("documentType", "GRN")
				.filter("direction", AccountingInterface.OUTBOUNDDIRECTION)
				.filter("sourceSystem", AppConstants.CCPM)
				.filter("destinationSystem", AppConstants.NBHC)
				.filter("documentStatus", "Cancelled").list();
		logger.log(Level.SEVERE, "accountingInterfaceList size:::::::"
				+ accountingInterfaceList.size());
		for (AccountingInterface accountingInterface : accountingInterfaceList) {
			JSONObject jSonObj = new JSONObject();
			jSonObj.put("POItemNumber", accountingInterface.getProductRefId());
//			jSonObj.put("Quantity", accountingInterface.getQuantity());
			jArray.put(jSonObj);
		}
		jObj.put("GrnItemlist", jArray);
		
		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "JSON GRN DATA:::" + jsonString);
		
		return jsonString;
	}
	
public String callNBHCSalesOrder(AccountingInterface entityAI) {
	// TODO Auto-generated method stub
	final String SALESORDER=AppConstants.NBHCSALESORDERINTEGRATE;
	String data="";
	
	URL url = null;
	try {
		url = new URL(SALESORDER);
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    HttpURLConnection con = null;
	try {
		con = (HttpURLConnection) url.openConnection();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    con.setDoInput(true);
    con.setDoOutput(true);
    con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    try {
		con.setRequestMethod("POST");
	} catch (ProtocolException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    OutputStream os = null;
	try {
		os = con.getOutputStream();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
		e.printStackTrace();
	}
    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
//    Log.d("Test", "From Get Post Method" + getPostData(values));
    try {
		writer.write(createSOJSONObject(entityAI));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    try {
		writer.flush();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    
    InputStream is = null;
	try {
		is = con.getInputStream();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    BufferedReader br = new BufferedReader(
            new InputStreamReader(is));
    String temp;

    try {
		while ((temp = br.readLine()) != null) {
		    data = data + temp;
		}
		logger.log(Level.SEVERE,"data data::::::::::"+data);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
		e.printStackTrace();
	}
    
    logger.log(Level.SEVERE,"Data::::::::::"+data);
    return data;
}

private String createSOJSONObject(AccountingInterface entityAI) {
	// TODO Auto-generated method stub
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	  
	JSONObject jObj=new JSONObject();
	jObj.put("SODocCode", entityAI.getDocumentID());
	jObj.put("CustomerCode", entityAI.getCustomerRefId());
	jObj.put("CondValue", entityAI.getNetPayable());
	String branch=entityAI.getBranch().trim();
//	if(branch.trim().equalsIgnoreCase("banglore")){
//		branch="Bangalore";
//	}
	jObj.put("Salesoffice", branch.trim());
	jObj.put("ActionTask", "SalesOrder");
	jObj.put("EVA_IfId", entityAI.getCount()+"");
	jObj.put("EVA_DocID", entityAI.getDocumentID()+"");
	jObj.put("EVA_DocDate", sdf.format(entityAI.getDocumentDate()));
	jObj.put("EVA_ContractCategory", entityAI.getConractCategory());
	String textProdName=null;
	List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", entityAI.getCompanyId()).filter("documentType","Contract").filter("documentID", entityAI.getDocumentID()).list();
	int servicesInContract=0;
	for (int i = 0; i < accountingInterfaceList.size(); i++) {
		servicesInContract=servicesInContract+accountingInterfaceList.get(i).getProdServices();
		if(textProdName!=null){
			textProdName=textProdName+","+accountingInterfaceList.get(i).getProductName();
		}else{
			textProdName=accountingInterfaceList.get(i).getProductName();
		}
	}
	jObj.put("Eva_Services", servicesInContract);
	jObj.put("Text", textProdName);
	
	String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
	logger.log(Level.SEVERE,"JSON SO DATA:::"+jsonString);
	return jsonString;
}

	
public String callNBHCINVOICE(AccountingInterface entityAI)
{
		logger.log(Level.SEVERE,"This is where i will call invoice URL");
		final String INVOICEURL=AppConstants.NBHCINVOICEINTEGRATE;
		String data="";
		
		URL url = null;
		try {
			url = new URL(INVOICEURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
//        Log.d("Test", "From Get Post Method" + getPostData(values));
        try {
			writer.write(createJSONObject(entityAI));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        BufferedReader br = new BufferedReader(
                new InputStreamReader(is));
        String temp;

        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp;
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"Data::::::::::"+data);
		
		return data;
	}
	
private String createJSONObject(AccountingInterface entityAI) {
	// TODO Auto-generated method stub
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	  
	JSONObject jObj=new JSONObject();
	jObj.put("DocumentNo", entityAI.getDocumentID());//r
	jObj.put("BillDate", sdf.format(entityAI.getDocumentDate()));//r
	jObj.put("SoldTo", entityAI.getCustomerRefId()); //Customer Reference Id //r
	jObj.put("BillTo", entityAI.getCustomerRefId());//r
	jObj.put("Payer", entityAI.getCustomerRefId());//r
	jObj.put("ShipTo", entityAI.getCustomerRefId());//r
	jObj.put("Plant", "");//r
	jObj.put("Material", "");//r
	jObj.put("ClientCode", "");//r
	jObj.put("RefDoc", entityAI.getReferenceDocumentNo1());//r
	jObj.put("RefDocCategory", "");//r
	jObj.put("CondValue", entityAI.getNetPayable());//r
//	jObj.put("ActionTask","InvoiceBillData" );
	String branch=entityAI.getBranch().trim();
//	if(branch.trim().equalsIgnoreCase("banglore")){
//		branch="Bangalore";
//	}
	jObj.put("SalesOffice",branch);//r
	/**
	 * Rahul changed this on 27 Feb 2018 and added from from invoice group in category instead of contract category
	 */
//	Contract contract=ofy().load().type(Contract.class).filter("companyId", entityAI.getCompanyId()).filter("count",Integer.parseInt(entityAI.getReferenceDocumentNo1())).first().now();
	String taxCategory="";
	if(entityAI.getConractCategory()!=null&&!entityAI.getConractCategory().equals("")){
	
		if(entityAI.getConractCategory().trim().equalsIgnoreCase("Service Charges - Service Tax Applicable")){
			taxCategory="Service Charges - Service Tax Applicable";
		//  rohan added this code for NBHC on 19-05-2017 for sap integration and sending hard coded category
		}else if(entityAI.getConractCategory().trim().equalsIgnoreCase("Service Charges - ST applicable")){
			taxCategory="Service Charges - Service Tax Applicable";
		}else if(entityAI.getConractCategory().trim().equalsIgnoreCase("Charges exclusive Service Tax")){
			taxCategory="Service Charges - Service Tax Applicable";
		//   ends here 	
		}else if(entityAI.getConractCategory().trim().equalsIgnoreCase("Service Charges - ST not applicable")){
			taxCategory="Service Charges - Service Tax Exempted";
		//   ends here 	
		}else{
			taxCategory=entityAI.getConractCategory().trim();
		}
		
	}else{
		Contract contract=ofy().load().type(Contract.class).filter("companyId", entityAI.getCompanyId()).filter("count",Integer.parseInt(entityAI.getReferenceDocumentNo1())).first().now();

	    if(contract.getCategory().trim().equalsIgnoreCase("Service Charges - Service Tax Applicable")){
	        taxCategory="Service Charges - Service Tax Applicable";
	    //  rohan added this code for NBHC on 19-05-2017 for sap integration and sending hard coded category
	    }else if(contract.getCategory().trim().equalsIgnoreCase("Service Charges - ST applicable")){
	        taxCategory="Service Charges - Service Tax Applicable";
	    }else if(contract.getCategory().trim().equalsIgnoreCase("Charges exclusive Service Tax")){
	        taxCategory="Service Charges - Service Tax Applicable";
	    //   ends here
	    }else if(contract.getCategory().trim().equalsIgnoreCase("Service Charges - ST not applicable")){
	        taxCategory="Service Charges - Service Tax Exempted";
	    //   ends here
	    }else{
	        taxCategory=contract.getCategory().trim();
	    }
	}
	jObj.put("EVAContractCategory",taxCategory);//r
//	jObj.put("EVA_IfId", entityAI.getCount());
//	jObj.put("EVA_DocID", entityAI.getDocumentID());
	jObj.put("BillingPeriodFrom", sdf.format(entityAI.getBillingPeroidFromDate()));//r
	jObj.put("BillingPeriodTo", sdf.format(entityAI.getBillingPeroidToDate()));//r
//	jObj.put("EVA_DocDate", sdf.format(entityAI.getDocumentDate()));
	
	//  rohan added this code NBHC for considering Fail and created status. on Date : 05-04-2017
	 ArrayList<String> obj = new ArrayList<String>();
	 obj.add(AccountingInterface.TALLYCREATED);
	 obj.add(AccountingInterface.FAILED);
	
	List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", entityAI.getCompanyId()).filter("documentType","Invoice").filter("status IN", obj).filter("documentID", entityAI.getDocumentID()).list();
	int servicesInInvoice=0;
	for (int i = 0; i < accountingInterfaceList.size(); i++) {
		servicesInInvoice=servicesInInvoice+accountingInterfaceList.get(i).getProdServices();
	}
	/**
	 * Date : 14-11-2017 BY ANIL
	 * earlier we are passing product name as text in sap now we are sending invoice comment
	 */
//	String textProdName=null;
//	int servicesInContract=0;
//	for (int i = 0; i < accountingInterfaceList.size(); i++) {
//		servicesInContract=servicesInContract+accountingInterfaceList.get(i).getProdServices();
//		if(textProdName!=null){
//			textProdName=textProdName+","+accountingInterfaceList.get(i).getProductName();
//		}else{
//			textProdName=accountingInterfaceList.get(i).getProductName();
//		}
//	}
//	jObj.put("Text", textProdName); //r null 
	String text="";
	if(entityAI.getDocumentRemark()!=null){
		text=entityAI.getDocumentRemark();
	}
	jObj.put("Text", text);
	/**
	 * End
	 */
	jObj.put("Eva_Services", servicesInInvoice);//r 0
	
	/** date 15.4.2019 added by komal to send amount service branch wise**/
	/** Description :- It is not stored in accounting interface as accounting interface method has been called from
	 * so many places and if tried to add any parameter in it then need to change at every method call. As nbhc syncs only one invoice
	 * at a time so invoice has been load and values are takeb from invoice directly
	 */
	
	JSONArray serviceBranchArray = new JSONArray();
	Invoice invoice=ofy().load().type(Invoice.class).filter("companyId", entityAI.getCompanyId()).filter("count", entityAI.getDocumentID()).first().now();
	HashSet<String> branchSet = new HashSet<String>();
	if(invoice != null){
		Map<String,Double> branchValueMap = new HashMap<String,Double>();
		for(SalesOrderProductLineItem item : invoice.getSalesOrderProducts()){
			/**
			 * @author Vijay Chougule Date 27-12-2019
			 * Des :- Bug :- When product SR name is the same for two product then branch split amount is going to apply to both
			 * products so double amount is going at SAP. As disscussed with Sandeep if two products are in the invoice
			 * he add branch split bifiracation in only one product and second product he making its payable amt 0.
			 * so if product payable amt is 0 then i will not read this amount to send to SAP with process cocnfig.
			 *  This issue due to duplicate SRNumber.
			 */
			boolean flag = false;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableDontReadBranchSplitAmtForPayableAmtZero", entityAI.getCompanyId())){
				flag = true;
			}
			else{
				flag = false;
			}
			if(flag){
				/** here added only one new condition payble amount > 1 rest is same code **/
				if(item.getServiceBranchesInfo() != null && item.getServiceBranchesInfo().size() >0 && item.getBasePaymentAmount()>1){
					for(BranchWiseScheduling branchScheduling : item.getServiceBranchesInfo().get(item.getProductSrNumber())){
						if(branchScheduling.isCheck()){
							branchSet.add(branchScheduling.getBranchName());
							if(branchValueMap.containsKey(branchScheduling.getBranchName())){
								branchValueMap.put(branchScheduling.getBranchName(), branchValueMap.get(branchScheduling.getBranchName()) + branchScheduling.getArea());
							}else{
								branchValueMap.put(branchScheduling.getBranchName(), branchScheduling.getArea());
							}
						}
					}
				}
			}
			else{
				if(item.getServiceBranchesInfo() != null && item.getServiceBranchesInfo().size() >0){
					for(BranchWiseScheduling branchScheduling : item.getServiceBranchesInfo().get(item.getProductSrNumber())){
						if(branchScheduling.isCheck()){
							branchSet.add(branchScheduling.getBranchName());
							if(branchValueMap.containsKey(branchScheduling.getBranchName())){
								branchValueMap.put(branchScheduling.getBranchName(), branchValueMap.get(branchScheduling.getBranchName()) + branchScheduling.getArea());
							}else{
								branchValueMap.put(branchScheduling.getBranchName(), branchScheduling.getArea());
							}
						}
					}
				}
			}
			/** below is original code added above in if or else condition to handle the Serial number duplication issue ***/
//				if(item.getServiceBranchesInfo() != null && item.getServiceBranchesInfo().size() >0){
//					for(BranchWiseScheduling branchScheduling : item.getServiceBranchesInfo().get(item.getProductSrNumber())){
//						if(branchScheduling.isCheck()){
//							branchSet.add(branchScheduling.getBranchName());
//							if(branchValueMap.containsKey(branchScheduling.getBranchName())){
//								branchValueMap.put(branchScheduling.getBranchName(), branchValueMap.get(branchScheduling.getBranchName()) + branchScheduling.getArea());
//							}else{
//								branchValueMap.put(branchScheduling.getBranchName(), branchScheduling.getArea());
//							}
//						}
//					}
//				}
		}
		HashMap<String ,String> profitCenterMap = new HashMap<String ,String>();
		if(branchSet.size() >0){
			List<String> serviceBranchList = new ArrayList<String>(branchSet);
			List<Branch> branchList = ofy().load().type(Branch.class).filter("companyId", entityAI.getCompanyId()).filter("buisnessUnitName IN", serviceBranchList).list();
			for(Branch serBranch : branchList){
				if(serBranch.getReferenceNumber2() != null){
					profitCenterMap.put(serBranch.getBusinessUnitName(), serBranch.getReferenceNumber2());
				}else{
					profitCenterMap.put(serBranch.getBusinessUnitName()," ");
				}
			}
			
		}
		if(branchValueMap.size() > 0){
			JSONObject object = null;
			for(Map.Entry<String , Double> entry : branchValueMap.entrySet()){
				object = new JSONObject();
				object.put("BranchName",entry.getKey());
				object.put("Amount",entry.getValue());
				try{
					object.put("Profit_Center",profitCenterMap.get(entry.getKey()));
				}catch(Exception e){
					object.put("Profit_Center", "");
				}
				serviceBranchArray.put(object);
			}
		}	
	}
	
	jObj.put("ServiceBranches", serviceBranchArray);
	
	/**
	 * end komal
	 */
	
	/**
	 * Date 19-06-2019 By Vijay 
	 * Des :- added new parameter for TaxType based on Tax group 
	 */
	String taxType ="";
	if(taxCategory.trim().equalsIgnoreCase("Service Charges - Service Tax Applicable")){
		taxType="ZS03";
	}
	else if(taxCategory.trim().equalsIgnoreCase("Service Charges - Service Tax Exempted")){
		taxType="ZNON";
	}
	jObj.put("DocType", taxType);
	
	String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
	logger.log(Level.SEVERE,"Invoice Data::"+jsonString);
	return jsonString;
}



@Override
public String callLicenseUpdateInteface(Contract contract,String actionTask) {
	String msg="";
	ArrayList<LicenseDetails> licenseList=new ArrayList<LicenseDetails>();
	if(contract!=null){
		for(SalesLineItem item:contract.getItems()){
			ServiceProduct serProduct=ofy().load().type(ServiceProduct.class).filter("companyId", contract.getCompanyId()).filter("count", item.getPrduct().getCount()).first().now();
			if(serProduct!=null){
//				item.setProductCategory(serProduct.getProductCategory());
				item.setProductCode(serProduct.getProductCode());
				item.getPrduct().setProductClassification(serProduct.getProductClassification());
			}
			if(AppConstants.LICENSETYPELIST.EVA_VALID_PRODUCT_CATEGORY.equals(item.getPrduct().getProductClassification())){
				int no_Of_License;
				try{
					no_Of_License=Integer.parseInt(item.getArea());
				}catch(NumberFormatException e){
					return msg="Please put valid number of license.";
				}
				
				LicenseDetails licenseObj=new LicenseDetails();
				licenseObj.setLicenseType(item.getProductCode());
				licenseObj.setStartDate(item.getStartDate());
				licenseObj.setEndDate(item.getEndDate());
				licenseObj.setDuration(item.getDuration());
				licenseObj.setNoOfLicense(no_Of_License);
				licenseObj.setContractCount(Long.parseLong(contract.getCount()+""));//23-01-2024
				licenseObj.setCreatedBy(contract.getCreatedBy());
				licenseObj.setStatus(true);
				licenseList.add(licenseObj);
			}
		}
		
		if(licenseList.size()==0){
			return msg="You can update the license of only those product who are classified as 'Licensed'.";
		}
		
		
		
		Customer customer=ofy().load().type(Customer.class).filter("companyId", contract.getCompanyId()).filter("count", contract.getCinfo().getCount()).first().now();
		if(customer!=null){
			if(customer.getRefrNumber1()!=null&&!customer.getRefrNumber1().equals("")){
				
				final String CLIENTURL=customer.getRefrNumber1()+AppConstants.LICENSETYPELIST.EVA_LICENSE_UPDATE_URL;
				logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
				
				String data="";
				URL url = null;
				try {
					url = new URL(CLIENTURL);
				} catch (MalformedURLException e) {
					logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
					e.printStackTrace();
					return msg="Please add proper client url.";
				}
				logger.log(Level.SEVERE,"STAGE 1");
		        HttpURLConnection con = null;
				try {
					con = (HttpURLConnection) url.openConnection();
					con.setConnectTimeout(60000); //Ashwini Patil Date:14-03-2022 Description: solution for  java.net.SocketTimeoutException 
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
					e.printStackTrace();
					return msg="Error occured in establishing connection.";
				}
				logger.log(Level.SEVERE,"STAGE 2");
		        con.setDoInput(true);
		        con.setDoOutput(true);
//		        con.setRequestProperty("Content-Type","application/json; charset=UTF-8");
		        try {
					con.setRequestMethod("POST");
				} catch (ProtocolException e) {
					logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
					e.printStackTrace();
					return msg="Protocol error.";
				}
		        logger.log(Level.SEVERE,"STAGE 3");
		        OutputStream os = null;
				try {
					os = con.getOutputStream();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
					e.printStackTrace();
				}
				logger.log(Level.SEVERE,"STAGE 4");
		        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		        try {
					writer.write(getDataTosend(contract, actionTask, licenseList));
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        try {
					writer.flush();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        logger.log(Level.SEVERE,"STAGE 5");
		        
		        
		        
		        InputStream is = null;
				try {
					is = con.getInputStream();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
					e.printStackTrace();
				}
				
				 logger.log(Level.SEVERE,"STAGE 6");
		        BufferedReader br = new BufferedReader(new InputStreamReader(is)); 
		        String temp;
		        try {
					while ((temp = br.readLine()) != null) {
					    data = data + temp+"\n"+"\n";
					}
					logger.log(Level.SEVERE,"data data::::::::::"+data);
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        
		        logger.log(Level.SEVERE,"STAGE 7");
		        
		        logger.log(Level.SEVERE,"Data::::::::::"+data);
				
				return msg=data;
				
				
				
				
				
				
				
				
			}else{
				return msg="Client url is not mentioned in the reference number 1.";
			}
		}else{
			return msg="Client details not found.";
		}
	}else{
		return msg="Contract not found.";
	}
//	return null;
}

private String getDataTosend(Contract contract, String actionTask, ArrayList<LicenseDetails> licenseList) {
	// TODO Auto-generated method stub
	StringBuilder strB=new StringBuilder();
	strB.append("jsonString");
	strB.append("=");
	strB.append(createJSONObjectFromContract(contract, actionTask, licenseList));
	
	return strB.toString();
}

private String createJSONObjectFromContract(Contract entityAI,String actionTask,ArrayList<LicenseDetails> licenseList) {
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	Gson gson = new Gson();  
	JSONObject jObj=new JSONObject();
	jObj.put("action_task", actionTask);
	jObj.put("contract_id", entityAI.getCount());
	jObj.put("update_Date", sdf.format(new Date()));
	jObj.put("updated_By", entityAI.getCreatedBy());
	JSONArray jsonArray = null;
	try {
		jsonArray = new JSONArray(gson.toJson(licenseList));
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		logger.log(Level.SEVERE,"Json array exception : "+e);
		
	}
	jObj.put("license_list",jsonArray);
	
	String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
	logger.log(Level.SEVERE,"License Data:: "+jsonString);
	
	
	return jsonString;
}

@Override
public String getIRN(Invoice invoice,String loggedInUser,String taskId,boolean bulkGenerationFlag) {
	SimpleDateFormat datetime = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	datetime.setTimeZone(TimeZone.getTimeZone("IST"));
	
	String responseString="";
	
	logger.log(Level.SEVERE,"This is where i will call invoice API for Invoice Id"+invoice.getCount());
	ServerAppUtility serverapp=new ServerAppUtility();
	String gstin="";
	String username="";
	String password="";
	String custId="";
	String apiId="";
	String apiSecret="";
	boolean refNoAsDocumentIdFlag=false; //Orion wants to generate einvoice with the reference number instead of document id
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "GenerateEinvoiceFromReferenceNumber", invoice.getCompanyId())){
		refNoAsDocumentIdFlag = true;
	}
	
	
	
	if(invoice.getNumberRange()!=null&&!invoice.getNumberRange().equals(""))
	{
		Config numberrange=ofy().load().type(Config.class)
				.filter("companyId", invoice.getCompanyId())
				.filter("type", 91)
				.filter("name",invoice.getNumberRange())
				.first().now();
		if(numberrange!=null) {
			if(!numberrange.isGstApplicable())
				responseString+= "It seems non billing Invoice. Tax is not applicable for the selected number range. \n If you want to generate eInvoice then go to Service-> Service Configuration -> Number Range -> tick tax applicable checkbox. ";
		}			
	
	}

	if(invoice.getIRN()!=null&&!invoice.getIRN().equals("")) {
		if(invoice.getIrnStatus()!=null&&!invoice.getIrnStatus().equals("")) {
			if(invoice.getIrnStatus().equals("Cancelled"))
				responseString+= "eInvoice has been cancelled. Check details in eInvoice tab! ";
			else
				responseString+= "IRN already generated! Check details in eInvoice tab! ";
		}			
//		responseString+= "IRN already generated! Check details in eInvoice tab! ";
	}

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	
	DecimalFormat df = new DecimalFormat("0.00");
	
	Company company=ofy().load().type(Company.class)
			.filter("companyId", invoice.getCompanyId()).first()
			.now();
	
	gstin=serverapp.getGSTINOfCompany(company,invoice.getBranch());
	
	Branch branch=null;
	boolean branchAsCompany=false;
	branchAsCompany=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", invoice.getCompanyId());
	
	if(branchAsCompany){
		logger.log(Level.SEVERE,"Inside BranchAsCompany");
		if(invoice.getBranch()!=null&&!invoice.getBranch().equals("")){
			
			branch= ofy().load().type(Branch.class)
						.filter("companyId", invoice.getCompanyId())
						.filter("buisnessUnitName", invoice.getBranch()).first().now();
		}
		if(branch != null&&branch.getGSTINNumber()!=null&&!branch.getGSTINNumber().equals("")){
			company = ServerAppUtility.changeBranchASCompany(branch, company);
	 }
	}
	
	//Checking configurations and generating data to be sent with headers
		
		
		String pattern="^[0-9]{2}[A-Z 0-9]{13}$";
		Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		
		String stCodePattern="^[0-9]{1,2}$";
		Pattern stateCodePattern = Pattern.compile(stCodePattern, Pattern.CASE_INSENSITIVE);
		
		
	
//		if(branchAsCompany) {
//			if(company.getCompanyGSTTypeText()!=null&&!company.getCompanyGSTTypeText().equals(""))
//				gstin=company.getCompanyGSTTypeText();
//			else
//				return "Error: GSTIN is missing in Branch.\n Go to Settings -> Branch -> Enter Branch GSTIN ";			
//		}else {
//			gstin=getCompanyGstIn(company);
//			if(gstin.equals(""))
//				return "Error: GSTIN is missing in Company.\n Go to Accounts -> Account Configurations -> Company -> Tax Details -> Add article GSTIN";			
//		}
		
		if(gstin.equals(""))
			responseString+= "Error: GSTIN is missing in Company/Branch. ";
		
		if(!gstin.equals("")){
			Matcher m = p.matcher(String.format(company.getCompanyGSTTypeText()));
			if(!m.find()) {				
				if(branchAsCompany)
					responseString+= "Error: Enter a valid GSTIN in Branch.\n Go to Settings -> Branch -> Enter Branch GSTIN. ";			
				else
					responseString+= "Error: Enter a valid GSTIN in Company.\n Go to Accounts -> Account Configurations -> Company -> Tax Details -> Add article GSTIN. ";					
			}				
		}
		
		logger.log(Level.SEVERE,"GSTIN : "+gstin);
		if(branchAsCompany) {
			logger.log(Level.SEVERE,"branchAsCompany active ");
//			ArrayList<BranchUsernamePassword> branchUsernamePasswordList= new ArrayList<BranchUsernamePassword>();
			ArrayList<BranchUsernamePassword> branchUsernamePasswordList=company.getBranchUsernamePasswordList();
			try {
				logger.log(Level.SEVERE,"branchUsernamePasswordList size="+branchUsernamePasswordList.size());
			
			if(branchUsernamePasswordList!=null&&branchUsernamePasswordList.size()>0) {
				 logger.log(Level.SEVERE,"branchUsernamePasswordList size ::"+branchUsernamePasswordList.size());
				for(BranchUsernamePassword up:branchUsernamePasswordList) {
					logger.log(Level.SEVERE,"up.getBranchName()="+up.getBranchName()+" invoice branch="+invoice.getBranch());
					
					if(up.getBranchName().equals(invoice.getBranch())) {
						username=up.getUsername();
						password=up.getPassword();
						break;
					}
				}
				if(username.equals("")||password.equals("")) {
					responseString+= "Error: Username/Password is missing for branch "+invoice.getBranch()+". \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Username and Password received for the branch from Government portal.";					
				}
			}else {
				responseString+= "Error: Username/Password is missing for branch "+company.getBusinessUnitName()+". \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Username and Password received for the branch from Government portal. ";									
			}
			}catch(Exception e) {
				e.printStackTrace();
			}
				
		}else {
			logger.log(Level.SEVERE,"branchAsCompany not active ");
			if(company.getUserName()!=null&&!company.getUserName().equals(""))
				username=company.getUserName();
			else
				responseString+= "Error: Username is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Username received from Government portal. ";
			
			if(company.getPassword()!=null&&!company.getPassword().equals(""))
				password=company.getPassword();
			else
				responseString+= "Error: Password is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Password received from Government portal. ";
			
		}
		logger.log(Level.SEVERE,"Username : "+username);
		logger.log(Level.SEVERE,"Password : "+password);
		if(company.getEinvoicevendor()!=null&&!company.getEinvoicevendor().equals(AppConstants.TAXILLA)) {
			if(company.getCustomerId()!=null&&!company.getCustomerId().equals("")) 				
				custId=company.getCustomerId();
			else
				responseString+= "Error: Customer Id is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Customer Id received from Government portal. ";
				
		}
			
		logger.log(Level.SEVERE,"Customer Id : "+custId);
		if(company.getApiId()!=null&&!company.getApiId().equals(""))
			apiId=company.getApiId();
		else
			responseString+= "Error: API ID is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter API ID received from Government portal. ";
		
		logger.log(Level.SEVERE,"API ID : "+apiId);
		if(company.getApiSecret()!=null&&!company.getApiSecret().equals(""))
			apiSecret=company.getApiSecret();
		else
			responseString+= "Error: API Secret is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter API Secret received from Government portal. ";
		logger.log(Level.SEVERE,"API Secret : "+apiSecret);
				
		//Checking if states are defined in the system
		List<State> stateList= ofy().load().type(State.class)
				.filter("companyId", invoice.getCompanyId()).list();
		if(stateList==null)
			responseString+= "Error: Define states and state codes in system and try again. ";
		if(stateList.size()<1)
			responseString+= "Error: Define states and state codes in system and try again. ";
		
		
		//Loading customer
		Customer cust = ofy().load().type(Customer.class)
				.filter("count", invoice.getPersonInfo().getCount())
				.filter("companyId", invoice.getCompanyId()).first()
				.now();
		
		CustomerBranchDetails customerBranch=null;
		boolean customerBranchGSTFlag=false;
		if(invoice.getCustomerBranch()!=null&&!invoice.getCustomerBranch().equals("")) {
			customerBranch= ofy().load().type(CustomerBranchDetails.class)
					.filter("cinfo.count",invoice.getPersonInfo().getCount())
					.filter("companyId", invoice.getCompanyId())
					.filter("buisnessUnitName", invoice.getCustomerBranch()).first().now();
		
			if(customerBranch!=null&&customerBranch.getGSTINNumber()!=null&&!customerBranch.getGSTINNumber().equals(""))
				customerBranchGSTFlag=true;
		}
		
		
		
		
		String supplyType="B2B";
		if(cust!=null&&cust.geteInvoiceSupplyType()!=null&&!cust.geteInvoiceSupplyType().equals(""))
			supplyType=cust.geteInvoiceSupplyType();
		
		
	if(company.getEinvoicevendor()!=null&&company.getEinvoicevendor().equals(AppConstants.MYGSTCAFE)) {
		logger.log(Level.SEVERE,"einvoice vendor="+company.getEinvoicevendor());
		//==========================================================================
		//Generating JSON body to be sent with the request
		JSONObject mainObj=new JSONObject();
		
//		Header Details : This section has Tax Scheme, Version, Invoice Reference No. 
		mainObj.put("Version", "1.1");
			
//		Transaction Details :  This section has Transaction details
		JSONObject transDtLsObj=new JSONObject();
		transDtLsObj.put("TaxSch", "GST");
		transDtLsObj.put("SupTyp", "B2B");
		transDtLsObj.put("RegRev", "N");//optional
//		 "EcmGstin": null,     //optional
//		 "IgstOnIntra": "N" 	//optional	
		mainObj.put("TranDtls", transDtLsObj);
		
//		Document Details : This section has the Document details like Type, Number , Date etc
		JSONObject docDtlsObj=new JSONObject();
		docDtlsObj.put("Typ", "INV");
		if(refNoAsDocumentIdFlag) {
			if(invoice.getRefNumber()!=null&&!invoice.getRefNumber().equals(""))
				docDtlsObj.put("No",invoice.getRefNumber()+"");
			else
				responseString+= "Error: Reference Number is missing in Invoice. Please add and try again. ";
		}else		
			docDtlsObj.put("No",invoice.getCount()+"");
		if(invoice.getInvoiceDate()!=null)
			docDtlsObj.put("Dt",sdf.format(invoice.getInvoiceDate())+"");
		else
			responseString+= "Error: Invoice Date required. Please define and try again. ";
		mainObj.put("DocDtls", docDtlsObj);
		
//		Seller Details : This section contains the Seller details like GSTIN, Tradename , Address etc
		JSONObject sellerDtlsObj=new JSONObject();	
		
		

		if(!gstin.equals("")){
			Matcher m = p.matcher(String.format(gstin));
			if(m.find()) {
				sellerDtlsObj.put("Gstin", gstin);
			}else {
				if(branchAsCompany)
					responseString+= "Error: Enter a valid GSTIN in Branch.\n Go to Settings -> Branch -> Enter Branch GSTIN. ";			
				else
					responseString+= "Error: Enter a valid GSTIN in Company.\n Go to Accounts -> Account Configurations -> Company -> Tax Details -> Add article GSTIN. ";					
			}
				
		}

		logger.log(Level.SEVERE,"Company address="+company.getAddress().getCompleteAddress());
		sellerDtlsObj.put("LglNm", company.getBusinessUnitName());
//		sellerDtlsObj.put("TrdNm", "");//optional		
		sellerDtlsObj.put("Addr1", company.getAddress().getAddrLine1());
		sellerDtlsObj.put("Addr2",company.getAddress().getAddrLine2());//optional	
		sellerDtlsObj.put("Loc", company.getAddress().getCity());
		String pin=String.valueOf(company.getAddress().getPin());
		logger.log(Level.SEVERE,"Company pin="+company.getAddress().getPin());
		if(pin.length()==6)	
			sellerDtlsObj.put("Pin", company.getAddress().getPin());
		else
			responseString+= "Error: Enter a valid 6 digit pincode in Company/Branch address and try again. ";
		String compStateCode="";
		
		if(company.getContact().get(0).getAddress().getState()!=null&&!company.getContact().get(0).getAddress().getState().equals(""))
			compStateCode=getStateCode(company.getContact().get(0).getAddress().getState(), stateList);
		else
			responseString+= "Error: State is missing in Company/Branch address. Please define and try again. ";	
		
		if(compStateCode.equals(""))
			responseString+= "Error: State code missing for "+company.getContact().get(0).getAddress().getState()+". Please define and try again. ";

		if(compStateCode!=null&&!compStateCode.equals("")) {
			Matcher m = stateCodePattern.matcher(String.format(compStateCode));
			if(!m.find()) 
				responseString+= "Error: Wrong state code found for "+company.getContact().get(0).getAddress().getState()+ " state. Go to settings -> State -> Set correct state code. ";	
		}
		sellerDtlsObj.put("Stcd",compStateCode);//company.getStateCode()
		sellerDtlsObj.put("Ph", company.getCellNumber1());//optional	
		sellerDtlsObj.put("Em", company.getEmail());//optional		
		mainObj.put("SellerDtls", sellerDtlsObj);
		
//		Buyer Details : This section contains the Buyer details like GSTIN, Tradename , Address etc
		
		JSONObject buyerDtlsObj=new JSONObject();
		
		if(customerBranchGSTFlag) {
			
			if(!customerBranch.getGSTINNumber().equals("")) {
				Matcher m = p.matcher(String.format(customerBranch.getGSTINNumber()));
				if(m.find()) 
					buyerDtlsObj.put("Gstin", customerBranch.getGSTINNumber());
				else 
					responseString+= "Error: Enter valid GSTIN for Customer branch "+customerBranch.getBusinessUnitName() +" and try again. ";
			}		
			String custName="";
			if (cust.getCustPrintableName() != null
					&& !cust.getCustPrintableName().equals("")) {
				custName = cust.getCustPrintableName().trim();
			} else if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			}else {
				custName =  cust.getFullname().trim();
			}
				
			buyerDtlsObj.put("LglNm", custName);
//			buyerDtlsObj.put("TrdNm", "");//optional
			
			buyerDtlsObj.put("Addr1", customerBranch.getBillingAddress().getAddrLine1());
			if(customerBranch.getBillingAddress().getAddrLine2()!=null&&!customerBranch.getBillingAddress().getAddrLine2().equals(""))
				buyerDtlsObj.put("Addr2", customerBranch.getBillingAddress().getAddrLine2());//optional
			else
				responseString+= "Error: Enter address line2 in customer branch address. Customer branch : "+customerBranch.getBusinessUnitName()+". ";
			buyerDtlsObj.put("Loc", customerBranch.getBillingAddress().getCity());
			String custStateCode="";
			if(customerBranch.getBillingAddress().getState()!=null&&!customerBranch.getBillingAddress().getState().equals(""))
				custStateCode=getStateCode(customerBranch.getBillingAddress().getState(), stateList);
			else
				responseString+= "Error: State is missing customer branch address. Please define and try again. Customer branch : "+customerBranch.getBusinessUnitName()+". ";
			
			if(custStateCode.equals(""))
				responseString+= "Error: State code missing for "+customerBranch.getBillingAddress().getState()+". \n Go to settings -> State -> Enter statecode and try again. ";
			
			if(custStateCode!=null&&!custStateCode.equals("")) {
				Matcher m = stateCodePattern.matcher(String.format(custStateCode));
				if(!m.find()) 
					responseString+= "Error: Wrong state code found for "+customerBranch.getBillingAddress().getState()+ " state. Go to settings -> State -> Set correct state code. ";	
			}
			
			buyerDtlsObj.put("Pos", custStateCode);//place of suppy can be different if goods are getting delivered to different state than customer's state
			buyerDtlsObj.put("Stcd", custStateCode);
			String custPin=String.valueOf(cust.getAdress().getPin());
			if(custPin!=null&&custPin.length()==6)		
				buyerDtlsObj.put("Pin",customerBranch.getBillingAddress().getPin());//optional	
			else
				responseString+= "Error: Enter a valid 6 digit pincode in customer branch address and try again. Customer branch : "+customerBranch.getBusinessUnitName()+". ";
			if(customerBranch.getCellNumber1()!=null&&!customerBranch.getCellNumber1().equals(""))
				buyerDtlsObj.put("Ph", customerBranch.getCellNumber1());//optional	
			if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals(""))
				buyerDtlsObj.put("Em", customerBranch.getEmail());//optional		
			
		}else {
			ArrayList <ArticleType> articleList=cust.getArticleTypeDetails();
			String custGstNo="";
			for(ArticleType ar:articleList)
			{
				if(ar.getArticleTypeName().equals("GSTIN"))
					custGstNo=ar.getArticleTypeValue();
			}
			if(!custGstNo.equals("")) {
				Matcher m = p.matcher(String.format(custGstNo));
				if(m.find()) 
					buyerDtlsObj.put("Gstin", custGstNo);
				else 
					responseString+= "Error: Enter valid GSTIN for Customer and try again. \n Go to customer ->Add GSTIN in Article Information tab. ";
			}		
			else
				responseString+= "Error: Customer GSTIN required. \n Go to customer ->Add GSTIN in Article Information tab. ";
			String custName="";
			if (cust.getCustPrintableName() != null
					&& !cust.getCustPrintableName().equals("")) {
				custName = cust.getCustPrintableName().trim();
			} else if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			}else {
				custName =  cust.getFullname().trim();
			}
				
			buyerDtlsObj.put("LglNm", custName);
//			buyerDtlsObj.put("TrdNm", "");//optional
			
			buyerDtlsObj.put("Addr1", cust.getAdress().getAddrLine1());
			if(cust.getAdress().getAddrLine2()!=null&&!cust.getAdress().getAddrLine2().equals(""))
				buyerDtlsObj.put("Addr2", cust.getAdress().getAddrLine2());//optional
			else
				responseString+= "Error: Enter address line2 in customer address. ";
			buyerDtlsObj.put("Loc", cust.getAdress().getCity());
			String custStateCode="";
			if(cust.getAdress().getState()!=null&&!cust.getAdress().getState().equals(""))
				custStateCode=getStateCode(cust.getAdress().getState(), stateList);
			else
				responseString+= "Error: State is missing customer address. Please define and try again. ";
			
			if(custStateCode.equals(""))
				responseString+= "Error: State code missing for "+cust.getAdress().getState()+". \n Go to settings -> State -> Enter statecode and try again. ";
			
			if(custStateCode!=null&&!custStateCode.equals("")) {
				Matcher m = stateCodePattern.matcher(String.format(custStateCode));
				if(!m.find()) 
					responseString+= "Error: Wrong state code found for "+cust.getAdress().getState()+ " state. Go to settings -> State -> Set correct state code. ";	
			}
			
			buyerDtlsObj.put("Pos", custStateCode);//place of suppy can be different if goods are getting delivered to different state than customer's state
			buyerDtlsObj.put("Stcd", custStateCode);
			String custPin=String.valueOf(cust.getAdress().getPin());
			if(custPin.length()==6)		
				buyerDtlsObj.put("Pin",cust.getAdress().getPin());//optional	
			else
				responseString+= "Error: Enter a valid 6 digit pincode in customer address and try again. ";
			if(cust.getCellNumber1()!=null&&!cust.getCellNumber1().equals(""))
				buyerDtlsObj.put("Ph", cust.getCellNumber1());//optional	
			if(cust.getEmail()!=null&&!cust.getEmail().equals(""))
				buyerDtlsObj.put("Em", cust.getEmail());//optional		
		}
		
		
		
		
		mainObj.put("BuyerDtls", buyerDtlsObj);
		
//		Since Dispatch details and Ship to details are non mandatory we are not sending any data regarding it.
//		if(invoice.getTypeOfOrder().equals("Sales Order")) {
////			Dispatch Details :  Contains Dispatch details such as GSTIN, Tradename, Address etc	
//			JSONObject dispDtlsObj=new JSONObject();
//			dispDtlsObj.put("Nm", "");
//			dispDtlsObj.put("Addr1", "");
//			dispDtlsObj.put("Addr2", "");//optional	
//			dispDtlsObj.put("Loc", "");
//			dispDtlsObj.put("Pin", "");
//			dispDtlsObj.put("Stcd", "");
//			mainObj.put("DispDtls", dispDtlsObj);
//			
////			Ship To Details : Contains the Ship To details   such as GSTIN, Tradename, Address etc 
//			JSONObject shipDtlsObj=new JSONObject();
//			shipDtlsObj.put("Gstin", "");//optional	
//			shipDtlsObj.put("LglNm", "");
//			shipDtlsObj.put("TrdNm", "");//optional	
//			shipDtlsObj.put("Addr1", "");
//			shipDtlsObj.put("Addr2", "");//optional	
//			shipDtlsObj.put("Loc", "");
//			shipDtlsObj.put("Pin", "");
//			shipDtlsObj.put("Stcd", "");
//			mainObj.put("ShipDtls", shipDtlsObj);
//		}
		
//		Item Details : This section contains details of Line Items
		JSONArray itemListArray=new JSONArray();
		
		int productCount=invoice.getSalesOrderProducts().size();
		double totalAssAmt=0;
		double totalCgst=0,totalSgst=0,totalIgst=0;
		for(int i=0;i<productCount;i++) {
			JSONObject itemObj=new JSONObject();
			SalesOrderProductLineItem item= invoice.getSalesOrderProducts().get(i);
			itemObj.put("SlNo", i);
			itemObj.put("PrdDesc", item.getProdName());//optional
			if(invoice.getTypeOfOrder().equals("Sales Order")) {
				itemObj.put("IsServc", "N");
				if(item.getHsnCode()!=null && !item.getHsnCode().equals(""))
					itemObj.put("HsnCd", item.getHsnCode());
				else
					responseString+= "Error: HSN code required for product "+item.getProdName()+". ";
			}else {
				itemObj.put("IsServc", "Y");
				if(item.getHsnCode()!=null && !item.getHsnCode().equals(""))
					itemObj.put("HsnCd", item.getHsnCode());//SAC code it should be
				else
					responseString+= "Error: SAC code required for product "+item.getProdName()+". ";		
			}
			itemObj.put("UnitPrice", df.format(item.getPrice()));
			itemObj.put("TotAmt", df.format(item.getBasePaymentAmount()));//Total Amount (Unit Price * Quantity) //df.format(item.getTotalAmount() removed this coz in case of monthly payment total amount and base amount was different
			//base billing amount - getBaseBillingAmount
			//payable amount -getBasePaymentAmount
			itemObj.put("AssAmt",  df.format(item.getBasePaymentAmount()));//Assessable Amount = Total Amount - Discount 
			totalAssAmt+=item.getBasePaymentAmount();
			double gstrate=0;
			double sgstAmt=0;
			double cgstAmt=0;
			double igstAmt=0;
			if(item.getVatTax()!=null) {
				gstrate+=item.getVatTax().getPercentage();
				if(item.getVatTax().getTaxName().startsWith("CGST")){
					cgstAmt=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalCgst+=cgstAmt;
				}else if(item.getVatTax().getTaxName().startsWith("SGST")){
					sgstAmt=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalSgst+=sgstAmt;
				}else if(item.getVatTax().getTaxName().startsWith("IGST")){
					igstAmt=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalIgst+=igstAmt;
				}
			}
			if(item.getServiceTax()!=null) {
				gstrate+=item.getServiceTax().getPercentage();
				if(item.getServiceTax().getTaxName().startsWith("CGST")){
					cgstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalCgst+=cgstAmt;
				}else if(item.getServiceTax().getTaxName().startsWith("SGST")){
					sgstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalSgst+=sgstAmt;
				}else if(item.getServiceTax().getTaxName().startsWith("IGST")){
					igstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalIgst+=igstAmt;
				}			
			}
			itemObj.put("GstRt", gstrate);//GST Rate 
				
			itemObj.put("SgstAmt", df.format(sgstAmt));
			itemObj.put("CgstAmt",  df.format(cgstAmt));
			itemObj.put("IgstAmt",  df.format(igstAmt));
			
			double totalItemValue=item.getBasePaymentAmount() + (item.getBasePaymentAmount()*gstrate/100);
			itemObj.put("TotItemVal",  df.format(totalItemValue)); //Total Item Value = Assessable Amount + Igst Amount + Cgst Amount + Sgst Amount + Cess Amount + Cess Nonadvol + State cess amount + State Cess Non advol +  Other charges
					
			itemListArray.put(itemObj);//added on object
		
		}
		mainObj.put("ItemList", itemListArray);
		
		
		//Document Total: This section contains all totals of the document
		JSONObject docDetailsObj=new JSONObject();
		docDetailsObj.put("AssVal",  df.format(totalAssAmt));
		docDetailsObj.put("TotInvVal",  df.format(invoice.getNetPayable()));
		docDetailsObj.put("CgstVal",  df.format(totalCgst));
		docDetailsObj.put("SgstVal",  df.format(totalSgst));
		docDetailsObj.put("IgstVal",  df.format(totalIgst));	
		mainObj.put("ValDtls", docDetailsObj);

		//==========================================================================
		Invoice inv=ofy().load().type(Invoice.class)
				.filter("companyId", invoice.getCompanyId()).filter("count", invoice.getCount()).first()
				.now();
		
		
		//===================================API call============================================
		logger.log(Level.SEVERE,"responseString ::"+responseString);
		if(!responseString.equals("")) {
			if(taskId!=null) {
				inv.setBulkEinvoiceTaskID(taskId);
				inv.seteInvoiceResult(responseString);
				ofy().save().entity(inv).now();
			}
			return responseString;
		}else {
				OkHttpClient client = new OkHttpClient().newBuilder()
			      .build();
			    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
//			    String jsonString=createJSONForIRNRequest(invoice);
			    String jsonString=mainObj.toJSONString();
			    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
			    if(jsonString.startsWith("Error:"))
			    	responseString+= jsonString;
			    RequestBody body = RequestBody.create(mediaType, jsonString);
			    Request request = new Request.Builder()
			      .url("https://testapi.mygstcafe.com/eicore/v1.03/Invoice")
			      .method("POST", body)
			      .addHeader("Source", "API")
			      .addHeader("Gstin",gstin )//"27AAFCP0535R012"
			      .addHeader("Username",username)//"speaktosatishmh"
			      .addHeader("Password",password)//"Satish@9044"
			      .addHeader("CustomerId",custId)//"C30026"
			      .addHeader("APIId",apiId)//"f7KWT4e0-iQUb-bpf2-G1lY-uUGDebdP"
			      .addHeader("APISecret",apiSecret)//"f7KWT4e0iQUbbpf2"
			      .addHeader("Encryption", "False")
			      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
			      .build();
			  Response response;
				
				try {
					response = client.newCall(request).execute();
					String res=response.body().string();
					logger.log(Level.SEVERE,"Response: "+res);
					
					org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
					logger.log(Level.SEVERE,"response filter 2"+resObj.toString());
					org.json.JSONObject resData=null;
					if(res.contains("response_data")) {
						resData=resObj.getJSONObject("response_data");
						logger.log(Level.SEVERE,"response_data: "+resData.toString());
						String irn="";
						String ackNo="";
						String ackDate="";
						String qrCode="";
						if(resData!=null) {
							irn=resData.getString("Irn");
							ackNo=resData.getString("AckNo");
							ackDate=resData.getString("AckDt");
							qrCode=resData.getString("SignedQrCode");//SignedInvoice
							inv.setIRN(irn);
							inv.setIrnAckNo(ackNo);
							inv.setIrnAckDate(ackDate);
							inv.setIrnQrCode(qrCode);
							inv.setIrnStatus("Active");
							if(loggedInUser!=null&&!loggedInUser.equals("")) {
								if(inv.getLog()!=null)
									inv.setLog(inv.getLog()+" "+loggedInUser+" has generated einvoice on "+datetime.format(new Date()));
								else
									inv.setLog(loggedInUser+" has generated einvoice on "+datetime.format(new Date()));							
							}
							logger.log(Level.SEVERE,"IRN status set to Active");
							
							//task id is generated and getting passed when bulk einvoice genration process starts.
							if(taskId!=null) {
								inv.setBulkEinvoiceTaskID(taskId);
								inv.seteInvoiceResult("IRN generated successfully through bulk generation.");								
							}
							if(bulkGenerationFlag) {
								//Creating accounting interface entry only in case of bulk einvoice generation								
								CommonServiceImpl commonservice = new CommonServiceImpl();
								commonservice.createAccountingInterface(inv);
							}
							
							ofy().save().entity(inv).now();
							responseString+= "Success"; //"irn="+irn;							
							return responseString;
						}
					}else if(res.contains("\"Error\":[")) {
						org.json.JSONObject errorObj;
						String errorString="";
						JSONArray errorArray=resObj.getJSONArray("Error");
						logger.log(Level.SEVERE,"errorArray length"+errorArray.length());
						for(int i=0;i<errorArray.length();i++){
							logger.log(Level.SEVERE,"in for");
							errorObj= errorArray.getJSONObject(i);                 //new org.json.JSONObject(errorArray.get(i));
							logger.log(Level.SEVERE,errorObj.toString());
							errorString+=errorObj.optString("ErrorMessage")+"\n";
						}
						logger.log(Level.SEVERE,"Errors: "+errorString);
						responseString+= errorString;
						if(taskId!=null) {
							inv.setBulkEinvoiceTaskID(taskId);
							inv.seteInvoiceResult(responseString);
							ofy().save().entity(inv).now();
						}
						return responseString;
					}				
					else {
						logger.log(Level.SEVERE,"Errors: "+res);
						responseString+= res;
						if(taskId!=null) {
							inv.setBulkEinvoiceTaskID(taskId);
							inv.seteInvoiceResult(responseString);
							ofy().save().entity(inv).now();
						}
						return responseString;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				} 
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	
			
	}else if(company.getEinvoicevendor()!=null&&company.getEinvoicevendor().equals(AppConstants.TAXILLA)) {
		logger.log(Level.SEVERE,"einvoice vendor="+company.getEinvoicevendor());
		
		
		//==========================================================================
		String environment="";
		if(company.geteInvoiceEnvironment()!=null&&!company.geteInvoiceEnvironment().equals(""))
			environment=company.geteInvoiceEnvironment();
		else
			responseString+= "Error: Einvoice environment not set! Go to Settings -> Company -> Einvoice tab -> select eInvoice environment and save. ";
		
		
		//Generating JSON body to be sent with the request
		JSONObject mainObj=new JSONObject();
		
//		Header Details : This section has Tax Scheme, Version, Invoice Reference No. 
		mainObj.put("Version", "1.1");
			
//		Transaction Details :  This section has Transaction details
		JSONObject transDtLsObj=new JSONObject();
		transDtLsObj.put("TaxSch", "GST");
		transDtLsObj.put("SupTyp", supplyType);
		transDtLsObj.put("RegRev", "N");//optional
//		 "EcmGstin": null,     //optional
//		 "IgstOnIntra": "N" 	//optional	
		mainObj.put("TranDtls", transDtLsObj);
		
//		Document Details : This section has the Document details like Type, Number , Date etc
		JSONObject docDtlsObj=new JSONObject();
		docDtlsObj.put("Typ", "INV");
		if(refNoAsDocumentIdFlag) {
			if(invoice.getRefNumber()!=null&&!invoice.getRefNumber().equals(""))
				docDtlsObj.put("No",invoice.getRefNumber()+"");
			else
				responseString+= "Error: Reference Number is missing in Invoice. Please add and try again. ";
		}else		
			docDtlsObj.put("No",invoice.getCount()+"");
		
		
		
		
		if(invoice.getInvoiceDate()!=null)
			docDtlsObj.put("Dt",sdf.format(invoice.getInvoiceDate())+"");
		else
			responseString+= "Error: Invoice Date required. Please define and try again. ";
		mainObj.put("DocDtls", docDtlsObj);
		
//		Seller Details : This section contains the Seller details like GSTIN, Tradename , Address etc
		JSONObject sellerDtlsObj=new JSONObject();	
		
		

		if(!gstin.equals("")){
			Matcher m = p.matcher(String.format(gstin));
			if(m.find()) {
				sellerDtlsObj.put("Gstin", gstin);
			}else {
				if(branchAsCompany)
					responseString+= "Error: Enter a valid GSTIN in Branch.\n Go to Settings -> Branch -> Enter Branch GSTIN. ";			
				else
					responseString+= "Error: Enter a valid GSTIN in Company.\n Go to Accounts -> Account Configurations -> Company -> Tax Details -> Add article GSTIN. ";					
			}
				
		}

		logger.log(Level.SEVERE,"Company address="+company.getAddress().getCompleteAddress());
		sellerDtlsObj.put("LglNm", company.getBusinessUnitName());
//		sellerDtlsObj.put("TrdNm", "");//optional		
		sellerDtlsObj.put("Addr1", company.getAddress().getAddrLine1());
		sellerDtlsObj.put("Addr2",company.getAddress().getAddrLine2());//optional	
		sellerDtlsObj.put("Loc", company.getAddress().getCity());
		String pin=String.valueOf(company.getAddress().getPin());
		logger.log(Level.SEVERE,"Company pin="+company.getAddress().getPin());
		if(pin.length()==6)	
			sellerDtlsObj.put("Pin", company.getAddress().getPin());
		else
			responseString+= "Error: Enter a valid 6 digit pincode in Company/Branch address and try again. ";
		String compStateCode="";
		
		if(company.getContact().get(0).getAddress().getState()!=null&&!company.getContact().get(0).getAddress().getState().equals(""))
			compStateCode=getStateCode(company.getContact().get(0).getAddress().getState(), stateList);
		else
			responseString+= "Error: State is missing in Company/Branch address. Please define and try again. ";	
		
		if(compStateCode.equals(""))
			responseString+= "Error: State code missing for "+company.getContact().get(0).getAddress().getState()+". Please define and try again. ";

		if(compStateCode!=null&&!compStateCode.equals("")) {
			Matcher m = stateCodePattern.matcher(String.format(compStateCode));
			if(!m.find()) 
				responseString+= "Error: Wrong state code found for "+company.getContact().get(0).getAddress().getState()+ " state. Go to settings -> State -> Set correct state code. ";	
		}
		sellerDtlsObj.put("Stcd",compStateCode);//company.getStateCode()
		sellerDtlsObj.put("Ph", company.getCellNumber1()+"");//optional	
		sellerDtlsObj.put("Em", company.getEmail());//optional		
		mainObj.put("SellerDtls", sellerDtlsObj);
		
//		Buyer Details : This section contains the Buyer details like GSTIN, Tradename , Address etc
		
		JSONObject buyerDtlsObj=new JSONObject();
		
		if(customerBranchGSTFlag) {
			
			if(customerBranch.getGSTINNumber()!=null) {
				Matcher m = p.matcher(String.format(customerBranch.getGSTINNumber()));
				if(m.find()) 
					buyerDtlsObj.put("Gstin", customerBranch.getGSTINNumber());
				else 
					responseString+= "Error: Enter valid GSTIN for Customer branch "+ customerBranch.getBusinessUnitName()+" and try again. ";
			}		
			
			String custName="";
			if (cust.getCustPrintableName() != null
					&& !cust.getCustPrintableName().equals("")) {
				custName = cust.getCustPrintableName().trim();
			} else if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			}else {
				custName =  cust.getFullname().trim();
			}
				
			buyerDtlsObj.put("LglNm", custName);
//			buyerDtlsObj.put("TrdNm", "");//optional
//			buyerDtlsObj.put("Pos", compStateCode);
			buyerDtlsObj.put("Addr1", customerBranch.getBillingAddress().getAddrLine1());
			if(customerBranch.getBillingAddress().getAddrLine2()!=null&&!customerBranch.getBillingAddress().getAddrLine2().equals(""))
				buyerDtlsObj.put("Addr2", customerBranch.getBillingAddress().getAddrLine2());//optional
			else
				responseString+= "Error: Enter address line2 in customer branch address. Customer branch name: "+customerBranch.getBusinessUnitName()+". ";
			buyerDtlsObj.put("Loc", customerBranch.getBillingAddress().getCity());
			String custStateCode="";
			if(customerBranch.getBillingAddress().getState()!=null&&!customerBranch.getBillingAddress().getState().equals(""))
				custStateCode=getStateCode(customerBranch.getBillingAddress().getState(), stateList);
			else
				responseString+= "Error: State is missing customer branch address. Please define and try again.  Customer branch name: "+customerBranch.getBusinessUnitName()+". ";
			
			if(custStateCode.equals(""))
				responseString+= "Error: State code missing for "+customerBranch.getBillingAddress().getState()+". \n Go to settings -> State -> Enter statecode and try again. ";
			
			if(custStateCode!=null&&!custStateCode.equals("")) {
				Matcher m = stateCodePattern.matcher(String.format(custStateCode));
				if(!m.find()) 
					responseString+= "Error: Wrong state code found for "+customerBranch.getBillingAddress().getState()+ " state. Go to settings -> State -> Set correct state code. ";	
			}
			
			buyerDtlsObj.put("Pos", custStateCode);//place of suppy can be different if goods are getting delivered to different state than customer's state		
			buyerDtlsObj.put("Stcd", custStateCode);
			String custPin=String.valueOf(customerBranch.getBillingAddress().getPin());
			if(custPin!=null&&custPin.length()==6)		
				buyerDtlsObj.put("Pin",customerBranch.getBillingAddress().getPin());//optional	
			else
				responseString+= "Error: Enter a valid 6 digit pincode in customer address and try again. ";
			if(customerBranch.getCellNumber1()!=null&&!customerBranch.getCellNumber1().equals("")&&customerBranch.getCellNumber1()>0)
				buyerDtlsObj.put("Ph", customerBranch.getCellNumber1()+"");
			else
				responseString+= "Error: Add cell number in customer branch: "+customerBranch.getBusinessUnitName()+". ";
			
			if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals(""))
				buyerDtlsObj.put("Em", customerBranch.getEmail());//optional	
			
			
		}else {
			ArrayList <ArticleType> articleList=cust.getArticleTypeDetails();
			String custGstNo="";
			for(ArticleType ar:articleList)
			{
				if(ar.getArticleTypeName().equals("GSTIN"))
					custGstNo=ar.getArticleTypeValue();
			}
			if(!custGstNo.equals("")) {
				Matcher m = p.matcher(String.format(custGstNo));
				if(m.find()) 
					buyerDtlsObj.put("Gstin", custGstNo);
				else 
					responseString+= "Error: Enter valid GSTIN for Customer and try again. \n Go to customer ->Add GSTIN in Article Information tab. ";
			}		
			else
				responseString+= "Error: Customer GSTIN required. \n Go to customer ->Add GSTIN in Article Information tab. ";
			String custName="";
			if (cust.getCustPrintableName() != null
					&& !cust.getCustPrintableName().equals("")) {
				custName = cust.getCustPrintableName().trim();
			} else if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			}else {
				custName =  cust.getFullname().trim();
			}
				
			buyerDtlsObj.put("LglNm", custName);
//			buyerDtlsObj.put("TrdNm", "");//optional
//			buyerDtlsObj.put("Pos", compStateCode);
			buyerDtlsObj.put("Addr1", cust.getAdress().getAddrLine1());
			if(cust.getAdress().getAddrLine2()!=null&&!cust.getAdress().getAddrLine2().equals(""))
				buyerDtlsObj.put("Addr2", cust.getAdress().getAddrLine2());//optional
			else
				responseString+= "Error: Enter address line2 in customer address. ";
			buyerDtlsObj.put("Loc", cust.getAdress().getCity());
			String custStateCode="";
			if(cust.getAdress().getState()!=null&&!cust.getAdress().getState().equals(""))
				custStateCode=getStateCode(cust.getAdress().getState(), stateList);
			else
				responseString+= "Error: State is missing customer address. Please define and try again. ";
			
			if(custStateCode.equals(""))
				responseString+= "Error: State code missing for "+cust.getAdress().getState()+". \n Go to settings -> State -> Enter statecode and try again. ";
			
			if(custStateCode!=null&&!custStateCode.equals("")) {
				Matcher m = stateCodePattern.matcher(String.format(custStateCode));
				if(!m.find()) 
					responseString+= "Error: Wrong state code found for "+cust.getAdress().getState()+ " state. Go to settings -> State -> Set correct state code. ";	
			}
			
			buyerDtlsObj.put("Pos", custStateCode);//place of suppy can be different if goods are getting delivered to different state than customer's state		
			buyerDtlsObj.put("Stcd", custStateCode);
			String custPin=String.valueOf(cust.getAdress().getPin());
			if(custPin!=null&&custPin.length()==6)		
				buyerDtlsObj.put("Pin",cust.getAdress().getPin());//optional	
			else
				responseString+= "Error: Enter a valid 6 digit pincode in customer address and try again. ";
			if(cust.getCellNumber1()!=null&&!cust.getCellNumber1().equals("")&&cust.getCellNumber1()>0)
				buyerDtlsObj.put("Ph", cust.getCellNumber1()+"");
			else
				responseString+= "Error: Add cell number in customer. ";
			
			if(cust.getEmail()!=null&&!cust.getEmail().equals(""))
				buyerDtlsObj.put("Em", cust.getEmail());//optional		
		}
		
		
		
		mainObj.put("BuyerDtls", buyerDtlsObj);
		
//		Since Dispatch details and Ship to details are non mandatory we are not sending any data regarding it.
//		if(invoice.getTypeOfOrder().equals("Sales Order")) {
////			Dispatch Details :  Contains Dispatch details such as GSTIN, Tradename, Address etc	
//			JSONObject dispDtlsObj=new JSONObject();
//			dispDtlsObj.put("Nm", "");
//			dispDtlsObj.put("Addr1", "");
//			dispDtlsObj.put("Addr2", "");//optional	
//			dispDtlsObj.put("Loc", "");
//			dispDtlsObj.put("Pin", "");
//			dispDtlsObj.put("Stcd", "");
//			mainObj.put("DispDtls", dispDtlsObj);
//			
////			Ship To Details : Contains the Ship To details   such as GSTIN, Tradename, Address etc 
//			JSONObject shipDtlsObj=new JSONObject();
//			shipDtlsObj.put("Gstin", "");//optional	
//			shipDtlsObj.put("LglNm", "");
//			shipDtlsObj.put("TrdNm", "");//optional	
//			shipDtlsObj.put("Addr1", "");
//			shipDtlsObj.put("Addr2", "");//optional	
//			shipDtlsObj.put("Loc", "");
//			shipDtlsObj.put("Pin", "");
//			shipDtlsObj.put("Stcd", "");
//			mainObj.put("ShipDtls", shipDtlsObj);
//		}
		
//		Item Details : This section contains details of Line Items
		JSONArray itemListArray=new JSONArray();
		
		int productCount=invoice.getSalesOrderProducts().size();
		double totalAssAmt=0;
		double totalCgst=0,totalSgst=0,totalIgst=0;
		for(int i=0;i<productCount;i++) {
			JSONObject itemObj=new JSONObject();
			SalesOrderProductLineItem item= invoice.getSalesOrderProducts().get(i);
			itemObj.put("SlNo", i+"");
			itemObj.put("PrdDesc", item.getProdName());//optional
			if(invoice.getTypeOfOrder().equals("Sales Order")) {
				itemObj.put("IsServc", "N");
				if(item.getHsnCode()!=null && !item.getHsnCode().equals(""))
					itemObj.put("HsnCd", item.getHsnCode()+"");
				else
					responseString+= "Error: HSN code required for product "+item.getProdName()+". ";
			}else {
				itemObj.put("IsServc", "Y");
				if(item.getHsnCode()!=null && !item.getHsnCode().equals(""))
					itemObj.put("HsnCd", item.getHsnCode());//SAC code it should be
				else
					responseString+= "Error: SAC code required for product "+item.getProdName()+". ";			
			}
			itemObj.put("UnitPrice", df.format(item.getPrice()));
			itemObj.put("TotAmt", df.format(item.getBasePaymentAmount()));//Total Amount (Unit Price * Quantity) //df.format(item.getTotalAmount() removed this coz in case of monthly payment total amount and base amount was different
			//base billing amount - getBaseBillingAmount
			//payable amount -getBasePaymentAmount
			itemObj.put("AssAmt",  df.format(item.getBasePaymentAmount()));//Assessable Amount = Total Amount - Discount 
			totalAssAmt+=item.getBasePaymentAmount();
			double gstrate=0;
			double sgstAmt=0;
			double cgstAmt=0;
			double igstAmt=0;
			if(item.getVatTax()!=null) {
				gstrate+=item.getVatTax().getPercentage();
				if(item.getVatTax().getTaxName().startsWith("CGST")){
					cgstAmt=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalCgst+=cgstAmt;
				}else if(item.getVatTax().getTaxName().startsWith("SGST")){
					sgstAmt=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalSgst+=sgstAmt;
				}else if(item.getVatTax().getTaxName().startsWith("IGST")){
					igstAmt=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalIgst+=igstAmt;
				}
			}
			if(item.getServiceTax()!=null) {
				gstrate+=item.getServiceTax().getPercentage();
				if(item.getServiceTax().getTaxName().startsWith("CGST")){
					cgstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalCgst+=cgstAmt;
				}else if(item.getServiceTax().getTaxName().startsWith("SGST")){
					sgstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalSgst+=sgstAmt;
				}else if(item.getServiceTax().getTaxName().startsWith("IGST")){
					igstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalIgst+=igstAmt;
				}			
			}
			itemObj.put("GstRt", gstrate);//GST Rate 
				
			itemObj.put("SgstAmt", df.format(sgstAmt));
			itemObj.put("CgstAmt",  df.format(cgstAmt));
			itemObj.put("IgstAmt", df.format(igstAmt));
			
			double totalItemValue=item.getBasePaymentAmount() + (item.getBasePaymentAmount()*gstrate/100);
			itemObj.put("TotItemVal",  df.format(totalItemValue)); //Total Item Value = Assessable Amount + Igst Amount + Cgst Amount + Sgst Amount + Cess Amount + Cess Nonadvol + State cess amount + State Cess Non advol +  Other charges
					

			//in case of qty taxilla live api gives error for quantity so passing the required parameters
			
			if(invoice.getTypeOfOrder() != null && (invoice.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)||invoice.getTypeOfOrder().equals(AppConstants.ORDERTYPEFORPURCHASE))){
				logger.log(Level.SEVERE,"invoice.getTypeOfOrder() ::"+invoice.getTypeOfOrder());
				if(item.getQuantity()>=1) {
					itemObj.put("Qty", item.getQuantity());
					if(item.getUnitOfMeasurement()!=null&&!item.getUnitOfMeasurement().equals(""))
						itemObj.put("Unit", item.getUnitOfMeasurement());
					else
						itemObj.put("Unit", "NA ");
				}
			}
//			else if(!item.getArea().equalsIgnoreCase("na")) {
//				logger.log(Level.SEVERE,"invoice.getTypeOfOrder() ::"+invoice.getTypeOfOrder());
//				itemObj.put("Qty", Integer.parseInt(item.getArea()));
//				if(item.getUnitOfMeasurement()!=null&&!item.getUnitOfMeasurement().equals(""))
//					itemObj.put("Unit", item.getUnitOfMeasurement());
//				else
//					itemObj.put("Unit", "NA ");
//			}
			itemListArray.put(itemObj);//added on object
		
		}
		mainObj.put("ItemList", itemListArray);
		
		
		//Document Total: This section contains all totals of the document
		JSONObject docDetailsObj=new JSONObject();
		docDetailsObj.put("AssVal",  df.format(totalAssAmt));
		docDetailsObj.put("TotInvVal",  df.format(invoice.getNetPayable()));
		docDetailsObj.put("CgstVal",  df.format(totalCgst));
		docDetailsObj.put("SgstVal",  df.format(totalSgst));
		docDetailsObj.put("IgstVal",df.format( totalIgst));	
		mainObj.put("ValDtls", docDetailsObj);

		//==========================================================================
		Invoice inv=ofy().load().type(Invoice.class)
				.filter("companyId", invoice.getCompanyId()).filter("count", invoice.getCount()).first()
				.now();
		
		
		//===================================API call============================================
		
		//Api to get token: https://gsp.adaequare.com/gsp/authenticate?grant_type=token
		logger.log(Level.SEVERE,"responseString: "+responseString);
		if(!responseString.equals("")) {
			if(taskId!=null) {
				inv.setBulkEinvoiceTaskID(taskId);
				inv.seteInvoiceResult(responseString);
				ofy().save().entity(inv).now();
			}
			return responseString;
		}else {
			String accessToken="";
			OkHttpClient client = new OkHttpClient().newBuilder()
			      .build();
			    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
//			    String jsonString=createJSONForIRNRequest(invoice);
//			    String jsonString=mainObj.toJSONString();
//			    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
//			    if(jsonString.startsWith("Error:"))
//			    	return jsonString;
			    RequestBody body = RequestBody.create(mediaType, "");
			    Request request = new Request.Builder()
			      .url("https://gsp.adaequare.com/gsp/authenticate?grant_type=token")
			      .method("POST", body)
			      .addHeader("gspappid",apiId)//"C3D0123D942A428E85D4FA2EE1926596"
			      .addHeader("gspappsecret",apiSecret)//"7EBBBFFEG4E8EG4377G9E70G0F7C58996C25"
			      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
			      .build();
			  Response response;
				
				try {
					response = client.newCall(request).execute();
					String res=response.body().string();
					logger.log(Level.SEVERE,"Response1: "+res);
					
					org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
					logger.log(Level.SEVERE,"response1 filter 2"+resObj.toString());
					if(res.contains("access_token")) {
						accessToken=resObj.getString("access_token");
						logger.log(Level.SEVERE,"access_token"+accessToken);
						
					}else if(res.contains("\"Error\":[")) {
						org.json.JSONObject errorObj;
						String errorString="";
						JSONArray errorArray=resObj.getJSONArray("Error");
						logger.log(Level.SEVERE,"errorArray1 length"+errorArray.length());
						for(int i=0;i<errorArray.length();i++){
							logger.log(Level.SEVERE,"in for");
							errorObj= errorArray.getJSONObject(i);                 //new org.json.JSONObject(errorArray.get(i));
							logger.log(Level.SEVERE,errorObj.toString());
							errorString+=errorObj.optString("ErrorMessage")+"\n";
						}
						logger.log(Level.SEVERE,"Errors1: "+errorString);
						responseString+=errorString;
						if(taskId!=null) {
							inv.setBulkEinvoiceTaskID(taskId);
							inv.seteInvoiceResult(responseString);
							ofy().save().entity(inv).now();
						}
						return responseString;
						
					}				
					else {
						logger.log(Level.SEVERE,"Errors1: "+res);
						responseString+=res;
						if(taskId!=null) {
							inv.setBulkEinvoiceTaskID(taskId);
							inv.seteInvoiceResult(responseString);
							ofy().save().entity(inv).now();
						}
						return responseString;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				} 
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				if(accessToken!=null&&!accessToken.equals("")) {
					
					//Increased request timeout
					OkHttpClient client2 =  new OkHttpClient.Builder()
						    .connectTimeout(30, TimeUnit.SECONDS)
						    .writeTimeout(30, TimeUnit.SECONDS)
						    .readTimeout(30, TimeUnit.SECONDS)
						    .build();
							
							
//							new OkHttpClient().newBuilder()
//						      .build();
						    MediaType mediaType2 = MediaType.parse("application/json;charset=utf-8");
//						    String jsonString=createJSONForIRNRequest(invoice);
						    String jsonString2=mainObj.toJSONString();
						    logger.log(Level.SEVERE,"jsonString2 ::"+jsonString2);
						    if(jsonString2.startsWith("Error:")) {
						    	responseString+=jsonString2;
								if(taskId!=null) {
									inv.setBulkEinvoiceTaskID(taskId);
									inv.seteInvoiceResult(responseString);
									ofy().save().entity(inv).now();
								}
								return responseString;
						    }
						    RequestBody body2 = RequestBody.create(mediaType2, jsonString2);
						    String url="";
						    if(environment.equalsIgnoreCase("Staging"))
						    	url="https://gsp.adaequare.com/test/enriched/ei/api/invoice";
						    else if(environment.equalsIgnoreCase("Live"))
						    	url="https://gsp.adaequare.com/enriched/ei/api/invoice";
						    logger.log(Level.SEVERE,"url ::"+url);
						    Request request2 = new Request.Builder()
						      .url(url)
						      .method("POST", body2)
						      .addHeader("gstin",gstin )//"01AMBPG7773M002"
						      .addHeader("user_name",username)//"adqgspjkusr1"
						      .addHeader("password",password)//"Gsp@1234"
						      .addHeader("requestid",""+invoice.getCount()+new Date().getTime())//unique id required
						      .addHeader("Authorization","Bearer "+accessToken) //token generated from previous api
						      .addHeader("Content-Type", "application/json;charset=utf-8")
						      .build();
						  Response response2;
							
							try {
								response2 = client2.newCall(request2).execute();
								String res=response2.body().string();
								logger.log(Level.SEVERE,"Response2: "+res);
								
								org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
								logger.log(Level.SEVERE,"response2 filter 2"+resObj.toString());
								org.json.JSONObject resData=null;
								if(res.contains("result")) {
									resData=resObj.getJSONObject("result");
									logger.log(Level.SEVERE,"response_data2: "+resData.toString());
									String irn="";
									String ackNo="";
									String ackDate="";
									String qrCode="";
									if(resData!=null) {
										irn=resData.getString("Irn");
										ackNo=resData.getString("AckNo");
										ackDate=resData.getString("AckDt");
										qrCode=resData.getString("SignedQRCode");//SignedInvoice
										inv.setIRN(irn);
										inv.setIrnAckNo(ackNo);
										inv.setIrnAckDate(ackDate);
										inv.setIrnQrCode(qrCode);
										inv.setIrnStatus("Active");
										if(loggedInUser!=null&&!loggedInUser.equals("")) {
											if(inv.getLog()!=null)
												inv.setLog(inv.getLog()+" "+loggedInUser+" has generated einvoice on "+datetime.format(new Date()));
											else
												inv.setLog(loggedInUser+" has generated einvoice on "+datetime.format(new Date()));							
										}
										
										//task id is generated and getting passed when bulk einvoice genration process starts.
										if(taskId!=null) {
											inv.setBulkEinvoiceTaskID(taskId);
											inv.seteInvoiceResult("IRN generated successfully through bulk generation.");
										}
										
										if(bulkGenerationFlag) {
											//Creating accounting interface entry only in case of bulk einvoice generation								
											CommonServiceImpl commonservice = new CommonServiceImpl();
											commonservice.createAccountingInterface(inv);
										}
										
										logger.log(Level.SEVERE,"IRN status set to Active");
										ofy().save().entity(inv).now();
										return "Success"; //"irn="+irn;
									}
								}else if(res.contains("\"Error\":[")) {
									org.json.JSONObject errorObj;
									String errorString="";
									JSONArray errorArray=resObj.getJSONArray("Error");
									logger.log(Level.SEVERE,"errorArray length"+errorArray.length());
									for(int i=0;i<errorArray.length();i++){
										logger.log(Level.SEVERE,"in for");
										errorObj= errorArray.getJSONObject(i);                 //new org.json.JSONObject(errorArray.get(i));
										logger.log(Level.SEVERE,errorObj.toString());
										errorString+=errorObj.optString("ErrorMessage")+"\n";
									}
									logger.log(Level.SEVERE,"Errors2: "+errorString);
									responseString+=errorString;
									if(taskId!=null) {
										inv.setBulkEinvoiceTaskID(taskId);
										inv.seteInvoiceResult(responseString);
										ofy().save().entity(inv).now();
									}
									return responseString;
									
								}				
								else {
									logger.log(Level.SEVERE,"Errors2: "+res);
									responseString+=res;
									if(taskId!=null) {
										inv.setBulkEinvoiceTaskID(taskId);
										inv.seteInvoiceResult(responseString);
										ofy().save().entity(inv).now();
									}
									return responseString;
								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								
							} 
							catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
					
				}else {					
					responseString+="Error: Failed to generate accessToken. Check log";
					if(taskId!=null) {
						inv.setBulkEinvoiceTaskID(taskId);
						inv.seteInvoiceResult(responseString);
						ofy().save().entity(inv).now();
					}
					return responseString;
				}
		}
		
	}else {
		responseString+="Error: Einvoice vendor not set! Go to Settings -> Company -> Einvoice tab -> select eInvoice vendor and save";
		if(taskId!=null) {
			invoice.setBulkEinvoiceTaskID(taskId);
			invoice.seteInvoiceResult(responseString);
			ofy().save().entity(invoice).now();
		}
		return responseString;
	}
		
	
	return "Success";
}


//Ashwini Patil Date:10-06-2022
public String getStateCode(String stateName,List<State> listOfSates) {
	String stateCode="";
	for (int i = 0; i < listOfSates.size(); i++) {
		if (listOfSates
				.get(i)
				.getStateName()
				.trim()
				.equalsIgnoreCase(stateName.trim())) {
			stateCode = listOfSates.get(i).getStateCode().trim();
			break;
		}
	}
	return stateCode;
}

@Override
public String cancelIRN(Invoice invoice) {
	// TODO Auto-generated method stub

	ServerAppUtility serverapp=new ServerAppUtility();
	
	String gstin="";
	String username="";
	String password="";
	String custId="";
	String apiId="";
	String apiSecret="";

	logger.log(Level.SEVERE,"cancelIRN for invoice id "+invoice.getCount());
	Invoice inv=ofy().load().type(Invoice.class)
			.filter("companyId", invoice.getCompanyId()).filter("count", invoice.getCount()).first()
			.now();

	if(inv.getIRN()==null||inv.getIRN().equals("")) {
		return "No IRN generated for this Invoice!";
	}
	if(inv.getIrnStatus()!=null) {
		if(inv.getIrnStatus().equals("Cancelled")) 
			return "eInvoice already cancelled! Check eInvoice tab for details.";
	}
	
	//==========================================================================	
	
	Company company=ofy().load().type(Company.class)
			.filter("companyId", invoice.getCompanyId()).first()
			.now();
	
	gstin=gstin=serverapp.getGSTINOfCompany(company,inv.getBranch());
	
	
	
	Branch branch=null;
	boolean branchAsCompany=false;
	branchAsCompany=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", invoice.getCompanyId());
	if(branchAsCompany){
		logger.log(Level.SEVERE,"Inside BranchAsCompany");
		if(invoice.getBranch()!=null&&!invoice.getBranch().equals("")){
			
			branch= ofy().load().type(Branch.class)
						.filter("companyId", invoice.getCompanyId())
						.filter("buisnessUnitName", invoice.getBranch()).first().now();
		}
		if(branch != null&&branch.getGSTINNumber()!=null&&!branch.getGSTINNumber().equals("")){
			company = ServerAppUtility.changeBranchASCompany(branch, company);
		}
	}
	String environment="";
	if(company!=null&&company.geteInvoiceEnvironment()!=null&&!company.geteInvoiceEnvironment().equals(""))
		environment=company.geteInvoiceEnvironment();
	else
		return "Error: Einvoice environment not set! Go to Settings -> Company -> Einvoice tab -> select eInvoice environment and save";
	
	
	
		
	
	//==========================================================================
	//API Call
	
	
//	if(branchAsCompany) {
//		if(company.getCompanyGSTTypeText()!=null&&!company.getCompanyGSTTypeText().equals(""))
//			gstin=company.getCompanyGSTTypeText();
//		else
//			return "Error: GSTIN is missing in Branch.\n Go to Settings -> Branch -> Enter Branch GSTIN ";			
//	}else {
		
//		if(gstin.equals(""))
//			return "Error: GSTIN is missing in Company.\n Go to Accounts -> Account Configurations -> Company -> Tax Details -> Add article GSTIN";			
//	}
	if(gstin.equals(""))
		return "Error: GSTIN is missing in Company/Branch";			

	
	if(branchAsCompany) {
		ArrayList<BranchUsernamePassword> branchUsernamePasswordList= new ArrayList<BranchUsernamePassword>();
		branchUsernamePasswordList=company.getBranchUsernamePasswordList();
		if(branchUsernamePasswordList!=null&&branchUsernamePasswordList.size()>0) {
			 logger.log(Level.SEVERE,"branchUsernamePasswordList size ::"+branchUsernamePasswordList.size());
			for(BranchUsernamePassword up:branchUsernamePasswordList) {
				logger.log(Level.SEVERE,"up.getBranchName()="+up.getBranchName()+" invoice branch="+invoice.getBranch());
				
				if(up.getBranchName().equals(invoice.getBranch())) {
					logger.log(Level.SEVERE,"branch matched");
					username=up.getUsername();
					password=up.getPassword();
					break;
				}
			}
			if(username.equals("")||password.equals("")) {
				return "Error: Username/Password is missing for branch "+invoice.getBranch()+". \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Username and Password received for the branch from Government portal";					
			}
		}else {
			return "Error: Username/Password is missing for branch "+company.getBusinessUnitName()+". \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Username and Password received for the branch from Government portal";									
		}			
			
	}else {
		if(company.getUserName()!=null&&!company.getUserName().equals(""))
			username=company.getUserName();
		else
			return "Error: Username is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Username received from Government portal";
		
		if(company.getPassword()!=null&&!company.getPassword().equals(""))
			password=company.getPassword();
		else
			return "Error: Password is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Password received from Government portal";
		
	}
	if(company.getEinvoicevendor()!=null&&!company.getEinvoicevendor().equals(AppConstants.TAXILLA)) {		
		if(company.getCustomerId()!=null&&!company.getCustomerId().equals(""))
			custId=company.getCustomerId();
		else
			return "Error: Customer Id is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter Customer Id received from Government portal";
	}
	if(company.getApiId()!=null&&!company.getApiId().equals(""))
		apiId=company.getApiId();
	else
		return "Error: API ID is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter API ID received from Government portal";
	
	if(company.getApiSecret()!=null&&!company.getApiSecret().equals(""))
		apiSecret=company.getApiSecret();
	else
		return "Error: API Secret is missing in Company. \n Go to Accounts -> Account Configurations -> Company -> E-Invoice tab -> Enter API Secret received from Government portal";
	
	
	
	//===================================API call============================================
	
	if(company.getEinvoicevendor()!=null&&company.getEinvoicevendor().equals(AppConstants.MYGSTCAFE)) {

		JSONObject mainObj=new JSONObject();
		
		mainObj.put("Irn", invoice.getIRN());
		mainObj.put("CnlRsn", "1");
		mainObj.put("CnlRem", "Wrong Entry");
		
		OkHttpClient client = new OkHttpClient().newBuilder()
			      .build();
			    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
//			    String jsonString=createJSONForIRNRequest(invoice);
			    String jsonString=mainObj.toJSONString();
			    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
			    
			    RequestBody body = RequestBody.create(mediaType, jsonString);
			    Request request = new Request.Builder()
			      .url("https://testapi.mygstcafe.com/eicore/v1.03/cancel")
			      .method("POST", body)
			      .addHeader("Source", "API")
			      .addHeader("Gstin",gstin )//"27AAFCP0535R012"
			      .addHeader("Username",username)//"speaktosatishmh"
			      .addHeader("Password",password)//"Satish@9044"
			      .addHeader("CustomerId",custId)//"C30026"
			      .addHeader("APIId",apiId)//"f7KWT4e0-iQUb-bpf2-G1lY-uUGDebdP"
			      .addHeader("APISecret",apiSecret)//"f7KWT4e0iQUbbpf2"
			      .addHeader("Encryption", "False")
			      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
			      .build();
			  Response response;
				
				try {
					response = client.newCall(request).execute();
					String res=response.body().string();
					logger.log(Level.SEVERE,"Response: "+res);
					
					org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
					logger.log(Level.SEVERE,"response filter 2"+resObj.toString());
					org.json.JSONObject resData=null;
					if(res.contains("response_data")) {
						resData=resObj.getJSONObject("response_data");
						logger.log(Level.SEVERE,"response_data: "+resData.toString());
						String cancellationDate="";
						
						if(resData!=null) {
							cancellationDate=resData.getString("CancelDate");
							inv.setIrnStatus("Cancelled");
							inv.setIrnCancellationDate(cancellationDate);
							ofy().save().entity(inv).now();
							return "Success";
						}
					}else if(res.contains("\"Error\":[")) {
						org.json.JSONObject errorObj;
						String errorString="";
						JSONArray errorArray=resObj.getJSONArray("Error");
						logger.log(Level.SEVERE,"errorArray length"+errorArray.length());
						for(int i=0;i<errorArray.length();i++){
							logger.log(Level.SEVERE,"in for");
							errorObj= errorArray.getJSONObject(i);                 //new org.json.JSONObject(errorArray.get(i));
							logger.log(Level.SEVERE,errorObj.toString());
							errorString+=errorObj.optString("ErrorMessage")+"\n";
						}
						logger.log(Level.SEVERE,"Errors: "+errorString);
						return errorString;
						
					}				
					else {
						logger.log(Level.SEVERE,"Errors: "+res);
						return res;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				} 
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
		
	}else if(company.getEinvoicevendor()!=null&&company.getEinvoicevendor().equals(AppConstants.TAXILLA)) {
		logger.log(Level.SEVERE,"vendor taxilla");
		JSONObject mainObj=new JSONObject();
		
		mainObj.put("Irn", invoice.getIRN());
		mainObj.put("Cnlrsn", "1");
		mainObj.put("Cnlrem", "Wrong Entry");
		
		logger.log(Level.SEVERE,"username= "+ username+" password= "+password+" appid="+apiId+" appscret= "+apiSecret);
		String accessToken="";
		OkHttpClient client = new OkHttpClient().newBuilder()
			      .build();
			    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");

			    RequestBody body = RequestBody.create(mediaType, "");
			    Request request = new Request.Builder()
			      .url("https://gsp.adaequare.com/gsp/authenticate?grant_type=token")
			      .method("POST", body)
			      .addHeader("gspappid",apiId)//"C3D0123D942A428E85D4FA2EE1926596"
			      .addHeader("gspappsecret",apiSecret)//"7EBBBFFEG4E8EG4377G9E70G0F7C58996C25"
			      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
			      .build();
			  Response response;
				
				try {
					response = client.newCall(request).execute();
					String res=response.body().string();
					logger.log(Level.SEVERE,"Response1: "+res);
					
					org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
					logger.log(Level.SEVERE,"response1 filter 2"+resObj.toString());
					if(res.contains("access_token")) {
						accessToken=resObj.getString("access_token");
						logger.log(Level.SEVERE,"access_token"+accessToken);
						
					}else if(res.contains("\"Error\":[")) {
						org.json.JSONObject errorObj;
						String errorString="";
						JSONArray errorArray=resObj.getJSONArray("Error");
						logger.log(Level.SEVERE,"errorArray1 length"+errorArray.length());
						for(int i=0;i<errorArray.length();i++){
							logger.log(Level.SEVERE,"in for");
							errorObj= errorArray.getJSONObject(i);                 //new org.json.JSONObject(errorArray.get(i));
							logger.log(Level.SEVERE,errorObj.toString());
							errorString+=errorObj.optString("ErrorMessage")+"\n";
						}
						logger.log(Level.SEVERE,"Errors1: "+errorString);
						return errorString;
						
					}				
					else {
						logger.log(Level.SEVERE,"Errors1: "+res);
						return res;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				} 
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				if(accessToken!=null&&!accessToken.equals("")) {
					OkHttpClient client2 = 
							new OkHttpClient.Builder()
						    .connectTimeout(10, TimeUnit.SECONDS)
						    .writeTimeout(10, TimeUnit.SECONDS)
						    .readTimeout(30, TimeUnit.SECONDS)
						    .build();
							
//							new OkHttpClient().newBuilder()
//						      .build();
						    String jsonString=mainObj.toJSONString();
						    logger.log(Level.SEVERE,"jsonString ::"+jsonString);	
						    logger.log(Level.SEVERE,"accessToken ::"+accessToken);
						    String url="";
						    if(environment.equalsIgnoreCase("Staging"))
						    	url="https://gsp.adaequare.com/test/enriched/ei/api/invoice/cancel";
						    else if(environment.equalsIgnoreCase("Live"))
						    	url="https://gsp.adaequare.com/enriched/ei/api/invoice/cancel";
						    logger.log(Level.SEVERE,"url ::"+url);
						    RequestBody body2 = RequestBody.create(mediaType, jsonString);
						    Request request2= new Request.Builder()
						      .url(url)
						      .method("POST", body2)
						      .addHeader("gstin",gstin )//"01AMBPG7773M002"
						      .addHeader("user_name",username)//"adqgspjkusr1"
						      .addHeader("password",password)//"Gsp@1234"
						      .addHeader("requestid",""+invoice.getCount()+new Date().getTime())//unique id required
						      .addHeader("Authorization","Bearer "+accessToken) //token generated from previous api	      
						      .addHeader("Content-Type", "application/json;charset=utf-8")
						      .build();
						  Response response2;
							
							try {
								response2 = client2.newCall(request2).execute();
								String res=response2.body().string();
								logger.log(Level.SEVERE,"Response2: "+res);
								
								org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
								logger.log(Level.SEVERE,"response filter 2"+resObj.toString());
								org.json.JSONObject resData=null;
								if(res.contains("result")&&res.contains("success")) {
									resData=resObj.getJSONObject("result");
									logger.log(Level.SEVERE,"response_data: "+resData.toString());
									String cancellationDate="";
									
									if(resData!=null) {
										cancellationDate=resData.getString("CancelDate");
										inv.setIrnStatus("Cancelled");
										inv.setIrnCancellationDate(cancellationDate);
										ofy().save().entity(inv).now();
										return "Success";
									}
								}else if(res.contains("\"Error\":[")) {
									org.json.JSONObject errorObj;
									String errorString="";
									JSONArray errorArray=resObj.getJSONArray("Error");
									logger.log(Level.SEVERE,"errorArray length"+errorArray.length());
									for(int i=0;i<errorArray.length();i++){
										logger.log(Level.SEVERE,"in for");
										errorObj= errorArray.getJSONObject(i);                 //new org.json.JSONObject(errorArray.get(i));
										logger.log(Level.SEVERE,errorObj.toString());
										errorString+=errorObj.optString("ErrorMessage")+"\n";
									}
									logger.log(Level.SEVERE,"Errors: "+errorString);
									return errorString;
									
								}				
								else {
									logger.log(Level.SEVERE,"Errors: "+res);
									return res;
								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								
							} 
							catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				}
	
	}else {
		return "Error: Einvoice vendor not set! Go to Settings -> Company -> Einvoice tab -> select eInvoice vendor and save";		
	}
		
		return "Success";

}

//public String getCompanyGstIn(Company company) {
//	String gstNo="";
//	if(company.getArticleTypeDetails()!=null) {
//		ArrayList <ArticleType> articleList=company.getArticleTypeDetails();
//		
//		for(ArticleType ar:articleList)
//		{
//			if(ar.getArticleTypeName().equals("GSTIN")) {
//				gstNo=ar.getArticleTypeValue();
//				return gstNo;
//			}
//		}
//		if(gstNo.equals("")) {
//			if(company.getCompanyGSTTypeText()!=null&&!company.getCompanyGSTTypeText().equals(""))
//				gstNo=company.getCompanyGSTTypeText();	
//			else 
//				return gstNo;
//		}
//	}	
//	else if(company.getCompanyGSTTypeText()!=null&&!company.getCompanyGSTTypeText().equals(""))
//			gstNo=company.getCompanyGSTTypeText();	
//	else 
//		return gstNo;
//	
//	return gstNo;
//}

@Override
public ArrayList<LicenseDetails> getLicenseInfo(Customer customer,String actionTask) {
	

	String msg="";
	ArrayList<LicenseDetails> licenseList=new ArrayList<LicenseDetails>();
		
			if(customer.getRefrNumber1()!=null&&!customer.getRefrNumber1().equals("")){
				
				final String CLIENTURL=customer.getRefrNumber1()+"/slick_erp/getlicenseinfo";
				logger.log(Level.SEVERE,"CLIENT'S URL : "+CLIENTURL);
				
				String data="";
				URL url = null;
				try {
					url = new URL(CLIENTURL);
				} catch (MalformedURLException e) {
					logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
					e.printStackTrace();
					msg="Please add proper client url.";
				}
				logger.log(Level.SEVERE,"STAGE 1");
		        HttpURLConnection con = null;
				try {
					con = (HttpURLConnection) url.openConnection();
					con.setConnectTimeout(60000); 
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
					e.printStackTrace();
					msg +="Error occured in establishing connection.";
				}
				logger.log(Level.SEVERE,"STAGE 2");
		        con.setDoInput(true);
		        con.setDoOutput(true);
//		        con.setRequestProperty("Content-Type","application/json; charset=UTF-8");
		        try {
					con.setRequestMethod("POST");
				} catch (ProtocolException e) {
					logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
					e.printStackTrace();
					msg+="Protocol error.";
				}
		        logger.log(Level.SEVERE,"STAGE 3");
		        OutputStream os = null;
				try {
					os = con.getOutputStream();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
					e.printStackTrace();
				}
				logger.log(Level.SEVERE,"STAGE 4");
		        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		        try {
					writer.write(getDataTosend(actionTask));
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        try {
					writer.flush();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        logger.log(Level.SEVERE,"STAGE 5");
		        
		        
		        
		        InputStream is = null;
				try {
					is = con.getInputStream();
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
					e.printStackTrace();
				}
				
				 logger.log(Level.SEVERE,"STAGE 6");
		        BufferedReader br = new BufferedReader(new InputStreamReader(is)); 
		        String temp;
		        try {
					while ((temp = br.readLine()) != null) {
					    data = data + temp+"\n"+"\n";
					}
					logger.log(Level.SEVERE,"data data::::::::::"+data);
				} catch (IOException e) {
					logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
					e.printStackTrace();
				}
		        
		        logger.log(Level.SEVERE,"STAGE 7");
		        
		        logger.log(Level.SEVERE,"Data::::::::::"+data);
//		        JSONObject jObj=new JSONObject();
//		        jObj.put("response", data);
		        
		        org.json.JSONObject object = null;
				try {
					object = new org.json.JSONObject(data);
				} catch (Exception e) {
					e.printStackTrace();
					msg += e.getMessage();
				}
		        
				Gson gson = new Gson();
				JSONArray jsonarr = new JSONArray();
				try {
					
					logger.log(Level.SEVERE, "object.getNames(object)"+object.getNames(object).toString());
					jsonarr=object.getJSONArray("license_list");
				} catch (JSONException e2) {
					e2.printStackTrace();
					msg += e2.getMessage();
				}
				

				if(msg.trim().equals("")){
					try {
						LicenseDetails licnesedata =null;
						for(int k=0;k<jsonarr.length();k++){
							logger.log(Level.SEVERE,"JSON ARRAY LENGTH :: "+jsonarr.length());
							org.json.JSONObject jobj = jsonarr.getJSONObject(k);
							licnesedata = new LicenseDetails();
							licnesedata = gson.fromJson(jobj.toString(), LicenseDetails.class);
							
							logger.log(Level.SEVERE, licnesedata.getLicenseType());
							logger.log(Level.SEVERE, licnesedata.getContractCount()+"");
							licenseList.add(licnesedata);
						}
						logger.log(Level.SEVERE,"licenselist size"+licenseList.size());

						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else{
					logger.log(Level.SEVERE,"msg"+msg);
					LicenseDetails license = new LicenseDetails();
					license.setContractCount(-1);
					license.setLicenseType(msg);
					licenseList.add(license);
				}
				
				//Ashwini Patil Date:3-07-2024 
				if(licenseList!=null) {
					try {
						Comparator<LicenseDetails> endDateComp=new Comparator<LicenseDetails>() {
							@Override
							public int compare(LicenseDetails arg0, LicenseDetails arg1) {
								return arg0.getEndDate().compareTo(arg1.getEndDate());
							}
						};
						Collections.sort(licenseList, endDateComp);
						logger.log(Level.SEVERE,"licenseList sorted by end date");
					}catch(Exception e) {
						
					}
				}
				return licenseList;

				
			}else{
				 msg+="";
				 	LicenseDetails license = new LicenseDetails();
					license.setContractCount(-1);
					license.setLicenseType("Client url is not mentioned in the reference number 1.");
					licenseList.add(license);
					
					
			}
		
			return licenseList;
	
   }

private String getDataTosend(String actionTask) {

	ServerAppUtility utility = new ServerAppUtility();
	StringBuilder strB=new StringBuilder();
	strB.append("jsonString");
	strB.append("=");
	
	JSONObject jObj=new JSONObject();
	jObj.put("action_task", actionTask);
	
	String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
	
	strB.append(jsonString);
	
	logger.log(Level.SEVERE,"final string"+strB);
	return strB.toString();
	
}

@Override
public String generateEinvoicesInBulk(long companyId, Date fDate, Date tDate, ArrayList<Invoice> invoiceList,String loggedInUser) {
	// TODO Auto-generated method stub
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat idFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	String fromDateInString=null;
	String toDateInString=null;	
	String taskId=idFormat.format(new Date());
	if(invoiceList!=null&&invoiceList.size()>0) {
		logger.log(Level.SEVERE,"Received selected invoices list. task id="+taskId);
		for(Invoice inv:invoiceList) {	
			if(inv.getStatus().equals(Invoice.APPROVED)&&inv.getInvoiceType().equals("Tax Invoice")) {
				String taskName="generateIRN"+"$"+companyId+"$"+inv.getCount()+"$"+taskId+"$"+loggedInUser;
				Queue queue = QueueFactory.getQueue("documentCancellation-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
			}
		}		
	}else {
		try{
			logger.log(Level.SEVERE,"Received fromdate todate. task id="+taskId);
			fDate=DateUtility.getDateWithTimeZone("IST", fDate);
			tDate=DateUtility.getDateWithTimeZone("IST", tDate);
			
			fromDateInString=dateFormat.format(fDate);
			toDateInString=dateFormat.format(tDate);
			
			
			Date fromDate=dateFormat.parse(fromDateInString);
		 
			Date toDate=dateFormat.parse(toDateInString);
			 
			Calendar cal2=Calendar.getInstance();
			cal2.setTime(toDate);
			
			cal2.set(Calendar.HOUR_OF_DAY,23);
			cal2.set(Calendar.MINUTE,59);
			cal2.set(Calendar.SECOND,59);
			cal2.set(Calendar.MILLISECOND,999);
			toDate=cal2.getTime();
			
			
			if(fromDate!=null && toDate!=null) {
				logger.log(Level.SEVERE,"2. COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate);
				List<Invoice> datewiseInvoiceList=ofy().load().type(Invoice.class).filter("companyId", companyId).filter("invoiceDate >=", fromDate).filter("invoiceDate <=", toDate).filter("status", Invoice.APPROVED).list();
				if(datewiseInvoiceList!=null) {
					logger.log(Level.SEVERE,"datewiseInvoiceList size="+datewiseInvoiceList.size());
					for(Invoice inv:datewiseInvoiceList) {	
						if(inv.getStatus().equals(Invoice.APPROVED)&&inv.getInvoiceType().equals("Tax Invoice")) {
							String taskName="generateIRN"+"$"+companyId+"$"+inv.getCount()+"$"+taskId+"$"+loggedInUser;
							Queue queue = QueueFactory.getQueue("documentCancellation-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));						
						}
					}
				}else {
					logger.log(Level.SEVERE,"datewiseInvoiceList size=null");
				}
			}
			
		 } catch (ParseException e) {
			e.printStackTrace();
		 }	
	}
	
	
	
	
	return null;
}


	public String createAttendanceInZoho(long companyId, String checkIn, String checkOut, String empid) {
		logger.log(Level.SEVERE,"In createAttendanceInZoho");
	String result="zohosuccess";
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	
	DecimalFormat df = new DecimalFormat("0.00");
		Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		HashSet<Integer> prodIdSet=new HashSet<Integer>();
		HashMap<Integer, Long> prodZohoIdMap=new HashMap<Integer, Long>();
		
//		String orgId=comp.getZohoOrganisationID();
		String refreshToken=comp.getZohoPermissionsCode();
		String accessToken="";
		String clientid=comp.getZohoClientID();
		String clientSecret=comp.getZohoClientSecret();
	
		JSONObject mainObj=new JSONObject();
		mainObj.put("dateFormat", "dd/MM/yyyy HH:mm:ss");
		mainObj.put("checkIn", checkIn);//"01/08/2024 09:30:45"
		mainObj.put("checkOut",checkOut);//"01/08/2024 18:30:45"
		mainObj.put("empId", empid);//"100000069"
		
		
		String urlForAccessToken="https://accounts.zoho.in/oauth/v2/token?"
				+ "refresh_token="+ refreshToken
				+ "&client_id="+clientid//1000.X4IH6P16MDWMLVTBXU25VOOB4G1OXT
				+ "&client_secret="+clientSecret//5a515dbc7d037d572bb8ef7503355580ade75ece65
				+ "&grant_type=refresh_token";
		
		OkHttpClient client = new OkHttpClient().newBuilder()
			      .build();
			    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
			  
			    RequestBody body = RequestBody.create(mediaType, "");
			    Request request = new Request.Builder()
			      .url(urlForAccessToken)
			      .method("POST", body)
//			      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
			      .build();
			  Response response;
				
				try {
					response = client.newCall(request).execute();
					String res=response.body().string();
					logger.log(Level.SEVERE,"1st Response: "+res);
					org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
					if(res.contains("access_token")) {
						accessToken=resObj.getString("access_token");
						String createAttendanceUrl="https://people.zoho.in/people/api/attendance"+"?dateFormat=dd/MM/yyyy HH:mm:ss&checkIn="+checkIn+"&checkOut="+checkOut+"&empId="+empid;
						logger.log(Level.SEVERE,"createAttendanceUrl= "+createAttendanceUrl);
						String jsonString=mainObj.toJSONString();
					    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
					   
						RequestBody body2 = RequestBody.create(mediaType, "");
						Request request2 = new Request.Builder()
							      .url(createAttendanceUrl)
							      .method("POST", body2)
							      .addHeader("Authorization", "Zoho-oauthtoken "+accessToken)
							      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")							      
							      .build();
							  Response response2;
								
								try {
									response2 = client.newCall(request2).execute();
									String res2=response2.body().string();
									logger.log(Level.SEVERE,"2nd Response: "+res2);
//									if(res2.contains("Error"))
//										return res2;
//									else if(res2.contains("contact")) {
//										org.json.JSONObject res2Obj=new org.json.JSONObject(res2.trim());											
//										org.json.JSONObject resData=res2Obj.getJSONObject("contact");
//										String zohoCustUniqueId=resData.getString("contact_id");
//										cust.setZohoCustomerId(Long.parseLong(zohoCustUniqueId));
//										ofy().save().entity(cust);
//										logger.log(Level.SEVERE,"zoho cust id "+zohoCustUniqueId+" saved in Eva invoice. with value "+Long.parseLong(zohoCustUniqueId) );
//									}else if(res2.contains("message")) {
//										org.json.JSONObject res2Obj=new org.json.JSONObject(res2.trim());											
//										String msg=res2Obj.getString("message");
//										return msg;
//									}else
										return res2;
								}catch(Exception ex) {
									ex.printStackTrace();
									result="Failed";
								}
						
					}
					
				}catch(Exception e) {
					e.printStackTrace();
					result="Failed";
				}
		
		
		
	
	return result;


}
}