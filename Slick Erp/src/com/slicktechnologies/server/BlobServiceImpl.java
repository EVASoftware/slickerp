package com.slicktechnologies.server;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlbeans.SystemProperties;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.libservice.BlobService;
import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;



@SuppressWarnings("serial")
public class BlobServiceImpl extends RemoteServiceServlet implements BlobService{

	
	BlobstoreService blobstoreservice=BlobstoreServiceFactory.getBlobstoreService();
	
	Logger logger = Logger.getLogger("Name of logger");
	
	@Override
	public String getBlobStoreUploadUrl() {
		
		String url=blobstoreservice.createUploadUrl("/slick_erp/uploadservice");
		System.out.println("Created URL IS ---"+url);
		return url;
	}

	@Override
	public DocumentUpload getDocument(String id) {
		
		
	    return null;
	}
	
	 @Override
	  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	      throws ServletException, IOException {
		    BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));
		    String filename=req.getParameter("filename");
		    resp.setHeader("Content-Disposition", "Attachment;filename="+filename);
		    System.out.println("Ajay---");
	        blobstoreservice.serve(blobKey, resp);
	        

	  }

	/**
	 * Date 18-07-2019 by Vijay Des :- NBHC CCPM for Contract Upload and also
	 * usable for static blob key for other operation with new static blobkey
	 */
	@Override
	public String getBlobStoreNewUploadUrl(String transactionName) {
		logger.log(Level.SEVERE, "BLob Service impl");
		String url = null;
		
		/***Deepak Salve added this code for PR Upload***/
		if(transactionName.equals("PurchaseRequisition")){	
			logger.log(Level.SEVERE, "BLob Service impl PurchaseRequisition");
//			 if (DocumentUploadTaskQueue.prequisitionUploadBlobKey == null) {
				 logger.log(Level.SEVERE, "Inside BLob Service impl PurchaseRequisition"); 
				url = blobstoreservice.createUploadUrl("/slick_erp/uploadTaskQueue?TransactionType=" + transactionName);
//				if(SystemProperty.environment.value()== SystemProperty.Environment.Value.Development){
//					url=url.replace("DESKTOP-IJNHQ2G","127.0.0.1");
//				}
				logger.log(Level.SEVERE, "BLob Service impl PurchaseRequisition url here :- "+url);
				return url;
//			}else{
//				return "Inprocess";
//			}
			
		}
		/***End***/
		
		if (DocumentUploadTaskQueue.contractUploadBlobKey == null) {
			url = blobstoreservice.createUploadUrl("/slick_erp/uploadTaskQueue?TransactionType=" + transactionName);
			return url;

		}
		else {
			return "Inprocess";
		}

	}

}
