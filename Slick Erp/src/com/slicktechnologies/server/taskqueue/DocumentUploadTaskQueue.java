package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gwt.core.shared.GWT;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.quotation.SalesLineItemQuotationTable;
import com.slicktechnologies.server.ApprovableServiceImplementor;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.DataMigrationImpl;
import com.slicktechnologies.server.DataMigrationTaskQueueImpl;
import com.slicktechnologies.server.DocumentUploadServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.android.ReportedServiceDetailsSaveServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ComponentDetails;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseBean;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceListDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.SerialNumberMapping;
import com.slicktechnologies.shared.common.inventory.SerialNumberStockMaster;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

/**
 * Date 20-02-2018 By vijay for Uploading data task queue 
 */

public class DocumentUploadTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1040438483441346676L;

	Logger logger = Logger.getLogger("Name of logger");
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");

	SimpleDateFormat sefmt = new SimpleDateFormat("dd-MM-yyyy");

	public static BlobKey blobkey;

	public ArrayList<String> customerNameList = new ArrayList<String>();

	/*** Date 17-08-2019 by Vijay for NBHC CCPM Contract Upload ****/
	public static BlobKey contractUploadBlobKey;
	
	/*** Date 19-11-2019 by Deepak Salve for NBHC Purchase Requisition Upload ****/
	public static BlobKey prequisitionUploadBlobKey; 

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.log(Level.SEVERE, "Welcome to Document upload taskqueue");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		customerNameList.clear();
		String entityName = request.getParameter("EntityName");
		logger.log(Level.SEVERE, " entityName "+entityName);
		long companyId = Long.parseLong(request.getParameter("companyId"));
		logger.log(Level.SEVERE, " companyId "+companyId);
		String priceFlag = request.getParameter("masterPriceFlag");
		String loggedinuser=null;//Ashwini Patil Date:13-05-2023
		try{
			loggedinuser=request.getParameter("loggedinuser");
		}catch(Exception e){
			
		}

		ArrayList<String> exceldatalist = new ArrayList<String>();
		
		 /** Date 16-08-2019 by Vijay for NBHC CCPM Contract upload ***/
		 BlobKey  contractUploadTaskBlobKey = null;
		 if(request.getParameter("contractUploadblobkey")!=null){
			 contractUploadTaskBlobKey = new BlobKey(request.getParameter("contractUploadblobkey"));
		 }
		 BlobKey purchaseRequisitionUploadTaskBlobKey = null;
		 if(request.getParameter("prequisitionUploadBlobKey")!=null){
			 logger.log(Level.SEVERE, " Inside prequisitionUploadBlobKey "+request.getParameter("prequisitionUploadBlobKey"));
			 purchaseRequisitionUploadTaskBlobKey= new BlobKey(request.getParameter("prequisitionUploadBlobKey"));
			 logger.log(Level.SEVERE, " After set purchaseRequisitionUploadTaskBlobKey "+purchaseRequisitionUploadTaskBlobKey);
				System.out.println(" PR "+prequisitionUploadBlobKey);
			 }
		 
		 if(request.getParameter("blobkey")!=null){
			 blobkey = new BlobKey(request.getParameter("blobkey"));
		 }
		 
		 
		if(entityName.equals("CTC Allocation")){
			fmt = new SimpleDateFormat("dd/MM/yyyy");
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
		}
		
		if(entityName.equals(AppConstants.SERVICECREATIONUPLOAD)){
			fmt = new SimpleDateFormat("dd/MM/yyyy");
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
		}

		/*******************
		 * Reading Excel Data & adding in list
		 *****************************/
		try {
			if(!entityName.equals("Payment Updation")){
			logger.log(Level.SEVERE, "Reading excel ");
			logger.log(Level.SEVERE, "Blobkey value" + blobkey);

			long i;
			Workbook wb = null;
			try {
//				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				/*** Date 17-08-2019 by Vijay for NBHC CCPM Contract Upload and else for existing operation ****/
				if(contractUploadTaskBlobKey!=null){
		    		wb = WorkbookFactory.create(new BlobstoreInputStream(contractUploadTaskBlobKey));
		    	}
				else if(purchaseRequisitionUploadTaskBlobKey!=null){
		    		wb = WorkbookFactory.create(new BlobstoreInputStream(purchaseRequisitionUploadTaskBlobKey));
		    	}
		    	else{
					wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
		    	}
				
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {

							// i = (long) cell.getNumericCellValue();
							// logger.log(Level.SEVERE, "Value : "+i);
//							logger.log(Level.SEVERE, "Value : " + cell.getNumericCellValue());
							// exceldatalist.add(cell.getNumericCellValue() +
							// "");
							DecimalFormat decimalFormat = new DecimalFormat("#########.##");
							int value1 = 0, value2 = 0;
							String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
							if (array.length > 0) {
								// value1 = Integer.parseInt(array[0]);
							}
							if (array.length > 1) {
								value2 = Integer.parseInt(array[1]);
							}
							if (value2 == 0) {
								i = (long) cell.getNumericCellValue();
								exceldatalist.add(i + "");
							} else {
								exceldatalist.add(cell.getNumericCellValue() + "");
							}

						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:
					}
				}
			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist.size());

			wb.close();

		}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		/*******************************
		 * end of excel data reading and adding in list
		 *******************/

		/*******************************
		 * Methods for inserting records in Entity
		 ********************/
		if (entityName.equals("Locality")) {
			localitySave(companyId, exceldatalist);
		}
		if (entityName.equals("Customer")) {
			customersSave(companyId, exceldatalist);
		}
		if (entityName.equalsIgnoreCase("Employee Attendance")) {
			SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			String month = request.getParameter("month");
			String year = request.getParameter("year");
			
			Date startDate=null;
			Date endDate=null;
			int startLimit=0;
			int endLimit=0;
			int noOfDays=0;
			int lastUpdatedIndex=0;
			try{
				String startDateStr = request.getParameter("startDate");
				String endDateStr = request.getParameter("endDate");
				
				startDate=fmt.parse(startDateStr);
				endDate=fmt.parse(endDateStr);
				
				
				String startlimitStr = request.getParameter("startLimit");
				String endDatelimitStr = request.getParameter("endLimit");
				
				startLimit=Integer.parseInt(startlimitStr);
				endLimit=Integer.parseInt(endDatelimitStr);
				
				String noOfDaysStr = request.getParameter("noOfDays");
				String lastUpdatedIndexStr = request.getParameter("lastUpdatedIndex");
				
				noOfDays=Integer.parseInt(noOfDaysStr);
				lastUpdatedIndex=Integer.parseInt(lastUpdatedIndexStr);
				
			}catch(Exception e){
				
			}
			logger.log(Level.SEVERE, "Start Date " + startDate+" End Date "+endDate);
			logger.log(Level.SEVERE, "Start Limit " + startLimit+" End Limit "+endLimit);
			logger.log(Level.SEVERE, "No Of Days " + noOfDays+" Last Updated Index "+lastUpdatedIndex);
			employeeAttendanceSave(companyId, exceldatalist, month, year,startDate,endDate,startLimit,endLimit,noOfDays,lastUpdatedIndex);
		}
		/** 
		 * 
		 */
		if (entityName.equals("TechnicianWarehouseDetails")) {
			saveTechnicianWarehouseDetails(companyId, exceldatalist);
		}

		/**
		 * @author Anil , Date : 22-07-2019 Employee Asset upload through task
		 *         queue
		 */

		if (entityName.equals("Employee Asset Allocation")) {
			saveEmployeeAssetAllocation(companyId, exceldatalist);
		}
		
		/**
		 * Date 22-07-2019 by Vijay
		 * Des :- for NBHC CCPM Contract Upload
		 */
		if(entityName.equals("Contract Upload")){
			reactOnContractUpload(companyId,exceldatalist);
		}

		/**
		 * Date 21-11-2019 by Vijay
		 * Des :- NBHC Inventory Management GRN Upload for lock seal
		 */
		if(entityName.equals("GRN")){
			reactonGRN(companyId,exceldatalist);
		}
		if(entityName.equals("MMN")){
			reactonMMN(companyId,exceldatalist);
		}
		/**
		 * Date 27-11-2019
		 * Des :- NBHC Lock Seal IM Stock update directly
		 */
		if(entityName.equals("Lock Seal Stock Update")){
			reactonLockSealStockUpload(companyId,exceldatalist);
		}
		
		if(entityName.equals("Stock Update")){
			reactOnStockUpdateUpload(companyId,exceldatalist);
		}
		if(entityName.equals("Purchase Requisition Upload")){
			reactOnPurchaseRequisitionUpdateUpload(priceFlag,companyId,exceldatalist);
		}
		
		
		if(entityName.equals("Update Fumigation Services")){
			reactOnInHouseServiceUpdation(companyId,exceldatalist);
		}
		
		if(entityName.equals("Vendor Product Price")){
			reactonuploadVendorProdcutPrice(companyId,exceldatalist);
		}
		
		if(entityName.equals("CTC Template")){
			reactonCTCTemplateUpload(companyId);
		}
		
		if(entityName.equals(AppConstants.UPDATEASSETDETAILS)){
			reactonUpdateAssetDetails(companyId,exceldatalist);
		}
		
		if(entityName.equals("Employee Upload")){
			reactonemployeeUpload(companyId,exceldatalist,loggedinuser);
		}
		
		if(entityName.equals("Update Employee")){
			reactonUpdateEmployee(companyId,exceldatalist);
		}
		
		/**
		 * @author Vijay Date 13-10-2020
		 * Des :- below method used for Payment Upload excel and also used for android API to update payment Documents
		 */
		if(entityName.equals("Payment Updation")){
			String strJsonData = request.getParameter("paymentUpdationData");
			reactonPaymentUpdation(companyId,strJsonData);
		}
		
		if(entityName.equals("Service Product Upload")){
			reactonServiceProductUpload(companyId,exceldatalist);
		}
		
		if(entityName.equals("Item Product Upload")){
			reactonItemProductUpload(companyId,exceldatalist);
		}
		
		if(entityName.equals("CTC Allocation")){
			String loggedInUser = request.getParameter("loggedInUser");
			reactonCTCAllocation(companyId,exceldatalist,loggedInUser);
		}
		
		if(entityName.equals("Credit Note Upload")){
			reactonCreditNoteUpload(companyId,exceldatalist);
		}
		if(entityName.equals("Upload Inhouse Services")){
			reactonInhouseServiceUpload(companyId,exceldatalist);
		}
		
		if(entityName.equals("Stock Upload")){
			reactonStockUpload(companyId,exceldatalist);
		}
		
		if(entityName.equals(AppConstants.SERVICECREATIONUPLOAD)){
			reactonServiceCreationUpload(companyId,exceldatalist);
		}
		/**@author Sheetal : 03-06-2022 
		 * Des : This method used for Customer Branch with Service Location and area Upload**/
		
		if(entityName.equals("Customer Branch with Service Location and area Upload")){
			reactonCustomerBranchWithServiceLocationUpload(companyId,exceldatalist);
		}
		
		if(entityName.equals("Service Cancellation Upload")){
			reactOnServiceCancellationUpload(companyId,exceldatalist,loggedinuser);
		}
		if(entityName.equals("Customer Branch Upload")){
			reactOnCustomerBranchUpload(companyId,exceldatalist);
		}
		if(entityName.equals("UpdateCustomerBranchStatus")){
			
			String strJsonData = request.getParameter("UpdationData");
			boolean serviceCancellationFlag =false;
			try{
				serviceCancellationFlag=Boolean.parseBoolean(request.getParameter("ServiceCancellationFlag"));
			}catch(Exception e){
				
			}
			reactonUpdateCustomerBranchStatus(companyId,strJsonData,serviceCancellationFlag);
		}
		if(entityName.equals(AppConstants.ScheduleOrCompleteViaExcel)){
			reactOnServiceUpdate(companyId,exceldatalist,loggedinuser);			
		}
	}



	



	/**
	 * Date 15-03-2018 By vijay for customer upload save
	 */

	private void customersSave(long companyId, ArrayList<String> exceldatalist) {

		ArrayList<Customer> customerlist = new ArrayList<Customer>();

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Customer")
				.filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		List<Customer> listofcustomer = ofy().load().type(Customer.class).filter("companyId", companyId).list();

		System.out.println("exceldatalist  ==" + exceldatalist.size());
		for (int p = 41; p < exceldatalist.size(); p += 41) {
			Customer customer = new Customer();

			boolean customercheck = true;
			boolean clientcheck = true;

			if (exceldatalist.get(p).equalsIgnoreCase("yes")) {
				clientcheck = validationforclientcompnay(listofcustomer, exceldatalist, p);
			} else {
				customercheck = validationforcustomer(listofcustomer, exceldatalist, p);
			}
			if (customercheck == false || clientcheck == false) {
				continue;
			}
			if (exceldatalist.get(p).trim().equalsIgnoreCase("NO")
					|| exceldatalist.get(p).trim().equalsIgnoreCase("NA")) {
				customer.setCompany(false);
			} else {
				customer.setCompany(true);
			}

			if (exceldatalist.get(p + 1).trim().equalsIgnoreCase("na")) {
				customer.setCompanyName("");
			} else {
				String companyName = exceldatalist.get(p + 1).trim().toUpperCase();
				customer.setCompanyName(companyName);
			}

			try {

				if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("NA")) {
					// customer.setDob(fmt.parse(""));
				} else {
					customer.setDob(fmt.parse(exceldatalist.get(p + 2)));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			// rohan added salutation for NBHC in customer Dat : 7/2/2017
			if (exceldatalist.get(p + 3).trim().equalsIgnoreCase("na")) {
				customer.setSalutation("");
			} else {
				customer.setSalutation(exceldatalist.get(p + 3).trim());
			}

			if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("NA")) {
				customer.setFullname();

			} else {
				String fullname = exceldatalist.get(p + 4).trim();
				System.out.println("Fname is  :" + fullname);
				customer.setFullname(fullname.toUpperCase());
				System.out.println("fname.toUpperCase()" + fullname.toUpperCase());
			}
			if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
				customer.setEmail("");

			} else {
				customer.setEmail(exceldatalist.get(p + 5).trim());
			}
			if (exceldatalist.get(p + 6).trim().equalsIgnoreCase("na")) {
				// customer.setLandLine(0l);
			} else {
				customer.setLandLine(Long.parseLong(exceldatalist.get(p + 6).trim()));
			}
			if (exceldatalist.get(p + 7).trim().equalsIgnoreCase("NA")) {
				// customer.setCellNumber1(0l);
			} else {
				customer.setCellNumber1(Long.parseLong(exceldatalist.get(p + 7).trim()));
			}

			if (exceldatalist.get(p + 8).trim().equalsIgnoreCase("na")) {
				// customer.setCellNumber2(0l);
			} else {
				customer.setCellNumber2(Long.parseLong(exceldatalist.get(p + 8).trim()));
			}
			if (exceldatalist.get(p + 9).trim().equalsIgnoreCase("na")) {
				// customer.setFaxNumber(0l);
			} else {
				customer.setFaxNumber(Long.parseLong(exceldatalist.get(p + 9).trim()));
			}
			if (exceldatalist.get(p + 10).trim().equalsIgnoreCase("na")) {
				customer.setBranch("");
			} else {
				customer.setBranch(exceldatalist.get(p + 10).trim());
			}
			if (exceldatalist.get(p + 11).trim().equalsIgnoreCase("na")) {
				customer.setEmployee("");
			} else {
				customer.setEmployee(exceldatalist.get(p + 11).trim());
			}
			if (exceldatalist.get(p + 12).trim().equalsIgnoreCase("na")) {
				customer.setGroup("");
			} else {
				customer.setGroup(exceldatalist.get(p + 12).trim());
			}
			if (exceldatalist.get(p + 13).trim().equalsIgnoreCase("na")) {
				customer.setCategory("");
			} else {
				customer.setCategory(exceldatalist.get(p + 13).trim());
			}
			if (exceldatalist.get(p + 14).trim().equalsIgnoreCase("na")) {
				customer.setType("");
			} else {
				customer.setType(exceldatalist.get(p + 14).trim());
			}
			if (exceldatalist.get(p + 15).trim().equalsIgnoreCase("na")) {
				customer.setCustomerPriority("");
			} else {
				customer.setCustomerPriority(exceldatalist.get(p + 15).trim());
			}
			if (exceldatalist.get(p + 16).trim().equalsIgnoreCase("na")) {
				customer.setCustomerLevel("");
			} else {
				customer.setCustomerLevel(exceldatalist.get(p + 16).trim());
			}
			if (exceldatalist.get(p + 17).trim().equalsIgnoreCase("na")) {
				customer.setRefrNumber1("");
			} else {
				customer.setRefrNumber1(exceldatalist.get(p + 17).trim());
			}
			if (exceldatalist.get(p + 18).trim().equalsIgnoreCase("na")) {
				customer.setRefrNumber2("");
			} else {
				customer.setRefrNumber2(exceldatalist.get(p + 18).trim());
			}
			if (exceldatalist.get(p + 19).trim().equalsIgnoreCase("na")) {
				customer.setWebsite("");
			} else {
				customer.setWebsite(exceldatalist.get(p + 19).trim());
			}
			if (exceldatalist.get(p + 20).trim().equalsIgnoreCase("NA")) {
				customer.setDescription("");
			} else {
				customer.setDescription(exceldatalist.get(p + 20).trim());
			}

			SocialInformation socinfo = new SocialInformation();

			if (exceldatalist.get(p + 21).trim().equalsIgnoreCase("NA")) {
				socinfo.setGooglePlusId("");
			} else {
				socinfo.setGooglePlusId(exceldatalist.get(p + 21).trim());
			}
			if (exceldatalist.get(p + 22).trim().equalsIgnoreCase("na")) {
				socinfo.setFaceBookId("");
			} else {
				socinfo.setFaceBookId(exceldatalist.get(p + 22).trim());
			}
			if (exceldatalist.get(p + 23).trim().equalsIgnoreCase("na")) {
				socinfo.setTwitterId("");
			} else {
				socinfo.setTwitterId(exceldatalist.get(p + 23).trim());
			}
			customer.setSocialInfo(socinfo);

			Address address = new Address();

			if (exceldatalist.get(p + 24).trim().equalsIgnoreCase("na")) {
				address.setAddrLine1("");
			} else {
				address.setAddrLine1(exceldatalist.get(p + 24).trim());
			}
			if (exceldatalist.get(p + 25).trim().equalsIgnoreCase("na")) {
				address.setAddrLine2("");
			} else {
				address.setAddrLine2(exceldatalist.get(p + 25).trim());
			}
			if (exceldatalist.get(p + 26).trim().equalsIgnoreCase("na")) {
				address.setLandmark("");
			} else {
				address.setLandmark(exceldatalist.get(p + 26).trim());
			}
			if (exceldatalist.get(p + 27).trim().equalsIgnoreCase("na")) {
				address.setCountry("");
			} else {
				address.setCountry(exceldatalist.get(p + 27).trim());
			}
			if (exceldatalist.get(p + 28).trim().equalsIgnoreCase("na")) {
				address.setState("");
			} else {
				address.setState(exceldatalist.get(p + 28).trim());
			}
			if (exceldatalist.get(p + 29).trim().equalsIgnoreCase("na")) {
				address.setCity("");
			} else {
				address.setCity(exceldatalist.get(p + 29).trim());
			}
			if (exceldatalist.get(p + 30).trim().equalsIgnoreCase("na")) {
				address.setLocality("");
			} else {
				address.setLocality(exceldatalist.get(p + 30).trim());
			}
			if (exceldatalist.get(p + 31).trim().equalsIgnoreCase("na")) {
				address.setPin(0l);
			} else {
				address.setPin(Long.parseLong(exceldatalist.get(p + 31).trim()));
			}
			customer.setAdress(address);

			Address address2 = new Address();
			if (exceldatalist.get(p + 32).trim().equalsIgnoreCase("na")) {
				address2.setAddrLine1("");
			} else {
				address2.setAddrLine1(exceldatalist.get(p + 32).trim());
			}
			if (exceldatalist.get(p + 33).trim().equalsIgnoreCase("na")) {
				address2.setAddrLine2("");
			} else {
				address2.setAddrLine2(exceldatalist.get(p + 33).trim());
			}
			if (exceldatalist.get(p + 34).trim().equalsIgnoreCase("na")) {
				address2.setLandmark("");
			} else {
				address2.setLandmark(exceldatalist.get(p + 34).trim());
			}
			if (exceldatalist.get(p + 35).trim().equalsIgnoreCase("na")) {
				address2.setCountry("");
			} else {
				address2.setCountry(exceldatalist.get(p + 35).trim());
			}
			if (exceldatalist.get(p + 36).trim().equalsIgnoreCase("na")) {
				address2.setState("");
			} else {
				address2.setState(exceldatalist.get(p + 36).trim());
			}
			if (exceldatalist.get(p + 37).trim().equalsIgnoreCase("na")) {
				address2.setCity("");
			} else {
				address2.setCity(exceldatalist.get(p + 37).trim());
			}
			if (exceldatalist.get(p + 38).trim().equalsIgnoreCase("")) {
				address2.setLocality("");
			} else {
				address2.setLocality(exceldatalist.get(p + 38).trim());
			}
			if (exceldatalist.get(p + 39).trim().equalsIgnoreCase("na")) {
				address2.setPin(0l);
			} else {
				address2.setPin(Long.parseLong(exceldatalist.get(p + 39).trim()));
			}
			customer.setSecondaryAdress(address2);

			if (!exceldatalist.get(p + 40).trim().equalsIgnoreCase("na")) {

				ArrayList<ArticleType> articletypelist = new ArrayList<ArticleType>();
				ArrayList<String> documentsNameList = new ArrayList<String>();
				documentsNameList.add("Quotation");
				documentsNameList.add("Contract");
				documentsNameList.add("Invoice Details");
				documentsNameList.add("SalesInvoice");

				for (int i = 0; i < documentsNameList.size(); i++) {
					ArticleType articletype = new ArticleType();
					articletype.setDocumentName(documentsNameList.get(i));
					articletype.setArticleTypeName("GSTIN");
					articletype.setArticleTypeValue(exceldatalist.get(p + 40).trim());
					articletype.setArticleDescription("");
					articletype.setArticlePrint("YES");
					articletypelist.add(articletype);
				}
				if (articletypelist.size() != 0)
					customer.setArticleTypeDetails(articletypelist);

			}

			customer.setCount(++count);

			customer.setCompanyId(companyId);

			customerlist.add(customer);

			if (p == exceldatalist.size() - 41) {
				Company compEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				if (compEntity != null) {
					Email cronEmail = new Email();
					ArrayList<String> toEmailList = new ArrayList<String>();
					toEmailList.add(compEntity.getPocEmail());
					ArrayList<String> tbl_header = new ArrayList<String>();
					tbl_header.add("Customer Name");

					ArrayList<String> tbl1 = new ArrayList<String>();
					for (int j = 0; j < customerNameList.size(); j++) {
						tbl1.add(customerNameList.get(j));
					}
					if (customerNameList.size() > 0) {
						try {
							cronEmail.uploadDataSendEmail(toEmailList, "Data Upload Duplicate Customer list",
									"Data Upload Duplicate Customer list", compEntity, tbl_header, tbl1, null, null,
									null, null);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}

			}

		}
		ofy().save().entities(customerlist).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		System.out.println("Data saved successfuly=======================================");
		logger.log(Level.SEVERE, "Data saved successfuly=======================================");

	}

	// vijay added method for customer validation

	private boolean validationforclientcompnay(List<Customer> listofcustomer, ArrayList<String> exceldatalist, int p) {
		boolean value = true;
		logger.log(Level.SEVERE, "hiiiiiiiiiiiiiiiiiii in validation client is company");
		for (int i = 0; i < listofcustomer.size(); i++) {

			logger.log(Level.SEVERE, "in validation client is company name from database ====="
					+ listofcustomer.get(i).getCompanyName().trim());
			logger.log(Level.SEVERE,
					"in validation client is company name from Excel =====" + exceldatalist.get(p + 1));
			if (!listofcustomer.get(i).getCompanyName().trim().equals("")
					&& listofcustomer.get(i).getCompanyName().trim() != null) {
				if (listofcustomer.get(i).getCompanyName().trim().equalsIgnoreCase(exceldatalist.get(p + 1).trim())) {
					customerNameList.add(exceldatalist.get(p + 1).trim());
					logger.log(Level.SEVERE, "Duplicate Client Name from Excel = " + exceldatalist.get(p + 1).trim());
					return false;
				}
			}
		}

		return value;
	}

	private boolean validationforcustomer(List<Customer> listofcustomer, ArrayList<String> exceldatalist, int p) {
		boolean value = true;

		for (int i = 0; i < listofcustomer.size(); i++) {
			if (!listofcustomer.get(i).getFullname().trim().equals("")
					&& listofcustomer.get(i).getFullname().trim() != null) {
				System.out.println("exceldatalist.get(p + 3).trim() ==" + exceldatalist.get(p + 4).trim());
				if (listofcustomer.get(i).getFullname().trim().equalsIgnoreCase(exceldatalist.get(p + 4).trim())
						&& listofcustomer.get(i).isCompany() == false) {
					customerNameList.add(exceldatalist.get(p + 4).trim());
					logger.log(Level.SEVERE, "Duplicate Customer Name from Excel = " + exceldatalist.get(p + 4).trim());

					return false;
				}
			}
		}
		return value;
	}

	private void localitySave(long companyId, ArrayList<String> exceldatalist) {

		if (exceldatalist.size() != 0) {
			NumberGeneration numGen = new NumberGeneration();
			numGen = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
					.filter("processName", "Locality").filter("status", true).first().now();

			long number = numGen.getNumber();
			int count = (int) number;
			int num = exceldatalist.size() / 4;
			int countNumber = count + num - 1;
			numGen.setNumber(countNumber);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(numGen);

			// here i am first number genration updated sucessfully

			ArrayList<Locality> localitylist = new ArrayList<Locality>();

			for (int i = 4; i < exceldatalist.size(); i += 4) {
				Locality locality = new Locality();
				if (exceldatalist.get(i).equalsIgnoreCase("na")) {
					locality.setCityName("");
				} else {
					locality.setCityName(exceldatalist.get(i));
				}
				if (exceldatalist.get(i + 1).equalsIgnoreCase("na")) {
					locality.setLocality("");
				} else {
					locality.setLocality(exceldatalist.get(i + 1));
				}
				if (exceldatalist.get(i + 2).equalsIgnoreCase("na")) {
					locality.setPinCode(0l);
				} else {
					locality.setPinCode(Long.parseLong(exceldatalist.get(i + 2).trim()));
				}

				if (exceldatalist.get(i + 3).equalsIgnoreCase("no")) {
					locality.setStatus(false);
				} else {
					locality.setStatus(true);
				}
				locality.setCount(++count);
				locality.setCompanyId(companyId);
				localitylist.add(locality);
			}

			ofy().save().entities(localitylist).now();
			logger.log(Level.SEVERE, "Locality List size ==" + localitylist.size());
			logger.log(Level.SEVERE, "Locality Saved Sucessfully");
		}
	}

	boolean singleOtFlag = false;
	boolean doubleOtFlag = false;

	@SuppressWarnings("unused")
	private void employeeAttendanceSave(long companyId, ArrayList<String> exceldatalist, String month, String year, Date startDate, Date endDate, int startLimit, int endLimit, int noOfDays, int lastUpdatedIndex) {
		/**
		 * @author Anil , Date : 09-05-2018 adding single Ot and double ot
		 *         column in attendance upload excel Process name : Attendance
		 *         Process Type : AddSingleAndDoubleOtInAttendanceUploadTemplate
		 *         raised by Sonu for Charlene Hospitality
		 */
		boolean otDotFlag = false;
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance",
				"AddSingleAndDoubleOtInAttendanceUploadTemplate", companyId)) {
			otDotFlag = true;
		}
		/**
		 * @author Anil
		 * @since 27-08-2020
		 */
		boolean siteLocation = false;
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance","EnableSiteLocation", companyId)) {
			siteLocation = true;
		}
		
		singleOtFlag = false;
		doubleOtFlag = false;

		if(startLimit==0||endLimit==0){
			endLimit = 36;
			startLimit = 38;
			if (otDotFlag) {
				endLimit = 38;
				startLimit = 40;
			}
			/**
			 * @author Anil
			 * @since 27-08-2020
			 */
			if(siteLocation){
				endLimit = 37;
				startLimit = 39;
				if (otDotFlag) {
					endLimit = 39;
					startLimit = 41;
				}
			}
		}

		logger.log(Level.SEVERE, "OT/DOT : " + otDotFlag + " SL/DL OT: " + singleOtFlag + "/" + doubleOtFlag + " STR/END LIM : " + startLimit+ "/" + endLimit + " SITE LOC "+siteLocation);

		/**
		 * @author Anil,Date :24-01-2019 requirement is instead of putting
		 *         working hours ,wanted to put label as 'P','HD' For Orion By
		 *         Sonu
		 */
		boolean labelAsInputFlag = false;
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "AttendanceLabelAsInput",
				companyId)) {
			labelAsInputFlag = true;
		}

		int mon = Integer.parseInt(month);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mon);
		c.set(Calendar.YEAR, Integer.parseInt(year));
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		int dayOfWeek1 = c.get(Calendar.DAY_OF_WEEK);
		
//		Date startDate = c.getTime();
		if(startDate==null){
			startDate = c.getTime();
		}
//		int noOfDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		int otCol=30;
		if(noOfDays==0){
			noOfDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		if(lastUpdatedIndex!=0){
			otCol=noOfDays-1;
		}
//		if(otDotFlag){
//			otCol=otCol+2;
//		}
		
		logger.log(Level.SEVERE,"otCol : " + otCol);
		
		HashMap<Integer, Integer> endCondMap=new HashMap<Integer, Integer>();
		
		/**
		 * Date : 16-07-2018 BY ANIL
		 */
		HashSet<String> hsProjName = new HashSet<String>();
		HashSet<Integer> hsEmpId = new HashSet<Integer>();
		int counter=1;
		loop: for (int p = startLimit; p < exceldatalist.size(); p += endLimit) {
			hsProjName.add(exceldatalist.get(p + 3).trim());
			hsEmpId.add(Integer.parseInt(exceldatalist.get(p).trim()));
			
			/**
			 * @author Anil
			 * @since 08-09-2020
			 */
			int indexNum=5;
			if(siteLocation){
				indexNum=6;
			}
			
			for (int i = 0; i < noOfDays; i++) {
				if (exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("END")) {
					if (otDotFlag) {
//						p = p - (32 - i);
						p = p - (otCol - i);
					} else {
//						p = p - (30 - i);
						p = p - (otCol - i);
					}
					int index1 = p + indexNum + 31;
					if(lastUpdatedIndex!=0){
						index1 = p + indexNum + noOfDays;
					}
					endCondMap.put(counter, index1);
					counter++;
					continue loop;
				}
			}
			counter++;
		}
		
		if(endCondMap.size()>0) {
			for (Map.Entry<Integer, Integer> entry : endCondMap.entrySet()){  
				logger.log(Level.SEVERE,"Key = " + entry.getKey() + ", Value = " + entry.getValue()); 
			}
		}
		ArrayList<String> projList = new ArrayList<String>(hsProjName);
		ArrayList<Integer> empList = new ArrayList<Integer>(hsEmpId);

		List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
		if (empList.size() != 0) {
			empInfoList = ofy().load().type(EmployeeInfo.class).filter("companyId", companyId)
					.filter("empCount IN", empList).list();
		}

		ServerAppUtility utility = new ServerAppUtility();
		ArrayList<Attendance> attendanceList = new ArrayList<Attendance>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat monthSdf = new SimpleDateFormat("MMM-yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		NumberGeneration ng = new NumberGeneration();
		ng = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
				.filter("processName", "Attendance").filter("status", true).first().now();

		long number = ng.getNumber();
		int count = (int) number;

		// List<Employee> listOfEmployees =
		// ofy().load().type(Employee.class).filter("companyId",
		// companyId).list();

		System.out.println("exceldatalist  ==" + exceldatalist.size());

		List<HrProject> projectList = ofy().load().type(HrProject.class).filter("companyId", companyId)
				.filter("projectName IN", projList).filter("status", true).list();

		List<LeaveBalance> empLeaveBalList = ofy().load().type(LeaveBalance.class).filter("companyId", companyId)
				.filter("empInfo.empCount IN", empList).list();

		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();

		logger.log(Level.SEVERE, "EMPLOYEE LEAVE LIST SIZE : " + empLeaveBalList.size());
		/**
		 * End
		 */
		counter=1;
		for (int p = startLimit; p < exceldatalist.size(); p += endLimit) {
			logger.log(Level.SEVERE, "date :" + c.getTime() + "Strt date :" + startDate);
			logger.log(Level.SEVERE, "p :" + p + "p end :" + (p+endLimit));
			c.setTime(startDate);
			singleOtFlag = false;
			doubleOtFlag = false;
			EmployeeInfo empInfo = null;
			com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal = null;
			Double calWorkingHours = 0.0;

			/**
			 * @author Anil , Date : 07-05-2019 If attendance is marked as OP
			 *         then we will not consider that attendance for save raised
			 *         by Nitin Sir,Sonu
			 */
			if (exceldatalist.get(p).trim().equalsIgnoreCase("na")|| exceldatalist.get(p).trim().equalsIgnoreCase("OP")) {
			} else {
				empInfo = getEmployeeInfo(Integer.parseInt(exceldatalist.get(p).trim()), empInfoList);
				cal = empInfo.getLeaveCalendar();
				calWorkingHours = empInfo.getLeaveCalendar().getWorkingHours();

				for (int i = 0; i < noOfDays; i++) {
					Attendance attendace = new Attendance();
					
					attendace.setStartDate(startDate);
					attendace.setEndDate(endDate);
					if(lastUpdatedIndex!=0&&endDate!=null){
						attendace.setMonth(monthSdf.format(endDate));
					}else{
						attendace.setMonth(monthSdf.format(startDate));
					}
					attendace.setStatus(ConcreteBusinessProcess.APPROVED);
					try {
						attendace.setApprovalDate(sdf.parse(sdf.format(new Date())));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					if (exceldatalist.get(p).trim().equalsIgnoreCase("na")) {
						attendace.setEmpId(0);
					} else {
						attendace.setEmpId(empInfo.getEmpCount());
						attendace.setEmployeeName(empInfo.getFullName());
						attendace.setEmpDesignation(empInfo.getDesignation());
					}

					if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("na")) {
						attendace.setEmpDesignation("");
					} else {
						if (!attendace.getEmpDesignation().equals(""))
							attendace.setEmpDesignation(exceldatalist.get(p + 2).trim());
					}
					String projectName = "";
					if (exceldatalist.get(p + 3).trim().equalsIgnoreCase("na")) {
						projectName = "";
					} else {
						projectName = exceldatalist.get(p + 3).trim();
					}
					attendace.setProjectName(projectName);

					/**
					 * Date : 14-07-2018 By ANIL
					 */
					HrProject project = getProjectName(projectList, projectName);
					LeaveBalance leaveBalance = getLeaveBalance(attendace.getEmpId(), empLeaveBalList);
					String overTime = "";
					/**
					 * End
					 */
					
					/**
					 * @author Anil 
					 * @since 27-08-2020
					 * enable site location
					 */
					int indexNum=5;
					if(siteLocation){
						indexNum=6;
						if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("na")) {
							attendace.setSiteLocation("");
						} else {
							attendace.setSiteLocation(exceldatalist.get(p + 4).trim());
						}
						if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
							attendace.setShift("");
						} else {
							attendace.setShift(exceldatalist.get(p + 5).trim());
						}
					}else{
						if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("na")) {
							attendace.setShift("");
						} else {
							attendace.setShift(exceldatalist.get(p + 4).trim());
						}
					}

					if (exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("END")) {
						if (otDotFlag) {
//							p = p - (32 - i);
							p = p - (otCol - i);
						} else {
//							p = p - (30 - i);
							p = p - (otCol - i);
						}
						logger.log(Level.SEVERE,"END VALUE OF P : " + p);
						counter++;
						break;
					}

					/**
					 * @author Anil , Date : 07-05-2019 If attendance is marked
					 *         as OP then we will not consider that attendance
					 *         for save raised by Nitin Sir,Sonu
					 */
					if (!exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("na")
							&& !exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("OP")) {
						try {
							attendace.setAttendanceDate(sdf.parse(sdf.format(c.getTime())));
						} catch (ParseException e) {
							e.printStackTrace();
						} // monthSdf
						attendace.setBranch(empInfo.getBranch());
						/**
						 * @author Anil ,Date : 24-01-2019 if label as input is
						 *         active then we have to put calendar working
						 *         hour for P
						 */
						if (labelAsInputFlag) {
							String workingHours = exceldatalist.get(p + indexNum + i).trim();
							if (exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("P")) {
								workingHours = empInfo.getLeaveCalendar().getWorkingHours() + "";
							} else if (exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("HD")) {
								workingHours = empInfo.getLeaveCalendar().getWorkingHours() / 2 + "";
							}
							attendace = getAttendance(attendace, workingHours, calWorkingHours, overTime, cal, project,
									leaveBalance, companyId,attendace.getShift(),empInfo,siteLocation);
						} else {
							attendace = getAttendance(attendace, exceldatalist.get(p + indexNum + i).trim(), calWorkingHours,
									overTime, cal, project, leaveBalance, companyId, attendace.getShift(),empInfo,siteLocation);
						}
						attendace.setCount(++count);
						attendace.setCompanyId(companyId);

						/**
						 * @author Anil , Date : 09-05-2019 updating attendance
						 *         if OT/DOT is active
						 */
						if (otDotFlag && (!singleOtFlag || !doubleOtFlag)) {

							try {
								int index = p + indexNum + 31;
								if(lastUpdatedIndex!=0){
									index = p + indexNum + noOfDays;
								}
								if(endCondMap.size()>0) {
									logger.log(Level.SEVERE, "COUNTER " + counter);
									if(endCondMap.containsKey(counter)) {
										index=endCondMap.get(counter);
									}
								}
								logger.log(Level.SEVERE, "Inside OT/DOT CONDITION " + index);
								attendace = updateOtInAttendance(attendace, index, exceldatalist, project,empInfo,siteLocation);
							} catch (Exception e) {
								logger.log(Level.SEVERE, "Inside OT/DOT exception");
							}
						}

						utility.ApproveLeave(attendace);
						attendanceList.add(attendace);
						c.add(Calendar.DATE, 1);

					} else {
						/**
						 * @author Anil ,Date : 24-01-2019 if label as input is
						 *         active then we have to put calendar working
						 *         hour for P
						 */
						if (!exceldatalist.get(p + indexNum + i).trim().equalsIgnoreCase("OP")) {
							/**
							 * Date : 25-08-2018 by ANIL
							 */
							try {
								attendace.setAttendanceDate(sdf.parse(sdf.format(c.getTime())));
							} catch (ParseException e) {
								e.printStackTrace();
							} // monthSdf
							attendace.setBranch(empInfo.getBranch());
							attendace = getAttendance(attendace, exceldatalist.get(p + indexNum + i).trim(), calWorkingHours,
									overTime, cal, project, leaveBalance, companyId, attendace.getShift(),empInfo,siteLocation);
							attendace.setCount(++count);
							attendace.setCompanyId(companyId);
							utility.ApproveLeave(attendace);
							attendanceList.add(attendace);

							/**
							 * End
							 */
						}
						c.add(Calendar.DATE, 1);
					}
				}
			}
			counter++;
		}

		ofy().save().entities(attendanceList).now();

		ng.setNumber(count);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(ng);

		/**
		 * Date : 15-12-2018 BY ANIL for clearing all loaded data from appengine
		 * cache memory
		 */
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		/**
		 * End
		 */

		System.out.println("Data saved successfuly=======================================");
		logger.log(Level.SEVERE, "Data saved successfuly=======================================");
	}
	
	/**
	 * @author Anil
	 * @since 31-08-2020
	 * Added employeeInfo and siteLocation for reliever OT calculation
	 * @param empInfo
	 * @param siteLocation
	 * @return
	 */
	private Attendance updateOtInAttendance(Attendance attendace, int index, ArrayList<String> exceldatalist,
			HrProject project,EmployeeInfo empInfo,boolean siteLocation) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"INSIDE OT/DOT UPDATE METHOD " + attendace.getAttendanceDate() + " LBL : " + attendace.getActionLabel()
						+ " HOUR : " + attendace.getTotalWorkedHours() + " ot col " + exceldatalist.get(index).trim());

		double singleOt = 0;
		double doubleOt = 0;

		ArrayList<Overtime> otList = new ArrayList<Overtime>();
		String designation=null;
		if (project != null) {
			/**
			 * @author Anil
			 * @since 31-08-2020
			 * Relievers OT
			 */
//			if(empInfo.isReliever()){
//				if(project.getRelieversOtList()!=null){
//					otList=project.getRelieversOtList();
//					designation=attendace.getEmpDesignation();
//				}
//			}else{
//				if(project.getOtList()!=null){
//					otList=project.getOtList();
//				}
//			}
//			if (project.getOtList() != null) {
//				otList = project.getOtList();
//			}
			
			/**
			 * @author Anil @since 13-04-2021
			 * Updated logic of Reliever ot validation 
			 * Raised by Rahul Tiwari for sasha
			 */
			if(empInfo.isReliever()){
				if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//					otList=project.getRelieversOtList();
					otList.addAll(project.getRelieversOtList());
					designation=attendace.getEmpDesignation();
				}
			}
			
//			if(project.getOtList()!=null&&project.getOtList().size()!=0){
////				otList=project.getOtList();
//				otList.addAll(project.getOtList());
//			}
			
			/**
			 * @author Vijay Date :- 08-09-2022
			 * Des :- used common method to read OT from project or from entity HrProjectOvertime`
			 */
			ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
			CommonServiceImpl commonservice = new CommonServiceImpl();
			overtimelist = commonservice.getHRProjectOvertimelist(project);
			/**
			 * ends here
			 */
			
			if(overtimelist!=null&&overtimelist.size()!=0){
				otList.addAll(overtimelist);
			}
		}

		// Overtime ot1=new Overtime();

		if (!singleOtFlag && !exceldatalist.get(index).equalsIgnoreCase("NA")) {
			try {
				singleOt = Double.parseDouble(exceldatalist.get(index).trim());
				if (singleOt != 0) {
					Overtime ot = getOvertimeName(otList, "OT", attendace.getEmpId(),designation);
					logger.log(Level.SEVERE, "SL OT : " + singleOtFlag + " HRS : " + singleOt);
					if (ot != null && attendace.isHoliday() == false && attendace.isWeeklyOff() == false
							&& attendace.isHalfDay() == false && attendace.isLeave() == false
							&& attendace.isOvertime() == false) {
						singleOtFlag = true;

						attendace.setOvertime(true);
						attendace.setOvertimeHours(singleOt);
						attendace.setProjectNameForOT(attendace.getProjectName());
						attendace.setOvertimeType(ot.getName());

						attendace.setTotalWorkedHours(attendace.getTotalWorkedHours() + singleOt);
						attendace.setWorkedHours(attendace.getWorkedHours() + singleOt);
						logger.log(Level.SEVERE, "SL OT : " + singleOtFlag + " HRS : " + singleOt);
						return attendace;
					}
				}
			} catch (Exception e) {
			}
		}

		if (!doubleOtFlag && !exceldatalist.get(index + 1).equalsIgnoreCase("NA")) {
			try {
				doubleOt = Double.parseDouble(exceldatalist.get(index + 1).trim());
				if (doubleOt != 0) {
					Overtime ot = getOvertimeName(otList, "DOT", attendace.getEmpId(),designation);
					if (ot != null && attendace.isHoliday() == false && attendace.isWeeklyOff() == false
							&& attendace.isHalfDay() == false && attendace.isLeave() == false
							&& attendace.isOvertime() == false) {
						doubleOtFlag = true;
						attendace.setOvertime(true);
						attendace.setOvertimeHours(doubleOt);
						attendace.setProjectNameForOT(attendace.getProjectName());
						attendace.setOvertimeType(ot.getName());

						attendace.setTotalWorkedHours(attendace.getTotalWorkedHours() + doubleOt);
						attendace.setWorkedHours(attendace.getWorkedHours() + doubleOt);
						logger.log(Level.SEVERE, " DL OT : " + doubleOtFlag + " HRS : " + doubleOt);
						return attendace;
					}
				}
			} catch (Exception e) {
			}
		}

		logger.log(Level.SEVERE,
				"SL OT : " + singleOtFlag + " HRS : " + singleOt + " DL OT : " + doubleOtFlag + " HRS : " + doubleOt);

		return attendace;
	}

	public EmployeeInfo getEmployeeInfo(int empId, List<EmployeeInfo> empInfoList) {
		// TODO Auto-generated method stub
		for (EmployeeInfo info : empInfoList) {
			if (info.getEmpCount() == empId) {
				return info;
			}
		}
		return null;
	}

	public LeaveBalance getLeaveBalance(int empId, List<LeaveBalance> empLeaveBalList) {
		// TODO Auto-generated method stub
		for (LeaveBalance leave : empLeaveBalList) {
			if (empId == leave.getEmpCount()) {
				return leave;
			}
		}
		return null;
	}

	private HrProject getProjectName(List<HrProject> projectList, String projectName) {
		for (HrProject proj : projectList) {
			if (proj.getProjectName().equals(projectName)) {
				return proj;
			}
		}
		return null;
	}
	/**
	 * @author Anil
	 * @since 31-08-2020
	 * Added employeeInfo and siteLocation for reliever OT calculation
	 * @param empInfo
	 * @param siteLocation
	 * @return
	 */
	private Attendance getAttendance(Attendance attendace, String actualWorkingHrs, double calWorkingHrs,
			String overTime, com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar cal,
			HrProject project, LeaveBalance leaveBalance, long companyId, String shiftName,EmployeeInfo empInfo,boolean siteLocation) {
		DataMigrationTaskQueueImpl queueImpl = new DataMigrationTaskQueueImpl();
		
		
		/**
		 * @author Vijay Date :- 08-09-2022
		 * Des :- used common method to read OT from project or from entity HrProjectOvertime`
		 */
		ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
		CommonServiceImpl commonservice = new CommonServiceImpl();
		overtimelist = commonservice.getHRProjectOvertimelist(project);
		/**
		 * ends here
		 */
		
		if (actualWorkingHrs.equalsIgnoreCase("na")) {
			attendace.setTotalWorkedHours(0.0);
			attendace.setWorkedHours(0.0);
			attendace.setLeaveType("NA");
			attendace.setActionLabel("NA");
		} else {
			try {
				Double ot = 0.0;
				Double hf = 0.0;

				/**
				 * Date : 27-08-2018 BY ANIL
				 */
				WeekleyOff wo = cal.getWeeklyoff();
				boolean isWo = queueImpl.isWeeklyOff(wo, attendace.getAttendanceDate());
				boolean isHoliday = cal.isHolidayDate(attendace.getAttendanceDate());
				/**
				 * @author Anil
				 * @since 13-07-2020
				 */
				String holidayType=cal.getHolidayType(attendace.getAttendanceDate());
				String holidayLabel="H";
				if(holidayType!=null){
					logger.log(Level.SEVERE , attendace.getAttendanceDate()+"holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
					if(holidayType.equalsIgnoreCase("National Holiday")){
						holidayLabel="NH";
					}else if(holidayType.equalsIgnoreCase("Public Holiday")){
						holidayLabel="PH";
					}
				}
				//logger.log(Level.SEVERE , "actualWorkingHrs ="+actualWorkingHrs+" calWorkingHrs="+calWorkingHrs);
				if (Double.parseDouble(actualWorkingHrs) >= calWorkingHrs) {
					ot = Double.parseDouble(actualWorkingHrs) - calWorkingHrs;
					// projectHoursList.add(hours);
					// for(String s : set){
					attendace.setPresent(true);
					attendace.setTotalWorkedHours(Double.parseDouble(actualWorkingHrs));
					attendace.setWorkedHours(calWorkingHrs);
					attendace.setActionLabel(AppConstants.P);

					if ((isWo || isHoliday)&&project != null&&!project.isRotationalWeeklyOff()) {//Ashwini Patil Date:18-07-2023
						/**
						 * @author Anil, Date : 20-06-2019 Earlier if employee
						 *         works more than working hours on weekly
						 *         off/holiday then he was paid only for working
						 *         hours t thats why adding working hours in it
						 */
						ot = ot + calWorkingHrs;
					}
					/**
					 * End
					 */
				//	logger.log(Level.SEVERE , "OT="+ot);
					if (ot != 0) {
						/**
						 * Date : 16-07-2018
						 */
						// WeekleyOff wo = cal.getWeeklyoff();
						// boolean isWo =
						// queueImpl.isWeeklyOff(wo,attendace.getAttendanceDate());
						// boolean isHoliday =
						// cal.isHolidayDate(attendace.getAttendanceDate());
						Overtime ot1 = new Overtime();
						if (project != null) {
							ArrayList<Overtime> otList = new ArrayList<Overtime>();
							/**
							 * @author Anil
							 * @since 31-08-2020
							 * Relievers OT
							 */
							String designation=null;
//							if(empInfo.isReliever()){
//								if(project.getRelieversOtList()!=null){
//									otList=project.getRelieversOtList();
//									designation=attendace.getEmpDesignation();
//								}
//							}else{
//								if(project.getOtList()!=null){
//									otList=project.getOtList();
//								}
//							}
//							if (project.getOtList() != null) {
//								otList = project.getOtList();
//							}
							
							/**
							 * @author Anil @since 13-04-2021
							 * Updated logic of Reliever ot validation 
							 * Raised by Rahul Tiwari for sasha
							 */
							if(empInfo.isReliever()){
								if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//									otList=project.getRelieversOtList();
									otList.addAll(project.getRelieversOtList());
									designation=attendace.getEmpDesignation();
								}
							}
							
							
//							if(project.getOtList()!=null&&project.getOtList().size()!=0){
////								otList=project.getOtList();
//								otList.addAll(project.getOtList());
//							}
							
							if(overtimelist!=null&&overtimelist.size()!=0){
								otList.addAll(overtimelist);
							}
							
							
							if (otList.size() != 0) {
								if (isWo) {
									ot1 = getOvertimeName(otList, "WO", attendace.getEmpId(),designation);
								} else if (isHoliday) {
									ot1 = getOvertimeName(otList, holidayLabel, attendace.getEmpId(),designation);
									logger.log(Level.SEVERE , attendace.getAttendanceDate()+"holidayType: "+ holidayType+" holidayLabel "+holidayLabel+" ot1"+ot1);
								} else {
									ot1 = getOvertimeName(otList, "N", attendace.getEmpId(),designation);
								}
								
								logger.log(Level.SEVERE ,"Reliever "+empInfo.isReliever()+" desg "+designation);
								logger.log(Level.SEVERE ,"W>CW "+ot1);
							}
						}

						attendace.setOvertime(true);
						attendace.setOvertimeHours(ot);
						attendace.setProjectNameForOT(attendace.getProjectName());
						attendace.setOvertimeType(ot1.getName());

					} else {

						Overtime ot1 = new Overtime();
						if (isWo&&project != null&&!project.isRotationalWeeklyOff()) {
							ArrayList<Overtime> otList = new ArrayList<Overtime>();
							String designation=null;
							if (project != null) {
								
								/**
								 * @author Anil
								 * @since 31-08-2020
								 * Relievers OT
								 */
//								if(siteLocation&&empInfo.isReliever()){
//									if(project.getRelieversOtList()!=null){
//										otList=project.getRelieversOtList();
//										designation=attendace.getEmpDesignation();
//									}
//								}else{
//									if(project.getOtList()!=null){
//										otList=project.getOtList();
//									}
//								}
//								if (project.getOtList() != null) {
//									otList = project.getOtList();
//								}
								
								/**
								 * @author Anil @since 13-04-2021
								 * Updated logic of Reliever ot validation 
								 * Raised by Rahul Tiwari for sasha
								 */
								if(empInfo.isReliever()){
									if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//										otList=project.getRelieversOtList();
										otList.addAll(project.getRelieversOtList());
										designation=attendace.getEmpDesignation();
									}
								}
								
//								if(project.getOtList()!=null&&project.getOtList().size()!=0){
////									otList=project.getOtList();
//									otList.addAll(project.getOtList());
//								}
								
								if(overtimelist!=null&&overtimelist.size()!=0){
									otList.addAll(overtimelist);
								}
							}
							ot1 = getOvertimeName(otList, "WO", attendace.getEmpId(),designation);
							attendace.setOvertime(true);
							attendace.setOvertimeHours(ot);
							attendace.setProjectNameForOT(attendace.getProjectName());
							attendace.setOvertimeType(ot1.getName());
							
							logger.log(Level.SEVERE ,"Reliever "+empInfo.isReliever()+" desg "+designation);
							logger.log(Level.SEVERE ,"W0 "+ot1);
						} else if (isHoliday&&project != null&&!project.isRotationalWeeklyOff()) {
							ArrayList<Overtime> otList = new ArrayList<Overtime>();
							String designation=null;
							if (project != null) {
								/**
								 * @author Anil
								 * @since 31-08-2020
								 * Relievers OT
								 */
//								if(siteLocation&&empInfo.isReliever()){
//									if(project.getRelieversOtList()!=null){
//										otList=project.getRelieversOtList();
//										designation=attendace.getEmpDesignation();
//									}
//								}else{
//									if(project.getOtList()!=null){
//										otList=project.getOtList();
//									}
//								}
//								if (project.getOtList() != null) {
//									otList = project.getOtList();
//								}
								
								/**
								 * @author Anil @since 13-04-2021
								 * Updated logic of Reliever ot validation 
								 * Raised by Rahul Tiwari for sasha
								 */
								if(empInfo.isReliever()){
									if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//										otList=project.getRelieversOtList();
										otList.addAll(project.getRelieversOtList());
										designation=attendace.getEmpDesignation();
									}
								}
								
//								if(project.getOtList()!=null&&project.getOtList().size()!=0){
////									otList=project.getOtList();
//									otList.addAll(project.getOtList());
//								}
								if(overtimelist!=null&&overtimelist.size()!=0){
									otList.addAll(overtimelist);
								}
							}
							ot1 = getOvertimeName(otList, holidayLabel, attendace.getEmpId(),designation);
							attendace.setOvertime(true);
							attendace.setOvertimeHours(ot);
							attendace.setProjectNameForOT(attendace.getProjectName());
							attendace.setOvertimeType(ot1.getName());
							
							logger.log(Level.SEVERE ,"Reliever "+empInfo.isReliever()+" desg "+designation);
							logger.log(Level.SEVERE ,"H"+ot1+" "+holidayLabel);
						}
					}

				} else if (Double.parseDouble(actualWorkingHrs) < calWorkingHrs) {

					/**
					 * Date 07-01-2019 By Vijay Des :- updating attendance if
					 * didnt complete working hours then half day and half also
					 * not completed then marking as leave with process config
					 */
					if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance",
							"EnableStandardAttendance", companyId)) {
						Shift shift = ofy().load().type(Shift.class).filter("companyId", companyId)
								.filter("shiftName", shiftName).first().now();
						logger.log(Level.SEVERE, "shift ==" + shift);
						double workinghours = calWorkingHrs;
						if (shift != null) {
							calWorkingHrs = calWorkingHrs
									- ((shift.getIntimeGracePeriod() + shift.getOuttimeGracePeriod()) / 60);
						}
						if (Double.parseDouble(actualWorkingHrs) >= calWorkingHrs) {
							attendace.setPresent(true);
							attendace.setTotalWorkedHours(workinghours);
							attendace.setWorkedHours(workinghours);
							attendace.setActionLabel(AppConstants.P);
						} else {
							double halfDayHours = calWorkingHrs / 2;
							System.out.println("hours ==" + calWorkingHrs);
							System.out.println("halfDayHours ==" + halfDayHours);
							if (Double.parseDouble(actualWorkingHrs) >= halfDayHours) {
								attendace.setTotalWorkedHours(halfDayHours);
								attendace.setWorkedHours(calWorkingHrs);
								double hoursConvertedInday = halfDayHours / cal.getWorkingHours();
								AllocatedLeaves leaves = null;
								leaves = getLeaveDetails("Paid Leave", hoursConvertedInday, leaveBalance);
								// attendace.setHalfDay(true);
								if (leaves != null) {
									attendace.setLeaveType(leaves.getName());
								} else {
									leaves = getLeaveDetails("Unpaid Leave", hoursConvertedInday, leaveBalance);
									attendace.setLeaveType(leaves.getName());
								}
								attendace.setLeaveHours(workinghours / 2);
								attendace.setLeave(true);

								attendace.setActionLabel(AppConstants.HF);
								attendace.setPresent(true);

							} else {
								AllocatedLeaves leaves = null;
								leaves = getLeaveDetails("Paid Leave", 1.0, leaveBalance);
								if (leaves != null) {
									attendace.setLeaveType(leaves.getName());
								} else {
									leaves = getLeaveDetails("Unpaid Leave", 1.0, leaveBalance);
									attendace.setLeaveType(leaves.getName());
								}

								attendace.setTotalWorkedHours(0.0);
								attendace.setWorkedHours(0.0);
								attendace.setLeaveHours(workinghours);
								attendace.setLeave(true);
								attendace.setActionLabel(AppConstants.L);
							}
						}
					} else {
						/**
						 * @author Anil , Date : 20-06-2019 if employee was
						 *         working on weekly off or holiday but working
						 *         hour is less than calendar working hour then
						 *         system will consider it as half day
						 */
						if (isWo || isHoliday) {
							
							attendace.setPresent(true);
							attendace.setTotalWorkedHours(Double.parseDouble(actualWorkingHrs));
							attendace.setWorkedHours(calWorkingHrs);
							attendace.setActionLabel(AppConstants.P);

							Overtime ot1 = new Overtime();
							if (project != null) {
								ArrayList<Overtime> otList = new ArrayList<Overtime>();
								/**
								 * @author Anil
								 * @since 31-08-2020
								 * Relievers OT
								 */
								String designation=null;
//								if(empInfo.isReliever()){
//									if(project.getRelieversOtList()!=null){
//										otList=project.getRelieversOtList();
//										designation=attendace.getEmpDesignation();
//									}
//								}else{
//									if(project.getOtList()!=null){
//										otList=project.getOtList();
//									}
//								}
//								if (project.getOtList() != null) {
//									otList = project.getOtList();
//								}
								
								/**
								 * @author Anil @since 13-04-2021
								 * Updated logic of Reliever ot validation 
								 * Raised by Rahul Tiwari for sasha
								 */
								if(empInfo.isReliever()){
									if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//										otList=project.getRelieversOtList();
										otList.addAll(project.getRelieversOtList());
										designation=attendace.getEmpDesignation();
									}
								}
								
//								if(project.getOtList()!=null&&project.getOtList().size()!=0){
////									otList=project.getOtList();
//									otList.addAll(project.getOtList());
//								}
								
								if(overtimelist!=null&&overtimelist.size()!=0){
									otList.addAll(overtimelist);
								}
								
								if (otList.size() != 0) {
									if (isWo) {
										ot1 = getOvertimeName(otList, "WO", attendace.getEmpId(),designation);
									} else if (isHoliday) {
										ot1 = getOvertimeName(otList, holidayLabel, attendace.getEmpId(),designation);
									}
								}
								
								logger.log(Level.SEVERE ,"Reliever "+empInfo.isReliever()+" desg "+designation);
								logger.log(Level.SEVERE ,"W<CW"+ot1);
							}

							attendace.setOvertime(true);
							attendace.setOvertimeHours(Double.parseDouble(actualWorkingHrs));
							attendace.setProjectNameForOT(attendace.getProjectName());
							attendace.setOvertimeType(ot1.getName());
						} else {
							hf = calWorkingHrs - Double.parseDouble(actualWorkingHrs);
							attendace.setTotalWorkedHours(Double.parseDouble(actualWorkingHrs));
							attendace.setWorkedHours(Double.parseDouble(actualWorkingHrs));

							double hoursConvertedInday = hf / cal.getWorkingHours();

							AllocatedLeaves leaves = null;
							leaves = getLeaveDetails("Paid Leave", hoursConvertedInday, leaveBalance);
							// attendace.setHalfDay(true);
							if (leaves != null) {
								attendace.setLeaveType(leaves.getName());
							} else {
								leaves = getLeaveDetails("Unpaid Leave", hoursConvertedInday, leaveBalance);
								attendace.setLeaveType(leaves.getName());
							}
							attendace.setLeaveHours(hf);
							attendace.setLeave(true);

							attendace.setActionLabel(AppConstants.HF);
							attendace.setPresent(true);
						}
					}

					}
					// }
				} catch (NumberFormatException e) {
//					logger.log(Level.SEVERE,"");
//					for(AllocatedLeaves bal:leaveBalance.getAllocatedLeaves()){
//						logger.log(Level.SEVERE,"LNAME "+bal.getName()+" LSN "+bal.getShortName()+" BAL "+bal.getBalance());
//					}
//					logger.log(Level.SEVERE,"");
					if (actualWorkingHrs.trim().equalsIgnoreCase("WO")||actualWorkingHrs.trim().equalsIgnoreCase("W/O")) {
						
						/**
						 * @author Anil , Date : 20-08-2019
						 * We will not maintain WO as leave, will just mark attendance as weekly off
						 * Suggested by Nitin Sir
						 */
						
//						logger.log(Level.SEVERE,"WO --- EMP ID "+attendace.getEmpId()+" "+attendace.getEmployeeName());
//						WeekleyOff wo = cal.getWeeklyoff();
//						boolean isWo = queueImpl.isWeeklyOff(wo,attendace.getAttendanceDate());
//						boolean isWoDefinedInCal=queueImpl.isWeeklyOffDefined(wo);
//						if(!isWo){
//							AllocatedLeaves leaves=null;
//							leaves=getLeaveDetails("WO",1.0,leaveBalance);
//							if(leaves!=null){
////								logger.log(Level.SEVERE,"WO INSIDE  "+leaves.getName()+" ");
//								attendace.setLeaveType(leaves.getName());
//							}else{
//								leaves=getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
////								logger.log(Level.SEVERE,"WO ELSE  "+leaves.getName());
//								attendace.setLeaveType(leaves.getName());
//							}
//						}else{
//							logger.log(Level.SEVERE,"WO FOUND  "+wo);
//						}
						/**
						 * End
						 */
						attendace.setTotalWorkedHours(0.0);
						attendace.setWorkedHours(0.0);
						attendace.setLeaveHours(calWorkingHrs);
//						attendace.setLeave(true);
//						attendace.setWeeklyOff(isWo);
						attendace.setLeave(false);
						attendace.setWeeklyOff(true);
						attendace.setActionLabel(AppConstants.WO);
						
					} else if(actualWorkingHrs.equalsIgnoreCase("H"))  {
						
						String holidayName="";
						for(Holiday holiday:cal.getHoliday()){
							if(holiday.getHolidaydate().equals(attendace.getAttendanceDate())){
								holidayName=holiday.getHolidayname();
								break;
							}
						}
						String holidayType=cal.getHolidayType(attendace.getAttendanceDate());
						String holidayLabel="H";
						if(holidayType!=null){
							logger.log(Level.SEVERE , attendace.getAttendanceDate()+"holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
							if(holidayType.equalsIgnoreCase("National Holiday")){
								holidayLabel="NH";
							}else if(holidayType.equalsIgnoreCase("Public Holiday")){
								holidayLabel="PH";
							}
						}
						
						logger.log(Level.SEVERE , attendace.getAttendanceDate()+"holidayType: "+ holidayType+" holidayLabel "+holidayLabel);
						
						attendace.setTotalWorkedHours(0.0);
						attendace.setWorkedHours(0.0);
						attendace.setLeaveHours(calWorkingHrs);
						attendace.setLeave(true);
						attendace.setLeaveType(holidayName);
						attendace.setHoliday(true);
							attendace.setActionLabel(AppConstants.H);
					}else if(actualWorkingHrs.equalsIgnoreCase("L")){
						
						AllocatedLeaves leaves=null;
						leaves=getLeaveDetails("Paid Leave",1.0,leaveBalance);
						if(leaves!=null){
							attendace.setLeaveType(leaves.getName());
						}else{
							leaves=getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
							attendace.setLeaveType(leaves.getName());
						}
						
						attendace.setTotalWorkedHours(0.0);
						attendace.setWorkedHours(0.0);
						attendace.setLeaveHours(calWorkingHrs);
						attendace.setLeave(true);
						attendace.setActionLabel(AppConstants.L);
						
					}else if(actualWorkingHrs.equalsIgnoreCase("A")){
						logger.log(Level.SEVERE,"ABSENT --- EMP ID "+attendace.getEmpId()+" "+attendace.getEmployeeName());
						AllocatedLeaves leaves=null;
						leaves=getLeaveDetails("Unpaid Leave",1.0,leaveBalance);
						attendace.setLeaveType(leaves.getName());
						
						attendace.setTotalWorkedHours(0.0);
						attendace.setWorkedHours(0.0);
						attendace.setLeaveHours(calWorkingHrs);
						attendace.setLeave(true);
						attendace.setActionLabel(AppConstants.A);
					}
					/**
					 * @author Anil
					 * @since 10-07-2020
					 */
					else if(actualWorkingHrs.contains("PH")&&queueImpl.validPH_OT(actualWorkingHrs)){
						attendace.setTotalWorkedHours(0.0);
						attendace.setWorkedHours(0.0);
						attendace.setLeaveHours(calWorkingHrs);
						attendace.setHoliday(true);
						attendace.setActionLabel("PH");
						attendace.setHolidayLabel("PH");
						
						double ot=queueImpl.getTotalPhOtHours(actualWorkingHrs);
						if (ot != 0) {
							Overtime ot1 = new Overtime();
							if (project != null) {
								ArrayList<Overtime> otList = new ArrayList<Overtime>();
								/**
								 * @author Anil
								 * @since 31-08-2020
								 * Relievers OT
								 */
								String designation=null;
//								if(empInfo.isReliever()){
//									if(project.getRelieversOtList()!=null){
//										otList=project.getRelieversOtList();
//										designation=attendace.getEmpDesignation();
//									}
//								}else{
//									if(project.getOtList()!=null){
//										otList=project.getOtList();
//									}
//								}
//								if (project.getOtList() != null) {
//									otList = project.getOtList();
//								}
								
								/**
								 * @author Anil @since 13-04-2021
								 * Updated logic of Reliever ot validation 
								 * Raised by Rahul Tiwari for sasha
								 */
								if(empInfo.isReliever()){
									if(project.getRelieversOtList()!=null&&project.getRelieversOtList().size()!=0){
//										otList=project.getRelieversOtList();
										otList.addAll(project.getRelieversOtList());
										designation=attendace.getEmpDesignation();
									}
								}
								
//								if(project.getOtList()!=null&&project.getOtList().size()!=0){
////									otList=project.getOtList();
//									otList.addAll(project.getOtList());
//								}
								
								if(overtimelist!=null&&overtimelist.size()!=0){
									otList.addAll(overtimelist);
								}
								if (otList.size() != 0) {
									ot1 = getOvertimeName(otList, "PH", attendace.getEmpId(),designation);
								}
								
								logger.log(Level.SEVERE ,"Reliever "+empInfo.isReliever()+" desg "+designation);
								logger.log(Level.SEVERE ,"PH"+ot1);
							}
							
							if(ot1!=null){
								/**
								 * @author Anil
								 * @since 13-07-2020
								 */
								attendace.setPresent(true);
								attendace.setWorkedHours(calWorkingHrs);
								attendace.setActionLabel(AppConstants.P);
								attendace.setHoliday(false);
								
								attendace.setOvertime(true);
								attendace.setOvertimeHours(ot);
								attendace.setProjectNameForOT(attendace.getProjectName());
								attendace.setOvertimeType(ot1.getName());
								attendace.setTotalWorkedHours(ot);
							}

						} 
					}
					else{
						/** date 05-07-2019 added by komal for woven fabric(short name wise leave deduction)**/ 
						logger.log(Level.SEVERE,"leave type --- EMP ID "+attendace.getEmpId()+" "+attendace.getEmployeeName()+" "+actualWorkingHrs);
						AllocatedLeaves leaves=null;
						leaves=getLeaveDetails(actualWorkingHrs,1.0,leaveBalance);
						attendace.setLeaveType(leaves.getName());
						
						attendace.setTotalWorkedHours(0.0);
						attendace.setWorkedHours(0.0);
						attendace.setLeaveHours(calWorkingHrs);
						attendace.setLeave(true);
						attendace.setActionLabel(actualWorkingHrs);
					}
					
					

			}
		}

		return attendace;
	}

	public AllocatedLeaves getLeaveDetails(String leaveType, Double leaveHrs, LeaveBalance leaveBalance) {
		// TODO Auto-generated method stub
		for (AllocatedLeaves leave : leaveBalance.getLeaveGroup().getAllocatedLeaves()) {

			logger.log(Level.SEVERE, "Leave TYPE " + leaveType + "  LEAVE NAME " + leave.getName() + " SHORT NAME "
					+ leave.getShortName() + " BAL " + leave.getBalance() + " APPLIED LEAVE " + leaveHrs);

//			if ((leaveType.equalsIgnoreCase("Paid Leave") && leave.getPaidLeave() == true)
//					&& ((!leave.getShortName().trim().equalsIgnoreCase("WO"))
//							&& (!leave.getShortName().trim().equalsIgnoreCase("W/O")))) {
			if (leaveType.equalsIgnoreCase("Paid Leave") && leave.getPaidLeave() == true&&leave.getShortName().contains("PL") ) {
				if (leave.getBalance() >= leaveHrs) {
					double updateBalance = leave.getBalance() - leaveHrs;
					leave.setBalance(updateBalance);
					logger.log(Level.SEVERE, "BALANCE " + updateBalance);
					return leave;
				}
			}
			/**
			 * date 05-07-2019 added by komal for woven fabric(short name wise
			 * leave deduction)
			 **/
			if ((leaveType.equalsIgnoreCase(leave.getShortName()) && leave.getPaidLeave() == true)
					&& ((!leave.getShortName().trim().equalsIgnoreCase("WO"))
							&& (!leave.getShortName().trim().equalsIgnoreCase("W/O")))) {
				if (leave.getBalance() >= leaveHrs) {
					double updateBalance = leave.getBalance() - leaveHrs;
					leave.setBalance(updateBalance);
					logger.log(Level.SEVERE, "BALANCE " + updateBalance);
					return leave;
				}
			}

			if (leaveType.equalsIgnoreCase("Unpaid Leave") && leave.getPaidLeave() == false) {
				logger.log(Level.SEVERE, "UNPAID....");
				// if(leave.getBalance()>leaveHrs){
				// double updateBalance=leave.getBalance()-leaveHrs;
				// leave.setBalance(updateBalance);
				return leave;
				// }
			}

			if (leaveType.equalsIgnoreCase("WO")
					&& (leave.getShortName().equalsIgnoreCase("WO") || leave.getShortName().equalsIgnoreCase("W/O"))) {
				// logger.log(Level.SEVERE,"WOOO "+" BAL " +leave.getBalance()+"
				// CURR "+leaveHrs);
				if (leave.getBalance() >= leaveHrs) {
					double updateBalance = leave.getBalance() - leaveHrs;
					leave.setBalance(updateBalance);
					return leave;
				}
			}
		}
		return null;
	}

	/**
	 * @author Anil
	 * @since 07-09-2020
	 * Added designation in case of reliever employee to get OT details
	 * @param designation
	 * @return
	 */
	private Overtime getOvertimeName(ArrayList<Overtime> otList, String otType, int empId,String designation) {
		for (Overtime ot : otList) {
			
			if (otType.equalsIgnoreCase("WO") && ot.isWeeklyOff() == true && ot.getEmpId() == empId) {
				return ot;
			}
			if (otType.equalsIgnoreCase("N") && ot.isNormalDays() == true && ot.getEmpId() == empId) {
				return ot;
			}
			if (otType.equalsIgnoreCase("H") && ot.getHolidayType() != null && !ot.getHolidayType().equals("")
					&& ot.getEmpId() == empId) {
				return ot;
			}
			
			if((otType.equalsIgnoreCase("PH")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
				return ot;
			}
			
			if((otType.equalsIgnoreCase("NH")&&ot.getEmpId()==empId)&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
				return ot;
			}

			/**
			 * @author Anil, Date :09-05-2019
			 */
			if ((otType.equalsIgnoreCase("OT") && ot.getEmpId() == empId)
					&& (ot.getShortName().equalsIgnoreCase("OT"))) {
				return ot;
			}
			if ((otType.equalsIgnoreCase("DOT") && ot.getEmpId() == empId)
					&& (ot.getShortName().equalsIgnoreCase("DOT"))) {
				return ot;
			}
		}
		
		
		for (Overtime ot : otList) {
			if(ot.getEmpId()==0&&designation!=null){
				if(otType.equalsIgnoreCase("WO")&&ot.isWeeklyOff()==true&&ot.getEmpDesignation().equals(designation)){
					return ot;
				}
				if(otType.equalsIgnoreCase("N")&&ot.isNormalDays()==true&&ot.getEmpDesignation().equals(designation)){
					return ot;
				}
				if((otType.equalsIgnoreCase("H")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))){
					return ot;
				}
				
				if((otType.equalsIgnoreCase("PH")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("Public Holiday")){
					return ot;
				}
				
				if((otType.equalsIgnoreCase("NH")&&ot.getEmpDesignation().equals(designation))&&(ot.getHolidayType()!=null&&!ot.getHolidayType().equals(""))&&ot.getHolidayType().equalsIgnoreCase("National Holiday")){
					return ot;
				}
				
				if((otType.equalsIgnoreCase("OT")&&ot.getEmpDesignation().equals(designation))&&(ot.getShortName().equalsIgnoreCase("OT"))){
					return ot;
				}
				if((otType.equalsIgnoreCase("DOT")&&ot.getEmpDesignation().equals(designation))&&(ot.getShortName().equalsIgnoreCase("DOT"))){
					return ot;
				}
			}
		}
		
		
		return null;
	}

	/**
	 * 
	 * @param companyId
	 * @param exceldatalist
	 * Description : 
	 * 1.This method is used to create employee with minimum details , user ,
	 * storage bin same name as username , entry in Technical warehouse details entity(for mapping
	 * technician with parent warehouse and technician bin , make entry of technician bin in all product's 
	 * product inventory view details.
	 * 2.In user role need "Operator" role.
	 * 3.Number generation  of "TechnicianWareHouseDetailsList" entity
	 * 4.for new employee creation put 0 in employee id otherwise enter employee count while uploading.
	 * 5. To use technician wise stock maintainance , entry in TechnicianWareHouseDetailsList is must
	 * so that when tou select techncinan then parent warehouse details an technician warehouse details will be mapped automatically
	 */
	private void saveTechnicianWarehouseDetails(long companyId, ArrayList<String> exceldatalist) {

		if (exceldatalist.size() != 0) {
			// NumberGeneration numGen = new NumberGeneration();
			// numGen =
			// ofy().load().type(NumberGeneration.class).filter("companyId",
			// companyId).filter("processName", "Locality").filter("status",
			// true).first().now();
			//
			// long number = numGen.getNumber();
			// int count = (int) number;
			// int num = exceldatalist.size()/4;
			// int countNumber = count + num-1;
			// numGen.setNumber(countNumber);
			// GenricServiceImpl impl = new GenricServiceImpl();
			// impl.save(numGen);

			// here i am first number genration updated sucessfully

			ArrayList<TechnicianWarehouseBean> technicianWarehouseBeanList = new ArrayList<TechnicianWarehouseBean>();
			List<String> employeeNameList = new ArrayList<String>();
			List<String> binList = new ArrayList<String>();
			List<Integer> employeeIdList = new ArrayList<Integer>();

			for (int i = 11; i < exceldatalist.size(); i += 11) {
				TechnicianWarehouseBean technicianWarehouseBean = new TechnicianWarehouseBean();
				if (exceldatalist.get(i).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setEmployeeId(0);
				} else {
					technicianWarehouseBean.setEmployeeId(Integer.parseInt(exceldatalist.get(i).trim()));
					employeeIdList.add(Integer.parseInt(exceldatalist.get(i).trim()));

				}

				if (exceldatalist.get(i + 1).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setEmployeeName("");
				} else {
					technicianWarehouseBean.setEmployeeName(exceldatalist.get(i + 1).trim());
					employeeNameList.add(exceldatalist.get(i + 1).trim());

				}
				if (exceldatalist.get(i + 2).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setUserName(exceldatalist.get(i + 1).trim());
				} else {
					technicianWarehouseBean.setUserName(exceldatalist.get(i + 2).trim());
					binList.add(exceldatalist.get(i + 2).trim());

				}
				if (exceldatalist.get(i + 3).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setPassword("pass123");
				} else {
					technicianWarehouseBean.setPassword(exceldatalist.get(i + 3).trim());

				}

				if (exceldatalist.get(i + 4).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setSalary(0);
				} else {
					double salary = 0;
					try {
						salary = Double.parseDouble(exceldatalist.get(i + 4).trim());
					} catch (Exception e) {
						salary = 0;
					}
					technicianWarehouseBean.setSalary(salary);
				}
				if (exceldatalist.get(i + 5).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setWorkingHours(0);
				} else {
					double workingHours = 0;
					try {
						workingHours = Double.parseDouble(exceldatalist.get(i + 5).trim());
					} catch (Exception e) {
						workingHours = 0;
					}
					technicianWarehouseBean.setWorkingHours(workingHours);
				}

				if (exceldatalist.get(i + 6).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setCellNumber(0);
				} else {
					long cellNumber = 0;
					try {
						cellNumber = Long.parseLong(exceldatalist.get(i + 6).trim());
					} catch (Exception e) {
						cellNumber = 0;
					}
					technicianWarehouseBean.setCellNumber(cellNumber);
				}
				if (exceldatalist.get(i + 7).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setBranch("");
				} else {
					technicianWarehouseBean.setBranch(exceldatalist.get(i + 7).trim());
				}
				if (exceldatalist.get(i + 8).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setParentWarehouse("");
				} else {
					technicianWarehouseBean.setParentWarehouse(exceldatalist.get(i + 8).trim());
				}
				if (exceldatalist.get(i + 9).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setParentStorageLocation("");
				} else {
					technicianWarehouseBean.setParentStorageLocation(exceldatalist.get(i + 9).trim());
				}
				if (exceldatalist.get(i + 10).trim().equalsIgnoreCase("na")) {
					technicianWarehouseBean.setParentStorageBin("");
				} else {
					technicianWarehouseBean.setParentStorageBin(exceldatalist.get(i + 10).trim());
				}
				;
				//
				technicianWarehouseBeanList.add(technicianWarehouseBean);
			}

			List<Employee> employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
					.filter("count IN", employeeIdList).filter("status", true).list();
			List<User> userList = ofy().load().type(User.class).filter("companyId", companyId)
					.filter("employeeName IN", employeeNameList).list();
			List<Storagebin> storageBinList = ofy().load().type(Storagebin.class).filter("companyId", companyId)
					.filter("binName IN", binList).list();

			NumberGeneration numGenEmployee = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
					.filter("processName", "Employee").filter("status", true).first().now();
			NumberGeneration numGenUser = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
					.filter("processName", "User").filter("status", true).first().now();
			NumberGeneration numGenStoragebin = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
					.filter("processName", "Storagebin").filter("status", true).first().now();

			long numberEmp = numGenEmployee.getNumber();
			int countEmp = (int) numberEmp;

			long numberUser = numGenUser.getNumber();
			int countUser = (int) numberUser;

			long numberStorageBin = numGenStoragebin.getNumber();
			int countStorageBin = (int) numberStorageBin;

			numGenEmployee.setNumber(countEmp + technicianWarehouseBeanList.size() - employeeList.size());
			numGenUser.setNumber(countUser + technicianWarehouseBeanList.size() - userList.size());
			numGenStoragebin.setNumber(countStorageBin + technicianWarehouseBeanList.size() - storageBinList.size());

			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(numGenEmployee);
			impl.save(numGenUser);
			impl.save(numGenStoragebin);

			logger.log(Level.SEVERE, "Bean size ==" + technicianWarehouseBeanList.size());
			createAllData(technicianWarehouseBeanList, employeeList, userList, storageBinList, countEmp, countUser,
					countStorageBin, companyId);
			logger.log(Level.SEVERE, "data Saved Sucessfully.");

		}

	}

	private void createAllData(List<TechnicianWarehouseBean> technicianWarehouseBeanList, List<Employee> employeeList,
			List<User> userList, List<Storagebin> storageBinList, int countEmp, int countUser, int countStorageBin,
			long companyId) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		HashMap<Integer, Employee> employeeMap = new HashMap<Integer, Employee>();
		HashMap<String, User> userMap = new HashMap<String, User>();
		HashMap<String, Storagebin> binMap = new HashMap<String, Storagebin>();
		GenricServiceImpl impl = new GenricServiceImpl();
		for (Employee emp : employeeList) {
			employeeMap.put(emp.getCount(), emp);
		}
		for (User user : userList) {
			userMap.put(user.getEmployeeName().trim(), user);
		}
		for (Storagebin bin : storageBinList) {
			binMap.put(bin.getBinName(), bin);
		}

		List<TechnicianWarehouseDetails> detailsList = new ArrayList<TechnicianWarehouseDetails>();
		Branch branch = null;
		if (technicianWarehouseBeanList.size() > 0) {
			branch = ofy().load().type(Branch.class).filter("companyId", companyId)
					.filter("buisnessUnitName", technicianWarehouseBeanList.get(0).getBranch()).first().now();
		}
		UserRole role = ofy().load().type(UserRole.class).filter("companyId", companyId).filter("roleName", "Operator")
				.first().now();
		TechnicianWareHouseDetailsList technicianWareHouseDetailsList = null;
		List<ProductInventoryView> viewDetailsList = ofy().load().type(ProductInventoryView.class)
				.filter("companyId", companyId).list();
		for (TechnicianWarehouseBean bean : technicianWarehouseBeanList) {
			Employee employee = null;
			User user = null;
			Storagebin storageBin = null;

			if (employeeMap.containsKey(bean.getEmployeeId())) {
				employee = employeeMap.get(bean.getEmployeeId());
				employee.setSalary(bean.getSalary());
				employee.setMonthlyWorkingHrs(bean.getWorkingHours());
				employee.setRoleName("Operator");
			} else {
				employee = new Employee();
				employee.setFullname(bean.getEmployeeName());
				employee.setBranchName(bean.getBranch());
				employee.setCountry("India");
				employee.setCellNumber1(bean.getCellNumber());
				if (branch != null && branch.getAddress() != null) {
					employee.setSecondaryAddress(branch.getAddress());
					employee.setAddress(branch.getAddress());
				}
				employee.setEmail("xyz@gmail.com");
				try {
					employee.setDob(format.parse("2000/01/01"));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				employee.setSalaryAmt(bean.getSalary());
				employee.setMonthlyWorkingHrs(bean.getWorkingHours());
				employee.setCompanyId(companyId);
				employee.setCount(++countEmp);
				employee.setStatus(true);
				employee.setRoleName("Operator");
			}
			if (userMap.containsKey(bean.getEmployeeName())) {
				user = userMap.get(bean.getEmployeeName());
				user.setStatus(true);
				user.setUserName(bean.getUserName());
				user.setPassword(bean.getPassword());
			} else {

				user = new User();
				if (role != null) {
					user.setRole(role);
					user.setRoleName("Operator");
				}
				user.setEmployeeName(bean.getEmployeeName());
				user.setBranch(bean.getBranch());
				user.setUserName(bean.getUserName());
				user.setPassword(bean.getPassword());
				user.setEmail("xyz@gmail.com");
				user.setCompanyId(companyId);
				user.setStatus(true);
				user.setCount(++countUser);
			}
			if (binMap.containsKey(bean.getUserName())) {
				storageBin = binMap.get(bean.getUserName());
			} else {
				storageBin = new Storagebin();
				storageBin.setBinName(bean.getUserName());
				storageBin.setWarehouseName(bean.getParentWarehouse());
				storageBin.setStoragelocation(bean.getParentStorageLocation());
				storageBin.setStatus(true);
				storageBin.setXcoordinate("1");
				storageBin.setYcoordinate("2");
				storageBin.setCompanyId(companyId);
				storageBin.setCount(++countStorageBin);
			}
			// detailsList
			ofy().save().entity(employee);
			ofy().save().entity(user);
			ofy().save().entity(storageBin);
			technicianWareHouseDetailsList = ofy().load().type(TechnicianWareHouseDetailsList.class)
					.filter("companyId", companyId).filter("parentWareHouse", bean.getParentWarehouse())
					.filter("parentStorageLocation", bean.getParentStorageLocation())
					.filter("pearentStorageBin", bean.getParentStorageBin()).first().now();
			if (technicianWareHouseDetailsList != null) {
				detailsList = technicianWareHouseDetailsList.getWareHouseList();
				boolean whFlag = true;
				for (TechnicianWarehouseDetails tDetails : technicianWareHouseDetailsList.getWareHouseList()) {
					if (tDetails.getTechnicianStorageBin().equalsIgnoreCase(bean.getUserName())) {
						whFlag = false;
					}
				}
				if (whFlag) {
					TechnicianWarehouseDetails details = new TechnicianWarehouseDetails();
					details.setParentWareHouse(bean.getParentWarehouse());
					details.setParentStorageLocation(bean.getParentStorageLocation());
					details.setPearentStorageBin(bean.getParentStorageBin());
					details.setTechnicianName(bean.getEmployeeName());
					details.setTechnicianWareHouse(bean.getParentWarehouse());
					details.setTechnicianStorageLocation(bean.getParentStorageLocation());
					details.setTechnicianStorageBin(bean.getUserName());
					detailsList.add(details);
				}
				technicianWareHouseDetailsList.setWareHouseList(detailsList);
			} else {

				technicianWareHouseDetailsList = new TechnicianWareHouseDetailsList();
				technicianWareHouseDetailsList.setParentWareHouse(bean.getParentWarehouse());
				technicianWareHouseDetailsList.setParentStorageLocation(bean.getParentStorageLocation());
				technicianWareHouseDetailsList.setPearentStorageBin(bean.getParentStorageBin());
				detailsList = new ArrayList<TechnicianWarehouseDetails>();

				TechnicianWarehouseDetails details = new TechnicianWarehouseDetails();
				details.setParentWareHouse(bean.getParentWarehouse());
				details.setParentStorageLocation(bean.getParentStorageLocation());
				details.setPearentStorageBin(bean.getParentStorageBin());
				details.setTechnicianName(bean.getEmployeeName());
				details.setTechnicianWareHouse(bean.getParentWarehouse());
				details.setTechnicianStorageLocation(bean.getParentStorageLocation());
				details.setTechnicianStorageBin(bean.getUserName());
				detailsList.add(details);
				technicianWareHouseDetailsList.setWareHouseList(detailsList);
				technicianWareHouseDetailsList.setCompanyId(companyId);
			}
			impl.save(technicianWareHouseDetailsList);
		}
		List<ProductInventoryView> viewList = new ArrayList<ProductInventoryView>();

		logger.log(Level.SEVERE, "list size :" + viewDetailsList.size());
		
		
Comparator<ProductInventoryView> comparator = new Comparator<ProductInventoryView>() {
				
	 @Override
	  public int compare(ProductInventoryView e1,ProductInventoryView e2)
	  {
			  if(e1!=null && e2!=null)
			  {
				  if(e1.getProdID()== e2.getProdID()){
					  return 0;
				  }
				  if(e1.getProdID()> e2.getProdID()){
					  return 1;
				  }else{
					  return -1;
				  }
			  }else{
				  return 0;
			  }
	  }
};
	 
	Collections.sort(viewDetailsList,comparator);
			 
			  
		
		for (ProductInventoryView view : viewDetailsList) {
			logger.log(Level.SEVERE, "Product id :" + view.getProdID());
			List<ProductInventoryViewDetails> list = new ArrayList<ProductInventoryViewDetails>();
			list = view.getDetails();

			for (TechnicianWarehouseBean bean : technicianWarehouseBeanList) {
				boolean flag = false, technicianBinFlag = false;
				logger.log(Level.SEVERE, "list size 1:" + list.size());
				logger.log(Level.SEVERE, "Product id inside:" + list.get(0).getProdid());
				for (ProductInventoryViewDetails details : list) {
					
					if (bean.getParentWarehouse().equalsIgnoreCase(details.getWarehousename().trim())
							&& bean.getParentStorageLocation().equalsIgnoreCase(details.getStoragelocation().trim())
							&& bean.getParentStorageBin().equalsIgnoreCase(details.getStoragebin().trim())) {
						flag = true;
					}
					if (bean.getParentWarehouse().equalsIgnoreCase(details.getWarehousename().trim())
							&& bean.getParentStorageLocation().equalsIgnoreCase(details.getStoragelocation().trim())
							&& bean.getUserName().equalsIgnoreCase(details.getStoragebin().trim())) {
						technicianBinFlag = true;
					}
				}
				logger.log(Level.SEVERE, "flags :" + flag + " " + technicianBinFlag);
				if (!flag) {
					ProductInventoryViewDetails pDetails = new ProductInventoryViewDetails();
					pDetails.setWarehousename(bean.getParentWarehouse());
					pDetails.setStoragelocation(bean.getParentStorageLocation());
					pDetails.setStoragebin(bean.getParentStorageBin());
					pDetails.setCompanyId(companyId);
					pDetails.setProdid(view.getProdID());
					pDetails.setProdname(view.getProductName());
					pDetails.setProdcode(view.getProductCode());
					pDetails.setAvailableqty(0);
					pDetails.setProductCategory(view.getProductCategory());
					pDetails.setProdInvViewCount(view.getCount());
					
					list.add(pDetails);
					ProductInventoryTransaction pit=UpdateStock.getProductInventoryTransaction(pDetails.getProdid(), pDetails.getProdname(),pDetails.getAvailableqty(),
							view.getProductUOM(), view.getProductPrice(), pDetails.getWarehousename(), pDetails.getStoragelocation(), 
							pDetails.getStoragebin(), "Product Inventory View", pDetails.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date()),AppConstants.ADD, 0, pDetails.getAvailableqty(), pDetails.getCompanyId(),"","");
					impl.save(pDetails);
					impl.save(pit);
				}
				if (!technicianBinFlag) {
					ProductInventoryViewDetails pDetails = new ProductInventoryViewDetails();
					pDetails.setWarehousename(bean.getParentWarehouse());
					pDetails.setStoragelocation(bean.getParentStorageLocation());
					pDetails.setStoragebin(bean.getUserName());
					pDetails.setCompanyId(companyId);
					pDetails.setProdid(view.getProdID());
					pDetails.setProdname(view.getProductName());
					pDetails.setProdcode(view.getProductCode());
					pDetails.setAvailableqty(0);
					pDetails.setProductCategory(view.getProductCategory());
					list.add(pDetails);
					pDetails.setProdInvViewCount(view.getCount());
					ProductInventoryTransaction pit=UpdateStock.getProductInventoryTransaction(pDetails.getProdid(), pDetails.getProdname(),pDetails.getAvailableqty(),
							view.getProductUOM(), view.getProductPrice(), pDetails.getWarehousename(), pDetails.getStoragelocation(), 
							pDetails.getStoragebin(), "Product Inventory View", view.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date()),AppConstants.ADD, 0, pDetails.getAvailableqty(), pDetails.getCompanyId(),"","");
					impl.save(pDetails);
					impl.save(pit);
				}
			}
		//	if(size > list.size()){
			view.setDetails(list);
			//view.setEditFlag(true);
			ofy().save().entity(view);
			
		//	}
		}
	}

	private void saveEmployeeAssetAllocation(long companyId, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		if (exceldatalist.get(0).trim().equalsIgnoreCase("Employee Id")
				&& exceldatalist.get(1).trim().equalsIgnoreCase("Employee Name")
				&& exceldatalist.get(2).trim().equalsIgnoreCase("Branch")
				&& exceldatalist.get(3).trim().equalsIgnoreCase("Product Id")
				&& exceldatalist.get(4).trim().equalsIgnoreCase("Product Code")
				&& exceldatalist.get(5).trim().equalsIgnoreCase("Product Name")
				&& exceldatalist.get(6).trim().equalsIgnoreCase("Quantity")
				&& exceldatalist.get(7).trim().equalsIgnoreCase("Uom")
				&& exceldatalist.get(8).trim().equalsIgnoreCase("Price")
				&& exceldatalist.get(9).trim().equalsIgnoreCase("Total Amount")
				&& exceldatalist.get(10).trim().equalsIgnoreCase("Issue From Date")
				&& exceldatalist.get(11).trim().equalsIgnoreCase("Issue To Date")
				&& exceldatalist.get(12).trim().equalsIgnoreCase("Update Stock(Yes/No)")
				&& exceldatalist.get(13).trim().equalsIgnoreCase("Warehouse Name")
				&& exceldatalist.get(14).trim().equalsIgnoreCase("Storage Location")
				&& exceldatalist.get(15).trim().equalsIgnoreCase("Storage Bin")) {
			// intlist = leadsave(companyid, exceldatalist);
			ArrayList<EmployeeAsset> list = saveEmployeeAsset(companyId, exceldatalist);
		}
	}

	private ArrayList<EmployeeAsset> saveEmployeeAsset(long companyid, ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Inside Employee Asset Save");

		ArrayList<EmployeeAsset> empAssetList = new ArrayList<EmployeeAsset>();
		ArrayList<Integer> integerlist = new ArrayList<Integer>();

		for (int p = 16; p < exceldatalist.size(); p += 16) {

			EmployeeAsset empAsset = new EmployeeAsset();
			EmployeeAssetBean assetBean = new EmployeeAssetBean();

			if (exceldatalist.get(p).trim().equalsIgnoreCase("na")) {
				empAsset.setEmpId(Integer.parseInt(exceldatalist.get(p).trim()));
			} else {
				empAsset.setEmpId(Integer.parseInt(exceldatalist.get(p).trim()));
			}
			if (exceldatalist.get(p + 1).trim().equalsIgnoreCase("na")) {
				empAsset.setEmpName(exceldatalist.get(p + 1).trim());
			} else {
				empAsset.setEmpName(exceldatalist.get(p + 1).trim());
			}
			if (exceldatalist.get(p + 2).trim().equalsIgnoreCase("na")) {
				empAsset.setBranch(exceldatalist.get(p + 2).trim());
			} else {
				empAsset.setBranch(exceldatalist.get(p + 2).trim());
			}
			if (exceldatalist.get(p + 3).trim().equalsIgnoreCase("NA")) {
				assetBean.setAssetId(Integer.parseInt(exceldatalist.get(p + 3).trim()));
			} else {
				assetBean.setAssetId(Integer.parseInt(exceldatalist.get(p + 3).trim()));
			}
			if (exceldatalist.get(p + 4).trim().equalsIgnoreCase("NA")) {
				assetBean.setAssetCode(exceldatalist.get(p + 4).trim());
			} else {
				assetBean.setAssetCode(exceldatalist.get(p + 4).trim());
			}
			if (exceldatalist.get(p + 5).trim().equalsIgnoreCase("na")) {
				assetBean.setAssetName(exceldatalist.get(p + 5).trim());
			} else {
				assetBean.setAssetName(exceldatalist.get(p + 5).trim());
			}
			if (exceldatalist.get(p + 6).trim().equalsIgnoreCase("na")) {
				assetBean.setQty(Double.parseDouble(exceldatalist.get(p + 6).trim()));
			} else {
				assetBean.setQty(Double.parseDouble(exceldatalist.get(p + 6).trim()));
			}
			if (exceldatalist.get(p + 7).trim().equalsIgnoreCase("na")) {
				assetBean.setUom(exceldatalist.get(p + 7).trim());
			} else {
				assetBean.setUom(exceldatalist.get(p + 7).trim());
			}
			if (exceldatalist.get(p + 8).trim().equalsIgnoreCase("NA")) {
				assetBean.setPerUnitPrice(Double.parseDouble(exceldatalist.get(p + 8).trim()));
			} else {
				assetBean.setPerUnitPrice(Double.parseDouble(exceldatalist.get(p + 8).trim()));
			}
			if (exceldatalist.get(p + 9).trim().equalsIgnoreCase("na")) {
				assetBean.setPrice(Double.parseDouble(exceldatalist.get(p + 9).trim()));
			} else {
				assetBean.setPrice(Double.parseDouble(exceldatalist.get(p + 9).trim()));
			}
			if (exceldatalist.get(p + 10).trim().equalsIgnoreCase("na")) {
				try {
					assetBean.setFromDate(sefmt.parse(exceldatalist.get(p + 10).trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					assetBean.setFromDate(sefmt.parse(exceldatalist.get(p + 10).trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (exceldatalist.get(p + 11).trim().equalsIgnoreCase("na")) {
				try {
					assetBean.setToDate(sefmt.parse(exceldatalist.get(p + 11).trim()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					assetBean.setToDate(sefmt.parse(exceldatalist.get(p + 11).trim()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (exceldatalist.get(p + 12).trim().equalsIgnoreCase("na")) {
				assetBean.setStatus(exceldatalist.get(p + 12).trim());
			} else {
				assetBean.setStatus(exceldatalist.get(p + 12).trim());
			}

			if (exceldatalist.get(p + 13).trim().equalsIgnoreCase("na")) {
				assetBean.setWarehouseName(exceldatalist.get(p + 13).trim());
			} else {
				assetBean.setWarehouseName(exceldatalist.get(p + 13).trim());
			}

			if (exceldatalist.get(p + 14).trim().equalsIgnoreCase("na")) {
				assetBean.setStorageLocName(exceldatalist.get(p + 14).trim());
			} else {
				assetBean.setStorageLocName(exceldatalist.get(p + 14).trim());
			}

			if (exceldatalist.get(p + 15).trim().equalsIgnoreCase("na")) {
				assetBean.setStorageLocBin(exceldatalist.get(p + 15).trim());
			} else {
				assetBean.setStorageLocBin(exceldatalist.get(p + 15).trim());
			}
			empAsset.getEmpAssetList().add(assetBean);
			// empAsset.setCount(++count);
			empAsset.setCompanyId(companyid);
			empAssetList.add(empAsset);

		}
		logger.log(Level.SEVERE, "EMPLOYEE ASSET SIZE : " + empAssetList.size());

		ArrayList<EmployeeAsset> empAssList = validateEmployeeAsset(empAssetList);
		ArrayList<SuperModel> modelList = new ArrayList<SuperModel>();

		for (EmployeeAsset object : empAssList) {
			SuperModel model = object;
			modelList.add(model);
		}
		if (modelList != null && modelList.size() != 0) {
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(modelList);
		}

		return empAssetList;
	}

	public ArrayList<EmployeeAsset> validateEmployeeAsset(ArrayList<EmployeeAsset> empAssetList) {

		/**
		 * Emp Id Emp Name Branch Product Id Product Code Product Name Remark
		 */
		ArrayList<String> errorList = new ArrayList<String>();
		errorList.add("");
		errorList.add("");
		errorList.add("");
		errorList.add("");
		errorList.add("");
		errorList.add("");

		String errorMsg = "";

		long companyId = empAssetList.get(0).getCompanyId();
		boolean stockUpdateFlag = false;
		ArrayList<EmployeeAsset> assetList = new ArrayList<EmployeeAsset>();
		HashSet<Integer> empIdHs = new HashSet<Integer>();
		HashSet<Integer> prodIdHs = new HashSet<Integer>();
		HashSet<Integer> prodInvHs = new HashSet<Integer>();

		logger.log(Level.SEVERE, "INSIDE ASSET VALIDATION  " + companyId);

		HashMap<Integer, ArrayList<EmployeeAsset>> assetMap = new HashMap<Integer, ArrayList<EmployeeAsset>>();

		for (EmployeeAsset empAsset : empAssetList) {
			empIdHs.add(empAsset.getEmpId());
			for (EmployeeAssetBean assetBean : empAsset.getEmpAssetList()) {
				prodIdHs.add(assetBean.getAssetId());
				if (assetBean.getStatus().equalsIgnoreCase("Yes")) {
					prodInvHs.add(assetBean.getAssetId());
					stockUpdateFlag = true;
				}

				if (assetMap != null && assetMap.size() != 0) {
					if (assetMap.containsKey(empAsset.getEmpId())) {
						assetMap.get(empAsset.getEmpId()).add(empAsset);
					} else {
						ArrayList<EmployeeAsset> list = new ArrayList<EmployeeAsset>();
						list.add(empAsset);
						assetMap.put(empAsset.getEmpId(), list);
					}
				} else {
					ArrayList<EmployeeAsset> list = new ArrayList<EmployeeAsset>();
					list.add(empAsset);
					assetMap.put(empAsset.getEmpId(), list);
				}
			}
		}

		logger.log(Level.SEVERE, "ASSET MAP " + assetMap.size());

		ArrayList<Integer> empIdList = new ArrayList<Integer>(empIdHs);
		ArrayList<Integer> prodIdList = new ArrayList<Integer>(prodIdHs);
		ArrayList<Integer> prodInvList = new ArrayList<Integer>(prodInvHs);

		List<Employee> employeeList = new ArrayList<Employee>();
		List<EmployeeAsset> employeeAssetList = new ArrayList<EmployeeAsset>();
		if (empIdList.size() != 0) {
			employeeList = ofy().load().type(Employee.class).filter("companyId", companyId)
					.filter("count IN", empIdList).list();
			employeeAssetList = ofy().load().type(EmployeeAsset.class).filter("companyId", companyId)
					.filter("empId IN", empIdList).list();
			logger.log(Level.SEVERE, "UNIQUE EMP " + empIdList.size() + " EMP MAS " + employeeList.size());
			logger.log(Level.SEVERE, "EXISTING EMP ASSET LIST  " + employeeAssetList.size());

		} else {
			// Error msg come here
			errorMsg = errorMsg + "No Employee Found." + "/";

		}
		
		List<ItemProduct> itemProductList = new ArrayList<ItemProduct>();
		if (prodIdList.size() != 0) {
			itemProductList = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
					.filter("count IN", prodIdList).list();
			logger.log(Level.SEVERE, "UNIQUE PROD " + prodIdList.size() + " PROD MAS " + itemProductList.size());
		} else {
			// Error msg come here
			errorMsg = errorMsg + "No ItemProduct Found." + "/";
		}

		List<ProductInventoryView> pivList = new ArrayList<ProductInventoryView>();
		if (prodInvList.size() != 0) {
			pivList = ofy().load().type(ProductInventoryView.class).filter("companyId", companyId)
					.filter("productinfo.prodID IN", prodInvList).list();
			logger.log(Level.SEVERE, "UNIQUE PIV " + prodInvList.size() + " PIV MAS " + pivList.size());
		} else {
			// Error msg come here
			errorMsg = errorMsg + "No Stock Master Found." + "/";
		}
		
		errorList.add(errorMsg);

		for (Map.Entry<Integer, ArrayList<EmployeeAsset>> entry : assetMap.entrySet()) {
			EmployeeAsset asset = getUpdatedEmployeeAsset(entry.getValue(), employeeList, itemProductList, pivList,
					employeeAssetList, errorList);
			if (asset != null) {
				assetList.add(asset);
			}
		}
		// CsvWriter.employeeAssetErrorList=errorList;
		logger.log(Level.SEVERE, "ASSET LIST  " + assetList.size());

		return assetList;
	}

	private EmployeeAsset getUpdatedEmployeeAsset(ArrayList<EmployeeAsset> value, List<Employee> employeeList,
			List<ItemProduct> itemProductList, List<ProductInventoryView> pivList,
			List<EmployeeAsset> employeeAssetList, ArrayList<String> errorList) {
		// TODO Auto-generated method stub

		String errorMsg = "";
		HashMap<String, Double> stockMap = new HashMap<String, Double>();
		EmployeeAsset object = new EmployeeAsset();
		Employee employee = getEmployeeDetails(value.get(0).getEmpId(), employeeList);
		if (getEmployeeAssetDetail(value.get(0).getEmpId(), employeeAssetList) != null) {
			// ERROR
			errorList.add(value.get(0).getEmpId() + "");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorMsg = errorMsg + "Employee asset master already exist" + " / ";
			errorList.add(errorMsg);
			// CsvWriter.employeeAssetErrorList=errorList;
			// object.setCount(-1);
			return object;

		}
		if (employee == null) {
			// ERROR
			errorList.add(value.get(0).getEmpId() + "");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorList.add("");
			errorMsg = errorMsg + "No Employee Master Found." + " / ";
			errorList.add(errorMsg);
			// CsvWriter.employeeAssetErrorList=errorList;
			// object.setCount(-1);
			return object;
		}
		for (EmployeeAsset obj : value) {
			object.setCompanyId(obj.getCompanyId());
			object.setEmpId(obj.getEmpId());
			if (employee.getFullname().equals(obj.getEmpName())) {
				object.setEmpName(obj.getEmpName());
			} else {
				// Error
				errorMsg = errorMsg + "Employee name mismatched." + " / ";
				// object.setCount(-1);
			}
			/**
			 * @author Anil , Date : 12-11-2019
			 */
			if(employee.getCellNumber1()!=null){
				object.setEmpCellNo(employee.getCellNumber1());
			}

			if (employee.getBranchName().equals(obj.getBranch())) {
				object.setBranch(obj.getBranch());
			} else {
				// ERROR
				errorMsg = errorMsg + "Employee branch mismatched." + " / ";
				// object.setCount(-1);
			}
			for (EmployeeAssetBean bean : obj.getEmpAssetList()) {

				EmployeeAssetBean beanObj = new EmployeeAssetBean();
				EmployeeAssetBean beanHisObj = new EmployeeAssetBean();

				ItemProduct product = getProductDetail(bean.getAssetId(), itemProductList);
				if (product != null) {
					beanHisObj.setAssetId(bean.getAssetId());
					beanObj.setAssetId(bean.getAssetId());
					if (bean.getAssetCode().equals(product.getProductCode())) {
						beanHisObj.setAssetCode(bean.getAssetCode());
						beanObj.setAssetCode(bean.getAssetCode());
					} else {
						// ERROR
						errorMsg = errorMsg + "Asset code is mismatched." + " / ";
						// object.setCount(-1);
					}
					// if(bean.getAssetCategory().equals(product.getProductCategory())){
					beanHisObj.setAssetCategory(product.getProductCategory());
					beanObj.setAssetCategory(product.getProductCategory());
					// }else{
					// //ERROR
					// errorMsg=errorMsg+"Asset category is mismatched."+" / ";
					// object.setCount(-1);
					// }
					if (bean.getAssetName().equals(product.getProductName())) {
						beanHisObj.setAssetName(bean.getAssetName());
						beanObj.setAssetName(bean.getAssetName());
					} else {
						// ERROR
						errorMsg = errorMsg + "Asset name is mismatched." + " / ";
						// object.setCount(-1);
					}
					if (bean.getUom().equals(product.getUnitOfMeasurement())) {
						beanHisObj.setUom(bean.getUom());
						beanObj.setUom(bean.getUom());
					} else {
						// ERROR
						errorMsg = errorMsg + "Uom is mismatched." + " / ";
						// object.setCount(-1);
					}

					if (bean.getFromDate() != null) {
						beanHisObj.setFromDate(bean.getFromDate());
						beanObj.setFromDate(bean.getFromDate());
					}
					if (bean.getToDate() != null) {
						beanHisObj.setToDate(bean.getToDate());
						beanObj.setToDate(bean.getToDate());
					}

					beanHisObj.setQty(bean.getQty());
					beanHisObj.setPerUnitPrice(bean.getPerUnitPrice());
					beanHisObj.setPrice(bean.getPrice());

					beanObj.setQty(bean.getQty());
					beanObj.setPerUnitPrice(bean.getPerUnitPrice());
					beanObj.setPrice(bean.getPrice());

					if (bean.getStatus().equalsIgnoreCase("No")) {
						beanHisObj.setStatus("Issue");
						beanHisObj.setStockUpdated(true);

						beanObj.setStatus("Issue");
						beanObj.setStockUpdated(true);
					} else if (bean.getStatus().equalsIgnoreCase("Yes")) {
						beanHisObj.setStatus("Issue");
						beanHisObj.setStockUpdated(false);

						beanObj.setStatus("Issue");
						beanObj.setStockUpdated(true);

						object.setTransactionType("Issue");

						ProductInventoryView piv = getStockDetails(beanHisObj.getAssetId(), bean.getWarehouseName(),
								bean.getStorageLocName(), bean.getStorageLocBin(), pivList);
						if (piv != null) {
							beanHisObj.setWarehouseName(bean.getWarehouseName());
							beanHisObj.setStorageLocName(bean.getStorageLocName());
							beanHisObj.setStorageLocBin(bean.getStorageLocBin());
							beanHisObj.setAvailableQty(getAvailableQty(bean.getWarehouseName(),
									bean.getStorageLocName(), bean.getStorageLocBin(), piv));

							beanObj.setWarehouseName(bean.getWarehouseName());
							beanObj.setStorageLocName(bean.getStorageLocName());
							beanObj.setStorageLocBin(bean.getStorageLocBin());
							beanObj.setAvailableQty(getAvailableQty(bean.getWarehouseName(), bean.getStorageLocName(),
									bean.getStorageLocBin(), piv));

							String key = bean.getAssetId() + bean.getWarehouseName() + bean.getStorageLocName()
									+ bean.getStorageLocBin();
							if (stockMap != null && stockMap.size() != 0) {
								if (stockMap.containsKey(key)) {
									double qty = stockMap.get(key);
									qty = qty - bean.getQty();
									if (qty < beanHisObj.getQty()) {
										// ERROR
										errorMsg = errorMsg + "Available stock is less than required quantity." + " / ";
										// object.setCount(-1);
									} else {
										stockMap.put(key, qty);
									}
								} else {
									stockMap.put(key, beanHisObj.getAvailableQty());
									if (beanHisObj.getAvailableQty() < beanHisObj.getQty()) {
										// ERROR
										errorMsg = errorMsg + "Available stock is less than required quantity." + " / ";
										// object.setCount(-1);
									}
								}
							} else {
								stockMap.put(key, beanHisObj.getAvailableQty());
								if (beanHisObj.getAvailableQty() < beanHisObj.getQty()) {
									// ERROR
									errorMsg = errorMsg + "Available stock is less than required quantity." + " / ";
									// object.setCount(-1);
								}
							}
						} else {
							// ERROR
							errorMsg = errorMsg + "Product master not found." + " / ";
							// object.setCount(-1);
						}

					} else {
						// ERROR
						errorMsg = errorMsg + "Invalid input at update stock column." + " / ";
						// object.setCount(-1);
					}
				} else {
					// ERROR
					errorMsg = errorMsg + "Product master not found." + " / ";
					// object.setCount(-1);
				}

				object.getEmpAssetList().add(beanObj);
				if (bean.getStatus().equalsIgnoreCase("Yes")) {
					if (object.getEmpAssetHisList() != null) {
						object.getEmpAssetHisList().add(beanHisObj);
					} else {
						ArrayList<EmployeeAssetBean> beanList = new ArrayList<EmployeeAssetBean>();
						beanList.add(beanHisObj);
						object.setEmpAssetHisList(beanList);
					}

				}

				errorList.add(obj.getEmpId() + "");
				errorList.add(obj.getEmpName());
				errorList.add(obj.getBranch());
				errorList.add(bean.getAssetId() + "");
				errorList.add(bean.getAssetCode() + "");
				errorList.add(bean.getAssetName());
				errorList.add(errorMsg);

			}

		}

		return object;
	}

	private EmployeeAsset getEmployeeAssetDetail(int empId, List<EmployeeAsset> employeeAssetList) {
		// TODO Auto-generated method stub
		for (EmployeeAsset obj : employeeAssetList) {
			if (obj.getEmpId() == empId) {
				return obj;
			}
		}
		return null;
	}

	private double getAvailableQty(String warehouseName, String storageLocation, String storageBin,
			ProductInventoryView piv) {
		// TODO Auto-generated method stub
		double qty = 0;
		for (ProductInventoryViewDetails pivd : piv.getDetails()) {
			if (pivd.getWarehousename().equals(warehouseName) && pivd.getStoragelocation().equals(storageLocation)
					&& pivd.getStoragebin().equals(storageBin)) {
				return pivd.getAvailableqty();
			}
		}
		return 0;
	}

	private ProductInventoryView getStockDetails(int assetId, String warehouseName, String storageLocation,
			String storageBin, List<ProductInventoryView> pivList) {
		// TODO Auto-generated method stub
		for (ProductInventoryView piv : pivList) {
			if (assetId == piv.getProdID()) {
				for (ProductInventoryViewDetails pivd : piv.getDetails()) {
					if (pivd.getWarehousename().equals(warehouseName)
							&& pivd.getStoragelocation().equals(storageLocation)
							&& pivd.getStoragebin().equals(storageBin)) {
						return piv;
					}
				}
			}
		}
		return null;
	}

	private ItemProduct getProductDetail(int assetId, List<ItemProduct> itemProductList) {
		// TODO Auto-generated method stub
		for (ItemProduct product : itemProductList) {
			if (assetId == product.getCount()) {
				return product;
			}
		}
		return null;
	}

	private Employee getEmployeeDetails(int empId, List<Employee> employeeList) {
		// TODO Auto-generated method stub
		for (Employee employee : employeeList) {
			if (empId == employee.getCount()) {
				return employee;
			}
		}
		return null;
	}
	
	
	
	
	/**
	 * Date 22-07-2019 by Vijay
	 * Des :- for NBHC CCPM Contract Upload
	 */

	 private void reactOnContractUpload(long companyId, ArrayList<String> exceldatalist) {
		 	SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//			String todayDate = dateFormat.format(new Date());
			GenricServiceImpl genimpl = new GenricServiceImpl();
			ContractDetailsTaskQueue conTaskQueue=new ContractDetailsTaskQueue();
			ServerAppUtility serverapp = new ServerAppUtility();
			boolean complainServiceWithTurnAroundTimeFlag = serverapp.checkForProcessConfigurartionIsActiveOrNot("Contract", "COMPLAINSERVICEWITHTURNAROUNDTIME", companyId);
			List<TaxDetails> taxDetailslist = ofy().load().type(TaxDetails.class).filter("taxChargeStatus", true).filter("companyId", companyId).list();
			
			System.out.println("Done here");
			this.contractUploadBlobKey=null;
			ArrayList<Contract> contractlist = new ArrayList<Contract>();
			
			
					
			ArrayList<Contract> createdContractsList=new ArrayList<Contract>();
			
			for(int i=37;i<exceldatalist.size();i+=37){ //old 25
				Contract contractEntity = new Contract();
				PersonInfo personinfo = new PersonInfo();
				if(!exceldatalist.get(i).trim().equalsIgnoreCase("NA")){
					personinfo.setCount(Integer.parseInt(exceldatalist.get(i).trim()));
//					personinfo.setCellNumber(Long.parseLong(exceldatalist.get(i+1).trim()));
				}
				contractEntity.setCinfo(personinfo);
				if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")){ // client code
					contractEntity.setRefNo(exceldatalist.get(i+2).trim());
				}
				if(!exceldatalist.get(i+3).trim().equalsIgnoreCase("NA")){
					contractEntity.setBranch(exceldatalist.get(i+3).trim());
				}
				if(!exceldatalist.get(i+4).trim().equalsIgnoreCase("NA")){
					contractEntity.setEmployee(exceldatalist.get(i+4).trim());
				}
				if(!exceldatalist.get(i+5).trim().equalsIgnoreCase("NA")){
					contractEntity.setPaymentMethod(exceldatalist.get(i+5).trim());
				}
				if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
					contractEntity.setApproverName(exceldatalist.get(i+6).trim());
				}
				
				if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
					contractEntity.setNumberRange(exceldatalist.get(i+7).trim());
				}
				if(!exceldatalist.get(i+8).trim().equalsIgnoreCase("NA")){
					contractEntity.setPayTerms(exceldatalist.get(i+8).trim());
				}
				
				
				if(!exceldatalist.get(i+9).trim().equalsIgnoreCase("NA")){
					contractEntity.setGroup(exceldatalist.get(i+9).trim());
				}
				if(!exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")){
					contractEntity.setCategory(exceldatalist.get(i+10).trim());
				}
				if(!exceldatalist.get(i+11).trim().equalsIgnoreCase("NA")){
					contractEntity.setType(exceldatalist.get(i+11).trim());
				}
			
				if(!exceldatalist.get(i+12).trim().equalsIgnoreCase("NA")){
					try {
						System.out.println("exceldatalist.get(i+12).trim() "+exceldatalist.get(i+12).trim());
						contractEntity.setContractDate(dateFormat.parse(exceldatalist.get(i+12).trim()));
						System.out.println("Contract Date"+contractEntity.getContractDate());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				SalesLineItem saleslineitem = new SalesLineItem();
				ServiceProduct prod = new ServiceProduct();
				saleslineitem.setPrduct(prod);
				
				if(!exceldatalist.get(i+13).trim().equalsIgnoreCase("NA")){
					Date productStartdDate = null;
					try {
						System.out.println("Excel Start Date =="+exceldatalist.get(i+13).trim());
						logger.log(Level.SEVERE, "productStartdDate =="+productStartdDate);
						productStartdDate = dateFormat.parse(exceldatalist.get(i+13).trim());
						System.out.println("after parse Date =="+productStartdDate);
						logger.log(Level.SEVERE, "productStartdDate"+productStartdDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(productStartdDate);
					int duration = Integer.parseInt(exceldatalist.get(i+14).trim());
					cal.add(Calendar.DATE, +duration); 
					Date productEndDate = cal.getTime();
					saleslineitem.setStartDate(productStartdDate);
					saleslineitem.setEndDate(productEndDate);
					saleslineitem.setDuration(duration);
					logger.log(Level.SEVERE, "productStartdDate"+productStartdDate);
					logger.log(Level.SEVERE, "productEndDate"+productEndDate);
					
				}
				
				if(exceldatalist.get(i+15).trim().equalsIgnoreCase("yes")){
					contractEntity.setContractRate(true);
				}else
					contractEntity.setContractRate(false);
				
				if(exceldatalist.get(i+16).trim().equalsIgnoreCase("yes")){
					contractEntity.setServiceWiseBilling(true);
				}else{
					contractEntity.setServiceWiseBilling(false);
				}
				if(exceldatalist.get(i+17).trim().equalsIgnoreCase("yes")){
					contractEntity.setConsolidatePrice(true);
				}else
					contractEntity.setConsolidatePrice(false);
				
				if(!exceldatalist.get(i+18).trim().equalsIgnoreCase("NA")){
					contractEntity.setDescription(exceldatalist.get(i+18).trim());
				}
				
				
				if(!exceldatalist.get(i+19).trim().equalsIgnoreCase("NA")){
					saleslineitem.setProductCode(exceldatalist.get(i+19).trim());
				}
				/*** Date 18-09-2019 by Vijay This is used for logic for adding multiple products with product No ***/
				if(!exceldatalist.get(i+20).trim().equalsIgnoreCase("NA")){
					saleslineitem.setRemark(exceldatalist.get(i+20).trim());
				}
				if(!exceldatalist.get(i+21).trim().equalsIgnoreCase("NA")){
					saleslineitem.setNumberOfService(Integer.parseInt(exceldatalist.get(i+21).trim()));
				}
				/** Date 25-09-2020 by VIjay For Aset Qty map into Contract Customer branch Level ***/
				if(complainServiceWithTurnAroundTimeFlag && !exceldatalist.get(i+22).trim().equalsIgnoreCase("NA") &&
						!exceldatalist.get(i+22).trim().equalsIgnoreCase("0")){
					saleslineitem.setBranchSchedulingInfo4(exceldatalist.get(i+22).trim());
				}
				
				if(!exceldatalist.get(i+23).trim().equalsIgnoreCase("NA")){ // UNIT
					saleslineitem.setArea(exceldatalist.get(i+23).trim());
				}
				if(!exceldatalist.get(i+24).trim().equalsIgnoreCase("NA")){
					saleslineitem.setUnitOfMeasurement(exceldatalist.get(i+24).trim());
				}
				if(!exceldatalist.get(i+25).trim().equalsIgnoreCase("NA")){
					saleslineitem.setPrice(Double.parseDouble(exceldatalist.get(i+25).trim()));
				}
				/*** these are for logic reference ******/
				if(!exceldatalist.get(i+26).trim().equalsIgnoreCase("NA")){
					saleslineitem.setBranchSchedulingInfo(exceldatalist.get(i+26).trim());
				}
				
				
				if(!exceldatalist.get(i+27).trim().equalsIgnoreCase("NA")){
					saleslineitem.setBranchSchedulingInfo5(exceldatalist.get(i+27).trim()); //for branchwise amount
				}
				
				saleslineitem.setBranchSchedulingInfo6(exceldatalist.get(i+28).trim()); //for tax1
				
				
				saleslineitem.setBranchSchedulingInfo7(exceldatalist.get(i+29).trim());//for tax2
				
				if(!exceldatalist.get(i+30).trim().equalsIgnoreCase("NA")){
					saleslineitem.setPremisesDetails(exceldatalist.get(i+30).trim());
				}
				if(!exceldatalist.get(i+31).trim().equalsIgnoreCase("NA")){
					saleslineitem.setBranchSchedulingInfo8(exceldatalist.get(i+31).trim());//for remark
				}
				if(!exceldatalist.get(i+32).trim().equalsIgnoreCase("NA")){
					saleslineitem.setProModelNo(exceldatalist.get(i+32).trim());
				}
				if(!exceldatalist.get(i+33).trim().equalsIgnoreCase("NA")){
					saleslineitem.setProSerialNo(exceldatalist.get(i+33).trim());
				}
				
				if(!exceldatalist.get(i+34).trim().equalsIgnoreCase("NA")){
					saleslineitem.setBranchSchedulingInfo1(exceldatalist.get(i+34).trim());
				}
				if(!exceldatalist.get(i+35).trim().equalsIgnoreCase("NA")){
					saleslineitem.setBranchSchedulingInfo2(exceldatalist.get(i+35).trim());
				}
				if(!exceldatalist.get(i+36).trim().equalsIgnoreCase("NA")){
					saleslineitem.setBranchSchedulingInfo3(exceldatalist.get(i+36).trim());
				}
								
//				if(!exceldatalist.get(i+22).trim().equalsIgnoreCase("NA")){
//					contractEntity.setReferenceNo(exceldatalist.get(i+22).trim());
//				}
				
				saleslineitem.setTermsAndConditions(new DocumentUpload());
				saleslineitem.setProductImage(new DocumentUpload());
				saleslineitem.getPrduct().setProductImage1(new DocumentUpload());
				saleslineitem.getPrduct().setProductImage2(new DocumentUpload());
				saleslineitem.getPrduct().setProductImage3(new DocumentUpload());
				saleslineitem.getPrduct().setComment("");
				saleslineitem.getPrduct().setCommentdesc("");
				saleslineitem.getPrduct().setCommentdesc1("");
				saleslineitem.getPrduct().setCommentdesc2("");
				   
				contractEntity.getItems().add(saleslineitem);
				/**** ends here ************/
				contractlist.add(contractEntity);
			}
		
			HashSet<Integer> hscustomerClientCode=new HashSet<Integer>();
			HashSet<String> hsprodCode=new HashSet<String>();
			HashSet<String> hscustomerBranch = new HashSet<String>();
			
			HashMap<Integer, ArrayList<Contract>> contractHashMap = new HashMap<Integer, ArrayList<Contract>>();
			List<ConfigCategory> payTermsconfig =   ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("internalType", 25).list();

			logger.log(Level.SEVERE, "contractlist size"+contractlist.size());
			for(Contract contract :contractlist){
				hscustomerClientCode.add(contract.getCinfo().getCount());
				hsprodCode.add(contract.getItems().get(0).getProductCode());
				hscustomerBranch.add(contract.getItems().get(0).getBranchSchedulingInfo());
				
				if(contractHashMap!=null && contractHashMap.size()!=0){
					if(contractHashMap.containsKey(contract.getCinfo().getCount())){
						contractHashMap.get(contract.getCinfo().getCount()).add(contract);
					}
					else{
						ArrayList<Contract> list = new ArrayList<Contract>();
						list.add(contract);
						contractHashMap.put(contract.getCinfo().getCount(), list);
					}
				}
				else{
					ArrayList<Contract> list = new ArrayList<Contract>();
					list.add(contract);
					contractHashMap.put(contract.getCinfo().getCount(), list);
				}
			}
			logger.log(Level.SEVERE, "contractHashMap size"+contractHashMap.size());

			ArrayList<String> strcustomerBrnach = new ArrayList<String>(hscustomerBranch);
			
//			List<CustomerBranchDetails> globalCustomerBranchDetailsList = ofy().load().type(CustomerBranchDetails.class)
//											.filter("companyId", companyId).filter("buisnessUnitName IN", strcustomerBrnach).list();
			ArrayList<Integer> clientcodelist = new ArrayList<Integer>(hscustomerClientCode);
			List<CustomerBranchDetails> globalCustomerBranchDetailsList = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", companyId).filter("cinfo.count IN", clientcodelist).list();
			logger.log(Level.SEVERE, "globalCustomerBranchDetailsList size"+globalCustomerBranchDetailsList.size());
			HashMap<String, Double> branchAreaList = new HashMap<String, Double>(); //Ashwini Patil Date:2-04-2024 orion wants to map area from customer branch to service
			if(globalCustomerBranchDetailsList!=null) {
				for(CustomerBranchDetails branchDt : globalCustomerBranchDetailsList){
					if(branchDt.getArea()>0)
						branchAreaList.put(branchDt.getBusinessUnitName(), branchDt.getArea());
				}			
			}
			
			
			ArrayList<String> productCodelist = new ArrayList<String>(hsprodCode);
			List<ServiceProduct> serviceproductlist = ofy().load().type(ServiceProduct.class)
												.filter("companyId", companyId).filter("productCode IN", productCodelist).list();
			List<Customer> customerlist = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count IN", clientcodelist).list();
			// Customer branch address updation
			ArrayList<CustomerBranchDetails> customerBranchUpdation = new ArrayList<CustomerBranchDetails>();
			
		for (Map.Entry<Integer, ArrayList<Contract>> entryset : contractHashMap.entrySet()) {
			System.out.println("entryset.getkey" + entryset.getKey());
			Contract contractEntity = new Contract();

			ArrayList<Contract> listcontract = entryset.getValue();
			System.out.println("listcontract size"+listcontract.size());
			logger.log(Level.SEVERE, "listcontract size"+listcontract.size());
			HashMap<String, ArrayList<Contract>> producthashmap = new HashMap<String, ArrayList<Contract>>();
			for (Contract contract : listcontract) {
				String productkey = contract.getItems().get(0).getProductCode()+contract.getItems().get(0).getRemark();
				if (producthashmap != null && producthashmap.size() != 0) {
					if (producthashmap.containsKey(productkey)) {
						producthashmap.get(productkey).add(contract);
					} else {
						ArrayList<Contract> list = new ArrayList<Contract>();
						list.add(contract);
						producthashmap.put(productkey, list);
					}
				} else {
					ArrayList<Contract> list = new ArrayList<Contract>();
					list.add(contract);
					producthashmap.put(productkey, list);
				}
			}
			System.out.println("producthashmap"+producthashmap.size());
			int productSrNo = 0;
			double totalAmt  = 0,contractAmt = 0;
			ArrayList<SalesLineItem> saleslineitemlist = new ArrayList<SalesLineItem>();
			// for product looping
			for (Map.Entry<String, ArrayList<Contract>> saleslineEntrySet : producthashmap.entrySet()) {
				System.out.println("Product Name" + saleslineEntrySet.getKey());
				ArrayList<Contract> productlist = saleslineEntrySet.getValue();
				System.out.println("productlist =="+productlist.size());
				ArrayList<BranchWiseScheduling> customerbranchlist = new ArrayList<BranchWiseScheduling>();
				for (Contract item : productlist) {
					BranchWiseScheduling serviceLocation = new BranchWiseScheduling();
					if (item.getItems().get(0).getArea() != null && !item.getItems().get(0).getArea().equals("NA")) {
						serviceLocation.setArea(Double.parseDouble(item.getItems().get(0).getArea()));
					}
					if(item.getItems().get(0).getBranchSchedulingInfo5()!=null&&!item.getItems().get(0).getBranchSchedulingInfo5().equals(""))
					serviceLocation.setAmount(Double.parseDouble(item.getItems().get(0).getBranchSchedulingInfo5()));//Ashwini Patil
					serviceLocation.setBranchName(item.getItems().get(0).getBranchSchedulingInfo());
					serviceLocation.setCheck(true);
					serviceLocation.setUnitOfMeasurement(item.getItems().get(0).getUnitOfMeasurement());
					/** Date 31-10-2019 By Vijay for mapping servicing branch ***/
					serviceLocation.setServicingBranch(getCustomerBranchServingBrnach(item.getItems().get(0).getBranchSchedulingInfo(),globalCustomerBranchDetailsList));
					if(item.getItems().get(0).getArea()!=null && !item.getItems().get(0).getArea().equalsIgnoreCase("NA") 
							&& !item.getItems().get(0).getArea().equals("")){
						serviceLocation.setArea(Double.parseDouble(item.getItems().get(0).getArea()));
					}else {
						if(branchAreaList.get(item.getItems().get(0).getBranchSchedulingInfo())!=null) {
							serviceLocation.setArea(branchAreaList.get(item.getItems().get(0).getBranchSchedulingInfo()));
							logger.log(Level.SEVERE, "branch area updated");
						}
					}
						
					
					/** @author Vijay Date 25-09-2020
					 * Des :- if complainServiceWithTurnAroundTimeFlag is active then it will map 
					 * asset Qty to Contract and will schedule Services requirement raised by Rahul Tiwari
					 */
					if(complainServiceWithTurnAroundTimeFlag){
						if(item.getItems().get(0).getBranchSchedulingInfo4()!=null && !item.getItems().get(0).getBranchSchedulingInfo4().equals("")){
							try {
								int assetQty = Integer.parseInt(item.getItems().get(0).getBranchSchedulingInfo4());
								if(assetQty>0){
									serviceLocation.setAssetQty(assetQty);
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
						CustomerBranchDetails customerbranchDetails = getcustomerBranch(item.getItems().get(0).getBranchSchedulingInfo(),globalCustomerBranchDetailsList);
						if(customerbranchDetails!=null){
							if(customerbranchDetails.getTat()!=null){
								serviceLocation.setTat(customerbranchDetails.getTat());
							}
							if(customerbranchDetails.getTierName()!=null){
								serviceLocation.setTierName(customerbranchDetails.getTierName());
							}
						}
						ServiceProduct serviceprod = getserviceProduct(productlist.get(0).getItems().get(0).getProductCode(), serviceproductlist);
						if(serviceprod.getProductGroup()!=null)
						serviceLocation.setComponentName(serviceprod.getProductGroup());
						serviceLocation.setDurationForReplacement(serviceprod.getWarrantyPeriod());
				    }
					
					
					
					/**
					 * ends here 
					 */
					
					customerbranchlist.add(serviceLocation);
					/** Customer Branch Address Updation *****/
					if(item.getItems().get(0).getBranchSchedulingInfo1()!=null
							|| item.getItems().get(0).getBranchSchedulingInfo2()!=null
							|| item.getItems().get(0).getBranchSchedulingInfo3()!=null){
						CustomerBranchDetails customerBranch = new CustomerBranchDetails();
						customerBranch.setBusinessUnitName(serviceLocation.getBranchName());
						Address address = new Address();
						if(item.getItems().get(0).getBranchSchedulingInfo1()!=null)
						address.setAddrLine1(item.getItems().get(0).getBranchSchedulingInfo1());
						if(item.getItems().get(0).getBranchSchedulingInfo2()!=null)
						address.setAddrLine2(item.getItems().get(0).getBranchSchedulingInfo2());
						if(item.getItems().get(0).getBranchSchedulingInfo3()!=null)
						address.setCity(item.getItems().get(0).getBranchSchedulingInfo3());
						customerBranch.setAddress(address);
						customerBranchUpdation.add(customerBranch);
					}
					
				}
				
				if(complainServiceWithTurnAroundTimeFlag){
					for(BranchWiseScheduling obj:customerbranchlist){
						List<ComponentDetails> list=new ArrayList<ComponentDetails>();
						if(obj.isCheck()
								&&(obj.getComponentList()==null||obj.getComponentList().size()==0)){
							if(obj.getAssetQty()>0){
								for(int i=0;i<obj.getAssetQty();i++){
									ComponentDetails comp=new ComponentDetails();
									comp.setComponentName(obj.getComponentName());
									comp.setSrNo(i);
									
									/**
									 * @author Anil
									 * @since 15-10-2020
									 * setting duration
									 */
									comp.setDurationForReplacement(obj.getDurationForReplacement());
									list.add(comp);
								}
							}
							if(list.size()!=0){
								obj.setComponentList(list);
							}
						}
					}
				}
				
				SalesLineItem saleslineItem = new SalesLineItem();
				ServiceProduct serviceprod = getserviceProduct(productlist.get(0).getItems().get(0).getProductCode(), serviceproductlist);
				ServiceProduct serviceproduct = new ServiceProduct();
				serviceproduct = mycloneServiceProduct(serviceprod,companyId);
				saleslineItem.setPrduct(serviceproduct);
				saleslineItem.setProductSrNo(++productSrNo);
				
				HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlistInfo = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
				customerBranchlistInfo.put(productSrNo, customerbranchlist);
				saleslineItem.setCustomerBranchSchedulingInfo(customerBranchlistInfo);

				saleslineItem.setProductName(saleslineItem.getProductName());
				int duration = productlist.get(0).getItems().get(0).getDuration();
				double price = productlist.get(0).getItems().get(0).getPrice();
				int noofservices = productlist.get(0).getItems().get(0).getNumberOfServices();
				saleslineItem.setDuration(duration);
				saleslineItem.setPrice(price);
				saleslineItem.setNumberOfService(noofservices);
				System.out.println("saleslineItem duration=="+saleslineItem.getDuration());
				saleslineItem.setUnitOfMeasurement(productlist.get(0).getItems().get(0).getUnitOfMeasurement());
				double qty = customerbranchlist.size();
				saleslineItem.setQuantity(qty);
				Date prosuctStartdDate = null;
				prosuctStartdDate = productlist.get(0).getItems().get(0).getStartDate();
				System.out.println("Product Start Date =="+DateUtility.getDateWithTimeZone("IST", prosuctStartdDate));
				saleslineItem.setStartDate(DateUtility.getDateWithTimeZone("IST", prosuctStartdDate));
				int prodDuration = saleslineItem.getDuration()-1;
				Calendar cal = Calendar.getInstance();
				cal.setTime(prosuctStartdDate);
				cal.add(Calendar.DATE, prodDuration);
				Date productEndDate = cal.getTime();
				if (!productlist.get(0).getItems().get(0).getArea().equalsIgnoreCase("NA")) {
					saleslineItem.setArea(productlist.get(0).getItems().get(0).getArea());
				}
				if (productlist.get(0).getItems().get(0).getBranchSchedulingInfo6()!=null) {					
					Tax tax1 = new Tax();
					
                    tax1 = conTaskQueue.getTaxDetails(productlist.get(0).getItems().get(0).getBranchSchedulingInfo6(), taxDetailslist);
                    saleslineItem.setVatTax(tax1);
                    saleslineItem.setVatTaxEdit(tax1.getTaxConfigName());
				}
				if (productlist.get(0).getItems().get(0).getBranchSchedulingInfo7()!=null) {					
					Tax tax2 = new Tax();
                    tax2 = conTaskQueue.getTaxDetails(productlist.get(0).getItems().get(0).getBranchSchedulingInfo7(), taxDetailslist);
                    saleslineItem.setServiceTax(tax2);
                    saleslineItem.setServiceTaxEdit(tax2.getTaxConfigName());
				}				
				
				if (productlist.get(0).getItems().get(0).getBranchSchedulingInfo8()!=null) {
					saleslineItem.setRemark(productlist.get(0).getItems().get(0).getBranchSchedulingInfo8());
				}
				if (productlist.get(0).getItems().get(0).getPremisesDetails()!=null) {
					saleslineItem.setPremisesDetails(productlist.get(0).getItems().get(0).getPremisesDetails());
				}
				
				if(productlist.get(0).getItems().get(0).getArea().matches("[0-9.]+") && Double.parseDouble(productlist.get(0).getItems().get(0).getArea()) > 0){
					totalAmt = productlist.get(0).getItems().get(0).getPrice() * Double.parseDouble(productlist.get(0).getItems().get(0).getArea());
				}else{
					totalAmt = productlist.get(0).getItems().get(0).getPrice();
				}
				contractAmt = contractAmt+totalAmt;
				saleslineItem.setTotalAmount(totalAmt);
				
				System.out.println("Product END Date =="+DateUtility.getDateWithTimeZone("IST", productEndDate));
				saleslineItem.setEndDate(DateUtility.getDateWithTimeZone("IST", productEndDate));
				
				saleslineItem.setBranchSchedulingInfo("");
				saleslineItem.setBranchSchedulingInfo1("");
				saleslineItem.setBranchSchedulingInfo2("");
				saleslineItem.setBranchSchedulingInfo5("");
				saleslineItem.setBranchSchedulingInfo6("");
				saleslineItem.setBranchSchedulingInfo7("");
				saleslineItem.setBranchSchedulingInfo8("");
//				saleslineItem.setRemark("");
				
				
				saleslineItem.setTermsAndConditions(new DocumentUpload());
				saleslineItem.setProductImage(new DocumentUpload());
				saleslineItem.getPrduct().setProductImage1(new DocumentUpload());
				saleslineItem.getPrduct().setProductImage2(new DocumentUpload());
				saleslineItem.getPrduct().setProductImage3(new DocumentUpload());
				saleslineItem.getPrduct().setComment("");
				saleslineItem.getPrduct().setCommentdesc("");
				saleslineItem.getPrduct().setCommentdesc1("");
				saleslineItem.getPrduct().setCommentdesc2("");
				
				saleslineitemlist.add(saleslineItem);
			}
			contractEntity.setItems(saleslineitemlist);
			contractEntity.setCinfo(getPersonInfo(listcontract.get(0).getCinfo().getCount(),customerlist));
			contractEntity.setBranch(listcontract.get(0).getBranch());
			contractEntity.setEmployee(listcontract.get(0).getEmployee());
			contractEntity.setPaymentMethod(listcontract.get(0).getPaymentMethod());
			contractEntity.setApproverName(listcontract.get(0).getApproverName());
			contractEntity.setGroup(listcontract.get(0).getGroup());
			contractEntity.setCategory(listcontract.get(0).getCategory());
			contractEntity.setType(listcontract.get(0).getType());
			System.out.println("listcontract.get(0).getContractDate() =="+listcontract.get(0).getContractDate());
			contractEntity.setContractDate(listcontract.get(0).getContractDate());
			
			if(!contractEntity.isServiceWiseBilling()&&!contractEntity.isContractRate())
				contractEntity.setPayTerms(listcontract.get(0).getPayTerms());
			contractEntity.setType(listcontract.get(0).getType());
			
			if(listcontract.get(0).getRefNo()!=null)
				contractEntity.setRefNo(listcontract.get(0).getRefNo());
			contractEntity.setNumberRange(listcontract.get(0).getNumberRange());
			contractEntity.setContractRate(listcontract.get(0).isContractRate());
			contractEntity.setServiceWiseBilling(listcontract.get(0).isServiceWiseBilling());
			contractEntity.setConsolidatePrice(listcontract.get(0).isConsolidatePrice());
			if(listcontract.get(0).getDescription()!=null&&!listcontract.get(0).getDescription().equals(""))
				contractEntity.setDescription("This data uploded through upload program " + dateFormat.format(new Date())+"\n"+listcontract.get(0).getDescription());
			else
				contractEntity.setDescription("This data uploded through upload program " + dateFormat.format(new Date()));
			
			
			if(!contractEntity.isServiceWiseBilling()&&!contractEntity.isContractRate()){
				ConfigCategory configcategory = getPaymentTerms(payTermsconfig,contractEntity.getPayTerms());
				contractEntity.setPaymentTermsList(reactOnAddPayTerms(contractEntity.getItems(), contractEntity.getPayTerms(), configcategory, contractEntity.getContractDate()));
				// payment terms pending and service scheduling part and total amount				
			}else if(contractEntity.isContractRate()){				
				List<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();
				PaymentTerms pterm = new PaymentTerms();
				pterm.setPayTermDays(0);
				pterm.setPayTermPercent(100.00);
				pterm.setPayTermComment("Advance");
				ptermsList.add(pterm);
				contractEntity.setPaymentTermsList(ptermsList);
			}
			ArrayList<ServiceSchedule> serviceList = new ArrayList<ServiceSchedule> ();
			serviceList = createServiceList(contractEntity.getItems(),globalCustomerBranchDetailsList,companyId,complainServiceWithTurnAroundTimeFlag);
			int numberOfServiceLimit = 700;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial",companyId)){
				numberOfServiceLimit = 350;
			}
			if(serviceList.size()>numberOfServiceLimit){
				
			}
			else{
				contractEntity.setServiceScheduleList(serviceList);
			}
			contractEntity.setCompanyId(companyId);	
			
			Date contractStartDate = getcontractStartDate(contractEntity.getItems());
			Date contractEndDate = getContractEndDate(contractEntity.getItems());
			logger.log(Level.SEVERE, "contractStartDate"+contractStartDate);
			logger.log(Level.SEVERE, "contractEndDate"+contractEndDate);
			contractEntity.setEndDate(contractEndDate);
			contractEntity.setStartDate(contractStartDate);
			
			//old before texes
//			contractEntity.setTotalAmount(contractAmt);
//			contractEntity.setNetpayable(contractAmt);
//			contractEntity.setGrandTotal(contractAmt);
//			contractEntity.setFinalTotalAmt(contractAmt);
//			contractEntity.setInclutaxtotalAmount(contractAmt);
//			contractEntity.setGrandTotalAmount(contractAmt);
			
			
			
			
			ArrayList<ProductOtherCharges> prodTaxeslist = new ArrayList<ProductOtherCharges>();
			prodTaxeslist = conTaskQueue.getproductTaxeslist(saleslineitemlist);
			
			contractEntity.setProductTaxes(prodTaxeslist);
			
			double totalAmountWithtax = conTaskQueue.getTotalAmountWithTax(prodTaxeslist,contractAmt);						
			contractEntity.setTotalAmount(contractAmt);
			contractEntity.setNetpayable(totalAmountWithtax);
			contractEntity.setFinalTotalAmt(contractAmt);
			contractEntity.setGrandTotal(totalAmountWithtax);
			contractEntity.setGrandTotalAmount(totalAmountWithtax);
			contractEntity.setInclutaxtotalAmount(totalAmountWithtax);
					
			
			contractEntity.setStatus(Contract.CREATED);
			

			int contractId=0;
			if(contractEntity.getNumberRange()!=null && !contractEntity.getNumberRange().equals("")) {
				String processName = "C_"+contractEntity.getNumberRange();
				contractId=(int) numGen.getLastUpdatedNumber(processName, contractEntity.getCompanyId(), (long) 1);
				logger.log(Level.SEVERE, "Number Range - contract count" + contractId);
				contractEntity.setCount(contractId);
				ReturnFromServer server = new ReturnFromServer();
				server = genimpl.save(contractEntity);
				contractEntity.setId(server.id);
				createdContractsList.add(contractEntity);
				logger.log(Level.SEVERE, "Number Range - contract key" + contractEntity.getId());
			}
			else {
				contractId=(int) numGen.getLastUpdatedNumber("Contract", contractEntity.getCompanyId(), (long) 1);
				contractEntity.setCount(contractId);
				ReturnFromServer server = new ReturnFromServer();
				server = genimpl.save(contractEntity);
				contractEntity.setId(server.id);
				createdContractsList.add(contractEntity);
				logger.log(Level.SEVERE, "Default Number Range - contract count="+contractId+" "+contractEntity.getCount()+" contract key" + contractEntity.getId());
			}
	
			logger.log(Level.SEVERE,"Contract saved successfull");
			if(serviceList.size()>numberOfServiceLimit){
				for(ServiceSchedule sch:serviceList){
					sch.setDocumentType("Contract");
					sch.setDocumentId(contractId);//server.count
					sch.setCompanyId(companyId);
				}
				ofy().save().entities(serviceList).now();
				logger.log(Level.SEVERE,"Service list saved successfully"+serviceList.size());
			}
			
			
			
			logger.log(Level.SEVERE, "customerBranchUpdation Size to update"+customerBranchUpdation.size());
			HashSet<String> customerBranchNames = new HashSet<String>();
			for(CustomerBranchDetails customerBranch : customerBranchUpdation){
				customerBranchNames.add(customerBranch.getBusinessUnitName());
			}
			ArrayList<String> customerbranches = new ArrayList<String>(customerBranchNames);
			if(customerbranches.size()!=0){
				List<CustomerBranchDetails> customerbranchlist = ofy().load().type(CustomerBranchDetails.class)
						.filter("buisnessUnitName IN", customerbranches).list();
				logger.log(Level.SEVERE, "customerbranchlist==" + customerbranchlist.size());

				ArrayList<CustomerBranchDetails> updatedCustomerBranchlist = new ArrayList<CustomerBranchDetails>();
				
				for (CustomerBranchDetails customerBranch : customerbranchlist) {
					for (CustomerBranchDetails servicelocation : customerBranchUpdation) {
						if (customerBranch.getBusinessUnitName().trim()
								.equals(servicelocation.getBusinessUnitName().trim())) {
							if(!servicelocation.getAddress().getAddrLine1().equalsIgnoreCase("NA")){
								customerBranch.getAddress().setAddrLine1(servicelocation.getAddress().getAddrLine1());
							}
							if(!servicelocation.getAddress().getAddrLine2().equalsIgnoreCase("NA")){
								customerBranch.getAddress().setAddrLine2(servicelocation.getAddress().getAddrLine2());
							}
							if(!servicelocation.getAddress().getCity().equalsIgnoreCase("NA")){
								customerBranch.getAddress().setCity(servicelocation.getAddress().getCity());
							}
							updatedCustomerBranchlist.add(customerBranch);
						}
					}
				}
				logger.log(Level.SEVERE, "Customer Branch Updation list size"+updatedCustomerBranchlist.size());
				ofy().save().entities(updatedCustomerBranchlist).now();
				logger.log(Level.SEVERE, "Customer Branch Updated successfully");
			}
			
			
		}
		
		
		//Ashwini Patil Date:30-03-2023 Previously contracts were getting created and approval request was getting sent but orion want automatic contract approval so added this code
		if(createdContractsList!=null){
			logger.log(Level.SEVERE, "Contracts loaded and sending to taskqueue for approval "+createdContractsList.size());
			
			for(Contract model:createdContractsList){
				Approvals approval=new Approvals();
				approval.setApproverName(model.getApproverName());
				approval.setBranchname(model.getBranch());
				approval.setBusinessprocessId(model.getCount());
				approval.setPersonResponsible(model.getEmployee());
				approval.setDocumentCreatedBy(model.getApproverName());
				approval.setApprovalLevel(1);
				approval.setBusinessprocesstype("Contract");
				approval.setBpId(model.getCinfo().getCount()+"");
				approval.setBpName(model.getCinfo().getFullName());
				approval.setDocumentValidation(false); //managed for nbhc in standard method
				
				approval.setCompanyId(model.getCompanyId());
				approval.setStatus(ConcreteBusinessProcess.APPROVED);
				approval.setApprovalOrRejectDate(new Date());
				logger.log(Level.SEVERE, "before approval");
				ReturnFromServer result =genimpl.save(approval);
				approval.setId(result.id);
				logger.log(Level.SEVERE, "after approval id="+approval.getId());
				ApprovableServiceImplementor aaprovalservice =new ApprovableServiceImplementor();
				aaprovalservice.sendApproveRequest(approval);
				
			}	
			logger.log(Level.SEVERE, "after approving all contracts");
			CommonServiceImpl commonservice = new CommonServiceImpl();
			//Approval process may take some time to create services so calling update service value method separately
			for(Contract model:createdContractsList){
				logger.log(Level.SEVERE, "in update for "+model.isServiceWiseBilling());
				
				if(model.isServiceWiseBilling()){
					logger.log(Level.SEVERE, "updateServiceValue called");
					commonservice.updateServiceValue(model.getCount(), model.getCompanyId());
				}				
			}
		}
	 }
















private ServiceProduct mycloneServiceProduct(ServiceProduct serviceprod, long companyId) {
		ServiceProduct serProduct = new ServiceProduct();
		serProduct.setDuration(serviceprod.getDuration());
		serProduct.setNumberOfServices(serviceprod.getNumberOfServices());
		serProduct.setPrice(serviceprod.getPrice());
		serProduct.setServiceTime(serviceprod.getServiceTime());
		serProduct.setTermsoftreatment(serviceprod.getTermsoftreatment());
		serProduct.setWarrantyPeriod(serviceprod.getWarrantyPeriod());
		serProduct.setProductCode(serviceprod.getProductCode());
		serProduct.setProductName(serviceprod.getProductName());
		serProduct.setSpecifications(serviceprod.getSpecifications());
		serProduct.setComment(serviceprod.getComment());
		serProduct.setCommentdesc(serviceprod.getCommentdesc());
		serProduct.setCommentdesc1(serviceprod.getCommentdesc1());
		serProduct.setCommentdesc2(serviceprod.getCommentdesc2());
		serProduct.setUnitOfMeasurement(serviceprod.getUnitOfMeasurement());
		serProduct.setPrice(serviceprod.getPrice());
		serProduct.setTermsAndConditions(serviceprod.getTermsAndConditions());
		serProduct.setStatus(serviceprod.isStatus());
		serProduct.setProductCategory(serviceprod.getProductCategory());
		serProduct.setProductType(serviceprod.getProductType());
		serProduct.setProductGroup(serviceprod.getProductGroup());
		serProduct.setProductClassification(serviceprod.getProductClassification());
		serProduct.setProductImage(serviceprod.getProductImage());
		serProduct.setProductImage1(serviceprod.getProductImage1());
		serProduct.setProductImage2(serviceprod.getProductImage2());
		serProduct.setProductImage3(serviceprod.getProductImage3());
		serProduct.setServiceTax(serviceprod.getServiceTax());
		serProduct.setVatTax(serviceprod.getVatTax());
		serProduct.setRefNumber1(serviceprod.getRefNumber1());
		serProduct.setRefNumber2(serviceprod.getRefNumber2());
		serProduct.setHsnNumber(serviceprod.getHsnNumber());
		serProduct.setPurchasePrice(serviceprod.getPrice());
		serProduct.setPurchaseTax1(serviceprod.getPurchaseTax1());
		serProduct.setPurchaseTax2(serviceprod.getPurchaseTax2());
		serProduct.setCompanyId(serviceprod.getCompanyId());
		serProduct.setUserId(serviceprod.getUserId());
		serProduct.setId(serviceprod.getId());
		serProduct.setCreatedBy(serviceprod.getCreatedBy());
		serProduct.setCount(serviceprod.getCount());
		return serProduct;
	}

	private Date getContractEndDate(List<SalesLineItem> items) {
		Date endDate = items.get(0).getEndDate();
		Date Date = items.get(0).getEndDate();
		for(SalesLineItem salesline : items){
			if(salesline.getEndDate().after(Date)){
				endDate = salesline.getStartDate();
			}
		}
		return endDate;
	}



	private Date getcontractStartDate(List<SalesLineItem> items) {
		Date startDate = items.get(0).getStartDate();
		Date Date = items.get(0).getStartDate();
		for(SalesLineItem salesline : items){
			if(salesline.getStartDate().before(Date)){
				startDate = salesline.getStartDate();
			}
		}
		return startDate;
	}



	private ConfigCategory getPaymentTerms(List<ConfigCategory> payTermsconfig, String payTerms) {
		for(ConfigCategory config : payTermsconfig){
			if(config.getCategoryName().trim().equals(payTerms)){
				return config;
			}
		}
		return null;
	}



	private PersonInfo getPersonInfo(int customerId, List<Customer> customerlist) {
		// TODO Auto-generated method stub
		for(Customer customer : customerlist){
				if(customer.getCount() == customerId){
				PersonInfo personinfo = new PersonInfo();
				if(customer.isCompany()){
					personinfo.setFullName(customer.getCompanyName());
					personinfo.setPocName(customer.getFullname());
				}
				else{
					personinfo.setFullName(customer.getFullname());
					personinfo.setPocName(customer.getFullname());
				}
				personinfo.setCount(customer.getCount());
				personinfo.setCellNumber(customer.getCellNumber1());
				return personinfo;
			}
		}
		return null;
	}



	private ServiceProduct getserviceProduct(String productCode, List<ServiceProduct> serviceproductlist) {
		
		for(ServiceProduct product : serviceproductlist){
			if(productCode.trim().equals(product.getProductCode().trim())){
				return product;
			}
		}
		return null;
	}		


	
	private List<PaymentTerms> reactOnAddPayTerms(List<SalesLineItem> serProduct, String paymentTerms, ConfigCategory confi, Date startDate) 
	{
		List<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();
			ConfigCategory conCategory = confi;
			if (conCategory != null) {


				int days = Integer.parseInt(conCategory.getDescription());
				System.out.println("Payment terms Days::::::::::::::" + days);

				if (serProduct.size() == 0) {
					return null;
				}

				int duration = getDurationFromProductTable(serProduct);
				System.out.println("Duration::::::::: " + duration);

				if (duration == 0) {
					return null;
				}

				int noOfPayTerms = duration / days;
				System.out.println("No. of payment terms::::::::: "+ noOfPayTerms);

				if (noOfPayTerms == 0) {
					noOfPayTerms = 1;
				}

				Float no = (float) noOfPayTerms;
				
				System.out.println("No. Value::::::::: "+ no);
				
				float percent1 = 100 / no;
				System.out.println("Percent1::::::::: " + percent1);

				Double percent = (double) percent1;
				System.out.println("Percent2:::::::::  " + percent);

				int day = 0;
				double totalPer = 0;

					day = days;

				/**
				 * Date : 08-08-2017 BY ANIL
				 * 
				 */
					int monthCounter=0;
					if(days<30&&noOfPayTerms>1){
						monthCounter = 1;
					}
			
				
				if(days==30){
					monthCounter=1;
				}else if(days==60){
					monthCounter=2;
				}else if(days==90){
					monthCounter=3;
				}else if(days==120){
					monthCounter=4;
				}else if(days==150){
					monthCounter=5;
				}else if(days==180){
					monthCounter=6;
				}else if(days==210){
					monthCounter=7;
				}else if(days==240){
					monthCounter=8;
				}else if(days==270){
					monthCounter=9;
				}else if(days==300){
					monthCounter=10;
				}else if(days==330){
					monthCounter=11;
				}else if(days>360&&days<=366){
					monthCounter=12;
				}
				Date paymentDate=null;
				/**
				 * End
				 */
				if(startDate!=null){
					paymentDate = (Date) startDate.clone();
				}
				
				System.out.println("month counter "+monthCounter);
				for (int i = 1; i <= noOfPayTerms; i++) {
					Date paymentDt= (Date) paymentDate.clone();
					
					
					totalPer = totalPer + percent;
					
					if (i == noOfPayTerms) {
						if (totalPer == 100) {
							PaymentTerms pterm = new PaymentTerms();
							pterm.setPayTermDays(day);
							pterm.setPayTermPercent(percent);
							pterm.setPayTermComment(i + " Payment");
							logger.log(Level.SEVERE," Get in total 100 condition -- " + paymentDt);
							ptermsList.add(pterm);

						} else {
							double diff = 0;
							if (totalPer < 100) {
								diff = 100 - totalPer;
								percent = percent + diff;
								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(paymentDt != null){
//									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							} else {
								diff = totalPer - 100;
								percent = percent - diff;
								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(paymentDt!=null){
//									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							}
						}

					} else {
						PaymentTerms pterm = new PaymentTerms();
						pterm.setPayTermDays(day);
						pterm.setPayTermPercent(percent);
						pterm.setPayTermComment(i + " Payment");
						/**
						 * Date : 08-08-2017 BY ANIL
						 */
						if(paymentDt!=null){
//							pterm.setPaymentDate(paymentDt);
						}
						/**
						 * End
						 */
						
						
						ptermsList.add(pterm);
					}
					day = day + days;
					
					if(paymentDate!=null){
						System.out.println(" BF DATE :: "+paymentDate);
						int month=paymentDate.getMonth();
						month=month+monthCounter;
						paymentDate.setMonth(month);
						System.out.println(" AF DATE :: "+paymentDate);
					}
				}
				
				
			}
			return ptermsList;
		}



	private int getDurationFromProductTable(List<SalesLineItem> serProduct) {
		int maxDur = 0;
		int currDur = 0;
		for (int i = 0; i < serProduct.size(); i++) {
			currDur = serProduct.get(i).getDuration();
			System.out.println(" curruent dur" + currDur);
			if (maxDur <= currDur) {
				maxDur = currDur;
			}
		}
	
		return maxDur;
	}
	
	
	public ArrayList<ServiceSchedule> createServiceList(List<SalesLineItem> salesitemlis, List<CustomerBranchDetails> globalCustomerBranchDetailsList, long companyId, boolean complainServiceWithTurnAroundTimeFlag){
		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		
		boolean serviceScheduleMinMaxRoundOffFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", AppConstants.PC_SERVICESCHEDULEMINMAXROUNDUP, companyId);

			for(int i=0;i<salesitemlis.size();i++){	
				ArrayList<BranchWiseScheduling> branchSchedulingList = salesitemlis.get(i).getCustomerBranchSchedulingInfo().get(salesitemlis.get(i).getProductSrNo());
				boolean  branchSchFlag = false;
				int qty=0;
				if(branchSchedulingList!=null&&branchSchedulingList.size()!=0){
					branchSchFlag=true;
					qty=branchSchedulingList.size();
				}else{
					qty=(int) salesitemlis.get(i).getQty();
				}
				System.out.println("Qty ==="+qty);
				if(branchSchFlag==true){
					for(int k=0;k < qty;k++){
						System.out.println("Inside branch ===");
					 	if(branchSchedulingList.get(k).isCheck()==true){
						long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
						int noOfdays=salesitemlis.get(i).getDuration();
						int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
						/**
						 * @author Vijay Date 11-10-2021
						 *  Des :- Service Scheduling with new Min Max round up logic for innovative
						 */
						if(serviceScheduleMinMaxRoundOffFlag){
							ServerAppUtility serverappUtility = new ServerAppUtility();
							double noofDays = salesitemlis.get(i).getDuration();
							double noOfservices = (salesitemlis.get(i).getNumberOfServices());
							double dbinterval= noofDays/noOfservices;

							interval = serverappUtility.getValueWithMinMaxRoundUp(dbinterval);
						}
						/**
						 * ends here
						 */
						Date servicedate=(salesitemlis.get(i).getStartDate());
						Date d=new Date(servicedate.getTime());
						
						Date productEndDate= new Date(servicedate.getTime());
						Calendar cal1 = Calendar.getInstance();
						cal1.setTime(DateUtility.getDateWithTimeZone("IST", productEndDate));
						cal1.add(Calendar.DATE, noOfdays-1); 
						
						Date prodenddate=cal1.getTime();
						productEndDate=prodenddate;
						System.out.println("productEndDate =="+productEndDate);
						Date tempdate=new Date();
						for(int j=0;j<noServices;j++){
							System.out.println("No of services");
							if(j>0)
							{
								Calendar cal2 = Calendar.getInstance();
								cal2.setTime(d);
								cal2.add(Calendar.DATE, interval); 
								System.out.println("interval =="+interval);
								d = cal2.getTime();
								System.out.println("D =="+d);
								tempdate=d;
								System.out.println("tempdate"+tempdate);

							}
							
							ServiceSchedule ssch=new ServiceSchedule();
							Date Stardaate=servicedate;
							ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
							ssch.setScheduleStartDate(DateUtility.getDateWithTimeZone("IST", Stardaate));
							ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
							System.out.println("Product Id"+salesitemlis.get(i).getPrduct().getCount());
							ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
							ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
							ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
							ssch.setScheduleServiceNo(j+1);
							ssch.setScheduleProdStartDate(DateUtility.getDateWithTimeZone("IST", Stardaate));
							ssch.setScheduleProdEndDate(DateUtility.getDateWithTimeZone("IST", productEndDate));
							ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
							ssch.setScheduleServiceTime("Flexible");
							if(j==0){
								cal1.setTime(DateUtility.getDateWithTimeZone("IST", servicedate));
								int week = cal1.get(Calendar.WEEK_OF_MONTH);
								ssch.setWeekNo(week);
								ssch.setScheduleServiceDate(servicedate);
							}
							if(j!=0){
								if(productEndDate.before(d)==false){
									cal1.setTime(DateUtility.getDateWithTimeZone("IST", tempdate));
									int week = cal1.get(Calendar.WEEK_OF_MONTH);
									ssch.setWeekNo(week);
									ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST", tempdate));
								}else{
									cal1.setTime(DateUtility.getDateWithTimeZone("IST", productEndDate));
									int week = cal1.get(Calendar.WEEK_OF_MONTH);
									ssch.setWeekNo(week);
									ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST", productEndDate));
								}
							}
							ssch.setScheduleProBranch(branchSchedulingList.get(k).getBranchName());
							ssch.setServicingBranch(getCustomerBranchServingBrnach(branchSchedulingList.get(k).getBranchName(),globalCustomerBranchDetailsList));
							SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
							String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (ssch.getScheduleServiceDate())));
							logger.log(Level.SEVERE,"get service Day --" +serviceDay);
							ssch.setScheduleServiceDay(serviceDay);
							ssch.setCompanyId(companyId);
							
						serschelist.add(ssch);
						
						
						if(complainServiceWithTurnAroundTimeFlag){
							ServerAppUtility apputility = new ServerAppUtility();
							List<ServiceSchedule>list = apputility.updateSchedulingListComponentWise(ssch, branchSchedulingList.get(k));
							if(list!=null&&list.size()!=0){
								serschelist.addAll(list);
							}
						}
						
						}
						
					}
					}
				}else{
				System.out.println("Without branch");
				for(int k=0;k < qty;k++){
				
				 	
				long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
				int noOfdays=salesitemlis.get(i).getDuration();
				int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
				
				Date servicedate=(salesitemlis.get(i).getStartDate());
				Date d=new Date(servicedate.getTime());
				
				Date productEndDate= new Date(servicedate.getTime());
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(productEndDate);
				cal1.add(Calendar.DATE, noOfdays); 
				
				Date prodenddate=new Date(productEndDate.getTime());
				productEndDate=prodenddate;
				Date tempdate=new Date();
				
				for(int j=0;j<noServices;j++){
					if(j>0)
					{
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(d);
						cal2.add(Calendar.DATE, interval); 
						tempdate=new Date(d.getTime());
					}
					
					ServiceSchedule ssch=new ServiceSchedule();
					Date Stardaate=servicedate;
					ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
					ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
					ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
					ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
					ssch.setScheduleServiceNo(j+1);
					ssch.setScheduleProdStartDate(Stardaate);
					ssch.setScheduleProdEndDate(productEndDate);
					ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
					ssch.setScheduleServiceTime("Flexible");
					if(j==0){
						ssch.setScheduleServiceDate(servicedate);
					}
					if(j!=0){
						if(productEndDate.before(d)==false){
							ssch.setScheduleServiceDate(tempdate);
						}else{
							ssch.setScheduleServiceDate(productEndDate);
						}
					}
					
					SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
					String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (ssch.getScheduleServiceDate())));
					logger.log(Level.SEVERE,"get service Day --" +serviceDay);
					ssch.setScheduleServiceDay(serviceDay);
					
					/**
					 * Date 01-04-2017 added by vijay for week number
					 */
					int weaknumber = AppUtility.getweeknumber(ssch.getScheduleServiceDate());
					ssch.setWeekNo(weaknumber);
					
					/**
					 * ends here
					 */
					serschelist.add(ssch);
				}
				}
			}
				
			}	
		return serschelist;
		}



	private String getCustomerBranchServingBrnach(String branchName,
			List<CustomerBranchDetails> globalCustomerBranchDetailsList) {
//		logger.log(Level.SEVERE, "customerBranch"+branchName);
//		logger.log(Level.SEVERE, "globalCustomerBranchDetailsList =="+globalCustomerBranchDetailsList.size());
		for(CustomerBranchDetails customerBranch : globalCustomerBranchDetailsList){
			if(customerBranch.getBusinessUnitName().trim().equals(branchName.trim())){
//				logger.log(Level.SEVERE, "servicing Branch"+customerBranch.getBranch());
				return customerBranch.getBranch();
			}
		}
//		logger.log(Level.SEVERE, "Returning Null");
		return null;
	}

	/**
	 * Date 21-11-2019 by Vijay
	 * Des :- NBHC Inventory Management GRN Upload for lock seal
	 */
	private void reactonGRN(long companyId, ArrayList<String> exceldatalist) {
		ItemProduct product = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productCode", exceldatalist.get(13)).first().now();
		logger.log(Level.SEVERE, "lock seal product "+product.getProductName());
		System.out.println("exceldatalist =="+exceldatalist.size());
		if(product!=null){
			for(int i=10;i<exceldatalist.size();i+=10){
				try {
					Thread.sleep(5000);
				
				GRN grnEntity = new GRN();
				try {
					Date documentDate = fmt.parse(exceldatalist.get(i));
					grnEntity.setGrnCreationDate(documentDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				grnEntity.setTitle("");
				grnEntity.setBranch(exceldatalist.get(i+1));
				grnEntity.setEmployee(exceldatalist.get(i+2));
				grnEntity.setApproverName(exceldatalist.get(i+2));
				grnEntity.setStatus(GRN.APPROVED);
				grnEntity.setInspectionRequired(AppConstants.NO);
				ArrayList<GRNDetails> grnlist = new ArrayList<GRNDetails>();	
				GRNDetails grn = new GRNDetails();
				grn.setProductID(product.getCount());
				grn.setProductCode(product.getProductCode());
				grn.setProductCategory(product.getProductCategory());
				grn.setProductName(product.getProductName());
				grn.setUnitOfmeasurement(product.getUnitOfMeasurement());
				double qty = Double.parseDouble(exceldatalist.get(i+4));
				grn.setProductQuantity(qty);
				grn.setProdPrice(product.getPrice());
				grn.setTotal(product.getPrice()*qty);
				grn.setDiscount(0);
				grn.setWarehouseLocation(exceldatalist.get(i+7));
				grn.setWarehouseName(exceldatalist.get(i+7));
				grn.setStorageLoc(exceldatalist.get(i+8));
				grn.setStorageBin(exceldatalist.get(i+9));
				grn.setPrduct(product);
				grn.setPurchaseTax1(product.getPurchaseTax1());
				grn.setPurchaseTax2(product.getPurchaseTax2());
				HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
				serialNoMappinglist = getProductSerialNoMapping(exceldatalist.get(i+5),exceldatalist.get(i+6));
				prserdt.put(0, serialNoMappinglist);
				grn.setProSerialNoDetails(prserdt);
				grnlist.add(grn);
				grnEntity.setInventoryProductItem(grnlist);
				grnEntity.setCompanyId(companyId);
				GenricServiceImpl impl=new GenricServiceImpl();
				ReturnFromServer server = new ReturnFromServer();
				server = impl.save(grnEntity);
				int grnId = server.count;
				logger.log(Level.SEVERE,"GRN Saved Successfully GRN ID"+grnId);
				grnEntity.reactOnApproval();
//				Approvals model = sendApprovalRequest(grnId,exceldatalist.get(i+2),companyId,grnEntity);
//				if(model!=null){
//					String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
//					Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//					queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
//					logger.log(Level.SEVERE,"GRN Saved Successfully");
//				}
				
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		}
		
	}
	
	private ArrayList<ProductSerialNoMapping> getProductSerialNoMapping(String startSerialNo, String endSerialNo) {
		ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
			ProductSerialNoMapping serialNoMapping = new ProductSerialNoMapping();
			serialNoMapping.setProSerialNo(startSerialNo+"-"+endSerialNo);		
			serialNoMapping.setStatus(true);
			serialNoMappinglist.add(serialNoMapping);
		return serialNoMappinglist;
		
	}
	
	private Approvals sendApprovalRequest(int count, String username, long companyId, GRN grn) {

		logger.log(Level.SEVERE, "Approval Sending == MIN =="+count);
		Approvals approval = new Approvals();
		try {
		approval.setApproverName(username);
		approval.setBranchname(grn.getBranch());
		approval.setBusinessprocessId(count);
		if(username!=null){
		approval.setRequestedBy(username);
		}
		if(grn.getEmployee()!=null)
		approval.setPersonResponsible(grn.getEmployee());
		if(grn.getCreatedBy()!=null){
			approval.setDocumentCreatedBy(grn.getCreatedBy());
		}
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.APPROVED);
		approval.setBusinessprocesstype("GRN");
		approval.setDocumentValidation(false);
		approval.setCompanyId(companyId);
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(approval);
		logger.log(Level.SEVERE, "Auto Invoice Approval Sent successfully");
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		return approval;
	}
	
	private void reactonMMN(long companyId, ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "exceldatalist.get(16) "+exceldatalist.get(16));
		ItemProduct product = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productCode", exceldatalist.get(16)).first().now();
		logger.log(Level.SEVERE, "Lock Seal product"+product.getProductName());
		if(product!=null){
			for(int i=13;i<exceldatalist.size();i+=13){
				try {
					Thread.sleep(5000);
				MaterialMovementNote mmn = new MaterialMovementNote();
				mmn.setCompanyId(companyId);
				mmn.setStatus(MaterialMovementNote.APPROVED);
				mmn.setCreationDate(new Date());
				Date documentDate;
				try {
					documentDate = fmt.parse(exceldatalist.get(i));
					mmn.setMmnDate(documentDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				mmn.setBranch(exceldatalist.get(i+1));
				mmn.setEmployee(exceldatalist.get(i+2));
				mmn.setApproverName(exceldatalist.get(i+2));
				mmn.setMmnTransactionType("TransferOUT");
				mmn.setTransDirection("TRANSFEROUT");
				ArrayList<MaterialProduct> materiallist = new ArrayList<MaterialProduct>();
				mmn.setTransToWareHouse(exceldatalist.get(i+10));
				mmn.setTransToStorLoc(exceldatalist.get(i+11));
				mmn.setTransToStorBin(exceldatalist.get(i+12));
				MaterialProduct materialProd = new MaterialProduct();
				materialProd.setMaterialProductId(product.getCount());
				materialProd.setMaterialProductCode(product.getProductCode());
				materialProd.setMaterialProductName(product.getProductName());
				double qty = Double.parseDouble(exceldatalist.get(i+4));
				materialProd.setMaterialProductRequiredQuantity(qty);
				materialProd.setMaterialProductRemarks("");
				materialProd.setMaterialProductUOM(product.getUnitOfMeasurement());
				materialProd.setMaterialProductWarehouse(exceldatalist.get(i+7));
				materialProd.setMaterialProductStorageLocation(exceldatalist.get(i+8));
				materialProd.setMaterialProductStorageBin(exceldatalist.get(i+9));
				HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
				serialNoMappinglist = getProductSerialNoMapping(exceldatalist.get(i+5),exceldatalist.get(i+6));
				prserdt.put(0, serialNoMappinglist);
				materialProd.setProSerialNoDetails(prserdt);
				
				materiallist.add(materialProd);
				mmn.setSubProductTableMmn(materiallist);
					
				GenricServiceImpl genimpl = new GenricServiceImpl();
				ReturnFromServer server = new ReturnFromServer();
				server = genimpl.save(mmn);
				
				logger.log(Level.SEVERE,"MMN Saved Successfully MMN ID"+server.count);
				mmn.reactOnApproval();
				
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
//				Approvals model = sendApprovalRequest(server.count,exceldatalist.get(i+2),companyId,mmn);
//				if(model!=null){
//					String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
//					Queue queue = QueueFactory.getQueue("documentCancellation-queue");
//					queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
//					logger.log(Level.SEVERE,"MMN Approval process sent to task queue Successfully");
//				}
				
			}
		}
	}




	private Approvals sendApprovalRequest(int count, String username, long companyId, MaterialMovementNote mmn) {
		logger.log(Level.SEVERE, "Approval Sending == MMN =="+count);
		Approvals approval = new Approvals();
		try {
		approval.setApproverName(username);
		approval.setBranchname(mmn.getBranch());
		approval.setBusinessprocessId(count);
		if(username!=null){
		approval.setRequestedBy(username);
		}
		if(mmn.getEmployee()!=null)
		approval.setPersonResponsible(mmn.getEmployee());
		if(mmn.getCreatedBy()!=null){
			approval.setDocumentCreatedBy(mmn.getCreatedBy());
		}
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.APPROVED);
		approval.setBusinessprocesstype("MMN");
		approval.setDocumentValidation(false);
		approval.setCompanyId(companyId);
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(approval);
		logger.log(Level.SEVERE, " Approval Sent successfully");
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		return approval;
	
	}

	/**
	 * Date 27-11-2019
	 * Des :- NBHC Lock Seal IM Stock update directly
	 * @param exceldatalist 
	 * @param companyId  

	 */
	private void reactonLockSealStockUpload(long companyId, ArrayList<String> exceldatalist) {
		ItemProduct product = ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productCode", exceldatalist.get(11)).first().now();
		logger.log(Level.SEVERE, "product"+product);
		DecimalFormat df=new DecimalFormat("0.0000");
		try {
			
		
		if(product!=null){
			for(int i=11;i<exceldatalist.size();i+=11){
				if(!exceldatalist.get(i+10).equalsIgnoreCase("NA") && exceldatalist.get(i+10).equalsIgnoreCase("yes")){ // Directly adding to warehouses stock this is new Stock master not exist in the system
					SerialNumberStockMaster serialNumberMaster = new SerialNumberStockMaster();
					serialNumberMaster.setProductId(product.getCount());
					serialNumberMaster.setProductCode(product.getProductCode());
					serialNumberMaster.setProductName(product.getProductName());
					serialNumberMaster.setCompanyId(companyId);
					String serialNumber =exceldatalist.get(i+2)+"-"+exceldatalist.get(i+3);
					serialNumberMaster.setWarehouseName(exceldatalist.get(i+7));
					serialNumberMaster.setStorageLocation(exceldatalist.get(i+8));
					serialNumberMaster.setStorageBin(exceldatalist.get(i+9));

					if(serialNumber!=null && !serialNumber.equals("")){
						String arrayserialNumber[] = serialNumber.split("-");
						String startNumber = arrayserialNumber[0].trim();
						String endNumber = arrayserialNumber[1].trim();
						long startSerialNumber = Long.parseLong(startNumber);
						long endSerialNumber = Long.parseLong(endNumber);
						ArrayList<SerialNumberMapping> serailNumberlist = new ArrayList<SerialNumberMapping>();

						for(long j=startSerialNumber;j<=endSerialNumber;j++){
							SerialNumberMapping serialNumberMapping = new SerialNumberMapping();
							serialNumberMapping.setStatus(true);
							serialNumberMapping.setSerialNumber(j);
							serailNumberlist.add(serialNumberMapping);
						}
						serialNumberMaster.setSerailNumberlist(serailNumberlist);
						serialNumberMaster.setStartSerialNumber(startSerialNumber);
						serialNumberMaster.setEndSerialNumber(endSerialNumber);
					}
					serialNumberMaster.setStatus(true);
					serialNumberMaster.setSerialNumberRange(serialNumber);
					
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(serialNumberMaster);
					logger.log(Level.SEVERE, "Serial Number Stock Master updated successfully");
					
					
					ProductInventoryViewDetails prodinvenoryViewDetails = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId)
							.filter("prodcode", exceldatalist.get(i)).filter("warehousename", exceldatalist.get(i+7))
							.filter("storagelocation", exceldatalist.get(i+8))
							.filter("storagebin", exceldatalist.get(i+9)).first().now();
					logger.log(Level.SEVERE, "prodinvenoryViewDetails ="+prodinvenoryViewDetails);
					if(prodinvenoryViewDetails==null){
						double closingStock = Double.parseDouble(exceldatalist.get(i+1));
						ProductInventoryViewDetails pvdDetails = new ProductInventoryViewDetails();
						pvdDetails.setAvailableqty(Double.parseDouble(df.format(closingStock)));
						WareHouse warehouseEntity = ofy().load().type(WareHouse.class)
								.filter("buisnessUnitName", serialNumberMaster.getWarehouseName()).filter("companyId",companyId)
								.first().now();
						if (warehouseEntity != null && warehouseEntity.getWarehouseType() != null) {
							pvdDetails.setWarehouseType(warehouseEntity.getWarehouseType());
						}
						pvdDetails.setCompanyId(companyId);
						pvdDetails.setPrice(product.getPrice());
						pvdDetails.setProdcode(product.getProductCode());
						pvdDetails.setProdid(product.getCount());
						pvdDetails.setProdname(product.getProductName());
						if(product.getProductCategory()!=null)
						pvdDetails.setProductCategory(product.getProductCategory());
						pvdDetails.setWarehousename(exceldatalist.get(i+7));
						pvdDetails.setStoragelocation(exceldatalist.get(i+8));
						pvdDetails.setStoragebin(exceldatalist.get(i+9));
						
//						HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
//						ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
//						serialNoMappinglist = getProductSerialNoMapping(exceldatalist.get(i+2),exceldatalist.get(i+3));
//						prserdt.put(0, serialNoMappinglist);
//						pvdDetails.setProSerialNoDetails(prserdt);
						
						GenricServiceImpl implnew = new GenricServiceImpl();
						implnew.save(pvdDetails);
						logger.log(Level.SEVERE, "Stock Updated successfully");
					}
					else{
						double availabletQty = prodinvenoryViewDetails.getAvailableqty();
						double additionQty = Double.parseDouble(exceldatalist.get(i+1));
						double closingStock = availabletQty+additionQty;
						prodinvenoryViewDetails.setAvailableqty(Double.parseDouble(df.format(closingStock)));
						ofy().save().entities(prodinvenoryViewDetails);
						logger.log(Level.SEVERE, "Stock Updated successfully");
					}
					
					
				}
				else{
					
					ProductInventoryViewDetails pvdDetails = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId)
															.filter("prodcode", exceldatalist.get(i)).filter("warehousename", exceldatalist.get(i+4))
															.filter("storagelocation", exceldatalist.get(i+5))
															.filter("storagebin", exceldatalist.get(i+6)).first().now();
					logger.log(Level.SEVERE, "pvdDetails "+pvdDetails);
					if(pvdDetails!=null){
						String serialNumber =exceldatalist.get(i+2)+"-"+exceldatalist.get(i+3);
						double openingStock = pvdDetails.getAvailableqty();
						double qty = Double.parseDouble(exceldatalist.get(i+1));
						double closingStock = openingStock-qty;
						pvdDetails.setAvailableqty(Double.parseDouble(df.format(closingStock)));
						ofy().save().entities(pvdDetails);
						logger.log(Level.SEVERE, "Transfer out qty updated in warehouse stock master");

						SerialNumberStockMaster serialStockMaster = ofy().load().type(SerialNumberStockMaster.class)
								.filter("warehouseName", pvdDetails.getWarehousename())
								.filter("storageLocation", pvdDetails.getStoragelocation())
								.filter("storageBin", pvdDetails.getStoragebin())
								.filter("productCode", pvdDetails.getProdcode())
								.filter("companyId", pvdDetails.getCompanyId())
								.filter("serialNumberRange", serialNumber)
								.filter("status", true).first().now();
						if(serialStockMaster!=null){
							serialStockMaster.setStatus(false);
							for(SerialNumberMapping serialNumberMapping : serialStockMaster.getSerailNumberlist()){
								serialNumberMapping.setStatus(false);
							}
							ofy().save().entity(serialStockMaster);
							Logger logger = Logger.getLogger("logger");
							logger.log(Level.SEVERE, "Stock master Updated Sucessfully for transfer Out");
						}
						
						
//						ProductInventoryViewDetails pvdDetailstransferIn = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId)
//								.filter("prodcode", exceldatalist.get(i)).filter("warehousename", exceldatalist.get(i+7))
//								.filter("storagelocation", exceldatalist.get(i+8))
//								.filter("storagebin", exceldatalist.get(i+9)).first().now();
//						logger.log(Level.SEVERE, "pvdDetailstransferIn "+pvdDetailstransferIn);
//						if(pvdDetailstransferIn==null){
							SerialNumberStockMaster serialNumberMaster = new SerialNumberStockMaster();
							serialNumberMaster.setProductId(product.getCount());
							serialNumberMaster.setProductCode(product.getProductCode());
							serialNumberMaster.setProductName(product.getProductName());
							serialNumberMaster.setCompanyId(companyId);
//							String serialNumber =exceldatalist.get(i+2)+"-"+exceldatalist.get(i+3);
							serialNumberMaster.setWarehouseName(exceldatalist.get(i+7));
							serialNumberMaster.setStorageLocation(exceldatalist.get(i+8));
							serialNumberMaster.setStorageBin(exceldatalist.get(i+9));

							if(serialNumber!=null && !serialNumber.equals("")){
								String arrayserialNumber[] = serialNumber.split("-");
								String startNumber = arrayserialNumber[0].trim();
								String endNumber = arrayserialNumber[1].trim();
								long startSerialNumber = Long.parseLong(startNumber);
								long endSerialNumber = Long.parseLong(endNumber);
								ArrayList<SerialNumberMapping> serailNumberlist = new ArrayList<SerialNumberMapping>();

								for(long j=startSerialNumber;j<=endSerialNumber;j++){
									SerialNumberMapping serialNumberMapping = new SerialNumberMapping();
									serialNumberMapping.setStatus(true);
									serialNumberMapping.setSerialNumber(j);
									serailNumberlist.add(serialNumberMapping);
								}
								serialNumberMaster.setSerailNumberlist(serailNumberlist);
								serialNumberMaster.setStartSerialNumber(startSerialNumber);
								serialNumberMaster.setEndSerialNumber(endSerialNumber);
							}
							serialNumberMaster.setStatus(true);
							serialNumberMaster.setSerialNumberRange(serialNumber);
							
							GenricServiceImpl impl = new GenricServiceImpl();
							impl.save(serialNumberMaster);
							logger.log(Level.SEVERE, "Serial Number Stock Master updated successfully for Transfer IN");
							
							
							
							ProductInventoryViewDetails prodinvenoryViewDetails = ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId)
									.filter("prodcode", exceldatalist.get(i)).filter("warehousename", exceldatalist.get(i+7))
									.filter("storagelocation", exceldatalist.get(i+8))
									.filter("storagebin", exceldatalist.get(i+9)).first().now();
							logger.log(Level.SEVERE, "prodinvenoryViewDetails ="+prodinvenoryViewDetails);
							if(prodinvenoryViewDetails==null){
								
								double closingStocknew = Double.parseDouble(exceldatalist.get(i+1));
								ProductInventoryViewDetails productinventoryViewdDetails = new ProductInventoryViewDetails();
								productinventoryViewdDetails.setAvailableqty(Double.parseDouble(df.format(closingStocknew)));
								WareHouse warehouseEntity = ofy().load().type(WareHouse.class)
										.filter("buisnessUnitName", serialNumberMaster.getWarehouseName()).filter("companyId",companyId)
										.first().now();
								if (warehouseEntity != null && warehouseEntity.getWarehouseType() != null) {
									productinventoryViewdDetails.setWarehouseType(warehouseEntity.getWarehouseType());
								}
								productinventoryViewdDetails.setCompanyId(companyId);
								productinventoryViewdDetails.setPrice(product.getPrice());
								productinventoryViewdDetails.setProdcode(product.getProductCode());
								productinventoryViewdDetails.setProdid(product.getCount());
								productinventoryViewdDetails.setProdname(product.getProductName());
								if(product.getProductCategory()!=null)
									productinventoryViewdDetails.setProductCategory(product.getProductCategory());
								productinventoryViewdDetails.setWarehousename(exceldatalist.get(i+7));
								productinventoryViewdDetails.setStoragelocation(exceldatalist.get(i+8));
								productinventoryViewdDetails.setStoragebin(exceldatalist.get(i+9));
								
//								HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
//								ArrayList<ProductSerialNoMapping> serialNoMappinglist = new ArrayList<ProductSerialNoMapping>();
//								serialNoMappinglist = getProductSerialNoMapping(exceldatalist.get(i+2),exceldatalist.get(i+3));
//								prserdt.put(0, serialNoMappinglist);
//								productinventoryViewdDetails.setProSerialNoDetails(prserdt);
								
								GenricServiceImpl implnew = new GenricServiceImpl();
								implnew.save(productinventoryViewdDetails);
								logger.log(Level.SEVERE, "warehouse Stock  master Updated successfully for transfer in");
							}
							else{
								double availabletQty = prodinvenoryViewDetails.getAvailableqty();
								double additionQty = Double.parseDouble(exceldatalist.get(i+1));
								double newclosingStock = availabletQty+additionQty;
								prodinvenoryViewDetails.setAvailableqty(Double.parseDouble(df.format(newclosingStock)));
								ofy().save().entities(prodinvenoryViewDetails);
								logger.log(Level.SEVERE, "Stock Updated successfully");
							}
							
//						}

					}
					
				}
			}
		}
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception Raised here =="+e.getMessage());
		}
	
	}


   
   	private void reactOnPurchaseRequisitionUpdateUpload(String priceFlag, long companyId,
			ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
	try{
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		System.out.println("Done here");
		this.prequisitionUploadBlobKey=null;
		
		ArrayList<String> temp =new  ArrayList<String>();
		ArrayList<PurchaseRequisition> prequisitionlist = new ArrayList<PurchaseRequisition>();
		System.out.println(exceldatalist.size());
		
		HashSet<String> productCodeforPrice=new HashSet<String>();
		
		for(int i=17;i<exceldatalist.size();i+=17){
			productCodeforPrice.add(exceldatalist.get(i+10));
			System.out.println("Product Code "+exceldatalist.get(i+10));
		}
		
		System.out.println("productCodeforPrice size "+productCodeforPrice.size());
		
		ArrayList<String> productCodeforPriceArrayList=new ArrayList<String>(productCodeforPrice);
		
		System.out.println(" Super Product size for price "+productCodeforPriceArrayList);
		
		List<SuperProduct> superProductCodeforPrice=ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("productCode IN", productCodeforPriceArrayList).list();
		
		System.out.println(" Super Product List size for price "+superProductCodeforPrice);
		
		for(int i=17;i<exceldatalist.size();i+=17){
			
			PurchaseRequisition prequisitionEntity = new PurchaseRequisition();
			ProductDetails product_details=new ProductDetails();
					
			if(!exceldatalist.get(i).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setPrTitle(exceldatalist.get(i));
				System.out.println(exceldatalist.get(i));
			}
			if(!exceldatalist.get(i+1).trim().equalsIgnoreCase("NA")){
				try {
//					Date date=dateFormat.parse(exceldatalist.get(i+1).trim());
//					dateFormat.format(date);
//					System.out.println(" Date into Uploading :- "+date);
//					prequisitionEntity.setExpectedDeliveryDate(dateFormat.parse(exceldatalist.get(i+1)));
					logger.log(Level.SEVERE," Excel Value  "+exceldatalist.get(i+1));
					Date prexpectedDate = dateFormat.parse(exceldatalist.get(i+1));
					logger.log(Level.SEVERE," prexpectedDate "+prexpectedDate);
//					System.out.println("prexpectedDate"+prexpectedDate);
					String strDate = dateFormat.format(prexpectedDate);
					logger.log(Level.SEVERE," strDate "+strDate);
//					System.out.println("strDate"+strDate);
					Date prExpectedDate = dateFormat.parse(strDate);
//					System.out.println("prDate"+prExpectedDate);
					logger.log(Level.SEVERE," prExpectedDate "+prExpectedDate);

//					System.out.println(prequisitionEntity.getExpectedDeliveryDate()+" / "+exceldatalist.get(i+1));
//					logger.log(Level.SEVERE,"Excel Date Received "+exceldatalist.get(i+1)+" / "+" Set Date in Object "+prequisitionEntity.getExpectedDeliveryDate());
					prequisitionEntity.setExpectedDeliveryDate(prExpectedDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setPrGroup(exceldatalist.get(i+2));
				System.out.println(exceldatalist.get(i+2));
			}
			if(!exceldatalist.get(i+3).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setPrCategory(exceldatalist.get(i+3));
				System.out.println(exceldatalist.get(i+3));
			}
			if(!exceldatalist.get(i+4).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setPrType(exceldatalist.get(i+4));
				System.out.println(exceldatalist.get(i+4));
			}
			if(!exceldatalist.get(i+5).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setBranch(exceldatalist.get(i+5));
				System.out.println(exceldatalist.get(i+5));
			}
			if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setEmployee(exceldatalist.get(i+6));
				System.out.println(exceldatalist.get(i+6));
			}
			if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setApproverName(exceldatalist.get(i+7));
				System.out.println(exceldatalist.get(i+7));
			}
			if(!exceldatalist.get(i+8).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setDescription(exceldatalist.get(i+8));
				System.out.println(exceldatalist.get(i+8));
			}else{
				prequisitionEntity.setDescription("");
			}
			if(!exceldatalist.get(i+9).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setDescription2(exceldatalist.get(i+9));
				System.out.println(exceldatalist.get(i+9));
			}else{
				prequisitionEntity.setDescription2("");
			}
			ItemProduct pitem=new ItemProduct();
			product_details.setPrduct(pitem);
               	   
			if(!exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")){
				product_details.setProductCode(exceldatalist.get(i+10));
				System.out.println(exceldatalist.get(i+10));
			}
			if(priceFlag.equals("true")){
				product_details.setProdPrice(getProductPrice(superProductCodeforPrice,exceldatalist.get(i+10)));
				logger.log(Level.SEVERE," setProdPrice "+product_details.getProdPrice());
//				product_details.setProdPrice(Double.valueOf(exceldatalist.get(i+11)));
//				System.out.println(exceldatalist.get(i+11));
			}else{
				product_details.setProdPrice(Double.valueOf(exceldatalist.get(i+11)));
			}
			if(!exceldatalist.get(i+12).trim().equalsIgnoreCase("NA")){
				product_details.setProductQuantity(Double.valueOf(exceldatalist.get(i+12)));
				System.out.println(exceldatalist.get(i+12));
			}
			/***Deepak Salve remove this code for Repeated value***/
//	        	if(!exceldatalist.get(i+13).trim().equalsIgnoreCase("NA")){
//				product_details.setProdDate(dateFormat.parse(exceldatalist.get(i+13)));
//	        		System.out.println(exceldatalist.get(i+13));
//			}
			/***End***/
	        if(!exceldatalist.get(i+13).trim().equalsIgnoreCase("NA")){
					product_details.setWarehouseName(exceldatalist.get(i+13));
					System.out.println(exceldatalist.get(i+13));
			}
			if(!exceldatalist.get(i+14).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setHeader(exceldatalist.get(i+14));
				System.out.println(exceldatalist.get(i+14));
			}else{
				prequisitionEntity.setHeader("");
			}
			if(!exceldatalist.get(i+15).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setFooter(exceldatalist.get(i+15));
				System.out.println(exceldatalist.get(i+15));
			}else{
				prequisitionEntity.setFooter("");
			}
			if(!exceldatalist.get(i+16).trim().equalsIgnoreCase("NA")){
				prequisitionEntity.setRemark(exceldatalist.get(i+16));
				System.out.println(exceldatalist.get(i+16));
			}	
			prequisitionEntity.getPrproduct().add(product_details);
			prequisitionlist.add(prequisitionEntity);
	    }
		
		HashSet<String> warehouselist=new HashSet<String>();
		HashSet<String> pcode=new HashSet<String>();
		HashSet<String> itemList=new HashSet<String>();
		HashMap<String, ArrayList<PurchaseRequisition>> prequsitionHashMap = new HashMap<String, ArrayList<PurchaseRequisition>>();
		System.out.println("P requisition Value"+prequisitionlist);
		
		try{
			for(PurchaseRequisition prdata: prequisitionlist ){
				
				pcode.add(prdata.getRemark());
				warehouselist.add(prdata.getPrproduct().get(0).getWarehouseName());
			    itemList.add(prdata.getPrproduct().get(0).getProductCode());
			    
			    System.out.println("Product Code into Uploading "+prdata.getPrproduct().get(0).getProductCode());
				
				if(prequsitionHashMap!=null && prequsitionHashMap.size()!=0){
					if(prequsitionHashMap.containsKey(prdata.getRemark())){
						prequsitionHashMap.get(prdata.getRemark()).add(prdata);					
					}else{
						ArrayList<PurchaseRequisition> list = new ArrayList<PurchaseRequisition>();
						list.add(prdata);
						prequsitionHashMap.put(prdata.getRemark(), list);
					}
				}else{
					ArrayList<PurchaseRequisition> list = new ArrayList<PurchaseRequisition>();
					list.add(prdata);
					prequsitionHashMap.put(prdata.getRemark(), list);
				}
			}
		}catch(Exception e){System.out.println("Null Pointer Exception"+e);}
		
		if(itemList.isEmpty())
		{
			System.out.println("Item List Is Empty");
		}
		ArrayList<String> listItem=new ArrayList<String>(itemList);
		System.out.println("warehouselist Size"+warehouselist.size());
		System.out.println("warehouseData"+warehouselist);
		ArrayList<String> listwarehouse=new ArrayList<String>(warehouselist);
		System.out.println("Item list value "+listItem);
		List<SuperProduct> itemproductlist = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("productCode IN", listItem).list();
		List<WareHouse> warehousedatalist = ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("buisnessUnitName IN", listwarehouse).list();
		System.out.println("warehouselist Query Size"+warehousedatalist.size());
		System.out.println(" Item list size into uploading "+itemproductlist.size());
		System.out.println(" Item list into uploading "+itemproductlist);
		
		ArrayList<PurchaseRequisition> prequisitionEntityList=new ArrayList<PurchaseRequisition>();
		ArrayList<String> employeeNameList=new ArrayList<String>();
		for (Map.Entry<String, ArrayList<PurchaseRequisition>> entryset : prequsitionHashMap.entrySet()) {
			PurchaseRequisition prequisitionEntity = new PurchaseRequisition();
			ArrayList<PurchaseRequisition> prlist= entryset.getValue();
			ArrayList<ProductDetails> preqlist=new ArrayList<ProductDetails>();
			ArrayList<PurchaseRequisition> addlist=new ArrayList<PurchaseRequisition>();
			HashMap<String, ArrayList<PurchaseRequisition>> productHashmap=new HashMap<String,ArrayList<PurchaseRequisition>>();
			System.out.println("List Value "+prlist);
			for(PurchaseRequisition prequisition : prlist){
				ProductDetails product_details=new ProductDetails();
//				ItemProduct itempprod = getItemProduct(prequisition.getPrproduct().get(0).getProductCode(),itemproductlist);
				SuperProduct itempprod = getItemProduct(prequisition.getPrproduct().get(0).getProductCode(),itemproductlist);
				
				itempprod.setTermsAndConditions(new DocumentUpload());
				itempprod.setProductImage(new DocumentUpload());
				itempprod.setProductImage1(new DocumentUpload());
				itempprod.setProductImage2(new DocumentUpload());
				itempprod.setProductImage3(new DocumentUpload());
				
				product_details.setPrduct(itempprod);
				product_details.setProductName(itempprod.getProductName());
				product_details.setProductCategory(itempprod.getProductCategory());
				product_details.setProductID(itempprod.getCount());
				product_details.setProductCode(itempprod.getProductCode());
				System.out.println(" Product Code inside for Loop "+itempprod.getProductCode());
				product_details.setId(Long.valueOf(itempprod.getId()));
//				product_details.setProductID(getproCode(itempprod.getPr,itemproductlist));
				if(product_details.getUnitOfmeasurement()==null){
				product_details.setUnitOfmeasurement(itempprod.getUnitOfMeasurement());
				}
				double availableStock=getWarehouseAvailableStock(prequisition,prequisition.getPrproduct().get(0).getWarehouseName());
				product_details.setAvailableStock(availableStock);
				product_details.setProdPrice(prequisition.getPrproduct().get(0).getProdPrice());
				double price=prequisition.getPrproduct().get(0).getProdPrice();
				product_details.setProductQuantity(prequisition.getPrproduct().get(0).getProductQuantity());
				double qty=Double.valueOf(prequisition.getPrproduct().get(0).getProductQuantity());
//				product_details.setProdDate(prequisition.getPrproduct().get(0).getProdDate());
				product_details.setProdDate(prlist.get(0).getExpectedDeliveryDate());
				logger.log(Level.SEVERE," Product Date Value Put in Database "+prlist.get(0).getExpectedDeliveryDate());
				product_details.setWarehouseName(prequisition.getPrproduct().get(0).getWarehouseName());
				product_details.setWarehouseAddress(getWarehouseAddress1(prequisition.getPrproduct().get(0).getWarehouseName(),warehousedatalist));
				product_details.setTotal(getTotal(prequisition.getPrproduct().get(0).getTotal(),qty,price));
				
//				String productkey=prequisition.getRemark();
//				
//				if (productHashmap != null && productHashmap.size() != 0) {
//					if (productHashmap.containsKey(productkey)) {
//						productHashmap.get(productkey).add(prequisition);
//					} else {
//						ArrayList<PurchaseRequisition> list = new ArrayList<PurchaseRequisition>();
//						list.add(prequisition);
//						productHashmap.put(productkey, list);
//					}
//				} else {
//					ArrayList<PurchaseRequisition> list = new ArrayList<PurchaseRequisition>();
//					list.add(prequisition);
//					productHashmap.put(productkey, list);			
			      preqlist.add(product_details);
			}
			prequisitionEntity.setPrproduct(preqlist);
			prequisitionEntity.setPrTitle(prlist.get(0).getPrTitle());
			prequisitionEntity.setCreationDate(getCurrentDate(prlist.get(0).getCreationDate()));
			prequisitionEntity.setExpectedDeliveryDate(prlist.get(0).getExpectedDeliveryDate());
			prequisitionEntity.setPrGroup(prlist.get(0).getPrGroup());
			prequisitionEntity.setPrCategory(prlist.get(0).getPrCategory());
			prequisitionEntity.setPrType(prlist.get(0).getPrType());
			prequisitionEntity.setBranch(prlist.get(0).getBranch());
			prequisitionEntity.setEmployee(prlist.get(0).getEmployee());
			prequisitionEntity.setApproverName(prlist.get(0).getApproverName());
			prequisitionEntity.setDescription(prlist.get(0).getDescription());
			prequisitionEntity.setDescription2(prlist.get(0).getDescription2());
			prequisitionEntity.setHeader(prlist.get(0).getHeader());
			prequisitionEntity.setFooter(prlist.get(0).getFooter());
			prequisitionEntity.setCompanyId(companyId);
//			prequisitionEntity.setStatus(PurchaseRequisition.CREATED);
			prequisitionEntity.setStatus(PurchaseRequisition.REQUESTED);
//			Single
			
//			Thread thread=new Thread();
			
//			GenricServiceImpl genimpl = new GenricServiceImpl();
//			ReturnFromServer server = new ReturnFromServer();
//			server = genimpl.save(prequisitionEntity);
			
//			getSerialNumberGeneration(server);			
//			server=genimpl.save(prequisitionEntityList);
			employeeNameList.add(prlist.get(0).getEmployee());
			prequisitionEntityList.add(prequisitionEntity);
			logger.log(Level.SEVERE,"Purchase Requisition saved successfull");
//			ServerAppUtility serverApputility = new ServerAppUtility();
//			serverApputility.sendApprovalRequestPRUpload(prequisitionEntity,prlist,prequisitionEntity.getBranch(), server.count, companyId, prequisitionEntity.getApproverName(),prlist.get(0).getEmployee(),"Purchase Requisition");
//			if(flag){
//				prequisitionEntity.setStatus(PurchaseRequisition.CREATED);
//				ofy().save().entities(prequisitionEntity).now();
//				logger.log(Level.SEVERE,"Purchase Requisition saved with ofy");
//			}
			System.out.println("Purchase Requisition Excel Upload successfull");	
		}
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		ArrayList<ReturnFromServer> server = new ArrayList<ReturnFromServer>();
//		server=null;
		ArrayList<SuperModel> superModelObj=new ArrayList<SuperModel>(prequisitionEntityList);
		server=genimpl.save(superModelObj);//(prequisitionEntityList);
		
		ArrayList<Integer> serverCount=new ArrayList<>();
//		serverCount=null;
		for(ReturnFromServer serverDate:server){
			serverCount.add(serverDate.count);
		}
		
		int count=0;
		for(PurchaseRequisition purchaseRequsitionList:prequisitionEntityList){
			ServerAppUtility serverApputility = new ServerAppUtility();
			serverApputility.sendApprovalRequestPRUpload(purchaseRequsitionList,"",purchaseRequsitionList.getBranch(), serverCount.get(count), companyId, purchaseRequsitionList.getApproverName(),employeeNameList.get(count),"Purchase Requisition");
			count++;
		}
		
		
		
	 }catch(Exception e){System.out.println(" Exception is Here "+e);}
		
	 }
	private SuperProduct getItemProduct(String productCode,
			List<SuperProduct> itemproductlist) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		for(SuperProduct product : itemproductlist){
			if(productCode.trim().equals(product.getProductCode().trim())){
				return product;
			}
		}
		return null;
	}
	private Date getCurrentDate(Date creationDate) {
		// TODO Auto-generated method stub
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		df.setTimeZone(TimeZone.getTimeZone("Chennai, Kolkata, Mumbai, New Delhi"));
	//	String date1=df.format(date);
		return date;
		
	}
	private double getTotal(double total, double qty, double price) {

		double res=0.0;
		try{
			
			 res=qty*price;
			 System.out.println("Total Value "+res);
			 System.out.println("Total Value "+res);
			 System.out.println("Total Value "+res);
		}catch(ArithmeticException e){
			
			e.getMessage();
		}
		   
		return res;
	}
	
	private String getWarehouseAddress1(String warehouseName,List<WareHouse> warehouselist) {
		// TODO Auto-generated method stub

	      String address="";
	     for(WareHouse warehouse: warehouselist){
	    	 if(warehouseName.equals(warehouse.getBusinessUnitName())){
	    		  address = warehouse.getAddress().getCompleteAddress();
	    		  System.out.println(" Warehouse Value"+warehouse);
	    		  return address;
	    	 }
	     }
			return address;
	}
	
	private double getProductPrice(List<SuperProduct> superProductCodeforPrice, String prodCode) {
		// TODO Auto-generated method stub
		
		double productPriceValue=0.0;
		
		for(SuperProduct product:superProductCodeforPrice){
			if(product.getProductCode().equals(prodCode)){
				productPriceValue=product.getPrice();
				System.out.println("Product price at method "+productPriceValue);
				return productPriceValue;
			}
		}
		
		return productPriceValue;
	}
	
	private double getWarehouseAvailableStock(PurchaseRequisition prequisition, String warehouseName) {
		// TODO Auto-generated method stub
		
		double stock=0.0;
		
		for(ProductDetails warehouse:prequisition.getPrproduct()){
			if(warehouseName.equals(warehouse.getWarehouseName())){
				stock=warehouse.getAvailableStock();
				System.out.println("Available Stock "+warehouse.getAvailableStock());
				System.out.println("Available Stock "+warehouse.getAcceptedQty());
				return stock;
			}
		}
		
		return 0;
	}
	private String getWarehouseName(String warehousename,
			List<WareHouse> qwarehouseName) {
		// TODO Auto-generated method stub
		String wname="";
		for(WareHouse name:qwarehouseName){
		  if(warehousename.equalsIgnoreCase(name.getBusinessUnitName()))
		  {
			  wname=name.getBusinessUnitName();
		  }
		}
		return wname;
	}
	
	private String getStorageLocation(String storagelocation,
			List<WareHouse> qwarehouseName, List<StorageLocation> qstorageLoc) {
		// TODO Auto-generated method stub
		String storeName="";
		
		for(StorageLocation location:qstorageLoc)
		{
			if(storagelocation.equalsIgnoreCase(location.getBusinessUnitName())){
				storeName=location.getBusinessUnitName();
				return storeName;
				
			}
		}
		return storeName;
	}
	
	private String getWarehouseType(String warehouseType,
			List<WareHouse> qwarehousetype) {
		// TODO Auto-generated method stub
		String type="";
		for(WareHouse waretype:qwarehousetype){
		 	
			if(warehouseType.equalsIgnoreCase(waretype.getWarehouseType()))
			{
				type=waretype.getWarehouseType();
			}
		}
		return type;
	}
	private String getBin(String storagebin, List<Storagebin> qstoragebin) {
		// TODO Auto-generated method stub
		String binName="";
		for(Storagebin bin:qstoragebin){
			
			if(storagebin.equalsIgnoreCase(bin.getBinName())){
				binName=bin.getBinName();
			}
		}
		return binName;
	}
	
	private void reactOnStockUpdateUpload(long companyId,ArrayList<String> exceldatalist) {
		// TODO Auto-generated method stub
	
		System.out.println(" Inside Document Upload File Task Queue ");
		ArrayList<ProductInventoryViewDetails> prodInventoryViewList=new ArrayList<ProductInventoryViewDetails>();
		System.out.println(" Excel Size Inside Document Upload "+exceldatalist.size());
		for(int i=14;i<exceldatalist.size();i+=14){
			
			ProductInventoryViewDetails prodInventoryViewEntity=new ProductInventoryViewDetails();
			
			if(!exceldatalist.get(i).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i)) "+exceldatalist.get(i));
				prodInventoryViewEntity.setProdid(Integer.parseInt(exceldatalist.get(i)));
			}
			if(!exceldatalist.get(i+1).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+1)) "+exceldatalist.get(i+1));
				prodInventoryViewEntity.setProdname(exceldatalist.get(i+1));
			}
			if(!exceldatalist.get(i+2).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+2)) "+exceldatalist.get(i+2));
				prodInventoryViewEntity.setWarehousename(exceldatalist.get(i+2));
			}
			if(!exceldatalist.get(i+3).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+3)) "+exceldatalist.get(i+3));
				prodInventoryViewEntity.setStoragelocation(exceldatalist.get(i+3));
			}
			if(!exceldatalist.get(i+4).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+4)) "+exceldatalist.get(i+4));
				prodInventoryViewEntity.setStoragebin(exceldatalist.get(i+4));
			}
			if(!exceldatalist.get(i+5).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+5)) "+exceldatalist.get(i+5));
				prodInventoryViewEntity.setWarehouseType(exceldatalist.get(i+5));
			}
			if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
				System.out.println(" exceldatalist.get(i+6)) Here "+exceldatalist.get(i+6));
				prodInventoryViewEntity.setMinqty(Double.parseDouble(exceldatalist.get(i+6)));
			}
			if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+7)) "+exceldatalist.get(i+7));
				prodInventoryViewEntity.setMaxqty(Double.parseDouble(exceldatalist.get(i+7)));
			}
			if(!exceldatalist.get(i+8).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+8)) "+exceldatalist.get(i+8));
				prodInventoryViewEntity.setNormallevel(Double.parseDouble(exceldatalist.get(i+8)));
			}
			if(!exceldatalist.get(i+9).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setReorderlevel(Double.parseDouble(exceldatalist.get(i)));
//				System.out.println(" exceldatalist.get(i+9)) "+exceldatalist.get(i+9));
				prodInventoryViewEntity.setAvailableqty(Double.parseDouble(exceldatalist.get(i+9)));
			}
			if(!exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setReorderqty(Double.parseDouble(exceldatalist.get(i)));
//				System.out.println(" exceldatalist.get(i+10)) "+exceldatalist.get(i+10));
				prodInventoryViewEntity.setAvgConsumption(Double.parseDouble(exceldatalist.get(i+10)));
			}
			if(!exceldatalist.get(i+11).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+11)) "+exceldatalist.get(i+11));
//				prodInventoryViewEntity.setAvailableqty(Double.parseDouble(exceldatalist.get(i)));
				prodInventoryViewEntity.setLeadDays(Integer.parseInt(exceldatalist.get(i+11)));
			}
			if(!exceldatalist.get(i+12).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i+12)) "+exceldatalist.get(i+12));
//				prodInventoryViewEntity.setAvgConsumption(Double.parseDouble(exceldatalist.get(i)));
				prodInventoryViewEntity.setMinOrderQty(Double.parseDouble(exceldatalist.get(i+12)));
			}
			if(!exceldatalist.get(i+13).trim().equalsIgnoreCase("NA")){
//				System.out.println(" exceldatalist.get(i)) "+exceldatalist.get(i+13));
//				prodInventoryViewEntity.setLeadDays(Integer.parseInt(exceldatalist.get(i)));
				prodInventoryViewEntity.setForecastDemand(Double.parseDouble(exceldatalist.get(i+13)));
			}
//			if(!exceldatalist.get(i+14).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setSafetyStockLevel(Double.parseDouble(exceldatalist.get(i)));
//			}
//			if(!exceldatalist.get(i+15).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setMinOrderQty(Double.parseDouble(exceldatalist.get(i)));
//			}
//			if(!exceldatalist.get(i+16).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setForecastDemand(Double.parseDouble(exceldatalist.get(i)));
//			}
//			if(!exceldatalist.get(i+17).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setOrderQty(Double.parseDouble(exceldatalist.get(i)));
//			}
//			if(!exceldatalist.get(i+18).trim().equalsIgnoreCase("NA")){
//				prodInventoryViewEntity.setExpectedStock(Double.parseDouble(exceldatalist.get(i)));
//			}
			prodInventoryViewList.add(prodInventoryViewEntity);
		}
		
		   HashSet<String> storageLocation=new HashSet<String>();
	       HashSet<String> storageBin=new HashSet<String>();
	       HashSet<String> warehouseName=new HashSet<String>();
	       HashSet<String> warehouseType=new HashSet<String>();
	       HashSet<Integer> prodID=new HashSet<Integer>();
	       HashSet<String>  itemName=new HashSet<String>();
	       
		  for(ProductInventoryViewDetails prodIV:prodInventoryViewList){
			  
			  storageLocation.add(prodIV.getStoragelocation());
			  storageBin.add(prodIV.getStoragebin());
			  warehouseName.add(prodIV.getWarehousename());
			  warehouseType.add(prodIV.getWarehouseType());
			  prodID.add(prodIV.getProdid());
			  itemName.add(prodIV.getProdname());
			  
		  }
		  
		  System.out.println(" itemList "+itemName.size());
		  ArrayList<String> Storageloc=new ArrayList<String>(storageLocation);
		  ArrayList<String> storagebin1=new ArrayList<String>(storageBin);
		  ArrayList<String> warehouseName1=new ArrayList<String>(warehouseName);
		  ArrayList<String> warehousetype1=new ArrayList<String>(warehouseType);
		  ArrayList<Integer> prodID1=new ArrayList<Integer>(prodID);
		  ArrayList<String> itemList=new ArrayList<String>(itemName);
		  
//		  System.out.println(" Storageloc "+Storageloc);
//		  System.out.println(" storagebin1 "+storagebin1);
//		  System.out.println(" warehouseName1 "+warehouseName1);
//		  System.out.println(" warehousetype1 "+warehousetype1);
//		  System.out.println(" prodID1 "+prodID1);
//		  System.out.println(" itemList "+itemList);
		  
//		  Below code for Query   
		  List<WareHouse> qwarehouseName = ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("buisnessUnitName IN", warehouseName1).list();
//		  System.out.println(" WareHouse size "+qwarehouseName);
		  
		  List<StorageLocation> qstorageLoc=ofy().load().type(StorageLocation.class).filter("companyId", companyId).filter("buisnessUnitName IN", Storageloc).list();
//		  System.out.println(" StorageLocation size "+qstorageLoc);
		  
		  List<Storagebin> qstoragebin=ofy().load().type(Storagebin.class).filter("companyId",companyId).filter("binName IN", storageBin).list();
//		  System.out.println(" Storagebin size "+qstoragebin);
		  
		  List<WareHouse> qwarehousetype=ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("warehouseType IN",warehousetype1).list();
//		  System.out.println(" WareHouse size "+qwarehousetype);
		  
		  List<ProductInventoryViewDetails> prodInventViewDetailsforTesting=ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).filter("prodid IN", prodID1).list();
		  System.out.println("prodInventViewDetailsforTesting size here :- "+prodInventViewDetailsforTesting.size());
		  List<ProductInventoryView> prodInventView=ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).filter("productinfo.prodID IN", prodID1).list();
//		  List<ProductInventoryViewDetails> prodInventView=ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).filter("prodid IN", prodID1).list();
//		  System.out.println(" ProductInventoryView size "+prodInventView);
		  
		  List<ItemProduct> itemName2=ofy().load().type(ItemProduct.class).filter("companyId",companyId).filter("productCode IN", itemList).list();
//		  System.out.println(" ItemProduct size "+itemName2);
		  
		  
		  System.out.println(" Backend Data  size "+prodInventView.size()+" Warehouse");
		  
//		  ArrayList<ProductInventoryView> prodInventoryUpdateList=new ArrayList<ProductInventoryView>();
		  ArrayList<ProductInventoryViewDetails> prodInventoryCreateList=new ArrayList<ProductInventoryViewDetails>();
		  ArrayList<ProductInventoryViewDetails> prodInventoryUpdateList=new ArrayList<ProductInventoryViewDetails>();
//		  ArrayList<ProductInventoryViewDetails> prodInventoryCreateList=new ArrayList<ProductInventoryViewDetails>();
		  
		  HashMap<Integer,ProductInventoryView> newProdStockMap=new HashMap<Integer,ProductInventoryView>();
//		  HashMap<Integer,ProductInventoryViewDetails> newProdStockMap=new HashMap<Integer,ProductInventoryViewDetails>();
		  int i=0;
		 
		  for(ProductInventoryViewDetails excelProdInventoryView : prodInventViewDetailsforTesting){
			  
			     double avgconsume=0.0;
				 int leadDays=0;
				 double safestock=0.0;
				 double avilableqty=0.0;
				 double forcaseDemand=0.0;
				 double orderQTY1=0.0;
				 double furtherConsumptiontillStockReceived=0.0;
				 
			  
			 ProductInventoryViewDetails prodInventoryViewDetailsResult=getCreationOrUpdationStock(prodInventoryViewList,prodInventViewDetailsforTesting);
			 
			 if(prodInventoryViewDetailsResult!=null){
				 
				 System.out.println(" Inside value is not null ");
				 
				 prodInventoryUpdateList.add(prodInventoryViewDetailsResult); 
				 
			 }else{
				
				 System.out.println(" Inside value is null ");
				 
			     leadDays=excelProdInventoryView.getLeadDays(); 
				 avgconsume=excelProdInventoryView.getAvgConsumption();
				 safestock = (avgconsume*110/100);
				 avilableqty=excelProdInventoryView.getAvailableqty();
				 forcaseDemand=excelProdInventoryView.getForecastDemand();
				 furtherConsumptiontillStockReceived = Math.round(avgconsume*leadDays/30);
				 
				ProductInventoryViewDetails prodDetails=new ProductInventoryViewDetails();
				
				
				  prodDetails.setCompanyId(companyId);
				  prodDetails.setProdid(excelProdInventoryView.getProdid());
				  prodDetails.setProdname(excelProdInventoryView.getProdname());
				  prodDetails.setWarehousename(excelProdInventoryView.getWarehousename());
				  prodDetails.setStoragelocation(excelProdInventoryView.getStoragelocation());
				  prodDetails.setStoragebin(excelProdInventoryView.getStoragebin());
				 
				prodDetails.setProdname(excelProdInventoryView.getProdname());
			    prodDetails.setProdcode(prodInventView.get(0).getProductCode());
				prodDetails.setMinOrderQty(excelProdInventoryView.getMinOrderQty());
				prodDetails.setMaxqty(excelProdInventoryView.getMaxqty());
				prodDetails.setNormallevel(excelProdInventoryView.getNormallevel());
				double reOrderLevel = (((avgconsume/30)*leadDays)+safestock);
				reOrderLevel = Math.round(reOrderLevel);
				prodDetails.setReorderlevel(reOrderLevel);
				prodDetails.setAvailableqty(avilableqty);
				prodDetails.setAvgConsumption(excelProdInventoryView.getAvgConsumption());
				prodDetails.setLeadDays(leadDays);
				prodDetails.setSafetyStockLevel(safestock);
				prodDetails.setMinOrderQty(excelProdInventoryView.getMinOrderQty());
				prodDetails.setForecastDemand(forcaseDemand);
				orderQTY1=getOrderqty(avilableqty, forcaseDemand, furtherConsumptiontillStockReceived, reOrderLevel, excelProdInventoryView.getMinOrderQty(), safestock);
				System.out.println(" Order Quantity "+orderQTY1);
				prodDetails.setOrderQty(orderQTY1);
				prodDetails.setExpectedStock(orderQTY1+avilableqty-furtherConsumptiontillStockReceived);
				 
				prodInventoryCreateList.add(prodDetails); 
				System.out.println(" prodInventoryCreateList size "+prodInventoryCreateList.size());
			 }
			  
		  }
		  
		  if(prodInventoryUpdateList!=null){
			  System.out.println(" Inside updation Save ");
			  ofy().save().entities(prodInventoryUpdateList).now(); 
		  }
		  
		  if(prodInventoryCreateList!=null){
			  System.out.println(" Inside Creation Save ");
			  GenricServiceImpl impl=new GenricServiceImpl();
			  for(ProductInventoryViewDetails prodDetails:prodInventoryCreateList){
		         impl.save(prodDetails);
			  }
		  }
		  
//		  for(ProductInventoryViewDetails excelProdInventoryView : prodInventoryViewList ){
//			  
//			  ProductInventoryView prodIVResult=getProductInventoryViewResult1(excelProdInventoryView,prodInventView,companyId);
//			  
//			  if(prodIVResult==null){
//				  System.out.println(" Inside prodIVResult==null condition ");
//				  
////				  prodInventoryUpdateList.add(prodIVResult);
//			  }
//			  else{ 
//				  
//				  double avgconsume=0.0;
//				  int leadDays=0;
//				  double safestock=0.0;
//				  double avilableqty=0.0;
//				  double forcaseDemand=0.0;
//				  double orderQTY1=0.0;
//				  double furtherConsumptiontillStockReceived=0.0;
//				  
//				  ProductInventoryView piv=null;
////				  ProductInventoryViewDetails pivd=null;
//				  if(newProdStockMap!=null&&newProdStockMap.size()!=0){
//						if(newProdStockMap.containsKey(excelProdInventoryView.getProdid())){
//							piv=newProdStockMap.get(excelProdInventoryView.getProdid());
////							pivd=newProdStockMap.get(excelProdInventoryView.getProdid());
//							System.out.println(" Inside piv ");
//						}else{
//							piv=new ProductInventoryView();
////							pivd=new ProductInventoryViewDetails();
//							System.out.println(" Inside new piv Create ");
//						}
//				  }else{
//					  piv=new ProductInventoryView();
////					  pivd=new ProductInventoryViewDetails();
//				  }
//				  
//				  ProductInventoryViewDetails prodDetails=new ProductInventoryViewDetails();
//				  
//				  prodDetails.setCompanyId(companyId);
//				  prodDetails.setProdid(excelProdInventoryView.getProdid());
//				  prodDetails.setProdname(excelProdInventoryView.getProdname());
//				  prodDetails.setWarehousename(excelProdInventoryView.getWarehousename());
//				  prodDetails.setStoragelocation(excelProdInventoryView.getStoragelocation());
//				  prodDetails.setStoragebin(excelProdInventoryView.getStoragebin());
//				  
//				  
//				    leadDays=excelProdInventoryView.getLeadDays();
//					avgconsume=excelProdInventoryView.getAvgConsumption();
//					safestock = (avgconsume*110/100);
//					avilableqty=excelProdInventoryView.getAvailableqty();
//					forcaseDemand=excelProdInventoryView.getForecastDemand();
//					furtherConsumptiontillStockReceived = Math.round(avgconsume*leadDays/30);
//					
//					System.out.println(" furtherConsumptiontillStockReceived "+furtherConsumptiontillStockReceived);
//					
//					prodDetails.setProdname(excelProdInventoryView.getProdname());
////					prodDetails.setProdcode(prodInventView.get(0).getProductCode());
//					prodDetails.setMinOrderQty(excelProdInventoryView.getMinOrderQty());
//					prodDetails.setMaxqty(excelProdInventoryView.getMaxqty());
//					prodDetails.setNormallevel(excelProdInventoryView.getNormallevel());
//					double reOrderLevel = (((avgconsume/30)*leadDays)+safestock);
//					reOrderLevel = Math.round(reOrderLevel);
//					prodDetails.setReorderlevel(reOrderLevel);
//					prodDetails.setAvailableqty(avilableqty);
//					prodDetails.setAvgConsumption(excelProdInventoryView.getAvgConsumption());
//					prodDetails.setLeadDays(leadDays);
//					prodDetails.setSafetyStockLevel(safestock);
//					prodDetails.setMinOrderQty(excelProdInventoryView.getMinOrderQty());
//					prodDetails.setForecastDemand(forcaseDemand);
//					orderQTY1=getOrderqty(avilableqty, forcaseDemand, furtherConsumptiontillStockReceived, reOrderLevel, excelProdInventoryView.getMinOrderQty(), safestock);
//					System.out.println(" Order Quantity "+orderQTY1);
//					prodDetails.setOrderQty(orderQTY1);
//					prodDetails.setExpectedStock(orderQTY1+avilableqty-furtherConsumptiontillStockReceived);
//				    
//					System.out.println("prodDetails size :- "+prodDetails);
//					
//					if(piv.getDetails()==null){
////					if(pivd==null){
//						System.out.println("Inside piv.setDetails(obj) ");
//						ArrayList<ProductInventoryViewDetails> obj=new ArrayList<ProductInventoryViewDetails>();
//						obj.add(prodDetails);
////						GenricServiceImpl impl=new GenricServiceImpl();
////						  impl.save(prodDetails);
//						piv.setDetails(obj);
//						System.out.println(" Here Come pivd.setDetails(obj)==Null");
//						
//					}else{
//						GenricServiceImpl impl=new GenricServiceImpl();
//						  impl.save(prodDetails);
//						  System.out.println("Inside impl.save(prodDetails) ");
//						  System.out.println("Save Date Here with Warehouse "+prodDetails.getWarehousename());
//						piv.getDetails().add(prodDetails);
////						pivd.add (prodDetails);
//						System.out.println(" Here Come pivd.(prodDetails)!=null ");
//					}
//					
//					piv.setCompanyId(companyId);
//					piv.setProductName(excelProdInventoryView.getProdname());
//					piv.setCount(excelProdInventoryView.getProdid());
//					piv.setProductCode(prodInventView.get(0).getProductCode());
//					
////					pivd.setCompanyId(companyId);
////					pivd.setProdname(excelProdInventoryView.getProdname());
////					pivd.setCount(excelProdInventoryView.getProdid());
////					pivd.setProductCode(prodInventView.get(0).getProductCode());
//					
//					prodInventoryCreateList.add(piv);
//					
//					newProdStockMap.put(prodDetails.getProdid(), piv);				
////					newProdStockMap.put(prodDetails.getProdid(), pivd);
//					
//			  }
//		  }
//		    
//		  if(prodInventView!=null&&prodInventView.size()!=0){
//			  System.out.println(" Inside prodInventView!=null&&prodInventView.size()!=0 ");
//			  ofy().save().entities(prodInventView).now();
//		  }
//		  
//		  if(newProdStockMap!=null&&newProdStockMap.size()!=0){
//			  System.out.println(" Inside newProdStockMap!=null&&newProdStockMap.size()!=0 ");
//			  for(Integer key:newProdStockMap.keySet()){
//				  prodInventoryCreateList.add(newProdStockMap.get(key));
//			  }
//		  }else{
//			  
//			  System.out.println(" Its Null "+" newProdStockMap Size "+newProdStockMap.size());
//		  }
////		  GenricServiceImpl impl=new GenricServiceImpl();
////		  impl.save(prodInventoryCreateList);
//		  
//		  if(prodInventoryCreateList.size()!=0){
//			  System.out.println(" Inside prodInventoryCreateList.size()!=0 ");
//			  ofy().save().entities(prodInventoryCreateList).now();
//		  }	  	  
//		  System.out.println(" ProductInventoryUpdate List Size "+prodInventoryUpdateList.size());
//		  System.out.println(" ProductInventoryCreate List Size "+prodInventoryCreateList.size());
			
		
		
	}




	private ProductInventoryViewDetails getCreationOrUpdationStock(
			ArrayList<ProductInventoryViewDetails> prodInventoryViewList,
			List<ProductInventoryViewDetails> prodInventViewDetailsforTesting) {
		// TODO Auto-generated method stub
		System.out.println(" Inside Update and Create Stock Method ");
		
		  double avgconsume=0.0;
		  int leadDays=0;
		  double safestock=0.0;
		  double avilableqty=0.0;
		  double forcaseDemand=0.0;
		  double orderQTY1=0.0;
		  double furtherConsumptiontillStockReceived=0.0;
		
		  System.out.println(" Inventory View Details size :- "+prodInventoryViewList.size());
		
		
		for(ProductInventoryViewDetails excelValue:prodInventoryViewList){
		for(ProductInventoryViewDetails prodDetailsValue:prodInventViewDetailsforTesting){
		
			if(excelValue.getProdid()==prodDetailsValue.getProdid()&&
				excelValue.getWarehousename().equals(prodDetailsValue.getWarehousename())&&
				excelValue.getStoragelocation().equals(prodDetailsValue.getStoragelocation())&&
				excelValue.getStoragebin().equals(prodDetailsValue.getStoragebin())){
				System.out.println(" Inside Updation ");
				
				leadDays=excelValue.getLeadDays(); 
				avgconsume=excelValue.getAvgConsumption();
				safestock = (avgconsume*110/100);
				avilableqty=excelValue.getAvailableqty();
				forcaseDemand=excelValue.getForecastDemand();
				furtherConsumptiontillStockReceived = Math.round(avgconsume*leadDays/30);
				
				System.out.println(" Lead Day "+leadDays);
				System.out.println(" avgconsume "+avgconsume);
				System.out.println(" avgconsume "+avgconsume);
				System.out.println(" safestock "+safestock);
				System.out.println(" avilableqty "+avilableqty);
				System.out.println(" forcaseDemand "+forcaseDemand);
				System.out.println(" furtherConsumptiontillStockReceived "+furtherConsumptiontillStockReceived);
				
				prodDetailsValue.setProdname(excelValue.getProdname());
//				prodDetails.setProdcode(prodView.getProductCode()); Comment by Deepak Salve
				prodDetailsValue.setMinOrderQty(excelValue.getMinOrderQty());
				prodDetailsValue.setMaxqty(excelValue.getMaxqty());
				prodDetailsValue.setNormallevel(excelValue.getNormallevel());
				double reOrderLevel = (((avgconsume/30)*leadDays)+safestock);
				reOrderLevel = Math.round(reOrderLevel);
				prodDetailsValue.setReorderlevel(reOrderLevel);
				prodDetailsValue.setAvailableqty(avilableqty);
				prodDetailsValue.setAvgConsumption(excelValue.getAvgConsumption());
				prodDetailsValue.setLeadDays(leadDays);
				prodDetailsValue.setSafetyStockLevel(safestock);
				prodDetailsValue.setMinOrderQty(excelValue.getMinOrderQty());
				prodDetailsValue.setForecastDemand(forcaseDemand);
				orderQTY1=getOrderqty(prodDetailsValue.getAvailableqty(), forcaseDemand, furtherConsumptiontillStockReceived, reOrderLevel, excelValue.getMinOrderQty(), safestock);
				System.out.println(" Order Quantity "+orderQTY1);
				prodDetailsValue.setOrderQty(orderQTY1);
				prodDetailsValue.setExpectedStock(orderQTY1+avilableqty-furtherConsumptiontillStockReceived);
				prodInventoryViewList.add(prodDetailsValue);
				
				 System.out.println(" Inside updation Save ");
				  ofy().save().entities(prodDetailsValue).now(); 
				
				return prodDetailsValue;
				
			}else{
				
				if(excelValue.getProdid()==prodDetailsValue.getProdid()&&
						!excelValue.getWarehousename().equals(prodDetailsValue.getWarehousename())&&
						!excelValue.getStoragelocation().equals(prodDetailsValue.getStoragelocation())&&
						!excelValue.getStoragebin().equals(prodDetailsValue.getStoragebin())){
						System.out.println(" Inside Creation ");
						return null;
					}
				
			}
		}
	}
		
		return null;
	}




	private double getOrderqty(double stockInHand,double forecastDemand,
			double futherConsmptntillStockReceived,double reorderLevel,double minOrderQty,
			double safetyStock) {
		// TODO Auto-generated method stub
		double orderQty = 0;
		int finalOrderQty = 0;
		if(stockInHand<=forecastDemand){
//			System.out.println(" stockInHand "+stockInHand);
//			System.out.println(" forecastDemand "+forecastDemand);
//			System.out.println(" futherConsmptntillStockReceived "+futherConsmptntillStockReceived);
//			System.out.println(" minOrderQty "+minOrderQty);
			orderQty = (forecastDemand-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
//			System.out.println(" if order Quantity inside method "+orderQty);
			orderQty = Math.round(orderQty)*minOrderQty;
//			System.out.println(" if order Quantity inside method after"+orderQty);
		}
		else if(stockInHand<=reorderLevel){
//			System.out.println(" stockInHand "+stockInHand);
//			System.out.println(" forecastDemand "+forecastDemand);
//			System.out.println(" futherConsmptntillStockReceived "+futherConsmptntillStockReceived);
//			System.out.println(" minOrderQty "+minOrderQty);
//			System.out.println(" reorderLevel "+reorderLevel);
			orderQty = (reorderLevel-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
//			System.out.println(" else if order Quantity inside method "+orderQty);
			orderQty = Math.round(orderQty)*minOrderQty;
//			System.out.println(" else if order Quantity inside method after "+orderQty);
		}
		else if(stockInHand<=safetyStock){
//			System.out.println(" stockInHand "+stockInHand);
//			System.out.println(" forecastDemand "+forecastDemand);
//			System.out.println(" futherConsmptntillStockReceived "+futherConsmptntillStockReceived);
//			System.out.println(" minOrderQty "+minOrderQty);
//			System.out.println(" safetyStock "+safetyStock);
			orderQty = (safetyStock-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
//			System.out.println(" 2 else if order Quantity inside method "+orderQty);
			orderQty = Math.round(orderQty)*minOrderQty;
//			System.out.println(" 2 else if order Quantity inside method after"+orderQty);
		}
		finalOrderQty = (int) orderQty;
//		System.out.println(" Final order qty value inside method "+finalOrderQty);
	return finalOrderQty;
	}

	
	
	 /**
 	 * @author Vijay Chougule Date 10-07-2020
 	 * Des :- To Update inhouse fumigation services properly in fumigation report and if any services missing it also create the same
 	 */

	private void reactOnInHouseServiceUpdation(long companyId, ArrayList<String> exceldatalist) {
		
		
		ArrayList<Integer> serviceIdlist = new ArrayList<Integer>();
		for(String serviceId : exceldatalist){
			if(!serviceId.equalsIgnoreCase("Service Id")) {
				serviceIdlist.add(Integer.parseInt(serviceId));	
			}
			
		}
		logger.log(Level.SEVERE, "serviceIdlist size"+serviceIdlist.size());
		
		List<Service> servicelist = ofy().load().type(Service.class).filter("wmsServiceFlag", true).filter("companyId", companyId).filter("count IN", serviceIdlist).list();
		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
		  
		  
		String completionremarkfromsystem = getDataFromSystem("DegassingCompletionRemark",servicelist.get(0).getCompanyId());

		String deggassingstatus = getDataFromSystem("DegassingServiceStatus",servicelist.get(0).getCompanyId());
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableUpdateDeggassingServiceStatusInFumigationService", servicelist.get(0).getCompanyId())){
			for(Service serviceEntity : servicelist){
				if(serviceEntity.getDeggassingServiceId()!=0){
					Service deggasingService = ofy().load().type(Service.class).filter("companyId", serviceEntity.getCompanyId()).filter("count", serviceEntity.getDeggassingServiceId()).first().now();
					if(deggasingService!=null){
						serviceEntity.setDegassingServiceStatus(deggasingService.getStatus());
						ofy().save().entity(serviceEntity);
					}
				}
				
			}
		}
		else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "UpdateDuplicateWMSServiceId", servicelist.get(0).getCompanyId())){
			reactonupdateDuplicateServiceId(companyId,servicelist);
		}
		else{
			
			for(Service serviceEntity : servicelist){
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableUpdateFumigationServiceCompletedThroughMINUpload", serviceEntity.getCompanyId())){
					
					String updatestatus = UpdateServiceCompletetionInFumigationServiceReport(serviceEntity);
					
					if(serviceEntity.getProphylacticServiceId()!=0){
						List<Service> prophylacticservicelist = ofy().load().type(Service.class).filter("companyId",serviceEntity.getCompanyId())
								.filter("count", serviceEntity.getProphylacticServiceId()).list();
						logger.log(Level.SEVERE, "prophylacticservicelist servicelist size"+prophylacticservicelist.size());
						for(Service prophylacticservice : prophylacticservicelist){
							if(prophylacticservice.getProduct().getProductCode().equals("DSSG")){
								if(serviceEntity.getRefNo2().equals(prophylacticservice.getRefNo2())
										&& serviceEntity.getCompartment().equals(prophylacticservice.getCompartment())
										 && serviceEntity.getStackNo().equals(prophylacticservice.getStackNo())){
									
									UpdateServiceCompletetionInFumigationServiceReport(prophylacticservice);

								}
							}
						}
						
						
					}
					
					/** If Deggasing Service not created then Create the same ***/
					logger.log(Level.SEVERE, "Deggassing service ID"+serviceEntity.getDeggassingServiceId());
					if(serviceEntity.getDeggassingServiceId()==0 ||(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCreateDegassingService", serviceEntity.getCompanyId())) ){
						logger.log(Level.SEVERE, "Creating deggssing Service");
						ArrayList<Service> serviceentitylist = new  ArrayList<Service>();
						serviceentitylist.add(serviceEntity);
						String str = createFumigationDeggassingService(serviceentitylist,"DSSG",null,completionremarkfromsystem,deggassingstatus);
					}
					else{ /** if deggasing service created but its status not updated in fumigationServiceReport Entity **/ 
						List<Service> degassingServicelist = ofy().load().type(Service.class).filter("companyId",serviceEntity.getCompanyId())
													.filter("count", serviceEntity.getDeggassingServiceId()).list();
						logger.log(Level.SEVERE, "Deggassing servicelist size"+degassingServicelist.size());
						for(Service degassingService : degassingServicelist){
							if(degassingService.getProduct().getProductCode().equals("DSSG")){
								if(serviceEntity.getRefNo2().equals(degassingService.getRefNo2())
										&& serviceEntity.getCompartment().equals(degassingService.getCompartment())
										 && serviceEntity.getStackNo().equals(degassingService.getStackNo())){
									
									UpdateServiceCompletetionInFumigationServiceReport(degassingService);

								}
							}
						}
					}
					
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableCreateNewFumigationService", serviceEntity.getCompanyId())){
						logger.log(Level.SEVERE, "Fumigation sucessfialure status "+serviceEntity.getFumigationStatus());
						if(!serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSSUCCESS) || !serviceEntity.getFumigationStatus().equals(Service.SERVICESTATUSFAILURE) ){
							String str = createFumigationAndProphalacticService(serviceEntity,serviceEntity.getApproverName(),serviceEntity.getRemark());
						}
					}
				}
				
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "EnableUpdateDataIntoFumigationReportOnly", serviceEntity.getCompanyId())){
					
					String updatestatus = UpdateServiceCompletetionInFumigationServiceReport(serviceEntity);

					List<Service> prophylacticservicelist = ofy().load().type(Service.class).filter("companyId",serviceEntity.getCompanyId())
							.filter("count", serviceEntity.getProphylacticServiceId()).list();
					logger.log(Level.SEVERE, "prophylacticservicelist servicelist size"+prophylacticservicelist.size());
					for(Service prophylacticservice : prophylacticservicelist){
						if(prophylacticservice.getProduct().getProductCode().equals("DSSG")){
							if(serviceEntity.getRefNo2().equals(prophylacticservice.getRefNo2())
									&& serviceEntity.getCompartment().equals(prophylacticservice.getCompartment())
									 && serviceEntity.getStackNo().equals(prophylacticservice.getStackNo())){
								
								UpdateServiceCompletetionInFumigationServiceReport(prophylacticservice);

							}
						}
					}
					
					 /** if deggasing service created but its status not updated in fumigationServiceReport Entity **/ 
					List<Service> degassingServicelist = ofy().load().type(Service.class).filter("companyId",serviceEntity.getCompanyId())
												.filter("count", serviceEntity.getDeggassingServiceId()).list();
					logger.log(Level.SEVERE, "Deggassing servicelist size"+degassingServicelist.size());
					for(Service degassingService : degassingServicelist){
						if(degassingService.getProduct().getProductCode().equals("DSSG")){
							if(serviceEntity.getRefNo2().equals(degassingService.getRefNo2())
									&& serviceEntity.getCompartment().equals(degassingService.getCompartment())
									 && serviceEntity.getStackNo().equals(degassingService.getStackNo())){
								
								UpdateServiceCompletetionInFumigationServiceReport(degassingService);

							}
						}
					}
					
				}

				
			}
		}
		
		
		
		
		
		
		
	}
	

	private String getDataFromSystem(String processName, Long companyId) {

		List<ProcessConfiguration> processList = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName", processName).list();
		int cnt = 0;
		for (int k = 0; k < processList.size(); k++) {
			if (processList.get(k).getProcessName().trim()
					.equalsIgnoreCase(processName)) {

				for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
					if (processList.get(k).getProcessList().get(i)
									.isStatus() == true) {
						return processList.get(k).getProcessList().get(i).getProcessType().trim();

					}
				}

			}
		}
		return "";
	}




	/**
	 *  Date 13-11-2019
	 *  Des :- When Fumigation Service mark successfull then new service created with 45 days
	 *  and also create prophylactic service
	 * @param remark 
	 * @param userName 
	 * @param string 
	 */
	public String createFumigationAndProphalacticService(Service serviceEntity,String userName, String remark) {
		String response = "";
		if(serviceEntity.getStackQty()>0){
			logger.log(Level.SEVERE, "Inside Fumigation Service Creation method");
			Service prophylacticService = ofy().load().type(Service.class).filter("companyId", serviceEntity.getCompanyId())
										.filter("count", serviceEntity.getProphylacticServiceId()).first().now();
			logger.log(Level.SEVERE, "serviceProduct =="+prophylacticService);
			if(prophylacticService!=null){
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
				int serviceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), (long) 2);
				logger.log(Level.SEVERE, "serviceId ==", +serviceId);
				int fumigationServiceId = ++serviceId;
				int prophylacticServiceId = ++serviceId;
				logger.log(Level.SEVERE, "fumigationServiceId ==", +fumigationServiceId);
				logger.log(Level.SEVERE, "prophylacticServiceId ==", +prophylacticServiceId);

				for(int i=0;i<2;i++){
					if(i==0){
						Service service = new Service();
						service = getServiceDetails(prophylacticService,serviceEntity);
						service.setCount(prophylacticServiceId);
						service.setFumigationServiceId(fumigationServiceId);
						ofy().save().entity(service).now();
					}
					else{
						Service service = new Service();
						service = getServiceDetails(serviceEntity,serviceEntity);
						service.setCount(fumigationServiceId);
						service.setProphylacticServiceId(prophylacticServiceId);
						ofy().save().entity(service).now();
						CreateFumigationReport(service,-1);
						
						/*** Date 11-11-2019 By Vijay Fumigation Service marking as Successfull ***/
						serviceEntity.setFumigationStatus(Service.SERVICESTATUSSUCCESS);
						Date today=DateUtility.getDateWithTimeZone("IST", new Date());
						serviceEntity.setFumigationStatusDate(today);
						if(userName!=null){
							serviceEntity.setActionBy(userName);
						}
						else{
							serviceEntity.setActionBy("Cron Job");
						}
						if(remark!=null){
							serviceEntity.setActionRemark(remark);
						}
						else{
							serviceEntity.setActionRemark("This service marked as successful by System");
						}
						ofy().save().entity(serviceEntity);
						response = "New service scheduled successfully with 45 Days";
						logger.log(Level.SEVERE, "New service scheduled successfully with 45 Days");
						updateFumigationSucessfullStatuInFumigationReport(serviceEntity);
					}
				}
			}
			else{
				response = "Propholactic Service not found for this fumigation service so future services not created";
			}
			
		}
		else{
			logger.log(Level.SEVERE, "There no stack Qty so futre services not created");
			response = "There no stack qty so future services not created";
			/*** Date 11-11-2019 By Vijay Fumigation Service marking as Successfull ***/
			serviceEntity.setFumigationStatus(Service.SERVICESTATUSSUCCESS);
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			serviceEntity.setFumigationStatusDate(today);
			if(userName!=null){
				serviceEntity.setActionBy(userName);
			}
			else{
				serviceEntity.setActionBy("Cron Job");
			}
			if(remark!=null){
				serviceEntity.setActionRemark(remark);
			}
			else{
				serviceEntity.setActionRemark("This service marked as successful by System");
			}
			ofy().save().entity(serviceEntity);
			logger.log(Level.SEVERE, "Stack Qty Zero so new service not created");
			updateFumigationSucessfullStatuInFumigationReport(serviceEntity);

		}
		
		
		
		return response;
		
	}

	private String CreateFumigationReport(Service service, int originalfumigationServiceId) {
		String remark = "";
		logger.log(Level.SEVERE, "In the fumigation report entity prophylactic Service ID"+service.getProphylacticServiceId());
		logger.log(Level.SEVERE, "In the fumigation report entity Fumigation Service ID"+service.getCount());

		FumigationServiceReport fumigationReport = new FumigationServiceReport();
		fumigationReport.setCompanyId(service.getCompanyId());
		fumigationReport.setWarehouseName(service.getWareHouse());
		fumigationReport.setWarehouseCode(service.getRefNo2());
		fumigationReport.setCompartment(service.getCompartment());
		fumigationReport.setShade(service.getShade());
		fumigationReport.setStackNumber(service.getStackNo());
		fumigationReport.setStackQuantity(service.getStackQty());
		fumigationReport.setFumigationServiceId(service.getCount());
		fumigationReport.setFumigationServiceDate(service.getOldServiceDate());
		Service serviceEntity = ofy().load().type(Service.class).filter("count", service.getProphylacticServiceId())
								.filter("companyId", service.getCompanyId()).filter("contractCount", service.getContractCount())
								.first().now();
		logger.log(Level.SEVERE,"prophylactic service serviceEntity"+serviceEntity);
		if(serviceEntity!=null){
			fumigationReport.setProphylacticServiceDate(serviceEntity.getOldServiceDate());
			fumigationReport.setProphylacticServiceId(service.getProphylacticServiceId());
		}
		fumigationReport.setContractId(service.getContractCount());
		fumigationReport.setFumigationServiceStatus(Service.SERVICESTATUSSCHEDULE);
		/*** This is for only when fumigation service mark as failure ***/
		if(originalfumigationServiceId!=-1){
		fumigationReport.setOriginalfumigationId(originalfumigationServiceId);
		}
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(fumigationReport);
		logger.log(Level.SEVERE, "Fumigation Report created for new fumigation service");
		remark = "Fumigation Report created for new fumigation service";
		return remark;
	}
	
	private void updateFumigationSucessfullStatuInFumigationReport(Service service) {
		if(service.getProduct().getProductCode().equals("STK-01")){
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				fumigationReport.setFumigationStatusDate(service.getFumigationStatusDate());
				fumigationReport.setFumigationRemark(service.getActionRemark());
				fumigationReport.setFumigationStatus(service.getFumigationStatus());
				fumigationReport.setStackQuantity(service.getStackQty());
				fumigationReport.setSuccessFailureBy(service.getActionBy());
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
	}
	
	private Service getServiceDetails(Service serviceEntity, Service fumigationService) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Service temp = new Service();
		temp.setRefNo(serviceEntity.getRefNo()); // inward // transaction // id
		temp.setPersonInfo(serviceEntity.getPersonInfo());
		temp.setContractCount(serviceEntity.getContractCount());
		temp.setProduct((ServiceProduct) serviceEntity.getProduct());
		temp.setBranch(serviceEntity.getBranch());
		temp.setStatus(Service.SERVICESTATUSSCHEDULE);
		temp.setContractStartDate(serviceEntity.getContractStartDate());
		temp.setContractEndDate(serviceEntity.getContractEndDate());
		temp.setAdHocService(false);
		temp.setServiceIndexNo(serviceEntity.getServiceIndexNo()+1);
		temp.setServiceType(serviceEntity.getServiceType());

		temp.setServiceSerialNo(serviceEntity.getServiceSerialNo()+1);
		/**
		 * Date 11-12-2019 by Vijay
		 * Des :- New service will create based on Degassing Original Service Date + 45 Days
		 */
//		Date tPlus45DaysDate = serviceEntity.getServiceCompletionDate();
		
		Date tPlus45DaysDate = null;
//		Service deggassingService = ofy().load().type(Service.class).filter("companyId", fumigationService.getCompanyId())
//									.filter("count", fumigationService.getDeggassingServiceId()).first().now();
//		if(deggassingService!=null){
//			logger.log(Level.SEVERE, "deggassingService"+deggassingService.getOldServiceDate());
//			tPlus45DaysDate = deggassingService.getOldServiceDate();
//		}
		
		List<Service> degassingServicelist = ofy().load().type(Service.class).filter("companyId", fumigationService.getCompanyId())
				.filter("count", fumigationService.getDeggassingServiceId()).list();
			logger.log(Level.SEVERE, "degassingServicelist size "+degassingServicelist.size());	
		for(Service degassingService : degassingServicelist){
			if(degassingService.getProduct().getProductCode().equals("DSSG")){
				if(serviceEntity.getRefNo2().equals(degassingService.getRefNo2())
						&& serviceEntity.getCompartment().equals(degassingService.getCompartment())
						 && serviceEntity.getStackNo().equals(degassingService.getStackNo())){
					
					tPlus45DaysDate = degassingService.getOldServiceDate();

				}
			}
		}
		
		try {
		Calendar c4 = Calendar.getInstance();
		c4.setTime(tPlus45DaysDate);
		c4.add(Calendar.DATE, +45);
		logger.log(Level.SEVERE, "Date after 45 Days===" + tPlus45DaysDate);
	
			tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
		} catch (ParseException e) {
			logger.log(Level.SEVERE, " Deggasing service not found");
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "Date after 45 Days" + tPlus45DaysDate);

		temp.setServiceDate(tPlus45DaysDate);
		temp.setOldServiceDate(tPlus45DaysDate);
		temp.setAddress(serviceEntity.getAddress());

		temp.setServiceTime(serviceEntity.getServiceTime());
		temp.setServiceBranch(serviceEntity.getServiceBranch());
		temp.setServiceDateDay(serviceEntity.getServiceDateDay());

		temp.setBranch(serviceEntity.getBranch());
		temp.setServicingTime(serviceEntity.getServicingTime());

		temp.setServiceSrNo(serviceEntity.getServiceSerialNo());
		temp.setCompanyId(serviceEntity.getCompanyId());
		/**
		 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation
		 * Services for every Stack This code only executed from
		 * Web Services of NBHC Fumigation service
		 */
		temp.setWareHouse(serviceEntity.getWareHouse());
		temp.setRefNo2(serviceEntity.getRefNo2());
		temp.setShade(serviceEntity.getShade());
		temp.setCompartment(serviceEntity.getCompartment());
		temp.setStackNo(serviceEntity.getStackNo());
		temp.setStackQty(serviceEntity.getStackQty());
		if (serviceEntity.getDepositeDate() != null)
			temp.setDepositeDate(serviceEntity.getDepositeDate());
		temp.setCommodityName(serviceEntity.getCommodityName());
		
		temp.setWmsServiceFlag(true);

		return temp;
	}
	
	
	/**
	 * @author Vijay Chougule
	 * project :- Fumigation Tracker
	 * Des :- when wms (fumigation) services mark completed then updating its status in fumigationReportEntity
	 * if condition for When wms fumigation service is completed then its status and date updating in fumigationReportEntity.
	 * else condition for when wms prophylactic and degassing service is completed then its status and date updating in fumigationReportEntity
	 */
	public String UpdateServiceCompletetionInFumigationServiceReport(Service service) {
		logger.log(Level.SEVERE, "Service completion uodation in fumigation Report");
		if(service.getProduct().getProductCode().equals("STK-01")){
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				fumigationReport.setFumigationServiceCompletionDate(service.getServiceCompletionDate());
				if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals("")){
					fumigationReport.setFumigationCompletionRemark(service.getServiceCompleteRemark());
				}
				else{
					fumigationReport.setFumigationCompletionRemark(service.getRemark());

				}
				fumigationReport.setStackQuantity(service.getStackQty());
				fumigationReport.setFumigationTechnicianName(service.getEmployee());
				fumigationReport.setRescheduleCount(service.getListHistory().size());
				fumigationReport.setFumigationServiceStatus(service.getStatus());// service completion status updation in report
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
		else{
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getFumigationServiceId()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				
				fumigationReport.setStackQuantity(service.getStackQty());
				if(service.getProduct().getProductCode().equals("PHM-01")){
					fumigationReport.setProphylacticStatus(Service.SERVICESTATUSCOMPLETED);
					fumigationReport.setProphylacticServiceCompletionDate(service.getServiceCompletionDate());
					fumigationReport.setProphylacticCompletionRemark(service.getServiceCompleteRemark());
					if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals("")){
						fumigationReport.setProphylacticCompletionRemark(service.getServiceCompleteRemark());
					}
					else{
						fumigationReport.setProphylacticCompletionRemark(service.getRemark());

					}
					fumigationReport.setProphylacticTechnicianName(service.getEmployee());
					fumigationReport.setRescheduleCount(service.getListHistory().size());
				}
				else{
					fumigationReport.setDegassingStatus(Service.SERVICESTATUSCOMPLETED);
					fumigationReport.setDeggasingServiceCompletionDate(service.getServiceCompletionDate());
//					fumigationReport.setDegassingCompletionRemark(service.getServiceCompleteRemark());
					if(service.getServiceCompleteRemark()!=null && !service.getServiceCompleteRemark().equals("")){
						fumigationReport.setDegassingCompletionRemark(service.getServiceCompleteRemark());
					}
					else{
						fumigationReport.setDegassingCompletionRemark(service.getRemark());

					}
					fumigationReport.setDegassingTechnicianName(service.getEmployee());
					fumigationReport.setRescheduleCount(service.getListHistory().size());
				}
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
		return "done";
	}
	
	public String createFumigationDeggassingService(List<Service> servicelist, String productCode, Date tcompletionDate, String remarkfromsystem, String deggassingstatus) {
		String strresponse = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		ArrayList<SuperModel> deggassingServicelist = new ArrayList<SuperModel>();
		ServiceProduct serviceProd = ofy().load().type(ServiceProduct.class).filter("productCode", productCode)
				.filter("companyId", servicelist.get(0).getCompanyId()).first().now();
		logger.log(Level.SEVERE, "serviceProd"+serviceProd);
		try {
			
		if(serviceProd!=null){
			for(Service serviceEntity : servicelist){
				logger.log(Level.SEVERE, "serviceProd");
				Service temp = new Service();
				temp.setRefNo(serviceEntity.getRefNo()); // inward transaction id
				temp.setPersonInfo(serviceEntity.getPersonInfo());
				temp.setContractCount(serviceEntity.getContractCount());
				temp.setProduct(serviceProd);
				temp.setBranch(serviceEntity.getBranch());
				
				temp.setContractStartDate(serviceEntity.getContractStartDate());
				temp.setContractEndDate(serviceEntity.getContractEndDate());
				temp.setAdHocService(false);
				temp.setServiceIndexNo(serviceEntity.getServiceIndexNo()+1);
				temp.setServiceType(serviceEntity.getServiceType());
				temp.setServiceSerialNo(serviceEntity.getServiceSerialNo());
				Date servicecompletionDateWit7Days = null;
				if(tcompletionDate!=null){
					servicecompletionDateWit7Days = tcompletionDate;
				}
				else{
					servicecompletionDateWit7Days = serviceEntity.getServiceCompletionDate();
				}
				Calendar c4 = Calendar.getInstance();
				c4.setTime(servicecompletionDateWit7Days);
				c4.add(Calendar.DATE, +7);
				logger.log(Level.SEVERE, "Date after 7 Days===" + servicecompletionDateWit7Days);
				servicecompletionDateWit7Days = sdf.parse(sdf.format(c4.getTime()));
				logger.log(Level.SEVERE, "Date after 7 Days" + servicecompletionDateWit7Days);

				temp.setServiceDate(servicecompletionDateWit7Days);
				temp.setOldServiceDate(servicecompletionDateWit7Days);
				temp.setAddress(serviceEntity.getAddress());

				temp.setServiceTime(serviceEntity.getServiceTime());
				temp.setServiceBranch(serviceEntity.getServiceBranch());
				temp.setServiceDateDay(serviceEntity.getServiceDateDay());

				temp.setBranch(serviceEntity.getBranch());
				temp.setServicingTime(serviceEntity.getServicingTime());

				temp.setServiceSrNo(serviceEntity.getServiceSerialNo());
				temp.setCompanyId(serviceEntity.getCompanyId());
				/**
				 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation
				 * Services for every Stack This code only executed from
				 * Web Services of NBHC Fumigation service
				 */
				temp.setWareHouse(serviceEntity.getWareHouse());
				temp.setRefNo2(serviceEntity.getRefNo2());
				temp.setShade(serviceEntity.getShade());
				temp.setCompartment(serviceEntity.getCompartment());
				temp.setStackNo(serviceEntity.getStackNo());
				temp.setCommodityName(serviceEntity.getCommodityName());
				temp.setStackQty(serviceEntity.getStackQty());
				
				if(deggassingstatus!=null && !deggassingstatus.equals("")){
					temp.setStatus(deggassingstatus);
				}
				else{
					temp.setStatus(Service.SERVICESTATUSSCHEDULE);

				}
				if(deggassingstatus.equals(Service.SERVICESTATUSCOMPLETED)){
					temp.setServiceCompleteRemark(remarkfromsystem);
					temp.setServiceCompletionDate(temp.getServiceDate());
				}
				
				
				if (serviceEntity.getDepositeDate() != null)
					temp.setDepositeDate(serviceEntity.getDepositeDate());
				temp.setWmsServiceFlag(true);
				temp.setFumigationServiceId(serviceEntity.getCount());
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
				int degassingServiceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), 1);
				temp.setCount(degassingServiceId);
				ofy().save().entity(temp);
				
				serviceEntity.setDeggassingServiceId(degassingServiceId);
				ofy().save().entity(serviceEntity);
				
				
				logger.log(Level.SEVERE, "New service Deggassing scheduled successfully with 7 Days");
				updateFumigationReport(serviceEntity,degassingServiceId,temp.getOldServiceDate());
				strresponse = "New service Deggassing scheduled successfully with 7 Days";
				
				if(temp.equals(Service.SERVICESTATUSCOMPLETED)){
					String updatestatus = UpdateServiceCompletetionInFumigationServiceReport(temp);
					
					/**
					 * Date 10-12-2019 by Vijay 
					 * Des :- Degassing Services Tcompleted or completed then updated this status in its fumigation Service
					 */
					if(temp.isWmsServiceFlag() && temp.getProduct()!=null && temp.getProduct().getProductCode().trim().equals("DSSG")){
						if(temp.getFumigationServiceId()!=0){
							Service fumigationService = ofy().load().type(Service.class).filter("companyId", temp.getCompanyId())
									.filter("count", temp.getFumigationServiceId()).first().now();
							if(fumigationService!=null){
								fumigationService.setDegassingServiceStatus(Service.SERVICESTATUSCOMPLETED);
								ofy().save().entities(fumigationService);
								logger.log(Level.SEVERE, "Deggasing Service status updated in its fumigation service");
								
								String updatstatus = UpdateServiceCompletetionInFumigationServiceReport(serviceEntity);

							}
						}
						
					}
				}


			}
		
		}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
			strresponse = e.getMessage();
		}
		return strresponse;
	}

/**
 * @author Vijay Chougule
 * Des :- here i am updating fumigation report entity with degassing service id and degassing service Date
 * @param serviceEntity
 * @param degassingServiceId
 * @param degassingServiceDate
 */
	private void updateFumigationReport(Service serviceEntity, int degassingServiceId, Date degassingServiceDate) {
		logger.log(Level.SEVERE, "Warehouse Code"+serviceEntity.getRefNo2());
		logger.log(Level.SEVERE, "compartment"+serviceEntity.getCompartment());
		logger.log(Level.SEVERE, "contractId"+serviceEntity.getContractCount());
		logger.log(Level.SEVERE, "shade"+serviceEntity.getShade());
		logger.log(Level.SEVERE, "fumigationServiceId "+serviceEntity.getCount());

		FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
												.filter("companyId", serviceEntity.getCompanyId()).filter("contractId", serviceEntity.getContractCount())
												.filter("warehouseCode", serviceEntity.getRefNo2()).filter("compartment", serviceEntity.getCompartment())
												.filter("shade", serviceEntity.getShade()).filter("fumigationServiceId", serviceEntity.getCount()).first().now();
		if(fumigationReport!=null){
			logger.log(Level.SEVERE, "fumigationReport for updating degassing Service id and Service Date"+fumigationReport);
			fumigationReport.setDeggasingServiceId(degassingServiceId);
			fumigationReport.setDeggasingServiceDate(degassingServiceDate);
			fumigationReport.setFumigationServiceStatus(Service.SERVICETCOMPLETED);
			ofy().save().entity(fumigationReport);
			logger.log(Level.SEVERE, "Degassing Service id and date updated in fumigation report");
		}
												
	}
	

	private void reactonuploadVendorProdcutPrice(long companyId, ArrayList<String> exceldatalist) {
		
		logger.log(Level.SEVERE, "Inside Vendor Product Price Uplaod task queue");


		if (exceldatalist.get(0).trim().equalsIgnoreCase("Title")
				&& exceldatalist.get(1).trim()
						.equalsIgnoreCase("Product Id")
				&& exceldatalist.get(2).trim()
						.equalsIgnoreCase("Product Name")
				&& exceldatalist.get(3).trim()
						.equalsIgnoreCase("Vendor Id")
				&& exceldatalist.get(4).trim()
						.equalsIgnoreCase("Vendor Name")
				&& exceldatalist.get(5).trim()
						.equalsIgnoreCase("Vendor Price")
				&& exceldatalist.get(6).trim()
						.equalsIgnoreCase("From Date")
				&& exceldatalist.get(7).trim()
						.equalsIgnoreCase("To Date") ){
			
			  	reactOnVendorProductPrice(companyId, exceldatalist);
		}
		else{
			logger.log(Level.SEVERE,"Invalid Upload File");
		}
		
	}



	/**
	 * @author Vijay Chougule Date :- 16-07-2020
	 * Des :- Vendor Product Price Upload
	 */

	private void reactOnVendorProductPrice(long companyId,
			ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Vendor Product Price Uplaod task queue method == ");

		List<ItemProduct> productlist = ofy().load().type(ItemProduct.class).filter("companyId", companyId).list();
		logger.log(Level.SEVERE, "productlist size"+productlist.size());
		
		
		List<Vendor> vendorlist = ofy().load().type(Vendor.class).filter("companyId", companyId).list();
		logger.log(Level.SEVERE, "vendorlist size"+vendorlist.size());

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		
		ArrayList<PriceList> pricelistarray = new ArrayList<PriceList>();
		
		for(int i=8; i<exceldatalist.size();i+=8){
			
			PriceList pricelist = new PriceList();
			
			ItemProduct product = getproduct(exceldatalist.get(i+1).trim(),productlist);
			
			
			if(!exceldatalist.get(i).trim().equalsIgnoreCase("NA")){
				pricelist.setPricelistTitle(exceldatalist.get(i).trim());
			}
			else{
				pricelist.setPricelistTitle(product.getProductName());
			}
			
			if(product!=null){
				
				pricelist.setProdID(product.getCount());
				pricelist.setProdName(product.getProductName());
				pricelist.setProdCode(product.getProductCode());
				pricelist.setProdCategory(product.getProductCategory());
				pricelist.setProdPrice(product.getPrice());
				pricelist.setUnitOfmeasurement(product.getUnitOfMeasurement());
			}
			
			
			Vendor vendor = getVendor(exceldatalist.get(i+3).trim(),vendorlist);
			ArrayList<PriceListDetails> list = new ArrayList<PriceListDetails>();
			
			if(vendor!=null){
				PriceListDetails vednorpricedetails = new PriceListDetails();
				vednorpricedetails.setCount(vendor.getCount());
				vednorpricedetails.setFullName(vendor.getVendorName());
				vednorpricedetails.setCellNumber(vendor.getCellNumber1());
				vednorpricedetails.setProductName(product.getProductName());
				vednorpricedetails.setProductCode(product.getProductCode());
				vednorpricedetails.setProductCategory(product.getProductCategory());
				vednorpricedetails.setProductPrice(Double.parseDouble(exceldatalist.get(i+5).trim()));
				vednorpricedetails.setUnitofMeasure(product.getUnitOfMeasurement());
				vednorpricedetails.setProdID(product.getCount());
				vednorpricedetails.setPocName(vendor.getPointOfContact().getFullname());
				
				if(!exceldatalist.get(i+6).trim().equalsIgnoreCase("NA")){
					
					try {
						vednorpricedetails.setFromdate(dateFormat.parse(exceldatalist.get(i+6).trim()));
						logger.log(Level.SEVERE, "From Date "+vednorpricedetails.getFromdate());

					} catch (ParseException e) {
						logger.log(Level.SEVERE, "Exception in from Date"+e.getMessage());
						e.printStackTrace();
					}
				}
				else{
					vednorpricedetails.setFromdate(DateUtility.getDateWithTimeZone("IST", new Date()));
				}
				
				if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
					try {
						vednorpricedetails.setTodate(dateFormat.parse(exceldatalist.get(i+7).trim()));
						logger.log(Level.SEVERE, "To Date "+vednorpricedetails.getTodate());

					} catch (ParseException e) {
						logger.log(Level.SEVERE, "Exception in To Date"+e.getMessage());
						e.printStackTrace();
					}
				}
				else{
					vednorpricedetails.setTodate(getTodate(vednorpricedetails.getFromdate(),dateFormat));
				}
				
				
				list.add(vednorpricedetails);
				
				pricelist.setPriceInfo(list);
			}
			
			pricelistarray.add(pricelist);
		}
		
		HashMap<Integer, ArrayList<PriceList>> pricelistHashmap = new HashMap<Integer, ArrayList<PriceList>>();
		
		for(PriceList pricelistEntity : pricelistarray){
			
			if(pricelistHashmap!=null && pricelistHashmap.size()!=0){
				
				if(pricelistHashmap.containsKey(pricelistEntity.getProdID())){
					pricelistHashmap.get(pricelistEntity.getProdID()).add(pricelistEntity);
				}
				else{
					ArrayList<PriceList> list = new  ArrayList<PriceList>();
					list.add(pricelistEntity);
					pricelistHashmap.put(pricelistEntity.getProdID(), list);
				}
			}
			else{
				ArrayList<PriceList> list = new  ArrayList<PriceList>();
				list.add(pricelistEntity);
				pricelistHashmap.put(pricelistEntity.getProdID(), list);
			}
		}
		
		
		for (Map.Entry<Integer, ArrayList<PriceList>> entryset : pricelistHashmap.entrySet()) {

//			PriceList pricelistEntity = new PriceList();

			ArrayList<PriceList> listpricelist = entryset.getValue();
			
			logger.log(Level.SEVERE, "listpricelist size"+listpricelist.size());

			PriceList pricelistEntity = null;

			
			boolean updateflag = false;
			if(listpricelist.size()!=0){
				try {
					
				
				pricelistEntity = ofy().load().type(PriceList.class).filter("companyId", companyId)
									  .filter("prodID", listpricelist.get(0).getProdID()).first().now();
				logger.log(Level.SEVERE,"this is for update pricelist =="+pricelistEntity);
				
				if(pricelistEntity!=null){
					updateflag = true;
				}
				else{
					pricelistEntity = new PriceList();
					logger.log(Level.SEVERE,"this is for new entry pricelist ==");
				}
				logger.log(Level.SEVERE,"updateflag"+updateflag);

				if(!updateflag){
				
				pricelistEntity.setPricelistTitle(listpricelist.get(0).getPricelistTitle());
				System.out.println("PRODUCT ID"+listpricelist.get(0).getCount());
				pricelistEntity.setProdID(listpricelist.get(0).getProdID());
				pricelistEntity.setProdName(listpricelist.get(0).getProdName());
				pricelistEntity.setProdCode(listpricelist.get(0).getProdCode());
				pricelistEntity.setProdCategory(listpricelist.get(0).getProdCategory());
				pricelistEntity.setProdPrice(listpricelist.get(0).getProdPrice());
				pricelistEntity.setUnitOfmeasurement(listpricelist.get(0).getUnitOfmeasurement());
				
				}
				
//				ArrayList<PriceListDetails> list = new ArrayList<PriceListDetails>();
				List<PriceListDetails> list = null;

				if(updateflag){
					list = pricelistEntity.getPriceInfo();
				}
				else{
					list = new ArrayList<PriceListDetails>();
				}
				
				for(PriceList pricelist : listpricelist){
					
					for(PriceListDetails pricelistdetails : pricelist.getPriceInfo()){
						PriceListDetails vednorpricedetails = new PriceListDetails();
						vednorpricedetails.setCount(pricelistdetails.getVendorID());
						vednorpricedetails.setFullName(pricelistdetails.getFullName());
						vednorpricedetails.setCellNumber(pricelistdetails.getCellNumber());
						vednorpricedetails.setProductName(pricelistdetails.getProductName());
						vednorpricedetails.setProductCode(pricelistdetails.getProductCode());
						vednorpricedetails.setProductCategory(pricelistdetails.getProductCategory());
						vednorpricedetails.setProductPrice(pricelistdetails.getProductPrice());
						vednorpricedetails.setUnitofMeasure(pricelistdetails.getUnitofMeasure());
						vednorpricedetails.setProdID(pricelistdetails.getProdID());
						vednorpricedetails.setPocName(pricelistdetails.getPocName());
						
						vednorpricedetails.setFromdate(pricelistdetails.getFromdate());
						vednorpricedetails.setTodate(pricelistdetails.getTodate());
						list.add(vednorpricedetails);
					}
					
					
				}
				if(list.size()!=0){
					pricelistEntity.setPriceInfo(list);

				}

				pricelistEntity.setCompanyId(companyId);
				
				GenricServiceImpl genimpl = new GenricServiceImpl();
				genimpl.save(pricelistEntity);
				
				logger.log(Level.SEVERE, "Vendor Product price saved successfully");
				
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			
			
			
		}	
	}


	private Date getTodate(Date date, SimpleDateFormat dateFormat) {

		Calendar cal3=Calendar.getInstance();
		cal3.setTime(date);
		cal3.add(Calendar.DATE, +365);
		
		Date nextYearDate=null;
		
		try {
			nextYearDate=dateFormat.parse(dateFormat.format(cal3.getTime()));
			nextYearDate=cal3.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return nextYearDate;
	}


	private Vendor getVendor(String strVendorId, List<Vendor> vendorlist) {
		int vendorId = Integer.parseInt(strVendorId);
		
		for(Vendor vendor : vendorlist){
			if(vendor.getCount()==vendorId){
				return vendor;
			}
		}
		
		return null;
	}



	private ItemProduct getproduct(String strproductId, List<ItemProduct> productlist) {
		int productId = Integer.parseInt(strproductId);
		
		for(ItemProduct product : productlist){
			if(product.getCount()==productId){
				return product;
			}
		}
		
		return null;
	}
	
	
	private void reactonCTCTemplateUpload(long companyId) {
		
		try {
		
		ArrayList<String> exceldatalist = new ArrayList<String>();
		exceldatalist.addAll(readExcelfile());
		logger.log(Level.SEVERE, "excel list after reading data exceldatalist size  =="+exceldatalist.size());
		
		int columncount = getColumnCount(exceldatalist,"Bonus (Correspondence Name)");
		logger.log(Level.SEVERE, "columncount"+columncount);
		
		if(columncount!=0){
			columncount++;
			
			int startcolumncount = columncount;
			logger.log(Level.SEVERE, "startcolumncount"+startcolumncount);

			
			List<CtcComponent> ctcComponentlist = ofy().load().type(CtcComponent.class).filter("companyId", companyId).list();
			logger.log(Level.SEVERE, "ctcComponentlist"+ctcComponentlist.size());
			
			List<ProvidentFund> providentfundlist = ofy().load().type(ProvidentFund.class).filter("companyId", companyId).list();
			logger.log(Level.SEVERE, "providentfundlist"+providentfundlist.size());
			
			List<Esic> esiclist = ofy().load().type(Esic.class).filter("companyId", companyId).list();
			logger.log(Level.SEVERE, "esiclist"+esiclist.size());

			int earningStartColumn = 5;
			int employeeDeductionStartColumn = getEmployeeDeductionColumnNumber(exceldatalist,"Employee Deduction");
			logger.log(Level.SEVERE, "employeeDeductionStartColumn"+employeeDeductionStartColumn);

			int companyDeductionStartColumn = getEmployeeDeductionColumnNumber(exceldatalist,"Company Deduction");
			logger.log(Level.SEVERE, "companyDeductionStartColumn"+companyDeductionStartColumn);

			int otherStartColumn = getEmployeeDeductionColumnNumber(exceldatalist,"Other");
			logger.log(Level.SEVERE, "otherStartColumn"+otherStartColumn);
			
			ArrayList<CTCTemplate> ctctemplatelist = new ArrayList<CTCTemplate>();
			
			for(int i=columncount;i<exceldatalist.size();i+=columncount){

				CTCTemplate ctctemplate = new CTCTemplate();

				double totalEarningAmt = 0;
				double totalEmployeeDeductionAmt=0;
				double totalCompanyDeductionAmt=0;
				
				
				logger.log(Level.SEVERE, "exceldatalist.get(i+1) == "+exceldatalist.get(i+1));

				if(!exceldatalist.get(i+1).equalsIgnoreCase("NA")){
					ctctemplate.setCtcTemplateName(exceldatalist.get(i+1));
				}
				
				if(!exceldatalist.get(i+2).equalsIgnoreCase("NA")){
					ctctemplate.setMonthlyCtcAmount(Double.parseDouble(exceldatalist.get(i+2)));
					double totalctcamt = (Double.parseDouble(exceldatalist.get(i+2))*12);
					ctctemplate.setTotalCtcAmount(totalctcamt);
				}
				
				
				if(!exceldatalist.get(i+3).equalsIgnoreCase("NA")){
					ctctemplate.setGender(exceldatalist.get(i+3));
				}
				
				if(!exceldatalist.get(i+4).equalsIgnoreCase("NA")){
					ctctemplate.setCategory(exceldatalist.get(i+4));
				}
				
				ArrayList<CtcComponent> earningComponnets = new ArrayList<CtcComponent>();
					if(earningStartColumn < employeeDeductionStartColumn){
						label :		for(int j=earningStartColumn;j<employeeDeductionStartColumn;j++){
						try {
							if(j==earningStartColumn){
								System.out.println("For Earning Column.");
							}
							else if(!exceldatalist.get(i+j).equalsIgnoreCase("NA")){
								String EarningComponentscolumnName = getColunName(exceldatalist,j);
								CtcComponent ctcomponent = new CtcComponent();
								ctcomponent = getearningComponent(ctcComponentlist,EarningComponentscolumnName);
								if(ctcomponent!=null){
									double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
									ctcomponent.setAmount(amt);
									earningComponnets.add(ctcomponent);
									totalEarningAmt +=amt;
								}
							}
						} catch (Exception e) {
						
						}
						
					}
				}
				
				if(earningComponnets.size()!=0){
					ctctemplate.setEarning(earningComponnets);
				}
				
				ArrayList<CtcComponent> deductionComponnets = new ArrayList<CtcComponent>();

				if(employeeDeductionStartColumn < companyDeductionStartColumn){
					label :	for(int j=employeeDeductionStartColumn;j<companyDeductionStartColumn;j++){
						try {
							System.out.println("exceldatalist.get(i+j) "+exceldatalist.get(i+j));
							if(j==employeeDeductionStartColumn){
								System.out.println("For Employee Deduction Column");
							}
							else if(!exceldatalist.get(i+j).equalsIgnoreCase("NA")){
								String employeeDeductionComponentscolumnName = getColunName(exceldatalist,j);
								if(employeeDeductionComponentscolumnName.equalsIgnoreCase("PF")){
									ProvidentFund profund= getPF(providentfundlist);
									CtcComponent ctcomponent = new CtcComponent();
									 ctcomponent = getProvidentFund(profund);
									if(ctcomponent!=null){
										double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
										ctcomponent.setAmount(amt);
										deductionComponnets.add(ctcomponent);
										totalEmployeeDeductionAmt+=amt;
									}
								}
								else if(employeeDeductionComponentscolumnName.equalsIgnoreCase("ESIC")){
									Esic esic = getESIC(esiclist);
									CtcComponent ctcomponent = getEsicList(esic);
									if(ctcomponent!=null){
										double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
										ctcomponent.setAmount(amt);
										ctcomponent.setAmount(amt);
										deductionComponnets.add(ctcomponent);
										totalEmployeeDeductionAmt+=amt;
									}
								}
								else{
									CtcComponent ctcomponent = new CtcComponent();
									ctcomponent = getEmployeeDeductionComponent(ctcComponentlist,employeeDeductionComponentscolumnName);
									if(ctcomponent!=null){
										double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
										ctcomponent.setAmount(amt);
										deductionComponnets.add(ctcomponent);
										totalEmployeeDeductionAmt+=amt;
									}
								}
								
							}
						} catch (Exception e) {

						}
					}
				}
				if(deductionComponnets.size()!=0){
					ctctemplate.setDeduction(deductionComponnets);
				}
				
				
				ArrayList<CtcComponent> companyContribution = new ArrayList<CtcComponent>();

				if(companyDeductionStartColumn < otherStartColumn){
					label : for(int j=companyDeductionStartColumn;j<otherStartColumn;j++){
						try {
							if(j==companyDeductionStartColumn){
								System.out.println("For Company Deduction Column");
							}
							else if(!exceldatalist.get(i+j).equalsIgnoreCase("NA")){
								String companyContributionComponentscolumnName = getColunName(exceldatalist,j);

								if(companyContributionComponentscolumnName.equalsIgnoreCase("PF")){
									ProvidentFund profund= getPF(providentfundlist);
									CtcComponent ctcomponent = new CtcComponent();
									ctcomponent = getCompanyProvidentFund(profund);
									if(ctcomponent!=null){
										double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
										ctcomponent.setAmount(amt);
										companyContribution.add(ctcomponent);
										totalCompanyDeductionAmt+=amt;
									}
								}
								else if(companyContributionComponentscolumnName.equalsIgnoreCase("ESIC")){
									Esic esic = getESIC(esiclist);
									CtcComponent ctcomponent = new CtcComponent();
									ctcomponent = getCompanyEsicList(esic);
									if(ctcomponent!=null){
										double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
										ctcomponent.setAmount(amt);
										companyContribution.add(ctcomponent);
										totalCompanyDeductionAmt+=amt;
									}
								}
								else{
									CtcComponent ctcomponent = new CtcComponent();
									ctcomponent = getCompanyDeductionComponent(ctcComponentlist,companyContributionComponentscolumnName);
									if(ctcomponent!=null){
										double amt = ((Double.parseDouble(exceldatalist.get(i+j)))*12);
										ctcomponent.setAmount(amt);
										companyContribution.add(ctcomponent);
										totalCompanyDeductionAmt +=amt;
									}
								}
								
							}
						} catch (Exception e) {
						}
					}
				}
				if(companyContribution.size()!=0){
					ctctemplate.setCompanyContribution(companyContribution);
				}
				
				if(!exceldatalist.get(i+otherStartColumn+1).equalsIgnoreCase("NA")){
					try {
						ctctemplate.setPaidLeaveAmt(Double.parseDouble(exceldatalist.get(i+otherStartColumn+1)));
						ctctemplate.setPaidLeavesAvailable(true);
						ctctemplate.setPlIncludedInEarning(true);
					} catch (Exception e) {

					}

				}
				else {
					ctctemplate.setPaidLeavesAvailable(false);
				}
				
				if(!exceldatalist.get(i+otherStartColumn+2).equalsIgnoreCase("NA")){
					if(ctctemplate.isPaidLeavesAvailable()){
						ctctemplate.setPlName(exceldatalist.get(i+otherStartColumn+2));
					}

				}
				if(!exceldatalist.get(i+otherStartColumn+3).equalsIgnoreCase("NA")){
					if(ctctemplate.isPaidLeavesAvailable()){
						ctctemplate.setPlCorresponadenceName(exceldatalist.get(i+otherStartColumn+3));
					}
				}

				if(!exceldatalist.get(i+otherStartColumn+4).equalsIgnoreCase("NA")){
					try {
						ctctemplate.setBonusAmt(Double.parseDouble(exceldatalist.get(i+otherStartColumn+4)));
						ctctemplate.setBonus(true);
						ctctemplate.setBonusIncludedInEarning(true);
					} catch (Exception e) {

					}

				}
				if(!exceldatalist.get(i+otherStartColumn+5).equalsIgnoreCase("NA")){
					if(ctctemplate.isBonus()){
						ctctemplate.setBonusName(exceldatalist.get(i+otherStartColumn+5));
					}
				}
				if(!exceldatalist.get(i+otherStartColumn+6).equalsIgnoreCase("NA")){
					if(ctctemplate.isBonus()){
						ctctemplate.setBonusCorresponadenceName(exceldatalist.get(i+otherStartColumn+6));
					}
				}
				
				ctctemplate.setStatus(true);
				ctctemplate.setCompanyId(companyId);
				ctctemplate.setGrossEarning(totalEarningAmt);
				ctctemplate.setTotalDeductionAmount(totalEmployeeDeductionAmt);
				ctctemplate.setCompanyTotalDeduction(totalCompanyDeductionAmt);
				ctctemplate.setCtcCopy(ctctemplate.getTotalCtcAmount());
				ctctemplate.setMonthly(true);
				ctctemplatelist.add(ctctemplate);
				GenricServiceImpl impl = new GenricServiceImpl();
				ReturnFromServer server = new ReturnFromServer();
				server = impl.save(ctctemplate);
				logger.log(Level.SEVERE, "CTC template saved succussfully"+server.count);
//				Thread.sleep(3000);
//				logger.log(Level.SEVERE,"after sleep mode");
//				System.out.println("after sleep mode");
			}
			
		}
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception "+e.getMessage());
			logger.log(Level.SEVERE, "Exception "+e);

		}
	}

	private CtcComponent getCompanyEsicList(Esic esic) {
		CtcComponent comp1=new CtcComponent();
		comp1.setCtcComponentName("Gross-Washing");
		comp1.setDeduction(true);
		comp1.setIsRecord(true);
		comp1.setName(esic.getEsicName());
		comp1.setShortName(esic.getEsicShortName());
		comp1.setMaxPerOfCTC(esic.getEmployerContribution());
		comp1.setCondition(">");
		comp1.setConditionValue((int) esic.getEmployerToAmount());
		comp1.setFromRangeAmount(esic.getEmployerFromAmount());
		comp1.setToRangeAmount(esic.getEmployerToAmount());
		return comp1;
	}



	private CtcComponent getCompanyProvidentFund(ProvidentFund pf) {
		CtcComponent comp1=new CtcComponent();
		comp1.setCtcComponentName("Gross-HRA");
		comp1.setDeduction(true);
		comp1.setIsRecord(true);
		comp1.setName(pf.getPfName());
		comp1.setShortName(pf.getPfShortName());
		comp1.setMaxPerOfCTC(pf.getEmployerContribution());
		comp1.setPercent1(pf.getEmpoyerPF());
		comp1.setPercent2(pf.getEdli());
		return comp1;
	}

	protected CtcComponent getEsicList(Esic esic) {
			CtcComponent comp=new CtcComponent();
			comp.setCtcComponentName("Gross-Washing");
			comp.setDeduction(true);
			comp.setIsRecord(false);
			comp.setName(esic.getEsicName());
			comp.setShortName(esic.getEsicShortName());
			comp.setMaxPerOfCTC(esic.getEmployeeContribution());
			comp.setCondition(">");
			comp.setConditionValue((int) esic.getEmployeeToAmount());
			
			comp.setFromRangeAmount(esic.getEmployeeFromAmount());
			comp.setToRangeAmount(esic.getEmployeeToAmount());
			
		return comp;
	}

	private Esic getESIC(List<Esic> esiclist) {
		for(Esic esic : esiclist){
			if(esic.getEsicName().equals("ESIC")){
				return esic;
			}
		}
		return null;
	}

	private ProvidentFund getPF(List<ProvidentFund> providentfundlist) {
		for(ProvidentFund pfdetails : providentfundlist){
			if(pfdetails.getPfName().equals("PF")){
				return pfdetails;
			}
		}
		return null;
	}


	private CtcComponent getProvidentFund(ProvidentFund providentfund) {
			
				CtcComponent comp=new CtcComponent();
				comp.setCtcComponentName("Gross-HRA");
				comp.setDeduction(true);
				comp.setIsRecord(false);
				comp.setName(providentfund.getPfName());
				comp.setShortName(providentfund.getPfShortName());
				comp.setMaxPerOfCTC(providentfund.getEmployeeContribution());
				
			return comp;
		
	}













	private CtcComponent getearningComponent(List<CtcComponent> CtcComponentlist,String ComponentscolumnName) {
		for(CtcComponent ctccomponent : CtcComponentlist){
			if((ctccomponent.getName().equals(ComponentscolumnName)||ctccomponent.getShortName().equals(ComponentscolumnName)) && ctccomponent.isDeduction()==false 
					&& ctccomponent.getIsRecord()==false){
				return ctccomponent;
			}
		}
		return null;
	}

	private CtcComponent getEmployeeDeductionComponent(List<CtcComponent> CtcComponentlist,String ComponentscolumnName) {
		for(CtcComponent ctccomponent : CtcComponentlist){
			if(ctccomponent.isDeduction() && ctccomponent.getIsRecord()==false && (ctccomponent.getName().equals(ComponentscolumnName)||ctccomponent.getShortName().equals(ComponentscolumnName))){
				return ctccomponent;
			}
		}
		return null;
	}
	
	private CtcComponent getCompanyDeductionComponent(List<CtcComponent> CtcComponentlist,String ComponentscolumnName) {
		for(CtcComponent ctccomponent : CtcComponentlist){
			if(ctccomponent.isDeduction() && ctccomponent.getIsRecord() && (ctccomponent.getName().equals(ComponentscolumnName)||ctccomponent.getShortName().equals(ComponentscolumnName))){
				return ctccomponent;
			}
		}
		return null;
	}

	private String getColunName(ArrayList<String> exceldatalist, int j) {
		return exceldatalist.get(j);
	}

	private int getEmployeeDeductionColumnNumber(ArrayList<String> exceldatalist, String ColumnaName) {
		
		for(int i=0;i<exceldatalist.size();i++){
			if(exceldatalist.get(i).equalsIgnoreCase(ColumnaName)){
				System.out.println("column number"+i);
				return i;
			}
		}
		
		return 0;
	}


	private int getColumnCount(ArrayList<String> exceldatalist, String lastColumnName) {
		for(int i=0;i<exceldatalist.size();i++){
			if(exceldatalist.get(i).equals(lastColumnName)){
				return i;
			}
		}
		return 0;
	}

	/**
	 * @author Vijay Chougule Date :- 30-07-2020
	 * Des :- reading excel data with trim fuction
	 */
	private ArrayList<String> readExcelfile() {
		
		ArrayList<String> exceldatalist = new ArrayList<String>();

		/*******************
		 * Reading Excel Data & adding in list
		 *****************************/
		try {
			logger.log(Level.SEVERE, "Reading excel ");
			logger.log(Level.SEVERE, "Blobkey value" + blobkey);

			long i;
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(new BlobstoreInputStream(blobkey));
				
			} catch (EncryptedDocumentException e) {
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				e.printStackTrace();
			}
			Sheet sheet = wb.getSheetAt(0);
			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			// Traversing over each row of XLSX file
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						exceldatalist.add(cell.getStringCellValue().trim());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							exceldatalist.add(fmt.format(cell.getDateCellValue()));
						} else {

							// i = (long) cell.getNumericCellValue();
							// logger.log(Level.SEVERE, "Value : "+i);
							logger.log(Level.SEVERE, "Value : " + cell.getNumericCellValue());
							// exceldatalist.add(cell.getNumericCellValue() +
							// "");
							DecimalFormat decimalFormat = new DecimalFormat("#########.##");
							int value1 = 0, value2 = 0;
							String[] array = decimalFormat.format(cell.getNumericCellValue()).split("\\.");
							if (array.length > 0) {
								// value1 = Integer.parseInt(array[0]);
							}
							if (array.length > 1) {
								value2 = Integer.parseInt(array[1]);
							}
							if (value2 == 0) {
								i = (long) cell.getNumericCellValue();
								exceldatalist.add(i+"".trim());
							} else {
								exceldatalist.add(cell.getNumericCellValue()+"".trim());
							}

						}
						break;
					case Cell.CELL_TYPE_BOOLEAN:

						break;
					default:
					}
				}
			}

			System.out.println("Total size:" + exceldatalist.size());
			logger.log(Level.SEVERE, "FileString List Size:" + exceldatalist.size());

			wb.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*******************************
		 * end of excel data reading and adding in list
		 *******************/
		
		return exceldatalist;

		
	}
	
	private CustomerBranchDetails getcustomerBranch(String customerBranchName,	List<CustomerBranchDetails> globalCustomerBranchDetailsList) {
		for(CustomerBranchDetails branch : globalCustomerBranchDetailsList){
			if(branch.getBusinessUnitName().trim().equals(customerBranchName)){
				return branch;
			}
		}
		return null;
	}
	
	
	private void reactonUpdateAssetDetails(long companyId, ArrayList<String> exceldatalist) {

		
		logger.log(Level.SEVERE, "Inside Update Asset Details Uplaod task queue");


		if (exceldatalist.get(0).trim().equalsIgnoreCase("Customer Id")
				&& exceldatalist.get(1).trim()
					.equalsIgnoreCase("Customer Name")
			&& exceldatalist.get(2).trim()
					.equalsIgnoreCase("Contract Id")
			&& exceldatalist.get(3).trim()
					.equalsIgnoreCase("Service Name")
			&& exceldatalist.get(4).trim()
					.equalsIgnoreCase("Customer Branch")
			&& exceldatalist.get(5).trim()
					.equalsIgnoreCase("Asset Id")
			&& exceldatalist.get(6).trim()
					.equalsIgnoreCase("Component Name")
			&& exceldatalist.get(7).trim()
					.equalsIgnoreCase("Mfg No") 
			&& exceldatalist.get(8).trim()
					.equalsIgnoreCase("Mfg Date")  
			&& exceldatalist.get(9).trim()
					.equalsIgnoreCase("Replacement Date") 	
			&& exceldatalist.get(10).trim()
					.equalsIgnoreCase("Unit") 	){
			
			  	updateAssetDetails(companyId, exceldatalist);
		}
		else{
			logger.log(Level.SEVERE,"Invalid Upload File");
		}
		
	}


	private void updateAssetDetails(long companyId,	ArrayList<String> exceldatalist) {
		
		HashSet<Integer> hsAssestId = new HashSet<Integer>();
		try {
			
		
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		
		for(int i=11; i<exceldatalist.size();i+=11){
			hsAssestId.add(Integer.parseInt(exceldatalist.get(i+5)));
		}
		
		ArrayList<Integer> arrayassetId = new ArrayList<Integer>(hsAssestId);

		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("assetId IN", arrayassetId).list();
		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
		
		/**
		 * @author Anil
		 * @since 19-10-2020
		 * updating asset/component details on contract master as well
		 */
		HashSet<Integer> hsContractId = new HashSet<Integer>();
		for(Service service:servicelist){
			hsContractId.add(service.getContractCount());
		}
		List<Contract> contractList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count IN", hsContractId).list();
		logger.log(Level.SEVERE, "contractList size"+contractList.size());

		
		for(int i=11; i<exceldatalist.size();i+=11){
			int assetId = Integer.parseInt(exceldatalist.get(i+5).trim());
			ArrayList<Service> serviceEntitylist = getAssetIdService(assetId,servicelist);
			if(serviceEntitylist.size()!=0){
					for(Service serviceEntity : serviceEntitylist){
						if(!exceldatalist.get(i+7).trim().equalsIgnoreCase("NA")){
							serviceEntity.setMfgNo(exceldatalist.get(i+7).trim());
							
						}
						if(!exceldatalist.get(i+8).trim().equalsIgnoreCase("NA")){
							try {
								Date mfgDate = format.parse(exceldatalist.get(i+8).trim());
								serviceEntity.setMfgDate(mfgDate);
								logger.log(Level.SEVERE, "mfgDate "+mfgDate);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if(!exceldatalist.get(i+9).trim().equalsIgnoreCase("NA")){
							try {
								Date replacementDate = format.parse(exceldatalist.get(i+9).trim());
								serviceEntity.setReplacementDate(replacementDate);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if(!exceldatalist.get(i+10).trim().equalsIgnoreCase("NA")){
							serviceEntity.setAssetUnit(exceldatalist.get(i+10).trim());
						}
						
						
					}
					ofy().save().entities(serviceEntitylist).now();
					
					/**
					 * @author Anil
					 * @since 19-10-2020
					 * updating asset/component details on contract master as well
					 */
					updateAssetDetailsInContract(serviceEntitylist,contractList);
					
			}
		}
		
		if(contractList!=null&&contractList.size()!=0){
			ofy().save().entities(contractList).now();
		}
		
		logger.log(Level.SEVERE, "Asset Details Updated Succesfully ");

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception "+e.getCause());
		}	
		
	}
	private void updateAssetDetailsInContract(ArrayList<Service> serviceEntitylist, List<Contract> contractList) {
		// TODO Auto-generated method stub
		for(Service service:serviceEntitylist){
			for(Contract contract:contractList){
				if(service.getContractCount()==contract.getCount()){
					for(SalesLineItem item:contract.getItems()){
						if(item.getPrduct().getCount()==service.getProduct().getCount()&&item.getProductSrNo()==service.getServiceSrNo()){
						
							ArrayList<BranchWiseScheduling> branchWiseSchedulingList=null;
							if(item.getCustomerBranchSchedulingInfo()!=null){
								branchWiseSchedulingList=item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo());
							}
							if(branchWiseSchedulingList!=null){
								for(BranchWiseScheduling branch:branchWiseSchedulingList){
									
									if(branch.isCheck()&&branch.getAssetQty()!=0){
										if(branch.getComponentList()!=null&&branch.getComponentList().size()!=0){
											for(ComponentDetails obj:branch.getComponentList()){
												
												if(obj.getAssetId()==service.getAssetId()){
													logger.log(Level.SEVERE,"8getAssetId "+obj.getAssetId());
													obj.setComponentName(service.getComponentName());
													obj.setMfgNum(service.getMfgNo());
													obj.setMfgDate(service.getMfgDate());
													obj.setReplacementDate(service.getReplacementDate());
													obj.setAssetUnit(service.getAssetUnit());
												}
											}
										}
									}
								}
							}
							
							break;
						}
					}
				}
			}
		}
	}


























	private ArrayList<Service> getAssetIdService(int assetId, List<Service> allservicelist) {
		ArrayList<Service> servicelist = new ArrayList<Service>();
		for(Service service : allservicelist){
			 if(service.getAssetId()==assetId){
				 servicelist.add(service);
			 }
		 }
		return servicelist;
	}
	
	private void reactonupdateDuplicateServiceId(long companyId,List<Service> servicelist) {
		long number = servicelist.size();
		
		SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
		int serviceId=(int) numGen.getLastUpdatedNumber("Service", companyId, number);
		logger.log(Level.SEVERE, "serviceId "+serviceId);

		for(Service serviceEnity : servicelist){
			if((serviceEnity.getStatus().equals(Service.SERVICESTATUSSCHEDULE) || serviceEnity.getStatus().equals(Service.SERVICESTATUSRESCHEDULE))
					 && serviceEnity.getProduct().getProductCode().equals("DSSG")){
				int newserviceId = serviceId;
				Service fumigationService = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("count", serviceEnity.getFumigationServiceId()).first().now();
				if(fumigationService!=null){
					fumigationService.setDeggassingServiceId(newserviceId);
					ofy().save().entity(fumigationService);
				}
				serviceEnity.setCount(newserviceId);
				logger.log(Level.SEVERE, "newserviceId "+newserviceId);
				ofy().save().entity(serviceEnity);
				serviceId++;
				
			}
			else if((serviceEnity.getStatus().equals(Service.SERVICESTATUSSCHEDULE) || serviceEnity.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) 
						&& serviceEnity.getProduct().getProductCode().equals("PHM-01")){
				int newserviceId = serviceId;
				Service fumigationService = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("count", serviceEnity.getFumigationServiceId()).first().now();
				if(fumigationService!=null){
					fumigationService.setProphylacticServiceId(newserviceId);
					ofy().save().entity(fumigationService);
				}
				serviceEnity.setCount(newserviceId);
				logger.log(Level.SEVERE, "newserviceId== "+newserviceId);
				ofy().save().entity(serviceEnity);
				serviceId++;
			}
			
		}
	}

	
	private void reactonemployeeUpload(long companyId, ArrayList<String> exceldatalist,String loggedinuser) {
		logger.log(Level.SEVERE, "Inside Employee upload task queue");
		sendEmployeeDataInParts(exceldatalist, companyId,loggedinuser);//Ashwini Patil Date:16-05-2023 sendind employee upload adata in parts to task queue
//		dataimpl.saveEmployeeData(exceldatalist, companyId,loggedinuser);
	}
	
	private void sendEmployeeDataInParts(ArrayList<String> exceldatalist,long companyId, String loggedinuser) {
		logger.log(Level.SEVERE, " step 1 sendEmployeeDataInParts");
		JSONObject jobj = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		if(exceldatalist!=null){
			int datalength=exceldatalist.size();
			int slotSize=25;
			int noOfRecords=(datalength/69)-1;			
			logger.log(Level.SEVERE, " step 1 noOfRecords-"+noOfRecords);
			int noOfTaskqueueCalls=(int) Math.ceil(noOfRecords/25.0);
			logger.log(Level.SEVERE, " step 1 noOfTaskqueueCalls-"+noOfTaskqueueCalls);
			ArrayList<String> headers=new ArrayList<String>();
			ArrayList<String> dataSlot=null;
			
			for(int i=0;i<69;i++){
				headers.add(exceldatalist.get(i));
			}
			
			int start=69;
			int end=69+(69*slotSize);
			logger.log(Level.SEVERE, "start="+start+" end="+end);
			long millisec=1;
			
			for(int j=1;j<=noOfTaskqueueCalls;j++){
				jsonArray = new JSONArray();
				dataSlot=new ArrayList<String>();
				logger.log(Level.SEVERE, "dataSlot size before="+dataSlot.size()+" start="+start+" end="+end);
				dataSlot.addAll(headers);
				for(int k=start;k<end;k++){
					if(k<datalength)
						dataSlot.add(exceldatalist.get(k));					
				}
				
				logger.log(Level.SEVERE, "dataSlot size after="+dataSlot.size());
				
				for(String item:dataSlot){
					jsonArray.put(item);
				}
				try {
					jobj.put("datalist", jsonArray);
					String jsonString = jobj.toString();
					logger.log(Level.SEVERE, dataSlot.size()+"step1 jsonString="+jsonString);
					Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
					queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
							.param("OperationName", "employeeUploadPartly")
							.param("companyId", companyId+ "")
							.param("excelData",jsonString )
							.param("loggedinuser", loggedinuser).etaMillis(System.currentTimeMillis()+millisec));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				start=end;
				end=end +(69*slotSize);
				millisec=millisec+180000;
				logger.log(Level.SEVERE, j+" next start="+start+" next end="+end+" delay="+millisec);
			}
			
			
		}
	}



	private void reactonUpdateEmployee(long companyId,ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Update Employee task queue");

		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.updateEmployeeDataWithTaskQueue(companyId, exceldatalist);
	}
	
	
	private void reactonPaymentUpdation(long companyId, String strJsonData) {

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat sdformat = new SimpleDateFormat("dd-MMM-yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdformat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		SimpleDateFormat dateformat = new SimpleDateFormat("MM-dd-yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateformat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		try {
			JSONObject jsondata = new JSONObject(strJsonData);
			
			int paymentid = -1;
			int invoiceId = -1;
			try {
				 if(jsondata.optString("paymentId")!=null && !jsondata.optString("paymentId").equals("")&& !jsondata.optString("paymentId").equals("NA")&& !jsondata.optString("paymentId").equals("N A")){
					 logger.log(Level.SEVERE, "paymentid in json="+jsondata.optString("paymentId"));
					
					 paymentid = jsondata.optInt("paymentId");
				 }
				 
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				 invoiceId = jsondata.optInt("invoiceId");				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		//	logger.log(Level.SEVERE, "paymentid"+paymentid);
			logger.log(Level.SEVERE, "invoiceId "+invoiceId);

			
			CustomerPayment paymentEntity = null;
			
			if(paymentid!=0&&paymentid!=-1 && invoiceId!=-1){
				paymentEntity = ofy().load().type(CustomerPayment.class)
				.filter("count", paymentid).filter("invoiceCount", invoiceId).filter("companyId", companyId).first().now();
				logger.log(Level.SEVERE, "paymentEntity "+paymentEntity);
			}
			else{
				List<CustomerPayment> paymentList= ofy().load().type(CustomerPayment.class).filter("invoiceCount", invoiceId)
						.filter("companyId", companyId).filter("status", CustomerPayment.CREATED).list();
				double netreceivable = jsondata.optDouble("netReceivable");
				if(paymentList!=null) {
					Comparator<CustomerPayment> dueDate=new Comparator<CustomerPayment>() {
						@Override
						public int compare(CustomerPayment arg0, CustomerPayment arg1) {
							return arg0.getPaymentDate().compareTo(arg1.getPaymentDate());
						}
					};
					Collections.sort(paymentList, dueDate);
					for(CustomerPayment cp:paymentList) {
						if(cp.getPaymentAmt()>=netreceivable) {
							paymentEntity=cp;
							break;
						}
					}
				}
								
//				paymentEntity = ofy().load().type(CustomerPayment.class).filter("invoiceCount", invoiceId)
//								.filter("companyId", companyId).filter("status", CustomerPayment.CREATED).first().now();
				
				
				
			}
			

			if(paymentEntity!=null){
				logger.log(Level.SEVERE, "paymentEntity == "+paymentEntity.getCount());
				Date paymentDate = null;
				
				try {
					if(!jsondata.optString("paymentDate").equals("")){
						paymentDate = format.parse(jsondata.optString("paymentDate"));
						logger.log(Level.SEVERE, "paymentDate "+paymentDate);
						
//						try {
//							paymentDate = sdformat.parse(jsondata.optString("paymentDate"));
//							logger.log(Level.SEVERE, "paymentDate =="+paymentDate);
//
//						} catch (ParseException e1) {
//							// TODO Auto-generated catch block
//							e1.printStackTrace();
//						}

					}
				} catch (ParseException e) {
					logger.log(Level.SEVERE, "failed reading Payment Date");
					e.printStackTrace();
					if(!jsondata.optString("paymentDate").equals("")){
						try {
							paymentDate = sdformat.parse(jsondata.optString("paymentDate"));
							logger.log(Level.SEVERE, "paymentDate =="+paymentDate);
							String strdate = dateformat.format(paymentDate);
							logger.log(Level.SEVERE, "strdate =="+strdate);
							paymentDate = dateformat.parse(strdate);
							logger.log(Level.SEVERE, "paymentDate== =="+paymentDate);
		
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
				//Ashwini Patil Date:29-06-2023 commented this as Payment due date field was getting updated
//				if(paymentDate!=null){
//					paymentEntity.setPaymentDate(paymentDate);
//				}
				String paymentMode=jsondata.optString("paymentMode");
				String	paymentMethod = jsondata.optString("paymentMethod");
				double netreceivable = jsondata.optDouble("netReceivable");
				boolean istdsapplicable = false;
				double tdsAmount = 0;
				double tdsPecentage = 0;
				if(jsondata.optString("is Tds Applicable").equalsIgnoreCase("yes")){
					if(!jsondata.optString("Tds Pecentage").equalsIgnoreCase("")){
						tdsPecentage = Double.parseDouble(jsondata.optString("Tds Pecentage"));
					}
					if(!jsondata.optString("Tds Amount").equalsIgnoreCase("")){
						tdsAmount = Double.parseDouble(jsondata.optString("Tds Amount"));
					}
					paymentEntity.setTdsApplicable(true);
					paymentEntity.setTdsPercentage(tdsPecentage+"");
					paymentEntity.setTdsTaxValue(tdsAmount);

				}
				String chequeNumber = jsondata.optString("cheque Number");
				Date chequeDate = null;
				if(jsondata.optString("cheque Date")!=null && !jsondata.optString("cheque Date").equals("") ){
					try {
						chequeDate = format.parse(jsondata.optString("cheque Date"));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						if(jsondata.optString("cheque Date")!=null && !jsondata.optString("cheque Date").equals("") ){
							try {
								chequeDate = sdformat.parse(jsondata.optString("cheque Date"));
								logger.log(Level.SEVERE, "chequeDate =="+chequeDate);
								String strdate = dateformat.format(chequeDate);
								logger.log(Level.SEVERE, "strdate =="+strdate);
								chequeDate = dateformat.parse(strdate);
								logger.log(Level.SEVERE, "chequeDate == =="+chequeDate);
			
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}	
					}
				}
				
				String chequeBankName = jsondata.optString("cheque-Bank Name");
				String chequeBankAccNo = jsondata.optString("cheque-Bank Account No");
				String referenceNo = jsondata.optString("reference No");
				
				Date transferDate = null;
//				if(jsondata.optString("NEFT-Transfer Date")!=null && !jsondata.optString("NEFT-Transfer Date").equals("") ){
//					try {
//						transferDate = format.parse(jsondata.optString("NEFT-Transfer Date"));
//					} catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						if(jsondata.optString("NEFT-Transfer Date")!=null && !jsondata.optString("NEFT-Transfer Date").equals("") ){
//							try {
//								transferDate = sdformat.parse(jsondata.optString("NEFT-Transfer Date"));
//								logger.log(Level.SEVERE, "transferDate =="+transferDate);
//								String strdate = dateformat.format(transferDate);
//								logger.log(Level.SEVERE, "strdate =="+strdate);
//								transferDate = dateformat.parse(strdate);
//								logger.log(Level.SEVERE, "transferDate == =="+transferDate);
//			
//							} catch (ParseException e1) {
//								// TODO Auto-generated catch block
//								e1.printStackTrace();
//							}
//						}
//					}
//				}
				if(jsondata.optString("Payment Received Date")!=null && !jsondata.optString("Payment Received Date").equals("") ){
					try {
						transferDate = format.parse(jsondata.optString("Payment Received Date"));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						if(jsondata.optString("Payment Received Date")!=null && !jsondata.optString("Payment Received Date").equals("") ){
							try {
								transferDate = sdformat.parse(jsondata.optString("Payment Received Date"));
								logger.log(Level.SEVERE, "transferDate =="+transferDate);
								String strdate = dateformat.format(transferDate);
								logger.log(Level.SEVERE, "strdate =="+strdate);
								transferDate = dateformat.parse(strdate);
								logger.log(Level.SEVERE, "transferDate == =="+transferDate);
			
							} catch (ParseException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
				}
				paymentEntity.setChequeNo(chequeNumber);
				if(chequeDate!=null){
					paymentEntity.setChequeDate(chequeDate);
				}
				paymentEntity.setBankName(chequeBankName);
				paymentEntity.setBankAccNo(chequeBankAccNo);
				paymentEntity.setReferenceNo(referenceNo);
				
				if(paymentMode!=null&&!paymentMode.equals(""))
					paymentEntity.setPaymentMode(paymentMode);//Ashwini Patil Date:27-06-2023
					
				if(transferDate!=null){//&& !paymentMethod.contains("Cheque/DD") 
					paymentEntity.setAmountTransferDate(transferDate);
					if(paymentEntity.getPaymentDate()==null||paymentEntity.getPaymentDate().equals("") || ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CustomerPayment", AppConstants.PC_SETPAYMENTRECEIVEDATETOPAYMENTDUEDATE, companyId))
						paymentEntity.setPaymentDate(transferDate);
				}
				
				paymentEntity.setPaymentMethod(paymentMethod);
				if(jsondata.optString("Description")!=null && !jsondata.optString("Description").equals("") ){
					paymentEntity.setComment(jsondata.optString("Description"));
				}

				if(jsondata.optString("ref No")!=null && !jsondata.optString("ref No").equals("")) {
					paymentEntity.setRefNumber(jsondata.optString("ref No"));
				}
				paymentEntity.setNetPay(netreceivable);
				double amountrecievedpaid = netreceivable;
				if(tdsPecentage>0){
					tdsAmount = 0;
					tdsAmount = (paymentEntity.getBalanceRevenue()*tdsPecentage)/100;
					amountrecievedpaid +=tdsAmount;
				}
				else if(tdsAmount>0){
					amountrecievedpaid +=tdsAmount;
				}
				paymentEntity.setPaymentReceived(amountrecievedpaid);
				

				double taxAmount = paymentEntity.getBalanceTax();
				double adujustedTaxAmt = 0;
				double baseAmt = 0;
				System.out.println("taxAmount "+taxAmount);
				System.out.println("netreceivable "+netreceivable);
				
				if(netreceivable>taxAmount){
					adujustedTaxAmt = taxAmount;
					baseAmt = netreceivable - taxAmount;
				}
				else if(netreceivable<taxAmount){
					adujustedTaxAmt = netreceivable;
				}
				else{
					adujustedTaxAmt = taxAmount;

				}
				if(tdsAmount>0){
					baseAmt += tdsAmount;
				}
				System.out.println("baseAmt"+baseAmt);
				System.out.println("adujustedTaxAmt "+adujustedTaxAmt);
				
				paymentEntity.setReceivedRevenue(baseAmt);
				paymentEntity.setReceivedTax(adujustedTaxAmt);
				
				paymentEntity.setStatus(CustomerPayment.CLOSED);
				
				
				
				ofy().save().entity(paymentEntity);
				logger.log(Level.SEVERE, "paymentEntity.getPaymentReceived() ="+paymentEntity.getPaymentReceived()+" paymentEntity.getPaymentAmt()="+paymentEntity.getPaymentAmt());
				if(paymentEntity.getPaymentReceived()<paymentEntity.getPaymentAmt()){
					
					createbalanceAmountPaymentDocument(paymentEntity);
					logger.log(Level.SEVERE, "created balance Amount Payment Document");
				}
				
			}else {
				logger.log(Level.SEVERE, "No open payment document found");
			}
			
			
		} catch (JSONException e) {
			logger.log(Level.SEVERE, "failed reading json data in task queue");
			e.printStackTrace();
		}
		
	}


	private void createbalanceAmountPaymentDocument(CustomerPayment paymentEntity) {

		CustomerPayment paydtls=new CustomerPayment();
		
		
		paydtls.setPersonInfo(paymentEntity.getPersonInfo());
		paydtls.setContractCount(paymentEntity.getContractCount());
		paydtls.setTotalSalesAmount(paymentEntity.getTotalSalesAmount());
		paydtls.setArrayBillingDocument(paymentEntity.getArrayBillingDocument());
		paydtls.setTotalBillingAmount(paymentEntity.getTotalBillingAmount());
		paydtls.setInvoiceCount(paymentEntity.getInvoiceCount());
		if(paymentEntity.getInvoiceDate()!=null)
			paydtls.setInvoiceDate(paymentEntity.getInvoiceDate());
		if(paymentEntity.getInvoiceType()!=null)
			paydtls.setInvoiceType(paymentEntity.getInvoiceType());
		if(paymentEntity.getPaymentMethod()!=null)
			paydtls.setPaymentMethod(paymentEntity.getPaymentMethod());
		if(paymentEntity.getContractStartDate()!=null){
			paydtls.setContractStartDate(paymentEntity.getContractStartDate());
		}
		if(paymentEntity.getContractEndDate()!=null){
			paydtls.setContractEndDate(paymentEntity.getContractEndDate());
		}
		
		paydtls.setInvoiceAmount(paymentEntity.getInvoiceAmount());
			
		double payableAmt=paymentEntity.getPaymentAmt();
		double payreceived=paymentEntity.getPaymentReceived();
		int payAmt=(int) (payableAmt-payreceived);
		paydtls.setPaymentAmt(payAmt);
		
		if(paymentEntity.getPaymentDate()!=null)
			paydtls.setPaymentDate(paymentEntity.getPaymentDate());
		
		paydtls.setTypeOfOrder(paymentEntity.getTypeOfOrder());
		
		paydtls.setOrderCreationDate(paymentEntity.getOrderCreationDate());
		if(paymentEntity.getOrderCformStatus()!=null){
			paydtls.setOrderCformStatus(paymentEntity.getOrderCformStatus());
		}
		if(paymentEntity.getOrderCformPercent()!=-1){
			paydtls.setOrderCformPercent(paymentEntity.getOrderCformPercent());
		}
		paydtls.setOrderCreationDate(paymentEntity.getOrderCreationDate());
		
		paydtls.setAccountType(paymentEntity.getAccountType());
		
		paydtls.setStatus(CustomerPayment.CREATED);
		
		///   rohan added code for new payment should search with branch level restriction
		//    Date : 8/2/2017 
		paydtls.setBranch(paymentEntity.getBranch());
		
		/**
		 * added by vijay on Date 3 march 2017
		 */
		paydtls.setAddintoPettyCash(paymentEntity.isAddintoPettyCash());
		if(paymentEntity.getPettyCashName()!=null)
		paydtls.setPettyCashName(paymentEntity.getPettyCashName());
		
		/**
		 * ends here
		 */
		
		/**
		 * Date:25-11-2017 BY ANIL
		 * setting number range to newly generated payment document
		 */
		if(paymentEntity.getNumberRange()!=null){
			paydtls.setNumberRange(paymentEntity.getNumberRange());
		}
		/**
		 * End
		 */
		
		
		/** date 21/02/2018 added by komal for actual revenue and tax **/
		if(payreceived < paymentEntity.getPaymentAmt()){
			double tax = paymentEntity.getBalanceTax();
				
			if(payreceived < tax){
				paydtls.setBalanceRevenue(Math.round(paymentEntity.getBalanceRevenue() - paymentEntity.getReceivedRevenue()));
				paydtls.setBalanceTax(Math.round(tax - paymentEntity.getReceivedTax()));
			}
			if(payreceived == tax){
				paydtls.setBalanceRevenue(Math.round(paymentEntity.getBalanceRevenue()));
				paydtls.setBalanceTax(0);
			}
			if(payreceived > tax){
				paydtls.setBalanceRevenue(Math.round(paymentEntity.getBalanceRevenue()-paymentEntity.getReceivedRevenue()));
				paydtls.setBalanceTax(0);
			}
		}
		//Ashwini Patil Date:29-04-2024 Ref number was not getting mapped in part payments when payments updated through payment upload
		if(paymentEntity.getRefNumber()!=null)
			paydtls.setRefNumber(paymentEntity.getRefNumber());
		
		/*** Date 07-08-2020 Bug :- partial payment tax amount not showing and TDS set default as 0 **/
		paydtls.setTdsPercentage(0+"");
		double balanceTax = (paydtls.getPaymentAmt() - paydtls.getBalanceRevenue());
		paydtls.setBalanceTax(balanceTax);
		
		paydtls.setCompanyId(paymentEntity.getCompanyId());
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(paydtls);
		
	}
	
	private void reactonServiceProductUpload(long companyId, ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Service Product upload task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.serviceproductsave(companyId,exceldatalist);
	}
	
	private void reactonItemProductUpload(long companyId,	ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Item Product upload task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.itemproductsave(companyId,exceldatalist);
		
	}
	
	private void reactonCTCAllocation(long companyId, ArrayList<String> exceldatalist, String loggedInUser) {
		logger.log(Level.SEVERE, "Inside CTC Allocation upload task queue");
		
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.ctcAllocationUpload(companyId,exceldatalist,loggedInUser);
		
	}
	
	private void reactonCreditNoteUpload(long companyId,ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Credit Note upload task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.creditNoteUpload(companyId,exceldatalist);
	}
	
	private void reactonInhouseServiceUpload(long companyId, ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Inhouse service upload task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.InhouseServiceCreationUpload(companyId,exceldatalist);
	}
	
	private void reactonStockUpload(long companyId,	ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside stock upload and update task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.stockUpload(companyId,exceldatalist);
	}
	
	private void reactonServiceCreationUpload(long companyId, ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Service Creation upload task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.ServiceCreationUpload(companyId,exceldatalist);
	}
		
	private void reactonCustomerBranchWithServiceLocationUpload(long companyId,ArrayList<String> exceldatalist) {
		logger.log(Level.SEVERE, "Inside Customer branch with service location upload task queue");
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.CusBranchServiceLocationUpload(companyId,exceldatalist);
	}
	
	private void reactOnServiceCancellationUpload(long companyId,ArrayList<String> exceldatalist, String loggedinuser) {
		logger.log(Level.SEVERE, "Inside Service Cancellation Upload task queue");
		DocumentUploadServiceImpl documentimpl = new DocumentUploadServiceImpl();
		documentimpl.ServiceCancelationUpload(companyId,exceldatalist,loggedinuser);
	}
	private void reactOnCustomerBranchUpload(long companyId,ArrayList<String> exceldatalist) {
		DataMigrationImpl dataimpl = new DataMigrationImpl();
		dataimpl.customerBranchUpload(companyId,exceldatalist);

	}
	
	private void reactonUpdateCustomerBranchStatus(long companyId, String jsonData,boolean serviceCancellationFlag){
		logger.log(Level.SEVERE, "Inside reactonUpdateCustomerBranchStatus serviceCancellationFlag="+serviceCancellationFlag);
		
		if(serviceCancellationFlag){
			try {
				JSONObject jobj=new JSONObject(jsonData);
				long custBranchId=Long.parseLong(jobj.getString("custBranchId"));
				String status=jobj.getString("status");
				logger.log(Level.SEVERE, "custBranchId="+custBranchId+" status="+status);
				CustomerBranchDetails custBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).filter("count",custBranchId)
						.first().now();
				if(custBranch!=null){
					logger.log(Level.SEVERE, "1. custBranch not null");
					if(status.equalsIgnoreCase("Active")){
						custBranch.setStatus(true);
						ofy().save().entity(custBranch);
					}else{
						custBranch.setStatus(false);
						ofy().save().entity(custBranch);
						
						List<String> statusList=new ArrayList<String>();
						statusList.add(Service.SERVICESTATUSSCHEDULE);
						statusList.add(Service.SERVICESTATUSRESCHEDULE);
						statusList.add(Service.SERVICESTATUSOPEN);
						statusList.add(Service.SERVICESTATUSPENDING);
						statusList.add(Service.SERVICESTATUSPLANNED);
						
						String custBranchName=custBranch.getBusinessUnitName();
						int custId= custBranch.getCinfo().getCount();
					
						List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", custId).filter("serviceBranch", custBranchName).filter("status IN", statusList).list();
						if(serviceList!=null){
							logger.log(Level.SEVERE, "serviceList size="+serviceList.size());
							for(Service service:serviceList){
								service.setStatus(Service.SERVICESTATUSCANCELLED);
								
							}
							ofy().save().entities(serviceList).now();
						}else
							logger.log(Level.SEVERE, "serviceList null");
					}
				}
				else
					logger.log(Level.SEVERE, "custBranch null");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{

			try {
				JSONObject jobj=new JSONObject(jsonData);
				String data=jobj.getString("data");
				logger.log(Level.SEVERE, "data="+data);
				JSONArray dataArray=new JSONArray(data);
				logger.log(Level.SEVERE, "dataArray::"+dataArray+" dataArray size="+dataArray.length());
				HashMap<Integer,String> custIdStatusMap=new HashMap<Integer, String>();
				ArrayList<Integer> idList=new ArrayList<Integer>();
				
				for(int i=0;i<dataArray.length();i++){
					JSONObject obj=dataArray.getJSONObject(i);
					Integer custBranchId=Integer.parseInt(obj.getString("custBranchId"));
					String status=obj.getString("status");
					custIdStatusMap.put(custBranchId, status);
					idList.add(custBranchId);
				}	
				logger.log(Level.SEVERE, "custIdStatusMap size="+custIdStatusMap.size());
				logger.log(Level.SEVERE, "idList="+idList.size());
					
					
				List<CustomerBranchDetails> custBranchList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).filter("count IN",idList).list();
				if(custBranchList!=null){
					logger.log(Level.SEVERE, "custBranchList not null");
					for(CustomerBranchDetails custBranch:custBranchList){
						if(custIdStatusMap.get(custBranch.getCount()).equalsIgnoreCase("Active")){
							custBranch.setStatus(true);
						}else{
							custBranch.setStatus(false);
						}
					}
					ofy().save().entities(custBranchList);
				}
				else
					logger.log(Level.SEVERE, "custBranchList null");				
								
				}
			 catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			 
		}
	}
	private void reactOnServiceUpdate(long companyId,ArrayList<String> exceldatalist, String loggedinuser) {
		logger.log(Level.SEVERE, "Inside Service Cancellation Upload task queue");
		DataMigrationImpl documentimpl = new DataMigrationImpl();
		documentimpl.ServiceUpdateViaExcelUpload(companyId,exceldatalist,loggedinuser);
	}
}
