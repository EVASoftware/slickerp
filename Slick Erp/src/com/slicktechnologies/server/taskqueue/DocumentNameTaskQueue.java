package com.slicktechnologies.server.taskqueue;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.common.helperlayer.Type;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class DocumentNameTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3883109262852719100L;

	Logger logger = Logger.getLogger("Name Of logger");

	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		logger.log(Level.SEVERE, "Welcome to Module name Task Queue");

		String documentNamewithdoller = request.getParameter("documentNamekey");

		logger.log(Level.SEVERE, "Module name Data === with $ ==  "+ documentNamewithdoller);

		String[] documentNamewithoutdoller = documentNamewithdoller.split("[$]");

		Type types = new Type();

		types.setInternalType(Integer.parseInt(documentNamewithoutdoller[0]));
		types.setCategoryName(documentNamewithoutdoller[1]);
		types.setTypeName(documentNamewithoutdoller[2]);
		types.setTypeCode(documentNamewithoutdoller[3]);
		types.setCatCode(documentNamewithoutdoller[4]);
		types.setCompanyId(Long.parseLong(documentNamewithoutdoller[5]));
		types.setStatus(true);
		types.setCount(Integer.parseInt(documentNamewithoutdoller[6]));

		ofy().save().entity(types);

		logger.log(Level.SEVERE, "Module Name saved successfully");

	}
}
