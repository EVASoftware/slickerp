package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class UpdateServiceAddressLatLongQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1795811994700875770L;
	Logger logger = Logger.getLogger("Name of logger");
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdf= new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)throws ServletException, IOException {
	
		 String compId = request.getParameter("companyId");
		 long companyId = 0;
		 if(compId!=null){
			 companyId = Long.parseLong(request.getParameter("companyId"));
		 }
		 String taskKeyAndValue = request.getParameter("taskKeyAndValue");
		 logger.log(Level.SEVERE, "Task Key And Value "	+ taskKeyAndValue);
		 String[] taskValue = taskKeyAndValue.split("[$]");
	
		 if(taskValue[0].equalsIgnoreCase("updateServiceAddressLatLong")){
				
				/**
				 * @author Anil , Date : 09-07-2019
				 */
				ObjectifyService.reset();
				logger.log(Level.SEVERE, "Objectify Consistency Strong");
				ofy().consistency(Consistency.STRONG);
				ofy().clear();
				
				
				/***
				 * 
				 */
				try{
					/**
					 * @author Anil , Date : 06-02-2020
					 * for company id proper value is not getting fetched when calling from markcompletemultipleservice
					 */
					if(companyId==0){
						companyId = Long.parseLong(taskValue[1].trim());
					}
					
					
				  int serviceId = Integer.parseInt(taskValue[2].trim());
				  Service service = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId).first().now();
				  Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count", service.getPersonInfo().getCount()).first().now();
				 logger.log(Level.SEVERE, "Customer:");
//				 ServiceImplementor serviceImpl= new ServiceImplementor();
//				 serviceImpl.saveStatus(service);
				  if(customer != null){
					  if(customer.getSecondaryAdress() != null){
						  if(service.getAddress().getAddrLine1().equalsIgnoreCase(customer.getSecondaryAdress().getAddrLine1())
								  && service.getAddress().getCity().equalsIgnoreCase(customer.getSecondaryAdress().getCity())){
							 if((customer.getSecondaryAdress().getLatitude() == null || customer.getSecondaryAdress().getLatitude().equals(""))&& service.getAddress().getLatitude() != null){
								  if(!service.getAddress().getLatitude().equals("")){
									 customer.getSecondaryAdress().setLatitude(service.getAddress().getLatitude());
									 logger.log(Level.SEVERE, "Customer:");
								  }
							 }
							 if((customer.getSecondaryAdress().getLongitude() == null || customer.getSecondaryAdress().getLongitude().equals("")) && service.getAddress().getLongitude() != null){
								 if(!service.getAddress().getLongitude().equals("")){
									 customer.getSecondaryAdress().setLongitude(service.getAddress().getLongitude());
									 logger.log(Level.SEVERE, "Customer:");
								 }
							 
						  }
					  }
					  }
					  ofy().save().entity(customer);
				  }
				  CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).filter("buisnessUnitName", service.getServiceBranch()).first().now();
				  logger.log(Level.SEVERE, "Customer Branch");
				  if(customerBranch != null){
					  if(customerBranch.getAddress() != null){
						  if(service.getAddress().getAddrLine1().equalsIgnoreCase(customerBranch.getAddress().getAddrLine1())
								  && service.getAddress().getCity().equalsIgnoreCase(customerBranch.getAddress().getCity())){
							  if((customerBranch.getAddress().getLatitude() == null  || customerBranch.getAddress().getLatitude().equals(""))&& service.getAddress().getLatitude() != null){
								  if(!service.getAddress().getLatitude().equals("")){
									 customerBranch.getAddress().setLatitude(service.getAddress().getLatitude());
									 logger.log(Level.SEVERE, "Customer Branch");
								  }
							  }
							  if((customerBranch.getAddress().getLongitude() == null || customerBranch.getAddress().getLongitude().equals("")) && service.getAddress().getLongitude() != null){
								 if(!service.getAddress().getLongitude().equals("")){
									 customerBranch.getAddress().setLongitude(service.getAddress().getLongitude());
									 logger.log(Level.SEVERE, "Customer Branch");
								 }
							  }
						  }
					  }
					  ofy().save().entity(customerBranch);
				  }
				  List<Contract> contractList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("cinfo.count", service.getPersonInfo().getCount()).list();
				  logger.log(Level.SEVERE, "Contract");
				  for(Contract con : contractList){

					  if(con.getCustomerServiceAddress()!= null){
						  if(service.getAddress().getAddrLine1().equalsIgnoreCase(con.getCustomerServiceAddress().getAddrLine1())
								  && service.getAddress().getCity().equalsIgnoreCase(con.getCustomerServiceAddress().getCity())){
							  if((con.getCustomerServiceAddress().getLatitude() == null || con.getCustomerServiceAddress().getLatitude().equals("")) && service.getAddress().getLatitude() != null){
							  if(!service.getAddress().getLatitude().equals("")){
								 con.getCustomerServiceAddress().setLatitude(service.getAddress().getLatitude());
								 logger.log(Level.SEVERE, "Contract");
							  }
							  }
							  if((con.getCustomerServiceAddress().getLongitude() == null || con.getCustomerServiceAddress().getLongitude().equals("") ) && service.getAddress().getLongitude() != null){
							  if(!service.getAddress().getLongitude().equals("")){
								 con.getCustomerServiceAddress().setLongitude(service.getAddress().getLongitude());
								 logger.log(Level.SEVERE, "Contract");
							  }
							  }
						  }
					  }
					  ofy().save().entity(con);
				  
				  }
				  
				  List<Service> serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", service.getPersonInfo().getCount())
						  .filter("contractCount", service.getContractCount()).list();
				  logger.log(Level.SEVERE, "Service");
				  for(Service ser : serviceList){

					  if(service.getAddress().getAddrLine1().equalsIgnoreCase(ser.getAddress().getAddrLine1())
							  && service.getAddress().getCity().equalsIgnoreCase(ser.getAddress().getCity())){
						  if((ser.getAddress().getLatitude() == null || ser.getAddress().getLatitude().equals("")) && service.getAddress().getLatitude() != null){
							  if( !service.getAddress().getLatitude().equals("")){
								 ser.getAddress().setLatitude(service.getAddress().getLatitude());
								  logger.log(Level.SEVERE, "Service");
							  }
						  }
						  if((ser.getAddress().getLongitude() == null  || ser.getAddress().getLongitude().equals(""))&& service.getAddress().getLongitude() != null){
							 if( !service.getAddress().getLongitude().equals("")){
								 ser.getAddress().setLongitude(service.getAddress().getLongitude());
								  logger.log(Level.SEVERE, "Service");
							 }
						  } 
					  }
				    ofy().save().entity(ser);
				  }
			 }catch(Exception e){
				 e.printStackTrace();
				 ObjectifyService.reset();
				 logger.log(Level.SEVERE, "Objectify Consistency Strong");
				 ofy().consistency(Consistency.STRONG);
				 ofy().clear();
			 }
			}
	
	
	}

	
	
	
}
