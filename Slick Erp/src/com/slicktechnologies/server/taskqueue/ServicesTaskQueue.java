package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class ServicesTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3630394758372273060L;


	Logger logger=Logger.getLogger("Name Of logger");
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		logger.log(Level.SEVERE,"Welcome to Services Task Queue");
		
		String servicesStringwithdoller = request.getParameter("servicekey");
		
		String[] serviceStringwithoutdoller = servicesStringwithdoller.split("[$]");
		logger.log(Level.SEVERE,"String size: "+serviceStringwithoutdoller.length);
		System.out.print("String size: "+serviceStringwithoutdoller.length);
//		for(int i=0; i<serviceStringwithoutdoller.length;i++){
//			System.out.print("String value: "+i+":::::::"+serviceStringwithoutdoller[i]);
//		}
		
		
		 SimpleDateFormat dateFormat=new SimpleDateFormat("E MMM dd hh:mm:ss Z yyyy");
		
		Service service=new Service();
		
		PersonInfo personinfo = new PersonInfo();
		personinfo.setCount(Integer.parseInt(serviceStringwithoutdoller[0]));
		personinfo.setEmail(serviceStringwithoutdoller[1]);
		personinfo.setFullName(serviceStringwithoutdoller[2]);
		personinfo.setPocName(serviceStringwithoutdoller[3]);
		personinfo.setCellNumber(Long.parseLong(serviceStringwithoutdoller[4]));
		service.setPersonInfo(personinfo);
		
		
		service.setContractCount(Integer.parseInt(serviceStringwithoutdoller[5]));
		
		
		service.setBranch(serviceStringwithoutdoller[6]);
		
		/**
		 * @author Abhinav Bihade
		 * @since 05/12/2019
		 * As per Rahul Tiwari,Prasad Shilwant's Requirement Backdated Dated Quick Contract will completed Automatically and from todays contract(Features Contracts) it will be SCHEDULE for this Anil Pal and Vijay Chougule Has Guide me for the changes  
		 */
		
		DateFormat dateFormat1=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("IST"));
		  String date = dateFormat1.format(new Date());
		  try {
		  Date strSerDate = dateFormat.parse(serviceStringwithoutdoller[14]);
		  String strServiceDate = dateFormat1.format(strSerDate);
		  
			Date today = dateFormat1.parse(date);
			Date serviceDate = dateFormat1.parse(strServiceDate);
		  System.out.println("today"+today);
		  System.out.println("serviceDate"+serviceDate);
		if(Boolean.parseBoolean(serviceStringwithoutdoller[60]) && !serviceDate.equals(today) &&serviceDate.before(today)){
			service.setStatus("Completed");
		}else{
			service.setStatus(serviceStringwithoutdoller[7]);
		}
		
		 } catch (ParseException e) {
				e.printStackTrace();
		}
		
		/**
		 * @author Abhinav Bihade
		 * @since 05/12/2019
		 * below 7 lines Commented as per Rahul Tiwari, Prasad Shilwant Quick Contract Requirement 
		 */
//		/*************changes for quickcontrct and normal contract vijay *****************/
//		if(Boolean.parseBoolean(serviceStringwithoutdoller[60])){
//			service.setStatus("Completed");
//		}else{
//			service.setStatus(serviceStringwithoutdoller[7]);
//		}
//		/*********************************************************/
		  /**
		   * end here
		   */
		
//		service.setStatus(serviceStringwithoutdoller[7]);
		/**Date 13feb 2017
		 * vijay commented for in service service engineer should be blank by deafult
		 */
//		service.setEmployee(serviceStringwithoutdoller[8]);
		/**
		 * end here
		 */
		try {
			service.setContractStartDate(dateFormat.parse(serviceStringwithoutdoller[8]));
		
		service.setContractEndDate(dateFormat.parse(serviceStringwithoutdoller[9]));
		service.setAdHocService(Boolean.valueOf(serviceStringwithoutdoller[10]));
		service.setServiceIndexNo(Integer.parseInt(serviceStringwithoutdoller[11]));
		service.setServiceType(serviceStringwithoutdoller[12]);
//		ProductInfo prodInfo=new ProductInfo();
//		prodInfo.setProductCode(item.getProductCode());
//		prodInfo.setProductName(item.getProductName());
//		prodInfo.setProdID(item.getPrduct().getCount());
//		
//		if(prodInfo!=null){
//			temp.setProductInfoDetails(prodInfo);
//		}
		service.setServiceSerialNo(Integer.parseInt(serviceStringwithoutdoller[13]));
		service.setServiceDate(dateFormat.parse(serviceStringwithoutdoller[14]));
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Address address = new Address();
		address.setAddrLine1(serviceStringwithoutdoller[15]);
		if(serviceStringwithoutdoller[16].equalsIgnoreCase(null)){
			address.setAddrLine2("");
		}else{
		address.setAddrLine2(serviceStringwithoutdoller[16]);
		}
		address.setCountry(serviceStringwithoutdoller[17]);
		address.setState(serviceStringwithoutdoller[18]);
		address.setCity(serviceStringwithoutdoller[19]);
		if(serviceStringwithoutdoller[20].equalsIgnoreCase(null)){
			address.setLocality("");
		}else{
		    address.setLocality(serviceStringwithoutdoller[20]);
		}

		address.setPin(Long.parseLong(serviceStringwithoutdoller[21]));
		if(serviceStringwithoutdoller[22].equalsIgnoreCase(null)){
			address.setLandmark("");
		}else{
			address.setLandmark(serviceStringwithoutdoller[22]);
		}
		
		service.setAddress(address);
    	
		if(serviceStringwithoutdoller[23]!=null){
    		service.setServiceDay(serviceStringwithoutdoller[23]);
		}
    	service.setServiceTime(serviceStringwithoutdoller[24]);
    	service.setServiceBranch(serviceStringwithoutdoller[25]);
    	service.setServiceDateDay(serviceStringwithoutdoller[26]);
    	
    	System.out.println("Company Id=="+serviceStringwithoutdoller[27]);
    	
		if(serviceStringwithoutdoller[27]!=null){
			service.setCompanyId(Long.parseLong(serviceStringwithoutdoller[27]));
		}
    	
//    	SalesLineItem item = new SalesLineItem();
    	
		ServiceProduct serviceprod = new ServiceProduct();

    	
    	if(!serviceStringwithoutdoller[28].equals("")){
    		serviceprod.setComment(serviceStringwithoutdoller[28]);    
    	}	
    	if(!serviceStringwithoutdoller[29].equals("")){
    		serviceprod.setCommentdesc(serviceStringwithoutdoller[29]);
    	}
    	System.out.println("Company=="+serviceStringwithoutdoller[30]);
    	if(!serviceStringwithoutdoller[30].equals("")){
    		serviceprod.setCompanyId(Long.parseLong(serviceStringwithoutdoller[30]));
    	}
    	if(!serviceStringwithoutdoller[31].equals("")){
    		serviceprod.setCount(Integer.parseInt(serviceStringwithoutdoller[31]));
    	}
    	if(!serviceStringwithoutdoller[32].equals("")){
    		serviceprod.setCreatedBy(serviceStringwithoutdoller[32]);
    	}
    	if(!serviceStringwithoutdoller[33].equals("")){
    		serviceprod.setDeleted(Boolean.valueOf(serviceStringwithoutdoller[33]));
    	}
    	if(!serviceStringwithoutdoller[34].equals("")){
    		serviceprod.setDuration(Integer.parseInt(serviceStringwithoutdoller[34]));
    	}
    	if(!serviceStringwithoutdoller[35].equals("")){
    		serviceprod.setId(Long.parseLong(serviceStringwithoutdoller[35]));
    	}
    	if(!serviceStringwithoutdoller[36].equals("")){
    		serviceprod.setNumberOfService(Integer.parseInt(serviceStringwithoutdoller[36]));
    	}
    	if(!serviceStringwithoutdoller[37].equals("")){
    		serviceprod.setPrice(Double.parseDouble(serviceStringwithoutdoller[37]));
    	}
    	if(!serviceStringwithoutdoller[38].equals("")){
    		serviceprod.setProductCategory(serviceStringwithoutdoller[38]);
    	}
    	if(!serviceStringwithoutdoller[39].equals("")){
    		serviceprod.setProductClassification(serviceStringwithoutdoller[39]);
    	}
    	if(!serviceStringwithoutdoller[40].equals("")){
    		serviceprod.setProductCode(serviceStringwithoutdoller[40]);
    	}
    	if(!serviceStringwithoutdoller[41].equals("")){
    		serviceprod.setProductGroup(serviceStringwithoutdoller[41]);
    	}
    	if(!serviceStringwithoutdoller[42].equals("")){
    		serviceprod.setProductName(serviceStringwithoutdoller[42]);
    	}
    	if(!serviceStringwithoutdoller[43].equals("")){
    		serviceprod.setProductType(serviceStringwithoutdoller[43]);    
    	}
    	if(!serviceStringwithoutdoller[44].equals("")){
    		serviceprod.setRefNumber1(serviceStringwithoutdoller[44]);
    	}
    	if(!serviceStringwithoutdoller[45].equals("")){
    		serviceprod.setRefNumber2(serviceStringwithoutdoller[45]);
    	}
    	if(!serviceStringwithoutdoller[46].equals("")){
    		serviceprod.getServiceTax().setInclusive(Boolean.valueOf(serviceStringwithoutdoller[46]));
    	}
    	if(!serviceStringwithoutdoller[47].equals("")){
    		serviceprod.getServiceTax().setPercentage(Double.parseDouble(serviceStringwithoutdoller[47]));
    	}
    	if(!serviceStringwithoutdoller[48].equals("")){
    		serviceprod.getServiceTax().setTaxConfigName(serviceStringwithoutdoller[48]);
    	}
    	if(!serviceStringwithoutdoller[49].equals("")){
    		serviceprod.getServiceTax().setTaxName(serviceStringwithoutdoller[49]);
    	}
    	if(!serviceStringwithoutdoller[50].equals("")){
    		serviceprod.setSpecifications(serviceStringwithoutdoller[50]);
    	}
    	if(!serviceStringwithoutdoller[51].equals("")){
    		serviceprod.setStatus(Boolean.valueOf(serviceStringwithoutdoller[51]));
    	}
    	
    	
//    	item.getPrduct().getTermsAndConditions().setName(serviceStringwithoutdoller[54]);
//    	item.getPrduct().getTermsAndConditions().setStatus(Boolean.valueOf(serviceStringwithoutdoller[55]));
//    	item.getPrduct().getTermsAndConditions().setUrl(serviceStringwithoutdoller[56]);
//    	item.getPrduct().setUnitOfMeasurement(serviceStringwithoutdoller[57]);
//    	item.getPrduct().setUserId(serviceStringwithoutdoller[58]);
//    	item.getPrduct().getVatTax().setInclusive(Boolean.valueOf(serviceStringwithoutdoller[59]));
//    	item.getPrduct().getVatTax().setPercentage(Double.parseDouble(serviceStringwithoutdoller[60]));
//    	item.getPrduct().getVatTax().setTaxConfigName(serviceStringwithoutdoller[61]);
//    	item.getPrduct().getVatTax().setTaxName(serviceStringwithoutdoller[62]);
    	
    	if(!serviceStringwithoutdoller[52].equals("")){
    		serviceprod.setUnitOfMeasurement(serviceStringwithoutdoller[52]);
    	}
    	if(!serviceStringwithoutdoller[53].equals("")){
    		serviceprod.setUserId(serviceStringwithoutdoller[53]);
    	}
    	if(!serviceStringwithoutdoller[54].equals("")){
    		serviceprod.getVatTax().setInclusive(Boolean.valueOf(serviceStringwithoutdoller[54]));
    		
    	}
    	if(!serviceStringwithoutdoller[55].equals("")){
    		serviceprod.getVatTax().setPercentage(Double.parseDouble(serviceStringwithoutdoller[55]));
    	}
    	
    	if(!serviceStringwithoutdoller[56].equals("")){
    		serviceprod.getVatTax().setTaxConfigName(serviceStringwithoutdoller[56]);
    	}
    	if(!serviceStringwithoutdoller[57].equals("")){
    		serviceprod.getVatTax().setTaxName(serviceStringwithoutdoller[57]);
    	}
    	
    	service.setCount(Integer.parseInt(serviceStringwithoutdoller[58]));
    	
    	service.setNumberRange(serviceStringwithoutdoller[59]);
    	
    	service.setProduct(serviceprod);
    	
    	service.setRefNo(serviceStringwithoutdoller[61]);
    	
    	service.setRefNo2(serviceStringwithoutdoller[62]);
    	
    	if(serviceStringwithoutdoller[63].equals("0")){
    		service.setRemark("");

    	}else{
    		service.setRemark(serviceStringwithoutdoller[63]);
    	}
    	
    	service.setServiceWeekNo(Integer.parseInt(serviceStringwithoutdoller[64]));
    	
    	
    	if(serviceStringwithoutdoller[65].equals("0")){
    		service.setEmployee("");

    	}else{
    		service.setEmployee(serviceStringwithoutdoller[65]);
    	}
    	
    	
    	/*** 29-09-2017 sagar sore [setting service value through task queue] **/
    	
    	if(serviceStringwithoutdoller[66].equals("0")){
    		service.setServiceValue(0.00);

    	}else{
    		service.setServiceValue(Double.parseDouble(serviceStringwithoutdoller[66]));
    	}
    	
    	/**
    	 * Date : 06-10-2017 BY ANIL
    	 */
    	if(serviceStringwithoutdoller[67].equals("NO")){
    		service.setServiceWiseBilling(false);
    	}else{
    		service.setServiceWiseBilling(true);
    	}
    	
    	if(serviceStringwithoutdoller[68].equals("")){
//    		service.setServiceWiseBilling(false);
    	}else{
    		service.setServiceSrNo(Integer.parseInt(serviceStringwithoutdoller[68]));
    	}
    	/**
    	 * End
    	 */
    	/**
    	 *  nidhi
    	 *  Date : 4-12-2017
    	 *  for save service type as process configration
    	 */
    	if(serviceStringwithoutdoller[69].equalsIgnoreCase("true")){
    		service.setServiceType(serviceStringwithoutdoller[70]);
    	}
    	/*
    	 * end
    	 */
    	/**
		 *  nidhi
		 *  29-01-2018
		 *  for copy pocname in person info
		 */
		if(serviceStringwithoutdoller[71]!=null && !serviceStringwithoutdoller[71].trim().equals("")){
			service.getPersonInfo().setPocName(serviceStringwithoutdoller[71]);
		}
		/**
		 *  end
		 */
		/**
    	 * date 17.02.2018 added by komal for area wise billing **
    	 * 
    	 */
    	if(serviceStringwithoutdoller[72].equals("0")){
    		service.setPerUnitPrice(0);
    	}else{
    		service.setPerUnitPrice(Double.parseDouble(serviceStringwithoutdoller[72]));
    	}
    	
    	if(serviceStringwithoutdoller[73].equals("na")){
    		service.setCostCenter(null);
    	}else{
    		service.setCostCenter(serviceStringwithoutdoller[73]);
    	}
    	if(serviceStringwithoutdoller[74].equals("0")){
    		service.setQuantity(0);
    	}else{
    		service.setQuantity(Double.parseDouble(serviceStringwithoutdoller[74]));
    	}
    	
    	if(serviceStringwithoutdoller[75].equals("na")){
   
    	}else{
    		service.setUom(serviceStringwithoutdoller[75]);
    	}
		/**
		 * end komal
		 */
    	/**
    	 * nidhi
    	 */
     	if(serviceStringwithoutdoller[76].equalsIgnoreCase("false")){
     	   
    	}else{
    		service.setRenewContractFlag(true);
    	}
    	
     	/**
		 * Date 04-07-2018 
		 * Developer :- Vijay
		 * Des :- Primises not setting to services
		 */
     	if(!serviceStringwithoutdoller[77].equals("0")){
     		service.setPremises(serviceStringwithoutdoller[77]);
     	}
     	/**
     	 * ends here
     	 */
     	
     	/**
		 * nidhi
		 * 8-08-2018
		 */
		if(!serviceStringwithoutdoller[78].equals("0")){
			if(serviceStringwithoutdoller[78].equalsIgnoreCase("ServiceThroughApp")){
				service.setServiceScheduled(true);
				logger.log(Level.SEVERE,"data saved successfully");
			}else{
				service.setProModelNo(serviceStringwithoutdoller[78]);
			}
		}
		if(!serviceStringwithoutdoller[79].equals("0")){
			service.setProSerialNo(serviceStringwithoutdoller[79]);
		}
		/**
		 * end
		 */
		/**
		 * nidhi
		 * 5-10-2018
		 */
		if(serviceStringwithoutdoller[80] !=null && !serviceStringwithoutdoller[80].equals("NA")){
			try {
				JSONArray jsonarr=new JSONArray(serviceStringwithoutdoller[80].trim());
				ArrayList<ServiceProductGroupList> serProList = new ArrayList<ServiceProductGroupList>();
				Gson gson = new GsonBuilder().create();
				for(int i=0;i<jsonarr.length();i++){
							
							JSONObject jsonObj = jsonarr.getJSONObject(i);
							ProductGroupList proDetail = new ProductGroupList();
							proDetail = gson.fromJson(jsonObj.toString(), ProductGroupList.class);
							service.setUom(proDetail.getUnit());
							service.setQuantity(proDetail.getQuantity());
							serProList.add(service.getServiceProdDt(proDetail));
							service.setServiceProductList(serProList);
							logger.log(Level.SEVERE,"get pro list --" + serProList.size());
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**
		 * @author Anil
		 * @since 06-06-2020
		 * setting additional details for premuim tech requirement
		 */
		try{
			service.setTierName(serviceStringwithoutdoller[81]);
			service.setComponentName(serviceStringwithoutdoller[82]);
			service.setMfgNo(serviceStringwithoutdoller[83]);
			if(serviceStringwithoutdoller[84]!=null&&!serviceStringwithoutdoller[84].equals("null")){
				service.setMfgDate(dateFormat.parse(serviceStringwithoutdoller[84]));
			}
			if(serviceStringwithoutdoller[85]!=null&&!serviceStringwithoutdoller[85].equals("null")){
				service.setReplacementDate(dateFormat.parse(serviceStringwithoutdoller[85]));
			}
			service.setAssetId(Integer.parseInt(serviceStringwithoutdoller[86]));
			
			/**
			 * @author Anil
			 * @since 12-01-2021
			 * Asset Unit was missing
			 * 
			 */
			service.setAssetUnit(serviceStringwithoutdoller[87]);
			
			logger.log(Level.SEVERE , "assetId: " + service.getAssetId());
			logger.log(Level.SEVERE , "setTierName: " + serviceStringwithoutdoller[81]);
			logger.log(Level.SEVERE , "setComponentName: " + serviceStringwithoutdoller[82]);
			logger.log(Level.SEVERE , "setMfgNo: " + serviceStringwithoutdoller[83]);
			logger.log(Level.SEVERE , "setMfgDate: " + serviceStringwithoutdoller[84]);
			logger.log(Level.SEVERE , "setReplacementDate: " + serviceStringwithoutdoller[85]);
			logger.log(Level.SEVERE , "setAssetUnit: " + serviceStringwithoutdoller[87]);
			/**
			 * @author Anil
			 * @since 23-06-2020
			 * setting asset id,asset unit and mfg num in description
			 */
			String descHead="";
			String descVal="";
			if(service.getAssetId()!=0){
				descHead="Asset Id";
				descVal=service.getAssetId()+"";
			}
			if(service.getAssetUnit()!=null&&!service.getAssetUnit().equals("")){
				if(!descHead.equals("")){
					descHead=descHead+"/"+"Asset Unit";
					descVal=descVal+"/"+service.getAssetUnit();
				}else{
					descHead="Asset Unit";
					descVal=service.getAssetUnit();
				}
			}
			if(service.getMfgNo()!=null&&!service.getMfgNo().equals("")){
				if(!descHead.equals("")){
					descHead=descHead+"/"+"Mfg No.";
					descVal=descVal+"/"+service.getMfgNo();
				}else{
					descHead="Mfg No.";
					descVal=service.getMfgNo();
				}
			}
			String description="";
			if(!descHead.equals("")){
				description=descHead+" : "+descVal+" ";
			}
			
			if(!description.equals("")){
				description=description+" "+service.getDescription();
			}else{
				description=service.getDescription();
			}
			service.setDescription(description);
			
		}catch(Exception e){
			
		}
		
		
		
		try {
			if(serviceStringwithoutdoller[88]!=null && serviceStringwithoutdoller[88].equals("")){
				service.setServiceDuration(Double.parseDouble(serviceStringwithoutdoller[88]));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		boolean automaticSchedulingwithBOM = false;
		try {
			automaticSchedulingwithBOM = Boolean.valueOf(serviceStringwithoutdoller[89]);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			if(Boolean.valueOf(serviceStringwithoutdoller[90])){
				service.setServiceScheduled(true);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		/**
		 * @author Anil
		 * @Since 13-08-2020
		 * Setting contract description in service description
		 * For UMAS raised by Vaishnavi and Nitin
		 * @author Anil
		 * @since 12-01-2021
		 * code was missing
		 */
		try{
			if(serviceStringwithoutdoller[91]!=null&&!serviceStringwithoutdoller[91].equals("")&&!serviceStringwithoutdoller[91].equals("null")){
				service.setDescription(serviceStringwithoutdoller[91]);
			}
		}catch(Exception e){
			
		}
		

		try {
			if(serviceStringwithoutdoller[92]!=null && serviceStringwithoutdoller[92].equals("")){
				service.setProductFrequency(serviceStringwithoutdoller[92]);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
    	 ofy().save().entity(service).now(); 
		 logger.log(Level.SEVERE,"data saved successfully");
		 
	    /**
	     * @author Vijay Chougule
	     * Des :- if Automatich Scheduling process Config is active then Service Project will create with Automatic
	     * Service Scheduling for Technician App. Material Mapping From BOM to project
	     */
	    if(automaticSchedulingwithBOM){
	    	if(service.getServiceProductList()!=null && service.getServiceProductList().size()!=0){
	    		String companyId = service.getCompanyId()+"";
		    	String serviceId = service.getCount()+"";
		    	Queue queue = QueueFactory.getQueue("ProjectCreationTaskQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/projectcreationtaskqueue").param("serviceId", serviceId)
													.param("companyId", companyId));
	    	}

	    		
					
	    }
	    /**
	     * ends here
	     */
		
	}
}
