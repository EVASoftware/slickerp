package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class QuickContractSubmitTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2959414765071483393L;
	
	
Logger logger=Logger.getLogger("Name Of logger");
	
	double paymentrecievedAmount=0;
	double balancePayment =0;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		logger.log(Level.SEVERE,"Welcome to QuickContract Task Queue");
		
		String contractwithdoller = request.getParameter("quickcontractkey");
		
		String[] contract = contractwithdoller.split("[$]");
		
		long companyId = Long.parseLong(contract[0]);
		int contractId = Integer.parseInt(contract[1]);
		String status = contract[2];
		
		
		/**
		 * Date 03-04-2017
		 * added by vijay for quick contract accounting interface will create
		 */
		
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGCO).filter("companyId", companyId).filter("configStatus", true).first().now();
		}
		int flag =0;
		
		if(processConfig!=null){
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			quickContractaccountingInterface(contractId ,companyId, status );
		}
		
		
		
		/**
		 * End here
		 */
		
		Contract contractEntity = ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", contractId).filter("status", status).first().now();
		
		if(contractEntity!=null){
			
			
			/*Developed by vijay
			 * Release 31 august 2016
			 * bellow flag is used for when in quickcontract partail payment then normally create 2 billing 2 invoice and 2 paymnet document will be created
			 * and if we enable quickcontract processConfig for partialpayment 1 billing 1 invoice and 2 payment documnets will be created
			 */
			boolean partialquickcontractcontractoneinvoice = false;
			 paymentrecievedAmount = contractEntity.getPaymentRecieved();
			 balancePayment = contractEntity.getBalancePayment();
			
			/************** here i am creating services ***********************/
				  if(contractEntity.getStatus().equals(Contract.APPROVED)){
					  logger.log(Level.SEVERE,"Making Services");
					  System.out.println("before Calling services method");
						CreateServices(contractEntity);
						System.out.println("After Calling services method");
						
				  }
				  
				  if(contractEntity.getStatus().equals(Contract.CANCELLED)){
					  cancelServices(contractEntity);
				  }
				  
				  changeCustomerStatus(contractEntity);
				  if(contractEntity.getQuotationCount()!=0 && contractEntity.getQuotationCount()!=-1)
				  {
					  changeQuotationStatusToSucessfull(contractEntity);  
				  }
				  
				  /**
				   * @author Vijay Date 05-03-2011
				   * Des :- As per Nitin Sir If contract group is One Time or Single then it should not come for renewal and renewal reminder
				   */
					if(contractEntity.getGroup()!=null && !contractEntity.getGroup().equals("") && contractEntity.getGroup().equalsIgnoreCase("One Time") || contractEntity.getGroup().equalsIgnoreCase("Single")){
						contractEntity.setCustomerInterestFlag(true);
					}
					
				  ofy().save().entity(contractEntity);
				  
			   /**
				 * Date 05-03-2018 By vijay 
				 * for changing lead status 
				 */
				ChangeLeadStatus(contractEntity);
				/**
				 * ends here
				 */
				
				 
					
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "PedioContract", companyId)){
				return;
			}
			/************************** services end here ****************************************/
				  
			/**************************** Billing and payment methods start here ******************/	  
		    logger.log(Level.SEVERE," PAyement Recieved ===== "+contractEntity.getPaymentRecieved());
			double percentage = getpercentage(contractEntity.getNetpayable(),contractEntity.getPaymentRecieved());
			logger.log(Level.SEVERE," Percentage == ====== "+percentage);
			
			if(100-percentage==0 || 100-percentage==100.0){
				System.out.println("For only 100 paymet");
				
				if(contractEntity.getPaymentRecieved()==0.0 || contractEntity.getPaymentRecieved()==0){
					System.out.println("if payment recieved nothing");
					percentage =100;
					createBillingDocuments(contractEntity,percentage,false,false,partialquickcontractcontractoneinvoice,"ClosedPayment");
				}
				else{
					System.out.println("for payment recievd");
					createBillingDocuments(contractEntity,percentage,true,true,partialquickcontractcontractoneinvoice,"OpenPayment");
				}
				
				
			}else{
				
				/* This is old code for if quickcontract has partial payment 2 billing 2 invoioce and 2 payment document will create
				 * 
				 */
				
//				System.out.println("For partial payment");
//				for(int i=0;i<2;i++){
//					if(i==1){
//						System.out.println("Balance payment Billing doc");
//						double percent = 100-percentage;
//						System.out.println("Percentage ==== $$$$$$$$$$$$$$$$$ "+percent);
//						logger.log(Level.SEVERE,"  For partial reamaing percenatge ==="+percent);
	//
//						createBillingDocuments(model,percent,false,true);
//					}else{
//						
//						System.out.println("Payment recieved billing doc ================");
//						createBillingDocuments(model,percentage, true,false);
//					}
//					
//				}
				
				
				/*  this new code aaded by vijay beacuse if we enable process config then in partial quickcontract only one invoicee will created and two payment created
				 *   and old code added in else block
				 */
				
				ProcessName processname = ofy().load().type(ProcessName.class).filter("processName", "QuickContract").filter("status",true).filter("companyId", contractEntity.getCompanyId()).first().now();
				ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "QuickContract").filter("processList.processType", "PartialPaymentOneInvoiceAndPaymentAsPerrecievedAndBalanceAmt").filter("processList.status", true).filter("companyId", contractEntity.getCompanyId()).first().now();
				if(processname!=null){
					logger.log(Level.SEVERE,"in the processName !=null for SMS========"+processname);
					
					if(processconfiguration!=null){
						percentage =100;
						partialquickcontractcontractoneinvoice = true;
						createBillingDocuments(contractEntity,percentage,false,false,partialquickcontractcontractoneinvoice,"OpenPayment");
						
					}else{  // old code added here
						
						System.out.println("For partial payment");
						for(int i=0;i<2;i++){
							if(i==1){
								System.out.println("Balance payment Billing doc");
								double percent = 100-percentage;
								logger.log(Level.SEVERE,"  For partial reamaing percenatge ==="+percent);

								createBillingDocuments(contractEntity,percent,false,false,partialquickcontractcontractoneinvoice,"ClosedPayment");
							}else{
								
								System.out.println("Payment recieved billing doc ================");
								createBillingDocuments(contractEntity,percentage, true,true,partialquickcontractcontractoneinvoice,"OpenPayment");
							}
					}
				}

					
				}else{   // old code added here
					
					System.out.println("For partial payment");
					for(int i=0;i<2;i++){
						if(i==1){
							System.out.println("Balance payment Billing doc");
							double percent = 100-percentage;
							logger.log(Level.SEVERE,"  For partial reamaing percenatge ==="+percent);
		
							createBillingDocuments(contractEntity,percent,false,false,partialquickcontractcontractoneinvoice,"ClosedPayment");
						}else{
							
							System.out.println("Payment recieved billing doc ================");
							createBillingDocuments(contractEntity,percentage, true,true,partialquickcontractcontractoneinvoice,"ReceivedPayment");
						}
				}
			
				}
				
			}
			
			
			/**
			 * Date 22 Feb 2017
			 * SMS for Contract approval this sms for Ankita pest control (welcome Message)
			 */
			CheckSMSConfiguration(contractEntity);
			/**
			 * End here
			 */
			
			
		}
		
		
		
	}
	
	
	 private void ChangeLeadStatus(Contract con) {

	    	if(con.getLeadCount()!=0){
	    		Lead leadEntity = ofy().load().type(Lead.class).filter("companyId", con.getCompanyId()).filter("count", con.getLeadCount()).first().now();
	    		if(leadEntity!=null){
	    			leadEntity.setStatus(Quotation.QUOTATIONSUCESSFUL);
	    			ofy().save().entity(leadEntity);
	    		}
	    	}
		}

	private void CheckSMSConfiguration(Contract model) {

		 logger.log(Level.SEVERE," inside sms method");
//		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",model.getCompanyId()).filter("status",true).first().now();
		SimpleDateFormat fmtcondate = new SimpleDateFormat("dd/MM/yyyy");

//		if(smsconfig!=null){
//			
//			String accountauthkey = smsconfig.getAuthToken();
//			String accSenderId =smsconfig.getAccountSID(); 
//			String accRoute = smsconfig.getPassword();
//			 logger.log(Level.SEVERE,"in sms config");
			
			 /**
				 * Date 9-03-2017
				 * added by vijay
				 * for ankita pest control needed reference number of contract in place of contract id
				 * so while sending sms below if condition for contract id value is reference number and else for contract id value
				 */
			ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "ContractApprovedSMS").filter("processList.processType", "SMSWithReferenceNumber").filter("processList.status", true).filter("companyId", model.getCompanyId()).first().now();
			
			logger.log(Level.SEVERE,"Procees config value=="+processconfiguration);
			
			/**
			 * ends here
			 */
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", model.getCompanyId()).filter("event", "Contract Approved" ).filter("status",true).first().now();
			if(smsEntity!=null){
				
				String templatemsgwithbraces = smsEntity.getMessage();
				Company comp = ofy().load().type(Company.class).filter("companyId", model.getCompanyId()).first().now();
				String fullname = model.getCinfo().getFullName();
				System.out.println(" customerName ="+model.getCinfo().getCellNumber());
				String constartDate = fmtcondate.format(model.getStartDate());
				String conendDate = fmtcondate.format(model.getEndDate());
				
				
				/**
				 * Date 29 jun 2017 added by vijay for Eco friendly
				 */
				
				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", model.getCompanyId()).first().now();
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", model.getCompanyId()).filter("processName","SMS").first().now();				
					
				boolean prossconfigBhashSMSflag = false;
				if(processName!=null){
				if(processConfig!=null){
					if(processConfig.isConfigStatus()){
						for(int l=0;l<processConfig.getProcessList().size();l++){
							if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
								prossconfigBhashSMSflag=true;
							}
						}
				   }
				}
				}

				logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSflag);
				String actualMsg="";
				if(prossconfigBhashSMSflag){
					fullname = getCustomerName(fullname,model.getCompanyId());
					String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
					String companyName = customerName.replace("{companyName}",comp.getBusinessUnitName());
					String contractStartDate = companyName.replace("{contractstartDate}", constartDate);
					 actualMsg = contractStartDate.replace("{contractendDate}", conendDate);
				}/**
				 * ends here
				 */
				else{
					
					String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
					String companyName = customerName.replace("{companyName}",comp.getBusinessUnitName());
					String contractid;
					if(processconfiguration!=null){
						contractid = companyName.replace("{referenceNum}", model.getRefNo()+"");
					}else{
						contractid = companyName.replace("{contractId}", model.getCount()+"");
					}
					
					String contractStartDate = contractid.replace("{contractstartDate}", constartDate);
					 actualMsg = contractStartDate.replace("{contractendDate}", conendDate);
				}
				
				long customermobileNo = model.getCinfo().getCellNumber();

//				ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", model.getCompanyId()).filter("processName","SMS").first().now();				
//				
//				boolean bhashSMSAPIFlag = false;
//				if(processconfig!=null){
//					if(processconfig.isConfigStatus()){
//						for(int l=0;l<processconfig.getProcessList().size();l++){
//							if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BhashSMSAPI") && processconfig.getProcessList().get(l).isStatus()==true){
//								bhashSMSAPIFlag=true;
//							}
//						}
//				   }
//				}
//				logger.log(Level.SEVERE,"process config  SMS Flag =="+bhashSMSAPIFlag);
				
				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				smsimpl.sendSmsToClient(actualMsg, customermobileNo, accSenderId, accountauthkey, accRoute,model.getCompanyId());
//				logger.log(Level.SEVERE," Impl method called");
				
				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,smsEntity.getEvent(),smsEntity.getCompanyId(),actualMsg,customermobileNo,false);
				logger.log(Level.SEVERE,"after sendMessage method");

			}
//		}
	}
	
	/**
	 * SMS End here
	 */
	
	
	/**
	 * ******************************* Creating services section methods start here *******************
	 */
	
	private void cancelServices(Contract con)
	   {
		   List<Service>service;
		   if(con.getCompanyId()!=null)
	   	{
			   service =ofy().load().type(Service.class).filter("contractCount",con.getCount()).
	   			filter("companyId",con.getCompanyId()).list();
	   	}
	   	
	   	else
	   		service =	ofy().load().type(Service.class).filter("contractCount",con.getCount()).list();
		   
		   if(service!=null)
		   {
			   for(Service ser:service)
			   {
				   if(ser!=null)
				   {
					   ser.setStatus(Service.SERVICESTATUSCANCELLED);
				   }
			   }
		   }
		   if(service.size()!=0)
			   ofy().save().entities(service).now();
	   }
	 
	 
	private void CreateServices(Contract con) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Service Creaiton Methd");
		
		boolean firstServCompFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", AppConstants.PC_COMPLETEFIRSTSERVICE, con.getCompanyId());
		
		ArrayList<SuperModel> services=new ArrayList<SuperModel>();
		Customer cust=null;
		Address addr=null;
		CustomerBranchDetails custBrach;
		
		 double totalservices=0;
		 
		 totalservices = gettotalnumberofservices(con.getItems());

		 /**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", con.getCompanyId());
		 
		 /**
		  * end
		  */
		 
		/**29-09-2017 sagar sore [variable added serviceValue]**/
		double serviceValue=0.0;
			
		/** Date 12-09-2017 added by vijay for for task queue services number genration first updated and below olc code commented  ***/ 
		 int count=0;
		 if(totalservices>50){
			 
		long companyId = con.getCompanyId();
		
		NumberGeneration numbergen = null;	
		 
			boolean processconfigflag = checkprocessconfiguration(con.getCompanyId());
			
			if(processconfigflag){
				logger.log(Level.SEVERE,"In the process config true");

				System.out.println("In the process config true");
				List<NumberGeneration> numbergenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", con.getCompanyId()).list();
				if(con.getNumberRange()!=null ){
					
					logger.log(Level.SEVERE,"111111111111");

					String selectedNumberRange = con.getNumberRange();
					String prefixselectedNumberRange = "S_"+selectedNumberRange;
					
					for(int i=0;i<numbergenrationList.size();i++){
						
						if(prefixselectedNumberRange.equals(numbergenrationList.get(i).getProcessName())){
							logger.log(Level.SEVERE,"22222222222");

							numbergen=ofy().load().type(NumberGeneration.class).filter("companyId",con.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
						}
					}
				}
				else{
					logger.log(Level.SEVERE,"33333333333333");

					numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
				}
			}
			else{
				logger.log(Level.SEVERE,"44444444444444");

				numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
			}
		 
	 
		/************************** ende here  some change while saving services bellow for normal services & taskqueue services*********************************/
		
	    long number = numbergen.getNumber();
	    
	    logger.log(Level.SEVERE,"Last Number===="+number);
    	  count = (int) number;

    	 number = number+(int)totalservices;
    	 numbergen.setNumber(number);
    	 GenricServiceImpl genimpl = new GenricServiceImpl();
    	 genimpl.save(numbergen);
    	 
	  } 
    	 /*************** Ends here ***************************/ 
    	 
    	 
		for(SalesLineItem item:con.getItems())
		{
			
			/** Date 12-09-2017 new code above added by vijay for for task queue services number genration first updated and below old code commented  ***/ 

//			/************************** vijay ***********************************/
//			 System.out.println("In service ===");
//			 
//				boolean processconfigflag = checkprocessconfiguration(con.getCompanyId());
//				
//				if(processconfigflag){
//					logger.log(Level.SEVERE,"In the process config true");
//
//					System.out.println("In the process config true");
//					List<NumberGeneration> numbergenrationList = ofy().load().type(NumberGeneration.class).filter("companyId", con.getCompanyId()).list();
//					if(con.getNumberRange()!=null ){
//						
//						logger.log(Level.SEVERE,"111111111111");
//
//						String selectedNumberRange = con.getNumberRange();
//						String prefixselectedNumberRange = "S_"+selectedNumberRange;
//						
//						for(int i=0;i<numbergenrationList.size();i++){
//							
//							if(prefixselectedNumberRange.equals(numbergenrationList.get(i).getProcessName())){
//								logger.log(Level.SEVERE,"22222222222");
//
//								numbergen=ofy().load().type(NumberGeneration.class).filter("companyId",con.getCompanyId()).filter("processName", prefixselectedNumberRange).filter("status", true).first().now();
//							}
//						}
//					}
//					else{
//						logger.log(Level.SEVERE,"33333333333333");
//
//						numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
//					}
//				}
//				else{
//					logger.log(Level.SEVERE,"44444444444444");
//
//					numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Service").filter("status", true).first().now();
//				}
//			 
//		 
//			/************************** ende here  some change while saving services bellow for normal services & taskqueue services*********************************/
//			
//		    long number = numbergen.getNumber();
//		    
//		    logger.log(Level.SEVERE,"Last Number===="+number);
//	    	 int count = (int) number;
	    	 
			
			if(item.getPrduct() instanceof ServiceProduct)
		    {
			   if(con.getCinfo()!=null)
			   {
				   if(con.getCompanyId()!=null)	
				   {
					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
					   filter("companyId",con.getCompanyId()).first().now();
				   }
				   else
				   {
					   cust=ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
							   first().now();  
				   }
			   }
			   
			   //  rohan has commented this code for for below commented reson 
			   // date : 
					//A patch as Neither Customer nor Contract has Customer Address
//					if(cust!=null)
//					    addr=cust.getSecondaryAdress();
				
				for(int k=0;k<con.getServiceScheduleList().size();k++)
				{
					if(item.getProductSrNo()==con.getServiceScheduleList().get(k).getSerSrNo()){
					
						/** Developed by : Rohan D Bhagde.
						 * Reason : added this code for adding selected customer branch address as service address.
						 * ie. map branch address in service address if branch is selected in service schedule pop up.
						 * 
						 *   date : 26/10/2016
						 */
						
						if(con.getServiceScheduleList().get(k).getScheduleProBranch().equalsIgnoreCase("Service Address"))
						{
							if(cust!=null)
							    addr=cust.getSecondaryAdress();
						}
						else
						{
							   if(con.getServiceScheduleList().get(k).getScheduleProBranch()!=null)	
							   {
								   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",con.getServiceScheduleList().get(k).getScheduleProBranch()).
								   filter("companyId",con.getCompanyId()).first().now();
							   }
							   else
							   {
								   custBrach=ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName",con.getServiceScheduleList().get(k).getScheduleProBranch()).
										   first().now();  
							   }
							   
							   addr = custBrach.getAddress();
							   
						}
						
						/**
						 *  ends here 
						 */
						
						
						if(totalservices > 50){
							
					
							String refNo="";
							if(con.getRefNo()!= null && !con.getRefNo().equals(""))
							{
								refNo = con.getRefNo();
							}
							else
							{
								refNo = 0+"";
							}
							
							String refNo2="";
							if(con.getRefNo2()!= null && !con.getRefNo2().equals(""))
							{
								refNo2 = con.getRefNo2();
							}
							else
							{
								refNo2 = 0+"";
							}
							
							String serviceRemark="";
							if(con.getServiceScheduleList().get(k).getServiceRemark()!=null && !con.getServiceScheduleList().get(k).getServiceRemark().equals("")){
								serviceRemark=con.getServiceScheduleList().get(k).getServiceRemark();
							}else{
								serviceRemark=0+"";
							}
							
							String techName ="";
							if(con.getTechnicianName()!= null && !con.getTechnicianName().equals(""))
							{
								techName = con.getTechnicianName();
							}
							else
							{
								techName = 0+"";
							}
							
							logger.log(Level.SEVERE,"WITH TASK QUEUE SEVICES");

							int setcount = ++count;
							int seviceindex = k+1;
							
							String serviceString="";
							
							/**29-09-2017 sagar sore [setting service value through taskqueue also added to serviceString]**/
							serviceValue=item.getTotalAmount()/(item.getNumberOfServices()*item.getQty());
							String primisesName ="";
							if(item.getPremisesDetails()!= null && !item.getPremisesDetails().equals(""))
							{
								primisesName = item.getPremisesDetails();
							}
							else
							{
								primisesName = 0+"";
							}
							
							/**
							 * nidhi
							 * 8-08-2018
							 * for map model and serial no
							 */

							String proSerialNo ="0", proModelNo = "0";
							
							if(item.getProModelNo() != null && !item.getProModelNo().equals("")){
								proModelNo = item.getProModelNo();
							}
						
							if(item.getProSerialNo() != null && !item.getProSerialNo().equals("")){
								proSerialNo = item.getProSerialNo();
							}
							
							/**
							 * @author Anil @since 27-05-2021
							 * if the first service of contract is before or equals todays date then it should be
							 * created with Completed status
							 * Requirement raised by Rahul Tiwari for Surat Pest
							 */
							String status=Service.SERVICESTATUSSCHEDULE;
							if(firstServCompFlag){
								DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
								dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
								String todaysDateStr = dateFormat.format(new Date());
								String servDateStr = dateFormat.format(con.getServiceScheduleList().get(k).getScheduleServiceDate());
								try {
									Date todayDate = dateFormat.parse(todaysDateStr);
									Date serviceDate = dateFormat.parse(servDateStr);
									if (con.getServiceScheduleList().get(k).getScheduleServiceNo() == 1) {
										if (serviceDate.equals(todayDate)|| serviceDate.before(todayDate)) {
											status=Service.SERVICESTATUSCOMPLETED;
										}
									}
								} catch (Exception e) {
	
								}
							}
							
							
							serviceString= con.getCinfo().getCount()+"$"+con.getCinfo().getEmail()+"$"+con.getCinfo().getFullName()+"$"+con.getCinfo().getPocName()+"$"+con.getCinfo().getCellNumber()
									+"$"+con.getCount()+"$"+con.getBranch()+"$"+status+"$"+con.getStartDate()+"$"+con.getEndDate()+"$"+false+
									"$"+seviceindex+"$"+"Periodic"+"$"+con.getServiceScheduleList().get(k).getScheduleServiceNo()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceDate()+
									"$"+addr.getAddrLine1()+"$"+addr.getAddrLine2()+"$"+addr.getCountry()+"$"+addr.getState()+"$"+addr.getCity()+"$"+addr.getLocality()+"$"+addr.getPin()+"$"+addr.getLandmark()+"$"+con.getScheduleServiceDay()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceTime()+
									"$"+con.getServiceScheduleList().get(k).getScheduleProBranch()+"$"+con.getServiceScheduleList().get(k).getScheduleServiceDay()+
									"$"+con.getCompanyId()+"$"+item.getPrduct().getComment()+"$"+item.getPrduct().getCommentdesc()+"$"+item.getPrduct().getCompanyId()+"$"+item.getPrduct().getCount()+"$"
									+item.getPrduct().getCreatedBy()+"$"+item.getPrduct().isDeleted()+"$"+item.getDuration()+"$"+item.getPrduct().getId()+"$"+item.getNumberOfServices()+"$"+item.getPrice()+"$"
									+item.getPrduct().getProductCategory()+"$"+item.getPrduct().getProductClassification()+"$"+item.getPrduct().getProductCode()+"$"
									+item.getPrduct().getProductGroup()+"$"+item.getPrduct().getProductName()+"$"+item.getPrduct().getProductType()+"$"+item.getPrduct().getRefNumber1()+"$"+item.getPrduct().getRefNumber2()+"$"
									+item.getPrduct().getServiceTax().isInclusive()+"$"+item.getPrduct().getServiceTax().getPercentage()+"$"+item.getPrduct().getServiceTax().getTaxConfigName()+"$"
									+item.getPrduct().getServiceTax().getTaxName()+"$"+item.getPrduct().getSpecifications()+"$"+item.getPrduct().isStatus()+"$"
									+item.getPrduct().getUnitOfMeasurement()+"$"+item.getPrduct().getUserId()+"$"+item.getPrduct().getVatTax().isInclusive()+"$"+item.getPrduct().getVatTax().getPercentage()+"$"
									+item.getPrduct().getVatTax().getTaxConfigName()+"$"+item.getPrduct().getVatTax().getTaxName()
									+"$"+setcount+"$"+con.getNumberRange()+"$"+con.getServiceScheduleList().get(k).isCheckService()
									+""+"$"+refNo+"$"+refNo2+"$"+serviceRemark+"$"+con.getServiceScheduleList().get(k).getWeekNo()
									+"$"+techName+"$"+String.format("%.2f", serviceValue)/**
									 nidhi 
									 date : 4-12-2017
									 set service type as process configration 
									 */+"$"+con.isServiceWiseBilling()
									+"$"+item.getProductSrNo()+"$"+confiFlag +"$"+con.getCategory()/**
									 * end *//**  nidhi Date : 29-01-2018 */+"$"+con.getPocName()
									 /** komal date 8.3.2018 **/+"$"+"0"+"$"+"na"+"$"+"0"+"$"+"na"+"$"+false
									 +"$"+primisesName+"$"+proModelNo+"$"+proSerialNo+"$"+"NA"
									 +"$"+" "+"$"+" "+"$"+" "+"$"+" "+"$"+" "+"$"+" "+"$"+" "+con.getDescription();//For Umas added description

							
							Queue queue = QueueFactory.getQueue("ContractServices-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/contractservicestaskqueue").param("servicekey", serviceString));
									
				}
				else
				{
					
					logger.log(Level.SEVERE,"FOR NORMAL Sevices");
					Service temp=new Service();
					
					temp.setPersonInfo(con.getCinfo());
					temp.setContractCount(con.getCount());
					temp.setProduct((ServiceProduct) item.getPrduct());
					temp.setBranch(con.getBranch());
//					temp.setStatus(Service.SERVICESTATUSSCHEDULE);
					/**
					 * Date 13 Feb 2017
					 * by Vijay
					 * for every service service engineer should be blank by default and select service engineer in service
					 */
//					temp.setEmployee(con.getEmployee());
					/**
					 * ends here 
					 */
					temp.setContractStartDate(con.getStartDate());
					temp.setContractEndDate(con.getEndDate());
					temp.setAdHocService(false);
					temp.setServiceIndexNo(k+1);
					temp.setServiceType("Periodic");
					temp.setServiceSerialNo(con.getServiceScheduleList().get(k).getScheduleServiceNo());
					temp.setServiceDate(con.getServiceScheduleList().get(k).getScheduleServiceDate());
			    	temp.setAddress(addr);
			    	temp.setRefNo(con.getRefNo());
			    	
			    	/**
					 * Date:24-01-2017 By Anil
					 * setting ref no.2 
					 */
					temp.setRefNo2(con.getRefNo2());
					/**
					 * End
					 */
			    	
//			    	if(con.getScheduleServiceDay()!=null){
//						temp.setServiceDay(con.getScheduleServiceDay());
//					}
					
					/**above old code commented by vijay
			    	 * Date 1 March 2017 changed by vijay for setter value service day in services
			    	 */
			    	if(con.getServiceScheduleList().get(k).getScheduleServiceDay()!=null){
						temp.setServiceDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
					}
			    	/**
			    	 * Ends here
			    	 */
					
					temp.setServiceTime(con.getServiceScheduleList().get(k).getScheduleServiceTime());
					temp.setServiceBranch(con.getServiceScheduleList().get(k).getScheduleProBranch());
					temp.setServiceDateDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
			    	
					temp.setBranch(con.getServiceScheduleList().get(k).getServicingBranch());
					temp.setServicingTime(con.getServiceScheduleList().get(k).getTotalServicingTime());
					
					/**
					 * @author Abhinav Bihade
					 * @since 05/12/2019
					 * As per Rahul Tiwari,Prasad Shilwant's Requirement Backdated Dated Quick Contract will completed Automatically and from todays contract(Features Contracts) it will be SCHEDULE for this Anil Pal and Vijay Chougule Has Guide me for the changes  
					 */
					System.out.println("in Quick Contract1");
					DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
					  dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					  String date = dateFormat.format(new Date());
					  String date2 =dateFormat.format(con.getServiceScheduleList().get(k).getScheduleServiceDate());
					try {
						Date today = dateFormat.parse(date);
						Date serviceDate = dateFormat.parse(date2);
					  System.out.println("today"+today);
					  System.out.println("serviceDate"+serviceDate);
					if(con.getServiceScheduleList().get(k).isCheckService()&& !serviceDate.equals(today) &&serviceDate.before(today)){
						temp.setStatus(Service.SERVICESTATUSCOMPLETED);

					}else{
						temp.setStatus(Service.SERVICESTATUSSCHEDULE);
						System.out.println("in Else of Quick Contract3");
					}
					
					
					/**
					 * @author Anil @since 27-05-2021
					 * if the first service of contract is before or equals todays date then it should be
					 * created with Completed status
					 * Requirement raised by Rahul Tiwari for Surat Pest
					 */
					
					if(firstServCompFlag&&con.getServiceScheduleList().get(k).getScheduleServiceNo()==1){
						if(serviceDate.equals(today)||serviceDate.before(today)){
							temp.setStatus(Service.SERVICESTATUSCOMPLETED);
						}
					}
					
					 } catch (ParseException e) {
							e.printStackTrace();
					}
					
					
					
					
					if(con.getNumberRange()!=null )
						temp.setNumberRange(con.getNumberRange());
					
					
					/**
					 * Date 07 Feb 2017
					 * vijay added this for in contract product level remark will set in service entity
					 */
					
					temp.setRemark(con.getServiceScheduleList().get(k).getServiceRemark());
					
					/*
					 * End here
					 */
					
					
					/**
					 * Date 02-04-2017 added by vijay for service week no of month
					 */
					temp.setServiceWeekNo(con.getServiceScheduleList().get(k).getWeekNo());
					/**
					 * ends here
					 */
					
					/**
					 * rohan added this code for technician 
					 */
					temp.setEmployee(con.getTechnicianName());
					
					/**
					 * ends here
					 */
					
					/** 29-07-2017 sagar sore setting service value**/
					temp.setServiceValue(Double.parseDouble(String.format("%.2f",(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())))));
					
					temp.setCompanyId(con.getCompanyId());
					
					
					/**
					 *  nidhi
					 *  4-12-2017
					 *  process config
					 *    "Service" - "ContractCategoryAsServiceType"
					 */
					if(confiFlag){
						temp.setServiceType(con.getCategory());
						
					}
					/**
					 * end
					 */
					/**
					 *  nidhi
					 *  29-01-2018
					 *  for copy pocname in person info
					 */
					if(con.getPocName()!=null && !con.getPocName().trim().equals("")){
						temp.getPersonInfo().setPocName(con.getPocName());
					}
					/**
					 *  end
					 */
					
					/**
					 * nidhi
					 * 3-08-2018
					 * for save primies
					 * 
					 */
						if(item.getPremisesDetails()!=null){
							temp.setPremises(item.getPremisesDetails());
						}
						/**
						 * nidhi
						 * 8-08-2018
						 */
						if(item.getProModelNo() != null){
							temp.setProModelNo(item.getProModelNo());
						}
						if(item.getProSerialNo()!=null){
							temp.setProSerialNo(item.getProSerialNo());
						}
						/**
						 * end
						 */
					/**
					 * @author Anil
					 * @Since 13-08-2020
					 * Setting contract description in service description
					 * For UMAS raised by Vaishnavi and Nitin
					 */
					temp.setDescription(con.getDescription());
					System.out.println("sr no"+temp.getServiceSrNo());
					System.out.println("no "+temp.getServiceSerialNo());
					services.add(temp);
					
				
					}
				}
		    }
				
		}
			logger.log(Level.SEVERE,"Services Creation Complete");
			System.out.println("services"+services.size());
//			if(totalservices>50){
//				numbergen.setNumber(count);
//				ofy().save().entity(numbergen);
//		    	 GenricServiceImpl impl = new GenricServiceImpl();
//				  impl.save(numbergen);
//			}else{
				
			/*** 
			 * @author  Vijay Date 31-08-2020
			 * Des :- Below code commented becuase service will create after all product loop
			 * due to below code services creating with 0 id's
			 */
//				GenricServiceImpl impl=new GenricServiceImpl();
//				if(services!=null&&services.size()!=0){
//					logger.log(Level.SEVERE,"Saving Services");
//				    impl.save(services);
//				}
				
//			}
		}
		
		/**
		 *  nidhi
		 *  4-12-2017
		 *  process config
		 *    "Service" - "ContractCategoryAsServiceType"
		 */
		if(confiFlag){
			boolean status = ServerAppUtility.checkServiceTypeAvailableOrNot(con.getCategory(), con.getCompanyId(), con.getApproverName());
		}
		/**
		 * end
		 */
		/*** 
		 * @author  Vijay Date 31-08-2020
		 * Des :- added above code after product loop to create proper service id's 
		 */
		GenricServiceImpl impl=new GenricServiceImpl();
		if(services!=null&&services.size()!=0){
			logger.log(Level.SEVERE,"Saving Services");
		    impl.save(services);
		}
		/**
		 * ends here
		 */
		
	}

	
	private double gettotalnumberofservices(List<SalesLineItem> getproductitem) {
		double totalservices = 0;

		for(int i=0;i<getproductitem.size();i++){
			
			totalservices = totalservices + (getproductitem.get(i).getQty()*getproductitem.get(i).getNumberOfServices());
		}
		
		return totalservices;
		
	}
	
	
	private boolean checkprocessconfiguration(long companyId) {

		boolean flag = false;
		System.out.println("hi ===== ");
		ProcessName processname = ofy().load().type(ProcessName.class).filter("processName", "NumberRange").filter("status",true).filter("companyId", companyId).first().now();
		ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "NumberRange").filter("processList.processType", "NumberRangeProcess").filter("processList.status", true).filter("companyId", companyId).first().now();
		if(processname!=null){
			System.out.println("In process name");
			if(processconfiguration!=null){
				System.out.println("In Processconfig");
				flag=true;
			}
		}
		
		return flag;
	}
	
	@OnSave
	@GwtIncompatible
	protected void changeCustomerStatus(Contract con) {
		
		if(con.getCinfo()!=null)
 	{
			List<Customer> customerList=null;
			if(con.getCompanyId()!=null)
			{
				customerList =	ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).
				filter("companyId",con.getCompanyId())
				.list();
 	
			}
		else
			customerList =	ofy().load().type(Customer.class).filter("count",con.getCinfo().getCount()).list();
	    
		if(customerList!=null&&customerList.size()!=0)
 	 {
 	
			if(con.getStatus().equals(Contract.APPROVED))
			{
				for(Customer cust:customerList)
				{
					cust.setStatus(Customer.CUSTOMERACTIVE);
				}
			}
			
			ofy().save().entities(customerList).now();
 	 }
		}
	}
	
	
	/**
	 * *************************************  Services section ends here ***********************
	 */

	
	/**
	 * Rohan added this code for making quotation status Successful
	 * @param model
	 */
	private void changeQuotationStatusToSucessfull(Contract model) {
		
		Quotation quot = ofy().load().type(Quotation.class).filter("count",model.getQuotationCount()).filter("companyId",model.getCompanyId()).first().now();
		
		quot.setStatus("Successful");
		ofy().save().entity(quot);
		
	}
	
	/**
	 * ends here
	 */
	
	/**
	 * Here i am getting contract percentage for payment terms based on net payable and  payment received 
	 */
	
	private double getpercentage(double netpayable, double paymentRecieved) {
		double totalpercentage;
		totalpercentage= paymentRecieved/netpayable*100;
		return totalpercentage;
	}
	
	/**
	 * ends here
	 */
	
	
	/*************************** here Billing Document section related Methods start here ***************/
	
public void createBillingDocuments(Contract model,double percentage,boolean paymentflag, boolean paymentermscomment,boolean partialquickcontractcontractoneinvoice,String balancedPayment) {
		
		/**
		 * @author Anil @since 23-09-2021
		 * Multiple bill created aginst quick contract if contract value is zero
		 * issue raised by Rahul Tiwari for eco safe
		 */
		if(model!=null){
			if(model.getNetpayable()==0){
				logger.log(Level.SEVERE, "Bill not created as contract value is zero!!");
				return;
			}
		}
	
		ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();


		/**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", model.getCompanyId());
		 
		 /**
		  * end
		  */
		
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());

		BillingDocument billingDocEntity=new BillingDocument();
		List<SalesOrderProductLineItem> salesProdLis=null;
		List<ContractCharges> billTaxLis=null;
		ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
		PaymentTerms paymentTerms=new PaymentTerms();
		
		Date conStartDate=null;
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		System.out.println("Contract Start Date"+conStartDate);
	
		/**
		 * Date 2 march 2017
		 * below code commented by vijay and new code added 
		 */
		/***********************Invoice Date******************************/
//		
//		Date calculatedInvoiceDate=null;
//		
//		if(model.getQuickContractInvoiceDate()!=null){
//			calculatedInvoiceDate = model.getQuickContractInvoiceDate();
//		}
//		else
//		{
//		
//		logger.log(Level.SEVERE,"Contract Date As per IST"+DateUtility.getDateWithTimeZone("IST",model.getContractDate()));
//		logger.log(Level.SEVERE,"Contract Date As Per Saved"+model.getContractDate());
//		
//		Calendar calInvoiceDate = Calendar.getInstance();
//		if(model.getContractDate()!=null){
//			Date serConDate=DateUtility.getDateWithTimeZone("IST", model.getContractDate());
//			calInvoiceDate.setTime(serConDate);
//		}
//		else{
//			conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
//			calInvoiceDate.setTime(conStartDate);
//		}
//		
//		calInvoiceDate.add(Calendar.DATE, 0);
////		Date calculatedInvoiceDate=null;
//		try {
//			calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
//			calInvoiceDate.set(Calendar.MINUTE,59);
//			calInvoiceDate.set(Calendar.SECOND,59);
//			calInvoiceDate.set(Calendar.MILLISECOND,999);
//			calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
//			logger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
//			if(currentDate.after(calculatedInvoiceDate))
//			{
//				calculatedInvoiceDate=currentDate;
//			}
//			
//			
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		}
//		/***********************************************************************************/
//		
//		/*********************************Billing Date************************************/
//		
//		Date calBillingDate=null;
//		
//		if(model.getQuickContractInvoiceDate()!=null){
//			calBillingDate = model.getQuickContractInvoiceDate();
//		}else{
////		Date calBillingDate=null;
//		Calendar calBilling=Calendar.getInstance();
//		if(calculatedInvoiceDate!=null){
//			calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
//		}
//		
//		logger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
//		
//		calBilling.add(Calendar.DATE, -2);
//		try {
//			calBilling.set(Calendar.HOUR_OF_DAY,23);
//			calBilling.set(Calendar.MINUTE,59);
//			calBilling.set(Calendar.SECOND,59);
//			calBilling.set(Calendar.MILLISECOND,999);
//			calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
//			
//			if(currentDate.after(calBillingDate)){
//				calBillingDate=currentDate;
//			}
//			
//			logger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		}
//		/***************************************************************************************/
//		
//		/********************************Payment Date******************************************/
//		
//		Date calPaymentDate=null;
//		
//		if(model.getQuickContractInvoiceDate()!=null){
//			calPaymentDate = model.getQuickContractInvoiceDate();
//		}else{
////		Date calPaymentDate=null;
//		int creditVal=0;
//		if(model.getCreditPeriod()!=null){
//			creditVal=model.getCreditPeriod();
//		}
//		
//		Calendar calPayment=Calendar.getInstance();
//		if(calculatedInvoiceDate!=null){
//			calPayment.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate));
//		}
//		logger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
//		
//		calPayment.add(Calendar.DATE, creditVal);
//		try {
//			calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
//			calPayment.set(Calendar.HOUR_OF_DAY,23);
//			calPayment.set(Calendar.MINUTE,59);
//			calPayment.set(Calendar.SECOND,59);
//			calPayment.set(Calendar.MILLISECOND,999);
//			calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
//			logger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		}
//		/***********************************************************************************/
//		
//		System.out.println("Billing date to be saved==="+calBillingDate);
		
		/******************************************************************************/
		
		if(model.getCinfo()!=null)
			billingDocEntity.setPersonInfo(model.getCinfo());
		if(model.getCount()!=0)
			billingDocEntity.setContractCount(model.getCount());
		if(model.getStartDate()!=null)
			billingDocEntity.setContractStartDate(model.getStartDate());
		if(model.getEndDate()!=null)
			billingDocEntity.setContractEndDate(model.getEndDate());
		if(model.getNetpayable()!=0)
			billingDocEntity.setTotalSalesAmount(model.getNetpayable());
		salesProdLis = retrieveSalesproduct(model,percentage);
		ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
		salesOrdArr.addAll(salesProdLis);
		System.out.println("Size here here"+salesOrdArr.size());
		if(salesOrdArr.size()!=0)
			billingDocEntity.setSalesOrderProducts(salesOrdArr);
		if(model.getCompanyId()!=null)
		billingDocEntity.setCompanyId(model.getCompanyId());

		/**
		 * Date 2 March 2017
		 * below code added by vijay
		 * if invoice date is selected in form then as per invoice date billing invoice and payment date is set in billing invoice pament docs
		 * if invoice date is not selected in form then as per contract date billing invoice and payment date set in billing invoice pament docs
		 */
		Date invoiceDate=null;
		if(model.getQuickContractInvoiceDate()!=null){
			invoiceDate = model.getQuickContractInvoiceDate();
		}
		else
		{
			invoiceDate = model.getContractDate();
		}
		logger.log(Level.SEVERE," Billing Invoice paymnent Date"+invoiceDate);
		
		if(invoiceDate!=null){
			billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
			logger.log(Level.SEVERE," Billing Date While SAving"+billingDocEntity.getBillingDate());
			billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
			logger.log(Level.SEVERE," Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
			billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
			logger.log(Level.SEVERE," Payment Date While SAving"+billingDocEntity.getPaymentDate());
		}
		
		/**
		 * ends here
		 */
		if(model.getPaymentMethod()!=null)
			billingDocEntity.setPaymentMethod(model.getPaymentMethod());
		if(model.getApproverName()!=null)
			billingDocEntity.setApproverName(model.getApproverName());
		if(model.getEmployee()!=null)
			billingDocEntity.setEmployee(model.getEmployee());
		if(model.getBranch()!=null)
			billingDocEntity.setBranch(model.getBranch());
		ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
		
		if(model.getProductTaxes().size()!=0){
			ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
			billTaxLis=listForBillingTaxesforquickcontract(model,percentage);
			billTaxArr.addAll(billTaxLis);
			billingDocEntity.setBillingTaxes(billTaxArr);
			billTaxArrROHAN.addAll(billTaxLis);
		}

		if(model.getStartDate()!=null){
			billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST", model.getStartDate()));
		}
		if(model.getEndDate()!=null){
			billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST", model.getEndDate()));
		}
		
//***********************changes made by rohan for saving gross value *************	
		double grossValue=model.getTotalAmount();
		billingDocEntity.setGrossValue(grossValue);
//********************************changes ends here *******************************		
		double totBillAmt=getTotalBillAmtforQuickContract(salesProdLis);
		
		double taxAmt= getTotalFromTaxTableforQickContract(billTaxArrROHAN);
		
		System.out.println("totBillAmt" +totBillAmt);
		System.out.println(" totBillAmt with round == "+Math.round(totBillAmt));
		
		System.out.println("taxAmt "+taxAmt);
		System.out.println("taxAmt with round"+Math.round(taxAmt));
		
		double totalBillingAmount = totBillAmt+taxAmt;
		System.out.println("Toatl billing amount without round ===  "+totalBillingAmount);
		System.out.println("Toatl billing amount with round ===  "+Math.round(totalBillingAmount));
		
		billingDocEntity.setTotalBillingAmount(totalBillingAmount);
		
		billingDocEntity.setOrderCreationDate(model.getCreationDate());
		
		DecimalFormat df = new DecimalFormat("0.00");
			
		
		paymentTerms.setPayTermDays(0);
		
		double paypercent= Math.round(percentage);
		System.out.println("while saving percentage === "+paypercent);
		paymentTerms.setPayTermPercent(paypercent);
		if(paymentermscomment){
			paymentTerms.setPayTermComment("Payment Recieved");
		}else{
			paymentTerms.setPayTermComment("Balance Payment");
		}
		billingPayTerms.add(paymentTerms);
		billingDocEntity.setArrPayTerms(billingPayTerms);
		billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
		billingDocEntity.setStatus(BillingDocument.APPROVED);
		billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
		billingDocEntity.setOrderCformStatus("");
		billingDocEntity.setOrderCformPercent(-1);


		/*************************** vijay number range **************************/
		if(model.getNumberRange()!=null)
			billingDocEntity.setNumberRange(model.getNumberRange());
		/******************************************************************************/
		if(model.getCustomerGSTNumber()!=null && !model.getCustomerGSTNumber().equals(""))
		billingDocEntity.setGstinNumber(model.getCustomerGSTNumber());
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(billingDocEntity);
		
		logger.log(Level.SEVERE,"Here Billing created successfully");
		
		List<ContractCharges> billingtaxlist = billingDocEntity.getBillingTaxes();
		List<ContractCharges> taxBillingArr = new ArrayList<ContractCharges>();
		if(billingtaxlist.size()!=0){
			System.out.println("SIZE====="+billingtaxlist.size());
			for(int k=0;k<billingtaxlist.size();k++){
				double totalCalcAmt=0;
				totalCalcAmt = billingtaxlist.get(k).getTaxChargeAssesVal()*billingtaxlist.get(k).getTaxChargePercent()/100;
				billingtaxlist.get(k).setPayableAmt(totalCalcAmt);
				System.out.println("AMONT+++"+totalCalcAmt);
				taxBillingArr.add(billingtaxlist.get(k));
			}
			billingDocEntity.setBillingTaxes(taxBillingArr);
		
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(billingDocEntity);
			
		}
		
		
		/*************** new code for issues *******************************/
		double payableamt=0;
		payableamt = calculateTotalBillAmt(billingDocEntity.getSalesOrderProducts()) + calculateTotalTaxes(billingDocEntity.getBillingTaxes()) + calculateTotalBillingCharges(billingDocEntity.getBillingOtherCharges());
		payableamt=Math.round(payableamt);
		System.out.println("Payable amt =="+payableamt);
		
		List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
		
		BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
		
		if(billingDocEntity.getContractCount()!=null){
			billingDocDetails.setOrderId(billingDocEntity.getContractCount());
		}
		
		if(billingDocEntity.getCount()!=0)
			billingDocDetails.setBillId(billingDocEntity.getCount());
		if(billingDocEntity.getBillingDate()!=null)
			billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
		if(billingDocEntity.getBillAmount()!=null)
			billingDocDetails.setBillAmount(payableamt);
		if(billingDocEntity.getStatus()!=null)
			billingDocDetails.setBillstatus(billingDocEntity.getStatus());
		System.out.println("Billing IDDDDD===="+billingDocDetails.getBillId());
		billtablelis.add(billingDocDetails);
		
		billingDocEntity.setTotalBillingAmount(payableamt);
		System.out.println(" getTotalBillingAmount amount ===");
		System.out.println("billing amount ==" +billingDocEntity.getBillAmount());
		System.out.println("billing amount ####  ==" +Math.round(billingDocEntity.getTotalBillingAmount()));
		
		ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
		billtablearr.addAll(billtablelis);
		billingDocEntity.setArrayBillingDocument(billtablelis);
		
		
		/** Date 03-09-2017 added by vijay for total amount before taxes**/
		billingDocEntity.setTotalAmount(Double.parseDouble(df.format(totBillAmt)));
		/** Date 05-09-2017 added by vijay for Discount amount**/
			billingDocEntity.setDiscountAmt(0);
		/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
		billingDocEntity.setFinalTotalAmt(Double.parseDouble(df.format(totBillAmt)));
		 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
		billingDocEntity.setTotalAmtIncludingTax(payableamt);
		/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
		billingDocEntity.setGrandTotalAmount(payableamt);
		
		
		/**
		 *  nidhi
		 *  Date : 4-12-2017
		 *  For copy configration details to bill
		 */
		if(confiFlag){
			billingDocEntity.setBillingCategory(model.getCategory());
			billingDocEntity.setBillingType(model.getType());
			billingDocEntity.setBillingGroup(model.getGroup());
		}
		/**
		 *  end
		 */
		/** date 06-02-2018 added by komal for consolidate price**/
		billingDocEntity.setConsolidatePrice(model.isConsolidatePrice());
		/**
		 * end komal
		 */
		/**
		 * nidhi
		 * Date : 29-01-2018
		 * save poc name from multiple contact list
		 */
		if(model.getPocName()!=null && !model.getPocName().trim().equals("")){
			billingDocEntity.getPersonInfo().setPocName(model.getPocName());
		}
		/** 
		 * end
		 */
		
		
		/**
		 * @author Anil @since 24-05-2021
		 * Mapping contract start date and end date as billing from date and to date
		 * Raised by Rahul Tiwari for Surat Pest Control
		 */
		if(model.getStartDate()!=null){
			billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", model.getStartDate()));
		}
		if(model.getEndDate()!=null){
			billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST", model.getEndDate()));
		}
		
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(billingDocEntity);
		
		/******************************************************************/
		
		

		/**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		if(confiFlag){
			boolean updateFlag = false;
			updateFlag = ServerAppUtility.checkContractCategoryAvailableOrNot(model.getCategory(), model.getCompanyId(), model.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkContractTypeAvailableOrNot(model.getType(), model.getCategory(), model.getCompanyId(), model.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkServiceTypeAvailableOrNot(model.getGroup(), model.getCompanyId(), model.getApproverName(), 46);
		}
		/**
		 * end
		 */
		logger.log(Level.SEVERE,"Here Billing updated successfully");
		
		System.out.println("Here started for creting invoice =======================");
		logger.log(Level.SEVERE,"Here started for creting invoice =======================");

		
//		Createinvoice(billingDocEntity,paymentflag,partialquickcontractcontractoneinvoice);
	//  rohan remove paymentflag and send true for all invoices as they are received
			if(!balancedPayment.equalsIgnoreCase("ClosedPayment")){
				Createinvoice(model,billingDocEntity,true,partialquickcontractcontractoneinvoice);
			}	
		
	
	}
	
private double calculateTotalBillingCharges(List<ContractCharges> billingOtherCharges) {

	List<ContractCharges> list = billingOtherCharges;
	double sum = 0;
	for (int i = 0; i < list.size(); i++) {
		ContractCharges entity = list.get(i);
		sum = sum+ (entity.getChargesBalanceAmt() * entity.getPaypercent() / 100);
	}
	return sum;

}


private double calculateTotalTaxes(List<ContractCharges> billingTaxes) {

	List<ContractCharges>list=billingTaxes;
	double sum=0;
	for(int i=0;i<list.size();i++)
	{
		ContractCharges entity=list.get(i);
		sum=sum+(entity.getTaxChargeAssesVal()*entity.getTaxChargePercent()/100);
	}
	System.out.println("Sum Taxes"+sum);
	return sum;

}

private double calculateTotalBillAmt(ArrayList<SalesOrderProductLineItem> salesOrderProducts) {

	System.out.println("Entered in table calculate");
	List<SalesOrderProductLineItem>list=salesOrderProducts;

	double caltotalamt=0;
	for(int i=0;i<list.size();i++)
	{
		SalesOrderProductLineItem entity=list.get(i);
		
		if(entity.getFlatDiscount()==0)
		{
			caltotalamt=caltotalamt+(entity.getBaseBillingAmount()*entity.getPaymentPercent())/100;
		}
		else
		{
			caltotalamt=caltotalamt+((entity.getBaseBillingAmount()*entity.getPaymentPercent())/100)-entity.getFlatDiscount();
		}
	}
	return caltotalamt;

}

private double getTotalFromTaxTableforQickContract(	ArrayList<ContractCharges> lisForTotalBill) {
	// TODO Auto-generated method stub
	
	double totalCalcAmt=0;
	for(int i=0;i<lisForTotalBill.size();i++)
	{
		//*****old code   
		totalCalcAmt=lisForTotalBill.get(i).getTaxChargeAssesVal()*lisForTotalBill.get(i).getTaxChargePercent()/100;
		
		System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getChargesBalanceAmt());
		System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getPayableAmt());
		System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getTaxChargeAbsVal());
		System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getTaxChargeAssesVal());
		
		//***************new code 
//		saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getTotalAmount();
	}
	

	System.out.println("Save Total Billing Amount"+totalCalcAmt);
	return totalCalcAmt;


}

private double getTotalBillAmtforQuickContract(List<SalesOrderProductLineItem> lisForTotalBill) {

	double saveTotalBillAmt=0;
	for(int i=0;i<lisForTotalBill.size();i++)
	{
		//*****old code   
		saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getBaseBillingAmount();
		
		//***************new code 
//		saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getTotalAmount();
	}
	
	System.out.println("Save Total Billing Amount"+saveTotalBillAmt);
	return saveTotalBillAmt;


}

private List<ContractCharges> listForBillingTaxesforquickcontract(
		Contract model, double paymentRecieved) {


	ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
	double assessValue=0;
	for(int i=0;i<model.productTaxes.size();i++){
		ContractCharges taxDetails=new ContractCharges();
		taxDetails.setTaxChargeName(model.getProductTaxes().get(i).getChargeName());
		taxDetails.setTaxChargePercent(model.getProductTaxes().get(i).getChargePercent());
		assessValue=model.getProductTaxes().get(i).getAssessableAmount()*paymentRecieved/100;
		taxDetails.setTaxChargeAssesVal(assessValue);
		
		taxDetails.setIdentifyTaxCharge(model.getProductTaxes().get(i).getIndexCheck());
		arrBillTax.add(taxDetails);
	}
	return arrBillTax;


}

private List<SalesOrderProductLineItem> retrieveSalesproduct(Contract model, double percentage) {


	ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
	double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0,discAmt=0;
	String prodDesc1="",prodDesc2="";
	for(int i=0;i<model.getItems().size();i++)
	{
		SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", model.getItems().get(i).getPrduct().getCount()).first().now();
		if(superProdEntity!=null){
			System.out.println("Superprod Not Null");
			if(superProdEntity.getComment()!=null){
				System.out.println("Desc 1 Not Null");
				prodDesc1=superProdEntity.getComment();
			}
			if(superProdEntity.getCommentdesc()!=null){
				System.out.println("Desc 2 Not Null");
				prodDesc2=superProdEntity.getCommentdesc();
			}
		}
		
		SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
		//SuperProduct productEnt=this.getItems().get(i).getPrduct();
		salesOrder.setProdId(model.getItems().get(i).getPrduct().getCount());
		salesOrder.setProdCategory(model.getItems().get(i).getProductCategory());
		salesOrder.setProdCode(model.getItems().get(i).getProductCode());
		salesOrder.setProdName(model.getItems().get(i).getProductName());
		salesOrder.setQuantity(model.getItems().get(i).getQty());
		salesOrder.setOrderDuration(model.getItems().get(i).getDuration());
		salesOrder.setOrderServices(model.getItems().get(i).getNumberOfServices());
		totalTax=model.removeTaxAmt(model.getItems().get(i).getPrduct());
		System.out.println("totalTax =="+totalTax);
		prodPrice=model.getItems().get(i).getPrice()-totalTax;
		System.out.println("Prod Price"+prodPrice);
		salesOrder.setPrice(prodPrice);
		salesOrder.setVatTaxEdit(model.getItems().get(i).getVatTaxEdit());
		salesOrder.setServiceTaxEdit(model.getItems().get(i).getServiceTaxEdit());
		salesOrder.setVatTax(model.getItems().get(i).getVatTax());
		salesOrder.setServiceTax(model.getItems().get(i).getServiceTax());
		
		salesOrder.setProdPercDiscount(model.getItems().get(i).getPercentageDiscount());
		salesOrder.setProdDesc1(prodDesc1);
		salesOrder.setProdDesc2(prodDesc2);
		if(model.getItems().get(i).getUnitOfMeasurement()!=null){
			salesOrder.setUnitOfMeasurement(model.getItems().get(i).getUnitOfMeasurement());
		}
		
		
		//  rohan added this code 
		
		salesOrder.setArea(model.getItems().get(i).getArea());
		
		//*****************rohan commented this code***************** 
		
		
		salesOrder.setDiscountAmt(model.getItems().get(i).getDiscountAmt());
		
		
//		if(this.getItems().get(i).getPercentageDiscount()!=0){
//			percAmt=prodPrice-(prodPrice*this.getItems().get(i).getPercentageDiscount()/100);
//			percAmt=percAmt*this.getItems().get(i).getQty();
//		}
//		if(this.getItems().get(i).getPercentageDiscount()==0){
//			percAmt=prodPrice*this.getItems().get(i).getQty();
//		}
		
		//   rohan Bhagde make changes here for consideration of area in the calculations
		
		
		double area =0;
		if(!model.getItems().get(i).getArea().equalsIgnoreCase("NA"))
		{
			area = Double.parseDouble(model.getItems().get(i).getArea());
		}
		else
		{
			area = 1.0;
		}
		
		
		if((model.getItems().get(i).getPercentageDiscount()==null && model.getItems().get(i).getPercentageDiscount()==0) && (model.getItems().get(i).getDiscountAmt()==0) ){
			
			System.out.println("inside both 0 condition");
			percAmt=prodPrice * area;
		}
		
		else if((model.getItems().get(i).getPercentageDiscount()!=null)&& (model.getItems().get(i).getDiscountAmt()!=0)){
			
			System.out.println("inside both not null condition");
			percAmt=prodPrice * area;
			percAmt=percAmt-(percAmt*model.getItems().get(i).getPercentageDiscount()/100);
			percAmt=percAmt-(model.getItems().get(i).getDiscountAmt());
//			percAmt=percAmt;
		}
		else 
		{
			System.out.println("inside oneof the null condition");
			
				if(model.getItems().get(i).getPercentageDiscount()!=null && model.getItems().get(i).getPercentageDiscount()!=0){
					System.out.println("inside getPercentageDiscount oneof the null condition");
					percAmt=prodPrice * area;
					percAmt=percAmt-(percAmt*model.getItems().get(i).getPercentageDiscount()/100);
					
				}
				else 
				{
					System.out.println("inside getDiscountAmt oneof the null condition");
					percAmt=prodPrice * area;
					percAmt=percAmt-(model.getItems().get(i).getDiscountAmt());
				}
//				percAmt=percAmt;
		}
		
		salesOrder.setTotalAmount(percAmt);
		
		System.out.println("percAmt = = "+percAmt);
		System.out.println("percAmt with math round = = "+ Math.round(percAmt));

		System.out.println("payTermPercent === "+percentage);
		
		if(percentage!=0){
			baseBillingAmount=(percAmt*percentage)/100;
		}
	
		System.out.println(" withoud round ==== "+baseBillingAmount);
		System.out.println(" after round ==== "+Math.round(baseBillingAmount));
		
		salesOrder.setBaseBillingAmount(baseBillingAmount);
		salesOrder.setPaymentPercent(100.0);
		
		/**
		 * Date : 04-08-2017 By Anil
		 * Setting base bill amount to base payment amount
		 */
		salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
		/**
		 * End
		 */
		
		salesOrder.setIndexVal(i+1);
		/**
		 * nidhi
		 * 20-06-2018
		 * for product sr no
		 */
		salesOrder.setProductSrNumber(model.getItems().get(i).getProductSrNo());
		salesOrder.setPrduct(superProdEntity);
		/**
		 * nidhi
		 * 10-08-2018
		 * 
		 */
		salesOrder.setProModelNo(model.getItems().get(i).getProModelNo());
		salesOrder.setProSerialNo(model.getItems().get(i).getProSerialNo());
		/**
		 * end
		 */
		salesProdArr.add(salesOrder);
	}
	return salesProdArr;
	
   }
  
 /**************************************** Billing Related Methods Ends here ***********************/


 /******************************** invoice methods start here ****************************************/

private void Createinvoice(Contract model,BillingDocument billingDocEntity,boolean paymentflag,boolean partialquickcontractcontractoneinvoice) {
	// TODO Auto-generated method stub
	System.out.println("GET STATUS"+billingDocEntity.getStatus());
	System.out.println("========"+billingDocEntity.getContractCount());
	
	 /**
	  *  nidhi
	  *  Date : 4-12-2017
	  *  Check process configration for 
	  */
	 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", billingDocEntity.getCompanyId());
	 
	 /**
	  * end
	  */
	
	
	final Invoice inventity = new Invoice();
	PersonInfo pinfo = new PersonInfo();
	if (billingDocEntity.getPersonInfo().getCount()!=0) {
		pinfo.setCount(billingDocEntity.getPersonInfo().getCount());
	}
	if (billingDocEntity.getPersonInfo().getFullName() != null) {
		pinfo.setFullName(billingDocEntity.getPersonInfo().getFullName());
	}
	if (billingDocEntity.getPersonInfo().getCellNumber() != null) {
		pinfo.setCellNumber(billingDocEntity.getPersonInfo().getCellNumber());
	}
	if (billingDocEntity.getPersonInfo().getPocName()!= null) {
		pinfo.setPocName(billingDocEntity.getPersonInfo().getPocName());
	}

	if (billingDocEntity.getGrossValue() != 0) {
		inventity.setGrossValue(billingDocEntity.getGrossValue());
	}


	if (pinfo != null) {
		inventity.setPersonInfo(pinfo);
	}

	if (billingDocEntity.getContractCount()!=0)
		inventity.setContractCount(billingDocEntity.getContractCount());
	if (billingDocEntity.getContractStartDate() != null) {
		inventity.setContractStartDate(billingDocEntity.getContractStartDate());
	}
	if (billingDocEntity.getContractEndDate() != null) {
		inventity.setContractEndDate(billingDocEntity.getContractEndDate());
	}
	if (billingDocEntity.getTotalBillingAmount()!=null)
		inventity.setTotalSalesAmount(Math.round(billingDocEntity.getTotalSalesAmount()));
	
	
	
//	List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
//	
//	BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
//	
//	if(billingDocEntity.getContractCount()!=null){
//		billingDocDetails.setOrderId(billingDocEntity.getContractCount());
//	}
//	
//	if(billingDocEntity.getCount()!=0)
//		billingDocDetails.setBillId(billingDocEntity.getCount());
//	if(billingDocEntity.getBillingDate()!=null)
//		billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
//	if(billingDocEntity.getBillAmount()!=null)
//		billingDocDetails.setBillAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
//	if(billingDocEntity.getStatus()!=null)
//		billingDocDetails.setBillstatus(billingDocEntity.getStatus());
//	billtablelis.add(billingDocDetails);
//	
//	System.out.println("billing amount ==" +billingDocEntity.getBillAmount());
//	System.out.println("billing amount ####  ==" +Math.round(billingDocEntity.getTotalBillingAmount()));
//	
//	ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
//	billtablearr.addAll(billtablelis);
//	inventity.setArrayBillingDocument(billtablearr);
	
	inventity.setArrayBillingDocument(billingDocEntity.getArrayBillingDocument());
	if (billingDocEntity.getTotalBillingAmount()!=null)
		inventity.setTotalBillingAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
	
	if (billingDocEntity.getTotalBillingAmount()!=null)
		inventity.setNetPayable(Math.round(billingDocEntity.getTotalBillingAmount()));
	
	inventity.setDiscount(0.0);
	
	/** Date 06-09-2017 added by vijay for total amt new change *******/
	inventity.setTotalAmtExcludingTax(billingDocEntity.getTotalAmount());
	inventity.setDiscountAmt(billingDocEntity.getDiscountAmt());
	inventity.setFinalTotalAmt(billingDocEntity.getTotalAmount());
	inventity.setTotalAmtIncludingTax(billingDocEntity.getTotalAmtIncludingTax());
	/** ends here **********************/
	
	if (billingDocEntity.getInvoiceDate() != null)
		inventity.setInvoiceDate(billingDocEntity.getInvoiceDate());
	if (billingDocEntity.getPaymentDate() != null)
		inventity.setPaymentDate(billingDocEntity.getPaymentDate());
	if (billingDocEntity.getApproverName() != null)
		inventity.setApproverName(billingDocEntity.getApproverName());
	if (billingDocEntity.getEmployee() != null)
		inventity.setEmployee(billingDocEntity.getEmployee());
	if (billingDocEntity.getBranch()!= null)
		inventity.setBranch(billingDocEntity.getBranch());
	
	inventity.setOrderCreationDate(billingDocEntity.getOrderCreationDate());
	inventity.setCompanyId(billingDocEntity.getCompanyId());
	inventity.setInvoiceAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
	inventity.setInvoiceType(AppConstants.CREATETAXINVOICE);
	inventity.setPaymentMethod(billingDocEntity.getPaymentMethod());
	inventity.setAccountType(billingDocEntity.getAccountType());
	inventity.setTypeOfOrder(billingDocEntity.getTypeOfOrder());
	inventity.setStatus(Invoice.APPROVED);
	
	List<SalesOrderProductLineItem>productlist=billingDocEntity.getSalesOrderProducts();
	ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
	prodList.addAll(productlist);
	System.out.println();
	System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());

	List<ContractCharges>taxestable=billingDocEntity.getBillingTaxes();
	ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
	taxesList.addAll(taxestable);
	System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
	
	List<ContractCharges>otherChargesable=billingDocEntity.getBillingOtherCharges();
	ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
	otherchargesList.addAll(otherChargesable);
	System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
	inventity.setSalesOrderProductFromBilling(prodList);
	inventity.setBillingTaxes(taxesList);
	inventity.setBillingOtherCharges(otherchargesList);
	
	inventity.setCustomerBranch(billingDocEntity.getCustomerBranch());

	inventity.setAccountType(billingDocEntity.getAccountType());

//	ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
//	PaymentTerms paymentTerms=new PaymentTerms();
//	
//	paymentTerms.setPayTermDays(0);
//	paymentTerms.setPayTermPercent(percentage);
//	paymentTerms.setPayTermComment("Payment Recieved");
//	billingPayTerms.add(paymentTerms);
//	billingDocEntity.setArrPayTerms(billingPayTerms);
	
	inventity.setArrPayTerms(billingDocEntity.getArrPayTerms());

	
	if(billingDocEntity.getNumberRange()!=null)
		inventity.setNumberRange(billingDocEntity.getNumberRange());
	
	if(billingDocEntity.getGstinNumber()!=null && !billingDocEntity.getGstinNumber().equals(""))
		inventity.setGstinNumber(billingDocEntity.getGstinNumber());
		
	/**
	 *  nidhi
	 *  Date : 4-12-2017
	 *  For copy configration details to bill
	 */
	if(confiFlag){
		inventity.setInvoiceCategory(billingDocEntity.getBillingCategory());
		inventity.setInvoiceConfigType(billingDocEntity.getBillingType());
		inventity.setInvoiceGroup(billingDocEntity.getBillingType());
	}
	/**
	 *  end
	 */
	
	/** date 06-02-2018 added by komal for consolidate price**/
	inventity.setConsolidatePrice(model.isConsolidatePrice());
	/**
	 * end komal
	 */
	
	
	/**
	 * @author Anil @since 24-05-2021
	 * Mapping contract start date and end date as billing from date and to date
	 * Raised by Rahul Tiwari for Surat Pest Control
	 */
	if(billingDocEntity.getBillingPeroidFromDate()!=null){
		inventity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", billingDocEntity.getBillingPeroidFromDate()));
	}
	if(billingDocEntity.getBillingPeroidToDate()!=null){
		inventity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST", billingDocEntity.getBillingPeroidToDate()));
	}
	
//	GenricServiceImpl impl = new GenricServiceImpl();
//	impl.save(inventity);
	
	SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
	int invoiceId=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, inventity.getCompanyId(), 1);
	inventity.setCount(invoiceId);
	logger.log(Level.SEVERE,"invoiceId "+invoiceId);

	GenricServiceImpl impl = new GenricServiceImpl();
	impl.save(inventity);
	
	/**
	  *  nidhi
	  *  Date : 4-12-2017
	  *  Check process configration for 
	  */
	if(confiFlag){
		boolean updateFlag = false;
		updateFlag = ServerAppUtility.checkContractCategoryAvailableOrNot(billingDocEntity.getBillingCategory(), billingDocEntity.getCompanyId(), billingDocEntity.getApproverName(), 24);
		updateFlag = ServerAppUtility.checkContractTypeAvailableOrNot(billingDocEntity.getBillingType(), billingDocEntity.getBillingCategory(), billingDocEntity.getCompanyId(), billingDocEntity.getApproverName(), 24);
		updateFlag = ServerAppUtility.checkServiceTypeAvailableOrNot(billingDocEntity.getBillingGroup(), billingDocEntity.getCompanyId(), billingDocEntity.getApproverName(), 77);
	}
	/**
	 * end
	 */
	
	System.out.println("Here Invoice created Successfully");
	
	System.out.println("Invoice idd===="+inventity.getCount());
	
	billingDocEntity.setStatus(BillingDocument.BILLINGINVOICED);
	billingDocEntity.setInvoiceCount(inventity.getCount());
	
	GenricServiceImpl genimpl = new GenricServiceImpl();
	genimpl.save(billingDocEntity);
	
	System.out.println("Here billing status updated");

	
	/**
	 * Date 03-04-2017
	 * added by vijay
	 * for Invoice Accounting Inteface
	 */
	ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", inventity.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
	ProcessConfiguration processConfig=null;
	if(processName!=null){
		processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGINV).filter("companyId", inventity.getCompanyId()).filter("configStatus", true).first().now();
	}
	int flag =0;
	
	if(processConfig!=null){
		for(int i = 0;i<processConfig.getProcessList().size();i++){
			if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
				flag = 1;
			}
		}
	}
	logger.log(Level.SEVERE,"Invoice before Call AI");
	if(flag == 1&&processConfig!=null){
		logger.log(Level.SEVERE,"Invoice before Call AI 1");
		InvoiceAccountIngInterface(inventity);
	}
	
	/**
	 * Ends here
	 */
	
	
//	createpayementdocument(inventity,paymentflag);
	
	/*
	 * above old code commented by vijay and old code added in else block and new code added in if condition for partial payment two payment document will create
	 */ 
	
	
	if(partialquickcontractcontractoneinvoice){
		for(int k =0;k<2;k++){
			if(k==0){
				paymentflag =true;
			}else{
				paymentflag = false;
			}
			createpayementdocument(model,inventity,paymentflag,partialquickcontractcontractoneinvoice);
		}
		
	}else{
		createpayementdocument(model,inventity,paymentflag,partialquickcontractcontractoneinvoice);
	}
	
}

/****************************** invoice methods end here *****************************/



/**************************** payment docs start here *************************/

private void createpayementdocument(Contract model,Invoice inventity,boolean paymentflag,boolean partialquickcontractcontractoneinvoice) {

	System.out.println("Here started for payment creation method");
	
	if(paymentflag){
		
		System.out.println("This is for 100 % payment recieved so payment doc closed ");
		
		
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(inventity.getPersonInfo());
		paydetails.setContractCount(inventity.getContractCount());
		if(inventity.getContractStartDate()!=null){
			paydetails.setContractStartDate(inventity.getContractStartDate());
		}
		if(inventity.getContractEndDate()!=null){
			paydetails.setContractEndDate(inventity.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
		paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
		paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
		paydetails.setInvoiceCount(inventity.getCount());
		paydetails.setInvoiceDate(inventity.getInvoiceDate());
		paydetails.setInvoiceType(inventity.getInvoiceType());
		paydetails.setPaymentDate(inventity.getPaymentDate());
		
		/**
		 *  this if condtion executed when quickcontract is partial payment and process config for one invoce is active
		 *  then  payement document create on the payment recieved amount and old code excecuted in else block
		 */
		 
		int payAmt;
		if(partialquickcontractcontractoneinvoice){
			payAmt = (int) paymentrecievedAmount;
			paydetails.setPaymentAmt(payAmt);
			
		}else{
			double invoiceamt=inventity.getInvoiceAmount();
			payAmt=(int)(invoiceamt);
			paydetails.setPaymentAmt(payAmt);
		}
		
		paydetails.setPaymentMethod(inventity.getPaymentMethod());
		paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
		paydetails.setStatus("Closed");
		paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
		paydetails.setBranch(inventity.getBranch());
		paydetails.setAccountType(inventity.getAccountType());
		paydetails.setEmployee(inventity.getEmployee());
		paydetails.setCompanyId(inventity.getCompanyId());
		paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
		paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
		
		/***************** vijay*********************/
		if(inventity.getNumberRange()!=null)
		paydetails.setNumberRange(inventity.getNumberRange());
		/*******************************************/
		paydetails.setPaymentReceived(payAmt);
		
		
		/** Date 28-09-2018 added by Vijay for balance tax and revenue **/
		double taxAmount = 0;
		taxAmount = Math.round(inventity.getTotalAmtIncludingTax() - (inventity.getFinalTotalAmt() + inventity.getTotalOtherCharges()));
		paydetails.setBalanceTax(taxAmount);
		paydetails.setBalanceRevenue(inventity.getNetPayable() - taxAmount);
		
		paydetails.setReceivedRevenue(inventity.getNetPayable() - taxAmount);
		paydetails.setReceivedTax(taxAmount);
		/**
		 * ends here
		 */
		if(model.getBankAccNo()!=null)
			paydetails.setBankAccNo(model.getBankAccNo());
			logger.log(Level.SEVERE, "bank acc no11 "+model.getBankAccNo());
			if(model.getBankName()!=null)
			paydetails.setBankName(model.getBankName());
			logger.log(Level.SEVERE, "bank name11 "+model.getBankName());
			
			 if(model.getBankBranch()!=null)
			paydetails.setBankBranch(model.getBankBranch());
			 if(model.getChequeNo()!=null)
			paydetails.setChequeNo(model.getChequeNo());
			 
			 if(model.getChequeDate()!=null)
			paydetails.setChequeDate(model.getChequeDate());
			 
			 if(model.getAmountTransferDate()!=null)
			paydetails.setAmountTransferDate(model.getAmountTransferDate());
		   
			 if(model.getReferenceNo()!=null)
				 paydetails.setReferenceNo(model.getReferenceNo());
			 
			 if(model.getChequeIssuedBy()!=null)
				 paydetails.setChequeIssuedBy(model.getChequeIssuedBy()); 
		

			 if(model.getPaymentRecieved()!=0)
				 paydetails.setNetPay(model.getPaymentRecieved());
			 
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(paydetails);
		
		System.out.println("Payment created successfully");
		
		
		
		/**
		 * Date 03-04-2017
		 * added by vijay
		 * for closed payment documnet create accounting interface
		 */
		
		
//		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", paydetails.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
//		ProcessConfiguration processConfig=null;
//		if(processName!=null){
//			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGCP).filter("companyId", paydetails.getCompanyId()).filter("configStatus", true).first().now();
//		}
//		int flag =0;
//		if(processConfig!=null){
//			for(int i = 0;i<processConfig.getProcessList().size();i++){
//				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
//					flag = 1;
//				}
//			}
//		}
//		if(flag == 1&&processConfig!=null){
//			System.out.println("ExecutingAccInterface");
//			accountingInterface(paydetails);
//		}
		
		/**
		 * Ends here
		 */
		
		
	}else{
		
		System.out.println("This is for balance remaining so payment doc created not closed");
		
		
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(inventity.getPersonInfo());
		paydetails.setContractCount(inventity.getContractCount());
		if(inventity.getContractStartDate()!=null){
			paydetails.setContractStartDate(inventity.getContractStartDate());
		}
		if(inventity.getContractEndDate()!=null){
			paydetails.setContractEndDate(inventity.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
		paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
		paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
		paydetails.setInvoiceCount(inventity.getCount());
		paydetails.setInvoiceDate(inventity.getInvoiceDate());
		paydetails.setInvoiceType(inventity.getInvoiceType());
		paydetails.setPaymentDate(inventity.getPaymentDate());
		
		/**
		 *  this if condtion executed when quickcontract is partial payment and process config for one invoce is active
		 *  then  payement document create on the Balance payment amount and old code excecuted in else block
		 */
		 
		int payAmt;
		if(partialquickcontractcontractoneinvoice){
			payAmt = (int) balancePayment;
			paydetails.setPaymentAmt(payAmt);
			
		}else{
			double invoiceamt=inventity.getInvoiceAmount();
			payAmt=(int)(invoiceamt);
			paydetails.setPaymentAmt(payAmt);
		}
		
		paydetails.setPaymentMethod(inventity.getPaymentMethod());
		paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
		paydetails.setStatus(CustomerPayment.CREATED);
		paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
		paydetails.setBranch(inventity.getBranch());
		paydetails.setAccountType(inventity.getAccountType());
		paydetails.setEmployee(inventity.getEmployee());
		paydetails.setCompanyId(inventity.getCompanyId());
		paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
		paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
		
		/***************** vijay*********************/
		if(inventity.getNumberRange()!=null)
		paydetails.setNumberRange(inventity.getNumberRange());
		/*******************************************/
		
		/** Date 28-09-2018 added by Vijay for balance tax and revenue **/
		double taxAmount = 0;
		taxAmount = Math.round(inventity.getTotalAmtIncludingTax() - (inventity.getFinalTotalAmt() + inventity.getTotalOtherCharges()));
		paydetails.setBalanceTax(taxAmount);
		paydetails.setBalanceRevenue(inventity.getNetPayable() - taxAmount);
		/**
		 * ends here
		 */
		
		if(model.getBankAccNo()!=null)
		paydetails.setBankAccNo(model.getBankAccNo());
		logger.log(Level.SEVERE, "bank acc no "+model.getBankAccNo());
		if(model.getBankName()!=null)
		paydetails.setBankName(model.getBankName());
		logger.log(Level.SEVERE, "bank name "+model.getBankName());
		
		 if(model.getBankBranch()!=null)
		paydetails.setBankBranch(model.getBankBranch());
		 if(model.getChequeNo()!=null)
		paydetails.setChequeNo(model.getChequeNo());
		 
		 if(model.getChequeDate()!=null)
		paydetails.setChequeDate(model.getChequeDate());
		 
		 if(model.getAmountTransferDate()!=null)
		paydetails.setAmountTransferDate(model.getAmountTransferDate());
		 
		 if(model.getReferenceNo()!=null)
			 paydetails.setReferenceNo(model.getReferenceNo());
		 
		 if(model.getChequeIssuedBy()!=null)
			 paydetails.setChequeIssuedBy(model.getChequeIssuedBy()); 
	
		 if(model.getPaymentRecieved()!=0)
			 paydetails.setNetPay(model.getPaymentRecieved());
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(paydetails);
		
		
		System.out.println("Payment created successfully");
		
	}

}

  /**************************** Payment Section ends here ******************************************/



	/**
     * Date 03-04-2017
     * added by vijay
     * for quick contract Accounting interface
     */

	private void quickContractaccountingInterface(int contractId, long companyId, String status) {
		
	Contract contract = ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", contractId).filter("status", status).first().now();
		
		if(contract!=null){
			for(int  i = 0;i<contract.getItems().size();i++){
				
				Customer cust = ofy().load().type(Customer.class).filter("companyId",contract.getCompanyId()).filter("count",contract.getCinfo().getCount()).first().now();
				
				
				int prodId=contract.getItems().get(i).getPrduct().getCount();
				String productCode = contract.getItems().get(i).getProductCode();
				String productName = contract.getItems().get(i).getProductName();
				double productQuantity = contract.getItems().get(i).getQty();
				
				double taxamount = removeTaxAmt(contract.getItems().get(i).getPrduct());
				double productprice = (contract.getItems().get(i).getPrice()-taxamount);
				double totalAmount = (contract.getItems().get(i).getPrice()-taxamount)*contract.getItems().get(i).getQty();

				 double serviceTax=0 ;
				 double vatPercent=0;
				 double calculatedamt = 0;
				 double calculetedservice = 0;
				 int vatglaccno=0;
				 int serglaccno=0;
				 
				 
				 
				 if(contract.getItems().get(i).getVatTax().getPercentage()!=0)
				 {
					 vatPercent=contract.getItems().get(i).getVatTax().getPercentage();
					 calculatedamt=contract.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
				 }
				 
				 if(contract.getItems().get(i).getServiceTax().getPercentage()!=0)
				 {
					 serviceTax=contract.getItems().get(i).getServiceTax().getPercentage();
					 calculetedservice=(contract.getItems().get(i).getServiceTax().getPercentage()*(totalAmount+calculatedamt))/100;
				 }
				 
				
				 
				 
				 if(contract.getItems().get(i).getVatTax().getPercentage()!=0)
				 {
					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",contract.getCompanyId()).filter("taxChargePercent",contract.getItems().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
					 GLAccount vatglAcc =null;
					 if(vattaxDtls!=null){
						 vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",contract.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
					 }
					 if(vatglAcc!=null){
						 vatglaccno=vatglAcc.getGlAccountNo();
					 }
				 }
				 
				 if(contract.getItems().get(i).getServiceTax().getPercentage()!=0)
				 {
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",contract.getCompanyId()).filter("taxChargePercent",contract.getItems().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
					 GLAccount serglAcc=null;
					 if(sertaxDtls!=null){
						 serglAcc = ofy().load().type(GLAccount.class).filter("companyId",contract.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now(); 
					 }
					 if(serglAcc!=null){
						 serglaccno=serglAcc.getGlAccountNo();
					 }
					
				 }
				 double netPayable =contract.getNetpayable();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
				 
				 for(int k = 0; k < contract.getProductCharges().size();k++){
					 oc[k] = contract.getProductCharges().get(k).getChargeName();
					 oca[k] = contract.getProductCharges().get(k).getChargePayable();
				 }
				 String numberRange="";
				 if(contract.getNumberRange()!=null){
					 numberRange = contract.getNumberRange();
				 }
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
													 contract.getEmployee(),//accountingInterfaceCreatedBy
													 contract.getStatus(),
						 							AppConstants.STATUS ,//Status
						 							"",//Remark
						 							"Services",//Module
						 							"Contract",//documentType
						 							contract.getCount(),//documentID
													" ",//documentTitle
													contract.getCreationDate(),//documentDate
													"Contract",//doucmentGL
													contract.getRefNo(),//referenceDocumentNumber1
													contract.getRefDate(),//referenceDocumentDate1
													AppConstants.REFERENCEDOCUMENTTYPE,//referenceDocumentType1
													"",//referenceDocumentNumber2
													null,//referenceDocumentDate2
													"",//referenceDocumentType2
													AppConstants.ACCOUNTTYPE,//accountType
													contract.getCinfo().getCount(),//customerID
													contract.getCinfo().getFullName(),//customerName
													contract.getCinfo().getCellNumber(),//customerCell
													0,//VendorId
													"",//VendorName
													0,//VendorCell
													0,//employeeID
													"",//employeeName
													contract.getBranch(),//Branch
													contract.getEmployee(),//personResponsible
													"",//requestedBy
													contract.getApproverName(),//approvedBy
													contract.getPaymentMethod(),//paymentMethod
													null,//paymentDate
													"",//chequeNumber
													null,//chequeDate
													null,//BankName
													null,//bankAccount
													0,//transferReferenceNumber
													null,//transactionDate
													contract.getStartDate(),// contract start date
													contract.getEndDate(), // contract end date
													prodId,//productID
													productCode,//productCode
													productName,//productName
													productQuantity,//Quantity
													contract.getItems().get(i).getStartDate(),//productDate
													contract.getItems().get(i).getDuration(),//duration
													contract.getItems().get(i).getNumberOfServices(),//services
													null,//unitofmeasurement
													productprice,//productPrice
													vatPercent,//VATpercent
													calculatedamt,// VATamount
													vatglaccno,//VATglAccount
													serviceTax,//serviceTaxPercent
													calculetedservice,//servicetaxamount
													serglaccno,//servicetaxglaccno
													null,//cform
													0,//cformpercent
													0,//cformAmount
													0,//cformglaccno
													totalAmount,//totalamount
													netPayable,//netpayable
													contract.getCategory(),//This is added by RV for NBHC to distinguish between Tax Applicable and not Applicale in Contract
													0,			//amountRecieved
													0.0,         // base Amt in payment for tds calc by rohan 
													0,   //  tds percentage by rohan 
													0,   //  tds amount by rohan 
													oc[0],
													oca[0],
													oc[1],
													oca[1],
													oc[2],
													oca[2],
													oc[3],
													oca[3],
													oc[4],
													oca[4],
													oc[5],
													oca[5],
													oc[6],
													oca[6],
													oc[7],
													oca[7],
													oc[8],
													oca[8],
													oc[9],
													oca[9],
													oc[10],
													oca[10],
													oc[11],
													oca[11],
													cust.getSecondaryAdress().getAddrLine1(),
													cust.getSecondaryAdress().getLocality(),
													cust.getSecondaryAdress().getLandmark(),
													cust.getSecondaryAdress().getCountry(),
													cust.getSecondaryAdress().getState(),
													cust.getSecondaryAdress().getCity(),
													cust.getSecondaryAdress().getPin(),
													contract.getCompanyId(),
													null,				//  billing from date (rohan)
													null,			//  billing to date (rohan)
													"", //Warehouse
													"",				//warehouseCode
													"",				//ProductRefId
													"",				//Direction
													"",				//sourceSystem
													"",				//Destination System
													null,
													null,
													numberRange
						 );
	    }
		}
	}

	public double removeTaxAmt(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			double removeServiceTax=(entity.getPrice()/(1+service/100));
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
//			double taxPerc=service+vat;
//			retrServ=Math.round(entity.getPrice()/(1+taxPerc/100));
//			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	/**
	 * Ends here
	 */
	
	
	/**
	 * Date 03-04-2017
	 * added by vijay
	 * for Quick Contract Invoice Accounting Interface
	 */
	
	
//	private void InvoiceAccountIngInterface(Invoice inventity) {
//		// TODO Auto-generated method stub
//			for(int  i = 0;i<inventity.getSalesOrderProducts().size();i++){
//				
//				Customer cust=null;
//				Vendor vendor=null;
//				Employee emp=null;
//				
//				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
//					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//				}
//				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
//					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//				}				
//				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//					vendor=ofy().load().type(Vendor.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//				}
//				
//				
//				Date docCreationDate=null;
//				 int prodId=inventity.getSalesOrderProducts().get(i).getProdId();
//				 String productCode = inventity.getSalesOrderProducts().get(i).getProdCode();
//				 String productName = inventity.getSalesOrderProducts().get(i).getProdName();
//				 double productQuantity = inventity.getSalesOrderProducts().get(i).getQuantity();
//					 double productprice = (inventity.getSalesOrderProducts().get(i).getPrice());
//				 String unitofmeasurement = inventity.getSalesOrderProducts().get(i).getUnitOfMeasurement();
//				 //  rohan commented this code 
////				 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
//				
//				 //   rohan added new code for calculations 
//				 double totalAmount = inventity.getSalesOrderProducts().get(i).getBaseBillingAmount();
//				 
//				 double calculatedamt = 0;
//				 double calculetedservice =0;
//				 	
//				 int orderDuration = inventity.getSalesOrderProducts().get(i).getOrderDuration();
//				 int orderServices = inventity.getSalesOrderProducts().get(i).getOrderServices();
//				 
//				 double cformamtService = 0;
//				 double serviceTax=0 ;
//				 double cformPercent=0;
//				 double cformAmount=0;					
//				 double vatPercent=0;
//				 String cform = "";
//				 String refDocType="";
//				 double cformP=0;
//				 String accountType="";
//				 Date contractStartDate=null;
//				 Date contractEndDate=null;
//				 
//				 int vatglaccno=0;
//				 int serglaccno=0;
//				 int cformglaccno=0;
//				 
//				 int personInfoId = 0;
//				 String personInfoName="";
//				 long personInfoCell = 0;
//				 int vendorInfoId = 0;
//				 String vendorInfoName="";
//				 long vendorInfoCell=0;
//				 int empInfoId;
//				 String empInfoName="";
//				 long empinfoCell;
//				 
//				 String addrLine1="";
//				 String addrLocality="";
//				 String addrLandmark="";
//				 String addrCountry="";
//				 String addrState="";
//				 String addrCity="";
//				 long addrPin=0;
//	/************************************for document type********************************************/			 
//				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
//					docCreationDate=inventity.getOrderCreationDate();
//					refDocType=AppConstants.REFDOCSO;
//					personInfoId=inventity.getPersonInfo().getCount();
//					personInfoName=inventity.getPersonInfo().getFullName();
//					personInfoCell=inventity.getPersonInfo().getCellNumber();
//					
//					 addrLine1=cust.getAdress().getAddrLine1();
//					 addrLocality=cust.getAdress().getLocality();
//					 addrLandmark=cust.getAdress().getLandmark();
//					 addrCountry=cust.getAdress().getCountry();
//					 addrState=cust.getAdress().getState();
//					 addrCity=cust.getAdress().getCity();
//					 addrPin=cust.getAdress().getPin();
//					
//					if(inventity.getOrderCformStatus()!=null){
//						cform=inventity.getOrderCformStatus();
//						cformP=inventity.getOrderCformPercent();
//					}
//				}
//				
//				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
//					docCreationDate=inventity.getOrderCreationDate();
//					refDocType=AppConstants.REFDOCCONTRACT;
//					personInfoId=inventity.getPersonInfo().getCount();
//					personInfoName=inventity.getPersonInfo().getFullName();
//					personInfoCell=inventity.getPersonInfo().getCellNumber();
//				
//					addrLine1=cust.getAdress().getAddrLine1();
//					addrLocality=cust.getAdress().getLocality();
//					addrLandmark=cust.getAdress().getLandmark();
//					addrCountry=cust.getAdress().getCountry();
//					addrState=cust.getAdress().getState();
//					addrCity=cust.getAdress().getCity();
//					addrPin=cust.getAdress().getPin();
//					contractStartDate=inventity.getContractStartDate();
//					contractEndDate=inventity.getContractEndDate();
//				}
//				
//				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//					docCreationDate=inventity.getOrderCreationDate();
//					refDocType=AppConstants.REFDOCPO;
//					
//					vendorInfoId=inventity.getPersonInfo().getCount();
//					vendorInfoName=inventity.getPersonInfo().getFullName();
//					vendorInfoCell=inventity.getCellNumber();
//					
//					addrLine1=vendor.getPrimaryAddress().getAddrLine1();
//					addrLocality=vendor.getPrimaryAddress().getLocality();
//					addrLandmark=vendor.getPrimaryAddress().getLandmark();
//					addrCountry=vendor.getPrimaryAddress().getCountry();
//					addrState=vendor.getPrimaryAddress().getState();
//					addrCity=vendor.getPrimaryAddress().getCity();
//					addrPin=vendor.getPrimaryAddress().getPin();
//					
//					if(inventity.getOrderCformStatus()!=null){
//						cform=inventity.getOrderCformStatus();
//						cformP=inventity.getOrderCformPercent();
//					}
//				}
//				
//			/*********************************for cform*************************************************/	
//				
//				 if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.YES)){
//					 cform=AppConstants.YES;
//					 cformPercent=inventity.getOrderCformPercent();
//					 cformAmount=inventity.getOrderCformPercent()*totalAmount/100;
//					 cformamtService=inventity.getOrderCformPercent()*totalAmount/100;
//					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//					 }
//					 
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//					 cformglaccno=serglAcc.getGlAccountNo();
//				 }
//				 
//				 else if(inventity.getOrderCformStatus()!= null&inventity.getOrderCformStatus().trim().equals(AppConstants.NO)){
//					 cform=AppConstants.NO;
//					 cformPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//					 cformAmount=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//					 cformamtService=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//					 
//					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//					 }
//					 
//					 
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
//				 }
//				 
//				 
//				 	else{//if the customer is of same state
//					 
//					 if(inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
//					 {
//						 vatPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//						 calculatedamt=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//						 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
//						 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//						 vatglaccno=vatglAcc.getGlAccountNo();
//					 }
//					 
//					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
//					 {
//						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//						 calculetedservice=(calculatedamt+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
//					 }
//					 }
//				 
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
//				 {
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
//					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//					 serglaccno=serglAcc.getGlAccountNo();
//				 }
//
//				 /*****************************************************************************************/
//				
//				 //  old code 
////				 double netPayable = getTotalSalesAmount();
//				 //  new code by rohan as there is calculations error
//				 double netPayable = inventity.getNetPayable();
//				 
//				 
//				 String[] oc = new String[12];
//				 double[] oca = new double[12];
//				 
//				 
////				 if(this.getBillingOtherCharges().size()!=0){
//					 for(int k = 0; k < inventity.getBillingOtherCharges().size();k++){
//						 oc[k] = inventity.getBillingOtherCharges().get(k).getTaxChargeName();
//						 oca[k] = inventity.getBillingOtherCharges().get(k).getPayableAmt();
//					 }
////				 }
//				 /**********************************End************************************************/
//				 
//				 logger.log(Level.SEVERE,"Before Invoice Accounting Interface Call");
//				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
//						 									inventity.getEmployee(),//accountingInterfaceCreatedBy
//						 									inventity.getStatus(),
//														AppConstants.STATUS ,//Status
//														"",//Remark
//														"Accounts",//Module
//														"Invoice",//documentType
//														inventity.getCount(),//documentID
//															" ", //documentTitle
//															inventity.getInvoiceDate(),//documentDate
//															inventity.getAccountType().trim()+"-Invoice",//documentGl
//															inventity.getContractCount()+"",//referenceDocumentNumber1
//															docCreationDate,//referenceDocumentDate1
//															refDocType,//referenceDocumentType
//															"",//referenceDoc.no.2
//															null,//reference doc.date.2
//															"",//reference doc type no2.
//															inventity.getAccountType(),//accountType	
//															personInfoId,//custID	
//															personInfoName,//custName	
//															personInfoCell,//custCell
//															vendorInfoId,//venID	
//															vendorInfoName,//venName	
//															vendorInfoCell,//venCell
//															0,//emplID
//															" ",//emplName
//															inventity.getBranch(),//branch
//															inventity.getEmployee(),//personResponsible
//															"",//requestdBy
//															inventity.getApproverName(),//AproverName
//															inventity.getPaymentMethod(),//PaymentMethod
//															inventity.getPaymentDate(),//PaymentDate
//															"",//Cheq no.
//															null,//cheq date.
//															null,//Bank Name
//															null,//bankAccount
//															0,//transferReferenceNumber
//															null,//transactionDate
//															contractStartDate,  //contract start date
//															contractEndDate,  // contract end date
//															prodId,//prodID
//															productCode,//prodCode
//															productName, //prodNAme
//															productQuantity,//prodQuant
//															inventity.getContractStartDate(),//Prod DAte
//															orderDuration,//duration	
//															orderServices,//services
//															unitofmeasurement,//UOM
//															productprice, //prod Price
//															vatPercent,//vat%
//															calculatedamt,//vatamt
//															vatglaccno,//VATglAccount
//															serviceTax,//serviceTaxPercent
//															calculetedservice,//serviceTaxAmount
//															serglaccno,//serviceTaxGLaccount
//															cform,//cform
//															cformPercent,//cform%
//															cformAmount,//cformamt
//															cformglaccno, //cformglacc
//															totalAmount,//totalamt
//															netPayable,//netpay
//															"",//Contract Category
//															0,			//amountRecieved
//															0.0,     // Base Amount taken in payment for tds calculation by rohan 
//															0,   //  tds percentage by rohan 
//															0,   //  tds amount by rohan 
//															oc[0],
//															oca[0],
//															oc[1],
//															oca[1],
//															oc[2],
//															oca[2],
//															oc[3],
//															oca[3],
//															oc[4],
//															oca[4],
//															oc[5],
//															oca[5],
//															oc[6],
//															oca[6],
//															oc[7],
//															oca[7],
//															oc[8],
//															oca[8],
//															oc[9],
//															oca[9],
//															oc[10],
//															oca[10],
//															oc[11],
//															oca[11],
//															addrLine1,
//															addrLocality,
//															addrLandmark,
//															addrCountry,
//															addrState,
//															addrCity,
//															addrPin,
//															inventity.getCompanyId(),
//															inventity.getBillingPeroidFromDate(),//  billing from date (rohan)
//															inventity.getBillingPeroidToDate(),//  billing to date (rohan)
//															"", //Warehouse
//															"",				//warehouseCode
//															"",				//ProductRefId
//															"",				//Direction
//															"",				//sourceSystem
//															""				//Destination Syste
//															);
//				 
//			
//			}
//			
//	}
	
	private void InvoiceAccountIngInterface(Invoice inventity) {
//		// TODO Auto-generated method stub

		/** date 23.6.2018 added by komal for new accountion interface changes**/
		if(inventity.getCount()!=0&&inventity.getStatus().equals(Invoice.APPROVED)){
	
			
			for(int  i = 0;i<inventity.getSalesOrderProducts().size();i++){
				
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					vendor=ofy().load().type(Vendor.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}
				
				
				Date docCreationDate=null;
				 int prodId=inventity.getSalesOrderProducts().get(i).getProdId();
				 String productCode = inventity.getSalesOrderProducts().get(i).getProdCode();
				 String productName = inventity.getSalesOrderProducts().get(i).getProdName();
				 double productQuantity = inventity.getSalesOrderProducts().get(i).getQuantity();
 				 double productprice = (inventity.getSalesOrderProducts().get(i).getPrice());
				 String unitofmeasurement = inventity.getSalesOrderProducts().get(i).getUnitOfMeasurement();
				 //  rohan commented this code 
//				 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
				
				 //   rohan added new code for calculations 
				 double totalAmount = inventity.getSalesOrderProducts().get(i).getBaseBillingAmount();
				 
				 double calculatedamt = 0;
				 double calculetedservice =0;
				 	
				 int orderDuration = inventity.getSalesOrderProducts().get(i).getOrderDuration();
				 int orderServices = inventity.getSalesOrderProducts().get(i).getOrderServices();
				 
				 double cformamtService = 0;
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;					
				 double vatPercent=0;
				 String cform = "";
				 String refDocType="";
				 double cformP=0;
				 String accountType="";
				 Date contractStartDate=null;
				 Date contractEndDate=null;
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
				 int personInfoId = 0;
				 String personInfoName="";
				 long personInfoCell = 0;
				 int vendorInfoId = 0;
				 String vendorInfoName="";
				 long vendorInfoCell=0;
				 int empInfoId;
				 String empInfoName="";
				 long empinfoCell;
				 
				 String addrLine1="";
				 String addrLocality="";
				 String addrLandmark="";
				 String addrCountry="";
				 String addrState="";
				 String addrCity="";
				 long addrPin=0;
				 
				 /** date 29/3/2018 added by komal for tax name **/
				 String taxvatName="";
				 String serTaxName = "";
				 String glAccountName1 = "";
				 String glAccountName2 = ""; 
				 /** date 29.5.2018 added by komal for billing address **/
				 String saddrLine1 ="";
				 String addrLine2 ="";
				 String saddrLine2 ="";
				 String saddrLocality = "";
				 String saddrLandmark = "";
				 String saddrCountry = "";
				 String saddrState="";
				 String saddrCity="";
				 long saddrPin = 0;
	/************************************for document type********************************************/			 
				 
				 
				 
				 
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCSO;
					personInfoId=inventity.getPersonInfo().getCount();
					personInfoName=inventity.getPersonInfo().getFullName();
					personInfoCell=inventity.getPersonInfo().getCellNumber();
					
					 addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
					 
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
					
					if(inventity.getOrderCformStatus()!=null){
						cform=inventity.getOrderCformStatus();
						cformP=inventity.getOrderCformPercent();
					}
				}
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCCONTRACT;
					personInfoId=inventity.getPersonInfo().getCount();
					personInfoName=inventity.getPersonInfo().getFullName();
					personInfoCell=inventity.getPersonInfo().getCellNumber();
				
					addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					addrLocality=cust.getAdress().getLocality();
					addrLandmark=cust.getAdress().getLandmark();
					addrCountry=cust.getAdress().getCountry();
					addrState=cust.getAdress().getState();
					addrCity=cust.getAdress().getCity();
					addrPin=cust.getAdress().getPin();
					contractStartDate=inventity.getContractStartDate();
					contractEndDate=inventity.getContractEndDate();
					
					 addrPin=cust.getAdress().getPin();
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
				}
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCPO;
					
					vendorInfoId=inventity.getPersonInfo().getCount();
					vendorInfoName=inventity.getPersonInfo().getFullName();
					vendorInfoCell=inventity.getCellNumber();
					
					addrLine1=vendor.getPrimaryAddress().getAddrLine1();
					/** date 29.5.2018 added by komal for billing address **/
					addrLine2=vendor.getPrimaryAddress().getAddrLine2();
					addrLocality=vendor.getPrimaryAddress().getLocality();
					addrLandmark=vendor.getPrimaryAddress().getLandmark();
					addrCountry=vendor.getPrimaryAddress().getCountry();
					addrState=vendor.getPrimaryAddress().getState();
					addrCity=vendor.getPrimaryAddress().getCity();
					addrPin=vendor.getPrimaryAddress().getPin();
					
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=vendor.getSecondaryAddress().getAddrLine1();
					saddrLine2=vendor.getSecondaryAddress().getAddrLine2();
					saddrLocality=vendor.getSecondaryAddress().getLocality();
					saddrLandmark=vendor.getSecondaryAddress().getLandmark();
					saddrCountry=vendor.getSecondaryAddress().getCountry();
					saddrState=vendor.getSecondaryAddress().getState();
					saddrCity=vendor.getSecondaryAddress().getCity();
					saddrPin=vendor.getSecondaryAddress().getPin();
					
					if(inventity.getOrderCformStatus()!=null){
						cform=inventity.getOrderCformStatus();
						cformP=inventity.getOrderCformPercent();
					}
				}
				
				
				
			/*********************************for cform*************************************************/	
				
				 if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=inventity.getOrderCformPercent();
					 cformAmount=inventity.getOrderCformPercent()*totalAmount/100;
					 cformamtService=inventity.getOrderCformPercent()*totalAmount/100;
					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//					 cformglaccno=serglAcc.getGlAccountNo();
					 cformglaccno=0;
				 }
				 
				 else if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
					 cformAmount=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 cformamtService=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 
					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
					 
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
					 vatglaccno=0;
				 }
				 
				 
				 
				 	else{//if the customer is of same state
					 
						 if(inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
						 {
							 vatPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
							 calculatedamt=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
							 //date 29.3.2018 added by komal
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargeName",inventity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName()).first().now();
							 if(vattaxDtls != null)	
							 glAccountName2 = vattaxDtls.getGlAccountName();
//							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//							 vatglaccno=vatglAcc.getGlAccountNo();
							 vatglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							// String taxvatName="";
									if(inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName()!=null && !inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName().equals("")){
										taxvatName =inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName();
									}
									else{
										taxvatName =inventity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName();
									}
						 }
						 
						 
						
						 				 
						 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
						 {
							 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
							 calculetedservice=(totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
							//date 29.3.2018 added by komal 
							 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargeName",inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName()).first().now();
							 if(sertaxDtls != null)
							 glAccountName1 = sertaxDtls.getGlAccountName();
//							 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//							 serglaccno=serglAcc.getGlAccountNo();
							 serglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							 /** date 03/04/2018 changed by komal  vat tax to service tax**/
							 //String serTaxName="";
								if(inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName()!=null && !inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName().equals("")){
									serTaxName =inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName();
								}
								else{
									serTaxName =inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName();
								}
						 }
				 	}
				 
				 
				 
				

				 /*****************************************************************************************/
				
				 //  old code 
//				 double netPayable = getTotalSalesAmount();
				 //  new code by rohan as there is calculations error
				 double netPayable = inventity.getNetPayable();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
//				 if(this.getBillingOtherCharges().size()!=0){
					 for(int k = 0; k < inventity.getBillingOtherCharges().size();k++){
						 oc[k] = inventity.getBillingOtherCharges().get(k).getTaxChargeName();
						 oca[k] = inventity.getBillingOtherCharges().get(k).getPayableAmt();
					 }
//				 }
					 
				/**
				 * Date : 01-11-2017 BY ANIL
				 * Setting invoice comment to accounting interface remark	 
				 */
				String invoiceComment="";
				if(inventity.getComment()!=null){
					invoiceComment=inventity.getComment();
				}
				Logger logger = Logger.getLogger("Name of logger");
				logger.log(Level.SEVERE,"Invoice Group"+inventity.getInvoiceGroup());
				/**
				 * End
				 */
				/** date 29/03/2018 added by  komal for new tally interface requirement**/
				String gstn = "";
				double discountAmount = inventity.getDiscountAmt();
				double indirectExpenses = inventity.getTotalOtherCharges();
				if(inventity.getGstinNumber()!=null){
					gstn = inventity.getGstinNumber();
				}else{
					if(inventity.getAccountType().equals("AR")){
						System.out.println("id ");
						try{
							Customer customer=ofy().load().type(Customer.class).filter("companyId", inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
							if(customer!=null){
								System.out.println("id222222222222222 ");
								if(customer.getArticleTypeDetails()!=null){  
									System.out.println("id 33333333333333333");
									for(ArticleType object:customer.getArticleTypeDetails()){
										System.out.println("id 4444444444444444");  
										if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
											System.out.println("id 555555555555555555555");
											inventity.setGstinNumber(object.getArticleTypeValue());
											break;
										}
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					gstn = inventity.getGstinNumber();
				}
		       String hsnNumber = "";
		       SuperProduct superProduct = ofy().load().type(SuperProduct.class).filter("companyId",inventity.getCompanyId()).filter("productCode", inventity.getSalesOrderProducts().get(i).getProdCode()).first().now();
		       if(superProduct.getHsnNumber()!=null){
		    	   hsnNumber = superProduct.getHsnNumber();
		       }
		     
		    	 personInfoName=inventity.getPersonInfo().getFullName(); 
		    	
		    	 /**
		    	  * end komal
		    	  */
					 
				 /**********************************End************************************************/
		    	 String numberRange="";
				 if(inventity.getNumberRange()!=null){
					 numberRange = inventity.getNumberRange();
				 }
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
						 								inventity.getEmployee(),//accountingInterfaceCreatedBy
						 								inventity.getStatus(),
														AppConstants.STATUS ,//Status
														invoiceComment,//Remark
														"Accounts",//Module
														"Invoice",//documentType
														inventity.getCount(),//documentID
															" ", //documentTitle
															inventity.getInvoiceDate(),//documentDate
															inventity.getAccountType().trim()+"-Invoice",//documentGl
															inventity.getContractCount()+"",//referenceDocumentNumber1
															docCreationDate,//referenceDocumentDate1
															refDocType,//referenceDocumentType
															inventity.getRefNumber(),//referenceDoc.no.2 Date 09-11-2017 this is sap customer ref num for NBHC
															null,//reference doc.date.2
															"",//reference doc type no2.
															inventity.getAccountType(),//accountType	
															personInfoId,//custID	
															personInfoName,//custName	
															personInfoCell,//custCell
															vendorInfoId,//venID	
															vendorInfoName,//venName	
															vendorInfoCell,//venCell
															0,//emplID
															" ",//emplName
															inventity.getBranch(),//branch
															inventity.getEmployee(),//personResponsible
															"",//requestdBy
															inventity.getApproverName(),//AproverName
															inventity.getPaymentMethod(),//PaymentMethod
															inventity.getPaymentDate(),//PaymentDate
															"",//Cheq no.
															null,//cheq date.
															null,//Bank Name
															null,//bankAccount
															0,//transferReferenceNumber
															null,//transactionDate
															contractStartDate,  //contract start date
															contractEndDate,  // contract end date
															prodId,//prodID
															productCode,//prodCode
															productName, //prodNAme
															productQuantity,//prodQuant
															inventity.getContractStartDate(),//Prod DAte
															orderDuration,//duration	
															orderServices,//services
															unitofmeasurement,//UOM
															productprice, //prod Price
															vatPercent,//vat%
															calculatedamt,//vatamt
															vatglaccno,//VATglAccount
															serviceTax,//serviceTaxPercent
															calculetedservice,//serviceTaxAmount
															serglaccno,//serviceTaxGLaccount
															cform,//cform
															cformPercent,//cform%
															cformAmount,//cformamt
															cformglaccno, //cformglacc
															totalAmount,//totalamt
															netPayable,//netpay
															inventity.getInvoiceGroup(),//Contract Category//27 fEB PReviously it was contract category now it is from invoice group
															0,			//amountRecieved
															0.0,     // Base Amount taken in payment for tds calculation by rohan 
															0,   //  tds percentage by rohan 
															0,   //  tds amount by rohan 
															oc[0],
															oca[0],
															oc[1],
															oca[1],
															oc[2],
															oca[2],
															oc[3],
															oca[3],
															oc[4],
															oca[4],
															oc[5],
															oca[5],
															oc[6],
															oca[6],
															oc[7],
															oca[7],
															oc[8],
															oca[8],
															oc[9],
															oca[9],
															oc[10],
															oca[10],
															oc[11],
															oca[11],
															addrLine1,
															addrLine2,
															addrLocality,
															addrLandmark,
															addrCountry,
															addrState,
															addrCity,
															addrPin,
															inventity.getCompanyId(),
															inventity.getBillingPeroidFromDate(),//  billing from date (rohan)
															inventity.getBillingPeroidToDate(),//  billing to date (rohan)
															"", //Warehouse
															"",				//warehouseCode
															"",				//ProductRefId
															"",				//Direction
															"",				//sourceSystem
															""	,			//Destination Syste
															hsnNumber, // hsc/Sac
															gstn, // gstn
															glAccountName1, // goupname1 for tax1
															glAccountName2, // groupname2 for tax 2
															serTaxName, // tax1 name
															taxvatName, // tax2 name
															discountAmount, // discount amount
															indirectExpenses, // indirect expenses
															saddrLine1,
															saddrLine2, 
															saddrLocality, 
															saddrLandmark,
															saddrCountry,
															saddrState,
															saddrCity, 
															saddrPin,
															null,null,null,null,
															null,null,null,null,null,null, numberRange
															);
				 
			
			}
			
		}
	
		
}
	
/**
 * ends here	
 */

	/**
	 * Ends here
	 */
	
	
	/**
	 * Date 03-04-2017
	 * added by vijay
	 * for Payment documnet Accounting interface
	 */
	   private void accountingInterface(CustomerPayment paydetails) {
			
		   Invoice so=ofy().load().type(Invoice.class).filter("companyId",paydetails.getCompanyId()).filter("count", paydetails.getInvoiceCount()).first().now();
			for(int  i = 0;i<so.getSalesOrderProducts().size();i++){
				
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(paydetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					cust=ofy().load().type(Customer.class).filter("companyId",paydetails.getCompanyId()).filter("count", paydetails.getPersonInfo().getCount()).first().now();
				}
				if(paydetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",paydetails.getCompanyId()).filter("count", paydetails.getPersonInfo().getCount()).first().now();
				}				
				if(paydetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					vendor=ofy().load().type(Vendor.class).filter("companyId",paydetails.getCompanyId()).filter("count", paydetails.getPersonInfo().getCount()).first().now();
				}
				
		/*************************************************Start************************************************/
				
				
				 int prodId=so.getSalesOrderProducts().get(i).getProdId();
				 String productCode = so.getSalesOrderProducts().get(i).getProdCode();
				 String productName = so.getSalesOrderProducts().get(i).getProdName();
				 double productQuantity = so.getSalesOrderProducts().get(i).getQuantity();
				 double totalAmount =  so.getSalesOrderProducts().get(i).getPrice()*so.getSalesOrderProducts().get(i).getQuantity();
					double calculatedamt = totalAmount*so.getSalesOrderProducts().get(i).getVatTax().getPercentage()/100;
					double productprice = (so.getSalesOrderProducts().get(i).getPrice());
				 double calculetedservice = 0;
				 
				 double cformamtService = 0;
				 
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;
				 double vatPercent=0;
				 double servicePercent=0;
				 String cform = "";
				 double cformP=0;
				 Date contractStartDate=null;
				 Date contractEndDate=null;
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
				 int personInfoId = 0;
				 String personInfoName="";
				 long personInfoCell = 0;
				 int vendorInfoId = 0;
				 String vendorInfoName="";
				 long vendorInfoCell=0;
				 int empInfoId;
				 String empInfoName="";
				 long empinfoCell;
				 
				 String addrLine1="";
				 String addrLocality="";
				 String addrLandmark="";
				 String addrCountry="";
				 String addrState="";
				 String addrCity="";
				 long addrPin=0;
				 
			/******************************************for cform*********************************************/
				 if(so.getOrderCformStatus()!= null&&so.getOrderCformStatus().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=so.getOrderCformPercent();
					 cformAmount=so.getOrderCformPercent()*totalAmount/100;
					 cformamtService=so.getOrderCformPercent()*totalAmount/100;
					 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=so.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*so.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",paydetails.getCompanyId()).filter("taxChargePercent",so.getOrderCformPercent()).filter("isCentralTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",paydetails.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 cformglaccno=serglAcc.getGlAccountNo();
					
				 }
				 
				 else if(so.getOrderCformStatus()!= null&&so.getOrderCformStatus().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=so.getSalesOrderProducts().get(i).getVatTax().getPercentage();
					 cformAmount=so.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 cformamtService=so.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 
					 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=so.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*so.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",paydetails.getCompanyId()).filter("taxChargeName",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",paydetails.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 cformglaccno=serglAcc.getGlAccountNo();
				 }
				 
				 else{//if the customer is of same state
					 
					 if(so.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
					 {
						 vatPercent=so.getSalesOrderProducts().get(i).getVatTax().getPercentage();
						 calculatedamt=so.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
						 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",paydetails.getCompanyId()).filter("taxChargePercent",so.getSalesOrderProducts().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
						 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",paydetails.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
						 vatglaccno=vatglAcc.getGlAccountNo();
					 }
					 
					 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
					 {
						 serviceTax=so.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=(calculatedamt+totalAmount)*so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
					 }
				 }
				 
				 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
				 {
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",paydetails.getCompanyId()).filter("taxChargePercent",so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",paydetails.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 serglaccno=serglAcc.getGlAccountNo();
				 }
				 double netPayable = so.getTotalSalesAmount();
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 for(int k = 0; k < so.getBillingOtherCharges().size();k++){
					 oc[k] = so.getBillingOtherCharges().get(k).getTaxChargeName();
					 oca[k] = so.getBillingOtherCharges().get(k).getPayableAmt();
				 }
				 
				 if(paydetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					 
					 personInfoId=paydetails.getPersonInfo().getCount();
					 personInfoName=paydetails.getPersonInfo().getFullName();
					 personInfoCell=paydetails.getPersonInfo().getCellNumber();
					 
					 addrLine1=cust.getAdress().getAddrLine1();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
				 }
				 

				 if(paydetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					 
					 personInfoId=paydetails.getPersonInfo().getCount();
					 personInfoName=paydetails.getPersonInfo().getFullName();
					 personInfoCell=paydetails.getPersonInfo().getCellNumber();
					 
					 addrLine1=cust.getAdress().getAddrLine1();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
					 if(paydetails.getContractStartDate()!=null){
						 contractStartDate=paydetails.getContractStartDate();
					 }
					 if(paydetails.getContractEndDate()!=null){
						 contractEndDate=paydetails.getContractEndDate();
					 }
				 }
				 
				 

				 if(paydetails.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					 
					 vendorInfoId=paydetails.getPersonInfo().getCount();
					 vendorInfoName=paydetails.getPersonInfo().getFullName();
					 vendorInfoCell=paydetails.getCellNumber();
					 
					 addrLine1=vendor.getPrimaryAddress().getAddrLine1();
					 addrLocality=vendor.getPrimaryAddress().getLocality();
					 addrLandmark=vendor.getPrimaryAddress().getLandmark();
					 addrCountry=vendor.getPrimaryAddress().getCountry();
					 addrState=vendor.getPrimaryAddress().getState();
					 addrCity=vendor.getPrimaryAddress().getCity();
					 addrPin=vendor.getPrimaryAddress().getPin();
				 }
				 
				 
				 
				 /**********************************End************************************************/
				 System.out.println("in side accnt interface:::: before updateTally");
				 Double tdsper=0d;
				 double tdsValue = 0;
				 		if(paydetails.getTdsPercentage()!=null && !paydetails.getTdsPercentage().equals("")){
				 			tdsper = Double.parseDouble(paydetails.getTdsPercentage());
						}
						else
						{
							tdsper= 0d;
						}
				 		
				 		
				 		
				 		if(paydetails.getTdsTaxValue()!=0.0)
				 		{
				 			tdsValue =paydetails.getTdsTaxValue();
				 		}
				 		else
				 		{
				 			tdsValue=0;
				 		}
				 		
				 		double baseAmt=0;
				 		if(paydetails.getBaseAmmount()!=0)
				 		{
				 			baseAmt = paydetails.getBaseAmmount();
				 		}
						 logger.log(Level.SEVERE,"Before Payment Accounting Interface Call");

						 String numberRange="";
						 if(paydetails.getNumberRange()!=null){
							 numberRange = paydetails.getNumberRange();
						 }
						 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
						 							paydetails.getEmployee(),//accountingInterfaceCreatedBy
						 							paydetails.getStatus(),
													 AppConstants.STATUS ,//status
													 "",//remark
													 "Accounts",//module
													 "Customer Payment",//doc type
													 paydetails.getCount(),//doc ID
														" ",//doc title
														paydetails.getCreationDate(),//doc date
														paydetails.getAccountType()+"-Payment Doc",//doc gl
														paydetails.getInvoiceCount()+"",//ref doc no.1
														so.getCreationDate(),//ref. doc date 1
														"Invoice",//ref.doc.type.1
														"",//ref doc no.2
														null,//ref doc date 2
														"",//ref doc type 2
														paydetails.getAccountType(),//accounttype
														personInfoId,//custID	
														personInfoName,//custName	
														personInfoCell,//custCell
														vendorInfoId,//venID	
														vendorInfoName,//venName	
														vendorInfoCell,//venCell
														0,//emp Id
														" ",//emp Name
														paydetails.getBranch(),//Branch
														paydetails.getEmployee(),//personResponsible
														"",//requested By
														paydetails.getApproverName(),//approverName
														paydetails.getPaymentMethod(),//paymentmethod
														paydetails.getPaymentDate(),//paymentDate
														paydetails.getChequeNo(),//cheque no.
														paydetails.getChequeDate(),//cheque date
														paydetails.getBankName(),//bank name
														paydetails.getBankAccNo(),//bank acc.
														0,//transferrefernceno.
														null,//transactiondate
														contractStartDate,//contract start date
														contractEndDate, // contract end date
														prodId,//prodId
														productCode,//prodCode
														productName,//prodName
														productQuantity,//prodQuantity
														null,//productDate
														0,//duration
														0,//services
														null,//UOM
														productprice,//product price
														vatPercent,//vat %
														calculatedamt,//vat amount
														vatglaccno,//vatglaccno.
														serviceTax,//service tax percent
														calculetedservice,//service tax amount
														serglaccno,//service tax gl accno.
														cform,//cformstatus
														cformPercent,//cformpercent
														cformAmount,//cformAmount
														cformglaccno,//cformglaccno
														totalAmount,//totalamount
														netPayable,//netpayable
														"",//Contract Category
														paydetails.getPaymentReceived(),//amountRecieved
														
														//   rohan added this 2 fields 
														baseAmt,
														tdsper,
														tdsValue,
														
														
														oc[0],
														oca[0],
														oc[1],
														oca[1],
														oc[2],
														oca[2],
														oc[3],
														oca[3],
														oc[4],
														oca[4],
														oc[5],
														oca[5],
														oc[6],
														oca[6],
														oc[7],
														oca[7],
														oc[8],
														oca[8],
														oc[9],
														oca[9],
														oc[10],
														oca[10],
														oc[11],
														oca[11], 
														addrLine1,
														addrLocality,
														addrLandmark,
														addrCountry,
														addrState,
														addrCity,
														addrPin,
														paydetails.getCompanyId(),
														paydetails.getBillingPeroidFromDate(),//  billing from date (rohan)
														paydetails.getBillingPeroidToDate(), //  billing to date (rohan)
													
														"", //Warehouse
														"",				//warehouseCode
														"",				//ProductRefId
														"",				//Direction
														"",				//sourceSystem
														"",				//Destination System
														null,
														null,
														numberRange
						 );
			
						}
		}
	   
	   /**
	    * ends here
	    */
	   
	   /**
		 * Date 29 jun 2017 added by vijay for gtetting customer correspondence Name or full name
		 * @param customerName
		 * @param companyId
		 * @return
		 */
		
		private String getCustomerName(String customerName, long companyId) {
			
			String custName;
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
			
			if(customer!=null){
				
				if(!customer.getCustPrintableName().equals("")){
					custName = customer.getCustPrintableName();
				}else{
					custName = customer.getFullname();
				}
				
			}else{
				custName = customerName;
			}
			
			
			return custName;
		}
		
		/**
		 * ends here
		 */

}
