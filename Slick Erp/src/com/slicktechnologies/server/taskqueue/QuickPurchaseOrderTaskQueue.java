package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.PurchaseOrderServiceImplementor;
import com.slicktechnologies.server.SalesOrderServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

public class QuickPurchaseOrderTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -63224935489282415L;
	
	Logger logger = Logger.getLogger("Name of the logger");
	double paymentPaid=0;
	double balancePayment =0;
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		logger.log(Level.SEVERE,"Welcome to Quick PurchaseOrder Task Queue");
		
		String purchaseOrderwithdollar = request.getParameter("purchaseoderkey");
		
		String purchaseOrder[] = purchaseOrderwithdollar.split("[$]");
		
		long companyId = Long.parseLong(purchaseOrder[0]);
		int purchaseOrderId = Integer.parseInt(purchaseOrder[1]);
		
		
		
		/**
		 * added by vijay for quick Purchase Order accounting interface
		 */
		
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGCO).filter("companyId", companyId).filter("configStatus", true).first().now();
		}
		int flag =0;
		
		if(processConfig!=null){
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			quickPurchaseOrderccountingInterface(purchaseOrderId ,companyId);
		}
		
		
		/**
		 * ends here
		 */
		
		
		
		PurchaseOrder purchaseOrderEntity = ofy().load().type(PurchaseOrder.class).filter("companyId", companyId).filter("count", purchaseOrderId).first().now();
		purchaseOrderEntity.setStatus("Approved");
		logger.log(Level.SEVERE,"Purchase ORDER STATUS In TASK Queue=="+purchaseOrderEntity.getStatus());
		if(purchaseOrderEntity!=null){ 
			
			 paymentPaid = purchaseOrderEntity.getPaymentPaid();
			 balancePayment = purchaseOrderEntity.getBalancePayment();
			 
			saveTaxesAndCharges(purchaseOrderEntity);
			
			GRN grnObject = createGRN(purchaseOrderEntity);
				
			
			/***
			 * Date 14-sep-2017 old code commented by vijay old code was payment paid invoice approved payment closed
			 * new code is now only one invoice and paymnet paid closed payment doc and balance payment paymnet documnet created open
			 */
			 
//			double percentage = getpercentage(purchaseOrderEntity.getNetpayble(),purchaseOrderEntity.getPaymentPaid());
			
//			if(100-percentage==0 || 100-percentage==100.0){
//				System.out.println("For only 100 paymet");
				
//				if(purchaseOrderEntity.getPaymentPaid()==0.0 || purchaseOrderEntity.getPaymentPaid()==0){
//					System.out.println("if payment recieved nothing  =  this is for balance payment");
//					percentage =100;
			/**
			 * Updated By: Viraj
			 * Date: 09-02-2019
			 * Description: To set grn if warehouse present.
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRNWAREHOUSE","GrnStatusOnWarehouse", purchaseOrderEntity.getCompanyId())) {
				System.out.println("inside GRNWAREHOUSE configuration for billing");
				if(grnObject != null) {
					if(grnObject.getStatus().equals(GRN.APPROVED)) {
						System.out.println("above createBillingDocuments");
						createBillingDocuments(purchaseOrderEntity);
					}
				}
			} else {
				createBillingDocuments(purchaseOrderEntity);
			}				
		/** Ends **/
//				}
//				else{
//					System.out.println("for payment received full 100 percent ");
//					createBillingDocuments(purchaseOrderEntity,percentage,true,"ClosedPayment");
//				}
//				
//				
//			}else{
//				
//				System.out.println("For partial payment");
//				for(int i=0;i<2;i++){
//					if(i==1){
//						System.out.println("Balance payment Billing doc");
//						double percent = 100-percentage;
//						logger.log(Level.SEVERE,"  For partial balance Payment remaining percenatge ==="+percent);
//	
//						createBillingDocuments(purchaseOrderEntity,percent,false,"OpenPayment");
//					}else{
//						
//						System.out.println("Payment recieved billing doc ================");
//						createBillingDocuments(purchaseOrderEntity,percentage,true,"ClosedPayment");
//					}
//				}
//			}
			
		}
	}
	
	/**
	 * Purchase Order Interface
	 */

	private void quickPurchaseOrderccountingInterface(int purchaseOrderId, long companyId) {
		
		PurchaseOrder purchaseorder =ofy().load().type(PurchaseOrder.class).filter("companyId", companyId).filter("count", purchaseOrderId).first().now();
		if(purchaseorder.getCount()!=0){
			
			for(int  i = 0;i<purchaseorder.getProductDetails().size();i++){
				
				String unitofmeasurement = purchaseorder.getProductDetails().get(i).getUnitOfmeasurement();
				double productprice = purchaseorder.getProductDetails().get(i).getProdPrice();
				 int prodId=purchaseorder.getProductDetails().get(i).getProductID();
				 String productCode = purchaseorder.getProductDetails().get(i).getProductCode();
				 String productName = purchaseorder.getProductDetails().get(i).getProductName();
				 double productQuantity = purchaseorder.getProductDetails().get(i).getProductQuantity();
				 double totalAmount = purchaseorder.getProductDetails().get(i).getProdPrice()*purchaseorder.getProductDetails().get(i).getProductQuantity();
				 double calculatedamt = 0;
					double calculetedservice = 0;
				 
					double cformamtService= 0;
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;
				 double vatPercent=0;
				 String cform = "";
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
			/***************************************************************************************/
				 
				 if(purchaseorder.getcForm()!= null&&purchaseorder.getcForm().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=purchaseorder.getCstpercent();
					 cformAmount=purchaseorder.getCstpercent()*totalAmount/100;
					 cformamtService=purchaseorder.getCstpercent()*totalAmount/100;
					 if(purchaseorder.getProductDetails().get(i).getTax()!=0){
						 serviceTax=purchaseorder.getProductDetails().get(i).getTax();
						 calculetedservice=((cformamtService+totalAmount)*purchaseorder.getProductDetails().get(i).getTax())/100;
					 }
					 
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",purchaseorder.getCompanyId()).filter("taxChargePercent",purchaseorder.getCstpercent()).filter("isCentralTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",purchaseorder.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 cformglaccno=serglAcc.getGlAccountNo();
				 }
				 
				 else if(purchaseorder.getcForm()!= null&&purchaseorder.getcForm().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=purchaseorder.getProductDetails().get(i).getVat();
					 cformAmount=purchaseorder.getProductDetails().get(i).getVat()*totalAmount/100;
					 cformamtService=purchaseorder.getProductDetails().get(i).getVat()*totalAmount/100;
					 
					 if(purchaseorder.getProductDetails().get(i).getTax()!=0){
						 serviceTax=purchaseorder.getProductDetails().get(i).getTax();
						 calculetedservice=((cformamtService+totalAmount)*purchaseorder.getProductDetails().get(i).getTax())/100;
					 }
					 
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",purchaseorder.getCompanyId()).filter("taxChargePercent",purchaseorder.getCstpercent()).filter("isCentralTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",purchaseorder.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 serglaccno=serglAcc.getGlAccountNo();
				 }
				 
				 
				 else{//if the customer is of same state
					 
					 if(purchaseorder.getProductDetails().get(i).getVat()!=0)
					 {
						 vatPercent=purchaseorder.getProductDetails().get(i).getVat();
						 calculatedamt=purchaseorder.getProductDetails().get(i).getVat()*totalAmount/100;
						 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",purchaseorder.getCompanyId()).filter("taxChargePercent",purchaseorder.getProductDetails().get(i).getVat()).filter("isVatTax",true).first().now();
						 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",purchaseorder.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
						 vatglaccno=vatglAcc.getGlAccountNo();
					 }
					 
					 if(purchaseorder.getProductDetails().get(i).getTax()!=0)
					 {
						 serviceTax=purchaseorder.getProductDetails().get(i).getTax();
						 calculetedservice=(calculatedamt+totalAmount)*purchaseorder.getProductDetails().get(i).getTax()/100;
						 
					 }
					 
					 
				 }
				 
				 
				 if(purchaseorder.getProductDetails().get(i).getTax()!=0)
				 {
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",purchaseorder.getCompanyId()).filter("taxChargePercent",purchaseorder.getProductDetails().get(i).getTax()).filter("isServiceTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",purchaseorder.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 serglaccno=serglAcc.getGlAccountNo();
				 }
				 
			
				 
				 if(purchaseorder.getProductDetails().get(i).getVat()!=0&&purchaseorder.getcForm()==null){
					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",purchaseorder.getCompanyId()).filter("taxChargePercent",purchaseorder.getProductDetails().get(i).getVat()).filter("isVatTax",true).first().now();
//					 System.out.println("tax val"+vattaxDtls.getGlAccountName().trim());
					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",purchaseorder.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
					 vatglaccno=vatglAcc.getGlAccountNo();
				 }
				 
				 if(purchaseorder.getProductDetails().get(i).getTax()!=0){
					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",purchaseorder.getCompanyId()).filter("taxChargePercent",purchaseorder.getProductDetails().get(i).getTax()).filter("isServiceTax",true).first().now();
					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",purchaseorder.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 serglaccno=serglAcc.getGlAccountNo();
				 }

				 
				 double netPayable = purchaseorder.getNetpayble();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
				 
				 for(int k = 0; k < purchaseorder.getProductCharges().size();k++){
					 oc[k] = purchaseorder.getProductCharges().get(k).getChargeName();
					 oca[k] = purchaseorder.getProductCharges().get(k).getChargePayable();
				 }
				 
				 /**********************************End************************************************/
				
				 String vName = null;
				 long vCell = 0;
				 int vId = 0;
				 Vendor vendor = null;
				 
				 for(int o  = 0; o<purchaseorder.getVendorDetails().size();o++){
					 if(purchaseorder.getVendorDetails().get(o).getStatus()==true){
						
					vName = purchaseorder.getVendorDetails().get(o).getVendorName();
					vCell = purchaseorder.getVendorDetails().get(o).getVendorPhone();
					vId= purchaseorder.getVendorDetails().get(o).getVendorId();
					
					 vendor=ofy().load().type(Vendor.class).filter("companyId",purchaseorder.getCompanyId()).filter("count", vId).first().now();
						 
					 }
					 }
					 
					 
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
						 								purchaseorder.getEmployee(),//accountingInterfaceCreatedBy
						 								purchaseorder.getStatus(),
													 AppConstants.STATUS ,//Status
													 "",//remark
													 "Purchase",//module
													 "Purchase Order",//documentType
													 purchaseorder.getCount(),//docID
													 purchaseorder.getPOName(),//DOCtile
													 purchaseorder.getCreationDate(), //docDate
														AppConstants.DOCUMENTGL,//docGL
														"",//ref doc no.1
														purchaseorder.getCreationDate(),//ref doc date.1
														AppConstants.REFDOCPO,//ref doc type 1
														"",//ref doc no 2
														null,//ref doc date 2
														"",//ref doc type 2
														AppConstants.ACCOUNTTYPEAP,//accountType
														0,//custID
														"",//custName
														0,//custCell
														vId,//vendor ID
														vName,//vendor NAme
														vCell,//vendorCell
														0,//empId
														" ",//empNAme
														purchaseorder.getBranch(),//branch
														purchaseorder.getEmployee(),//personResponsible
														"",//requestedBY
														purchaseorder.getApproverName(),//approvedBY
														purchaseorder.getPaymentMethod(),//paymentmethod
														null,//paymentdate
														"",//cheqno.
														null,//cheqDate
														null,//bankName
														null,//bankAccount
														0,//transferReferenceNumber
														null,//transactionDate
														null,//contract start date
														null, // contract end date
														prodId,//prod ID
														productCode,//prod Code
														productName,//productNAme
														productQuantity,//prodQuantity
														null,//productDate
														0,//duration
														0,//services
														unitofmeasurement,//unit of measurement
														productprice,//productPrice
														vatPercent,//VATpercent
														calculatedamt,//VATamt
														vatglaccno,//VATglAccount
														serviceTax,//serviceTaxPercent
														calculetedservice,//serviceTaxAmount
														serglaccno,//serviceTaxGLaccount
														cform,//cform
														cformPercent,//cformpercent
														cformAmount,//cformamount
														cformglaccno,//cformGlaccount	
														totalAmount,//totalamouNT
														netPayable,//NET PAPAYABLE
														"",//Contract Category
														0,//amount Recieved
														0.0,// base Amt for tdscal in pay  by rohan  
														0, //  tds percentage by rohan 
														0,//  tds amount  by rohan 
														oc[0],
														oca[0],
														oc[1],
														oca[1],
														oc[2],
														oca[2],
														oc[3],
														oca[3],
														oc[4],
														oca[4],
														oc[5],
														oca[5],
														oc[6],
														oca[6],
														oc[7],
														oca[7],
														oc[8],
														oca[8],
														oc[9],
														oca[9],
														oc[10],
														oca[10],
														oc[11],
														oca[11],
														vendor.getPrimaryAddress().getAddrLine1(),
														vendor.getPrimaryAddress() .getLocality(),
														vendor.getPrimaryAddress().getLandmark(),
														vendor.getPrimaryAddress().getCountry(),
														vendor.getPrimaryAddress().getState(),
														vendor.getPrimaryAddress().getCity(),
														vendor.getPrimaryAddress().getPin(),
														purchaseorder.getCompanyId(),
														null,				//  billing from date (rohan)
														null,			//  billing to date (rohan)
														"", //Warehouse
														"",				//warehouseCode
														"",				//ProductRefId
														"",				//Direction
														"",				//sourceSystem
														"",				//Destination System
														null,
														null,
														null
														);

		}
		
	}

	}



	/**
	 * Here i am getting Purchase Order percentage for payment terms based on net payable and  payment paid 
	 */
	
	private double getpercentage(double netpayable, double paymentPaid) {
		double totalpercentage;
		totalpercentage= paymentPaid/netpayable*100;
		return totalpercentage;
	}
	
	/**
	 * ends here
	 * @param purchaseOrderEntity 
	 */

	/**************************** Tax part Started here **************************************/

	private void saveTaxesAndCharges(PurchaseOrder purchaseOrderEntity) {
		if(purchaseOrderEntity.getCount()!=0){
			final GenricServiceImpl genimpl=new GenricServiceImpl();
			System.out.println("kasbdsdadads");
			TaxesAndCharges taxChargeEntity=new TaxesAndCharges();
			System.out.println("Hi vijay po charges ==="+purchaseOrderEntity.getProductCharges());
			List<ContractCharges> listaxcharge=saveCharges(purchaseOrderEntity.getProductCharges());
			System.out.println("FROM PO ==="+listaxcharge.size());
			ArrayList<ContractCharges> arrtaxchrg=new ArrayList<ContractCharges>();
			arrtaxchrg.addAll(listaxcharge);
			taxChargeEntity.setContractId(purchaseOrderEntity.getCount());
			taxChargeEntity.setIdentifyOrder(AppConstants.BILLINGPURCHASEFLOW.trim());
			taxChargeEntity.setTaxesChargesList(arrtaxchrg);
			taxChargeEntity.setCompanyId(purchaseOrderEntity.getCompanyId());
			
			genimpl.save(taxChargeEntity);
		}
	}
	
	private List<ContractCharges> saveCharges(List<ProductOtherCharges> chargesList)
	{
		ArrayList<ContractCharges> arrCharge=new ArrayList<ContractCharges>();
		double calcBalAmt=0;
		for(int i=0;i<chargesList.size();i++)
		{
			ContractCharges chargeDetails=new ContractCharges();
			chargeDetails.setTaxChargeName(chargesList.get(i).getChargeName());
			chargeDetails.setTaxChargePercent(chargesList.get(i).getChargePercent());
			chargeDetails.setTaxChargeAbsVal(chargesList.get(i).getChargeAbsValue());
			chargeDetails.setTaxChargeAssesVal(chargesList.get(i).getAssessableAmount());
			if(chargesList.get(i).getChargeAbsValue()!=0){
				calcBalAmt=chargesList.get(i).getChargeAbsValue();
			}
			if(chargesList.get(i).getChargePercent()!=0){
				calcBalAmt=chargesList.get(i).getChargePercent()*chargesList.get(i).getAssessableAmount()/100;
			}
			chargeDetails.setChargesBalanceAmt(calcBalAmt);
			arrCharge.add(chargeDetails);
		}
		return arrCharge;
		
	}
	
	/*************************** Tax Part completed here **************************************/
	
	
	/************************* Create GRN Part start here ***************************************/
	
	private GRN createGRN(PurchaseOrder purchaseOrderEntity) {

		final GenricServiceImpl genimpl=new GenricServiceImpl();
		
		GRN grnEntity=new GRN();
		grnEntity.setPoNo(purchaseOrderEntity.getCount());
		grnEntity.setPoTitle(purchaseOrderEntity.getPOName());
		grnEntity.setPoCreationDate(purchaseOrderEntity.getPODate());
		grnEntity.setTitle(purchaseOrderEntity.getPOName());
		
		if(purchaseOrderEntity.getProject()!=null){
			grnEntity.setProJectName(purchaseOrderEntity.getProject());
		}
		grnEntity.setCreationDate(purchaseOrderEntity.getDeliveryDate());
		if(purchaseOrderEntity.getEmployee()!=null){
			grnEntity.setEmployee(purchaseOrderEntity.getEmployee());
		}
		if(purchaseOrderEntity.getApproverName()!=null){
			grnEntity.setApproverName(purchaseOrderEntity.getApproverName());
		}
//		grnEntity.setStatus(GRN.APPROVED);
		/**
		 * nidhi
		 * 23-08-2018
		 */
		boolean hvac = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC","HideOrShowModelNoAndSerialNoDetails", purchaseOrderEntity.getCompanyId());
		boolean createGrn = true;
		if(hvac){
			createGrn = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC","CreateGrnFromPurchaseRegister", purchaseOrderEntity.getCompanyId()); 
		}
		System.out.println("createGrn: "+createGrn);
		if(createGrn){
			grnEntity.setStatus(GRN.APPROVED);
			/**
			 * Updated By: Viraj
			 * Date: 09-02-2019
			 * Description: To set grn if warehouse present.
			 */
			System.out.println("before GRNWAREHOUSE process configuration");
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRNWAREHOUSE","GrnStatusOnWarehouse", purchaseOrderEntity.getCompanyId())) {
				int count = 0;
				System.out.println("inside GRNWAREHOUSE process configuration");
				System.out.println("product details: "+ purchaseOrderEntity.getProductDetails().size());
				for(int i=0;i < purchaseOrderEntity.getProductDetails().size(); i++) {
					System.out.println("Inside for of productDetails: "+purchaseOrderEntity.getProductDetails().get(i).getItemProductWarehouseName());
					if(!purchaseOrderEntity.getProductDetails().get(i).getItemProductWarehouseName().equals("") ) {
						System.out.println("inside warehouse");
						count += 1;
					} 
				}
				if(count == 0) {
					grnEntity.setStatus(GRN.CREATED);
					logger.log(Level.SEVERE,"inside create grn");
				} else if(count == purchaseOrderEntity.getProductDetails().size()) {
					grnEntity.setStatus(GRN.APPROVED);
					logger.log(Level.SEVERE,"inside approved grn");
				}
			}
			/** Ends **/
		}else{
//			grnEntity.setStatus(GRN.CREATED);
			/**Date 11-6-2020 by Amol set status as approved raised by Ashwini Bhagwat.**/
			grnEntity.setStatus(GRN.APPROVED);
		}
		/**
		 * end
		 */
		grnEntity.setInspectionRequired(AppConstants.NO);
		grnEntity.setBranch(purchaseOrderEntity.getBranch());
		if(!purchaseOrderEntity.getVendorDetails().isEmpty()){
			for(int i=0;i<purchaseOrderEntity.getVendorDetails().size();i++){
				if(purchaseOrderEntity.getVendorDetails().get(i).getStatus()==true){
					grnEntity.getVendorInfo().setFullName(purchaseOrderEntity.getVendorDetails().get(i).getVendorName());
					grnEntity.getVendorInfo().setCount(purchaseOrderEntity.getVendorDetails().get(i).getVendorId());
					grnEntity.getVendorInfo().setCellNumber(purchaseOrderEntity.getVendorDetails().get(i).getVendorPhone());
				
					/**
					 * Date : 06-02-2017 By Anil
					 * 
					 */
					grnEntity.getVendorInfo().setPocName(purchaseOrderEntity.getVendorDetails().get(i).getPocName());
					/**
					 * End
					 */
					
				}
			}
		}
		
		if(purchaseOrderEntity.getProductDetails().size()!=0){
			ArrayList<GRNDetails> newprod = new ArrayList<GRNDetails>();
			ArrayList<ProductDetailsPO> prodlist = purchaseOrderEntity.getProductDetails();
			
			for (ProductDetailsPO temp : prodlist) {
				GRNDetails grn = new GRNDetails();
				// added By: Viraj date: 13-02-2019 Description: To approve grn this was updated in PO but not in PR By Komal
				grn.setPrduct(temp.getPrduct());
				grn.setPurchaseTax1(temp.getPurchaseTax1());
				grn.setPurchaseTax2(temp.getPurchaseTax2());
				// ends
				grn.setProductID(temp.getProductID());
				grn.setProductCode(temp.getProductCode());
				grn.setProductCategory(temp.getProductCategory());
				grn.setProductName(temp.getProductName());
				grn.setUnitOfmeasurement(temp.getUnitOfmeasurement());
				grn.setProductQuantity(temp.getProductQuantity());
				grn.setProdPrice(temp.getProdPrice());
				grn.setTotal(temp.getTotal());
				grn.setDiscount(temp.getDiscount());
				grn.setTax(temp.getTax());
				grn.setVat(temp.getVat());
				
				if(!temp.getItemProductWarehouseName().equals("") && !temp.getItemProductStrorageLocation().equals("")
						&& !temp.getItemProductStorageBin().equals("")){
					
					grn.setWarehouseLocation(temp.getItemProductWarehouseName());
					grn.setStorageLoc(temp.getItemProductStrorageLocation());
					grn.setStorageBin(temp.getItemProductStorageBin());
					
				}else{
					grn.setWarehouseLocation("");
					grn.setStorageLoc("");
					grn.setStorageBin("");
				}
			
				/**
				 * nidhi
				 * 23-08-2018
				 */
				if(temp.getProSerialNoDetails()!= null && temp.getProSerialNoDetails().size()>0){
					grn.setProSerialNoDetails(temp.getProSerialNoDetails());
				}
				/**
				 * end
				 */
				
				
				newprod.add(grn);
			}
			grnEntity.setInventoryProductItem(newprod);
		}
		
		grnEntity.setCompanyId(purchaseOrderEntity.getCompanyId());
		
		// vijay
		grnEntity.setTitle(purchaseOrderEntity.getPOName());
		
		genimpl.save(grnEntity);
		
		/********************** upating stock         ****************************/
		/**
		 * Updated By: Viraj
		 * Date: 09-02-2019
		 * Description: To set grn if warehouse present.
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRNWAREHOUSE","GrnStatusOnWarehouse", purchaseOrderEntity.getCompanyId())) {
			if(grnEntity.getStatus().equals(GRN.APPROVED)) {
				ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
				for(GRNDetails product:grnEntity.getInventoryProductItem()){
					InventoryTransactionLineItem item=new InventoryTransactionLineItem();
					item.setCompanyId(grnEntity.getCompanyId());
					item.setBranch(grnEntity.getBranch());
					item.setDocumentType(AppConstants.GRN);
					item.setDocumentId(grnEntity.getCount());
					item.setDocumentDate(grnEntity.getCreationDate());
					item.setDocumnetTitle("");
					item.setProdId(product.getProductID());
					item.setUpdateQty(product.getProductQuantity());
					item.setOperation(AppConstants.ADD);
					item.setWarehouseName(product.getWarehouseLocation());
					item.setStorageLocation(product.getStorageLoc());
					item.setStorageBin(product.getStorageBin());
					/** date 30.11.2018 added by komal **/
					item.setProductName(product.getProductName());
					item.setProductCode(product.getProductCode());
					if(product.getPrduct() != null){
						if(product.getPrduct().getUnitOfMeasurement() != null){
							item.setProductUOM(product.getPrduct().getUnitOfMeasurement());
						}
						if(product.getPrduct().getProductCategory() != null){
							item.setProductCategory(product.getPrduct().getProductCategory());
						}
					}
			
					HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
						
					if(product.getProSerialNoDetails() !=null &&
							product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
						prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
						for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
							 ProductSerialNoMapping element = new ProductSerialNoMapping();
							 element.setAvailableStatus(pr.isAvailableStatus());
							 element.setNewAddNo(pr.isNewAddNo());
							 element.setStatus(pr.isStatus());
							 element.setProSerialNo(pr.getProSerialNo());
							 prserdt.get(0).add(element);
						}
						for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
					      {
							ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
							if(!pr.isStatus()){
									itr.remove();
							}
					      }
					}
					item.setProSerialNoDetails(prserdt);
					itemList.add(item);
				}
				UpdateStock.setProductInventory(itemList , true);
			}
		} else {
			ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
			for(GRNDetails product:grnEntity.getInventoryProductItem()){
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(grnEntity.getCompanyId());
				item.setBranch(grnEntity.getBranch());
				item.setDocumentType(AppConstants.GRN);
				item.setDocumentId(grnEntity.getCount());
				item.setDocumentDate(grnEntity.getCreationDate());
				item.setDocumnetTitle("");
				item.setProdId(product.getProductID());
				item.setUpdateQty(product.getProductQuantity());
				item.setOperation(AppConstants.ADD);
				item.setWarehouseName(product.getWarehouseLocation());
				item.setStorageLocation(product.getStorageLoc());
				item.setStorageBin(product.getStorageBin());
				
				/** date 30.11.2018 added by komal **/
				item.setProductName(product.getProductName());
				item.setProductCode(product.getProductCode());
				if(product.getPrduct() != null){
					if(product.getPrduct().getUnitOfMeasurement() != null){
						item.setProductUOM(product.getPrduct().getUnitOfMeasurement());
					}
					if(product.getPrduct().getProductCategory() != null){
						item.setProductCategory(product.getPrduct().getProductCategory());
					}
				}
		
				HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					
				if(product.getProSerialNoDetails() !=null &&
						product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
					prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
					for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
						 ProductSerialNoMapping element = new ProductSerialNoMapping();
						 element.setAvailableStatus(pr.isAvailableStatus());
						 element.setNewAddNo(pr.isNewAddNo());
						 element.setStatus(pr.isStatus());
						 element.setProSerialNo(pr.getProSerialNo());
						 prserdt.get(0).add(element);
					}
					for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
				      {
						ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
						if(!pr.isStatus()){
								itr.remove();
						}
				      }
				}
				item.setProSerialNoDetails(prserdt);
				itemList.add(item);
			}
			UpdateStock.setProductInventory(itemList , true);
		}
	/** Ends **/
		return grnEntity;
	}
	
	/************************************* GRN Completed here ***********************************/
	
	/************************************* Billing part start here *******************************/
	
	
	private void createBillingDocuments(PurchaseOrder poEntity) {
		double percentage=100;

		List<VendorDetails> personInfo=poEntity.getVendorDetails();
		PersonInfo personInfoEntity=new PersonInfo();
		for(int i=0;i<personInfo.size();i++)
		{
			if(personInfo.get(i).getStatus()==true)
			{
				personInfoEntity.setCount(personInfo.get(i).getVendorId());
				personInfoEntity.setFullName(personInfo.get(i).getVendorName());
				personInfoEntity.setCellNumber(personInfo.get(i).getVendorPhone());
			}
		}
		
			ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			PaymentTerms paymentTerms=new PaymentTerms();
			Date conStartDate=null;
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			
			billingDocEntity.setPersonInfo(personInfoEntity);
			billingDocEntity.setContractCount(poEntity.getCount());
		
			if(poEntity.getNetpayble()!=0){
				billingDocEntity.setTotalSalesAmount(poEntity.getNetpayble());
			}
			
			salesProdLis=this.retrieveSalesProducts(poEntity, percentage);
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			if(salesOrdArr.size()!=0)
				billingDocEntity.setSalesOrderProducts(salesOrdArr);
			if(poEntity.getCompanyId()!=null)
				billingDocEntity.setCompanyId(poEntity.getCompanyId());
			
			Date invoiceDate=null;
			if(poEntity.getQuickPurchaseInvoiceDate()!=null){
				invoiceDate = poEntity.getQuickPurchaseInvoiceDate();
			}
			else
			{
				invoiceDate = poEntity.getPODate();
			}
			logger.log(Level.SEVERE," Billing Invoice paymnent Date"+invoiceDate);
			
			if(invoiceDate!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
				logger.log(Level.SEVERE," Billing Date While SAving"+billingDocEntity.getBillingDate());
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
				logger.log(Level.SEVERE," Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
				logger.log(Level.SEVERE," Payment Date While SAving"+billingDocEntity.getPaymentDate());
			}
			
			if(poEntity.getPaymentMethod()!=null)
				billingDocEntity.setPaymentMethod(poEntity.getPaymentMethod());
			if(poEntity.getApproverName()!=null)
				billingDocEntity.setApproverName(poEntity.getApproverName());
			if(poEntity.getEmployee()!=null)
				billingDocEntity.setEmployee(poEntity.getEmployee());
			if(poEntity.getBranch()!=null)
				billingDocEntity.setBranch(poEntity.getBranch());
			if(poEntity.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				//billTaxLis=this.listForBillingTaxes(salesProdLis);
				billTaxLis=this.listForBillingTaxes(poEntity,percentage);
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
			}
			
			billingDocEntity.setOrderCreationDate(poEntity.getCreationDate());

			double totBillAmt=getTotalBillAmt(salesProdLis);
			billingDocEntity.setTotalBillingAmount(totBillAmt);
			
			paymentTerms.setPayTermDays(0);
			
//			double paypercent= Math.round(percentage);
//			System.out.println("while saving percentage === "+paypercent);
			paymentTerms.setPayTermPercent(percentage);
			paymentTerms.setPayTermComment("");
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			
			if(poEntity.getcForm()!=null){
				billingDocEntity.setOrderCformStatus(poEntity.getcForm());
			}
			else{
				billingDocEntity.setOrderCformStatus("");
			}
			
			if(poEntity.getCstpercent()!=0){
				billingDocEntity.setOrderCformPercent(poEntity.getCstpercent());
			}
			else{
				billingDocEntity.setOrderCformPercent(-1);
			}
			
	//***********************changes made by rohan for saving gross value *************	
			double grossValue=poEntity.getTotalAmount();
			
			
			billingDocEntity.setGrossValue(grossValue);
			
			billingDocEntity.setArrPayTerms(billingPayTerms);
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPEPURCHASE);
			billingDocEntity.setStatus(BillingDocument.APPROVED);
			billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAP);
			
			arrbilling.add(billingDocEntity);
			/******************************************************************************/
			
			double payableamt=0;
			payableamt = calculateTotalBillAmt(billingDocEntity.getSalesOrderProducts()) + calculateTotalTaxes(billingDocEntity.getBillingTaxes()) + calculateTotalBillingCharges(billingDocEntity.getBillingOtherCharges());
			payableamt=Math.round(payableamt);
			System.out.println("Payable amt =="+payableamt);
			
			List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
			
			BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
			
			if(billingDocEntity.getContractCount()!=null){
				billingDocDetails.setOrderId(billingDocEntity.getContractCount());
			}
			
			if(billingDocEntity.getCount()!=0)
				billingDocDetails.setBillId(billingDocEntity.getCount());
			if(billingDocEntity.getBillingDate()!=null)
				billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
			if(billingDocEntity.getBillAmount()!=null)
				billingDocDetails.setBillAmount(payableamt);
			if(billingDocEntity.getStatus()!=null)
				billingDocDetails.setBillstatus(billingDocEntity.getStatus());
			System.out.println("Billing IDDDDD===="+billingDocDetails.getBillId());
			billtablelis.add(billingDocDetails);
			
			billingDocEntity.setTotalBillingAmount(payableamt);
			System.out.println(" getTotalBillingAmount amount ===");
			System.out.println("billing amount ==" +billingDocEntity.getBillAmount());
			System.out.println("billing amount ####  ==" +Math.round(billingDocEntity.getTotalBillingAmount()));
			
			ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
			billtablearr.addAll(billtablelis);
			billingDocEntity.setArrayBillingDocument(billtablelis);
			
			
			/** Date 14-09-2017 added by vijay for total amount before taxes**/
			billingDocEntity.setTotalAmount(poEntity.getTotalAmount());
			/** Date 14-09-2017 added by vijay for total amount after discount amount before taxes**/
			billingDocEntity.setFinalTotalAmt(poEntity.getTotalAmount()); 
			 /** Date 14-09-2017 added by vijay for total amount after taxes and before other charges **/
			billingDocEntity.setTotalAmtIncludingTax(poEntity.getGtotal());
			/*** Date 14-09-2017 added by vijay for grand total before roundoff *********/
			billingDocEntity.setGrandTotalAmount(poEntity.getNetpayble());
			
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(billingDocEntity);
			
			
			GenricServiceImpl impl=new GenricServiceImpl();
			if(arrbilling.size()!=0){
				impl.save(arrbilling);
			}
			
			logger.log(Level.SEVERE,"Here Billing updated successfully");
			
//			System.out.println("payament received for invoice and payment == "+balancepayment);
//			if(balancepayment.equalsIgnoreCase("ClosedPayment")){
				Createinvoice(poEntity,billingDocEntity);
//			}	

//		}
	
	}


	



	private double calculateTotalBillingCharges(List<ContractCharges> billingOtherCharges) {

		List<ContractCharges> list = billingOtherCharges;
		double sum = 0;
		for (int i = 0; i < list.size(); i++) {
			ContractCharges entity = list.get(i);
			sum = sum+ (entity.getChargesBalanceAmt() * entity.getPaypercent() / 100);
		}
		return sum;

	}


	private double calculateTotalTaxes(List<ContractCharges> billingTaxes) {

		List<ContractCharges>list=billingTaxes;
		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ContractCharges entity=list.get(i);
			sum=sum+(entity.getTaxChargeAssesVal()*entity.getTaxChargePercent()/100);
		}
		System.out.println("Sum Taxes"+sum);
		return sum;

	}

	private double calculateTotalBillAmt(ArrayList<SalesOrderProductLineItem> salesOrderProducts) {

		System.out.println("Entered in table calculate");
		List<SalesOrderProductLineItem>list=salesOrderProducts;

		double caltotalamt=0;
		for(int i=0;i<list.size();i++)
		{
			SalesOrderProductLineItem entity=list.get(i);
			
			if(entity.getFlatDiscount()==0)
			{
				caltotalamt=caltotalamt+(entity.getBaseBillingAmount()*entity.getPaymentPercent())/100;
			}
			else
			{
				caltotalamt=caltotalamt+((entity.getBaseBillingAmount()*entity.getPaymentPercent())/100)-entity.getFlatDiscount();
			}
		}
		return caltotalamt;

	}
	
	

private double getTotalBillAmt(List<SalesOrderProductLineItem> lisForTotalBill) {
		
		
		double saveTotalBillAmt=0;
		for(int i=0;i<lisForTotalBill.size();i++)
		{
			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getBaseBillingAmount();
		}
		System.out.println("Save Total Billing Amount"+saveTotalBillAmt);
		return saveTotalBillAmt;
	}

	private List<ContractCharges> listForBillingTaxes(PurchaseOrder poEntity,
			double percentage) {
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
		double assessValue=0;
		for(int i=0;i<poEntity.getProductTaxes().size();i++){
			ContractCharges taxDetails=new ContractCharges();
			taxDetails.setTaxChargeName(poEntity.getProductTaxes().get(i).getChargeName());
			taxDetails.setTaxChargePercent(poEntity.getProductTaxes().get(i).getChargePercent());
			assessValue=poEntity.getProductTaxes().get(i).getAssessableAmount()*percentage/100;
			taxDetails.setTaxChargeAssesVal(assessValue);
			
			taxDetails.setIdentifyTaxCharge(poEntity.getProductTaxes().get(i).getIndexCheck());
			arrBillTax.add(taxDetails);
		}
		return arrBillTax;
	}



	private List<SalesOrderProductLineItem> retrieveSalesProducts(PurchaseOrder poEntity, double percentage) {
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<poEntity.getProductDetails().size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", poEntity.getProductDetails().get(i).getProductID()).filter("companyId",poEntity.getCompanyId()).first().now();
			
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
//			SuperProduct productEnt=this.getItems().get(i).getPrduct();
			/**Date 13-07-2020 by Amol vendor invoice Pdf not printed issue at the time of Purchase register Flow**/
			salesOrder.setPrduct(superProdEntity);
			salesOrder.setProdId(poEntity.getProductDetails().get(i).getProductID());
			salesOrder.setProdCategory(poEntity.getProductDetails().get(i).getProductCategory());
			salesOrder.setProdCode(poEntity.getProductDetails().get(i).getProductCode());
			salesOrder.setProdName(poEntity.getProductDetails().get(i).getProductName());
			salesOrder.setQuantity(poEntity.getProductDetails().get(i).getProductQuantity());
			salesOrder.setOrderDuration(0);
			salesOrder.setOrderServices(0);
			totalTax=this.removeTaxAmt(poEntity.getProductDetails().get(i).getPrduct());
			prodPrice=poEntity.getProductDetails().get(i).getProdPrice()-totalTax;
			salesOrder.setPrice(prodPrice);
			
			Tax vatTax=new Tax();
			/**
			 * @author Anil
			 * @since 12-01-2021
			 */
//			vatTax.setTaxConfigName("CGST@"+poEntity.getProductDetails().get(i).getVat()+"");
			vatTax=poEntity.getProductDetails().get(i).getVatTax();
			
			
			Tax servTax=new Tax();
			/**
			 * @author Anil
			 * @since 12-01-2021
			 */
//			servTax.setTaxConfigName("SGST@"+poEntity.getProductDetails().get(i).getTax()+"");
			servTax=poEntity.getProductDetails().get(i).getServiceTax();
			
			salesOrder.setVatTax(vatTax);
			salesOrder.setServiceTax(servTax);
			salesOrder.setProdPercDiscount(poEntity.getProductDetails().get(i).getDiscount());
			salesOrder.setDiscountAmt(poEntity.getProductDetails().get(i).getDiscountAmt());
			salesOrder.setUnitOfMeasurement(poEntity.getProductDetails().get(i).getUnitOfmeasurement());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			
			
		//  Vijay added this code for setting HSN Code Date : 03-07-2017
			if(poEntity.getProductDetails().get(i).getPrduct().getHsnNumber()!=null && !poEntity.getProductDetails().get(i).getPrduct().getHsnNumber().equals(""))
			salesOrder.setHsnCode(poEntity.getProductDetails().get(i).getPrduct().getHsnNumber());
			
					
			
/***************** new code area calculation and discount on total *******************/
			
			if((poEntity.getProductDetails().get(i).getDiscount()==null && poEntity.getProductDetails().get(i).getDiscount()==0) && (poEntity.getProductDetails().get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				percAmt=prodPrice;
			}
			
			else if((poEntity.getProductDetails().get(i).getDiscount()!=null)&& (poEntity.getProductDetails().get(i).getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				
				percAmt=prodPrice-(prodPrice*poEntity.getProductDetails().get(i).getDiscount()/100);
				percAmt=percAmt-(poEntity.getProductDetails().get(i).getDiscountAmt());
				percAmt=percAmt*poEntity.getProductDetails().get(i).getProductQuantity();
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					if(poEntity.getProductDetails().get(i).getDiscount()!=null && poEntity.getProductDetails().get(i).getDiscount()!=0){
						System.out.println("inside getPercentageDiscount oneof the null condition");
						percAmt=prodPrice-(prodPrice*poEntity.getProductDetails().get(i).getDiscount()/100);
						
					}
					else 
					{
						System.out.println("inside getDiscountAmt oneof the null condition");
						percAmt=prodPrice-(poEntity.getProductDetails().get(i).getDiscountAmt());
					}
					percAmt=percAmt*poEntity.getProductDetails().get(i).getProductQuantity();
			}
			
			
			salesOrder.setTotalAmount(percAmt);
			
			if(percentage!=0){
				baseBillingAmount=(percAmt*percentage)/100;
			}
			System.out.println("Base Billing Amount Through Payment Terms"+baseBillingAmount);
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			/**
			 * Date : 05-08-2017 By ANIL
			 */
			salesOrder.setBasePaymentAmount(baseBillingAmount);
			salesOrder.setIndexVal(i+1);
			/////////////////////////////////////////////////////////////////////////////////
			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
	}



	public double removeTaxAmt(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			/**
			 * Date : 03-07-2017 By vijay 
			 * Checking GST printable name 
			 */
			if(entity.getServiceTax().getTaxPrintName()!=null && ! entity.getServiceTax().getTaxPrintName().equals("")
					   && entity.getVatTax().getTaxPrintName()!=null && ! entity.getVatTax().getTaxPrintName().equals(""))
			{
				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
			}
			else
			{
				double removeServiceTax=(entity.getPrice()/(1+service/100));
				retrServ=(removeServiceTax/(1+vat/100));
				retrServ=entity.getPrice()-retrServ;
//				double taxPerc=service+vat;
//				retrServ=entity.getPrice()/(1+taxPerc/100);
//				retrServ=entity.getPrice()-retrServ;
				
			}
		}
		tax=retrVat+retrServ;
		System.out.println("Shared Tax Return"+tax);
		return tax;
	}
	
	private void Createinvoice(PurchaseOrder poEntity,BillingDocument billingDocEntity) {
		System.out.println("GET STATUS"+billingDocEntity.getStatus());
		System.out.println("========"+billingDocEntity.getContractCount());
		
		/** Date 25.6.2018 added by komal for vendor invoice**/
		//final Invoice inventity = new ();
		final VendorInvoice inventity = new VendorInvoice();

		PersonInfo pinfo = new PersonInfo();
		if (billingDocEntity.getPersonInfo().getCount()!=0) {
			pinfo.setCount(billingDocEntity.getPersonInfo().getCount());
		}
		if (billingDocEntity.getPersonInfo().getFullName() != null) {
			pinfo.setFullName(billingDocEntity.getPersonInfo().getFullName());
		}
		if (billingDocEntity.getPersonInfo().getCellNumber() != null) {
			pinfo.setCellNumber(billingDocEntity.getPersonInfo().getCellNumber());
		}
		if (billingDocEntity.getPersonInfo().getPocName()!= null) {
			pinfo.setPocName(billingDocEntity.getPersonInfo().getPocName());
		}

		if (billingDocEntity.getGrossValue() != 0) {
			inventity.setGrossValue(billingDocEntity.getGrossValue());
		}


		if (pinfo != null) {
			inventity.setPersonInfo(pinfo);
		}

		if (billingDocEntity.getContractCount()!=0)
			inventity.setContractCount(billingDocEntity.getContractCount());
		if (billingDocEntity.getContractStartDate() != null) {
			inventity.setContractStartDate(billingDocEntity.getContractStartDate());
		}
		if (billingDocEntity.getContractEndDate() != null) {
			inventity.setContractEndDate(billingDocEntity.getContractEndDate());
		}
		if (billingDocEntity.getTotalBillingAmount()!=null)
			inventity.setTotalSalesAmount(Math.round(billingDocEntity.getTotalSalesAmount()));
		
		
		inventity.setArrayBillingDocument(billingDocEntity.getArrayBillingDocument());
		if (billingDocEntity.getTotalBillingAmount()!=null)
			inventity.setTotalBillingAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
		
		if (billingDocEntity.getTotalBillingAmount()!=null)
			inventity.setNetPayable(Math.round(billingDocEntity.getTotalBillingAmount()));
		
		inventity.setDiscount(0.0);
		
		if (billingDocEntity.getInvoiceDate() != null)
			inventity.setInvoiceDate(billingDocEntity.getInvoiceDate());
		if (billingDocEntity.getPaymentDate() != null)
			inventity.setPaymentDate(billingDocEntity.getPaymentDate());
		if (billingDocEntity.getApproverName() != null)
			inventity.setApproverName(billingDocEntity.getApproverName());
		if (billingDocEntity.getEmployee() != null)
			inventity.setEmployee(billingDocEntity.getEmployee());
		if (billingDocEntity.getBranch()!= null)
			inventity.setBranch(billingDocEntity.getBranch());
		
		inventity.setOrderCreationDate(billingDocEntity.getOrderCreationDate());
		inventity.setCompanyId(billingDocEntity.getCompanyId());
		inventity.setInvoiceAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
		inventity.setInvoiceType(AppConstants.CREATETAXINVOICE);
		inventity.setPaymentMethod(billingDocEntity.getPaymentMethod());
		inventity.setAccountType(billingDocEntity.getAccountType());
		inventity.setTypeOfOrder(billingDocEntity.getTypeOfOrder());
		
		inventity.setStatus(Invoice.APPROVED);
		
		
		List<SalesOrderProductLineItem>productlist=billingDocEntity.getSalesOrderProducts();
		
		
		ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
		prodList.addAll(productlist);
		System.out.println();
		System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());

		List<ContractCharges>taxestable=billingDocEntity.getBillingTaxes();
		ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
		taxesList.addAll(taxestable);
		System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
		
		List<ContractCharges>otherChargesable=billingDocEntity.getBillingOtherCharges();
		ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
		otherchargesList.addAll(otherChargesable);
		System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
		inventity.setSalesOrderProductFromBilling(prodList);
		/**Date 13-7-2020 by Amol**/
		inventity.setSalesOrderProducts(prodList);
		
		
		
		
		List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
		
		BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
		
		double payableamt=0;
		payableamt = calculateTotalBillAmt(billingDocEntity.getSalesOrderProducts()) + calculateTotalTaxes(billingDocEntity.getBillingTaxes()) + calculateTotalBillingCharges(billingDocEntity.getBillingOtherCharges());
		payableamt=Math.round(payableamt);
		System.out.println("Payable amt =="+payableamt);
		
		if(billingDocEntity.getContractCount()!=null){
			billingDocDetails.setOrderId(billingDocEntity.getContractCount());
		}
		
		if(billingDocEntity.getCount()!=0)
			billingDocDetails.setBillId(billingDocEntity.getCount());
		if(billingDocEntity.getBillingDate()!=null)
			billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
		if(billingDocEntity.getBillAmount()!=null)
			billingDocDetails.setBillAmount(payableamt);
		if(billingDocEntity.getStatus()!=null)
			billingDocDetails.setBillstatus(billingDocEntity.getStatus());
		System.out.println("Billing IDDDDD===="+billingDocDetails.getBillId());
		billtablelis.add(billingDocDetails);
		
		
		ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
		billtablearr.addAll(billtablelis);
		inventity.setArrayBillingDocument(billtablelis);
		
		
		
		
		
		inventity.setBillingTaxes(taxesList);
		inventity.setBillingOtherCharges(otherchargesList);
		
		inventity.setCustomerBranch(billingDocEntity.getCustomerBranch());

		inventity.setAccountType(billingDocEntity.getAccountType());

		inventity.setArrPayTerms(billingDocEntity.getArrPayTerms());

		
//		if(billingDocEntity.getNumberRange()!=null)
//			inventity.setNumberRange(billingDocEntity.getNumberRange());
		
		/** Date 14-09-2017 added by vijay as per new change *********/
		inventity.setTotalAmtExcludingTax(billingDocEntity.getTotalAmount());
		inventity.setFinalTotalAmt(billingDocEntity.getFinalTotalAmt());
		inventity.setTotalAmtIncludingTax(billingDocEntity.getTotalAmtIncludingTax());
		inventity.setTotalBillingAmount(billingDocEntity.getGrandTotalAmount()); 
		/******* ends here *******************************/
		
		
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(inventity);
		
		System.out.println("Here Invoice created Successfully");
		
		System.out.println("Invoice idd===="+inventity.getCount());
		
		billingDocEntity.setStatus(BillingDocument.BILLINGINVOICED);
		billingDocEntity.setInvoiceCount(inventity.getCount());
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(billingDocEntity);
		
		System.out.println("Here billing status updated");

		
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", inventity.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGINV).filter("companyId", inventity.getCompanyId()).filter("configStatus", true).first().now();
		}
		int flag =0;
		
		if(processConfig!=null){
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					flag = 1;
				}
			}
		}
		logger.log(Level.SEVERE,"Invoice before Call AI");
		if(flag == 1&&processConfig!=null){
			logger.log(Level.SEVERE,"Invoice before Call AI 1");
			InvoiceAccountIngInterface(inventity);
		}
		
		/**
		 * Ends here
		 */
		
		createpayementdocument(inventity,poEntity.getPaymentPaid(),poEntity.getBalancePayment());
			
		
  }
	private void createpayementdocument(Invoice inventity, double paidAmount, double balancePayment) {

		System.out.println("PAYMNET DOCUmnet");
		System.out.println("paid payment ===="+paidAmount);
		System.out.println("Balance ======"+balancePayment);
		
		if(balancePayment>0){
			
			CustomerPayment paydetails=new CustomerPayment();
			paydetails.setPersonInfo(inventity.getPersonInfo());
			paydetails.setContractCount(inventity.getContractCount());
			if(inventity.getContractStartDate()!=null){
				paydetails.setContractStartDate(inventity.getContractStartDate());
			}
			if(inventity.getContractEndDate()!=null){
				paydetails.setContractEndDate(inventity.getContractEndDate());
			}
			paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
			paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
			paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
			paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
			paydetails.setInvoiceCount(inventity.getCount());
			paydetails.setInvoiceDate(inventity.getInvoiceDate());
			paydetails.setInvoiceType(inventity.getInvoiceType());
			paydetails.setPaymentDate(inventity.getPaymentDate());
			
			 
			paydetails.setPaymentAmt((int)balancePayment);
			
			paydetails.setPaymentMethod(inventity.getPaymentMethod());
			paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
			paydetails.setStatus("Created");
			paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
			paydetails.setBranch(inventity.getBranch());
			paydetails.setAccountType(inventity.getAccountType());
			paydetails.setEmployee(inventity.getEmployee());
			paydetails.setCompanyId(inventity.getCompanyId());
			paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
			paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
			
			paydetails.setPaymentReceived(0);
			
			
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(paydetails);
			
			System.out.println("Payment created successfully");
		}
		if(paidAmount>0){
			
			CustomerPayment paydetails=new CustomerPayment();
			paydetails.setPersonInfo(inventity.getPersonInfo());
			paydetails.setContractCount(inventity.getContractCount());
			if(inventity.getContractStartDate()!=null){
				paydetails.setContractStartDate(inventity.getContractStartDate());
			}
			if(inventity.getContractEndDate()!=null){
				paydetails.setContractEndDate(inventity.getContractEndDate());
			}
			paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
			paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
			paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
			paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
			paydetails.setInvoiceCount(inventity.getCount());
			paydetails.setInvoiceDate(inventity.getInvoiceDate());
			paydetails.setInvoiceType(inventity.getInvoiceType());
			paydetails.setPaymentDate(inventity.getPaymentDate());
			
			 
			int payAmt=(int)(paidAmount);
			paydetails.setPaymentAmt(payAmt);
			
			paydetails.setPaymentMethod(inventity.getPaymentMethod());
			paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
			paydetails.setStatus("Closed");
			paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
			paydetails.setBranch(inventity.getBranch());
			paydetails.setAccountType(inventity.getAccountType());
			paydetails.setEmployee(inventity.getEmployee());
			paydetails.setCompanyId(inventity.getCompanyId());
			paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
			paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
			
			paydetails.setPaymentReceived(payAmt);
			
			
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(paydetails);
			
			System.out.println("Payment created successfully");
		}
		
	
		
	
		
	}
	
	
	/**
	 * Invoice Accounting Interface
	 */
	
//	private void InvoiceAccountIngInterface(Invoice inventity) {
//		// TODO Auto-generated method stub
//		for(int  i = 0;i<inventity.getSalesOrderProducts().size();i++){
//			
//			Customer cust=null;
//			Vendor vendor=null;
//			Employee emp=null;
//			
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
//				cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//			}
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
//				cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//			}				
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//				vendor=ofy().load().type(Vendor.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//			}
//			
//			
//			Date docCreationDate=null;
//			 int prodId=inventity.getSalesOrderProducts().get(i).getProdId();
//			 String productCode = inventity.getSalesOrderProducts().get(i).getProdCode();
//			 String productName = inventity.getSalesOrderProducts().get(i).getProdName();
//			 double productQuantity = inventity.getSalesOrderProducts().get(i).getQuantity();
//				 double productprice = (inventity.getSalesOrderProducts().get(i).getPrice());
//			 String unitofmeasurement = inventity.getSalesOrderProducts().get(i).getUnitOfMeasurement();
//			 //  rohan commented this code 
////			 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
//			
//			 //   rohan added new code for calculations 
//			 double totalAmount = inventity.getSalesOrderProducts().get(i).getBaseBillingAmount();
//			 
//			 double calculatedamt = 0;
//			 double calculetedservice =0;
//			 	
//			 int orderDuration = inventity.getSalesOrderProducts().get(i).getOrderDuration();
//			 int orderServices = inventity.getSalesOrderProducts().get(i).getOrderServices();
//			 
//			 double cformamtService = 0;
//			 double serviceTax=0 ;
//			 double cformPercent=0;
//			 double cformAmount=0;					
//			 double vatPercent=0;
//			 String cform = "";
//			 String refDocType="";
//			 double cformP=0;
//			 String accountType="";
//			 Date contractStartDate=null;
//			 Date contractEndDate=null;
//			 
//			 int vatglaccno=0;
//			 int serglaccno=0;
//			 int cformglaccno=0;
//			 
//			 int personInfoId = 0;
//			 String personInfoName="";
//			 long personInfoCell = 0;
//			 int vendorInfoId = 0;
//			 String vendorInfoName="";
//			 long vendorInfoCell=0;
//			 int empInfoId;
//			 String empInfoName="";
//			 long empinfoCell;
//			 
//			 String addrLine1="";
//			 String addrLocality="";
//			 String addrLandmark="";
//			 String addrCountry="";
//			 String addrState="";
//			 String addrCity="";
//			 long addrPin=0;
///************************************for document type********************************************/			 
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
//				docCreationDate=inventity.getOrderCreationDate();
//				refDocType=AppConstants.REFDOCSO;
//				personInfoId=inventity.getPersonInfo().getCount();
//				personInfoName=inventity.getPersonInfo().getFullName();
//				personInfoCell=inventity.getPersonInfo().getCellNumber();
//				
//				 addrLine1=cust.getAdress().getAddrLine1();
//				 addrLocality=cust.getAdress().getLocality();
//				 addrLandmark=cust.getAdress().getLandmark();
//				 addrCountry=cust.getAdress().getCountry();
//				 addrState=cust.getAdress().getState();
//				 addrCity=cust.getAdress().getCity();
//				 addrPin=cust.getAdress().getPin();
//				
//				if(inventity.getOrderCformStatus()!=null){
//					cform=inventity.getOrderCformStatus();
//					cformP=inventity.getOrderCformPercent();
//				}
//			}
//			
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
//				docCreationDate=inventity.getOrderCreationDate();
//				refDocType=AppConstants.REFDOCCONTRACT;
//				personInfoId=inventity.getPersonInfo().getCount();
//				personInfoName=inventity.getPersonInfo().getFullName();
//				personInfoCell=inventity.getPersonInfo().getCellNumber();
//			
//				addrLine1=cust.getAdress().getAddrLine1();
//				addrLocality=cust.getAdress().getLocality();
//				addrLandmark=cust.getAdress().getLandmark();
//				addrCountry=cust.getAdress().getCountry();
//				addrState=cust.getAdress().getState();
//				addrCity=cust.getAdress().getCity();
//				addrPin=cust.getAdress().getPin();
//				contractStartDate=inventity.getContractStartDate();
//				contractEndDate=inventity.getContractEndDate();
//			}
//			
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//				docCreationDate=inventity.getOrderCreationDate();
//				refDocType=AppConstants.REFDOCPO;
//				
//				vendorInfoId=inventity.getPersonInfo().getCount();
//				vendorInfoName=inventity.getPersonInfo().getFullName();
//				vendorInfoCell=inventity.getCellNumber();
//				
//				addrLine1=vendor.getPrimaryAddress().getAddrLine1();
//				addrLocality=vendor.getPrimaryAddress().getLocality();
//				addrLandmark=vendor.getPrimaryAddress().getLandmark();
//				addrCountry=vendor.getPrimaryAddress().getCountry();
//				addrState=vendor.getPrimaryAddress().getState();
//				addrCity=vendor.getPrimaryAddress().getCity();
//				addrPin=vendor.getPrimaryAddress().getPin();
//				
//				if(inventity.getOrderCformStatus()!=null){
//					cform=inventity.getOrderCformStatus();
//					cformP=inventity.getOrderCformPercent();
//				}
//			}
//			
//		/*********************************for cform*************************************************/	
//			
//			 if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.YES)){
//				 cform=AppConstants.YES;
//				 cformPercent=inventity.getOrderCformPercent();
//				 cformAmount=inventity.getOrderCformPercent()*totalAmount/100;
//				 cformamtService=inventity.getOrderCformPercent()*totalAmount/100;
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//					 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//					 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//				 }
//				 
//				 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//				 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//				 cformglaccno=serglAcc.getGlAccountNo();
//			 }
//			 
//			 else if(inventity.getOrderCformStatus()!= null&inventity.getOrderCformStatus().trim().equals(AppConstants.NO)){
//				 cform=AppConstants.NO;
//				 cformPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//				 cformAmount=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//				 cformamtService=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//					 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//					 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//				 }
//				 
//				 
//				 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//				 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//				 vatglaccno=vatglAcc.getGlAccountNo();
//			 }
//			 
//			 
//			 	else{//if the customer is of same state
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
//				 {
//					 vatPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//					 calculatedamt=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
//				 }
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
//				 {
//					 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//					 calculetedservice=(calculatedamt+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
//				 }
//				 }
//			 
//			 
//			 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
//			 {
//				 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
//				 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//				 serglaccno=serglAcc.getGlAccountNo();
//			 }
//
//			 /*****************************************************************************************/
//			
//			 //  old code 
////			 double netPayable = getTotalSalesAmount();
//			 //  new code by rohan as there is calculations error
//			 double netPayable = inventity.getNetPayable();
//			 
//			 
//			 String[] oc = new String[12];
//			 double[] oca = new double[12];
//			 
//			 
////			 if(this.getBillingOtherCharges().size()!=0){
//				 for(int k = 0; k < inventity.getBillingOtherCharges().size();k++){
//					 oc[k] = inventity.getBillingOtherCharges().get(k).getTaxChargeName();
//					 oca[k] = inventity.getBillingOtherCharges().get(k).getPayableAmt();
//				 }
////			 }
//			 /**********************************End************************************************/
//			 
//			 logger.log(Level.SEVERE,"Before Invoice Accounting Interface Call");
//			 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
//					 									inventity.getEmployee(),//accountingInterfaceCreatedBy
//					 									inventity.getStatus(),
//													AppConstants.STATUS ,//Status
//													"",//Remark
//													"Accounts",//Module
//													"Invoice",//documentType
//													inventity.getCount(),//documentID
//														" ", //documentTitle
//														inventity.getInvoiceDate(),//documentDate
//														inventity.getAccountType().trim()+"-Invoice",//documentGl
//														inventity.getContractCount()+"",//referenceDocumentNumber1
//														docCreationDate,//referenceDocumentDate1
//														refDocType,//referenceDocumentType
//														"",//referenceDoc.no.2
//														null,//reference doc.date.2
//														"",//reference doc type no2.
//														inventity.getAccountType(),//accountType	
//														personInfoId,//custID	
//														personInfoName,//custName	
//														personInfoCell,//custCell
//														vendorInfoId,//venID	
//														vendorInfoName,//venName	
//														vendorInfoCell,//venCell
//														0,//emplID
//														" ",//emplName
//														inventity.getBranch(),//branch
//														inventity.getEmployee(),//personResponsible
//														"",//requestdBy
//														inventity.getApproverName(),//AproverName
//														inventity.getPaymentMethod(),//PaymentMethod
//														inventity.getPaymentDate(),//PaymentDate
//														"",//Cheq no.
//														null,//cheq date.
//														null,//Bank Name
//														null,//bankAccount
//														0,//transferReferenceNumber
//														null,//transactionDate
//														contractStartDate,  //contract start date
//														contractEndDate,  // contract end date
//														prodId,//prodID
//														productCode,//prodCode
//														productName, //prodNAme
//														productQuantity,//prodQuant
//														inventity.getContractStartDate(),//Prod DAte
//														orderDuration,//duration	
//														orderServices,//services
//														unitofmeasurement,//UOM
//														productprice, //prod Price
//														vatPercent,//vat%
//														calculatedamt,//vatamt
//														vatglaccno,//VATglAccount
//														serviceTax,//serviceTaxPercent
//														calculetedservice,//serviceTaxAmount
//														serglaccno,//serviceTaxGLaccount
//														cform,//cform
//														cformPercent,//cform%
//														cformAmount,//cformamt
//														cformglaccno, //cformglacc
//														totalAmount,//totalamt
//														netPayable,//netpay
//														"",//Contract Category
//														0,			//amountRecieved
//														0.0,     // Base Amount taken in payment for tds calculation by rohan 
//														0,   //  tds percentage by rohan 
//														0,   //  tds amount by rohan 
//														oc[0],
//														oca[0],
//														oc[1],
//														oca[1],
//														oc[2],
//														oca[2],
//														oc[3],
//														oca[3],
//														oc[4],
//														oca[4],
//														oc[5],
//														oca[5],
//														oc[6],
//														oca[6],
//														oc[7],
//														oca[7],
//														oc[8],
//														oca[8],
//														oc[9],
//														oca[9],
//														oc[10],
//														oca[10],
//														oc[11],
//														oca[11],
//														addrLine1,
//														addrLocality,
//														addrLandmark,
//														addrCountry,
//														addrState,
//														addrCity,
//														addrPin,
//														inventity.getCompanyId(),
//														inventity.getBillingPeroidFromDate(),//  billing from date (rohan)
//														inventity.getBillingPeroidToDate(),//  billing to date (rohan)
//														"", //Warehouse
//														"",				//warehouseCode
//														"",				//ProductRefId
//														"",				//Direction
//														"",				//sourceSystem
//														""				//Destination Syste
//														);
//			 
//		
//		}
//		
//}
	
/**
 * ends here	
 */
	private void InvoiceAccountIngInterface(Invoice inventity) {
//		// TODO Auto-generated method stub

		/** date 23.6.2018 added by komal for new accountion interface changes**/
		if(inventity.getCount()!=0&&inventity.getStatus().equals(Invoice.APPROVED)){
	
			
			for(int  i = 0;i<inventity.getSalesOrderProducts().size();i++){
				
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					vendor=ofy().load().type(Vendor.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}
				
				
				Date docCreationDate=null;
				 int prodId=inventity.getSalesOrderProducts().get(i).getProdId();
				 String productCode = inventity.getSalesOrderProducts().get(i).getProdCode();
				 String productName = inventity.getSalesOrderProducts().get(i).getProdName();
				 double productQuantity = inventity.getSalesOrderProducts().get(i).getQuantity();
 				 double productprice = (inventity.getSalesOrderProducts().get(i).getPrice());
				 String unitofmeasurement = inventity.getSalesOrderProducts().get(i).getUnitOfMeasurement();
				 //  rohan commented this code 
//				 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
				
				 //   rohan added new code for calculations 
				 double totalAmount = inventity.getSalesOrderProducts().get(i).getBaseBillingAmount();
				 
				 double calculatedamt = 0;
				 double calculetedservice =0;
				 	
				 int orderDuration = inventity.getSalesOrderProducts().get(i).getOrderDuration();
				 int orderServices = inventity.getSalesOrderProducts().get(i).getOrderServices();
				 
				 double cformamtService = 0;
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;					
				 double vatPercent=0;
				 String cform = "";
				 String refDocType="";
				 double cformP=0;
				 String accountType="";
				 Date contractStartDate=null;
				 Date contractEndDate=null;
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
				 int personInfoId = 0;
				 String personInfoName="";
				 long personInfoCell = 0;
				 int vendorInfoId = 0;
				 String vendorInfoName="";
				 long vendorInfoCell=0;
				 int empInfoId;
				 String empInfoName="";
				 long empinfoCell;
				 
				 String addrLine1="";
				 String addrLocality="";
				 String addrLandmark="";
				 String addrCountry="";
				 String addrState="";
				 String addrCity="";
				 long addrPin=0;
				 
				 /** date 29/3/2018 added by komal for tax name **/
				 String taxvatName="";
				 String serTaxName = "";
				 String glAccountName1 = "";
				 String glAccountName2 = ""; 
				 /** date 29.5.2018 added by komal for billing address **/
				 String saddrLine1 ="";
				 String addrLine2 ="";
				 String saddrLine2 ="";
				 String saddrLocality = "";
				 String saddrLandmark = "";
				 String saddrCountry = "";
				 String saddrState="";
				 String saddrCity="";
				 long saddrPin = 0;
	/************************************for document type********************************************/			 
				 
				 
				 
				 
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCSO;
					personInfoId=inventity.getPersonInfo().getCount();
					personInfoName=inventity.getPersonInfo().getFullName();
					personInfoCell=inventity.getPersonInfo().getCellNumber();
					
					 addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
					 
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
					
					if(inventity.getOrderCformStatus()!=null){
						cform=inventity.getOrderCformStatus();
						cformP=inventity.getOrderCformPercent();
					}
				}
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCCONTRACT;
					personInfoId=inventity.getPersonInfo().getCount();
					personInfoName=inventity.getPersonInfo().getFullName();
					personInfoCell=inventity.getPersonInfo().getCellNumber();
				
					addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					addrLocality=cust.getAdress().getLocality();
					addrLandmark=cust.getAdress().getLandmark();
					addrCountry=cust.getAdress().getCountry();
					addrState=cust.getAdress().getState();
					addrCity=cust.getAdress().getCity();
					addrPin=cust.getAdress().getPin();
					contractStartDate=inventity.getContractStartDate();
					contractEndDate=inventity.getContractEndDate();
					
					 addrPin=cust.getAdress().getPin();
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
				}
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.QUICKPURCHASEORDER;
					
					vendorInfoId=inventity.getPersonInfo().getCount();
					vendorInfoName=inventity.getPersonInfo().getFullName();
					vendorInfoCell=inventity.getCellNumber();
					
					addrLine1=vendor.getPrimaryAddress().getAddrLine1();
					/** date 29.5.2018 added by komal for billing address **/
					addrLine2=vendor.getPrimaryAddress().getAddrLine2();
					addrLocality=vendor.getPrimaryAddress().getLocality();
					addrLandmark=vendor.getPrimaryAddress().getLandmark();
					addrCountry=vendor.getPrimaryAddress().getCountry();
					addrState=vendor.getPrimaryAddress().getState();
					addrCity=vendor.getPrimaryAddress().getCity();
					addrPin=vendor.getPrimaryAddress().getPin();
					
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=vendor.getSecondaryAddress().getAddrLine1();
					saddrLine2=vendor.getSecondaryAddress().getAddrLine2();
					saddrLocality=vendor.getSecondaryAddress().getLocality();
					saddrLandmark=vendor.getSecondaryAddress().getLandmark();
					saddrCountry=vendor.getSecondaryAddress().getCountry();
					saddrState=vendor.getSecondaryAddress().getState();
					saddrCity=vendor.getSecondaryAddress().getCity();
					saddrPin=vendor.getSecondaryAddress().getPin();
					
					if(inventity.getOrderCformStatus()!=null){
						cform=inventity.getOrderCformStatus();
						cformP=inventity.getOrderCformPercent();
					}
				}
				
				
				
			/*********************************for cform*************************************************/	
				
				 if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=inventity.getOrderCformPercent();
					 cformAmount=inventity.getOrderCformPercent()*totalAmount/100;
					 cformamtService=inventity.getOrderCformPercent()*totalAmount/100;
					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//					 cformglaccno=serglAcc.getGlAccountNo();
					 cformglaccno=0;
				 }
				 
				 else if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
					 cformAmount=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 cformamtService=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 
					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
					 
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
					 vatglaccno=0;
				 }
				 
				 
				 
				 	else{//if the customer is of same state
					 
						 if(inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
						 {
							 vatPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
							 calculatedamt=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
							 //date 29.3.2018 added by komal
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargeName",inventity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName()).first().now();
							 if(vattaxDtls != null)	
							 glAccountName2 = vattaxDtls.getGlAccountName();
//							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//							 vatglaccno=vatglAcc.getGlAccountNo();
							 vatglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							// String taxvatName="";
									if(inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName()!=null && !inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName().equals("")){
										taxvatName =inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName();
									}
									else{
										taxvatName =inventity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName();
									}
						 }
						 
						 
						
						 				 
						 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
						 {
							 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
							 calculetedservice=(totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
							//date 29.3.2018 added by komal 
							 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargeName",inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName()).first().now();
							 if(sertaxDtls != null)
							 glAccountName1 = sertaxDtls.getGlAccountName();
//							 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//							 serglaccno=serglAcc.getGlAccountNo();
							 serglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							 /** date 03/04/2018 changed by komal  vat tax to service tax**/
							 //String serTaxName="";
								if(inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName()!=null && !inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName().equals("")){
									serTaxName =inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName();
								}
								else{
									serTaxName =inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName();
								}
						 }
				 	}
				 
				 
				 
				

				 /*****************************************************************************************/
				
				 //  old code 
//				 double netPayable = getTotalSalesAmount();
				 //  new code by rohan as there is calculations error
				 double netPayable = inventity.getNetPayable();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
//				 if(this.getBillingOtherCharges().size()!=0){
					 for(int k = 0; k < inventity.getBillingOtherCharges().size();k++){
						 oc[k] = inventity.getBillingOtherCharges().get(k).getTaxChargeName();
						 oca[k] = inventity.getBillingOtherCharges().get(k).getPayableAmt();
					 }
//				 }
					 
				/**
				 * Date : 01-11-2017 BY ANIL
				 * Setting invoice comment to accounting interface remark	 
				 */
				String invoiceComment="";
				if(inventity.getComment()!=null){
					invoiceComment=inventity.getComment();
				}
				Logger logger = Logger.getLogger("Name of logger");
				logger.log(Level.SEVERE,"Invoice Group"+inventity.getInvoiceGroup());
				/**
				 * End
				 */
				/** date 29/03/2018 added by  komal for new tally interface requirement**/
				String gstn = "";
				double discountAmount = inventity.getDiscountAmt();
				double indirectExpenses = inventity.getTotalOtherCharges();
				if(inventity.getGstinNumber()!=null){
					gstn = inventity.getGstinNumber();
				}else{
					if(inventity.getAccountType().equals("AR")){
						System.out.println("id ");
						try{
							Customer customer=ofy().load().type(Customer.class).filter("companyId", inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
							if(customer!=null){
								System.out.println("id222222222222222 ");
								if(customer.getArticleTypeDetails()!=null){  
									System.out.println("id 33333333333333333");
									for(ArticleType object:customer.getArticleTypeDetails()){
										System.out.println("id 4444444444444444");  
										if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
											System.out.println("id 555555555555555555555");
											inventity.setGstinNumber(object.getArticleTypeValue());
											break;
										}
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					gstn = inventity.getGstinNumber();
				}
		       String hsnNumber = "";
		       SuperProduct superProduct = ofy().load().type(SuperProduct.class).filter("companyId",inventity.getCompanyId()).filter("productCode", inventity.getSalesOrderProducts().get(i).getProdCode()).first().now();
		       if(superProduct.getHsnNumber()!=null){
		    	   hsnNumber = superProduct.getHsnNumber();
		       }
		     
		    	 personInfoName=inventity.getPersonInfo().getFullName(); 
		    	
		    	 /**
		    	  * end komal
		    	  */
					 
				 /**********************************End************************************************/
				 String numberRange="";
				 if(inventity.getNumberRange()!=null){
					 numberRange = inventity.getNumberRange();
				 }
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
						 								inventity.getEmployee(),//accountingInterfaceCreatedBy
						 								inventity.getStatus(),
														AppConstants.STATUS ,//Status
														invoiceComment,//Remark
														"Accounts",//Module
														"Invoice",//documentType
														inventity.getCount(),//documentID
															" ", //documentTitle
															inventity.getInvoiceDate(),//documentDate
															inventity.getAccountType().trim()+"-Invoice",//documentGl
															inventity.getContractCount()+"",//referenceDocumentNumber1
															docCreationDate,//referenceDocumentDate1
															refDocType,//referenceDocumentType
															inventity.getRefNumber(),//referenceDoc.no.2 Date 09-11-2017 this is sap customer ref num for NBHC
															null,//reference doc.date.2
															"",//reference doc type no2.
															inventity.getAccountType(),//accountType	
															personInfoId,//custID	
															personInfoName,//custName	
															personInfoCell,//custCell
															vendorInfoId,//venID	
															vendorInfoName,//venName	
															vendorInfoCell,//venCell
															0,//emplID
															" ",//emplName
															inventity.getBranch(),//branch
															inventity.getEmployee(),//personResponsible
															"",//requestdBy
															inventity.getApproverName(),//AproverName
															inventity.getPaymentMethod(),//PaymentMethod
															inventity.getPaymentDate(),//PaymentDate
															"",//Cheq no.
															null,//cheq date.
															null,//Bank Name
															null,//bankAccount
															0,//transferReferenceNumber
															null,//transactionDate
															contractStartDate,  //contract start date
															contractEndDate,  // contract end date
															prodId,//prodID
															productCode,//prodCode
															productName, //prodNAme
															productQuantity,//prodQuant
															inventity.getContractStartDate(),//Prod DAte
															orderDuration,//duration	
															orderServices,//services
															unitofmeasurement,//UOM
															productprice, //prod Price
															vatPercent,//vat%
															calculatedamt,//vatamt
															vatglaccno,//VATglAccount
															serviceTax,//serviceTaxPercent
															calculetedservice,//serviceTaxAmount
															serglaccno,//serviceTaxGLaccount
															cform,//cform
															cformPercent,//cform%
															cformAmount,//cformamt
															cformglaccno, //cformglacc
															totalAmount,//totalamt
															netPayable,//netpay
															inventity.getInvoiceGroup(),//Contract Category//27 fEB PReviously it was contract category now it is from invoice group
															0,			//amountRecieved
															0.0,     // Base Amount taken in payment for tds calculation by rohan 
															0,   //  tds percentage by rohan 
															0,   //  tds amount by rohan 
															oc[0],
															oca[0],
															oc[1],
															oca[1],
															oc[2],
															oca[2],
															oc[3],
															oca[3],
															oc[4],
															oca[4],
															oc[5],
															oca[5],
															oc[6],
															oca[6],
															oc[7],
															oca[7],
															oc[8],
															oca[8],
															oc[9],
															oca[9],
															oc[10],
															oca[10],
															oc[11],
															oca[11],
															addrLine1,
															addrLine2,
															addrLocality,
															addrLandmark,
															addrCountry,
															addrState,
															addrCity,
															addrPin,
															inventity.getCompanyId(),
															inventity.getBillingPeroidFromDate(),//  billing from date (rohan)
															inventity.getBillingPeroidToDate(),//  billing to date (rohan)
															"", //Warehouse
															"",				//warehouseCode
															"",				//ProductRefId
															"",				//Direction
															"",				//sourceSystem
															""	,			//Destination Syste
															hsnNumber, // hsc/Sac
															gstn, // gstn
															glAccountName1, // goupname1 for tax1
															glAccountName2, // groupname2 for tax 2
															serTaxName, // tax1 name
															taxvatName, // tax2 name
															discountAmount, // discount amount
															indirectExpenses, // indirect expenses
															saddrLine1,
															saddrLine2, 
															saddrLocality, 
															saddrLandmark,
															saddrCountry,
															saddrState,
															saddrCity, 
															saddrPin,
															null,null,null,null,
															null,null,null,null,null,null,numberRange
															);
				 
			
			}
			
		}
	
		
}
	
/**
 * ends here	
 */

}
