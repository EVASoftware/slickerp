package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class InvoiceMapOnServiceCompletionTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -147165310258154337L;
	Logger logger=Logger.getLogger("Name Of logger");
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
			
				String	jsonContSerDtSet = req.getParameter("jsonContSerDtSet").trim();
				String	jsonContSerSet = req.getParameter("jsonContSerSet").trim();
				String	jsonContractIdSet = req.getParameter("jsonContractIdSet");
				Long companyId = Long.parseLong(req.getParameter("companyId"));
		HashSet<Integer> contractIdSet = new HashSet<Integer>();
		HashMap<Integer, ArrayList<Integer>> contSerSet = new HashMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, ArrayList<Service>> contSerDtSet = new HashMap<Integer, ArrayList<Service>>();
		try {
				
				ArrayList<ServiceProductGroupList> serProList = new ArrayList<ServiceProductGroupList>();
				Gson gson = new GsonBuilder().create();
							
							JSONObject jsonContSerSetObj = new JSONObject(jsonContSerSet);
							
							JSONObject jsonContSerDtSetObj = new JSONObject(jsonContSerDtSet.trim());
							
							ProductGroupList proDetail = new ProductGroupList();
							
							 contSerSet  = gson.fromJson(jsonContSerSetObj.toString(), new TypeToken<HashMap<Integer, ArrayList<Integer>>>() {}.getType());
							logger.log(Level.SEVERE,"get pro list --" + contSerSet.size());
							
							 contSerDtSet  = gson.fromJson(jsonContSerDtSetObj.toString(), new TypeToken<HashMap<Integer, ArrayList<Service>>>() {}.getType());
						
							logger.log(Level.SEVERE,"get pro list --" + contSerDtSet.size());
							contractIdSet   = gson.fromJson(jsonContractIdSet.toString(), new TypeToken<HashSet<Integer>>() {}.getType());
							
							logger.log(Level.SEVERE,"get pro list --" + contractIdSet.size());
							
							
							
							
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(contractIdSet.size()>0 && contSerDtSet.size()>0){
				ArrayList<Integer> contractCount = new ArrayList<Integer>();
				contractCount.addAll(contractIdSet);
				List<Invoice> invoiceList = ofy().load().type(Invoice.class).filter("companyId", companyId)
						.filter("invoiceType",AppConstants.CREATETAXINVOICE)
						.filter("contractCount IN" , contractCount).list();
				
				if(invoiceList== null ){
					invoiceList = ofy().load().type(Invoice.class).filter("companyId", companyId)
							.filter("invoiceType",AppConstants.CREATEPROFORMAINVOICE)
							.filter("contractCount IN" , contractCount).list();
					
					return;
				}
				
					for(Integer contractId : contractCount ){
						Integer invId1  = new Integer(0);
						Integer invId2  = new Integer(0);
						
						Integer billId1  = new Integer(0);
						Integer billId2  = new Integer(0);
						
						Date invDate = new Date();
						Date invDate2 = null;
						ArrayList<Integer> invList = new ArrayList<Integer>();
						HashMap<Integer, Invoice> invDt = new HashMap<Integer, Invoice>();
						HashMap<Integer, HashSet<Integer>> billList = new HashMap<Integer, HashSet<Integer>>();
						
						
					/*	 Comparator<BillingDocument> invoiceDateComparator2 = new Comparator<BillingDocument>() {
								public int compare(BillingDocument s1, BillingDocument s2) {
								
								Date date1 = s1.getBillingDate();
								Date date2 = s2.getBillingDate();
								
								//ascending order
								return date1.compareTo(date2);
								}
								};
								Collections.sort(billList, invoiceDateComparator2);*/
						
						for(Invoice inv : invoiceList){
							logger.log(Level.SEVERE,"get count --" + inv.getContractCount()  +" -- contract count  "+ contractId);
							boolean flag = (int)inv.getContractCount()  ==(int) contractId;
							logger.log(Level.SEVERE," condition -- "+ flag);
							if((int)inv.getContractCount()  ==(int) contractId){
								invList.add(inv.getCount());
								invDt.put(inv.getCount(),inv);
								invId1 = new Integer(inv.getCount());
								invDate = inv.getInvoiceDate();
								if(invDate2 == null){
									invDate2 = invDate;
									invId2 = new Integer(invId1);
								}
								/*if(invId1 >= invId2){
									invId2 = new Integer(invId1);
								}*/
								
								if(invDate.after(invDate2)){
									invDate2 = invDate;
									invId2 = new Integer(invId1);
								}
									for(BillingDocumentDetails billDt : inv.getArrayBillingDocument()){
										
										if(billDt.getOrderId() == contractId){
											if(billList.containsKey(inv.getCount())){
												billList.get(inv.getCount()).add(billDt.getBillId());
											}else{
												HashSet<Integer> billCount = new HashSet<Integer>();
												billCount.add(billDt.getBillId());
												billList.put(inv.getCount(), billCount);
											}
											billId1 = new Integer(billDt.getBillId());
											if(billId2 == 0){
												billId2 = new Integer(billId1);
											}
											if(billId1 >= billId2){
												billId2 = new Integer(billId1);
											}
										}
								}
							}
						}
						
						if(invId2>0){
							logger.log(Level.SEVERE,"ger inv date -- " + invDate2);
							logger.log(Level.SEVERE,"ger inv id -- " + invId2);
							if(contSerDtSet.containsKey(contractId) && contSerDtSet.get(contractId).size()>0){
								
								List<Service> serList = contSerDtSet.get(contractId);
								
								serList = ofy().load().type(Service.class).filter("companyId", companyId)
										.filter("contractCount", contractId).filter("count IN", contSerSet.get(contractId)).list();
								Invoice inDt = invDt.get(invId2);
								HashSet<Integer> serInvList =new HashSet<Integer>();
								HashSet<Integer> serBillList =new HashSet<Integer>();
								BillingDocument billDt = new BillingDocument();
								if(billId2!=0){
									billDt =	ofy().load().type(BillingDocument.class).filter("companyId", companyId)
											.filter("count" , billId2).filter("contractCount", contractId).first().now();
								}
								
								
								if(inDt!=null){
									if(inDt.getServiceId() != null){
										
										serInvList.addAll(inDt.getServiceId());
									}
								}
								
								if(billDt!=null){
									if(billDt.getServiceId() != null){
										
										serBillList.addAll(billDt.getServiceId());
									}
								}
								ArrayList<SuperModel> supSerList = new ArrayList<SuperModel>();
								for(int i = 0;i<serList.size();i++){
									
									serList.get(i).setInvoiceId(invId2);
									serList.get(i).setBillingStatus(billDt.getStatus());
									serList.get(i).setBillingCount(billId2);
									serInvList.add(serList.get(i).getCount());
									serBillList.add(serList.get(i).getCount());
									supSerList.add(serList.get(i));
									
									Service serDt = new Service();
//									serDt = serList.get(i).Myclone();
									logger.log(Level.SEVERE,"get service bill updated -- " + serList.get(i).getInvoiceId());
						
//									if( serDt!=null){
										ofy().save().entity(serList.get(i)).now();
										
//									}
									
									logger.log(Level.SEVERE,"get service bill updated -- " + serList.get(i).getCount());
								}
								
								
								/*
								GenricServiceImpl impl=new GenricServiceImpl();
								if(supSerList!=null&&supSerList.size()!=0){
								    impl.save(supSerList);
								}*/
								if(inDt!=null){
									inDt.getServiceId().clear();
									inDt.getServiceId().addAll(serInvList);
									ofy().save().entity(inDt).now();
								}
								if(billDt != null) {
									billDt.getServiceId().clear();
									billDt.getServiceId().addAll(serBillList);
									ofy().save().entity(billDt).now();
								}
								
							}
						}
						
					}
				
			}
		}

}
