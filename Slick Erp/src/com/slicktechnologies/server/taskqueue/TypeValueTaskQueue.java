package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;

public class TypeValueTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8149512925893893912L;
	Logger logger = Logger.getLogger("Name Of logger");
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		logger.log(Level.SEVERE, "Welcome to Type name Task Queue");

		String typeNamewithdoller = req.getParameter("typeValuekey");

		logger.log(Level.SEVERE, "Type name Data === with $ ==  "	+ typeNamewithdoller);

		String[] typeNamewithoutdoller = typeNamewithdoller.split("[$]");


		Type types = new Type();
		types.setInternalType(Integer.parseInt(typeNamewithoutdoller[0]));
		types.setCategoryName(typeNamewithoutdoller[1]);
		types.setTypeName(typeNamewithoutdoller[2]);
		types.setTypeCode(typeNamewithoutdoller[3]);
		types.setCatCode(typeNamewithoutdoller[4]);
		types.setCompanyId(Long.parseLong(typeNamewithoutdoller[5]));
		types.setCount(Integer.parseInt(typeNamewithoutdoller[6]));
		types.setStatus(true);
		
	    ofy().save().entity(types);

		logger.log(Level.SEVERE, "Type Name saved successfully");
	}
	
	
}
