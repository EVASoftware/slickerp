package com.slicktechnologies.server.taskqueue;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.UpdateStock;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ProductInventoryListUpdateIndex extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2116611278561220743L;

	Logger logger = Logger.getLogger("Name Of logger");
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		logger.log(Level.SEVERE, "Welcome to Lead Data Task Queue");
		String companyIdString = req.getParameter("ProductTransactionDetaiilsValueKey");
		
		String[] moduleNamewithoutdoller = companyIdString.split("[$]");
		logger.log(Level.SEVERE, "companyIdString value  " +companyIdString);
		
		
		if(moduleNamewithoutdoller[0].trim().equals("UpdateIndex")){
			
			
			Long companyId=Long.parseLong(moduleNamewithoutdoller[1]);
			
			List<ProductInventoryTransaction> transactionList =new ArrayList<ProductInventoryTransaction>();
			
			transactionList = ofy().load().type(ProductInventoryTransaction.class).filter("companyId", companyId).list();
			
			System.out.println("transaction list COunt"+transactionList.size());
			ofy().save().entities(transactionList);
		}
		else if(moduleNamewithoutdoller[0].trim().equalsIgnoreCase("UpdateStock"))
		{
			/**
			 *    this is old code for stock update and Anil has written new code for stock update
			 *    so use new code for further stock updates
			 *    Date : 20/1/2017
			 *    Last parameter is branch name form min which is "" at the moment.  
			 */
			
			UpdateStock.setProductInventory(Integer.parseInt(moduleNamewithoutdoller[1]),Double.parseDouble(moduleNamewithoutdoller[2]),Integer.parseInt(moduleNamewithoutdoller[3]),new Date(),moduleNamewithoutdoller[5],moduleNamewithoutdoller[6],
					moduleNamewithoutdoller[7], moduleNamewithoutdoller[8], moduleNamewithoutdoller[9],AppConstants.ADD,Long.parseLong(moduleNamewithoutdoller[11]),"");
		}
		
		
	}
}
