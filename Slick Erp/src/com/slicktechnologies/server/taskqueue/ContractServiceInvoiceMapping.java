package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.dev.util.collect.HashMap;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class ContractServiceInvoiceMapping extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -285085106032948381L;

	Logger logger=Logger.getLogger("Name Of logger == ContractServiceInvoiceMapping");
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		final Integer contractCount = Integer.parseInt(req.getParameter("contract"));
		final Long companyId = Long.parseLong(req.getParameter("companyId"));
		final int totalservices = (int) Double.parseDouble(req.getParameter("totalservices"));
		try {
			Thread.sleep(1500);
			serviceInvoiceMapping(companyId,contractCount,totalservices);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void serviceInvoiceMapping(final Long  companyId,final Integer contractCount,final int totalservices){
		
		/**
		 * @author Anil,Date : 06-03-2019
		 * Clearing data from cache memory
		 */
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		 List<Service>  serviceList = new ArrayList<Service>();
		 
		 serviceList = ofy().load().type(Service.class).filter("contractCount",contractCount)
				 		.filter("companyId",companyId).list();
		logger.log(Level.SEVERE," get servvice count---" + serviceList.size());
		 if(serviceList.size() == totalservices){
			 
			 List<BillingDocument> billList = ofy().load().type(BillingDocument.class).filter("contractCount",contractCount)
				 		.filter("companyId",companyId).filter("typeOfOrder",(AppConstants.ORDERTYPESERVICE)).list();
			 
			 Contract cont  = ofy().load().type(Contract.class).filter("count",contractCount)
				 		.filter("companyId",companyId).first().now();
			 /**
			  * @author Anil ,Date : 06-03-2019
			  * changed condition AND to OR
			  */
			 if(billList.size() == 0 || billList.size() != cont.getPaymentTermsList().size()){

				 try {
					Thread.sleep(1500);
					serviceInvoiceMapping(companyId,contractCount,totalservices);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
			 
			 }
			 if(billList.size() == cont.getPaymentTermsList().size() && billList.size()>1)
			 {
			
				 /**
				  * @author Anil,Date : 06-03-2019
				  * Sorting was called after assigning date
				  */
				Comparator<BillingDocument> invoiceDateComparator2 = new Comparator<BillingDocument>() {
					public int compare(BillingDocument s1, BillingDocument s2) {

						Date date1 = s1.getBillingDate();
						Date date2 = s2.getBillingDate();

						// ascending order
						return date1.compareTo(date2);
					}
				};
				Collections.sort(billList, invoiceDateComparator2);
						
				 Date firstDate  = billList.get(0).getBillingDate();
				 Date secondDate = billList.get(0).getBillingDate();
				 Date thirdDate = billList.get(0).getBillingDate();
				 Integer billId1 = billList.get(0).getCount();
				 
				 firstDate = billList.get(0).getBillingDate();
				 secondDate = billList.get(0).getBillingDate();
				 
				 if(billList.size()>1){
					 thirdDate = billList.get(1).getBillingDate();
				 }
				 int serCount = 0;
				 
				 int bilcount = 0;
				 
				 for(int i = 0 ;i<billList.size();i++){
				
					 
						 if(i==0){
							 if(firstDate.after(billList.get(i).getBillingDate())){
								 firstDate = billList.get(i).getBillingDate();
								 billId1 = billList.get(i).getCount();
								 bilcount = i;
							 }
						 }else{
								 firstDate = billList.get(i-1).getBillingDate();
								 secondDate = billList.get(i).getBillingDate();
								 billId1 = billList.get(i).getCount();
								 bilcount = i;
						 }
						 

					 logger.log(Level.SEVERE,"billing date 1 -- "+firstDate + " second date -- " + secondDate );
					 List<Service> serList;
					 if(i == 0){
						  serList = ofy().load().type(Service.class).filter("companyId", companyId)
									.filter("contractCount", contractCount).filter("serviceDate <=", firstDate).list();
					 }else{
						 serList = ofy().load().type(Service.class).filter("companyId", companyId)
									.filter("contractCount", contractCount).filter("serviceDate >", firstDate)
									.filter("serviceDate <=", secondDate).list();
					 }
					 
					 
					 if(serList!=null){
						 HashSet<Integer> serBillIds = new HashSet<Integer>();
						 
						 if(billList.get(i).getServiceId()!=null){
							 serBillIds.addAll(billList.get(i).getServiceId());
							 billList.get(i).getServiceId().clear();
						 }
						 
						 for(int sCount = 0 ; sCount<serList.size() ; sCount++){
							 serList.get(sCount).setBillingCount(billId1);
							 serList.get(sCount).setBillingStatus(BillingDocument.CREATED);
							 serBillIds.add(serList.get(sCount).getCount());
							 serCount++;
						 }
						 
						 
						 
						 billList.get(i).getServiceId().addAll(serBillIds);
						 
						 ofy().save().entities(serList);
					 }
					 
				 }
				 
				 if(serCount!=totalservices){
					 logger.log(Level.SEVERE,"get billing extra servvice -- " + serCount + " total ser  ==0 " + totalservices);
					 List<Service> serList;
					 serList = ofy().load().type(Service.class).filter("companyId", companyId)
								.filter("contractCount", contractCount).filter("serviceDate >", secondDate).list();
					 
					 if(serList!=null){

						 HashSet<Integer> serBillIds = new HashSet<Integer>();
						 
						 if(billList.get(bilcount).getServiceId()!=null){
							 serBillIds.addAll(billList.get(bilcount).getServiceId());
							 billList.get(bilcount).getServiceId().clear();
						 }
						 
						 for(int sCount = 0 ; sCount<serList.size() ; sCount++){
							 serList.get(sCount).setBillingCount(billId1);
							 serList.get(sCount).setBillingStatus(BillingDocument.CREATED);
							 serBillIds.add(serList.get(sCount).getCount());
							 billId1 = billList.get(billList.size()-1).getCount();
							 serCount++;
						 }
						 
						 
						 
						 billList.get(bilcount).getServiceId().addAll(serBillIds);
						 
						 ofy().save().entities(serList);
					 
					 }
					 
				 }
				 ofy().save().entities(billList).now();
				 
			 }else{

					
				 Date firstDate  = billList.get(0).getBillingDate();
				 Date secondDate = billList.get(0).getBillingDate();
				 Integer billId1 = billList.get(0).getCount();
				 
				 firstDate = billList.get(0).getBillingDate();
				 secondDate = billList.get(0).getBillingDate();
				 
				 
				 for(int i = 0 ;i<billList.size();i++){
					 int bilcount = 0;

					 logger.log(Level.SEVERE,"billing date 1 -- "+firstDate + " second date -- " + secondDate );
					 List<Service> serList;
						  serList = ofy().load().type(Service.class).filter("companyId", companyId)
									.filter("contractCount", contractCount).list();
					 
					 
					 if(serList!=null){
						 HashSet<Integer> serBillIds = new HashSet<Integer>();
						 
						 if(billList.get(bilcount).getServiceId()!=null){
							 serBillIds.addAll(billList.get(bilcount).getServiceId());
							 billList.get(bilcount).getServiceId().clear();
						 }
						 
						 for(int sCount = 0 ; sCount<serList.size() ; sCount++){
							 serList.get(sCount).setBillingCount(billId1);
							 serList.get(sCount).setBillingStatus(BillingDocument.CREATED);
							 serBillIds.add(serList.get(sCount).getCount());
						 }
						 
						 logger.log(Level.SEVERE,"get servicce List " + serBillIds.toString());
						 
						 billList.get(bilcount).getServiceId().addAll(serBillIds);
						 
						 ofy().save().entities(serList);
					 }
					 
				 }
				 
				 
				 ofy().save().entities(billList).now();
				 
			 
			 }
			 
		 }else{
			 try {
				Thread.sleep(1500);
				serviceInvoiceMapping(companyId,contractCount,totalservices);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
				
				
		 }
		 
		 
	}
}
