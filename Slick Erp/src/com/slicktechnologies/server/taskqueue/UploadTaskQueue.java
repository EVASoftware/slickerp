package com.slicktechnologies.server.taskqueue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.client.Window;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.BlobServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Date 17-08-2019
 * @author Vijay Chougule
 * Des :- blob key with different uploader
 */
public class UploadTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 762992893704992585L;
	
	Logger logger = Logger.getLogger("Name of logger");
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	
	BlobstoreService blobstoreservice=BlobstoreServiceFactory.getBlobstoreService();
	BlobKey blobkey;
	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {

			String transactionType = null;
			
			Map<String, List<BlobKey>> blobs = blobstoreservice.getUploads(req);
			System.out.println("Size of blob == " + blobs.size());
			logger.log(Level.SEVERE, "Block size -- == " + blobs.size());
			List<BlobKey> blobKey = blobs.get("upload");
			logger.log(Level.SEVERE, "upload  -- == " + blobKey);
			System.out.println("Blobs " + blobKey.get(0).getKeyString());
			String onlyblobkey = blobKey.get(0).getKeyString();
			logger.log(Level.SEVERE, " blobKey.get(0).getKeyString()  -- == " + onlyblobkey);
			blobkey = new BlobKey(onlyblobkey);
			req.setAttribute("keyValue", onlyblobkey);
            
            
            resp.setContentType("text/html");
            resp.setHeader("Content-Type", "text/html");
            resp.getWriter().write(onlyblobkey);
            resp.getWriter().flush();
            
            
			
			/** Date 16-08-2019 by Vijay
			 * Des :- for Every Transactions uploading we can define different blobkey and use for the same for specific transaction 
			 */
			if(req.getParameter("TransactionType")!=null){
				transactionType = req.getParameter("TransactionType");
				logger.log(Level.SEVERE, " TransactionType  -- == " + transactionType);
				if(transactionType.equals("Contract")){
					logger.log(Level.SEVERE, " Inside transactionType.equals(Contract)  -- == ");
					DocumentUploadTaskQueue.contractUploadBlobKey = new BlobKey(onlyblobkey);
				}
				else if(transactionType.equals("PurchaseRequisition")){
					logger.log(Level.SEVERE, " Inside transactionType.equals(PurchaseRequisition)  -- == ");
					DocumentUploadTaskQueue.prequisitionUploadBlobKey =new BlobKey(onlyblobkey);
				}
			}
			

		} catch (Exception e) {
			// TODO: handle exception
			logger.log(Level.SEVERE, "Exceptionsssss ===" + e);
			logger.log(Level.SEVERE, "Exceptionsssss ===" + e.getMessage());
		}

	}

}
