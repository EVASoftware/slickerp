package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.ServiceListServiceImpl;
import com.slicktechnologies.server.android.contractwiseservice.MarkCompletedMultipleServices;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class SRCopyEmailTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 945468096297890842L;
	Logger logger = Logger.getLogger("Name of logger");
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdf= new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)throws ServletException, IOException {
		 logger.log(Level.SEVERE, "INSIDE SR COPY EMAIL");
		 String compId = request.getParameter("companyId");
		 long companyId = 0;
		 if(compId!=null){
			 companyId = Long.parseLong(request.getParameter("companyId"));
		 }
		 String taskKeyAndValue = request.getParameter("taskKeyAndValue");
		 logger.log(Level.SEVERE, "Task Key And Value "	+ taskKeyAndValue);
		 String[] taskValue = taskKeyAndValue.split("[$]");
		 
		 
		 if(taskValue[0].equalsIgnoreCase("SendSRCopyEmail")){
				int serviceId = Integer.parseInt(taskValue[2]);
				companyId=Long.parseLong(taskValue[1]);
				String srNumber = taskValue[3];
				
				/**
				 * @author Anil , Date : 09-03-2020
				 * activated cron job thats why commenting sleep
				 */
//				try {
//					/**
//					 * @author Anil , Date : 20-02-2020
//					 * Changed sleep time to 1.5 min (90000 ms) from 5 min (300000 ms)
//					 * Date : 27-02-2020 
//					 * Changed time from 1.5 min (90000 ms) to 3 min (180000 md)
//					 */
//					Thread.sleep(180000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
				String branchEmail = null;
				try{
					branchEmail = taskValue[4];
				}catch(Exception e){
					branchEmail = null;
				}
				
				ArrayList<Integer> serviceIdlist = null;
				try {
					 String strServiceId = request.getParameter("serviceid");
					 logger.log(Level.SEVERE, "strServiceId = "	+ strServiceId);
					 String strServiceids[] = strServiceId.split("[$]");
					 
					serviceIdlist = new ArrayList<Integer>();
					for(int i=0;i<strServiceids.length;i++) {
						serviceIdlist.add(Integer.parseInt(strServiceids[i]));
					}
					logger.log(Level.SEVERE, "serviceIdlist"	+ serviceIdlist);

				} catch (Exception e) {
					e.printStackTrace();
				}
				
				List<Service> servicelist = null;
				if(serviceIdlist!=null&&serviceIdlist.size()>0) {
					try {
						servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceIdlist).list();
						logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				else {
					Service service = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId).first().now();
					servicelist = new ArrayList<Service>();
					servicelist.add(service);
				}
				
				Email email = new Email();
				MarkCompletedMultipleServices servlet = new MarkCompletedMultipleServices();
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", servicelist.get(0).getCompanyId())){
					logger.log(Level.SEVERE, "servicelist size == "+servicelist.size());
					servlet.sendEmailtoCustomer(servicelist,srNumber,email,branchEmail);
				}
				
			
			}
	}
}
