package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.googlecode.objectify.ObjectifyService;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

public class ServiceCancellationTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8829426820750091018L;

//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
//			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		
//	}
	Logger logger = Logger.getLogger("ServiceCancellationTaskQueue Of logger");
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		Long companyId = Long.parseLong(req.getParameter("companyId"));
		String processName = req.getParameter("processName").trim();
			
		if(processName.equals("ProductUtilty")){
			productUtiltyProcess(companyId);
		}else if(processName.equals("ServiceCanceltion")){
			serviceCanceltion(companyId);
		}
	}

	
	public void serviceCanceltion(Long companyId){

		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String dateValue = ServerAppUtility.getForProcessConfigurartionIsActiveOrNot("CancelServiceUptoDate", companyId);
		logger.log(Level.SEVERE," get service size -- " + dateValue );
		if(dateValue != null){
			Date validateDate = null;
			try {
				validateDate = sdf.parse(dateValue);
				} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int check = 0;
			
			ArrayList<String> serStatus = new ArrayList<String>();
			serStatus.add(Service.SERVICESTATUSSCHEDULE);
			serStatus.add(Service.SERVICESTATUSRESCHEDULE);
			serStatus.add(Service.SERVICESTATUSPENDING);
			Date  fromDate = validateDate;
			Date  toDate = validateDate;
			while(check<2){
			logger.log(Level.SEVERE," from date -- " + fromDate +" to date -- " + toDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(fromDate);
				cal.add(Calendar.MONTH, -3);
				fromDate = cal.getTime();
				
				
				List<Service> serviceList = ofy().load().type(Service.class).filter("companyId",companyId).
											filter("serviceDate <=",toDate).
											filter("status IN",serStatus).limit(4000).list();
				logger.log(Level.SEVERE," get service size -- " + serviceList.size() +" from date -- " + fromDate +" to date -- " + toDate);
				
				Comparator<Service> serviceDateComparator2 = new Comparator<Service>() {
					public int compare(Service s1, Service s2) {
					
					Date date1 = s1.getServiceDate();
					Date date2 = s2.getServiceDate();
					
					//ascending order
					return date1.compareTo(date2);
					}
					};
					Collections.sort(serviceList, serviceDateComparator2);
				
				
				if(serviceList.size()>0){
					
					for(int i =0; i<serviceList.size();i++){
//						logger.log(Level.SEVERE," get service count -- " + serviceList.get(i).getCount());
						serviceList.get(i).setStatus(Service.SERVICESTATUSCANCELLED);
						serviceList.get(i).setRemark("This service is canceled throught canceltion process on -" + sdf.format(new Date()));
					}
					check= 0;
				}else{
					check++;
				}
				logger.log(Level.SEVERE,"last service date id " + serviceList.get(0).getCount() + " date -- " + serviceList.get(0).getServiceDate());
				ofy().save().entities(serviceList);
				ObjectifyService.reset();
				ofy().consistency(Consistency.STRONG);
				ofy().clear();
				serviceList.clear();
				serviceList = null;
				toDate = fromDate;
//				System.gc(serviceList);
				
				
				
				
			}
		}
	
	}
	
	public void productUtiltyProcess(Long companyId){
		
		List<ItemProduct> itemList = ofy().load().type(ItemProduct.class).filter("companyId",companyId).list();
		
		if(itemList!=null && itemList.size()>0){
			for(ItemProduct itm : itemList){
				if(itm.getPurchasePrice()==0 && itm.getPrice()>0 ){
					itm.setPurchasePrice(itm.getPrice());
				}
				
				if(itm.getServiceTax()!=null && itm.getPurchaseTax1()==null){
					itm.setPurchaseTax1(itm.getServiceTax());
				}
				
				if(itm.getVatTax()!=null && itm.getPurchaseTax2()==null){
					itm.setPurchaseTax2(itm.getVatTax());
				}
			}
			
			ofy().save().entities(itemList);
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
		}
	}
}
