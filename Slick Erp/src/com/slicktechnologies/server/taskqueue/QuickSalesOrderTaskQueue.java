package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;







import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.SalesOrderServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

import static com.googlecode.objectify.ObjectifyService.ofy;
public class QuickSalesOrderTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8386933109922393585L;
	
	
	
	Logger logger = Logger.getLogger("Name of the logger");
	double paymentrecievedAmount=0;
	double balancePayment =0;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		logger.log(Level.SEVERE,"Welcome to QuickSales Task Queue");
		
		String salesOrderwithdollar = request.getParameter("quicksalesorderkey");
		
		String salesOrder[] = salesOrderwithdollar.split("[$]");
		
		long companyId = Long.parseLong(salesOrder[0]);
		int salesOrderId = Integer.parseInt(salesOrder[1]);
		
		
		
		/**
		 * Date 03-04-2017
		 * added by vijay for quick Sales Order accounting interface will 
		 */
		
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGCO).filter("companyId", companyId).filter("configStatus", true).first().now();
		}
		int flag =0;
		
		if(processConfig!=null){
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			quickSalesOrderccountingInterface(salesOrderId ,companyId);
		}
		
		
		/**
		 * ends here
		 */
		
		
		
		
		SalesOrder salesorderEntity = ofy().load().type(SalesOrder.class).filter("companyId", companyId).filter("count", salesOrderId).first().now();
		salesorderEntity.setStatus("Approved");
		logger.log(Level.SEVERE,"SALES ORDER STATUS In TASK Queue=="+salesorderEntity.getStatus());
		if(salesorderEntity!=null){ 
			
			 paymentrecievedAmount = salesorderEntity.getPaymentRecieved();
			 balancePayment = salesorderEntity.getBalancePayment();
			 
			 
			 // this for changing quotation status and customer status
			 SalesOrderServiceImpl implementor=new SalesOrderServiceImpl();
			 implementor.changeStatus(salesorderEntity);
			  
		 	saveTaxesAndCharges(salesorderEntity);
			
			createDeliveryNote(salesorderEntity);
				
			 
			logger.log(Level.SEVERE," PAyement Recieved ===== "+salesorderEntity.getPaymentRecieved());
			double percentage = getpercentage(salesorderEntity.getNetpayable(),salesorderEntity.getPaymentRecieved());
			logger.log(Level.SEVERE," Percentage == ====== "+percentage);
			
			if(100-percentage==0 || 100-percentage==100.0){
				System.out.println("For only 100 paymet");
				
				if(salesorderEntity.getPaymentRecieved()==0.0 || salesorderEntity.getPaymentRecieved()==0){
					System.out.println("if payment recieved nothing  =  this is for balance payment");
					percentage =100;
					createBillingDocuments(salesorderEntity,percentage,false,"OpenPayment");
				}
				else{
					System.out.println("for payment received full 100 percent ");
					createBillingDocuments(salesorderEntity,percentage,true,"ClosedPayment");
				}
				
				
			}else{
				
				System.out.println("For partial payment");
				for(int i=0;i<2;i++){
					if(i==1){
						System.out.println("Balance payment Billing doc");
						double percent = 100-percentage;
						logger.log(Level.SEVERE,"  For partial balance Payment remaining percenatge ==="+percent);
	
						createBillingDocuments(salesorderEntity,percent,false,"OpenPayment");
					}else{
						
						System.out.println("Payment recieved billing doc ================");
						createBillingDocuments(salesorderEntity,percentage,true,"ClosedPayment");
					}
				}
			}
			
		}
		
		
	}
	
	
	


	/**
	 * Here i am getting Sales Order percentage for payment terms based on net payable and  payment received 
	 */
	
	private double getpercentage(double netpayable, double paymentRecieved) {
		double totalpercentage;
		totalpercentage= paymentRecieved/netpayable*100;
		return totalpercentage;
	}
	
	/**
	 * ends here
	 * @param salesorderEntity 
	 */
	
	
	private void saveTaxesAndCharges(SalesOrder salesorderEntity)
	{
		final GenricServiceImpl genimpl=new GenricServiceImpl();
		TaxesAndCharges taxChargeEntity=new TaxesAndCharges();
		List<ContractCharges> listaxcharge=saveCharges(salesorderEntity);
		ArrayList<ContractCharges> arrtaxchrg=new ArrayList<ContractCharges>();
		arrtaxchrg.addAll(listaxcharge);
		taxChargeEntity.setContractId(salesorderEntity.getCount());
		taxChargeEntity.setIdentifyOrder(AppConstants.BILLINGSALESFLOW.trim());
		taxChargeEntity.setTaxesChargesList(arrtaxchrg);
		taxChargeEntity.setCompanyId(salesorderEntity.getCompanyId());
		
		genimpl.save(taxChargeEntity);
	}
	
	private List<ContractCharges> saveCharges(SalesOrder salesorderEntity)
	{
		ArrayList<ContractCharges> arrCharge=new ArrayList<ContractCharges>();
		double calcBalAmt=0;
		for(int i=0;i<salesorderEntity.getProductCharges().size();i++)
		{
			ContractCharges chargeDetails=new ContractCharges();
			chargeDetails.setTaxChargeName(salesorderEntity.getProductCharges().get(i).getChargeName());
			chargeDetails.setTaxChargePercent(salesorderEntity.getProductCharges().get(i).getChargePercent());
			chargeDetails.setTaxChargeAbsVal(salesorderEntity.getProductCharges().get(i).getChargeAbsValue());
			System.out.println("Find Assess"+salesorderEntity.getProductCharges().get(i).getAssessableAmount());
			chargeDetails.setTaxChargeAssesVal(salesorderEntity.getProductCharges().get(i).getAssessableAmount());
			if(salesorderEntity.getProductCharges().get(i).getChargeAbsValue()!=0){
				calcBalAmt=salesorderEntity.getProductCharges().get(i).getChargeAbsValue();
			}
			if(salesorderEntity.getProductCharges().get(i).getChargePercent()!=0){
				calcBalAmt=salesorderEntity.getProductCharges().get(i).getChargePercent()*salesorderEntity.getProductCharges().get(i).getAssessableAmount()/100;
			}
			chargeDetails.setChargesBalanceAmt(calcBalAmt);
			arrCharge.add(chargeDetails);
		}
		return arrCharge;
		
	}
	
	protected void createDeliveryNote(SalesOrder salesorderEntity)
	{
		final GenricServiceImpl genimpl=new GenricServiceImpl();
		
		DeliveryNote deliveryEntity=new DeliveryNote();
		
		deliveryEntity.setCinfo(salesorderEntity.getCinfo());
		deliveryEntity.setSalesOrderCount(salesorderEntity.getCount());
		if(salesorderEntity.getLeadCount()!=0){
			deliveryEntity.setLeadCount(salesorderEntity.getLeadCount());
		}
		if(salesorderEntity.getQuotationCount()!=0){
			deliveryEntity.setQuotationCount(salesorderEntity.getQuotationCount());
		}
		if(!salesorderEntity.getRefNo().equals("")){
			deliveryEntity.setReferenceNumber(salesorderEntity.getRefNo());
		}
		if(salesorderEntity.getReferenceDate()!=null){
			deliveryEntity.setReferenceDate(salesorderEntity.getReferenceDate());
		}
		if(salesorderEntity.getEmployee()!=null){
			deliveryEntity.setEmployee(salesorderEntity.getEmployee());
		}
		if(salesorderEntity.getCformstatus()!=null){
			deliveryEntity.setCformStatus(salesorderEntity.getCformstatus());
		}
		if(salesorderEntity.getCstpercent()!=0){
			deliveryEntity.setCstPercent(salesorderEntity.getCstpercent());
		}
		if(salesorderEntity.getCstName()!=null){
			deliveryEntity.setCstName(salesorderEntity.getCstName());
		}
		
		

		deliveryEntity.setBranch(salesorderEntity.getBranch());
		deliveryEntity.setDeliveryDate(salesorderEntity.getDeliveryDate());
		
		/**Date 21-7-2020 by Amol ****/
		if(salesorderEntity.getNumberRange()!=null){
			deliveryEntity.setNumberRange(salesorderEntity.getNumberRange());
		}
		
		if(salesorderEntity.getItems().size()!=0){
			List<DeliveryLineItems> deliveryLis=retrieveDeliveryItems(salesorderEntity);
			ArrayList<DeliveryLineItems> arrLineItems=new ArrayList<DeliveryLineItems>();
			arrLineItems.addAll(deliveryLis);
			deliveryEntity.setDeliveryItems(arrLineItems);
		}
		if(salesorderEntity.getTotalAmount()!=0){
			deliveryEntity.setTotalAmount(salesorderEntity.getTotalAmount());
		}
		if(salesorderEntity.getProductTaxes().size()!=0){
			deliveryEntity.setProdTaxesLis(salesorderEntity.getProductTaxes());
		}
		if(salesorderEntity.getProductCharges().size()!=0){
			deliveryEntity.setProdChargeLis(salesorderEntity.getProductCharges());
		}
		if(salesorderEntity.getNetpayable()!=0){
			deliveryEntity.setNetpayable(salesorderEntity.getNetpayable());
		}
		if(salesorderEntity.getShippingAddress()!=null){
			deliveryEntity.setShippingAddress(salesorderEntity.getShippingAddress());
		}
		deliveryEntity.setCompanyId(salesorderEntity.getCompanyId());
		
		
		if(salesorderEntity.getDeliveryDate().before(new Date())){
			deliveryEntity.setStatus("Completed");
		}else{
			System.out.println("DELIVERY Note Created");
			deliveryEntity.setStatus(DeliveryNote.CREATED);
		}
		
		genimpl.save(deliveryEntity);
		
		if(deliveryEntity.getStatus().equals("Completed")){
			/**
			 * Date : 18-01-2017 By Anil
			 * New Stock Updation and Transaction creation Code
			 */
			
			ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
			for(DeliveryLineItems product:deliveryEntity.getDeliveryItems()){
				
				if(!product.getWareHouseName().equals("") && !product.getStorageLocName().equals("") && !product.getStorageBinName().equals("")){
				System.out.println("=====warehouse is there");
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(deliveryEntity.getCompanyId());
				item.setBranch(deliveryEntity.getBranch());
				item.setDocumentType(AppConstants.DELIVERYNOTE);
				item.setDocumentId(deliveryEntity.getCount());
				item.setDocumentDate(deliveryEntity.getCreationDate());
				item.setDocumnetTitle("");
				item.setProdId(product.getProdId());
				item.setUpdateQty(product.getQuantity());
				item.setOperation(AppConstants.SUBTRACT);
				item.setWarehouseName(product.getWareHouseName());
				item.setStorageLocation(product.getStorageLocName());
				item.setStorageBin(product.getStorageBinName());
				itemList.add(item);
				
				}
			}
			
			System.out.println("SIZE=="+itemList.size());
			if(itemList.size()!=0)
			UpdateStock.setProductInventory(itemList);
			
			/**
			 * End
			 */
		}
	}
	
	
	private List<DeliveryLineItems> retrieveDeliveryItems(SalesOrder salesorderEntity)
	{
		ArrayList<DeliveryLineItems> arrDeliveryItems=new ArrayList<DeliveryLineItems>();
		double priceVal=0,taxValueAmt=0,totalAmt=0;
		
		for(int i=0;i<salesorderEntity.getItems().size();i++)
		{
			DeliveryLineItems delLine=new DeliveryLineItems();
			ItemProduct ipEntity=ofy().load().type(ItemProduct.class).filter("companyId",salesorderEntity.getCompanyId()).filter("productCode", salesorderEntity.getItems().get(i).getProductCode().trim()).first().now();
			System.out.println("ASD"+salesorderEntity.getItems().get(i).getProductCode());
			delLine.setProdId(ipEntity.getCount());
			delLine.setProdCode(salesorderEntity.getItems().get(i).getProductCode());
			delLine.setProdCategory(salesorderEntity.getItems().get(i).getProductCategory());
			delLine.setProdName(salesorderEntity.getItems().get(i).getProductName());
			delLine.setQuantity(salesorderEntity.getItems().get(i).getQty());
			taxValueAmt=removeTaxAmt(salesorderEntity.getItems().get(i).getPrduct());
			priceVal=salesorderEntity.getItems().get(i).getPrice()-taxValueAmt;
			delLine.setPrice(priceVal);
			delLine.setVatTax(salesorderEntity.getItems().get(i).getVatTax());
			delLine.setServiceTax(salesorderEntity.getItems().get(i).getServiceTax());
			delLine.setProdPercDiscount(salesorderEntity.getItems().get(i).getPercentageDiscount());
			
			/*Rohan added below line for copy Disc Amount from sales order to delivery note 
			 * Dated : 12/09/2016 
			 */
			delLine.setDiscountAmt(salesorderEntity.getItems().get(i).getDiscountAmt());
			if(salesorderEntity.getItems().get(i).getPercentageDiscount()!=0){
				totalAmt=priceVal-(priceVal*salesorderEntity.getItems().get(i).getPercentageDiscount()/100);
				totalAmt=totalAmt*salesorderEntity.getItems().get(i).getQty();
			}
			if(salesorderEntity.getItems().get(i).getPercentageDiscount()==0){
				totalAmt=priceVal*salesorderEntity.getItems().get(i).getQty();
			}
			delLine.setTotalAmount(totalAmt);
			
			if(!salesorderEntity.getItems().get(i).getItemProductWarehouseName().equals("") && !salesorderEntity.getItems().get(i).getItemProductStrorageLocation().equals("")
					&& !salesorderEntity.getItems().get(i).getItemProductStorageBin().equals("")){
				
				delLine.setWareHouseName(salesorderEntity.getItems().get(i).getItemProductWarehouseName());
				delLine.setStorageLocName(salesorderEntity.getItems().get(i).getItemProductStrorageLocation());
				delLine.setStorageBinName(salesorderEntity.getItems().get(i).getItemProductStorageBin());
				
			}else{
				delLine.setWareHouseName("");
				delLine.setStorageLocName("");
				delLine.setStorageBinName("");
			}
			
			delLine.setPrduct(salesorderEntity.getItems().get(i).getPrduct());
			if(salesorderEntity.getItems().get(i).getPrduct().getHsnNumber()!=null&& !salesorderEntity.getItems().get(i).getPrduct().getHsnNumber().equals("")){
				delLine.getPrduct().setHsnNumber(salesorderEntity.getItems().get(i).getPrduct().getHsnNumber());

			}
			
			arrDeliveryItems.add(delLine);
		}
		return arrDeliveryItems;
		
	}
	
	public double removeTaxAmt(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			double removeServiceTax=(entity.getPrice()/(1+service/100));
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
//			double taxPerc=service+vat;
//			retrServ=entity.getPrice()/(1+taxPerc/100);
//			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		System.out.println("Shared Tax Return"+tax);
		return tax;
	}
	
	
	
	private void createBillingDocuments(SalesOrder salesorderEntity,double percentage, boolean paymentermscomment, String balancepayment) {
		// TODO Auto-generated method stub
		
			System.out.println("Sales order id"+salesorderEntity.getCount());
			ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			
			PaymentTerms paymentTerms=new PaymentTerms();
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			/******************************************************************************/
			if(salesorderEntity.getCinfo()!=null)
				billingDocEntity.setPersonInfo(salesorderEntity.getCinfo());
			if(salesorderEntity.getCount()!=0)
				billingDocEntity.setContractCount(salesorderEntity.getCount());
			if(salesorderEntity.getNetpayable()!=0)
				billingDocEntity.setTotalSalesAmount(salesorderEntity.getNetpayable());
			salesProdLis=this.retrieveSalesProducts(salesorderEntity, percentage);
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			System.out.println("Size here here"+salesOrdArr.size());
			if(salesOrdArr.size()!=0)
				billingDocEntity.setSalesOrderProducts(salesOrdArr);
			if(salesorderEntity.getCompanyId()!=null)
				billingDocEntity.setCompanyId(salesorderEntity.getCompanyId());
			
			
			Date invoiceDate=null;
			if(salesorderEntity.getQuickSalesInvoiceDate()!=null){
				invoiceDate = salesorderEntity.getQuickSalesInvoiceDate();
			}
			else
			{
				invoiceDate = salesorderEntity.getSalesOrderDate();
			}
			logger.log(Level.SEVERE," Billing Invoice paymnent Date"+invoiceDate);
			
			if(invoiceDate!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
				logger.log(Level.SEVERE," Billing Date While SAving"+billingDocEntity.getBillingDate());
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
				logger.log(Level.SEVERE," Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
				logger.log(Level.SEVERE," Payment Date While SAving"+billingDocEntity.getPaymentDate());
			}
			if(salesorderEntity.getPaymentMethod()!=null)
				billingDocEntity.setPaymentMethod(salesorderEntity.getPaymentMethod());
			if(salesorderEntity.getApproverName()!=null)
				billingDocEntity.setApproverName(salesorderEntity.getApproverName());
			if(salesorderEntity.getEmployee()!=null)
				billingDocEntity.setEmployee(salesorderEntity.getEmployee());
			if(salesorderEntity.getBranch()!=null)
				billingDocEntity.setBranch(salesorderEntity.getBranch());
			
			if(salesorderEntity.getNumberRange()!=null){
				billingDocEntity.setNumberRange(salesorderEntity.getNumberRange());
			}
			
			
			
			/**
			 * Date : 21-09-2017 BY ANIL
			 * If other charges are selected at sales order level then we have to split those charges as per payment terms.
			 */
			double sumOfOc=0;
			if(salesorderEntity.getOtherCharges()!=null){
				ArrayList<OtherCharges> ocList=salesorderEntity.retrieveOtherCharges(percentage);
				for(OtherCharges oc:ocList){
					sumOfOc=sumOfOc+oc.getAmount();
				}
				billingDocEntity.setOtherCharges(ocList);
				billingDocEntity.setTotalOtherCharges(sumOfOc);
			}
			/**
			 * End
			 */
			double taxamt =0;
			
			if(salesorderEntity.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				//billTaxLis=this.listForBillingTaxes(salesProdLis);
				billTaxLis=listForBillingTaxesforQuickSales(salesorderEntity, percentage);
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
				
				//Date 20-sep-2017 added by vijay for tax amt calculation
				taxamt = salesorderEntity.getTaxAmt(billTaxArr);
			}
			
			billingDocEntity.setOrderCreationDate(salesorderEntity.getCreationDate());

			double totBillAmt=getTotalBillAmt(salesProdLis);
			billingDocEntity.setTotalBillingAmount(totBillAmt);
			
//			int payTrmsDays=this.getPaymentTermsList().get(i).getPayTermDays();
//			double payTrmsPercent=this.getPaymentTermsList().get(i).getPayTermPercent();
//			String payTrmsComment=this.getPaymentTermsList().get(i).getPayTermComment().trim();
//			
//			paymentTerms.setPayTermDays(payTrmsDays);
//			paymentTerms.setPayTermPercent(payTrmsPercent);
//			paymentTerms.setPayTermComment(payTrmsComment);
//			billingPayTerms.add(paymentTerms);
			
			
			paymentTerms.setPayTermDays(0);
			
			double paypercent= Math.round(percentage);
			System.out.println("while saving percentage === "+paypercent);
			paymentTerms.setPayTermPercent(paypercent);
			if(paymentermscomment){
				paymentTerms.setPayTermComment("Payment Recieved");
			}else{
				paymentTerms.setPayTermComment("Balance Payment");
			}
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			
			
			if(salesorderEntity.getCformstatus()!=null){
				billingDocEntity.setOrderCformStatus(salesorderEntity.getCformstatus());
			}
			else{
				billingDocEntity.setOrderCformStatus("");
			}
			
			if(salesorderEntity.getCstpercent()!=null){
				billingDocEntity.setOrderCformPercent(salesorderEntity.getCstpercent());
			}
			else{
				billingDocEntity.setOrderCformPercent(-1);
			}
			
			
	//***********************changes made by rohan for saving gross value *************	
			double grossValue=salesorderEntity.getTotalAmount();
			
			
			billingDocEntity.setGrossValue(grossValue);
			
	//********************************changes ends here *******************************		
			
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESALES);
			
//			if(paymentermscomment){
				billingDocEntity.setStatus(BillingDocument.APPROVED);

//			}else{
//				billingDocEntity.setStatus(BillingDocument.CREATED);

//			}
			
			billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
			
			
//			if(salesorderEntity.getNumberRange()!=null)
//				billingDocEntity.setNumberRange(billingDocEntity.getNumberRange());
			
			
//			GenricServiceImpl impl=new GenricServiceImpl();
//			impl.save(billingDocEntity);
			

			List<ContractCharges> billingtaxlist = billingDocEntity.getBillingTaxes();
			List<ContractCharges> taxBillingArr = new ArrayList<ContractCharges>();
			if(billingtaxlist.size()!=0){
				System.out.println("SIZE====="+billingtaxlist.size());
				for(int k=0;k<billingtaxlist.size();k++){
					double totalCalcAmt=0;
					totalCalcAmt = billingtaxlist.get(k).getTaxChargeAssesVal()*billingtaxlist.get(k).getTaxChargePercent()/100;
					billingtaxlist.get(k).setPayableAmt(totalCalcAmt);
					System.out.println("AMONT+++"+totalCalcAmt);
					taxBillingArr.add(billingtaxlist.get(k));
				}
				billingDocEntity.setBillingTaxes(taxBillingArr);
			
//				GenricServiceImpl genimpl = new GenricServiceImpl();
//				genimpl.save(billingDocEntity);
				
			}
			
			
			double payableamt=0;
			payableamt = calculateTotalBillAmt(billingDocEntity.getSalesOrderProducts()) + calculateTotalTaxes(billingDocEntity.getBillingTaxes()) + calculateTotalBillingCharges(billingDocEntity.getBillingOtherCharges())+sumOfOc;
			payableamt=Math.round(payableamt);
			System.out.println("Payable amt =="+payableamt);
			
			List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
			
			BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
			
			if(billingDocEntity.getContractCount()!=null){
				billingDocDetails.setOrderId(billingDocEntity.getContractCount());
			}
			
			if(billingDocEntity.getCount()!=0)
				billingDocDetails.setBillId(billingDocEntity.getCount());
			if(billingDocEntity.getBillingDate()!=null)
				billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
			if(billingDocEntity.getBillAmount()!=null)
				billingDocDetails.setBillAmount(payableamt);
			if(billingDocEntity.getStatus()!=null)
				billingDocDetails.setBillstatus(billingDocEntity.getStatus());
			System.out.println("Billing IDDDDD===="+billingDocDetails.getBillId());
			billtablelis.add(billingDocDetails);
			
			billingDocEntity.setTotalBillingAmount(payableamt);
			System.out.println(" getTotalBillingAmount amount ===");
			System.out.println("billing amount ==" +billingDocEntity.getBillAmount());
			System.out.println("billing amount ####  ==" +Math.round(billingDocEntity.getTotalBillingAmount()));
			
			ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
			billtablearr.addAll(billtablelis);
			billingDocEntity.setArrayBillingDocument(billtablelis);
			
			
			/****
			 * Date : 11-10-2017 BY ANIL
			 */
			/** Date 03-09-2017 added by vijay for total amount before taxes**/
			billingDocEntity.setTotalAmount(totBillAmt);
			/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
			billingDocEntity.setFinalTotalAmt(totBillAmt);
			 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
			double totalamtincludingtax=totBillAmt+taxamt+sumOfOc;
			
			billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
			/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
			billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
			
			/**
			 * END
			 */
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(billingDocEntity);
			
			/******************************************************************/
			
			logger.log(Level.SEVERE,"Here Billing updated successfully");
			
//			GenricServiceImpl impl=new GenricServiceImpl();
//				impl.save(billingDocEntity);
				
				System.out.println("payament received for invoice and payment == "+balancepayment);
				if(balancepayment.equalsIgnoreCase("ClosedPayment")){
					Createinvoice(salesorderEntity,billingDocEntity,balancepayment);
				}	
		}

	
	
	

	private double calculateTotalBillingCharges(List<ContractCharges> billingOtherCharges) {

		List<ContractCharges> list = billingOtherCharges;
		double sum = 0;
		for (int i = 0; i < list.size(); i++) {
			ContractCharges entity = list.get(i);
			sum = sum+ (entity.getChargesBalanceAmt() * entity.getPaypercent() / 100);
		}
		return sum;

	}


	private double calculateTotalTaxes(List<ContractCharges> billingTaxes) {

		List<ContractCharges>list=billingTaxes;
		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ContractCharges entity=list.get(i);
			sum=sum+(entity.getTaxChargeAssesVal()*entity.getTaxChargePercent()/100);
		}
		System.out.println("Sum Taxes"+sum);
		return sum;

	}

	private double calculateTotalBillAmt(ArrayList<SalesOrderProductLineItem> salesOrderProducts) {

		System.out.println("Entered in table calculate");
		List<SalesOrderProductLineItem>list=salesOrderProducts;

		double caltotalamt=0;
		for(int i=0;i<list.size();i++)
		{
			SalesOrderProductLineItem entity=list.get(i);
			
			if(entity.getFlatDiscount()==0)
			{
				caltotalamt=caltotalamt+(entity.getBaseBillingAmount()*entity.getPaymentPercent())/100;
			}
			else
			{
				caltotalamt=caltotalamt+((entity.getBaseBillingAmount()*entity.getPaymentPercent())/100)-entity.getFlatDiscount();
			}
		}
		return caltotalamt;

	}
	
	
	
	

	private double getTotalBillAmt(List<SalesOrderProductLineItem> lisForTotalBill) {
		
		
		double saveTotalBillAmt=0;
		for(int i=0;i<lisForTotalBill.size();i++)
		{
			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getBaseBillingAmount();
		}
		System.out.println("Save Total Billing Amount"+saveTotalBillAmt);
		return saveTotalBillAmt;
	}


	private List<ContractCharges> listForBillingTaxesforQuickSales(SalesOrder salesorderEntity, double percentage) {
		
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
		double assessValue=0;
		for(int i=0;i<salesorderEntity.productTaxes.size();i++){
			ContractCharges taxDetails=new ContractCharges();
			taxDetails.setTaxChargeName(salesorderEntity.getProductTaxes().get(i).getChargeName());
			taxDetails.setTaxChargePercent(salesorderEntity.getProductTaxes().get(i).getChargePercent());
			assessValue=salesorderEntity.getProductTaxes().get(i).getAssessableAmount()*percentage/100;
			taxDetails.setTaxChargeAssesVal(assessValue);
			
			taxDetails.setIdentifyTaxCharge(salesorderEntity.getProductTaxes().get(i).getIndexCheck());
			arrBillTax.add(taxDetails);
		}
		return arrBillTax;

	}


	private List<SalesOrderProductLineItem> retrieveSalesProducts(SalesOrder salesOrderEntity, double percentage) {
		

		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<salesOrderEntity.getItems().size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", salesOrderEntity.getItems().get(i).getPrduct().getCount()).first().now();
			
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
//			SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProdId(salesOrderEntity.getItems().get(i).getPrduct().getCount());
			salesOrder.setProdCategory(salesOrderEntity.getItems().get(i).getProductCategory());
			salesOrder.setProdCode(salesOrderEntity.getItems().get(i).getProductCode());
			salesOrder.setProdName(salesOrderEntity.getItems().get(i).getProductName());
			salesOrder.setQuantity(salesOrderEntity.getItems().get(i).getQty());
			salesOrder.setOrderDuration(0);
			salesOrder.setOrderServices(0);
			totalTax=this.removeTaxAmt(salesOrderEntity.getItems().get(i).getPrduct());
			System.out.println("Price in entity"+salesOrderEntity.getItems().get(i).getPrice()+"   TaxAMt  "+totalTax);
			prodPrice=salesOrderEntity.getItems().get(i).getPrice()-totalTax;
			System.out.println("Prod"+prodPrice);
			salesOrder.setPrice(prodPrice);
			salesOrder.setVatTax(salesOrderEntity.getItems().get(i).getVatTax());
			salesOrder.setServiceTax(salesOrderEntity.getItems().get(i).getServiceTax());
			
		//  vijay added this code for setting HSN Code Date : 25-07-2017
			if(salesOrderEntity.getItems().get(i).getPrduct().getHsnNumber()!=null)
			salesOrder.setHsnCode(salesOrderEntity.getItems().get(i).getPrduct().getHsnNumber());
					
					
			salesOrder.setProdPercDiscount(salesOrderEntity.getItems().get(i).getPercentageDiscount());
			salesOrder.setUnitOfMeasurement(salesOrderEntity.getItems().get(i).getUnitOfMeasurement());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			if(salesOrderEntity.getItems().get(i).getPercentageDiscount()!=0){
				percAmt=prodPrice-(prodPrice*salesOrderEntity.getItems().get(i).getPercentageDiscount()/100);
				percAmt=percAmt*salesOrderEntity.getItems().get(i).getQty();
			}
			if(salesOrderEntity.getItems().get(i).getPercentageDiscount()==0){
				percAmt=prodPrice*salesOrderEntity.getItems().get(i).getQty();
			}
			salesOrder.setTotalAmount(percAmt);
			
//			if(payTermPercent!=0){
//				baseBillingAmount=(percAmt*payTermPercent)/100;
//			}
			
			System.out.println("payTermPercent === "+percentage);
			
			if(percentage!=0){
				baseBillingAmount=(percAmt*percentage)/100;
			}
			
			System.out.println("Base Billing Amount Through Payment Terms"+baseBillingAmount);
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			
			/**
			 * Date : 04-08-2017 By Anil
			 * Setting base bill amount to base payment amount
			 */
			salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
			/**
			 * End
			 */
		//  date 24-08-2017 vijay added this code for area considered in calculation
			if(salesOrderEntity.getItems().get(i).getArea()!=null)
				salesOrder.setArea(salesOrderEntity.getItems().get(i).getArea());
				
			salesOrder.setIndexVal(i+1);
			/////////////////////////////////////////////////////////////////////////////////
			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
	
	}
	
	
	private void Createinvoice(SalesOrder salesorderEntity,BillingDocument billingDocEntity, String balancepayment) {
		System.out.println("GET STATUS"+billingDocEntity.getStatus());
		System.out.println("========"+billingDocEntity.getContractCount());
		
		final Invoice inventity = new Invoice();
		PersonInfo pinfo = new PersonInfo();
		if (billingDocEntity.getPersonInfo().getCount()!=0) {
			pinfo.setCount(billingDocEntity.getPersonInfo().getCount());
		}
		if (billingDocEntity.getPersonInfo().getFullName() != null) {
			pinfo.setFullName(billingDocEntity.getPersonInfo().getFullName());
		}
		if (billingDocEntity.getPersonInfo().getCellNumber() != null) {
			pinfo.setCellNumber(billingDocEntity.getPersonInfo().getCellNumber());
		}
		if (billingDocEntity.getPersonInfo().getPocName()!= null) {
			pinfo.setPocName(billingDocEntity.getPersonInfo().getPocName());
		}

		if (billingDocEntity.getGrossValue() != 0) {
			inventity.setGrossValue(billingDocEntity.getGrossValue());
		}


		if (pinfo != null) {
			inventity.setPersonInfo(pinfo);
		}

		if (billingDocEntity.getContractCount()!=0)
			inventity.setContractCount(billingDocEntity.getContractCount());
		if (billingDocEntity.getContractStartDate() != null) {
			inventity.setContractStartDate(billingDocEntity.getContractStartDate());
		}
		if (billingDocEntity.getContractEndDate() != null) {
			inventity.setContractEndDate(billingDocEntity.getContractEndDate());
		}
		if (billingDocEntity.getTotalBillingAmount()!=null)
			inventity.setTotalSalesAmount(Math.round(billingDocEntity.getTotalSalesAmount()));
		
		
		inventity.setArrayBillingDocument(billingDocEntity.getArrayBillingDocument());
		if (billingDocEntity.getTotalBillingAmount()!=null)
			inventity.setTotalBillingAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
		
		if (billingDocEntity.getTotalBillingAmount()!=null)
			inventity.setNetPayable(Math.round(billingDocEntity.getTotalBillingAmount()));
		
		inventity.setDiscount(0.0);
		
		if (billingDocEntity.getInvoiceDate() != null)
			inventity.setInvoiceDate(billingDocEntity.getInvoiceDate());
		if (billingDocEntity.getPaymentDate() != null)
			inventity.setPaymentDate(billingDocEntity.getPaymentDate());
		if (billingDocEntity.getApproverName() != null)
			inventity.setApproverName(billingDocEntity.getApproverName());
		if (billingDocEntity.getEmployee() != null)
			inventity.setEmployee(billingDocEntity.getEmployee());
		if (billingDocEntity.getBranch()!= null)
			inventity.setBranch(billingDocEntity.getBranch());
		
		inventity.setOrderCreationDate(billingDocEntity.getOrderCreationDate());
		inventity.setCompanyId(billingDocEntity.getCompanyId());
		inventity.setInvoiceAmount(Math.round(billingDocEntity.getTotalBillingAmount()));
		inventity.setInvoiceType(AppConstants.CREATETAXINVOICE);
		inventity.setPaymentMethod(billingDocEntity.getPaymentMethod());
		inventity.setAccountType(billingDocEntity.getAccountType());
		inventity.setTypeOfOrder(billingDocEntity.getTypeOfOrder());
		
		if(balancepayment.equalsIgnoreCase("ClosedPayment")){
			inventity.setStatus(Invoice.APPROVED);
		}else{
			inventity.setStatus(Invoice.CREATED);
		}
		
		
		List<SalesOrderProductLineItem>productlist=billingDocEntity.getSalesOrderProducts();
		ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();
		prodList.addAll(productlist);
		System.out.println();
		System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());

		List<ContractCharges>taxestable=billingDocEntity.getBillingTaxes();
		ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
		taxesList.addAll(taxestable);
		System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
		
		List<ContractCharges>otherChargesable=billingDocEntity.getBillingOtherCharges();
		ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
		otherchargesList.addAll(otherChargesable);
		System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
		inventity.setSalesOrderProductFromBilling(prodList);
		inventity.setBillingTaxes(taxesList);
		inventity.setBillingOtherCharges(otherchargesList);
		
		inventity.setCustomerBranch(billingDocEntity.getCustomerBranch());

		inventity.setAccountType(billingDocEntity.getAccountType());

		inventity.setArrPayTerms(billingDocEntity.getArrPayTerms());
		
		
		if(billingDocEntity.getNumberRange()!=null){
			inventity.setNumberRange(billingDocEntity.getNumberRange());	
		}
		
		/**
		 * Date : 11-10-2017 BY ANIL
		 */
		inventity.setOtherCharges(billingDocEntity.getOtherCharges());
		inventity.setTotalOtherCharges(billingDocEntity.getTotalOtherCharges());

		
		inventity.setTotalAmtExcludingTax(billingDocEntity.getTotalAmount());
		inventity.setFinalTotalAmt(billingDocEntity.getFinalTotalAmt());
		inventity.setTotalAmtIncludingTax(billingDocEntity.getTotalAmtIncludingTax());
//		inventity.setGrandTotalAmount(billingDocEntity.getGrandTotalAmount());
		
		
		/**
		 * End
		 */
		
//		if(billingDocEntity.getNumberRange()!=null)
//			inventity.setNumberRange(billingDocEntity.getNumberRange());
		
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(inventity);
		
		System.out.println("Here Invoice created Successfully");
		
		System.out.println("Invoice idd===="+inventity.getCount());
		
		billingDocEntity.setStatus(BillingDocument.BILLINGINVOICED);
		billingDocEntity.setInvoiceCount(inventity.getCount());
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(billingDocEntity);
		
		System.out.println("Here billing status updated");

		
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", inventity.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGINV).filter("companyId", inventity.getCompanyId()).filter("configStatus", true).first().now();
		}
		int flag =0;
		
		if(processConfig!=null){
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					flag = 1;
				}
			}
		}
		logger.log(Level.SEVERE,"Invoice before Call AI");
		if(flag == 1&&processConfig!=null){
			logger.log(Level.SEVERE,"Invoice before Call AI 1");
			InvoiceAccountIngInterface(inventity);
		}
		
		/**
		 * Ends here
		 */
		
		if(balancepayment.equalsIgnoreCase("ClosedPayment")){
			createpayementdocument(inventity);
		}
			
		
  }


	

	private void createpayementdocument(Invoice inventity) {

		System.out.println("This is for 100 % payment recieved so payment doc closed ");
		
		
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(inventity.getPersonInfo());
		paydetails.setContractCount(inventity.getContractCount());
		if(inventity.getContractStartDate()!=null){
			paydetails.setContractStartDate(inventity.getContractStartDate());
		}
		if(inventity.getContractEndDate()!=null){
			paydetails.setContractEndDate(inventity.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(inventity.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(inventity.getArrayBillingDocument());
		paydetails.setTotalBillingAmount(inventity.getTotalBillingAmount());
		paydetails.setInvoiceAmount(inventity.getInvoiceAmount());
		paydetails.setInvoiceCount(inventity.getCount());
		paydetails.setInvoiceDate(inventity.getInvoiceDate());
		paydetails.setInvoiceType(inventity.getInvoiceType());
		paydetails.setPaymentDate(inventity.getPaymentDate());
		
		 
		double invoiceamt=inventity.getInvoiceAmount();
		int payAmt=(int)(invoiceamt);
		paydetails.setPaymentAmt(payAmt);
		
		paydetails.setPaymentMethod(inventity.getPaymentMethod());
		paydetails.setTypeOfOrder(inventity.getTypeOfOrder());
		paydetails.setStatus("Closed");
		paydetails.setOrderCreationDate(inventity.getOrderCreationDate());
		paydetails.setBranch(inventity.getBranch());
		paydetails.setAccountType(inventity.getAccountType());
		paydetails.setEmployee(inventity.getEmployee());
		paydetails.setCompanyId(inventity.getCompanyId());
		paydetails.setOrderCformStatus(inventity.getOrderCformStatus());
		paydetails.setOrderCformPercent(inventity.getOrderCformPercent());
		
		/***************** vijay*********************/
//		if(inventity.getNumberRange()!=null)
//		paydetails.setNumberRange(inventity.getNumberRange());
		/*******************************************/
		paydetails.setPaymentReceived(payAmt);
		
		if(inventity.getNumberRange()!=null){
			paydetails.setNumberRange(inventity.getNumberRange());	
		}
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(paydetails);
		
		System.out.println("Payment created successfully");
		
	
		
	}
	
	/**
	 *   Accounting Interface for SalesOrder 
	 */
	
	private void quickSalesOrderccountingInterface(int salesOrderId, long companyId) {

		SalesOrder salesorderEntity = ofy().load().type(SalesOrder.class).filter("companyId", companyId).filter("count", salesOrderId).first().now();

		 if(salesorderEntity.getCount()!=0)
		 {
			for(int  i = 0;i<salesorderEntity.getItems().size();i++){
				
					String unitofmeasurement = salesorderEntity.getItems().get(i).getUnitOfMeasurement();
					int prodId=salesorderEntity.getItems().get(i).getPrduct().getCount();
					String productCode = salesorderEntity.getItems().get(i).getProductCode();
					String productName = salesorderEntity.getItems().get(i).getProductName();
					double productQuantity = salesorderEntity.getItems().get(i).getQty();
					double taxamount = removeTaxAmt(salesorderEntity.getItems().get(i).getPrduct());
					double productprice = (salesorderEntity.getItems().get(i).getPrice()-taxamount);
					double totalAmount =(salesorderEntity.getItems().get(i).getPrice()-taxamount)*salesorderEntity.getItems().get(i).getQty();
					
					double vatPercent=0;
					double calculatedamt = 0;
					double serviceTax=0 ;
					double calculetedservice = 0;
					double cformamtService=0;
					 
					String cform = "";
					double cformPercent=0;
					double cformAmount=0;
					
//					double vatAmount=0; 
					 
					int vatglaccno=0;
					int serglaccno=0;
					int cformglaccno=0;
					 
				/********************************************for cform*******************************************/
					
					 if(salesorderEntity.getCformstatus()!= null&&salesorderEntity.getCformstatus().trim().equals(AppConstants.YES)){
						 cform=AppConstants.YES;
						 cformPercent=salesorderEntity.getCstpercent();
						 cformAmount=salesorderEntity.getCstpercent()*totalAmount/100;
						 cformamtService=salesorderEntity.getCstpercent()*totalAmount/100;
						 if(salesorderEntity.getItems().get(i).getServiceTax().getPercentage()!=0){
							 serviceTax=salesorderEntity.getItems().get(i).getServiceTax().getPercentage();
							 calculetedservice=((cformamtService+totalAmount)*salesorderEntity.getItems().get(i).getServiceTax().getPercentage())/100;
						 }
						 
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",companyId).filter("taxChargePercent",salesorderEntity.getCstpercent()).filter("isCentralTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",companyId).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 cformglaccno=serglAcc.getGlAccountNo();
					 }
					 
					 else if(salesorderEntity.getCformstatus()!= null&&salesorderEntity.getCformstatus().trim().equals(AppConstants.NO)){
						 cform=AppConstants.NO;
						 cformPercent=salesorderEntity.getItems().get(i).getVatTax().getPercentage();
						 cformAmount=salesorderEntity.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
						 cformamtService=salesorderEntity.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
						 
						 if(salesorderEntity.getItems().get(i).getServiceTax().getPercentage()!=0){
							 serviceTax=salesorderEntity.getItems().get(i).getServiceTax().getPercentage();
							 calculetedservice=((cformamtService+totalAmount)*salesorderEntity.getItems().get(i).getServiceTax().getPercentage())/100;
						 }
						 
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",companyId).filter("taxChargeName",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",companyId).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 cformglaccno=serglAcc.getGlAccountNo();
					 }
					 else{//if the customer is of same state
						 
						 if(salesorderEntity.getItems().get(i).getVatTax().getPercentage()!=0)
						 {
							 vatPercent=salesorderEntity.getItems().get(i).getVatTax().getPercentage();
							 calculatedamt=salesorderEntity.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",companyId).filter("taxChargePercent",salesorderEntity.getItems().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",companyId).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
							 vatglaccno=vatglAcc.getGlAccountNo();
						 }
						 
						 if(salesorderEntity.getItems().get(i).getServiceTax().getPercentage()!=0)
						 {
							 serviceTax=salesorderEntity.getItems().get(i).getServiceTax().getPercentage();
							 calculetedservice=(calculatedamt+totalAmount)*salesorderEntity.getItems().get(i).getServiceTax().getPercentage()/100;
						 }
					 }
					
					 
					 if(salesorderEntity.getItems().get(i).getServiceTax().getPercentage()!=0)
					 {
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",companyId).filter("taxChargePercent",salesorderEntity.getItems().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",companyId).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 serglaccno=serglAcc.getGlAccountNo();
					 }
					 
					 double netPayable = salesorderEntity.getNetpayable();
					 
					 String[] oc = new String[12];
					 double[] oca = new double[12];
					 
					 System.out.println("qwedsa"+salesorderEntity.getProductCharges().size());
					 for(int k = 0; k < salesorderEntity.getProductCharges().size();k++){
						 oc[k] = salesorderEntity.getProductCharges().get(k).getChargeName();
						 oca[k] = salesorderEntity.getProductCharges().get(k).getChargePayable();
					 }
					 
					 String numberRange="";
					 if(salesorderEntity.getNumberRange()!=null){
						 numberRange = salesorderEntity.getNumberRange();
					 }
					 
					 /**********************************End************************************************/
					 UpdateAccountingInterface.updateTally
					 (
							 new Date(),   								//accountingInterfaceCreationDate
							 salesorderEntity.getEmployee(),						//	accountingInterfaceCreatedBy
							 salesorderEntity.getStatus(),							//  document status
							 AppConstants.STATUS ,						//	Status
							 "",						//	Remark
							 "Sales",//	Module
							 "Sales Order",			//	documentType				
							 salesorderEntity.getCount(),		//	documentID				
								" ", 					//	documentTitle			
								salesorderEntity.getCreationDate(),		//	documentDate			
								AppConstants.REFGLACCOUNTSO,			//	doucmentGL				
								salesorderEntity.getRefNo()+"",			//	referenceDocumentNumber1				
								salesorderEntity.getReferenceDate(),			//	referenceDocumentDate1
								AppConstants.REFDOCSO,					//	referenceDocumentType
								"",										//	referenceDocumentNumber2
								null,										//	referencedocumentDate2
								"",										//	referencedocumentType2
								AppConstants.ACCOUNTTYPE,				//	accountType
								salesorderEntity.getCinfo().getCount(),				//	customerID
								salesorderEntity.getCinfo().getFullName(),			//	customerName
								salesorderEntity.getCinfo().getCellNumber(),		//	customerCell
								0,										//vendorID
								"",										//	vendorName
								0,										//	vendorCell	
								0,											//employeeID
								"",									//employeeName
								salesorderEntity.getBranch(),						//	Branch
								salesorderEntity.getEmployee(),						//personResponsible
								"",											//requestedBy
								salesorderEntity.getApproverName(),					//approvedBy
								salesorderEntity.getPaymentMethod(),			//paymentMethod
								null,									//paymentDate		
								"",										//	chequeNumber
								null,									//	chequeDate
								null,									//bankName
								null,										//bankAccount
								0,										//transferReferenceNumber
								null,									//transactionDate
								null,									// contract start date
								null,									// contract end date
								prodId,										//	productID	
								productCode,							//productCode
								productName,						//productName
								productQuantity,						//Quantity
								null,						//	productDate
								0,										//	duration
								0,										//services
								unitofmeasurement,									//	unitOfMeasurement		
								productprice, 									//productPrice
								vatPercent,								//VATpercent
								calculatedamt,								//VATamount
								vatglaccno,								//	VATglAccount
								serviceTax,									//	serviceTaxPercent
								calculetedservice,					//	serviceTaxAmount
								serglaccno,								//serviceTaxGLaccount
								cform,										//cForm
								cformPercent,						//	cFormPercent
								cformAmount,						//cFormAmount
								cformglaccno,							//cFormGlACcount
								totalAmount,							// totalAmount		
								netPayable,								//		netPayable	
								"",//Contract Category
								0,									//amountRecieved
								0.0,                               //  base Amt taken in payment for tds by rohan  
								0,                                 //  tds percentage by rohan 
								0,                                 //  tds amount by rohan 
								oc[0],										
								oca[0],									
								oc[1],									
								oca[1],										
								oc[2],										
								oca[2],										
								oc[3],										
								oca[3],									
								oc[4],
								oca[4],
								oc[5],
								oca[5],
								oc[6],
								oca[6],
								oc[7],
								oca[7],
								oc[8],
								oca[8],
								oc[9],
								oca[9],
								oc[10],
								oca[10],
								oc[11],
								oca[11],
								salesorderEntity.getShippingAddress().getAddrLine1(),
								salesorderEntity.getShippingAddress().getLocality(),
								salesorderEntity.getShippingAddress().getLandmark(),
								salesorderEntity.getShippingAddress().getCountry(),
								salesorderEntity.getShippingAddress().getState(),
								salesorderEntity.getShippingAddress().getCity(),
								salesorderEntity.getShippingAddress().getPin(),
								salesorderEntity.getCompanyId(),
								null,				//  billing from date (rohan)
								null,		//  billing to date (rohan)
								"", //Warehouse
								"",				//warehouseCode
								"",				//ProductRefId
								"",				//Direction
								"",				//sourceSystem
								"",				//Destination System
								null,
								null,
								numberRange
							);
					 
				}
		 	}
		 
		
	}
	
	/**
	 * Invoice Accounting Interface
	 */
	
//	private void InvoiceAccountIngInterface(Invoice inventity) {
//		// TODO Auto-generated method stub
//		for(int  i = 0;i<inventity.getSalesOrderProducts().size();i++){
//			
//			Customer cust=null;
//			Vendor vendor=null;
//			Employee emp=null;
//			
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
//				cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//			}
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
//				cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//			}				
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//				vendor=ofy().load().type(Vendor.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
//			}
//			
//			
//			Date docCreationDate=null;
//			 int prodId=inventity.getSalesOrderProducts().get(i).getProdId();
//			 String productCode = inventity.getSalesOrderProducts().get(i).getProdCode();
//			 String productName = inventity.getSalesOrderProducts().get(i).getProdName();
//			 double productQuantity = inventity.getSalesOrderProducts().get(i).getQuantity();
//				 double productprice = (inventity.getSalesOrderProducts().get(i).getPrice());
//			 String unitofmeasurement = inventity.getSalesOrderProducts().get(i).getUnitOfMeasurement();
//			 //  rohan commented this code 
////			 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
//			
//			 //   rohan added new code for calculations 
//			 double totalAmount = inventity.getSalesOrderProducts().get(i).getBaseBillingAmount();
//			 
//			 double calculatedamt = 0;
//			 double calculetedservice =0;
//			 	
//			 int orderDuration = inventity.getSalesOrderProducts().get(i).getOrderDuration();
//			 int orderServices = inventity.getSalesOrderProducts().get(i).getOrderServices();
//			 
//			 double cformamtService = 0;
//			 double serviceTax=0 ;
//			 double cformPercent=0;
//			 double cformAmount=0;					
//			 double vatPercent=0;
//			 String cform = "";
//			 String refDocType="";
//			 double cformP=0;
//			 String accountType="";
//			 Date contractStartDate=null;
//			 Date contractEndDate=null;
//			 
//			 int vatglaccno=0;
//			 int serglaccno=0;
//			 int cformglaccno=0;
//			 
//			 int personInfoId = 0;
//			 String personInfoName="";
//			 long personInfoCell = 0;
//			 int vendorInfoId = 0;
//			 String vendorInfoName="";
//			 long vendorInfoCell=0;
//			 int empInfoId;
//			 String empInfoName="";
//			 long empinfoCell;
//			 
//			 String addrLine1="";
//			 String addrLocality="";
//			 String addrLandmark="";
//			 String addrCountry="";
//			 String addrState="";
//			 String addrCity="";
//			 long addrPin=0;
///************************************for document type********************************************/			 
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
//				docCreationDate=inventity.getOrderCreationDate();
//				refDocType=AppConstants.REFDOCSO;
//				personInfoId=inventity.getPersonInfo().getCount();
//				personInfoName=inventity.getPersonInfo().getFullName();
//				personInfoCell=inventity.getPersonInfo().getCellNumber();
//				
//				 addrLine1=cust.getAdress().getAddrLine1();
//				 addrLocality=cust.getAdress().getLocality();
//				 addrLandmark=cust.getAdress().getLandmark();
//				 addrCountry=cust.getAdress().getCountry();
//				 addrState=cust.getAdress().getState();
//				 addrCity=cust.getAdress().getCity();
//				 addrPin=cust.getAdress().getPin();
//				
//				if(inventity.getOrderCformStatus()!=null){
//					cform=inventity.getOrderCformStatus();
//					cformP=inventity.getOrderCformPercent();
//				}
//			}
//			
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
//				docCreationDate=inventity.getOrderCreationDate();
//				refDocType=AppConstants.REFDOCCONTRACT;
//				personInfoId=inventity.getPersonInfo().getCount();
//				personInfoName=inventity.getPersonInfo().getFullName();
//				personInfoCell=inventity.getPersonInfo().getCellNumber();
//			
//				addrLine1=cust.getAdress().getAddrLine1();
//				addrLocality=cust.getAdress().getLocality();
//				addrLandmark=cust.getAdress().getLandmark();
//				addrCountry=cust.getAdress().getCountry();
//				addrState=cust.getAdress().getState();
//				addrCity=cust.getAdress().getCity();
//				addrPin=cust.getAdress().getPin();
//				contractStartDate=inventity.getContractStartDate();
//				contractEndDate=inventity.getContractEndDate();
//			}
//			
//			if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//				docCreationDate=inventity.getOrderCreationDate();
//				refDocType=AppConstants.REFDOCPO;
//				
//				vendorInfoId=inventity.getPersonInfo().getCount();
//				vendorInfoName=inventity.getPersonInfo().getFullName();
//				vendorInfoCell=inventity.getCellNumber();
//				
//				addrLine1=vendor.getPrimaryAddress().getAddrLine1();
//				addrLocality=vendor.getPrimaryAddress().getLocality();
//				addrLandmark=vendor.getPrimaryAddress().getLandmark();
//				addrCountry=vendor.getPrimaryAddress().getCountry();
//				addrState=vendor.getPrimaryAddress().getState();
//				addrCity=vendor.getPrimaryAddress().getCity();
//				addrPin=vendor.getPrimaryAddress().getPin();
//				
//				if(inventity.getOrderCformStatus()!=null){
//					cform=inventity.getOrderCformStatus();
//					cformP=inventity.getOrderCformPercent();
//				}
//			}
//			
//		/*********************************for cform*************************************************/	
//			
//			 if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.YES)){
//				 cform=AppConstants.YES;
//				 cformPercent=inventity.getOrderCformPercent();
//				 cformAmount=inventity.getOrderCformPercent()*totalAmount/100;
//				 cformamtService=inventity.getOrderCformPercent()*totalAmount/100;
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//					 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//					 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//				 }
//				 
//				 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//				 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//				 cformglaccno=serglAcc.getGlAccountNo();
//			 }
//			 
//			 else if(inventity.getOrderCformStatus()!= null&inventity.getOrderCformStatus().trim().equals(AppConstants.NO)){
//				 cform=AppConstants.NO;
//				 cformPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//				 cformAmount=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//				 cformamtService=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//					 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//					 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//				 }
//				 
//				 
//				 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//				 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//				 vatglaccno=vatglAcc.getGlAccountNo();
//			 }
//			 
//			 
//			 	else{//if the customer is of same state
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
//				 {
//					 vatPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//					 calculatedamt=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
//				 }
//				 
//				 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
//				 {
//					 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//					 calculetedservice=(calculatedamt+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
//				 }
//				 }
//			 
//			 
//			 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
//			 {
//				 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargePercent",inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
//				 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",inventity.getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//				 serglaccno=serglAcc.getGlAccountNo();
//			 }
//
//			 /*****************************************************************************************/
//			
//			 //  old code 
////			 double netPayable = getTotalSalesAmount();
//			 //  new code by rohan as there is calculations error
//			 double netPayable = inventity.getNetPayable();
//			 
//			 
//			 String[] oc = new String[12];
//			 double[] oca = new double[12];
//			 
//			 
////			 if(this.getBillingOtherCharges().size()!=0){
//				 for(int k = 0; k < inventity.getBillingOtherCharges().size();k++){
//					 oc[k] = inventity.getBillingOtherCharges().get(k).getTaxChargeName();
//					 oca[k] = inventity.getBillingOtherCharges().get(k).getPayableAmt();
//				 }
////			 }
//			 /**********************************End************************************************/
//			 
//			 logger.log(Level.SEVERE,"Before Invoice Accounting Interface Call");
//			 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
//					 									inventity.getEmployee(),//accountingInterfaceCreatedBy
//					 									inventity.getStatus(),
//													AppConstants.STATUS ,//Status
//													"",//Remark
//													"Accounts",//Module
//													"Invoice",//documentType
//													inventity.getCount(),//documentID
//														" ", //documentTitle
//														inventity.getInvoiceDate(),//documentDate
//														inventity.getAccountType().trim()+"-Invoice",//documentGl
//														inventity.getContractCount()+"",//referenceDocumentNumber1
//														docCreationDate,//referenceDocumentDate1
//														refDocType,//referenceDocumentType
//														"",//referenceDoc.no.2
//														null,//reference doc.date.2
//														"",//reference doc type no2.
//														inventity.getAccountType(),//accountType	
//														personInfoId,//custID	
//														personInfoName,//custName	
//														personInfoCell,//custCell
//														vendorInfoId,//venID	
//														vendorInfoName,//venName	
//														vendorInfoCell,//venCell
//														0,//emplID
//														" ",//emplName
//														inventity.getBranch(),//branch
//														inventity.getEmployee(),//personResponsible
//														"",//requestdBy
//														inventity.getApproverName(),//AproverName
//														inventity.getPaymentMethod(),//PaymentMethod
//														inventity.getPaymentDate(),//PaymentDate
//														"",//Cheq no.
//														null,//cheq date.
//														null,//Bank Name
//														null,//bankAccount
//														0,//transferReferenceNumber
//														null,//transactionDate
//														contractStartDate,  //contract start date
//														contractEndDate,  // contract end date
//														prodId,//prodID
//														productCode,//prodCode
//														productName, //prodNAme
//														productQuantity,//prodQuant
//														inventity.getContractStartDate(),//Prod DAte
//														orderDuration,//duration	
//														orderServices,//services
//														unitofmeasurement,//UOM
//														productprice, //prod Price
//														vatPercent,//vat%
//														calculatedamt,//vatamt
//														vatglaccno,//VATglAccount
//														serviceTax,//serviceTaxPercent
//														calculetedservice,//serviceTaxAmount
//														serglaccno,//serviceTaxGLaccount
//														cform,//cform
//														cformPercent,//cform%
//														cformAmount,//cformamt
//														cformglaccno, //cformglacc
//														totalAmount,//totalamt
//														netPayable,//netpay
//														"",//Contract Category
//														0,			//amountRecieved
//														0.0,     // Base Amount taken in payment for tds calculation by rohan 
//														0,   //  tds percentage by rohan 
//														0,   //  tds amount by rohan 
//														oc[0],
//														oca[0],
//														oc[1],
//														oca[1],
//														oc[2],
//														oca[2],
//														oc[3],
//														oca[3],
//														oc[4],
//														oca[4],
//														oc[5],
//														oca[5],
//														oc[6],
//														oca[6],
//														oc[7],
//														oca[7],
//														oc[8],
//														oca[8],
//														oc[9],
//														oca[9],
//														oc[10],
//														oca[10],
//														oc[11],
//														oca[11],
//														addrLine1,
//														addrLocality,
//														addrLandmark,
//														addrCountry,
//														addrState,
//														addrCity,
//														addrPin,
//														inventity.getCompanyId(),
//														inventity.getBillingPeroidFromDate(),//  billing from date (rohan)
//														inventity.getBillingPeroidToDate(),//  billing to date (rohan)
//														"", //Warehouse
//														"",				//warehouseCode
//														"",				//ProductRefId
//														"",				//Direction
//														"",				//sourceSystem
//														""				//Destination Syste
//														);
//			 
//		
//		}
//		
//}
	
/**
 * ends here	
 */
	private void InvoiceAccountIngInterface(Invoice inventity) {
//		// TODO Auto-generated method stub

		/** date 23.6.2018 added by komal for new accountion interface changes**/
		if(inventity.getCount()!=0&&inventity.getStatus().equals(Invoice.APPROVED)){
	
			
			for(int  i = 0;i<inventity.getSalesOrderProducts().size();i++){
				
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					vendor=ofy().load().type(Vendor.class).filter("companyId",inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
				}
				
				
				Date docCreationDate=null;
				 int prodId=inventity.getSalesOrderProducts().get(i).getProdId();
				 String productCode = inventity.getSalesOrderProducts().get(i).getProdCode();
				 String productName = inventity.getSalesOrderProducts().get(i).getProdName();
				 double productQuantity = inventity.getSalesOrderProducts().get(i).getQuantity();
 				 double productprice = (inventity.getSalesOrderProducts().get(i).getPrice());
				 String unitofmeasurement = inventity.getSalesOrderProducts().get(i).getUnitOfMeasurement();
				 //  rohan commented this code 
//				 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
				
				 //   rohan added new code for calculations 
				 double totalAmount = inventity.getSalesOrderProducts().get(i).getBaseBillingAmount();
				 
				 double calculatedamt = 0;
				 double calculetedservice =0;
				 	
				 int orderDuration = inventity.getSalesOrderProducts().get(i).getOrderDuration();
				 int orderServices = inventity.getSalesOrderProducts().get(i).getOrderServices();
				 
				 double cformamtService = 0;
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;					
				 double vatPercent=0;
				 String cform = "";
				 String refDocType="";
				 double cformP=0;
				 String accountType="";
				 Date contractStartDate=null;
				 Date contractEndDate=null;
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
				 int personInfoId = 0;
				 String personInfoName="";
				 long personInfoCell = 0;
				 int vendorInfoId = 0;
				 String vendorInfoName="";
				 long vendorInfoCell=0;
				 int empInfoId;
				 String empInfoName="";
				 long empinfoCell;
				 
				 String addrLine1="";
				 String addrLocality="";
				 String addrLandmark="";
				 String addrCountry="";
				 String addrState="";
				 String addrCity="";
				 long addrPin=0;
				 
				 /** date 29/3/2018 added by komal for tax name **/
				 String taxvatName="";
				 String serTaxName = "";
				 String glAccountName1 = "";
				 String glAccountName2 = ""; 
				 /** date 29.5.2018 added by komal for billing address **/
				 String saddrLine1 ="";
				 String addrLine2 ="";
				 String saddrLine2 ="";
				 String saddrLocality = "";
				 String saddrLandmark = "";
				 String saddrCountry = "";
				 String saddrState="";
				 String saddrCity="";
				 long saddrPin = 0;
	/************************************for document type********************************************/			 
				 
				 
				 
				 
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCSO;
					personInfoId=inventity.getPersonInfo().getCount();
					personInfoName=inventity.getPersonInfo().getFullName();
					personInfoCell=inventity.getPersonInfo().getCellNumber();
					
					 addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
					 
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
					
					if(inventity.getOrderCformStatus()!=null){
						cform=inventity.getOrderCformStatus();
						cformP=inventity.getOrderCformPercent();
					}
				}
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCCONTRACT;
					personInfoId=inventity.getPersonInfo().getCount();
					personInfoName=inventity.getPersonInfo().getFullName();
					personInfoCell=inventity.getPersonInfo().getCellNumber();
				
					addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					addrLocality=cust.getAdress().getLocality();
					addrLandmark=cust.getAdress().getLandmark();
					addrCountry=cust.getAdress().getCountry();
					addrState=cust.getAdress().getState();
					addrCity=cust.getAdress().getCity();
					addrPin=cust.getAdress().getPin();
					contractStartDate=inventity.getContractStartDate();
					contractEndDate=inventity.getContractEndDate();
					
					 addrPin=cust.getAdress().getPin();
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
				}
				
				if(inventity.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					docCreationDate=inventity.getOrderCreationDate();
					refDocType=AppConstants.REFDOCPO;
					
					vendorInfoId=inventity.getPersonInfo().getCount();
					vendorInfoName=inventity.getPersonInfo().getFullName();
					vendorInfoCell=inventity.getCellNumber();
					
					addrLine1=vendor.getPrimaryAddress().getAddrLine1();
					/** date 29.5.2018 added by komal for billing address **/
					addrLine2=vendor.getPrimaryAddress().getAddrLine2();
					addrLocality=vendor.getPrimaryAddress().getLocality();
					addrLandmark=vendor.getPrimaryAddress().getLandmark();
					addrCountry=vendor.getPrimaryAddress().getCountry();
					addrState=vendor.getPrimaryAddress().getState();
					addrCity=vendor.getPrimaryAddress().getCity();
					addrPin=vendor.getPrimaryAddress().getPin();
					
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=vendor.getSecondaryAddress().getAddrLine1();
					saddrLine2=vendor.getSecondaryAddress().getAddrLine2();
					saddrLocality=vendor.getSecondaryAddress().getLocality();
					saddrLandmark=vendor.getSecondaryAddress().getLandmark();
					saddrCountry=vendor.getSecondaryAddress().getCountry();
					saddrState=vendor.getSecondaryAddress().getState();
					saddrCity=vendor.getSecondaryAddress().getCity();
					saddrPin=vendor.getSecondaryAddress().getPin();
					
					if(inventity.getOrderCformStatus()!=null){
						cform=inventity.getOrderCformStatus();
						cformP=inventity.getOrderCformPercent();
					}
				}
				
				
				
			/*********************************for cform*************************************************/	
				
				 if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=inventity.getOrderCformPercent();
					 cformAmount=inventity.getOrderCformPercent()*totalAmount/100;
					 cformamtService=inventity.getOrderCformPercent()*totalAmount/100;
					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//					 cformglaccno=serglAcc.getGlAccountNo();
					 cformglaccno=0;
				 }
				 
				 else if(inventity.getOrderCformStatus()!= null&&inventity.getOrderCformStatus().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
					 cformAmount=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 cformamtService=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 
					 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
					 }
					 
					 
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
					 vatglaccno=0;
				 }
				 
				 
				 
				 	else{//if the customer is of same state
					 
						 if(inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
						 {
							 vatPercent=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage();
							 calculatedamt=inventity.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
							 //date 29.3.2018 added by komal
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargeName",inventity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName()).first().now();
							 if(vattaxDtls != null)	
							 glAccountName2 = vattaxDtls.getGlAccountName();
//							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//							 vatglaccno=vatglAcc.getGlAccountNo();
							 vatglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							// String taxvatName="";
									if(inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName()!=null && !inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName().equals("")){
										taxvatName =inventity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName();
									}
									else{
										taxvatName =inventity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName();
									}
						 }
						 
						 
						
						 				 
						 if(inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
						 {
							 serviceTax=inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
							 calculetedservice=(totalAmount)*inventity.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
							//date 29.3.2018 added by komal 
							 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",inventity.getCompanyId()).filter("taxChargeName",inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName()).first().now();
							 if(sertaxDtls != null)
							 glAccountName1 = sertaxDtls.getGlAccountName();
//							 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//							 serglaccno=serglAcc.getGlAccountNo();
							 serglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							 /** date 03/04/2018 changed by komal  vat tax to service tax**/
							 //String serTaxName="";
								if(inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName()!=null && !inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName().equals("")){
									serTaxName =inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName();
								}
								else{
									serTaxName =inventity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName();
								}
						 }
				 	}
				 
				 
				 
				

				 /*****************************************************************************************/
				
				 //  old code 
//				 double netPayable = getTotalSalesAmount();
				 //  new code by rohan as there is calculations error
				 double netPayable = inventity.getNetPayable();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
//				 if(this.getBillingOtherCharges().size()!=0){
					 for(int k = 0; k < inventity.getBillingOtherCharges().size();k++){
						 oc[k] = inventity.getBillingOtherCharges().get(k).getTaxChargeName();
						 oca[k] = inventity.getBillingOtherCharges().get(k).getPayableAmt();
					 }
//				 }
					 
				/**
				 * Date : 01-11-2017 BY ANIL
				 * Setting invoice comment to accounting interface remark	 
				 */
				String invoiceComment="";
				if(inventity.getComment()!=null){
					invoiceComment=inventity.getComment();
				}
				Logger logger = Logger.getLogger("Name of logger");
				logger.log(Level.SEVERE,"Invoice Group"+inventity.getInvoiceGroup());
				/**
				 * End
				 */
				/** date 29/03/2018 added by  komal for new tally interface requirement**/
				String gstn = "";
				double discountAmount = inventity.getDiscountAmt();
				double indirectExpenses = inventity.getTotalOtherCharges();
				if(inventity.getGstinNumber()!=null){
					gstn = inventity.getGstinNumber();
				}else{
					if(inventity.getAccountType().equals("AR")){
						System.out.println("id ");
						try{
							Customer customer=ofy().load().type(Customer.class).filter("companyId", inventity.getCompanyId()).filter("count", inventity.getPersonInfo().getCount()).first().now();
							if(customer!=null){
								System.out.println("id222222222222222 ");
								if(customer.getArticleTypeDetails()!=null){  
									System.out.println("id 33333333333333333");
									for(ArticleType object:customer.getArticleTypeDetails()){
										System.out.println("id 4444444444444444");  
										if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
											System.out.println("id 555555555555555555555");
											inventity.setGstinNumber(object.getArticleTypeValue());
											break;
										}
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					gstn = inventity.getGstinNumber();
				}
		       String hsnNumber = "";
		       SuperProduct superProduct = ofy().load().type(SuperProduct.class).filter("companyId",inventity.getCompanyId()).filter("productCode", inventity.getSalesOrderProducts().get(i).getProdCode()).first().now();
		       if(superProduct.getHsnNumber()!=null){
		    	   hsnNumber = superProduct.getHsnNumber();
		       }
		     
		    	 personInfoName=inventity.getPersonInfo().getFullName(); 
		    	
		    	 /**
		    	  * end komal
		    	  */
					 
				 /**********************************End************************************************/
				 
		    	 String numberRange="";
				 if(inventity.getNumberRange()!=null){
					 numberRange = inventity.getNumberRange();
				 }
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
						 								inventity.getEmployee(),//accountingInterfaceCreatedBy
						 								inventity.getStatus(),
														AppConstants.STATUS ,//Status
														invoiceComment,//Remark
														"Accounts",//Module
														"Invoice",//documentType
														inventity.getCount(),//documentID
															" ", //documentTitle
															inventity.getInvoiceDate(),//documentDate
															inventity.getAccountType().trim()+"-Invoice",//documentGl
															inventity.getContractCount()+"",//referenceDocumentNumber1
															docCreationDate,//referenceDocumentDate1
															refDocType,//referenceDocumentType
															inventity.getRefNumber(),//referenceDoc.no.2 Date 09-11-2017 this is sap customer ref num for NBHC
															null,//reference doc.date.2
															"",//reference doc type no2.
															inventity.getAccountType(),//accountType	
															personInfoId,//custID	
															personInfoName,//custName	
															personInfoCell,//custCell
															vendorInfoId,//venID	
															vendorInfoName,//venName	
															vendorInfoCell,//venCell
															0,//emplID
															" ",//emplName
															inventity.getBranch(),//branch
															inventity.getEmployee(),//personResponsible
															"",//requestdBy
															inventity.getApproverName(),//AproverName
															inventity.getPaymentMethod(),//PaymentMethod
															inventity.getPaymentDate(),//PaymentDate
															"",//Cheq no.
															null,//cheq date.
															null,//Bank Name
															null,//bankAccount
															0,//transferReferenceNumber
															null,//transactionDate
															contractStartDate,  //contract start date
															contractEndDate,  // contract end date
															prodId,//prodID
															productCode,//prodCode
															productName, //prodNAme
															productQuantity,//prodQuant
															inventity.getContractStartDate(),//Prod DAte
															orderDuration,//duration	
															orderServices,//services
															unitofmeasurement,//UOM
															productprice, //prod Price
															vatPercent,//vat%
															calculatedamt,//vatamt
															vatglaccno,//VATglAccount
															serviceTax,//serviceTaxPercent
															calculetedservice,//serviceTaxAmount
															serglaccno,//serviceTaxGLaccount
															cform,//cform
															cformPercent,//cform%
															cformAmount,//cformamt
															cformglaccno, //cformglacc
															totalAmount,//totalamt
															netPayable,//netpay
															inventity.getInvoiceGroup(),//Contract Category//27 fEB PReviously it was contract category now it is from invoice group
															0,			//amountRecieved
															0.0,     // Base Amount taken in payment for tds calculation by rohan 
															0,   //  tds percentage by rohan 
															0,   //  tds amount by rohan 
															oc[0],
															oca[0],
															oc[1],
															oca[1],
															oc[2],
															oca[2],
															oc[3],
															oca[3],
															oc[4],
															oca[4],
															oc[5],
															oca[5],
															oc[6],
															oca[6],
															oc[7],
															oca[7],
															oc[8],
															oca[8],
															oc[9],
															oca[9],
															oc[10],
															oca[10],
															oc[11],
															oca[11],
															addrLine1,
															addrLine2,
															addrLocality,
															addrLandmark,
															addrCountry,
															addrState,
															addrCity,
															addrPin,
															inventity.getCompanyId(),
															inventity.getBillingPeroidFromDate(),//  billing from date (rohan)
															inventity.getBillingPeroidToDate(),//  billing to date (rohan)
															"", //Warehouse
															"",				//warehouseCode
															"",				//ProductRefId
															"",				//Direction
															"",				//sourceSystem
															""	,			//Destination Syste
															hsnNumber, // hsc/Sac
															gstn, // gstn
															glAccountName1, // goupname1 for tax1
															glAccountName2, // groupname2 for tax 2
															serTaxName, // tax1 name
															taxvatName, // tax2 name
															discountAmount, // discount amount
															indirectExpenses, // indirect expenses
															saddrLine1,
															saddrLine2, 
															saddrLocality, 
															saddrLandmark,
															saddrCountry,
															saddrState,
															saddrCity, 
															saddrPin,
															null,null,null,null,
															null,null,null,null,null,null,numberRange
															);
				 
			
			}
			
		}
	
		
}
	
/**
 * ends here	
 */


}
