package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.CustomerNameChangeServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class CustomerNameChangeTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7917096107413570137L;
	Logger logger = Logger.getLogger("Name of logger");
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdf= new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)throws ServletException, IOException {
		 String operationName = request.getParameter("OperationName");
		 String compId = request.getParameter("companyId");
		 long companyId = 0;
		 if(compId!=null){
			 companyId = Long.parseLong(request.getParameter("companyId"));
		 }
		 
		 
		 switch (operationName) {
		 case "CustomerNameChangeUpdation":
				customerNameUpdation(request,companyId);
				break;
				
		 case "CustomerMailIDUpdateInContracts":
		       customerMailUpdation(request,companyId);
				break;
				
		 case "UpdateServiceAddressInServiceAndContract":
			 	updateServiceAddressInServicesAndContracts(request,companyId);
			 	break;
		 case "VendorUpdation":
			 	updateVendorDetails(request,companyId);
			 	break;
		 default:
				break;			
				
				
		 }
	}
	private void customerNameUpdation(HttpServletRequest request, long companyId) {
		logger.log(Level.SEVERE,"Inside Customername change tasqueue ");
		Customer customerInfo = new Customer();
		if(request.getParameter("isCompany").equals("Yes")){
			customerInfo.setCompany(true);
		}
		else{
			customerInfo.setCompany(false);
		}
		customerInfo.setCompanyName(request.getParameter("CompanyName"));
		customerInfo.setFullname(request.getParameter("FullName"));
		customerInfo.setCellNumber1(Long.parseLong(request.getParameter("CellNumber")));
		customerInfo.setCount(Integer.parseInt(request.getParameter("CustomerId")));
		customerInfo.setEmail(request.getParameter("email"));
		customerInfo.setCompanyId(companyId);
		
		String customerAddressUpdatedFlag = request.getParameter("updateCustomerAddress");
		
		if(customerAddressUpdatedFlag.equalsIgnoreCase("yes")){
			String updatedServiceAddress = request.getParameter("customerServiceAddress");
			Address serviceAddress = getUpdateAddress(updatedServiceAddress);
			customerInfo.setSecondaryAdress(serviceAddress);
			
			String updatedBillingAddress = request.getParameter("customerBillingAddress");
			Address billingAddress = getUpdateAddress(updatedBillingAddress);
			customerInfo.setAdress(billingAddress);
		}
		
		CustomerNameChangeServiceImpl customerNameUpdation = new CustomerNameChangeServiceImpl();
		customerNameUpdation.UpdateAllDocuments(customerInfo,customerAddressUpdatedFlag);
		
	}
	
   private Address getUpdateAddress(String updatedServiceAddress) {
	   String [] address = updatedServiceAddress.split("\\$@");
	   String addressLine1 = address[0];
	   String addressLine2 = address[1];
	   String landmark = address[2];
	   String locality = address[3];
	   String city = address[4];
	   String state = address[5];
	   String country = address[6];
	   long pin =0;
	   
	   if(address[7]!=null && !address[7].equals(""))
	   pin = Long.parseLong(address[7]);
	   
	   Address customeraddress = new Address();
	   customeraddress.setAddrLine1(addressLine1);
	   customeraddress.setAddrLine2(addressLine2);
	   customeraddress.setLandmark(landmark);
	   customeraddress.setLocality(locality);
	   customeraddress.setCity(city);
	   customeraddress.setState(state);
	   customeraddress.setCountry(country);
	   if(pin!=0)
	   customeraddress.setPin(pin);
	   
		return customeraddress;
	}
   
private void customerMailUpdation(HttpServletRequest request, long companyId) {
	   logger.log(Level.SEVERE,"Inside Customername change tasqueue ");
	   CustomerNameChangeServiceImpl customerNameUpdation = new CustomerNameChangeServiceImpl();
//		Customer customerInfo = new Customer();
		
		String fromDate1 = request.getParameter("fromDate").trim();
		String toDate1 = request.getParameter("toDate").trim();
//		String companyId=request.getParameter("companyId").trim();
		 
			Date fromDate = null , toDate = null;
			try {
				if(fromDate1 != null && !fromDate1.equalsIgnoreCase("null")){
					fromDate = isoFormat.parse(isoFormat.format(sdf.parse(fromDate1)));
					
				}
				if(toDate1 != null && !toDate1.equalsIgnoreCase("null") ){
					toDate = isoFormat.parse(isoFormat.format(sdf.parse(toDate1)));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//		customerInfo.setCompanyName(request.getParameter("CompanyName"));
////		customerInfo.setFullname(request.getParameter("FullName"));
//		customerInfo.setCellNumber1(Long.parseLong(request.getParameter("CellNumber")));
//		customerInfo.setCount(Integer.parseInt(request.getParameter("CustomerId")));
//		customerInfo.setEmail(request.getParameter("email"));
//		customerInfo.setCompanyId(companyId);
		
		
		customerNameUpdation.UpdateCustomerEmailInContracts(companyId,fromDate,toDate);
		
	}
	
	
	
	
	
		private void updateServiceAddressInServicesAndContracts(HttpServletRequest request, long companyId) {
			
			int customerId = Integer.parseInt(request.getParameter("customerId"));
			
			   CustomerNameChangeServiceImpl customerNameUpdation = new CustomerNameChangeServiceImpl();

			   customerNameUpdation.updateServiceAddressInServicesAndContracts(companyId,customerId);
		}
	
	
		private void updateVendorDetails(HttpServletRequest request, long companyId) {
			logger.log(Level.SEVERE, "in updateVendorDetails");

			int vendorId = Integer.parseInt(request.getParameter("vendorId"));
			
			Vendor vendor = ofy().load().type(Vendor.class).filter("companyId", companyId).filter("count", vendorId).first().now();
			if(vendor!=null) {
				List<CustomerPayment> customerpaymentList= ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
						.filter("personInfo.count", vendor.getCount())
						.filter("accountType", "AP").list();
				
				if(customerpaymentList!=null) {
					logger.log(Level.SEVERE, "customerpaymentList size="+customerpaymentList.size());
					String name=vendor.getVendorName();
					String poc=vendor.getfullName();
					long cell=0;
					if(vendor.getCellNumber1()!=null)
						cell=vendor.getCellNumber1();
					logger.log(Level.SEVERE, "vendor name="+name+ " cell="+cell);
					
					for(CustomerPayment cp:customerpaymentList) {
						cp.getPersonInfo().setFullName(name);
						cp.getPersonInfo().setCellNumber(cell);
						cp.getPersonInfo().setPocName(poc);
					}
					ofy().save().entities(customerpaymentList);
					logger.log(Level.SEVERE, "Vendor Name and Cell no updated in Payments");
					
				}
				
				List<BillingDocument> billingList= null;
				 billingList = ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("personInfo.count", vendor.getCount()).filter("accountType", "AP").list();
				 if(billingList!=null) {
						logger.log(Level.SEVERE, "billingList size="+billingList.size());
						String name=vendor.getVendorName();
						String poc=vendor.getfullName();
						long cell=0;
						if(vendor.getCellNumber1()!=null)
							cell=vendor.getCellNumber1();
						
						for(BillingDocument bill:billingList) {
							bill.getPersonInfo().setFullName(name);
							bill.getPersonInfo().setCellNumber(cell);
							bill.getPersonInfo().setPocName(poc);
						}
						ofy().save().entities(billingList);
						logger.log(Level.SEVERE, "Vendor Name and Cell no updated in Bills");
						
					}
				
				 List<VendorInvoice> invoiceList= null;
				 invoiceList = ofy().load().type(VendorInvoice.class).filter("companyId", companyId).filter("personInfo.count", vendor.getCount()).filter("accountType", "AP").list();
				 if(invoiceList!=null) {
						logger.log(Level.SEVERE, "invoiceList size="+invoiceList.size());
						String name=vendor.getVendorName();
						String poc=vendor.getfullName();
						long cell=0;
						if(vendor.getCellNumber1()!=null)
							cell=vendor.getCellNumber1();
						
						for(VendorInvoice invoice:invoiceList) {
							invoice.getPersonInfo().setFullName(name);
							invoice.getPersonInfo().setCellNumber(cell);
							invoice.getPersonInfo().setPocName(poc);
						}
						ofy().save().entities(invoiceList);
						logger.log(Level.SEVERE, "Vendor Name and Cell no updated in Invoices");
						
				}
				 
			}else
				logger.log(Level.SEVERE, "vendor "+vendorId+" not found");
			
		}
	
	
}
