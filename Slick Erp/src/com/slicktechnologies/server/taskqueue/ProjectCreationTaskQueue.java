package com.slicktechnologies.server.taskqueue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.TechnicianWarehouseDetails;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;

public class ProjectCreationTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8130192084591830629L;

	Logger logger=Logger.getLogger("Name Of logger");
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			
		
		String strcompanyId = req.getParameter("companyId").trim();
		String strserviceId = req.getParameter("serviceId").trim();
		long companyId = 0;
		int serviceId = 0;
		
		if(strcompanyId!=null && !strcompanyId.equals("")){
			companyId = Long.parseLong(strcompanyId);
		}
		if(strserviceId!=null && !strserviceId.equals("")){
			serviceId = Integer.parseInt(strserviceId);
		}
		logger.log(Level.SEVERE, "companyId"+companyId);
		logger.log(Level.SEVERE, "serviceId"+serviceId);

		if(companyId!=0 && serviceId!=0){
			
			Service serviceEntity = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId).first().now();
			logger.log(Level.SEVERE,"serviceEntity "+serviceEntity);
			if(serviceEntity!=null){
				
				ArrayList<EmployeeInfo> technicianlist = new ArrayList<EmployeeInfo>();
				Employee empEntity = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", serviceEntity.getEmployee())
										.first().now();
				logger.log(Level.SEVERE,"empEntity "+empEntity);
				if(empEntity!=null){
					EmployeeInfo empInfoEntity = new EmployeeInfo();
					empInfoEntity.setEmpCount(empEntity.getCount());
			  		empInfoEntity.setFullName(empEntity.getFullName());
			      	empInfoEntity.setCellNumber(empEntity.getCellNumber1());
			      	empInfoEntity.setDesignation(empEntity.getDesignation());
			      	empInfoEntity.setDepartment(empEntity.getDepartMent());
			      	empInfoEntity.setEmployeeType(empEntity.getEmployeeType());
			      	empInfoEntity.setEmployeerole(empEntity.getRoleName());
			      	empInfoEntity.setBranch(empEntity.getBranchName());
			      	empInfoEntity.setCountry(empEntity.getCountry());		
			      	technicianlist.add(empInfoEntity);
					
			      	List<TechnicianWareHouseDetailsList> technicianWarehouselist = ofy().load().type(TechnicianWareHouseDetailsList.class)
			      					.filter("wareHouseList.technicianName", serviceEntity.getEmployee()).list();
			      	logger.log(Level.SEVERE,"technicianWarehouselist "+technicianWarehouselist.size());
			      	
			      	String warehouseName = "" ,storageLocation = "", storageBin = "",
							parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
					if(technicianWarehouselist.size()!=0){
					
						for(TechnicianWarehouseDetails details : technicianWarehouselist.get(0).getWareHouseList()){
							if(details.getTechnicianName().equalsIgnoreCase(serviceEntity.getEmployee())){
								warehouseName = details.getTechnicianWareHouse();
								storageLocation = details.getTechnicianStorageLocation();
								storageBin = details.getTechnicianStorageBin();
								parentWareHouse = details.getParentWareHouse();
								parentStorageLocation = details.getParentStorageLocation();
								parentStorageBin = details.getPearentStorageBin();
								break;
							}
						}
					}
						
			      	logger.log(Level.SEVERE,"warehouseName "+warehouseName);
			      	logger.log(Level.SEVERE,"storageLocation "+storageLocation);
			      	logger.log(Level.SEVERE,"storageBin "+storageBin);
			      	logger.log(Level.SEVERE,"parentWareHouse "+parentWareHouse);
			      	logger.log(Level.SEVERE,"parentStorageLocation "+parentStorageLocation);
			      	logger.log(Level.SEVERE,"parentStorageBin "+parentStorageBin);

					if(!warehouseName.equals("") && !storageLocation.equals("") &&!storageBin.equals("") && !parentWareHouse.equals("") && !parentStorageLocation.equals("") && !parentStorageBin.equals("")){
						
						ArrayList<ProductGroupList> materialInfoList = new ArrayList<ProductGroupList>();
						for(ServiceProductGroupList serviceprodgroup : serviceEntity.getServiceProductList()){
							ProductGroupList technicianMaterialIfno  = new ProductGroupList();
							technicianMaterialIfno.setName(serviceprodgroup.getName());
							technicianMaterialIfno.setQuantity(serviceprodgroup.getQuantity());
							technicianMaterialIfno.setPlannedQty(serviceprodgroup.getPlannedQty());
							technicianMaterialIfno.setProActualQty(serviceprodgroup.getQuantity());
							technicianMaterialIfno.setProduct_id(serviceprodgroup.getProduct_id());
							technicianMaterialIfno.setCode(serviceprodgroup.getCode());
							technicianMaterialIfno.setUnit(serviceprodgroup.getUnit());
							technicianMaterialIfno.setPrice(serviceprodgroup.getPrice());
							technicianMaterialIfno.setWarehouse(warehouseName);
							technicianMaterialIfno.setStorageLocation(storageLocation);
							technicianMaterialIfno.setStorageBin(storageBin);
							technicianMaterialIfno.setParentWarentwarehouse(parentWareHouse);
							technicianMaterialIfno.setParentStorageLocation(parentStorageLocation);
							technicianMaterialIfno.setParentStorageBin(parentStorageBin);
							materialInfoList.add(technicianMaterialIfno);
						}
				      	logger.log(Level.SEVERE,"materialInfoList"+materialInfoList.size());
				      	if(materialInfoList.size()!=0){
				      		GeneralServiceImpl genral = new GeneralServiceImpl();
							String invoiceDate = null;
							int id = genral.createProject(serviceEntity,technicianlist,
									null,materialInfoList,serviceEntity.getServiceDate(),serviceEntity.getServiceTime(), 0, invoiceDate, serviceEntity.getEmployee(),0);
							logger.log(Level.SEVERE,"id "+id);
				      	}
						
					}
					
				}

			}
			
		}
		
		} catch (Exception e) {
			
	      	logger.log(Level.SEVERE,"Exception "+e.getLocalizedMessage());

		}
	}

	

}
