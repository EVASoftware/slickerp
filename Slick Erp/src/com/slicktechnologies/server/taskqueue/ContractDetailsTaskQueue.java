package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.sql.Array;
import java.sql.Driver;
import java.sql.DriverManager;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.server.ContractInvoicePayment;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CustomerContractDetails;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class ContractDetailsTaskQueue extends HttpServlet {

	/**
	 * do this first
	 * 
	 * service.java

	Service() --  construsctor
	
	refNo = "";
	
	7-09-2017
	
	salesorderproducttable class for display table
	 */
	
	Logger logger=Logger.getLogger("Services Creation On Approval");
	private static final long serialVersionUID = -6952518224000713009L;
	SalesLineItemTable saleslineitemtable;
	ProductTaxesTable productTaxTable;
	//int serCount;
	public  NumberGeneration contractSrNo,customerSrNo,  serNo, billNumGe;
	public static String jsonStr ;
	public static ArrayList<String> refStr;
	boolean billStatus = false,serviceStatus = false;
	GenricServiceImpl impl = new GenricServiceImpl();
	private static SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	boolean custEntry = true, contEntry = true, serEntry = true, billingEntry = true;
	boolean lastObj ;
	
	/**
	 *  2-09-2017
	 *  for customer contract and service upload process
	 */
	
	public static int serCount, contCount, custCount, billCount;
	
	int customerCount ;
	int contractCount ;
	int billingCount  ; 
	int serviceCount;
	
	public  List<Customer> cust ;
	
	public  ServiceProduct pro;
	String modifiedDate;
//	public static NumberGeneration contractSrNo,customerSrNo,  serNo, billNumGe;
	/**
	 *  end here...
	 */
	
	/**
	 *  nidhi
	 *   18-09-2017
	 *   
	 */
	String loginPerson = "";
	@SuppressWarnings("unused")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		System.out.println("In servlet ... " );
		try {
			//String json = req.getParameter("contractkey");
			logger.log(Level.SEVERE," refStr string -- " + refStr);
			logger.log(Level.SEVERE, " Queue string ;; -- " + jsonStr);
			JSONArray jsonarr=new JSONArray(jsonStr.trim());
			System.out.println("Array :-- " + jsonarr);
			long companyId = Long.parseLong(req.getParameter("companyId"));
			logger.log(Level.SEVERE,"get company id -- " + companyId);
			Gson gson = new GsonBuilder().create();
			loginPerson = req.getParameter("user");
//			String[] refId = refStr.trim().replace("[", "").replace("]", "").split(",");
			boolean serviceStatus = Boolean.parseBoolean(req.getParameter("serviceStatus"));
			boolean billStatus = Boolean.parseBoolean(req.getParameter("billingStatus")); 
			 lastObj =   Boolean.parseBoolean(req.getParameter("lastObject")); //billingStatus
			
		 	Date date = new Date();
			modifiedDate= new SimpleDateFormat("MM/dd/yyyy HH:MM").format(date);
	//				custdetail.setCutOffDate(fmt.parse(modifiedDate));
			 
			
			Customer customerdetail = new Customer();
			Contract contractdetail=null;
			Service servDetail = null;
			BillingDocument billDetail = null;
			List<SalesLineItem> listofsaleslineitem = null;
			ArrayList<ServiceSchedule> serviceList = null;
			//List<CustomerContractDetails> custStatusList = new ArrayList<CustomerContractDetails>();
			double billAmt =  0.0;
			
			
			double totalAmt = 0.0,outStandAmt = 0.0;
			int proSrNo = 0;
			String oldRefId = "";
			 CustomerContractDetails custDetail = null;
			 System.out.println(" cust   -- " + custCount);
			 logger.log(Level.SEVERE," cust   -- " + custCount);
			 int loopcount = 0;
			 
			 List<Customer> custList = new ArrayList<Customer>();
			 List<Contract> contList = new ArrayList<Contract>();
			 List<Service>  servList =  new ArrayList<Service>();
			 List<Service>  servDtList =  new ArrayList<Service>();
			 List<BillingDocument> billList = new ArrayList<BillingDocument>();
			 
			 SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
			 List<TaxDetails> taxDetailslist = ofy().load().type(TaxDetails.class).filter("taxChargeStatus", true).filter("companyId", companyId).list();
			boolean custDetailFlag = createCountDetails(companyId,jsonarr);
			 customerCount = custCount;
			 contractCount = contCount;
			 billingCount = billCount ; 
			 serviceCount = serCount;
			 
			 List<SuperProduct> productList = ofy().load().type(SuperProduct.class)
 					.filter("companyId", companyId).list();
			 logger.log(Level.SEVERE,"productlist size= "+productList.size());
			 
			 if(custDetailFlag){
			for(String refid : refStr){
				System.out.println("Get list  -- "+ refid);
				contractdetail = null;
				proSrNo=0;
				totalAmt = 0;
				outStandAmt = 0 ;
				listofsaleslineitem = null;
				for(int i=0;i<jsonarr.length();i++){
					try {
						System.out.println("refid " + refid+ " array id"+ jsonarr.getJSONObject(i).getString("refId").trim() +"\nCheck ref if "+ refid.trim().equalsIgnoreCase(jsonarr.getJSONObject(i).getString("refId").trim()));
						if(refid.trim().equalsIgnoreCase(jsonarr.getJSONObject(i).getString("refId").trim())){
							
							loopcount++;
							JSONObject jsonObj = jsonarr.getJSONObject(i);
							custDetail = new CustomerContractDetails();
							custDetail = gson.fromJson(jsonObj.toString(), CustomerContractDetails.class);
							System.out.println("Convert obj  ;-- "+ custDetail.getCompanyName());
							
							try {
								if(!oldRefId.equalsIgnoreCase(custDetail.getRefId())){
									Customer custid = validateCustomerDetails(cust, custDetail, i);
									System.out.println("GEt id" + custid);
									logger.log(Level.SEVERE,"GEt id" + custid);
									if(custid==null){
											customerdetail = new Customer();
										
											System.out.println("I -- " + i);
											System.out.println("new customer count -- " + custDetail.getFullName());
											logger.log(Level.SEVERE,"new customer count -- " + custDetail.getFullName());
											customerdetail.setFullname(custDetail.getFullName().toUpperCase());
											customerdetail.setCompany(custDetail.getIsCompany());
											customerdetail.setContact(custDetail.getContacts());
											customerdetail.setAdress(custDetail.getAdress());
											customerdetail.setSalutation(custDetail.getSalutation());
											customerdetail.setCompanyName(custDetail.getCompanyName().toUpperCase());
											customerdetail.setCellNumber1(custDetail.getCellNumber());
											/*customerdetail.setCategory("General Category");
											customerdetail.setGroup("General Group");
											customerdetail.setType("General Type");*/
											customerdetail.setCategory(custDetail.getContractCategory());
											customerdetail.setGroup(custDetail.getContractGroup());
											customerdetail.setType(custDetail.getContractType());
											customerdetail.setBranch(custDetail.getBranch());
											customerdetail.setEmployee(custDetail.getSalePerson());
											customerdetail.setLandLine(custDetail.getLandLine());
											customerdetail.setDescription(custDetail.getDescription()+ fmt.parse(modifiedDate)); //// @author Priyanka Bhagwat Date 18-11-2020
											customerdetail.setStatus("Active");
											customerdetail.setEmail(custDetail.getEmail());
											customerdetail.setCustPrintableName(custDetail.getCorrespondenceName()); // @author Priyanka Bhagwat Date 18-11-2020
											customerdetail.setCompanyId(companyId);
											oldRefId = custDetail.getRefId();
											ArrayList<ArticleType> artList = new ArrayList<ArticleType>();
													
											if(custDetail.getCustGST()!=null && custDetail.getCustGST().trim().length()>0){
												ArticleType Articletypedetails = new ArticleType();
												  
												 Articletypedetails.setDocumentName("Contract");
												 Articletypedetails.setArticleTypeName("GSTIN");
												 Articletypedetails.setArticleTypeValue(custDetail.getCustGST().trim());
												 Articletypedetails.setArticleDescription("");
												 Articletypedetails.setArticlePrint("Yes");
												 artList.add(Articletypedetails);
												 
												 
												 ArticleType Articletypedetails1 = new ArticleType();
												  
												 Articletypedetails1.setDocumentName("Invoice Details");
												 Articletypedetails1.setArticleTypeName("GSTIN");
												 Articletypedetails1.setArticleTypeValue(custDetail.getCustGST().trim());
												 Articletypedetails1.setArticleDescription("");
												 Articletypedetails1.setArticlePrint("Yes");
												 artList.add(Articletypedetails1);
												 
												 ArticleType Articletypedetails2 = new ArticleType();
												  
												 Articletypedetails2.setDocumentName("Quotation");
												 Articletypedetails2.setArticleTypeName("GSTIN");
												 Articletypedetails2.setArticleTypeValue(custDetail.getCustGST().trim());
												 Articletypedetails2.setArticleDescription("");
												 Articletypedetails2.setArticlePrint("Yes");
												 artList.add(Articletypedetails2);
											}
											customerdetail.setArticleTypeDetails(artList);
											customerdetail.setNewCustomerFlag(false);
											System.out.println(custDetail.getFullName());
											customerCount = custCount;
											customerCount++;
											customerdetail.setCount(customerCount);
											
											custEntry = true;
									}else{
										customerdetail = new Customer();
										customerdetail = custid;
										oldRefId = custDetail.getRefId();
//										customerCount = custid.getCount();
										custEntry = true;
									}
//									custDetail.setRemark("Done");
//									CsvWriter.custStatusList.add(custDetail);
									
								}else{
									System.out.println("Repeated ref id -- "+ custDetail.getRefId());
									logger.log(Level.SEVERE, "Repeated ref id -- "+ custDetail.getRefId());
								}
								
							} catch (Exception e) {
								custDetail.setRemark("Customer Creation error Please first correcrt the Data. Than try again for upload");
								CsvWriter.custStatusList.add(custDetail);
								System.out.println(e.getMessage());
								e.printStackTrace();
								logger.log(Level.SEVERE,e.getMessage());
								custEntry = false;
								contEntry = false;
								serEntry = false;
								billingEntry = false;
								break;
							}
								
								try {
									if(custCount>-1){
										
										if(contractdetail==null){
											contractdetail = new Contract();
											proSrNo = 0;
											listofsaleslineitem = new ArrayList<SalesLineItem>();
											
											contractdetail.setCustomerId(customerdetail.getCount());
											if(custDetail.getIsCompany()){
												contractdetail.getCinfo().setFullName(custDetail.getCompanyName());
											}else{
												contractdetail.getCinfo().setFullName(custDetail.getFullName());
											}
											contractdetail.setGroup(custDetail.getContractGroup());
											contractdetail.setCategory(custDetail.getContractCategory());
											contractdetail.setType(custDetail.getContractType());
											contractdetail.setEmployee(custDetail.getSalePerson());
											contractdetail.getCinfo().setPocName(custDetail.getFullName());
											contractdetail.getCinfo().setCellNumber(custDetail.getCellNumber());
											contractdetail.getCinfo().setCount(customerdetail.getCount());
											contractdetail.setCreationDate(custDetail.getContractStartDate());
											contractdetail.setSdAddress(custDetail.getContacts().get(1).getAddress()); 
											contractdetail.setStatus("Approved");
											contractdetail.getCinfo().setEmail(custDetail.getEmail());
											contractdetail.setCompanyId(companyId);
											contractdetail.setApproverName(loginPerson);
											contractdetail.setContractDate(custDetail.getContractDate());
											contractdetail.setStartDate(custDetail.getContractStartDate());
											contractdetail.setEndDate(custDetail.getContractEndDate());
											contractdetail.setBranch(custDetail.getBranch());
//											contractdetail
											contractdetail.setRefNo(custDetail.getRefId());
											contractdetail.setContractRate(custDetail.isRateContract());
											contractdetail.setConsolidatePrice(custDetail.isConsilated());
											contractdetail.setServiceWiseBilling(custDetail.isServiceBilling());
											contractdetail.setContractPeriod(custDetail.getSalesSource());				
											contractdetail.setCustomerGSTNumber(custDetail.getCustGST());
											contractdetail.setDescription(custDetail.getDescription() + fmt.parse(modifiedDate)); // added by Priyanka
											System.out.println("Get brach -- " + contractdetail.getBranch() +" Detail branch "+ custDetail.getBranch());
										}
											Calendar c1 = Calendar.getInstance();
											Calendar c2 = Calendar.getInstance();
											c1.setTime(custDetail.getContractStartDate());
											c2.setTime(custDetail.getContractEndDate());
											long milliofc1 = c1.getTimeInMillis();
											long milliofc2 = c2.getTimeInMillis();
											long diffInMilli = milliofc2 - milliofc1;
		
											long diffInDays = diffInMilli / (1000 * 60 * 60 * 24);
											int duration = (int) diffInDays;
											
											
											SalesLineItem item = new SalesLineItem();
											ServiceProduct serPro = new ServiceProduct();
											serPro.copyOfObject(pro);												
											
											item.setPrduct(serPro);
											//Ashwini Patil Date:27-09-2022
											if(productList!=null) {
												for(SuperProduct sp:productList) {
													if(sp.getProductName().equals(custDetail.getProName())) {														
														item.setProductCode(sp.getProductCode());														
														item.setCount(sp.getCount());															
														break;
													}
												}
											}
											
											item.setProductName(custDetail.getProName());
											if(custDetail.getPrice() == 0){
												item.setPrice(pro.getPrice());
											}else{
												item.setPrice(custDetail.getPrice());
											}
											logger.log(Level.SEVERE, "Duration -- "+ duration + " no of service "+ custDetail.getNoOfService());
											System.out.println("Duration -- "+ duration + " no of service "+ custDetail.getNoOfService());
											proSrNo++;
											item.setProductSrNo(proSrNo); 
											logger.log(Level.SEVERE, "SEr sr no " +item.getProductSrNo()+" proSrNo " + proSrNo);
											item.setDuration(duration);
										
											PaymentTerms payterms = new PaymentTerms();
											payterms.setPayTermDays(0);
											payterms.setPayTermPercent(100.0);
											payterms.setPayTermComment("Done");
											
											ArrayList<PaymentTerms> payTrmsArr=new ArrayList<PaymentTerms>(); 
											payTrmsArr.add(payterms);
											item.setTermsoftreatment(custDetail.getTypeOfTretment());
											item.setProfrequency(custDetail.getTypeOfFreq());											
											contractdetail.setPaymentTermsList(payTrmsArr);
											item.setNumberOfService(custDetail.getNoOfService());
											item.setStartDate(custDetail.getContractStartDate());
											item.setEndDate(custDetail.getContractEndDate());
											totalAmt = totalAmt+item.getPrice();
											item.setQuantity(1.0);
											item.setVatTax(new Tax());
											item.setTotalAmount(item.getPrice());
											item.setServiceTax(new Tax());
											
											/*added by sheetal: 18-11-2021*/
											
//										    item.setVatTaxEdit(custDetail.getTax1());
//											item.setServiceTaxEdit(custDetail.getTax2());
											logger.log(Level.SEVERE, "custDetail.getTax1() "+custDetail.getTax1());
											logger.log(Level.SEVERE, "custDetail.getTax2()"+custDetail.getTax2());

                                            Tax tax1 = new Tax();
                                            tax1 = getTaxDetails(custDetail.getTax1(), taxDetailslist);
                                            item.setVatTax(tax1);
      									    item.setVatTaxEdit(tax1.getTaxConfigName());
                                            
                                            Tax tax2 = new Tax();
                                            tax2 = getTaxDetails(custDetail.getTax2(), taxDetailslist);
                                            item.setServiceTax(tax2);
      									    item.setServiceTaxEdit(tax2.getTaxConfigName());
                                            
                                            /*end*/
      									    
											System.out.println("custDetail Service duration"+custDetail.getServiceDuration());
											item.setBranchSchedulingInfo("Service Address#Yes#"+custDetail.getBranch()+"#"+custDetail.getServiceDuration()+""+"#$");

											//Ashwini Patil Date:27-09-2023
											//When we reset and approve contracts uploaded through data migration - customer with service details option bills and services not getting created
											//it was giving null pointer exceltion for CustomerBranchSchedulingInfo. so setting that
											try {
												ArrayList<BranchWiseScheduling> customerBranchList = new ArrayList<BranchWiseScheduling>();

												BranchWiseScheduling cbObj1 = new BranchWiseScheduling();
												cbObj1.setBranchName("Service Address");
												cbObj1.setServicingBranch(custDetail.getBranch());
												cbObj1.setCheck(true);
												customerBranchList.add(cbObj1);

												Integer prodSrNo = item.getProductSrNo();
												HashMap<Integer, ArrayList<BranchWiseScheduling>> hsmpcustomerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
												hsmpcustomerBranchlist.put(prodSrNo, customerBranchList);
												item.setCustomerBranchSchedulingInfo(hsmpcustomerBranchlist);
												logger.log(Level.SEVERE, "set setCustomerBranchSchedulingInfo to saleslineitem");
											} catch (Exception e) {
												logger.log(Level.SEVERE, "failed to set setCustomerBranchSchedulingInfo to saleslineitem");
											}
											
											
											listofsaleslineitem.add(item);
											outStandAmt = outStandAmt+custDetail.getOutStandingamount();
											
											System.out.println("Item list size -- " +listofsaleslineitem.size() + " Outstanding amt == "+ outStandAmt);
											contractdetail.setDescription(custDetail.getDescription()); // added by Priyanka.
											contractdetail.setStatus(Contract.APPROVED);
											if(contractdetail.getEndDate().before(custDetail.getContractEndDate())){
												contractdetail.setEndDate(custDetail.getContractEndDate());
											}
											/**
											 * nidhi
											 * 20-08-2018
											 */
											item.setProModelNo(custDetail.getProModelNo());
											item.setProSerialNo(custDetail.getProSerialNo());
											custDetail.setRemark("Done!!!");
										//	CsvWriter.custStatusList.add(custDetail);
									}
								} catch (Exception e) {
									custDetail.setRemark("contract Creation error Please first correcrt the Data. Than try again for upload");
									CsvWriter.custStatusList.add(custDetail);
									e.printStackTrace();
									System.out.println(e.getMessage());
									logger.log(Level.SEVERE,e.getMessage());
									custEntry = false;
									contEntry = false;
									serEntry = false;
									billingEntry = false;
									break;
								}
								
						}
					} catch (Exception e) {
						custDetail.setRemark("Customer and contract Creation error Please first correcrt the Data. Than try again for upload");
						CsvWriter.custStatusList.add(custDetail);
						System.out.println(e.getMessage());
						e.printStackTrace();
						logger.log(Level.SEVERE,e.getMessage());
						custEntry = false;
						contEntry = false;
						serEntry = false;
						billingEntry = false;
						break;
					}
						
				}
				
				if(custEntry && contractdetail!=null){
						try {
							/**Added by sheetal:24-11-2021 **/
							
							ArrayList<ProductOtherCharges> prodTaxeslist = new ArrayList<ProductOtherCharges>();
							prodTaxeslist = getproductTaxeslist(listofsaleslineitem);
							
							contractdetail.setProductTaxes(prodTaxeslist);
							for(ProductOtherCharges prodcharges : prodTaxeslist){
								System.out.println("taxes value"+prodcharges.getChargePayable());
							}
							double totalAmountWithtax = getTotalAmountWithTax(prodTaxeslist,totalAmt);						
							contractdetail.setTotalAmount(totalAmt);
							contractdetail.setNetpayable(totalAmountWithtax);
//							contractdetail.setGrandTotal(totalAmt);
							contractdetail.setFinalTotalAmt(totalAmt);
							contractdetail.setGrandTotal(totalAmountWithtax);
							contractdetail.setGrandTotalAmount(totalAmountWithtax);
							contractdetail.setInclutaxtotalAmount(totalAmountWithtax);
							contractdetail.setCompanyId(companyId);
							contractdetail.setNumberRange(custDetail.getNumberRang());
							logger.log(Level.SEVERE," product size -- " + listofsaleslineitem.size());
							contractdetail.setItems(listofsaleslineitem);
							 serviceList = CreateServiceList(custDetail,listofsaleslineitem,serCount,contractdetail);
							System.out.println("Get contract service List : -- " + serviceList.size());
							logger.log(Level.SEVERE, "Get contract service List : -- " + serviceList.size());
							contractdetail.setServiceScheduleList(serviceList);
//							contCount++;
							if(serEntry){
								logger.log(Level.SEVERE, "Number Range -" + contractdetail.getNumberRange());
								if(contractdetail.getNumberRange()!=null && !contractdetail.getNumberRange().equals("")) {
									String processName = "C_"+contractdetail.getNumberRange();
									int contractId=(int) numGen.getLastUpdatedNumber(processName, contractdetail.getCompanyId(), (long) 1);
									logger.log(Level.SEVERE, "Number Range - contractId" + contractId);
									contractdetail.setCount(contractId);
								}
								else {
									contractCount = contCount;
									contractCount++;
									contractdetail.setCount(contractCount);

								}
//								contractCount = contCount;
//								contractCount++;
//								contractdetail.setCount(contractCount);
								contEntry = true;
							}
						} catch (Exception e) {
							custDetail.setRemark("Contract Creation error Please first correcrt the Data. Than try again for upload");
							CsvWriter.custStatusList.add(custDetail);
							e.printStackTrace();
							System.out.println(e.getMessage());
							logger.log(Level.SEVERE,e.getMessage());
							custEntry = false;
							contEntry = false;
							serEntry = false;
							billingEntry = false;
							break;
						}
					
				}
				
				if(contEntry && custEntry && contractdetail!=null && serviceStatus && serviceList.size()>0 && !contractdetail.isContractRate()){
					try {
						serEntry = true;
						servDtList.clear();
						servDtList.addAll(GetServiceList(contractdetail,custDetail,serviceList));
						System.out.println("Service List -- "+ servList.size());
						logger.log(Level.SEVERE, "Service List -- "+ servList.size());
						serCount = serviceCount;
					} catch (Exception e) {
						custDetail.setRemark("Service Creation error Please first correcrt the Data. Than try again for upload");
						CsvWriter.custStatusList.add(custDetail);
						e.printStackTrace();
						System.out.println(e.getMessage());
						custEntry = false;
						contEntry = false;
						serEntry = false;
						billingEntry = false;
						logger.log(Level.SEVERE,e.getMessage());
						break;
					}finally
					{
						
					}
				}
				
				System.out.println(" Contract -[- "+ contEntry + "  CustEnt " + custEntry + " billStatus  -- "+ billStatus + " outs -- " + outStandAmt +"  contractdetail " + (contractdetail==null));
				billingEntry = true;
				if(contractdetail!=null) {
				logger.log(Level.SEVERE,"get rate contract status -- " + contractdetail.isContractRate() + " service wise billing  -- " + contractdetail.isServiceWiseBilling());
				logger.log(Level.SEVERE,"contEntry" +contEntry + " custEntry  -- " +custEntry+" serEntry "+serEntry+" billStatus "+billStatus
						+" outStandAmt "+outStandAmt + "contractdetail "+contractdetail);
				}

				if(contEntry && custEntry && serEntry && billStatus && outStandAmt > 0 && contractdetail!=null && !contractdetail.isContractRate() && !contractdetail.isServiceWiseBilling()){
					try {
						
						billingEntry = true;
						billDetail = new BillingDocument();
						System.out.println("Billi count=--- " +billCount);
						List<SalesOrderProductLineItem> salesProdLis=null;
						
						billDetail.setContractCount(contractdetail.getCount());
						billDetail.setPersonInfo(contractdetail.getCinfo());
						billDetail.setContractStartDate(contractdetail.getStartDate());
						billDetail.setContractEndDate(contractdetail.getEndDate());
						billDetail.setBillstatus("Created");
						billDetail.setComment("This data uploded through upload program.  " + fmt.parse(modifiedDate));
						billDetail.setBranch(contractdetail.getBranch());
						billDetail.setCompanyId(companyId);
						salesProdLis=this.retrieveSalesProducts(contractdetail,outStandAmt);
						ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
						salesOrdArr.addAll(salesProdLis);
						System.out.println("Size here here"+salesOrdArr.size());
						if(salesOrdArr.size()!=0){
							billDetail.setSalesOrderProducts(salesOrdArr);
						}
						ContractInvoicePayment contractinvoicepayment=new ContractInvoicePayment();
//						ArrayList<ContractCharges> Taxeslist=new ArrayList<ContractCharges>();
//						Taxeslist =  contractinvoicepayment.getproductTaxeslistforBill(salesOrdArr);
//						billDetail.setBillingTaxes(Taxeslist);
					
						logger.log(Level.SEVERE, "totalAmt "+totalAmt);
						logger.log(Level.SEVERE, "outStandAmt "+outStandAmt);
						
						double amountPercentage = getamountpercentage(totalAmt,outStandAmt);	
						logger.log(Level.SEVERE, "amountPercentage "+amountPercentage);
						
						List<ContractCharges> billTaxLis=null;
//						ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
						if(contractdetail.getProductTaxes().size()!=0){
							ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
							billTaxLis=contractinvoicepayment.listForBillingTaxes(contractdetail, amountPercentage);
							billTaxArr.addAll(billTaxLis);
							billDetail.setBillingTaxes(billTaxArr);
//							billTaxArrROHAN.addAll(billTaxLis);
						}
						
						double totalAmountWithtax = getTotalAmountWithTax(billDetail.getBillingTaxes(),outStandAmt);						

						
						ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
						PaymentTerms paymentTerms=new PaymentTerms();
						paymentTerms.setPayTermDays(0);
						billDetail.setApproverName(loginPerson);
						paymentTerms.setPayTermPercent(100.0);
						paymentTerms.setPayTermComment("Done");
						billingPayTerms.add(paymentTerms);
						billDetail.setArrPayTerms(billingPayTerms);
						billDetail.setTypeOfOrder("Service Order");
						billDetail.setStatus("Created");
						billDetail.setAccountType("AR");
						billDetail.setOrderCformStatus("");
						billDetail.setOrderCformPercent(-1);//totalBillingAmount
						billDetail.setTotalBillingAmount(outStandAmt);
						billDetail.setTotalSalesAmount(outStandAmt);
						billDetail.setGstinNumber(custDetail.getCustGST());
						billDetail.setConsolidatePrice(contractdetail.isConsolidatePrice());
						billDetail.setNumberRange(custDetail.getNumberRang());
						/** Date 03-09-2017 added by vijay for total amount before taxes**/
						billDetail.setTotalAmount(outStandAmt);
						/** Date 05-09-2017 added by vijay for Discount amount**/
						double discountamt = 0;
						/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
						billDetail.setFinalTotalAmt(outStandAmt);
						 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
						billDetail.setTotalAmtIncludingTax(totalAmountWithtax);
						 /** Date 05-09-2017 added by vijay for roundoff amount **/
						double roundoff=0;
						
						/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
						billDetail.setGrandTotalAmount(outStandAmt);
						logger.log(Level.SEVERE, "Number Range -- " + billDetail.getNumberRange());

						if(billDetail.getNumberRange()!=null && !billDetail.getNumberRange().equals("")) {
							String processName = "B_"+billDetail.getNumberRange();
							int billingId=(int) numGen.getLastUpdatedNumber(processName, billDetail.getCompanyId(), (long) 1);
							billDetail.setCount(billingId);
							logger.log(Level.SEVERE, "Number Range - billingId" + billingId);
						}
						else {
							billingCount = billCount;
							billingCount++;
							billDetail.setCount(billingCount);
						}
						
						/**
						 * @author Anil @since 03-09-2021
						 * Billing details were not shown on invoice and payment popup due to billing date null
						 * Raised by Rahul Tiwari for Life Line while uploading contract with service details
						 */
						billDetail.setBillingDate(contractdetail.getContractDate());
						
//						billingCount = billCount;
//						billingCount++;
//						billDetail.setCount(billingCount);
						
						//billList.add(billDetail);
						System.out.println("Billing list ---- size -- "+ billList.size() + " Count  "+ billCount);
					} catch (Exception e) {
						custDetail.setRemark("Billing Creation error Please first correcrt the Data. Than try again for upload");
						CsvWriter.custStatusList.add(custDetail);
						e.printStackTrace();
						System.out.println(e.getMessage());
						logger.log(Level.SEVERE,e.getMessage());
						custEntry = false;
						contEntry = false;
						serEntry = false;
						billingEntry = false;
						break;
					}
				}
			
				if(contEntry && custEntry && serEntry && ( billingEntry)){

					custList.add(customerdetail);
					cust.add(customerdetail);
					contList.add(contractdetail);
					
					contCount = contractCount;
					custCount = customerCount;
					
					if(serviceStatus  ){
						servList.addAll(servDtList);
						serCount = serviceCount;
					}
					
					if(billStatus &&  outStandAmt > 0  && billDetail!=null){
						billList.add(billDetail);
						billCount = billingCount;
						billDetail = null;
					}
					
					logger.log(Level.SEVERE, "customer list  "+ custList.size());

				}
				
			}
			System.out.println("Billing list ---- size -- "+ billList.size() + " Count  "+ billCount);
			System.out.println("Entru status -- ");
			System.out.println("Cust -- "+ custEntry + " \nContract "+ contEntry +" \n servicee ---  " +serEntry +" \n billing entry  -- "+ billingEntry);
			logger.log(Level.SEVERE, "Cust -- "+ custEntry + " \nContract "+ contEntry +" \n servicee ---  " +serEntry +" \n billing entry  -- "+ billingEntry);
			if(custEntry && contEntry && serEntry && billingEntry){
				try {
					for(Customer customer : custList){
						System.out.println("Get cust new id -- " + customer.getCount());
						ofy().save().entity(customer).now();

					}
					logger.log(Level.SEVERE, "customer  list saving   "+ custList.size());

					 System.out.println(" count length  -- " + loopcount);
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
					logger.log(Level.SEVERE, "Customer add exception \n "+ e.getMessage());
					custEntry = false;
				}
				
				if(custEntry && contEntry ){
					try {
						for(Contract contract : contList){
							System.out.println("Get contract new id -- " + contract.getCount());
							ofy().save().entity(contract).now();
							
						}
//						if(contList.size()>0) {
//							ArrayList<SuperModel> list = new ArrayList<SuperModel>();
//							list.addAll(contList);
//							GenricServiceImpl genimpl = new GenricServiceImpl();
//							genimpl.save(list);
//						}
						logger.log(Level.SEVERE, "Contract  list saving   "+ contList.size());

						
					} catch (Exception e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
						logger.log(Level.SEVERE, "Contract add exception \n "+ e.getMessage());
						custEntry = false;
						contEntry = false;
					}
				}
				
				if(serviceStatus && custEntry && contEntry && serEntry){
					try {
						for(Service service : servList){
							System.out.println("Get service new id -- " + service.getCount());
							ofy().save().entity(service).now();
						}
						logger.log(Level.SEVERE, "Service  list saving   "+ servList.size());

//						if(servList.size()>0) {
//							ArrayList<SuperModel> list = new ArrayList<SuperModel>();
//							list.addAll(servList);
//							GenricServiceImpl genimpl = new GenricServiceImpl();
//							genimpl.save(list);
//						}
						
					} catch (Exception e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
						logger.log(Level.SEVERE, "Contract add exception \n "+ e.getMessage());
						custEntry = false;
						contEntry = false;
						serEntry = false;
					}
				}
				
				if(billStatus && custEntry && contEntry && serEntry && billingEntry ){
					try {
						for(BillingDocument billing : billList){
							
							System.out.println("Get billing new id -- " + billing.getCount());
							ofy().save().entity(billing).now();
						}
						logger.log(Level.SEVERE, "Billing  list saving   "+ billList.size());

//						if(billList.size()>0) {
//							ArrayList<SuperModel> list = new ArrayList<SuperModel>();
//							list.addAll(billList);
//							GenricServiceImpl genimpl = new GenricServiceImpl();
//							genimpl.save(list);
//						}
						
					} catch (Exception e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
						logger.log(Level.SEVERE, "Contract add exception \n "+ e.getMessage());
						custEntry = false;
						contEntry = false;
						serEntry = false;
						billingEntry = false;
					}
				}
				
				logger.log(Level.SEVERE, "last object boolean -- " + lastObj);
				
				
				 System.out.println("Cust list " + custList.size() + "before  count " + customerSrNo.getNumber() +" after --"+ custCount );
					logger.log(Level.SEVERE, "Cust list " + custList.size() + "before  count " + customerSrNo.getNumber() +" after --"+ custCount);
					customerSrNo.setNumber(custCount);
						
						impl.save(customerSrNo);
					
					
						logger.log(Level.SEVERE,"Cont list " + contList.size() + "before  count " + contractSrNo.getNumber() +" after --"+ contCount);
					
					contractSrNo.setNumber(contCount);
						impl.save(contractSrNo);
					
					
						logger.log(Level.SEVERE,"contract service No list " + servList.size() + "before  count " + serNo.getNumber() +" after --"+ serCount );
					 //**  service 
					 serNo.setNumber(serCount);
					 impl.save(serNo);
					
					
					 logger.log(Level.SEVERE,"bill list " + billList.size() + "before  count " + billNumGe.getNumber() +" after --"+ billCount);
					 billNumGe.setNumber(billCount);
					 impl.save(billNumGe);
					 
					 logger.log(Level.SEVERE, "Get email method start..!!");
				
			}else{
				
				
				
			}
			
					
					
			 }
			 try {
				 
				 EmailUploadDetails(companyId);	  
			} catch (Exception e) {
			}
			 jsonStr = "";
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE," last obj method " +e.getMessage());
			logger.log(Level.SEVERE," last obj method " +e.getLocalizedMessage());
		}finally{
			refStr.clear();
			jsonStr = "";
		}
//		window.location.href = "";
//		resp.sendRedirect("popup.aspx", "_blank", "menubar=0,width=100,height=100");
//		System.out.println("get here ");
//		resp.setContentType("text/html"); 
//		RequestDispatcher requestDispatcher = 
//    			req.getRequestDispatcher("/csvservlet?type=" + 123);
//    		requestDispatcher.include(req, resp);
////    		PrintWriter out = resp.getWriter();
////    		out.println("window.open(\"/pageA.jsp\",\"_blank\")");
//    		
//    		
////		 RequestDispatcher reqDispatcher = getServletConfig().getServletContext().getRequestDispatcher("/csvservlet?type=" + 123);
////			 reqDispatcher.forward(req,resp);
////			  
//			 PrintWriter out = resp.getWriter();
//		        out.println("<html><body>");
//		        out.println("<script type=\"text/javascript\">");
//		        out.println("window.open(\"csvservlet?type=" + 123+"\",\"_blank\")");
//		        out.println("</script>");
//		        out.println("</body></html>");
//		        System.out.println("get here ");
//		        resp.sendRedirect("/csvservlet?type=" + 123);
	}
	
	private HashMap<Integer, ArrayList<BranchWiseScheduling>> getcustomerBranchInfo(String branch, String unitOfMeasurement, int productSrNo) {

		ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
		
		BranchWiseScheduling custBranchSchedule = new BranchWiseScheduling();
		custBranchSchedule.setBranchName("Service Address");
		custBranchSchedule.setCheck(true);
		custBranchSchedule.setServicingBranch(branch);
		custBranchSchedule.setUnitOfMeasurement(unitOfMeasurement);

		scheduleBranchlist.add(custBranchSchedule);
		
		HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
		customerBranchlist.put(productSrNo, scheduleBranchlist);
		
		return customerBranchlist;
	}

	public void EmailUploadDetails(long companyId){
		try {
			logger.log(Level.SEVERE,"Enter for mail...");
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			 Date d=new Date();
			 String dateString = sdf.format(d);
			
//			Calendar calendar=Calendar.getInstance();
//			calendar.setTime(new Date());
//			calendar.add(Calendar.DAY_OF_YEAR, -2);
//			Date afterDate = calendar.getTime();
//			String previousDateinString=sdf.format(afterDate);
//			String todayDateinString=sdf.format(new Date());
//			logger.log(Level.SEVERE,"previousDateinString::"+previousDateinString+"todayDateinString:::"+todayDateinString);
			
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, -1);
			Date previousDate = calendar.getTime();
			String todayDateinString=sdf.format(previousDate);
//			
			logger.log(Level.SEVERE,"Date ::"+todayDateinString);
			Company company=ofy().load().type(Company.class).filter("companyId",companyId).first().now();
			
			long compId=company.getCompanyId();
			logger.log(Level.SEVERE,"company id -- " + compId);
			
			ArrayList<String> contractRefList=new ArrayList<String>();
			contractRefList.add("No.");
			contractRefList.add("Reference Order Id");
			contractRefList.add("Status");
			
			System.out.println("get here mail .... !! "+ company.getEmail());
			ArrayList<String> tbl1=new ArrayList<String>();
			int count=1;
			
			for (CustomerContractDetails obj : CsvWriter.custStatusList) {
				
				tbl1.add(count+"");
				tbl1.add(obj.getRefId());
				tbl1.add(obj.getRemark());
				count=count+1;
				logger.log(Level.SEVERE,"Error : obj.getRefId()="+obj.getRefId()+" remark= "+obj.getRemark());
				
			}
			
			Email cronEmail = new Email();
			
			try {
				//if( CsvWriter.custStatusList.size()!=0)
				{
					ArrayList<String> emailTo = new ArrayList<String>();
					emailTo.add(company.getEmail());
					logger.log(Level.SEVERE,"get here mail .... !! "+ company.getEmail());
					cronEmail.cronSendEmail(emailTo, "Data Migration Process As On Date"+" "+ todayDateinString, "Contract Report As On Date" +"  "+ todayDateinString, company, contractRefList, tbl1, null, null, null, null);
					logger.log(Level.SEVERE,"get here mail .... !! "+ company.getBusinessUnitName());
				}
			}catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE,"Customer details upload..."+e.getMessage());
//				return "Failed to send emails";
			}
			
		} catch (Exception e) {
			e.getMessage();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, "Customer details upload..."+e.getMessage());
		}
	}
	
	public Customer validateCustomerDetails(List<Customer> customer,CustomerContractDetails custdetail, int pos){
		int id = -1;
		try {
			System.out.println(" company status--- "+custdetail.getIsCompany());
			for(Customer cust :  customer){
				
				if(cust.isCompany() && custdetail.getIsCompany()){
					
					if(cust.getCompanyName().trim().equals(custdetail.getCompanyName().trim())){
						if(cust.getFullname().trim().equals(custdetail.getFullName().trim())){
							if(custdetail.getCellNumber()==(cust.getCellNumber1()) || custdetail.getEmail().trim().equals(cust.getEmail().trim())){
								return cust;
							}
						}
					}
				}else{
					if(cust.getFullname().trim().equals(custdetail.getFullName().trim())){
						if(custdetail.getCellNumber()==(cust.getCellNumber1()) || custdetail.getEmail().trim().equals(cust.getEmail().trim())){
							return cust;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "lat obj status-- " + lastObj);
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE, "customer compare  -- " + e.getMessage());
		
		}
		return null;
	}
	
	
	public ArrayList<ServiceSchedule> CreateServiceList(CustomerContractDetails custdetail,List<SalesLineItem> salesitemlis,int servlistCount, Contract contDetail){
		ArrayList<ServiceSchedule> sersechList = new ArrayList<ServiceSchedule>();
		
		boolean serviceScheduleMinMaxRoundOffFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", AppConstants.PC_SERVICESCHEDULEMINMAXROUNDUP, contDetail.getCompanyId());
		
		try {
			SimpleDateFormat formatday = new SimpleDateFormat("EEEE");
			for(int i=0;i<salesitemlis.size();i++){	
			 int qty=(int) salesitemlis.get(i).getQty();
					for(int k=0;k < qty;k++){
					
					 	
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					
					/********* Date 02 oct 2017 below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					
					
					/*** Date 02 Oct 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
					double days =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/
					
					/**
					 * @author Vijay Date 11-10-2021
					 *  Des :- Service Scheduling with new Min Max round up logic for innovative
					 */
					if(serviceScheduleMinMaxRoundOffFlag){
						ServerAppUtility serverappUtility = new ServerAppUtility();
						interval = serverappUtility.getValueWithMinMaxRoundUp(interval);
						temp = interval;
						tempvalue=interval;
					}
					/**
					 * ends here
					 */
					
					//** set sales line contract End date
					Date servicedate=(salesitemlis.get(i).getStartDate());
					Date d=new Date(servicedate.getTime());
					
					
//					Date productEndDate= new Date(servicedate.getTime());
//					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					 
					Calendar cal = Calendar.getInstance();
					cal.setTime(servicedate);
					cal.add(Calendar.DATE, noOfdays); 
					 
					Date productEndDate = cal.getTime();
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					
					Date tempdate=new Date();
					int servcount = 1;
					System.out.println("No of ser --- " + salesitemlis.get(i).getNumberOfServices() +" duration -- "+ noOfdays +" interval -- "+interval+" ");
					logger.log(Level.SEVERE, "No of ser --- " + salesitemlis.get(i).getNumberOfServices() +" duration -- "+ noOfdays +" interval -- "+interval+" ");
					for(int j=0;j<noServices;j++){
						
						/*** Date 02 oct 2017 added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);
						
						
						if(j>0)
						{
							Calendar cal1 = Calendar.getInstance();
							cal1.setTime(d);
							cal1.add(Calendar.DATE, interval2); // add 10 days
							 
							d = cal1.getTime();
							tempdate=d;
							
							/************* Date 02 oct 2017 added by vijay *********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /************** ends here ************************/
						}
						System.out.println(" tempdate End Date --- " + tempdate +" Branch -- "+custdetail.getBranch() );
						logger.log(Level.SEVERE, " tempdate End Date --- " + tempdate +" Branch -- "+custdetail.getBranch());
						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=new Date();
						System.out.println("Service seduled Branch "+custdetail.getBranch() +" getProductSrNo " + salesitemlis.get(i).getProductSrNo());
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						ssch.setServicingBranch(contDetail.getBranch());
						ssch.setScheduleStartDate(contDetail.getStartDate());
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(servcount);
						servcount++;
						ssch.setScheduleProdStartDate(Stardaate);
						ssch.setScheduleProdEndDate(productEndDate);
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						ssch.setScheduleServiceTime("Flexible");
						
						/**********Code Added For Service Day Operation ******** Start **********/ 
						
						if(custdetail.getServiceWeekDay()!=null){
//							try{
//							String time = String.valueOf(custdetail.getServiceTime());
//							String newTime=time+"";
//			                System.out.println("Coverted Time"+newTime);
							ssch.setScheduleServiceTime(custdetail.getServiceTime());
//							}catch(Exception e){System.out.println("Error in Duble value format"+e);}
						int selectedIndex=getEnterDayValue(custdetail.getServiceWeekDay());
						String dayOfWeek1 = formatday.format(d);
						System.out.println("I print dayOfWeek1 =="+dayOfWeek1);
						int adate=getEnterDayValue(dayOfWeek1);
						System.out.println("I print adate =="+adate);
						int adday1 = selectedIndex;
						System.out.println("I print adate =="+adday1);
						
						int adday = 0;
						if (adate <= adday1) {
							adday = (adday1 - adate);
						} else {
							adday = (7 - adate + adday1);
						}
						System.out.println(" I am peint adday after == "+adday);
						Date newDate = new Date(d.getTime());
						System.out.println("NewData I am peint"+newDate);
						Calendar cal2 = Calendar.getInstance();
						cal2.setTime(newDate);
						System.out.println(" Get New Date "+newDate);
//						cal2.add(newDate, adday); 
						cal2.add(Calendar.DATE, adday); 
						newDate=cal2.getTime();
						 System.out.println("New Date  after "+newDate+"  / "+cal2.getTime());
						Date day = new Date(newDate.getTime());
					    System.out.println("Date in Day "+day);	
						
					    /********* First Time Execute **********/
					    if(j==0){
//							ssch.setScheduleServiceDate(servicedate);
							ssch.setScheduleServiceDate(day);
							System.out.println("Service Date Alredy set Here"+servicedate);
						}
					    
					    /********* After First Time Execute **********/
					    if(j!=0){						
//							if(productEndDate.before(d)==false){
//								ssch.setScheduleServiceDate(tempdate);
//							}
							if (productEndDate.before(day) == false) {
								ssch.setScheduleServiceDate(day);
								System.out.println("Into Day if Loop ");
							}
							else{
								ssch.setScheduleServiceDate(productEndDate);
								
							}
						}	    
					}
						/**********Service Day Operation ********Code End **********/
						
						else{	
					    	if(j==0){
								ssch.setScheduleServiceDate(servicedate);
					    	    }
					    	
					    	if(j!=0){						
								if(productEndDate.before(d)==false){
									ssch.setScheduleServiceDate(tempdate);
								}
								else{
									ssch.setScheduleServiceDate(productEndDate);	
								}
					         }
						}
						
							ssch.setScheduleProBranch("Service Address");//old value null added by Ashwini Patil Date:27-09-2023 as after contract reset bill and services were not getting created
						
						ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
						
						/**
						 * @author Vijay Choguule Date 10-09-2020
						 * Des :- added Service Duration column for Life Line Services
						 */
						if(salesitemlis.get(i).getBranchSchedulingInfo()!=null && !salesitemlis.get(i).getBranchSchedulingInfo().equals("")){
							try {
								String[] branchscheduling = salesitemlis.get(i).getBranchSchedulingInfo().split("#");
								if(branchscheduling.length>3){
									ssch.setServiceDuration(Double.parseDouble(branchscheduling[3]));
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
						/**
						 * ends here
						 */
						
						sersechList.add(ssch);
						
					}
					}
		}	
		} catch (Exception e) {
			 custEntry = false;
			 contEntry = false; 
			 serEntry = false;
			 billingEntry = false;
			 custdetail.setRemark("Service Creation error Please first correcrt the Data. Than try again for upload");
				CsvWriter.custStatusList.add(custdetail);
			e.printStackTrace();
			sersechList.clear();
			System.out.println(e.getMessage());
		}
		System.out.println("sersechList "+sersechList.size());
		return sersechList;
	}
	
	
	private int getEnterDayValue(String serviceWeekDay) {
		int i=0;
		try{
			
		switch(serviceWeekDay){
		 
		case "Sunday" :   
			            i=0;
			            System.out.println("I am Print value switch case"+i);
		                break;
		case "Monday" :     
			            i= 1;
			            System.out.println("I am Print value switch case"+i);
                        break;                 
		case "Tuesday" :     
			            i=2;
			            System.out.println("I am Print value switch case"+i);
                        break;
        case "Wednesday" :  
        	            i= 3;
        	            System.out.println("I am Print value switch case"+i);
                        break;                 
        case "Thursday" :  
        	            i=4;
        	            System.out.println("I am Print value switch case"+i);
                       break;
        case "Friday" :    
        	            i= 5;
        	            System.out.println("I am Print value switch case"+i);
                        break;                 
        case "Saturday" :  
        	            i=6;
        	            System.out.println("I am Print value switch case"+i);
                        break;
         default :  System.out.println("Please check Given Day");               
		}
		}catch(Exception e){System.out.println("Input Day Error"+e);}
		System.out.println("I Value here printed"+i);
		return i;
	}

	private Date calculateEndDate(Date servicedate, int i) {
		// TODO Auto-generated method stub
		
//	    servicedate = servicedt;
		Date d = new Date(servicedate.getTime());
		Date productEndDate = new Date(servicedate.getTime());
		CalendarUtil.addDaysToDate(productEndDate, i);
		Date prodenddate = new Date(productEndDate.getTime());
		productEndDate = prodenddate;

		return productEndDate;
		
		
	}

	public List<Service> GetServiceList(Contract contractdetail, CustomerContractDetails custDetail, ArrayList<ServiceSchedule> serviceList){
		List<Service> serlist =  new ArrayList<Service>();
		 SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
		try {
			Service serDetailService =null;
			List<ServiceSchedule> serScheList = new ArrayList<ServiceSchedule>();
			serviceCount = serCount;
			for(ServiceSchedule ser : serviceList){
				System.out.println(" serv date -- "+ ser.getScheduleServiceDate() +" ");
				if(custDetail.getCutOffDate()!= null && (ser.getScheduleServiceDate().after(custDetail.getCutOffDate())|| ser.getScheduleServiceDate().equals(custDetail.getCutOffDate()) )){
					System.out.println(" serv date -- "+ custDetail.getCutOffDate() +" ");
					serDetailService = new Service();
					serDetailService.setPersonInfo(contractdetail.getCinfo());
					serDetailService.setAddress(contractdetail.getSdAddress());
					ServiceProduct serPro = getProCode(contractdetail,ser.getScheduleProdName());
					if(serPro!=null){
						serDetailService.setProduct(serPro);
					}
					serDetailService.setServiceSerialNo(ser.getScheduleServiceNo());
					serDetailService.setServiceWiseBilling(contractdetail.isServiceWiseBilling());
					serDetailService.setApproverName(loginPerson);
//					serDetailService.setServiceBranch(ser.getServiceRemark());
					serDetailService.setServiceBranch(ser.getScheduleProBranch());//Ashwini Patil Date:16-05-2024 commented above line and added this as service address was getting set as blank through upload
					System.out.println("Serv Branch " + ser.getServicingBranch());
					serDetailService.setBranch(ser.getServicingBranch());
					serDetailService.setServiceDate(ser.getScheduleServiceDate());
					serDetailService.setContractCount(contractdetail.getCount());
					serDetailService.setContractEndDate(contractdetail.getEndDate());
					serDetailService.setContractStartDate(contractdetail.getStartDate());
					serDetailService.setCompanyId(contractdetail.getCompanyId());
					serDetailService.setQuantity(ser.getScheduleProQty());
					serDetailService.setRefNo(contractdetail.getRefNo());
					serDetailService.setComment("This data uploded through upload program.  "  + fmt.parse(modifiedDate));
					serDetailService.setStatus("Scheduled");
					serDetailService.setCategory(contractdetail.getCategory());
					serDetailService.setType(contractdetail.getType());
					serDetailService.setGroup(contractdetail.getGroup());
					logger.log(Level.SEVERE," get count ---- " +contractdetail.getItems().size() + " SER COUNT -- " + serDetailService.getServiceSerialNo());
					for(SalesLineItem item : contractdetail.getItems()){
						logger.log(Level.SEVERE," get pro name -- " +serDetailService.getProductName().trim() + " item name == " +  item.getPrduct().getProductName());
						if(serDetailService.getProductName().trim().equalsIgnoreCase(item.getPrduct().getProductName().trim())){
							logger.log(Level.SEVERE,"get total --" + item.getTotalAmount());
							serDetailService.setServiceValue(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty()));
							serDetailService.setServiceSrNo(item.getProductSrNo());
							serDetailService.setProSerialNo(item.getProSerialNo());
							serDetailService.setProModelNo(item.getProModelNo());
						}
					}
					/**
					 * @author Vijay Choguule Date 10-09-2020
					 * Des :- added Service Duration column for Life Line Services
					 */
					if(ser.getServiceDuration()!=0){
						serDetailService.setServiceDuration(ser.getServiceDuration());
					}
					/**
					 * ends here
					 */
					if(contractdetail.getNumberRange()!=null) {
						serDetailService.setNumberRange(contractdetail.getNumberRange());
					}
					logger.log(Level.SEVERE, "Number Range == " + contractdetail.getNumberRange());
					if(serDetailService.getNumberRange()!=null && !serDetailService.getNumberRange().equals("")) {
						String processName = "S_"+serDetailService.getNumberRange();
						int serviceId=(int) numGen.getLastUpdatedNumber(processName, serDetailService.getCompanyId(), (long) 1);
						serDetailService.setCount(serviceId);
						logger.log(Level.SEVERE, "Number Range - billingId" + processName);

					}
					else {
						serviceCount++;
						serDetailService.setCount(serviceCount);
					}
//					serviceCount++;
//					serDetailService.setCount(serviceCount);
					logger.log(Level.SEVERE,"Description is --" + custDetail.getDescription());
					serDetailService.setDescription(custDetail.getDescription()); // @author Priyanka Bhagwat Date 18-11-2020 Des : add Description & Remark column for star-cool
					serDetailService.setServiceDay(custDetail.getServiceWeekDay());
					if(custDetail.getServiceTime()!=null && !custDetail.getServiceTime().equals("")){
						String serviceTime = custDetail.getServiceTime();
						String []servicetime = serviceTime.split(":");
						String serTime = "";
						if(servicetime.length>0){
							serTime = servicetime[0];
						}
						if(servicetime.length>1){
							serTime += ":"+servicetime[1]; 
						}
						if(servicetime.length>2){
							serTime += servicetime[2]; 
						}
						serDetailService.setServiceTime(serTime);

					}
					
					serlist.add(serDetailService);
					
					
				}
					
			}
			
		} catch (Exception e) {
			serviceCount = serCount;
			custEntry = false;
			 contEntry = false; 
			 serEntry = false;
			 billingEntry = false;
			 serlist.clear();
			e.printStackTrace();
			custDetail.setRemark("Service Creation error Please first correcrt the Data. Than try again for upload");
			CsvWriter.custStatusList.add(custDetail);
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE,"Service List -- "+e.getMessage());
		}
		return serlist;
	}
	
	public ServiceProduct getProCode(Contract Condetail,String proID){
		String code = null;
		ServiceProduct pro = null;
		try {
			for(SalesLineItem list : Condetail.getItems()){
				if(list.getPrduct().getProductName().equals(proID)){
					return (ServiceProduct)list.getPrduct();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			custEntry = false;
			 contEntry = false; 
			 serEntry = false;
			 billingEntry = false;
			
			logger.log(Level.SEVERE, e.getMessage());
		}
		return pro;
	}
	
	public Double getProPrice(Contract Condetail,int proID){
		Double price = null;
		try {
			for(SalesLineItem list : Condetail.getItems()){
				if(list.getPrduct().getCount() == proID){
					return list.getPrduct().getPrice();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, e.getMessage());
		}
		return price;
	}
	
	/**
	 * @author Anil , Date : 03-07-2019
	 * added paramenter oustanding amount,earlier billing line item price was set as per the contract but for outstanding amount it should be as per ouutstanding amount
	 * For all pest rid raised by Rahul Tiwari
	 * @param conDetail
	 * @param outstandingAmount
	 * @return
	 */
	private List<SalesOrderProductLineItem> retrieveSalesProducts(Contract conDetail,double outstandingAmount)
	{
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double baseBillingAmount=0;
		String prodDesc1="",prodDesc2="";

		try {
			for(int i=0;i<conDetail.getItems().size();i++)
			{
				SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", conDetail.getItems().get(i).getPrduct().getCount()).first().now();
				if(superProdEntity!=null){
					System.out.println("Superprod Not Null");
					if(superProdEntity.getComment()!=null){
						System.out.println("Desc 1 Not Null");
						prodDesc1=superProdEntity.getComment();
					}
					if(superProdEntity.getCommentdesc()!=null){
						System.out.println("Desc 2 Not Null");
						prodDesc2=superProdEntity.getCommentdesc();
					}
				}
				
				SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
				salesOrder.setProductSrNumber(conDetail.getItems().get(i).getProductSrNo());
				salesOrder.setProdId(conDetail.getItems().get(i).getPrduct().getCount());
				salesOrder.setProdCategory(conDetail.getItems().get(i).getProductCategory());
				salesOrder.setProdCode(conDetail.getItems().get(i).getProductCode());
				salesOrder.setProdName(conDetail.getItems().get(i).getProductName());
				salesOrder.setQuantity(conDetail.getItems().get(i).getQty());
				salesOrder.setOrderDuration(conDetail.getItems().get(i).getDuration());
				salesOrder.setOrderServices(conDetail.getItems().get(i).getNumberOfServices());
				salesOrder.setPrice(conDetail.getItems().get(i).getPrice());
				salesOrder.setProdDesc1("This data uploded through upload program.  " + fmt.parse(modifiedDate));
				if(conDetail.getItems().get(i).getUnitOfMeasurement()!=null){
					salesOrder.setUnitOfMeasurement(conDetail.getItems().get(i).getUnitOfMeasurement());
				}
				salesOrder.setTotalAmount(conDetail.getItems().get(i).getPrice());
				salesOrder.setPaymentPercent(100);
				salesOrder.setVatTax(conDetail.getItems().get(i).getVatTax());
				salesOrder.setServiceTax(conDetail.getItems().get(i).getServiceTax());
				
				/**
				 * @author Anil , Date : 03-07-2019
				 * updating base bill amount, instead of contract product price setting outstanding amount
				 */
//				baseBillingAmount = baseBillingAmount + conDetail.getItems().get(i).getPrice();
//				logger.log(Level.SEVERE,  " Base billing amount -- " +baseBillingAmount  + " item price -- " +  conDetail.getItems().get(i).getPrice());
//				salesOrder.setBaseBillingAmount(Math.round(conDetail.getItems().get(i).getPrice()));
////				salesOrder.setPayableBillingAmt(Math.round(Long.parseLong(conDetail.getItems().get(i).getPrice()+"")));
//				salesOrder.setBasePaymentAmount(conDetail.getItems().get(i).getPrice());
				double billAmt=outstandingAmount/conDetail.getItems().size();
				salesOrder.setBaseBillingAmount(Math.round(billAmt));
				salesOrder.setBasePaymentAmount(Math.round(billAmt));
				/**
				 * nidhi
				 * 20-08-2018
				 */
				salesOrder.setProSerialNo(conDetail.getItems().get(i).getProSerialNo());
				salesOrder.setProModelNo(conDetail.getItems().get(i).getProModelNo());
				salesOrder.setPrduct(superProdEntity);
				
				salesProdArr.add(salesOrder);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			custEntry = false;
			 contEntry = false; 
			 serEntry = false;
			 billingEntry = false;
			
		}
		return salesProdArr;
	}
	
	public static String serviceDay(Date d){
		String day=null;
		
		String dayOfWeek = fmt.format(d);
		if(dayOfWeek.equals("0"))
		   return day="Sunday";
		if(dayOfWeek.equals("1"))
			return day="Monday";
		if(dayOfWeek.equals("2"))
			return day="Tuesday";
		if(dayOfWeek.equals("3"))
			return day="Wednesday";
		if(dayOfWeek.equals("4"))
			return day="Thursday";
		if(dayOfWeek.equals("5"))
			return day="Friday";
		if(dayOfWeek.equals("6"))
			return day="Saturday";
		
		return day;
	}
	
	public boolean createCountDetails(long companyId,JSONArray jsonarr){
		 boolean flag = false;
		 try {
			 GenricServiceImpl impl = new GenricServiceImpl();
			 
			 
				pro = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productCode", "PCSP001").first().now();
				System.out.println("Servicce Pro status .. "+ (pro==null));
				
				if(pro==null){
					NumberGeneration ngp1 = new NumberGeneration();
					ngp1 = ofy().load().type(NumberGeneration.class)
							.filter("companyId", companyId)
							.filter("processName", "ServiceProduct").filter("status", true)
							.first().now();
					long number = ngp1.getNumber();
					int proCount = (int) number++;
					pro = new ServiceProduct();
					pro.setProductName("Pest Control Serivce Product");
					pro.setProductCode("PCSP001");
					pro.setPrice(1.0);
					pro.setCount(proCount);
					pro.setUnitOfMeasurement("Nos");
					pro.setNumberOfService(1);
					pro.setComment("This data uploded through upload program.  " + fmt.parse(modifiedDate));
					pro.setCompanyId(companyId);
					pro.setDuration(365);
					pro.setProductCategory("Service");
					ofy().save().entity(pro).now();
					
					ngp1.setNumber(proCount);
					
					impl.save(ngp1);
				}
				
				 contractSrNo = new NumberGeneration();
				contractSrNo = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId)
						.filter("processName", "Contract").filter("status", true)
						.first().now();
				long number = contractSrNo.getNumber();
				contCount = (int) number;
				
				 customerSrNo = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId)
						.filter("processName", "Customer").filter("status", true)
						.first().now();
				 number = customerSrNo.getNumber();
				 custCount = (int) number;
				 
				
				 serNo = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId)
						.filter("processName", "Service").filter("status", true)
						.first().now();
				 number = serNo.getNumber();
				 serCount = (int) number;
				
				  billNumGe = ofy().load().type(NumberGeneration.class)
							.filter("companyId", companyId)
							.filter("processName", "BillingDocument").filter("status", true)
							.first().now();
					 number = billNumGe.getNumber();
					billCount = (int) number;
					//commented on 18-09-2024
					cust = new ArrayList<Customer>();
					
//					cust = ofy().load().type(Customer.class).filter("companyId", companyId).list();
					Gson gson = new GsonBuilder().create();
					HashSet<Integer> custIdSet=new HashSet<Integer>();
						for(int i=0;i<jsonarr.length();i++){
							try {
									JSONObject jsonObj = jsonarr.getJSONObject(i);
									CustomerContractDetails custDetail = new CustomerContractDetails();
									custDetail = gson.fromJson(jsonObj.toString(), CustomerContractDetails.class);
									if(custDetail!=null&&custDetail.getCustomerCount()>0) {
										custIdSet.add(custDetail.getCustomerCount());
									}
							}catch(Exception e) {
								
							}
						}
						logger.log(Level.SEVERE,"custIdSet size="+custIdSet.size());		
					if(custIdSet.size()>0) {
						List<Integer> custidlist = new ArrayList<Integer>();
						custidlist.addAll(custIdSet);
						cust = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count IN", custidlist).list();
						if(cust!=null)
							logger.log(Level.SEVERE,"custlist size="+cust.size());	
					}
					
			 
			flag = true;
		} catch (Exception e) {
			
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE," exception  -- " ,e.getMessage());
			flag = false;
		}
		 return flag;
	 }

	public Tax getTaxDetails(String taxName, List<TaxDetails> taxDetailslist) {
		
		try {
			
			for(TaxDetails taxDetails : taxDetailslist){
				if(taxDetails.getTaxChargeName().trim().equalsIgnoreCase(taxName.trim())){
					
					Tax tax = new Tax();
					tax.setPercentage(taxDetails.getTaxChargePercent());
					tax.setTaxConfigName(taxDetails.getTaxChargeName());
					tax.setTaxName(taxDetails.getTaxChargeName());
					tax.setTaxPrintName(taxDetails.getTaxPrintName());
					
					return tax;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * 
	 * ServiceProduct pro =  new ServiceProduct();
			pro = ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("productCode", "PCSP001").first().now();
			System.out.println("Servicce Pro status .. "+ (pro==null));
			
			if(pro==null){
				NumberGeneration ngp1 = new NumberGeneration();
				ngp1 = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId)
						.filter("processName", "ServiceProduct").filter("status", true)
						.first().now();
				long number = ngp1.getNumber();
				int proCount = (int) number;
				pro = new ServiceProduct();
				pro.setProductName("Pest Control Serivce Product");
				pro.setProductCode("PCSP001");
				pro.setPrice(1.0);
				pro.setCount(proCount);
				pro.setUnitOfMeasurement("Nos");
				pro.setNumberOfService(1);
				pro.setDuration(365);
				pro.setProductCategory("Service");
				ofy().save().entity(pro).now();
				
				ngp1.setNumber(proCount);
				
				impl.save(ngp1);
			}
			
			NumberGeneration contractSrNo = new NumberGeneration();
			contractSrNo = ofy().load().type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "Contract").filter("status", true)
					.first().now();
			long number = contractSrNo.getNumber();
			int contCount = (int) number;
			
			NumberGeneration customerSrNo = ofy().load().type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "Customer").filter("status", true)
					.first().now();
			 number = customerSrNo.getNumber();
			int custCount = (int) number;
			
			
			NumberGeneration serNo = ofy().load().type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "Service").filter("status", true)
					.first().now();
			 number = serNo.getNumber();
			 serCount = (int) number;
			
			 NumberGeneration billNumGe = ofy().load().type(NumberGeneration.class)
						.filter("companyId", companyId)
						.filter("processName", "BillingDocument").filter("status", true)
						.first().now();
				 number = billNumGe.getNumber();
				int billCount = (int) number;
			 
			List<Customer> cust = ofy().load().type(Customer.class).filter("companyId", companyId).list();
	 */
     /* added by sheetal:24-11-2021*/
	public double getTotalAmountWithTax(ArrayList<ProductOtherCharges> prodTaxeslist, double totalAmt) {
		if(prodTaxeslist.size()>0){
			for(ProductOtherCharges prodTax : prodTaxeslist){
				totalAmt += prodTax.getChargePayable();
				logger.log(Level.SEVERE, "Tax amount" + totalAmt);
			}
		}
		return Math.round(totalAmt);
	}

	private double getTotalAmountWithTax(List<ContractCharges> billingTaxes, double totalAmt) {
		
		if(billingTaxes.size()>0){
			for(ContractCharges prodTax : billingTaxes){
				totalAmt += prodTax.getPayableAmt();
				logger.log(Level.SEVERE, "Tax amount" + totalAmt);
			}
		}
		return  Math.round(totalAmt);
	}



	private double getamountpercentage(double totalAmt, double outStandAmt) {
		
		double percentage = (outStandAmt/totalAmt)*100;
		logger.log(Level.SEVERE, "percentage "+percentage);
		return percentage;
	}

	
public ArrayList<ProductOtherCharges> getproductTaxeslist(List<SalesLineItem> salesLineItemLis) {
		
		ContractInvoicePayment coninvoicepayment = new ContractInvoicePayment();
		ArrayList<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
//		NumberFormat nf=NumberFormat.getFormat("#.00");

		DecimalFormat nf = new DecimalFormat("#.00");
		
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=Double.parseDouble(nf.format(salesLineItemLis.get(i).getPrice()));

			logger.log(Level.SEVERE, "salesLineItemLis.get(i).getVatTax().getPercentage() " + salesLineItemLis.get(i).getVatTax().getPercentage());

			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=coninvoicepayment.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),
							salesLineItemLis.get(i).getVatTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
					
					double calcAmt=pocentity.getAssessableAmount()*pocentity.getChargePercent()/100;
					//return Math.round(calcAmt)+"";
					nf.format(calcAmt);
					pocentity.setChargePayable(calcAmt);
					
					taxList.add(pocentity);
				}
			}
			
			logger.log(Level.SEVERE, "salesLineItemLis.get(i).getServiceTax().getPercentage() " + salesLineItemLis.get(i).getServiceTax().getPercentage());

			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=coninvoicepayment.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),
							 		salesLineItemLis.get(i).getServiceTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					double calcAmt=pocentity.getAssessableAmount()*pocentity.getChargePercent()/100;
					//return Math.round(calcAmt)+"";
					nf.format(calcAmt);
					pocentity.setChargePayable(calcAmt);
					taxList.add(pocentity);
				}
			}
			
		}
		
		return taxList;
	}
}
