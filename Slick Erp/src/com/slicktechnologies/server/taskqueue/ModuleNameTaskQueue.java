package com.slicktechnologies.server.taskqueue;

import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class ModuleNameTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -873686335076786531L;

	Logger logger = Logger.getLogger("Name Of logger");

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		logger.log(Level.SEVERE, "Welcome to Module name Task Queue");

		String modulenamewithdoller = request.getParameter("moduleNamekey");

		logger.log(Level.SEVERE, "Module name Data === with $ ==  "
				+ modulenamewithdoller);

		String[] moduleNamewithoutdoller = modulenamewithdoller.split("[$]");

		ConfigCategory configCategory = new ConfigCategory();

		configCategory.setInternalType(Integer
				.parseInt(moduleNamewithoutdoller[0]));
		configCategory.setCategoryName(moduleNamewithoutdoller[1]);
		configCategory.setCategoryCode(moduleNamewithoutdoller[2]);
		configCategory.setCompanyId(Long.parseLong(moduleNamewithoutdoller[3]));
		configCategory.setCount(Integer.parseInt(moduleNamewithoutdoller[4]));
		configCategory.setStatus(true);

		ofy().save().entity(configCategory);

		logger.log(Level.SEVERE, "Module Name saved successfully");

	}
}
