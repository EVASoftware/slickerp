package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;
import static com.googlecode.objectify.ObjectifyService.ofy;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class Datamigrationservicetaskqueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4116675679076351422L;

	
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	Logger logger = Logger.getLogger("Name of logger");
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doPost(req, resp);
		logger.log(Level.SEVERE, "Welcome to service task of Data migration");
		System.out.println("Welcome to service task of Data migration");
		
		String servicedatawithdollar=request.getParameter("servicekey");
		String[] servicedatawithoutdollar=servicedatawithdollar.split("[$]");
		
		for (int i = 0; i < servicedatawithoutdollar.length; i++) {
			System.out.println("The Data of place "+i+"in service is :"+ servicedatawithoutdollar[i]);
		}
		
		Service service=new Service();
		
		service.setContractCount(Integer.parseInt(servicedatawithoutdollar[0]));
		try {
			service.setContractStartDate(fmt.parse(servicedatawithoutdollar[1]));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			service.setContractEndDate(fmt.parse(servicedatawithoutdollar[2]));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		service.getPersonInfo().setCount(Integer.parseInt(servicedatawithoutdollar[3]));
//		
//		service.getPersonInfo().setFullName(servicedatawithoutdollar[4]);
//		
//		service.getPersonInfo().setCellNumber(Long.parseLong(servicedatawithoutdollar[5]));
////		6 bacha h
//		service.getPersonInfo().setPocName(servicedatawithoutdollar[7]);
		
		PersonInfo personinfo = new PersonInfo();
		
		personinfo.setCount(Integer.parseInt(servicedatawithoutdollar[3]));
//		personinfo.setEmail(serviceStringwithoutdoller[1]);
		personinfo.setFullName(servicedatawithoutdollar[4]);
		personinfo.setPocName(servicedatawithoutdollar[7]);
		personinfo.setCellNumber(Long.parseLong(servicedatawithoutdollar[5]));
		
		service.setPersonInfo(personinfo);
		
		/*****************Product Section******************************/
		ServiceProduct spn=new ServiceProduct();
		spn.setProductName(servicedatawithoutdollar[8]);
		service.setProduct(spn);
		service.getProduct().setProductCode(servicedatawithoutdollar[8]);
		service.getProduct().setProductName(servicedatawithoutdollar[9]);
		ServiceProduct sp=ofy().load().type(ServiceProduct.class).filter("companyId",Long.parseLong(servicedatawithoutdollar[18])).filter("productCode",servicedatawithoutdollar[8]).first().now();
		if(sp!=null){
			service.getProduct().setCount(sp.getCount());
		}
		
		/****************Service Information****************************/
//		service.setAdHocService(false);
//		service.setServiceIndexNo(Integer.parseInt(servicedatawithoutdollar[10]));
		service.setServiceSerialNo(Integer.parseInt(servicedatawithoutdollar[10]));
		service.setEmployee(servicedatawithoutdollar[11]);
		service.setBranch(servicedatawithoutdollar[12]);
		
		String startdate=servicedatawithoutdollar[13];
		Calendar c=Calendar.getInstance();
		try {
			c.setTime(fmt.parse(startdate));
			System.out.println("The current time is :"+fmt.format(c.getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c.add(Calendar.DATE, Integer.parseInt(servicedatawithoutdollar[14]));
		System.out.println("The changed date is :"+fmt.format(c.getTime()));
		service.setServiceDate(c.getTime());
		service.setStatus("Scheduled");
		service.setServiceTime(servicedatawithoutdollar[15]);
		service.setServiceType(servicedatawithoutdollar[16]);
		service.setCount(Integer.parseInt(servicedatawithoutdollar[17]));
		service.setCompanyId(Long.parseLong(servicedatawithoutdollar[18]));
		

		ofy().save().entity(service).now();
		
//		super.doPost(request, response);
		 logger.log(Level.SEVERE,"data of service saved successfully");
		    System.out.println("All Data Saved");
	}
}
