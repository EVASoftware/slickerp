package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;


import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class NBHCInterfaceDeleteProcessQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 153388877253756971L;
	Logger logger = Logger.getLogger("Name Of logger");
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		try {
			JSONArray idlist = new JSONArray();
			JSONObject idobjObject ;
			Long companyId = Long.parseLong(req.getParameter("companyId"));
			List<Contract> contractList = new ArrayList<Contract>();
			contractList =ofy().load().type(Contract.class).filter("companyId",companyId).
					filter("employee", "Snehal Abhishek Palav").filter("approverName", "Snehal Abhishek Palav")
					.filter("group", "Fumigation Service-In House")
					.filter("category", "Service Charges - Service Tax Applicable")
					.filter("type","Rate Contract(New)").filter("createdBy","Snehal Abhishek Palav").list();
			
			logger.log(Level.SEVERE, "Server company Id  -- "+ companyId + " contractList -- " + contractList.size());
			HashSet<Integer> conId = new HashSet<Integer>();
			
			List<Contract> contractBatchList = ofy().load().type(Contract.class).filter("companyId",companyId).
			filter("employee", "Batch Employee").list();
			List<Contract> contractMainList = new ArrayList<Contract>();
			
			HashMap<String, Integer> idDetails; 
			logger.log(Level.SEVERE, "Server company Id  -- "+ companyId + " contractBatchList -- " + contractBatchList.size());
			int cont = 0;
			for(Contract con : contractList){
				if(cont<200)
				{
					idDetails = new HashMap<String, Integer>();
					
					idDetails.put("Contract_Id", con.getCount());
					
					idobjObject = new JSONObject();
					idobjObject.put("Details_Contract_Id", idDetails);
					
					idlist.put(idobjObject);
					
					contractMainList.add(con);
					conId.add(con.getCount());
				}else{
					break;
				}
//				logger.log(Level.SEVERE, "Contract count.." + con.getCount());
				cont++;
			}
			
			 cont = 0;
			for(Contract con : contractBatchList){
				if(cont<200)
				{
					idDetails = new HashMap<String, Integer>();
					
					idDetails.put("Contract_Id", con.getCount());
					
					idobjObject = new JSONObject();
					idobjObject.put("Banch_Employee_Contract_Id", idDetails);
					
					idlist.put(idobjObject);
					contractMainList.add(con);
					conId.add(con.getCount());
				}else{
					break;
				}
//				logger.log(Level.SEVERE, "Contract count.." + con.getCount());
				cont++;
			}
			
			
			
//			contractList.addAll(ofy().load().type(Contract.class).filter("companyId",companyId).
//					filter("employee", "Batch Employee").list());
			
			logger.log(Level.SEVERE, " conract list --"+ conId.size());
			List<Service> serList ;
			List<BillingDocument> billList;
			List<Invoice> invList ;
			List<CustomerPayment> payList;
			
		if(conId.size()>0){
			 serList = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
			 logger.log(Level.SEVERE, " serv list --"+ serList.size());
			 if(serList.size()>0){
					ofy().delete().entities(serList);
				}
			
			billList = ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
			logger.log(Level.SEVERE, " billList list --"+ billList.size());
			if(billList.size()>0){
				ofy().delete().entities(billList);
			}
			
			 invList =ofy().load().type(Invoice.class).filter("companyId", companyId).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
			 logger.log(Level.SEVERE, " invList list --"+ invList.size());
			 if(invList.size()>0){
					ofy().delete().entities(invList);
				}
			 
			
			payList =ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
			 logger.log(Level.SEVERE, " payList list --"+ payList.size());
			if(payList.size()>0){
				ofy().delete().entities(payList);
			}
			
				ofy().delete().entities(contractMainList);
		}
			
			
			
			 contractList=ofy().load().type(Contract.class).filter("companyId",companyId).
					filter("status", Contract.CONTRACTCANCEL).list();
			
			 contractMainList.clear();
			 conId.clear();
			 cont = 0;
			for(Contract con : contractList){
				if(cont<200)
				{
					idDetails = new HashMap<String, Integer>();
					
					idDetails.put("Contract_Id", con.getCount());
					
					idobjObject = new JSONObject();
					idobjObject.put("Cancel_Contract_Id", idDetails);
					
					idlist.put(idobjObject);
					contractMainList.add(con);
					conId.add(con.getCount());
					
				}else{
					break;
				}
//				logger.log(Level.SEVERE, "Contract cancel count.." + con.getCount());
				cont++;
			}
			
			logger.log(Level.SEVERE, "Cancle conrac list --"+ conId.size());
			if(conId.size()>0){
				 serList = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
				 logger.log(Level.SEVERE, " serv list --"+ serList.size());
				 if(serList.size()>0){
						ofy().delete().entities(serList);
					}
					
				billList = ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
				 logger.log(Level.SEVERE, " billList list --"+ billList.size());
				if(billList.size()>0){
					ofy().delete().entities(billList);
				}
					
				invList =ofy().load().type(Invoice.class).filter("companyId", companyId).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
				 logger.log(Level.SEVERE, " invList list --"+ invList.size());
				if(invList.size()>0){
					ofy().delete().entities(invList);
				}
					
				payList =ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).filter("contractCount IN", new ArrayList<Integer>(conId)).list();
				 logger.log(Level.SEVERE, " payList list --"+ payList.size());
				if(payList.size()>0){
					ofy().delete().entities(payList);
				}
				
					ofy().delete().entities(contractMainList);
			}
			
			System.out.println("Deleted contract_Id List -- "+ idlist);
			logger.log(Level.SEVERE,"Deleted contract_Id List -- "+ idlist.toString());
			
		} catch (Exception e) {
			logger.log(Level.SEVERE, " delete proccess -- " + e.getMessage() );
			e.printStackTrace();
			logger.log(Level.SEVERE, e.getLocalizedMessage());
		}
		
	}
	

}
