package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class DatamigrationPaymenttaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1997347700094560623L;

	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	Logger logger = Logger.getLogger("Name of logger");

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doPost(req, resp);
		logger.log(Level.SEVERE, "Welcome to Payment task of Data migration");
		System.out.println("Welcome to the Payment Task Queue");
		String paymentdata = request.getParameter("paymentkey");
		String[] paymentdatawithoutdollar = paymentdata.split("[$]");

		long companyId = Long.parseLong(paymentdatawithoutdollar[9]);

		for (int p = 0; p < paymentdatawithoutdollar.length; p++) {
			System.out.println("The data at " + p + " is :"
					+ paymentdatawithoutdollar[p]);
		}
		/******* Fetching Billing,Invoice and Payment Id ********************/
		int billingid = Integer.parseInt(paymentdatawithoutdollar[6]);
		int invoiceid = Integer.parseInt(paymentdatawithoutdollar[7]);
		int paymentid=Integer.parseInt(paymentdatawithoutdollar[8])	;
		/******* Done with Fetching Billing,Invoice and Payment Id ********************/

		List<Contract> listofcontract = ofy()
				.load()
				.type(Contract.class)
				.filter("companyId", companyId)
				.filter("refContractCount",
						Integer.parseInt(paymentdatawithoutdollar[0])).list();

		logger.log(Level.SEVERE, "List Size :" + listofcontract.size());

		System.out.println("Task k ander ka data:" + paymentdata);
		System.out.println("The Contract List size is :"
				+ listofcontract.size());

		///////////////////////Billing Info////////////////////////////////////

		BillingDocument billing = new BillingDocument();

		// if(listofcontract!=null){

		PersonInfo personInfo = new PersonInfo();
		personInfo.setCount(listofcontract.get(0).getCinfo().getCount());
		personInfo.setFullName(listofcontract.get(0).getCinfo().getFullName());
		personInfo.setCellNumber(listofcontract.get(0).getCinfo()
				.getCellNumber());
		personInfo.setPocName(listofcontract.get(0).getCinfo().getPocName());
		billing.setPersonInfo(personInfo);
		/***************************** Order Informaton **************************/
		billing.setContractCount(listofcontract.get(0).getCount());
		System.out.println("The Contract Count is"
				+ listofcontract.get(0).getCount());
		billing.setTotalSalesAmount(listofcontract.get(0).getNetpayable());
		
		System.out.println("Net Payable1 is :"
				+ listofcontract.get(0).getNetpayable());

		billing.setAccountType("AR");
		/******************** Done With Order Information *******************************************/

		/********* Payment Terms are Hard Coded ********************/
		ArrayList<PaymentTerms> listofpayterms = new ArrayList<PaymentTerms>();
		PaymentTerms paymentterms = new PaymentTerms();
		paymentterms.setPayTermDays(0);
		paymentterms.setPayTermPercent(100d);
		paymentterms.setPayTermComment("Default by Data Migration");
		listofpayterms.add(paymentterms);
		billing.setArrPayTerms(listofpayterms);
		/************ Done with Payment Terms *********************/

		/************* Billing Information ******************/
		billing.setCount(billingid);
		if(paymentdatawithoutdollar[3].equalsIgnoreCase("na")){
			billing.setBillingDate(new Date());
		}else{
			try {
				billing.setBillingDate(fmt.parse(paymentdatawithoutdollar[3]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(paymentdatawithoutdollar[4].equalsIgnoreCase("na")){
			billing.setInvoiceDate(new Date());
		}else{
			try {
				billing.setInvoiceDate(fmt.parse(paymentdatawithoutdollar[4]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(paymentdatawithoutdollar[5].equalsIgnoreCase("na")){
			billing.setPaymentDate(new Date());
		}else{
			try {
				billing.setPaymentDate(fmt.parse(paymentdatawithoutdollar[5]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		billing.setStatus(BillingDocument.BILLINGINVOICED);
		billing.setInvoiceCount(invoiceid);
		billing.setApproverName(paymentdatawithoutdollar[1]);
		billing.setEmployee(paymentdatawithoutdollar[2]);
		billing.setBranch(listofcontract.get(0).getBranch());
		/*************** Done With Billing Information *******************/

		/********** Billing Document Information ****************/
		billing.getBillingDocumentInfo().setBillId(billingid);
		billing.getBillingDocumentInfo().setBillingDate(new Date());
		billing.getBillingDocumentInfo().setBillAmount(
				listofcontract.get(0).getNetpayable());
		System.out.println("net payable amount 2:"
				+ listofcontract.get(0).getNetpayable());
		billing.getBillingDocumentInfo()
				.setBillstatus(BillingDocument.APPROVED);
		/********** Billing Document Information ****************/
		
		
		/************ Product Details ***************/

		int noOfProduct = listofcontract.get(0).getItems().size();
		System.out.println("The No of Product" + noOfProduct);

		ArrayList<SalesOrderProductLineItem> productTable = new ArrayList<SalesOrderProductLineItem>();

		for (int i = 0; i < noOfProduct; i++) {
			
			SalesOrderProductLineItem product = new SalesOrderProductLineItem();
			
			System.out.println("product.setProdId(listofcontract.get(0).getItems().get(i).getPrduct().getCount())"+(listofcontract.get(0).getItems().get(i).getPrduct().getCount()));
			
			if(listofcontract.get(0).getItems().get(i).getPrduct().getCount()!=0){
				
				product.setProdId(listofcontract.get(0).getItems().get(i).getPrduct().getCount());
				
			}
			
			product.setProdCategory(listofcontract.get(0).getItems().get(i)
					.getProductCategory());
			product.setProdCode(listofcontract.get(0).getItems().get(i)
					.getProductCode());
			product.setProdName(listofcontract.get(0).getItems().get(i)
					.getProductName());
			product.setQuantity(listofcontract.get(0).getItems().get(i)
					.getQty());
			product.setPrice(listofcontract.get(0).getItems().get(i).getPrice());
			product.setVatTax(listofcontract.get(0).getItems().get(i)
					.getVatTax());
			product.setServiceTax(listofcontract.get(0).getItems().get(i)
					.getServiceTax());
			// Tax tax=new Tax();
			// tax.setPercentage(14.5);
			// product.setServiceTax(tax);
			product.setTotalAmount(listofcontract.get(0).getItems().get(i)
					.getPrice());
			product.setBaseBillingAmount((listofcontract.get(0).getItems().get(i).getQty())*(listofcontract.get(0).getItems()
					.get(i).getPrice()));
			product.setPaymentPercent(100);
			// PAyable amount bacha hai

			productTable.add(product);
		}
		
		billing.setSalesOrderProducts(productTable);
		System.out.println("listofcontract.get(0).getNetpayable()"+listofcontract.get(0).getNetpayable());
		billing.setTotalBillingAmount(listofcontract.get(0).getNetpayable());
		
		
		// billing.setTotalSalesAmount(billing.getTotalBillingAmount());
		// System.out.println("The Total Value is :"+billing.getTotalBillingAmount());
		/************ Finish with Product Details ***************/
		/********************Tax Entity Saving***********************/
		
//		double servicetaxfortable=0d;
//		double assval=0d;
//		double totalamount2=0d;
////		Productta
//		for (int i = 0; i < listofcontract.get(0).getItems().size(); i++) {
//			totalamount2=totalamount2+((listofcontract.get(0).getItems().get(i).getPrice())*(listofcontract.get(0).getItems().get(i).getQty()));
//		}
	
//		ContractCharges taxtable2=new ContractCharges();
//			taxtable2.setTaxChargeName("VAT TAX");
//			taxtable2.setTaxChargePercent(0.0);
//			taxtable2.setTaxChargeAssesVal(totalamount2);
//			taxtable2.setPayableAmt(0d);
//			listoftaxtable.add(taxtable2);
//			System.out.println("Added 1st time");
////			billing.getBillingTaxes().addAll(listoftaxtable2);
	/**********************VAT Tax**********************************************/
			double assvalforvat=0d;
			List<ContractCharges> listofvattaxtable=new ArrayList<ContractCharges>();
			double totalvatpay=0d;
			List<Double> listvatpercent=new ArrayList<Double>();
			for (int j = 0; j < listofcontract.get(0).getItems().size(); j++) {
				listvatpercent.add(Double.parseDouble(listofcontract.get(0).getItems().get(j).getVatTax()+""));
			}
			HashSet<Double> uniquevat=new HashSet<Double>();
			List<Double> hashvatllist=new ArrayList<Double>();
			for (int j = 0; j < listofcontract.get(0).getItems().size(); j++) {
				uniquevat.add(Double.parseDouble(listofcontract.get(0).getItems().get(j).getVatTax()+""));
				
			}
			hashvatllist.addAll(uniquevat);
			for (int j = 0; j < hashvatllist.size(); j++) {
				System.out.println("HashSet Loop of vat"+hashvatllist.get(j));
				assvalforvat=0d;
				for (int j2 = 0; j2 < listvatpercent.size(); j2++) {
					if(hashvatllist.get(j).equals(listvatpercent.get(j2))){
						System.out.println("Ass-Val Increased");
						assvalforvat=assvalforvat+((listofcontract.get(0).getItems().get(j2).getQty())*(listofcontract.get(0).getItems().get(j2).getPrice()));
						System.out.println("assvalforvat"+assvalforvat);
					}	
				}
				System.out.println("Setting Vat Data");
				ContractCharges taxtableforvat=new ContractCharges();
				taxtableforvat.setTaxChargeName("VAT");
				taxtableforvat.setTaxChargePercent(hashvatllist.get(j));
				taxtableforvat.setTaxChargeAssesVal(assvalforvat);
				double pay=(assvalforvat/100)*hashvatllist.get(j);
				totalvatpay=totalvatpay+pay;
				taxtableforvat.setPayableAmt(pay);
				listofvattaxtable.add(taxtableforvat);
			}
			billing.getBillingTaxes().addAll((listofvattaxtable));
			
			System.out.println("Total Vat Pay is :"+totalvatpay);
	/**********************Finished with VAT Tax**********************************************/	
			double assval=0d;
			double totalassessamount=0d;
			double totalservicepay=0d;
			double assvalsrcal=0d;
			List<ContractCharges> listoftaxtable=new ArrayList<ContractCharges>();
//		List<ContractCharges> listoftaxtable=new ArrayList<ContractCharges>();
		List<Double> listservicepercent=new ArrayList<Double>();
		for (int j = 0; j < listofcontract.get(0).getItems().size(); j++) {
			listservicepercent.add(Double.parseDouble(listofcontract.get(0).getItems().get(j).getServiceTax()+""));
		}
		System.out.println("listofsaleslineitem.size()"+listofcontract.get(0).getItems().size());
		HashSet<Double> uniquetax=new HashSet<Double>();
		List<Double> hashllist=new ArrayList<Double>();
		for (int j = 0; j < listofcontract.get(0).getItems().size(); j++) {
			uniquetax.add(Double.parseDouble(listofcontract.get(0).getItems().get(j).getServiceTax()+""));
			
		}
		hashllist.addAll(uniquetax);
		
		for (int j = 0; j < hashllist.size(); j++) {
			System.out.println("HashSet Loop"+hashllist.get(j));
			assval=0d;
			for (int j2 = 0; j2 < listservicepercent.size(); j2++) {
				System.out.println("Run for"+j2);
				System.out.println(listservicepercent.get(j2));
				if(hashllist.get(j).equals(listservicepercent.get(j2))){
					System.out.println("Ass-Val Increased");
					assval=assval+(listofcontract.get(0).getItems().get(j2).getQty()*listofcontract.get(0).getItems().get(j2).getPrice());
					
					System.out.println("assval"+assval);
				}
			}
			System.out.println("Setting the data");
			ContractCharges taxtable=new ContractCharges();
			taxtable.setTaxChargeName("Service Tax");
			taxtable.setTaxChargePercent(hashllist.get(j));
			assvalsrcal=assval+(totalvatpay/hashllist.size());
			taxtable.setTaxChargeAssesVal(assvalsrcal);
			totalassessamount=totalassessamount+assvalsrcal;
			double pay=(assvalsrcal/100)*hashllist.get(j);
			totalservicepay=totalservicepay+pay;
			listoftaxtable.add(taxtable);
//			double pay=(assval/100)*hashllist.get(j);
//			taxtable.setPayableAmt(pay);
//			listoftaxtable.add(taxtable);
		}
		
		billing.getBillingTaxes().addAll(listoftaxtable);
		/********************Finished Tax Entity Saving***********************/
		
		billing.setRemark("Default by Data Migration");
		billing.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
		billing.setCompanyId(companyId);
		ofy().save().entities(billing).now();
		logger.log(Level.SEVERE, "All Data Saved of Billing");
		System.out.println("All Data Saved of Billing");
		System.out.println("Net Payable after setting value :"+billing.getTotalBillingAmount());
/**************Done With Billing Info**********************************************************/

		///////Done with Billing Data//////////////////////////////////////////////////////
		
		////////////////////Invoice Data//////////////////////////////////////////////
		
		Invoice invoice=new Invoice();
		/*********Customer Info*********************/
		PersonInfo personInfoinvoice=new PersonInfo();
		personInfoinvoice.setCount(listofcontract.get(0).getCinfo().getCount());
		personInfoinvoice.setFullName(listofcontract.get(0).getCinfo().getFullName());
		personInfoinvoice.setCellNumber(listofcontract.get(0).getCinfo().getCellNumber());
		personInfoinvoice.setPocName(listofcontract.get(0).getCinfo().getPocName());
		
		invoice.setPersonInfo(personInfo);
		/****************Done with Customer Info********************/
		
		/******Order Info****************/
		invoice.setContractCount(listofcontract.get(0).getCount());
		invoice.setTotalSalesAmount(listofcontract.get(0).getNetpayable());
		invoice.setAccountType("AR");
		/******Done Order Info****************/
		
		/***********Invoice Information*************************/
		invoice.setStatus(Invoice.APPROVED);
		invoice.setInvoiceType("Tax Invoice");
		invoice.setInvoiceAmount(listofcontract.get(0).getNetpayable());
		if(paymentdatawithoutdollar[4].equalsIgnoreCase("na")){
			invoice.setInvoiceDate(new Date());
		}else{
			try {
				invoice.setInvoiceDate(fmt.parse(paymentdatawithoutdollar[4]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(paymentdatawithoutdollar[5].equalsIgnoreCase("na")){
			invoice.setPaymentDate(new Date());
		}else{
			try {
				invoice.setPaymentDate(fmt.parse(paymentdatawithoutdollar[5]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		invoice.setApproverName(paymentdatawithoutdollar[1]);
		invoice.setEmployee(paymentdatawithoutdollar[2]);
		invoice.setBranch(listofcontract.get(0).getBranch());
		invoice.setRemark("Default by Data Migration");
		/***********Done with Invoice Information*************************/
		/*********Retriving Data of Billing**************/
		System.out.println("contractCount"+listofcontract.get(0).getCount());
		BillingDocument bill=ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("contractCount", listofcontract.get(0).getCount()).first().now();
		System.out.println("bill"+bill);
		/**************Billing Doc Info**********************/
		List<BillingDocumentDetails> listofbilldetails=new ArrayList<BillingDocumentDetails>();
		BillingDocumentDetails billingdetails=new BillingDocumentDetails();
//		System.out.println("bill count"+bill.getCount());
		billingdetails.setBillId(bill.getCount());
		billingdetails.setBillingDate(bill.getBillingDate());
		billingdetails.setBillAmount(bill.getBillingDocumentInfo().getBillAmount());
		billingdetails.setBillstatus(bill.getStatus());
		listofbilldetails.add(billingdetails);
		invoice.setArrayBillingDocument(listofbilldetails);
		invoice.setTotalBillingAmount(bill.getBillingDocumentInfo().getBillAmount());

		/**************Product Table***********************/
		System.out.println("listofcontract.get(0).getItems().size()"+listofcontract.get(0).getItems().size());
		
		ArrayList<SalesOrderProductLineItem> productTable2 = new ArrayList<SalesOrderProductLineItem>();

		for (int i = 0; i < listofcontract.get(0).getItems().size(); i++) {
			
			SalesOrderProductLineItem product = new SalesOrderProductLineItem();
			
			System.out.println("product.setProdId(listofcontract.get(0).getItems().get(i).getPrduct().getCount())"+(listofcontract.get(0).getItems().get(i).getPrduct().getCount()));
			
			if(listofcontract.get(0).getItems().get(i).getPrduct().getCount()!=0){
				
				product.setProdId(listofcontract.get(0).getItems().get(i).getPrduct().getCount());
				
			}
			
			product.setProdCategory(listofcontract.get(0).getItems().get(i)
					.getProductCategory());
			product.setProdCode(listofcontract.get(0).getItems().get(i)
					.getProductCode());
			product.setProdName(listofcontract.get(0).getItems().get(i)
					.getProductName());
			product.setQuantity(listofcontract.get(0).getItems().get(i)
					.getQty());
			product.setPrice(listofcontract.get(0).getItems().get(i).getPrice());
			product.setVatTax(listofcontract.get(0).getItems().get(i)
					.getVatTax());
			product.setServiceTax(listofcontract.get(0).getItems().get(i)
					.getServiceTax());
			// Tax tax=new Tax();
			// tax.setPercentage(14.5);
			// product.setServiceTax(tax);
			product.setTotalAmount(listofcontract.get(0).getItems().get(i)
					.getPrice());
			product.setBaseBillingAmount((listofcontract.get(0).getItems().get(i).getQty())*(listofcontract.get(0).getItems()
					.get(i).getPrice()));
			product.setPaymentPercent(100);
			// PAyable amount bacha hai

			productTable2.add(product);
		}
		
		invoice.setSalesOrderProducts(productTable2);
		/**************Billing Doc Info**********************/
		invoice.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
		invoice.setCount(invoiceid);
		invoice.setCompanyId(companyId);
		ofy().save().entities(invoice).now();
		System.out.println("All Data Saved Of Invocie");
		
		///////Invoice////////////////////////////////////////////////////////
		
		////////////////Payment Data/////////////////////////////////////////
		CustomerPayment custpay=new CustomerPayment();
		
		/*********Customer Info*********************/
		PersonInfo personInfoforpayment=new PersonInfo();
		personInfoforpayment.setCount(listofcontract.get(0).getCinfo().getCount());
		personInfoforpayment.setFullName(listofcontract.get(0).getCinfo().getFullName());
		personInfoforpayment.setCellNumber(listofcontract.get(0).getCinfo().getCellNumber());
		personInfoforpayment.setPocName(listofcontract.get(0).getCinfo().getPocName());
		
		custpay.setPersonInfo(personInfoforpayment);
		/****************Done with Customer Info********************/
		
		/************Order Information**********************/
		custpay.setContractCount(listofcontract.get(0).getCount());
		custpay.setTotalSalesAmount(listofcontract.get(0).getNetpayable());
		custpay.setAccountType("AR");
		/************Done with Order Information**********************/
		BillingDocument billforpayment=ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("contractCount", listofcontract.get(0).getCount()).first().now();
		/*********Billing Document information***************/
		List<BillingDocumentDetails> listofbilldetailsforpay=new ArrayList<BillingDocumentDetails>();
		BillingDocumentDetails billingdetailsforpay=new BillingDocumentDetails();
		billingdetailsforpay.setBillId(billforpayment.getCount());
		billingdetailsforpay.setBillingDate(billforpayment.getBillingDate());
		billingdetailsforpay.setBillAmount(billforpayment.getBillingDocumentInfo().getBillAmount());
		billingdetailsforpay.setBillstatus(billforpayment.getStatus());
		listofbilldetailsforpay.add(billingdetailsforpay);
		custpay.setArrayBillingDocument(listofbilldetailsforpay);
		custpay.setTotalBillingAmount(billforpayment.getBillingDocumentInfo().getBillAmount());
		/*********Done Billing Document information***************/
		
		/*********Invoice information***************/
		Invoice invoiceforpayment=ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", listofcontract.get(0).getCount()).first().now();
		System.out.println("invoiceforpayment"+invoiceforpayment);
		System.out.println("invoiceforpayment.getCount()"+invoiceforpayment.getCount());
		custpay.setInvoiceCount(invoiceforpayment.getCount());
		custpay.setInvoiceDate(invoiceforpayment.getInvoiceDate());
		custpay.setInvoiceType(invoiceforpayment.getInvoiceType());
		custpay.setInvoiceAmount(invoiceforpayment.getInvoiceAmount());
		/*********Done with Invoice information***************/
		
		/***********Payment Info************************/
		custpay.setCount(paymentid);
		
		if(paymentdatawithoutdollar[5].equalsIgnoreCase("na")){
			custpay.setPaymentDate(new Date());
		}else{
			try {
				custpay.setPaymentDate(fmt.parse(paymentdatawithoutdollar[5]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		custpay.setPaymentMethod(paymentdatawithoutdollar[10]);
		custpay.setEmployee(paymentdatawithoutdollar[2]);
		custpay.setStatus(CustomerPayment.CREATED);
		custpay.setPaymentAmt((int) listofcontract.get(0).getNetpayable());
		if(paymentdatawithoutdollar[10].equalsIgnoreCase("Cheque")){
			custpay.setBankAccNo(paymentdatawithoutdollar[11]);
			custpay.setBankName(paymentdatawithoutdollar[12]);
			custpay.setBankBranch(paymentdatawithoutdollar[13]);
			custpay.setChequeNo(paymentdatawithoutdollar[14]);
			try {
				custpay.setChequeDate(fmt.parse(paymentdatawithoutdollar[15]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/***********Done Payment Info************************/
		custpay.setCompanyId(companyId);
		ofy().save().entities(custpay).now();
	}
}
