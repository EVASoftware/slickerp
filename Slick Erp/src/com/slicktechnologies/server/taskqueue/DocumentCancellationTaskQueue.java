package com.slicktechnologies.server.taskqueue;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
//import java.util.function.Function;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
//import java.util.stream.Collectors;




import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tools.ant.Project;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer.Vanilla;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.logging.client.DefaultLevel.Severe;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.UpdateService;
import com.slicktechnologies.client.services.UpdateServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummaryPopUpTable;
import com.slicktechnologies.client.views.humanresource.allocation.leavecalendarallocation.AllocationResult;
import com.slicktechnologies.server.ApprovableServiceImplementor;
import com.slicktechnologies.server.CncBillServiceImpl;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.ServiceImplementor;
import com.slicktechnologies.server.ServiceListServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.UpdateServiceImpl;
import com.slicktechnologies.server.android.MarkCompletedSubmitServlet;
import com.slicktechnologies.server.android.contractwiseservice.MarkCompletedMultipleServices;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.humanresourcelayer.PaySlipServiceImpl;
import com.slicktechnologies.server.outboundwebservice.IntegrateSyncServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.CNCBillAnnexure;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.EmployeeAsset;
import com.slicktechnologies.shared.EmployeeTrackingDetails;
import com.slicktechnologies.shared.FinancialCalender;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.PettyCashDeposits;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.Purchase;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.TechnicianWareHouseDetailsList;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.CustomerTrainingDetails;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.android.EmployeeFingerDB;
import com.slicktechnologies.shared.common.android.RegisterDevice;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.billofproductmaterial.BillOfProductMaterial;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.ExpensePolicies;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessprocesslayer.PriceList;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorPriceListDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cancelsummary.CancelSummary;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.cnc.VersionDetails;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.customerlog.MaterialRequired;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.IPAddressAuthorization;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneEmi;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneType;
import com.slicktechnologies.shared.common.humanresourcelayer.attendance.EmployeeAttendance;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HolidayType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTCTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.EmployeeShift;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportConfig;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.inventory.PhysicalInventoryMaintaince;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.UploadHistory;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.relationalLayer.VendorRelation;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesperson.SalesPersonTargets;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.scheduling.TeamManagement;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListStep;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.shippingpackingprocess.PackingMethod;
import com.slicktechnologies.shared.common.shippingpackingprocess.ShippingMethod;
import com.slicktechnologies.shared.common.smshistory.SmsHistory;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class DocumentCancellationTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8974009900941440152L;

	
	Logger logger = Logger.getLogger("Document Cancellation Task Queue");

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		logger.log(Level.SEVERE, "Rohan in side  ");
		
		String taskKeyAndValue = req.getParameter("taskKeyAndValue");
		logger.log(Level.SEVERE, "Task Key And Value "	+ taskKeyAndValue);
		String[] taskValue = taskKeyAndValue.split("[$]");
		Long companyId=Long.parseLong(taskValue[1]);
		logger.log(Level.SEVERE, "Company Id "	+ companyId);
		
		if(taskValue[0].equals("PartialServiceCancellation")){
			
			int contractCount=Integer.parseInt(taskValue[2]);
			logger.log(Level.SEVERE, "PartialServiceCancellation");
			logger.log(Level.SEVERE, "CONTRACT ID : "+contractCount);
			/**
			logger.log(Level.SEVERE, "PartialServiceCancellation");
			List<Service> serviceList=new ArrayList<Service>();
			List<Contract> cancelContractList=ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", Contract.CANCELLED).list();
			logger.log(Level.SEVERE, "Cancel Contract List Size "	+ cancelContractList.size());
			Comparator<Contract> comparator = new Comparator<Contract>() {  
				@Override  
				public int compare(Contract o1, Contract o2) {
					Integer prodId1=o1.getCount();
					Integer prodId2=o2.getCount();
					return prodId1.compareTo(prodId2);  
				}  
			};  
			Collections.sort(cancelContractList,comparator);
			
			if(cancelContractList.size()!=0){
				List<String> statusList=new ArrayList<String>();
				statusList.add(Service.SERVICESTATUSCOMPLETED);
				statusList.add(Service.SERVICESTATUSSCHEDULE);
				statusList.add(Service.SERVICESTATUSRESCHEDULE);
				int counter=0;
				int contractCount=0;
				Outer:
				for(int i=contractCount;i<cancelContractList.size();i++){
					counter++;
					contractCount=cancelContractList.get(i).getCount();
					List<Service> list=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", cancelContractList.get(i).getCount()).filter("status IN", statusList).list();
					if(list.size()!=0){
						serviceList.addAll(list);
						for(Service service:list){
							if(service.getReasonForChange()!=null&&service.getReasonForChange().equals("")){
								service.setReasonForChange("Cancelled on "+new Date());
					 		}
							service.setStatus(Service.SERVICESTATUSCANCELLED);
						}
					}
					if(counter==250||counter==500||counter==750){
//					if(counter==100 || counter==200||counter==300||counter==400||counter==500){
						logger.log(Level.SEVERE, "LOOP BREAK"	+ " Counter"+counter +" Contract COUNT "+contractCount  );
						for(Service service:serviceList){
							if(service.getReasonForChange()!=null&&service.getReasonForChange().equals("")){
								service.setReasonForChange("Cancelled on "+new Date());
					 		}
							service.setStatus(Service.SERVICESTATUSCANCELLED);
						}
						if(serviceList.size()!=0){
							ofy().save().entities(serviceList).now();
							ObjectifyService.reset();
							ofy().consistency(Consistency.STRONG);
							ofy().clear();
						}
						logger.log(Level.SEVERE, "IN Service List Size  "	+ serviceList.size()+ "Successfull");
						if(counter>cancelContractList.size()){
							serviceList=new ArrayList<Service>();
							logger.log(Level.SEVERE, "Continuing LOOP"	+ contractCount);
							continue Outer;
						}
					}
				}
				logger.log(Level.SEVERE, "Service List Size "	+ serviceList.size()+ "Successfull  "+counter);
				for(Service service:serviceList){
					if(service.getReasonForChange()!=null&&service.getReasonForChange().equals("")){
						service.setReasonForChange("Cancelled on "+new Date());
			 		}
					service.setStatus(Service.SERVICESTATUSCANCELLED);
				}
				if(serviceList.size()!=0){
					ofy().save().entities(serviceList).now();
					ObjectifyService.reset();
					ofy().consistency(Consistency.STRONG);
					ofy().clear();
				}
				logger.log(Level.SEVERE, "TASK QUEUE EXECUTED SUCCESSFULLY !!!"	);
			}
			
			**/
			
			Contract contract=ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", contractCount).first().now();
			if(contract!=null){
				List<String> statusList=new ArrayList<String>();
				statusList.add(Service.SERVICESTATUSCOMPLETED);
				statusList.add(Service.SERVICESTATUSSCHEDULE);
				statusList.add(Service.SERVICESTATUSRESCHEDULE);
				List<Service> list=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contract.getCount()).filter("status IN", statusList).list();
				if(list.size()!=0){
					for(Service service:list){
						if(service.getReasonForChange()!=null&&service.getReasonForChange().equals("")){
							service.setReasonForChange("Cancelled on "+new Date());
				 		}
						service.setStatus(Service.SERVICESTATUSCANCELLED);
					}
					ofy().save().entities(list).now();
					ObjectifyService.reset();
					ofy().consistency(Consistency.STRONG);
					ofy().clear();
				}
				logger.log(Level.SEVERE, "SERVICE TO BE CANCEL SIZE "+list.size());
			}
			logger.log(Level.SEVERE, "TASK QUEUE EXECUTED SUCCESSFULLY !!!"	);
			
			
			
		}else if(taskValue[0].equals("UpdateQuickContractAndServices")){
			
			logger.log(Level.SEVERE, "UpdateQuickContractAndServices");
			
			List<Service> serviceList=new ArrayList<Service>(); 
			List<Contract> quickContractList=new ArrayList<Contract>();
			List<Contract> contractTobeUpdateList=new ArrayList<Contract>();
			List<Contract> totalContractList=ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", Contract.APPROVED).list();
			logger.log(Level.SEVERE, "Total Contract List Size "	+ totalContractList.size());
			
			for(Contract contract:totalContractList){
				if(contract.getPaymentRecieved()!=0||contract.getBalancePayment()!=0){
					quickContractList.add(contract);
				}
			}
			logger.log(Level.SEVERE, "Quick Contract List Size "	+ quickContractList.size());
			
			for(Contract contract:quickContractList){
				if(isContractDateIsLessThanStartDate(contract)){
					contractTobeUpdateList.add(contract);
				}
			}
			logger.log(Level.SEVERE, "Contract to be update List Size "	+ contractTobeUpdateList.size());
			
			
			for(Contract contract:contractTobeUpdateList){
				ArrayList<ServiceSchedule> scheduleList=new ArrayList<ServiceSchedule>();
				contract.setStartDate(contract.getContractDate());
				contract.setEndDate(getEndDateFromMaxDuration(contract));
				for(SalesLineItem item:contract.getItems()){
					item.setStartDate(contract.getContractDate());
					ArrayList<ServiceSchedule> list=getUpdatedScheduleList(contract,item);
					scheduleList.addAll(list);
				}
				
				List<Service> list=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contract.getCount()).list();
				serviceList.addAll(updateServiceList(contract,list));
			}
			logger.log(Level.SEVERE, "Service to be update List Size "	+ serviceList.size());
			
			if(contractTobeUpdateList.size()!=0){
				ofy().save().entities(contractTobeUpdateList).now();
			}
			
			if(serviceList.size()!=0){
				ofy().save().entities(serviceList).now();
			}
			
		}else if(taskValue[0].equals("UpdateQuickContractAndServices")){
			
			logger.log(Level.SEVERE, "UpdateQuickContractAndServices");
			
			List<Service> serviceList=new ArrayList<Service>(); 
			List<Contract> quickContractList=new ArrayList<Contract>();
			List<Contract> contractTobeUpdateList=new ArrayList<Contract>();
			List<Contract> totalContractList=ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", Contract.APPROVED).list();
			logger.log(Level.SEVERE, "Total Contract List Size "	+ totalContractList.size());
			
			for(Contract contract:totalContractList){
				if(contract.getPaymentRecieved()!=0||contract.getBalancePayment()!=0){
					quickContractList.add(contract);
				}
			}
			logger.log(Level.SEVERE, "Quick Contract List Size "	+ quickContractList.size());
			
			for(Contract contract:quickContractList){
				if(isContractDateIsLessThanStartDate(contract)){
					contractTobeUpdateList.add(contract);
				}
			}
			logger.log(Level.SEVERE, "Contract to be update List Size "	+ contractTobeUpdateList.size());
			
			
			for(Contract contract:contractTobeUpdateList){
				ArrayList<ServiceSchedule> scheduleList=new ArrayList<ServiceSchedule>();
				contract.setStartDate(contract.getContractDate());
				contract.setEndDate(getEndDateFromMaxDuration(contract));
				for(SalesLineItem item:contract.getItems()){
					item.setStartDate(contract.getContractDate());
					ArrayList<ServiceSchedule> list=getUpdatedScheduleList(contract,item);
					scheduleList.addAll(list);
				}
				
				List<Service> list=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contract.getCount()).list();
				serviceList.addAll(updateServiceList(contract,list));
			}
			logger.log(Level.SEVERE, "Service to be update List Size "	+ serviceList.size());
			
			if(contractTobeUpdateList.size()!=0){
				ofy().save().entities(contractTobeUpdateList).now();
			}
			
			if(serviceList.size()!=0){
				ofy().save().entities(serviceList).now();
			}
		
		}
		else if(taskValue[0].equals("UpdateQuickContractBillingInvoicePayment")){
			
			logger.log(Level.SEVERE, "UpdateQuickContractBillingInvoicePayment");
			
			List<BillingDocument>billingList=new ArrayList<BillingDocument>();
			List<Invoice>invoiceList=new ArrayList<Invoice>();
			List<CustomerPayment>paymentList=new ArrayList<CustomerPayment>();
			
			List<Contract> quickContractList=new ArrayList<Contract>();
			List<Contract> totalContractList=ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", Contract.APPROVED).list();
			logger.log(Level.SEVERE, "Total Contract List Size "	+ totalContractList.size());
			
			for(Contract contract:totalContractList){
				if(contract.getPaymentRecieved()!=0||contract.getBalancePayment()!=0){
					if(contract.getQuickContractInvoiceDate()==null){
						quickContractList.add(contract);
					}
				}
			}
			logger.log(Level.SEVERE, "Quick Contract List Size "	+ quickContractList.size());
			
			for(Contract contract:quickContractList){
				List<BillingDocument> billDocList=ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("contractCount", contract.getCount()).list();
				for(BillingDocument bill:billDocList){
					bill.setBillingDate(contract.getStartDate());
					if(bill.getInvoiceDate()!=null){
						bill.setInvoiceDate(contract.getStartDate());
					}
					if(bill.getPaymentDate()!=null){
						bill.setPaymentDate(contract.getStartDate());
					}
				}
				billingList.addAll(billDocList);
				
				List<Invoice> invList=ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", contract.getCount()).list();
				for(Invoice inv:invList){
					inv.setInvoiceDate(contract.getStartDate());
					for(BillingDocumentDetails bill:inv.getArrayBillingDocument()){
						if(bill.getOrderId()==contract.getCount()){
							bill.setBillingDate(contract.getStartDate());
						}
					}
				}
				invoiceList.addAll(invList);
				
				List<CustomerPayment> custPayList=ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("contractCount", contract.getCount()).list();
				for(CustomerPayment cust:custPayList){
					cust.setPaymentDate(contract.getStartDate());
					for(BillingDocumentDetails bill:cust.getArrayBillingDocument()){
						if(bill.getOrderId()==contract.getCount()){
							bill.setBillingDate(contract.getStartDate());
						}
					}
				}
				paymentList.addAll(custPayList);
			}
			
			logger.log(Level.SEVERE, "Billing List Size "	+ billingList.size());
			logger.log(Level.SEVERE, "Invoice List Size "	+ invoiceList.size());
			logger.log(Level.SEVERE, "Payment List Size "	+ paymentList.size());
			
			if(billingList.size()!=0){
				ofy().save().entities(billingList).now();
			}
			if(invoiceList.size()!=0){
				ofy().save().entities(invoiceList).now();
			}
			if(paymentList.size()!=0){
				ofy().save().entities(paymentList).now();
			}
		}else if(taskValue[0].equals("ApprovalTaskQueueProcess")){
			Long id=Long.parseLong(taskValue[2]);
			String status = taskValue[3];
			logger.log(Level.SEVERE, "ApprovalTaskQueueProcess");
			Approvals model=ofy().load().type(Approvals.class).id(id).now();
			model.setStatus(status);
			ApprovableServiceImplementor approvalImpl=new ApprovableServiceImplementor();
			ArrayList<ApprovableProcess> processList=approvalImpl.retrieveApprovableProcess(model.getBusinessprocesstype(), model.getBusinessprocessId(), model.getStatus(), model.getCompanyId());
			if(processList.size()==1){
				logger.log(Level.SEVERE, "Inside Approval Process  "+model.getBusinessprocesstype()+" "+model.getBusinessprocessId());
				ApprovableProcess process=processList.get(0);
				process.setStatus(model.getStatus());
				process.setRemark(model.getRemark());
				process.setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
				if(model.getStatus().equals(ConcreteBusinessProcess.APPROVED)){
					logger.log(Level.SEVERE, "Approval Code");
				    process.reactOnApproval();
				}else{
					process.reactOnRejected();
				}
				GenricServiceImpl impl=new GenricServiceImpl();
				SuperModel mod=(SuperModel) process;
				impl.save(model);
				impl.save(mod);
			}
		}
	else if(taskValue[0].equals("CancelContract")){
		int contractCount=Integer.parseInt(taskValue[2]);
		String remark = taskValue[3].trim();
		String loginUser = taskValue[4].trim();
		String status = taskValue[5].trim();
	//	reactOnCancelContract(companyId,contractCount,remark,loginUser);
		cancelOpenStatusDocuments(companyId,contractCount,remark,loginUser,status);
	}else if(taskValue[0].equals("UpdateCustomerStatus")){
		/**
		 * Date : 15-07-2017 By Anil
		 * Updating customer status to active for NBHC CCPM
		 */
		List<String> statusList=new ArrayList<String>();
		statusList.add("Created");
		statusList.add("Prospect");
		List<Customer> customerList=ofy().load().type(Customer.class).filter("companyId", companyId).filter("businessProcess.status IN", statusList).list();
		logger.log(Level.SEVERE, "Customer Size : "+customerList.size());
		for(Customer object:customerList){
			object.setStatus("Active");
		}
		
		if(customerList.size()!=0){
			ofy().save().entities(customerList);
		}
		logger.log(Level.SEVERE, "Successfully updated");
	}else if(taskValue[0].equals("updateFollowUpDateTaskQueueProcess")){
		
		/** date 26/10/2017 added by komal  to update followup date in quotation and lead **/
		List<Lead> leadList = ofy().load().type(Lead.class).filter("companyId", companyId).list();    
		for(Lead lead : leadList){
		   if(lead.getFollowUpDate() == null){
			   lead.setFollowUpDate(lead.getCreationDate());
		   }
		}	
		if(leadList.size()!= 0){
			ofy().save().entities(leadList);
		}
		
		List<Quotation> quotationList = ofy().load().type(Quotation.class).filter("companyId", companyId).list();    
		for(Quotation quotation : quotationList){
		   if(quotation.getFollowUpDate() == null){
			   quotation.setFollowUpDate(quotation.getQuotationDate());
		   }
		}	
		if(quotationList.size()!= 0){
			ofy().save().entities(quotationList);
		}
		/** date 10/03/2018 added   by komal foe sales quotation **/
		List<SalesQuotation> salesQuotationList = ofy().load().type(SalesQuotation.class).filter("companyId", companyId).list();    
		for(SalesQuotation quotation : salesQuotationList){
		   if(quotation.getFollowUpDate() == null){
			   quotation.setFollowUpDate(quotation.getQuotationDate());
		   }
		}	
		if(salesQuotationList.size()!= 0){
			ofy().save().entities(salesQuotationList);
		}
	}else if(taskValue[0].equals("ClearList")){
		/** date 27/11/2017 added by komal to cleas cancel list **/
		cancelDocumentList.clear();
	}else if(taskValue[0].equals("SelectedCancelContract")){
		/** date 01/11/2017 added by komal for selected contract renewal **/
		int contractCount = Integer.parseInt(taskValue[2]);
		int documentId = Integer.parseInt(taskValue[3]);
		String remark = taskValue[4].trim();
		String loginUser = taskValue[5].trim();
		String docType = taskValue[6].trim();
		int listSize = Integer.parseInt(taskValue[7]);
		if(docType.equalsIgnoreCase(AppConstants.SERVICETYPECUST)){
			 String str1 = req.getParameter("serviceId");
			 try{
					Gson gson = new GsonBuilder().create();	
					JSONArray jsonarr=new JSONArray(str1);
					logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
					for(int i=0;i<jsonarr.length();i++){
						contractCancel(companyId,contractCount,jsonarr.getInt(i),remark,loginUser,docType,listSize);
					}
			 }catch(Exception e){
					
				}
		}else{
			contractCancel(companyId,contractCount,documentId,remark,loginUser,docType,listSize);
		}
	}else if(taskValue[0].equals("InventoryTransaction")){
		 /** date 22/11/2017 added by komal for min transaction list **/
	    Gson gson = new GsonBuilder().create();	
		DecimalFormat df=new DecimalFormat("0.00");
		NumberGeneration numberGeneration = ofy().load().type(NumberGeneration.class)
				.filter("companyId", companyId).filter("processName", "ProductInventoryTransaction").first().now();

		if(taskValue[3].trim().equals(AppConstants.MIN)){
		MaterialIssueNote min = null;
		try {
			JSONArray jsonarr=new JSONArray(taskValue[2].trim());
			int count = Integer.parseInt(req.getParameter("count"));
			for(int i=0;i<jsonarr.length();i++){
						JSONObject jsonObj = jsonarr.getJSONObject(i);
						min = new MaterialIssueNote();
						min = gson.fromJson(jsonObj.toString(), MaterialIssueNote.class);
						if(min.getStatus().equals(MaterialIssueNote.APPROVED)){
							ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
							for (MaterialProduct product : min.getSubProductTablemin()) {
								count++;
								InventoryTransactionLineItem item = new InventoryTransactionLineItem();
								item.setCount(count);
								item.setCompanyId(min.getCompanyId());
								item.setBranch(min.getBranch());
								item.setDocumentType(AppConstants.MIN);
								item.setDocumentId(min.getCount());
								item.setDocumentDate(min.getCreationDate());
								item.setDocumnetTitle(min.getMinTitle()+ " (Cancelled MIN)");
								if(min.getServiceId()>0)
									item.setServiceId(min.getServiceId()+"");
								item.setProdId(product.getMaterialProductId());
								item.setProductName(product.getMaterialProductName());
								item.setUpdateQty(product.getMaterialProductRequiredQuantity());
								item.setOperation(AppConstants.ADD);
								item.setWarehouseName(product.getMaterialProductWarehouse());
								item.setStorageLocation(product.getMaterialProductStorageLocation());
								item.setStorageBin(product.getMaterialProductStorageBin());
								item.setOpeningStock(product.getMaterialProductAvailableQuantity());
								item.setClosingStock(Double.parseDouble(df.format(product.getMaterialProductAvailableQuantity()+ product.getMaterialProductRequiredQuantity())));
								itemList.add(item);
								
							}
							setProductInventory(itemList);
							
						}
						numberGeneration.setNumber(count);
						ofy().save().entity(numberGeneration);
						
			}	
   }catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
       }
  }	else if(taskValue[3].trim().equals(AppConstants.MMN)){	
	  MaterialMovementNote mmn = null;
		try {
			JSONArray jsonarr=new JSONArray(taskValue[2].trim());
			int count = Integer.parseInt(req.getParameter("count"));
			for(int i=0;i<jsonarr.length();i++){
						JSONObject jsonObj = jsonarr.getJSONObject(i);
						mmn = new MaterialMovementNote();
						mmn = gson.fromJson(jsonObj.toString(), MaterialMovementNote.class);
						if(mmn.getStatus().equals(MaterialMovementNote.APPROVED)){
  						ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
							for (MaterialProduct product : mmn.getSubProductTableMmn()) {
							InventoryTransactionLineItem item=new InventoryTransactionLineItem();
							count++;
							item.setCount(count);
							item.setCompanyId(mmn.getCompanyId());
							item.setBranch(mmn.getBranch());
							item.setDocumentType(AppConstants.MMN);
							item.setDocumentId(mmn.getCount());
							item.setDocumentDate(mmn.getCreationDate());
							item.setDocumnetTitle(mmn.getMmnTitle()+" (Cancelled MMN)");
							if(mmn.getServiceId()>0)
								item.setServiceId(mmn.getServiceId()+"");
							item.setProdId(product.getMaterialProductId());
							item.setProductName(product.getMaterialProductName());
							item.setUpdateQty(product.getMaterialProductRequiredQuantity());	
							item.setWarehouseName(product.getMaterialProductWarehouse());
							item.setStorageLocation(product.getMaterialProductStorageLocation());
							item.setStorageBin(product.getMaterialProductStorageBin());
							
							String transaction=mmn.getTransDirection().trim();
							String operation = null;
							double value = 0;
							if(transaction.equalsIgnoreCase("IN")){
								 operation=AppConstants.SUBTRACT;
								 value = product.getMaterialProductAvailableQuantity() - product.getMaterialProductRequiredQuantity();
							}else if(transaction.equalsIgnoreCase("OUT")){
								operation=AppConstants.ADD;
								 value = product.getMaterialProductAvailableQuantity() + product.getMaterialProductRequiredQuantity();
							}else if(transaction.equalsIgnoreCase("TRANSFEROUT")){
								operation=AppConstants.ADD;
								 value = product.getMaterialProductAvailableQuantity() + product.getMaterialProductRequiredQuantity();
							}else if(transaction.equalsIgnoreCase("TRANSFERIN")){
								operation = AppConstants.SUBTRACT;
								value = product.getMaterialProductAvailableQuantity() - product.getMaterialProductRequiredQuantity();
								item.setWarehouseName(mmn.getTransToWareHouse());
								item.setStorageLocation(mmn.getTransToStorLoc());
								item.setStorageBin(mmn.getTransToStorBin());
							}
							item.setOperation(operation);
							item.setOpeningStock(product.getMaterialProductAvailableQuantity());
       						item.setClosingStock(Double.parseDouble(df.format(value)));
       						/** 1/1/2018 added by komal for hvac **/
       						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("QuickSalesOrder", "SIDDHISERVICES" , companyId)) {
       							item.setDocumentSubType("OUT");
       						}else{
       							item.setDocumentSubType("");
       						}
							itemList.add(item);
							}
							setProductInventory(itemList);
						}
						numberGeneration.setNumber(count);
						ofy().save().entity(numberGeneration);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}		  
    }else if(taskValue[3].trim().equals(AppConstants.DELIVERYNOTE)){
    	DeliveryNote dNote = null;
		try {
			JSONArray jsonarr=new JSONArray(taskValue[2].trim());
			int count = Integer.parseInt(req.getParameter("count"));
			for(int i=0;i<jsonarr.length();i++){
						JSONObject jsonObj = jsonarr.getJSONObject(i);
						dNote = new DeliveryNote();
						dNote = gson.fromJson(jsonObj.toString(), DeliveryNote.class);
						if(dNote.getStatus().equals(DeliveryNote.APPROVED)){
							ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();
							for (DeliveryLineItems product : dNote.getDeliveryItems()) {
								count++;
								InventoryTransactionLineItem item = new InventoryTransactionLineItem();
								item.setCount(count);
								item.setCompanyId(dNote.getCompanyId());
								item.setBranch(dNote.getBranch());
								item.setDocumentType(AppConstants.DELIVERYNOTE);
								item.setDocumentId(dNote.getCount());
								item.setDocumentDate(dNote.getCreationDate());
								item.setDocumnetTitle("(Cancelled DeliveryNote)");
								item.setProdId(product.getProdId());
								item.setProductName(product.getProdName());
								item.setUpdateQty(product.getQuantity());
								item.setOperation(AppConstants.ADD);
								item.setWarehouseName(product.getWareHouseName());
								item.setStorageLocation(product.getStorageLocName());
								item.setStorageBin(product.getStorageBinName());
								item.setOpeningStock(product.getProdAvailableQuantity());
								item.setClosingStock(Double.parseDouble(df.format(product.getProdAvailableQuantity()+ product.getQuantity())));
								/**
								 * nidhi
								 * 23-08-2018
								 * for map serial number
								 */
									HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
									
								if(product.getProSerialNoDetails() != null 
										&& product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
									prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
									for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
										 ProductSerialNoMapping element = new ProductSerialNoMapping();
										 element.setAvailableStatus(pr.isAvailableStatus());
										 element.setNewAddNo(pr.isNewAddNo());
										 element.setStatus(pr.isStatus());
										 element.setProSerialNo(pr.getProSerialNo());
										 prserdt.get(0).add(element);
									}
									for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
								      {
										ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
										if(!pr.isStatus()){
												itr.remove();
										}
								      }
								}
								item.setProSerialNoDetails(prserdt);
								
								
								itemList.add(item);
								
							}
							setProductInventory(itemList);
						}
			}	
   }catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
       }
  }
  }else if(taskValue[0].equals("SelectedSalesOrderCancel")){
		/** date 01/11/2017 added by komal for selected contract renewal **/
		int salesOrderCount = Integer.parseInt(taskValue[2]);
		int documentId = Integer.parseInt(taskValue[3]);
		String remark = taskValue[4].trim();
		String loginUser = taskValue[5].trim();
		String docType = taskValue[6].trim();
		int listSize = Integer.parseInt(taskValue[7]);
		salesOrderCancel(companyId,salesOrderCount,documentId,remark,loginUser,docType,listSize);	
	}else if(taskValue[0].equals("ClearListForSalesOrder")){
		/** date 27/11/2017 added by komal to clear cancel list **/
		cancelDocumentList1.clear();
	}else if(taskValue[0].equals("InvoiceCreation")){
		  /** date 23/12/2017 added by komal for hvac invoice creation **/
		  logger.log(Level.SEVERE , "inside invoice creation");
		  int id = Integer.parseInt(taskValue[2].trim());
		  String str = taskValue[3].trim();
		  Gson gson = new GsonBuilder().create();	
		  CustomerLogDetails customerLog = null ;
		  DecimalFormat df=new DecimalFormat("0.00");
		try {
			JSONObject jsonObj = new JSONObject(str);
			customerLog = gson.fromJson(jsonObj.toString(), CustomerLogDetails.class);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		  GenricServiceImpl genimpl = new GenricServiceImpl();
		  ArrayList<SalesOrderProductLineItem> customerList=new ArrayList<SalesOrderProductLineItem>();
		  ArrayList<SalesOrderProductLineItem> companyList=new ArrayList<SalesOrderProductLineItem>();
		  
				if (customerLog.getWarrantyStatus().equalsIgnoreCase("Out Of Warranty")) {
					customerLog.getCustomerCost().addAll(customerLog.getServiceCharges());
				}
				if (customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLog.getCallType().equalsIgnoreCase("Installation")) {
					customerLog.getCompanyCost().addAll(customerLog.getServiceCharges());
				}
				if (customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && !(customerLog.getCallType().equalsIgnoreCase("Installation"))) {
					customerLog.getCompanyCost().addAll(customerLog.getServiceCharges());
				}
				if (customerLog.getWarrantyStatus().equalsIgnoreCase("AMC")) {
					customerLog.getCompanyCost().addAll(customerLog.getServiceCharges());
				}

		  SalesOrderProductLineItem salesLineItem = null;
		  for(MaterialRequired m :customerLog.getCompanyCost()){
			  SuperProduct  superProduct = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", m.getProductId()).filter("status", true).first().now();    
			  logger.log(Level.SEVERE , superProduct.toString());

				  salesLineItem = new SalesOrderProductLineItem();
				  salesLineItem.setPrduct(superProduct);
				 // salesLineItem.setArea((m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity())+"");
				  salesLineItem.setTotalAmount(m.getTotalAmount());
				  salesLineItem.setProdId(m.getProductId());
				  salesLineItem.setProdCode(m.getProductCode());
				  salesLineItem.setProdCategory(superProduct.getProductCategory());
				  salesLineItem.setProdName(m.getProductName());
				  salesLineItem.setPrice((m.getTotalAmount()+m.getDiscountAmt())/m.getProductIssueQty());
				//  salesLineItem.setQuantity((m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity()));
				  salesLineItem.setQuantity(m.getProductIssueQty());
				  salesLineItem.setVatTax(m.getVatTax());
				  salesLineItem.setServiceTax(m.getServiceTax());
				  salesLineItem.setDiscountAmt(m.getDiscountAmt());
				//  salesLineItem.setBasePaymentAmount(m.getTotalAmount());
				  salesLineItem.setBaseBillingAmount(m.getTotalAmount());
				  salesLineItem.setPaymentPercent(100.0);
				  companyList.add(salesLineItem);
		  }
		  for(MaterialRequired m :customerLog.getCustomerCost()){
			  SuperProduct  superProduct = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", m.getProductId()).filter("status", true).first().now();    
			  logger.log(Level.SEVERE , superProduct.toString());
				  salesLineItem = new SalesOrderProductLineItem();
				  salesLineItem.setPrduct(superProduct);
				 // salesLineItem.setArea((m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity())+"");
				  salesLineItem.setTotalAmount(m.getTotalAmount());
				  salesLineItem.setProdId(m.getProductId());
				  salesLineItem.setProdCode(m.getProductCode());
				  salesLineItem.setProdCategory(superProduct.getProductCategory());
				  salesLineItem.setProdName(m.getProductName());
				  salesLineItem.setPrice(m.getTotalAmount()/m.getProductIssueQty());
				//  salesLineItem.setQuantity((m.getProductIssueQty() - m.getProductReturnQty()-m.getProductPayableQuantity()));
				  salesLineItem.setQuantity(m.getProductIssueQty());
				  salesLineItem.setVatTax(m.getVatTax());
				  salesLineItem.setServiceTax(m.getServiceTax());
				  salesLineItem.setDiscountAmt(m.getDiscountAmt());
				 // salesLineItem.setBasePaymentAmount(m.getTotalAmount());
				  salesLineItem.setBaseBillingAmount(m.getTotalAmount());
				  salesLineItem.setPaymentPercent(100.0);
				  customerList.add(salesLineItem);

		  }
		  
		  logger.log(Level.SEVERE ,"company size:" + companyList.size());
		  logger.log(Level.SEVERE ,"customer size:" + customerList.size());
		  String flag = "";
		  if(customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLog.getCallType().equalsIgnoreCase("Installation")){
			// if(customerLog.getSchemeAmount()!=0){
				 flag = "payment";
			// }
		  }
		  if(customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && !(customerLog.getCallType().equalsIgnoreCase("Installation")) ){
			  flag = "bill";
		  }
		  if(customerLog.getWarrantyStatus().equalsIgnoreCase("AMC")){
			 flag = "bill";
		  }
		  if(customerLog.getWarrantyStatus().equalsIgnoreCase("Out Of Warranty")){
			  flag = "payment";
		  }
		 // if(customerList.size()!=0){
		  if(customerList.size()!=0 || flag.equalsIgnoreCase("payment")){
			  logger.log(Level.SEVERE,"inside payment ");
			  if(customerLog.getPaymentInfo().getPaymentReceived()>0){
			  CustomerPayment payment = new CustomerPayment();
			  customerLog.setStatus(CustomerLogDetails.CLOSED);
			  payment.setPersonInfo(customerLog.getCinfo());
			  //payment.setSalesOrderProducts(customerList);
			  payment.setStatus(CustomerPayment.CLOSED);
			//  payment.setInvoiceDate(new Date());
			  payment.setPaymentDate(new Date());
			  payment.setTotalSalesAmount(customerLog.getPaymentInfo().getNetPay());
			  payment.setTotalBillingAmount(customerLog.getPaymentInfo().getNetPay());
			  payment.setEmployee(customerLog.getCreatedBy());
			  payment.setPaymentAmt((int)customerLog.getPaymentInfo().getNetPay());
			  payment.setComment("Customer Payment : " + " Book no. :"+customerLog.getBookNo() +" "+" TCR no. :"+customerLog.getTcrNo() );
			  payment.setPaymentMethod(customerLog.getPaymentMethod());
			  payment.setRefNumber(customerLog.getCount()+"");
			  payment.setBankAccNo(customerLog.getPaymentInfo().getBankAccNo());
			  payment.setBankBranch(customerLog.getPaymentInfo().getBankBranch());
			  payment.setBankName(customerLog.getPaymentInfo().getBankName());
			  payment.setChequeNo(customerLog.getPaymentInfo().getChequeNo());
			  payment.setChequeDate(customerLog.getPaymentInfo().getChequeDate());
			  payment.setChequeIssuedBy(customerLog.getPaymentInfo().getChequeIssuedBy());
			  payment.setAmountTransferDate(customerLog.getPaymentInfo().getAmountTransferDate());
			  payment.setReferenceNo(customerLog.getPaymentInfo().getReferenceNo());
			  payment.setContractCount(0);
			  payment.setBranch(customerLog.getCompanyName());
			
			//payment.setNetPay(customerLog.getPaymentInfo().getNetPay());
			  payment.setPaymentReceived(customerLog.getPaymentInfo().getPaymentReceived());
			  payment.setCompanyId(customerLog.getCompanyId());
			  payment.setAccountType("AR");
			  ReturnFromServer entity  = genimpl.save(payment);
			  logger.log(Level.SEVERE ,"payment 1" + entity.count + customerLog.toString());
		  }
			  if(customerLog.getPaymentInfo().getBalancePayment()>0){
				  CustomerPayment payment1 = new CustomerPayment();
				  payment1.setPersonInfo(customerLog.getCinfo());
				  //payment.setSalesOrderProducts(customerList);
				  payment1.setStatus(CustomerPayment.CREATED);
				//  payment1.setInvoiceDate(new Date());
				  payment1.setPaymentDate(new Date());
				  payment1.setTotalSalesAmount(customerLog.getPaymentInfo().getNetPay());
				  payment1.setTotalBillingAmount(customerLog.getPaymentInfo().getNetPay());
				  payment1.setEmployee(customerLog.getCreatedBy());
				  payment1.setRefNumber(customerLog.getCount()+"");
				  payment1.setContractCount(0);
				  payment1.setBranch(customerLog.getCompanyName());
				//  payment1.setNetPay(customerLog.getPaymentInfo().getBalancePayment());
				  int balance =(int) (customerLog.getPaymentInfo().getNetPay() - customerLog.getPaymentInfo().getPaymentReceived());
				  payment1.setPaymentAmt(balance);
				  payment1.setCompanyId(customerLog.getCompanyId());
				  payment1.setAccountType("AR");
				  genimpl.save(payment1);
			  }
		  }
		 System.out.println("company List");
	     for(SalesOrderProductLineItem sale : companyList){
	    	 System.out.println("list item :"+ sale.toString());
	    	 System.out.println("list item :"+ sale.getTotalAmount());
	    	// System.out.println("list item :"+ sale.getPrduct().toString());
	     }
	     if(customerLog.getWarrantyStatus().equalsIgnoreCase("Warranty") && customerLog.getCallType().equalsIgnoreCase("Installation")&& customerLog.isSchemeFlag()){
				 flag = "bill";
		  }
		  if(companyList.size()!=0 || flag.equalsIgnoreCase("bill")){	
			  //NumberFormat nf=NumberFormat.getFormat("#.00");
			  Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("companyName", customerLog.getCompanyName().toUpperCase()).first().now();
			  customerLog.setStatus(CustomerLogDetails.CLOSED);
			  BillingDocument billing = new BillingDocument();
			  PersonInfo pinfo = new PersonInfo();
			  pinfo.setCount(customer.getCount());
			  pinfo.setFullName(customer.getCompanyName());
			  pinfo.setCellNumber(customer.getCellNumber1());
			  billing.setPersonInfo(pinfo);
			  billing.setContractCount(1);
			  billing.setBranch(customerLog.getCompanyName());
			  billing.setSalesOrderProducts(companyList);
			  billing.setStatus(Invoice.CREATED);
			  billing.setInvoiceDate(new Date());
			  billing.setPaymentDate(new Date());
			  billing.setBillingDate(new Date());
			  billing.setApproverName(customerLog.getCreatedBy());
			  billing.setRefNumber(customerLog.getCount()+"");
			  billing.setBillingCategory(customerLog.getWarrantyStatus());
			  billing.setBillingType(customerLog.getCallType());
			  billing.setComment("Customer name : " + customerLog.getCinfo().getFullName());
			  double sum = calculateTotalAmount(companyList);
			  logger.log(Level.SEVERE , "total amount : +" + sum);
		
			  billing.setCompanyId(customerLog.getCompanyId());
			  billing.setAccountType("AR");
			//  billing.setDiscountAmt(customerLog.getPaymentInfo().getDiscountAmount());
			  ArrayList<PaymentTerms> arrPayTerms = new ArrayList<PaymentTerms>();
			  PaymentTerms pt = new PaymentTerms();
			  pt.setPayTermDays(0);
			  pt.setPayTermPercent(100.0d);
			  pt.setPayTermComment(customerLog.getCompanyName() +" Payment");
			  arrPayTerms.add(pt);
			  billing.setArrPayTerms(arrPayTerms);
			  List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
			  try {
				list.addAll(addProdTaxes(companyList));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
				double assessValue=0;
				double tax = 0;
				for(int i=0;i<list.size();i++){
					ContractCharges taxDetails=new ContractCharges();
					taxDetails.setTaxChargeName(list.get(i).getChargeName());
					taxDetails.setTaxChargePercent(list.get(i).getChargePercent());
					assessValue=list.get(i).getAssessableAmount();//*paymentRecieved/100;
					taxDetails.setTaxChargeAssesVal(assessValue);				
					taxDetails.setIdentifyTaxCharge(list.get(i).getIndexCheck());
					tax = tax + taxDetails.getTaxChargeAssesVal()*taxDetails.getTaxChargePercent()/100;
					arrBillTax.add(taxDetails);
				}
			  billing.setBillingTaxes(arrBillTax);
			  billing.setTotalBillingAmount(Double.parseDouble(df.format(sum+tax)));
			  billing.setTotalAmount(Double.parseDouble(df.format(sum)));
			  billing.setFinalTotalAmt(Double.parseDouble(df.format(sum)));
			  billing.setTotalAmtIncludingTax(Double.parseDouble(df.format(sum+tax)));
			  billing.setGrandTotalAmount(Double.parseDouble(df.format(sum)));
			  billing.setBillAmount(Double.parseDouble(df.format(sum+tax)));
			  billing.setTotalSalesAmount(Math.round(Double.parseDouble(df.format(sum+tax))));
			  logger.log(Level.SEVERE , "tax amount : +" + tax);
			  genimpl.save(billing);
		   }
		   //ofy().save().entity(customerLog).now();
	      }else if(taskValue[0].equals("CreateGRN")){
	 		 /** date 22/11/2017 added by komal to create grn for defective(hvac)**/
	    	GenricServiceImpl genimpl = new GenricServiceImpl();
	  	    Gson gson = new GsonBuilder().create();	
	  	    MaterialMovementNote mmn = null;
	  			try {
					JSONArray jsonarr=new JSONArray(taskValue[2].trim());
					String approverName = req.getParameter("approverName");
					String count = createDeliveryNote(jsonarr, companyId, approverName);
					for(int i=0;i<jsonarr.length();i++){
						JSONObject jsonObj = jsonarr.getJSONObject(i);
						mmn = new MaterialMovementNote();
						mmn = gson.fromJson(jsonObj.toString(), MaterialMovementNote.class);
						mmn.setMmnGRNStatus(AppConstants.SENTTOCOMPANY);
						ArrayList<GRNDetails> list = null;
						String companyName = "";
						for(MaterialProduct m : mmn.getSubProductTableMmn())
						{
							list = new  ArrayList<GRNDetails>();
							if(m.isCreditReceived()==false){
								SuperProduct  superProduct = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", m.getMaterialProductId()).filter("status", true).first().now();
								GRNDetails poproducts = new GRNDetails();
								poproducts.setProductID(m.getMaterialProductId());
								poproducts.setProductCategory(m.getMaterialProductCategory());
								poproducts.setProductCode(m.getMaterialProductCode());
								poproducts.setProductName(m.getMaterialProductName());
								poproducts.setUnitOfmeasurement(m.getMaterialProductUOM());
								poproducts.setProdPrice(superProduct.getPrice());
								poproducts.setTax(superProduct.getServiceTax().getPercentage());
								poproducts.setVat(superProduct.getVatTax().getPercentage());
								poproducts.setProductQuantity(m.getMaterialProductRequiredQuantity());
								poproducts.setWarehouseLocation("");
								poproducts.setStorageLoc("");
								poproducts.setStorageBin("");
								companyName = m.getMaterialProductWarehouse();
								list.add(poproducts);
							}
						}
						if(list.size() >0){
							GRN grn = new GRN();
							//grn.setPoNo(mmn.getCount());
			                grn.setApproverName(mmn.getApproverName());
			                grn.setEmployee(mmn.getApproverName());
			                grn.setStatus(GRN.CREATED);
			                grn.setInspectionRequired("NO");
			                grn.setInventoryProductItem(list);
			                grn.setRefNo2(mmn.getMmnTitle());
			                grn.setCompanyId(mmn.getCompanyId());
			                grn.setRefNo(count);
			                grn.setCreationDate(new Date());
			                grn.setBranch(mmn.getBranch());
			                ReturnFromServer entity = genimpl.save(grn);
			                mmn.setMmnWoId(entity.count);
						}
						mmn.setMmnWoDate(new Date());
						ofy().save().entity(mmn);
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	      }else if(taskValue[0].equalsIgnoreCase("CreateCNCBill")){ 
				/** date 8.10.2018 added by komal **/
				GenricServiceImpl service = new GenricServiceImpl();
				logger.log(Level.SEVERE , "inside cnc bill creation");
				DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
				dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				
				String str = taskValue[2].trim();
				int id = Integer.parseInt(str);
				Date fromDate = null , toDate = null;
  				String param = taskValue[5];
  				
				try {
					fromDate = dateFormat.parse(taskValue[3]);
					toDate  = dateFormat.parse(taskValue[4]);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.log(Level.SEVERE , "Parameter" + param) ;
				CNC cnc = null ;
				HashMap<String , ArrayList<Integer>> employeeMap =  new HashMap<String , ArrayList<Integer>>();
				cnc = ofy().load().type(CNC.class).filter("companyId", companyId).filter("count", id).first().now();
				ProjectAllocation projectAllocation = ofy().load().type(ProjectAllocation.class).filter("companyId", cnc.getCompanyId()).filter("contractId",cnc.getCount()).first().now();
				Customer customer = ofy().load().type(Customer.class)
						.filter("count", cnc.getPersonInfo().getCount())
						.filter("companyId", cnc.getCompanyId()).first().now();
	
				Branch branch = ofy().load().type(Branch.class)
						.filter("companyId", cnc.getCompanyId()).filter("buisnessUnitName", cnc.getBranch()).first().now();
	
				DecimalFormat df=new DecimalFormat("0.00");
				ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
				ArrayList<SuperModel> newArrbilling=new ArrayList<SuperModel>();
				DateUtility.getDateWithTimeZone("IST",fromDate);
				DateUtility.getDateWithTimeZone("IST",toDate);
				CncBillServiceImpl serviceImpl = new CncBillServiceImpl();
				int noofdays = 0;
				ArrayList<Integer> empIdList= new ArrayList<Integer>();
				List<EmployeeInfo> empInfoList = new ArrayList<EmployeeInfo>();
				HashMap<String ,ArrayList<Attendance>> attendanceMap = new HashMap<String ,ArrayList<Attendance>>();
				if(param != null && param.equalsIgnoreCase(AppConstants.CONSUMABLES)){
					//if(cnc.getConsumablesList().size() > 0){
					logger.log(Level.SEVERE , "Parameter" + param) ;
						SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.CONSUMABLES,attendanceMap,empInfoList ,noofdays,customer ,branch, employeeMap);
						if(bill.getCount() == 0){
							newArrbilling.add(bill);
						}else{
							arrbilling.add(bill);
						}
					//}
					
				}else{
					
					/**
					 * @author Vijay @Since 21-09-2022
					 * Loading employee project allocation from different entity
					 */
					   CommonServiceImpl commonservice = new CommonServiceImpl();
					   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
				    /**
				     * ends here
				     */
//					if(projectAllocation.getEmployeeProjectAllocationList().size() >0){

					if(empprojectallocationlist.size() >0){
						logger.log(Level.SEVERE , "From date :" + fromDate);
						logger.log(Level.SEVERE , "To date :" + toDate);
						long diff= toDate.getTime() - fromDate.getTime();			
				        noofdays= ((int) (diff / (24 * 60 * 60 * 1000))) + 1;
				        logger.log(Level.SEVERE," :::=No of days  = :::  "+noofdays);
				        HashSet<Integer> idSet = new HashSet<Integer>();
//						for(EmployeeProjectAllocation pr : projectAllocation.getEmployeeProjectAllocationList()){
						for(EmployeeProjectAllocation pr : empprojectallocationlist){

							if(pr.getEmployeeId() != null && !(pr.getEmployeeId().equals(""))){
								String key = "";
								if(pr.isExtra())
									 key = "Additional " + pr.getEmployeeRole();
								else
									key = pr.getEmployeeRole();
								int count = Integer.parseInt(pr.getEmployeeId());
								idSet.add(count);
								if(employeeMap.containsKey(key)){
									empIdList = employeeMap.get(key);
									empIdList.add(count);
									employeeMap.put(key, empIdList);
								}else{
									empIdList = new ArrayList<Integer>();
									empIdList.add(count);
									employeeMap.put(key, empIdList);
								}
								
							}
						}
						ArrayList<Integer> empList=new ArrayList<Integer>(idSet);
						if(empList.size()!=0){
							empInfoList = ofy().load().type(EmployeeInfo.class).filter("companyId", cnc.getCompanyId()).filter("empCount IN", empList).list();
						}
						logger.log(Level.SEVERE , "empInfoList size before attendence load"+empInfoList.size());

						if(employeeMap.size()>0){
							/**
							 * @author Anil,Date : 07-05-2019
							 * Added parameter project name for loading attendance
							 * raised by Nitin sir,sonu
							 */
							attendanceMap = serviceImpl.loadAttandace(fromDate , toDate ,employeeMap , cnc.getCompanyId(),projectAllocation.getProjectName(),empInfoList);
						}
						logger.log(Level.SEVERE , "empInfoList size After attendence load"+empInfoList.size());

					}
	
				try {
				//	JSONObject jsonObj = new JSONObject(str);
				//	cnc = gson.fromJson(jsonObj.toString(), CNC.class);
					if(param != null && param.equalsIgnoreCase(AppConstants.ARREARSBILL)){
						if(cnc.getSaffingDetailsList() != null && cnc.getSaffingDetailsList().size() > 0){
							SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.ARREARSBILL,attendanceMap,empInfoList ,noofdays,customer ,branch,employeeMap);
							ReturnFromServer ser = null;
							if(bill.getCount() == 0){
								ser = service.save(bill);
							}else{
								ser = service.save(bill);							
							}
							BillingDocument document = (BillingDocument) bill;
							if(document != null && document.getCncBillAnnexureId().size() > 0){
							//	List<CNCBillAnnexure> list = new ArrayList<CNCBillAnnexure>();
								List<CNCBillAnnexure> annexureList = ofy().load().type(CNCBillAnnexure.class).filter("companyId", cnc.getCompanyId()).
										filter("count IN", document.getCncBillAnnexureId()).list();
								logger.log(Level.SEVERE , "bill id :" + ser.count +" " +annexureList);
								for(CNCBillAnnexure ann : annexureList){
									ann.setBillingId(ser.count);
									//list.add(ann);
									logger.log(Level.SEVERE , "bill id");
								}
								if(annexureList.size() > 0){
									ofy().save().entities(annexureList);
								}
								
							}
						}
					}else{
						if(cnc.getSaffingDetailsList()!=null&&(cnc.getSaffingDetailsList().size()!=0 || cnc.getEquipmentRentalList().size()!=0)){
							if(cnc.isStaffingRentalDifferent()){
								if(cnc.getSaffingDetailsList().size() > 0){
									SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.STAFFING,attendanceMap,empInfoList ,noofdays,customer ,branch,employeeMap);
									if(bill.getCount() == 0){
										newArrbilling.add(bill);
									}else{
										arrbilling.add(bill);
									}
								}
								if(cnc.getEquipmentRentalList().size() > 0){
									SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.EQUIPMENTRENTAL,attendanceMap,empInfoList ,noofdays,customer ,branch, employeeMap);
									if(bill.getCount() == 0){
										newArrbilling.add(bill);
									}else{
										arrbilling.add(bill);
									}
								}			
								
							}else{
								SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.STAFFINGANDRENTAL,attendanceMap,empInfoList ,noofdays,customer ,branch,employeeMap);
								if(bill.getCount() == 0){
									newArrbilling.add(bill);
								}else{
									arrbilling.add(bill);
								}
							}
						}
					
					if(cnc.getSaffingDetailsList()!= null && cnc.getSaffingDetailsList().size()>0){
						SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.NATIONALHOLIDAY,attendanceMap,empInfoList ,noofdays,customer ,branch,employeeMap);
						if(bill.getCount() != -1){
							if(bill.getCount() == 0){
								newArrbilling.add(bill);
							}else {
								arrbilling.add(bill);
							}
						}	
					}
					if(cnc.getSaffingDetailsList()!= null && cnc.getSaffingDetailsList().size()>0){
						SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.OVERTIME,attendanceMap,empInfoList ,noofdays,customer ,branch, employeeMap);
						if(bill.getCount() != -1){
							if(bill.getCount() == 0){
								newArrbilling.add(bill);
							}else {
								arrbilling.add(bill);
							}
						}	
					}
					/**
					 * @author Anil
					 * @since 21-10-2020
					 * creating Public holiday bill
					 */
					if(cnc.getSaffingDetailsList()!= null && cnc.getSaffingDetailsList().size()>0){
						SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.PUBLICHOLIDAY,attendanceMap,empInfoList ,noofdays,customer ,branch, employeeMap);
						if(bill.getCount() != -1){
							if(bill.getCount() == 0){
								newArrbilling.add(bill);
							}else {
								arrbilling.add(bill);
							}
						}	
					}
					
	//				if(cnc.getConsumablesList()!=null&&cnc.getConsumablesList().size()!=0){
	//					SuperModel bill=getBillingDocument(fromDate,toDate,cnc,null,"Consumable");
	//					arrbilling.add(bill);
	//				}
		//
	//				if(cnc.getEquipmentRentalList()!=null&&cnc.getEquipmentRentalList().size()!=0){
	//					SuperModel bill=getBillingDocument(fromDate,toDate,cnc,null,"Equipment");
	//					arrbilling.add(bill);
	//				}
		//
					if(cnc.getOtherServicesList()!=null&&cnc.getOtherServicesList().size()!=0){
						SuperModel bill=serviceImpl.getBillingDocument(fromDate,toDate,cnc,projectAllocation,AppConstants.OTHERSERVICES,attendanceMap,empInfoList ,noofdays,customer ,branch,employeeMap);
						if(bill.getCount() == 0){
							newArrbilling.add(bill);
						}else{
							arrbilling.add(bill);
						}
					}
				
				}					
			
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      }
				
				if(arrbilling.size()>0){
					service.save(arrbilling);
				}
				if(newArrbilling.size()>0){
					service.save(newArrbilling);
				}


	      
	      }
		/**
		 * Updated By: Viraj
		 * Date: 23-01-2019
		 * Description: To cancel all the documents related to purchase order
		 */
	      else if(taskValue[0].equalsIgnoreCase("SelectedCancelPO")) {
    	  
	    	  int contractCount = Integer.parseInt(taskValue[2]);
	    	  int documentId = Integer.parseInt(taskValue[3]);
	    	  String remark = taskValue[4].trim();
	    	  String loginUser = taskValue[5].trim();
	    	  String docType = taskValue[6].trim();
	    	  int listSize = Integer.parseInt(taskValue[7]);
	    	  poCancel(companyId,contractCount,documentId,remark,loginUser,docType,listSize);
	      }
//	      else if(taskValue[0].equalsIgnoreCase("CreateMIN")){
//	    	  /** date 7.3.2018 added by komal to craete MIN after service completion from android 
//	    	   *  requirement : nitin sir
//	    	   */
//	    	  String branch = taskValue[3].trim();
//		    	String loginUser = taskValue[4].trim();
//	    	  ServiceProject serProj = null;
//	    	  ServiceListServiceImpl impl = new ServiceListServiceImpl();
//			try {
//			//	jsonarr = new JSONArray(taskValue[2].trim());
//				
//	    	
//	    	  Gson gson = new GsonBuilder().create();	
//			//	for(int i=0;i<jsonarr.length();i++){
//							JSONObject jsonObj = new JSONObject(taskValue[2].trim());
//							serProj = new ServiceProject();
//							serProj = gson.fromJson(jsonObj.toString(), ServiceProject.class);
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			impl.CreateMIN(serProj ,branch , loginUser, true);
//							  
//				}
//	      else if(taskValue[0].equalsIgnoreCase("SendSRCopyEmail")){
//					int serviceId = Integer.parseInt(taskValue[2]);
//					String srNumber = taskValue[3];
//					try {
//						Thread.sleep(300000);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					String branchEmail = null;
//					try{
//						branchEmail = taskValue[4];
//					}catch(Exception e){
//						branchEmail = null;
//					}
//					
//					Email email = new Email();
//					MarkCompletedMultipleServices servlet = new MarkCompletedMultipleServices();
//					Service service = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId).first().now();
//					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", service.getCompanyId())){
//						servlet.sendEmailtoCustomer(service,srNumber,email,branchEmail);
//					}
//					
//				
//				}
//	      else if(taskValue[0].equals("updateServiceAddressLatLong")){
//				
//				/**
//				 * @author Anil , Date : 09-07-2019
//				 */
//				ObjectifyService.reset();
//				logger.log(Level.SEVERE, "Objectify Consistency Strong");
//				ofy().consistency(Consistency.STRONG);
//				ofy().clear();
//				
//				
//				/***
//				 * 
//				 */
//				try{
//				  int serviceId = Integer.parseInt(taskValue[2].trim());
//				  Service service = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId).first().now();
//				  Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count", service.getPersonInfo().getCount()).first().now();
//				 logger.log(Level.SEVERE, "Customer:");
////				 ServiceImplementor serviceImpl= new ServiceImplementor();
////				 serviceImpl.saveStatus(service);
//				  if(customer != null){
//					  if(customer.getSecondaryAdress() != null){
//						  if(service.getAddress().getAddrLine1().equalsIgnoreCase(customer.getSecondaryAdress().getAddrLine1())
//								  && service.getAddress().getCity().equalsIgnoreCase(customer.getSecondaryAdress().getCity())){
//							 if((customer.getSecondaryAdress().getLatitude() == null || customer.getSecondaryAdress().getLatitude().equals(""))&& service.getAddress().getLatitude() != null){
//								  if(!service.getAddress().getLatitude().equals("")){
//									 customer.getSecondaryAdress().setLatitude(service.getAddress().getLatitude());
//									 logger.log(Level.SEVERE, "Customer:");
//								  }
//							 }
//							 if((customer.getSecondaryAdress().getLongitude() == null || customer.getSecondaryAdress().getLongitude().equals("")) && service.getAddress().getLongitude() != null){
//								 if(!service.getAddress().getLongitude().equals("")){
//									 customer.getSecondaryAdress().setLongitude(service.getAddress().getLongitude());
//									 logger.log(Level.SEVERE, "Customer:");
//								 }
//							 
//						  }
//					  }
//					  }
//					  ofy().save().entity(customer);
//				  }
//				  CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).filter("buisnessUnitName", service.getServiceBranch()).first().now();
//				  logger.log(Level.SEVERE, "Customer Branch");
//				  if(customerBranch != null){
//					  if(customerBranch.getAddress() != null){
//						  if(service.getAddress().getAddrLine1().equalsIgnoreCase(customerBranch.getAddress().getAddrLine1())
//								  && service.getAddress().getCity().equalsIgnoreCase(customerBranch.getAddress().getCity())){
//							  if((customerBranch.getAddress().getLatitude() == null  || customerBranch.getAddress().getLatitude().equals(""))&& service.getAddress().getLatitude() != null){
//								  if(!service.getAddress().getLatitude().equals("")){
//									 customerBranch.getAddress().setLatitude(service.getAddress().getLatitude());
//									 logger.log(Level.SEVERE, "Customer Branch");
//								  }
//							  }
//							  if((customerBranch.getAddress().getLongitude() == null || customerBranch.getAddress().getLongitude().equals("")) && service.getAddress().getLongitude() != null){
//								 if(!service.getAddress().getLongitude().equals("")){
//									 customerBranch.getAddress().setLongitude(service.getAddress().getLongitude());
//									 logger.log(Level.SEVERE, "Customer Branch");
//								 }
//							  }
//						  }
//					  }
//					  ofy().save().entity(customerBranch);
//				  }
//				  List<Contract> contractList = ofy().load().type(Contract.class).filter("companyId", companyId).filter("cinfo.count", service.getPersonInfo().getCount()).list();
//				  logger.log(Level.SEVERE, "Contract");
//				  for(Contract con : contractList){
//
//					  if(con.getCustomerServiceAddress()!= null){
//						  if(service.getAddress().getAddrLine1().equalsIgnoreCase(con.getCustomerServiceAddress().getAddrLine1())
//								  && service.getAddress().getCity().equalsIgnoreCase(con.getCustomerServiceAddress().getCity())){
//							  if((con.getCustomerServiceAddress().getLatitude() == null || con.getCustomerServiceAddress().getLatitude().equals("")) && service.getAddress().getLatitude() != null){
//							  if(!service.getAddress().getLatitude().equals("")){
//								 con.getCustomerServiceAddress().setLatitude(service.getAddress().getLatitude());
//								 logger.log(Level.SEVERE, "Contract");
//							  }
//							  }
//							  if((con.getCustomerServiceAddress().getLongitude() == null || con.getCustomerServiceAddress().getLongitude().equals("") ) && service.getAddress().getLongitude() != null){
//							  if(!service.getAddress().getLongitude().equals("")){
//								 con.getCustomerServiceAddress().setLongitude(service.getAddress().getLongitude());
//								 logger.log(Level.SEVERE, "Contract");
//							  }
//							  }
//						  }
//					  }
//					  ofy().save().entity(con);
//				  
//				  }
//				  
//				  List<Service> serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", service.getPersonInfo().getCount())
//						  .filter("contractCount", service.getContractCount()).list();
//				  logger.log(Level.SEVERE, "Service");
//				  for(Service ser : serviceList){
//
//					  if(service.getAddress().getAddrLine1().equalsIgnoreCase(ser.getAddress().getAddrLine1())
//							  && service.getAddress().getCity().equalsIgnoreCase(ser.getAddress().getCity())){
//						  if((ser.getAddress().getLatitude() == null || ser.getAddress().getLatitude().equals("")) && service.getAddress().getLatitude() != null){
//							  if( !service.getAddress().getLatitude().equals("")){
//								 ser.getAddress().setLatitude(service.getAddress().getLatitude());
//								  logger.log(Level.SEVERE, "Service");
//							  }
//						  }
//						  if((ser.getAddress().getLongitude() == null  || ser.getAddress().getLongitude().equals(""))&& service.getAddress().getLongitude() != null){
//							 if( !service.getAddress().getLongitude().equals("")){
//								 ser.getAddress().setLongitude(service.getAddress().getLongitude());
//								  logger.log(Level.SEVERE, "Service");
//							 }
//						  } 
//					  }
//				    ofy().save().entity(ser);
//				  }
//			 }catch(Exception e){
//				 e.printStackTrace();
//				 ObjectifyService.reset();
//				 logger.log(Level.SEVERE, "Objectify Consistency Strong");
//				 ofy().consistency(Consistency.STRONG);
//				 ofy().clear();
//			 }
//			}	
				/**
				 * @author Anil ,Date : 13-06-2019
				 */
				else if(taskValue[0].equalsIgnoreCase("UpdateProductCode")){
					updateProductCode(companyId);
				}
		          /**Date 18-9-2019 by Amol **/
				else if(taskValue[0].equalsIgnoreCase("UpdateInvoices")){
					updateInvoices(companyId,taskValue[2],taskValue[3]);
				}
		
				/**
				 * @author Anil ,Date : 29-07-2019
				 * Payroll through taskQueue
				 */
				else if(taskValue[0].equalsIgnoreCase("ProcessPayRoll")){
					String empArrayStr=taskValue[2].trim();
					String payrollPeriod = req.getParameter("PayRollPeriod");
					logger.log(Level.SEVERE, "PAYROLL PERIOD :: "+payrollPeriod);
					logger.log(Level.SEVERE, empArrayStr);
					
					ArrayList<EmployeeInfo> empInfoArray=new ArrayList<EmployeeInfo>();
					ArrayList<Integer> empInfoId=new ArrayList<Integer>();
					
					try{
						Gson gson = new GsonBuilder().create();	
						JSONArray jsonarr=new JSONArray(empArrayStr);
						logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
						for(int i=0;i<jsonarr.length();i++){
//							JSONObject jsonObj = jsonarr.getJSONObject(i);
//							logger.log(Level.SEVERE, "JSON :: "+jsonarr.getInt(i));
//							EmployeeInfo empInfo = new EmployeeInfo();
//							empInfo = gson.fromJson(jsonObj.toString(), EmployeeInfo.class);
//							empInfoArray.add(empInfo);
							
//							int num=gson.fromJson(jsonObj.toString(), Integer.class);
							empInfoId.add(jsonarr.getInt(i));
						}
						logger.log(Level.SEVERE, "EMPLOYEE ID ARRAY SIZE :: "+empInfoId.size());
						if(empInfoId.size()!=0){
							List<EmployeeInfo> infoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empInfoId).list();
							empInfoArray.addAll(infoList);
						}
						
						logger.log(Level.SEVERE, "EMPLOYEE INFO ARRAY SIZE :: "+empInfoArray.size());
						
						PaySlipServiceImpl payImpl=new PaySlipServiceImpl();
						ArrayList<AllocationResult> result=payImpl.allocatePaySlip(empInfoArray, payrollPeriod, null, null, false);
						List<AllocationResult> unallocatedEmployees = new ArrayList<AllocationResult>();
						for (AllocationResult res : result) {
							if (res.status == false){
								unallocatedEmployees.add(res);
							}
						}
						logger.log(Level.SEVERE, "UNPROCESSED RECORDS :: "+unallocatedEmployees.size());
						
						
						if(unallocatedEmployees.size()==0){
							return;
						}
						ArrayList<String> table1=new ArrayList<String>();
						ArrayList<String> table1_Header=new ArrayList<String>();
						table1_Header.add("Id");
						table1_Header.add("Name");
						table1_Header.add("Cell Number");
						table1_Header.add("Branch");
//						table1_Header.add("Department");
						table1_Header.add("Designation");
//						table1_Header.add("Type");
//						table1_Header.add("Role");
						table1_Header.add("Comment");
						
						for(AllocationResult obj:unallocatedEmployees){
							table1.add(obj.getEmpCount()+"");
							table1.add(obj.getFullName()+"");
							table1.add(obj.getCellNumber()+"");
							table1.add(obj.getBranch()+"");
							table1.add(obj.getDesignation()+"");
							table1.add(obj.getReason()+"");
						}
						
						Company company= ofy().load().type(Company.class).filter("companyId", companyId).first().now();
						
						StringBuilder builder =new StringBuilder();
						builder.append("<!DOCTYPE html>");
						builder.append("<html lang=\"en\">");
						builder.append("<head>");
						builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
						builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
						builder.append("<h3 class=\"ex\" align=\"left\">" + "Payroll Issues "+payrollPeriod+ "</h3></br></br>");
						builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");
//						builder.append("Please find attachment.");
						
						
						
						/************************ Table 1 ***************************************/

						if (table1 != null && !table1.isEmpty()) {
							System.out.println("Table  1");
							// builder.append("<h2>"+heading1+"</h2>");

							builder.append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">");
							builder.append("<tr>");

							for(int i=0;i<table1_Header.size();i++)
							{
//								if(!table1_Header.get(i).equals("$#") && !table1_Header.get(i).equals("$##")){
//									if(table1_Header.get(i+1).equals("$#")){
//										
//										builder.append("<th colspan=\"2\">" + table1_Header.get(i) + "</th>");
//									}else if(table1_Header.get(i+1).equals("$##")){
//										builder.append("<th rowspan=\"2\">" + table1_Header.get(i) + "</th>");
//									}else{
									
										builder.append("<th>" +  table1_Header.get(i) + "</th>");
//									}
//								}
										
							}
							
//							table1_Header.remove("$#");
//							table1_Header.remove("$##");
								
							builder.append("</tr>");

							System.out.println("Size of table 1 " + table1.size());

							builder.append("<tr>");
							for (int i = 0; i < table1.size(); i++) {
//								if(!table1.get(i).equals("##")){
									builder.append("<td>" + table1.get(i) + "</td>");
//								}
								
//								System.out.println("Table Started 1" + table1.get(i));
								if (i > 0 && (i + 1) % table1_Header.size() == 0) {
									builder.append("</tr><tr>");

								}
							}
							builder.append(" </tr>  ");
							builder.append("</table></b></br>");
						}

						
						
						// Company Details
						builder.append("<br>");
						builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
						builder.append("<table>");

						builder.append("<tr>");
						builder.append("<td>");
						builder.append("Address : ");
						builder.append("</td>");
						builder.append("<td>");
						if (!company.getAddress().getAddrLine2().equals("")
								&& company.getAddress().getAddrLine2() != null)
							builder.append(company.getAddress().getAddrLine1() + " "
									+ company.getAddress().getAddrLine2() + " "
									+ company.getAddress().getLocality());
						else
							builder.append(company.getAddress().getAddrLine1() + " "
									+ company.getAddress().getLocality());
						builder.append("</td>");
						builder.append("</tr>");

						builder.append("<tr>");
						builder.append("<td>");
						builder.append("</td>");
						builder.append("<td>");
						if (!company.getAddress().getLandmark().equals("")
								&& company.getAddress().getLandmark() != null)
							builder.append(company.getAddress().getLandmark() + ","
									+ company.getAddress().getCity() + ","
									+ company.getAddress().getPin()
									+ company.getAddress().getState() + ","
									+ company.getAddress().getCountry());
						else
							builder.append(company.getAddress().getCity() + ","
									+ company.getAddress().getPin() + ","
									+ company.getAddress().getState() + ","
									+ company.getAddress().getCountry());
						builder.append("</td>");
						builder.append("</tr>");

						builder.append("<tr>");
						builder.append("<td>");
						builder.append("Website : ");
						builder.append("</td>");
						builder.append("<td>");
						builder.append(company.getWebsite());
						builder.append("</td>");
						builder.append("</tr>");

						builder.append("<tr>");
						builder.append("<td>");
						builder.append("Phone No : ");
						builder.append("</td>");
						builder.append("<td>");
						builder.append(company.getCellNumber1());
						builder.append("</td>");
						builder.append("<tr>");

						builder.append("</tr>");
						builder.append("</table>");
						builder.append("</body>");
						builder.append("</html>");

						String contentInHtml = builder.toString();
						ArrayList<String> emailList = new ArrayList<String>();
						emailList.add(company.getEmail());
						/**
						 * @author Anil , Date : 07-12-2019
						 * Payroll issue mail should also go to company POC
						 */
						if(company.getPocEmail()!=null&&!company.getPocEmail().equals("")){
							emailList.add(company.getPocEmail());
						}

						/**
						 * @author Anil , Date : 10-04-2020
						 * added display name for from email id
						 * ,company.getDisplayNameForCompanyEmailId()
						 */
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",companyId)){
							logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + emailList.toString());
							SendGridEmailServlet sdEmail=new SendGridEmailServlet();
							sdEmail.sendMailWithSendGrid(company.getEmail(), emailList, new ArrayList<String>(), new ArrayList<String>(), "Payroll Issues : "+ payrollPeriod, contentInHtml, "text/html",null,null,null,company.getDisplayNameForCompanyEmailId());
						}else{
							logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + emailList.toString());
							Email email = new Email();
							email.sendMailWithGmail(company.getEmail(), emailList,new ArrayList<String>(), new ArrayList<String>(), "Payroll Issues : "+ payrollPeriod, contentInHtml, "text/html",null,null,null);
						}
						
						
					}catch(Exception e){
						logger.log(Level.SEVERE,"INSIDE EXCEPTION...!!! ");
						e.printStackTrace();
					}

				}else if(taskValue[0].equalsIgnoreCase("UpdateDuplicateServices")){
					updateDuplicateServices(companyId,taskValue[2],taskValue[3],taskValue[4],taskValue[5],taskValue[6],taskValue[7],taskValue[8],taskValue[9]);
				}else if(taskValue[0].equalsIgnoreCase("UpdateDuplicateCustomerBranch")){
//					updateDuplicateServices(companyId,taskValue[2],taskValue[3],taskValue[4]);
					updateDuplicateCustomerBranch(companyId,taskValue[2]);
				}else if(taskValue[0].equalsIgnoreCase("UpdateFrequencyInServices")){
					updateFrequencyInServices(companyId,taskValue[2]);
				}else if(taskValue[0].equalsIgnoreCase("deleteDuplicateLeaves")){//Ashwini Patil Date:23-08-2023
					deleteDuplicateLeaves(companyId,taskValue[2],taskValue[3]);
				}else if(taskValue[0].equalsIgnoreCase("generateIRN")) {//Ashwini Patil Date:31-08-2023
					generateIrn(companyId,taskValue[2],taskValue[3],taskValue[4]);
				}else if(taskValue[0].equalsIgnoreCase("rescheduleForPeriod")){
					rescheduleForPeriod(companyId,taskValue[2],taskValue[3],taskValue[4]);
				}
				else if(taskValue[0].equalsIgnoreCase("ResetAppid")){
					resetAppid(companyId,taskValue[2],taskValue[3],taskValue[4],taskValue[5],taskValue[6]);
				}
	}

	

	private void updateDuplicateCustomerBranch(Long companyId, String custIdInString) {
		// TODO Auto-generated method stub
		HashMap<Integer,ArrayList<CustomerBranchDetails>> notToUpdateServMap=new HashMap<Integer,ArrayList<CustomerBranchDetails>>();
		HashMap<Integer,ArrayList<CustomerBranchDetails>> toUpdateServMap=new HashMap<Integer,ArrayList<CustomerBranchDetails>>();
		
		int custId=0;
		try{
			custId=Integer.parseInt(custIdInString);
			logger.log(Level.SEVERE,"INSIDE UPDATE DUPLICATE CUSTOMER BRANCH : "+custId);
		}catch(Exception e){
			
		}
		
		List<CustomerBranchDetails> customerBranchDetailList=ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).filter("cinfo.count", custId).list();
		if(customerBranchDetailList!=null&&customerBranchDetailList.size()!=0){
			logger.log(Level.SEVERE,"CUSTOMER BRANCH SIZE : "+customerBranchDetailList.size());
			
			 List<CustomerBranchDetails> duplicateCustBranchList=getDuplicateCustomerBranches(customerBranchDetailList);
			 logger.log(Level.SEVERE,"Duplicate SER LIST : "+duplicateCustBranchList.size());
			 if(duplicateCustBranchList.size()!=0){
				 
				 for(CustomerBranchDetails custBranch:duplicateCustBranchList){
						
					 if(notToUpdateServMap!=null&&notToUpdateServMap.size()!=0){
						 if(notToUpdateServMap.containsKey(custBranch.getCount())){
							 if(toUpdateServMap!=null&&toUpdateServMap.size()!=0){
								 if(toUpdateServMap.containsKey(custBranch.getCount())){
									 toUpdateServMap.get(custBranch.getCount()).add(custBranch);
								 }else{
									 ArrayList<CustomerBranchDetails> list=new ArrayList<CustomerBranchDetails>();
									 list.add(custBranch);
									 toUpdateServMap.put(custBranch.getCount(), list); 
								 }
							 }else{
								 ArrayList<CustomerBranchDetails> list=new ArrayList<CustomerBranchDetails>();
								 list.add(custBranch);
								 toUpdateServMap.put(custBranch.getCount(), list);
							 }
						 }else{
							 ArrayList<CustomerBranchDetails> list=new ArrayList<CustomerBranchDetails>();
							 list.add(custBranch);
							 notToUpdateServMap.put(custBranch.getCount(), list); 
						 }
						 
					 }else{
						 ArrayList<CustomerBranchDetails> list=new ArrayList<CustomerBranchDetails>();
						 list.add(custBranch);
						 notToUpdateServMap.put(custBranch.getCount(), list); 
					 }
				 }
				 
				 ArrayList<CustomerBranchDetails> toUpdateServiceList=new ArrayList<CustomerBranchDetails>();
				 for (Map.Entry<Integer,ArrayList<CustomerBranchDetails>> entry : toUpdateServMap.entrySet()){
					 toUpdateServiceList.addAll(entry.getValue());
				 }
				 logger.log(Level.SEVERE,"SERVICE LIST TO BE UPDATED "+toUpdateServiceList.size()); 
				 
//				
				 
				 
				 if(toUpdateServiceList.size()!=0){
					 long totalNoOfServices=toUpdateServiceList.size();
					 SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
					 long finalno=numGen.getLastUpdatedNumber("CustomerBranchDetails",companyId, totalNoOfServices);
					 for(CustomerBranchDetails obj:toUpdateServiceList){
						int count= (int) finalno;
						obj.setCount(count); 
						finalno++;
					 }
					 ofy().save().entities(toUpdateServiceList).now();
				 }
			 }
		}
	}
	
	private List<CustomerBranchDetails> getDuplicateCustomerBranches(List<CustomerBranchDetails> customerBranchDetailList) {
		// TODO Auto-generated method stub
		ArrayList<Integer> custBranchIdList=new ArrayList<Integer>();
		ArrayList<Integer> duplicateCustBranchIdList=new ArrayList<Integer>();
		ArrayList<CustomerBranchDetails> duplicateCustBranchList=new ArrayList<CustomerBranchDetails>();
		
		for(CustomerBranchDetails obj:customerBranchDetailList){
			if(custBranchIdList.size()!=0){
				if(custBranchIdList.contains(obj.getCount())){
					if(duplicateCustBranchIdList.size()!=0&&duplicateCustBranchIdList.contains(obj.getCount())){
						
					}else{
						duplicateCustBranchIdList.add(obj.getCount());
					}
				}else{
					custBranchIdList.add(obj.getCount());
				}
			}else{
				custBranchIdList.add(obj.getCount());
			}
		}
		
		logger.log(Level.SEVERE , "UNIQUE DUPLICATE CustomerBranchDetails LIST SIZE "+duplicateCustBranchIdList.size() );
		
		if(duplicateCustBranchIdList.size()!=0){
			for(Integer serviceId:duplicateCustBranchIdList){
				for(CustomerBranchDetails obj:customerBranchDetailList){
					if(serviceId==obj.getCount()){
						duplicateCustBranchList.add(obj);
					}
				}
			}
		}
		logger.log(Level.SEVERE , "DUPLICATE CustomerBranchDetails LIST SIZE "+duplicateCustBranchIdList.size() );
		return duplicateCustBranchList;
	}

	public void updateDuplicateServices(Long companyId, String fromDateInString,String toDateInString,String servIdInString, String day1String,String day2String,String contractIdInString,String fromServiceIdInString,String toServiceIdInString) {
		// TODO Auto-generated method stub
		 SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();		 
		 SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		 HashMap<Integer,ArrayList<Service>> completedServMap=new HashMap<Integer,ArrayList<Service>>();
		 HashMap<Integer,ArrayList<Service>> notToUpdateServMap=new HashMap<Integer,ArrayList<Service>>();
		 HashMap<Integer,ArrayList<Service>> toUpdateServMap=new HashMap<Integer,ArrayList<Service>>();
		
		 ArrayList<Service> cancelledFutureServiceList=new ArrayList<>();
		 
		 int serviceId=0;
		 int contractId=0;
		 int fromServiceId=0;
		 int toServiceId=0;
		 
		 try{
			 serviceId=Integer.parseInt(servIdInString);
		 }catch(Exception e){
			 
		 }
		 try{
			 contractId=Integer.parseInt(contractIdInString);
		 }catch(Exception e){
			 
		 }
		 try{
			 fromServiceId=Integer.parseInt(fromServiceIdInString);
			 toServiceId=Integer.parseInt(toServiceIdInString);
		 }catch(Exception e){
			 
		 }
		 
		 
		 Date fromDate=null;
		 Date toDate=null;
		 Date day1start=null;
		 Date day1end=null;
		 Date day2start=null;
		 Date day2end=null;
		 List<Service> serviceList=new ArrayList<Service>();
		 
		 if(serviceId==0){
			 if(fromDateInString!=null&&toDateInString!=null&&!fromDateInString.equals("null")&&!toDateInString.equals("null")){
				 logger.log(Level.SEVERE,"in fromdate todate");
				 try {
						fromDate=dateFormat.parse(fromDateInString);
					 } catch (ParseException e) {
						e.printStackTrace();
					 }
					 try {
						toDate=dateFormat.parse(toDateInString);
						 
						Calendar cal2=Calendar.getInstance();
						cal2.setTime(toDate);
//						cal2.add(Calendar.DATE, 0);
						
						
						cal2.set(Calendar.HOUR_OF_DAY,23);
						cal2.set(Calendar.MINUTE,59);
						cal2.set(Calendar.SECOND,59);
						cal2.set(Calendar.MILLISECOND,999);
						toDate=cal2.getTime();
					 } catch (ParseException e) {
						e.printStackTrace();
					 }
					 
					 
					 logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate+"day1 :"+day1String+"day2 :"+day2String);
					 
					 ArrayList<String> statusList=new ArrayList<String>();
					 statusList.add(Service.SERVICESTATUSSCHEDULE);
					 statusList.add(Service.SERVICESTATUSRESCHEDULE);
					 statusList.add(Service.SERVICESTATUSCOMPLETED);
					 statusList.add(Service.SERVICESTATUSCANCELLED);
					 
					 serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("status IN", statusList).filter("creationDate >=", fromDate).filter("creationDate <=", toDate).list();
//					 serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("status IN", statusList).filter("serviceDate >=", fromDate).filter("serviceDate <=", toDate).list();
					 
			 }else if(day1String!=null&&day2String!=null&&!day1String.equalsIgnoreCase("null")&&!day2String.equalsIgnoreCase("null")){
				 logger.log(Level.SEVERE,"in day1 day2");
				 try {
						day1start=dateFormat.parse(day1String);
					 } catch (ParseException e) {
						e.printStackTrace();
					 }
					 try {
						day1end=dateFormat.parse(day1String);
						 
						Calendar cal2=Calendar.getInstance();
						cal2.setTime(day1end);
//						cal2.add(Calendar.DATE, 0);
						
						
						cal2.set(Calendar.HOUR_OF_DAY,23);
						cal2.set(Calendar.MINUTE,59);
						cal2.set(Calendar.SECOND,59);
						cal2.set(Calendar.MILLISECOND,999);
						day1end=cal2.getTime();
					 } catch (ParseException e) {
						e.printStackTrace();
					 }
					 
					 try {
							day2start=dateFormat.parse(day2String);
						 } catch (ParseException e) {
							e.printStackTrace();
						 }
						 try {
							day2end=dateFormat.parse(day2String);
							 
							Calendar cal2=Calendar.getInstance();
							cal2.setTime(day2end);
//							cal2.add(Calendar.DATE, 0);
							
							
							cal2.set(Calendar.HOUR_OF_DAY,23);
							cal2.set(Calendar.MINUTE,59);
							cal2.set(Calendar.SECOND,59);
							cal2.set(Calendar.MILLISECOND,999);
							day2end=cal2.getTime();
						 } catch (ParseException e) {
							e.printStackTrace();
						 }
					 
					 
					 logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate+"day1 :"+day1start+"-"+day1end+"day2 :"+day2start+"-"+day2end);
					 
					 ArrayList<String> statusList=new ArrayList<String>();
					 statusList.add(Service.SERVICESTATUSSCHEDULE);
					 statusList.add(Service.SERVICESTATUSRESCHEDULE);
					 statusList.add(Service.SERVICESTATUSCOMPLETED);
					 statusList.add(Service.SERVICESTATUSCANCELLED);
					 
					 List<Service> serviceList1=new ArrayList<Service>();
					 List<Service> serviceList2=new ArrayList<Service>();
					 serviceList1=ofy().load().type(Service.class).filter("companyId", companyId).filter("status IN", statusList).filter("creationDate >=", day1start).filter("creationDate <=", day1end).list();
					 serviceList2=ofy().load().type(Service.class).filter("companyId", companyId).filter("status IN", statusList).filter("creationDate >=", day2start).filter("creationDate <=", day2end).list();
					 if(serviceList1!=null) {
						 serviceList.addAll(serviceList1);
						 logger.log(Level.SEVERE,"serviceList1 size : "+serviceList1.size()+" serviceList size : "+serviceList.size());
					 }
					 if(serviceList2!=null) {
						 serviceList.addAll(serviceList2);
						 logger.log(Level.SEVERE,"serviceList2 size : "+serviceList2.size()+" serviceList size : "+serviceList.size());							
					 }
			 }else if(contractId>0) {
				 List<Service> contractwiseserviceList=new ArrayList<Service>();
				 contractwiseserviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractId).list();					
				 ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
				 if(contractwiseserviceList!=null&&contractwiseserviceList.size()>0) {
					 logger.log(Level.SEVERE,"contractwiseserviceList size : "+contractwiseserviceList.size());
					 for(Service s:contractwiseserviceList)
						 serviceIdList.add(s.getCount());
					 logger.log(Level.SEVERE,"serviceIdList size : "+serviceIdList.size());	
					 serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceIdList).list();						
				 }
			 }else if(fromServiceId>0&&toServiceId>0) {
				 ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
				 for(int i=fromServiceId;i<=toServiceId;i++)
					 serviceIdList.add(i);
				 logger.log(Level.SEVERE,"serviceIdList size : "+serviceIdList.size());
				 serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceIdList).list();
					
			 }
		 	 
				
		 }else{
			 serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId).list();
		 }
		 
		
		
		 
		 if(serviceList!=null&&serviceList.size()!=0){
			 logger.log(Level.SEVERE,"SER LIST : "+serviceList.size());
			 List<Service> duplicateServiceList=getDuplicateServices(serviceList);
			 logger.log(Level.SEVERE,"Duplicate SER LIST : "+duplicateServiceList.size());
			 if(duplicateServiceList.size()!=0){
				 
				 //Completed services
				 for(Service service:duplicateServiceList){
					 if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						 if(completedServMap!=null&&completedServMap.size()!=0){
							 if(completedServMap.containsKey(service.getCount())){
								 completedServMap.get(service.getCount()).add(service);
							 }else{
								 ArrayList<Service> list=new ArrayList<Service>();
								 list.add(service);
								 completedServMap.put(service.getCount(), list); 
							 }
						 }else{
							 ArrayList<Service> list=new ArrayList<Service>();
							 list.add(service);
							 completedServMap.put(service.getCount(), list);
						 }
					 }
				 }
				 
				 //getting the service whose number range to be updated
				 for(Service service:duplicateServiceList){
					 if(!service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						 //Cancelled future services is to be deleted
						 if(service.getStatus().equals(Service.SERVICESTATUSCANCELLED)&&service.getServiceDate().after(new Date())){
							 cancelledFutureServiceList.add(service);
							 continue;
						 }
						 
						 if(completedServMap!=null&&completedServMap.size()!=0&&completedServMap.containsKey(service.getCount())){
							 if(toUpdateServMap!=null&&toUpdateServMap.size()!=0){
								 if(toUpdateServMap.containsKey(service.getCount())){
									 toUpdateServMap.get(service.getCount()).add(service);
								 }else{
									 ArrayList<Service> list=new ArrayList<Service>();
									 list.add(service);
									 toUpdateServMap.put(service.getCount(), list); 
								 }
							 }else{
								 ArrayList<Service> list=new ArrayList<Service>();
								 list.add(service);
								 toUpdateServMap.put(service.getCount(), list);
							 } 
						 }else{
							 if(notToUpdateServMap!=null&&notToUpdateServMap.size()!=0){
								 if(notToUpdateServMap.containsKey(service.getCount())){
									 if(toUpdateServMap!=null&&toUpdateServMap.size()!=0){
										 if(toUpdateServMap.containsKey(service.getCount())){
											 toUpdateServMap.get(service.getCount()).add(service);
										 }else{
											 ArrayList<Service> list=new ArrayList<Service>();
											 list.add(service);
											 toUpdateServMap.put(service.getCount(), list); 
										 }
									 }else{
										 ArrayList<Service> list=new ArrayList<Service>();
										 list.add(service);
										 toUpdateServMap.put(service.getCount(), list);
									 }
								 }else{
									 ArrayList<Service> list=new ArrayList<Service>();
									 list.add(service);
									 notToUpdateServMap.put(service.getCount(), list); 
								 }
								 
							 }else{
								 ArrayList<Service> list=new ArrayList<Service>();
								 list.add(service);
								 notToUpdateServMap.put(service.getCount(), list); 
							 }
						 }
					 }
				 }
				 logger.log(Level.SEVERE,"COMP MAP "+completedServMap.size()+" NOT MAP "+notToUpdateServMap.size()+" UPDATE MAP "+toUpdateServMap.size());
				 
				 ArrayList<Service> toUpdateServiceList=new ArrayList<Service>();
				 for (Map.Entry<Integer,ArrayList<Service>> entry : toUpdateServMap.entrySet()){
					 toUpdateServiceList.addAll(entry.getValue());
				 }
				 logger.log(Level.SEVERE,"SERVICE LIST TO BE UPDATED "+toUpdateServiceList.size()); 
				 
//				 ArrayList<String> statusList1=new ArrayList<String>();
//				 statusList1.add(Service.SERVICESTATUSSCHEDULE);
//				 statusList1.add(Service.SERVICESTATUSRESCHEDULE);
				 List<Service> zeroIdServiceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count", 0).list();
				 if(zeroIdServiceList!=null&&zeroIdServiceList.size()!=0){
					 toUpdateServiceList.addAll(zeroIdServiceList);
					 logger.log(Level.SEVERE,"SERVICE LIST TO BE UPDATED "+toUpdateServiceList.size()+" ZERO : "+zeroIdServiceList.size()); 
				 }
				 
				 
				 if(toUpdateServiceList.size()!=0){
					 HashSet<String> numberranges=new HashSet<String>();
					 HashMap<String, ArrayList<Service>> serviceNumberRangeMap=new HashMap<String, ArrayList<Service>>();
					 ArrayList<Service> servicesWithoutNumberRange=new ArrayList<Service>();
					 for(Service s:toUpdateServiceList) {
						 if(s.getNumberRange()!=null&&!s.getNumberRange().equals("")) {
							 numberranges.add(s.getNumberRange().trim());
						 }else {
							 numberranges.add("Service");
							 servicesWithoutNumberRange.add(s);
						 }
					 }
					 serviceNumberRangeMap.put("Service", servicesWithoutNumberRange);
					 logger.log(Level.SEVERE,"numberranges found "+numberranges.size());
					 
					 for(String range:numberranges) {
						 if(!range.equals("Service")) {
							 ArrayList<Service> tempserviceList=new ArrayList<Service>();
							 for(Service s:toUpdateServiceList) {
								 if(s.getNumberRange()!=null&&s.getNumberRange().equals(range)) {
									 tempserviceList.add(s);
								 }
							 }
							 serviceNumberRangeMap.put(range, tempserviceList);						 
						 }
					 }
					 
					 for(String range:numberranges) {
						 ArrayList<Service> rangeserviceList=new ArrayList<Service>();
						 rangeserviceList=serviceNumberRangeMap.get(range);
						 logger.log(Level.SEVERE,"range ="+range+" serviceListSize="+rangeserviceList.size());
						 if(rangeserviceList.size()>0) {
							 long totalNoOfServices=rangeserviceList.size();
							 long finalno=0;
							 if(range.equals("Service")||range.equals("null")||range==null)//null condition added on 13-06-2023 by Ashwini Patil
								  finalno=numGen.getLastUpdatedNumber("Service",companyId, totalNoOfServices);
							 else
								 finalno=numGen.getLastUpdatedNumber("S_"+range,companyId, totalNoOfServices);
							 
							 for(Service obj:rangeserviceList){
								int count= (int) finalno;
								/**
								 * @author Anil , Date : 15-04-2020
								 * if have to update service id of fumigation services then also  have to 
								 * update fumigationReport table as well
								 */
								if(obj.isWmsServiceFlag()){
									if(obj.getProduct().getProductCode().equals("STK-01")){
										FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
												.filter("companyId", obj.getCompanyId()).filter("contractId", obj.getContractCount())
												.filter("warehouseCode", obj.getRefNo2()).filter("compartment", obj.getCompartment())
												.filter("shade", obj.getShade()).filter("fumigationServiceId", obj.getCount()).first().now();
										if(fumigationReport!=null){
											logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
											fumigationReport.setFumigationServiceId(count);
											ofy().save().entity(fumigationReport);
											logger.log(Level.SEVERE, "fumigationReport updated successfully");
										}
									}else if(obj.getProduct().getProductCode().equals("PHM-01")){
										FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
												.filter("companyId", obj.getCompanyId()).filter("contractId", obj.getContractCount())
												.filter("warehouseCode", obj.getRefNo2()).filter("compartment", obj.getCompartment())
												.filter("shade", obj.getShade()).filter("prophylacticServiceId", obj.getCount()).first().now();
										if(fumigationReport!=null){
											logger.log(Level.SEVERE, "prophylacticServiceId Fumigation Report id"+fumigationReport.getCount());
											fumigationReport.setProphylacticServiceId(count);
											ofy().save().entity(fumigationReport);
											logger.log(Level.SEVERE, "fumigationReport updated successfully");
										}
										
									}else if(obj.getProduct().getProductCode().equals("DSSG")){
										FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
												.filter("companyId", obj.getCompanyId()).filter("contractId", obj.getContractCount())
												.filter("warehouseCode", obj.getRefNo2()).filter("compartment", obj.getCompartment())
												.filter("shade", obj.getShade()).filter("deggasingServiceId", obj.getCount()).first().now();
										if(fumigationReport!=null){
											logger.log(Level.SEVERE, "deggasingServiceId Fumigation Report id"+fumigationReport.getCount());
											fumigationReport.setDeggasingServiceId(count);
											ofy().save().entity(fumigationReport);
											logger.log(Level.SEVERE, "fumigationReport updated successfully");
										}
									}
									obj.setRemark(obj.getRemark()+"Old service id : "+obj.getCount());
								}
								obj.setCount(count); 
								finalno++;
								
								/**
								 * @author Anil
								 * @since 18-06-2020
								 * In case of rate contract service we need to update 
								 * updated service in billing document as well
								 */
								if(obj.isRateContractService()){
									BillingDocument billObject=ofy().load().type(BillingDocument.class).filter("companyId", obj.getCompanyId()).filter("count", obj.getBillingCount()).first().now();
									if(billObject!=null){
										billObject.setRateContractServiceId(obj.getCount());
										ofy().save().entity(billObject).now();
									}
								}
							 }
							 
							 ofy().save().entities(toUpdateServiceList).now();
						 }		 
						 
					 }
					 
					 
					 
					 
				 }
				 
				 if(cancelledFutureServiceList.size()!=0){
					 logger.log(Level.SEVERE,"CANCELLED FUTURE SERVICE LIST : "+cancelledFutureServiceList.size());
					 ofy().delete().entities(cancelledFutureServiceList).now(); 
				 }
				 
			 }
			 
			 
		 }else{
			 logger.log(Level.SEVERE,"SER LIST NULL : "); 
		 }
		 
		 
		 
	}

	private List<Service> getDuplicateServices(List<Service> serviceList) {
		// TODO Auto-generated method stub
		ArrayList<Integer> serviceIdList=new ArrayList<Integer>();
		ArrayList<Integer> duplicateServiceIdList=new ArrayList<Integer>();
		ArrayList<Service> duplicateServiceList=new ArrayList<Service>();
		
		for(Service obj:serviceList){
			if(serviceIdList.size()!=0){
				if(serviceIdList.contains(obj.getCount())){
					if(duplicateServiceIdList.size()!=0&&duplicateServiceIdList.contains(obj.getCount())){
						
					}else{
						duplicateServiceIdList.add(obj.getCount());
					}
				}else{
					serviceIdList.add(obj.getCount());
				}
			}else{
				serviceIdList.add(obj.getCount());
			}
		}
		
		logger.log(Level.SEVERE , "UNIQUE DUPLICATE SERVICE LIST SIZE "+duplicateServiceIdList.size() );
		
		if(duplicateServiceIdList.size()!=0){
			for(Integer serviceId:duplicateServiceIdList){
				for(Service obj:serviceList){
					if(serviceId==obj.getCount()){
						duplicateServiceList.add(obj);
					}
				}
			}
		}
		logger.log(Level.SEVERE , "DUPLICATE SERVICE LIST SIZE "+duplicateServiceIdList.size() );
		return duplicateServiceList;
	}

	private void updateInvoices(Long companyId,String fromDate,String toDate) {
		SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
		
		
		
		logger.log(Level.SEVERE , "Inside Invoices " );
		List<String> statusList= new ArrayList<String>();
		    statusList.add("Approved");
		    statusList.add("Created");
		    statusList.add("Rejected");
		    statusList.add("Requested");
		    
     List<Invoice> invoiceList = null;
	try {
		invoiceList = ofy().load().type(Invoice.class).filter("companyId", companyId)
				 .filter("status IN", statusList).filter("invoiceDate >=", format.parse(fromDate)).filter("invoiceDate <=", format.parse(toDate)).list();
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     
     HashSet<Integer> custHs=new HashSet<Integer>();
       for(Invoice invoice:invoiceList){
    	   custHs.add(invoice.getPersonInfo().getCount());
       }
     
     
     ArrayList<Integer> custList=new ArrayList<Integer>(custHs);
		
		List<Customer>customerList=ofy().load().type(Customer.class).filter("companyId", companyId)
				.filter("count IN", custList).list();
		
		for(Customer cust:customerList){
			for(Invoice invoice:invoiceList){
				if(cust.getCount()==invoice.getPersonInfo().getCount()){
					invoice.getPersonInfo().setEmail(cust.getEmail());
				}
				
			}
		}
		ofy().save().entities(invoiceList).now();
		
		
		logger.log(Level.SEVERE , "Update Invoices Successfully" );
	}

	/**
	 * Updated By: Viraj
	 * Date: 23-01-2019
	 * Description: To cancel all the documents related to purchase order
	 */
	private void poCancel(Long companyId, int contractCount, int documentId,
			String remark, String loginUser, String docType, int listSize) {
		logger.log(Level.SEVERE , "cancel list :" + cancelDocumentList.size());
		logger.log(Level.SEVERE , "cancel list from service:" + (listSize));		
		
		saveDataOnPOCancel(companyId, contractCount, documentId,
				remark, loginUser, docType, listSize);	
	}
	
	private void saveDataOnPOCancel(Long companyId, int contractCount,
			int documentId, String remark, String loginUser, String docType,
			int listSize) {
		CancellationSummary table1;
		Approvals approve;
		switch (docType) {
		
		case AppConstants.PURCHASEORDER : 
			PurchaseOrder po = ofy().load().type(PurchaseOrder.class)
					.filter("count", contractCount)
					.filter("companyId", companyId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(po.getCount());
			table1.setDocType(AppConstants.PURCHASEORDER);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(po.getStatus());
			table1.setNewStatus(PurchaseOrder.CANCELLED);
			table1.setSrNo(9);
			cancelDocumentList.add(table1);

				po.setStatus(PurchaseOrder.CANCELLED);
				if (po.getDescription() != null) {
					po.setDescription(po.getDescription() + "\n"
							+ "Purchase Order Id =" + contractCount + " "
							+ "Purchase Order Status = Approved" + "\n"
							+ "has been cancelled by " + loginUser
							+ " with remark" + "\n" + "Remark =" + remark);
				} else {
					po.setDescription("Purchase Order Id =" + contractCount + " "
							+ "Purchase Order Status = Approved" + "\n"
							+ "has been cancelled by " + loginUser
							+ " with remark" + "\n" + "Remark =" + remark);
				}

				ofy().save().entity(po).now();
				approve = ofy().load().type(Approvals.class)
						.filter("companyId", companyId)
						.filter("businessprocessId", po.getCount()).first()
						.now();
				if (approve != null) {
					approve.setStatus(Approvals.CANCELLED);
					approve.setRemark("Order Id =" + contractCount + " "
							+ "has been cancelled by " + loginUser
							+ " with remark " + "\n" + "Remark =" + remark);
					ofy().save().entity(approve).now();
				}
	            break;
	            
		case AppConstants.BILLINGDETAILS:
			BillingDocument billingDoc = ofy().load()
					.type(BillingDocument.class).filter("companyId", companyId)
					.filter("count", documentId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.BILLINGDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(billingDoc.getStatus());
			table1.setNewStatus(BillingDocument.CANCELLED);
			table1.setSrNo(1);

			cancelDocumentList.add(table1);
			if(billingDoc != null){
			billingDoc.setStatus(BillingDocument.CANCELLED);
			billingDoc.setBillstatus(BillingDocument.CANCELLED);
			billingDoc.setRemark(remark);
			billingDoc.setComment("Purchase Order Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(billingDoc);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", billingDoc.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Purchase Order Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
			
		case AppConstants.PROCESSCONFIGINVVENDOR:
			VendorInvoice vendorInvoiceDoc = ofy().load()
					.type(VendorInvoice.class).filter("companyId", companyId)
					.filter("count", documentId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.PROCESSCONFIGINVVENDOR);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(vendorInvoiceDoc.getStatus());
			table1.setNewStatus(BillingDocument.CANCELLED);
			table1.setSrNo(1);

			cancelDocumentList.add(table1);
			if(vendorInvoiceDoc != null){
				vendorInvoiceDoc.setStatus(VendorInvoice.CANCELLED);
				vendorInvoiceDoc.setRemark(remark);
				vendorInvoiceDoc.setComment("Purchase Order Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(vendorInvoiceDoc);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", vendorInvoiceDoc.getCount())
					.filter("businessprocesstype", AppConstants.VENDORINVOICEDETAILS).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Purchase Order Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
			
		case AppConstants.PAYMENTDETAILS:
			CustomerPayment paymentDoc = ofy().load()
					.type(CustomerPayment.class).filter("companyId", companyId)
					.filter("count", documentId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.PAYMENTDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(paymentDoc.getStatus());
			table1.setNewStatus(CustomerPayment.CANCELLED);
			table1.setSrNo(1);

			cancelDocumentList.add(table1);
			if(paymentDoc != null){
				paymentDoc.setStatus(CustomerPayment.CANCELLED);
				paymentDoc.setRemark(remark);
				paymentDoc.setComment("Purchase Order Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(paymentDoc);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", paymentDoc.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Purchase Order Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
			
		/**
		 * Updated By: Viraj
		 * Date: 14-02-2019
		 * Description: To include GRN documents in cancellation List
		 */
		case AppConstants.GRN:
			GRN grnDoc = ofy().load()
			.type(GRN.class).filter("companyId", companyId)
			.filter("count", documentId).first().now();

			String grnStatus = "";
			if(grnDoc != null) {
				grnStatus = grnDoc.getStatus();
			}
			logger.log(Level.SEVERE, "grn status123: "+grnStatus);
			
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.PAYMENTDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(grnDoc.getStatus());
			table1.setNewStatus(GRN.CANCELLED);
			table1.setSrNo(1);
			
			cancelDocumentList.add(table1);
			if(grnDoc != null){
				grnDoc.setStatus(GRN.CANCELLED);
				grnDoc.setRemark(remark);
				grnDoc.setDescription("Purchase Order Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(grnDoc);
				if(grnStatus.equalsIgnoreCase("Approved")) {
					logger.log(Level.SEVERE, "Inside approved status");
					updateStockGrn(grnDoc);
				}
				
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", grnDoc.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Purchase Order Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);
			
				ofy().save().entity(approve).now();
			}
			break;
			
		case AppConstants.PROCESSTYPEACCINTERFACE:
			ArrayList<String> list = new ArrayList<String>();
			list.add("PurchaseOrder");
			list.add("VendorInvoice");
			list.add("Customer Payment");

			AccountingInterface accInterface = ofy().load()
					.type(AccountingInterface.class)
					.filter("companyId", companyId).filter("count", documentId)
					.filter("documentType IN", list).first().now();
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType("Accounting Interface");
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(accInterface.getDocumentStatus());
			table1.setNewStatus(AccountingInterface.CANCELLED);
			table1.setSrNo(8);
			cancelDocumentList.add(table1);
			if(accInterface != null){
				accInterface.setDocumentStatus(AccountingInterface.CANCELLED);
				accInterface.setRemark(remark);
				ofy().save().entity(accInterface);
			}
			break;
		}
		logger.log(Level.SEVERE , "cancel list :" + cancelDocumentList.size());
		logger.log(Level.SEVERE , "cancel list from service:" + (listSize));		
		if (cancelDocumentList.size() == (listSize))
			saveCancellationSummary(companyId, contractCount);
		
	}
	/**
	 * Updated By: Viraj
	 * Date: 14-02-2019 
	 * Description: for stock updation after grn is cancelled.
	 * @param grnDoc
	 */
	private void updateStockGrn(GRN grnDoc) {
		UpdateServiceImpl updateService= new UpdateServiceImpl();
		logger.log(Level.SEVERE, "Inside update stock");
		updateService.cancelDocument(grnDoc);
		
	}

	private void saveCancellationSummary(Long companyId, int contractCount) {

		List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
		int size = 0;
		String docType = "";
		CancelSummary cancelSummary = ofy().load().type(CancelSummary.class)
				.filter("companyId", companyId).filter("docID", contractCount)
				.first().now();
		if (cancelSummary != null) {
			size = cancelSummary.getCancelLis().size();
			cancellistsetting1.addAll(cancelSummary.getCancelLis());
			docType = cancelSummary.getDocType();
		}
		if(docType.equalsIgnoreCase(AppConstants.PURCHASEORDER)){
		for (int i = 0; i < cancelDocumentList.size(); i++) {
			CancellationSummary sum = new CancellationSummary();
			sum.setSrNo(size + 1);
			sum.setDocID(cancelDocumentList.get(i).getDocID());
			sum.setDocType(cancelDocumentList.get(i).getDocType());
			sum.setLoggedInUser(cancelDocumentList.get(i).getLoggedInUser());
			sum.setOldStatus(cancelDocumentList.get(i).getOldStatus());
			sum.setNewStatus(cancelDocumentList.get(i).getNewStatus());
			sum.setRemark(cancelDocumentList.get(i).getRemark());
			size++;
			cancellistsetting1.add(sum);
		}
		cancelDocumentList.clear();
		}
		if (cancelSummary == null) {
			CancelSummary cancelSummaryNew = new CancelSummary();
			cancelSummaryNew.setCancelLis(cancellistsetting1);
			cancelSummaryNew.setDocType(AppConstants.PURCHASEORDER);
			cancelSummaryNew.setDocID(contractCount);
			cancelSummaryNew.setCompanyId(companyId);
			ofy().save().entities(cancelSummaryNew);
		} else {
			cancelSummary.setCancelLis(cancellistsetting1);
			ofy().save().entities(cancelSummary);
		}
	
		
	}

	/** Ends **/
		//komal for hvac
		private double calculateTotalAmount(ArrayList<SalesOrderProductLineItem> list){
			double d = 0.0;
			for(SalesOrderProductLineItem item : list){
				d = d + item.getTotalAmount();
			}
			return d;
		}
		 /** date 07/1/2018 added by komal  for hvac **/
		public int checkTaxPercent(double taxValue,String taxName ,List<ProductOtherCharges> taxesList)
		{
			//List<ProductOtherCharges> taxesList=this.prodTaxTable.getDataprovider().getList();
			System.out.println("rohan taxesList size"+taxesList.size());
			for(int i=0;i<taxesList.size();i++)
			{
				double listval=taxesList.get(i).getChargePercent();
				int taxval=(int)listval;
				
				if(taxName.equals("Service")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
						return i;
					}
				}
				if(taxName.equals("VAT")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
						return i;
					}
				}
				
				
				if(taxName.equals("CGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
						return i;
					}
				}
				
				if(taxName.equals("SGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
						return i;
					}
				}
				
				if(taxName.equals("IGST")){
					if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
					if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
						return i;
					}
				}
				
			}
			return -1;
		}
		//komal for hvac
		public List<ProductOtherCharges> addProdTaxes(ArrayList<SalesOrderProductLineItem> salesLineItemLis) throws Exception
		{
			//List<SalesLineItem> salesLineItemLis=this.saleslineitemtable.getDataprovider().getList();
			List<ProductOtherCharges> taxList= new ArrayList<ProductOtherCharges>();
			
			for(int i=0;i<salesLineItemLis.size();i++)
			{
				double priceqty=0,taxPrice=0;
				if((salesLineItemLis.get(i).getDiscountAmt()==0) ){
					
					System.out.println("inside both 0 condition");
					
				//	taxPrice=this.getSaleslineitemtable().removeAllTaxes(salesLineItemLis.get(i).getPrduct());
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
					if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){

						priceqty = salesLineItemLis.get(i).getTotalAmount();
						}else{

							priceqty = salesLineItemLis.get(i).getTotalAmount();
						}
				}
				
				else if((salesLineItemLis.get(i).getDiscountAmt()!=0)){
					
					System.out.println("inside both not null condition");
		
					/******************** Square Area Calculation code added in if condition and without square area calculation code in else block ***************************/
					if(!salesLineItemLis.get(i).getArea().equalsIgnoreCase("NA")){
		
						priceqty = salesLineItemLis.get(i).getTotalAmount();
						
					}else{
						priceqty = salesLineItemLis.get(i).getTotalAmount();
					}
				}
				if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
					System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
					{
						System.out.println("Inside vat GST");
						ProductOtherCharges pocentity=new ProductOtherCharges(); 
						System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
						pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),salesLineItemLis.get(i).getVatTax().getTaxPrintName(),taxList);
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//							pocentity.setAssessableAmount(priceqty);
							taxList.remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(priceqty);
						}
						taxList.add(pocentity);
					}
				}
				
				if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
					
					System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
					{
						System.out.println("Inside service GST");
						ProductOtherCharges pocentity=new ProductOtherCharges();
					 
						System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
						pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
						pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
						pocentity.setIndexCheck(i+1);
						int indexValue=checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),salesLineItemLis.get(i).getServiceTax().getTaxPrintName(),taxList);
						
						if(indexValue!=-1){
							System.out.println("Index As Not -1");
							pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//							pocentity.setAssessableAmount(priceqty);
							taxList.remove(indexValue);
						}
						if(indexValue==-1){
							pocentity.setAssessableAmount(priceqty);
						}
						taxList.add(pocentity);
					}
				}
			}
			return taxList;
		}
		 /** date 18/1/2018 added by komal  for hvac **/
		private String createDeliveryNote(JSONArray jsonarr,Long companyId,String approverName ){
			Gson gson = new GsonBuilder().create();	
			ArrayList<MaterialMovementNote> selectedList = new ArrayList<MaterialMovementNote>();
			for(int i=0;i<jsonarr.length();i++){
				JSONObject jsonObj;
				try {
					jsonObj = jsonarr.getJSONObject(i);
					selectedList.add(gson.fromJson(jsonObj.toString(), MaterialMovementNote.class));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			GeneralServiceImpl gsync = new GeneralServiceImpl();
			Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("companyName", selectedList.get(0).getBranch().toUpperCase()).first().now();
			ArrayList<MaterialProduct> materialProductList = new ArrayList<MaterialProduct>();
			ArrayList<DeliveryLineItems> DeliveryNoteList = new ArrayList<DeliveryLineItems>();
			for(MaterialMovementNote m  : selectedList){
				materialProductList.addAll(m.getSubProductTableMmn());
			}
			HashMap<String ,DeliveryLineItems> map = new HashMap<String ,DeliveryLineItems>();
			for(MaterialProduct m  : materialProductList){
				DeliveryLineItems dNote = new DeliveryLineItems();
				if(map.containsKey(m.getMaterialProductCode())){
					dNote = map.get(m.getMaterialProductCode());
					dNote.setQuantity(dNote.getQuantity()+1);
					logger.log(Level.SEVERE, "DNote Quantity :" + dNote.getQuantity());
				}else{
					dNote.setProdCode(m.getMaterialProductCode());
					dNote.setProdCategory(m.getMaterialProductCategory());
					dNote.setProdName(m.getMaterialProductName());
					dNote.setProdId(m.getMaterialProductId());
					dNote.setQuantity(m.getMaterialProductRequiredQuantity());
					dNote.setWareHouseName(m.getMaterialProductWarehouse());
					dNote.setStorageBinName(m.getMaterialProductStorageBin());
					dNote.setStorageLocName(m.getMaterialProductStorageLocation());
					logger.log(Level.SEVERE, "DNote Quantity :" + dNote.getQuantity());
				}	
				map.put(m.getMaterialProductCode(), dNote);
			}	
			Set<Map.Entry<String, DeliveryLineItems>> entrySet = map.entrySet();
			for (Entry entry : entrySet) {
				DeliveryNoteList.add((DeliveryLineItems)entry.getValue());
			}
			DeliveryNote deliveryNote =  new DeliveryNote();
			deliveryNote.setCompanyId(selectedList.get(0).getCompanyId());
			deliveryNote.setSalesOrderCount(1);
			deliveryNote.setDeliveryItems(DeliveryNoteList);
			deliveryNote.setStatus(DeliveryNote.CREATED);
			deliveryNote.setBranch(selectedList.get(0).getBranch());
			deliveryNote.setDeliveryItems(DeliveryNoteList);
			deliveryNote.setApproverName(approverName);
			deliveryNote.setDeliveryDate(new Date());
			deliveryNote.setCreatedBy(approverName);
			deliveryNote.setEmployee(approverName);
			deliveryNote.setShippingAddress(customer.getAdress());
			deliveryNote.getCinfo().setCount(customer.getCount());
			deliveryNote.getCinfo().setFullName(customer.getCompanyName());
			
			
			deliveryNote.getCinfo().setPocName(customer.getFullname());
			deliveryNote.getCinfo().setCellNumber(customer.getCellNumber1());
			deliveryNote.setCreationDate(new  Date());
			deliveryNote.setLeadCount(-1);
			deliveryNote.setQuotationCount(-1);
//			deliveryNote.setCategory("a");
//			deliveryNote.setType("a");
			deliveryNote.setTotalAmount(0.0d);
			deliveryNote.setNetpayable(0.0d);
			deliveryNote.setAmountInclTax(0.0d);
			//deliveryNote.set
			String count = gsync.saveAndApproveDocument(deliveryNote, "" ,"", AppConstants.DELIVERYNOTE);
			return count;
			
		}

	private void contractCancel(Long companyId, int contractCount,
			int documentId, String remark, String loginUser, String docType,
			int listSize) {
		//logger.log(Level.SEVERE , "cancel list :" + cancelDocumentList.size());
		//logger.log(Level.SEVERE , "cancel list from service:" + (listSize));		
		
		saveDataOnContractCancel(companyId, contractCount, documentId,
				remark, loginUser, docType, listSize);		
}
	List<CancellationSummary> cancelDocumentList = new ArrayList<CancellationSummary>();
	private void saveDataOnContractCancel(Long companyId, int contractCount,
			int documentId, String remark, String loginUser, String docType,
			int listSize) {
		try {
			
		CancellationSummary table1;
		Approvals approve;
		switch (docType) {
		case AppConstants.CONTRACT : 
			Contract con = ofy().load().type(Contract.class)
					.filter("count", contractCount)
					.filter("companyId", companyId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(con.getCount());
			table1.setDocType(AppConstants.APPROVALCONTRACT);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(con.getStatus());
			table1.setNewStatus(Contract.CANCELLED);
			table1.setSrNo(9);
			cancelDocumentList.add(table1);

				con.setStatus(Contract.CANCELLED);
				if (con.getDescription() != null) {
					con.setDescription(con.getDescription() + "\n"
							+ "Contract Id =" + contractCount + " "
							+ "Contract Status = Approved" + "\n"
							+ "has been cancelled by " + loginUser
							+ " with remark" + "\n" + "Remark =" + remark);
				} else {
					con.setDescription("Contract Id =" + contractCount + " "
							+ "Contract Status = Approved" + "\n"
							+ "has been cancelled by " + loginUser
							+ " with remark" + "\n" + "Remark =" + remark);
				}
				
				/***** Date 01-04-2019 by Vijay for NBHC CCPM contract cancellation remark must download seperatly ***/
				if(remark!=null)
				con.setRemark(remark);
				/**** Date 03-04-2019 by Vijay for cancellation Date must be stored ****/
				con.setCancellationDate(new Date());

				ofy().save().entity(con).now();
				approve = ofy().load().type(Approvals.class)
						.filter("companyId", companyId)
						.filter("businessprocessId", con.getCount()).first()
						.now();
				if (approve != null) {
					approve.setStatus(Approvals.CANCELLED);
					approve.setRemark("Order Id =" + contractCount + " "
							+ "has been cancelled by " + loginUser
							+ " with remark " + "\n" + "Remark =" + remark);
					ofy().save().entity(approve).now();
				}

	            break;
		case AppConstants.BILLINGDETAILS:
			BillingDocument billingDoc = ofy().load()
					.type(BillingDocument.class).filter("companyId", companyId)
					.filter("count", documentId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(billingDoc.getStatus());
			table1.setNewStatus(BillingDocument.CANCELLED);
			table1.setSrNo(1);

			cancelDocumentList.add(table1);
			if(billingDoc != null){
			billingDoc.setStatus(BillingDocument.CANCELLED);
			billingDoc.setBillstatus(BillingDocument.CANCELLED);
			billingDoc.setRemark(remark);
			billingDoc.setComment("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(billingDoc);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", billingDoc.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Contract Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;

		case AppConstants.PROCESSCONFIGINV:
			Invoice invoice = ofy().load().type(Invoice.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.INVOICEDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(invoice.getStatus());
			table1.setNewStatus(Invoice.CANCELLED);
			table1.setSrNo(2);

			cancelDocumentList.add(table1);
			if(invoice != null){
			invoice.setStatus(Invoice.CANCELLED);
			invoice.setRemark(remark);
			invoice.setComment("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			/**** Date 03-04-2019 by Vijay for cancellation Date must be stored ****/
			invoice.setCancellationDate(new Date());
			ofy().save().entity(invoice);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", invoice.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Contract Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.PAYMENT:
			CustomerPayment payment = ofy().load().type(CustomerPayment.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.PAYMENT);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(payment.getStatus());
			table1.setNewStatus(CustomerPayment.CANCELLED);
			table1.setSrNo(3);

			cancelDocumentList.add(table1);
			if(payment != null){
			payment.setStatus(CustomerPayment.CANCELLED);
			payment.setRemark(remark);
			payment.setComment("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(payment);
			}
			break;
		case AppConstants.SERVICETYPECUST:
			Service service = ofy().load().type(Service.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();
			if(service.getServiceDate().after(new Date())){
				logger.log(Level.SEVERE , "service delete :" +documentId);
				ofy().delete().entity(service);
			}else{
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType("Service");
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(service.getStatus());
			table1.setNewStatus(Service.CANCELLED);
			table1.setSrNo(4);

			cancelDocumentList.add(table1);
			if(service != null){
			service.setStatus(Service.CANCELLED);
			logger.log(Level.SEVERE , "service cancel :" +documentId);
			service.setRemark(remark);
			service.setComment("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			
			/**** Date 03-04-2019 by Vijay for cancellation Date must be stored ****/
			service.setCancellationDate(new Date());
			ofy().save().entity(service);
			}
			}
			break;

		case "Project":
			ServiceProject project = ofy().load().type(ServiceProject.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType("Service Project");
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(project.getProjectStatus());
			table1.setNewStatus(ServiceProject.CANCELLED);
			table1.setSrNo(5);

			cancelDocumentList.add(table1);
			if(project != null){
			project.setProjectStatus(ServiceProject.CANCELLED);
			project.setServiceStatus(ServiceProject.CANCELLED);
			project.setComment("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(project);
			}
			break;
		case AppConstants.MIN:
			MaterialIssueNote min = ofy().load().type(MaterialIssueNote.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.MIN);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(min.getStatus());
			table1.setNewStatus(MaterialIssueNote.CANCELLED);
			table1.setSrNo(6);

			cancelDocumentList.add(table1);				
			min.setStatus(MaterialIssueNote.CANCELLED);
			min.setRemark(remark);
			min.setMinDescription("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(min);

			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", min.getCount()).first().now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Contract Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.MMN:
			MaterialMovementNote mmn = ofy().load()
					.type(MaterialMovementNote.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();
			
			MaterialIssueNote min1 = ofy().load().type(MaterialIssueNote.class)
					.filter("companyId", companyId).filter("count", mmn.getMmnMinId())
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.MMN);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(mmn.getStatus());
			table1.setNewStatus(MaterialMovementNote.CANCELLED);
			table1.setSrNo(7);
			cancelDocumentList.add(table1);
			
			mmn.setStatus(MaterialMovementNote.CANCELLED);
			mmn.setRemark(remark);
			mmn.setMmnDescription("Contract Id =" + contractCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(mmn);
			
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", mmn.getCount()).first().now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Contract Id =" + contractCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.PROCESSTYPEACCINTERFACE:
			ArrayList<String> list = new ArrayList<String>();
			list.add("Contract");
			list.add("Invoice");
			list.add("Customer Payment");
			list.add(AppConstants.MIN);
			list.add(AppConstants.MMN);
			AccountingInterface accInterface = ofy().load()
					.type(AccountingInterface.class)
					.filter("companyId", companyId).filter("count", documentId)
					.filter("documentType IN", list).first().now();
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType("Accounting Interface");
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(accInterface.getDocumentStatus());
			table1.setNewStatus(AccountingInterface.CANCELLED);
			table1.setSrNo(8);
			cancelDocumentList.add(table1);
			if(accInterface != null){
			accInterface.setDocumentStatus(AccountingInterface.CANCELLED);
			accInterface.setRemark(remark);
			ofy().save().entity(accInterface);
			}break;
		}
//		logger.log(Level.SEVERE , "cancel list :" + cancelDocumentList.size());
//		logger.log(Level.SEVERE , "cancel list from service:" + (listSize));		
		if (cancelDocumentList.size() == (listSize))
			saveSummary(companyId, contractCount);
		
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void saveSummary(Long companyId, int contractCount) {
		List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
		int size = 0;
		String docType = "";
		CancelSummary cancelSummary = ofy().load().type(CancelSummary.class)
				.filter("companyId", companyId).filter("docID", contractCount)
				.first().now();
		if (cancelSummary != null) {
			size = cancelSummary.getCancelLis().size();
			cancellistsetting1.addAll(cancelSummary.getCancelLis());
			docType = cancelSummary.getDocType();
		}
		if(docType.equalsIgnoreCase(AppConstants.CONTRACT)){
		for (int i = 0; i < cancelDocumentList.size(); i++) {
			CancellationSummary sum = new CancellationSummary();
			sum.setSrNo(size + 1);
			sum.setDocID(cancelDocumentList.get(i).getDocID());
			sum.setDocType(cancelDocumentList.get(i).getDocType());
			sum.setLoggedInUser(cancelDocumentList.get(i).getLoggedInUser());
			sum.setOldStatus(cancelDocumentList.get(i).getOldStatus());
			sum.setNewStatus(cancelDocumentList.get(i).getNewStatus());
			sum.setRemark(cancelDocumentList.get(i).getRemark());
			size++;
			cancellistsetting1.add(sum);
		}
		cancelDocumentList.clear();
		}
		if (cancelSummary == null) {
			CancelSummary cancelSummaryNew = new CancelSummary();
			cancelSummaryNew.setCancelLis(cancellistsetting1);
			cancelSummaryNew.setDocType(AppConstants.APPROVALCONTRACT);
			cancelSummaryNew.setDocID(contractCount);
			cancelSummaryNew.setCompanyId(companyId);
			ofy().save().entities(cancelSummaryNew);
		} else {
			cancelSummary.setCancelLis(cancellistsetting1);
			ofy().save().entities(cancelSummary);
		}
	}
	
	 /** date 24/11/2017 added by komal for mmn transaction **/
    private void setProductInventory(ArrayList<InventoryTransactionLineItem> itemList){
		for(InventoryTransactionLineItem item:itemList){
			ProductInventoryTransaction prodInvTran = getProductInventoryTransaction(item);			
			GenricServiceImpl impl=new GenricServiceImpl();
			ReturnFromServer server =	impl.save(prodInvTran);
		}
    }
    /** date 24/11/2017 added by komal for mmn transaction **/
    public ProductInventoryTransaction getProductInventoryTransaction(InventoryTransactionLineItem item){
		ProductInventoryTransaction prodInvTran=new ProductInventoryTransaction();
		
		prodInvTran.setCount(item.getCount());
		prodInvTran.setProductId(item.getProdId());
		prodInvTran.setProductName(item.getProductName());
		prodInvTran.setProductPrice(item.getProductPrice());
		prodInvTran.setProductQty(item.getUpdateQty());
		prodInvTran.setProductUOM(item.getProductUOM());
		prodInvTran.setWarehouseName(item.getWarehouseName());
		prodInvTran.setStorageLocation(item.getStorageLocation());
		prodInvTran.setStorageBin(item.getStorageBin());
		prodInvTran.setDocumentType(item.getDocumentType());
		prodInvTran.setDocumentId(item.getDocumentId());
		prodInvTran.setDocumentTitle(item.getDocumnetTitle());
		prodInvTran.setDocumentDate(item.getDocumentDate());
		prodInvTran.setOperation(item.getOperation());
		prodInvTran.setOpeningStock(item.getOpeningStock());
		prodInvTran.setClosingStock(item.getClosingStock());
		prodInvTran.setCompanyId(item.getCompanyId());
		prodInvTran.setBranch(item.getBranch());
		prodInvTran.setInventoryTransactionDateTime(new Date());
		if(item.getServiceId()!=null&&!item.getServiceId().equals(""))
			prodInvTran.setServiceId(item.getServiceId());
		
		/**
		 * Date :01-03-2017 By ANIL
		 */
		if(item.getDocumentSubType()!=null){
			prodInvTran.setDocumentSubType(item.getDocumentSubType());
		}
		
		/**
		 * Date : 02-03-2017 By ANIL
		 */
		
		if(item.getProductCode()!=null){
			prodInvTran.setProductCode(item.getProductCode());
		}
		
		return prodInvTran;
		
	}

	private List<Service> updateServiceList(Contract contract, List<Service> list) {
		List<Service> globalServiceList=new ArrayList<Service>();
		for(SalesLineItem item:contract.getItems()){
			ArrayList<Service> serviceList=new ArrayList<Service>();
			for(int i=0;i<list.size();i++){
				if(item.getPrduct().getCount()==list.get(i).getProduct().getCount()){
					serviceList.add(list.get(i));
				}
			}
			
			Comparator<Service> comparator = new Comparator<Service>() {  
				@Override  
				public int compare(Service o1, Service o2) {  
					return o1.getServiceDate().compareTo(o2.getServiceDate());  
				}  
			};  
			Collections.sort(serviceList,comparator);
			
			int noOfdays=item.getDuration();
			int interval= (int) (noOfdays/item.getNumberOfServices());
			Date date=contract.getContractDate();
			Date newDate=DateUtility.getDateWithTimeZone("IST", date);
			for(int i=0;i<serviceList.size();i++){
				if(i==0){
					serviceList.get(i).setServiceDate(contract.getStartDate());
				}else{
					Calendar sDate = Calendar.getInstance();
					sDate.setTime(newDate);
					sDate.add(Calendar.DATE, interval);
					newDate=sDate.getTime();
					if(contract.getEndDate().before(newDate)==false){
						serviceList.get(i).setServiceDate(newDate);
					}else{
						serviceList.get(i).setServiceDate(contract.getEndDate());
					}
				}
			}
			
			globalServiceList.addAll(serviceList);
			
			
		}
		return globalServiceList;
	}



	private ArrayList<ServiceSchedule> getUpdatedScheduleList(Contract contract, SalesLineItem item) {
		ArrayList<ServiceSchedule> scheduleList=new ArrayList<ServiceSchedule>();
		for(ServiceSchedule scheduleSer:contract.getServiceScheduleList()){
			if(item.getPrduct().getCount()==scheduleSer.getScheduleProdId()){
				scheduleList.add(scheduleSer);
			}
		}
		
		Comparator<ServiceSchedule> comparator = new Comparator<ServiceSchedule>() {  
			@Override  
			public int compare(ServiceSchedule o1, ServiceSchedule o2) {  
				return o1.getScheduleServiceDate().compareTo(o2.getScheduleServiceDate());  
			}  
		};  
		Collections.sort(scheduleList,comparator);
		
		int noOfdays=item.getDuration();
		int interval= (int) (noOfdays/item.getNumberOfServices());
		Date date=contract.getContractDate();
		Date newDate=DateUtility.getDateWithTimeZone("IST", date);
		for(int i=0;i<scheduleList.size();i++){
			if(i==0){
				scheduleList.get(i).setScheduleServiceDate(contract.getStartDate());
			}else{
				Calendar sDate = Calendar.getInstance();
				sDate.setTime(newDate);
				sDate.add(Calendar.DATE, interval);
				newDate=sDate.getTime();
				
				if(contract.getEndDate().before(newDate)==false){
					scheduleList.get(i).setScheduleServiceDate(newDate);
				}else{
					scheduleList.get(i).setScheduleServiceDate(contract.getEndDate());
				}
			}
		}
		
		return scheduleList;
	}



	private Date getEndDateFromMaxDuration(Contract contract) {
		int maxDuration=0;
		for(SalesLineItem item:contract.getItems()){
			if(maxDuration!=0){
				if(maxDuration<item.getDuration()){
					maxDuration=item.getDuration();
				}
			}else{
				maxDuration=item.getDuration();
			}
		}
		Date date=contract.getContractDate();
		Date newDate=DateUtility.getDateWithTimeZone("IST", date);
		Calendar sDate = Calendar.getInstance();
		sDate.setTime(newDate);
		sDate.add(Calendar.DATE, maxDuration);
		return sDate.getTime();
	}



	private boolean isContractDateIsLessThanStartDate(Contract contract) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date contractDate=contract.getContractDate();
		try {
			contractDate=DateUtility.getDateWithTimeZone("IST", sdf.parse(sdf.format(contractDate)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Date contractStartDate=contract.getStartDate();
		try {
			contractStartDate=DateUtility.getDateWithTimeZone("IST", sdf.parse(sdf.format(contractStartDate)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(contractDate.before(contractStartDate)){
			return true;
		}
		
		return false;
	}
	
	
	
	/**
	 *  Developed by : Rohan Bhagde
	 *  Cancel Cotract Logic
	 *  Date : 28/2/2017
	 */
	
//		int accContract=0,accInvoice=0,accPayment=0; 
		List<CancellationSummary> cancellis = new ArrayList<CancellationSummary>();
		private void reactOnCancelContract(Long companyId,int contractCount,String remark , String loginUser)
		{
			/**
			 * Check for accouting intrface process config 
			 */
//			ProcessConfiguration processConfig;
//			processConfig= ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).first().now();
//			
//			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
//				
//				if(processConfig.getProcessList().get(i).getProcessName().equalsIgnoreCase("Contract") && processConfig.getProcessList().get(i).getProcessType().equalsIgnoreCase("Accounting Interface"))
//				{
//					accContract++;
//				}
//				else if(processConfig.getProcessList().get(i).getProcessName().equalsIgnoreCase("Invoice") && processConfig.getProcessList().get(i).getProcessType().equalsIgnoreCase("Accounting Interface"))
//				{
//					accInvoice++;
//				}
//				else if(processConfig.getProcessList().get(i).getProcessName().equalsIgnoreCase("CustomerPayment") && processConfig.getProcessList().get(i).getProcessType().equalsIgnoreCase("Accounting Interface"))
//				{
//					accPayment++;
//				}
//			}
			
			int invoice = 0,approved=0;
			List<BillingDocument> billingList = new ArrayList<BillingDocument>();
			List<Invoice> invoiceList = new ArrayList<Invoice>();
			List<CustomerPayment> customerpaymentList = new ArrayList<CustomerPayment>();
			
			
			billingList= ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("contractCount", contractCount).filter("typeOfOrder", AppConstants.ORDERTYPEFORSERVICE).list();
			
			if(billingList.size()==0)
			{
				cancelContract(companyId, contractCount, remark,loginUser);
			}
			else
			{
				for (BillingDocument billing : billingList) {
					
					if(billing.getStatus().equals("Invoiced"))
					{
						invoice=invoice+1;
					}
				}

				
				if(invoice > 0)
				{
					invoiceList = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", contractCount).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).list();
				
				
					for (Invoice invoicelis : invoiceList) {
						
						if(invoicelis.getStatus().equals("Approved"))
						{
							approved++;
						}
					}
					
					
					if(approved > 0)
					{
						customerpaymentList = ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("contractCount", contractCount).filter("typeOfOrder", AppConstants.ORDERTYPESERVICE).list();
					
						
						cancelBillingDocuments(companyId, contractCount, remark,billingList,loginUser);
						cancelInvoiceDocumnets(companyId, contractCount, remark,invoiceList,loginUser);
						cancelPaymentDocuments(companyId, contractCount, remark,customerpaymentList,loginUser);
						cancelServiceRelatedToContract(companyId, contractCount, remark,loginUser);
						cancelServiceProjectsRelatedToContract(companyId, contractCount, remark,loginUser);
						cancelMINRelatedToContract(companyId, contractCount, remark,loginUser);
//						cancelMMNRelatedToContract(companyId, contractCount, remark,loginUser);
						cancelContract(companyId, contractCount, remark,loginUser);
					}
					else
					{
						cancelBillingDocuments(companyId, contractCount, remark,billingList,loginUser);
						cancelInvoiceDocumnets(companyId, contractCount, remark,invoiceList,loginUser);
						cancelServiceRelatedToContract(companyId, contractCount, remark,loginUser);
						cancelServiceProjectsRelatedToContract(companyId, contractCount, remark,loginUser);
						cancelMINRelatedToContract(companyId, contractCount, remark,loginUser);
//						cancelMMNRelatedToContract(companyId, contractCount, remark,loginUser);
						cancelContract(companyId, contractCount, remark,loginUser);
					}
				}
				else
				{
					cancelBillingDocuments(companyId, contractCount, remark,billingList,loginUser);
					cancelServiceRelatedToContract(companyId, contractCount, remark,loginUser);
					cancelServiceProjectsRelatedToContract(companyId, contractCount, remark,loginUser);
					cancelMINRelatedToContract(companyId, contractCount, remark,loginUser);
//					cancelMMNRelatedToContract(companyId, contractCount, remark,loginUser);
					
					cancelContract(companyId, contractCount, remark,loginUser);
				}
			}
		}



		private void cancelContract(Long companyId, int contractCount, String remark, String loginUser) {
		
			Contract con = ofy().load().type(Contract.class).filter("count", contractCount).filter("companyId", companyId).first().now();
			
			con.setStatus(Contract.CANCELLED);
			con.setDescription(con.getDescription() + "\n" + "Contract Id ="
					+ contractCount + " "
					+ "Contract Status = Approved"
					+ "\n"
					+ "has been cancelled by " + loginUser
					+ " with remark" + "\n" + "Remark ="
					+ remark);
			
			ofy().save().entity(con).now();
			
			List<AccountingInterface> acntsList = new ArrayList<AccountingInterface>();
			
			acntsList = ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("documentID", contractCount).filter("documentType", "Contract").list();
					
			for (int i = 0; i < acntsList.size(); i++) {
				
				if(acntsList.get(i).getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){

					acntsList.get(i).setStatus(AccountingInterface.DELETEFROMTALLY);
					acntsList.get(i).setRemark(acntsList.get(i).getRemark()+"\n"+"Contract Id =" + contractCount
							+ " " + "Contract Status = Approved"
							+ "\n" + "has been cancelled by "
							+ ""
							+ " with remark " + "\n" + "Remark ="
							+ remark);
					}
					else{
						acntsList.get(i).setStatus(AccountingInterface.CANCELLED);
						acntsList.get(i).setRemark(acntsList.get(i).getRemark()+"\n"+"Contract Id =" + contractCount
							+ " " + "Contract Status = Approved"
							+ "\n" + "has been cancelled by "
							+ ""
							+ " with remark " + "\n" + "Remark ="
							+ remark);
					}
			}
			
				ofy().save().entities(acntsList);
			
			
			CancellationSummary table7 = new CancellationSummary();

			table7.setDocID(con.getCount());
			table7.setDocType(AppConstants.APPROVALCONTRACT);
			table7.setRemark(remark);
			table7.setLoggedInUser(loginUser);
			table7.setOldStatus(Contract.APPROVED);
			table7.setNewStatus(con.getStatus());
			table7.setSrNo(7);

			cancellis.add(table7);
			
			
			
			
			List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
			for (int i = 0; i < cancellis.size(); i++) {
				CancellationSummary sum = new CancellationSummary();
				sum.setSrNo(i + 1);
				sum.setDocID(cancellis.get(i).getDocID());
				sum.setDocType(cancellis.get(i).getDocType());
				sum.setLoggedInUser(cancellis.get(i).getLoggedInUser());
				sum.setOldStatus(cancellis.get(i).getOldStatus());
				sum.setNewStatus(cancellis.get(i).getNewStatus());
				sum.setRemark(cancellis.get(i).getRemark());

				cancellistsetting1.add(sum);
			}

			
			CancelSummary cancelSummary = new CancelSummary();
			cancelSummary.setCancelLis(cancellistsetting1);
			cancelSummary.setDocType(AppConstants.APPROVALCONTRACT);
			cancelSummary.setDocID(contractCount);
			cancelSummary.setCompanyId(companyId);
			
			
			ofy().save().entities(cancelSummary);
			
		}



		private void cancelPaymentDocuments(Long companyId, int contractCount,String remark, List<CustomerPayment> customerpaymentList, String loginUser) {
		
			logger.log(Level.SEVERE, "inside Cancel Payment list size "	+ customerpaymentList.size());
			
			for (int i = 0; i < customerpaymentList.size(); i++) {
				
				
				if (customerpaymentList.get(i).getStatus().equals(CustomerPayment.PAYMENTCLOSED)) {
					CancellationSummary table3 = new CancellationSummary();

					table3.setDocID(customerpaymentList.get(i).getCount());
					table3.setDocType("Payment Details");
					table3.setRemark(remark);
					table3.setLoggedInUser(loginUser);
					table3.setOldStatus(customerpaymentList.get(i).getStatus());
					table3.setNewStatus(CustomerPayment.PAYMENTCLOSED);
					table3.setSrNo(3);

					cancellis.add(table3);
				
				} else {
					CancellationSummary table3 = new CancellationSummary();

					table3.setDocID(customerpaymentList.get(i).getCount());
					table3.setDocType("Payment Details");
					table3.setRemark(remark);
					table3.setLoggedInUser(loginUser);
					table3.setOldStatus(customerpaymentList.get(i).getStatus());
					table3.setNewStatus(Invoice.CANCELLED);
					table3.setSrNo(3);

					cancellis.add(table3);
				}
			
				if (customerpaymentList.get(i).getStatus().equals(CustomerPayment.CREATED)) {
					customerpaymentList.get(i).setStatus(CustomerPayment.CANCELLED);
					customerpaymentList.get(i).setComment("Contract Id =" + contractCount
														+ " " + "Contract Status = Approved"
														+ "\n" + "has been cancelled by "
														+ loginUser
														+ " with remark " + "\n" + "Remark ="
														+ remark);
				}
				else if (customerpaymentList.get(i).getStatus().equals(CustomerPayment.PAYMENTCLOSED)) {
					customerpaymentList.get(i).setStatus(CustomerPayment.PAYMENTCLOSED);
					customerpaymentList.get(i).setRenewFlag(true);
					customerpaymentList.get(i).setComment("Contract Id =" + contractCount
														+ " " + "Contract Status = Approved"
														+ "\n" + "has been cancelled by "
														+ loginUser
														+ " with remark " + "\n" + "Remark ="
														+ remark);
					
					
					
						List<AccountingInterface> acntsList = new ArrayList<AccountingInterface>();
						acntsList = ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("documentID", customerpaymentList.get(i).getCount()).filter("documentType", "Customer Payment").list();
						
						
						
						for (int k = 0; k < acntsList.size(); k++) {
							
							if(acntsList.get(k).getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
	
								acntsList.get(k).setStatus(AccountingInterface.DELETEFROMTALLY);
								acntsList.get(k).setRemark(acntsList.get(k).getRemark()+"\n"+"Contract Id =" + contractCount
										+ " " + "Contract Status = Approved"
										+ "\n" + "has been cancelled by "
										+ ""
										+ " with remark " + "\n" + "Remark ="
										+ remark);
								}
								else{
									acntsList.get(k).setStatus(AccountingInterface.CANCELLED);
									acntsList.get(k).setRemark(acntsList.get(k).getRemark()+"\n"+"Contract Id =" + contractCount
										+ " " + "Contract Status = Approved"
										+ "\n" + "has been cancelled by "
										+ ""
										+ " with remark " + "\n" + "Remark ="
										+ remark);
								}
						}
				}
			}
			
			ofy().save().entities(customerpaymentList);
		}



		private void cancelInvoiceDocumnets(Long companyId, int contractCount,String remark, List<Invoice> invoiceList, String loginUser) {
		
			logger.log(Level.SEVERE, "inside Cancel invoiceList list size "	+ invoiceList.size());
			
			for (int i = 0; i < invoiceList.size(); i++) {
				
				
				if (Invoice.APPROVED.equals(invoiceList.get(i).getStatus().trim())
						|| (BillingDocument.BILLINGINVOICED.equals(invoiceList.get(i).getStatus().trim()))) {
					CancellationSummary table2 = new CancellationSummary();

					table2.setDocID(invoiceList.get(i).getCount());
					table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
					table2.setRemark(remark);
					table2.setLoggedInUser(loginUser);

					if (Invoice.APPROVED.equals(invoiceList.get(i).getStatus().trim())) {

						table2.setOldStatus(invoiceList.get(i).getStatus());
						table2.setNewStatus(Invoice.APPROVED);
					} else {
						table2.setOldStatus(invoiceList.get(i).getStatus());
						table2.setNewStatus(BillingDocument.BILLINGINVOICED);
					}

					table2.setSrNo(2);

					cancellis.add(table2);
			
				} else {
					CancellationSummary table2 = new CancellationSummary();

					table2.setDocID(invoiceList.get(i).getCount());
					table2.setDocType(AppConstants.APPROVALINVOICEDETAILS);
					table2.setRemark(remark);
					table2.setLoggedInUser(loginUser);
					table2.setOldStatus(invoiceList.get(i).getStatus());
					table2.setNewStatus(Invoice.CANCELLED);
					table2.setSrNo(2);

					cancellis.add(table2);
				}
				
				
				
				if (invoiceList.get(i).getStatus().equals(Invoice.APPROVED)) {
					invoiceList.get(i).setRenewFlag(true);
					invoiceList.get(i).setStatus(Invoice.APPROVED);
					invoiceList.get(i).setComment("Contract Id =" + contractCount
							+ " " + "Contract Status = Approved"
							+ "\n" + "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
					
						List<AccountingInterface> acntsList = new ArrayList<AccountingInterface>();
						acntsList = ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("documentID", invoiceList.get(i).getCount()).filter("documentType", "Invoice").list();
							
							for (int k = 0; k < acntsList.size(); k++) {
								
								if(acntsList.get(k).getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
		
									acntsList.get(k).setStatus(AccountingInterface.DELETEFROMTALLY);
									acntsList.get(k).setRemark(acntsList.get(k).getRemark()+"\n"+"Contract Id =" + contractCount
											+ " " + "Contract Status = Approved"
											+ "\n" + "has been cancelled by "
											+ ""
											+ " with remark " + "\n" + "Remark ="
											+ remark);
									}
									else{
										acntsList.get(k).setStatus(AccountingInterface.CANCELLED);
										acntsList.get(k).setRemark(acntsList.get(k).getRemark()+"\n"+"Contract Id =" + contractCount
											+ " " + "Contract Status = Approved"
											+ "\n" + "has been cancelled by "
											+ ""
											+ " with remark " + "\n" + "Remark ="
											+ remark);
									}
							}
							
							ofy().save().entities(acntsList);
							
				}
				else if (invoiceList.get(i).getStatus().equals(Invoice.CREATED) 
						|| invoiceList.get(i).getStatus().equals(Invoice.REJECTED)
						|| invoiceList.get(i).getStatus().equals(Invoice.PROFORMAINVOICE)
						|| invoiceList.get(i).getStatus().equals(BillingDocument.BILLINGINVOICED)) {
					invoiceList.get(i).setStatus(Invoice.CANCELLED);
					invoiceList.get(i).setComment("Contract Id =" + contractCount
							+ " " + "Contract Status = Approved"
							+ "\n" + "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
				}
				else if(invoiceList.get(i).getStatus().equals(Invoice.REQUESTED))
				{
					invoiceList.get(i).setStatus(Invoice.CANCELLED);
					invoiceList.get(i).setComment("Contract Id =" + contractCount
							+ " " + "Contract Status = Approved"
							+ "\n" + "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
					
				Approvals approve =	ofy().load().type(Approvals.class).filter("companyId", companyId).filter("businessprocessId", invoiceList.get(i).getCount()).first().now();
				
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Contract Id ="
						+ contractCount
						+ " "
						+ "Contract status = Approved"
						+ "\n"
						+ "has been cancelled by "
						+ loginUser
						+ " with remark "
						+ "\n"
						+ "Remark ="
						+ remark);
				
				ofy().save().entity(approve).now();
				}

			}
			
			ofy().save().entities(invoiceList).now();
			
		}



		private void cancelBillingDocuments(Long companyId, int contractCount,String remark, List<BillingDocument> billingList, String loginUser) {
		
			logger.log(Level.SEVERE, "inside Cancel billingList list size "	+ billingList.size());
			
			for (int i = 0; i < billingList.size(); i++) {
				
				logger.log(Level.SEVERE, "inside Cancel Payment list size "	+ billingList.size());
				
				if (billingList.get(i).getStatus().trim().equals(BillingDocument.BILLINGINVOICED)) {

					CancellationSummary table1 = new CancellationSummary();
					table1.setDocID(billingList.get(i).getCount());
					table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
					table1.setRemark(remark);
					table1.setLoggedInUser(loginUser);
					table1.setOldStatus(billingList.get(i).getStatus());
					table1.setNewStatus(BillingDocument.BILLINGINVOICED);
					table1.setSrNo(1);

					cancellis.add(table1);

				} else {
					CancellationSummary table6 = new CancellationSummary();

					table6.setDocID(billingList.get(i).getCount());
					table6.setDocType(AppConstants.APPROVALBILLINGDETAILS);
					table6.setRemark(remark);
					table6.setLoggedInUser(loginUser);
					table6.setOldStatus(billingList.get(i).getStatus());
					table6.setNewStatus(Service.CANCELLED);
					table6.setSrNo(1);

					cancellis.add(table6);
				}
				
				if (billingList.get(i).getStatus().equals(BillingDocument.CREATED)
						||billingList.get(i).getStatus().equals(BillingDocument.APPROVED)
						||billingList.get(i).getStatus().equals(BillingDocument.REJECTED)) {

					billingList.get(i).setStatus(BillingDocument.CANCELLED);
					billingList.get(i).setComment("Contract Id =" + contractCount + " "
							+ "Contract status = Approved"
							+ "\n" + "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
				}
				else if (billingList.get(i).getStatus().equals(BillingDocument.BILLINGINVOICED)){
					billingList.get(i).setRenewFlag(true);
					billingList.get(i).setStatus(BillingDocument.BILLINGINVOICED);
					billingList.get(i).setComment("Contract Id =" + contractCount + " "
							+ "Contract status = Approved"
							+ "\n" + "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
				}
				else if (billingList.get(i).getStatus().equals(BillingDocument.REQUESTED)) {

					billingList.get(i).setStatus(BillingDocument.CANCELLED);
					billingList.get(i).setComment("Contract Id =" + contractCount + " "
							+ "Contract status = Approved"
							+ "\n" + "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
					
					Approvals approve =	ofy().load().type(Approvals.class).filter("companyId", companyId).filter("businessprocessId", billingList.get(i).getCount()).first().now();
					
					approve.setStatus(Approvals.CANCELLED);
					approve.setRemark("Contract Id ="
							+ contractCount
							+ " "
							+ "Contract status = Approved"
							+ "\n"
							+ "has been cancelled by "
							+ loginUser
							+ " with remark "
							+ "\n"
							+ "Remark ="
							+ remark);
					
					ofy().save().entity(approve).now();

				}
			}
			
			
			ofy().save().entities(billingList).now();
		}



		private void cancelServiceRelatedToContract(Long companyId,int contractCount,String remark, String loginUser) {

			List<Service> serviceList = new ArrayList<Service>();
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractCount).list();
	
			logger.log(Level.SEVERE, "inside Cancel serviceList list size "	+ serviceList.size());
		
			for (int i = 0; i < serviceList.size(); i++) {
				
				if (serviceList.get(i).getStatus().trim().equals(Service.SERVICESTATUSCOMPLETED)) {
					CancellationSummary table4 = new CancellationSummary();

					table4.setDocID(serviceList.get(i).getCount());
					table4.setDocType("Service Details");
					table4.setRemark(remark);
					table4.setLoggedInUser(loginUser);
					table4.setOldStatus(serviceList.get(i).getStatus());
					table4.setNewStatus(Service.SERVICESTATUSCOMPLETED);
					table4.setSrNo(4);
					
					cancellis.add(table4);

				} else {
					CancellationSummary table4 = new CancellationSummary();

					table4.setDocID(serviceList.get(i).getCount());
					table4.setDocType("Service Details");
					table4.setRemark(remark);
					table4.setLoggedInUser(loginUser);
					table4.setOldStatus(serviceList.get(i).getStatus());
					table4.setNewStatus(Service.CANCELLED);
					table4.setSrNo(4);
					
					
					cancellis.add(table4);

				}
				
				if(serviceList.get(i).getStatus().equals(Service.SERVICESTATUSSCHEDULE) 
						|| serviceList.get(i).getStatus().equals(Service.SERVICESTATUSRESCHEDULE)
						||serviceList.get(i).getStatus().equals(Service.SERVICESTATUSPENDING))
				{
					serviceList.get(i).setStatus(Service.CANCELLED);
					serviceList.get(i).setRemark("Contract Id ="+ contractCount
							+ " "
							+ "Contract Status = Approved"
							+ "\n"
							+ "has been cancelled by "
							+ loginUser
							+ " with remark."
							+ "\n"
							+ "Remark ="
							+ remark);
				}
			} 
			
			
			ofy().save().entities(serviceList);
		
		} 
	
		private void cancelServiceProjectsRelatedToContract(Long companyId,int contractCount,String remark, String loginUser) {

			List<ServiceProject> projectList = new ArrayList<ServiceProject>();
			projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("contractCount", contractCount).list();
	
			for (int i = 0; i < projectList.size(); i++) {
				
			if (projectList.get(i).getProjectStatus().trim().equals(ServiceProject.COMPLETED)) {

				CancellationSummary table5 = new CancellationSummary();

				table5.setDocID(projectList.get(i).getCount());
				table5.setDocType("ServiceProject");
				table5.setRemark(remark);
				table5.setLoggedInUser(loginUser);
				table5.setOldStatus(projectList.get(i).getProjectStatus());
				table5.setNewStatus(ServiceProject.COMPLETED);
				table5.setSrNo(5);

				cancellis.add(table5);

			} else {
				CancellationSummary table5 = new CancellationSummary();

				table5.setDocID(projectList.get(i).getCount());
				table5.setDocType("ServiceProject");
				table5.setRemark(remark);
				table5.setLoggedInUser(loginUser);
				table5.setOldStatus(projectList.get(i).getProjectStatus());
				table5.setNewStatus(Service.CANCELLED);
				table5.setSrNo(5);

				cancellis.add(table5);
			}

			if (projectList.get(i).getProjectStatus().equals(ServiceProject.CREATED)
					||projectList.get(i).getProjectStatus().equals(ServiceProject.SCHEDULED)) {

				projectList.get(i).setProjectStatus(ServiceProject.CANCELLED);
				projectList.get(i).setComment("Contract Id =" + contractCount + " "
						+ "Contract Status = Approved"+ "\n" + "has been cancelled by "
						+ loginUser
						+ " with remark." + "\n" + "Remark ="
						+ remark);
			}
		}
			
		ofy().save().entities(projectList);	
	}
		
		
	private void cancelMINRelatedToContract(Long companyId,int contractCount,String remark, String loginUser) {	
		
		List<MaterialIssueNote> minList = new ArrayList<MaterialIssueNote>();
		
		minList = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("minSoId", contractCount).list();
		
		logger.log(Level.SEVERE, "inside Cancel minList list size "	+ minList.size());
		
		for (int i = 0; i < minList.size(); i++) {
			
			List<MaterialMovementNote> mmnList = new ArrayList<MaterialMovementNote>();
			mmnList = ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).filter("orderID", contractCount).list();
			
			if(minList.get(i).getStatus().equals(MaterialIssueNote.APPROVED))
			{
				CancellationSummary table6 = new CancellationSummary();

				table6.setDocID(minList.get(i).getCount());
				table6.setDocType("MIN");
				table6.setRemark(remark);
				table6.setLoggedInUser(loginUser);
				table6.setOldStatus(minList.get(i).getStatus());
				if(mmnList.size()==0){
					table6.setNewStatus(MaterialIssueNote.CANCELLED);
				}
				else{
					table6.setNewStatus(MaterialIssueNote.APPROVED);
				}
				table6.setSrNo(6);

				cancellis.add(table6);
			}
			else{
			CancellationSummary table6 = new CancellationSummary();

			table6.setDocID(minList.get(i).getCount());
			table6.setDocType("MIN");
			table6.setRemark(remark);
			table6.setLoggedInUser(loginUser);
			table6.setOldStatus(minList.get(i).getStatus());
			table6.setNewStatus(MaterialIssueNote.CANCELLED);
			table6.setSrNo(6);

			cancellis.add(table6);
			}
			
			if (minList.get(i).getStatus().equals(MaterialIssueNote.CREATED)
					||minList.get(i).getStatus().equals(MaterialIssueNote.REJECTED)){
				minList.get(i).setStatus(Service.CANCELLED);
				minList.get(i).setMinDescription("Contract Id ="
						+ contractCount + " " + "Contract Status = Approved"	+ "\n"
						+ "has been cancelled by "
						+ loginUser
						+ " with remark." + "\n" + "Remark ="
						+ remark);
				
			}
			else if (minList.get(i).getStatus().equals(MaterialIssueNote.REQUESTED)){
				minList.get(i).setStatus(Service.CANCELLED);
				minList.get(i).setMinDescription("Contract Id ="
						+ contractCount + " " + "Contract Status = Approved"	+ "\n"
						+ "has been cancelled by "
						+ loginUser
						+ " with remark." + "\n" + "Remark ="
						+ remark);
				
				Approvals approve =	ofy().load().type(Approvals.class).filter("companyId", companyId).filter("businessprocessId", minList.get(i).getCount()).first().now();
				
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Contract Id ="
						+ contractCount
						+ " "
						+ "Contract status = Approved"
						+ "\n"
						+ "has been cancelled by "
						+ loginUser
						+ " with remark "
						+ "\n"
						+ "Remark ="
						+ remark);
				
				ofy().save().entity(approve).now();
			}
			else  if (minList.get(i).getStatus().equals(MaterialIssueNote.APPROVED)){
				
				
				
				if(mmnList.size()==0)
				{
					minList.get(i).setStatus(Service.CANCELLED);
					minList.get(i).setMinDescription("Contract Id ="
							+ contractCount + " " + "Contract Status = Approved"
							+ "\n"
							+ "has been cancelled by "
							+ loginUser
							+ " with remark " + "\n" + "Remark ="
							+ remark);
					
					
				//  stock reverse logic
					
					ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
					for(MaterialProduct product:minList.get(i).getSubProductTablemin()){
						InventoryTransactionLineItem item=new InventoryTransactionLineItem();
						item.setCompanyId(minList.get(i).getCompanyId());
						item.setBranch(minList.get(i).getBranch());
						item.setDocumentType(AppConstants.MIN);
						item.setDocumentId(minList.get(i).getCount());
						item.setDocumentDate(minList.get(i).getCreationDate());
						item.setDocumnetTitle(minList.get(i).getMinTitle()+" (Cancelled MIN)");

						if(minList.get(i).getServiceId()>0)
							item.setServiceId(minList.get(i).getServiceId()+"");
						item.setProdId(product.getMaterialProductId());
						item.setUpdateQty(product.getMaterialProductRequiredQuantity());
						item.setOperation(AppConstants.ADD);
						item.setWarehouseName(product.getMaterialProductWarehouse());
						item.setStorageLocation(product.getMaterialProductStorageLocation());
						item.setStorageBin(product.getMaterialProductStorageBin());
						itemList.add(item);
					}
					UpdateStock.setProductInventory(itemList);
				}
				else
				{
					minList.get(i).setStatus(Service.APPROVED);
					minList.get(i).setMinDescription("As there are MMN against this MIN please cancel MIN manually..!");
				}
				
			}
		}
		
		ofy().save().entities(minList);
		
	}
	
	private void salesOrderCancel(Long companyId, int salesOrderCount,
			int documentId, String remark, String loginUser, String docType,
			int listSize) {
			saveDataOnSalesOrderCancel(companyId, salesOrderCount, documentId,
					remark, loginUser, docType, listSize);
			logger.log(Level.SEVERE , "cancel list :" + cancelDocumentList1.size());
			logger.log(Level.SEVERE , "cancel list from service:" + listSize);		
//			if (cancelDocumentList1.size() == listSize)
//				saveSummaryForSalesOrder(companyId, salesOrderCount);
	}
	
	List<CancellationSummary> cancelDocumentList1 = new ArrayList<CancellationSummary>();
	private void saveDataOnSalesOrderCancel(Long companyId, int salesOrderCount,
			int documentId, String remark, String loginUser, String docType,
			int listSize) {
		CancellationSummary table1;
		Approvals approve;
		switch (docType) {
		case AppConstants.BILLINGDETAILS:
			BillingDocument billingDoc = ofy().load()
					.type(BillingDocument.class).filter("companyId", companyId)
					.filter("count", documentId).first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.APPROVALBILLINGDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(billingDoc.getStatus());
			table1.setNewStatus(BillingDocument.CANCELLED);
			table1.setSrNo(1);

			cancelDocumentList1.add(table1);
			if(billingDoc != null){
			billingDoc.setStatus(BillingDocument.CANCELLED);
			billingDoc.setBillstatus(BillingDocument.CANCELLED);
			billingDoc.setRemark(remark);
			billingDoc.setComment("Billing Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(billingDoc);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", billingDoc.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Billing Id =" + salesOrderCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;

		case AppConstants.PROCESSCONFIGINV:
			Invoice invoice = ofy().load().type(Invoice.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.INVOICEDETAILS);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(invoice.getStatus());
			table1.setNewStatus(Invoice.CANCELLED);
			table1.setSrNo(2);

			cancelDocumentList1.add(table1);
			if(invoice != null){
			invoice.setStatus(Invoice.CANCELLED);
			invoice.setRemark(remark);
			invoice.setComment("Invoice Id =" + documentId  + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(invoice);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", invoice.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Invoice Id =" + documentId + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.PAYMENT:
			CustomerPayment payment = ofy().load().type(CustomerPayment.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.PAYMENT);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(payment.getStatus());
			table1.setNewStatus(CustomerPayment.CANCELLED);
			table1.setSrNo(3);

			cancelDocumentList1.add(table1);
			if(payment != null){
			payment.setStatus(CustomerPayment.CANCELLED);
			payment.setRemark(remark);
			payment.setComment("Payment Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(payment);
			}
			break;
		case AppConstants.WORK_ORDER:
				WorkOrder workOrder = ofy().load().type(WorkOrder.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.WORK_ORDER);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(workOrder.getStatus());
			table1.setNewStatus(SalesOrder.CANCELLED);
			table1.setSrNo(4);

			cancelDocumentList1.add(table1);
			if(workOrder != null){
				workOrder.setStatus(SalesOrder.CANCELLED);
			
				workOrder.setRemark(remark);
				workOrder.setRemark("Work Order Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(workOrder);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", workOrder.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Work Order Id =" + documentId + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;

		case AppConstants.SALESORDER:
			SalesOrder salesOrder = ofy().load().type(SalesOrder.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.SALESORDER);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(salesOrder.getStatus());
			table1.setNewStatus(SalesOrder.CANCELLED);
			table1.setSrNo(5);

			cancelDocumentList1.add(table1);
			if(salesOrder != null){
				salesOrder.setStatus(SalesOrder.CANCELLED);
				salesOrder.setDescription("Sales Order Id =" + salesOrderCount + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(salesOrder);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", salesOrder.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Sales Order Id =" + salesOrderCount + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);
				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.DELIVERYNOTE:
			DeliveryNote deliveryNote = ofy().load().type(DeliveryNote.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.DELIVERYNOTE);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(deliveryNote.getStatus());
			table1.setNewStatus(DeliveryNote.CANCELLED);
			table1.setSrNo(5);

			cancelDocumentList1.add(table1);
			if(deliveryNote != null){
				deliveryNote.setStatus(DeliveryNote.CANCELLED);
				deliveryNote.setDescription("Delivery Note Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(deliveryNote);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", deliveryNote.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("Delivery Note Id =" + documentId +  " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);
				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.MRN:
			MaterialRequestNote mrn = ofy().load().type(MaterialRequestNote.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.MRN);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(mrn.getStatus());
			table1.setNewStatus(MaterialRequestNote.CANCELLED);
			table1.setSrNo(5);

			cancelDocumentList1.add(table1);
			if(mrn != null){
				mrn.setStatus(SalesOrder.CANCELLED);
				mrn.setMrnDescription("MRN Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
				ofy().save().entity(mrn);
			}
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", mrn.getCount()).first()
					.now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("MRN Id =" + documentId + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);
				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.MIN:
			MaterialIssueNote min = ofy().load().type(MaterialIssueNote.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.MIN);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(min.getStatus());
			table1.setNewStatus(MaterialIssueNote.CANCELLED);
			table1.setSrNo(6);

			cancelDocumentList1.add(table1);				
			min.setStatus(MaterialIssueNote.CANCELLED);
			min.setRemark(remark);
			min.setMinDescription("MIN Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(min);

			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", min.getCount()).first().now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("MIN Id =" + documentId + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.MMN:
			MaterialMovementNote mmn = ofy().load()
					.type(MaterialMovementNote.class)
					.filter("companyId", companyId).filter("count", documentId)
					.first().now();
			
			MaterialIssueNote min1 = ofy().load().type(MaterialIssueNote.class)
					.filter("companyId", companyId).filter("count", mmn.getMmnMinId())
					.first().now();

			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType(AppConstants.MMN);
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(mmn.getStatus());
			table1.setNewStatus(MaterialMovementNote.CANCELLED);
			table1.setSrNo(7);
			cancelDocumentList1.add(table1);
			
			mmn.setStatus(MaterialMovementNote.CANCELLED);
			mmn.setRemark(remark);
			mmn.setMmnDescription("MMN Id =" + documentId + " "
					+ "has been cancelled by " + loginUser + " with remark "
					+ "\n" + "Remark =" + remark);
			ofy().save().entity(mmn);
			
			approve = ofy().load().type(Approvals.class)
					.filter("companyId", companyId)
					.filter("businessprocessId", mmn.getCount()).first().now();
			if (approve != null) {
				approve.setStatus(Approvals.CANCELLED);
				approve.setRemark("MMN Id =" + documentId + " "
						+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);

				ofy().save().entity(approve).now();
			}
			break;
		case AppConstants.PROCESSTYPEACCINTERFACE:
			ArrayList<String> list = new ArrayList<String>();
			list.add(AppConstants.SALESORDER);
			list.add("Invoice");
			list.add("Customer Payment");
			list.add(AppConstants.MRN);
			list.add(AppConstants.MIN);
			list.add(AppConstants.MMN);
			AccountingInterface accInterface = ofy().load()
					.type(AccountingInterface.class)
					.filter("companyId", companyId).filter("count", documentId)
					.filter("documentType IN", list).first().now();
			table1 = new CancellationSummary();
			table1.setDocID(documentId);
			table1.setDocType("Accounting Interface");
			table1.setRemark(remark);
			table1.setLoggedInUser(loginUser);
			table1.setOldStatus(accInterface.getDocumentStatus());
			table1.setNewStatus(AccountingInterface.CANCELLED);
			table1.setSrNo(8);
			cancelDocumentList1.add(table1);
			if(accInterface != null){
			accInterface.setDocumentStatus(AccountingInterface.CANCELLED);
			accInterface.setRemark(remark);
			ofy().save().entity(accInterface);
			}break;
		}
		logger.log(Level.SEVERE , "cancel list :" + cancelDocumentList1.size());
		logger.log(Level.SEVERE , "cancel list from service:" + listSize);		
		if (cancelDocumentList1.size() == listSize)
			saveSummaryForSalesOrder(companyId, salesOrderCount);
	}

	private void saveSummaryForSalesOrder(Long companyId, int salesOrderCount) {
		List<CancellationSummary> cancellistsetting1 = new ArrayList<CancellationSummary>();
		int size = 0;
		CancelSummary cancelSummary = ofy().load().type(CancelSummary.class)
				.filter("companyId", companyId).filter("docID", salesOrderCount)
				.first().now();
		if (cancelSummary != null) {
			size = cancelSummary.getCancelLis().size();
			cancellistsetting1.addAll(cancelSummary.getCancelLis());
		}
		for (int i = 0; i < cancelDocumentList1.size(); i++) {
			CancellationSummary sum = new CancellationSummary();
			sum.setSrNo(size + 1);
			sum.setDocID(cancelDocumentList1.get(i).getDocID());
			sum.setDocType(cancelDocumentList1.get(i).getDocType());
			sum.setLoggedInUser(cancelDocumentList1.get(i).getLoggedInUser());
			sum.setOldStatus(cancelDocumentList1.get(i).getOldStatus());
			sum.setNewStatus(cancelDocumentList1.get(i).getNewStatus());
			sum.setRemark(cancelDocumentList1.get(i).getRemark());
			size++;
			cancellistsetting1.add(sum);
		}
		cancelDocumentList1.clear();
		if (cancelSummary == null) {
			CancelSummary cancelSummaryNew = new CancelSummary();
			cancelSummaryNew.setCancelLis(cancellistsetting1);
			cancelSummaryNew.setDocType(AppConstants.SALESORDER);
			cancelSummaryNew.setDocID(salesOrderCount);
			cancelSummaryNew.setCompanyId(companyId);
			ofy().save().entities(cancelSummaryNew);
		} else {
			cancelSummary.setCancelLis(cancellistsetting1);
			ofy().save().entities(cancelSummary);
		}
	}
		private void updateProductCode(Long companyId) {
		
//		List<ProcessConfiguration> processList = ofy().load()
//				.type(ProcessConfiguration.class)
//				.filter("companyId", companyId)
//				.filter("processName", "ProductGroupAsAsset").list();
//		
//		ArrayList<String> productCatList=new ArrayList<String>();
//		for (int k = 0; k < processList.size(); k++) {
//			if (processList.get(k).getProcessName().trim().equalsIgnoreCase("ProductGroupAsAsset")) {
//				for (int i = 0; i < processList.get(k).getProcessList().size(); i++) {
//					if (processList.get(k).getProcessList().get(i).isStatus() == true) {
//						productCatList.add(processList.get(k).getProcessList().get(i).getProcessType());
//					}
//				}
//			}
//		}
		
//		if(productCatList.size()!=0){
//			List<ItemProduct> itemProductList=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("productCategory IN", productCatList).list();
//			if(itemProductList!=null&&itemProductList.size()!=0){
//				for(ItemProduct product:itemProductList){
//					product.setProductGroup("Asset");
//				}
//				ofy().save().entities(itemProductList).now();
//			}
//			return;
//		}
		
		
		
		logger.log(Level.SEVERE , "INSIDE UPDATE PRODUCT CODE-------------------------" );
		// TODO Auto-generated method stub
		List<ItemProduct> itemProductList=ofy().load().type(ItemProduct.class).filter("companyId", companyId).list();
//		List<ProductInventoryView> pivList=ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).list();
		List<ProductInventoryViewDetails> pivDetailsList=ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).list();
		
		logger.log(Level.SEVERE , "PRODUCT SIZE : " + itemProductList.size());
//		logger.log(Level.SEVERE , "PIV SIZE : " + pivList.size());
		logger.log(Level.SEVERE , "PIV DETAILS SIZE : " + pivDetailsList.size());
		
		
//		if(pivList!=null&&pivList.size()!=0){
//			for(ProductInventoryView piv:pivList){
//				ItemProduct itemProduct=getItemProductFromList(itemProductList,piv.getProdID());
//				if(itemProduct!=null){
//					piv.setProductCode(itemProduct.getProductCode());
//					piv.setProductName(itemProduct.getProductName());
//					for(ProductInventoryViewDetails details:piv.getDetails()){
//						details.setProdcode(itemProduct.getProductCode());
//						details.setProdname(itemProduct.getProductName());
//					}
//					/**
//					 * @author Anil , Date : 22-06-2019
//					 * Updating UOM from master to PIV
//					 */
//					piv.setProductUOM(itemProduct.getUnitOfMeasurement());
//				}
//			}
//			ofy().save().entities(pivList).now();
//		}
		
		if(pivDetailsList!=null&&pivDetailsList.size()!=0){
			for(ProductInventoryViewDetails details:pivDetailsList){
				ItemProduct itemProduct=getItemProductFromList(itemProductList,details.getProdid());
				if(itemProduct!=null){
				details.setProdcode(itemProduct.getProductCode());
				details.setProdname(itemProduct.getProductName());
				details.setProductCategory(itemProduct.getProductCategory());
				}
			}
			ofy().save().entities(pivDetailsList).now();
		}
		
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		logger.log(Level.SEVERE , "PRODUCT CODE UPDATED SUCCESFULLY....!");
	}

	private ItemProduct getItemProductFromList(List<ItemProduct> itemProductList, int prodID) {
		// TODO Auto-generated method stub
		for(ItemProduct product:itemProductList){
			if(product.getCount()==prodID){
				return product;
			}
		}
		return null;
	}
	private void cancelOpenStatusDocuments(long companyId,int contractCount,String remark,String loginUser, String status){
		Contract con = ofy().load().type(Contract.class).filter("count", contractCount).filter("companyId", companyId).first().now();

		UpdateServiceImpl impl = new UpdateServiceImpl();
		ArrayList<CancelContract> openDocList = new ArrayList<CancelContract>();
		if(status.equalsIgnoreCase(Contract.CONTRACTDISCOUNTINED)){
			
		}else{
		CancelContract canContract = new CancelContract();
 		canContract.setDocumnetId(con.getCount());
 		canContract.setBranch(con.getBranch());
 		canContract.setBpName(con.getCinfo().getFullName());
 		canContract.setDocumentStatus(con.getStatus());
 		canContract.setDocumnetName(AppConstants.CONTRACT);
 		canContract.setRecordSelect(true);
 		openDocList.add(canContract);
		}
		try {
			/**
			 * @author Anil @since 20-07-2021
			 * Issue raised by Ashwini when cancelling document directly then approved closed or completed records also get completed
			 * changed here contract flag to true
			 **/
 		ArrayList<CancelContract> list = impl.getRecordsForContractCancel(companyId, contractCount, true,false,"cancelOpenStatusDocuments");
 		openDocList.addAll(list);
 		logger.log(Level.SEVERE , "Open Document List : " + openDocList.size());
 		List<Integer> idList =new ArrayList<Integer>();
 		for(CancelContract cancelContract : openDocList){
 			if(cancelContract.getDocumnetName().equalsIgnoreCase(AppConstants.SERVICETYPECUST)){
 				idList.add(cancelContract.getDocumnetId());
 			}else{
 				saveDataOnContractCancel(companyId, contractCount, cancelContract.getDocumnetId(),
 						remark, loginUser, cancelContract.getDocumnetName(), openDocList.size());
 			}
	 	
 		}
 		logger.log(Level.SEVERE , "Open Document List 1 : " );
		} catch (Exception e) {
			e.printStackTrace();
		}
 		logger.log(Level.SEVERE , "Open Document List 2 : " );

 		/***
 		 * old logic for deleting and cancelling services
 		 */
 		
// 		ArrayList<Service> serList = new ArrayList<Service>();
// 		List<Service> serviceList = ofy().load().type(Service.class)
//				.filter("companyId", companyId).filter("count IN", idList).filter("contractCount", contractCount).list();
// 		int i = 1;
//	 		for(Service service : serviceList){
//			if(service.getServiceDate().after(new Date())){
//					logger.log(Level.SEVERE , "service delete :" +service.getCount() +"  "+ i);
//					ofy().delete().entity(service);
//				}else{
//				service.setStatus(Service.CANCELLED);
//				logger.log(Level.SEVERE , "service cancel :" +service.getCount() +"  "+i);
//				service.setRemark(remark);
//				service.setComment("Contract Id =" + contractCount + " "
//						+ "has been cancelled by " + loginUser + " with remark "
//						+ "\n" + "Remark =" + remark);
//				service.setCancellationDate(new Date());
//				serList.add(service);
//				
//				}
//			i++;
//	 		}
//	 		
//	 		if(serList != null && serList.size() > 0){
//	 			logger.log(Level.SEVERE , "service size :" + "  "+ i +"  "+ serList.size());
//	 			ofy().save().entities(serList);
//	 		}
	 		
	 		
	 		cancelOrDeleteServices(contractCount,companyId,remark,loginUser);
	 		
	 		
 		logger.log(Level.SEVERE , "ContractCancelled....!");		
	}

	private void cancelOrDeleteServices(int contractCount,long companyId,String remark,String loginUser) {
		logger.log(Level.SEVERE , "cancelOrDeleteServices....!");
		ArrayList<String> statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		
		/**
		 * @author Anil @since 23-07-2021
		 * As per the instruction of Ashwini and Nitin Sir 
		 * Need to close services those are started, reported, tcompleted and completed by technician
		 */
		statusList.add(Service.SERVICESTATUSSTARTED);
		statusList.add(Service.SERVICESTATUSREPORTED);
		statusList.add(Service.SERVICESTATUSTECHCOMPLETED);
		statusList.add(Service.SERVICETCOMPLETED);
		
		
		ArrayList<Service> serList = new ArrayList<Service>();
		ArrayList<Service> serDelList = new ArrayList<Service>();
		List<Service> serviceList = ofy().load().type(Service.class)
				.filter("companyId", companyId).filter("status IN", statusList)
				.filter("contractCount", contractCount).limit(1000).list();
		int i = 1;
		for (Service service : serviceList) {
			if (service.getServiceDate().after(new Date())) {
				serDelList.add(service);
			} else {
				service.setStatus(Service.CANCELLED);
//				logger.log(Level.SEVERE,"service cancel :" + service.getCount() + "  " + i);
				service.setRemark(remark);
				service.setComment("Contract Id =" + contractCount + " "+ "has been cancelled by " + loginUser
						+ " with remark " + "\n" + "Remark =" + remark);
				service.setCancellationDate(new Date());
				serList.add(service);
			}
			i++;
		}
		logger.log(Level.SEVERE , "cancelOrDeleteServices....!!");
		try{
			if (serDelList != null && serDelList.size() > 0) {
				logger.log(Level.SEVERE, "service Delete size :" + "  " + i + "  "+ serDelList.size());
				ofy().delete().entities(serDelList);
			}
		}catch(Exception e){
			
		}
		try{
			if (serList != null && serList.size() > 0) {
				logger.log(Level.SEVERE, "service size :" + "  " + i + "  "+ serList.size());
				ofy().save().entities(serList);
			}
		}catch(Exception e){
			
		}
		
		if(serviceList.size()!=0){
			cancelOrDeleteServices(contractCount, companyId, remark, loginUser);
		}
		
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
	}
	
	public void updateFrequencyInServices(Long companyId,String contractIdInString) {
		// TODO Auto-generated method stub	 
		 SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");		
		 int contractId=0;
	
		 try{
			 contractId=Integer.parseInt(contractIdInString);
		 }catch(Exception e){
			 
		 }
		 logger.log(Level.SEVERE,"In updateFrequencyInServices taskqueue: companyId="+companyId+" contract Id = "+contractId);
			
		 if(contractId!=0) {
			 Contract con=ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", contractId).first().now();
			if(con!=null) {
				List<SalesLineItem> items= con.getItems();
				for(SalesLineItem item:items) {
					String frequency="";
					if(item.getProfrequency()!=null&&!item.getProfrequency().equals(""))
						frequency=item.getProfrequency();
					logger.log(Level.SEVERE,"frequency="+frequency);
					
					if(frequency!=null&&!frequency.equals("")) {
						logger.log(Level.SEVERE,"fetching services of product = "+item.getProductName() +" / "+item.getPrduct().getCount());
						
						List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount",contractId).filter("product.count",item.getPrduct().getCount()).list();
						logger.log(Level.SEVERE,"serviceList size="+serviceList.size());
						
						if(serviceList!=null) {
							for(Service s:serviceList) {
								s.setProductFrequency(frequency);
								
							}
							ofy().save().entities(serviceList);
						}
					}
					
				}
			}
		 }
		
		 
	}
	
	public void deleteDuplicateLeaves(Long companyId, String fromDateInString,String toDateInString) {
//		logger.log(Level.SEVERE,"in deleteDuplicateLeaves method");
//		// TODO Auto-generated method stub
//		 SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//		 
//		 Date fromDate=null;
//		 Date toDate=null;
//		 List<EmployeeLeave> leaveList=new ArrayList<EmployeeLeave>();
//		 
//		 if(fromDateInString!=null&&toDateInString!=null&&!fromDateInString.equals("null")&&!toDateInString.equals("null")){
//			 logger.log(Level.SEVERE,"in fromdate todate");
//			 try {
//					fromDate=dateFormat.parse(fromDateInString);
//				 } catch (ParseException e) {
//					e.printStackTrace();
//				 }
//				 try {
//					toDate=dateFormat.parse(toDateInString);
//					 
//					Calendar cal2=Calendar.getInstance();
//					cal2.setTime(toDate);
////					cal2.add(Calendar.DATE, 0);
//					
//					
//					cal2.set(Calendar.HOUR_OF_DAY,23);
//					cal2.set(Calendar.MINUTE,59);
//					cal2.set(Calendar.SECOND,59);
//					cal2.set(Calendar.MILLISECOND,999);
//					toDate=cal2.getTime();
//				 } catch (ParseException e) {
//					e.printStackTrace();
//				 }
//				 
//				 
//				 logger.log(Level.SEVERE,"COM ID : "+companyId+" FRM DATE : "+fromDate+" TO DATE : "+toDate);
//				 
//				 
//				 
//				 leaveList=ofy().load().type(EmployeeLeave.class).filter("companyId", companyId).filter("leaveDate >=", fromDate).filter("leaveDate <=", toDate).list();
//		 }
//		 
//		
//		
//		 
//		 if(leaveList!=null&&leaveList.size()!=0){
//			 List<EmployeeLeave> tobedeletedList=new ArrayList<EmployeeLeave>();
//			 logger.log(Level.SEVERE,"leaveList size: "+leaveList.size());
//			 Map<Integer,List<EmployeeLeave>> empWiseLeavesMap= new HashMap<Integer,List<EmployeeLeave>>();
//			 empWiseLeavesMap= (Map<Integer, List<EmployeeLeave>>) leaveList.stream().collect(Collectors.groupingBy(EmployeeLeave::getEmplId,Collectors.toList()));
//			
//			 if(empWiseLeavesMap!=null&&empWiseLeavesMap.size()>0) {
//				 logger.log(Level.SEVERE,"empWiseLeavesMap size: "+empWiseLeavesMap.size());
//				 
//				 Set s1 = empWiseLeavesMap.entrySet();
//				 Iterator iterator =  s1.iterator();
//				
//				 while(iterator.hasNext()) {
//					 ArrayList<EmployeeLeave> empWiseLeaveList=new ArrayList<EmployeeLeave>();
//					 Map.Entry<Integer,List<EmployeeLeave>> m = (Map.Entry<Integer,List<EmployeeLeave>>) iterator.next();
//					 empWiseLeaveList.addAll(m.getValue());
//					
//					 Map<Date,List<EmployeeLeave>> dateWiseLeavesMap= new HashMap<Date,List<EmployeeLeave>>();					 
//					 
//					 if(empWiseLeaveList!=null&&empWiseLeaveList.size()>0) {
//						 logger.log(Level.SEVERE,"Employee id:"+empWiseLeaveList.get(0).getEmplId() +" empWiseLeaveList size: "+empWiseLeaveList.size());
//							
//						 dateWiseLeavesMap= (Map<Date,List<EmployeeLeave>>) empWiseLeaveList.stream().collect(Collectors.groupingBy(EmployeeLeave::getLeaveDate,Collectors.toList()));
//						if(dateWiseLeavesMap!=null&&dateWiseLeavesMap.size()>0) {
//							logger.log(Level.SEVERE,"dateWiseLeavesMap size: "+dateWiseLeavesMap.size());
//							Set s2 = dateWiseLeavesMap.entrySet();
//							Iterator iterator2 =  s2.iterator();
//							while(iterator2.hasNext()) {
//								ArrayList<EmployeeLeave> dateWiseLeaveList=new ArrayList<EmployeeLeave>();
//								 Map.Entry<Date,List<EmployeeLeave>> m2 = (Map.Entry<Date,List<EmployeeLeave>>) iterator2.next();
//								 dateWiseLeaveList.addAll(m2.getValue());
//								 if(dateWiseLeaveList!=null&&dateWiseLeaveList.size()>1) {
////									 logger.log(Level.SEVERE,"date: "+dateWiseLeaveList.get(0).getLeaveDate()+" dateWiseLeaveList size: "+dateWiseLeaveList.size());
//									 
//									 for(int i=1;i<dateWiseLeaveList.size();i++) {
////										 duplicateLeavesIDList.add(dateWiseLeaveList.get(i).getCount());
//										 tobedeletedList.add(dateWiseLeaveList.get(i));
//									 }
//								 }
//							}
//						}
//						
//					 }
//				 }	
//			
//			 }
//				
//			 logger.log(Level.SEVERE,"tobedeletedList size: "+tobedeletedList.size());
//
//			 if(tobedeletedList.size()>0) {
//				 ofy().delete().entities(tobedeletedList).now(); 
//				 
//				 logger.log(Level.SEVERE,"Duplicate Leaves deleted successfully"); 
//			 }
//			 
//			 
//		 }else{
//			 logger.log(Level.SEVERE,"LEAVE LIST NULL"); 
//		 }
//		 
//		 
//		 
	}

	public void generateIrn(Long companyId,String invoiceId,String taskId,String loggedInUser) {
		
		Invoice invoice=ofy().load().type(Invoice.class).filter("companyId", companyId).filter("count",Integer.parseInt(invoiceId)).first().now();
		if(invoice==null)
			logger.log(Level.SEVERE,"Failed to load invoice"); 
		else {
			IntegrateSyncServiceImpl impl=new IntegrateSyncServiceImpl();
			String result=impl.getIRN(invoice, loggedInUser,taskId,true);
			
		}
	}
	
	public void rescheduleForPeriod(Long companyId,String fromDateStr,String toDateStr,String serviceIdList) {
		logger.log(Level.SEVERE,"rescheduleForPeriod task queue");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		 
		Date fromDate=null;
		Date toDate=null;
		ArrayList<Integer> idList=new ArrayList<Integer>();
		List<String> statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		statusList.add(Service.SERVICESTATUSOPEN);
		statusList.add(Service.SERVICESTATUSPENDING);
		statusList.add(Service.SERVICESTATUSPLANNED);
		
		try {
				fromDate=dateFormat.parse(fromDateStr);
				toDate=dateFormat.parse(toDateStr);
				logger.log(Level.SEVERE,"fromDate="+fromDate+" toDate="+toDate);
				
		} catch (ParseException e) {
				e.printStackTrace();
		}
		try {
			JSONArray idArray=new JSONArray(serviceIdList);
			logger.log(Level.SEVERE,"idArray size="+idArray.length());
			for(int i=0;i<idArray.length();i++){
				idList.add(Integer.parseInt(idArray.getString(i)));				
			}
			logger.log(Level.SEVERE,"idList size="+idList.size());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(fromDate!=null&&toDate!=null&&idList!=null){
			int diffdays = getDifferenceDays(fromDate, toDate);
			logger.log(Level.SEVERE,"diffdays="+diffdays);
			
			List<Service> allServiceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", idList).list();
			logger.log(Level.SEVERE,"allServiceList size="+allServiceList.size());
			
			ArrayList<Service> serviceList=new ArrayList<Service>();
			if(allServiceList!=null && allServiceList.size()>0){
				for(Service s:allServiceList){
					if(s.getStatus()!=null&&!s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						serviceList.add(s);
					}
				}
			}
			logger.log(Level.SEVERE,"141/31="+141.0/31);
			if(serviceList!=null&&serviceList.size()>0){
				logger.log(Level.SEVERE,"serviceList size="+serviceList.size());
				int serviceCount=serviceList.size();
//				double PerDayCountInDecimal=serviceCount/diffdays;
//				logger.log(Level.SEVERE,"PerDayCountInDecimal="+PerDayCountInDecimal);
//				int PerDayCountInInt=(int) PerDayCountInDecimal;
//				logger.log(Level.SEVERE,"PerDayCountInInt"+PerDayCountInInt);
				int servicePerDayCount=serviceCount/diffdays;
//				if(PerDayCountInDecimal>PerDayCountInInt)
//					servicePerDayCount=PerDayCountInInt+1;
//				else
//					servicePerDayCount=PerDayCountInInt;
				logger.log(Level.SEVERE,"serviceCount="+serviceCount+" servicePerDayCount="+servicePerDayCount);
				
								
				int count=0;
				
				if(serviceList.size()>=diffdays){

					for(int i=1;i<=diffdays;i++){
						int endIndex;
						if(i==diffdays)
							endIndex=serviceList.size();
						else
							endIndex=count+servicePerDayCount;
						logger.log(Level.SEVERE,"i="+i+" count="+count+" endIndex="+endIndex);
						
						SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
						String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST",fromDate)); 
//						
						if(endIndex<=serviceCount){
							for(int j=count;j<endIndex;j++){
								serviceList.get(j).setServiceDate(fromDate);
								serviceList.get(j).setServiceDay(serviceDay);
								logger.log(Level.SEVERE,"updated status of "+serviceList.get(j).getCount());
							}	
							count=endIndex;
						}else{
							logger.log(Level.SEVERE,"in else");
							for(int j=count;j<serviceList.size();j++){
								serviceList.get(j).setServiceDate(fromDate);
								serviceList.get(j).setServiceDay(serviceDay);
							}	
							count=endIndex;
						}
						Calendar cal=Calendar.getInstance();
						cal.setTime(fromDate);
						cal.add(Calendar.DAY_OF_MONTH,1);
						fromDate=cal.getTime();
						logger.log(Level.SEVERE,"next date="+fromDate);
					}
					
				}else{
					for(Service s:serviceList){
						SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
						String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST",fromDate)); 
//						
						s.setServiceDate(fromDate);
						s.setServiceDay(serviceDay);
						
						logger.log(Level.SEVERE,"in else updated status of "+s.getCount());
						Calendar cal=Calendar.getInstance();
						cal.setTime(fromDate);
						cal.add(Calendar.DAY_OF_MONTH,1);
						fromDate=cal.getTime();
						logger.log(Level.SEVERE,"next date="+fromDate);
					}
				}
				ofy().save().entities(serviceList);		
			}
		
		}
		
	}
	 public static int getDifferenceDays(Date d1, Date d2) {
		  	int daysdiff = 0;
		    long diff = d2.getTime() - d1.getTime();
		    long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
		    daysdiff = (int) diffDays;
		    return daysdiff;
	}
	 private void resetAppid(Long companyId, String appId, String companyName, String loggedinuser, String otp,String approverCellNo) 
	 {

		 logger.log(Level.SEVERE,"in resetAppid deletion method company id= "+companyId+" appid="+appId+" companyName="+companyName+" loggedinuser="+loggedinuser+" otp="+otp);
		
		 List<CustomerBranchServiceLocation> customerBranchServiceLocationList= ofy().load().type(CustomerBranchServiceLocation.class).filter("companyId", companyId).list();
		 if(customerBranchServiceLocationList!=null)
			 ofy().delete().entities(customerBranchServiceLocationList);
		 
		 List<ScreenMenuConfiguration> ScreenMenuConfigurationList= ofy().load().type(ScreenMenuConfiguration.class).filter("companyId", companyId).list();
		 if(ScreenMenuConfigurationList!=null)
		 ofy().delete().entities(ScreenMenuConfigurationList);
		 
		 List<EmployeeTrackingDetails> EmployeeTrackingDetailsnList= ofy().load().type(EmployeeTrackingDetails.class).filter("companyId", companyId).list();
		 if(EmployeeTrackingDetailsnList!=null)
		 ofy().delete().entities(EmployeeTrackingDetailsnList);
		 
		 List<ExpensePolicies> ExpensePoliciesList= ofy().load().type(ExpensePolicies.class).filter("companyId", companyId).list();
		 if(ExpensePoliciesList!=null)
		 ofy().delete().entities(ExpensePoliciesList);
		 
		 List<Esic> EsicList= ofy().load().type(Esic.class).filter("companyId", companyId).list();
		 if(EsicList!=null)
		 ofy().delete().entities(EsicList);
		 
		 List<ProvidentFund> ProvidentFundList= ofy().load().type(ProvidentFund.class).filter("companyId", companyId).list();
		 if(ProvidentFundList!=null)
		 ofy().delete().entities(ProvidentFundList);
		 
		 List<Bonus> BonusList= ofy().load().type(Bonus.class).filter("companyId", companyId).list();
		 if(BonusList!=null)
		 ofy().delete().entities(BonusList);
		 
		 List<PaidLeave> PaidLeaveList= ofy().load().type(PaidLeave.class).filter("companyId", companyId).list();
		 if(PaidLeaveList!=null)
		 ofy().delete().entities(PaidLeaveList);
		 
		 List<LWF> LWFList= ofy().load().type(LWF.class).filter("companyId", companyId).list();
		 if(LWFList!=null)
		 ofy().delete().entities(LWFList);
		 
		 List<UnitConversion> UnitConversionList= ofy().load().type(UnitConversion.class).filter("companyId", companyId).list();
		 if(UnitConversionList!=null)
		 ofy().delete().entities(UnitConversionList);
		 
		 List<ProfessionalTax> ProfessionalTaxList= ofy().load().type(ProfessionalTax.class).filter("companyId", companyId).list();
		 if(ProfessionalTaxList!=null)
		 ofy().delete().entities(ProfessionalTaxList);
		 
		 List<EmployeeFingerDB> EmployeeFingerDBList= ofy().load().type(EmployeeFingerDB.class).filter("companyId", companyId).list();
		 if(EmployeeFingerDBList!=null)
		 ofy().delete().entities(EmployeeFingerDBList);
		 
		 List<Attendance> AttendanceList= ofy().load().type(Attendance.class).filter("companyId", companyId).list();
		 if(AttendanceList!=null)
		 ofy().delete().entities(AttendanceList);
		 
		 List<CNC> CNCList= ofy().load().type(CNC.class).filter("companyId", companyId).list();
		 if(CNCList!=null)
		 ofy().delete().entities(CNCList);
		 
		 List<VersionDetails> VersionDetailsList= ofy().load().type(VersionDetails.class).filter("companyId", companyId).list();
		 if(VersionDetailsList!=null)
		 ofy().delete().entities(VersionDetailsList);
		 
		 List<ProjectAllocation> ProjectAllocationList= ofy().load().type(ProjectAllocation.class).filter("companyId", companyId).list();
		 if(ProjectAllocationList!=null)
		 ofy().delete().entities(ProjectAllocationList);
		 
		 List<CronJobConfigration> CronJobConfigrationList= ofy().load().type(CronJobConfigration.class).filter("companyId", companyId).list();
		 if(CronJobConfigrationList!=null)
		 ofy().delete().entities(CronJobConfigrationList);
		 
		 List<ServiceSchedule> ServiceScheduleList= ofy().load().type(ServiceSchedule.class).filter("companyId", companyId).list();
		 if(ServiceScheduleList!=null)
		 ofy().delete().entities(ServiceScheduleList);
		 
		 List<RegisterDevice> RegisterDeviceList= ofy().load().type(RegisterDevice.class).filter("companyId", companyId).list();
		 if(RegisterDeviceList!=null)
		 ofy().delete().entities(RegisterDeviceList);
		 
		 List<LoneType> LoneTypeList= ofy().load().type(LoneType.class).filter("companyId", companyId).list();
		 if(LoneTypeList!=null)
		 ofy().delete().entities(LoneTypeList);
		 
		 List<PaySlip> PaySlipList= ofy().load().type(PaySlip.class).filter("companyId", companyId).list();
		 if(PaySlipList!=null)
		 ofy().delete().entities(PaySlipList);
		 
		 List<LoneEmi> LoneEmiList= ofy().load().type(LoneEmi.class).filter("companyId", companyId).list();
		 if(LoneEmiList!=null)
		 ofy().delete().entities(LoneEmiList);
		 
		 List<LeaveBalance> LeaveBalanceList= ofy().load().type(LeaveBalance.class).filter("companyId", companyId).list();
		 if(LeaveBalanceList!=null)
		 ofy().delete().entities(LeaveBalanceList);
		 
		 List<LoggedIn> LoggedInList= ofy().load().type(LoggedIn.class).filter("companyId", companyId).list();
		 if(LoggedInList!=null)
		 ofy().delete().entities(LoggedInList);
		 
		 List<Quotation> QuotationList= ofy().load().type(Quotation.class).filter("companyId", companyId).list();
		 if(QuotationList!=null)
		 ofy().delete().entities(QuotationList);
		 
		 List<Approvals> ApprovalsList= ofy().load().type(Approvals.class).filter("companyId", companyId).list();
		 if(ApprovalsList!=null)
		 ofy().delete().entities(ApprovalsList);
		 
		 //no entity annotation
		 List<Config> ConfigList= ofy().load().type(Config.class).list();
		 if(ConfigList!=null)
		 ofy().delete().entities(ConfigList);
		 
		 List<ConfigCategory> ConfigCategoryList= ofy().load().type(ConfigCategory.class).list();
		 if(ConfigCategoryList!=null)
		 ofy().delete().entities(ConfigCategoryList);
		 
		 List<Type> Type= ofy().load().type(Type.class).list();
		 if(Type!=null)
		 ofy().delete().entities(Type);
		 
//		 List<Activity> Activity= ofy().load().type(Activity.class).filter("companyId", companyId).list();
//		 if(Activity!=null)
//		 ofy().delete().entities(Activity);
		 
		 List<TimeReportConfig> TimeReportConfig= ofy().load().type(TimeReportConfig.class).filter("companyId", companyId).list();
		 if(TimeReportConfig!=null)
		 ofy().delete().entities(TimeReportConfig);
		 
		 List<Branch> Branch= ofy().load().type(Branch.class).filter("companyId", companyId).list();
		 if(Branch!=null)
		 ofy().delete().entities(Branch);
		 
		 List<BranchRelation> BranchRelation= ofy().load().type(BranchRelation.class).list();
		 if(BranchRelation!=null)
		 ofy().delete().entities(BranchRelation);
		 
		 List<Employee> Employee= ofy().load().type(Employee.class).filter("companyId", companyId).list();
		 if(Employee!=null)
		 ofy().delete().entities(Employee);
		 
		 List<EmployeeInfo> EmployeeInfo= ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).list();
		 if(EmployeeInfo!=null)
		 ofy().delete().entities(EmployeeInfo);
		 
		 List<EmployeeRelation> EmployeeRelation= ofy().load().type(EmployeeRelation.class).list();
		 if(EmployeeRelation!=null)
		 ofy().delete().entities(EmployeeRelation);
		 
		 List<SuperProduct> SuperProduct= ofy().load().type(SuperProduct.class).list();
		 if(SuperProduct!=null)
		 ofy().delete().entities(SuperProduct);
		 
		 List<Vendor> Vendor= ofy().load().type(Vendor.class).filter("companyId", companyId).list();
		 if(Vendor!=null)
		 ofy().delete().entities(Vendor);
		 
		 List<Expense> Expense= ofy().load().type(Expense.class).filter("companyId", companyId).list();
		 if(Expense!=null)
		 ofy().delete().entities(Expense);
		 		
		 List<VendorRelation> VendorRelation= ofy().load().type(VendorRelation.class).filter("companyId", companyId).list();
		 if(VendorRelation!=null)
		 ofy().delete().entities(VendorRelation);
		 
		 List<CustomerRelation> CustomerRelation= ofy().load().type(CustomerRelation.class).list();
		 if(CustomerRelation!=null)
		 ofy().delete().entities(CustomerRelation);
		 
		 List<Category> Category= ofy().load().type(Category.class).filter("companyId", companyId).list();
		 if(Category!=null)
		 ofy().delete().entities(Category);		 
		 
		 List<com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar> Calendar= ofy().load().type(com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.class).filter("companyId", companyId).list();
		 if(Calendar!=null)
		 ofy().delete().entities(Calendar);
	
		 List<GLAccount> GLAccount= ofy().load().type(GLAccount.class).filter("companyId", companyId).list();
		 if(GLAccount!=null)
		 ofy().delete().entities(GLAccount);
	
		 List<SalesOrder> SalesOrder= ofy().load().type(SalesOrder.class).filter("companyId", companyId).list();
		 if(SalesOrder!=null)
		 ofy().delete().entities(SalesOrder);
	
		 List<EmployeeLeave> EmployeeLeave= ofy().load().type(EmployeeLeave.class).filter("companyId", companyId).list();
		 if(EmployeeLeave!=null)
		 ofy().delete().entities(EmployeeLeave);
	
		 List<Loan> Loan= ofy().load().type(Loan.class).filter("companyId", companyId).list();
		 if(Loan!=null)
		 ofy().delete().entities(Loan);
	
		 List<Country> Country= ofy().load().type(Country.class).list();
		 if(Country!=null)
		 ofy().delete().entities(Country);
	
		 List<LeaveType> LeaveType= ofy().load().type(LeaveType.class).filter("companyId", companyId).list();
		 if(LeaveType!=null)
		 ofy().delete().entities(LeaveType);
	
		 List<Shift> Shift= ofy().load().type(Shift.class).filter("companyId", companyId).list();
		 if(Shift!=null)
		 ofy().delete().entities(Shift);
	
		 List<Department> Department= ofy().load().type(Department.class).filter("companyId", companyId).list();
		 if(Department!=null)
		 ofy().delete().entities(Department);
	
		 List<UserRole> UserRole= ofy().load().type(UserRole.class).list();
		 if(UserRole!=null)
		 ofy().delete().entities(UserRole);
	
		 List<HrProject> HrProject= ofy().load().type(HrProject.class).filter("companyId", companyId).list();
		 if(HrProject!=null)
		 ofy().delete().entities(HrProject);
	
		 List<HolidayType> HolidayType= ofy().load().type(HolidayType.class).filter("companyId", companyId).list();
		 if(HolidayType!=null)
		 ofy().delete().entities(HolidayType);
	
		 List<EmployeeGroup> EmployeeGroup= ofy().load().type(EmployeeGroup.class).filter("companyId", companyId).list();
		 if(EmployeeGroup!=null)
		 ofy().delete().entities(EmployeeGroup);
	
		 List<LeaveGroup> LeaveGroup= ofy().load().type(LeaveGroup.class).filter("companyId", companyId).list();
		 if(LeaveGroup!=null)
		 ofy().delete().entities(LeaveGroup);
	
		 List<CtcComponent> CtcComponent= ofy().load().type(CtcComponent.class).filter("companyId", companyId).list();
		 if(CtcComponent!=null)
		 ofy().delete().entities(CtcComponent);
	
		 List<CTC> CTC= ofy().load().type(CTC.class).filter("companyId", companyId).list();
		 if(CTC!=null)
		 ofy().delete().entities(CTC);
	
		 List<EmployeeOvertime> EmployeeOvertime= ofy().load().type(EmployeeOvertime.class).filter("companyId", companyId).list();
		 if(EmployeeOvertime!=null)
		 ofy().delete().entities(EmployeeOvertime);
	
		 List<CompanyAsset> CompanyAsset= ofy().load().type(CompanyAsset.class).filter("companyId", companyId).list();
		 if(CompanyAsset!=null)
		 ofy().delete().entities(CompanyAsset);
	
		 List<ServiceProject> ServiceProject= ofy().load().type(ServiceProject.class).filter("companyId", companyId).list();
		 if(ServiceProject!=null)
		 ofy().delete().entities(ServiceProject);
	
		 List<EmployeeAttendance> EmployeeAttendance= ofy().load().type(EmployeeAttendance.class).filter("companyId", companyId).list();
		 if(EmployeeAttendance!=null)
		 ofy().delete().entities(EmployeeAttendance);
	
		 List<SalesQuotation> SalesQuotation= ofy().load().type(SalesQuotation.class).filter("companyId", companyId).list();
		 if(SalesQuotation!=null)
		 ofy().delete().entities(SalesQuotation);
	
		 List<PriceList> PriceList= ofy().load().type(PriceList.class).filter("companyId", companyId).list();
		 if(PriceList!=null)
		 ofy().delete().entities(PriceList);
	
		 List<PettyCash> PettyCash= ofy().load().type(PettyCash.class).filter("companyId", companyId).list();
		 if(PettyCash!=null)
		 ofy().delete().entities(PettyCash);
	
		 List<PettyCashDeposits> PettyCashDeposits= ofy().load().type(PettyCashDeposits.class).filter("companyId", companyId).list();
		 if(PettyCashDeposits!=null)
		 ofy().delete().entities(PettyCashDeposits);
	
		 List<PettyCashTransaction> PettyCashTransaction= ofy().load().type(PettyCashTransaction.class).filter("companyId", companyId).list();
		 if(PettyCashTransaction!=null)
		 ofy().delete().entities(PettyCashTransaction);
	
		 List<TaxesAndCharges> TaxesAndCharges= ofy().load().type(TaxesAndCharges.class).filter("companyId", companyId).list();
		 if(TaxesAndCharges!=null)
		 ofy().delete().entities(TaxesAndCharges);
	
		 List<DeliveryNote> DeliveryNote= ofy().load().type(DeliveryNote.class).filter("companyId", companyId).list();
		 if(DeliveryNote!=null)
		 ofy().delete().entities(DeliveryNote);
	
		 List<PurchaseOrder> PurchaseOrder= ofy().load().type(PurchaseOrder.class).filter("companyId", companyId).list();
		 if(PurchaseOrder!=null)
			 ofy().delete().entities(PurchaseOrder);
	
		 List<PurchaseRequisition> PurchaseRequisition= ofy().load().type(PurchaseRequisition.class).filter("companyId", companyId).list();
		 if(PurchaseRequisition!=null)
		 ofy().delete().entities(PurchaseRequisition);
	
		 List<GRN> GRN= ofy().load().type(GRN.class).filter("companyId", companyId).list();
		 if(GRN!=null)
		 ofy().delete().entities(GRN);
	
		 List<WareHouse> WareHouse= ofy().load().type(WareHouse.class).filter("companyId", companyId).list();
		 if(WareHouse!=null)
		 ofy().delete().entities(WareHouse);
	
		 List<StorageLocation> StorageLocation= ofy().load().type(StorageLocation.class).filter("companyId", companyId).list();
		 if(StorageLocation!=null)
		 ofy().delete().entities(StorageLocation);
	
		 List<Storagebin> Storagebin= ofy().load().type(Storagebin.class).filter("companyId", companyId).list();
		 if(Storagebin!=null)
		 ofy().delete().entities(Storagebin);
	
		 List<ProductInventoryView> ProductInventoryView= ofy().load().type(ProductInventoryView.class).filter("companyId", companyId).list();
		 if(ProductInventoryView!=null)
		 ofy().delete().entities(ProductInventoryView);
	
		 List<ProductInventoryViewDetails> ProductInventoryViewDetails= ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", companyId).list();
		 if(ProductInventoryViewDetails!=null)
		 ofy().delete().entities(ProductInventoryViewDetails);
	
		 List<CompanyPayment> CompanyPayment= ofy().load().type(CompanyPayment.class).filter("companyId", companyId).list();
		 if(CompanyPayment!=null)
		 ofy().delete().entities(CompanyPayment);
	
		 List<VendorPayment> VendorPayment= ofy().load().type(VendorPayment.class).filter("companyId", companyId).list();
		 if(VendorPayment!=null)
		 ofy().delete().entities(VendorPayment);
	
		 List<TaxDetails> TaxDetails= ofy().load().type(TaxDetails.class).filter("companyId", companyId).list();
		 if(TaxDetails!=null)
		 ofy().delete().entities(TaxDetails);
	
		 List<State> State= ofy().load().type(State.class).filter("companyId", companyId).list();
		 if(State!=null)
		 ofy().delete().entities(State);
	
		 List<City> City= ofy().load().type(City.class).list();
		 if(City!=null)
		 ofy().delete().entities(City);
	
		 List<Locality> Locality= ofy().load().type(Locality.class).filter("companyId", companyId).list();
		 if(Locality!=null)
		 ofy().delete().entities(Locality);
	
		 List<CheckListStep> CheckListStep= ofy().load().type(CheckListStep.class).filter("companyId", companyId).list();
		 if(CheckListStep!=null)
		 ofy().delete().entities(CheckListStep);
	
		 List<CheckListType> CheckListType= ofy().load().type(CheckListType.class).filter("companyId", companyId).list();
		 if(CheckListType!=null)
		 ofy().delete().entities(CheckListType);
	
		 List<ShippingMethod> ShippingMethod= ofy().load().type(ShippingMethod.class).filter("companyId", companyId).list();
		 if(ShippingMethod!=null)
		 ofy().delete().entities(ShippingMethod);
		 
		 List<PackingMethod> PackingMethod= ofy().load().type(PackingMethod.class).filter("companyId", companyId).list();
		 if(PackingMethod!=null)
		 ofy().delete().entities(PackingMethod);
	
		 List<EmployeeShift> EmployeeShift= ofy().load().type(EmployeeShift.class).filter("companyId", companyId).list();
		 if(EmployeeShift!=null)
		 ofy().delete().entities(EmployeeShift);
	
		 List<PaySlipRecord> PaySlipRecord= ofy().load().type(PaySlipRecord.class).filter("companyId", companyId).list();
		 if(PaySlipRecord!=null)
		 ofy().delete().entities(PaySlipRecord);
	
		 List<ProductGroupList> ProductGroupList= ofy().load().type(ProductGroupList.class).filter("companyId", companyId).list();
		 if(ProductGroupList!=null)
		 ofy().delete().entities(ProductGroupList);
	
		 List<ProductGroupDetails> ProductGroupDetails= ofy().load().type(ProductGroupDetails.class).filter("companyId", companyId).list();
		 if(ProductGroupDetails!=null)
		 ofy().delete().entities(ProductGroupDetails);
	
		 List<BillOfMaterial> BillOfMaterial= ofy().load().type(BillOfMaterial.class).filter("companyId", companyId).list();
		 if(BillOfMaterial!=null)
		 ofy().delete().entities(BillOfMaterial);
	
		 List<AccountingInterface> AccountingInterface= ofy().load().type(AccountingInterface.class).filter("companyId", companyId).list();
		 if(AccountingInterface!=null)
		 ofy().delete().entities(AccountingInterface);
	
		 List<ContactPersonIdentification> ContactPersonIdentification= ofy().load().type(ContactPersonIdentification.class).filter("companyId", companyId).list();
		 if(ContactPersonIdentification!=null)
		 ofy().delete().entities(ContactPersonIdentification);
	
		 List<PhysicalInventoryMaintaince> PhysicalInventoryMaintaince= ofy().load().type(PhysicalInventoryMaintaince.class).filter("companyId", companyId).list();
		 if(PhysicalInventoryMaintaince!=null)
		 ofy().delete().entities(PhysicalInventoryMaintaince);
	
		 List<MaterialRequestNote> MaterialRequestNote= ofy().load().type(MaterialRequestNote.class).filter("companyId", companyId).list();
		 if(MaterialRequestNote!=null)
		 ofy().delete().entities(MaterialRequestNote);
	
		 List<MaterialIssueNote> MaterialIssueNote= ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).list();
		 if(MaterialIssueNote!=null)
		 ofy().delete().entities(MaterialIssueNote);
	
		 List<ProductInventoryTransaction> ProductInventoryTransaction= ofy().load().type(ProductInventoryTransaction.class).filter("companyId", companyId).list();
		 if(ProductInventoryTransaction!=null)
		 ofy().delete().entities(ProductInventoryTransaction);
	
		 List<MaterialMovementNote> MaterialMovementNote= ofy().load().type(MaterialMovementNote.class).filter("companyId", companyId).list();
		 if(MaterialMovementNote!=null)
		 ofy().delete().entities(MaterialMovementNote);
	
		 List<ProcessName> ProcessName= ofy().load().type(ProcessName.class).filter("companyId", companyId).list();
		 if(ProcessName!=null)
		 ofy().delete().entities(ProcessName);
	
		 List<InteractionType> InteractionType= ofy().load().type(InteractionType.class).filter("companyId", companyId).list();
		 if(InteractionType!=null)
		 ofy().delete().entities(InteractionType);
	
		 List<ProcessConfiguration> ProcessConfiguration= ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).list();
		 if(ProcessConfiguration!=null)
		 ofy().delete().entities(ProcessConfiguration);
	
		 List<DocumentHistory> DocumentHistory= ofy().load().type(DocumentHistory.class).filter("companyId", companyId).list();
		 if(DocumentHistory!=null)
		 ofy().delete().entities(DocumentHistory);
	
		 List<CancelSummary> CancelSummary= ofy().load().type(CancelSummary.class).filter("companyId", companyId).list();
		 if(CancelSummary!=null)
		 ofy().delete().entities(CancelSummary);
	
		 List<Inspection> Inspection= ofy().load().type(Inspection.class).filter("companyId", companyId).list();
		 if(Inspection!=null)
		 ofy().delete().entities(Inspection);
	
		 List<BillOfProductMaterial> BillOfProductMaterial= ofy().load().type(BillOfProductMaterial.class).filter("companyId", companyId).list();
		 if(BillOfProductMaterial!=null)
		 ofy().delete().entities(BillOfProductMaterial);
	
		 List<VendorPriceListDetails> VendorPriceListDetails= ofy().load().type(VendorPriceListDetails.class).filter("companyId", companyId).list();
		 if(VendorPriceListDetails!=null)
		 ofy().delete().entities(VendorPriceListDetails);
	
		 List<WorkOrder> WorkOrder= ofy().load().type(WorkOrder.class).filter("companyId", companyId).list();
		 if(WorkOrder!=null)
		 ofy().delete().entities(WorkOrder);
	
		 List<CustomerBranchDetails> CustomerBranchDetails= ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId).list();
		 if(CustomerBranchDetails!=null)
		 ofy().delete().entities(CustomerBranchDetails);
	
		 List<SmsTemplate> SmsTemplate= ofy().load().type(SmsTemplate.class).filter("companyId", companyId).list();
		 if(SmsTemplate!=null)
		 ofy().delete().entities(SmsTemplate);
	
		 List<SmsConfiguration> SmsConfiguration= ofy().load().type(SmsConfiguration.class).filter("companyId", companyId).list();
		 if(SmsConfiguration!=null)
		 ofy().delete().entities(SmsConfiguration);
	
		 List<IPAddressAuthorization> IPAddressAuthorization= ofy().load().type(IPAddressAuthorization.class).filter("companyId", companyId).list();
		 if(IPAddressAuthorization!=null)
		 ofy().delete().entities(IPAddressAuthorization);
	
		 List<MultilevelApproval> MultilevelApproval= ofy().load().type(MultilevelApproval.class).filter("companyId", companyId).list();
		 if(MultilevelApproval!=null)
		 ofy().delete().entities(MultilevelApproval);
	
		 List<SmsHistory> SmsHistory= ofy().load().type(SmsHistory.class).filter("companyId", companyId).list();
		 if(SmsHistory!=null)
		 ofy().delete().entities(SmsHistory);
	
		 List<Fumigation> Fumigation= ofy().load().type(Fumigation.class).filter("companyId", companyId).list();
		 if(Fumigation!=null)
		 ofy().delete().entities(Fumigation);
	
		 List<CustomerTrainingDetails> CustomerTrainingDetails= ofy().load().type(CustomerTrainingDetails.class).filter("companyId", companyId).list();
		 if(CustomerTrainingDetails!=null)
		 ofy().delete().entities(CustomerTrainingDetails);
	
		 List<EmployeeAdditionalDetails> EmployeeAdditionalDetails= ofy().load().type(EmployeeAdditionalDetails.class).filter("companyId", companyId).list();
		 if(EmployeeAdditionalDetails!=null)
		 ofy().delete().entities(EmployeeAdditionalDetails);
	
		 List<Overtime> Overtime= ofy().load().type(Overtime.class).filter("companyId", companyId).list();
		 if(Overtime!=null)
		 ofy().delete().entities(Overtime);
	
		 List<CompanyPayrollRecord> CompanyPayrollRecord= ofy().load().type(CompanyPayrollRecord.class).filter("companyId", companyId).list();
		 if(CompanyPayrollRecord!=null)
		 ofy().delete().entities(CompanyPayrollRecord);
	
		 List<Declaration> Declaration= ofy().load().type(Declaration.class).filter("companyId", companyId).list();
		 if(Declaration!=null)
		 ofy().delete().entities(Declaration);
	
		 List<Complain> Complain= ofy().load().type(Complain.class).filter("companyId", companyId).list();
		 if(Complain!=null)
		 ofy().delete().entities(Complain);
	
		 List<ContractRenewal> ContractRenewal= ofy().load().type(ContractRenewal.class).filter("companyId", companyId).list();
		 if(ContractRenewal!=null)
		 ofy().delete().entities(ContractRenewal);
	
		 List<TeamManagement> TeamManagement= ofy().load().type(TeamManagement.class).filter("companyId", companyId).list();
		 if(TeamManagement!=null)
		 ofy().delete().entities(TeamManagement);
	
		 List<CustomerUser> CustomerUser= ofy().load().type(CustomerUser.class).filter("companyId", companyId).list();
		 if(CustomerUser!=null)
		 ofy().delete().entities(CustomerUser);
	
//		 List<FinancialCalender> FinancialCalender= ofy().load().type(FinancialCalender.class).filter("companyId", companyId).list();
//		 if(FinancialCalender!=null)
//		 ofy().delete().entities(FinancialCalender);
	
//		 List<SalesPersonTargets> SalesPersonTargets= ofy().load().type(SalesPersonTargets.class).filter("companyId", companyId).list();
//		 if(SalesPersonTargets!=null)
//		 ofy().delete().entities(SalesPersonTargets);
	
		 List<AssesmentReport> AssesmentReport= ofy().load().type(AssesmentReport.class).filter("companyId", companyId).list();
		 if(AssesmentReport!=null)
		 ofy().delete().entities(AssesmentReport);
	
		 List<MultipleExpenseMngt> MultipleExpenseMngt= ofy().load().type(MultipleExpenseMngt.class).filter("companyId", companyId).list();
		 if(MultipleExpenseMngt!=null)
		 ofy().delete().entities(MultipleExpenseMngt);
	
		 List<CTCTemplate> CTCTemplate= ofy().load().type(CTCTemplate.class).filter("companyId", companyId).list();
		 if(CTCTemplate!=null)
		 ofy().delete().entities(CTCTemplate);
	
		 try {
		 List<EmployeeAsset> EmployeeAsset= ofy().load().type(EmployeeAsset.class).filter("companyId", companyId).list();
		 if(EmployeeAsset!=null)
		 ofy().delete().entities(EmployeeAsset);
		 }catch(Exception e) {
			 
		 }
	
		
		 try {
			 List<ArrearsDetails> ArrearsDetails= ofy().load().type(ArrearsDetails.class).filter("companyId", companyId).list();
			 if(ArrearsDetails!=null)
			 ofy().delete().entities(ArrearsDetails);
		 }catch(Exception e) {
			 
		 }
		 
		 try {
			 List<CNCVersion> CNCVersion= ofy().load().type(CNCVersion.class).filter("companyId", companyId).list();
			 if(CNCVersion!=null)
			 ofy().delete().entities(CNCVersion);
		
		 }catch(Exception e) {
			 
		 }

		
		 List<VendorInvoice> VendorInvoice= ofy().load().type(VendorInvoice.class).filter("companyId", companyId).list();
		 if(VendorInvoice!=null)
		 ofy().delete().entities(VendorInvoice);
	
		 try {
			 List<UploadHistory> UploadHistory= ofy().load().type(UploadHistory.class).filter("companyId", companyId).list();
			 if(UploadHistory!=null)
			 ofy().delete().entities(UploadHistory);
		 }catch(Exception e) {
			 
		 }
		 
		 
	
		 List<TechnicianWareHouseDetailsList> TechnicianWareHouseDetailsList= ofy().load().type(TechnicianWareHouseDetailsList.class).filter("companyId", companyId).list();
		 if(TechnicianWareHouseDetailsList!=null)
		 ofy().delete().entities(TechnicianWareHouseDetailsList);
	
		 List<ProvidentFund> ProvidentFund= ofy().load().type(ProvidentFund.class).filter("companyId", companyId).list();
		 if(ProvidentFund!=null)
		 ofy().delete().entities(ProvidentFund);
	
		 List<Esic> Esic= ofy().load().type(Esic.class).filter("companyId", companyId).list();
		 if(Esic!=null)
		 ofy().delete().entities(Esic);
	
		 List<ProfessionalTax> ProfessionalTax= ofy().load().type(ProfessionalTax.class).filter("companyId", companyId).list();
		 if(ProfessionalTax!=null)
		 ofy().delete().entities(ProfessionalTax);
	
		 List<LWF> LWF= ofy().load().type(LWF.class).filter("companyId", companyId).list();
		 if(LWF!=null)
		 ofy().delete().entities(LWF);
	
		 List<CreditNote> CreditNote= ofy().load().type(CreditNote.class).filter("companyId", companyId).list();
		 if(CreditNote!=null)
		 ofy().delete().entities(CreditNote);
	
		 List<EmailTemplate> EmailTemplate= ofy().load().type(EmailTemplate.class).filter("companyId", companyId).list();
		 if(EmailTemplate!=null)
		 ofy().delete().entities(EmailTemplate);
	
		 List<EmailTemplateConfiguration> EmailTemplateConfiguration= ofy().load().type(EmailTemplateConfiguration.class).filter("companyId", companyId).list();
		 if(EmailTemplateConfiguration!=null)
		 ofy().delete().entities(EmailTemplateConfiguration);
	
		 List<HrProjectOvertime> HrProjectOvertime= ofy().load().type(HrProjectOvertime.class).filter("companyId", companyId).list();
		 if(HrProjectOvertime!=null)
		 ofy().delete().entities(HrProjectOvertime);
	
		 List<EmployeeProjectAllocation> EmployeeProjectAllocation= ofy().load().type(EmployeeProjectAllocation.class).filter("companyId", companyId).list();
		 if(EmployeeProjectAllocation!=null)
		 ofy().delete().entities(EmployeeProjectAllocation);
	
		 List<AuditObservations> AuditObservations= ofy().load().type(AuditObservations.class).filter("companyId", companyId).list();
		 if(AuditObservations!=null)
		 ofy().delete().entities(AuditObservations);
	
		 List<TermsAndConditions> TermsAndConditions= ofy().load().type(TermsAndConditions.class).filter("companyId", companyId).list();
		 if(TermsAndConditions!=null)
		 ofy().delete().entities(TermsAndConditions);
	
		 List<Contract> ContractList= ofy().load().type(Contract.class).filter("companyId", companyId).list();
		 if(ContractList!=null)
		 ofy().delete().entities(ContractList);
		 
		 List<Lead> Lead= ofy().load().type(Lead.class).filter("companyId", companyId).list();
		 if(Lead!=null)
		 ofy().delete().entities(Lead);
		 
		 List<Customer> Customer= ofy().load().type(Customer.class).filter("companyId", companyId).list();
		 if(Customer!=null)
		 ofy().delete().entities(Customer);
		 
		 List<BillingDocument> BillingDocument= ofy().load().type(BillingDocument.class).filter("companyId", companyId).list();
		 if(BillingDocument!=null)
		 ofy().delete().entities(BillingDocument);
	
		 List<Invoice> Invoice= ofy().load().type(Invoice.class).filter("companyId", companyId).list();
		 if(Invoice!=null)
		 ofy().delete().entities(Invoice);
	
		 List<CustomerPayment> CustomerPayment= ofy().load().type(CustomerPayment.class).filter("companyId", companyId).list();
		 if(CustomerPayment!=null)
		 ofy().delete().entities(CustomerPayment);
	
		 try {
			 int count=ofy().load().type(Service.class).filter("companyId", companyId).count();
			 if(count>0&&count>500) {
				 logger.log(Level.SEVERE,"service count="+count);
				 int loop=count/500;
				 for(int i=0;i<=loop;i++) {
					 List<Service> ServiceList= ofy().load().type(Service.class).filter("companyId", companyId).limit(500).list();
					 if(ServiceList!=null) {
						 ofy().delete().entities(ServiceList);	
						 logger.log(Level.SEVERE,"service slot count="+ServiceList.size());
					 }
				 }
			 }
		 }catch(Exception e) {
			 logger.log(Level.SEVERE,"failed to delete services");
		 }
		 
		 List<VendorRelation> VendorRelationList= ofy().load().type(VendorRelation.class).list();
		 if(VendorRelationList!=null)
		 ofy().delete().entities(VendorRelationList);
		 
		
	 
		 EmailDetails email = new EmailDetails();
		 email.setFromEmailid("evasoftwaresolutionserp@gmail.com");
		 ArrayList<String> toemailId = new ArrayList<String>();
		 toemailId.add("support@evasoftwaresolutions.com");
		 email.setToEmailId(toemailId);
		 email.setSubject("Alert : Someone has reset appid "+appId);
		 email.setEmailBody("Dear Sir/Madam,<br> User "+loggedinuser+ " has reset the appid "+appId+" Company Name: "+companyName+" with OTP "+otp+". Otp was sent to cell number "+approverCellNo);
		 Email emailAsync=new Email();
		 emailAsync.sendEmail(email, companyId);
		 logger.log(Level.SEVERE,"email sent");
	 }
}
