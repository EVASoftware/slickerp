package com.slicktechnologies.server.taskqueue;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.slicktechnologies.client.views.contract.ProductTaxesTable;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

public class ContractTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7421478067060624084L;

	
	SalesLineItemTable saleslineitemtable;
	ProductTaxesTable productTaxTable;
	private SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
	Logger logger = Logger.getLogger("Name of logger");

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Welcome to Contract Task Queue");
		System.out.println("TaskQueue called");

		String contractdatawithdollar = request.getParameter("contractkey");

		System.out.println("The value is :" + contractdatawithdollar);
		String[] contractdatawithoutdollar = contractdatawithdollar
				.split("[$]");

		for (int i = 0; i < contractdatawithoutdollar.length; i++) {
			System.out.println("The Data of position " + i + " is :"
					+ contractdatawithoutdollar[i]);
		}
		/********** Data Required for end date *****************/
		String contractendDate = contractdatawithoutdollar[16];
		/********** Finished with Data Required for end date *****************/

		Contract contract = new Contract();

		long companyId = Long.parseLong(contractdatawithoutdollar[26]);
		System.out.println("The company Id is:" + companyId);
		String refrNumber2 = contractdatawithoutdollar[0];
		System.out.println("Ref No 2 of customer :" + refrNumber2);

		/******** Fetching Customer Data ****************/
		List<Customer> listofcustomer = ofy().load().type(Customer.class)
				.filter("companyId", companyId)
				.filter("refrNumber2", refrNumber2).list();

		System.out.println("listofcustomer" + listofcustomer.size());

		System.out.println("First Name :"
				+ listofcustomer.get(0).getFirstName());
		System.out.println("Middle Name :"
				+ listofcustomer.get(0).getMiddleName());
		System.out.println("Last Name :" + listofcustomer.get(0).getLastName());
		System.out.println("Customer ID:" + listofcustomer.get(0).getCount());
		System.out.println("cell number"
				+ listofcustomer.get(0).getCellNumber1());
		System.out.println("Company name"
				+ listofcustomer.get(0).getCompanyName());

		String firstname = listofcustomer.get(0).getFirstName();
		String middlename = listofcustomer.get(0).getMiddleName();
		String lastname = listofcustomer.get(0).getLastName();
		// String pocName=listofcustomer2.get(0).getName()
		int customerid = listofcustomer.get(0).getCount();
		Long cellnumber = listofcustomer.get(0).getCellNumber1();
		String fullname = firstname + " " + middlename + " " + lastname;
		String companyname = listofcustomer.get(0).getCompanyName();
		/************* Finished Fetching customer data **********************/

		/************** Customer Data Setting ***********************/
		contract.getCinfo().setCount(customerid);

		if (listofcustomer.get(0).getCompanyName() != null) {
			contract.getCinfo().setFullName(companyname);
		} else {
			contract.getCinfo().setFullName(fullname);
		}

		contract.getCinfo().setCellNumber(cellnumber);
		contract.getCinfo().setPocName(fullname);

		/************** Done with customer data setting ************/

		if (contractdatawithoutdollar[1].equalsIgnoreCase("na")) {
			contract.setRefNo("");
		} else {
			contract.setRefNo(contractdatawithoutdollar[1]);

		}

		if (contractdatawithoutdollar[2].equalsIgnoreCase("na")) {
			try {
				contract.setRefDate(fmt.parse(""));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				System.out.println("Ref Date is"+contractdatawithoutdollar[2]);
				contract.setRefDate(fmt.parse(contractdatawithoutdollar[2]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (contractdatawithoutdollar[3].equalsIgnoreCase("na")) {
			contract.setReferedBy("");
		} else {
			contract.setReferedBy(contractdatawithoutdollar[3]);
		}

		if (contractdatawithoutdollar[4].equalsIgnoreCase("na")) {
			try {
				contract.setStartDate(fmt.parse(""));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				contract.setStartDate(fmt.parse(contractdatawithoutdollar[4]));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// if (contractdatawithoutdollar[5].equalsIgnoreCase("na")) {
		// try {
		//
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// } else {
		// try {
		//
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		if (contractendDate != null) {
			try {
				contract.setEndDate(fmt.parse(contractendDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				contract.setEndDate(fmt.parse(""));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// /**********Logic For Setting End Date by
		// duration**********************/
		// String startdate=contractdatawithoutdollar[4];
		// Calendar c=Calendar.getInstance();
		// try {
		// c.setTime(fmt.parse(startdate));
		// System.out.println("The current time is :"+fmt.format(c.getTime()));
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// int
		// totalduration=(Integer.parseInt(contractdatawithoutdollar[15])*Integer.parseInt(contractdatawithoutdollar[16]));
		// c.add(Calendar.DATE, totalduration);
		// System.out.println("The changed date is :"+fmt.format(c.getTime()));
		// contract.setEndDate(c.getTime());
		// /**********End of Logic For Setting End Date**********************/
		//

		if (contractdatawithoutdollar[5].equalsIgnoreCase("na")) {
			contract.setRefContractCount(0);
		} else {
			contract.setRefContractCount(Integer
					.parseInt(contractdatawithoutdollar[5]));
		}
		if (contractdatawithoutdollar[6].equalsIgnoreCase("na")) {
			contract.setBranch("");
		} else {
			contract.setBranch(contractdatawithoutdollar[6]);
		}
		if (contractdatawithoutdollar[7].equalsIgnoreCase("na")) {
			contract.setEmployee("");
		} else {
			contract.setEmployee(contractdatawithoutdollar[7]);
		}
		if (contractdatawithoutdollar[8].equalsIgnoreCase("na")) {
			contract.setPaymentMethod("");
		} else {
			contract.setPaymentMethod(contractdatawithoutdollar[8]);
		}
		if (contractdatawithoutdollar[9].equalsIgnoreCase("na")) {
			contract.setApproverName("");
		} else {
			contract.setApproverName(contractdatawithoutdollar[9]);
		}
		if (contractdatawithoutdollar[10].equalsIgnoreCase("na")) {
			contract.setGroup("");
		} else {
			contract.setGroup(contractdatawithoutdollar[10]);
		}
		if (contractdatawithoutdollar[11].equalsIgnoreCase("na")) {
			contract.setCategory("");
		} else {
			contract.setCategory(contractdatawithoutdollar[11]);
		}
		if (contractdatawithoutdollar[12].equalsIgnoreCase("na")) {
			contract.setType("");
		} else {
			contract.setType(contractdatawithoutdollar[12]);
		}
		if (contractdatawithoutdollar[13].equalsIgnoreCase("na")) {
			try {
				contract.setContractDate(fmt.parse(""));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				contract.setContractDate(fmt
						.parse(contractdatawithoutdollar[13].trim()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (contractdatawithoutdollar[14].equalsIgnoreCase("na")) {
			contract.setCreditPeriod(0);
		} else {
			contract.setCreditPeriod(Integer
					.parseInt(contractdatawithoutdollar[14]));
		}

		contract.setStatus(Contract.APPROVED);
		contract.setContractstatus(contractdatawithoutdollar[15]);

		/************ For Payment Terms **********************/
		List<PaymentTerms> paymentlist = new ArrayList<PaymentTerms>();
		PaymentTerms payTerms = new PaymentTerms();
		payTerms.setPayTermDays(0);
		payTerms.setPayTermPercent(100.0);
		payTerms.setPayTermComment("Default By Data Migration");
		paymentlist.add(payTerms);
		contract.setPaymentTermsList(paymentlist);

		/***************** For Product Entries ********************/

		String productdata = contractdatawithoutdollar[17];
		String pcode=contractdatawithoutdollar[18];
		String vatTax=contractdatawithoutdollar[19];
		String serviceTax=contractdatawithoutdollar[20];
		String productquantity = contractdatawithoutdollar[21];
		String productNoOfServices = contractdatawithoutdollar[22];
		String productPrice = contractdatawithoutdollar[23];
		String forproductEndDate = contractdatawithoutdollar[24];

		System.out.println("The Product data in Taskqueue is : " + productdata);

		String[] producdatasep = productdata.split("[/]");
		String[] pcodesep=pcode.split("[/]");
		String[] vatTaxsep=vatTax.split("[/]");
		String[] serviceTaxsep=serviceTax.split("[/]");
		String[] productquantitysep = productquantity.split("[/]");
		String[] productNoOfServicessep = productNoOfServices.split("[/]");
		String[] productPricesep = productPrice.split("[/]");
		String[] forProductEndDatesep = forproductEndDate.split("[*]");
		for (int i = 0; i < forProductEndDatesep.length; i++) {
			System.out.println("The end date of product are"
					+ forProductEndDatesep[i]);
		}
		List<SalesLineItem> listofsaleslineitem = new ArrayList<SalesLineItem>();
		List<ServiceProduct> listofserviceproduct=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).list();
		double totalamount = 0;
		double grandtotalamount=0;
		double netpayable=0;
		double vatprice=0;
		double serviceprice=0;
		double taxrefvariable1=0;
		double taxrefvariable2=0;
		double grandTotal=0;
		int i = 0;
		
		while (i < producdatasep.length) {
			/********amount without tax Calculation************************/
			
			totalamount=totalamount+(Double.parseDouble(productquantitysep[i])*Double.parseDouble(productPricesep[i]));
			/*******************Finished amount Without Tax Calculation*************/
			
			/**************** Logic for calculating Duration of product ********************/
			String startDate = contractdatawithoutdollar[4];
			String endDate = forProductEndDatesep[i];
			Date date1 = null;
			Date date2 = null;
			try {
				date1 = fmt.parse(startDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				date2 = fmt.parse(endDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Calendar c1 = Calendar.getInstance();
			Calendar c2 = Calendar.getInstance();
			c1.setTime(date1);
			c2.setTime(date2);
			long milliofc1 = c1.getTimeInMillis();
			long milliofc2 = c2.getTimeInMillis();
			long diffInMilli = milliofc2 - milliofc1;

			long diffInDays = diffInMilli / (1000 * 60 * 60 * 24);
			int duration = (int) diffInDays;

			/**************** Finished Logic for calculating Duration of product ********************/

			System.out.println("Products are " + i + " " + producdatasep[i]);

//			List<ServiceProduct> listofserviceproduct = ofy().load()
//					.type(ServiceProduct.class).filter("companyId", companyId)
//					.filter("refNumber2", producdatasep[i]).list();

//			System.out.println("listofserviceproduct"
//					+ listofserviceproduct.size());

			String productcode = pcodesep[i].trim();
//			String productcategory = listofserviceproduct.get(0)
//					.getProductCategory();
			String productname = producdatasep[i];

			System.out.println("The productcode is:" + productcode);
//			System.out.println("The productcategory is:" + productcategory);
			System.out.println("The productname is:" + productname);
//
//			Tax vat = listofserviceproduct.get(0).getVatTax();
////			vat.setPercentage( listofserviceproduct.get(0).getVatTax());
			String vattax=vatTaxsep[i];
			String srtax = serviceTaxsep[i];
			System.out.println("Excel Service TAx 111111111111111:"+srtax);
			Tax vat=new Tax();
			vat.setPercentage(Double.parseDouble(vattax));
			Tax tax=new Tax();
			tax.setPercentage(Double.parseDouble(srtax));
			System.out.println("Double.parseDouble(srtax)111111111"+Double.parseDouble(srtax));
			SalesLineItem item = new SalesLineItem();
			
			ServiceProduct serviceprod=new ServiceProduct();
			serviceprod.setProductName(productname);
			item.setPrduct(serviceprod);

			/*********************This part is for product id**********************************/
			int productcount=0;
			
			for (int j = 0; j < listofserviceproduct.size(); j++) {
				System.out.println("listofserviceproduct.get(j).getProductCode().trim()"+listofserviceproduct.get(j).getProductCode().trim());
				System.out.println("listofserviceproduct.get(j).getCount()"+listofserviceproduct.get(j).getCount());
				if(listofserviceproduct.get(j).getProductCode().trim().equalsIgnoreCase(productcode)){
					productcount=listofserviceproduct.get(j).getCount();
					System.out.println("Matched Data");
				}
			}
			
			logger.log(Level.SEVERE, "productcount"+productcount );
			System.out.println("productcount"+productcount);
			
			
			/************************This part is for product id till here***********************/
			
			if(productcount!=0){
				System.out.println("productcount"+productcount);
				item.getPrduct().setCount(productcount);;
				logger.log(Level.SEVERE, "Product Mathched and count executate");
				System.out.println("Product Mathched and count executate");
			}
			item.setProductName(productname);
			item.setProductCode(productcode);
//			item.setProductCategory(productcategory);
			item.setDuration(duration);
			item.setQuantity(Double.parseDouble(productquantitysep[i]));
			item.setNumberOfService(Integer.parseInt(productNoOfServicessep[i]));
			item.setPrice(Double.parseDouble(productPricesep[i]));
			item.setStartDate(new Date());
//			item.setVatTax(vat);
			item.setVatTax(vat);
			item.setServiceTax(tax);
			
			listofsaleslineitem.add(item);
			i++;
			
		}
		contract.setItems(listofsaleslineitem);
		contract.setTotalAmount(totalamount);
		
//		/***************** Logic for Calculating Taxes Table ********************/
//		double totalamount2=0;
//		for (int j = 0; j < listofsaleslineitem.size(); j++) {
//			totalamount2=totalamount2+((listofsaleslineitem.get(j).getQty())*(listofsaleslineitem.get(j).getPrice()));
//			
//		}
		double servicetaxfortable=0d;
		double assvalforvat=0d;
//		Productta
		
//		List<ProductOtherCharges> listoftaxtable2=new ArrayList<ProductOtherCharges>();
//		ProductOtherCharges taxtable2=new ProductOtherCharges();
//			taxtable2.setChargeName("VAT TAX");
//			taxtable2.setChargePercent(0.0);
//			taxtable2.setAssessableAmount(totalamount2);
//			taxtable2.setChargePayable(0d);
//			listoftaxtable2.add(taxtable2);
//			System.out.println("Added 1st time");
//			contract.getProductTaxes().addAll(listoftaxtable2);
/***********************Vat TAx******************************************/
		List<ProductOtherCharges> listofvattaxtable=new ArrayList<ProductOtherCharges>();
		double totalvatpay=0d;
		List<Double> listvatpercent=new ArrayList<Double>();
		for (int j = 0; j < listofsaleslineitem.size(); j++) {
			listvatpercent.add(Double.parseDouble(listofsaleslineitem.get(j).getVatTax()+""));
		}
		HashSet<Double> uniquevat=new HashSet<Double>();
		List<Double> hashvatllist=new ArrayList<Double>();
		for (int j = 0; j < listofsaleslineitem.size(); j++) {
			uniquevat.add(Double.parseDouble(listofsaleslineitem.get(j).getVatTax()+""));
			
		}
		hashvatllist.addAll(uniquevat);
		for (int j = 0; j < hashvatllist.size(); j++) {
			System.out.println("HashSet Loop of vat"+hashvatllist.get(j));
			assvalforvat=0d;
			for (int j2 = 0; j2 < listvatpercent.size(); j2++) {
				if(hashvatllist.get(j).equals(listvatpercent.get(j2))){
					System.out.println("Ass-Val Increased");
					assvalforvat=assvalforvat+((listofsaleslineitem.get(j2).getQty())*(listofsaleslineitem.get(j2).getPrice()));
					System.out.println("assvalforvat"+assvalforvat);
				}	
			}
			System.out.println("Setting Vat Data");
			ProductOtherCharges taxtableforvat=new ProductOtherCharges();
			taxtableforvat.setChargeName("VAT");
			taxtableforvat.setChargePercent(hashvatllist.get(j));
			taxtableforvat.setAssessableAmount(assvalforvat);
			double pay=(assvalforvat/100)*hashvatllist.get(j);
			totalvatpay=totalvatpay+pay;
			taxtableforvat.setChargePayable(pay);
			listofvattaxtable.add(taxtableforvat);
		}
		contract.getProductTaxes().addAll(listofvattaxtable);
		
		System.out.println("Total Vat Pay is :"+totalvatpay);
			/***********************Finished with Vat TAx******************************************/
						
		/******************************Service Tax********************/
		double assval=0d;
		double totalassessamount=0d;
		double totalservicepay=0d;
		double assvalsrcal=0d;
		List<ProductOtherCharges> listoftaxtable=new ArrayList<ProductOtherCharges>();
		System.out.println("listofsaleslineitem.size()"+listofsaleslineitem.size());
		
		List<Double> listservicepercent=new ArrayList<Double>();
		for (int j = 0; j < listofsaleslineitem.size(); j++) {
			listservicepercent.add(Double.parseDouble(listofsaleslineitem.get(j).getServiceTax()+""));
		}
		System.out.println("listofsaleslineitem.size()"+listofsaleslineitem.size());
		HashSet<Double> uniquetax=new HashSet<Double>();
		List<Double> hashllist=new ArrayList<Double>();
		for (int j = 0; j < listofsaleslineitem.size(); j++) {
			uniquetax.add(Double.parseDouble(listofsaleslineitem.get(j).getServiceTax()+""));
			
		}
		hashllist.addAll(uniquetax);
		for (int j = 0; j < hashllist.size(); j++) {
			System.out.println("HashSet Loop"+hashllist.get(j));
			assval=0d;
			for (int j2 = 0; j2 < listservicepercent.size(); j2++) {
				System.out.println("Run for"+j2);
				System.out.println(listservicepercent.get(j2));
				if(hashllist.get(j).equals(listservicepercent.get(j2))){
					System.out.println("Ass-Val Increased");
					System.out.println("totalvatpay/hashllist.size()"+totalvatpay/hashllist.size());
					double vatTaxValue=((((listofsaleslineitem.get(j2).getQty())*(listofsaleslineitem.get(j2).getPrice()))/100)*Double.parseDouble(listofsaleslineitem.get(j2).getVatTax()+""));
					System.out.println("vatTaxValue"+vatTaxValue);
					assval=assval+((listofsaleslineitem.get(j2).getQty())*(listofsaleslineitem.get(j2).getPrice()))+vatTaxValue;
					System.out.println("assval"+assval);
				}
			}
			System.out.println("Setting the service data");
			ProductOtherCharges taxtable=new ProductOtherCharges();
			taxtable.setChargeName("Service Tax");
			taxtable.setChargePercent(hashllist.get(j));
			assvalsrcal=assval;
			taxtable.setAssessableAmount(assvalsrcal);
			totalassessamount=totalassessamount+assvalsrcal;
			double pay=(assvalsrcal/100)*hashllist.get(j);
			totalservicepay=totalservicepay+pay;
			taxtable.setChargePayable(pay);
			listoftaxtable.add(taxtable);
		}
		
		contract.getProductTaxes().addAll(listoftaxtable);
		/**************** Finished With Calculating Taxes Table**********************/
//		System.out.println("listofsaleslineitem" + listofsaleslineitem.size());
//		totalamount=totalamount+(Double.parseDouble(productquantitysep[i])*Double.parseDouble(productPricesep[i]));
//		taxrefvariable1=((Double.parseDouble(productquantitysep[i])*Double.parseDouble(productPricesep[i]))/100);
//		System.out.println("Tax Ref Variable:"+taxrefvariable1);
////		vatprice=taxrefvariable1*(Double.parseDouble(vatTaxsep[i]));
//		serviceprice=taxrefvariable1*(Double.parseDouble(serviceTaxsep[i]));
//		
//		grandtotalamount=grandtotalamount+serviceprice;
//		
//		
		
		contract.setGrandTotal(totalassessamount+totalservicepay);
		contract.setNetpayable(totalassessamount+totalservicepay);
		
		contract.setCount(Integer.parseInt(contractdatawithoutdollar[25]));
		contract.setCompanyId(companyId);
		/************* Temporary Part ***********************/
//		contract.setNetpayable(10010d);
		/***********************************************/
		System.out.println("contract last me before saving"+contract);
		
		ofy().save().entity(contract).now();
		System.out.println("Done With Task Queue Call");
	}
}
