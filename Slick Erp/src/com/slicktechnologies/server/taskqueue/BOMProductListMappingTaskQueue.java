package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

public class BOMProductListMappingTaskQueue  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4856508075374673411L;
	Logger logger=Logger.getLogger("BOMProductListMappingTaskQueue logger");
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		logger.log(Level.SEVERE,"Welcome to Services Task Queue");
		
		String processName = req.getParameter("processName"); //minMapping
		
		if(processName.equals("minMapping")){
			String minObj  = req.getParameter("minObject");
			Gson gson = new Gson();
			
			
			JSONObject jsonCust =null;
			try {
				jsonCust = new JSONObject(minObj.trim());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			MaterialIssueNote minDetails = gson.fromJson(jsonCust.toString(), MaterialIssueNote.class);
			
			minProListMapping(minDetails,null);
		}else if(processName.equals("mmnMapping")){
			String minObj  = req.getParameter("mmnObject");
			Gson gson = new Gson();
			
			
			JSONObject jsonCust =null;
			try {
				jsonCust = new JSONObject(minObj.trim());
				logger.log(Level.SEVERE,"mmn obj -- " + jsonCust);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			MaterialMovementNote minDetails = gson.fromJson(jsonCust.toString(), MaterialMovementNote.class);
			
			getMrnUpdateProductList(minDetails,null,null);
		}
		
	}
	
	public Service minProListMapping(MaterialIssueNote min,Service serObj){

		if(min!=null){
			if(serObj == null){
			 serObj  = ofy().load().type(Service.class).filter("companyId", min.getCompanyId())
					.filter("count", min.getServiceId()).filter("contractCount", min.getMinSoId()).first().now();
			}
			logger.log(Level.SEVERE,"get servicce object -- " + serObj.getCount());
			if(serObj!=null){
				
				if(serObj.getServiceProductList()!=null && serObj.getServiceProductList().size()>0){
					
					if(min.getSubProductTablemin().size()>0){
						ArrayList<Integer> minPro = new ArrayList<Integer>();
						
						for(int j=0;j<serObj.getServiceProductList().size();j++){
							minPro.add(serObj.getServiceProductList().get(j).getProduct_id());
						}
						
						List<ItemProduct> itemProList = ofy().load().type(ItemProduct.class).filter("companyId", min.getCompanyId())
								.filter("count IN", minPro).list();
						logger.log(Level.SEVERE,"get ser itm -- " + itemProList);
					List<UnitConversion> prodUnitConverList =ofy().load().type(UnitConversion.class).filter("companyId", min.getCompanyId()).filter("unitCode",47).list();
						
						for(int j = 0 ; j < min.getSubProductTablemin().size() ; j++){
							for(int i = 0 ; i < serObj.getServiceProductList().size() ; i++){
								if(min.getSubProductTablemin().get(j).getMaterialProductId() 
										== serObj.getServiceProductList().get(i).getProduct_id()){
									logger.log(Level.SEVERE,"ser pro id  -- " +serObj.getServiceProductList().get(i).getProduct_id() );
									serObj.getServiceProductList().get(i).setIssueCode(min.getSubProductTablemin().get(j).getMaterialProductCode());
									serObj.getServiceProductList().get(i).setIssueProduct_id(min.getSubProductTablemin().get(j).getMaterialProductId());
									serObj.getServiceProductList().get(i).setIssueName(min.getSubProductTablemin().get(j).getMaterialProductName());
									serObj.getServiceProductList().get(i).setIssueStorageBin(min.getSubProductTablemin().get(j).getMaterialProductStorageBin());
									serObj.getServiceProductList().get(i).setIssueStorageLocation(min.getSubProductTablemin().get(j).getMaterialProductStorageLocation());
									serObj.getServiceProductList().get(i).setIssueWarehouse(min.getSubProductTablemin().get(j).getMaterialProductWarehouse());
									serObj.getServiceProductList().get(i).setIssueQuantity(min.getSubProductTablemin().get(j).getMaterialProductRequiredQuantity());
									serObj.getServiceProductList().get(i).setIssueUnit(min.getSubProductTablemin().get(j).getMaterialProductUOM());
								
									
									serObj.getServiceProductList().get(i).setActulCode(min.getSubProductTablemin().get(j).getMaterialProductCode());
									serObj.getServiceProductList().get(i).setActulName(min.getSubProductTablemin().get(j).getMaterialProductName());
									serObj.getServiceProductList().get(i).setActulProduct_id(min.getSubProductTablemin().get(j).getMaterialProductId());
									serObj.getServiceProductList().get(i).setActulStorageBin(min.getSubProductTablemin().get(j).getMaterialProductStorageBin());
									serObj.getServiceProductList().get(i).setActulStorageLocation(min.getSubProductTablemin().get(j).getMaterialProductStorageLocation());
									serObj.getServiceProductList().get(i).setActulWarehouse(min.getSubProductTablemin().get(j).getMaterialProductWarehouse());
									serObj.getServiceProductList().get(i).setActulQuantity(min.getSubProductTablemin().get(j).getMaterialProductRequiredQuantity());
									serObj.getServiceProductList().get(i).setActulUnit(min.getSubProductTablemin().get(j).getMaterialProductUOM());
								
									
									double upPrice  = getPriceConversion(min.getSubProductTablemin().get(j).getMaterialProductUOM()
											,min.getSubProductTablemin().get(j).getMaterialProductRequiredQuantity(),
											serObj.getServiceProductList().get(i).getActulUnit(),
											min.getSubProductTablemin().get(j).getMaterialProductCode()
											,itemProList,prodUnitConverList);
									serObj.getServiceProductList().get(i).setActualPrice(upPrice);
									serObj.getServiceProductList().get(i).setIssuePrice(upPrice);
									
									logger.log(Level.SEVERE,serObj.getServiceProductList().get(i).getProduct_id() +"   get service list updatedd Min .."+upPrice);
									break;
								}
							}
						}
					}
					
					logger.log(Level.SEVERE,"get service list updatedd Min ..");
					ofy().save().entity(serObj);
					
				}
				
			}
			
		}
		return  serObj;
	}

	
	public double getPriceConversion(String proGroupUnit,double qty,String convertedUnit,String proCode,List<ItemProduct> proGroupItemList,List<UnitConversion>	prodUnitConverList){
		double upPrice = 1;
		
		
		ArrayList<Integer> proID = new ArrayList<Integer>();
				for(ItemProduct itm : proGroupItemList){
					if(itm.getProductCode().equals(proCode)){
						upPrice = convertItemProPrice(itm,proGroupUnit,qty,convertedUnit,proCode,prodUnitConverList);
						break;
					}
				}
				
			
		
		return upPrice;
	}
	
	public double convertItemProPrice(ItemProduct item,String prGroupUnit,double qty,String convertedUnit,String proCode,List<UnitConversion>	prodUnitConverList){
		double price = 1;
//			if(convertedUnit.equals(prGroupUnit)){
				price = item.getPrice() * qty;
				/*}else{
				for(UnitConversion unit : prodUnitConverList){
					if(unit.getUnit().equals(prGroupUnit) && item.getUnitOfMeasurement().equals(prGroupUnit)){
						for(UnitConversionDetailList unitDt : unit.getUnitConversionList()){
							if(unitDt.getMinUnit().equals(prGroupUnit) && unitDt.getMaxUnit().equals(convertedUnit) ){
								price = (qty * item.getPrice()) / unitDt.getMaxValue(); 
							}
						}
					}
				}
			}*/
			
		return price;
	}
	
	
	public Service getMrnUpdateProductList(MaterialMovementNote mmn,MaterialIssueNote min,Service serObj){

		
		if(mmn!=null){
			if(min==null){
				logger.log(Level.SEVERE,"mmn id -- " +  mmn.getCompanyId());
				logger.log(Level.SEVERE,"count id -- " +  mmn.getMmnMinId());
				logger.log(Level.SEVERE,"minSoId id -- " +  mmn.getOrderID());
				int minOrderid = Integer.parseInt(mmn.getOrderID().trim());
				min =  ofy().load().type(MaterialIssueNote.class).filter("companyId", mmn.getCompanyId())
						.filter("count",mmn.getMmnMinId()).filter("minSoId", minOrderid).first().now();
				logger.log(Level.SEVERE,"get min id -- " + min.getCount());
			}
			 	
			
			if(serObj==null && min != null){
				logger.log(Level.SEVERE,"get min id -- " + min.getCount());
				serObj  = ofy().load().type(Service.class).filter("companyId", min.getCompanyId())
						.filter("count", min.getServiceId()).filter("contractCount", min.getMinSoId()).first().now();
				
			}
			
			
			logger.log(Level.SEVERE,"get servicce object -- " + serObj.getCount());
			if(serObj!=null && min !=null){
				
				if(serObj.getServiceProductList()!=null && serObj.getServiceProductList().size()>0){
					
					if(min.getSubProductTablemin().size()>0){
						ArrayList<Integer> minPro = new ArrayList<Integer>();

						for(int j=0;j<serObj.getServiceProductList().size();j++){
							minPro.add(serObj.getServiceProductList().get(j).getProduct_id());
						}
						
						List<ItemProduct> itemProList = ofy().load().type(ItemProduct.class).filter("companyId", min.getCompanyId())
								.filter("count IN", minPro).list();

					List<UnitConversion> prodUnitConverList =ofy().load().type(UnitConversion.class).filter("companyId", min.getCompanyId()).filter("unitCode",47).list();
					String transaction=mmn.getTransDirection().trim();
					String operation = null;
					if(transaction.equals("IN")){
						 operation=AppConstants.ADD;
					}else if(transaction.equals("OUT")){
						operation=AppConstants.SUBTRACT;
					}else if(transaction.equals("TRANSFEROUT")){
						operation=AppConstants.SUBTRACT;
					}
						for(int j = 0 ; j < mmn.getSubProductTableMmn().size() ; j++){
							for(int i = 0 ; i < serObj.getServiceProductList().size() ; i++){
								if(mmn.getSubProductTableMmn().get(j).getMaterialProductId() 
										== serObj.getServiceProductList().get(i).getProduct_id() 
									&& mmn.getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity()>0){
								
									serObj.getServiceProductList().get(i).setActulCode(min.getSubProductTablemin().get(j).getMaterialProductCode());
									serObj.getServiceProductList().get(i).setActulName(min.getSubProductTablemin().get(j).getMaterialProductName());
									serObj.getServiceProductList().get(i).setActulProduct_id(min.getSubProductTablemin().get(j).getMaterialProductId());
									
									if(transaction.equals("IN")){
										double unit  = 	serObj.getServiceProductList().get(i).getActulQuantity();
										
										
										unit = serObj.getServiceProductList().get(i).getActulQuantity() - mmn.getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity();
										if(unit>0){
											serObj.getServiceProductList().get(i).setActulQuantity(unit);
										}
									}
									if(transaction.equals("OUT")){
										double unit  = 	serObj.getServiceProductList().get(i).getActulQuantity();
										unit = serObj.getServiceProductList().get(i).getActulQuantity() + mmn.getSubProductTableMmn().get(j).getMaterialProductRequiredQuantity();
										if(unit>0){
											serObj.getServiceProductList().get(i).setActulQuantity(unit);
										}
									}
									serObj.getServiceProductList().get(i).setActulUnit(min.getSubProductTablemin().get(j).getMaterialProductUOM());
								
									
									double upPrice  = getPriceConversion(min.getSubProductTablemin().get(j).getMaterialProductUOM()
											,serObj.getServiceProductList().get(i).getActulQuantity(),
											serObj.getServiceProductList().get(i).getActulUnit(),
											min.getSubProductTablemin().get(j).getMaterialProductCode()
											,itemProList,prodUnitConverList);
									serObj.getServiceProductList().get(i).setActualPrice(upPrice);
									
									break;
								}
							}
						}
					}
					
					logger.log(Level.SEVERE,"get service list updatedd Min ..");
					ofy().save().entity(serObj);
					
				}
				
			}
			
		}
		
		return serObj;
	}
}
