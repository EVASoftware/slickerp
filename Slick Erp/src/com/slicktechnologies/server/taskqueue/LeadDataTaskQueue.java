package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class LeadDataTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9163355186737772950L;
	
	
	List<Lead> leadObj = new ArrayList<Lead>();
	List<Quotation> quotationObj = new ArrayList<Quotation>();
	List<Contract> contractObj = new ArrayList<Contract>();
	
	Logger logger = Logger.getLogger("Name Of logger");
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		logger.log(Level.SEVERE, "Welcome to Lead Data Task Queue");
		String companyIdString = req.getParameter("LeadDataValuekey");
		
		logger.log(Level.SEVERE, "companyIdString value  " +companyIdString);
		
		long companyId=Long.parseLong(companyIdString);
		
		List<ServiceProduct> serviceProd =  new ArrayList<ServiceProduct>();
		serviceProd =ofy().load().type(ServiceProduct.class).filter("companyId",companyId).list();
		logger.log(Level.SEVERE, "serviceProd list size  " +serviceProd.size());
		
		// creating lead data 
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Lead").first().now();
		long lastNumber = numbergen.getNumber();
    	 int count = (int) lastNumber;
    	 
		List<Lead> leadData= createLeadData(companyId,serviceProd,count);
			
		//   creating quotation data 
		NumberGeneration numbergenQuotation = null;	
		numbergenQuotation = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Quotation").first().now();
		long lastNumbeQuotaion = numbergenQuotation.getNumber();
    	int countQuotation = (int) lastNumbeQuotaion;
	
    	List<Quotation> quotationList= createQuotationData(companyId,serviceProd,countQuotation);
		
		
		//   creating contract data 
		NumberGeneration numbergenContract = null;	
		numbergenContract = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Contract").first().now();
		long lastNumbeContract = numbergenContract.getNumber();
    	int countContract = (int) lastNumbeContract;
		
    	List<Contract> contractList= createContractData(companyId,serviceProd,countContract);
		
		
    	//   creating Billing data 
    	NumberGeneration numbergenbilling = null;	
    	numbergenbilling = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Contract").first().now();
		long lastNumbebilling = numbergenbilling.getNumber();
    	int countBilling = (int) lastNumbebilling;
		
    	List<BillingDocument> BillingList= createBillingData(companyId,serviceProd,countBilling);
    	
//		createInvoiceData(companyId);
//		createpaymentData(companyId);
		
		
		
			//  Lead saving completed  
			ofy().save().entities(leadData);
			 numbergen.setNumber(count+3);
		     GenricServiceImpl impl = new GenricServiceImpl();
			 impl.save(numbergen);
		
			 
			 //   all quotation data save here 
			 ofy().save().entities(quotationList);
			 numbergenQuotation.setNumber(count+2);
		     GenricServiceImpl implquotation = new GenricServiceImpl();
		     implquotation.save(numbergenQuotation);
		     
		     
		     //   all Contract data save here 
			 ofy().save().entities(contractList);
			 numbergenQuotation.setNumber(count+3);
		     GenricServiceImpl implContract = new GenricServiceImpl();
		     implContract.save(numbergenQuotation);
		     
	}
		
		private List<BillingDocument> createBillingData(long companyId,
			List<ServiceProduct> serviceProd, int countBilling) {
		return null;
	}


		private List<Contract> createContractData(long companyId, List<ServiceProduct> serviceProd, int count) {
		
			Contract con = createContractWithCreatedStatus(companyId,count+1,serviceProd);
			contractObj.add(con);
			
			Contract con1=CreateContractWithApprovedStatus(companyId,count+2,serviceProd);
			contractObj.add(con1);
			
			Contract con2=CreateContractWithRateContract(companyId,count+3,serviceProd);
			contractObj.add(con2);
			
			return contractObj;
		}
		
		
		private Contract CreateContractWithRateContract(long companyId, int i,List<ServiceProduct> serviceProd) {
		
			Contract con = new Contract();
			con.setApproverName("Vedant Kamble");
			con.setApprovalDate(new Date());
			con.setBranch("Andheri");
			con.setCategory("RESIDENCIAL");
			con.setCompanyId(companyId);
			con.setCount(i);
			con.setCreatedBy("Vedant Kamble");
			con.setCreationDate(new Date());
			con.setQuotationDate(new Date());
			con.setContractRate(true);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
		    cal.add(Calendar.DATE, 15);
		    Date date = cal.getTime();
		    con.setValidUntill(date);
		    con.setDescription("This is Contrcat With Rate Contract");
		    con.setEmployee("Rohan Bhagde");
			
			List<SalesLineItem> sproductList= new ArrayList<SalesLineItem>(); 
			for (int j = 0; j < serviceProd.size(); j++) {
			
			SalesLineItem items = new SalesLineItem();
			items.setArea("NA");
			items.setProductSrNo(j+1);
			items.setPrduct(serviceProd.get(j));
			items.setProductCategory(serviceProd.get(j).getProductCategory());
			items.setProductName(serviceProd.get(j).getProductName());
			items.setProductCode(serviceProd.get(j).getProductCode());
			items.setQuantity(Double.parseDouble("1"));
			items.setNumberOfService(serviceProd.get(j).getNumberOfServices());
			items.setDuration(serviceProd.get(j).getDuration());
			items.setPrice(serviceProd.get(j).getPrice());
			sproductList.add(items);
			
			if(j==1)
			{
				break;
			}
			}			
			
			con.setItems(sproductList);
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			con.setCinfo(personInfo);
			con.setPriority("Medium");
			con.setRemark("");
			con.setStatus("Approved");
			con.setType("1 BHK FLAT");
			con.setUserId("admin123");	
			con.setPaymentMethod("Cash");
			
			return con;
		}

		private Contract CreateContractWithApprovedStatus(long companyId,int i, List<ServiceProduct> serviceProd) {
			
			Contract con = new Contract();
			
			con.setApproverName("Vedant Kamble");
			con.setApprovalDate(new Date());
			con.setBranch("Andheri");
			con.setCategory("RESIDENCIAL");
			con.setCompanyId(companyId);
			con.setCount(i);
			con.setCreatedBy("Vedant Kamble");
			con.setCreationDate(new Date());
			con.setQuotationDate(new Date());
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
		    cal.add(Calendar.DATE, 15);
		    Date date = cal.getTime();
		    con.setValidUntill(date);
		    con.setDescription("This is Contract for Residencial Client with Approved Status");
		    con.setEmployee("Rohan Bhagde");
			
			List<SalesLineItem> sproductList= new ArrayList<SalesLineItem>(); 
			for (int j = 0; j < serviceProd.size(); j++) {
			
			SalesLineItem items = new SalesLineItem();
			items.setArea("NA");
			items.setProductSrNo(j+1);
			items.setPrduct(serviceProd.get(j));
			items.setProductCategory(serviceProd.get(j).getProductCategory());
			items.setProductName(serviceProd.get(j).getProductName());
			items.setProductCode(serviceProd.get(j).getProductCode());
			items.setQuantity(Double.parseDouble("1"));
			items.setNumberOfService(serviceProd.get(j).getNumberOfServices());
			items.setDuration(serviceProd.get(j).getDuration());
			items.setPrice(serviceProd.get(j).getPrice());
			sproductList.add(items);
			
			if(j==1)
			{
				break;
			}
			}			
			
			con.setItems(sproductList);
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			con.setCinfo(personInfo);
			con.setPriority("Medium");
			con.setRemark("");
			con.setStatus("Approved");
			con.setType("1 BHK FLAT");
			con.setUserId("admin123");	
			con.setPaymentMethod("Cash");
			
			return con;
		}

		private Contract createContractWithCreatedStatus(long companyId, int i,List<ServiceProduct> serviceProd) {
		
			Contract con = new Contract();
		
			con.setApproverName("Vedant Kamble");
			con.setApprovalDate(new Date());
			con.setBranch("Andheri");
			con.setCategory("RESIDENCIAL");
			con.setCompanyId(companyId);
			con.setCount(i);
			con.setCreatedBy("Vedant Kamble");
			con.setCreationDate(new Date());
			con.setQuotationDate(new Date());
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
		    cal.add(Calendar.DATE, 15);
		    Date date = cal.getTime();
		    con.setValidUntill(date);
		    con.setDescription("This is Contract for Residencial Client with Approved Status");
		    con.setEmployee("Rohan Bhagde");
			
			List<SalesLineItem> sproductList= new ArrayList<SalesLineItem>(); 
			for (int j = 0; j < serviceProd.size(); j++) {
			
			SalesLineItem items = new SalesLineItem();
			items.setArea("NA");
			items.setProductSrNo(j+1);
			items.setPrduct(serviceProd.get(j));
			items.setProductCategory(serviceProd.get(j).getProductCategory());
			items.setProductName(serviceProd.get(j).getProductName());
			items.setProductCode(serviceProd.get(j).getProductCode());
			items.setQuantity(Double.parseDouble("1"));
			items.setNumberOfService(serviceProd.get(j).getNumberOfServices());
			items.setDuration(serviceProd.get(j).getDuration());
			items.setPrice(serviceProd.get(j).getPrice());
			sproductList.add(items);
			
			if(j==1)
			{
				break;
			}
			}			
			
			con.setItems(sproductList);
			
			
			//   service schedule 
			
			 List<ServiceSchedule> popuplist=reactfordefaultTable(sproductList);
			 con.setServiceScheduleList(popuplist);
			 
			//  ends here
			
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			con.setCinfo(personInfo);
			con.setPriority("Medium");
			con.setRemark("");
			con.setStatus("Created");
			con.setType("1 BHK FLAT");
			con.setUserId("admin123");	
			con.setPaymentMethod("Cash");
			
//		    ofy().save().entity(quot);
//			logger.log(Level.SEVERE, "Lead Dat saved successfully");
			GeneralServiceImpl impl = new GeneralServiceImpl();
			return con;
		}

	
		
		private List<ServiceSchedule> reactfordefaultTable(List<SalesLineItem> sproductList) {

			
			List<SalesLineItem> salesitemlis=new ArrayList<SalesLineItem>();
			salesitemlis.addAll(sproductList);
			System.out.println("@@@@@@@@ PRODUCT LIST SIZE ::: "+salesitemlis.size());
			ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
			serschelist.clear();
			
			for(int i=0;i<salesitemlis.size();i++){
			
				int qty=0;
				qty=(int) salesitemlis.get(i).getQty();
				for(int k=0;k < qty;k++){
			
				 	
				long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
				int noOfdays=salesitemlis.get(i).getDuration();
				int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
				Date servicedate=new Date();
				Date d=new Date(servicedate.getTime());
				Date productEndDate= new Date(servicedate.getTime());
				CalendarUtil.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate=new Date(productEndDate.getTime());
				productEndDate=prodenddate;
				Date tempdate=new Date();
				
				for(int j=0;j<noServices;j++){
					if(j>0)
					{
						CalendarUtil.addDaysToDate(d, interval);
						tempdate=new Date(d.getTime());
					}
					
					ServiceSchedule ssch=new ServiceSchedule();
					Date Stardaate=new Date();
					
					//  rohan added this 1 field 
					System.out.println("In side method RRRRRRRRRRRRR"+salesitemlis.get(i).getProductSrNo());
					ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
					
					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
					ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
					ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
					ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
					ssch.setScheduleServiceNo(j+1);
					ssch.setScheduleProdStartDate(Stardaate);
					ssch.setScheduleProdEndDate(productEndDate);
					ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
					/**
					 *   rohan commented this code for NBHC 
					 *   Purpose : wanetd to set servicing branch as customer branch is selected  
					 */
//					ssch.setServicingBranch(olbbBranch.getValue());
					/**
					 * ends here 
					 */
					
					System.out.println("SERVICING TIME ::: "+salesitemlis.get(i).getServicingTIme());
//					System.out.println("BRANCH ::: "+olbbBranch.getValue());
					ssch.setScheduleServiceTime("Flexible");
					
					if(j==0){
						ssch.setScheduleServiceDate(servicedate);
					}
					if(j!=0){
						if(productEndDate.before(d)==false){
							ssch.setScheduleServiceDate(tempdate);
						}else{
							ssch.setScheduleServiceDate(productEndDate);
						}
					}
					
//					if(customerbranchlist.size()==qty){
						ssch.setScheduleProBranch("Andheri");
						/**
						 * rohan added this code for automatic servicing branch setting for NBHC 
						 */
						
//						System.out.println("reactfordefaultTable cust Branch Name "+customerbranchlist.get(k).getBusinessUnitName());
//						System.out.println("servicing Branch "+customerbranchlist.get(k).getBranch());
//						if(customerbranchlist.get(k).getBranch()!=null)
//						{
//							ssch.setServicingBranch("Andheri");
//						}
//						else
//						{
							ssch.setServicingBranch("Andheri");
//						}
						/**
						 * ends here 
						 */
//					}else if(customerbranchlist.size()==0){
//						ssch.setScheduleProBranch("Service Address");
//						/**
//						 * rohan added this code for automatic servicing branch setting for NBHC 
//						 */
//							ssch.setServicingBranch("Andheri");
//						/**
//						 * ends here 
//						 */	
//							
//					}else{
//						ssch.setScheduleProBranch(null);
//						
//					}
					ssch.setScheduleServiceDay(serviceDay(ssch.getScheduleServiceDate()));
					serschelist.add(ssch);
				}
			}
			}
			
			
			//  rohan added this for testing 
			
			for (int l = 0; l < serschelist.size(); l++)
			{
				System.out.println("My Rohan in service schedule table "+serschelist.get(l).getSerSrNo());
			}
			
			
			return serschelist;
		}
		
		
		
		public static String serviceDay(Date d){
			String day=null;
			DateTimeFormat format = DateTimeFormat.getFormat("c"); 
			String dayOfWeek = format.format(d);
			if(dayOfWeek.equals("0"))
			   return day="Sunday";
			if(dayOfWeek.equals("1"))
				return day="Monday";
			if(dayOfWeek.equals("2"))
				return day="Tuesday";
			if(dayOfWeek.equals("3"))
				return day="Wednesday";
			if(dayOfWeek.equals("4"))
				return day="Thursday";
			if(dayOfWeek.equals("5"))
				return day="Friday";
			if(dayOfWeek.equals("6"))
				return day="Saturday";
			
			return day;
		}

//   this is uded for creating quotation data 
		
		private List<Quotation> createQuotationData(long companyId, List<ServiceProduct> serviceProd, int count) {
		
			
			Quotation quot = createQuotationWithCreatedStatus(companyId,count+1,serviceProd);
			quotationObj.add(quot);
			
			Quotation quot1=CreateQuotationWithApprovedStatus(companyId,count+2,serviceProd);
			quotationObj.add(quot1);
			
			return quotationObj;
		}
		
		
		
		
		
		

		private Quotation CreateQuotationWithApprovedStatus(long companyId, int i, List<ServiceProduct> serviceProd) {
			
			Quotation quot = new Quotation();
			quot.setApproverName("Vedant Kamble");
			quot.setApprovalDate(new Date());
			
			quot.setBranch("Andheri");
			quot.setCategory("RESIDENCIAL");
			quot.setCompanyId(companyId);
			quot.setCount(i);
			quot.setCreatedBy("Vedant Kamble");
			quot.setCreationDate(new Date());
			quot.setQuotationDate(new Date());
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
		    cal.add(Calendar.DATE, 15);
		    Date date = cal.getTime();
			quot.setValidUntill(date);
			quot.setDescription("This is Quotation for Residencial Client with Approved Status");
			quot.setEmployee("Rohan Bhagde");
			
			
			List<SalesLineItem> sproductList= new ArrayList<SalesLineItem>(); 
			for (int j = 0; j < serviceProd.size(); j++) {
			
			SalesLineItem items = new SalesLineItem();
			items.setArea("NA");
			items.setProductSrNo(j+1);
			items.setPrduct(serviceProd.get(j));
			items.setProductCategory(serviceProd.get(j).getProductCategory());
			items.setProductName(serviceProd.get(j).getProductName());
			items.setProductCode(serviceProd.get(j).getProductCode());
			items.setQuantity(Double.parseDouble("1"));
			items.setNumberOfService(serviceProd.get(j).getNumberOfServices());
			items.setDuration(serviceProd.get(j).getDuration());
			items.setPrice(serviceProd.get(j).getPrice());
			sproductList.add(items);
			
			if(j==1)
			{
				break;
			}
			}			
			
			
			quot.setItems(sproductList);
			
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			quot.setCinfo(personInfo);
			quot.setPriority("Medium");
			quot.setRemark("");
			quot.setStatus("Approved");
			quot.setType("1 BHK FLAT");
			quot.setUserId("admin123");	
			quot.setPaymentMethod("Cash");
			
//		    ofy().save().entity(quot);
//			logger.log(Level.SEVERE, "Lead Dat saved successfully");
			
			return quot;
		}

		private Quotation createQuotationWithCreatedStatus(long companyId, int i, List<ServiceProduct> serviceProd) {
			
			Quotation quot = new Quotation();
			quot.setApproverName("");
			quot.setApprovalDate(null);
			
			quot.setBranch("Andheri");
			quot.setCategory("RESIDENCIAL");
			quot.setCompanyId(companyId);
			quot.setCount(i);
			quot.setCreatedBy("Vedant Kamble");
			quot.setCreationDate(new Date());
			quot.setQuotationDate(new Date());
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
		    cal.add(Calendar.DATE, 15);
			
		    Date date = cal.getTime();
		    
			quot.setValidUntill(date);
			quot.setDescription("This is Quotation for Residencial Client with created status");
			quot.setEmployee("Rohan Bhagde");
			
			List<SalesLineItem> sproductList=null; 
			for (int j = 0; j < serviceProd.size(); j++) {
		
			sproductList = new ArrayList<SalesLineItem>();
			SalesLineItem items = new SalesLineItem();
			items.setArea("NA");
			items.setProductSrNo(j+1);
			items.setPrduct(serviceProd.get(j));
			items.setProductCategory(serviceProd.get(j).getProductCategory());
			items.setProductName(serviceProd.get(j).getProductName());
			items.setProductCode(serviceProd.get(j).getProductCode());
			items.setQuantity(Double.parseDouble("1"));
			items.setNumberOfService(serviceProd.get(j).getNumberOfServices());
			items.setDuration(serviceProd.get(j).getDuration());
			items.setPrice(serviceProd.get(j).getPrice());
			sproductList.add(items);
			
			if(j==1)
			{
				break;
			}
			}			
			
			
			quot.setItems(sproductList);
			
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			quot.setCinfo(personInfo);
			quot.setPriority("Medium");
			quot.setRemark("");
			quot.setStatus("Created");
			quot.setType("1 BHK FLAT");
			quot.setUserId("admin123");	
			quot.setPaymentMethod("Cash");
			
//		    ofy().save().entity(quot);
//			logger.log(Level.SEVERE, "Lead Dat saved successfully");
			
			return quot;
		}

		
	
		private List<Lead> createLeadData(long companyId, List<ServiceProduct> serviceProd,int count) {
		
		
			Lead lead1=createLeadWithSuccessfulStatusButWithoutProduct(companyId,count+1);
			leadObj.add(lead1);
			
			Lead lead2=createLeadWithoutProductButCreatedStatus(companyId,count+2);
			leadObj .add(lead2);
		
			Lead lead3 =createLeadWithCreatedStatusWithProduct(companyId,count+3,serviceProd);
			leadObj .add(lead3);
			
			 return leadObj;
		}

		
		
		
		private Lead createLeadWithCreatedStatusWithProduct(long companyId,int i, List<ServiceProduct> serviceProd) {
			
			Lead lead = new Lead();
			lead.setApproverName("");
			lead.setApprovalDate(null);
			
			lead.setBranch("Andheri");
			lead.setCategory("RESIDENCIAL");
			lead.setCompanyId(companyId);
			lead.setCount(i);
			lead.setCreatedBy("Vedant Kamble");
			lead.setCreationDate(new Date());
			lead.setDescription("This is lead for Residencial Client");
			lead.setEmployee("Rohan Bhagde");
			
			List<SalesLineItem> sproductList= new ArrayList<SalesLineItem>();
			for (int j = 0; j < serviceProd.size(); j++) {
		
		
			SalesLineItem items = new SalesLineItem();
			items.setArea("NA");
			items.setProductSrNo(j+1);
			items.setPrduct(serviceProd.get(j));
			items.setProductCategory(serviceProd.get(j).getProductCategory());
			items.setProductName(serviceProd.get(j).getProductName());
			items.setProductCode(serviceProd.get(j).getProductCode());
			items.setQuantity(Double.parseDouble("1"));
			items.setNumberOfService(serviceProd.get(j).getNumberOfServices());
			items.setDuration(serviceProd.get(j).getDuration());
			items.setPrice(serviceProd.get(j).getPrice());
			sproductList.add(items);
			
			if(j==1)
			{
				break;
			}
			}			
			
			lead.setLeadProducts(sproductList);
			
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			lead.setPersonInfo(personInfo);
			lead.setPriority("Medium");
			lead.setRemark("");
			lead.setStatus("Created");
			lead.setTitle("Enquiry for Bed Bug");
			lead.setType("1 BHK FLAT");
			lead.setUserId("admin123");	
			lead.setGroup("Enquiry");
			
//		    ofy().save().entity(lead);
//			
//			logger.log(Level.SEVERE, "Lead Dat saved successfully");
			
			return lead;
		}

		private Lead createLeadWithSuccessfulStatusButWithoutProduct(long companyId, int i) {
			
			Lead lead = new Lead();
			lead.setApproverName("");
			lead.setApprovalDate(null);
			
			lead.setBranch("Andheri");
			lead.setCategory("RESIDENCIAL");
			lead.setCompanyId(companyId);
			lead.setCount(i);
			lead.setCreatedBy("Vedant Kamble");
			lead.setCreationDate(new Date());
			lead.setDescription("This Is Lead For Residencial Client Without Product Details");
			lead.setEmployee("Rohan Bhagde");
			
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			
			lead.setPersonInfo(personInfo);
			lead.setPriority("Medium");
			lead.setRemark("");
			lead.setStatus("Successful");
			lead.setTitle("Enquiry for Bed Bug");
			lead.setType("1 BHK FLAT");
			lead.setUserId("admin123");	
			lead.setGroup("Enquiry");
			
//		    ofy().save().entity(lead);
			
			return lead;
		}

		private Lead createLeadWithoutProductButCreatedStatus(long companyId,int j) {
			
			Lead lead = new Lead();
			lead.setApproverName("");
			lead.setApprovalDate(null);
			
			lead.setBranch("Andheri");
			lead.setCategory("RESIDENCIAL");
			lead.setCompanyId(companyId);
			lead.setCount(j);
			lead.setCreatedBy("Vedant Kamble");
			lead.setCreationDate(new Date());
			lead.setDescription("This is lead for Residencial Client");
			lead.setEmployee("Rohan Bhagde");
			
			PersonInfo personInfo=new PersonInfo();
			personInfo.setFullName("Suraj Sharma");
			personInfo.setCellNumber(Long.parseLong("9876543210"));
			personInfo.setCount(Integer.parseInt("100000001"));
			personInfo.setPocName("Suraj Sharma");
			personInfo.setEmail("evasoftwaresolutionserp@gmail.com");
			lead.setPersonInfo(personInfo);
			lead.setPriority("Medium");
			lead.setRemark("");
			lead.setStatus("Created");
			lead.setTitle("Enquiry for Bed Bug");
			lead.setType("1 BHK FLAT");
			lead.setUserId("admin123");	
			lead.setGroup("Enquiry");
			
//		    ofy().save().entity(lead);
//			logger.log(Level.SEVERE, "Lead Dat saved successfully");
			
			return lead;
		}

		
		
		
//		
//		
//		createBillingWithCreatedStatus();
//		createBillingWithApprovedStatus();
//		createBillingWithInvoiceStatus();
//		
//		
//		createInvoiceWithCreatedStatus();
//		createInvoiceWithApprovedStatus();
//
//		
//		createPaymentWithCreatedStatus();
//		createPaymentwithPartialPaymentCreatedStatus();
//		createPaymentWithPartialPaymentClosedStatus();
		
	
	
}
