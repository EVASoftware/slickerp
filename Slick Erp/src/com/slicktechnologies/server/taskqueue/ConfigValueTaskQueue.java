package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.common.helperlayer.Config;

public class ConfigValueTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8229263858006914311L;
	Logger logger = Logger.getLogger("Name Of logger");

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.log(Level.SEVERE, "Welcome to Module name Task Queue");

		String configNamewithdoller = req.getParameter("configValuekey");

		logger.log(Level.SEVERE, "Module name Data === with $ ==  "	+ configNamewithdoller);

		String[] configNamewithoutdoller = configNamewithdoller
				.split("[$]");

			
			Config config = new Config();
			
			config.setType(Integer.parseInt(configNamewithoutdoller[0]));
			config.setName(configNamewithoutdoller[1]);
			config.setStatus(true);
			config.setCompanyId(Long.parseLong(configNamewithoutdoller[2]));
			config.setCount(Integer.parseInt(configNamewithoutdoller[3]));
			
			ofy().save().entity(config);

		logger.log(Level.SEVERE, "Module Name saved successfully");
	}

	
}
