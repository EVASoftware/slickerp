package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;

public class CategoryValueTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -582903717133538659L;
	Logger logger = Logger.getLogger("Name Of logger");
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		logger.log(Level.SEVERE, "Welcome to Module name Task Queue");

		String categoryNamewithdoller = req.getParameter("categoryValuekey");

		logger.log(Level.SEVERE, "Module name Data === with $ ==  "	+ categoryNamewithdoller);

		String[] categoryNamewithoutdoller = categoryNamewithdoller
				.split("[$]");


		ConfigCategory configCategory = new ConfigCategory();
		configCategory.setInternalType(Integer.parseInt(categoryNamewithoutdoller[0]));
		configCategory.setCategoryName(categoryNamewithoutdoller[1]);
		configCategory.setCategoryCode(categoryNamewithoutdoller[2]);
		configCategory.setCompanyId(Long.parseLong(categoryNamewithoutdoller[3]));
		configCategory.setCount(Integer.parseInt(categoryNamewithoutdoller[4]));
		try {
			if(categoryNamewithoutdoller[5].equalsIgnoreCase("Blank"))
			{
				configCategory.setDescription("");
			}
			else
			{
				configCategory.setDescription(categoryNamewithoutdoller[5].trim());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	
		configCategory.setStatus(true);
		
	    ofy().save().entity(configCategory);

		logger.log(Level.SEVERE, "Module Name saved successfully");
	}

	
}
