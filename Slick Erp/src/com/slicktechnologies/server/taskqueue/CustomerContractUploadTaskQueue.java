package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CustomerContractDetails;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class CustomerContractUploadTaskQueue extends HttpServlet {

	Logger logger=Logger.getLogger("Customer ContractUploadTaskQueue");
	private static final long serialVersionUID = 3307347591955253671L;

	/**
	 *  Detail class objects
	 */
	private Customer custDetails;
	private	ServiceProduct serProduct;
	private	ConfigCategory conFiCat;
	private CustomerContractDetails contDetails;
	private ArrayList<String> ErrorList = new ArrayList<String>();
	/**
	 *  end
	 */
	
	/**
	 *  Created class objects
	 */
		String modifiedDate;
		private Contract contractDetail;
		SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
		private double contractAmt, BillingAmt;
		private	int serCount,contCount,billCount;
		private boolean serError=false,billingError=false;
		private List<Service>  servDtList =  new ArrayList<Service>();
		private	ArrayList<ServiceSchedule> serviceList = null;
		private	ArrayList<BillingDocument> billList = null;
		private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		SimpleDateFormat sdFmt = new SimpleDateFormat("yyyy-MM-dd");
		 Date date ;
//		public  NumberGeneration contractSrNo,  serNo, billNumGe;
		 private GenricServiceImpl impl = new GenricServiceImpl();
		 private String loginPerson = "";
	/**
	 *  end
	 */
	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		long companyId = Long.parseLong(req.getParameter("companyId"));
		try {
			int delayCount = Integer.parseInt(req.getParameter("delayCount"));
			
			int delay = 2000 *delayCount;
			
			Thread.sleep(delay);
			contractUploadDataProcess( req,  resp);
			
		} catch (Exception e) {
			serError = true;
			e.printStackTrace();
			logger.log(Level.SEVERE," Contract upload task que -- "+ e);
			contDetails.setRemark("There is some problem in contract Details "+e.getMessage() + " erroe tract -- "+ e);
			CsvWriter.custStatusList.add(contDetails);
			System.out.println("CustomerContractUploadTaskQueue error  -- "+ e);
		}finally{
//			if (CsvWriter.custStatusList.size()>0) {
//				EmailUploadDetails(companyId);
//			}
		}
			
			
	}
	
	public void contractUploadDataProcess(HttpServletRequest req, HttpServletResponse resp){

		 servDtList =  new ArrayList<Service>();
		billList = new ArrayList<BillingDocument>();
		serviceList =  new ArrayList<ServiceSchedule>();
		
		String CustContract = req.getParameter("CustContract");
		String customerDetail =  req.getParameter("customerDetail");
		String confiCat = req.getParameter("confiCat");
		loginPerson = req.getParameter("loginPerson");
		try {
			date = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e1) {
			logger.log(Level.SEVERE," this error for date"+e1);
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:MM");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		modifiedDate=sdf.format(new Date());
	
		Gson gson = new Gson();
		
		
		JSONObject jsonCust =null;
		try {
			jsonCust = new JSONObject(customerDetail.trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 custDetails = gson.fromJson(jsonCust.toString(), Customer.class);
		
		
		 conFiCat = gson.fromJson(confiCat.trim(), ConfigCategory.class);
		
		 contDetails = gson.fromJson(CustContract.trim(), CustomerContractDetails.class);
		 serCount = contDetails.getServiceCount();
		 contCount = contDetails.getContractCount();
		 billCount = contDetails.getBillingCount();
		
		 logger.log(Level.SEVERE," ser side Countract Count -- " + contCount + " Customer count -- "+ custDetails.getCount());
		 
		 if(custDetails!=null && contDetails != null){
			 
//			getServiceOrdeReg(custDetails.getCompanyId()); 
			 // DateUtility.getDateWithTimeZone("IST", sdFmt.parse(sdFmt.format(minDetail.getServiceComplDate())))
			 contractDetail = new Contract();
			 
			 int	proSrNo = 0;
			 
				
			 contractDetail.setCustomerId(custDetails.getCount());
				if(custDetails.isCompany()){
					contractDetail.getCinfo().setFullName(custDetails.getCompanyName());
				}else{
					contractDetail.getCinfo().setFullName(custDetails.getFullname());
				}
				contractDetail.setGroup(contDetails.getContractGroup());
				contractDetail.setCategory(contDetails.getContractCategory());
				contractDetail.setType(contDetails.getContractType());
				contractDetail.setEmployee(contDetails.getSalePerson());
				contractDetail.getCinfo().setPocName(custDetails.getFullname());
				contractDetail.getCinfo().setCellNumber(custDetails.getCellNumber1());
				contractDetail.getCinfo().setCount(custDetails.getCount());
				contractDetail.setSegment(custDetails.getCategory());
				contractDetail.setCreationDate(DateUtility.getDateWithTimeZone("IST", contDetails.getContractDate()));
				
				logger.log(Level.SEVERE," get customer name -- " + contractDetail.getCinfo().getFullName());
				contractDetail.setSdAddress(custDetails.getContacts().get(1).getAddress()); 
				if(custDetails.getContacts().get(1).getAddress().getAddrLine1().equals("") 
						&& custDetails.getContacts().get(1).getAddress().getAddrLine2().equals("")
						&& custDetails.getContacts().get(1).getAddress().getCity().equals("")
						&& custDetails.getContacts().get(1).getAddress().getCountry().equals("")
						&& custDetails.getContacts().get(1).getAddress().getLandmark().equals("")
						&& custDetails.getContacts().get(1).getAddress().getLocality().equals("")
						&& custDetails.getContacts().get(1).getAddress().getPin() == 0
						&& custDetails.getContacts().get(1).getAddress().getState().equals("")){
					contractDetail.setSdAddress(custDetails.getContacts().get(0).getAddress());
				}
//				
				contractDetail.setStatus("Approved");
				contractDetail.getCinfo().setEmail(custDetails.getEmail());
				contractDetail.setCompanyId(contDetails.getCompanyId());
				contractDetail.setApproverName(contDetails.getApproverName());
				contractDetail.setContractDate(DateUtility.getDateWithTimeZone("IST", (contDetails.getContractDate())));
				contractDetail.setStartDate(DateUtility.getDateWithTimeZone("IST",contDetails.getServiceProduct().get(0).getStartDate()));
				contractDetail.setEndDate(DateUtility.getDateWithTimeZone("IST", (contDetails.getServiceProduct().get(0).getEndDate())));
				contractDetail.setBranch(contDetails.getBranch());
				contractDetail.setRefNo(contDetails.getRefId());
				if( contDetails.getRefId()== null || contDetails.getRefId().trim().equals("") ){
					contractDetail.setRefNo(custDetails.getRefrNumber1());
				}
				try {
					contractDetail.setDescription("This data uploded through upload program " + sdf.parse(modifiedDate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.log(Level.SEVERE,"Get brach -- " + contDetails.getBranch() +" Detail branch "+ contDetails.getBranch());

				contractDetail.setContractRate(contDetails.isRateContract());
//				contractDetail.setRateContractService(contDetails.isRateContract());
				contractDetail.setBranchWiseBilling(false);
				contractDetail.setServiceWiseBilling(false);
				contractDetail.setConsolidatePrice(false);
				double totalAmt  = 0,contractAmt = 0;
					for(int j=0 ; j<contDetails.getServiceProduct().size(); j++){
							
							if(contDetails.getServiceProduct().get(j).getArea().matches("[0-9.]+") && Double.parseDouble(contDetails.getServiceProduct().get(j).getArea()) > 0){
								totalAmt = contDetails.getServiceProduct().get(j).getPrice() * Double.parseDouble(contDetails.getServiceProduct().get(j).getArea());
							}else{
								totalAmt = contDetails.getServiceProduct().get(j).getPrice();
							}
							contractAmt = contractAmt+totalAmt;
							
//							System.out.println("Item list size -- " +listofsaleslineitem.size() + " Outstanding amt == "+ outStandAmt);
							
							contDetails.getServiceProduct().get(j).setTotalAmount(totalAmt);
							contDetails.getServiceProduct().get(j).setTermsoftreatment("");
							contDetails.getServiceProduct().get(j).setProfrequency("");
							if(DateUtility.getDateWithTimeZone("IST", contractDetail.getEndDate()).before(DateUtility.getDateWithTimeZone("IST", contDetails.getServiceProduct().get(j).getEndDate()))){
								contractDetail.setEndDate(DateUtility.getDateWithTimeZone("IST", (contDetails.getServiceProduct().get(j).getEndDate())));
							}
//							logger.log(Level.SEVERE,"contract start date -- " +contractDetail.getStartDate() + " product start date -- " + contDetails.getServiceProduct().get(j).getStartDate());
//							logger.log(Level.SEVERE,"contract end date -- " +contractDetail.getEndDate() + " product start date -- " + contDetails.getServiceProduct().get(j).getEndDate());
							if(DateUtility.getDateWithTimeZone("IST", contDetails.getServiceProduct().get(j).getStartDate()).before(DateUtility.getDateWithTimeZone("IST", contractDetail.getStartDate()))){
								contractDetail.setStartDate(DateUtility.getDateWithTimeZone("IST", (contDetails.getServiceProduct().get(j).getStartDate())));
							}
							
//					}
					
//				}
			}
					contractDetail.setComplaintFlag(false);
					contractDetail.setItems(contDetails.getServiceProduct());
					try {
						contractDetail.setApprovalDate(sdf.parse(modifiedDate));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

//					contractDetail.setContractDate(DateUtility.getDateWithTimeZone("IST",(contractDetail.getStartDate())));
					try {
						contractDetail.setDescription("This contract was created through excel Upload Process on -"+ modifiedDate + " by :-"+ loginPerson);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					contractDetail.setStatus(Contract.APPROVED);
					contractDetail.setCreatedBy(contDetails.getApproverName());
					contractDetail.setTotalAmount(contractAmt);
			 
					if(contDetails.isRateContract()){
						PaymentTerms payTerms = new PaymentTerms();
						payTerms.setPayTermDays(0);
						payTerms.setPayTermPercent(Double.parseDouble("100"));
						payTerms.setPayTermComment("100% payment");
						payTerms.setPaymentDate(null);
						contractDetail.getPaymentTermsList().add(payTerms);
					}
					else{
						
						contractDetail.getPaymentTermsList().addAll((reactOnAddPayTerms(contractDetail.getItems(),contDetails.getPaymentTerms(),
								conFiCat, contractDetail.getContractDate(),contDetails)));
					}
					 serviceList = createServiceList(contDetails,contractDetail.getItems(),0,contractDetail);
					contractDetail.setServiceScheduleList(serviceList);
					contractDetail.setTotalAmount(contractAmt);
					contractDetail.setNetpayable(contractAmt);
					contractDetail.setGrandTotal(contractAmt);
					contractDetail.setFinalTotalAmt(contractAmt);
					contractDetail.setGrandTotal(contractAmt);
					contractDetail.setInclutaxtotalAmount(contractAmt);
					contractDetail.setPayTerms(contDetails.getPaymentTerms());
					contractDetail.setGrandTotalAmount(contractAmt);
					
					
					if(contractDetail != null){
						ArrayList<SuperModel> contList = new ArrayList<SuperModel>();
						logger.log(Level.SEVERE,"Before Contracct count -- "+ contCount);
						++contCount;
						contractDetail.setCount(contCount);
						contList.add(contractDetail);
						
						GenricServiceImpl implcon=new GenricServiceImpl();
						ArrayList<ReturnFromServer> con = implcon.save(contList);
						
						logger.log(Level.SEVERE,"return -- "  + con.get(0).count +"After update contract count  - " + contractDetail.getCount() +" get name -- " + contractDetail.getCustomerFullName());
					}
					
					
					logger.log(Level.SEVERE,"After Contracct count -- "+ contCount);
					if(!contractDetail.isContractRate()){
						 servDtList.addAll(getServiceList(contractDetail, contDetails, serviceList));
						
					}
					logger.log(Level.SEVERE," get ser list -- " + servDtList.size()  +" contract detail count -- "+ contractDetail.getCount() +"  billing count");
					if(billCount>0 && !contractDetail.isContractRate()){
						billList.addAll(createBillingDocs(contractDetail, contDetails));
					}
					
					logger.log(Level.SEVERE," get bill list -- " + billList.size()+ " ser error value "+ serError );
					
					if(!serError){
						
						if(servDtList.size()>0){
							logger.log(Level.SEVERE,"get start with service id -- " + servDtList.get(0).getCount());
							ArrayList<SuperModel> serviceDtList = new ArrayList<SuperModel>();
							serviceDtList.addAll(servDtList);
							
							GenricServiceImpl implser=new GenricServiceImpl();
							ArrayList<ReturnFromServer> serList = implser.save(serviceDtList);
							String serStr = "";
							for(ReturnFromServer ser : serList){
								serStr = serStr+ ser.count;
								logger.log(Level.SEVERE,"Ser save count -- " + ser.count + " contract count -- " + contractDetail.getCount());
							}
							
							logger.log(Level.SEVERE,"Ser save count -- " + serStr);
//							 
							 logger.log(Level.SEVERE,"After update service count  - " +serCount);
						}
						
						if(billList.size()>0){
							logger.log(Level.SEVERE," biiling count start --  " +billList.get(0).getCount() );
//							ofy().save().entities(billList).now();
							
//							ArrayList<SuperModel> billDtList = new ArrayList<SuperModel>();
//							billDtList.addAll(billList);
//							
//							GenricServiceImpl implbill=new GenricServiceImpl();
//							ArrayList<ReturnFromServer> serList = implbill.save(billDtList);
//							
//							String serStr = "";
//							for(ReturnFromServer ser : serList){
//								serStr = serStr+ ser.count;
//								logger.log(Level.SEVERE,"billing save count -- " + ser.count +  " contract count ---" + contractDetail.getCount());
//							}
							
//							logger.log(Level.SEVERE,"bill save count -- " + serStr);
//							 
							 logger.log(Level.SEVERE,"After update billing count  - " + billCount);
						}
							 
							 logger.log(Level.SEVERE, "Get email method start..!!");
					}
		 }
	}
	
	public void EmailUploadDetails(long companyId){
//		try {
			logger.log(Level.SEVERE,"Enter for mail...");
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			 Date d=new Date();
			 String dateString = sdf.format(d);
			
//			Calendar calendar=Calendar.getInstance();
//			calendar.setTime(new Date());
//			calendar.add(Calendar.DAY_OF_YEAR, -2);
//			Date afterDate = calendar.getTime();
//			String previousDateinString=sdf.format(afterDate);
//			String todayDateinString=sdf.format(new Date());
//			logger.log(Level.SEVERE,"previousDateinString::"+previousDateinString+"todayDateinString:::"+todayDateinString);
			
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, -1);
			Date previousDate = calendar.getTime();
			String todayDateinString=sdf.format(previousDate);
//			
			logger.log(Level.SEVERE,"Date ::"+todayDateinString);
			Company company=ofy().load().type(Company.class).filter("companyId",companyId).first().now();
			
			long compId=company.getCompanyId();
			logger.log(Level.SEVERE,"company id -- " + compId);
			
			ArrayList<String> contractRefList=new ArrayList<String>();
			contractRefList.add("No.");
			contractRefList.add("Reference Order Id");
			contractRefList.add("Status");
			
			System.out.println("get here mail .... !! "+ company.getEmail());
			ArrayList<String> tbl1=new ArrayList<String>();
			int count=1;
			for (CustomerContractDetails obj : CsvWriter.custStatusList) {
				
				tbl1.add(count+"");
				tbl1.add(obj.getRefId());
				tbl1.add(obj.getRemark());
				count=count+1;
			}
			
			Email cronEmail = new Email();
			
			try {
				//if( CsvWriter.custStatusList.size()!=0)
				{
					ArrayList<String> emailTo = new ArrayList<String>();
					emailTo.add(company.getEmail());
					logger.log(Level.SEVERE,"get here mail .... !! "+ company.getEmail());
					cronEmail.cronSendEmail(emailTo, "Data Migration Process As On Date"+" "+ todayDateinString, "Contract Report As On Date" +"  "+ todayDateinString, company, contractRefList, tbl1, null, null, null, null);
					logger.log(Level.SEVERE,"get here mail .... !! "+ company.getBusinessUnitName());
				}
			}catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE,"Customer details upload..."+e.getMessage());
//				return "Failed to send emails";
			}
			
//		} catch (Exception e) {
//			contDetails.setRemark("There is some error in Email process problem -- "+e.getMessage()+"customer details - " + contDetails.getCustomerCount());
//			e.printStackTrace();
//			System.out.println(e.getLocalizedMessage());
//			CsvWriter.custStatusList.add(contDetails);
//			e.getMessage();
//			System.out.println(e.getMessage());
//			logger.log(Level.SEVERE, "Customer details upload..."+e.getMessage());
//		}
	}
	
	private List<PaymentTerms> reactOnAddPayTerms(List<SalesLineItem> serProduct, String paymentTerms, ConfigCategory confi, Date startDate, CustomerContractDetails custdetail) 
	{
		List<PaymentTerms> ptermsList = new ArrayList<PaymentTerms>();
//		try {

			
			ConfigCategory conCategory = confi;
			if (conCategory != null) {


				int days = Integer.parseInt(conCategory.getDescription());
				System.out.println("Payment terms Days::::::::::::::" + days);

				if (serProduct.size() == 0) {
//					showDialogMessage("Please add Product");
					return null;
				}

				int duration = getDurationFromProductTable(serProduct);
				System.out.println("Duration::::::::: " + duration);

				if (duration == 0) {
					return null;
				}

				int noOfPayTerms = duration / days;
				System.out.println("No. of payment terms::::::::: "+ noOfPayTerms);

				if (noOfPayTerms == 0) {
					noOfPayTerms = 1;
				}

				Float no = (float) noOfPayTerms;
				
				System.out.println("No. Value::::::::: "+ no);
				
				float percent1 = 100 / no;
				System.out.println("Percent1::::::::: " + percent1);

				Double percent = (double) percent1;
				//rohan commentd this line for Date : 04/10/2017 
//				percent = Double.parseDouble(nf.format(percent));
				System.out.println("Percent2:::::::::  " + percent);

				int day = 0;
				double totalPer = 0;

					day = days;

				/**
				 * Date : 08-08-2017 BY ANIL
				 * 
				 */
					int monthCounter=0;
//				if(dbPaymentDate.getValue()!=null){
					if(days<30&&noOfPayTerms>1){
//						showDialogMessage("Payment term duration should be greater than 30 days for using payment date option.");
						monthCounter = 1;
					}
//				}
			
				
				if(days==30){
					monthCounter=1;
				}else if(days==60){
					monthCounter=2;
				}else if(days==90){
					monthCounter=3;
				}else if(days==120){
					monthCounter=4;
				}else if(days==150){
					monthCounter=5;
				}else if(days==180){
					monthCounter=6;
				}else if(days==210){
					monthCounter=7;
				}else if(days==240){
					monthCounter=8;
				}else if(days==270){
					monthCounter=9;
				}else if(days==300){
					monthCounter=10;
				}else if(days==330){
					monthCounter=11;
				}else if(days>360&&days<=366){
					monthCounter=12;
				}
				Date paymentDate=null;
				/**
				 * End
				 */
				if(startDate!=null){
//					paymentDate=Calen
					paymentDate = (Date) startDate.clone();
				}
				
				System.out.println("month counter "+monthCounter);
				for (int i = 1; i <= noOfPayTerms; i++) {
					Date paymentDt= (Date) paymentDate.clone();
					
					
					totalPer = totalPer + percent;
					
					if (i == noOfPayTerms) {
						if (totalPer == 100) {
							PaymentTerms pterm = new PaymentTerms();
							pterm.setPayTermDays(day);
							pterm.setPayTermPercent(percent);
							pterm.setPayTermComment(i + " Payment");
							/**
							 * Date : 08-08-2017 BY ANIL
							 */
							/**
							 * End
							 */
							
							logger.log(Level.SEVERE," Get in total 100 condition -- " + paymentDt);
//							pterm.setPaymentDate(paymentDt);
							ptermsList.add(pterm);

						} else {
							double diff = 0;
							if (totalPer < 100) {
								diff = 100 - totalPer;
								percent = percent + diff;
								//rohan commented this code on date date 04/10/2017
//								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(paymentDt != null){
//									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							} else {
								diff = totalPer - 100;
								percent = percent - diff;
								//rohan commented this code on date date 04/10/2017
//								percent = Double.parseDouble(nf.format(percent));

								PaymentTerms pterm = new PaymentTerms();
								pterm.setPayTermDays(day);
								pterm.setPayTermPercent(percent);
								pterm.setPayTermComment(i + " Payment");
								/**
								 * Date : 08-08-2017 BY ANIL
								 */
								if(paymentDt!=null){
//									pterm.setPaymentDate(paymentDt);
								}
								/**
								 * End
								 */
								ptermsList.add(pterm);
							}
						}

					} else {
						PaymentTerms pterm = new PaymentTerms();
						pterm.setPayTermDays(day);
						pterm.setPayTermPercent(percent);
						pterm.setPayTermComment(i + " Payment");
						/**
						 * Date : 08-08-2017 BY ANIL
						 */
						if(paymentDt!=null){
//							pterm.setPaymentDate(paymentDt);
						}
						/**
						 * End
						 */
						
						
						ptermsList.add(pterm);
					}
					day = day + days;
					
					if(paymentDate!=null){
						System.out.println(" BF DATE :: "+paymentDate);
						int month=paymentDate.getMonth();
						month=month+monthCounter;
						paymentDate.setMonth(month);
						System.out.println(" AF DATE :: "+paymentDate);
					}
				}
				
				
				//  rohan added 
//				paymentTermsTable.getDataprovider().setList(ptermsList);
			}
//		} catch (Exception e) {
//			serError = true;
//			logger.log(Level.SEVERE,"Get some error in payment terms method-- " + e);
//			 custdetail.setRemark("Service Creation error Please first correcrt the Data. Than try again for upload");
//				CsvWriter.custStatusList.add(custdetail);
//		}
			return ptermsList;
		}
	
	
	
	private int getDurationFromProductTable(List<SalesLineItem> serProduct) {
		int maxDur = 0;
		int currDur = 0;
		for (int i = 0; i < serProduct.size(); i++) {
			currDur = serProduct.get(i).getDuration();
			System.out.println(" curruent dur" + currDur);
			if (maxDur <= currDur) {
				maxDur = currDur;
			}
		}
	
		return maxDur;
	}
	
	
	public ArrayList<ServiceSchedule> createServiceList(CustomerContractDetails custdetail,List<SalesLineItem> salesitemlis,int servlistCount, Contract contDetail){
		ArrayList<ServiceSchedule> sersechList = new ArrayList<ServiceSchedule>();
		
		boolean serviceScheduleMinMaxRoundOffFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", AppConstants.PC_SERVICESCHEDULEMINMAXROUNDUP, contDetail.getCompanyId());

		try {
			serError = false;
			for(int i=0;i<salesitemlis.size();i++){	
			 int qty=(int) salesitemlis.get(i).getQty();
					for(int k=0;k < qty;k++){
					
					 	
					long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
					int noOfdays=salesitemlis.get(i).getDuration();
					
					/********* Date 02 oct 2017 below one line commented by vijay this interval calculation missing precision so services not getting created still end date ***********/
//					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					
					
					/*** Date 02 Oct 2017 below code added by vijay interval calculation with precision and using roundoff ******************/
					double days =  noOfdays;
					double noofservices= salesitemlis.get(i).getNumberOfServices();
					double interval =  days / noofservices;
					double temp = interval;
					double tempvalue=interval;
					/******************** ends here ********************/
					
					/**
					 * @author Vijay Date 11-10-2021
					 *  Des :- Service Scheduling with new Min Max round up logic for innovative
					 */
					if(serviceScheduleMinMaxRoundOffFlag){
						ServerAppUtility serverappUtility = new ServerAppUtility();
						interval = serverappUtility.getValueWithMinMaxRoundUp(interval);
						temp = interval;
						tempvalue=interval;
					}
					/**
					 * ends here
					 */
					//** set sales line contract End date
					Date servicedate=(salesitemlis.get(i).getStartDate());
					Date d=new Date(servicedate.getTime());
					
					Date productEndDate= new Date(servicedate.getTime());
//					CalendarUtil.addDaysToDate(productEndDate, noOfdays);
					 
					Calendar cal = Calendar.getInstance();
					cal.setTime(DateUtility.getDateWithTimeZone("IST", servicedate));
					cal.add(Calendar.DATE, noOfdays); 
					 
					productEndDate = cal.getTime();
					Date prodenddate=new Date(productEndDate.getTime());
					productEndDate=prodenddate;
					Date tempdate=new Date();
					int servcount = 1;
					System.out.println("No of ser --- " + salesitemlis.get(i).getNumberOfServices() +" duration -- "+ noOfdays +" interval -- "+interval+" ");
					logger.log(Level.SEVERE, "No of ser --- " + salesitemlis.get(i).getNumberOfServices() +" duration -- "+ noOfdays +" interval -- "+interval+" ");
					for(int j=0;j<noServices;j++){
						
						/*** Date 02 oct 2017 added by vijay for interval calculation *************/
						int interval2 =(int) Math.round(interval);
						
						
						if(j>0)
						{
							Calendar cal1 = Calendar.getInstance();
							cal1.setTime(d);
							cal1.add(Calendar.DATE, interval2); // add 10 days
							 
							d = cal1.getTime();
							tempdate=d;
							
							/************* Date 02 oct 2017 added by vijay *********************/
							interval =(interval+temp)-tempvalue;
						    tempvalue = Math.round(interval);
						    /************** ends here ************************/
						}
						System.out.println(" tempdate End Date --- " + tempdate +" Branch -- "+custdetail.getBranch() );
						logger.log(Level.SEVERE, " tempdate End Date --- " + tempdate +" Branch -- "+custdetail.getBranch());
						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=new Date();
						//DateUtility.getDateWithTimeZone("IST", sdFmt.parse(sdFmt.format(contDetail.getStartDate())))
						System.out.println("Service seduled Branch "+custdetail.getBranch() +" getProductSrNo " + salesitemlis.get(i).getProductSrNo());
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						ssch.setServicingBranch(contDetail.getBranch());
						ssch.setScheduleStartDate(DateUtility.getDateWithTimeZone("IST", (contDetail.getStartDate())));
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(servcount);
						servcount++;//DateUtility.getDateWithTimeZone("IST", sdFmt.parse(sdFmt.format(contDetail.getStartDate())))
						ssch.setScheduleProdStartDate(DateUtility.getDateWithTimeZone("IST", Stardaate));
						ssch.setScheduleProdEndDate(DateUtility.getDateWithTimeZone("IST",prodenddate));
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						ssch.setScheduleServiceTime("Flexible");
						
						
						
						
						if(j==0){
							cal.setTime(DateUtility.getDateWithTimeZone("IST", servicedate));
							int week = cal.get(Calendar.WEEK_OF_MONTH);
							ssch.setWeekNo(week);
							ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST",servicedate));
						}
						if(j!=0){
							if(productEndDate.before(d)==false){
								cal.setTime(DateUtility.getDateWithTimeZone("IST", tempdate));
								int week = cal.get(Calendar.WEEK_OF_MONTH);
								ssch.setWeekNo(week);
								ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST",tempdate));
							}else{
								
								cal.setTime(DateUtility.getDateWithTimeZone("IST", productEndDate));
								int week = cal.get(Calendar.WEEK_OF_MONTH);
								ssch.setWeekNo(week);
								ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST",productEndDate));
								
							}
						}
						
						SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
						String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (ssch.getScheduleServiceDate())));
						logger.log(Level.SEVERE,"get service Day --" +serviceDay);
						ssch.setScheduleServiceDay(serviceDay);
						ssch.setScheduleProBranch("Service Address");
						
						
						
						sersechList.add(ssch);
						
					}
					}
		}	
		} catch (Exception e) {
			serError = true;
			 custdetail.setRemark("Service Creation error Please first correcrt the Data. Than try again for upload"+custdetail.getCustomerCount());
			CsvWriter.custStatusList.add(custdetail);
			e.printStackTrace();
			sersechList.clear();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE,"CreateServiceList method exception -- " + e);
		}
		return sersechList;
	}
	
	
		List<Service> serlist =  new ArrayList<Service>();
	public List<Service> getServiceList(Contract contractdetail, CustomerContractDetails custDetail, ArrayList<ServiceSchedule> serviceList){
//		try {
		 	
			Service serDetailService =null;
			List<ServiceSchedule> serScheList = new ArrayList<ServiceSchedule>();
//			serviceCount = serCount;
//			DateUtility.getDateWithTimeZone("IST",contractdetail.getStartDate())
			if(serCount>0){
				for(ServiceSchedule ser : serviceList){
					System.out.println(" serv date -- "+ ser.getScheduleServiceDate() +" ");
//					if(custDetail.getCutOffDate()!= null && ser.getScheduleServiceDate().after(custDetail.getCutOffDate())){
//						System.out.println(" serv date -- "+ custDetail.getCutOffDate() +" ");
						serDetailService = new Service();
						serDetailService.setPersonInfo(contractdetail.getCinfo());
						serDetailService.setAddress(contractdetail.getSdAddress());
						ServiceProduct serPro = getProCode(contractdetail,ser.getScheduleProdName());
						if(serPro!=null){
							serDetailService.setProduct(serPro);
						}
						serDetailService.setServiceSerialNo(ser.getScheduleServiceNo());
						serDetailService.setApproverName(custDetail.getApproverName());
						serDetailService.setServiceBranch(ser.getScheduleProBranch());
						System.out.println("Serv Branch " + ser.getServicingBranch());
						serDetailService.setBranch(ser.getServicingBranch());
						serDetailService.setServiceDay(ser.getScheduleServiceDay());
						serDetailService.setServiceDate(DateUtility.getDateWithTimeZone("IST",ser.getScheduleServiceDate()));
						serDetailService.setContractCount(contractdetail.getCount());
						serDetailService.setContractEndDate(DateUtility.getDateWithTimeZone("IST",contractdetail.getEndDate()));
						
						for(SalesLineItem item : contractdetail.getItems()){
							if(serPro.getCount() == item.getPrduct().getCount()){
								serDetailService.setServiceValue(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty()));
							}
						}
						serDetailService.setServiceWeekNo(ser.getWeekNo());
						serDetailService.setContractStartDate(DateUtility.getDateWithTimeZone("IST",contractdetail.getStartDate()));
						serDetailService.setCompanyId(contractdetail.getCompanyId());
						serDetailService.setQuantity(ser.getScheduleProQty());
						serDetailService.setRateContractService(contractdetail.isContractRate());
						serDetailService.setRefNo(contractdetail.getRefNo());
						try {
							serDetailService.setRemark("This service was created through contract excel upload process on -"+ fmt.parse(modifiedDate) + " by :-"+ loginPerson);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						serDetailService.setStatus("Scheduled");
						serDetailService.setServiceTime("Flexible");
						serDetailService.setCategory(contractdetail.getCategory());
						serDetailService.setType(contractdetail.getType());
						serDetailService.setGroup(contractdetail.getGroup());
						serCount++;
						serDetailService.setCount(serCount);
						logger.log(Level.SEVERE,"service count -- " + serDetailService.getCount() );
						serlist.add(serDetailService);
					}
//						if(serlist.size()>0){
//							ofy().save().entities(serlist);
//						}
						
				}
				
//			}
//			
//		catch (Exception e) {
//			serCount = 0;
//			 serlist.clear();
//			 serError = true;
//			e.printStackTrace();
//			custDetail.setRemark("Service Creation error Please first correcrt the Data. Than try again for upload"+e.getMessage()+ " customer id -" +custDetail.getCustomerCount());
//			CsvWriter.custStatusList.add(custDetail);
//			System.out.println(e.getMessage());
//			logger.log(Level.SEVERE,"Service List -- "+e);
//		}
		return serlist;
	}
	
	public ServiceProduct getProCode(Contract Condetail,String proID){
		String code = null;
		ServiceProduct pro = null;
//		try {
			for(SalesLineItem list : Condetail.getItems()){
				if(list.getPrduct().getProductName().equals(proID)){
					return (ServiceProduct)list.getPrduct();
				}
			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			serError = true;
//			logger.log(Level.SEVERE," getProCode methosd == "+ e);
//		}
		return pro;
	}
	
//	public void getServiceOrdeReg(Long companyId){
//		try {
//			// contractSrNo,  serNo, billNumGe;
//			 serNo = ofy().load().type(NumberGeneration.class)
//					.filter("companyId", companyId)
//					.filter("processName", "Service").filter("status", true)
//					.first().now();
//			long number = serNo.getNumber();
//			serCount=  (int) number;
//			
//			contractSrNo = ofy().load().type(NumberGeneration.class)
//					.filter("companyId", companyId)
//					.filter("processName", "Contract").filter("status", true)
//					.first().now();
//			 number = contractSrNo.getNumber();
//			 contCount=  (int) number;
//			 
//			 billNumGe = ofy().load().type(NumberGeneration.class)
//						.filter("companyId", companyId)
//						.filter("processName", "BillingDocument").filter("status", true)
//						.first().now();
//				 number = billNumGe.getNumber();
//				billCount =  (int) number;
////			contCount
//		} catch (Exception e) {
//			serError = true;
//			e.printStackTrace();
//			System.out.println(e.getLocalizedMessage());
//			logger.log(Level.SEVERE,"get error in service reg number -- "+e.getLocalizedMessage());
//			
//		}
//	}
	
	private List<BillingDocument> createBillingDocs(Contract contract,CustomerContractDetails contDetails)
	{
		List<BillingDocument> billingDoc = new ArrayList<BillingDocument>();
//		try {
			if(billCount>0){
				Logger billingLogger=Logger.getLogger("Billing Logger");
				billingLogger.log(Level.SEVERE,"BIlling Process Started for Creation");
				Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
				
				// below count variable is added by vijay for billing period
			    int count=0;
			    
				for(int i=0;i<contract.getPaymentTermsList().size();i++)
				{
					ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();

					BillingDocument billingDocEntity=new BillingDocument();
					List<SalesOrderProductLineItem> salesProdLis=new ArrayList<SalesOrderProductLineItem>();
					List<ContractCharges> billTaxLis= new ArrayList<ContractCharges>();
					ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
					PaymentTerms paymentTerms=new PaymentTerms();
					Date conStartDate=null;
					DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
					dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					System.out.println("Contract Start Date"+conStartDate);
				
					
			/***********************Invoice Date******************************/
					
					billingLogger.log(Level.SEVERE,"Contract Date As per IST---"+DateUtility.getDateWithTimeZone("IST",contract.getContractDate()));
					billingLogger.log(Level.SEVERE,"Contract Date As Per Saved---"+contract.getContractDate());
					
					Calendar calInvoiceDate = Calendar.getInstance();
					if(contract.getContractDate()!=null){
						Date serConDate=DateUtility.getDateWithTimeZone("IST", contract.getContractDate());
						calInvoiceDate.setTime(serConDate);
					}
					else{
						conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
						calInvoiceDate.setTime(conStartDate);
					}
					
					calInvoiceDate.add(Calendar.DATE, contract.getPaymentTermsList().get(i).getPayTermDays());
					Date calculatedInvoiceDate=null;
					try {
						calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
						calInvoiceDate.set(Calendar.MINUTE,59);
						calInvoiceDate.set(Calendar.SECOND,59);
						calInvoiceDate.set(Calendar.MILLISECOND,999);
						calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
//						billingLogger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
						if(currentDate.after(calculatedInvoiceDate))
						{
							calculatedInvoiceDate=currentDate;
						}
						
						
					} catch (ParseException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE," Error in billing creation=="+e);
					}
					
					/***********************************************************************************/
					
					/*********************************Billing Date************************************/
					
					Date calBillingDate=null;
					Calendar calBilling=Calendar.getInstance();
					if(calculatedInvoiceDate!=null){
						calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
					}
					
//					billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
					
					calBilling.add(Calendar.DATE, -2);
					try {
						calBilling.set(Calendar.HOUR_OF_DAY,23);
						calBilling.set(Calendar.MINUTE,59);
						calBilling.set(Calendar.SECOND,59);
						calBilling.set(Calendar.MILLISECOND,999);
						calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
						
						if(currentDate.after(calBillingDate)){
							calBillingDate=currentDate;
						}
						
//						billingLogger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
					} catch (ParseException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE," Error in billing creation=="+e);
					}
					
					/***************************************************************************************/
					
					/********************************Payment Date******************************************/
					
					Date calPaymentDate=null;
					int creditVal=0;
					if(contract.getCreditPeriod()!=null){
						creditVal=contract.getCreditPeriod();
					}
					
					Calendar calPayment=Calendar.getInstance();
					if(calculatedInvoiceDate!=null){
						calPayment.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate));
					}
//					billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
					
					calPayment.add(Calendar.DATE, creditVal);
					try {
						calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
						calPayment.set(Calendar.HOUR_OF_DAY,23);
						calPayment.set(Calendar.MINUTE,59);
						calPayment.set(Calendar.SECOND,59);
						calPayment.set(Calendar.MILLISECOND,999);
						calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
//						billingLogger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
					} catch (ParseException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE," Error in billing creation=="+e);
					}
					
					/***********************************************************************************/
					
					System.out.println("Billing date to be saved==="+calBillingDate);
					
					/******************************************************************************/
					if(contract.getCinfo()!=null)
						billingDocEntity.setPersonInfo(contract.getCinfo());
					if(contract.getCount()!=0)
						billingDocEntity.setContractCount(contract.getCount());
					if(contract.getStartDate()!=null)
						billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST",contract.getStartDate()));
					if(contract.getEndDate()!=null)
						billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST",contract.getEndDate()));
					if(contract.getNetpayable()!=0)
						billingDocEntity.setTotalSalesAmount(contract.getNetpayable());
					salesProdLis=this.retrieveSalesProducts(contract.getPaymentTermsList().get(i).getPayTermPercent(),contract);
					ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
					logger.log(Level.SEVERE," contract billing count -- " + billingDocEntity.getCount());
					salesOrdArr.addAll(salesProdLis);
					System.out.println("Size here here"+salesOrdArr.size());
					if(salesOrdArr.size()!=0)
						billingDocEntity.setSalesOrderProducts(salesOrdArr);
					if(contract.getCompanyId()!=null)
						billingDocEntity.setCompanyId(contract.getCompanyId());
					/**
					 * Date : 09-08-2017 BY ANIL
					 * if payment date in payment term list is not null then we set bill,invoice and payment date as passed payment date. 
					 */
					if(contract.getPaymentTermsList().get(i).getPaymentDate()!=null){
						billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i).getPaymentDate()));
//						billingLogger.log(Level.SEVERE,"Bill-Payment Date Not Null : "+billingDocEntity.getBillingDate());
					
						billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i).getPaymentDate()));
//						billingLogger.log(Level.SEVERE,"Invoice-Payment Date Not Null : "+billingDocEntity.getInvoiceDate());
						
						billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i).getPaymentDate()));
//						billingLogger.log(Level.SEVERE,"Payment-Payment Date Not Null : "+billingDocEntity.getPaymentDate());
					
					}else{
						if(calBillingDate!=null){
							billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
//							billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
						}
						
						if(calculatedInvoiceDate!=null){
							billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
//							billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
						}
						if(calPaymentDate!=null){
							billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
//							billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
						}
					}
					
					
					/**
					if(calBillingDate!=null){
						billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
						billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
					}
					
					if(calculatedInvoiceDate!=null){
						billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
						billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
					}
					if(calPaymentDate!=null){
						billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
						billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
					}
					*/
					if(contract.getPaymentMethod()!=null)
						billingDocEntity.setPaymentMethod(contract.getPaymentMethod());
					if(contract.getApproverName()!=null)
						billingDocEntity.setApproverName(contract.getApproverName());
					if(contract.getEmployee()!=null)
						billingDocEntity.setEmployee(contract.getEmployee());
					if(contract.getBranch()!=null)
						billingDocEntity.setBranch(contract.getBranch());
					
					/*
					 *  nidhi
					 *  	30-06-2017
					 */
				
					if(contract.getSegment()!=null){
						billingDocEntity.setSegment(contract.getSegment());
					}
					/*
					 * End
					 */
					
					ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
					if(contract.getProductTaxes().size()!=0){
						ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
						billTaxLis=this.listForBillingTaxes(contract.getPaymentTermsList().get(i).getPayTermPercent(),contract);
						billTaxArr.addAll(billTaxLis);
						billingDocEntity.setBillingTaxes(billTaxArr);
						billTaxArrROHAN.addAll(billTaxLis);
					}

					if(contract.getStartDate()!=null){
						billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST", contract.getStartDate()));
					}
					if(contract.getEndDate()!=null){
						billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST", contract.getEndDate()));
					}
					
			//***********************changes made by rohan for saving gross value *************	
					double grossValue=contract.getTotalAmount();
					
					
					billingDocEntity.setGrossValue(grossValue);
					
			//********************************changes ends here *******************************		
					
					
					double totBillAmt=contract.getTotalBillAmt(salesProdLis);
					
					double taxAmt= contract.getTotalFromTaxTable(billTaxArrROHAN);
					
					double totalBillingAmount = totBillAmt+taxAmt;
					billingDocEntity.setTotalBillingAmount(totalBillingAmount);
					billingDocEntity.setGrandTotalAmount(totalBillingAmount);
					billingDocEntity.setOrderCreationDate(DateUtility.getDateWithTimeZone("IST",contract.getCreationDate()));
					
					int payTrmsDays=contract.getPaymentTermsList().get(i).getPayTermDays();
					double payTrmsPercent=contract.getPaymentTermsList().get(i).getPayTermPercent();
					String payTrmsComment=contract.getPaymentTermsList().get(i).getPayTermComment().trim();
					
					
					String paytrmsBranch = contract.getPaymentTermsList().get(i).getBranch();
					if(paytrmsBranch!=null){
						billingDocEntity.setCustomerBranch(paytrmsBranch);
					}
					
					
					paymentTerms.setPayTermDays(payTrmsDays);
					paymentTerms.setPayTermPercent(payTrmsPercent);
					paymentTerms.setPayTermComment(payTrmsComment);
					billingPayTerms.add(paymentTerms);
					billingDocEntity.setArrPayTerms(billingPayTerms);
					billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
					billingDocEntity.setStatus(BillingDocument.CREATED);
					billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
					billingDocEntity.setOrderCformStatus("");
					billingDocEntity.setOrderCformPercent(-1);


					/*************************** vijay number range **************************/
					if(contract.getNumberRange()!=null)
						billingDocEntity.setNumberRange(contract.getNumberRange());
					
					

					/**
					 * Date : 13 Feb 2017
					 * by Vijay
					 * here i am setting billing period based on payment terms
					 * billing period from date is billing date and To date is next payment terms -1 date if payment terms size not !=1
					 * and Last payment terms To date is set by contract end Date
					 * (logic is same as per above billing date calculated just i am changing next payment terms days -1 day date is calculating
					 * for to set billing period To date)
					 * if payment terms size is 1 then From date is billing date and To date Contract end Date
					 */
					
					System.out.println("From Billing peroid Date ==="+calBillingDate);
					
					count++;
					
					if(contract.getPaymentTermsList().size()==1){
						billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
						billingDocEntity.setBillingPeroidToDate(contract.getEndDate());
					}else{
						
						
						billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));

						
							if(count!=contract.getPaymentTermsList().size()){
								
							Calendar calInvoiceDate2 = Calendar.getInstance();
							if(contract.getContractDate()!=null){
								Date serConDate=DateUtility.getDateWithTimeZone("IST", contract.getContractDate());
								calInvoiceDate2.setTime(serConDate);
							}
							else{
								conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
								calInvoiceDate2.setTime(conStartDate);
							}
							
							calInvoiceDate2.add(Calendar.DATE, contract.getPaymentTermsList().get(i+1).getPayTermDays());
							Date calculatedInvoiceDate2=null;
							try {
								calInvoiceDate2.set(Calendar.HOUR_OF_DAY,23);
								calInvoiceDate2.set(Calendar.MINUTE,59);
								calInvoiceDate2.set(Calendar.SECOND,59);
								calInvoiceDate2.set(Calendar.MILLISECOND,999);
								calculatedInvoiceDate2=dateFormat.parse(dateFormat.format(calInvoiceDate2.getTime()));
	//							billingLogger.log(Level.SEVERE,"Calculated Invoice Date =="+calculatedInvoiceDate2);
								if(currentDate.after(calculatedInvoiceDate2))
								{
									calculatedInvoiceDate2=currentDate;
								}
								
								
							} catch (ParseException e) {
								e.printStackTrace();
								logger.log(Level.SEVERE," Error in billing creation=="+e);
							}
							
							/***********************************************************************************/
							
							
							Date calBillingDate2=null;
							Calendar calBilling2=Calendar.getInstance();
							if(calculatedInvoiceDate2!=null){
								calBilling2.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate2 ));
							}
							
	//						billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing=="+calculatedInvoiceDate2);
							
							calBilling2.add(Calendar.DATE, -3);
							try {
								calBilling2.set(Calendar.HOUR_OF_DAY,23);
								calBilling2.set(Calendar.MINUTE,59);
								calBilling2.set(Calendar.SECOND,59);
								calBilling2.set(Calendar.MILLISECOND,999);
								calBillingDate2=dateFormat.parse(dateFormat.format(calBilling2.getTime()));
								
								if(currentDate.after(calBillingDate2)){
									calBillingDate2=currentDate;
								}
								
	//							billingLogger.log(Level.SEVERE,"Calculated Billing=="+calBillingDate2);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							
								billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST", calBillingDate2));
								
								System.out.println("To billing peroid Date"+DateUtility.getDateWithTimeZone("IST", calBillingDate2));
	
							}else{
							
							billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST",contract.getEndDate()));
						}
						
					}	
					
					/**
					 * End here
					 */
					
					/**
					 * Date : 10-08-2017 By ANIL
					 * Recalculating billing period from and to date , if payment date on payment term is not null
					 */
					
					if(contract.getPaymentTermsList().get(i).getPaymentDate()!=null){
						if(contract.getPaymentTermsList().size()==1){
							billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i).getPaymentDate()));
							billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST",contract.getEndDate()));
						}else if(i==contract.getPaymentTermsList().size()-1){
							billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i).getPaymentDate()));
							billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST",contract.getEndDate()));
						}else{
							billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i).getPaymentDate()));
							
							Date date=DateUtility.getDateWithTimeZone("IST", contract.getPaymentTermsList().get(i+1).getPaymentDate());
							Calendar calendar = DateUtility.getCalendarForDate(date);
						    calendar.add(Calendar.DATE, -1);
						    calendar.getTime();
							billingDocEntity.setBillingPeroidToDate(calendar.getTime());
						}
					}
					
					/**
					 * End
					 */
					
					/** Date 03-09-2017 added by vijay for total amount before taxes**/
					billingDocEntity.setTotalAmount(totBillAmt);
					/** Date 05-09-2017 added by vijay for Discount amount**/
					double discountamt = 0;
					if(contract.getDiscountAmt()!=0){
						discountamt = contract.getDiscountAmt()/contract.getPaymentTermsList().size();
						billingDocEntity.setDiscountAmt(discountamt);
					}
					/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
					double totalfinalAmt=totBillAmt-discountamt;
					billingDocEntity.setFinalTotalAmt(totalfinalAmt);
					 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
					double totalamtincludingtax=totalfinalAmt+taxAmt;
					billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
					 /** Date 05-09-2017 added by vijay for roundoff amount **/
					double roundoff=0;
					if(contract.getRoundOffAmt()!=0){
						roundoff=contract.getRoundOffAmt()/contract.getPaymentTermsList().size();
						billingDocEntity.setRoundOffAmt(roundoff);
					}
					
					/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
					billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
					
					/*** Date 10-10-2017 added by vijay for total billing amount *********/
					billingDocEntity.setTotalBillingAmount(totalamtincludingtax+roundoff);
					
					/**
					 * Date :24-10-2017 BY ANIL
					 * Copying contract reference no to billing reference 
					 * as required by NBHC
					 */
					if(contract.getRefNo()!=null){
						billingDocEntity.setRefNumber(contract.getRefNo());
					}
					/**
					 * End
					 */
					billingDocEntity.setComment("This billing was created through contract excel upload process on -"+ (currentDate) + " by :-"+ loginPerson);
//					arrbilling.add(billingDocEntity);
					/******************************************************************************/
//					GenricServiceImpl impl=new GenricServiceImpl();
//					if(arrbilling.size()!=0){
						++billCount;
						billingDocEntity.setCount(billCount);
						billingDoc.add(billingDocEntity);
						logger.log(Level.SEVERE,"billing cust -- " + billingDocEntity.getPersonInfo().getFullName() + "contract count --  "+billingDocEntity.getContractCount() + " billing save count -- " +billingDocEntity.getCount());
//						impl.save(arrbilling);
//					}
				}
				billingLogger.log(Level.SEVERE,"Billing Process Completed");
			}
			
//		} catch (Exception e) {
//			billCount= 0;
//			serError = true;
//			contDetails.setRemark("There is some error in Billing creation -- "+e.getMessage()+"customer details - " + contDetails.getCustomerCount());
//			e.printStackTrace();
//			System.out.println(e.getLocalizedMessage());
//			CsvWriter.custStatusList.add(contDetails);
//			logger.log(Level.SEVERE," get error in billing doc creation. "+e);
//		}
			for(BillingDocument bill : billingDoc){
				logger.log(Level.SEVERE,"billing count -- " + bill.getCount() + " billing countract count ---" + bill.getContractCount());
			}
			ofy().save().entities(billingDoc);
		return billingDoc;
	}
	
	private List<SalesOrderProductLineItem> retrieveSalesProducts(double payTermPercent, Contract contract)
	{
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
//		try {

			
			double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0,discAmt=0;
			String prodDesc1="",prodDesc2="";
			for(int i=0;i<contract.getItems().size();i++)
			{
				SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", contract.getItems().get(i).getPrduct().getCount()).first().now();
				if(superProdEntity!=null){
					System.out.println("Superprod Not Null");
					if(superProdEntity.getComment()!=null){
						System.out.println("Desc 1 Not Null");
						prodDesc1=superProdEntity.getComment();
					}
					if(superProdEntity.getCommentdesc()!=null){
						System.out.println("Desc 2 Not Null");
						prodDesc2=superProdEntity.getCommentdesc();
					}
				}
				
				SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
				//SuperProduct productEnt=contract.getItems().get(i).getPrduct();
				salesOrder.setProductSrNumber(contract.getItems().get(i).getProductSrNo());
				salesOrder.setProdId(contract.getItems().get(i).getPrduct().getCount());
				salesOrder.setProdCategory(contract.getItems().get(i).getProductCategory());
				salesOrder.setProdCode(contract.getItems().get(i).getProductCode());
				salesOrder.setProdName(contract.getItems().get(i).getProductName());
				salesOrder.setQuantity(contract.getItems().get(i).getQty());
				salesOrder.setOrderDuration(contract.getItems().get(i).getDuration());
				salesOrder.setOrderServices(contract.getItems().get(i).getNumberOfServices());
				totalTax=contract.removeTaxAmt(contract.getItems().get(i).getPrduct());
				prodPrice=contract.getItems().get(i).getPrice()-totalTax;
				System.out.println("Prod Price"+prodPrice);
				salesOrder.setPrice(prodPrice);
				salesOrder.setVatTaxEdit(contract.getItems().get(i).getVatTaxEdit());
				salesOrder.setServiceTaxEdit(contract.getItems().get(i).getServiceTaxEdit());
				salesOrder.setVatTax(contract.getItems().get(i).getVatTax());
				salesOrder.setServiceTax(contract.getItems().get(i).getServiceTax());
				
				//  rohan added this code for area considered in calculation
				
				if(contract.getItems().get(i).getArea().equals("0")){
					salesOrder.setArea("NA");
				}else{
					salesOrder.setArea(contract.getItems().get(i).getArea());
				}
				
				
			//  rohan added contract code for setting HSN Code Date : 26-06-2017
				salesOrder.setHsnCode(contract.getItems().get(i).getPrduct().getHsnNumber());
				
				salesOrder.setProdPercDiscount(contract.getItems().get(i).getPercentageDiscount());
				salesOrder.setProdDesc1(prodDesc1);
				salesOrder.setProdDesc2(prodDesc2);
				if(contract.getItems().get(i).getUnitOfMeasurement()!=null){
					salesOrder.setUnitOfMeasurement(contract.getItems().get(i).getUnitOfMeasurement());
				}
				
				//*****************rohan commented this code***************** 
				
				
				salesOrder.setDiscountAmt(contract.getItems().get(i).getDiscountAmt());
				
				
				
				/***************** new code area calculation and discount on total *******************/
				//  rohan remove qty form price date : 10/11/2016
				
				if((contract.getItems().get(i).getPercentageDiscount()==null && contract.getItems().get(i).getPercentageDiscount()==0) && (contract.getItems().get(i).getDiscountAmt()==0) ){
					
					System.out.println("inside both 0 condition");
					
					if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA")  && !contract.getItems().get(i).getArea().equals("0")){
						double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
						percAmt=prodPrice*squareArea;
						}
						else{
							System.out.println("Old code");
							percAmt=prodPrice;
						}
					
				}
				
				else if((contract.getItems().get(i).getPercentageDiscount()!=null)&& (contract.getItems().get(i).getDiscountAmt()!=0)){
					
					System.out.println("inside both not null condition");
					
					if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA")  && !contract.getItems().get(i).getArea().equals("0")){
						double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
						percAmt=prodPrice*squareArea;
						percAmt= percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
						percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
					}else{
						
						percAmt=prodPrice;
						percAmt=percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
						percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
					}
				}
				else 
				{
					System.out.println("inside oneof the null condition");
					
					if(contract.getItems().get(i).getPercentageDiscount()!=null && contract.getItems().get(i).getPercentageDiscount()!=0){
						if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA")  && !contract.getItems().get(i).getArea().equalsIgnoreCase("0")){
							Double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
							percAmt = prodPrice*squareArea;
							percAmt=percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
						}else{
							percAmt=prodPrice;
							percAmt=percAmt-(percAmt*contract.getItems().get(i).getPercentageDiscount()/100);
						}
						
					}
					else 
					{
						if(!contract.getItems().get(i).getArea().equalsIgnoreCase("NA") && !contract.getItems().get(i).getArea().equalsIgnoreCase("0")){
							Double squareArea = Double.parseDouble(contract.getItems().get(i).getArea());
							percAmt = prodPrice*squareArea;
							percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
						}else{
							percAmt=prodPrice;
							percAmt=percAmt-contract.getItems().get(i).getDiscountAmt();
						}
					}
					System.out.println("percAmt ====== "+percAmt);
				}

		/******************************************************************************************************/
				
				salesOrder.setTotalAmount(percAmt);
				if(payTermPercent!=0){
					baseBillingAmount=(percAmt*payTermPercent)/100;
				}
			
//				salesOrder.setBaseBillingAmount(Math.round(baseBillingAmount));
				salesOrder.setBaseBillingAmount((baseBillingAmount));
				salesOrder.setPaymentPercent(100.0);
				
				/**
				 * Date : 04-08-2017 By Anil
				 * Setting base bill amount to base payment amount
				 */
//				salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
				salesOrder.setBasePaymentAmount(baseBillingAmount);
				/**
				 * End
				 */
				salesOrder.setIndexVal(i+1);
				salesOrder.setProductSrNumber(contract.getItems().get(i).getProductSrNo());
				salesOrder.setPrduct(superProdEntity);
				
				/////////////////////////////////////////////////////////////////////////////////
				salesProdArr.add(salesOrder);
			}
			
		
//		} catch (Exception e) {
//			contDetails.setRemark("There is some error in Billing SalesProducts -- "+e.getMessage()+"customer details - " + contDetails.getCustomerCount());
//			e.printStackTrace();
//			System.out.println(e.getLocalizedMessage());
//			CsvWriter.custStatusList.add(contDetails);
//			logger.log(Level.SEVERE," billing retrieve sales pro -- " +e);
//		}
		return salesProdArr;
	}
	
	public List<ContractCharges> listForBillingTaxes(double percPay,Contract contract){
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
//		try {
			double assessValue=0;
			for(int i=0;i<contract.productTaxes.size();i++){
				ContractCharges taxDetails=new ContractCharges();
				taxDetails.setTaxChargeName(contract.getProductTaxes().get(i).getChargeName());
				taxDetails.setTaxChargePercent(contract.getProductTaxes().get(i).getChargePercent());
				assessValue=contract.getProductTaxes().get(i).getAssessableAmount()*percPay/100;
				taxDetails.setTaxChargeAssesVal(assessValue);
				
				taxDetails.setIdentifyTaxCharge(contract.getProductTaxes().get(i).getIndexCheck());
				arrBillTax.add(taxDetails);
			}
//		} catch (Exception e) {
//			contDetails.setRemark("There is some error in Billing taxes SalesProducts -- "+e.getMessage()+"customer details - " + contDetails.getCustomerCount());
//			e.printStackTrace();
//			System.out.println(e.getLocalizedMessage());
//			CsvWriter.custStatusList.add(contDetails);
//			logger.log(Level.SEVERE,"retrieveSalesProducts -- " + e);
//		}
		return arrBillTax;
	}
	
	
}
