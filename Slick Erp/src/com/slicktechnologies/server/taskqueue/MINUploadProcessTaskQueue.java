package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.rmi.server.RemoteServer;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CustomerProjectServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.CustomerContractDetails;
import com.slicktechnologies.shared.MINUploadDetails;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryTransaction;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;


public class MINUploadProcessTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	Logger logger=Logger.getLogger("MIN Upload Process Task Queue");
	private static final long serialVersionUID = 2078519673999042716L;
	
	List<Service> serviceDetailList = new ArrayList<Service>();
//	 <Integer,int> contCount;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
//	private int mintrac = 0,mincount =0 ;
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		long companyId = Long.parseLong(req.getParameter("companyId"));
		try {
			int delayCount = Integer.parseInt(req.getParameter("delayCount"));
			
			int delay = 50 * delayCount;
			
//			Thread.sleep(delay);
			minUploadDataProcess( req,  resp);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE," Contract upload task que -- "+ e);
			System.out.println("CustomerContractUploadTaskQueue error  -- "+ e);
		}
		
	}
	
	void minUploadDataProcess(HttpServletRequest req, HttpServletResponse resp){
		try {
			 int mintrac = 0,mincount =0;
//			String serDt = req.getParameter("serviceDetail").trim();
		final	String minDt = req.getParameter("MinDetail").trim();
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		    Date date = sdf.parse(sdf.format(new Date()));
		    
		    
			
			JSONObject minObj = new JSONObject(minDt.trim());
			
			Gson gson = new GsonBuilder().create();
			
		  final MINUploadDetails minDetail =  gson.fromJson(minObj.toString(), MINUploadDetails.class).myClone();
			
			Service serDetail = ofy().load().type(Service.class).filter("companyId", minDetail.getCompanyId())
					.filter("contractCount", minDetail.getContractCount())
					.filter("count", minDetail.getServiceCount()).first().now();
			
		
			
			Integer mindocId = new Integer(minDetail.getMinProCount());
			Integer minProdocId = new Integer(minDetail.getMinTrCount());
			
			mincount = mindocId.intValue();
			mintrac= minProdocId.intValue();
			
			logger.log(Level.SEVERE,"get tr count --" + mintrac + " min pro countoo " + mincount + " service date -- " + minDetail.getServiceDate());
			SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
			String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (minDetail.getServiceDate()))); 
//			logger.log(Level.SEVERE,"get tr count --" + mintrac + " min pro unit " + minDetail.getUnit() + " uom -- " + minDetail.getUom());
			if(!minDetail.getUom().equalsIgnoreCase("NA")){
				serDetail.setUom(minDetail.getUom());
				
			}
				serDetail.setQuantity(minDetail.getUnit());
			Double dur = minDetail.getServiceComplTime();
			int intdur = dur.intValue();
			serDetail.setBranch(minDetail.getBranch());
			serDetail.setServiceCompleteDuration(intdur);
//			serDetail.setServiceTime(minDetail.getServiceComplTime()+"");
			serDetail.setServiceCompletionDate( DateUtility.getDateWithTimeZone("IST", (minDetail.getServiceComplDate())));
			serDetail.setServiceType(minDetail.getServiType());
			serDetail.setApproverName(minDetail.getAppEmpName());
			/** Date 08-01-2019 by vijay
			 * Des :- Bug :-updated Technician Name
			 */
//			serDetail.setEmployee(minDetail.getAppEmpName());
			if(minDetail.getEmpDetails().size()!=0){
				serDetail.setEmployee(minDetail.getEmpDetails().get(0).getFullName());
			}
			/**
			 * ends here
			 */
			serDetail.setServiceDate(DateUtility.getDateWithTimeZone("IST", (minDetail.getServiceDate())));
			serDetail.setRemark("This service Completed throught upload process - "+ date+" By " + minDetail.getAppEmpName());
			serDetail.setStatus(Service.SERVICESTATUSCOMPLETED);
			serDetail.setServiceDay(serviceDay);
			serDetail.setNumberRange("");
			serDetail.setMINUploaded(true);
			int result  = 0;
			
			if(minDetail.getEmpDetails().size()>0){
				serDetail.setTechnicians(minDetail.getEmpDetails());
			}
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(DateUtility.getDateWithTimeZone("IST", DateUtility.getDateWithTimeZone("IST", (minDetail.getServiceDate()))));
			int week = cal.get(Calendar.WEEK_OF_MONTH);
			serDetail.setServiceWeekNo(week);
		
			
			ofy().save().entity(serDetail);
			/**
			 * Date 14-11-2019
			 * Des :- NBHC CCPM when complaint service getting completed then its complaint also should be complete
			 */
			if(serDetail.getTicketNumber()!=-1){
				Complain complainentity = ofy().load().type(Complain.class).filter("companyId", minDetail.getCompanyId())
										.filter("count", serDetail.getTicketNumber()).first().now();
				if(complainentity!=null){
					complainentity.setCompStatus("Completed");
					ofy().save().entity(complainentity);
				}
			}
			/**
			 * ends here
			 */
			
			if(minDetail.getEmpDetails().size()>0){
				CustomerProjectServiceImpl custProject = new CustomerProjectServiceImpl();
				
				 result =	custProject.checkCustomerProject(minDetail.getCompanyId(), minDetail.getContractCount(), minDetail.getServiceCount(), Service.SERVICESTATUSCOMPLETED, minDetail.getEmpDetails(),serDetail,minDetail.getMinDetails(),new Integer( minDetail.getCustomerProCount()).intValue() );// minDetail.getCustomerProCount());
					
				if(result == 1){
					logger.log(Level.SEVERE," Get customer updated.. ");
				}
				serDetail.setTechnicians(minDetail.getEmpDetails());
			}
			logger.log(Level.SEVERE," get pro detail count --" +minDetail.getMinProdDetails().size() + " min trac +"+ mintrac + "  min count -- " + mincount);
			if(minDetail.getMinProdDetails().size()>0 && mintrac>0 && mincount>0){

				MaterialIssueNote material = new MaterialIssueNote();
					material.setCompanyId(minDetail.getCompanyId());
					material.setMinTitle("This material is issued from ");
					material.setMinDate(DateUtility.getDateWithTimeZone("IST",(minDetail.getServiceComplDate())));
//					material.setMinSoDeliveryDate(DateUtility.getDateWithTimeZone("IST",(minDetail.getServiceDate())));
					material.setServiceId(minDetail.getServiceCount());
					material.setMinSoId(minDetail.getContractCount());
					material.setMinSoDate(serDetail.getContractStartDate());
					material.setBranch(minDetail.getBranch());
					material.setMinSalesPerson(minDetail.getAppEmpName());		
					material.setEmployee(minDetail.getAppEmpName());
					material.setMinIssuedBy(minDetail.getAppEmpName());
					material.setApproverName(minDetail.getAppEmpName());
					material.setMinTitle("Service - ID"+ minDetail.getServiceCount());
					material.setStatus(material.APPROVED);
				
					material.setMinIssuedBy(minDetail.getAppEmpName());
//					material.set
					System.out.println(sdf.format(date));
					material.setMinDescription("This record is created through excel upload program on  - " + date+" By " + minDetail.getAppEmpName());
					material.setApprovalDate( DateUtility.getDateWithTimeZone("IST", (date)));
					logger.log(Level.SEVERE,"get date - "+sdf.format(date) );
					material.setSubProductTablemin(minDetail.getMinProdDetails());
					++mincount;
					material.setCount(mincount);
					logger.log(Level.SEVERE,"get min count --" + material.getCount() +"  service id --" + material.getServiceId());
					material.setCinfo(serDetail.getPersonInfo());
				
					ofy().save().entity(material);
					
					int updateValue = InventoryStockUpdate(material,mintrac);
					boolean bomProcessActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", material.getCompanyId());
					if(bomProcessActive && material.getServiceId()!=0){
						logger.log(Level.SEVERE,"get bom values -- " + bomProcessActive);
						BOMProductListMappingTaskQueue bomUtility = new BOMProductListMappingTaskQueue();
						Thread.sleep(5);
						bomUtility.minProListMapping(material, serDetail);
						
						}
				
			}
			
		
			/**
			 * Date : 23-12-2019 BY ANIL
			 * for clearing all loaded data from appengine cache memory
			 */
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			/**
			 * End
			 */
			
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			logger.log(Level.SEVERE, " error in min upload.."+e);
		}
	}
	
	
	public int InventoryStockUpdate(MaterialIssueNote material,int mintrac){
		int  i =0;
		try {
			DecimalFormat df=new DecimalFormat("0.0000");
			ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
			for(MaterialProduct product: material.getSubProductTablemin()){
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(material.getCompanyId());
				item.setBranch(material.getBranch());
				item.setDocumentType(AppConstants.MIN);
				item.setDocumentId(material.getCount());
				item.setDocumentDate(material.getMinDate());
				
				item.setDocumnetTitle(material.getMinTitle());
				if(material.getServiceId()>0)
					item.setServiceId(material.getServiceId()+"");
				item.setProductUOM(product.getMaterialProductUOM());
				item.setProdId(product.getMaterialProductId());
				item.setUpdateQty(product.getMaterialProductRequiredQuantity());
				/**
				 * nidhi ||*||
				 */
				item.setPlannedQty(product.getMaterialProductPlannedQuantity());
				/**
				 * end
				 */
				item.setOperation(AppConstants.SUBTRACT);
				item.setWarehouseName(product.getMaterialProductWarehouse());
				item.setStorageLocation(product.getMaterialProductStorageLocation());
				item.setStorageBin(product.getMaterialProductStorageBin());
				item.setOpeningStock(product.getMaterialProductAvailableQuantity());
				item.setClosingStock(Double.parseDouble(df.format(product.getMaterialProductAvailableQuantity()- product.getMaterialProductRequiredQuantity())));
				itemList.add(item);
			}
			
			i = this.setProductInventory(itemList,material,mintrac);
			
//			for(InventoryTransactionLineItem inv : itemList){
//				ArrayList<InventoryTransactionLineItem> mainitemList=new ArrayList<InventoryTransactionLineItem>();
//				mainitemList.add(inv);
//				
//			}
//		i =	this.setProductInventory(itemList);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			logger.log(Level.SEVERE,"InventoryStockUpdate-- +"+e.getLocalizedMessage());
		}
		return i;
	}
	
	private int setProductInventory(ArrayList<InventoryTransactionLineItem> itemList, MaterialIssueNote material,int mintrac){
		
		DecimalFormat df=new DecimalFormat("0.0000");
		HashSet<Integer> hset=new HashSet<Integer>();
		for(InventoryTransactionLineItem obj:itemList){
			hset.add(obj.getProdId());
		}
		List<Integer> prodIdList=new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList=ofy().load().type(ProductInventoryView.class).filter("companyId", itemList.get(0).getCompanyId()).filter("productinfo.prodID IN", prodIdList).list();
		List<ProductInventoryViewDetails> prodList=new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		
		for(ProductInventoryView piv:prodInvViewList){
			for(InventoryTransactionLineItem item:itemList){
				if(piv.getProdID()==item.getProdId()){
					piv.setEditFlag(false);
					double requestedProductQty=0;
					double openingStock=0;
					double closingStock=0;
					for(ProductInventoryViewDetails prod:piv.getDetails()){
						if(prod.getWarehousename().equals(item.getWarehouseName())
								&&prod.getStoragelocation().equals(item.getStorageLocation())
								&&prod.getStoragebin().equals(item.getStorageBin())){
							/*requestedProductQty=item.getUpdateQty();
							openingStock=prod.getAvailableqty();
							if(item.getOperation().equals("Add")){
								closingStock=openingStock+requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
							}
							if(item.getOperation().equals("Subtract")){
								closingStock=openingStock-requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								try {
									validateStock(closingStock);
								} catch (InvalidTransactionException e) {
									e.printStackTrace();
									prod.setWarehousename(null);
								}
							}
							ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class)
									.filter("companyId", piv.getCompanyId())
									.filter("prodInvViewCount",prod.getProdid())
									.filter("warehousename", prod.getWarehousename().trim())
									.filter("storagelocation", prod.getStoragelocation().trim())
									.filter("storagebin", prod.getStoragebin().trim()).first().now();
							if(prodInvDtls!=null){
								prodInvDtls.setAvailableqty(Double.parseDouble(df.format(closingStock)));
//								prodList.add(prodInvDtls);
								ofy().save().entities(prodInvDtls);
								ofy().save().entities(piv);
								
							}
							
							
							
							item.setOpeningStock(openingStock);
							item.setClosingStock(Double.parseDouble(df.format(closingStock)));*/
							
//							for(MaterialIssueNote min : material)
							
							item.setProductName(piv.getProductName());
							item.setProductPrice(piv.getProductPrice());
							item.setProductUOM(piv.getProductUOM());
							
							/**
							 * Date : 02-03-2017 By Anil
							 * setting product code
							 */
							item.setProductCode(piv.getProductCode());
							
							ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item);
							prodInvTran.setInventoryTransactionDateTime(material.getMinDate());
							++mintrac;
							prodInvTran.setCreationDate(item.getDocumentDate());
							prodInvTran.setCount(mintrac);
							transactionList.add(prodInvTran);
							
							logger.log(Level.SEVERE, " get stock detail -- " +prodInvTran.getCount()+ " count -- " +mintrac);
						}
					}
				}
			}
		}
		
//		SuperModel modelList = null;
//		for(ProductInventoryTransaction obj:transactionList){
//			modelList=obj;
//		}
//		ofy().save().entities(prodInvViewList);
//		ofy().save().entities(prodList);
//		GenricServiceImpl impl=new GenricServiceImpl();
//		ReturnFromServer serret =	impl.save(modelList);
		
		ArrayList<SuperModel>modelList=new ArrayList<SuperModel>();
		for(ProductInventoryTransaction obj:transactionList){
			logger.log(Level.SEVERE, " get stock detail -- " +obj.getCount()+ " count -- " + obj.getDocumentId());
			SuperModel model=obj;
			modelList.add(model);
		}
		
		ofy().save().entities(modelList);
//		GenricServiceImpl impl=new GenricServiceImpl();
//		impl.save(modelList);
		return 1;
	}
	
	static class InvalidTransactionException extends Exception{
		public InvalidTransactionException(String message) {
			super(message);
		}
	}
	
	public static void validateStock(double closingStock) throws InvalidTransactionException{
		System.out.println("Validating STOCK....!!!!");
		if(closingStock<0){  
		  throw new InvalidTransactionException("CLOSING STOCK IS GOING NEGATIVE....!!!!");  
		}
	}
	
	public static ProductInventoryTransaction getProductInventoryTransaction(InventoryTransactionLineItem item){
		ProductInventoryTransaction prodInvTran=new ProductInventoryTransaction();
		
		prodInvTran.setProductId(item.getProdId());
		prodInvTran.setProductName(item.getProductName());
		prodInvTran.setProductPrice(item.getProductPrice());
		prodInvTran.setProductQty(item.getUpdateQty());
		prodInvTran.setProductUOM(item.getProductUOM());
		prodInvTran.setWarehouseName(item.getWarehouseName());
		prodInvTran.setStorageLocation(item.getStorageLocation());
		prodInvTran.setStorageBin(item.getStorageBin());
		prodInvTran.setDocumentType(item.getDocumentType());
		prodInvTran.setDocumentId(item.getDocumentId());
		prodInvTran.setDocumentTitle(item.getDocumnetTitle());
		prodInvTran.setDocumentDate(item.getDocumentDate());
		prodInvTran.setOperation(item.getOperation());
		prodInvTran.setOpeningStock(item.getOpeningStock());
		prodInvTran.setClosingStock(item.getClosingStock());
		prodInvTran.setCompanyId(item.getCompanyId());
		prodInvTran.setBranch(item.getBranch());
//		prodInvTran.setInventoryTransactionDateTime();
		if(item.getServiceId()!=null&&!item.getServiceId().equals(""))
			prodInvTran.setServiceId(item.getServiceId());
		/**
		 * Date :01-03-2017 By ANIL
		 */
		if(item.getDocumentSubType()!=null){
			prodInvTran.setDocumentSubType(item.getDocumentSubType());
		}
		
		/**
		 * Date : 02-03-2017 By ANIL
		 */
		
		if(item.getProductCode()!=null){
			prodInvTran.setProductCode(item.getProductCode());
		}
		/**
		 * nidhi ||*||
		 */
		prodInvTran.setPlannedQty(item.getPlannedQty());
		/**
		 * end
		 */
//		ofy().save().entities(prodInvViewList);
		
		return prodInvTran;
		
	}
	
	
	
	private void accountingInterface(MaterialIssueNote material) {

		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", material.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGMIN)
					.filter("companyId", material.getCompanyId())
					.filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag = 0;

		if (processConfig != null) {
			System.out.println("Process Config Not Null");
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				System.out
						.println("----------------------------------------------------------");
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i)
								.getProcessType());
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i).isStatus());
				System.out
						.println("----------------------------------------------------------");
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {
			System.out.println("ExecutingAccInterface");

			if (material.getCount() != 0) {

				System.out.println("Product List Size :: "
						+ material.getProductTablemin().size());

				/*************************************************************************************************************/
				for (int i = 0; i < material.getSubProductTablemin().size(); i++) {

					/************************************************* Start ************************************************/

					String unitofmeasurement = material.getSubProductTablemin()
							.get(i).getMaterialProductUOM();
					int prodId = material.getSubProductTablemin().get(i)
							.getMaterialProductId();
					String productCode = material.getSubProductTablemin().get(i)
							.getMaterialProductCode();
					String productName = material.getSubProductTablemin().get(i)
							.getMaterialProductName();
					double productQuantity = material.getSubProductTablemin()
							.get(i).getMaterialProductRequiredQuantity();
					double availableQuantity = material.getSubProductTablemin()
							.get(i).getMaterialProductAvailableQuantity();
					String remarks = material.getSubProductTablemin().get(i)
							.getMaterialProductRemarks().trim();
					String warehouseName = material.getSubProductTablemin().get(i)
							.getMaterialProductWarehouse().trim();
					System.out.println("warehouseName" + warehouseName);
					WareHouse warehouse = ofy().load().type(WareHouse.class)
							.filter("companyId", material.getCompanyId())
							.filter("buisnessUnitName", warehouseName).first()
							.now();
					System.out.println("warehouse" + warehouse);
					String warehouseCode = "";
					if (warehouse.getWarehouseCode().trim() != null) {
						warehouseCode = warehouse.getWarehouseCode().trim();
					}
					String storageLocation = material.getSubProductTablemin()
							.get(i).getMaterialProductStorageLocation().trim();
					String storageBin = material.getSubProductTablemin().get(i)
							.getMaterialProductStorageBin().trim();

					UpdateAccountingInterface.updateTally(
							new Date(),// accountingInterfaceCreationDate
							material.getEmployee(),// accountingInterfaceCreatedBy
							material.getStatus(),
							AppConstants.STATUS,// Status
							"",// remark
							"Inventory",// module
							"MIN",// documentType
							material.getCount(),// docID
							material.getMinTitle(),// DOCtile
							material.getMinDate(), // docDate
							AppConstants.DOCUMENTGL,// docGL
							material.getMinMrnId() + "",// ref doc no.1 (Work Order
													// Id)
							material.getMinMrnDate(),// ref doc date.1 (Work Order
													// Date)
							AppConstants.REFDOCMRN,// ref doc type 1
							material.getMinSoId() + "",// ref doc no 2 (Order ID)
							material.getMinSoDate(),// ref doc date 2 (Order Date)
							AppConstants.REFDOCMIN,// ref doc type 2
							"",// accountType
							0,// custID
							"",// custName
							0l,// custCell
							0,// vendor ID
							"",// vendor NAme
							0l,// vendorCell
							0,// empId
							" ",// empNAme
							material.getBranch(),// branch
							material.getMinIssuedBy(),// personResponsible (Issued
													// By)
							material.getEmployee(),// requestedBY
							material.getApproverName(),// approvedBY
							"",// paymentmethod
							null,// paymentdate
							"",// cheqno.
							null,// cheqDate
							null,// bankName
							null,// bankAccount
							0,// transferReferenceNumber
							null,// transactionDate
							null,// contract start date
							null, // contract end date
							prodId,// prod ID
							productCode,// prod Code
							productName,// productNAme
							productQuantity,// prodQuantity
							null,// productDate
							0,// duration
							0,// services
							unitofmeasurement,// unit of measurement
							0.0,// productPrice
							0.0,// VATpercent
							0.0,// VATamt
							0,// VATglAccount
							0.0,// serviceTaxPercent
							0.0,// serviceTaxAmount
							0,// serviceTaxGLaccount
							"",// cform
							0,// cformpercent
							0,// cformamount
							0,// cformGlaccount
							0.0,// totalamouNT
							0.0,// NET PAPAYABLE
							"",// Contract Category
							0,// amount Recieved
							0.0,// base Amt for tdscal in pay by rohan
							0.0, // tds percentage by rohan
							0.0,// tds amount by rohan
							"", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "",
							0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0,
							"", 0.0, "", "", "", "", "", "", 0l,
							material.getCompanyId(), null, // billing from date
														// (rohan)
							null, // billing to date (rohan)
							warehouseName, // Warehouse
							warehouseCode, // warehouseCode
							"", // ProductRefId
							AccountingInterface.OUTBOUNDDIRECTION, // Direction
							AppConstants.CCPM, // sourceSystem
							AppConstants.NBHC, // Destination System
							null,null,null
							);

				}

			}
		}
	}
}
