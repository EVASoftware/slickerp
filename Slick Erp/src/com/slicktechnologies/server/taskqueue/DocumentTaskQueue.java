package com.slicktechnologies.server.taskqueue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.jetty.util.ajax.JSON;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.LoadType;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.CustomerNameChangeServiceImpl;
import com.slicktechnologies.server.DataMigrationImpl;
import com.slicktechnologies.server.GeneralServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.ServerUnitConversionUtility;
import com.slicktechnologies.server.ServiceImplementor;
import com.slicktechnologies.server.ServiceListServiceImpl;
//import com.slicktechnologies.server.ServiceSchedulingNotification;
import com.slicktechnologies.server.UpdateServiceImpl;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.android.MarkCompletedSubmitServlet;
import com.slicktechnologies.server.android.technicianapp.ServiceSchedulingNotification;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.materialconsumption.MaterialConsumptionImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.ScreenName;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.LetterOfIntent;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.RequsestForQuotation;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigrationDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;
import com.slicktechnologies.shared.common.sms.SMSDetails;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.screenmenuconfiguration.MenuConfiguration;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.cronjobimpl.UpdateDocumentCronJobImpl;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;

import static com.googlecode.objectify.ObjectifyService.ofy;


/**
 * Date 04-05-2018
 * Developer :- Vijay
 * Description :- This task queue is common for all task queue operation 
 * we can pass operation Name and also required parapeters and process id
 */
public class DocumentTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4290358487635989747L;
	
	Logger logger = Logger.getLogger("Name of logger");

	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdf= new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");


	public static ArrayList<Service> servicelist;
	
	/**
	 * Inner map will contain Name as a key and link/url as value
	 * outer map will have key in combination of module name,document name and action name i.e(Upload,Report,Help)
	 */
	HashMap<String,HashMap<String,String>>moduleWiseMenuConfgMap=new HashMap<String,HashMap<String,String>>();
	
	/**
	 * here key will be combination of user role, module name and document name
	 * value will be screen name
	 */
	HashMap<String,HashMap<String,ArrayList<String>>> userRoleMap=new HashMap<String,HashMap<String,ArrayList<String>>>();
	/**
	 * Here key will be event name and 
	 * value will be sms template.
	 */
	
	HashMap<String,String> smsTemplateMap=new HashMap<String,String>();
	Company companyobj;
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {

		 String operationName = request.getParameter("OperationName");
		 String compId = request.getParameter("companyId");
		 long companyId = 0;
		 if(compId!=null){
			 companyId = Long.parseLong(request.getParameter("companyId"));
		 }

		 
		 switch (operationName) {
		 
			case "ServiceMarkComplete":
				String Remark = request.getParameter("Remark");
				String strServiceCompletionDate = request.getParameter("ServiceCompletionDate");
				Date serviceCompletionDate = null;
				if(!strServiceCompletionDate.equals("NoDate")){
					try {
						serviceCompletionDate = fmt.parse(strServiceCompletionDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			//	logger.log(Level.SEVERE, "list before : " + servicelist.size());
				//if(servicelist == null || servicelist.size() == 0){
				companyId = Long.parseLong(request.getParameter("companyId"));
					logger.log(Level.SEVERE, "new service list : static null");
					 ArrayList<Integer> serviceIdList = new ArrayList<Integer>();
					 String str = request.getParameter("serviceId");
					 ArrayList<Service> serviceList = new ArrayList<Service>();
					 try{
							Gson gson = new GsonBuilder().create();	
							JSONArray jsonarr=new JSONArray(str);
							logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
							for(int i=0;i<jsonarr.length();i++){
								serviceIdList.add(jsonarr.getInt(i));
							//	logger.log(Level.SEVERE, "id: " + jsonarr.getInt(i));
							}
							logger.log(Level.SEVERE, "serviceIdList size"+serviceIdList.size());

							List<Service> serList = ofy().load().type(Service.class).filter("companyId", companyId)
								.filter("count IN",serviceIdList).list();
							serviceList.addAll(serList);
							logger.log(Level.SEVERE, "list after : " + serList);
							logger.log(Level.SEVERE, "list after : " + serList.size());
					 }catch(Exception e){
						e.printStackTrace(); 
					 }
					 
					 try {
					 if(serviceList.size()>0) {
						 boolean isservicewisebilling = false;
						 for(Service serviceEntity : serviceList){
							 if(serviceEntity.isServiceWiseBilling()){
								 isservicewisebilling = true;
								 ServiceImplementor serviceimplementor = new ServiceImplementor();
								 serviceimplementor.createBillOnServiceCompletion(serviceEntity);
							 }
								
						 }
						if(isservicewisebilling) {
							Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "MarkCompleteOnlyServices").param("Remark", Remark)
									.param("ServiceCompletionDate", strServiceCompletionDate).param("serviceId", str).param("companyId", companyId+""));
						}
						else {
							ServiceListServiceImpl servicelistimpl = new ServiceListServiceImpl();	
							servicelistimpl.multipleServicesMarkComplete(serviceList, Remark, serviceCompletionDate); 
						}
						
					 }
					
					} catch (Exception e) {
						e.printStackTrace();
					}
				break;
	
			case "UpdateFollowUpDatePaymentDocs":
				long companyid = Long.parseLong(request.getParameter("companyId"));
				updatePaymentDocsWithFollowUpDate(companyid);
				break;
				
			case "UpdateProductInventoryWithWarehouseType":	
				UpdateProductInventoryWithWarehouseType(companyId);
				break;
			
		/***Date 21-11-2019 commented by amol as per discussion with Vijay sir and make a new Tasqueue for customername change**/	
//				
//			case "CustomerNameChangeUpdation":
//				customerNameUpdation(request,companyId);
//				break;
			case "CreateStorageLocationAndStorageBinForWarehouse":
				CreateStorageLocationBinForWarehouse(companyId);
				break;
				
				/** date 10.4.2019 added by komal to update services **/
			case "UpdateServices":
				 String fromDate = request.getParameter("fromDate");
				 String toDate = request.getParameter("toDate");
				 String customerId = request.getParameter("customerId");
				 String branch = request.getParameter("branch");
				 UpdateServices(companyId , fromDate , toDate ,customerId , branch);
				 break;
			/** date 11.4.2019 added by komal to send email with BOM report**/	 
			case "BOMReport":
				reactOnBOMReport(request,companyId);
				break;
				
			case "updateServiceValueForRateContracts":
				reactonUpdateServiceValueforRateContracts(request,companyId);
				break;
				
			case "UpdateProjectMaterial":	
				updateProjectMaterial(request , companyId);
				logger.log(Level.SEVERE, "intask queue");
				break;
			case "SendSRMail":
				sendSrCopyMail(request , companyId);
				break;
				
			case "createTechnicianMMN":
				 createTechnicianMMN(request , companyId);
				 break;
				 
						 
//			case "RevenueLossReportWithMail":
//				sendServiceRevenueLossReport(request,companyId);
//				break;
//	
				 
				 
			
			case "UpdateServicingBranchInServices":
				 updateServicingBranchInServices(request,companyId);
				 break;
			case "ServiceMarkCancel":
				String remark = request.getParameter("Remark");
				String user = request.getParameter("user");
				 List<Integer> serviceIdList1 = new ArrayList<Integer>();
				 String str1 = request.getParameter("serviceId");
				 try{
						Gson gson = new GsonBuilder().create();	
						JSONArray jsonarr=new JSONArray(str1);
						logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
						for(int i=0;i<jsonarr.length();i++){
							serviceIdList1.add(jsonarr.getInt(i));
						}
						ArrayList<Service> servicelist = new ArrayList<Service>();
						List<Service> serList = ofy().load().type(Service.class).filter("companyId", companyId)
								.filter("count IN", serviceIdList1).list();
						servicelist.addAll(serList);
						logger.log(Level.SEVERE, "list after : " + servicelist.size());
						
						UpdateServiceImpl serImpl = new UpdateServiceImpl();
						serImpl.cancelAllServices(servicelist, remark, user);
				 }catch(Exception e){
					 
				 }
					
				break;
				

			case "SendMultipleSrCopies" :
				 companyId = Long.parseLong(request.getParameter("companyId"));
				 Date from = null , to = null;
				 try{
					 from = sdf.parse(request.getParameter("fromDate"));
					 logger.log(Level.SEVERE, "From Date :" + from);
				 }catch(Exception e){
					 
				 }
				try{
					 to = sdf.parse(request.getParameter("toDate"));
					 to = DateUtility.addDaysToDate(to, 1);
					 logger.log(Level.SEVERE, "To date :" + to);
				 }catch(Exception e){
					 
				 }
				 int id = 0;
				 try{
				  id =Integer.parseInt(request.getParameter("serviceId"));
				 }catch(Exception e){
					 
				 }
				 branch = request.getParameter("branch");
				
				 ArrayList<String> emailList = new ArrayList<String>();
				 Gson gson = new GsonBuilder().create();	
				 str = request.getParameter("email");
				 try{
						JSONArray jsonarr=new JSONArray(str);
						logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
						for(int i=0;i<jsonarr.length();i++){
							emailList.add(jsonarr.getString(i));
						}
				 }catch(Exception e){
					 
				 }
				 int contractId = Integer.parseInt(request.getParameter("contract"));
				 
				 Contract contract = ofy().load().type(Contract.class).filter("companyId", companyId)
						 				.filter("count", contractId).first().now();
				 logger.log(Level.SEVERE, companyId +" "+ from+" "+to+" "+branch+" "+id+" "+emailList.toString()+" "+contract);
				 CommonServiceImpl impl = new CommonServiceImpl();
				 impl.loadSRCopyData(companyId, from, to, branch, id,emailList,contract);

				 break;
				 
			case "SendSummarySrCopies" :
				 companyId = Long.parseLong(request.getParameter("companyId"));
				 Date from1 = null , to1 = null;
				 try{
					 from1 = sdf.parse(request.getParameter("fromDate"));
					 logger.log(Level.SEVERE, "From Date :" + from1);
				 }catch(Exception e){
					 try {
						from1 = fmt.parse(request.getParameter("fromDate"));
						 logger.log(Level.SEVERE, "From Date = " + from1);
					} catch (ParseException e1) {
						e1.printStackTrace();
					}

				 }
				try{
					 to1 = sdf.parse(request.getParameter("toDate"));
					 to1 = DateUtility.addDaysToDate(to1, 1);
					 logger.log(Level.SEVERE, "To date :" + to1);
				 }catch(Exception e){
					 try {
						 to1 = fmt.parse(request.getParameter("toDate"));
						 logger.log(Level.SEVERE, "To date = " + to1);
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
				 }
				 int id1 = 0;
				 try{
				  id1 =Integer.parseInt(request.getParameter("serviceId"));
				 }catch(Exception e){
					 
				 }
				 branch = request.getParameter("branch");
				
				 ArrayList<String> emailList1 = new ArrayList<String>();
				 Gson gson1 = new GsonBuilder().create();	
				 str = request.getParameter("email");
				 try{
						JSONArray jsonarr=new JSONArray(str);
						logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
						for(int i=0;i<jsonarr.length();i++){
							emailList1.add(jsonarr.getString(i));
						}
				 }catch(Exception e){
					 
				 }
				 int contractId1 = Integer.parseInt(request.getParameter("contract"));
				 
				 Contract contract1 = ofy().load().type(Contract.class).filter("companyId", companyId)
						 				.filter("count", contractId1).first().now();
				 
				 String reportName="";
				 reportName=request.getParameter("reportName");
				 
				 String reportFormat="";
				 reportFormat=request.getParameter("reportFormat");
				 
				 boolean completedFlag=Boolean.parseBoolean(request.getParameter("completedFlag"));
				 boolean cancelledFlag=Boolean.parseBoolean(request.getParameter("cancelledFlag"));
				 boolean otherFlag=Boolean.parseBoolean(request.getParameter("otherFlag"));
				 
				 logger.log(Level.SEVERE, companyId +" "+ from1+" "+to1+" "+branch+" "+id1+" "+emailList1.toString()+" "+contract1);
				 CommonServiceImpl impl1 = new CommonServiceImpl();
				 impl1.loadSummarySRCopyData(companyId, from1, to1, branch, id1,emailList1,contract1,reportName,reportFormat,completedFlag,cancelledFlag,otherFlag);
				 break;
				 
			case "SendCTCEmail": //Ashwini Patil Date: 31-03-2022 
				companyId = Long.parseLong(request.getParameter("companyId"));
				ArrayList<String> emailList2 = new ArrayList<String>();
				 Gson gson2 = new GsonBuilder().create();	
				 str = request.getParameter("email");
				 try{
						JSONArray jsonarr=new JSONArray(str);
						logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
						for(int i=0;i<jsonarr.length();i++){
							emailList2.add(jsonarr.getString(i));
						}
				 }catch(Exception e){
					 
				 }
				CommonServiceImpl impl2 = new CommonServiceImpl();
				impl2.createAndSendCTCEmail(companyId,emailList2);
				break;
				 
				 
				case "Delete Cancelled Services":
				deleteCancelledServices(companyId);
				break;
				
				case "Update WMS Flag Services":
					long updatedNumber = Long.parseLong(request.getParameter("Number"));
					updateServices(updatedNumber,companyId);
					break;
					// 
				case "Update CustomerBranch ServiceAddress":
					String customerBranch = request.getParameter("customerBranch");
					String strcontractId = request.getParameter("contractId");
					reactonUpdateCustomerBranchServices(companyId,strcontractId,customerBranch);
					break;
					
				case "Update Module And Document Name":
					String accessURL = request.getParameter("accessUrl");
					reactOnUpdateModuleAndDocumentNames(accessURL);
					break;	
					
				case "Define User Role ":
					reactOnDefineUserRole(companyId);
					break;
					
				case "Default Configuration":
					reactOnDefaultConfiguration(companyId);
					break;	
				case "Update Employee Site Location":
					int employeeId = Integer.parseInt(request.getParameter("employeeId"));
					String projectName = request.getParameter("projectName");
					String empSiteLocation = request.getParameter("empSiteLocation");
					reactonUpdateEmployeeSiteLocationInproject(companyId,employeeId,projectName,empSiteLocation);
					break;	
					
				case "Update Station Technician Contract Into Normal Contract":
					String strconId = request.getParameter("ContractId");
					int conid = Integer.parseInt(strconId);
					reactonConvertStationTechnicianContractIntoNormalContract(companyId,conid);
					break;	
					
				case "Update Normal Contract Into Station Technician Contract":
					String strcontratId = request.getParameter("ContractId");
					int contratid = Integer.parseInt(strcontratId);
					reactonConvertNormalContractIntoStationTechnicianContract(companyId,contratid);
					break;	
					
				case "Send Email":
//					String emaildetail = request.getParameter("emailDetails");
					
					EmailDetails emaildetails = new EmailDetails();
					int count = Integer.parseInt(request.getParameter("count"));
					emaildetails.setFromEmailid(request.getParameter("fromEmail"));
					emaildetails.setEmailBody(request.getParameter("emailBody"));
					emaildetails.setSubject(request.getParameter("subject"));
					emaildetails.setLandingPageText(request.getParameter("landingpageText"));
					emaildetails.setAppId(request.getParameter("appId"));
					if(request.getParameter("pdfattachment").equalsIgnoreCase("yes")) {
						emaildetails.setPdfAttachment(true);
					}
					else {
						emaildetails.setPdfAttachment(false);
					}
					emaildetails.setPdfURL(request.getParameter("pdfURL"));
					emaildetails.setEntityName(request.getParameter("entityName"));
					String uploadDocument = request.getParameter("uploadDocument");
					
					emaildetails.setToEmailId(getemailids(request.getParameter("toEmail")));
					emaildetails.setCcEmailId(getemailids(request.getParameter("ccEmail")));
					emaildetails.setBccEmailId(getemailids(request.getParameter("bccEmail")));
					emaildetails.setAppURL(request.getParameter("appURL"));

					reactonSendEmail(emaildetails,uploadDocument,count,companyId);
					break;
				case "copyConfiguration":
					 copyConfiguration(request , companyId);
					 break;
					
				case "Update Service Value":
					 UpdateServiceValue(request,companyId);
					 break;
				case "MarkCompleteOnlyServices":
						markcompleteServices(request,companyId);
					 break;
					 
				case "copyPedioConfiguration":
						copyPedioConfiguration(request,companyId);
				 break; 
				 
				case "shareCustomerPortalLink": //Ashwini Patil Date: 25-01-2023 
					companyId = Long.parseLong(request.getParameter("companyId"));	
					boolean smsflag=Boolean.parseBoolean(request.getParameter("smsflag"));
					boolean whatsappflag=Boolean.parseBoolean(request.getParameter("whatsappflag"));
					boolean emailflag=Boolean.parseBoolean(request.getParameter("emailflag"));
					reactOnShareCustomerPortalLink(companyId,smsflag,whatsappflag,emailflag);
					break;
					
				case "SendContractDataOnEmail":
					sendContractDataOnEmail(request,companyId);
					break;
					
				case "sendSRFormatVersion1OnEmail":
					sendSrCopyFormatVersion1OnEmail(request,companyId);
					break;
				case "employeeUploadPartly": //Ashwini Patil Date:20-05-2023
					logger.log(Level.SEVERE, " step 2 employeeUploadPartly");
					companyId = Long.parseLong(request.getParameter("companyId"));	
					String jsondata=request.getParameter("excelData");
					String loggedinuser=request.getParameter("loggedinuser");
					DataMigrationImpl dataimpl = new DataMigrationImpl();
					
					try {
						JSONObject jobj=new JSONObject(jsondata);
						JSONArray dataArray;
						dataArray = jobj.getJSONArray("datalist");
						ArrayList<String> exceldatalist=new ArrayList<String>();
						int length=dataArray.length();
						for(int i=0;i<length;i++){
							exceldatalist.add((String) dataArray.get(i));
						}
						logger.log(Level.SEVERE, " step 2 employeeUploadPartly exceldatalist size="+exceldatalist.size());
						dataimpl.saveEmployeeData(exceldatalist, companyId,loggedinuser);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					break;
					
				case "Suspend Services":
					String suspendremark = request.getParameter("Remark");
					 List<Integer> serviceIdList2 = new ArrayList<Integer>();
					 String str2 = request.getParameter("serviceId");
					 try{
							Gson gson3 = new GsonBuilder().create();	
							JSONArray jsonarr=new JSONArray(str2);
							logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
							for(int i=0;i<jsonarr.length();i++){
								serviceIdList2.add(jsonarr.getInt(i));
							}
							ArrayList<Service> servicelist = new ArrayList<Service>();
							List<Service> serList = ofy().load().type(Service.class).filter("companyId", companyId)
									.filter("count IN", serviceIdList2).list();
							servicelist.addAll(serList);
							logger.log(Level.SEVERE, "list after : " + servicelist.size());
							
							CommonServiceImpl commonservice = new CommonServiceImpl();
							commonservice.suspendAllServices(servicelist, suspendremark);
							
					 }catch(Exception e){
						 
					 }
					break;
					
				case "DeleteAndCreateAccountingInterface":
					deleteAndCreateAccountingInterface(request,companyId);
					break;
					
					
				case "DeleteAndCreateAccountingInterfacePaymentDocuments":
					deleteAndCreateAccountingInterfacePaymentDocument(request,companyId);
					break;
					
					
				case "Cancel Services":
					CancelServices(request,companyId);
					break;	
						
				case "Update Customer Branch Service Address":
					UpdateCustomerBranchServiceAddress(request,companyId);
					break;
					
				case "updateCustomerSignatureInService":
					updateCustomerSignatureInService(request,companyId);
					break;
				case "UpdateCustomerIdInAllDocs":
					UpdateCustomerIdInAllDocs(request,companyId);
					break;
				
			default:
				break;
		}
	}
	
	



	














	




















	









































	private void reactOnDefineUserRole(long companyId) {
		// TODO Auto-generated method stub
		createuserAuthorizationRoles(companyId);
	}



	/**
	 *  Date : 10/02/2021 By Priyanka.
	 *  Des : Default Configuration Requirement.
	 */
	public void createScreenMenuConfgData(){
		
		String key="";
		HashMap<String,String> innerMap=null;
		
		/****************************************************Service Module Start******************************************************************************/
		
		/*********************************************************** CUSTOMER SCREEN ***************************************************/
		key="Service"+"$"+"Customer"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Customer With Service Details", "https://drive.google.com/file/d/1vEs_VjQhzesBb25l65VUCjA12CPeUfQT/view?usp=sharing");
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		innerMap.put("Customer Branches", "https://drive.google.com/file/d/1eLrAW7zdioaTOfv-ZC5-wQrm1aD9_d41/view?usp=sharing");
		innerMap.put("Customer", "https://drive.google.com/file/d/1b3VyXAT7-0JXkABIsc6rGjKitoAqu-BC/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);

		/***********************************************************End CUSTOMER SCREEN ***************************************************/
		
		/*******************************************************************Lead**********************************************************/
		key="Service"+"$"+"Lead"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Service"+"$"+"Lead"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Lead", "https://youtu.be/NtLW7n-Tb3s");
		innerMap.put("Create Service Product", "https://youtu.be/QcA5JXhlqrQ");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************End Lead**********************************************************/
		
		/************************************************************QUOTATION SCREEN******************************************************/
		
		key="Service"+"$"+"Quotation"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Service"+"$"+"Quotation"+"$"+"Help";
		
		innerMap.put("Create Quotation", "https://youtu.be/0mKvqg4r-vs");
		innerMap.put("Define Quotation Priority", "https://youtu.be/rLR5pcPOUIU");
		innerMap.put("Edit Quotation", "https://youtu.be/0yJdJmM5Bp0");
		innerMap.put("Define Bank Details", "https://youtu.be/ODxjXwHcztE");
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/*************************************************************** End QUOTATION SCREEN*********************************************/
		
		/***************************************************************Contract screen***************************************************/
		key="Service"+"$"+"Contract"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Customer With Service Details", "https://drive.google.com/file/d/1vEs_VjQhzesBb25l65VUCjA12CPeUfQT/view?usp=sharing"); // check the link
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		innerMap.put("Contract", "https://docs.google.com/spreadsheets/d/19dPUcXWOCpBDKn6yEXU-NWRlKyoPYnAB/edit#gid=632787216");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Service"+"$"+"Contract"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Configure Contract Type", "https://youtu.be/ETg87QpScOs");
		innerMap.put("Configure Contract Group", "https://youtu.be/oYlUj-aB35Q");
		innerMap.put("Upload Customer, Customer Branches & Contracts", "https://youtu.be/FbyZfy3D_Bs");
		innerMap.put("Update Component", "https://youtu.be/_mEK0gt9vKk");
		innerMap.put("Mark contract as Never Renew", "https://youtu.be/14YWwIy9820"); //    Contract Is Not Getting Renewed
		innerMap.put("Set up Renewal Reminder", "https://youtu.be/14YWwIy9820");
		innerMap.put("Renew Bulk Contracts", "https://youtu.be/Xb6_KJRvz30");
		innerMap.put("Renew Single Contract", "https://youtu.be/uLIXhiwd55s");
		innerMap.put("Postpone Contract Renewal", "https://youtu.be/5TS919G8NO8");
		innerMap.put("Create Invoice ", "https://youtu.be/FvfyckcHGh0");
		innerMap.put("Set Up Multi Level Approval For Contract", "https://youtu.be/MlQnReELjqU");
		innerMap.put("Cancel Contract", "https://youtu.be/LTUsxRqER6E");
		innerMap.put("Create Multi Branch Contract", "https://youtu.be/bKVf4t9dVmw");
		innerMap.put("Bill After Service Completion", "https://youtu.be/5CU5z1P1wkk");
		innerMap.put("Create AMC Contract", "https://youtu.be/TmPPAA_ZnH4");
		innerMap.put("Set up Customer Contacts", "https://youtu.be/oWajpHN4QlE");
		innerMap.put("Create Rate Based Contract", "https://youtu.be/-SPTOT072II");
		innerMap.put("Schedule Services On Particular Day", "https://youtu.be/I8COgWm_r_E/edit");
		innerMap.put("Create Single Payment/Residential Contract", "https://youtu.be/x85XllOYzdM");
		innerMap.put("Define Tax Details", "https://youtu.be/Y-CKceuigOE");
		innerMap.put("Enter Balance Payment Received", "https://youtu.be/nHk8WInbupw");
		innerMap.put("Create Service Product", "https://youtu.be/QcA5JXhlqrQ");
		innerMap.put("Define Bank Details", "https://youtu.be/ODxjXwHcztE");
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***************************************************************Contract screen***************************************************/
		
		/*******************************************************Quick Contract screen****************************************************/
		key="Service"+"$"+"Quick Contract"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Enter Balance Payment Received", "https://youtu.be/Jb7trvujaGQ");
		innerMap.put("Create Single Payment/Residential Contract", "https://youtu.be/x85XllOYzdM");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/**************************************************End Quick Contract screen****************************************************/
		
		/*********************************************************Contract Renewal******************************************************/
		key="Service"+"$"+"Contract Renewal"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Contract Is Not Getting Renewed", "https://youtu.be/14YWwIy9820");
		innerMap.put("Set up Renewal Reminder", "https://youtu.be/14YWwIy9820");
		innerMap.put("Renew Bulk Contracts", "https://youtu.be/Xb6_KJRvz30");
		innerMap.put("Postpone Contract Renewal", "https://youtu.be/5TS919G8NO8");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/*************************************************End Contract Renewal**********************************************************/
		
		/*******************************************************Customer Service********************************************************/
		key="Service"+"$"+"Customer Service"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Schedule Service Without Material", "https://youtu.be/b4q6HwxoBxY/edit");
		innerMap.put("Print Service Certificate", "https://youtu.be/_BwTa4R0Toc/edit");
		innerMap.put("Complete Service With Material", "https://youtu.be/b3WyX5BWLVc/edit");
		innerMap.put("Cancel Service", "https://youtu.be/EoreaEuip8Q/edit");
		innerMap.put("Schedule Bulk Services", "https://youtu.be/YvCxJqlb1YU/edit");
		innerMap.put("Schedule Single Service", "https://youtu.be/TFdf-DrbaQw/edit");
		innerMap.put("Resume Service", "https://youtu.be/3isqZPR7yDw/edit");
		innerMap.put("Suspend Bulk Services", "https://youtu.be/2F1LpcNVILg/edit");
		innerMap.put("Suspend Single Service", "https://youtu.be/eUWagZ6vp8s/edit");
		innerMap.put("Reschedule Bulk Services", "https://youtu.be/QH8VpPgLW4I/edit");
		innerMap.put("Reschedule Service", "https://youtu.be/-RdO2tvkEQo/edit");
		innerMap.put("Create Complaint Service", "https://youtu.be/7ma5rZK4WX8/edit");
		innerMap.put("Reverse Service Completion", "https://youtu.be/xFHriY-d-AQ/edit");
		innerMap.put("Complete Service", "https://youtu.be/xQZ8uEXjjKU/edit");
		innerMap.put("Complete Single Service", "https://youtu.be/cxXJAbkzUmk/edit");
		innerMap.put("Cancel Service", "https://youtu.be/BxUc-4KL-ec/edit");
		innerMap.put("Bulk Service Completion", "https://youtu.be/dL7xhW2TJyQ/edit");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/************************************************************End Customer Service************************************************/
		
		/************************************************************Customer Support****************************************************/
		key="Service"+"$"+"Customer Support"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Customer Support", "https://youtu.be/9FrQNsDNuWg/edit");
		innerMap.put("Configure Contract Group", "https://youtu.be/oYlUj-aB35Q");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/********************************************************End Customer Support****************************************************/
		
		/******************************************************************End Service Module********************************************************************************/
		
		/******************************************************************Sale Module Start*********************************************************************************/
		
		key="Sales"+"$"+"Lead"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Item Product", "https://youtu.be/hGCGPDj6-c4");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/********************************************************End Sales lead****************************************************/
		
		/********************************************************Sales Order****************************************************/
		
		key="Sales"+"$"+"Sales Order"+"$"+"Upload";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Sales"+"$"+"Sales Order"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Sales Order", "https://youtu.be/TV04kwSa0fs");
		innerMap.put("Create Delivery Note", "https://youtu.be/xJmZAXNclGk");
		innerMap.put("Create Invoice ", "https://youtu.be/auQNbMZ8IR0");
		innerMap.put("Create AMC From Sales Order", "https://youtu.be/2HZm4cvh3E8");
		innerMap.put("Create PO From PO", "https://youtu.be/Ww174z3OQsA");
		innerMap.put("Define Bank Details", "https://youtu.be/ODxjXwHcztE");
		innerMap.put("Create Invoice For Product Item", "https://youtu.be/auQNbMZ8IR0");
		innerMap.put("Define Tax Details", "https://youtu.be/Y-CKceuigOE");
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		innerMap.put("Create Item Product", "https://youtu.be/hGCGPDj6-c4");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/********************************************************End Sales Order****************************************************/
		
		/********************************************************Sales Quotation****************************************************/
		key="Sales"+"$"+"Quotation"+"$"+"Upload";
		/** sales Quotation **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		
       key="Sales"+"$"+"Quotation"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Sales Quotation", "https://youtu.be/aXa0wG3wDgg");
		innerMap.put("Define Quotation Priority", "https://youtu.be/rLR5pcPOUIU");
		innerMap.put("Define Bank Details", "https://youtu.be/ODxjXwHcztE");
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		innerMap.put("Create Item Product", "https://youtu.be/hGCGPDj6-c4");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/********************************************************End Sales Quotation****************************************************/
		
		/********************************************************Delivery Note****************************************************/
		
		key="Sales"+"$"+"Delivery Note"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Delivery Note", "https://youtu.be/xJmZAXNclGk");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************End Sales Module***************************************************************************************/
		
		/******************************************************************Purchase Start***************************************************************************/
		
		key="Purchase"+"$"+"Purchase Requisition"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		innerMap.put("Purcahse Requisition (PR)", "https://drive.google.com/open?id=1DhyPj-sM33rtgkRLdtjz8NZ5nTIgmYvs");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Purchase"+"$"+"Purchase Requisition"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		/******************************************************************End PR***************************************************************************/
		
		/******************************************************************RFQ***************************************************************************/
		key="Purchase"+"$"+"RFQ"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Purchase"+"$"+"RFQ"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/******************************************************************End RFQ***************************************************************************/
		
		/******************************************************************LOI***************************************************************************/
		
		key="Purchase"+"$"+"Letter Of Intent(LOI)"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Purchase"+"$"+"Letter Of Intent(LOI)"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/******************************************************************End LOI***************************************************************************/
		
		/******************************************************************Purchase Order***************************************************************************/
		
		
		key="Purchase"+"$"+"Purchase Order"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Purchase"+"$"+"Purchase Order"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Vendor", "https://youtu.be/fhTdGtrzZhY");
		innerMap.put("Create Bulk Vendors", "https://youtu.be/kAkVlBf5r7I");
		innerMap.put("Create Purchase Order (PO)", "https://youtu.be/bSgtmA7LONs");
		innerMap.put("Receive Material In Warehouse", "https://youtu.be/k4e-Rq6QfjM");
		innerMap.put("Purchase Without PO", "https://youtu.be/z6AgpEUbT6E");
		innerMap.put("Create PO From PO", "https://youtu.be/Ww174z3OQsA");
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************** End Purchase Order*************************************************************************/
		
		/******************************************************************Vendor***************************************************************************/
		
		key="Purchase"+"$"+"Vendor"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		innerMap.put("Vendor", "https://drive.google.com/file/d/16pXJIpQy_V8v_031GHjrr9BQLtDCi7_e/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Purchase"+"$"+"Vendor"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Vendor", "https://youtu.be/fhTdGtrzZhY");
		innerMap.put("Create Bulk Vendors", "https://youtu.be/kAkVlBf5r7I");
		innerMap.put("Define Vendor Based Purchase", "https://youtu.be/b2_Eo_4Ve6g");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/******************************************************************End Vendor***************************************************************************/
		
		/******************************************************************Vendor product price***************************************************************************/
		
		key="Purchase"+"$"+"Vendor Product Price"+"$"+"Upload";
		/** Vendor product price list **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Vendor Product Price", "https://drive.google.com/file/d/1WUBnFshtVUYmXC0X_ZEnm9Gec5Ci1_uH/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Purchase"+"$"+"Vendor Product Price"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Vendor Based Purchase", "https://youtu.be/b2_Eo_4Ve6g");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/******************************************************************End Vendor product price***************************************************************************/
		
		/******************************************************************Service Po **************************************************************************/
		
		key="Purchase"+"$"+"Service PO"+"$"+"Help";
		/** sales order **/
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		
		
		
		/***********************************************************Purchase Module ENd******************************************************************************/
		
		/***********************************************************Inventory Start**********************************************************************************/
		

		key="Inventory"+"$"+"Material Request Note"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Inventory"+"$"+"Material Request Note"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		innerMap.put("Request Material ", "https://youtu.be/Rcs6htXf0fQ");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************MRN END**********************************************************************************/
		
		key="Inventory"+"$"+"Material Issue Note"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		innerMap.put("Material Issue Note", "https://drive.google.com/open?id=1FproQs9yyk2BJmdWwAoGIjxTLGKf97Fa");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Inventory"+"$"+"Material Issue Note"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		innerMap.put("Issue Material For Consumption", "https://youtu.be/N26EF1JxjF8");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************MIN END**********************************************************************************/
		
		key="Inventory"+"$"+"Material Movement Note"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Inventory"+"$"+"Material Movement Note"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Self Approval ", "https://youtu.be/RUtJVrp6-fg");
		innerMap.put("Transfer Material From One Warehouse to Another", "https://youtu.be/FYIdil-9VMY");
		innerMap.put("Write Off Material", "https://youtu.be/-FP2hEk-MlU");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************MMN END**********************************************************************************/
		
		key="Inventory"+"$"+"Warehouse"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Warehouse, Storage Location & Storage Bin", "https://youtu.be/pDNQ3QnQyDo");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************WAREHOUSE********************************************************************************/
		
		key="Inventory"+"$"+"Product Inventory View"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Allocate warehouse to product", "https://youtu.be/MEqsyFKXSsM");
		innerMap.put("Create Item & Upload Stock", "https://youtu.be/Z4eCFlb12hQ");
		innerMap.put("Update Stock ", "https://youtu.be/7XcJtth88n0");
		innerMap.put("Define Stock Alert ", "https://youtu.be/o0Kamgbf38Y");
		innerMap.put("Update Stock ", "https://youtu.be/7XcJtth88n0");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************END PRODUCT INVENTORY VIEW*************************************************************/
		
		key="Inventory"+"$"+"Stock Alert"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Stock Alert ", "https://youtu.be/o0Kamgbf38Y");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************Stock Alert END**********************************************************************************/
		
		key="Inventory"+"$"+"Inventory Transaction List"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Inventory Movement Details", "https://youtu.be/BbYHF2SvPDE");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************Inventory Transaction List END*******************************************************************/
		
		key="Inventory"+"$"+"Physical Inventory"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Carry Out Physical Inventory ", "https://youtu.be/rasYtHIqzls");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************End Inventory *****************************************************************************************/
		
		/***********************************************************Product **********************************************************************************************/
		
		
		key="Product"+"$"+"Service Product"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Service Product", "https://drive.google.com/open?id=1nVqPQI_7H8jhiC9bynqJRJpltB28iVvq");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Product"+"$"+"Service Product"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Service Product ", "https://youtu.be/QcA5JXhlqrQ");
		innerMap.put("Setup Services Offered", "https://youtu.be/QcA5JXhlqrQ/edit");

		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************End Service Product *******************************************************************************/
		/***********************************************************Item Product Product **************************************************************************/
		
		key="Product"+"$"+"Item Product"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Item Product", "https://drive.google.com/open?id=19m0HC7raHjbgntTjvs-1EHe0_rJRrxZh");
		innerMap.put("Item Product + Stock", "https://drive.google.com/open?id=19m0HC7raHjbgntTjvs-1EHe0_rJRrxZh");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Product"+"$"+"Item Product"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Item Product ", "https://youtu.be/hGCGPDj6-c4");
		innerMap.put("Define Stock Alert", "https://youtu.be/o0Kamgbf38Y");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/*********************************************************** End Product **********************************************************************************************/
		
		/*********************************************************** Account **********************************************************************************************/
		
        key="Accounts"+"$"+"Expense Management"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Accounts"+"$"+"Expense Management"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Deposit", "https://youtu.be/AJizNZQCuZM");
		innerMap.put("Deposit From Customer Payment", "https://youtu.be/7db01AAANNQ");
		innerMap.put("Withdraw", "https://youtu.be/f2UDLUOrYHg");  
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
        key="Accounts"+"$"+"Payment Details"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		innerMap.put("Payment Received", "https://drive.google.com/file/d/1vEs_VjQhzesBb25l65VUCjA12CPeUfQT/view?usp=sharing");
		innerMap.put("Payment Received In Tally", "https://drive.google.com/file/d/1VLPtdt3VfXDdF9WKlPXKvyt_5DWEEGM8/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		
		key="Accounts"+"$"+"Payment Details"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Deposit", "https://youtu.be/AJizNZQCuZM");
		innerMap.put("Deposit From Customer Payment", "https://youtu.be/7db01AAANNQ");
		innerMap.put("Withdraw", "https://youtu.be/f2UDLUOrYHg");
		innerMap.put("Setup Self Approval", "https://youtu.be/RUtJVrp6-fg");
		innerMap.put("Enter Balance Payment Received", "https://youtu.be/Jb7trvujaGQ");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		
		key="Accounts"+"$"+"Billing Details"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Accounts"+"$"+"Billing Details"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Bank Details ", "https://youtu.be/ODxjXwHcztE");
		//innerMap.put("Define Stock Alert", "https://youtu.be/o0Kamgbf38Y");
		innerMap.put("Setup Self Approval", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Accounts"+"$"+"Invoice Details"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Accounts"+"$"+"Invoice Details"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Bank Details ", "https://youtu.be/ODxjXwHcztE");
		innerMap.put("Print Bank Details On Invoice", "https://youtu.be/8PB6o7UJGPI");
		innerMap.put("Create Invoice ", "https://youtu.be/auQNbMZ8IR0");
		innerMap.put("Setup Self Approval", "https://youtu.be/RUtJVrp6-fg");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Accounts"+"$"+"Company"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Tax Details", "https://youtu.be/Y-CKceuigOE");
		innerMap.put("Setup Company", "https://youtu.be/6pg2vAURvC0");
		innerMap.put("Define GST, PAN, Statutory Details", "https://youtu.be/CiRy1UXfPMY");
		innerMap.put("Setup Company Logo, Header Footer Size ", "https://youtu.be/_Cxvvtlora0");
		innerMap.put("Define Logo, Header, Footer", "https://youtu.be/wHoE3f9vOYw");
		innerMap.put("Setup Billing / Non Billing Transcations", "https://youtu.be/4r2Yn9fbQv0");
		innerMap.put("Edit Company Details", "https://youtu.be/FlMWmWhTZ4c");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		
		
		/*********************************************************** Account End*******************************************************************************************/
		
		/*********************************************************** HR admin**********************************************************************************************/	
		
		key="HR Admin"+"$"+"Employee"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Category", "https://drive.google.com/open?id=1wL-1pWH6567D0pDmP7PVzYzFyrxWoroY");
		innerMap.put("Configuration", "https://drive.google.com/file/d/17-iJGsERWCxOtrobTPzhFrfGnqFiWmHl/view?usp=sharing");
		innerMap.put("Type", "https://drive.google.com/open?id=1Xd3WVanulgCCg1_s6H1JdYW5FfjTXb9n");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="HR Admin"+"$"+"Employee"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Employee ", "https://youtu.be/9WHpER0jAI0");
		innerMap.put("Create Employee Contact Information", "https://youtu.be/ZpUelTPx9Xg");
		innerMap.put("Allocate Asset To Employee", "https://youtu.be/Qe6gH_So1eU");
		innerMap.put("Asset Return", "https://youtu.be/OxTLT0f2TkI");
		innerMap.put("Employee Full & Final", "https://youtu.be/HD2MjA4SCKU");
		innerMap.put("Define Paid Leave ", "https://youtu.be/OyFt16Sjlqo");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="HR Admin"+"$"+"Employee Asset Allocation"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Asset Allcoation", "https://drive.google.com/open?id=19m0HC7raHjbgntTjvs-1EHe0_rJRrxZh");
		innerMap.put("Update Asset Allocation", "https://drive.google.com/open?id=19m0HC7raHjbgntTjvs-1EHe0_rJRrxZh");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="HR Admin"+"$"+"Employee Asset Allocation"+"$"+"Help";
		innerMap=new HashMap<String,String>();
		innerMap.put("Allocate Asset To Employee", "https://youtu.be/Qe6gH_So1eU");
		innerMap.put("Asset Return", "https://youtu.be/OxTLT0f2TkI");
		innerMap.put("Create Employee Contact Information", "https://youtu.be/ZpUelTPx9Xg");
		innerMap.put("Allocate Asset To Employee", "https://youtu.be/Qe6gH_So1eU");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Leave Balance"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Leave Balance", "https://drive.google.com/file/d/19mikwG-c_bQzb89Ob5Q3S5NL3GdiigpU/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="HR Admin"+"$"+"Leave Balance"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Paid Leave ", "https://youtu.be/OyFt16Sjlqo");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="HR Admin"+"$"+"Upload Attendance"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Attendance", "https://drive.google.com/open?id=1vXQr5SCSYv3n8SXoQLFowlLvi12YQjvb");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="HR Admin"+"$"+"Upload Attendance"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Upload Attendance Via Excel", "https://youtu.be/xJqnYI2Sq-s");
		innerMap.put("Upload Attendance", "https://youtu.be/IwOYs4xuBzI");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Salary Slip"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Upload Attendance Via Excel", "https://youtu.be/xJqnYI2Sq-s");
		innerMap.put("Download Overtime & Leave History", "https://youtu.be/TlvU5jZs_is");
		innerMap.put("Statutory Reports", "https://youtu.be/ltz9yFqqGy8");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Employee Leave History"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Download Overtime & Leave History", "https://youtu.be/TlvU5jZs_is");
		innerMap.put("Statutory Reports", "https://youtu.be/ltz9yFqqGy8");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Employee Overtime History"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Download Overtime & Leave History", "https://youtu.be/TlvU5jZs_is");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"CTC Template"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("CTC Templates", "https://drive.google.com/file/d/1uhhLjFrQlBj_zEvSZHOiQNwDvZTsUNYe/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"CTC Template"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create CTC Template", "https://youtu.be/heHH9zPC8iY");
		innerMap.put("Define Earning Component", "https://youtu.be/ry-9xdE7n48");
		innerMap.put("Setup PF Master", "https://youtu.be/SjjEYLt3nN0");
		innerMap.put("Allocate CTC To Employee", "https://youtu.be/jadWPP4ue1s");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Process Payroll"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define ESIC Master", "https://youtu.be/FVySZWbIGuI");
		innerMap.put("Define PT Master", "https://youtu.be/HJwJ9FMwXXk");
		innerMap.put("Define LWF", "https://youtu.be/hgsCGWYbbdI");
		innerMap.put("Process Payroll", "https://youtu.be/YaDZoM4au9c");
		innerMap.put("Upload Attendance", "https://youtu.be/IwOYs4xuBzI");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Attendance Report"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Upload Attendance", "https://youtu.be/IwOYs4xuBzI");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Project Allocation"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Calendar & Leave Group", "https://youtu.be/nRUXC-qQWyM");
		innerMap.put("Allocate Calendar & Leave Group", "https://youtu.be/pZPSjKQt20I");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Calendar"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Calendar & Leave Group", "https://youtu.be/nRUXC-qQWyM");
		innerMap.put("Allocate Calendar & Leave Group", "https://youtu.be/pZPSjKQt20I");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Project"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Calendar & Leave Group", "https://youtu.be/nRUXC-qQWyM");
		innerMap.put("Allocate Calendar & Leave Group", "https://youtu.be/pZPSjKQt20I");
		innerMap.put("Define Overtime - Variable", "https://youtu.be/j7dohNcbsFo");
		innerMap.put("Define Overtime - Fixed", "https://youtu.be/87Mce2mv9z0");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"CTC Component"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Upload Attendance", "https://youtu.be/IwOYs4xuBzI");
		innerMap.put("Define Earning Component", "https://youtu.be/ry-9xdE7n48");
		innerMap.put("Setup PF Master", "https://youtu.be/SjjEYLt3nN0");
		innerMap.put("Allocate CTC To Employee", "https://youtu.be/jadWPP4ue1s");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"CTC Allocation"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create CTC Template", "https://youtu.be/heHH9zPC8iY");
		innerMap.put("Define Earning Component", "https://youtu.be/ry-9xdE7n48");
		innerMap.put("Setup PF Master", "https://youtu.be/SjjEYLt3nN0");
		innerMap.put("Allocate CTC To Employee", "https://youtu.be/jadWPP4ue1s");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="HR Admin"+"$"+"Leave Type"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Leave Type", "https://youtu.be/b6KDITED40s");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="HR Admin"+"$"+"Leave Group"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Leave Type", "https://youtu.be/b6KDITED40s");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="HR Admin"+"$"+"Overtime"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Overtime - Variable", "https://youtu.be/j7dohNcbsFo");
		innerMap.put("Define Overtime - Fixed", "https://youtu.be/87Mce2mv9z0");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="HR Admin"+"$"+"Calendar & Leave Allocation"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Define Leave Type", "https://youtu.be/b6KDITED40s");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************End HR admin**********************************************************************************************/
		
		/***********************************************************Settings**********************************************************************************************/
		
		key="Settings"+"$"+"Branch"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Set Up Taxable Branch", "https://youtu.be/tIdTh6L8GNs");
		innerMap.put("Create Branch", "https://youtu.be/TYcRafSUkqY");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		key="Settings"+"$"+"Employee"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Employee", "https://drive.google.com/open?id=1U52v3qgfwHp3uUVgcALaE62AuEuErGzW");
		innerMap.put("Update Employee Details", "https://drive.google.com/file/d/17RSXOh6KjHgFqDkQ8TinLhnITDNkcrnS/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		
		key="Settings"+"$"+"Employee"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Employee", "https://youtu.be/9WHpER0jAI0");
		innerMap.put("Create Employee Contact Information", "https://youtu.be/ZpUelTPx9Xg");
		innerMap.put("Employee Full & Final", "https://youtu.be/HD2MjA4SCKU");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Settings"+"$"+"User"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create Technicians ", "https://drive.google.com/open?id=1aFOUavY-_aHAn9ju2yEcMGtTdR5ulzO6");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		key="Settings"+"$"+"User"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Create User", "https://youtu.be/wN_X02jtnTY");
		innerMap.put("Restrict User Access To Office", "https://youtu.be/JeDb9wf6iW0");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Settings"+"$"+"Process Configuration"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Process Configuration List ", "https://docs.google.com/spreadsheets/d/1W7ssfqNl5TstZCrFEQvJGaLdB5pf-qeQDNAzNS8qMGo/edit#gid=0");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		
		/****************************************************************************************************************************************************************/
		
		key="Settings"+"$"+"Number Generation"+"$"+"Help";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Setup Document Range Series", "https://youtu.be/m3vhC6XAftc");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Settings"+"$"+"City"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("City", "https://drive.google.com/file/d/170rsoQ3tKl2XglNHQNp2W-lg5l_pUEFO/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Settings"+"$"+"Locality"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Locality ", "https://drive.google.com/file/d/178Idfkhqk9HSsb_sDXfV6bu7iLdZufnU/view?usp=sharing");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/****************************************************************************************************************************************************************/
		
		key="Settings"+"$"+"Cron Job Reminder Setting"+"$"+"Upload";
		
		innerMap=new HashMap<String,String>();
		innerMap.put("Cron Job Reminders", "https://docs.google.com/spreadsheets/d/1nQIC3KkxS1pnwtEWXVCHYRfTExip4sK6/edit#gid=1630266651");
		moduleWiseMenuConfgMap.put(key, innerMap);
		
		/***********************************************************End Setting**********************************************************************************************/
		
	}
	





	









	private void deleteCancelledServices(long companyId) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside Delete cancelled service method "+companyId);
		ProcessConfiguration processConfiguration=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName", "Delete Service").first().now();
		if(processConfiguration!=null){
			for(ProcessTypeDetails obj:processConfiguration.getProcessList()){
				if(obj.isStatus()==true){
					Date serviceDate=null;
					try {
						serviceDate=fmt.parse(obj.getProcessType());
						logger.log(Level.SEVERE,"Service Date : "+serviceDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.log(Level.SEVERE,"Unparsable Date..");
					}
					
					if(serviceDate!=null){
						List<Service> cancelledServiceList=ofy().load().type(Service.class).filter("companyId", companyId).filter("status", "Cancelled").filter("serviceDate <", serviceDate).limit(1000).list();
						ObjectifyService.reset();
					    ofy().consistency(Consistency.STRONG);
					    ofy().clear();
						if(cancelledServiceList!=null&&cancelledServiceList.size()!=0){
							int totalNoOfSer=cancelledServiceList.size();
							logger.log(Level.SEVERE,"Cancelled Service List Size : "+totalNoOfSer);
							
							ofy().delete().entities(cancelledServiceList).now();
							
//							if(totalNoOfSer==1000){
								logger.log(Level.SEVERE,"Calling another task queue...");
								Queue queue = QueueFactory.getQueue("UpdateServices-queue");
								queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
										.param("OperationName", "Delete Cancelled Services")
										.param("companyId", companyId+""));
//							}
							
						}else{
							logger.log(Level.SEVERE,"No Cancelled Services....FOOOUND");
						}
					}
				}
			}
		}else{
			logger.log(Level.SEVERE,"Process configuration not added..");
		}
		List<Service> cancelledServiceList=ofy().load().type(Service.class).filter("companyId", companyId).list();
	}



	private void customerNameUpdation(HttpServletRequest request, long companyId) {
//		Customer customerInfo = new Customer();
//		if(request.getParameter("isCompany").equals("Yes")){
//			customerInfo.setCompany(true);
//		}
//		else{
//			customerInfo.setCompany(false);
//		}
//		customerInfo.setCompanyName(request.getParameter("CompanyName"));
//		customerInfo.setFullname(request.getParameter("FullName"));
//		customerInfo.setCellNumber1(Long.parseLong(request.getParameter("CellNumber")));
//		customerInfo.setCount(Integer.parseInt(request.getParameter("CustomerId")));
//		customerInfo.setCompanyId(companyId);
//		
//		CustomerNameChangeServiceImpl customerNameUpdation = new CustomerNameChangeServiceImpl();
//		customerNameUpdation.UpdateAllDocuments(customerInfo);
	}

	/**
	 * Date 09-05-2018
	 * Developer : Vijay
	 * Des :- Payment document update with follow up date for status created only because we load created status dashboard only
	 */
	private void updatePaymentDocsWithFollowUpDate(long companyid) {

		List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("status", CustomerPayment.CREATED).filter("companyId", companyid).list();
		for(int i=0;i<paymentlist.size();i++){
			if(paymentlist.get(i).getFollowUpDate()==null){
				if(paymentlist.get(i).getPaymentDate()!=null){
					paymentlist.get(i).setFollowUpDate(paymentlist.get(i).getPaymentDate());
				}else{
					paymentlist.get(i).setFollowUpDate(paymentlist.get(i).getCreationDate());
				}
			}
		}
		ofy().save().entities(paymentlist).now();
		logger.log(Level.SEVERE,"Data Updated successfully");
	}
	/**
	 * ends here
	 */
	
	/**
	 * Date 28-01-2019 By Vijay NBHC Inventory Management Updating product inventory with Warehouse type
	 * to identify warehouse and cluster warehouse
	 */
	private void UpdateProductInventoryWithWarehouseType(long companyId) {
		List<WareHouse> warehouselist = ofy().load().type(WareHouse.class)
										.filter("companyId", companyId).list();
		
		List<ProductInventoryView> productinventorylist = ofy().load().type(ProductInventoryView.class)
				.filter("companyId", companyId).list();
		for(ProductInventoryView productInv : productinventorylist){
			for(ProductInventoryViewDetails prodInvDetails :  productInv.getDetails()){
				for(WareHouse warehouse : warehouselist){
					if(warehouse.getBusinessUnitName().trim().equalsIgnoreCase(prodInvDetails.getWarehousename().trim())){
						if(warehouse.getWarehouseType()!=null)
						prodInvDetails.setWarehouseType(warehouse.getWarehouseType());
						if(warehouse.getBranchdetails()!=null){
							prodInvDetails.setBranchName(warehouse.getBranchdetails().get(0).getBranchName());
						}
					}
				}
			}
			ofy().save().entity(productInv);
		}
		
	}
	/**
	 * Date 11-05-2019 by Vijay for NBHC Inventory Management Creating Storage location bin for existing warehouses 
	 * @param companyId
	 */
	private void CreateStorageLocationBinForWarehouse(long companyId) {
		
		List<WareHouse> warehouselist = ofy().load().type(WareHouse.class).filter("companyId", companyId).list();
		List<StorageLocation> storageLocationlist = ofy().load().type(StorageLocation.class).filter("companyId", companyId).list(); 
		ArrayList<WareHouse> warehouselistNew = new ArrayList<WareHouse>();
		warehouselistNew.addAll(warehouselist);
		System.out.println("warehouselist == Before "+warehouselistNew.size());
		logger.log(Level.SEVERE, "All Warehouse list size "+warehouselistNew.size());
		for(WareHouse warehouse : warehouselist){
			for(StorageLocation strLocation :storageLocationlist){
				if(warehouse.getBusinessUnitName().trim().equals(strLocation.getWarehouseName())){
					warehouselistNew.remove(warehouse);
				}
			}
		}
		System.out.println("warehouselist == After "+warehouselistNew.size());
		logger.log(Level.SEVERE, "Updated warehouse list for location creation"+warehouselistNew.size());

		ArrayList<SuperModel> modellocationlist = new ArrayList<SuperModel>();
		ArrayList<StorageLocation> locationlist = new ArrayList<StorageLocation>();
		for(WareHouse warehouse : warehouselistNew){
			StorageLocation location = new StorageLocation();
			location.setAddress(warehouse.getAddress());
			location.setBusinessUnitName(warehouse.getBusinessUnitName()+" Location");
			location.setWarehouseName(warehouse.getBusinessUnitName());
			location.setCompanyId(warehouse.getCompanyId());
			location.setstatus(true);
			locationlist.add(location);
		}
		modellocationlist.addAll(locationlist);
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(modellocationlist);	
		
		ArrayList<Storagebin> storageBinlist = new  ArrayList<Storagebin>();
		ArrayList<SuperModel> modelBinlist = new ArrayList<SuperModel>();

		for(WareHouse warehouse : warehouselistNew){
			Storagebin strBin = new  Storagebin();
			strBin.setBinName(warehouse.getBusinessUnitName()+" Bin");
			strBin.setCompanyId(warehouse.getCompanyId());
			strBin.setWarehouseName(warehouse.getBusinessUnitName());
			strBin.setStoragelocation(warehouse.getBusinessUnitName()+" Location");
			strBin.setStatus(true);
			strBin.setXcoordinate(1+"");
			strBin.setYcoordinate(1+"");
			storageBinlist.add(strBin);
		}
		modelBinlist.addAll(storageBinlist);
		GenricServiceImpl impl2 = new GenricServiceImpl();
		impl2.save(modelBinlist);	
		
		logger.log(Level.SEVERE,"Storage Location And Bin Created Successfully");

	}
	
	
	

	/** date 10.4.2019 added by komal to update services **/
	private void  UpdateServices(long companyId ,String fromDate ,String toDate , String customerId , String branch){
		logger.log(Level.SEVERE , "customer id :" + customerId);
	//	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null , endDate = null ;
		int customerCount = 0;
		try {
			//System.out.println("from date :" +" "+fromDate1);
			//System.out.println("from date :"+ isoFormat.format(fromDate1) +" "+fromDate1);
			if(fromDate != null && !fromDate.equalsIgnoreCase("null")){
				startDate = isoFormat.parse(isoFormat.format(sdf.parse(fromDate)));
				
			}
			if(toDate != null && !toDate.equalsIgnoreCase("null") ){
				endDate = isoFormat.parse(isoFormat.format(DateUtility.addDaysToDate(sdf.parse(toDate), 1)));
			}
			if(customerId != null && !customerId.equalsIgnoreCase("0") && !customerId.equalsIgnoreCase("-1")){
				customerCount = Integer.parseInt(customerId);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<Integer,Contract> contractMap = new HashMap<Integer,Contract>();
		List<Service> servicelist = new ArrayList<Service>();
		if(startDate != null && endDate != null && customerCount == 0){
			servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("serviceDate >=", startDate)
				.filter("serviceDate <=", endDate).filter("branch", branch).list();
		}else if(startDate != null && endDate != null && customerCount != 0){
			servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("serviceDate >=", startDate)
					.filter("serviceDate <=", endDate).filter("personInfo.count", customerCount).list();
		}else if(customerCount != 0){
			servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", customerCount).list();
		}
		ServerUnitConversionUtility utility = new ServerUnitConversionUtility();
		
		for(Service service : servicelist){
			logger.log(Level.SEVERE, "ser : " + service.getCount());
			Contract contract = null;
			if(contractMap.containsKey(service.getContractCount())){
				contract = contractMap.get(service.getContractCount());
			}else{
				contract = ofy().load().type(Contract.class).filter("companyId", companyId).
					     filter("count", service.getContractCount()).first().now();
				logger.log(Level.SEVERE , "contract id :" + service.getContractCount() +" "+contract);
				contractMap.put(contract.getCount(), contract);
			}
			if(contract.getGroup() != null){
				service.setContractGroup(contract.getGroup());
			}
			if(contract.getType() != null){
				service.setContractType(contract.getType());
			}
			service.setContractAmount(contract.getNetpayable());
			if(service.getListHistory() != null && service.getListHistory().size() > 0){
				service.setOldServiceDate(service.getListHistory().get(0).getOldServiceDate());
			}
			Service serviceEntity = service.Myclone();
			String uom ="";
			double quantity = 0;
			if(contract != null){
				for(SalesLineItem item : contract.getItems()){
					if(item.getServiceBranchesInfo() != null && item.getServiceBranchesInfo().size() > 0){
					for(BranchWiseScheduling brch : item.getServiceBranchesInfo().get(item.getProductSrNo())){
						if(brch.isCheck()){
						if(brch.getBranchName().equals(service.getServiceBranch())){
							uom = brch.getUnitOfMeasurement();
							quantity = brch.getArea();
							break;
						}
					  }
					}
				}
			  }	
			}
			serviceEntity.setQuantity(quantity);
			serviceEntity.setUom(uom);
			ArrayList<ProductGroupList> groupList = utility.getServiceitemProList(null, null, serviceEntity, null);
			ArrayList<ServiceProductGroupList> serProList  = new ArrayList<ServiceProductGroupList>();
			for(ProductGroupList pList : groupList){	
				ServiceProductGroupList list = new ServiceProductGroupList();
				list.setPrice(pList.getPrice());
				list.setUnit(pList.getUnit());
				list.setQuantity(pList.getQuantity());
				list.setProduct_id(pList.getProduct_id());
				list.setName(pList.getName());
				list.setProActualUnit(pList.getProActualUnit());
				list.setProActualQty(pList.getProActualQty());
				list.setPlannedQty(pList.getPlannedQty());
				list.setPlannedUnit(pList.getPlannedUnit());
				
				serProList.add(list);
			}
			logger.log(Level.SEVERE , "customer id :" + serProList.size());
			service.setServiceProductList(serProList);
		//	getServiceitemProList
			
			ofy().save().entity(service);
		}
	}
	
	
	private void reactOnBOMReport(HttpServletRequest request, long companyId) {

		MaterialConsumptionImpl impl = new MaterialConsumptionImpl();
		String contractId  = request.getParameter("contractId");
		String type = request.getParameter("type");
		String fromDate1 = request.getParameter("fromDate").trim();
		String toDate1 = request.getParameter("toDate").trim();
		
			 
		Date startDate = null , endDate = null;
		try {
			//System.out.println("from date :" +" "+fromDate1);
			//System.out.println("from date :"+ isoFormat.format(fromDate1) +" "+fromDate1);
			if(fromDate1 != null && !fromDate1.equalsIgnoreCase("null")){
				startDate = isoFormat.parse(isoFormat.format(sdf.parse(fromDate1)));
				
			}
			if(toDate1 != null && !toDate1.equalsIgnoreCase("null") ){
				endDate = isoFormat.parse(isoFormat.format(sdf.parse(toDate1)));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<MaterialConsumptionReport>  materialConsumptionReportList = impl.retrieveData(companyId,contractId,type,startDate,endDate);
		XlsxWriter.contractProfitLossReportList = materialConsumptionReportList;
		XlsxWriter.FromDate = startDate;
		XlsxWriter.ToDate = endDate;
		try{
			int id = Integer.parseInt(contractId);
		}catch(Exception e){
			XlsxWriter.segment = contractId;
			e.printStackTrace();
		}
		XSSFWorkbook workbook = new XSSFWorkbook();
		XlsxWriter.companyId1 = companyId;
		XlsxWriter xlsx = new XlsxWriter();

//		xlsx.getFumigationReportDetails(workbook);
		xlsx.getContractServicePAndLReport(workbook);

		
		ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
		try {
			workbook.write(outByteStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] outArray = outByteStream.toByteArray();

		logger.log(Level.SEVERE, "outArray  -  size -- "
				+ outArray.length);
		
		ArrayList<String> emailList = MaterialConsumptionImpl.emailIdList;
		Company company= ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		StringBuilder builder =new StringBuilder();


		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + "Contract P and L"
				+ "</h3></br></br>");

		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("Please find attachment.");
		
		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		
		/**
		 * @author Anil , Date : 10-04-2020
		 * added display name for from email id
		 * ,company.getDisplayNameForCompanyEmailId()
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi",companyId)){
		
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + emailList.toString());
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), emailList, new ArrayList<String>(), new ArrayList<String>(), "Contract P and L AS On -"+ isoFormat.format(new Date()), contentInHtml, "text/html",outByteStream,"Contract P and L" + ".xlsx","application/vnd.ms-excel",company.getDisplayNameForCompanyEmailId());
		}else{
			logger.log(Level.SEVERE," company email --" +company.getEmail() +"  to email  " + emailList.toString());
			Email email = new Email();
			email.sendMailWithGmail(company.getEmail(), emailList,new ArrayList<String>(), new ArrayList<String>(), "Contract P and L AS On -"+ isoFormat.format(new Date()),  contentInHtml, "text/html", outByteStream,"Contract P and L" + ".xlsx", "application/vnd.ms-excel");
		}
	}
	
	/**
	 * Date 27-08-2019 by Vijay 
	 * Des :- NBHC CCPM Rate contracts Service value updation for Contract PNL Report 
	 * @param companyId 
	 * @param request 
	 */
	private void reactonUpdateServiceValueforRateContracts(HttpServletRequest request, long companyId) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strstartdDate = request.getParameter("fromDate");
		String strendDate = request.getParameter("toDate");
		String branch = request.getParameter("branch");
		try {
			Date startdDate = sdf.parse(strstartdDate);
			Date endDate =sdf.parse(strendDate);
			
			List<Service> servicelist = ofy().load().type(Service.class).filter("isRateContractService", true)
					.filter("companyId", companyId).filter("branch", branch)
					.filter("serviceDate >=",startdDate).filter("serviceDate <=", endDate).list();
			logger.log(Level.SEVERE, "Rate Contract Service list size"+servicelist.size());
			
			ArrayList<Service> updatedServicelist = new ArrayList<Service>();
			if(servicelist.size()!=0){
				for(Service serviceEntity : servicelist){
					if(serviceEntity.getQuantity()!=0){
						serviceEntity.setServiceValue(serviceEntity.getQuantity()*serviceEntity.getServiceValue());
						updatedServicelist.add(serviceEntity);
					}
				}
				ofy().save().entities(updatedServicelist);
				logger.log(Level.SEVERE, "Services updated successfully"+updatedServicelist.size());
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
  private void updateProjectMaterial(HttpServletRequest request ,long companyId){
	 List<String> statusList =new ArrayList<String>();
	 statusList.add(Service.SERVICESTATUSSCHEDULE);
	 statusList.add(Service.SERVICESTATUSRESCHEDULE);
	 
	 int serviceId = Integer.parseInt(request.getParameter("serviceId"));
	 int projectId = Integer.parseInt(request.getParameter("projectId"));
	 logger.log(Level.SEVERE, "service id " + serviceId);
	 
	 Service service = ofy().load().type(Service.class).filter("count", serviceId).filter("companyId", companyId).first().now();
	 ServiceProject projectEntity = ofy().load().type(ServiceProject.class).filter("serviceId", serviceId).filter("count", service.getProjectId()).filter("companyId", companyId).first().now();	
	 logger.log(Level.SEVERE, "service " + service);
	 if(service != null){
	 List<Service> serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
			 .filter("serviceBranch",service.getServiceBranch()).filter("contractCount", service.getContractCount()).filter("status IN", statusList)
			 .filter("product.count", service.getProduct().getCount()).list();
	 logger.log(Level.SEVERE, "service list " + serviceList);
	 
	 Map<Integer , ServiceProject> projectMap = new HashMap<Integer , ServiceProject>();
	List<Integer> serIdList = new ArrayList<Integer>();
	List<Integer> projectIdList =new ArrayList<Integer>();
	 if(serviceList != null){
		 for(Service ser : serviceList){
			// if(!ser.isMaterialAdded()){
				 serIdList.add(ser.getCount());
				 projectIdList.add(ser.getProjectId());
			// }
		 }
		 if(serIdList.size() > 0){
			 List<ServiceProject> projectList = ofy().load().type(ServiceProject.class).filter("companyId", companyId)
					 .filter("serviceId IN", serIdList).filter("count IN", projectIdList).list();
			 if(projectList != null && projectList.size() > 0){
				 for(ServiceProject srProject : projectList){
					 projectMap.put(srProject.getserviceId(), srProject);
				 }
			 }
		 }
	 }
	 GeneralServiceImpl impl = new GeneralServiceImpl();
	 if(serviceList != null){
		 logger.log(Level.SEVERE, "service list size" + serviceList.size());
		 for(Service ser : serviceList){
			 int id  = 0;
			 if(serviceId != ser.getCount()  && !service.isServiceScheduled()){
			 ServiceProject project = null;
			 ArrayList<ProductGroupList> prList = new ArrayList<ProductGroupList>();
			 ArrayList<EmployeeInfo> info = new ArrayList<EmployeeInfo>();
			 if(projectMap.containsKey(ser.getCount())){
				 project = projectMap.get(ser.getCount());
				 prList.addAll(project.getProdDetailsList());
				 if(prList.size() == 0){
					 prList.addAll(projectEntity.getProdDetailsList());
				 }
				if(prList.size() > 0){
					ser.setMaterialAdded(true);
				}
				if(ser.getEmployee() == null || ser.getEmployee().equals("")){
					 ser.setEmployee(service.getEmployee());
				 }
				 info.addAll(project.getTechnicians());
				 id = impl.createProject(ser, info, project.getTooltable(), prList, ser.getServiceDate(), ser.getServiceTime(), 0, null, ser.getEmployee(), project.getCount());
			 }else{			
				 prList.addAll(projectEntity.getProdDetailsList());
				 if(prList.size() > 0){
						ser.setMaterialAdded(true);
					}
				 if(ser.getEmployee() == null || ser.getEmployee().equals("")){
					 ser.setEmployee(service.getEmployee());
				 }
				 if(ser.getTechnicians() == null || ser.getTechnicians().size() == 0){
					 ser.setTechnicians(info);
				 }
				info.addAll(projectEntity.getTechnicians());
				 id = impl.createProject(ser, info, new ArrayList<CompanyAsset>(), prList, ser.getServiceDate(), ser.getServiceTime(), 0, "", ser.getEmployee(), 0);
			 }
			 
			 }
		 }
	 }
  }
}
  
 private void sendSrCopyMail(HttpServletRequest request , long companyId){
	
	 List<Integer> serviceIdList = new ArrayList<Integer>();
	 String str = request.getParameter("serviceId");
	 try{
			Gson gson = new GsonBuilder().create();	
			JSONArray jsonarr=new JSONArray(str);
			logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
			for(int i=0;i<jsonarr.length();i++){
				serviceIdList.add(jsonarr.getInt(i));
			}
		List<Service> serList = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("count IN", serviceIdList).list();
		if(serList != null){
			for(Service service : serList){
				Email email = new Email();
				MarkCompletedSubmitServlet servlet = new MarkCompletedSubmitServlet();
					servlet.sendEmailtoCustomer(service , email);
				}
			}
		}catch(Exception e){
			
		}
 }
 
	public void createTechnicianMMN(HttpServletRequest request, long companyId) {
		GeneralServiceImpl impl = new GeneralServiceImpl();
		List<Integer> serviceIdList = new ArrayList<Integer>();
		String str = request.getParameter("serviceIdList");
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(str);
			logger.log(Level.SEVERE, "JSONArray :: " + jsonarr.length());
			for (int i = 0; i < jsonarr.length(); i++) {
				serviceIdList.add(jsonarr.getInt(i));
			}
		} catch (Exception e) {

		}
		List<ProductGroupList> productList = new ArrayList<ProductGroupList>();
		String str1 = request.getParameter("productList");
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(str1);
			logger.log(Level.SEVERE, "JSONArray :: " + jsonarr.length());
			for (int i = 0; i < jsonarr.length(); i++) {
				productList.add(gson.fromJson(jsonarr.get(i).toString(),ProductGroupList.class));
			}
		} catch (Exception e) {

		}

		String user = request.getParameter("user");
		logger.log(Level.SEVERE, "product list :" + productList.size());
		List<Service> sList = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceIdList).list();
		List<Service> newSList = new ArrayList<Service>();
		boolean noMMNFlag = false;
		String branch = "";
		noMMNFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","NoTechnicianwiseMMN", companyId);
		for (Service s : sList) {
			branch = s.getBranch();
			break;
		}
		ArrayList<Integer> prodIdList = new ArrayList<Integer>();
		Map<String, ArrayList<ProductGroupList>> technicianwiseMap = new HashMap<String, ArrayList<ProductGroupList>>();
		ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
		List<String> techList = new ArrayList<String>();
		if (productList != null && productList.size() > 0 && !noMMNFlag) {
			for (ProductGroupList group : productList) {
				if (technicianwiseMap.containsKey(group.getCreatedBy())) {
					list = technicianwiseMap.get(group.getCreatedBy());

					list.add(group);
					technicianwiseMap.put(group.getCreatedBy(), list);
				} else {
					list = new ArrayList<ProductGroupList>();
					list.add(group);
					technicianwiseMap.put(group.getCreatedBy(), list);
					techList.add(group.getCreatedBy());
				}
				
				prodIdList.add(group.getProduct_id());
			}
			
			/**
			 * @author Anil @since 21-01-2021
			 * Commented below logic of calculating in use stock, we have already calculated and stored on stock master
			 * at the time of scheduling services
			 */

//			for (Map.Entry<String, ArrayList<ProductGroupList>> entry : technicianwiseMap.entrySet()) {
//				logger.log(Level.SEVERE, "key:" + entry.getKey());
//				logger.log(Level.SEVERE, "value" + entry.getValue().size());
//			}
//			List<String> statusList = new ArrayList<String>();
//			statusList.add(Service.SERVICESTATUSSCHEDULE);
//			statusList.add(Service.SERVICESTATUSRESCHEDULE);
//			statusList.add(Service.SERVICESTATUSSTARTED);
//			statusList.add(Service.SERVICESTATUSREPORTED);
//
//			List<Service> serviceList = ofy().load().type(Service.class)
//					.filter("companyId", companyId)
//					.filter("employee IN", techList)
//					.filter("isServiceScheduled", true)
//					.filter("status IN", statusList).list();
//			List<Integer> serIdList = new ArrayList<Integer>();
//			List<Integer> projectIdList = new ArrayList<Integer>();
//			// String branch = "";
//			for (Service ser : serviceList) {
//				if (ser.isServiceScheduled() && !(ser.getServiceNumber().equals("") || ser.getServiceNumber() == null)) {
//					if (ser.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
//
//					} else {
//						serIdList.add(ser.getCount());
//						projectIdList.add(ser.getProjectId());
//					}
//				}
//				// branch = ser.getBranch();
//			}
//			for (Integer c : serIdList) {
//				logger.log(Level.SEVERE, "servoce ids" + c);
//			}
//			List<ServiceProject> projectList = new ArrayList<ServiceProject>();
//			if (serIdList != null && serIdList.size() > 0) {
//				projectList = ofy().load().type(ServiceProject.class)
//						.filter("companyId", companyId)
//						.filter("serviceId IN", serIdList)
//						.filter("count IN", projectIdList).list();
//			}
//			Map<String, Double> requiredQuntityMap = new HashMap<String, Double>();
//			Map<String, Double> availableQuntityMap = new HashMap<String, Double>();
//			for (ServiceProject serProject : projectList) {
//				if (serProject != null
//						&& serProject.getProdDetailsList() != null
//						&& serProject.getProdDetailsList().size() > 0) {
//					for (ProductGroupList group : serProject.getProdDetailsList()) {
//						double value = 0;
//						String key = group.getName() + "-"+ group.getStorageBin().trim();
//						if (requiredQuntityMap.containsKey(key)) {
//							value = requiredQuntityMap.get(key);
//							requiredQuntityMap.put(key,value + group.getProActualQty());
//							logger.log(Level.SEVERE,"required map*****************"+ group.getProActualQty() + " "+ key);
//						} else {
//							requiredQuntityMap.put(key, group.getProActualQty());
//							ProductInventoryViewDetails prodInvDtls = ofy().load().type(ProductInventoryViewDetails.class)
//									.filter("companyId", companyId)
//									.filter("prodname", group.getName())
//									.filter("warehousename",group.getWarehouse())
//									.filter("storagelocation",group.getStorageLocation())
//									.filter("storagebin",group.getStorageBin().trim())
//									.first().now();
//							if (prodInvDtls != null) {
//								availableQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(),prodInvDtls.getAvailableqty());
//							} else {
//								availableQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(), 0.0);
//							}
//							logger.log(Level.SEVERE,"available map*****************"+ availableQuntityMap.get(group.getName()+ "-"+ group.getStorageBin().trim()));
//						}
//					}
//				}
//			}
//			
//			for (ProductGroupList group : productList) {
//				double value = 0;
//				String key = group.getName() + "-"+ group.getStorageBin().trim();
//				
//				ProductInventoryViewDetails prodInvDtls = ofy().load()
//						.type(ProductInventoryViewDetails.class)
//						.filter("companyId", companyId)
//						.filter("prodname", group.getName())
//						.filter("warehousename", group.getWarehouse())
//						.filter("storagelocation", group.getStorageLocation())
//						.filter("storagebin", group.getStorageBin().trim())
//						.first().now();
//				if (prodInvDtls != null) {
//					availableQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(),prodInvDtls.getAvailableqty());
//				} else {
//					availableQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(), 0.0);
//				}
//				logger.log(Level.SEVERE,"available map*****************"+ availableQuntityMap.get(group.getName() + "-"+ group.getStorageBin().trim())+" | "+"in use map*****************"+ requiredQuntityMap.get(group.getName() + "-"+ group.getStorageBin().trim()));
//				// }
//			}
			
			/**
			 * Service is scheduled here for technician and will be available on peiod app
			 * for completion
			 */
			for (Service s : sList) {
				s.setServiceScheduled(true);
				s.setServiceNumber(DateUtility.getDateWithTimeZone("IST",s.getServiceDate())+ "-" + s.getTechnicians());
				if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned", s.getCompanyId())) {
					s.setStatus(Service.SERVICESTATUSSCHEDULE);
				}
				newSList.add(s);
				ofy().save().entity(s);
			}
			
			/**
			 * @author Anil @since 22-01-2021
			 * Loading stock master to get  the available stock and in use stock
			 * Also here we will update in use stock
			 */
			Map<String, Double> requiredQuntityMap = new HashMap<String, Double>();
			Map<String, Double> availableQuntityMap = new HashMap<String, Double>();
			try{
				ArrayList<SuperModel> result = impl.getProductInventoryDetails(prodIdList,companyId);
				if(result!= null&&result.size()!=0){
					for(ProductGroupList group : productList){
						
						String techWH = group.getWarehouse();
						String techSL = group.getStorageLocation();
						String techSB = group.getStorageBin();
						boolean updateFlag=false;
						for(SuperModel model : result){
							ProductInventoryView piv = (ProductInventoryView) model;
							if(group.getProduct_id() == piv.getProdID()){
								for(ProductInventoryViewDetails pivd: piv.getDetails()){
								 
									if(pivd.getWarehousename().trim().equals(techWH.trim()) &&
										pivd.getStoragelocation().trim().equals(techSL.trim()) &&
										pivd.getStoragebin().trim().equals(techSB.trim()) ){
										availableQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(),pivd.getAvailableqty());
										requiredQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(),pivd.getInUseStock());
										updateFlag=true;
										double inUseStock = 0, availableStock = 0,quantity=0;
										inUseStock=pivd.getInUseStock();
										availableStock=pivd.getAvailableqty();
										quantity = inUseStock + group.getProActualQty() - availableStock;
										if (quantity<=0) {
											pivd.setInUseStock(pivd.getInUseStock()+group.getProActualQty());
										}
									}
								}
							}
						}
						
						if(!updateFlag){
							availableQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(),0.0);
							requiredQuntityMap.put(group.getName() + "-"+ group.getStorageBin().trim(),0.0);
						}
						logger.log(Level.SEVERE,"AVL MAP: "+ availableQuntityMap.get(group.getName() + "-"+ group.getStorageBin().trim())+" | "+"IN USE STOCK: "+ requiredQuntityMap.get(group.getName() + "-"+ group.getStorageBin().trim()));
					}
					logger.log(Level.SEVERE, "STOCK UPDATE " );
					GenricServiceImpl gimpl=new GenricServiceImpl();
					gimpl.save(result);
				}
			}catch(Exception e){
				
			}
			/**
			 * @author Anil , Date : 11-12-2019 Pedio app FCM Api-this api send
			 * scheduling notification on technician mobile phone
			 */
			ServiceSchedulingNotification obj = new ServiceSchedulingNotification();
			obj.sendSchedulingNotificationToTechnician(sList);

			/**
			 * @author Anil
			 * @since 12-05-2020
			 * updating complaint status created against services
			 */
			ArrayList<Service> servList = new ArrayList<Service>();
			servList.addAll(sList);
			ServiceListServiceImpl servimpl = new ServiceListServiceImpl();
			servimpl.updateComplainDocument(servList, false, false, true, false);

			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();
			
			try{
			
				for (Map.Entry<String, ArrayList<ProductGroupList>> entry : technicianwiseMap.entrySet()) {
					ArrayList<ProductGroupList> groupList = entry.getValue();
					String technician = entry.getKey();
					MaterialMovementNote mmn = new MaterialMovementNote();
					mmn.setServiceId(0);
					mmn.setCompanyId(companyId);
					mmn.setMmnDate(new Date());
					mmn.setBranch(branch);
					mmn.setMmnTransactionType("TransferOUT");
					mmn.setTransDirection("TRANSFEROUT");
					mmn.setEmployee(technician);
					mmn.setMmnPersonResposible(user);
					mmn.setApproverName(user);
					mmn.setCreatedBy(user);
					mmn.setStatus(MaterialMovementNote.REQUESTED);
					mmn.setMmnTitle("Technician Material Transfer");
					mmn.setCreationDate(new Date());
					logger.log(Level.SEVERE, "TransToStorBin******************"+ groupList.get(0).getStorageBin().trim());
					mmn.setTransToStorBin(groupList.get(0).getStorageBin().trim());
					logger.log(Level.SEVERE, "TransToStorLoc*****************"+ groupList.get(0).getStorageLocation());
					mmn.setTransToStorLoc(groupList.get(0).getStorageLocation());
					logger.log(Level.SEVERE, "TransToWareHouse*************"+ groupList.get(0).getWarehouse());
					mmn.setTransToWareHouse(groupList.get(0).getWarehouse());
					logger.log(Level.SEVERE, "from popup product*************"+ groupList.get(0).getName());
					ArrayList<MaterialProduct> materialProductList = new ArrayList<MaterialProduct>();
					for (ProductGroupList m : groupList) {
						MaterialProduct mp = new MaterialProduct();
						Company c = new Company();
						mp.setCompanyId(c.getCompanyId());
						mp.setMaterialProductId(m.getProduct_id());
						mp.setMaterialProductCode(m.getCode());
						mp.setMaterialProductName(m.getName());
	
						mp.setMaterialProductStorageBin(m.getParentStorageBin());
						mp.setMaterialProductStorageLocation(m.getParentStorageLocation());
						mp.setMaterialProductWarehouse(m.getParentWarentwarehouse());
						mp.setMaterialProductUOM(m.getUnit());
						String key = m.getName() + "-" + m.getStorageBin();
						double quantity = 0;
						logger.log(Level.SEVERE, "Transfer quamt******************"+ quantity + "  " + m.getName());
						double value1 = 0, value2 = 0;
						if (requiredQuntityMap.containsKey(key)) {
							value1 = requiredQuntityMap.get(key);
						}
						logger.log(Level.SEVERE, "value1******************"+ value1);
						if (availableQuntityMap.containsKey(key)) {
							value2 = availableQuntityMap.get(key);
						}
						logger.log(Level.SEVERE, "value2******************"+ value2);
						quantity = value1 + m.getProActualQty() - value2;
						if (quantity < 0) {
							quantity = 0;
						}
						logger.log(Level.SEVERE, "Transfer quamt******************"+ quantity);
						mp.setMaterialProductRequiredQuantity(quantity);
	
						materialProductList.add(mp);
					}
					System.out.println("product list*************"+ list.toString());
					
					mmn.setSubProductTableMmn(materialProductList);
					mmn.setServiceIdList(serviceIdList);
					mmn.setMmnCategory("Android");
					SuperModel model = (SuperModel) mmn;
					impl.saveAndApproveDocument(model, 0 + "", "", AppConstants.MMN);
				}
			
			}catch(Exception e){
				
			}
			
			
		} else {
			for (Service s : sList) {
				s.setServiceScheduled(true);
				s.setServiceNumber(DateUtility.getDateWithTimeZone("IST",s.getServiceDate())+ "-" + s.getTechnicians());
				if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned", s.getCompanyId())) {
					s.setStatus(Service.SERVICESTATUSSCHEDULE);
				}
//				newSList.add(s);
				logger.log(Level.SEVERE,s.getCount()+"/"+s.isServiceScheduled()+"/"+s.getServiceNumber());
			}
//			ofy().save().entities(newSList);
			ofy().save().entities(sList).now();

			logger.log(Level.SEVERE, "SERVICE UPDATE SUCCESSFUL");
			
			/**
			 * @author Anil , Date : 25-04-2020
			 */
			ObjectifyService.reset();
			ofy().consistency(Consistency.STRONG);
			ofy().clear();

			try{
				/**
				 * @author Anil , Date : 11-12-2019 Pedio app FCM Api-this api send
				 *         scheduling notification on technician mobile phone
				 */
				ServiceSchedulingNotification obj = new ServiceSchedulingNotification();
				obj.sendSchedulingNotificationToTechnician(sList);
			}catch(Exception e){
				
			}
			
			logger.log(Level.SEVERE, "PEDIO NOTIFICATION PART DONE");
			
			/**
			 * @author Anil
			 * @since 12-05-2020
			 */
			ArrayList<Service> servList = new ArrayList<Service>();
			servList.addAll(sList);
			ServiceListServiceImpl servimpl = new ServiceListServiceImpl();
			servimpl.updateComplainDocument(servList, false, false, true, false);
			
			logger.log(Level.SEVERE, "COMPLAINT STATUS UPDATED");
		}
		ObjectifyService.reset();
		ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		/**
		 * @author Ashwini Patil
		 * @since 04-05-2022
		 * Description: As per Fumitech requirement given by Nithila when technician is assigned, two sms should get send through system.
		 * One to customer and one to technician
		 */
//		SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status",true).filter("companyId", companyId).first().now();
		Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
//		if(smsConfig==null){
//			logger.log(Level.SEVERE,"SMS config is null");
//		}
//		if(smsConfig!=null||comp.isWhatsAppApiStatus()){//Ashwini Patil Date:24-02-2023
//			if(smsConfig.getStatus()==true){
//				String senderId = smsConfig.getAccountSID();
//				String userName = smsConfig.getAuthToken();
//				String password = smsConfig.getPassword();
				
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
				logger.log(Level.SEVERE,"List size before sms"+sList.size());
				SmsTemplate smsTemplateForCustomer = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", "AssignCustomer (Nayara)").first().now();
				SmsTemplate smsTemplateForTechnician = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", "AssignTechnician (Nayara)").first().now();
				
				SmsServiceImpl smsimpl=new SmsServiceImpl();
				
				if(smsTemplateForCustomer==null) {
					logger.log(Level.SEVERE,"AssignCustomer (Nayara) template not found");
				}else if(smsTemplateForCustomer.getStatus()==true) {	
					for(Service s:sList) {
						String msg=smsTemplateForCustomer.getMessage();
						String replacedMsg=msg.replace("{CustomerName}", s.getPersonInfo().getFullName());
						replacedMsg=replacedMsg.replace("{ServiceNumber}", s.getCount()+"");
						replacedMsg=replacedMsg.replace("{ProductName}", s.getProductName());
						replacedMsg=replacedMsg.replace("{DD-MM-YY}", sdf.format(s.getServiceDate())+"");
						replacedMsg=replacedMsg.replace("{TechnicianName}", s.getEmployee());
						
						Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", s.getEmployee()).first().now();				
						if(emp!=null&&emp.getContacts()!=null&&emp.getContacts().get(0).getCellNo1()>0)
						replacedMsg=replacedMsg.replace("{Number}", emp.getContacts().get(0).getCellNo1()+"");
//						replacedMsg=replacedMsg.replace("{Number}", s.getPersonInfo().getCellNumber()+"");
						//						smsimpl.sendSmsToClient(replacedMsg, s.getPersonInfo().getCellNumber(), senderId, userName, password, companyId);
//						logger.log(Level.SEVERE,"msg for customer="+replacedMsg);	
						
						/**
						 * @author Vijay Date :- 03-05-2022
						 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
						 * whats and sms based on communication channel configuration.
						 */
						if(s.getPersonInfo().getCellNumber()!=null) { //Ashwini Patil Date:19-05-2023
							smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.PEDIO,smsTemplateForCustomer.getEvent(),smsTemplateForCustomer.getCompanyId(),replacedMsg,s.getPersonInfo().getCellNumber(),false);
							logger.log(Level.SEVERE,"after sendMessage method");
						}
						else {
							logger.log(Level.SEVERE,"message could not be sent as customer phone is null in service!");
						}
					}
				}
				if(smsTemplateForTechnician==null) {
					logger.log(Level.SEVERE,"AssignTechnician (Nayara) template not found");
				}else if(smsTemplateForTechnician.getStatus()==true){
					for(Service s:sList) {
						HashMap<String, Long> technicianNameCellNoMap=new HashMap<String, Long>();
					    //Ashwini Patil Date:25-04-2023 Orion requirement- send sms/whatsapp message to team after service scheduling
						if(s.getTechnicians()!=null&&s.getTechnicians().size()>0) {
							for(EmployeeInfo info:s.getTechnicians()) {
								if(info.getCellNumber()!=null)
									technicianNameCellNoMap.put(info.getFullName(), info.getCellNumber());
								else
									technicianNameCellNoMap.put(info.getFullName(), null);
							}
							
							logger.log(Level.SEVERE,"team size="+s.getTechnicians().size()+" map size="+technicianNameCellNoMap.size());
							for (Map.Entry<String,Long> entry : technicianNameCellNoMap.entrySet()) {
								String msg=smsTemplateForTechnician.getMessage();						
								String replacedMsg=msg.replace("{TechnicianName}", entry.getKey());
								replacedMsg=replacedMsg.replace("{ServiceNumber}", s.getCount()+"");
								replacedMsg=replacedMsg.replace("{ProductName}", s.getProductName());
								replacedMsg=replacedMsg.replace("{Date}", sdf.format(s.getServiceDate())+"");
								replacedMsg=replacedMsg.replace("{Time}", s.getServiceTime()+"");
								replacedMsg=replacedMsg.replace("{companynumber}",comp.getCellNumber1()+"");
								replacedMsg = replacedMsg.replace("{companyName}", comp.getBusinessUnitName());//added on 6-02-2024
								replacedMsg = replacedMsg.replace("{CompanyName}", comp.getBusinessUnitName());//added on 6-02-2024
								
								if(entry.getValue()!=null&&entry.getValue()>0) {
									smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.PEDIO,smsTemplateForTechnician.getEvent(),smsTemplateForTechnician.getCompanyId(),replacedMsg,entry.getValue(),false);
									logger.log(Level.SEVERE,"after sendMessage method");
								}else
									logger.log(Level.SEVERE,"No cell number found for technician "+entry.getKey());
							}
							
							
						}else {
							String msg=smsTemplateForTechnician.getMessage();						
							String replacedMsg=msg.replace("{TechnicianName}", s.getEmployee());
							replacedMsg=replacedMsg.replace("{ServiceNumber}", s.getCount()+"");
							replacedMsg=replacedMsg.replace("{ProductName}", s.getProductName());
							replacedMsg=replacedMsg.replace("{Date}", sdf.format(s.getServiceDate())+"");
							replacedMsg=replacedMsg.replace("{Time}", s.getServiceTime()+"");
							replacedMsg=replacedMsg.replace("{companynumber}",comp.getCellNumber1()+"");
							replacedMsg = replacedMsg.replace("{companyName}", comp.getBusinessUnitName());//added on 6-02-2024
							replacedMsg = replacedMsg.replace("{CompanyName}", comp.getBusinessUnitName());//added on 6-02-2024
												
							Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", s.getEmployee()).first().now();				
							
//							smsimpl.sendSmsToClient(replacedMsg,emp.getContacts().get(0).getCellNo1(), senderId, userName, password, companyId);						
//							logger.log(Level.SEVERE,"msg for technician="+replacedMsg);			
							/**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							if(emp!=null&&emp.getContacts()!=null&&emp.getContacts().get(0).getCellNo1()>0) {
								smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.PEDIO,smsTemplateForTechnician.getEvent(),smsTemplateForTechnician.getCompanyId(),replacedMsg,emp.getContacts().get(0).getCellNo1(),false);
								logger.log(Level.SEVERE,"after sendMessage method");
							}else {
								logger.log(Level.SEVERE,"No cell number found for employee "+s.getEmployee());
							}
							
							
						}
						
						
						
					}
				}

//			}
//		}
	} 
 
	private void updateServicingBranchInServices(HttpServletRequest request, long companyId) {
 		logger.log(Level.SEVERE, "Servicing Branches Updation");
 		String contractid = request.getParameter("contractId");
 		try {
			
 			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "UpdateServiceBranchInCustomerBranch", companyId)){
 				
 				if(contractid!=null){
	 	 			int contractID = Integer.parseInt(contractid);
	 	 			if(contractID!=0){
	 	 				Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
								.filter("count", contractID).first().now();
	 	 				logger.log(Level.SEVERE, "contractEntity"+contractEntity);
	 	 				
	 	 				List<CustomerBranchDetails> globalCustomerBranchDetailsList = ofy().load().type(CustomerBranchDetails.class)
	 	 						.filter("companyId", companyId).filter("cinfo.count", contractEntity.getCinfo().getCount()).list();
	 	 				logger.log(Level.SEVERE, "globalCustomerBranchDetailsList size"+globalCustomerBranchDetailsList.size());
	 	 				
						if(contractEntity!=null){
							for(SalesLineItem saleslineitem : contractEntity.getItems()){
								ArrayList<BranchWiseScheduling> branchlist =  saleslineitem.getCustomerBranchSchedulingInfo().get(saleslineitem.getProductSrNo());
								ArrayList<BranchWiseScheduling> updatedlist = new ArrayList<BranchWiseScheduling>();

								for(BranchWiseScheduling branchschedule : branchlist){
									branchschedule.setServicingBranch(getCustomerBranchServingBranch(branchschedule.getBranchName(),globalCustomerBranchDetailsList));
									updatedlist.add(branchschedule);
								}
								saleslineitem.getCustomerBranchSchedulingInfo().put(saleslineitem.getProductSrNo(),updatedlist);
							}
							logger.log(Level.SEVERE, "Here contract Updated");
							ofy().save().entity(contractEntity);
							
							
							updateBillDocuments(contractEntity);
						}
	 	 			}
 				}	
 			}
 			else{
 				
 				if(contractid!=null){
	 	 			int contractID = Integer.parseInt(contractid);
	 	 			if(contractID!=0){
	 	 				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	 	 				List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
	 	 											.filter("contractCount", contractID).list();
	 	 		 		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
	 	 				if(servicelist.size()!=0){
	 	 					List<CustomerBranchDetails> customerbranchlist = ofy().load().type(CustomerBranchDetails.class)
	 	 																	.filter("companyId", companyId)
	 	 																	.filter("cinfo.count", servicelist.get(0).getPersonInfo().getCount()).list();
	 	 					logger.log(Level.SEVERE, "customerbranchlist size"+customerbranchlist);
	 	 					
	 	 					List<Service> updatedServicelist = new ArrayList<Service>();
	 	 					for(Service service : servicelist){
	 	 						for(CustomerBranchDetails customerBranch : customerbranchlist ){
	 	 							if(service.getServiceBranch().trim().equals(customerBranch.getBusinessUnitName().trim())){
	 	 								service.setBranch(customerBranch.getBranch());
	 	 								updatedServicelist.add(service);
	 	 							}
	 	 						}
	 	 					}
	 	 					logger.log(Level.SEVERE, "updatedServicelist size "+updatedServicelist.size());
							if(updatedServicelist.size()!=0){
								ofy().save().entities(updatedServicelist);
		 	 					logger.log(Level.SEVERE, "Services updated Successfully");
							}
							
							Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
													.filter("count", contractID).first().now();
							if(contractEntity!=null){
								if(contractEntity.getServiceScheduleList().size()!=0){
//									updateSchedulinglist(contractEntity.getServiceScheduleList(),customerbranchlist,contractEntity);
									ArrayList<ServiceSchedule> updatedScheduleServicelist = new ArrayList<ServiceSchedule>();
									for(ServiceSchedule serviceschedule : contractEntity.getServiceScheduleList()){
										for(CustomerBranchDetails customerbranch : customerbranchlist){
											if(serviceschedule.getScheduleProBranch().trim().equals(customerbranch.getBusinessUnitName().trim())){
												serviceschedule.setServicingBranch(customerbranch.getBranch());
												updatedScheduleServicelist.add(serviceschedule);
											}
										}
									}
									contractEntity.setServiceScheduleList(updatedScheduleServicelist);
									ofy().save().entity(contractEntity);
									logger.log(Level.SEVERE, "Contract Updated Successfully");
								}
								else{
									List<ServiceSchedule> schedulelist =ofy().load().type(ServiceSchedule.class).filter("companyId", companyId).filter("documentType", "Contract").filter("documentId",contractID).list();
			 	 					logger.log(Level.SEVERE, "schedulelist size"+schedulelist.size());
			 	 					if(schedulelist.size()!=0){
			 	 						ArrayList<ServiceSchedule> updatedScheduleServicelist = new ArrayList<ServiceSchedule>();

										for(ServiceSchedule serviceschedule : schedulelist){
											for(CustomerBranchDetails customerbranch : customerbranchlist){
												if(serviceschedule.getScheduleProBranch().trim().equals(customerbranch.getBusinessUnitName().trim())){
													serviceschedule.setServicingBranch(customerbranch.getBranch());
													updatedScheduleServicelist.add(serviceschedule);
												}
											}
										}
										logger.log(Level.SEVERE, "schedulelist updatedServicelist "+updatedScheduleServicelist.size());
										if(updatedScheduleServicelist.size()!=0){
												ofy().save().entity(updatedScheduleServicelist);
												logger.log(Level.SEVERE, "Contract  Sechdule Entity Updated Successfully");

										}
			 	 					}
								}
							}
	 	 				}
	 	 											
	 	 			}
	 	 		}
 			}
 			
				
	 			
		} catch (Exception e) {
			// TODO: handle exception
		}
 		
 		
	}


	



	private void updateSchedulinglist(List<ServiceSchedule> schedulelist, List<CustomerBranchDetails> customerbranchlist, Contract contractEntity) {
			ArrayList<ServiceSchedule> updatedServicelist = new ArrayList<ServiceSchedule>();

		for(ServiceSchedule serviceschedule : schedulelist){
			for(CustomerBranchDetails customerbranch : customerbranchlist){
				if(serviceschedule.getScheduleProBranch().trim().equals(customerbranch.getBusinessUnitName().trim())){
					serviceschedule.setServicingBranch(customerbranch.getBranch());
					updatedServicelist.add(serviceschedule);
				}
			}
		}
		logger.log(Level.SEVERE, "schedulelist updatedServicelist "+updatedServicelist.size());
		if(updatedServicelist.size()!=0){
			if(contractEntity.getServiceScheduleList().size()!=0){
				contractEntity.setServiceScheduleList(updatedServicelist);
				ofy().save().entity(contractEntity);
				logger.log(Level.SEVERE, "Contract Updated Successfully");
			}
			else{
				ofy().save().entity(updatedServicelist);
				logger.log(Level.SEVERE, "Contract  Sechdule Entity Updated Successfully");
			}

		}

	}
	
	private String getCustomerBranchServingBranch(String branchName,
			List<CustomerBranchDetails> globalCustomerBranchDetailsList) {
		for(CustomerBranchDetails customerBranch : globalCustomerBranchDetailsList){
			if(customerBranch.getBusinessUnitName().trim().equals(branchName.trim())){
				logger.log(Level.SEVERE, "servicing Branch"+customerBranch.getBranch());
				return customerBranch.getBranch();
			}
		}
		logger.log(Level.SEVERE, "Returning Null");
		return null;
	}
	
	private void updateBillDocuments(Contract contractEntity) {
		
		List<BillingDocument> billingList = ofy().load().type(BillingDocument.class)
											.filter("companyId", contractEntity.getCompanyId()).filter("contractCount", contractEntity.getCount())
											.list();
		logger.log(Level.SEVERE, "billingList size"+billingList.size());
		if(billingList.size()!=0){
			for(BillingDocument billingDoc : billingList){
				
				HashMap<Integer, ArrayList<BranchWiseScheduling>> map= new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
				ArrayList<BranchWiseScheduling> branchList = new ArrayList<BranchWiseScheduling>();
				Set<BranchWiseScheduling> branchSet = new HashSet<BranchWiseScheduling>();
				if(contractEntity != null && contractEntity.getItems() != null){
					for(SalesLineItem item :contractEntity.getItems()){
						
						branchSet = new HashSet<BranchWiseScheduling>();
						if(item.getCustomerBranchSchedulingInfo() != null && item.getCustomerBranchSchedulingInfo().size() > 0){
							for(BranchWiseScheduling branch : item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
								if(branch.isCheck()){
									if(!branch.getBranchName().equals("Service Address")){
										BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
										branchWiseScheduling.setBranchName(branch.getServicingBranch());
										branchWiseScheduling.setArea(0);
										branchWiseScheduling.setCheck(false);
										branchSet.add(branchWiseScheduling);
									
									}else{						
											BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
											branchWiseScheduling.setBranchName(contractEntity.getBranch());
											branchWiseScheduling.setArea(0);
											branchWiseScheduling.setCheck(false);
											branchSet.add(branchWiseScheduling);
											
									//	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ServiceBranchFromSchedulePopup", this.getCompanyId())){
											BranchWiseScheduling branchWiseScheduling1 = new BranchWiseScheduling();
											branchWiseScheduling1.setBranchName("Service Address");
											branchWiseScheduling1.setArea(0);
											branchWiseScheduling1.setCheck(false);
											branchWiseScheduling1.setServicingBranch(branch.getServicingBranch());
											branchSet.add(branchWiseScheduling1);
									//	}
									}
								}
								
							}
						}
						branchList = new ArrayList<BranchWiseScheduling>(branchSet);
						map.put(item.getProductSrNo(), branchList);
						
					}
					if(billingDoc.getSalesOrderProducts().size()!=0){
						
						for(int i=0;i<billingDoc.getSalesOrderProducts().size();i++){
							if(map != null && map.size() > 0 && map.containsKey(billingDoc.getSalesOrderProducts().get(i).getProductSrNumber())){
								HashMap<Integer, ArrayList<BranchWiseScheduling>> map1= new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								map1.put(billingDoc.getSalesOrderProducts().get(i).getProductSrNumber(), map.get(billingDoc.getSalesOrderProducts().get(i).getProductSrNumber()));
								billingDoc.getSalesOrderProducts().get(i).setServiceBranchesInfo(map1);
							}
							
						}
						ofy().save().entity(billingDoc);
						logger.log(Level.SEVERE, "Billing Doc Updated");
					}
				}
				
			}
		}

		
		
	
	}
		private void sendServiceRevenueLossReport(HttpServletRequest request, long companyId) throws IOException {
		logger.log(Level.SEVERE,"Inside Documenttasque send Service revenue loss report");
		CustomerNameChangeServiceImpl impl=new CustomerNameChangeServiceImpl();
		String fromDate1 = request.getParameter("fromDate").trim();
		String toDate1 = request.getParameter("toDate").trim();
		
		 
			Date fromDate = null , toDate = null;
			try {
				if(fromDate1 != null && !fromDate1.equalsIgnoreCase("null")){
					fromDate = isoFormat.parse(isoFormat.format(sdf.parse(fromDate1)));
					
				}
				if(toDate1 != null && !toDate1.equalsIgnoreCase("null") ){
					toDate = isoFormat.parse(isoFormat.format(sdf.parse(toDate1)));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			ArrayList<String> branchList = new ArrayList<String>();
			 String str = request.getParameter("brnachlist");
			 

				ArrayList<String> emailIdList = new ArrayList<String>();
				 String mail = request.getParameter("EmailList");
				 
			 
			 try{
					Gson gson = new GsonBuilder().create();	
					JSONArray jsonarr=new JSONArray(str);
					logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
					for(int i=0;i<jsonarr.length();i++){
						branchList.add(jsonarr.getString(i));
					}
					
					JSONArray jsonarr1=new JSONArray(mail);
					logger.log(Level.SEVERE, "JSONArray :: "+jsonarr1.length());
					for(int i=0;i<jsonarr1.length();i++){
						emailIdList.add(jsonarr1.getString(i));
					}
					
					
					
					
				logger.log(Level.SEVERE, "branchlist Size"+branchList.size());
					
		List<Service>lossRevenueReport=impl.getServiceListData(companyId, fromDate, toDate, branchList,emailIdList);
		
		logger.log(Level.SEVERE, "Lossrevenue Size"+lossRevenueReport.size());
		
		ArrayList<String>emailList1=emailIdList;
		
		
		
		ArrayList<Service> lossRevenueReportList = new ArrayList<Service>(lossRevenueReport);	 
		
		logger.log(Level.SEVERE, "LossrevenueReportList Size"+lossRevenueReportList.size());
		CsvWriter.services = lossRevenueReportList;
		
		CsvWriter.fromDate = fromDate;
		CsvWriter.toDate = toDate;
		
//		PrintWriter writer =null;      
//      writer = new PrintWriter(new File(".csv")); 
//		PrintWriter writer=new PrintWriter(writer);
		
		CsvWriter csv=new CsvWriter();
		Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
      
		ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
		PrintWriter pw=new PrintWriter(outByteStream);
		csv.getCustomeServiceRevenueLossDetailListCSV(pw);
		pw.flush();
		pw.close();
//		pw.write(outByteStream+"");
		
		byte[] outArray = outByteStream.toByteArray();
		
		logger.log(Level.SEVERE, "outArray  -  sizzzzzzzzze -- "+outArray.length);
		
		
		
		StringBuilder builder =new StringBuilder();


		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + "Service Revenue Loss Report"
				+ "</h3></br></br>");

		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("Please find attachment.");
		
		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		Email email = new Email();
		email.sendMailWithGmail(company.getEmail(),emailList1 ,new ArrayList<String>(), new ArrayList<String>(), "Service Revenue Report Between - "+(fmt.format(fromDate))+" To "+(fmt.format(toDate)),  contentInHtml, "text/html", outByteStream,"Service Revenue Report" + ".csv","text/csv");
        
	  }catch(Exception e){
			 
			 }
		
		
	}
		
	private void updateServices(long updatedNumber, long companyId) {
		
		long number = updatedNumber - 1000;
		logger.log(Level.SEVERE, "number "+number);
		if(number>0){
			List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("count >=", number).filter("count <=", updatedNumber).list();
			logger.log(Level.SEVERE, "servicelist for updating wms flag"+servicelist.size());
			for(Service service : servicelist){
				if(!service.getPersonInfo().getFullName().equalsIgnoreCase("NBHC INTERNAL")){
					service.setWmsServiceFlag(false);
				}
			}
			if(servicelist.size()!=0){
				ofy().save().entities(servicelist);
			}
			updatedNumber = number;
			logger.log(Level.SEVERE, "updatedNumber "+updatedNumber);

			Queue queue = QueueFactory.getQueue("UpdateServices-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "Update WMS Flag Services")
					.param("companyId", companyId+"").param("Number", updatedNumber+""));
		}
		else{
		
			logger.log(Level.SEVERE, "All Data Updated successfully");
			return;
		}
		
	}
	
	
	private void reactonUpdateCustomerBranchServices(long companyId, String strcontractId, String customerBranch) {

		int contractId = 0;
		if(strcontractId!=null && !strcontractId.equals("")) {
			contractId = Integer.parseInt(strcontractId);
		}
		
		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add(Service.SERVICESTATUSOPEN);
		statuslist.add(Service.SERVICESTATUSPENDING);
		statuslist.add(Service.SERVICESTATUSPLANNED);
		statuslist.add(Service.SERVICESTATUSREOPEN);
		statuslist.add(Service.SERVICESTATUSREPORTED);
		statuslist.add(Service.SERVICESTATUSSTARTED);
		statuslist.add(Service.SERVICESTATUSSCHEDULE);
		statuslist.add(Service.SERVICESTATUSRESCHEDULE);

		logger.log(Level.SEVERE, "customerBranch "+customerBranch);

		if(contractId!=0) {
			List<Service> servicelist = ofy().load().type(Service.class).filter("contractCount", contractId).filter("status IN", statuslist)
										.filter("companyId", companyId).filter("serviceBranch", customerBranch).list();
			logger.log(Level.SEVERE, "servicelist for updating Customer Branch service address "+servicelist.size());
			if(servicelist.size()>0) {
				CustomerBranchDetails customerbranchdetails = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId)
						.filter("cinfo.count", servicelist.get(0).getPersonInfo().getCount()).filter("buisnessUnitName", customerBranch).first().now();
				logger.log(Level.SEVERE, "customerbranchdetails"+customerbranchdetails);
				if(customerbranchdetails!=null) {
					for(Service serviceEntity : servicelist) {
						serviceEntity.setAddress(customerbranchdetails.getAddress());
						
						/**
						 *  Added by Priyanka - 20-08-2021
						 *  Des : update cell number, POC Name for branch Requiremnet raised by Rahul.
						 */
						if(customerbranchdetails.getPocName()!=null&&!customerbranchdetails.getPocName().equals("")){
							serviceEntity.getPersonInfo().setPocName(customerbranchdetails.getPocName());
						}
						if(customerbranchdetails.getCellNumber1()!=null&&customerbranchdetails.getCellNumber1()!=0){
							serviceEntity.getPersonInfo().setCellNumber(customerbranchdetails.getCellNumber1());
						}
					}
					ofy().save().entities(servicelist);
					logger.log(Level.SEVERE, "Services customer branch address updated sucessfully");

				}
			}
		}
	}
	
	

	/**
	 * @author Vijay Date 30-11-2020
	 * Des :- when New production link created then Module name and Document names will create
	 * @param accessURL 
	 */
	public void reactOnUpdateModuleAndDocumentNames(String accessURL) {

		UpdateDocumentCronJobImpl updateDocumentNames = new UpdateDocumentCronJobImpl();
		updateDocumentNames.updateDocumentName(accessURL);
	}
	
	/**
	 * @author Vijay Date 30-11-2020
	 * Des :- when New production link created then Tax details Payment terms Country metros cities states and User role 
	 * will create
	 */
	
	private void reactOnDefaultConfiguration(long companyId) {

		ArrayList<Screen> list = getDefualConfigScreenlist();
		for(Screen scr : list){
			createConfigs(companyId,scr);
		}
		
		createConfigDefaultCategory(companyId,Screen.ADDPAYMENTTERMS);
		createCountry(companyId);
		createState(companyId);
		createCity(companyId);
		createuserAuthorizationRoles(companyId);
		createGLAccount(companyId);
		createTaxDetails(companyId);
		createMenuScreenConfiguration(companyId);
		//createSMSTemplate(companyId);
	}

//	private void createSMSTemplate(long companyId) {
//		// TODO Auto-generated method stub
//		smsTemplateMap.put("Service Completion", "Dear {CustomerName} Service No {ServiceNo} Product Name {ProductName} Dated {ServiceDate} has been completed.- {companyName}");
//		smsTemplateMap.put("Payment Received", "Thank you for the payment of Rs {AmountRecieved} against Order No {OrderId} received on {Date}.- {companyName}");
//		smsTemplateMap.put("Contract Approved", "Thank you {customerName} for choosing {companyName}, your contract id is - {contractId} & contract period is from {contractstartDate} to {contractendDate}.");
//		smsTemplateMap.put("Service Due Cron Job", "Dear {CustomerName} your Service {ServiceId} is due on {ServiceDate}. Please revert back in case this date is not suitable to you.- {companyName}");
//		smsTemplateMap.put("Contract Renewal Cron Job", "Dear {CustomerName} your Contract {ContractId} is due for renewal on {endDate}. Please renew to receive uninterrupted services.- {companyName}");
//		smsTemplateMap.put("Amount Receivable Due Cron Job", "Dear {CustomerName} your payment of Rs. {PayableAmt} is due on {PaymentDate}. Please pay on time to avoid penalty thanks.- {companyName}");
//		smsTemplateMap.put("Complaint Created", "Dear {CustomerName} your Complaint for Service {ServiceID} has been registered. Your Complaint ID is {ComplaintID} - {CompanyName}");
//		smsTemplateMap.put("Complaint Completed", "Dear {CustomerName} Your Complaint {ComplaintID} has been resolved on {ServiceDate}. - {CompanyName}");
//		smsTemplateMap.put("Contract Cancellation", "Dear {CustomerName} your Service {ContractId} is cancelled at your request. Please revert back for rescheduling at a date suitable to you.- {CompanyName}");
//		smsTemplateMap.put("Amount Payable", "We have released payment of Rs {PayableAmt} against Invoice No {InvoiceNo} Dated {InvoiceDate} by {PaymentMethod}");
//		smsTemplateMap.put("Delivery Note Approved", "Dear {CustomerName} your Order No {OrderID} is scheduled for shipping. Delivery Date {DeliveryDate} LRNo {LRNo} Delivery Person {DeliveryPerson} CellNo {CellNo}");
//		smsTemplateMap.put("Lead Enquiry", "Thank you for contacting {companyName}. Your enquiry has been successfully registered with number: {leadId}. We will get back to you shortly.");
//		smsTemplateMap.put("Service Completion OTP", "DEAR {CustomerName}, Operator has completed service at your premise. Please provide this OTP {OTP} to the operator. {CompanyName}");
//		smsTemplateMap.put("Service Reschedule OTP", "Service {ServiceId} customer {CustomerName} / {CustomerCellNo} rescheduled to {RescheduleDate}. Provide Approval OTP {OTP} to Technician {TechnicianName}");
//		smsTemplateMap.put("Service Completion (EVRIM)", "Dear {CustomerName} Service No {ServiceNo} under Contract {ContractId} Dated {ServiceDate} has been completed.");
//		smsTemplateMap.put("AssignTechnician (Nayara)", "Dear #TechnicianName #ServiceNumber #ProductName #Date #Time service has been assigned to you. Call back #companynumber in case of any change");
//		smsTemplateMap.put("AssignCustomer (Nayara)", "Dear #CustomerName #ServiceNumber #ProductName service is scheduled on #DD-MM-YY. Technician #TechnicianName #Number will be visiting you shortly");
//		smsTemplateMap.put("StartServiceCustomer (Nayara)", "Dear {CustomerName} {TechnicianName} / {ServiceId} has started to visit your place to do {ProductName} {ServiceNumber}. He will be reaching you shortly. Call back on {CompanyCellNo} in case any change");
//		smsTemplateMap.put("CompleteServiceCustomer (Nayara)", "Dear {CustomerName} {ServiceId} {ProductName} has been completed by {TechnicianName} on {Date}. Call back on {CompanyCellNo} in case any change.");
//		smsTemplateMap.put("ReportedBranch (Nayara)", "Dear {CustomerName} {ServiceId} {ProductName} has been completed by {TechnicianName} on {Date}. Call back on {CompanyCellNo} in case any change.");
//		smsTemplateMap.put("Logout OTP (Universal Pest Control)", "Dear Branch Manager, operator {OperatorName}, wants to logout from application. Please provide this OTP {OTP} to the operator.");
//		smsTemplateMap.put("Technician Started", "Dear {CustomerName}, {TechnicianName} is on {ServiceDate} the way for {ProductName} service to your location.- {CompanyName}");
//		smsTemplateMap.put("Technician Reported", "Dear {CustomerName}, {TechnicianName} has reported to your location for service {ProductName}.- {CompanyName}");
//		smsTemplateMap.put("PaymentGateway", "Dear {CustomerName}, Thank you for signing with us! Complete your payment using the digital link - {ShortLink}");
//		smsTemplateMap.put("PaymentGateway", "Dear {CustomerName}, Thank you for signing with us! Your order {ContractId}  is confirmed and will be serviced shortly");
//		
//		//private ArrayList<SmsTemplate> getsmsTemplatelist(String eventName) {
//////	ArrayList<SmsTemplate> list = new ArrayList<SmsTemplate>();
//////	
//////	if(smsTemplateMap!=null&&smsTemplateMap.size()!=0){
//////		if(smsTemplateMap.containsKey(eventName)){
//////			HashMap<String,ArrayList<String>> map=smsTemplateMap.get(eventName);
//////			 for (Map.Entry<String,ArrayList<String>> entry : map.entrySet()){
//////		        System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
//////		        for(String smstemplate:entry.getValue()){
//////		        	SmsTemplate sms = new SmsTemplate();
//////		        	sms.setAvailableTags(availableTags);
//////		        	sms.setEvent(event);
//////		        	sms.setMessage(smstemplate);
//////		        	sms.setDay(0);
//////		        	sms.setStatus(true);
//////		        	list.add(sms);
//////		        }
//////			 }
//////			return list;
//////		}
//////	}
//////	
//////	
//////	
//////	return null;
//////}
////
//		
//	}

	public void createMenuScreenConfiguration(long companyId) {

		DemoConfigurationImpl demoimpl = new DemoConfigurationImpl();
		
//		ArrayList<String> modulelist = new ArrayList<String>();
//		modulelist.addAll(demoimpl.moduleNames());
//		
		List<ScreenMenuConfiguration> screenMenuEntityList = ofy().load().type(ScreenMenuConfiguration.class).filter("companyId", companyId).list();
//		if (ScreenMenuEntityList.size() != 0) {
//			for (int i = 0; i < ScreenMenuEntityList.size(); i++) {
//				if (modulelist.contains(ScreenMenuEntityList.get(i).getModuleName().trim())){
//					modulelist.remove(ScreenMenuEntityList.get(i).getModuleName().trim());
//				}
//			}
//		}

		//ofy().delete().entities(screenMenuEntityList).now();
		
				
		try{
		
			createScreenMenuConfgData();
			
			if(moduleWiseMenuConfgMap!=null&&moduleWiseMenuConfgMap.size()!=0){
				
				
		        ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
		        
		        for (Map.Entry<String,HashMap<String,String>> entry : moduleWiseMenuConfgMap.entrySet()){
		            
		        	System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
		           
		            String[] arrayStr = entry.getKey().split("[$]");
		            
		            String moduleName="";
		            String documentName="";
		            String menuName="";
		            
		            if(arrayStr.length!=0){
		            	moduleName=arrayStr[0];
		            	documentName=arrayStr[1];
		            	menuName=arrayStr[2];
		            }
		            
		            
		            if(isScreenConfgCreated(moduleName,documentName,menuName,screenMenuEntityList)){
		            	continue;
		            }
		            
		            ScreenMenuConfiguration smc = new ScreenMenuConfiguration();
					smc.setCompanyId(companyId);
					smc.setModuleName(moduleName);
					smc.setDocumentName(documentName);
					smc.setMenuName(menuName);
					smc.setStatus(true);
					
					ArrayList<MenuConfiguration> menuList= new ArrayList<MenuConfiguration>();
					
		            for (Map.Entry<String,String> entry1 : entry.getValue().entrySet())  {
						MenuConfiguration menu = new MenuConfiguration();
						menu.setCompanyId(companyId);
						menu.setStatus(true);
						menu.setName(entry1.getKey());
						menu.setLink(entry1.getValue());
						menuList.add(menu);
		            }
		            smc.setMenuConfigurationList(menuList);
		            
		            modelList.add(smc);
		            
		        }
		        
		        
		        
		        if(modelList.size()!=0){
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(modelList);
				}
		       
			}
		}catch(Exception e){
			
		}
	}

	private boolean isScreenConfgCreated(String moduleName,String documentName, String menuName,List<ScreenMenuConfiguration> screenMenuEntityList) {
		// TODO Auto-generated method stub
		if(screenMenuEntityList!=null&&screenMenuEntityList.size()!=0){
			for(ScreenMenuConfiguration smc:screenMenuEntityList){
				if(smc.getModuleName().equals(moduleName)&&smc.getDocumentName().equals(documentName)&&smc.getMenuName().equals(menuName)){
					return true;
				}
			}
		}
		return false;
	}

	private void createTaxDetails(long companyId) {

		ArrayList<String> taxlist = getTaxCGSTSGSTPecentlist();

		List<TaxDetails> taxdetailsEntitylist = ofy().load().type(TaxDetails.class).filter("companyId", companyId).list();
		if(taxdetailsEntitylist.size()!=0){

			for(int i=0;i<taxdetailsEntitylist.size();i++){
	    			if(taxlist.contains(taxdetailsEntitylist.get(i).getTaxChargePercent())){
	    				taxlist.remove(taxdetailsEntitylist.get(i).getTaxChargePercent());
	    			}
	    	}
		}
		
		ArrayList<TaxDetails> newtaxdetailsList = new ArrayList<TaxDetails>();
		
		for(String strtaxpercent : taxlist){
			
			TaxDetails taxdetails = new TaxDetails();
			taxdetails.setGlAccountName("GST");
			taxdetails.setCompanyId(companyId);
			taxdetails.setTaxChargeName("CGST@"+strtaxpercent);
			taxdetails.setVatTax(true);
			taxdetails.setTaxChargePercent(Double.parseDouble(strtaxpercent));
			taxdetails.setTaxPrintName("CGST");

			newtaxdetailsList.add(taxdetails);
			
			TaxDetails taxDetailsEntity = new TaxDetails();
			taxDetailsEntity.setGlAccountName("GST");
			taxDetailsEntity.setCompanyId(companyId);
			taxDetailsEntity.setTaxChargeName("SGST@"+strtaxpercent);
			taxDetailsEntity.setServiceTax(true);
			taxDetailsEntity.setTaxChargePercent(Double.parseDouble(strtaxpercent));
			taxDetailsEntity.setTaxPrintName("SGST");


			newtaxdetailsList.add(taxDetailsEntity);

		}
		if(newtaxdetailsList.size()!=0){
			ArrayList<SuperModel> list = new ArrayList<SuperModel>();
			list.addAll(newtaxdetailsList);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(list);
		}
		
		
		
		ArrayList<String> igstTaxlist = getTaxIGSTPecentlist();

		if(taxdetailsEntitylist.size()!=0){

			for(int i=0;i<taxdetailsEntitylist.size();i++){
	    			if(igstTaxlist.contains(taxdetailsEntitylist.get(i).getTaxChargePercent())){
	    				igstTaxlist.remove(taxdetailsEntitylist.get(i).getTaxChargePercent());
	    			}
	    	}
		}
		
		ArrayList<TaxDetails> newIGSTTaxdetailsList = new ArrayList<TaxDetails>();
		
		for(String strtaxpercent : igstTaxlist){
			
			TaxDetails taxigstdetails = new TaxDetails();
			taxigstdetails.setGlAccountName("GST");
			taxigstdetails.setCompanyId(companyId);
			taxigstdetails.setTaxChargeName("IGST@"+strtaxpercent);
			taxigstdetails.setVatTax(true);
			taxigstdetails.setTaxChargePercent(Double.parseDouble(strtaxpercent));
			taxigstdetails.setTaxPrintName("IGST");

			newIGSTTaxdetailsList.add(taxigstdetails);
			
		}
		if(newIGSTTaxdetailsList.size()!=0){
			ArrayList<SuperModel> list = new ArrayList<SuperModel>();
			list.addAll(newIGSTTaxdetailsList);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(list);
		}
	
		
	}

	private ArrayList<String> getTaxCGSTSGSTPecentlist() {
		ArrayList<String> gsttaxlist = new ArrayList<String>();
		gsttaxlist.add("9");
		gsttaxlist.add("0");
		gsttaxlist.add("2.5");
		gsttaxlist.add("6");
		gsttaxlist.add("14");

		return gsttaxlist;
	}



	private ArrayList<String> getTaxIGSTPecentlist() {
		ArrayList<String> gsttaxlist = new ArrayList<String>();
		gsttaxlist.add("18");
		gsttaxlist.add("5");
		gsttaxlist.add("12");
		gsttaxlist.add("28");

		return gsttaxlist;
	}





	private ArrayList<String> getTaxlist() {
		ArrayList<String> gsttaxlist = new ArrayList<String>();
		gsttaxlist.add("SGST");
		gsttaxlist.add("CGST");
		gsttaxlist.add("IGST");
		gsttaxlist.add("NA");


		return gsttaxlist;
	}



	private void createGLAccount(long companyId) {

		
		GLAccount glaccountEntity = ofy().load().type(GLAccount.class).filter("companyId", companyId)
				.filter("glAccountName", "GST").first().now();
		if(glaccountEntity==null){
			GLAccount glaccount = new GLAccount();
			glaccount.setCompanyId(companyId);
			glaccount.setGlAccountNo(1);
			glaccount.setGlAccountName("GST");
			glaccount.setStatus("Active");
			glaccount.setGlAccountGroup("GST");
			glaccount.setCreatedBy("");
			glaccount.setUserId("");
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(glaccount);
		}
		
	}



	public void createuserAuthorizationRoles(long companyId) {
		
		createUserRoleDataSet();

		ArrayList<String> roleNameList = getRoleList();
		
		List<UserRole> userRoleEntityList = ofy().load().type(UserRole.class).filter("companyId", companyId).list();
		if (userRoleEntityList.size() != 0) {
			for (int i = 0; i < userRoleEntityList.size(); i++) {
				if (roleNameList.contains(userRoleEntityList.get(i).getRoleName().trim())) {
					roleNameList.remove(userRoleEntityList.get(i).getRoleName().trim());
				}
			}
		}
		
		ArrayList<SuperModel> moduleList = new ArrayList<SuperModel>();
		
		for(String roleName : roleNameList){
			UserRole role=new UserRole();
			role.setRoleName(roleName);
			role.setRoleStatus(true);
			role.setCompanyId(companyId);
			

			List<ScreenAuthorization> authorizationlist = getAuthorizationlist(roleName);
			if(authorizationlist!=null&&authorizationlist.size()!=0){
				role.setAuthorization(authorizationlist);
			}else{
				continue;
			}
			
			moduleList.add(role);
		}
		if(moduleList.size()!=0){
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(moduleList);
		}
		
	}
	
	private void createUserRoleDataSet() {
		// TODO Auto-generated method stub
		String key="";//user role 
		String key2="";//module name
		ArrayList<String> screenNameList=null;
		HashMap<String,ArrayList<String>> map=null;
		
/********************************UserRole-SalePerson******************************************************/		
		key="Sales Person";
		key2="Service";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Customer");
		screenNameList.add("Lead");
		screenNameList.add("Quotation");
		screenNameList.add("Contract");
		screenNameList.add("Dashboard");
		screenNameList.add("Contract Renewal");
		screenNameList.add("Quick Contract");
		screenNameList.add("Sales Person Dashboard");
		screenNameList.add("Sales Person Targets Performance");
		screenNameList.add("Customer Support New");
		screenNameList.add("Customer Support Dashboard");
		screenNameList.add("Assessment Report");
		screenNameList.add("Interaction to Do");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Sales Person";
		key2="Accounts";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Dashboard(AR)");
		screenNameList.add("Billing List");
		screenNameList.add("Invoice List");
		screenNameList.add("Payment List");
		screenNameList.add("Payment Details");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
			
/********************************UserRole-SalePerson******************************************************/

/*******************************UserRole-Branch Manager**************************************************/
		
		key="Branch Manager";
		key2="Service";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Customer");
		screenNameList.add("Lead");
		screenNameList.add("Quotation");
		screenNameList.add("Contract");
		screenNameList.add("Dashboard");
		screenNameList.add("Contract Renewal");
		screenNameList.add("Customer Service List");
		screenNameList.add("Technician Dashboard");
		screenNameList.add("Quick Contract");
		screenNameList.add("Sales Person Dashboard");
		screenNameList.add("Sales Person Targets Performance");
		screenNameList.add("Customer Support New");
		screenNameList.add("Customer Support Dashboard");
		screenNameList.add("Assessment Report");
		screenNameList.add("Interaction to Do");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Branch Manager";
		key2="Accounts";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Dashboard(AR)");
		screenNameList.add("Billing List");
		screenNameList.add("Invoice List");
		screenNameList.add("Payment List");
		screenNameList.add("Payment Details");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Branch Manager";
		key2="Inventory";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Contract P & L");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
/*******************************UserRole-Branch Manager**************************************************/

/*******************************UserRole-Accountant**************************************************/

		key="Accountant";
		key2="Accounts";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Dashboard(AR)");
		screenNameList.add("Billing List");
		screenNameList.add("Invoice List");
		screenNameList.add("Payment List");
		screenNameList.add("Payment Details");
		screenNameList.add("Invoice Details");
		screenNameList.add("Billing Details");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Accountant";
		key2="Service";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Customer");
		screenNameList.add("Contract");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
/*******************************UserRole-Accountant**************************************************/
		
/*******************************UserRole-HR***********************************************************/
		key="HR";
		key2="HR Admin";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Employee");
		screenNameList.add("Leave Application");
		screenNameList.add("Attendence Request");
		screenNameList.add("Upload Attendence");
		screenNameList.add("Create Attendence");
		screenNameList.add("Employee Attendence");
		screenNameList.add("Time Report");
		screenNameList.add("CTC");
		screenNameList.add("CTC allocation");
		screenNameList.add("Leave Balance");
		screenNameList.add("Employee overtime history");
		screenNameList.add("Process Payroll");
		screenNameList.add("Salary Slip");
		screenNameList.add("Company's Contribution");
		screenNameList.add("Interaction To Do List");
		screenNameList.add("Interaction To Do");
		screenNameList.add("Contact Person List");
		screenNameList.add("Contact Person Details");
		screenNameList.add("Project Allocation");
		screenNameList.add("Statutory Reports");
		screenNameList.add("Attendance Report");
		screenNameList.add("Employee Investment");
		screenNameList.add("Employee Asset Allocation");
		screenNameList.add("Adhoc Settlement");
		screenNameList.add("Voluntary PF History");
		screenNameList.add("Compliance Forms");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
/*******************************UserRole-HR***********************************************************/

/*******************************UserRole-HR***********************************************************/
		key="Operations";
		key2="Service";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Customer");
		screenNameList.add("Quotation");
		screenNameList.add("Contract");
		screenNameList.add("Dashboard");
		screenNameList.add("Contract Renewal");
		screenNameList.add("Customer Service List");
		screenNameList.add("Technician Dashboard");
		screenNameList.add("Quick Contract");
		screenNameList.add("Customer Support New");
		screenNameList.add("Customer Support Dashboard");
		screenNameList.add("Assessment Report");
		screenNameList.add("Interaction to Do");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);

/*******************************UserRole-HR End***********************************************************/
		
/*******************************UserRole-Purchase***********************************************************/
		key="Purchase";
		key2="Inventory";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Dashboard");
		screenNameList.add("Inventory Transaction List");
		screenNameList.add("Material Request Note");
		screenNameList.add("Material Issue Note");
		screenNameList.add("Material Movement Note");
		screenNameList.add("Physical Inventory");
		screenNameList.add("Material Consumption Report");
		screenNameList.add("Purchase Requistion List");
		screenNameList.add("Purchase Order");
		screenNameList.add("Purchase Register");
		screenNameList.add("Vendor");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
/*******************************UserRole-Purchase End***********************************************************/
		
/*******************************UserRole-Back-Office***********************************************************/
		
		key="Back Office";
		key2="Service";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Customer");
		screenNameList.add("Quotation");
		screenNameList.add("Contract");
		screenNameList.add("Dashboard");
		screenNameList.add("Contract Renewal");
		screenNameList.add("Customer Service List");
		screenNameList.add("Technician Dashboard");
		screenNameList.add("Quick Contract");
		screenNameList.add("Customer Support New");
		screenNameList.add("Customer Support Dashboard");
		screenNameList.add("Assessment Report");
		screenNameList.add("Interaction to Do");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Back Office";
		key2="Accounts";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Dashboard(AR)");
		screenNameList.add("Billing List");
		screenNameList.add("Invoice List");
		screenNameList.add("Payment List");
		screenNameList.add("Payment Details");
		screenNameList.add("Invoice Details");
		screenNameList.add("Billing Details");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
/*******************************UserRole-Back-Office***********************************************************/

/*******************************UserRole-Manager***********************************************************/
		key="Manager";
		key2="HR Admin";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Employee");
		screenNameList.add("Leave Application");
		screenNameList.add("Attendence Request");
		screenNameList.add("Upload Attendence");
		screenNameList.add("Create Attendence");
		screenNameList.add("Employee Attendence");
		screenNameList.add("Time Report");
		screenNameList.add("CTC");
		screenNameList.add("CTC allocation");
		screenNameList.add("Leave Balance");
		screenNameList.add("Employee overtime history");
		screenNameList.add("Process Payroll");
		screenNameList.add("Salary Slip");
		screenNameList.add("Company's Contribution");
		screenNameList.add("Interaction To Do List");
		screenNameList.add("Interaction To Do");
		screenNameList.add("Contact Person List");
		screenNameList.add("Contact Person Details");
		screenNameList.add("Project Allocation");
		screenNameList.add("Statutory Reports");
		screenNameList.add("Attendance Report");
		screenNameList.add("Employee Investment");
		screenNameList.add("Employee Asset Allocation");
		screenNameList.add("Adhoc Settlement");
		screenNameList.add("Voluntary PF History");
		screenNameList.add("Compliance Forms");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Manager";
		key2="Service";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Customer");
		screenNameList.add("Quotation");
		screenNameList.add("Contract");
		screenNameList.add("Dashboard");
		screenNameList.add("Contract Renewal");
		screenNameList.add("Customer Service List");
		screenNameList.add("Technician Dashboard");
		screenNameList.add("Quick Contract");
		screenNameList.add("Customer Support New");
		screenNameList.add("Customer Support Dashboard");
		screenNameList.add("Assessment Report");
		screenNameList.add("Interaction to Do");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);
		
		key="Manager";
		key2="Accounts";
		screenNameList=new ArrayList<String>();
		map=new HashMap<String, ArrayList<String>>();
		screenNameList.add("Dashboard(AR)");
		screenNameList.add("Billing List");
		screenNameList.add("Invoice List");
		screenNameList.add("Payment List");
		screenNameList.add("Payment Details");
		screenNameList.add("Invoice Details");
		screenNameList.add("Billing Details");
		map.put(key2, screenNameList);
		userRoleMap.put(key, map);


/*******************************UserRole-Manager***********************************************************/
		
		
	}


	private ArrayList<ScreenAuthorization> getAuthorizationlist(String roleName) {
		ArrayList<ScreenAuthorization> list = new ArrayList<ScreenAuthorization>();
		
		if(userRoleMap!=null&&userRoleMap.size()!=0){
			if(userRoleMap.containsKey(roleName)){
				HashMap<String,ArrayList<String>> map=userRoleMap.get(roleName);
				 for (Map.Entry<String,ArrayList<String>> entry : map.entrySet()){
			        System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
			        for(String screenName:entry.getValue()){
			        	ScreenAuthorization screen = new ScreenAuthorization();
						screen.setAndroid(false);
						screen.setCreate(true);
						screen.setDownload(true);
						screen.setView(true);
						screen.setModule(entry.getKey());
						screen.setScreens(screenName);
						list.add(screen);
			        }
				 }
				return list;
			}
		}
		
		
		
		return null;
	}




















	private ArrayList<String> getRoleList() {
		ArrayList<String> rolelist = new ArrayList<String>();
		rolelist.add("Techinician");
		rolelist.add("Operator");
		rolelist.add("Sales Person");
		rolelist.add("Accountant");
		rolelist.add("Operations");
		rolelist.add("Back Office");
		rolelist.add("Zonal Cordinator");
		rolelist.add("Manager");
		rolelist.add("Branch Manager");
		rolelist.add("Admin");
		rolelist.add("Purchase");
		rolelist.add("HR");
		
		return rolelist;
	}


	private void createCity(long companyId) {

		ArrayList<String> citylist = getCitieslist();
		
		List<City> cityEntityList = ofy().load().type(City.class).filter("companyId", companyId).list();
		if(cityEntityList.size()!=0){

			for(int i=0;i<cityEntityList.size();i++){
	    			if(citylist.contains(cityEntityList.get(i).getCityName().trim())){
	    				citylist.remove(cityEntityList.get(i).getCityName().trim());
	    			}
	    	}
		}
		
		ArrayList<City> newcitylist = new ArrayList<City>();
		
		for(String strCity : citylist){
			City cityEntity = new City();
			cityEntity.setCompanyId(companyId);
			cityEntity.setStateName(getStateName(strCity));
			cityEntity.setCityName(strCity);
			cityEntity.setStatus(true);
			newcitylist.add(cityEntity);
		}
		if(newcitylist.size()!=0){
			ArrayList<SuperModel> list = new ArrayList<SuperModel>();
			list.addAll(newcitylist);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(list);
		}
		
	
	}



	private String getStateName(String strCity) {
		if(strCity.equals("Mumbai")){
			return "Maharastra";
		}
		else if(strCity.equals("Delhi")){
			return "Delhi";
		}
		else if(strCity.equals("Kolkata")){
			return "West Bengal";
		}
		else if(strCity.equals("Chennai")){
			return "Tamil Nadu";
		}
		else if(strCity.equals("Bangaluru")){
			return "Karnataka";
		}
		else if(strCity.equals("Ahmedabad")){
			return "Gujarat";
		}
		else if(strCity.equals("Hyderabad")){
			return "Telangana";
		}
		else if(strCity.equals("Pune")){
			return "Maharastra";
		}
		else if(strCity.equals("Surat")){
			return "Gujarat";
		}
		else if(strCity.equals("Jaipur")){
			return "Rajasthan";
		}
		return "";
	}



	private ArrayList<String> getCitieslist() {
		ArrayList<String> citylist = new ArrayList<String>();
		citylist.add("Mumbai");
		citylist.add("Delhi");
		citylist.add("Kolkata");
		citylist.add("Chennai");
		citylist.add("Bangaluru");
		citylist.add("Ahmedabad");
		citylist.add("Hyderabad");
		citylist.add("Pune");
		citylist.add("Surat");
		citylist.add("Jaipur");
		return citylist;
	}




	private void createState(long companyId) {

		ArrayList<String> statelist = getstateList();
		
		List<State> stateEntityList = ofy().load().type(State.class).filter("companyId", companyId).list();
		if(stateEntityList.size()!=0){

			for(int i=0;i<stateEntityList.size();i++){
	    			if(statelist.contains(stateEntityList.get(i).getStateName().trim())){
	    				statelist.remove(stateEntityList.get(i).getStateName().trim());
	    			}
	    	}
		}
		
		ArrayList<State> newstatelist = new ArrayList<State>();
		
		for(String strState : statelist){
			State stateEntity = new State();
			stateEntity.setCompanyId(companyId);
			stateEntity.setCountry("India");
			stateEntity.setStateName(strState);
			stateEntity.setStatus(true);
			newstatelist.add(stateEntity);
		}
		if(newstatelist.size()!=0){
			ArrayList<SuperModel> list = new ArrayList<SuperModel>();
			list.addAll(newstatelist);
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(list);
		}
		
	}


	private ArrayList<String> getstateList() {
		ArrayList<String> statelist = new ArrayList<String>();
		statelist.add("Maharashtra");
		statelist.add("Andra Pradesh");
		statelist.add("Arunachal Pradesh");
		statelist.add("Assam");
		statelist.add("Bihar");
		statelist.add("Chhattisgarh");
		statelist.add("Goa");
		statelist.add("Gujarat");
		statelist.add("Haryana");
		statelist.add("Himachal Pradesh");
		statelist.add("Jammu and Kashmir");
		statelist.add("Jharkhand");
		statelist.add("Karnataka");
		statelist.add("Kerala");
		statelist.add("Madya Pradesh");
		statelist.add("Manipur");
		statelist.add("Nagaland");
		statelist.add("Orissa");
		statelist.add("Punjab");
		statelist.add("Rajasthan");
		statelist.add("Sikkim");
		statelist.add("Tamil Nadu");
		statelist.add("Tripura");
		statelist.add("Uttaranchal");
		statelist.add("Uttar Pradesh");
		statelist.add("West Bengal");
		statelist.add("Chandigarh");
		statelist.add("Delhi");
		statelist.add("Pondicherry");
		
		return statelist;
	}


	private void createCountry(long companyId) {

		Country countryEntity =  ofy().load().type(Country.class).filter("companyId", companyId).filter("countryName", "India")
				.first().now();
		if(countryEntity==null){
			Country country = new Country();
			country.setCompanyId(companyId);
			country.setCountryName("India");
			country.setLanguage("Hindi");
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(country);
		}
		
		
	}



	private void createConfigDefaultCategory(long companyId,
			Screen screenName) {

		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "ConfigCategory").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	int count = (int) lastNumber;
		
    	if(screenName==Screen.ADDPAYMENTTERMS){
    		List<String> paymenttermslist = getpaymenttermslist();
    		List<ConfigCategory> configEntity = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
					.filter("internalType", 25).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(paymenttermslist.contains(configEntity.get(i).getCategoryName().trim())){
    	    				paymenttermslist.remove(configEntity.get(i).getCategoryName().trim());
    	    			}
    	    	}
    		}
    		for(String paymentterms : paymenttermslist){
        		int setcount = ++count;
    		
        		String des = "";
        		if(paymentterms.equals("Monthly")){
        			des = "30";
        		}
        		else if(paymentterms.equals("Half Yearly")){
        			des = "180";
        		}
        		else if(paymentterms.equals("Quartelry")){
        			des = "90";
        		}
        		else if(paymentterms.equals("Advance")){
        			des = "0";
        		}
        		else if(paymentterms.equals("Bi-Monthly")){
        			des = "60";
        		}
        		else if(paymentterms.equals("Yearly")){
        			des = "365";
        		}
        		
	    		String paymentTermsString = getCategoryDataForTaskQueue(CategoryTypes.getCategoryInternalType(screenName),
						paymentterms, "CC" + setcount, companyId,setcount, des);
				Queue queue = QueueFactory.getQueue("CategoryValue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/categoryvaluetaskqueue").param("categoryValuekey", paymentTermsString));
    		}
    	}
    	
    	numbergen.setNumber(count);
  		GenricServiceImpl impl = new GenricServiceImpl();
  		impl.save(numbergen);
	}

	

	private List<String> getpaymenttermslist() {
		ArrayList<String> paymenttermslist = new ArrayList<String>();
		paymenttermslist.add("Monthly");
		paymenttermslist.add("Half Yearly");
		paymenttermslist.add("Quartelry");
		paymenttermslist.add("Advance");
		paymenttermslist.add("Bi-Monthly");
		paymenttermslist.add("Yearly");
		

		return paymenttermslist;
	}



	public String getCategoryDataForTaskQueue(int internalType,String categoryName,String categoryCode,long companyId,int setcount,String desc)
	{
		 String categoryNameValue = new String();
		 categoryNameValue = internalType +"$" +categoryName+"$"+categoryCode +"$"+companyId+"$"+setcount+"$"+desc;
		 return categoryNameValue;
		
	}


	private ArrayList<Screen> getDefualConfigScreenlist() {
		ArrayList<Screen> screenlist = new ArrayList<Screen>();
		screenlist.add(Screen.ARTICLETYPE);
		screenlist.add(Screen.PAYMENTMETHODS);
		screenlist.add(Screen.COMPANYTYPE);
		screenlist.add(Screen.GLACCOUNTGROUP);

		return screenlist;
	}



	private void createConfigs(long companyId, Screen screenName) {

		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Config").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	int count = (int) lastNumber;
		
    	Screen scr = screenName; //74
    	
    	if(screenName==Screen.ARTICLETYPE){
    		List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
    									.filter("type", 74).list();
    		
    	List<String> articleTypelist = getArticleTypeList();  
//    	List<String> updatedArticleType = new ArrayList<String>();
//		updatedArticleType = getArticleTypeList(); 

    	if(configEntity.size()!=0){
    	for(int i=0;i<configEntity.size();i++){
//    		for(int j=0;j<articleTypelist.size();j++){
    			if(articleTypelist.contains(configEntity.get(i).getName().trim())){
    				articleTypelist.remove(configEntity.get(i).getName().trim());
    			}
//    		}
    	}
    	}
    	for(String articleType : articleTypelist){
    		int setcount = ++count;
    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), articleType, companyId,setcount);
    		
    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
    	}
    	
    	}
    	else if(screenName==Screen.PAYMENTMETHODS){
    		List<String> paymentMethodList = getPaymentMethodList();
    		List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
					.filter("type", 16).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(paymentMethodList.contains(configEntity.get(i).getName().trim())){
    	    				paymentMethodList.remove(configEntity.get(i).getName().trim());
    	    			}
    	    	}
    		}
    		for(String articleType : paymentMethodList){
        		int setcount = ++count;
        		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), articleType, companyId,setcount);
        		
        		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
        		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
        	}
        	
    	}
    	else if(screenName==Screen.COMPANYTYPE){

    		List<String> companyTypelist = getCompanyTypeList();
    		List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
					.filter("type", 2).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(companyTypelist.contains(configEntity.get(i).getName().trim())){
    	    				companyTypelist.remove(configEntity.get(i).getName().trim());
    	    			}
    	    	}
    		}
    		for(String articleType : companyTypelist){
        		int setcount = ++count;
        		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), articleType, companyId,setcount);
        		
        		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
        		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
        	}
        	
    	
    	}
    	
    	else if(screenName==Screen.GLACCOUNTGROUP){

    		List<String> glAccount = new ArrayList<String>();
    		glAccount.add("GST");
    		List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
					.filter("type", 48).list();

    		if(configEntity.size()!=0){
    			for(int i=0;i<configEntity.size();i++){
    	    			if(glAccount.contains(configEntity.get(i).getName().trim())){
    	    				glAccount.remove(configEntity.get(i).getName().trim());
    	    			}
    	    	}
    		}
    		for(String articleType : glAccount){
        		int setcount = ++count;
        		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(scr), articleType, companyId,setcount);
        		
        		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
        		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
        	}
        	
    	
    	}

    	
	 	numbergen.setNumber(count);
  		GenricServiceImpl impl = new GenricServiceImpl();
  		impl.save(numbergen);
    	
		
	}
	
	private List<String> getCompanyTypeList() {
		ArrayList<String> companyTypelist = new ArrayList<String>();
		companyTypelist.add("Pest Control");
		companyTypelist.add("HVAC");
		companyTypelist.add("Computer Hardware Maintenance");
		companyTypelist.add("Facility Management");
		companyTypelist.add("UPS Sales & Service");
		companyTypelist.add("Inverter Sales & Service");
		companyTypelist.add("Rental Dress");
		companyTypelist.add("Cleaning Services");
		companyTypelist.add("Bakery");

		return companyTypelist;
	}



	private List<String> getPaymentMethodList() {
		ArrayList<String> paymentMethod = new ArrayList<String>();
		paymentMethod.add("NEFT");
		paymentMethod.add("Cash");
		paymentMethod.add("Cheque");
		paymentMethod.add("UPI");
		paymentMethod.add("GPay");
		paymentMethod.add("PhonePay");
		paymentMethod.add("PayZapp");
		paymentMethod.add("Digital Wallet");
		return paymentMethod;
	}

	
	
	


	private List<String> getArticleTypeList() {
		ArrayList<String> articleTypeList = new ArrayList<String>();
		articleTypeList.add("GSTIN");
		articleTypeList.add("TAN");
		articleTypeList.add("TIN");
		articleTypeList.add("PAN");
		articleTypeList.add("CIN");
		articleTypeList.add("License No");
		articleTypeList.add("PT No");
		articleTypeList.add("PF No");
		articleTypeList.add("ESIC No");
		articleTypeList.add("MSME");
		return articleTypeList;
	}



	public String getConfigDataForTaskQueue(int type, String name, Long companyId, int setcount)
	{
		 String configName = new String();
		 configName = type +"$" +name +"$"+companyId+"$"+setcount;
		 return configName;
		
	}
	
	private void reactonUpdateEmployeeSiteLocationInproject(long companyId, int employeeId, String projectName,
			String empSiteLocation) {
		
			HrProject project=ofy().load().type(HrProject.class).filter("companyId", companyId)
					.filter("projectName", projectName).first().now();
				logger.log(Level.SEVERE, "project"+project);

				/**
				 * @author Vijay Date :- 08-09-2022
				 * Des :- used common method to read OT from project or from entity HrProjectOvertime`
				 */
				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
				CommonServiceImpl commonservice = new CommonServiceImpl();
				overtimelist = commonservice.getHRProjectOvertimelist(project);
				/**
				 * ends here
				 */
				
//				if(project!=null && project.getOtList()!=null&&project.getOtList().size()!=0){
//					for(Overtime ot:project.getOtList()){
				if(project!=null && overtimelist!=null&&overtimelist.size()!=0){
					for(Overtime ot:overtimelist){
						if(ot.getEmpId()==employeeId){
							if(empSiteLocation!=null) {
								ot.setSiteLocation(empSiteLocation);
								logger.log(Level.SEVERE, "emp.getSiteLocation() "+empSiteLocation);

							}
						}
					}
					ofy().save().entity(project).now();
					logger.log(Level.SEVERE, "HRproject employee succussfully ");

				}	
//		}
//		else {
//			if(hrProjectEntity.getProjectName().trim().equals(projectName.trim())) {
//				
//			}
//			else {
//				HrProject project=ofy().load().type(HrProject.class).filter("companyId", companyId)
//						.filter("projectName", projectName).first().now();
//					logger.log(Level.SEVERE, "project"+project);
//
//					if(project!=null && project.getOtList()!=null&&project.getOtList().size()!=0){
//						for(Overtime ot:project.getOtList()){
//							if(ot.getEmpId()==employeeId){
//								if(empSiteLocation!=null) {
//									ot.setSiteLocation(empSiteLocation);
//									logger.log(Level.SEVERE, "emp.getSiteLocation() "+empSiteLocation);
//
//								}
//							}
//						}
//						ofy().save().entity(project).now();
//						logger.log(Level.SEVERE, "HRproject employee succussfully ");
//
//					}	
//			}
//		}
		
		
	}
	
	private void reactonConvertStationTechnicianContractIntoNormalContract(long companyId, int contractId) {
		
		Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count",contractId).first().now();
		if(contractEntity!=null){
			contractEntity.setStationedTechnician(false);
			ofy().save().entity(contractEntity);
			logger.log(Level.SEVERE, "Stationed technician contract updated into normal contract");
		}
		
		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add(Service.SERVICESTATUSSCHEDULE);
		statuslist.add(Service.SERVICESTATUSRESCHEDULE);
		statuslist.add(Service.SERVICESTATUSOPEN);
		statuslist.add(Service.SERVICESTATUSREOPEN);
		
		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("contractCount", contractId).filter("status IN",statuslist).list();
		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
		
		for(Service serviceEntity : servicelist){
			serviceEntity.setServiceScheduled(false);
		}
		ofy().save().entities(servicelist);
		logger.log(Level.SEVERE, "Stationed Technician Services updated into normal services "+servicelist.size());

		
	}
	
	private void reactonConvertNormalContractIntoStationTechnicianContract(long companyId, int contratid) {
		logger.log(Level.SEVERE, "Inside Normal Contract update into Stationed Technician");
		Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count",contratid).first().now();
		if(contractEntity!=null){
			contractEntity.setStationedTechnician(false);
			ofy().save().entity(contractEntity);
			logger.log(Level.SEVERE, "Normal Contract updated into Stationed Technician");
		}
		
		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add(Service.SERVICESTATUSSCHEDULE);
		statuslist.add(Service.SERVICESTATUSRESCHEDULE);
		statuslist.add(Service.SERVICESTATUSOPEN);
		statuslist.add(Service.SERVICESTATUSREOPEN);


		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("contractCount", contratid).filter("status IN",statuslist).list();
		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
		
		for(Service serviceEntity : servicelist){
			serviceEntity.setServiceScheduled(true);
		}
		ofy().save().entities(servicelist);
		logger.log(Level.SEVERE, "Normal Contract updated into Stationed Technician "+servicelist.size());
		
		
	}
	
	private void reactonSendEmail(EmailDetails emaildetails, String uploadDocument, int documentId, long companyId) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Sending email from task queue ");

		Gson gson = new Gson();
		JSONObject jsonObj;
		try {
			try {
			if(uploadDocument!=null){
				jsonObj = new JSONObject(uploadDocument);
				DocumentUpload document = new DocumentUpload();
				document = gson.fromJson(jsonObj.toString(), DocumentUpload.class);
				emaildetails.setUploadedDocument(document);
					
			}
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			if(emaildetails.getEntityName().trim().equals("Lead")) {
				Lead lead = ofy().load().type(Lead.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(lead);
			}
			else if(emaildetails.getEntityName().trim().equals("Quotation")) {
				Quotation quotation = ofy().load().type(Quotation.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(quotation);
			}
			else if(emaildetails.getEntityName().trim().equals("Contract")) {
				Contract contract = ofy().load().type(Contract.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(contract);
			}
			else if(emaildetails.getEntityName().trim().equals("Invoice")) {
				Invoice invoice = ofy().load().type(Invoice.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(invoice);
			}
			else if(emaildetails.getEntityName().trim().equals("CustomerPayment")) {
				CustomerPayment customerpayment = ofy().load().type(CustomerPayment.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(customerpayment);
			}
			else if(emaildetails.getEntityName().trim().equals("SalesQuotation")) {
				SalesQuotation salesQuotation = ofy().load().type(SalesQuotation.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(salesQuotation);
			}
			else if(emaildetails.getEntityName().trim().equals("SalesOrder")) {
				SalesOrder model = ofy().load().type(SalesOrder.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("Complain")) {
				Complain model = ofy().load().type(Complain.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("Service")) {
				Service model = ofy().load().type(Service.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("DeliveryNote")) {
				DeliveryNote model = ofy().load().type(DeliveryNote.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("PurchaseRequisition")) {
				PurchaseRequisition model = ofy().load().type(PurchaseRequisition.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("RequsestForQuotation")) {
				RequsestForQuotation model = ofy().load().type(RequsestForQuotation.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("LetterOfIntent")) {
				LetterOfIntent model = ofy().load().type(LetterOfIntent.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("PurchaseOrder")) {
				PurchaseOrder model = ofy().load().type(PurchaseOrder.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("VendorInvoice")) {
				VendorInvoice model = ofy().load().type(VendorInvoice.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("PaySlip")) {
				PaySlip model = ofy().load().type(PaySlip.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("CustomerPayment")) {
				CustomerPayment model = ofy().load().type(CustomerPayment.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("Contract")) {
				Contract model = ofy().load().type(Contract.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("SalesOrder")) {
				SalesOrder model = ofy().load().type(SalesOrder.class).filter("count", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			else if(emaildetails.getEntityName().trim().equals("ContractRenewal")) {
				ContractRenewal model = ofy().load().type(ContractRenewal.class).filter("contractId", documentId).filter("companyId", companyId).first().now();
				emaildetails.setModel(model);
			}
			
			
			Email email = new Email();
			email.sendNewEmail(emaildetails, companyId,true);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	private ArrayList<String> getemailids(String emailids) {
		
		ArrayList<String> emailIdlist = new ArrayList<String>();
		if(emailids!=null && !emailids.equals("") && emailids.contains(",")){
			String emailidsplit[] = emailids.split("\\,");
			for(int i=0;i<emailidsplit.length;i++){
				if(!emailidsplit[i].equals("")){
					emailIdlist.add(emailidsplit[i].trim());
				}
			}
		}
		else{
			if(emailids!=null && !emailids.equals("")){
				emailIdlist.add(emailids);
			}
		}
		return emailIdlist;
	}
	
	
	private void copyConfiguration(HttpServletRequest request, long companyId) {
		String entity = request.getParameter("entityList");

		String fromLink = request.getParameter("fromLink");
		String toLink = request.getParameter("toLink");
		
		logger.log(Level.SEVERE, "fromLink: " + fromLink+" toLink: "+toLink +" entity "+entity);
		
		String referenceLink=toLink+"/slick_erp/copyConfigurationService";
		logger.log(Level.SEVERE,"referenceLink URL : "+referenceLink);
		
		URL url = null;
		try {
			url = new URL("https://"+referenceLink);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"STAGE 2");
        con.setDoInput(true);
        con.setDoOutput(true);
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 3");
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 4");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
			writer.write(getDataTosend(companyId,fromLink,toLink,entity));
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 5");
        
        
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"STAGE 6");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String temp;
        String data="";
        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp;
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
//        logger.log(Level.SEVERE,"data data::::::::::"+data);
      
        JSONObject jObj = null;
       
        String config=null;
        String configCategory=null;
        String type=null;
        
        String serviceProduct=null;
        String itemProduct=null;
        
        String screenmenuConfig=null;
        String smsTemplate=null;
        String emailTemplate=null;
        String userRole=null;
        String processType=null;
        String processName=null;
        String processConfig=null;
        String numberGeneration=null;
        String country=null;
        String state=null;
        String city=null;
        String productCategory=null;
        String roleDefinition=null;
        String communicationConfig=null;
        String warehouse=null;
        String storageLocation=null;
        String storageBin=null;
        String taxDetails=null; //Added by sheetal:22-01-2022
        String glAccounts=null;//Ashwini Patil Date:8-03-2024
        String shift=null;
        
        String leaveType=null;
        String leaveGroup=null;
        String overtime=null;
        String ctcComponent=null;
        String deduction=null;
        String taxslabConfig=null;
        String investmentConfig=null;
        String paidLeave=null;
        String bonus=null;
        String pt=null;
        String pf=null;
        String lwf=null;
        String esic=null;
        

        String cronJobConfigs=null;
        String cronJobNames=null;
        String materialMovementTypeConfigs=null;
        
        try {
			jObj = new JSONObject(data);
			config = jObj.optString("config").trim();
			configCategory = jObj.optString("configCategory").trim();
			type = jObj.optString("type").trim();
			
			serviceProduct = jObj.optString("serviceProduct").trim();
			itemProduct = jObj.optString("itemProduct").trim();
			
			screenmenuConfig = jObj.optString("screenmenuConfig").trim();
			smsTemplate = jObj.optString("smsTemplate").trim();
			emailTemplate = jObj.optString("emailTemplate").trim();
			userRole = jObj.optString("userRole").trim();
			processType = jObj.optString("processType").trim();
			processName = jObj.optString("processName").trim();
			processConfig = jObj.optString("processConfig").trim();
			numberGeneration = jObj.optString("numberGeneration").trim();
			country = jObj.optString("country").trim();
			state = jObj.optString("state").trim();
			city = jObj.optString("city").trim();
			productCategory = jObj.optString("productCategory").trim();
			roleDefinition = jObj.optString("roleDefinition").trim();
			communicationConfig = jObj.optString("communicationConfig").trim();
			warehouse = jObj.optString("warehouse").trim();
			storageLocation = jObj.optString("storageLocation").trim();
			storageBin = jObj.optString("storageBin").trim();
			taxDetails = jObj.optString("taxDetails").trim();
			try{
			glAccounts=jObj.optString("GLAccount").trim();
			}
			catch (Exception ge) {
			}
			shift = jObj.optString("shift").trim();
			
			leaveType = jObj.optString("leaveType").trim();
			leaveGroup = jObj.optString("leaveGroup").trim();
			overtime = jObj.optString("overtime").trim();
			ctcComponent = jObj.optString("ctcComponent").trim();
			deduction = jObj.optString("deduction").trim();
			taxslabConfig = jObj.optString("taxslabConfig").trim();
			investmentConfig = jObj.optString("investmentConfig").trim();
			paidLeave = jObj.optString("paidLeave").trim();
			bonus = jObj.optString("bonus").trim();
			pt = jObj.optString("pt").trim();
			pf = jObj.optString("pf").trim();
		    lwf = jObj.optString("lwf").trim();
		    esic = jObj.optString("esic").trim();

		    cronJobConfigs=jObj.optString("cronJobConfigrations").trim();
		    cronJobNames=jObj.optString("cronJobNames").trim();
		    materialMovementTypeConfigs=jObj.optString("MaterialMovementTypeConfigrations").trim();
			
			
        }catch (Exception e) {
			e.printStackTrace();
		}
		
        if(config!=null&&!config.equals("")) {
			copyConfig(config,companyId);
		}
        
        if(configCategory!=null&&!configCategory.equals("")) {
        	copyConfigCategory(configCategory,companyId);
        }
        
        if(type!=null&&!type.equals("")) {
        	copyType(type,companyId);
        }
        
        if(serviceProduct!=null&&!serviceProduct.equals("")) {
        	copyserviceProduct(serviceProduct,companyId);
        }
        
        if(itemProduct!=null&&!itemProduct.equals("")) {
        	copyitemProduct(itemProduct,companyId);
        }
        
        if(screenmenuConfig!=null&&!screenmenuConfig.equals("")) {
        	copyscreenmenuConfig(screenmenuConfig,companyId);
        }
        
        if(smsTemplate!=null&&!smsTemplate.equals("")) {
        	copysmsTemplate(smsTemplate,companyId);
        }
        
        if(emailTemplate!=null&&!emailTemplate.equals("")) {
        	copyemailTemplate(emailTemplate,companyId);
        }
        
        if(userRole!=null&&!userRole.equals("")) {
        	copyuserRole(userRole,companyId);
        }
        
        
        if(processName!=null&&!processName.equals("")) {
        	copyprocessName(processName,companyId);
        }
        
        if(processConfig!=null&&!processConfig.equals("")) {
        	copyprocessConfig(processConfig,companyId);
        }
        
        if(numberGeneration!=null&&!numberGeneration.equals("")) {
        	copynumberGeneration(numberGeneration,companyId);
        }
       
        
        if(country!=null&&!country.equals("")) {
        	copycountry(country,companyId);
        }
        
        if(state!=null&&!state.equals("")) {
        	copystate(state,companyId);
        }
        
        if(city!=null&&!city.equals("")) {
        	copycity(city,companyId);
        }
        
        if(productCategory!=null&&!productCategory.equals("")) {
        	copyproductCategory(productCategory,companyId);
        }
        
        if(roleDefinition!=null&&!roleDefinition.equals("")) {
        	copyroleDefinition(roleDefinition,companyId);
        }
        
        if(communicationConfig!=null&&!communicationConfig.equals("")) {
        	copycommunicationConfig(communicationConfig,companyId);
        }
        
        if(warehouse!=null&&!warehouse.equals("")) {
        	copywarehouse(warehouse,companyId);
        }
        
        if(storageLocation!=null&&!storageLocation.equals("")) {
        	copystorageLocation(storageLocation,companyId);
        }
        
        if(storageBin!=null&&!storageBin.equals("")) {
        	copystorageBin(storageBin,companyId);
        }
        
        if(taxDetails!=null&&!taxDetails.equals("")) {
        	copytaxDetails(taxDetails,companyId);
        }
        
        if(glAccounts!=null&&!glAccounts.equals("")) {
        	copyGLAccounts(glAccounts,companyId);
        }
        if(shift!=null&&!shift.equals("")) {
        	copyshift(shift,companyId);
        }
        
        if(leaveType!=null&&!leaveType.equals("")) {
        	copyleaveType(leaveType,companyId);
        }
        
        if(leaveGroup!=null&&!leaveGroup.equals("")) {
        	copyleaveGroup(leaveGroup,companyId);
        }
        
        if(overtime!=null&&!overtime.equals("")) {
        	copyovertime(overtime,companyId);
        }
        
        if(ctcComponent!=null&&!ctcComponent.equals("")) {
        	copyctcComponent(ctcComponent,companyId);
        }
        
        if(deduction!=null&&!deduction.equals("")) {
        	copydeduction(deduction,companyId);
        }
        
        if(taxslabConfig!=null&&!taxslabConfig.equals("")) {
        	copytaxslabConfig(taxslabConfig,companyId);
        }
        
        if(investmentConfig!=null&&!investmentConfig.equals("")) {
        	copyinvestmentConfig(investmentConfig,companyId);
        }
        
        if(paidLeave!=null&&!paidLeave.equals("")) {
        	copypaidLeave(paidLeave,companyId);
        }
        
        if(bonus!=null&&!bonus.equals("")) {
        	copybonus(bonus,companyId);
        }
        
        if(pt!=null&&!pt.equals("")) {
        	copypt(pt,companyId);
        }
        
        if(pf!=null&&!pf.equals("")) {
        	copypf(pf,companyId);
        }
        
        if(lwf!=null&&!lwf.equals("")) {
        	copylwf(lwf,companyId);
        }
        
        if(esic!=null&&!esic.equals("")) {
        	copyesic(esic,companyId);
        }
        
        if(cronJobConfigs!=null&&!cronJobConfigs.equals("")) {
        	 logger.log(Level.SEVERE,"cronJobConfigs received");
        	copyCronJobConfigs(cronJobConfigs,companyId);
        }
        
        if(cronJobNames!=null&&!cronJobNames.equals("")) { //Ashwini Patil Date:20-12-2022
        	logger.log(Level.SEVERE,"cronJobNames received");
        	copyConfigCategory(cronJobNames,companyId);
        }
        
        if(materialMovementTypeConfigs!=null&&!materialMovementTypeConfigs.equals("")) {
       	 logger.log(Level.SEVERE,"materialMovementTypeConfigs received");
       	 copyMaterialMovementTypeConfigs(materialMovementTypeConfigs,companyId);
       }
        
        
        
        logger.log(Level.SEVERE,"STAGE 7");
        
		
	}

	private String getDataTosend(long companyId,String fromLink,String toLink,String entity) {
		// TODO Auto-generated method stub
		StringBuilder strB=new StringBuilder();
		strB.append("jsonString");
		strB.append("=");
		strB.append(createDateInJSONObject(companyId,fromLink,toLink,entity));
		
		return strB.toString();
	}

	private String createDateInJSONObject(long companyId, String fromLink,String toLink, String entity) {
		
		org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
		
		JSONArray jarray = null;
		try {
			jarray = new JSONArray(entity);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		jObj.put("companyId", companyId);
		jObj.put("fromLink", fromLink);
		jObj.put("toLink", toLink);
		jObj.put("entity", jarray);
		logger.log(Level.SEVERE,"entity:: "+entity);
		logger.log(Level.SEVERE,"jarray:: "+jarray);
		
		String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE,"jsonString:: "+jsonString);
		return jsonString;
	}
	
	private void copynumberGeneration(String numberGeneration, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<NumberGeneration> entityList = new ArrayList<NumberGeneration>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(numberGeneration);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),NumberGeneration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Number Generation:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<NumberGeneration> existingEntityList=ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(NumberGeneration obj:entityList){
						boolean updateFlag=false;
						for(NumberGeneration obj1:existingEntityList){
							if(obj.getProcessName().equals(obj1.getProcessName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(NumberGeneration obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyConfig(String config,long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<Config> configList = new ArrayList<Config>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(config);
			for (int i = 0; i < jsonarr.length(); i++) {
				configList.add(gson.fromJson(jsonarr.get(i).toString(),Config.class));
			}
			
			if(configList!=null&&configList.size()!=0){
				logger.log(Level.SEVERE, "configList :: " + configList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Config> existingConfigList=ofy().load().type(Config.class).filter("companyId", companyId).filter("status", true).list();
				if(existingConfigList!=null&&existingConfigList.size()!=0){
					for(Config obj:configList){
						boolean updateFlag=false;
						for(Config obj1:existingConfigList){
							if(obj.getType()==obj1.getType()&&obj.getName().equals(obj1.getName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Config obj:configList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyConfigCategory(String configCategory, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<ConfigCategory> entityList = new ArrayList<ConfigCategory>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(configCategory);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ConfigCategory.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "configCategory :: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ConfigCategory> existingEntityList=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ConfigCategory obj:entityList){
						boolean updateFlag=false;
						for(ConfigCategory obj1:existingEntityList){
							if(obj.getInternalType()==obj1.getInternalType()&&obj.getCategoryName().equals(obj1.getCategoryName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ConfigCategory obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyType(String type, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Type> entityList = new ArrayList<Type>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(type);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Type.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Type :: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Type> existingEntityList=ofy().load().type(Type.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Type obj:entityList){
						boolean updateFlag=false;
						for(Type obj1:existingEntityList){
							if(obj.getInternalType()==obj1.getInternalType()&&obj.getCategoryName().equals(obj1.getCategoryName())&&obj.getTypeName().equals(obj1.getTypeName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Type obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyserviceProduct(String serviceProduct, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<ServiceProduct> entityList = new ArrayList<ServiceProduct>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(serviceProduct);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ServiceProduct.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "ServiceProduct:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ServiceProduct> existingEntityList=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ServiceProduct obj:entityList){
						boolean updateFlag=false;
						for(ServiceProduct obj1:existingEntityList){
							if(obj.getProductCode().equals(obj1.getProductCode())&&obj.getProductName().equals(obj1.getProductName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ServiceProduct obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}


	private void copyitemProduct(String itemProduct, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<ItemProduct> entityList = new ArrayList<ItemProduct>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(itemProduct);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ItemProduct.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Item Product :: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ItemProduct> existingEntityList=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ItemProduct obj:entityList){
						boolean updateFlag=false;
						for(ItemProduct obj1:existingEntityList){
							if(obj.getSerProdCode().equals(obj1.getSerProdCode())&&obj.getSerProdName().equals(obj1.getSerProdName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ItemProduct obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
		
	}
    
	private void copyscreenmenuConfig(String screenmenuConfig, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<ScreenMenuConfiguration> entityList = new ArrayList<ScreenMenuConfiguration>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(screenmenuConfig);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ScreenMenuConfiguration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Screen Menu Configuration :: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				/*
				 * Ashwini Patil 
				 * Date:8-03-2024
				 * We have updated all the help related videos on reference link
				 * Now we want to replace existing screen menu configurations of every client with the new configs from reference link
				 * so deleting old configs and creating new.
				 */
				List<ScreenMenuConfiguration> existingEntityList=ofy().load().type(ScreenMenuConfiguration.class).filter("companyId", companyId).filter("status", true).list();
				ofy().delete().entities(existingEntityList);
				logger.log(Level.SEVERE, "deleted existingEntityList size=" + existingEntityList.size());
				
				
//				if(existingEntityList!=null&&existingEntityList.size()!=0){
//					for(ScreenMenuConfiguration obj:entityList){
//						boolean updateFlag=false;
//						for(ScreenMenuConfiguration obj1:existingEntityList){
//							if(obj.getModuleName().equals(obj1.getModuleName())&&obj.getDocumentName().equals(obj1.getDocumentName())&&obj.getMenuName().equals(obj1.getMenuName())){
//								deletionList.add(obj1);								
//								updateFlag=true;
//								break;
//							}
//						}
//						if(updateFlag){
//							continue;
//						}
//						obj.setId(null);
//						obj.setCount(0);
//						obj.setCompanyId(companyId);
//						obj.setCreatedBy("");
//						SuperModel model=obj;
//						modelList.add(model);
//					}
//				}else{
					for(ScreenMenuConfiguration obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
//				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
		
	}
	
	private void copysmsTemplate(String smsTemplate, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<SmsTemplate> entityList = new ArrayList<SmsTemplate>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(smsTemplate);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),SmsTemplate.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "SMS Template:: " + entityList.size());
				Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				String companyName = companyEntity.getBusinessUnitName();
				List<SmsTemplate> updatedSMSTemplatelist = new ArrayList<SmsTemplate>();
				for(SmsTemplate smstemplate : entityList) {
					String message = smstemplate.getMessage();
					if(message.contains("{companyName}")) {
						message = message.replace("{companyName}", companyName);
					}
					else if(message.contains("{CompanyName}")) {
						message = message.replace("{companyName}", companyName);
					}
					else if(message.contains("{companyname}")) {
						message = message.replace("{companyname}", companyName);
					}
					
					smstemplate.setMessage(message);
					updatedSMSTemplatelist.add(smstemplate);
				}
				
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
//				List<SmsTemplate> existingEntityList=ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("status", true).list();
				List<SmsTemplate> existingEntityList=ofy().load().type(SmsTemplate.class).filter("companyId", companyId).list();

				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(SmsTemplate obj:updatedSMSTemplatelist){
						boolean updateFlag=false;
						for(SmsTemplate obj1:existingEntityList){
							if(obj.getEvent().equals(obj1.getEvent())){
								if(obj.getAutomaticMsg()!=null) {
									obj1.setAutomaticMsg(obj.getAutomaticMsg());
									ofy().save().entity(obj1);
								}
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(SmsTemplate obj:updatedSMSTemplatelist){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
   
	private void copyemailTemplate(String emailTemplate, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<EmailTemplate> entityList = new ArrayList<EmailTemplate>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(emailTemplate);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),EmailTemplate.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Email Template:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<EmailTemplate> existingEntityList=ofy().load().type(EmailTemplate.class).filter("companyId", companyId).filter("templateStatus", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(EmailTemplate obj:entityList){
						boolean updateFlag=false;
						for(EmailTemplate obj1:existingEntityList){
							if(obj.getTemplateName().equals(obj1.getTemplateName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						String emailBody=obj.getEmailBody();
						obj.setEmailBody(emailBody.replace("<slashn>","\n")); //Ashwini Patil Date:22-12-2022
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(EmailTemplate obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						String emailBody=obj.getEmailBody();
						obj.setEmailBody(emailBody.replace("<slashn>","\n"));//Ashwini Patil Date:22-12-2022
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
			
		} catch (Exception e) {

		}
	}
	
	private void copyuserRole(String userRole, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<UserRole> entityList = new ArrayList<UserRole>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(userRole);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),UserRole.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "User Role:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<UserRole> existingEntityList=ofy().load().type(UserRole.class).filter("companyId", companyId).filter("roleStatus", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(UserRole obj:entityList){
						boolean updateFlag=false;
						for(UserRole obj1:existingEntityList){
							if(obj.getRoleName().equals(obj1.getRoleName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(UserRole obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyprocessName(String processName, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<ProcessName> entityList = new ArrayList<ProcessName>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(processName);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ProcessName.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Process Name:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ProcessName> existingEntityList=ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ProcessName obj:entityList){
						boolean updateFlag=false;
						for(ProcessName obj1:existingEntityList){
							if(obj.getProcessName().equals(obj1.getProcessName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ProcessName obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
    
	private void copyprocessConfig(String processConfig, long companyId) {
		GenricServiceImpl impl=new GenricServiceImpl();
		List<ProcessConfiguration> entityList = new ArrayList<ProcessConfiguration>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(processConfig);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ProcessConfiguration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Process Configuration:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ProcessConfiguration> existingEntityList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("configStatus", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ProcessConfiguration obj:entityList){
						boolean updateFlag=false;
						for(ProcessConfiguration obj1:existingEntityList){
							if(obj.getProcessName().equals(obj1.getProcessName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ProcessConfiguration obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
					activateEVAERPUsageCronJob(companyId);  //we are activating evaerpusage cronjob for new clients when we copy process configurations from referencelink
			}
			
		} catch (Exception e) {

		}
	}
	
	
	
	
	private void copycountry(String country, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Country> entityList = new ArrayList<Country>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(country);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Country.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Country:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Country> existingEntityList=ofy().load().type(Country.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Country obj:entityList){
						boolean updateFlag=false;
						for(Country obj1:existingEntityList){
							if(obj.getCountryName().equals(obj1.getCountryName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Country obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copystate(String state, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<State> entityList = new ArrayList<State>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(state);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),State.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "State:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<State> existingEntityList=ofy().load().type(State.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(State obj:entityList){
						boolean updateFlag=false;
						for(State obj1:existingEntityList){
							if(obj.getCountry().equals(obj1.getCountry())&&obj.getStateName().equals(obj1.getStateName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(State obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copycity(String city, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<City> entityList = new ArrayList<City>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(city);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),City.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "City:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<City> existingEntityList=ofy().load().type(City.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(City obj:entityList){
						boolean updateFlag=false;
						for(City obj1:existingEntityList){
							if(obj.getStateName().equals(obj1.getStateName())&&obj.getCityName().equals(obj1.getCityName())){
								updateFlag=true;
								break;
							}
						} 
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(City obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyproductCategory(String productCategory, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Category> entityList = new ArrayList<Category>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(productCategory);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Category.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Product Category:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Category> existingEntityList=ofy().load().type(Category.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Category obj:entityList){
						boolean updateFlag=false;
						for(Category obj1:existingEntityList){
							if(obj.getCatCode().equals(obj1.getCatCode())&&obj.getCatName().equals(obj1.getCatName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Category obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyroleDefinition(String roleDefinition, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<RoleDefinition> entityList = new ArrayList<RoleDefinition>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(roleDefinition);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),RoleDefinition.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Role Definition:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<RoleDefinition> existingEntityList=ofy().load().type(RoleDefinition.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(RoleDefinition obj:entityList){
						boolean updateFlag=false;
						for(RoleDefinition obj1:existingEntityList){
							if(obj.getModuleName().equals(obj1.getModuleName())&&obj.getDocumentName().equals(obj1.getDocumentName())&&obj.getRoleType().equals(obj1.getRoleType())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(RoleDefinition obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copycommunicationConfig(String communicationConfig, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<EmailTemplateConfiguration> entityList = new ArrayList<EmailTemplateConfiguration>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(communicationConfig);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),EmailTemplateConfiguration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Communication Configuration:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
//				List<EmailTemplateConfiguration> existingEntityList=ofy().load().type(EmailTemplateConfiguration.class).filter("companyId", companyId).filter("status", true).list();
				List<EmailTemplateConfiguration> existingEntityList=ofy().load().type(EmailTemplateConfiguration.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(EmailTemplateConfiguration obj:entityList){
						boolean updateFlag=false;
						for(EmailTemplateConfiguration obj1:existingEntityList){
							if(obj.getModuleName().equals(obj1.getModuleName())&&obj.getDocumentName().equals(obj1.getDocumentName())&&obj.getCommunicationChannel().equals(obj1.getCommunicationChannel())&&obj.getTemplateName().equals(obj1.getTemplateName())){
								
								obj1.setAutomaticMsg(obj.isAutomaticMsg());
								obj1.setSendMsgAuto(obj.isSendMsgAuto());
								ofy().save().entity(obj1);
									
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(EmailTemplateConfiguration obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copywarehouse(String warehouse, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<WareHouse> entityList = new ArrayList<WareHouse>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(warehouse);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),WareHouse.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "warehouse:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<WareHouse> existingEntityList=ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("buisnessUnitName", "Central Warehouse").filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(WareHouse obj:entityList){
						boolean updateFlag=false;
						for(WareHouse obj1:existingEntityList){
							if(obj.getBusinessUnitName().equals(obj1.getBusinessUnitName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(WareHouse obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copystorageLocation(String storageLocation, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<StorageLocation> entityList = new ArrayList<StorageLocation>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(storageLocation);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),StorageLocation.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Storage Location:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<StorageLocation> existingEntityList=ofy().load().type(StorageLocation.class).filter("companyId", companyId).filter("buisnessUnitName", "Central Warehouse").filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(StorageLocation obj:entityList){
						boolean updateFlag=false;
						for(StorageLocation obj1:existingEntityList){
							if(obj.getBusinessUnitName().equals(obj1.getBusinessUnitName())&&obj.getWarehouseName().equals(obj1.getWarehouseName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(StorageLocation obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copystorageBin(String storageBin, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Storagebin> entityList = new ArrayList<Storagebin>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(storageBin);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Storagebin.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Storage bin:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Storagebin> existingEntityList=ofy().load().type(Storagebin.class).filter("companyId", companyId).filter("buisnessUnitName", "Central Warehouse").filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Storagebin obj:entityList){
						boolean updateFlag=false;
						for(Storagebin obj1:existingEntityList){
							if(obj.getWarehouseName().equals(obj1.getWarehouseName())&&obj.getStoragelocation().equals(obj1.getStoragelocation())&&obj.getBinName().equals(obj1.getBinName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Storagebin obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	// Added by sheetal:28-01-2022
	private void copytaxDetails(String taxDetails, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<TaxDetails> entityList = new ArrayList<TaxDetails>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(taxDetails);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),TaxDetails.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "TaxDetails:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<TaxDetails> existingEntityList=ofy().load().type(TaxDetails.class).filter("companyId", companyId).filter("taxChargeStatus", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(TaxDetails obj:entityList){
						boolean updateFlag=false;
						for(TaxDetails obj1:existingEntityList){
							if(obj.getTaxChargeName().equals(obj1.getTaxChargeName())&&obj.getTaxPrintName().equals(obj1.getTaxPrintName())&&obj.getTaxChargePercent().equals(obj1.getTaxChargePercent())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(TaxDetails obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	private void copyshift(String shift, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Shift> entityList = new ArrayList<Shift>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(shift);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Shift.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Shift:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Shift> existingEntityList=ofy().load().type(Shift.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Shift obj:entityList){
						boolean updateFlag=false;
						for(Shift obj1:existingEntityList){
							if(obj.getShiftName().equals(obj1.getShiftName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Shift obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyleaveType(String leaveType, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<LeaveType> entityList = new ArrayList<LeaveType>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(leaveType);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),LeaveType.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "LeaveType:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<LeaveType> existingEntityList=ofy().load().type(LeaveType.class).filter("companyId", companyId).filter("leaveStatus", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(LeaveType obj:entityList){
						boolean updateFlag=false;
						for(LeaveType obj1:existingEntityList){
							if(obj.getName().equals(obj1.getName())&&obj.getShortName().equals(obj1.getShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(LeaveType obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyleaveGroup(String leaveGroup, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<LeaveGroup> entityList = new ArrayList<LeaveGroup>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(leaveGroup);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),LeaveGroup.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Leave Group:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<LeaveGroup> existingEntityList=ofy().load().type(LeaveGroup.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(LeaveGroup obj:entityList){
						boolean updateFlag=false;
						for(LeaveGroup obj1:existingEntityList){
							if(obj.getGroupName().equals(obj1.getGroupName())&&obj.getStatus().equals(obj1.getStatus())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(LeaveGroup obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyovertime(String overtime, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Overtime> entityList = new ArrayList<Overtime>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(overtime);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Overtime.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Overtime:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Overtime> existingEntityList=ofy().load().type(Overtime.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Overtime obj:entityList){
						boolean updateFlag=false;
						for(Overtime obj1:existingEntityList){
							if(obj.getName().equals(obj1.getName())&&obj.getShortName().equals(obj1.getShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Overtime obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	
	
	private void copyctcComponent(String ctcComponent, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<CtcComponent> entityList = new ArrayList<CtcComponent>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(ctcComponent);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),CtcComponent.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "CTC Component:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<CtcComponent> existingEntityList=ofy().load().type(CtcComponent.class).filter("companyId", companyId).filter("status", true).filter("isDeduction", false).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(CtcComponent obj:entityList){
						boolean updateFlag=false;
						for(CtcComponent obj1:existingEntityList){
							if(obj.getName().equals(obj1.getName())&&obj.getShortName().equals(obj1.getShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(CtcComponent obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copydeduction(String deduction, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<CtcComponent> entityList = new ArrayList<CtcComponent>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(deduction);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),CtcComponent.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "deduction:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<CtcComponent> existingEntityList=ofy().load().type(CtcComponent.class).filter("companyId", companyId).filter("status", true).filter("isDeduction", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(CtcComponent obj:entityList){
						boolean updateFlag=false;
						for(CtcComponent obj1:existingEntityList){
							if(obj.getName().equals(obj1.getName())&&obj.getShortName().equals(obj1.getShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(CtcComponent obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}

	private void copytaxslabConfig(String taxslabConfig, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<TaxSlab> entityList = new ArrayList<TaxSlab>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(taxslabConfig);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),TaxSlab.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Tax Slab Configuration:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<TaxSlab> existingEntityList=ofy().load().type(TaxSlab.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(TaxSlab obj:entityList){
						boolean updateFlag=false;
						for(TaxSlab obj1:existingEntityList){
							if(obj.getYear().equals(obj1.getYear())&&obj.getCategory().equals(obj1.getCategory())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(TaxSlab obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyinvestmentConfig(String investmentConfig, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Investment> entityList = new ArrayList<Investment>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(investmentConfig);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Investment.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Investment Configuration:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Investment> existingEntityList=ofy().load().type(Investment.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Investment obj:entityList){
						boolean updateFlag=false;
						for(Investment obj1:existingEntityList){
							if(obj.getSectionName().equals(obj1.getSectionName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Investment obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copypaidLeave(String paidLeave, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<PaidLeave> entityList = new ArrayList<PaidLeave>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(paidLeave);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),PaidLeave.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Paid Leave:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<PaidLeave> existingEntityList=ofy().load().type(PaidLeave.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(PaidLeave obj:entityList){
						boolean updateFlag=false;
						for(PaidLeave obj1:existingEntityList){
							if(obj.getPaidLeaveName().equals(obj1.getPaidLeaveName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(PaidLeave obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copybonus(String bonus, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Bonus> entityList = new ArrayList<Bonus>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(bonus);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Bonus.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Bonus:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Bonus> existingEntityList=ofy().load().type(Bonus.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Bonus obj:entityList){
						boolean updateFlag=false;
						for(Bonus obj1:existingEntityList){
							if(obj.getBonusName().equals(obj1.getBonusName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Bonus obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copypt(String pt, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<ProfessionalTax> entityList = new ArrayList<ProfessionalTax>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(pt);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ProfessionalTax.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Professional Tax:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ProfessionalTax> existingEntityList=ofy().load().type(ProfessionalTax.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ProfessionalTax obj:entityList){
						boolean updateFlag=false;
						for(ProfessionalTax obj1:existingEntityList){
							if(obj.getState().equals(obj1.getState())&&obj.getComponentName().equals(obj1.getComponentName())&&obj.getShortName().equals(obj1.getShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ProfessionalTax obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copypf(String pf, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<ProvidentFund> entityList = new ArrayList<ProvidentFund>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(pf);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ProvidentFund.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Provident Fund:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ProvidentFund> existingEntityList=ofy().load().type(ProvidentFund.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ProvidentFund obj:entityList){
						boolean updateFlag=false;
						for(ProvidentFund obj1:existingEntityList){
							if(obj.getPfName().equals(obj1.getPfName())&&obj.getPfShortName().equals(obj1.getPfShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ProvidentFund obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copylwf(String lwf, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<LWF> entityList = new ArrayList<LWF>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(lwf);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),LWF.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "LWF:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<LWF> existingEntityList=ofy().load().type(LWF.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(LWF obj:entityList){
						boolean updateFlag=false;
						for(LWF obj1:existingEntityList){
							if(obj.getState().equals(obj1.getState())&&obj.getName().equals(obj1.getName())&&obj.getShortName().equals(obj1.getShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(LWF obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyesic(String esic, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<Esic> entityList = new ArrayList<Esic>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(esic);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Esic.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Esic:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Esic> existingEntityList=ofy().load().type(Esic.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Esic obj:entityList){
						boolean updateFlag=false;
						for(Esic obj1:existingEntityList){
							if(obj.getEsicName().equals(obj1.getEsicName())&&obj.getEsicShortName().equals(obj1.getEsicShortName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Esic obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyMaterialMovementTypeConfigs(String materialMovementTypeConfigs, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<MaterialMovementType> entityList = new ArrayList<MaterialMovementType>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(materialMovementTypeConfigs);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),MaterialMovementType.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "MaterialMovementType configs:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<MaterialMovementType> existingEntityList=ofy().load().type(MaterialMovementType.class).filter("companyId", companyId).filter("configStatus", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(MaterialMovementType obj:entityList){
						boolean updateFlag=false;
						for(MaterialMovementType obj1:existingEntityList){
							if(obj.getMmtName().equals(obj1.getMmtName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						logger.log(Level.SEVERE, "updating " + obj.getMmtName());
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(MaterialMovementType obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
					logger.log(Level.SEVERE, "MaterialMovementType config saved ");
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	private void copyCronJobConfigs(String cronJobConfigs, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<CronJobConfigration> entityList = new ArrayList<CronJobConfigration>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(cronJobConfigs);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),CronJobConfigration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "CronJobConfigrations:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
//				List<CronJobConfigration> existingEntityList=ofy().load().type(CronJobConfigration.class).filter("companyId", companyId).filter("configStatus", true).list();
				List<CronJobConfigration> existingEntityList=ofy().load().type(CronJobConfigration.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(CronJobConfigration obj:entityList){
						boolean updateFlag=false;
						for(CronJobConfigration obj1:existingEntityList){
							if(obj.getCronJobsName().equals(obj1.getCronJobsName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
					
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(CronJobConfigration obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
					logger.log(Level.SEVERE, "cron config saved ");
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	
	private void UpdateServiceValue(HttpServletRequest request, long companyId) {
		
		try {
			String strcontractId= request.getParameter("ContractId");
			int contractId= Integer.parseInt(strcontractId);
			
			ServerAppUtility serverappUtility = new ServerAppUtility();
			
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", contractId).first().now();
			if(contractEntity!=null){
				for(SalesLineItem item : contractEntity.getItems()){
					List<Service> serviceList = ofy().load().type(Service.class).filter("contractCount", contractEntity.getCount())
												.filter("product.productCode", item.getProductCode()).list();
					for(Service serviceEntity : serviceList){
						if(serviceEntity.getServiceSrNo()==item.getProductSrNo()){
							serviceEntity.setServiceValue(serverappUtility.getServiceValue(item, serviceEntity.getServiceBranch(), serviceEntity.getServiceSerialNo(), companyId,serviceEntity.isServiceWiseBilling()));
						}
					}
					ofy().save().entities(serviceList);
					logger.log(Level.SEVERE,"Service Value updation Product Code "+item.getProductCode() +" product sr No "+item.getProductSrNo() +"Updated sucessfully");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	private void markcompleteServices(HttpServletRequest request, long companyId) {
		
		String Remark = request.getParameter("Remark");
		String strServiceCompletionDate = request.getParameter("ServiceCompletionDate");
		Date serviceCompletionDate = null;
		if(!strServiceCompletionDate.equals("NoDate")){
			try {
				serviceCompletionDate = fmt.parse(strServiceCompletionDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		companyId = Long.parseLong(request.getParameter("companyId"));
		 ArrayList<Integer> serviceIdList = new ArrayList<Integer>();
		 String str = request.getParameter("serviceId");
		 ArrayList<Service> serviceList = new ArrayList<Service>();
		 try{
				Gson gson = new GsonBuilder().create();	
				JSONArray jsonarr=new JSONArray(str);
				logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
				for(int i=0;i<jsonarr.length();i++){
					serviceIdList.add(jsonarr.getInt(i));
				}
				logger.log(Level.SEVERE, "serviceIdList size"+serviceIdList.size());

				List<Service> serList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("count IN",serviceIdList).list();

				serviceList.addAll(serList);
				logger.log(Level.SEVERE, "list after : " + serList);
				logger.log(Level.SEVERE, "list after : " + serList.size());
		 }catch(Exception e){
			e.printStackTrace(); 
		 }
		 
		try {
			 if(serviceList.size()>0) {
					ServiceListServiceImpl servicelistimpl = new ServiceListServiceImpl();	
					servicelistimpl.multipleServicesMarkComplete(serviceList, Remark, serviceCompletionDate); 
			 }
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void copyPedioConfiguration(HttpServletRequest request,
			long companyId) {
		
//		String entity = request.getParameter("entityList");

		String fromLink = request.getParameter("fromLink");
		String toLink = request.getParameter("toLink");
		
		logger.log(Level.SEVERE, "fromLink :"+fromLink + " toLink: "+toLink );
		
		String referenceLink=toLink;
		logger.log(Level.SEVERE,"referenceLink URL : "+referenceLink);
		
		URL url = null;
		try {
			url = new URL("https://"+referenceLink);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(60000);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"STAGE 2");
        con.setDoInput(true);
        con.setDoOutput(true);
        try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			logger.log(Level.SEVERE,"ProtocolException ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 3");
        OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"STAGE 4");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
			writer.write(getDataTosend(companyId,fromLink,toLink));
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException2 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        try {
			writer.flush();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException3 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        logger.log(Level.SEVERE,"STAGE 5");
        
        
        
        InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException4 ERROR::::::::::"+e);
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"STAGE 6");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String temp;
        String data="";
        try {
			while ((temp = br.readLine()) != null) {
			    data = data + temp;
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException5 ERROR::::::::::"+e);
			e.printStackTrace();
		}
        
        logger.log(Level.SEVERE,"data data::::::::::"+data);
      
        JSONObject jObj = null;
        
        String numberGenration = null;
        String userRole = null;
        String processConfiguration=null;
        String moduleName=null;
        String documentName=null;
        
		try {
			jObj = new JSONObject(data);
			numberGenration = jObj.optString("numberGeneration").trim();
			userRole = jObj.optString("userRole").trim();
			processConfiguration = jObj.optString("processConfiguration").trim();
			moduleName = jObj.optString("moduleName").trim();
			documentName = jObj.optString("documentName").trim();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     	
		if(numberGenration!=null && !numberGenration.equals("")){
			copyNumberGenration(numberGenration,companyId);
		}
		if(userRole!=null && !userRole.equals("")){
			copyUserRole(userRole,companyId);
		}
		if(processConfiguration!=null && !processConfiguration.equals("")){
			copyProcessConfiguration(processConfiguration,companyId);
		}
		if(moduleName!=null && !moduleName.equals("")){
			copyModuleName(moduleName,companyId);
		}
		if(documentName!=null && !documentName.equals("")){
			copyDocumentName(documentName,companyId);
		}
	}
	
	private void copyDocumentName(String documentName, long companyId) {
		
		GenricServiceImpl impl=new GenricServiceImpl();
		List<Type> entityList = new ArrayList<Type>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(documentName);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),Type.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Type :: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<Type> existingEntityList=ofy().load().type(Type.class).filter("companyId", companyId)
								.filter("internalType", 13).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(Type obj:entityList){
						boolean updateFlag=false;
						for(Type obj1:existingEntityList){
							if(obj.getCategoryName().equals(obj1.getCategoryName())&&obj.getTypeName().equals(obj1.getTypeName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(Type obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {
			
		}
	
	}

	private void copyModuleName(String moduleName, long companyId) {
		

		GenricServiceImpl impl=new GenricServiceImpl();
		List<ConfigCategory> entityList = new ArrayList<ConfigCategory>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(moduleName);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ConfigCategory.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "configCategory :: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<ConfigCategory> existingEntityList=ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
								.filter("internalType", 13).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ConfigCategory obj:entityList){
						boolean updateFlag=false;
						for(ConfigCategory obj1:existingEntityList){
							if(obj.getCategoryName().equals(obj1.getCategoryName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(ConfigCategory obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


	private void copyProcessConfiguration(String processConfiguration, long companyId) {
		

		GenricServiceImpl impl=new GenricServiceImpl();
		List<ProcessConfiguration> entityList = new ArrayList<ProcessConfiguration>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(processConfiguration);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),ProcessConfiguration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Process Configuration:: " + entityList.size());
				List<ProcessConfiguration> newentrylist = new ArrayList<ProcessConfiguration>();
				newentrylist.addAll(entityList);
				
				List<ProcessConfiguration> existingEntityList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(ProcessConfiguration obj : entityList){
						for(ProcessConfiguration existingObj:existingEntityList){
							if(existingObj.getProcessName().equals(obj.getProcessName())){
								List<ProcessTypeDetails> updatedlist = new ArrayList<ProcessTypeDetails>();
								updatedlist.addAll(obj.getProcessList());
								
								for(ProcessTypeDetails processtype : existingObj.getProcessList()){
									for(ProcessTypeDetails newprocesstype : obj.getProcessList()){
										if(newprocesstype.getProcessType().equals(processtype.getProcessType())){
											updatedlist.remove(newprocesstype);
										}
//										else{
//											updatedlist.add(newprocesstype);
//										}
									}
								}
								logger.log(Level.SEVERE, "updatedlist"+updatedlist.size());

								newentrylist.remove(obj);
								if(updatedlist.size()>0) {
									updatedlist.addAll(existingObj.getProcessList());
									existingObj.setProcessList(updatedlist);
									ofy().save().entity(existingObj);
								}
								
							}
						}
					}

				}
				
				if(newentrylist!=null && newentrylist.size()>0){
					logger.log(Level.SEVERE, "no entry for EVA Pedio so copying all newentrylist "+newentrylist.size());
					ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
					for(ProcessConfiguration obj : newentrylist){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
					logger.log(Level.SEVERE, "modelList :: " + modelList.size());
					if(modelList.size()!=0){
						impl.save(modelList);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();

		}
	}


	private void copyUserRole(String userRole, long companyId) {
		

		GenricServiceImpl impl=new GenricServiceImpl();
		List<UserRole> entityList = new ArrayList<UserRole>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(userRole);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),UserRole.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "User Role:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				
				List<UserRole> newentrylist = new ArrayList<UserRole>();
				newentrylist.addAll(entityList);
				
				List<UserRole> existingEntityList=ofy().load().type(UserRole.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(UserRole obj:entityList){
						for(UserRole obj1:existingEntityList){
							if(obj.getRoleName().equals(obj1.getRoleName())){
								obj1.setAuthorization(obj.getAuthorization());
								newentrylist.remove(obj);
							}
							ofy().save().entities(obj1);
						}
						
					}
				}
				if(newentrylist!=null && newentrylist.size()>0){
					for(UserRole obj : newentrylist){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);	
					}
					logger.log(Level.SEVERE, "modelList :: " + modelList.size());
					if(modelList.size()!=0){
						impl.save(modelList);
					}
				}
				
			}
			
		} catch (Exception e) {

		}
	
		
	}


	private void copyNumberGenration(String numberGenration, long companyId) {
		
		try {
			GenricServiceImpl impl=new GenricServiceImpl();

			List<NumberGeneration> entityList = new ArrayList<NumberGeneration>();
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(numberGenration);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),NumberGeneration.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "Number Generation:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<NumberGeneration> existingEntityList=ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("status", true).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(NumberGeneration obj:entityList){
						boolean updateFlag=false;
						for(NumberGeneration obj1:existingEntityList){
							if(obj.getProcessName().equals(obj1.getProcessName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
					
					logger.log(Level.SEVERE, "modelList :: " + modelList.size());
					if(modelList.size()!=0){
						impl.save(modelList);
					}
				
				}
			}	
			
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
	}











	private String getDataTosend(long companyId,String fromLink,String toLink) {
		// TODO Auto-generated method stub
		StringBuilder strB=new StringBuilder();
		strB.append("jsonString");
		strB.append("=");
		strB.append(createDateInJSONObject(companyId,fromLink,toLink));
		
		return strB.toString();
	}

	private String createDateInJSONObject(long companyId, String fromLink,String toLink) {
		
		org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
		
		jObj.put("companyId", companyId);
		jObj.put("fromLink", fromLink);
		jObj.put("toLink", toLink);
		
		String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE,"jsonString:: "+jsonString);
		return jsonString;
	}
	
	private void reactOnShareCustomerPortalLink(long companyId,boolean smsflag,boolean whatsappflag,boolean emailflag) {
		try {
		logger.log(Level.SEVERE,"sms whatsapp and email flags are "+smsflag+whatsappflag+emailflag);
		String errorReport="";
		String successReport="<br/><b> Find the Success report below</b>";
		String custportalLink="";
		List<Contract> contractList = new ArrayList<Contract>();
		contractList = ofy().load().type(Contract.class)
				.filter("companyId", companyId).filter("endDate >=", new Date())
				.filter("status", Contract.APPROVED).list();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		List<Contract> contractList1 = new ArrayList<Contract>();
		Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();	
		companyobj=comp;
		String Url ="";
		custportalLink=ServerAppUtility.getCustomerPortalLink(comp);
		logger.log(Level.SEVERE,"custportalLink="+custportalLink);
		if(!custportalLink.equalsIgnoreCase("failed")) {
			
		if(contractList!=null) {
			logger.log(Level.SEVERE,"contractList size="+contractList.size());
			
			for(Contract c:contractList) {
				if(c.getStartDate().before(new Date())||sdf.format(c.getStartDate()).equals(sdf.format(new Date()))) {
					contractList1.add(c);
				}
			}				
		}
		if(contractList1!=null&&contractList1.size()>0) {
			logger.log(Level.SEVERE,"contractList1 size="+contractList1.size());
			HashSet<Integer> custIdSet=new HashSet<Integer>();
			HashSet<String> companyBranchSet=new HashSet<String>();
			HashMap<Integer,Contract> custAndContractMap=new HashMap<Integer, Contract>();
			SmsTemplate smsTemplate=null;
			EmailTemplate emailTemplate = null;
			
			SmsServiceImpl smsimpl=new SmsServiceImpl();
			
			if(smsflag||whatsappflag) {
				smsTemplate= ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE ).filter("status",true).first().now();
				if(smsTemplate==null) {
					errorReport+="<br/>"+"communication template 'Customer Portal Link Communication' not found";
					logger.log(Level.SEVERE,"communication template 'Customer Portal Link Communication' not found");
				}
			}
			if(emailflag) {
				emailTemplate =ofy().load().type(EmailTemplate.class).filter("companyId",companyId)
						.filter("templateName",AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE)	
						.filter("templateStatus", true).first().now();
				if(emailTemplate==null) {
						errorReport+="<br/>"+"Email template 'Customer Portal Link Communication' not found";
						logger.log(Level.SEVERE,"Email template 'Customer Portal Link Communication' not found");						
				}
			}	 
			for(Contract c:contractList1) {
				custIdSet.add( c.getCinfo().getCount());				
				custAndContractMap.put(c.getCinfo().getCount(), c);
			}
			logger.log(Level.SEVERE,"custIdSet size="+custIdSet.size());
			logger.log(Level.SEVERE,"custAndContractMap size="+custAndContractMap.size());
			ArrayList<Integer> custIdList=new ArrayList<Integer>(custIdSet);
			logger.log(Level.SEVERE,"custIdList size="+custIdList.size());
			if(custIdList!=null) {
				List<Customer> customerlist = new ArrayList<Customer>();
				customerlist = ofy().load().type(Customer.class).filter("companyId", companyId)
						.filter("count IN",custIdList).list();
				
				
				HashMap<String, String> branchSignatureMap=new HashMap<String, String>();
				HashMap<String, String> branchOrcompanyNameMap=new HashMap<String, String>();
				HashMap<String, String> branchOrcompanyEmailMap=new HashMap<String, String>();
				String companyName=comp.getBusinessUnitName();
				String companySignature=ServerAppUtility.getCompanySignature(comp, null);
				String companyEmail=comp.getEmail();
				if(customerlist!=null&customerlist.size()>0) {
					for(Customer cust:customerlist) {
						companyBranchSet.add(cust.getBranch());
					}
				}
				
				ArrayList<String> companyBranchNamesList=new ArrayList<String>(companyBranchSet);				
				logger.log(Level.SEVERE,"companyBranchNamesList size="+companyBranchNamesList.size());
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)){
					for(String bname:companyBranchNamesList) {
						branchOrcompanyNameMap.put(bname, ServerAppUtility.getCompanyName(bname, comp));
						branchSignatureMap.put(bname, ServerAppUtility.getCompanySignature(bname, comp.getCompanyId()));
						branchOrcompanyEmailMap.put(bname, ServerAppUtility.getCompanyEmail(bname, comp));
					}								
				}
					

				logger.log(Level.SEVERE,"branchOrcompanyNameMap size="+branchOrcompanyNameMap.size());
				logger.log(Level.SEVERE,"branchSignatureMap size="+branchSignatureMap.size());
				logger.log(Level.SEVERE,"branchOrcompanyEmailMap size="+branchOrcompanyEmailMap.size());
				
				if(customerlist!=null) {
					logger.log(Level.SEVERE,"customerlist size="+customerlist.size());
					for(Customer c:customerlist) {
						if(!c.isDisableCustomerPortal()) {
							Contract con=custAndContractMap.get(c.getCount());	
							logger.log(Level.SEVERE,"Customer Portal active for this customer");
							if(smsflag) {								 
									 if(smsTemplate!=null) {

										 String acManagerName="";
											if(con.getAccountManager()!=null)
												acManagerName=con.getAccountManager();
											String templatemsgwithbraces = smsTemplate.getMessage();
											String customerName = templatemsgwithbraces.replace("{CustomerName}", con.getCinfo().getFullName());
											String customerCell = customerName.replace("{CustomerMobile}", con.getCinfo().getCellNumber()+"");
											String conId=customerCell.replace("{ContractId}", con.getCount()+"");
											String conDate=conId.replace("{ContractDate}",sdf.format( con.getContractDate()));
											String conStartdate=conDate.replace("{ContractStartDate}", sdf.format(con.getStartDate()));
											String conEnddate=conStartdate.replace("{ContractEndDate}",sdf.format(con.getEndDate()));
											String acManager=conEnddate.replace("{AccountManager}",acManagerName);
											String portalLink=acManager.replace("{PortalLink}",custportalLink);
											String actualMessage =portalLink;
											if(branchOrcompanyNameMap.size()>0)
												actualMessage = portalLink.replace("{CompanyName}",branchOrcompanyNameMap.get(c.getBranch()) );
											else
												actualMessage = portalLink.replace("{CompanyName}",companyName );
											
											logger.log(Level.SEVERE, AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE+" actaulMsg" + actualMessage);
											if(c.getCellNumber1()>0) {
													String smsResponse = smsimpl.sendSmsToClient(actualMessage, c.getCellNumber1()+"", companyId, AppConstants.SMS);
													logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);	
													if(smsResponse.contains("success"))
														successReport+="<br/>"+"SMS may have been sent successfully to "+c.getFullname()+" ("+c.getCount()+"). Please check sms portal for Actual status.";
													else
														errorReport+="<br/>"+"SMS sending failed to customer "+c.getFullname()+" ("+c.getCount()+")";
													
														
											}else
												errorReport+="<br/>"+"No cell number found in customer "+c.getFullname()+" ("+c.getCount()+")";
											

				
									}else {
//										logger.log(Level.SEVERE,"sms template 'Customer Portal Link Communication' not found");									
									}
								 
								}							
							
							if(whatsappflag) {
									 if(smsTemplate!=null) {

										 String acManagerName="";
											if(con.getAccountManager()!=null)
												acManagerName=con.getAccountManager();
											String templatemsgwithbraces = smsTemplate.getMessage();
											String customerName = templatemsgwithbraces.replace("{CustomerName}", con.getCinfo().getFullName());
											String customerCell = customerName.replace("{CustomerMobile}", con.getCinfo().getCellNumber()+"");
											String conId=customerCell.replace("{ContractId}", con.getCount()+"");
											String conDate=conId.replace("{ContractDate}",sdf.format( con.getContractDate()));
											String conStartdate=conDate.replace("{ContractStartDate}", sdf.format(con.getStartDate()));
											String conEnddate=conStartdate.replace("{ContractEndDate}",sdf.format(con.getEndDate()));
											String acManager=conEnddate.replace("{AccountManager}",acManagerName);
											String portalLink=acManager.replace("{PortalLink}",custportalLink);
											
											String actualMessage =portalLink;
											if(branchOrcompanyNameMap.size()>0)
												actualMessage = portalLink.replace("{CompanyName}",branchOrcompanyNameMap.get(c.getBranch()) );
											else
												actualMessage = portalLink.replace("{CompanyName}",companyName );
											
											
											logger.log(Level.SEVERE, AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE+" actaulMsg" + actualMessage);
											if(c.getCellNumber1()>0) {
												String response = smsimpl.sendMessageOnWhatsApp(companyId, c.getCellNumber1()+"", actualMessage);
												logger.log(Level.SEVERE,"whats app response"+response);
												if(response.contains("success"))
													successReport+="<br/>"+"WhatsApp Message sent successfully to "+c.getFullname()+" ("+c.getCount()+")";
												else
													errorReport+="<br/>"+"WhatsApp Message sending failed to customer "+c.getFullname()+" ("+c.getCount()+")";
												
											}else {
												errorReport+="<br/>"+"No cell number found in customer "+c.getFullname()+" ("+c.getCount()+")";
											}

											
										
									}else {
//										logger.log(Level.SEVERE,"sms template 'Customer Portal Link Communication' not found");									
									}								 								
							
							}
							 if(emailflag) {
								 					 
								if(emailTemplate!=null) {
									String mailSubject = emailTemplate.getSubject();
									String emailbody = emailTemplate.getEmailBody();
									String resultString = emailbody.replaceAll("[\n]", "<br>");
									String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
									emailbody=resultString1;
									
									
									ArrayList<String> toEmailList=new ArrayList<String>();
									if(c.getEmail()!=null&&!c.getEmail().equals(""))
										toEmailList.add(c.getEmail());
									else {
										logger.log(Level.SEVERE,"No email found in customer");	
										errorReport+="<br/>"+"No email found in customer "+c.getFullname()+" ("+c.getCount()+")";
									}
									
									if(comp.getEmail()==null||comp.getEmail().equals("")) {
										logger.log(Level.SEVERE,"No email found in company");	
										errorReport+="<br/>"+"No email found in company";
									}
										
									String acManagerName="";
									if(con.getAccountManager()!=null)
										acManagerName=con.getAccountManager();
									String customerName = emailbody.replace("{CustomerName}", con.getCinfo().getFullName());
									String customerCell = customerName.replace("{CustomerMobile}", con.getCinfo().getCellNumber()+"");
									String conId=customerCell.replace("{ContractId}", con.getCount()+"");
									String conDate=conId.replace("{ContractDate}",sdf.format( con.getContractDate()));
									String conStartdate=conDate.replace("{ContractStartDate}", sdf.format(con.getStartDate()));
									String conEnddate=conStartdate.replace("{ContractEndDate}",sdf.format(con.getEndDate()));
									String acManager=conEnddate.replace("{AccountManager}",acManagerName);
									String portalLink=acManager.replace("{PortalLink}","<a href=\""+custportalLink+"\"><button> View Portal </button></a>");				
									String compName =portalLink;
									if(branchOrcompanyNameMap.size()>0)
										compName = portalLink.replace("{CompanyName}",branchOrcompanyNameMap.get(c.getBranch()) );
									else
										compName = portalLink.replace("{CompanyName}",companyName );
									
									if(branchSignatureMap.size()>0)
										emailbody=compName.replace("{CompanySignature}", branchSignatureMap.get(c.getBranch()));
									else
										emailbody=compName.replace("{CompanySignature}", companySignature);
									
									
									if(branchOrcompanyEmailMap.size()>0) {
										if(branchOrcompanyEmailMap.get(c.getBranch())!=null)
											companyEmail=branchOrcompanyEmailMap.get(c.getBranch());
										else
											companyEmail=comp.getEmail();
									}
									
										
									if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, comp.getCompanyId())){
										if(toEmailList.size()>0&&companyEmail!=null&&!companyEmail.equals("")) {
											SendGridEmailServlet sdEmail=new SendGridEmailServlet();
											String msg=sdEmail.sendMailWithSendGrid(companyEmail, toEmailList, null, null, mailSubject, emailbody, "text/html",null,null,"application/pdf",null);		
											logger.log(Level.SEVERE,"Email body:"+emailbody);
											if(msg.contains("Email sent"))
												successReport+="<br/>"+"Customer portal link successfully shared with customer "+con.getCinfo().getFullName()+" at "+c.getEmail();
										}
									}else {
											errorReport+="\n"+"Could not share customer portal link to customer via email as 'Email'-'EnableSendGridEmailApi' process config inactive.";															
					
									}
								}else {
//									logger.log(Level.SEVERE,"email template 'Customer Portal Link Communication' not found");
								}
							
							 }
						}else {
							errorReport+="<br/>"+"Customer Portal Disabled for "+c.getFullname()+" ("+c.getCount()+")";
						}
					}
				}
			}
		}else {
			errorReport+="<br/>"+"There are no active contracts";
			logger.log(Level.SEVERE,"There are no active contracts"); 
		}
		}
		else {
			errorReport+="<br/>"+"Company url missing in company information.";
			logger.log(Level.SEVERE,"Company url missing in company information.");
		}
		logger.log(Level.SEVERE,"errorReport="+errorReport); 
		String finalreport="";
		if(!errorReport.equals("")) {
			finalreport="<b>Find the Failure report below</b> <br/>"+errorReport+"<br/>"+successReport;
		}
		else {
			finalreport=successReport;
		}
		if(comp.getEmail()!=null&&!comp.getEmail().equals("")) {
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			ArrayList<String> toEmailList=new ArrayList<String>();
			toEmailList.add(comp.getEmail());
			logger.log(Level.SEVERE,"sending error report to "+comp.getEmail()); 
			sdEmail.sendMailWithSendGrid(comp.getEmail(), toEmailList, null, null, "Customer Portal Link Sharing Process Success/Failure Report", finalreport, "text/html",null,null,"application/pdf",null);												
		}
			
			
		
		}
		catch(Exception e) {
			e.printStackTrace();
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			ArrayList<String> toEmailList=new ArrayList<String>();
			toEmailList.add("support@evasoftwaresolutions.com");
			if(companyobj.getCompanyURL()!=null&&!companyobj.getCompanyURL().equals(""))
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, "Customer portal link sharing(Bulk) process failed for client "+companyobj.getCompanyURL()+" due to exception", e.getMessage(), "text/html",null,null,"application/pdf",null);															
			else
				sdEmail.sendMailWithSendGrid("support@evasoftwaresolutions.com", toEmailList, null, null, "Customer portal link sharing(Bulk) process failed for client "+companyobj.getBusinessUnitName()+" due to exception", e.getMessage(), "text/html",null,null,"application/pdf",null);															
			
		}
	}

	
	private void sendContractDataOnEmail(HttpServletRequest request, long companyId) {
	
		Company compEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		 SimpleDateFormat format= new SimpleDateFormat("MM/dd/yyyy");
		 String strfromDate = request.getParameter("fromDate");
		 String strtoDate = request.getParameter("toDate");
		 String activeContracts = request.getParameter("activeContracts");

		 String link = request.getParameter("link");
		 
		 ArrayList<String> emailList = new ArrayList<String>();
		 Gson gson = new GsonBuilder().create();	
		 String stremail = request.getParameter("email");
		 try{
				JSONArray jsonarr=new JSONArray(stremail);
				logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
				for(int i=0;i<jsonarr.length();i++){
					emailList.add(jsonarr.getString(i));
				}
		 }catch(Exception e){
			 
		 }
		 Date fromDate = null;
		 Date toDate = null;

		 try {
			fromDate = format.parse(strfromDate);
			toDate = format.parse(strtoDate);
		} catch (ParseException e) {
//			e.printStackTrace();
		}
		 boolean activeContractFlag = false;
		 if(activeContracts!=null && activeContracts.equals("Yes")){
			 activeContractFlag = true;
		 }
		 else{
			 activeContractFlag = false;
		 }
		 
		 logger.log(Level.SEVERE, "from Date"+fromDate);
		 logger.log(Level.SEVERE, "to Date"+toDate);
		 logger.log(Level.SEVERE, "activeContractFlag"+activeContractFlag);
		 logger.log(Level.SEVERE, "emailList"+emailList);

		 
		 if(toDate!=null){
			 Calendar cal=Calendar.getInstance();
			 cal.setTime(toDate);
			 cal.add(Calendar.DATE, 0);
				
				try {
					toDate=format.parse(format.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
					cal.set(Calendar.MILLISECOND,999);
					toDate=cal.getTime();
					 logger.log(Level.SEVERE, "to Date after time set"+toDate);

				} catch (ParseException e) {
					e.printStackTrace();
				}
		 }
		
		 List<Contract> contractlist = null;
		 if(activeContractFlag){
			 Date todayDate = new Date();
			 contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("endDate >=",todayDate).list();
		 }
		 else{
			 contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("endDate >=",fromDate)
				 		.filter("endDate <=", toDate).list(); 
		 }
		 logger.log(Level.SEVERE, "contractlist size "+contractlist.size());

		 
		 
		 if(contractlist.size()>0) {
			 ArrayList<Contract> updatedContractlist = new ArrayList<Contract>();

			 for(Contract contractEnity : contractlist) {
				 if(contractEnity.getStatus().equals(AppConstants.APPROVED)) {
					 updatedContractlist.add(contractEnity);
				 }
			 }

			 HashSet<Integer> customerId = new HashSet<Integer>();
			 HashSet<Integer> hscontractid = new HashSet<Integer>();
			 
			 for(Contract contract : updatedContractlist){
				 customerId.add(contract.getCinfo().getCount());
				 hscontractid.add(contract.getCount());
			 }
			 ArrayList<Integer> customerIdlist = new ArrayList<Integer>(customerId);
			 logger.log(Level.SEVERE, "customerIdlist size "+customerIdlist.size());
			 
			 List<Customer> customerlist = ofy().load().type(Customer.class).filter("count IN", customerIdlist)
					 					.filter("companyId", companyId).list();
			 
			 
			 ArrayList<Integer> contractidlist = new ArrayList<Integer>(hscontractid);
			 
			 ArrayList<String> statuslist = new ArrayList<String>();
			 statuslist.add(BillingDocument.CREATED);
			 statuslist.add(BillingDocument.REQUESTED);
			 statuslist.add(BillingDocument.APPROVED);

			 List<BillingDocument> bilinglist = ofy().load().type(BillingDocument.class).filter("status IN", statuslist)
					 				.filter("companyId", companyId).list();
			 
			 int columnCount =54; 
			 
			 ArrayList<String> list = new ArrayList<String>();
			 list.add("IsCompany");
			 list.add("Company Name");
			 list.add("Poc Name/Fullname");
			 list.add("Cell Number");
			 list.add("Landline Number");
			 list.add("Email");
			 list.add("Customer GST No.");
			 list.add("Address Line 1");
			 list.add("Address Line 2");
			 list.add("Landmark");
			 list.add("Locality");
			 list.add("Pin Number");
			 list.add("City");
			 list.add("State");
			 list.add("Country");
			 list.add("Address Line 1");
			 list.add("Address Line 2");
			 list.add("Landmark");
			 list.add("Locality");
			 list.add("Pin Number");
			 list.add("City");
			 list.add("State");
			 list.add("Country");
			 list.add("Reference Order Id");
			 list.add("Contract Date(MM/dd/yyyy)");
			 list.add("Contract Start Date(MM/dd/yyyy)");
			 list.add("Contract End Date(MM/dd/yyyy)");
			 list.add("Contract Group");
			 list.add("Contract Category");
			 list.add("Contract Type");
			 list.add("Sales Person");
			 list.add("Branch");
			 list.add("Product Name");
			 list.add("Number of Services");
			 list.add("Product Price");
			 list.add("Tax 1");
			 list.add("Tax 2");
			 list.add("Outsatanding Amount");
			 list.add("Cutoff Date For Service Creation(MM/dd/yyyy)");
			 list.add("Is Rate Contract (Yes/No)");
			 list.add("Service Wise Billing (Yes/No)");
			 list.add("Consilated (Yes/No)");
			 list.add("Sales source");
			 list.add("Types of frequancy");
			 list.add("Types of Treatment");
			 list.add("Salutation");
			 list.add("Model No.");
			 list.add("Serial No.");
			 list.add("Day");
			 list.add("Time");
			 list.add("Number Range");
			 list.add("Service Duration");
			 list.add("Correspondence Name");
			 list.add("Description");
			 
			 int referenceOrderNo=0;
			 for(Contract contractEntity : updatedContractlist){
				 
				 Customer customerEntity = getCustomerEntity(contractEntity.getCinfo().getCount(),customerlist);
				 
				 double outstandingAmount = 0;
				 
				 List<BillingDocument> openbillinglist = getContractWiseBillingList(contractEntity.getCount(),bilinglist);
				 
				 outstandingAmount = getoutStandingAmount(contractEntity,openbillinglist);
				 
				 try {
					
					 ++referenceOrderNo;
					 for(SalesLineItem saleslineitem : contractEntity.getItems()){
						 
					 if(customerEntity!=null){
						 if(customerEntity.isCompany()){
							 list.add("Yes");
						 }
						 else{
							 list.add("No");
						 }
						 
						 list.add(customerEntity.getCompanyName());
						 list.add(customerEntity.getFullname());
						 list.add(customerEntity.getCellNumber1()+"");
						 list.add(customerEntity.getLandline()+"");
						 if(customerEntity.getEmail()!=null &&!customerEntity.getEmail().equals("")){
							 list.add(customerEntity.getEmail());
						 }
						 else{
							 list.add("NA");

						 }
						 list.add(getGSTNumber(customerEntity));

						 if(customerEntity.getAdress().getAddrLine1()!=null && !customerEntity.getAdress().getAddrLine1().equals("")){
							 list.add(customerEntity.getAdress().getAddrLine1());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getAdress().getAddrLine2()!=null && !customerEntity.getAdress().getAddrLine2().equals("")){
							 list.add(customerEntity.getAdress().getAddrLine2());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getAdress().getLandmark()!=null && !customerEntity.getAdress().getLandmark().equals("")){
							 list.add(customerEntity.getAdress().getLandmark());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getAdress().getLocality()!=null && !customerEntity.getAdress().getLocality().equals("")){
							 list.add(customerEntity.getAdress().getLocality());
						 }
						 else{
							 list.add("NA");
						 }
						 list.add(customerEntity.getAdress().getPin()+"");

						 if(customerEntity.getAdress().getCity()!=null && !customerEntity.getAdress().getCity().equals("")){
							 list.add(customerEntity.getAdress().getCity());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getAdress().getState()!=null && !customerEntity.getAdress().getState().equals("")){
							 list.add(customerEntity.getAdress().getState());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getAdress().getCountry()!=null && !customerEntity.getAdress().getCountry().equals("")){
							 list.add(customerEntity.getAdress().getCountry());
						 }
						 else{
							 list.add("NA");
						 }
						 
						 
						 if(customerEntity.getSecondaryAdress().getAddrLine1()!=null && !customerEntity.getSecondaryAdress().getAddrLine1().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getAddrLine1());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getSecondaryAdress().getAddrLine2()!=null && !customerEntity.getSecondaryAdress().getAddrLine2().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getAddrLine2());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getSecondaryAdress().getLandmark()!=null && !customerEntity.getSecondaryAdress().getLandmark().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getLandmark());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getSecondaryAdress().getLocality()!=null && !customerEntity.getSecondaryAdress().getLocality().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getLocality());
						 }
						 else{
							 list.add("NA");
						 }
						 list.add(customerEntity.getSecondaryAdress().getPin()+"");

						 if(customerEntity.getSecondaryAdress().getCity()!=null && !customerEntity.getSecondaryAdress().getCity().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getCity());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getSecondaryAdress().getState()!=null && !customerEntity.getSecondaryAdress().getState().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getState());
						 }
						 else{
							 list.add("NA");
						 }
						 if(customerEntity.getSecondaryAdress().getCountry()!=null && !customerEntity.getSecondaryAdress().getCountry().equals("")){
							 list.add(customerEntity.getSecondaryAdress().getCountry());
						 }
						 else{
							 list.add("NA");
						 }
						 list.add(referenceOrderNo+"");

						 if(contractEntity.getContractDate()!=null){
							 list.add(format.format(contractEntity.getContractDate()));
						 }
						 else{
							 list.add("NA");
						 }
						 
						 if(contractEntity.getStartDate()!=null){
							 list.add(format.format(contractEntity.getStartDate()));
						 }
						 else{
							 list.add("NA");
						 }
						 
						 if(contractEntity.getStartDate()!=null){
							 list.add(format.format(contractEntity.getEndDate()));
						 }
						 else{
							 list.add("NA");
						 }
						 
						 if(contractEntity.getGroup()!=null && !contractEntity.getGroup().equals("")){
							 list.add(contractEntity.getGroup());
						 }
						 else{
							 list.add("NA");
						 }
						 if(contractEntity.getCategory()!=null && !contractEntity.getCategory().equals("")){
							 list.add(contractEntity.getCategory());
						 }
						 else{
							 list.add("NA");
						 }
						 if(contractEntity.getType()!=null && !contractEntity.getType().equals("")){
							 list.add(contractEntity.getType());
						 }
						 else{
							 list.add("NA");
						 }
						 if(contractEntity.getEmployee()!=null && !contractEntity.getEmployee().equals("")){
							 list.add(contractEntity.getEmployee());
						 }
						 else{
							 list.add("NA");
						 }
						 
						 if(contractEntity.getBranch()!=null && !contractEntity.getBranch().equals("")){
							 list.add(contractEntity.getBranch());
						 }
						 else{
							 list.add("");
						 }
						 
						  list.add(saleslineitem.getProductName());
						  list.add(saleslineitem.getNumberOfServices()+"");
						  list.add(saleslineitem.getPrice()+"");
						  if(saleslineitem.getVatTax()!=null && saleslineitem.getVatTax().getTaxConfigName()!=null){
							  list.add(saleslineitem.getVatTax().getTaxConfigName());
						  }
						  else{
							  list.add("");
						  }
						  try {
							  logger.log(Level.SEVERE, "contract ID "+contractEntity.getCount());

							  logger.log(Level.SEVERE, "saleslineitem.getVatTax().getTaxConfigName()"+saleslineitem.getVatTax().getTaxConfigName());
							  logger.log(Level.SEVERE, "saleslineitem.getVatTax().getTaxName()"+saleslineitem.getVatTax().getTaxName());
						} catch (Exception e) {
							e.printStackTrace();
						}
						 if(saleslineitem.getServiceTax()!=null && saleslineitem.getServiceTax().getTaxConfigName()!=null){
							  list.add(saleslineitem.getServiceTax().getTaxConfigName());
						 }
						 else{
							  list.add("");
						 }

						  list.add(outstandingAmount+"");
						  list.add("");
						  
						  if(contractEntity.isContractRate()){
							  list.add("Yes");
						  }
						  else{
							  list.add("No");
						  }
						  if(contractEntity.isServiceWiseBilling()){
							  list.add("Yes");
						  }
						  else{
							  list.add("No");
						  }
						  if(contractEntity.isConsolidatePrice()){
							  list.add("Yes");
						  }
						  else{
							  list.add("No");
						  }
						  
						  if(contractEntity.getContractPeriod()!=null && !contractEntity.getContractPeriod().equals("")){
							  list.add(contractEntity.getContractPeriod());
						  }
						  else{
							  list.add("NA");
						  }

						  if(saleslineitem.getProfrequency()!=null && !saleslineitem.getProfrequency().equals("")){
							  list.add(saleslineitem.getProfrequency());
						  }
						  else{
							  list.add("NA");
						  }
						  if(saleslineitem.getTermsoftreatment()!=null && !saleslineitem.getTermsoftreatment().equals("")){
							  list.add(saleslineitem.getTermsoftreatment());
						  }
						  else{
							  list.add("NA");
						  }
						  
						  if(customerEntity.getSalutation()!=null && !customerEntity.getSalutation().equals("")){
							  list.add(customerEntity.getSalutation());
						  }
						  else{
							  list.add("NA");
						  }
						  
						  if(saleslineitem.getProModelNo()!=null && !saleslineitem.getProModelNo().equals("")){
							  list.add(saleslineitem.getProModelNo());
						  }
						  else{
							  list.add("NA");
						  }
						  
						  if(saleslineitem.getProSerialNo()!=null && !saleslineitem.getProSerialNo().equals("")){
							  list.add(saleslineitem.getProSerialNo());
						  }
						  else{
							  list.add("NA");
						  }

						  
						  if(contractEntity.getServiceScheduleList().size()>0){
							  list.add(contractEntity.getServiceScheduleList().get(0).getScheduleServiceDay());
						  }
						  else{
							  list.add("");
						  }
						  
						  if(contractEntity.getServiceScheduleList().size()>0){
							  list.add(contractEntity.getServiceScheduleList().get(0).getScheduleServiceTime());
						  }
						  else{
							  list.add("");
						  }

						  
						  
						  if(contractEntity.getNumberRange()!=null && !contractEntity.getNumberRange().equals("")){
							  list.add(contractEntity.getNumberRange());
						  }
						  else{
							  list.add("NA");
						  }
						  
						 list.add(saleslineitem.getDuration()+"");

						 
						 if(customerEntity.getCustPrintableName()!=null && !customerEntity.getCustPrintableName().equals("")){
							  list.add(customerEntity.getCustPrintableName());
						  }
						  else{
							  list.add("NA");
						  }
						 
						 if(contractEntity.getDescription()!=null && !customerEntity.getDescription().equals("")){
							 String description = contractEntity.getDescription()+" Previous link contract Id "+contractEntity.getCount()+" previous link - "+link;
							 list.add(description);
						  }
						  else{
							  String description = " Previous link contract Id "+contractEntity.getCount()+" previous link - "+link;
							  list.add(description);
						  }
					 }
					 else{
						logger.log(Level.SEVERE,"customer not found"+contractEntity.getCount());

					 }
				  }
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			 
			 }
			 

			  ByteArrayOutputStream bytestream = new ByteArrayOutputStream();

				XSSFWorkbook workbook=new XSSFWorkbook();
				try {	
					XlsxWriter xlswriter = new XlsxWriter();
					xlswriter.createExcelData(list, columnCount, workbook);
			    	workbook.write(bytestream);
					logger.log(Level.SEVERE,"bytestream created");
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				String mailSub = "contract report";
				
				String mailMsg = "Dear Sir/Ma'am ,"+"\n"+"\n"+"\n"+ " <BR>"+"Please find the contract details report.";
				Date d=new Date();
				String filename="contract report "+d+".xlsx";
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(compEntity.getEmail(), emailList, null, null, mailSub, mailMsg, "text/html",bytestream,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",compEntity.getDisplayNameForCompanyEmailId());
					logger.log(Level.SEVERE,"attached xlsx");
				}else{
					Email email = new Email();
					email.sendMailWithGmail(compEntity.getEmail(), emailList, null, null, mailSub, mailMsg, "text/html",bytestream,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					logger.log(Level.SEVERE,"attached xlsx");
				}
				
		 }
		 else {
			 
			 String mailSub = "contract report";
				
				String mailMsg = "Dear Sir/Ma'am ,"+"\n"+"\n"+"\n"+ " <BR>"+"No contract data found for this duration.";
				Date d=new Date();
				String filename="contract report "+d+".xlsx";
				
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
					SendGridEmailServlet sdEmail=new SendGridEmailServlet();
					sdEmail.sendMailWithSendGrid(compEntity.getEmail(), emailList, null, null, mailSub, mailMsg, "text/html",null,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",compEntity.getDisplayNameForCompanyEmailId());
					logger.log(Level.SEVERE,"attached xlsx");
				}else{
					Email email = new Email();
					email.sendMailWithGmail(compEntity.getEmail(), emailList, null, null, mailSub, mailMsg, "text/html",null,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					logger.log(Level.SEVERE,"attached xlsx");
				}
		 }
		 
			
	}




















	private double getoutStandingAmount(Contract contractEntity, List<BillingDocument> openbillinglist) {
		
		double totalpriceAmt = 0;
		for(BillingDocument billingentity : openbillinglist){
			for(SalesOrderProductLineItem product : billingentity.getSalesOrderProducts()){
				totalpriceAmt +=product.getBaseBillingAmount();
			}
		}
		return totalpriceAmt;
	}




















	private List<BillingDocument> getContractWiseBillingList(int contractId,List<BillingDocument> bilinglist) {
		
		List<BillingDocument> list = new ArrayList<BillingDocument>();
		for(BillingDocument billingentity : bilinglist){
			if(contractId == billingentity.getContractCount()){
				list.add(billingentity);
			}
		}
		
		return list;
	}




















	private String getGSTNumber(Customer customerEntity) {
		
		for(ArticleType articleType :customerEntity.getArticleTypeDetails()){
			if(articleType.getArticleTypeName().trim().equalsIgnoreCase("GSTN")){
				return articleType.getArticleTypeValue();
			}
		}
		return "NA";
	}




















	private Customer getCustomerEntity(int count, List<Customer> customerlist) {
		for(Customer customer : customerlist){
			if(count==customer.getCount()){
				return customer;
			}
		}
		return null;
	}

	
   private void sendSrCopyFormatVersion1OnEmail(HttpServletRequest request, long companyId) {

		 Date fromDate = null , toDate = null;
		 try{
			 fromDate = sdf.parse(request.getParameter("fromDate"));
			 logger.log(Level.SEVERE, "From Date :" + fromDate);
		 }catch(Exception e){
			 
		 }
		try{
			toDate = sdf.parse(request.getParameter("toDate"));
//			toDate = DateUtility.addDaysToDate(toDate, 1);
			 logger.log(Level.SEVERE, "To date :" + toDate);
		 }catch(Exception e){
			 
		 }
		 int id = 0;
		 try{
		  id =Integer.parseInt(request.getParameter("serviceId"));
		 }catch(Exception e){
			 
		 }
		 
		 
		 Calendar cal=Calendar.getInstance();
			cal.setTime(fromDate);
			cal.add(Calendar.DATE, 0);
			
			Date formDatewithtime=null;
			
			try {
				formDatewithtime=sdf.parse(sdf.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "formDatewithtime " + formDatewithtime);

			Calendar cal3=Calendar.getInstance();
			cal3.setTime(toDate);
			cal3.add(Calendar.DATE, 0);
			
			Date todatewithtime=null;
			
			try {
				todatewithtime=sdf.parse(sdf.format(cal3.getTime()));
				cal3.set(Calendar.HOUR_OF_DAY,23);
				cal3.set(Calendar.MINUTE,59);
				cal3.set(Calendar.SECOND,59);
				cal3.set(Calendar.MILLISECOND,999);
				todatewithtime=cal3.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "todatewithtime" + todatewithtime);
			
		
		 ArrayList<String> toemaillst = new ArrayList<String>();
		 Gson gson = new GsonBuilder().create();	
		 String emailstr = request.getParameter("email");
		 try{
				JSONArray jsonarr=new JSONArray(emailstr);
				logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
				for(int i=0;i<jsonarr.length();i++){
					toemaillst.add(jsonarr.getString(i));
				}
		 }catch(Exception e){
			 
		 }
		 
		 String team = request.getParameter("team");

		 String technicianName = request.getParameter("technicianName");
		 
		 CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
		 commonserviceimpl.SRFormatVersion1CopyOnEmail(formDatewithtime, todatewithtime, team, technicianName, toemaillst, companyId);
   }
   
   

	private void deleteAndCreateAccountingInterface(HttpServletRequest request,
			long companyId) {

		 Date fromDate = null , toDate = null;
		 try{
			 fromDate = sdf.parse(request.getParameter("fromDate"));
			 logger.log(Level.SEVERE, "From Date :" + fromDate);
		 }catch(Exception e){
		 }
		try{
			toDate = sdf.parse(request.getParameter("toDate"));
			 logger.log(Level.SEVERE, "To date :" + toDate);
		 }catch(Exception e){
			 
		 }
		 
		 Calendar cal=Calendar.getInstance();
			cal.setTime(fromDate);
			cal.add(Calendar.DATE, 0);
			
			Date formDatewithtime=null;
			
			try {
				formDatewithtime=sdf.parse(sdf.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "formDatewithtime " + formDatewithtime);

			Calendar cal3=Calendar.getInstance();
			cal3.setTime(toDate);
			cal3.add(Calendar.DATE, 0);
			
			Date todatewithtime=null;
			
			try {
				todatewithtime=sdf.parse(sdf.format(cal3.getTime()));
				cal3.set(Calendar.HOUR_OF_DAY,23);
				cal3.set(Calendar.MINUTE,59);
				cal3.set(Calendar.SECOND,59);
				cal3.set(Calendar.MILLISECOND,999);
				todatewithtime=cal3.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "todatewithtime" + todatewithtime);
			
			try {
				
				List<Invoice> invoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId)
						.filter("invoiceDate >=", formDatewithtime).filter("invoiceDate <=", todatewithtime).list();
		
				logger.log(Level.SEVERE, "invoicelist size" + invoicelist.size());
				
				ArrayList<Integer> invoiceidlist = new ArrayList<Integer>();
				for(Invoice invoiceentity : invoicelist){
					invoiceidlist.add(invoiceentity.getCount());
				}
				if(invoiceidlist!=null&invoiceidlist.size()>0)
				logger.log(Level.SEVERE, "invoiceidlist" + invoiceidlist.toString());
				List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("invoiceCount IN", invoiceidlist).list();
				logger.log(Level.SEVERE, "paymentlist size" + paymentlist.size());
		
				ArrayList<Integer> paymentidlist = new ArrayList<Integer>();
				for(CustomerPayment paymentEntity:paymentlist){
					paymentidlist.add(paymentEntity.getCount());
				}
				if(paymentidlist!=null&paymentidlist.size()>0)
				logger.log(Level.SEVERE, "paymentidlist" + paymentidlist.toString());
		
				List<AccountingInterface> accountinginterfacelist = ofy().load().type(AccountingInterface.class).filter("companyId", companyId)
						.filter("documentID IN", invoiceidlist).filter("documentType", "Invoice").list();
				logger.log(Level.SEVERE, "invoice accountingInterface size"+accountinginterfacelist.size());
				if(accountinginterfacelist.size()>0){
					ofy().delete().entities(accountinginterfacelist);
				}
				CommonServiceImpl commonservice = new CommonServiceImpl();
				for(Invoice invoiceentity : invoicelist){
					logger.log(Level.SEVERE, "createSpecificDocumentAccountingInterface for invoice "+invoiceentity.getCount());
					commonservice.createSpecificDocumentAccountingInterface(invoiceentity);
				}
				
		
				List<AccountingInterface> accountinginterfacepaymentlist = ofy().load().type(AccountingInterface.class).filter("companyId", companyId)
						.filter("documentID IN", paymentidlist).filter("documentType", "Customer Payment").list();
				logger.log(Level.SEVERE, "payment accountingInterface size"+accountinginterfacepaymentlist.size());
				if(accountinginterfacepaymentlist.size()>0){
					ofy().delete().entities(accountinginterfacepaymentlist);
				}
				
				for(CustomerPayment paymentEntity:paymentlist){
					logger.log(Level.SEVERE, "createSpecificDocumentAccountingInterface for payment "+paymentEntity.getCount());					
					commonservice.createSpecificDocumentAccountingInterface(paymentEntity);
				}
		
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}
	
	private void deleteAndCreateAccountingInterfacePaymentDocument(
			HttpServletRequest request, long companyId) {

		Date fromDate = null , toDate = null;
		 try{
			 fromDate = sdf.parse(request.getParameter("fromDate"));
			 logger.log(Level.SEVERE, "From Date :" + fromDate);
		 }catch(Exception e){
		 }
		try{
			toDate = sdf.parse(request.getParameter("toDate"));
			 logger.log(Level.SEVERE, "To date :" + toDate);
		 }catch(Exception e){
			 
		 }
		 
		 Calendar cal=Calendar.getInstance();
			cal.setTime(fromDate);
			cal.add(Calendar.DATE, 0);
			
			Date formDatewithtime=null;
			
			try {
				formDatewithtime=sdf.parse(sdf.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "formDatewithtime " + formDatewithtime);

			Calendar cal3=Calendar.getInstance();
			cal3.setTime(toDate);
			cal3.add(Calendar.DATE, 0);
			
			Date todatewithtime=null;
			
			try {
				todatewithtime=sdf.parse(sdf.format(cal3.getTime()));
				cal3.set(Calendar.HOUR_OF_DAY,23);
				cal3.set(Calendar.MINUTE,59);
				cal3.set(Calendar.SECOND,59);
				cal3.set(Calendar.MILLISECOND,999);
				todatewithtime=cal3.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "todatewithtime" + todatewithtime);
			
			try {
				
				List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
						.filter("paymentDate >=", formDatewithtime).filter("paymentDate <=", todatewithtime).list();
		
				logger.log(Level.SEVERE, "payment list size" + paymentlist.size());
				CommonServiceImpl commonservice = new CommonServiceImpl();

				ArrayList<Integer> paymentidlist = new ArrayList<Integer>();
				for(CustomerPayment paymentEntity:paymentlist){
					paymentidlist.add(paymentEntity.getCount());
				}
				
				List<AccountingInterface> accountinginterfacepaymentlist = ofy().load().type(AccountingInterface.class).filter("companyId", companyId)
						.filter("documentID IN", paymentidlist).filter("documentType", "Customer Payment").list();
				logger.log(Level.SEVERE, "payment accountingInterface size"+accountinginterfacepaymentlist.size());
				if(accountinginterfacepaymentlist.size()>0){
					ofy().delete().entities(accountinginterfacepaymentlist);
				}
				
				for(CustomerPayment paymentEntity:paymentlist){
					commonservice.createSpecificDocumentAccountingInterface(paymentEntity);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}

	
	private void CancelServices(HttpServletRequest request, long companyId) {
		
		String remark = request.getParameter("Remark");
		String user = request.getParameter("loggedinuser");
		String customerbranchName = request.getParameter("customerbranchName");
		String strcustomerId = request.getParameter("customerId").trim();
		
		
		
		 try{
				int customerId = Integer.parseInt(strcustomerId);

			 ArrayList<String> statuslist = new ArrayList<String>();
			 statuslist.add(Service.SERVICESTATUSSCHEDULE);
			 statuslist.add(Service.SERVICESTATUSRESCHEDULE);

			ArrayList<Service> servicelist = new ArrayList<Service>();
			List<Service> serList = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", customerId)
					.filter("serviceBranch", customerbranchName).filter("status IN", statuslist).list();
			logger.log(Level.SEVERE, "serList" + serList.size());
			
			servicelist.addAll(serList);
			
			UpdateServiceImpl serImpl = new UpdateServiceImpl();
			serImpl.cancelAllServices(servicelist, remark, user);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	}
	
	private void UpdateCustomerBranchServiceAddress(HttpServletRequest request,
			long companyId) {

		 try{
			 String customerbranchName = request.getParameter("customerbranchName");
			 String strcustomerId = request.getParameter("customerId").trim();
			 int customerId = Integer.parseInt(strcustomerId);

			 ArrayList<String> statuslist = new ArrayList<String>();
			 statuslist.add(Service.SERVICESTATUSSCHEDULE);
			 statuslist.add(Service.SERVICESTATUSRESCHEDULE);
			 logger.log(Level.SEVERE, "customerId" + customerId);
			 logger.log(Level.SEVERE, "customerbranchName" + customerbranchName);
			 logger.log(Level.SEVERE, "companyId" + companyId);

			 CustomerBranchDetails customerbranchEntity = ofy().load().type(CustomerBranchDetails.class)
						.filter("companyId", companyId).filter("buisnessUnitName", customerbranchName)
						.filter("cinfo.count", customerId).filter("status", true).first().now();
				logger.log(Level.SEVERE, "customerbranchEntity" + customerbranchEntity);
				
			 if(customerbranchEntity!=null){
				 List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", customerId)
							.filter("serviceBranch", customerbranchName).filter("status IN", statuslist).list();
					logger.log(Level.SEVERE, "servicelist" + servicelist.size());
					
					for(Service serviceEntity : servicelist){
						serviceEntity.setAddress(customerbranchEntity.getAddress());
					}
					ofy().save().entities(servicelist);
			 }
			
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		
	}
	
	
	private void activateEVAERPUsageCronJob(long companyId) {
		
		ConfigCategory oldCC=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("categoryName", AppConstants.EVAERPUsageReport).first().now();
		if(oldCC!=null)
			ofy().delete().entity(oldCC);
		
		CronJobConfigration oldCronJob=ofy().load().type(CronJobConfigration.class).filter("companyId", companyId).filter("cronJobsName", AppConstants.EVAERPUsageReport).first().now();
		if(oldCronJob!=null)
			ofy().delete().entity(oldCronJob);		
		
		
		Date startDate = DateUtility.getDateWithTimeZone("IST", new Date());
		logger.log(Level.SEVERE, "startDate="+startDate);
		logger.log(Level.SEVERE, "start time="+startDate.getTime());
		logger.log(Level.SEVERE, "1 day  time="+1000*60*60*24);
		long time= startDate.getTime() + (1000*60*60*24*30);
		startDate.setTime(time);
		logger.log(Level.SEVERE, "time after adding 30 days="+time);
		
		
		Date startOnThursday=DateUtility.getDateWithTimeZone("IST", new Date());
		startOnThursday.setDate(6); //gets set as 6+1 ie 7
		startOnThursday.setMonth(11);
		startOnThursday.setYear(123); //gets set as 123+1900 ie 2023
		logger.log(Level.SEVERE, "startOnThursdayd="+startOnThursday);
		
		logger.log(Level.SEVERE, "startDate revised="+startDate);
		
		ConfigCategory cc=new ConfigCategory();
		cc.setInternalType(29);
		cc.setStatus(true);
		cc.setCompanyId(companyId);
		cc.setCategoryName(AppConstants.EVAERPUsageReport);
		cc.setCreatedBy("CronJob");
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(cc);
		
		CronJobConfigration cronjob=new CronJobConfigration();
		cronjob.setConfigStatus(true);
		cronjob.setCronJobsName(AppConstants.EVAERPUsageReport);
		cronjob.setCreatedBy("EVA");
		cronjob.setCompanyId(companyId);
		ArrayList<CronJobConfigrationDetails> cronJobsProcessList=new ArrayList<CronJobConfigrationDetails>();
		CronJobConfigrationDetails cronDetails=new CronJobConfigrationDetails();
		cronDetails.setCronJobName(AppConstants.EVAERPUsageReport);
		cronDetails.setEmployeeRole("ADMIN");
		cronDetails.setFrequencyType("Weekly");
		cronDetails.setTemplateName("EVAERPLowUsageReportEmail");
		cronDetails.setCommunicationChannel("Email");
		cronDetails.setFrequencyDay(7);
		cronDetails.setStartDay(0);
		cronDetails.setStartingDate(startOnThursday); //Nitin sir asked to set this cronjob for thursday
		cronDetails.setOverdueDays(7);
		cronDetails.setOverdueStatus(true);
		cronDetails.setCreatedDate(new Date());
		cronDetails.setStatus(true);
		cronJobsProcessList.add(cronDetails);
		cronjob.setCronJobsNameList(cronJobsProcessList);
		cronjob.setLastUpdatedDate(new Date());
		cronjob.setUpdatedBy("EVA");
		
		impl.save(cronjob);
		
		logger.log(Level.SEVERE, "cron job created");
		
	}
	
	private void updateCustomerSignatureInService(HttpServletRequest request,
			long companyId) {

		 
			 String url = request.getParameter("url");
			 String filename = request.getParameter("filename").trim();
			 String serviceId = "";
			 try{
				 serviceId= request.getParameter("serviceId").trim();
			 }catch(Exception e){
				 
			 }
			 String multipleServiceIds = "";
			 try{
				 multipleServiceIds=request.getParameter("multipleServiceIds").trim();
			 }catch(Exception e){
				 
			 }
			

				
				DocumentUpload upload=new DocumentUpload();
				try {

				logger.log(Level.SEVERE, "URL : "+ url);
				logger.log(Level.SEVERE, "filename : "+ filename);
				logger.log(Level.SEVERE, "multipleServiceIds : "+ multipleServiceIds);
				logger.log(Level.SEVERE, "serviceId : "+ serviceId);
				upload.setUrl(url);
				upload.setName(filename);
				upload.setStatus(true);
				
				try {
					logger.log(Level.SEVERE, "upload "+upload.getUrl());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
				
			if(multipleServiceIds!=null && !multipleServiceIds.equals("")) {
				logger.log(Level.SEVERE, "For multiple service completion");

				ArrayList<Integer> serviceidarray = new ArrayList<Integer>();
//				List<String> serviceidlist = Arrays.asList(multipleServiceIds); 
				String [] serviceidlist = multipleServiceIds.split("\\,");
				for(int i=0; i<serviceidlist.length;i++) {
					logger.log(Level.SEVERE,"serviceidlist"+serviceidlist[i]);
					serviceidlist[i].trim().replaceAll("\"", "");
					String str = serviceidlist[i].trim().replaceAll("^\\[|]$", "");
					logger.log(Level.SEVERE,"str"+str.trim());
					serviceidarray.add(Integer.parseInt(str.trim()));
				}
				List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("count IN", serviceidarray).list();
				logger.log(Level.SEVERE, "servicelist size "+servicelist.size());
				if(servicelist.size()>0) {
					for(Service serviceEntity : servicelist) {
						serviceEntity.setCustomerSignature(upload);
					}
					ofy().save().entities(servicelist);
					logger.log(Level.SEVERE, "Services updated with customer signature");
					logger.log(Level.SEVERE, "SERVICE UPDATED "+ new Date());
				}
			}
			else {
				logger.log(Level.SEVERE, "inside Single service completion");
				Service service = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", Integer.parseInt(serviceId)).first().now();
				logger.log(Level.SEVERE,"service::::::::::::"+service);
				service.setCustomerSignature(upload);
				ofy().save().entity(service);
				logger.log(Level.SEVERE, "SERVICE UPDATED : "+ new Date());
				
			}
			
			logger.log(Level.SEVERE, "end here");

	}
	
	private void copyGLAccounts(String glAccounts, long companyId) {

		GenricServiceImpl impl=new GenricServiceImpl();
		List<GLAccount> entityList = new ArrayList<GLAccount>();
		try {
			Gson gson = new GsonBuilder().create();
			JSONArray jsonarr = new JSONArray(glAccounts);
			for (int i = 0; i < jsonarr.length(); i++) {
				entityList.add(gson.fromJson(jsonarr.get(i).toString(),GLAccount.class));
			}
			
			if(entityList!=null&&entityList.size()!=0){
				logger.log(Level.SEVERE, "GLAccount details:: " + entityList.size());
				ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
				List<GLAccount> existingEntityList=ofy().load().type(GLAccount.class).filter("companyId", companyId).list();
				if(existingEntityList!=null&&existingEntityList.size()!=0){
					for(GLAccount obj:entityList){
						boolean updateFlag=false;
						for(GLAccount obj1:existingEntityList){
							if(obj.getGlAccountName().equals(obj1.getGlAccountName())){
								updateFlag=true;
								break;
							}
						}
						if(updateFlag){
							continue;
						}
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}else{
					for(GLAccount obj:entityList){
						obj.setId(null);
						obj.setCount(0);
						obj.setCompanyId(companyId);
						obj.setCreatedBy("");
						SuperModel model=obj;
						modelList.add(model);
					}
				}
				logger.log(Level.SEVERE, "modelList :: " + modelList.size());
				if(modelList.size()!=0){
					impl.save(modelList);
				}
			}
			
		} catch (Exception e) {

		}
	}
	
	
	private void UpdateCustomerIdInAllDocs(HttpServletRequest request,
			long companyId) {
		
		int oldCustId=Integer.parseInt(request.getParameter("oldCustId"));
		int newCustId=Integer.parseInt(request.getParameter("newCustId"));
		String loggedInUser=request.getParameter("loggedInUser");
		Customer newcust = ofy().load().type(Customer.class).filter("companyId",companyId).filter("count", newCustId).first().now();
		if(newcust!=null) {
			Long cellnumber=0l;
			if(newcust.getCellNumber1()!=null)
				cellnumber=newcust.getCellNumber1();
			String fullname="";
			if(newcust.isCompany())
				fullname=newcust.getCompanyName();
			else
				fullname=newcust.getFullname();
			String poc=newcust.getFullname();
			String email="";
			if(newcust.getEmail()!=null&&!newcust.getEmail().equals("")){
				email=newcust.getEmail();
			}
			

			try {
				
			
			/********* here i am updating customer name in lead entity **********************/
			List<Lead> leadEntity = null;
			
				leadEntity = ofy().load().type(Lead.class).filter("companyId", companyId).filter("personInfo.count", oldCustId).list();
		
			if(leadEntity!=null&&leadEntity.size()>0){	
				logger.log(Level.SEVERE,"lead entity Size === "+leadEntity.size());

				for(int i=0;i<leadEntity.size();i++){
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(fullname);
						personinfo.setPocName(poc);
						personinfo.setCellNumber(cellnumber);
						personinfo.setCount(newCustId);
						leadEntity.get(i).setPersonInfo(personinfo);
					
				}
				ofy().save().entities(leadEntity);
				logger.log(Level.SEVERE," Here lead entity updated successfully");

			}
			
			} catch (Exception e) {
				logger.log(Level.SEVERE," Here lead entity updation"+e);
			}
			
			
			/********* here i am updating customer in Quotation entity **********************/
			try {

				
				List<Quotation> quotationEntity = null;
				quotationEntity = ofy().load().type(Quotation.class).filter("companyId", newcust.getCompanyId()).filter("cinfo.count", oldCustId).list();
				
				if(quotationEntity!=null&&quotationEntity.size()>0){	
					logger.log(Level.SEVERE,"Quotation entity Size === "+quotationEntity.size());
					for(int i=0;i<quotationEntity.size();i++){
						
							PersonInfo personinfo = new PersonInfo();
							personinfo.setFullName(fullname);
							personinfo.setPocName(poc);
							personinfo.setCellNumber(cellnumber);
							personinfo.setCount(newCustId);
							quotationEntity.get(i).setCinfo(personinfo);
						
					}
					ofy().save().entities(quotationEntity);
					System.out.println(" Here Quotation entity updated successfully");
					logger.log(Level.SEVERE," Here Quotation entity updated successfully");
				}
				
			} catch (Exception e) {
				logger.log(Level.SEVERE," Here Quotation entity updation"+e);
			}
			
			
			/********* here i am updating customer in Contract entity **********************/
			
			try {
				
			List<Contract> contractEntity  = null;
			
			contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("cinfo.count",oldCustId).list();
			
			if(contractEntity!=null&&contractEntity.size()>0){	
				logger.log(Level.SEVERE,"Contract entity Size === "+contractEntity.size());

				for(int i=0;i<contractEntity.size();i++){
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(fullname);
						personinfo.setPocName(poc);
						personinfo.setCellNumber(cellnumber);
						personinfo.setCount(newCustId);
						personinfo.setEmail(email);
						
						contractEntity.get(i).setCinfo(personinfo);
						
						contractEntity.get(i).setNewcompanyname(fullname);
						contractEntity.get(i).setNewcustomerfullName(poc);
						contractEntity.get(i).setNewcustomercellNumber(cellnumber);
						contractEntity.get(i).setNewcustomerEmail(email);
						
					
				}
				ofy().save().entities(contractEntity);
				logger.log(Level.SEVERE," Here Contract entity updated successfully");
				
			}
			
			} catch (Exception e) {
				logger.log(Level.SEVERE," Here Contract entity updation"+e);
			}
			
			
			
			
			/********* here i am updating customer name in Billing entity **********************/
			try {
			List<BillingDocument> billingEntity= null;
			
				   billingEntity = ofy().load().type(BillingDocument.class).filter("companyId",companyId).filter("personInfo.count", oldCustId).list();
				 
			
			if(billingEntity!=null&&billingEntity.size()>0){	
				  logger.log(Level.SEVERE,"Billing entity Size === "+billingEntity.size());

				  for(int i=0;i<billingEntity.size();i++){
						System.out.println(" if customer is company");
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(fullname);
						personinfo.setPocName(poc);
						personinfo.setCellNumber(cellnumber);
						personinfo.setCount(newCustId);
						billingEntity.get(i).setPersonInfo(personinfo);
				  }
				ofy().save().entities(billingEntity);
				logger.log(Level.SEVERE," Here Billing entity updated successfully");

			}
			} catch (Exception e) {
				logger.log(Level.SEVERE," Here Billing entity updation"+e);
			}
			
			
			/********* here i am updating customer name in Invoice entity **********************/
			try {
			
			List<Invoice> invoiceEntity= null;
			
			invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("personInfo.count", oldCustId).list();
				  
			
			if(invoiceEntity!=null&&invoiceEntity.size()>0){	
				logger.log(Level.SEVERE,"Invoice entity Size === "+invoiceEntity.size());

				for(int i=0;i<invoiceEntity.size();i++){
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(fullname);
						personinfo.setPocName(poc);
						personinfo.setCellNumber(cellnumber);
						personinfo.setCount(newCustId);
						invoiceEntity.get(i).setPersonInfo(personinfo);
						
				}
				ofy().save().entities(invoiceEntity);
				logger.log(Level.SEVERE," Here Invoice entity updated successfully");

			}
			} catch (Exception e) {
				logger.log(Level.SEVERE," Here Invoice entity updation"+e);
			}
		
			
			/********* here i am updating customer name in Payment entity **********************/
			try {
				
			List<CustomerPayment> customerpaymentEntity = null;
			
			customerpaymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("personInfo.count", oldCustId).list();
			
			if(customerpaymentEntity!=null&&customerpaymentEntity.size()>0){
				logger.log(Level.SEVERE,"Payment entity Size === "+customerpaymentEntity.size());

				for(int i=0;i<customerpaymentEntity.size();i++){
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(fullname);
						personinfo.setPocName(poc);
						personinfo.setCellNumber(cellnumber);
						personinfo.setCount(newCustId);
						customerpaymentEntity.get(i).setPersonInfo(personinfo);					
					
				}
				ofy().save().entities(customerpaymentEntity);
				logger.log(Level.SEVERE," Here Payment entity updated successfully");

			}
			} catch (Exception e) {
				logger.log(Level.SEVERE," Here Payment entity updation"+e);
			}
			
		
			try {
				
				Service serviceObject=ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count",oldCustId).first().now();
		    	if(serviceObject!=null){
		    		 int count= ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count",oldCustId).count();
		 		    if(count>500) {
		 		    	int slot=count/500;
		 		    	for(int s=0;s<=slot;s++) {
		 		    		List<Service> services = null;
		 		    		services = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", oldCustId).limit(500).list();
		 		    		if(services!=null&&services.size()>0){	
		 						for(int i=0;i<services.size();i++){
		 							PersonInfo personinfo = new PersonInfo();
		 							personinfo.setFullName(fullname);
		 							personinfo.setPocName(poc);
		 							personinfo.setCellNumber(cellnumber);
		 							personinfo.setCount(newCustId);
		 							services.get(i).setPersonInfo(personinfo);		
		 						}
		 		    		}
		 		    		ofy().save().entities(services);
		 		    		logger.log(Level.SEVERE," Here Service entity updated successfully in slot "+ s);

		 		    	}
		 		    }
		 		    else {
		 		    	List<Service> services = null;
	 		    		services = ofy().load().type(Service.class).filter("companyId", companyId).filter("personInfo.count", oldCustId).list();
	 		    		if(services!=null&&services.size()>0){	
	 						for(int i=0;i<services.size();i++){
	 							PersonInfo personinfo = new PersonInfo();
	 							personinfo.setFullName(fullname);
	 							personinfo.setPocName(poc);
	 							personinfo.setCellNumber(cellnumber);
	 							personinfo.setCount(newCustId);
	 							services.get(i).setPersonInfo(personinfo);		
	 						}
	 		    		}
	 		    		ofy().save().entities(services);
	 		    		logger.log(Level.SEVERE," Here Service entity updated successfully");

		 		    }
				
				
			}
			}catch (Exception e) {
				logger.log(Level.SEVERE," Here service entity updation"+e);
			}
			
		
			try {
				
			
			/********* here i am updating customer name in Service entity **********************/
			
			List<ServiceProject> serviceprojectEntity= null;
			
			serviceprojectEntity = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("personInfo.count",oldCustId).list();
			
			
			if(serviceprojectEntity!=null&&serviceprojectEntity.size()>0){	
				logger.log(Level.SEVERE,"Service project entity Size === "+serviceprojectEntity.size());

				for(int i=0;i<serviceprojectEntity.size();i++){
					
						PersonInfo personinfo = new PersonInfo();
						personinfo.setFullName(fullname);
						personinfo.setPocName(poc);
						personinfo.setCellNumber(cellnumber);
						personinfo.setCount(newCustId);
						serviceprojectEntity.get(i).setPersonInfo(personinfo);
					
				}
				ofy().save().entities(serviceprojectEntity);
				logger.log(Level.SEVERE," Here Service project entity updated successfully");

			}
			} catch (Exception e) {
				logger.log(Level.SEVERE,"Servie Project updation"+e);

			}
			
		}else {
			logger.log(Level.SEVERE, "Failed to update customer id in all docs");
		}
		if(newcust.getUpdateCustomerIdLog()!=null)
			newcust.setUpdateCustomerIdLog(newcust.getUpdateCustomerIdLog()+" - "+loggedInUser+" executed update customer id program for customer Id "+oldCustId +" on "+new Date());
		else
			newcust.setUpdateCustomerIdLog(loggedInUser+" executed update customer id program for customer Id "+oldCustId +" on "+new Date());
		ofy().save().entity(newcust);
		logger.log(Level.SEVERE, "Updation process completed");
	}
	
}

