package com.slicktechnologies.server.taskqueue;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.addhocprinting.GSTQuotationPdf;
import com.slicktechnologies.server.addhocprinting.NBHCQuotationPdf;
import com.slicktechnologies.server.addhocprinting.PestleQuotationPdf;
import com.slicktechnologies.server.addhocprinting.PestoIndiaQuotationPdf;
import com.slicktechnologies.server.addhocprinting.QuotationPdf;
import com.slicktechnologies.server.addhocprinting.UniversalPestQuotationPdf;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

/**
 * 
 * @author Viraj
 * Date: 14-11-2018
 * Description: For uploading terms and condition,company profile documents
 */

public class EmailUploadTaskQueue extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2031478339208880594L;
	Sales servq;
	Sales servquot1;
	GSTQuotationPdf serviceGSTpdf;
	ByteArrayOutputStream pdfstream;
	Quotation servquot;
	Customer c;
	Company comp;
	CompanyPayment compPayModeDefault;
	CompanyPayment compPayModeUndefault;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	Logger logger = Logger.getLogger("NameOfYourLogger");
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		long quotationId = Long.parseLong(request.getParameter("quotationId"));
		long companyId = Long.parseLong(request.getParameter("companyId"));
		
		logger.log(Level.SEVERE, "quotationId :::: " + quotationId);
		logger.log(Level.SEVERE, "companyId :::: " + companyId);
		
		servquot = ofy().load().type(Quotation.class).filter("companyId", companyId).filter("count", quotationId)
				.first().now();
		Email email = new Email();
		email.initiateServiceQuotationEmail(servquot);

	}
	
}
