package com.slicktechnologies.server.taskqueue;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class PoCreationInterfaceTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8120859152824667988L;
	
	Logger logger=Logger.getLogger("PoCreationInterfaceTaskQueue.class");
	SimpleDateFormat spf;
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		spf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		spf.setTimeZone(TimeZone.getTimeZone("IST"));
		String  poData= req.getParameter("poData").trim();
		String[] dataArray=poData.split("\\$");
		String poJsonArrayData=dataArray[0].trim();
		String companyIdStr=dataArray[1].trim();
//		String companyIdStr = req.getParameter("companyId");
		long companyId=Long.parseLong( companyIdStr.trim());
		logger.log(Level.SEVERE,"poJsonArrayData::::"+poJsonArrayData);
		logger.log(Level.SEVERE,"companyId::::::::"+companyId);
		JSONArray jArray = null;
		
		try {
			jArray = new JSONArray(poJsonArrayData.trim());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"jArray::::::::"+e);
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"jArray::::::::"+jArray.length());
		for (int i = 0; i < jArray.length(); i++) {
			JSONObject poObj = null;
			try {
				poObj=jArray.getJSONObject(i);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PurchaseOrder po=new PurchaseOrder();
			po.setCompanyId(companyId);
			po.setRefOrderNO(poObj.optString("refDocNo"));
			po.setcForm("NO");
			try {
				po.setReferenceDate(spf.parse(poObj.optString("refDocDate")));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				po.setPODate(spf.parse(poObj.optString("poDate")));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			po.setPOName(poObj.optString("poTitle"));
			po.setBranch(poObj.optString("branch"));
			po.setStatus(PurchaseOrder.APPROVED);
			JSONArray productArray = null;
			try {
				productArray = poObj.getJSONArray("productList");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			boolean noProductFound=false;
			boolean noWarehouseFound=false;
			String productCode;
			double totalAmount=0;
			ArrayList<ProductDetailsPO> productArrayList=new ArrayList<ProductDetailsPO>();
			for (int j = 0; j < productArray.length(); j++) {
				JSONObject productObj = null;
				try {
					productObj = productArray.getJSONObject(j);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					logger.log(Level.SEVERE,"ERROR 1::::::::::::::"+e1);
					e1.printStackTrace();
				}
				
				productCode=productObj.optString("productCode");
				logger.log(Level.SEVERE,"productCode::::"+productCode);
				ItemProduct itemProduct=ofy().load().type(ItemProduct.class).filter("companyId",companyId).filter("productCode",productCode.trim()).first().now();
				logger.log(Level.SEVERE,"itemProduct::::"+itemProduct);
				if(itemProduct==null){
					noProductFound=true;
					logger.log(Level.SEVERE,"itemProduct NULL::::");
					break;
				}else{
					logger.log(Level.SEVERE,"itemProduct PRESENT::::");
					ProductDetailsPO prodDetails=new ProductDetailsPO();
					prodDetails.setProductID(itemProduct.getCount());
					prodDetails.setProductName(itemProduct.getProductName());
					prodDetails.setProductCode(itemProduct.getProductCode());
					prodDetails.setProductCategory(itemProduct.getProductCategory());
					prodDetails.setProdPrice(Double.parseDouble(productObj.optString("price")));
					prodDetails.setProductQuantity(Double.parseDouble(productObj.optString("quantity")));
					prodDetails.setUnitOfmeasurement(productObj.optString("uom"));
					prodDetails.setVat(0);
					prodDetails.setPrduct(itemProduct);
//					Tax vatTax=new Tax();
//					vatTax.setPercentage(0);
//					prodDetails.setVatTax();					
					prodDetails.setDiscount(0);
					prodDetails.setTax(0);
					try {
						prodDetails.setProdDate(spf.parse(productObj.optString("deliveryDate")));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					prodDetails.setProductRefId(productObj.optString("productRefId").trim());
					double totalAmountOfOneProd=Double.parseDouble(productObj.optString("price"))*Double.parseDouble(productObj.optString("quantity"));
					prodDetails.setTotal(totalAmountOfOneProd);
					totalAmount=totalAmount+totalAmountOfOneProd;
					WareHouse warehouse=ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("warehouseCode",productObj.optString("warehouseCode").trim()).first().now();
					if(warehouse==null){
						noWarehouseFound=true;
						break;
					}else{
						prodDetails.setWarehouseName(warehouse.getBusinessUnitName().trim());
//						prodDetails.setWarehouseDeliveryAddress(getWarehouseAddress(warehouse));
					}
					productArrayList.add(prodDetails);
				}	
			}
			po.setTotal(totalAmount);
			po.setNetpayble(totalAmount);
			po.setApproverName("");
			po.setEmployee("");
			po.setAssignTo1("Neha Singh");
			ArrayList<String> statusList=new ArrayList<>();
			statusList.add(AccountingInterface.TALLYCREATED);
			statusList.add(AccountingInterface.FAILED);
			if(noProductFound){
//				productCode
				logger.log(Level.SEVERE,"IN noProductFound");
				List<AccountingInterface> acctingInt=ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("referenceDocumentNo1",poObj.optString("refDocNo").trim()).filter("status IN", statusList).filter("documentType", "Purchase Order").filter("direction", AccountingInterface.INBOUNDDIRECTION).filter("sourceSystem", AppConstants.NBHC).filter("destinationSystem", AppConstants.CCPM).list();
				for (AccountingInterface accountingInterface : acctingInt) {
					accountingInterface.setStatus(AccountingInterface.FAILED);
					accountingInterface.setRemark("Product Not Found!!");
					accountingInterface.setDateofSynch(new Date());
					ofy().save().entity(accountingInterface);
				}
				break;
			}else{
				if(noWarehouseFound){
					logger.log(Level.SEVERE,"IN noWarehouseFound");
					List<AccountingInterface> acctingInt=ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("referenceDocumentNo1",poObj.optString("refDocNo").trim()).filter("status IN",statusList).filter("documentType", "Purchase Order").filter("direction", AccountingInterface.INBOUNDDIRECTION).filter("sourceSystem", AppConstants.NBHC).filter("destinationSystem", AppConstants.CCPM).list();
					for (AccountingInterface accountingInterface : acctingInt) {
						accountingInterface.setStatus(AccountingInterface.FAILED);
						accountingInterface.setRemark("Warehouse not found!!");
						accountingInterface.setDateofSynch(new Date());
						ofy().save().entity(accountingInterface);
					}
					
				}else{
					po.setProductDetails(productArrayList);
					logger.log(Level.SEVERE,"vendor Ref Id::::::::"+poObj.optString("vendorRefId").trim());
					Vendor vendor=ofy().load().type(Vendor.class).filter("companyId", companyId).filter("vendorRefNo", poObj.optString("vendorRefId").trim()).first().now();
					logger.log(Level.SEVERE,"vendor::::::::"+vendor);
					VendorDetails vendorDetails=new VendorDetails();
					if(vendor==null){
						Vendor newVendor=new Vendor();
						String vendRefId=poObj.optString("vendorRefId").trim();
						String vendorName=poObj.optString("vendorName").trim();
						String vendorCell=poObj.optString("vendorCell").trim();
						NumberGeneration ngVendor=ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("processName", "Vendor").filter("status", true).first().now();
						long number = ngVendor.getNumber();
						int count = (int) number+1;
						ngVendor.setNumber(count);
						ofy().save().entity(ngVendor);
						newVendor=createNewVendor(vendRefId,vendorName,vendorCell,companyId,count);
						vendorDetails.setCount(count);
						vendorDetails.setRefNo(vendRefId);
						vendorDetails.setVendorName(vendorName);
						vendorDetails.setVendorEmailId("");
						vendorDetails.setPocName("");
						vendorDetails.setVendorPhone(Long.parseLong(vendorCell));
						vendorDetails.setStatus(true);
					}else{
						vendorDetails.setVendorId(vendor.getCount());
						vendorDetails.setVendorName(vendor.getVendorName().trim());
						vendorDetails.setVendorEmailId(vendor.getEmail().trim());
						vendorDetails.setVendorPhone(vendor.getCellNumber1());
						vendorDetails.setPocName(vendor.getfullName().trim());
						vendorDetails.setRefNo(vendor.getVendorRefNo()+"");
						vendorDetails.setStatus(true);
					}
					List<VendorDetails> vendorDetailsList=new ArrayList<VendorDetails>();
					vendorDetailsList.add(vendorDetails);
					po.setVendorDetails(vendorDetailsList);
					logger.log(Level.SEVERE,"Done with PO Creation!!");
					logger.log(Level.SEVERE,"Creating GRN Now for PO:"+po.getRefOrderNO());
					NumberGeneration ng = new NumberGeneration();
					ng = ofy().load().type(NumberGeneration.class)
							.filter("companyId", companyId)
							.filter("processName", "PurchaseOrder").filter("status", true)
							.first()
							.now();

					long number = ng.getNumber();
					int count = (int) number;
					po.setCount(count+1);
					ng.setNumber(count+1);
					ofy().save().entity(ng);
					ofy().save().entity(po);
					createGRNDocuments(po,companyId);
				}
			}
			
		}
		logger.log(Level.SEVERE,"DONE WITH IT!!!!!!");
	}
	
	private Vendor createNewVendor(String vendRefId, String vendorName, String vendorCell, long companyId, int count) {
		// TODO Auto-generated method stub
		Vendor vendor=new Vendor();
		vendor.setCompanyId(companyId);
		vendor.setVendorName(vendorName);
		vendor.setVendorRefNo(vendRefId.trim());
		vendor.setCellNumber1(Long.parseLong(vendorCell.trim()));
		vendor.setCount(count);
		vendor.setVendorStatus(true);
//		vendor.setStatus();
		ofy().save().entity(vendor);
		return vendor;
	}

	private void createGRNDocuments(PurchaseOrder po, long companyId) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside GRN Creation!!");
		HashSet<String> warehouseName=new HashSet<String>();
		for (int j = 0; j < po.getProductDetails().size(); j++) {
			warehouseName.add(po.getProductDetails().get(j).getWarehouseName().trim());
		}
		Object[] warehouseNameArray=warehouseName.toArray();
		
		for (int i = 0; i < warehouseNameArray.length; i++) {
			GRN grn=new GRN();
			grn.setCompanyId(companyId);
			grn.setPoNo(po.getCount());
			grn.setPoTitle(po.getPOName().trim());
			grn.setPoCreationDate(po.getPODate());
			grn.setTitle(po.getPOName().trim());
			try {
				grn.setGrnCreationDate(spf.parse(spf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			grn.setRefNo(po.getRefOrderNO().trim()+"");
			grn.setGrnReferenceDate(po.getReferenceDate());
			grn.setBranch(po.getBranch().trim());
			grn.setEmployee(po.getEmployee().trim());
			grn.setStatus(GRN.CREATED);
			grn.setInspectionRequired("NO");
			PersonInfo pi=new PersonInfo();
			pi.setCount(po.getVendorDetails().get(0).getVendorId());
			pi.setFullName(po.getVendorDetails().get(0).getVendorName());
			pi.setCellNumber(po.getVendorDetails().get(0).getVendorPhone());
			pi.setPocName(po.getVendorDetails().get(0).getPocName());
			grn.setVendorInfo(pi);
			grn.setVendorReferenceId(po.getVendorDetails().get(0).getRefNo());
			grn.setApproverName("BATCH EMPLOYEE");
			grn.setEmployee("BATCH EMPLOYEE");
			grn.setWarehouseName(warehouseNameArray[i].toString());
			
			
//			for (int j = 0; j < warehouseNameArray.length; j++) {
				List<GRNDetails> grnDetailsList=new ArrayList<GRNDetails>();
				
				for (int j2 = 0; j2 < po.getProductDetails().size(); j2++) {
					if(warehouseNameArray[i].toString().equalsIgnoreCase(po.getProductDetails().get(j2).getWarehouseName().trim())){
						GRNDetails grnDetails=new GRNDetails();
						grnDetails.setProductID(po.getProductDetails().get(j2).getProductID());
						grnDetails.setProductName(po.getProductDetails().get(j2).getProductName());
						grnDetails.setProductCode(po.getProductDetails().get(j2).getProductCode());
						grnDetails.setProductCategory(po.getProductDetails().get(j2).getProductCategory());
						grnDetails.setProdPrice(po.getProductDetails().get(j2).getProdPrice());
						grnDetails.setProductQuantity(po.getProductDetails().get(j2).getProductQuantity());
						grnDetails.setUnitOfmeasurement(po.getProductDetails().get(j2).getUnitOfmeasurement());
						grnDetails.setWarehouseLocation(po.getProductDetails().get(j2).getWarehouseName());
						StorageLocation storageLocation=ofy().load().type(StorageLocation.class).filter("companyId", companyId).filter("warehouseName", po.getProductDetails().get(j2).getWarehouseName().trim()).first().now();
						grnDetails.setStorageLoc(storageLocation.getBusinessUnitName().trim());
						Storagebin storageBin=ofy().load().type(Storagebin.class).filter("companyId", companyId).filter("storagelocation", storageLocation.getBusinessUnitName().trim()).first().now();
						grnDetails.setStorageBin(storageBin.getBinName().trim());
						grnDetails.setProductRefId(po.getProductDetails().get(j2).getProductRefId());
						grnDetails.setTotal(po.getTotal());
						grnDetails.setVat(po.getProductDetails().get(j2).getVat());
						grnDetails.setTax(po.getProductDetails().get(j2).getTax());
						grnDetails.setDiscount(po.getProductDetails().get(j2).getDiscount());
						grnDetailsList.add(grnDetails);
					}
//				}
				grn.setInventoryProductItem(grnDetailsList);
				logger.log(Level.SEVERE,"Done with GRN Creation!!");
				/*
				 * Saving here because if warehouse changes for product it will create new grn rest data will be same
				 */
				GenricServiceImpl implSave= new GenricServiceImpl();
				implSave.save(grn);
				logger.log(Level.SEVERE,"GRN SAVED");
			}
			
		}
//		GenricServiceImpl implSave= new GenricServiceImpl();
//		implSave.save(po);
		logger.log(Level.SEVERE,"PO SAVED");
		ArrayList<String> statusList=new ArrayList<>();
		statusList.add(AccountingInterface.TALLYCREATED);
		statusList.add(AccountingInterface.FAILED);
		List<AccountingInterface> acctingInt=ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("referenceDocumentNo1",po.getRefOrderNO().trim()).filter("status IN",statusList).filter("documentType", "Purchase Order").filter("direction", AccountingInterface.INBOUNDDIRECTION).filter("sourceSystem", AppConstants.NBHC).filter("destinationSystem", AppConstants.CCPM).list();
		for (AccountingInterface accountingInterface : acctingInt) {
			accountingInterface.setStatus(AccountingInterface.TALLYSYNCED);
			accountingInterface.setRemark("Successfully Created Documents");
			accountingInterface.setDateofSynch(new Date());
			ofy().save().entity(accountingInterface);
		}
	}

}
