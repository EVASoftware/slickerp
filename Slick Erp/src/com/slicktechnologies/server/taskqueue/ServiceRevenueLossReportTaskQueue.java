package com.slicktechnologies.server.taskqueue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;


//import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.slicktechnologies.server.CustomerNameChangeServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.ServerUnitConversionUtility;
import com.slicktechnologies.server.ServiceListServiceImpl;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.android.MarkCompletedSubmitServlet;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.materialconsumption.MaterialConsumptionImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.servicerelated.ToolGroup;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ServiceRevenueLossReportTaskQueue extends HttpServlet{

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6162632154517072419L;
	Logger logger = Logger.getLogger("Name of logger");
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdf= new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)
			throws ServletException, IOException {
		
		 String operationName = request.getParameter("OperationName");
		 String compId = request.getParameter("companyId");
		 long companyId = 0;
		 if(compId!=null){
			 companyId = Long.parseLong(request.getParameter("companyId"));
		 }
		
		 switch (operationName) {
		 
		 case "RevenueLossReportWithMail":
			 sendServiceRevenueLossReport(request,companyId);
			 break;
		 
		 default:
				break;		 
		 
		 
		 }
		 
		 
		 
		
		
	}

	private void sendServiceRevenueLossReport(HttpServletRequest request, long companyId) throws IOException {
		logger.log(Level.SEVERE,"Inside ServiceRevenueLossReportTaskQueue send Service revenue loss report");
		CustomerNameChangeServiceImpl impl=new CustomerNameChangeServiceImpl();
		String fromDate1 = request.getParameter("fromDate").trim();
		String toDate1 = request.getParameter("toDate").trim();
		
		 
			Date fromDate = null , toDate = null;
			try {
				if(fromDate1 != null && !fromDate1.equalsIgnoreCase("null")){
					fromDate = isoFormat.parse(isoFormat.format(sdf.parse(fromDate1)));
					
				}
				if(toDate1 != null && !toDate1.equalsIgnoreCase("null") ){
					toDate = isoFormat.parse(isoFormat.format(sdf.parse(toDate1)));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			ArrayList<String> branchList = new ArrayList<String>();
			 String str = request.getParameter("brnachlist");
			 

				ArrayList<String> emailIdList = new ArrayList<String>();
				 String mail = request.getParameter("EmailList");
				 
			 
			 try{
					Gson gson = new GsonBuilder().create();	
					JSONArray jsonarr=new JSONArray(str);
					logger.log(Level.SEVERE, "JSONArray :: "+jsonarr.length());
					for(int i=0;i<jsonarr.length();i++){
						branchList.add(jsonarr.getString(i));
					}
					
					JSONArray jsonarr1=new JSONArray(mail);
					logger.log(Level.SEVERE, "JSONArray :: "+jsonarr1.length());
					for(int i=0;i<jsonarr1.length();i++){
						emailIdList.add(jsonarr1.getString(i));
					}
					
					
					
					
				logger.log(Level.SEVERE, "branchlist Size"+branchList.size());
					
		List<Service>lossRevenueReport=impl.getServiceListData(companyId, fromDate, toDate, branchList,emailIdList);
		
		logger.log(Level.SEVERE, "Lossrevenue Size"+lossRevenueReport.size());
		
		ArrayList<String>emailList1=emailIdList;
		
		
		
		ArrayList<Service> lossRevenueReportList = new ArrayList<Service>(lossRevenueReport);	 
		
		logger.log(Level.SEVERE, "LossrevenueReportList Size"+lossRevenueReportList.size());
		CsvWriter.services = lossRevenueReportList;
		
		CsvWriter.fromDate = fromDate;
		CsvWriter.toDate = toDate;
		
//		PrintWriter writer =null;      
//      writer = new PrintWriter(new File(".csv")); 
//		PrintWriter writer=new PrintWriter(writer);
		
		CsvWriter csv=new CsvWriter();
		Company company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
      
		ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
		PrintWriter pw=new PrintWriter(outByteStream);
		csv.getCustomeServiceRevenueLossDetailListCSV(pw);
		pw.flush();
		pw.close();
//		pw.write(outByteStream+"");
		
		byte[] outArray = outByteStream.toByteArray();
		
		logger.log(Level.SEVERE, "outArray  -  sizzzzzzzzze -- "+outArray.length);
		
		
		
		StringBuilder builder =new StringBuilder();


		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head>");
		builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("<h3 class=\"ex\" align=\"left\">" + "Service Revenue Loss Report"
				+ "</h3></br></br>");

		builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

		builder.append("Please find attachment.");
		
		// Company Details
		builder.append("<br>");
		builder.append("<b>" + company.getBusinessUnitName() + "</b></br>");
		builder.append("<table>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Address : ");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getAddrLine2().equals("")
				&& company.getAddress().getAddrLine2() != null)
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getAddrLine2() + " "
					+ company.getAddress().getLocality());
		else
			builder.append(company.getAddress().getAddrLine1() + " "
					+ company.getAddress().getLocality());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("</td>");
		builder.append("<td>");
		if (!company.getAddress().getLandmark().equals("")
				&& company.getAddress().getLandmark() != null)
			builder.append(company.getAddress().getLandmark() + ","
					+ company.getAddress().getCity() + ","
					+ company.getAddress().getPin()
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		else
			builder.append(company.getAddress().getCity() + ","
					+ company.getAddress().getPin() + ","
					+ company.getAddress().getState() + ","
					+ company.getAddress().getCountry());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Website : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getWebsite());
		builder.append("</td>");
		builder.append("</tr>");

		builder.append("<tr>");
		builder.append("<td>");
		builder.append("Phone No : ");
		builder.append("</td>");
		builder.append("<td>");
		builder.append(company.getCellNumber1());
		builder.append("</td>");
		builder.append("<tr>");

		builder.append("</tr>");
		builder.append("</table>");
		builder.append("</body>");
		builder.append("</html>");

		String contentInHtml = builder.toString();
		System.out.println(contentInHtml);
		
		Email email = new Email();
		email.sendMailWithGmail(company.getEmail(),emailList1 ,new ArrayList<String>(), new ArrayList<String>(), "Service Revenue Report Between - "+(fmt.format(fromDate))+" To "+(fmt.format(toDate)),  contentInHtml, "text/html", outByteStream,"Service Revenue Report" + ".csv","text/csv");
        
	  }catch(Exception e){
			 
			 }
		
		
	}


	
	
	
	
	
	
}
