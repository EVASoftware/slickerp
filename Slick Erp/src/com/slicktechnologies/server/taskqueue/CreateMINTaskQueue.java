package com.slicktechnologies.server.taskqueue;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.slicktechnologies.server.ServiceListServiceImpl;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class CreateMINTaskQueue extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 848173150656434024L;
	Logger logger = Logger.getLogger("Name of logger");
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdf= new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp)throws ServletException, IOException {
		
		 logger.log(Level.SEVERE, "INSIDE CREATE MIN TSKQUEUE");
		 String compId = request.getParameter("companyId");
		 long companyId = 0;
		 if(compId!=null){
			 companyId = Long.parseLong(request.getParameter("companyId"));
		 }
		 ServiceProject serProj = null;
		 ServiceListServiceImpl impl = new ServiceListServiceImpl();
		 String taskKeyAndValue = request.getParameter("taskKeyAndValue");
		 logger.log(Level.SEVERE, "Task Key And Value "	+ taskKeyAndValue);
		 String[] taskValue = taskKeyAndValue.split("[$]");
		 String branch = taskValue[3].trim();
	     String loginUser = taskValue[4].trim();
		
		
		
		  try {
		    	  Gson gson = new GsonBuilder().create();	
								JSONObject jsonObj = new JSONObject(taskValue[2].trim());
								serProj = new ServiceProject();
								serProj = gson.fromJson(jsonObj.toString(), ServiceProject.class);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 
			if( taskValue[0].equalsIgnoreCase("CreateMIN")){
				impl.CreateMIN(serProj ,branch , loginUser, true);
			}
				 
		 
		 
		 
	
	
	
	}

}
