package com.slicktechnologies.server.taskqueue;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

public class ProductDataTaskQueue extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1512834868955646921L;
	Logger logger = Logger.getLogger("Name Of logger");
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		
		logger.log(Level.SEVERE, "Welcome to Service Product details  Task Queue");

		String 	companyIdString = req.getParameter("ProductDetaiilsValueKey");
		
		logger.log(Level.SEVERE, "companyIdString value  " +companyIdString);
		
		long companyId=Long.parseLong(companyIdString);
		
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "ServiceProduct").first().now();
		long lastNumber = numbergen.getNumber();
    	 int count = (int) lastNumber;
    	 
    	 /**Date 28 feb 2017
    	  * added by vijay
    	  * below code is added for if company status is Active then below flag will false for not creating customer branch and employee
    	  */
    	 boolean companyStatusflag=true;
 		 Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
 			if(company.getStatus().equalsIgnoreCase("Active")){
 				companyStatusflag = false;
 			}
 		
 		/**
 		 * ends here
 		 */
		
		createGIPFortnightlyProduct(companyId,count+1);
		createBedBugQuarterlyProduct(companyId,count+2);
		createSpiderControlAltMonthly(companyId,count+3);
		createSpiderControlAltMonthly(companyId,count+4);
		createRodentControlWeekly(companyId,count+5);
		createGIPWeekly(companyId,count+6);
		createSpiderControlQuartly(companyId,count+7);
		createLizzardControlWeekly(companyId,count+8);
		createFlyControlSingle(companyId,count+9);
		createRodentControlSingle(companyId,count+10);
		createMosquitoControlMonthly(companyId,count+11);
		createFOGGINGTREATMENT3Sservices(companyId,count+12);
		createFUNGUSTREATMENT(companyId,count+13);
		createHONEYCOMBSINGLE(companyId,count+14);
		createTCSPRAYINGSINGLESERVICE(companyId,count+15);
		createWOODBORER(companyId,count+16);
		createGENERALDISINFESTATIONDRAINAGES(companyId,count+17);
		createTERMITECONTROLPRECONSTRUCTION(companyId,count+18);
		createTERMITECONTROLCOMPREHENSIVE(companyId,count+19);
		createTERMITECONTROLEXTERNAL(companyId,count+20);
		createFOGGINGALTERNATEMONTH(companyId,count+21);
		creatGLUEPAD(companyId,count+22);
		createBirdNetting(companyId,count+23);
		
		
	 numbergen.setNumber(count+23);
     GenricServiceImpl impl = new GenricServiceImpl();
	 impl.save(numbergen);
	 
	 //    for item product details
	 createItemProductDetails(companyId);
	
	 createTrancsactionType(companyId);
	 
	 //   set other details 
	 
	 createCountryDetails(companyId);
	 createStateDetails(companyId);
	 createCityDetails(companyId);
	 createLocalityDetails(companyId);
	 
	 /**Date 28 feb 2017
	  * added by vijay
	  *  if company status is Active (production) then below flag is false for not creating customer branch  employee
	  *  warehouse location and bin and if company is demo then below flag will true for creating customer branch employee and Demo.
	  */
	 if(companyStatusflag){
		 createBranchDetails(companyId);
		 createEmployeeDetails(companyId);
		 createCustomerDetails(companyId);
		 createWareHouse(companyId);
		 createStorageLocation(companyId);
		 createStorageBin(companyId);
	 }
	 /**
	  * ends here
	  */
	
	 createDepartmentDetails(companyId);
	 
	 
	}

/**************************************all Transaction types*****************************************/	
	private void createTrancsactionType(long companyId) {
		
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "MaterialMovementType").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		getReturnTransactionTypeData(companyId,count+1);
		getScrapTransactionTypeData(companyId,count+2);
		getTinTransactionTypeData(companyId,count+3);
		getToutTransactionTypeData(companyId,count+4);
		
		numbergen.setNumber(count+4);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}
	
	
	

	private void getReturnTransactionTypeData(long companyId, int i) {
		
		MaterialMovementType transactionType=new MaterialMovementType();
		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("Return");
		transactionType.setMmtType("IN");
		transactionType.setCount(i);
		ofy().save().entity(transactionType);
	}

	private void getScrapTransactionTypeData(long companyId, int i) {
	
		MaterialMovementType transactionType=new MaterialMovementType();
		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("Scrap");
		transactionType.setMmtType("OUT");
		transactionType.setCount(i);
		ofy().save().entity(transactionType);
	}

	private void getTinTransactionTypeData(long companyId, int i) {
	
		MaterialMovementType transactionType=new MaterialMovementType();
		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("TransferIN");
		transactionType.setMmtType("TRANSFERIN");
		transactionType.setCount(i);
		ofy().save().entity(transactionType);
	}

	private void getToutTransactionTypeData(long companyId, int i) {
		
		MaterialMovementType transactionType=new MaterialMovementType();
		transactionType.setCompanyId(companyId);
		transactionType.setMmtStatus(true);
		transactionType.setMmtName("TransferOUT");
		transactionType.setMmtType("TRANSFEROUT");
		transactionType.setCount(i);
		ofy().save().entity(transactionType);
	}

/*****************************department Details *************************************************/	
	
	private void createDepartmentDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Department").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createDepartmentService(companyId,count+1);
		createDepartmentAdmin(companyId,count+2);
		createDepartmentSales(companyId,count+3);
		createDepartmentBackOffice(companyId,count+4);
		createDepartmentAccounts(companyId,count+5);
		createDepartmentHR(companyId,count+6);
		
		numbergen.setNumber(count+6);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}

	private void createDepartmentService(long companyId, int i) {
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setDeptName("Service");
		dept.setCellNo1(9513202569L);
		dept.setHodName("Vedant Kamble");
		dept.setCount(i);
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		ofy().save().entity(dept);
	}

	private void createDepartmentAdmin(long companyId, int i) {
	
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setDeptName("Admin");
		dept.setCellNo1(9513202569L);
		dept.setCount(i);
		dept.setHodName("Vedant Kamble");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		ofy().save().entity(dept);
	}

	private void createDepartmentSales(long companyId, int i) {
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setCount(i);
		dept.setDeptName("Sales");
		dept.setCellNo1(9513202569L);
		dept.setHodName("Vedant Kamble");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		ofy().save().entity(dept);
	}

	private void createDepartmentBackOffice(long companyId, int i) {
	
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setCount(i);
		dept.setDeptName("Back Office");
		dept.setCellNo1(9513202569L);
		dept.setHodName("Vedant Kamble");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		ofy().save().entity(dept);
	}

	private void createDepartmentAccounts(long companyId, int i) {
	
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setCount(i);
		dept.setDeptName("Accounts");
		dept.setCellNo1(9513202569L);
		dept.setHodName("Vedant Kamble");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		ofy().save().entity(dept);
	}

	private void createDepartmentHR(long companyId, int i) {
		
		Department dept = new Department();
		dept.setCompanyId(companyId);
		dept.setStatus(true);
		dept.setDeptName("HR");
		dept.setCount(i);
		dept.setCellNo1(9513202569L);
		dept.setHodName("Vedant Kamble");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		dept.setAddress(address);
		ofy().save().entity(dept);
	}

/***********************************customer details *****************************************/	
	private void createCustomerDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Customer").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createEmployeeResidencial(companyId,count+1);
		createEmployeeCommercial(companyId,count+2);
		
		numbergen.setNumber(count+2);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}

	private void createEmployeeResidencial(long companyId, int i) {
	
		Customer cust = new Customer();
		cust.setCompany(false);
		cust.setCompanyName("");
		cust.setCount(i);
		cust.setDob(new Date());
		cust.setFullname("Suraj Sharma");
		cust.setEmail("evasoftwaresolutionserp@gmail.com");
		cust.setCellNumber1(9876543210L);
		cust.setBranch("Dadar");
		cust.setCategory("RESIDENCIAL");
		cust.setEmployee("Rohan Bhagde");
		cust.setType("1 BHK FLAT");
		cust.setGroup("Register Billing");
		cust.setCustomerLevel("Silver");
		cust.setCustomerPriority("MEDIUM");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		cust.setAdress(address);
		cust.setSecondaryAdress(address);
		cust.setCompanyId(companyId);
		ofy().save().entity(cust);
	}

	private void createEmployeeCommercial(long companyId, int i) {
		
		Customer cust = new Customer();
		cust.setCompany(true);
		cust.setCompanyName("Sharma & Sons");
		cust.setDob(new Date());
		cust.setFullname("Suraj Sharma");
		cust.setEmail("evasoftwaresolutionserp@gmail.com");
		cust.setCellNumber1(9876543210L);
		cust.setBranch("Dadar");
		cust.setCategory("COMMERCIAL");
		cust.setEmployee("Vedant Kamble");
		cust.setType("OFFICES");
		cust.setGroup("Register Billing");
		cust.setCustomerLevel("Silver");
		cust.setCustomerPriority("MEDIUM");
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		cust.setAdress(address);
		cust.setSecondaryAdress(address);
		cust.setCompanyId(companyId);
		cust.setCount(i);
		ofy().save().entity(cust);
	}

/***********************************All employee details *********************************************/ 
	private void createEmployeeDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Employee").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createEmployeeVedantKamble(companyId,count+1);
		createEmployeeRohanBhagde(companyId,count+2);
		createEmployeeRahulVerma(companyId,count+3);
		
		numbergen.setNumber(count+3);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}


	private void createEmployeeVedantKamble(long companyId, int i) {
		Employee emp = new Employee();
		emp.setCountry("India");
		emp.setFullname("Vedant Kamble");
		emp.setBranchName("Navi Mumbai");
		emp.setRoleName("Head Person");
		emp.setDesignation("Service Manager");
		emp.setStatus(true);
		emp.setEmail("evasoftwaresolutionserp@gmail.com");
		emp.setCount(i);
		emp.setCellNumber1(9876546985L);
		Address address=new Address();
		address.setAddrLine1("Vashi");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		
		emp.setAddress(address);
		emp.setCompanyId(companyId);
		ofy().save().entity(emp);
	}


	private void createEmployeeRohanBhagde(long companyId, int i) {
		
		Employee emp = new Employee();
		emp.setCountry("India");
		emp.setFullname("Rohan Bhagde");
		emp.setBranchName("Dadar");
		emp.setRoleName("Head Person");
		emp.setDesignation("Service Manager");
		emp.setStatus(true);
		emp.setEmail("evasoftwaresolutionserp@gmail.com");
		emp.setCount(i);
		emp.setCellNumber1(9876546985L);
		Address address=new Address();
		address.setAddrLine1("Vikhroli");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		
		emp.setAddress(address);
		emp.setCompanyId(companyId);
		ofy().save().entity(emp);
	}


	private void createEmployeeRahulVerma(long companyId, int i) {
		
		Employee emp = new Employee();
		emp.setCountry("India");
		emp.setFullname("Rahul Verma");
		emp.setBranchName("Andheri");
		emp.setRoleName("Head Person");
		emp.setDesignation("Service Manager");
		emp.setStatus(true);
		emp.setEmail("evasoftwaresolutionserp@gmail.com");
		emp.setCount(i);
		emp.setCellNumber1(9876546985L);
		Address address=new Address();
		address.setAddrLine1("Andheri");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		
		emp.setAddress(address);
		emp.setCompanyId(companyId);
		ofy().save().entity(emp);
	}


/**************************************all Branch Details *********************************************/	
	private void createBranchDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Branch").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createDadarBranch(companyId,count+1);
		createAndheriBranch(companyId,count+2);
		createNaviMumbaiLocality(companyId,count+3);
		
		numbergen.setNumber(count+3);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}

	
	
	
	private void createDadarBranch(long companyId, int i) {
		
		Branch branch = new Branch();
		branch.setBusinessUnitName("Dadar");
		branch.setEmail("evasoftwaresolutionsdev@gmail.com");
		branch.setstatus(true);
		branch.setCellNumber1(981234567);
		branch.setPocName("Kiran Kadwadkar");
		branch.setPocCell(981234567);
		branch.setCount(i);
		
		Address address=new Address();
		address.setAddrLine1("Dadar");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		branch.setAddress(address);
		branch.setCompanyId(companyId);
		ofy().save().entity(branch);
	}

	private void createAndheriBranch(long companyId, int i) {
		
		Branch branch = new Branch();
		branch.setBusinessUnitName("Andheri");
		branch.setEmail("evasoftwaresolutionsdev@gmail.com");
		branch.setstatus(true);
		branch.setCellNumber1(981234567);
		branch.setPocName("Kiran Kadwadkar");
		branch.setPocCell(981234567);
		branch.setCount(i);
		
		Address address=new Address();
		address.setAddrLine1("Andheri");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		branch.setAddress(address);
		branch.setCompanyId(companyId);
		ofy().save().entity(branch);
	}

	private void createNaviMumbaiLocality(long companyId, int i) {
		
		Branch branch = new Branch();
		branch.setBusinessUnitName("Navi Mumbai");
		branch.setEmail("evasoftwaresolutionsdev@gmail.com");
		branch.setstatus(true);
		branch.setCellNumber1(981234567);
		branch.setPocName("Kiran Kadwadkar");
		branch.setPocCell(981234567);
		branch.setCount(i);
		
		Address address=new Address();
		address.setAddrLine1("Navi Mumbai");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(476288);
		branch.setAddress(address);
		branch.setCompanyId(companyId);
		ofy().save().entity(branch);
	}

/***********************************all  localities ********************************************/	
	
	private void createLocalityDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Locality").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createDadarLocality(companyId,count+1);
		createGhatkoparLocality(companyId,count+2);
		
		numbergen.setNumber(count+2);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}
	
	private void createGhatkoparLocality(long companyId, int i) {
		Locality lt = new Locality();
		lt.setLocality("Ghatkopar");
		lt.setCityName("Mumbai");
		lt.setStatus(true);
		lt.setCompanyId(companyId);
		lt.setCount(i);
		ofy().save().entity(lt);
	}

	private void createDadarLocality(long companyId, int i) {
		
		Locality lt = new Locality();
		lt.setLocality("Dadar");
		lt.setCityName("Mumbai");
		lt.setStatus(true);
		lt.setCompanyId(companyId);
		lt.setCount(i);
		ofy().save().entity(lt);
	}

/*****************************All  City data ***************************************************/	
	
	private void createCityDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "City").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createMumbaiCity(companyId,count+1);
		createPuneCity(companyId,count+2);
		createJodhpurCity(companyId,count+3);
		createJaipurCity(companyId,count+4);
		
		numbergen.setNumber(count+4);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}

	private void createJaipurCity(long companyId, int i) {
		City ct = new City();
		ct.setCityName("Jodhpur");
		ct.setState("Rajasthan");
		ct.setStatus(true);
		ct.setCompanyId(companyId);
		ct.setCount(i);
		ofy().save().entity(ct);
	}

	private void createJodhpurCity(long companyId, int i) {
		City ct = new City();
		ct.setCityName("Jaipur");
		ct.setState("Rajasthan");
		ct.setStatus(true);
		ct.setCompanyId(companyId);
		ct.setCount(i);
		ofy().save().entity(ct);
	}

	private void createPuneCity(long companyId, int i) {
		City ct = new City();
		ct.setCityName("Pune");
		ct.setState("Maharashtra");
		ct.setStatus(true);
		ct.setCompanyId(companyId);
		ct.setCount(i);
		ofy().save().entity(ct);
	}

	private void createMumbaiCity(long companyId, int i) {
		
		City ct = new City();
		ct.setCityName("Mumbai");
		ct.setState("Maharashtra");
		ct.setStatus(true);
		ct.setCompanyId(companyId);
		ct.setCount(i);
		ofy().save().entity(ct);
	}

/***********************All state data	********************************************/
	
	private void createStateDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "State").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createMaharashtraDetails(companyId,count+1);
		createGujaratDetails(companyId,count+2);
		createRajasthanDetails(companyId,count+3);
		createMadhyaPradeshDetails(companyId,count+4);
		createUttarPradeshDetails(companyId,count+5);
		createJammuAndKashmirDetails(companyId,count+6);
		createKarnatakaDetails(companyId,count+7);
		createAndhraPradeshDetails(companyId,count+8);
		createOdishaDetails(companyId,count+9);
		createChhattisgarhDetails(companyId,count+10);
		createTamilNaduDetails(companyId,count+11);
		createTelanganaDetails(companyId,count+12);
		createBiharDetails(companyId,count+13);
		createWestBengalDetails(companyId,count+14);
		createArunachalPradeshDetails(companyId,count+15);
		createJharkhandDetails(companyId,count+16);
		createAssamDetails(companyId,count+17);
		createHimachalPradeshDetails(companyId,count+18);
		createUttarakhandDetails(companyId,count+19);
		createPunjabDetails(companyId,count+20);
		createHaryanaDetails(companyId,count+21);
		createKeralaDetails(companyId,count+22);
		createMeghalayaDetails(companyId,count+23);
		createManipurDetails(companyId,count+24);
		createDelhiDetails(companyId,count+25);
		
	
		numbergen.setNumber(count+25);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
	}

	private void createDelhiDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Delhi");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createManipurDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Manipur");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createMeghalayaDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Meghalaya");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createKeralaDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("Kerala");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createHaryanaDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("Haryana");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createPunjabDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Punjab");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createUttarakhandDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("Uttarakhand");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createHimachalPradeshDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("Himachal Pradesh");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);		
	}

	private void createAssamDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Assam");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createJharkhandDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Jharkhand");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createArunachalPradeshDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("Arunachal Pradesh");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createWestBengalDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("West Bengal");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createBiharDetails(long companyId, int i) {
		State st = new State();
		st.setStateName("Bihar");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createTelanganaDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Telangana");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createTamilNaduDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Tamil Nadu");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createChhattisgarhDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Chhattisgarh");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createOdishaDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Odisha");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createAndhraPradeshDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Andhra Pradesh");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createKarnatakaDetails(long companyId, int i) {
		

		State st = new State();
		st.setStateName("Karnataka");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createJammuAndKashmirDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Jammu and Kashmir");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createUttarPradeshDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Uttar Pradesh");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createMadhyaPradeshDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Madhya Pradesh");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createRajasthanDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Rajasthan");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createGujaratDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Gujarat");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	private void createMaharashtraDetails(long companyId, int i) {
		
		State st = new State();
		st.setStateName("Maharashtra");
		st.setCountry("India");
		st.setStatus(true);
		st.setCompanyId(companyId);
		st.setCount(i);
		
		ofy().save().entity(st);
	}

	/**********************************This is Country Data ****************************************/
	private void createCountryDetails(long companyId) {
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Country").first().now();
		long lastNumber = numbergen.getNumber();
		int count = (int) lastNumber;
		
		createIndiaDetails(companyId,count+1);
		createAustraliaDetails(companyId,count+2);
		
		numbergen.setNumber(count+2);
	    GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(numbergen);
		
	}


	private void createIndiaDetails(long companyId, int i) {
		
		Country country = new Country();
		country.setCountryName("India");
		country.setLanguage("EN");
		country.setCount(i);
		country.setCompanyId(companyId);
		
		ofy().save().entity(country);
	}


	private void createAustraliaDetails(long companyId, int i) {
		
		Country country = new Country();
		country.setCountryName("Australia");
		country.setLanguage("EN");
		country.setCount(i);
		country.setCompanyId(companyId);
		
		ofy().save().entity(country);
	}


/********************************Storage bin ************************************************************/
	private void createStorageBin(long companyId) {
		
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Storagebin").first().now();
		long lastNumber = numbergen.getNumber();
    	 int count = (int) lastNumber;
		
		createMumbiaBinDetails(companyId,count+1);
		createKatrapRoadBinDetails(companyId,count+2);
		
		numbergen.setNumber(count+2);
	     GenricServiceImpl impl = new GenricServiceImpl();
		 impl.save(numbergen);
		
		
	}



	private void createKatrapRoadBinDetails(long companyId, int i) {
		
		Storagebin binEntity=new Storagebin();
		binEntity.setCompanyId(companyId);
		//vijay added warehouseName
		binEntity.setWarehouseName("Mumbai");
		//end here
		binEntity.setBinName("KATRAP ROAD BIN");
		binEntity.setXcoordinate("20");
		binEntity.setYcoordinate("25");
		binEntity.setStatus(true);
		binEntity.setStoragelocation("Mumbai LOC");
		binEntity.setCount(i);
		
		ofy().save().entity(binEntity);
	}



	private void createMumbiaBinDetails(long companyId, int i) {
		
		Storagebin binEntity=new Storagebin();
		binEntity.setCompanyId(companyId);
		//vijay added warehouseName
		binEntity.setWarehouseName("Mumbai");
		//end here
		binEntity.setBinName("Mumbai BIN");
		binEntity.setXcoordinate("20");
		binEntity.setYcoordinate("25");
		binEntity.setStatus(true);
		binEntity.setStoragelocation("Mumbai LOC");
		binEntity.setCount(i);
		
		ofy().save().entity(binEntity);
	}



/************************************Storage LOcation *************************************************/	
	
	private void createStorageLocation(long companyId) {

		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "StorageLocation").first().now();
		long lastNumber = numbergen.getNumber();
    	 int count = (int) lastNumber;
		
		createMumbaiLOCDetails(companyId,count+1);
		createBadlapurDetails(companyId,count+2);
		
		numbergen.setNumber(count+2);
	     GenricServiceImpl impl = new GenricServiceImpl();
		 impl.save(numbergen);
		
		
		
	}



	private void createBadlapurDetails(long companyId, int i) {
		
		StorageLocation locationEntity=new StorageLocation();
		locationEntity.setCompanyId(companyId);
		locationEntity.setWarehouseName("Kalyan");
		locationEntity.setBusinessUnitName("Badlapur");
		locationEntity.setstatus(true);
		locationEntity.setEmail("evasoftwaresolutionsdev@gmail.com");
		locationEntity.setCellNumber1(9876543210L);
		locationEntity.setPocName("AJINKYA JADHAV");
		locationEntity.setPocCell(9876543210L);
		locationEntity.setCount(i);
		Address address=new Address();
		address.setAddrLine1("Katrap Road");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(400083);
		locationEntity.setAddress(address);
		ofy().save().entity(locationEntity);
	}



	private void createMumbaiLOCDetails(long companyId, int i) {
		
			StorageLocation locationEntity=new StorageLocation();
			locationEntity.setCompanyId(companyId);
			locationEntity.setWarehouseName("Mumbai");
			locationEntity.setBusinessUnitName("Mumbai LOC");
			locationEntity.setstatus(true);
			locationEntity.setEmail("evasoftwaresolutionsdev@gmail.com");
			locationEntity.setCellNumber1(9876543210L);
			locationEntity.setPocName("ROHIT SHARMA");
			locationEntity.setPocCell(9876543210L);
			locationEntity.setCount(i);
			Address address=new Address();
			address.setAddrLine1("Ghatkopar");
			address.setCountry("India");
			address.setState("Maharashtra");
			address.setCity("Mumbai");
			address.setPin(400083);
			locationEntity.setAddress(address);
			ofy().save().entity(locationEntity);
	}


	/*********************************warehouse Details **************************************************/

	private void createWareHouse(long companyId) {
		
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "WareHouse").first().now();
		long lastNumber = numbergen.getNumber();
    	 int count = (int) lastNumber;
		
		createMumbaiWarehouseDetails(companyId,count+1);
		createNashikWarehouseDetails(companyId,count+2);
		
		numbergen.setNumber(count+2);
	     GenricServiceImpl impl = new GenricServiceImpl();
		 impl.save(numbergen);
		
		
		
	}



	private void createNashikWarehouseDetails(long companyId, int i) {
		
		WareHouse warehouseEntity=new WareHouse();
		warehouseEntity.setBusinessUnitName("Kalyan");
		warehouseEntity.setEmail("evasoftwaresolutionserp@gmail.com");
		warehouseEntity.setstatus(true);
		warehouseEntity.setPocName("AJINKYA JADHAV");
		warehouseEntity.setPocCell(9876543210L);
		warehouseEntity.setCount(i);
		
		Address address=new Address();
		address.setAddrLine1("Katrap Road");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(400503);
		warehouseEntity.setAddress(address);
		warehouseEntity.setCompanyId(companyId);
		
		ofy().save().entity(warehouseEntity);
	}



	private void createMumbaiWarehouseDetails(long companyId, int i) {
		
		WareHouse warehouseEntity=new WareHouse();
		warehouseEntity.setBusinessUnitName("Mumbai");
		warehouseEntity.setEmail("evasoftwaresolutionserp@gmail.com");
		warehouseEntity.setstatus(true);
		warehouseEntity.setPocName("ROHIT SHARMA");
		warehouseEntity.setPocCell(9876543210L);
		warehouseEntity.setCount(i);
		
		Address address=new Address();
		address.setAddrLine1("Ghatkopar");
		address.setCountry("India");
		address.setState("Maharashtra");
		address.setCity("Mumbai");
		address.setPin(400083);
		warehouseEntity.setAddress(address);
		warehouseEntity.setCompanyId(companyId);
		
		ofy().save().entity(warehouseEntity);
	}


/***************************************All Service products ******************************************************/

	private void createBirdNetting(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(1);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("BN001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("Bird Netting");
		serviceProd.setPrice(5000);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(serviceProd);	
	}



	private void creatGLUEPAD(long companyId, int i) {
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(1);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("GLP001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("GLUE PAD");
		serviceProd.setPrice(7000);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);		
	}



	private void createFOGGINGALTERNATEMONTH(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(6);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("FG004");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("FOGGING - ALTERNATE MONTH");
		serviceProd.setPrice(7000);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);		
	}



	private void createTERMITECONTROLEXTERNAL(long companyId, int i) {
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(1095);
		serviceProd.setNumberOfServices(4);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("TCE001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setCount(i);
		serviceProd.setProductName("TERMITE CONTROL - EXTERNAL");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);		
	}



	private void createTERMITECONTROLCOMPREHENSIVE(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(1825);
		serviceProd.setNumberOfServices(8);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("TCC001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("TERMITE CONTROL - COMPREHENSIVE");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createTERMITECONTROLPRECONSTRUCTION(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(30);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("TCPRE001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("TERMITE CONTROL - PRECONSTRUCTION");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createGENERALDISINFESTATIONDRAINAGES(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(4);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("GD001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("GENERAL DISINFESTATION - DRAINAGES");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createWOODBORER(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(3);
		serviceProd.setCompanyId(companyId);
		serviceProd.setCount(i);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("WB001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("WOOD BORER");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createTCSPRAYINGSINGLESERVICE(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(1);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setCount(i);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("SRPCC023");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("TC - SPRAYING - SINGLE SERVICE");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createHONEYCOMBSINGLE(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(1);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("H001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("HONEY COMB - SINGLE");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createFUNGUSTREATMENT(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(30);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setCount(i);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("FN001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("FUNGUS TREATMENT");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createFOGGINGTREATMENT3Sservices(long companyId, int i) {

		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(30);
		serviceProd.setNumberOfServices(3);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("FOGG03");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("FOGGING TREATMENT - 3 SERVICES");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
		
	}



	private void createMosquitoControlMonthly(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(12);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("MS-MTH");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("MOSQUITO CONTROL - MONTHLY");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
		
		

	}



	private void createRodentControlSingle(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(30);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCount(i);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("RC003");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("RODENT CONTROL - SINGLE");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createFlyControlSingle(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(30);
		serviceProd.setNumberOfServices(1);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("SRPCC021");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("FLY CONTROL - SINGLE");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createLizzardControlWeekly(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(48);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setCount(i);
		serviceProd.setProductCode("LC001");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("LIZARD CONTROL - WEEKLY");
		serviceProd.setPrice(0);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createSpiderControlQuartly(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(4);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("SPC");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("SPIDER CONTROL - QUARTERLY");
		serviceProd.setPrice(0);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createGIPWeekly(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(48);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("GIPC");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("GENERAL INSECT PEST CONTROL - WEEKLY");
		serviceProd.setPrice(0);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		
		ofy().save().entity(serviceProd);
	}



	private void createRodentControlWeekly(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(48);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("RC");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("RODENT CONTROL - WEEKLY");
		serviceProd.setPrice(0);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(serviceProd);
	}



	private void createSpiderControlAltMonthly(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(6);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("SPC006");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("SPIDER CONTROL - ALT MONTH");
		serviceProd.setPrice(0);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(serviceProd);
	}



	private void createGIPFortnightlyProduct(long companyId, int i) {
		
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(24);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("GR");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("GENERAL INSECT PEST CONTROL - FORTNIGHTLY");
		serviceProd.setPrice(2000);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(serviceProd);
	}
	
	private void createBedBugQuarterlyProduct(long companyId, int i) {
		ServiceProduct serviceProd=new ServiceProduct();
		serviceProd.setDuration(365);
		serviceProd.setNumberOfServices(4);
		serviceProd.setCompanyId(companyId);
		serviceProd.setStatus(true);
		serviceProd.setProductCode("B004");
		serviceProd.setProductCategory("Service Category");
		serviceProd.setProductName("BED BUGS - QUARTERLY");
		serviceProd.setPrice(8000);
		serviceProd.setCount(i);
		/*
		 * nidhi commented for stop display old tex
		 * 		Tax serTax=new Tax();
			serTax.setPercentage(15.0);
			serTax.setTaxConfigName("SER 15");
			serviceProd.setServiceTax(serTax);*/
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		serviceProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		serviceProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(serviceProd);
	}
	
/*************************************All Service Product **********************************************/	
	private void createItemProductDetails(long companyId) {
		
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "ItemProduct").first().now();
		long lastNumber = numbergen.getNumber();
    	 int count = (int) lastNumber;
		
		
		createKOTHRIN(companyId,count+1);
		createRESPONSER(companyId,count+2);
		createBAYTEXT(companyId,count+3);
		createRODABOX(companyId,count+4);
		createNUVAN(companyId,count+5);
		createKEROSENCE(companyId,count+6);
		createPETROL(companyId,count+7);
		createDIESEL(companyId,count+8);
		createTEMPRID(companyId,count+9);
		createRIBANROLL(companyId,count+10);
		
		/**
		 * Date 28 feb 2017
		 * added by vijay
		 * for assets we created item product here
		 */
		createMask(companyId,count+11);
		createBrush(companyId,count+12);
		createRod(companyId, count+13);
		/**
		 * end here
		 */
		
		numbergen.setNumber(count+13);
	     GenricServiceImpl impl = new GenricServiceImpl();
		 impl.save(numbergen);
	}

	private void createRod(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("Rd001");
		itemProd.setProductCategory("Asset");
		itemProd.setProductName("Rod");
		itemProd.setPrice(100);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
		
	}

	private void createBrush(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("Brsh001");
		itemProd.setProductCategory("Asset");
		itemProd.setProductName("Brush");
		itemProd.setPrice(100);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
		
	}

	private void createMask(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("Msk001");
		itemProd.setProductCategory("Asset");
		itemProd.setProductName("Mask");
		itemProd.setPrice(100);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
		
	}

	private void createRIBANROLL(long companyId, int i) {
		
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("RI001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("RIBAN ROLL");
		itemProd.setPrice(67);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
		
	}



	private void createTEMPRID(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("TE001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("TEMPRID");
		itemProd.setPrice(1678);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ltr");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		ofy().save().entity(itemProd);
		
	}



	private void createDIESEL(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("DI001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("DIESEL");
		itemProd.setPrice(55);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ltr");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		ofy().save().entity(itemProd);
	}



	private void createPETROL(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("PE001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("PETROL");
		itemProd.setPrice(68);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ltr");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		ofy().save().entity(itemProd);
	}



	private void createKEROSENCE(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("KE001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("KEROSENCE");
		itemProd.setPrice(160);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ltr");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		ofy().save().entity(itemProd);
	}



	private void createNUVAN(long companyId, int i) {
		
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("NU001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("NUVAN");
		itemProd.setPrice(604);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ltr");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);
		ofy().save().entity(itemProd);
	}



	private void createRODABOX(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("RB001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("RODA BOX");
		itemProd.setPrice(500);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
	}



	private void createBAYTEXT(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("B001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("BAYTEX");
		itemProd.setPrice(2160);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Nos");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
	}

	

	private void createRESPONSER(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("R001");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("RESPONSER");
		itemProd.setPrice(854);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ltr");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
		/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
	}



	private void createKOTHRIN(long companyId, int i) {
		
		ItemProduct itemProd=new ItemProduct();
		itemProd.setProductCode("KO");
		itemProd.setProductCategory("Chemicals");
		itemProd.setProductName("K-OTHRIN");
		itemProd.setPrice(1000);
		itemProd.setCount(i);
		itemProd.setUnitOfMeasurement("Ml");
		itemProd.setCompanyId(companyId);
		itemProd.setStatus(true);
		
	/*	Tax vatTax=new Tax();
		vatTax.setPercentage(6.0);
		vatTax.setTaxConfigName("VAT 6");
		itemProd.setVatTax(vatTax);*/
		/*
		 * nidhi commented for stop display old tax
			/**
			 * nidhi
			 *  2-04-2018 for new tax flow
			 */
		Tax vatTax=new Tax();
		vatTax.setPercentage(9.0);
		vatTax.setTaxConfigName("SGST@9");
		vatTax.setTaxPrintName("SGST");
		itemProd.setVatTax(vatTax);
		
		Tax serTax=new Tax();
		serTax.setPercentage(9.0);
		serTax.setTaxConfigName("CGST@9");
		serTax.setTaxPrintName("CGST");
		itemProd.setServiceTax(serTax);
			
			/*
			 * end
			 */
		ofy().save().entity(itemProd);
	}
	
	

}
