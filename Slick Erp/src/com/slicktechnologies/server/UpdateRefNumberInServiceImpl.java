package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.slicktechnologies.client.views.contract.UpdateRefNumberInService;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;

public class UpdateRefNumberInServiceImpl extends RemoteServiceServlet implements UpdateRefNumberInService{

	/**
	 * 
	 */
	Logger logger = Logger.getLogger("NameOfYourLogger");
	List<Contract> conlis = new ArrayList<Contract>();
	List<Service> servicelis = new ArrayList<Service>();
	Service ser;
	
	private static final long serialVersionUID = 2759288325419256834L;

	@Override
	public void updateService(Date fromDate, Date toDate) {
		
		System.out.println("on server side method ");
		
		conlis=ofy().load().type(Contract.class).filter("contractDate >=",fromDate).filter("contractDate <=",toDate).list();
		
		 logger.log(Level.SEVERE,"From date :::"+fromDate);	
		 logger.log(Level.SEVERE,"To date :::"+toDate);	
		 logger.log(Level.SEVERE,"No of Contracts :::"+conlis.size());	
		 updateservices(conlis);
			
	}

	private void updateservices(List<Contract> conlis2) {
		
		for(int i=0; i<conlis2.size();i++)
		{
			servicelis =ofy().load().type(Service.class).filter("contractCount", conlis2.get(i).getCount())
					.filter("companyId", conlis2.get(i).getCompanyId()).list();
			
			System.out.println("service list size"+servicelis.size());
			
			for(int j=0;j<servicelis.size();j++)
			{
				try{
				if(conlis2.get(i).getCount()== servicelis.get(j).getContractCount())
				{
					ser = new Service();
					ser = ofy().load().type(Service.class).filter("count", servicelis.get(j).getCount()).first().now();
					
					System.out.println("in side condition "+conlis2.get(i).getRefNo());
					if(!conlis2.get(i).getRefNo().equals("") || conlis2.get(i).getRefNo()!=null){
						ser.setRefNo(conlis2.get(i).getRefNo());
					}
					else
					{
						ser.setRefNo(0+"");
					}
					ser.setCompanyId(conlis2.get(i).getCompanyId());
					
					ofy().save().entity(ser).now();
				}
				}
				catch(Exception e)
				{
					System.out.println("in side catch"+e);
				}
			}
		}
	}
	
	
	
	

}
