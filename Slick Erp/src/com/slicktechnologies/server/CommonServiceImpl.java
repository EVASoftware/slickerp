package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.OverridingMethodsMustInvokeSuper;
import javax.validation.OverridesAttribute;

import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.Region;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.api.server.spi.auth.common.User;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.addhocprinting.CustomerServiceRecordPdf;
import com.slicktechnologies.server.addhocprinting.CustomerServiceRecordVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.HeaderFooterPageEvent;
import com.slicktechnologies.server.addhocprinting.MultipleServicesRecordPdf;
import com.slicktechnologies.server.addhocprinting.MultipleServicesRecordVersionOnePdf;
import com.slicktechnologies.server.addhocprinting.SRFormatVersion1;
import com.slicktechnologies.server.api.CreateUpdateObservationApi;
import com.slicktechnologies.server.api.GetUserRegistrationOtp;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.AuditObservations;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.CreditNote;
import com.slicktechnologies.shared.ObservationImpactRecommendation;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractcancel.CancelContract;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class CommonServiceImpl extends RemoteServiceServlet implements CommonService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6756874276334352058L;

	Logger logger = Logger.getLogger("common service:");
	@Override
	public void printSRCopy(long companyId , Date fromDate, Date toDate, String branch,
			int serviceId,ArrayList<String> emailIdList,Contract contract) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "sr" + companyId);
		Gson gson = new Gson();
		String str = gson.toJson(emailIdList);
	//	String str1 = gson.toJson(contract , Contract.class);
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "SendMultipleSrCopies")
					.param("companyId", companyId+ "")
					.param("fromDate", fromDate+"" )
					.param("toDate", toDate+"")
					.param("branch", branch)
					.param("serviceId", serviceId+"")
					.param("contract", contract.getCount()+"")
					.param("email", str));
			logger.log(Level.SEVERE, "sr" + queue);
	//(companyId,fromDate,toDate,branch,serviceId);
	}

	@Override
	public void callSRCopyEmailTaskQueue(long companyId, Service service, String branchEmail) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "in callSRCopyEmailTaskQueue" );
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SendSRCopyOnServiceCompletion", companyId)){

			logger.log(Level.SEVERE, "serviceid "+service.getCount() );
			SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			format1.setTimeZone(TimeZone.getTimeZone("IST"));
			String srNumber = format1.format(new Date());

			String taskName="SendSRCopyEmail"+"$"+companyId+"$"+service.getCount()+"$"+srNumber+"$"+branchEmail;
			logger.log(Level.SEVERE, "task call= "+taskName );
			Queue queue = QueueFactory.getQueue("SRCopyEmailQueueRevised-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/SRCopyEmailTaskQueue").param("taskKeyAndValue", taskName)
					.param("serviceid", service.getCount()+"").etaMillis(System.currentTimeMillis()+180000));
		}
		
	}
	
	
	public void loadSRCopyData(long companyId ,Date fromDate, Date toDate, String branch,int serviceId,ArrayList<String> emailIdList,Contract contract) {
		
		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		List<Service> serviceList = new ArrayList<Service>();
		
		List<Service> totalServicesList = new ArrayList<Service>();
		if(serviceId != 0){
			logger.log(Level.SEVERE, "Condition 1");
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId)
					.filter("contractCount", contract.getCount())
					 .filter("status", Service.SERVICESTATUSCOMPLETED).list();
			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId)
					.filter("contractCount", contract.getCount()).list();
		}else if(fromDate != null && toDate != null &&(branch == null || branch.equals(""))){
			logger.log(Level.SEVERE, "Condition 2");
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("contractCount", contract.getCount())
						.filter("status", Service.SERVICESTATUSCOMPLETED).list();
			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=",fromDate)
					.filter("serviceDate <",toDate)
					.filter("contractCount", contract.getCount()).list();
			
			
		}else if(fromDate != null && toDate != null &&(branch != null || !branch.equals(""))){
			logger.log(Level.SEVERE, "Condition 3");
//			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceDate >=",fromDate)
//					.filter("serviceDate <",toDate)
//					.filter("contractCount", contract.getCount())
//					.filter("status", Service.SERVICESTATUSCOMPLETED).list();
//			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceDate >=",fromDate)
//					.filter("serviceDate <",toDate)
//					.filter("contractCount", contract.getCount())
//					.filter("serviceBranch", branch).list();
			
			
			List<Service> allServiceList = new ArrayList<Service>();
			allServiceList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=",fromDate)
					.filter("serviceDate <",toDate)
					.filter("contractCount", contract.getCount()).list();
			
			if(allServiceList.size()>0) {
				for(Service s:allServiceList) {
					if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)&&s.getServiceBranch().equals(branch))
						serviceList.add(s);
					if(s.getServiceBranch().equals(branch))
						totalServicesList.add(s);
				}
			}
		}else if(fromDate == null && toDate == null &&branch != null && !branch.equals("")){
			logger.log(Level.SEVERE, "Condition 4");
//			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceBranch", branch)
//					.filter("contractCount", contract.getCount())
//				    .filter("status", Service.SERVICESTATUSCOMPLETED).list();
//			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceBranch", branch)
//					.filter("contractCount", contract.getCount()).list();
			
			List<Service> allServiceList = new ArrayList<Service>();
			allServiceList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount", contract.getCount()).list();
			if(allServiceList.size()>0) {
				for(Service s:allServiceList) {
					if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)&&s.getServiceBranch().equals(branch))
						serviceList.add(s);
					if(s.getServiceBranch().equals(branch))
						totalServicesList.add(s);
				}
			}
			
			
		}else {
			logger.log(Level.SEVERE, "Condition 5");
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount", contract.getCount())
				    .filter("status", Service.SERVICESTATUSCOMPLETED).list();
			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount", contract.getCount()).list();
		}
		logger.log(Level.SEVERE, "serviceListcount="+serviceList.size()+" totalServicesList="+totalServicesList.size());
		/**
		 * @author Anil , Date : 04-03-2020
		 * Sending SR copy date wise
		 * right if user download bulk SR copy it generated randomly
		 */
		Comparator<Service> servDateComp=new Comparator<Service>() {
			@Override
			public int compare(Service arg0, Service arg1) {
				return arg0.getServiceDate().compareTo(arg1.getServiceDate());
			}
		};
		Collections.sort(serviceList, servDateComp);
		
		
		/**
		 * @author Anil, Date : 13-03-2020
		 * Changed Map to LinkedHashMap, in order to preserve the order of insertion
		 */
		LinkedHashMap<String , ArrayList<Service>> serviceMap = new LinkedHashMap<String , ArrayList<Service>>();
		ArrayList<Service> serList = new ArrayList<Service>();
		int contractId = 0;
		if(serviceList !=  null){
			for(Service ser : serviceList){
				contractId = ser.getContractCount();
				/**
				 * @author Anil
				 * @since 08-07-2020
				 * If sr copy is not generated then set  service id as service number
				 * @author Anil
				 * @since 20-08-2020
				 * revoking last changes of sendin sr copy whose sr no. not generated
				 * on the ask of vaishnavi and NBHC Team
				 */
				
				if(ser.getSrCopyNumber()==null||ser.getSrCopyNumber().equals("")){
//					ser.setSrCopyNumber(ser.getCount()+"");
					continue;
				}
				
				if(serviceMap.containsKey(ser.getSrCopyNumber())){
					serList = serviceMap.get(ser.getSrCopyNumber());
					serList.add(ser);
					serviceMap.put(ser.getSrCopyNumber(), serList);
				}else{
					serList = new ArrayList<Service>();
					serList.add(ser);
					serviceMap.put(ser.getSrCopyNumber(), serList);
				}
				
			}
		}
		
		/**
		 * @author Anil @since 20-05-2021
		 * SR copy for other than NBHC client
		 */
		ByteArrayOutputStream pdfstream = new ByteArrayOutputStream();
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "OnlyForNBHC", companyId)){
			logger.log(Level.SEVERE,"Inside NBHC specific SR Record PDf");
			MultipleServicesRecordPdf serviceRecordPdf = new MultipleServicesRecordPdf();
			serviceRecordPdf.document = new Document(); 
			
			try {
				PdfWriter.getInstance(serviceRecordPdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			serviceRecordPdf.document.open();
			logger.log(Level.SEVERE, "map size" + serviceMap.size());
			serviceRecordPdf.loadMultipleSRCopies(serviceMap , companyId);
			
			serviceRecordPdf.document.close();
		/**
		 *  Added By Priyanka
		 *  Date : 04-07-2021
		 *  Des : Pecoppp load SR copy
		 */
		}else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "MailSRPecopp", companyId)){
			logger.log(Level.SEVERE,"Inside pecopp specific SR Record PDf");
			System.out.println("Inside Pecopp Sr mail");
			MultipleServicesRecordVersionOnePdf serviceRecordPdf = new MultipleServicesRecordVersionOnePdf();
			serviceRecordPdf.document = new Document(); 
			
			try {
				PdfWriter.getInstance(serviceRecordPdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			serviceRecordPdf.document.open();
			logger.log(Level.SEVERE, "Servicelist size" + serviceList.size());
			System.out.println("Servicelist size" + serviceList.size());
			
			serviceRecordPdf.loadMultipleSRCopies(serviceList);
			logger.log(Level.SEVERE, "load multiple Sr Copy");
			serviceRecordPdf.document.close();
		}else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "PC_ServiceReportV3", companyId)){
			logger.log(Level.SEVERE,"Inside pecopp specific SR Record PDf");
			System.out.println("Inside Innovative Sr mail");
			CustomerServiceRecordVersionOnePdf serviceRecordPdf = new CustomerServiceRecordVersionOnePdf();
			serviceRecordPdf.document = new Document(); 
			
			try {
				PdfWriter.getInstance(serviceRecordPdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			serviceRecordPdf.document.open();
			logger.log(Level.SEVERE, "Servicelist size" + serviceList.size());
			System.out.println("Servicelist size" + serviceList.size());
			
			serviceRecordPdf.loadMultipleSRCopies(serviceList);
			logger.log(Level.SEVERE, "load multiple Sr Copy");
			serviceRecordPdf.document.close();
		}else{
			logger.log(Level.SEVERE,"Inside GENERAL SR Record PDf");
			CustomerServiceRecordPdf customerServiceRecordpdf = new CustomerServiceRecordPdf();
						
			customerServiceRecordpdf.document = new Document();
		//	customerServiceRecordpdf.document.setPageSize(PageSize.A4.rotate());
			try {
				PdfWriter.getInstance(customerServiceRecordpdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			customerServiceRecordpdf.document.open();
			customerServiceRecordpdf.loadMultipleSRCopies(serviceList);
			
			customerServiceRecordpdf.document.close();
					
		}
		
		
		/**
		 * @author Anil , Date : 05-03-2020
		 * Right now Pestinct is send in all mail for all client
		 * it should be only for NBHC
		 */
		String compName="SR - ";
		if(company!=null&&company.getBusinessUnitName()!=null){
			if(company.getBusinessUnitName().contains("NBHC")){
				compName="Pestinct SR - ";
			}
		}
		String mailSub = compName+contractId;
		
//		String mailSub = "Pestinct SR - "+contractId;
		String mailMsg = "Dear Sir/Ma'am ,"+"\n"+"\n"+"\n"+ " <BR>"+"Please find the service record copies.";

		ArrayList<String> toEmailList = new ArrayList<String>();
	//	toEmailList.add("komal.chaudhari@evasoftwaresolutions.com");
		toEmailList.addAll(emailIdList);
		/**
		 * @author Anil , Date : 10-04-2020
		 * added display name for from email id
		 * ,company.getDisplayNameForCompanyEmailId()
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailList, null, null, mailSub, mailMsg, "text/html",pdfstream,"SR Copies.pdf","application/pdf",company.getDisplayNameForCompanyEmailId());
		}else{
			Email email = new Email();
			email.sendMailWithGmail(company.getEmail(), toEmailList, null, null, mailSub, mailMsg, "text/html",pdfstream,"SR Copies.pdf","application/pdf");
		}
	}
	
	
	//Ashwini Patil
	public void loadSummarySRCopyData(long companyId ,Date fromDate, Date toDate, String branch,int serviceId,ArrayList<String> emailIdList,Contract contract,String reportName,String reportFormat,boolean completedFlag,boolean cancelledFlag,boolean otherFlag) {
		
		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		List<Service> serviceList = new ArrayList<Service>();
		
		List<Service> totalServicesList = new ArrayList<Service>();
		if(serviceId != 0){
			logger.log(Level.SEVERE, "Condition 1");
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId)
					.filter("contractCount", contract.getCount())
					 .filter("status", Service.SERVICESTATUSCOMPLETED).list();
			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId).filter("count", serviceId)
					.filter("contractCount", contract.getCount()).list();
		}else if(fromDate != null && toDate != null &&(branch == null || branch.equals(""))){
			logger.log(Level.SEVERE, "Condition 2");
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
						.filter("serviceDate >=",fromDate)
						.filter("serviceDate <",toDate)
						.filter("contractCount", contract.getCount())
						.filter("status", Service.SERVICESTATUSCOMPLETED).list();
			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=",fromDate)
					.filter("serviceDate <",toDate)
					.filter("contractCount", contract.getCount()).list();
		}else if(fromDate != null && toDate != null &&(branch != null || !branch.equals(""))){
			logger.log(Level.SEVERE, "Condition 3");
			List<Service> allServiceList = new ArrayList<Service>();
			allServiceList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("serviceDate >=",fromDate)
					.filter("serviceDate <",toDate)
					.filter("contractCount", contract.getCount()).list();
			
			if(allServiceList.size()>0) {
				for(Service s:allServiceList) {
					if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)&&s.getServiceBranch().equals(branch))
						serviceList.add(s);
					if(s.getServiceBranch().equals(branch))
						totalServicesList.add(s);
				}
			}
					
//			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceDate >=",fromDate)
//					.filter("serviceDate <",toDate)
//					.filter("contractCount", contract.getCount())
//					.filter("serviceBranch", branch)
//					.filter("status", Service.SERVICESTATUSCOMPLETED).list();
//			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceDate >=",fromDate)
//					.filter("serviceDate <",toDate)
//					.filter("contractCount", contract.getCount())
//					.filter("serviceBranch", branch).list();
		}else if(fromDate == null && toDate == null && branch != null && !branch.equals("")){
			logger.log(Level.SEVERE, "Condition 4");
			
			List<Service> allServiceList = new ArrayList<Service>();
			allServiceList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount", contract.getCount()).list();
			
			if(allServiceList.size()>0) {
				for(Service s:allServiceList) {
					if(s.getStatus().equals(Service.SERVICESTATUSCOMPLETED)&&s.getServiceBranch().equals(branch))
						serviceList.add(s);
					if(s.getServiceBranch().equals(branch))
						totalServicesList.add(s);
				}
			}
//			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceBranch", branch)
//					.filter("contractCount", contract.getCount())
//				    .filter("status", Service.SERVICESTATUSCOMPLETED).list();
//			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
//					.filter("serviceBranch", branch)
//					.filter("contractCount", contract.getCount()).list();
		}else {
			logger.log(Level.SEVERE, "Condition 5");
			serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount", contract.getCount())
				    .filter("status", Service.SERVICESTATUSCOMPLETED).list();
			totalServicesList=ofy().load().type(Service.class).filter("companyId", companyId)
					.filter("contractCount", contract.getCount()).list();
		}
		logger.log(Level.SEVERE, "serviceListcount="+serviceList.size()+" totalServicesList="+totalServicesList.size());
		/**
		 * @author Anil , Date : 04-03-2020
		 * Sending SR copy date wise
		 * right if user download bulk SR copy it generated randomly
		 */
		Comparator<Service> servDateComp=new Comparator<Service>() {
			@Override
			public int compare(Service arg0, Service arg1) {
				return arg0.getServiceDate().compareTo(arg1.getServiceDate());
			}
		};
		Collections.sort(serviceList, servDateComp);
		
		
		/**
		 * @author Anil, Date : 13-03-2020
		 * Changed Map to LinkedHashMap, in order to preserve the order of insertion
		 */
		LinkedHashMap<String , ArrayList<Service>> serviceMap = new LinkedHashMap<String , ArrayList<Service>>();
		ArrayList<Service> serList = new ArrayList<Service>();
		int contractId = 0;
		if(serviceList !=  null){
			for(Service ser : serviceList){
				contractId = ser.getContractCount();
				/**
				 * @author Anil
				 * @since 08-07-2020
				 * If sr copy is not generated then set  service id as service number
				 * @author Anil
				 * @since 20-08-2020
				 * revoking last changes of sendin sr copy whose sr no. not generated
				 * on the ask of vaishnavi and NBHC Team
				 */
				
				if(ser.getSrCopyNumber()==null||ser.getSrCopyNumber().equals("")){
//					ser.setSrCopyNumber(ser.getCount()+"");
					continue;
				}
				
				if(serviceMap.containsKey(ser.getSrCopyNumber())){
					serList = serviceMap.get(ser.getSrCopyNumber());
					serList.add(ser);
					serviceMap.put(ser.getSrCopyNumber(), serList);
				}else{
					serList = new ArrayList<Service>();
					serList.add(ser);
					serviceMap.put(ser.getSrCopyNumber(), serList);
				}
				
			}
		}
		
	
		ByteArrayOutputStream pdfstream = new ByteArrayOutputStream();
	
		logger.log(Level.SEVERE,"Calling CustomerServiceRecordPdf");
				
		Calendar cal = Calendar.getInstance();
		cal.setTime(toDate);
		cal.add(Calendar.DATE, -1);
		Date orgToDate = cal.getTime();
		logger.log(Level.SEVERE,"before calling constructor fromDate1="+fromDate+" toDate1= "+orgToDate);
		
		CustomerServiceRecordPdf customerServiceRecordpdf = new CustomerServiceRecordPdf(fromDate,orgToDate,branch,totalServicesList,reportName,reportFormat,completedFlag,cancelledFlag,otherFlag);
			
		if(reportName.equalsIgnoreCase("summary")) {
			if(reportFormat.equalsIgnoreCase("pdf")) {

				logger.log(Level.SEVERE,"Inside PDf");
				
				customerServiceRecordpdf.document = new Document();
				customerServiceRecordpdf.document.setPageSize(PageSize.A4.rotate());
				try {
					PdfWriter.getInstance(customerServiceRecordpdf.document, pdfstream);
				} catch (DocumentException e) {
					e.printStackTrace();
				}

				customerServiceRecordpdf.document.open();
				customerServiceRecordpdf.loadMultipleSRCopies(serviceList);	
				customerServiceRecordpdf.document.close();					
			
			}else {
				logger.log(Level.SEVERE,"Inside excel");
				XSSFWorkbook workbook=new XSSFWorkbook();
				try {
					customerServiceRecordpdf.loadMultipleSRCopies(serviceList,workbook);			
			    	workbook.write(pdfstream);
					logger.log(Level.SEVERE,"pdfstream created");
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
						
			
			}
		}else if(reportName.equalsIgnoreCase("checklist")) {
			logger.log(Level.SEVERE,"Inside checklist PDf");
			
			customerServiceRecordpdf.document = new Document();
			customerServiceRecordpdf.document.setPageSize(PageSize.A4.rotate());
			try {
				PdfWriter.getInstance(customerServiceRecordpdf.document, pdfstream);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			customerServiceRecordpdf.document.open();
			customerServiceRecordpdf.loadChecklistReport(totalServicesList);	
			customerServiceRecordpdf.document.close();
		}
		
		
		/**
		 * @author Anil , Date : 05-03-2020
		 * Right now Pestinct is send in all mail for all client
		 * it should be only for NBHC
		 */
		String compName="SR - ";
		if(company!=null&&company.getBusinessUnitName()!=null){
			if(company.getBusinessUnitName().contains("NBHC")){
				compName="Pestinct SR - ";
			}
		}
		String mailSub = compName+contract.getCount();//contractId;
		

		String mailMsg = "Dear Sir/Ma'am ,"+"\n"+"\n"+"\n"+ " <BR>"+"Please find the service record copies.";

		ArrayList<String> toEmailList = new ArrayList<String>();

		toEmailList.addAll(emailIdList);
		
		Date d=new Date();
		
		if(reportFormat.equalsIgnoreCase("pdf")) {
			String filename="";
			if(reportName.equals("checklist"))
				filename="Checklist Report "+d+".pdf";
			else
				filename="SR Copies "+d+".pdf";
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailList, null, null, mailSub, mailMsg, "text/html",pdfstream,filename,"application/pdf",company.getDisplayNameForCompanyEmailId());
				logger.log(Level.SEVERE,"attached pdf");
			}else{
				Email email = new Email();
				email.sendMailWithGmail(company.getEmail(), toEmailList, null, null, mailSub, mailMsg, "text/html",pdfstream,filename,"application/pdf");
			}
		
		}else {
			String filename="SR Copies "+d+".xlsx";
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				sdEmail.sendMailWithSendGrid(company.getEmail(), toEmailList, null, null, mailSub, mailMsg, "text/html",pdfstream,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",company.getDisplayNameForCompanyEmailId());
				logger.log(Level.SEVERE,"attached xlsx");
			}else{
				Email email = new Email();
				email.sendMailWithGmail(company.getEmail(), toEmailList, null, null, mailSub, mailMsg, "text/html",pdfstream,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}			
		}
	}

	
	@Override
	public String deleteStaticDataFromHeapMemory() {
		int counter=0;
		
		if(CsvWriter.adhocdownloadList!=null&&CsvWriter.adhocdownloadList.size()!=0){
			CsvWriter.adhocdownloadList.clear();
			counter++;
		}
		if(CsvWriter.advanced!=null&&CsvWriter.advanced.size()!=0){
			CsvWriter.advanced.clear();
			counter++;
		}
		if(CsvWriter.amcAssuredRevenueReportList!=null&&CsvWriter.amcAssuredRevenueReportList.size()!=0){
			CsvWriter.amcAssuredRevenueReportList.clear();
			counter++;
		}
		if(CsvWriter.amcAssuredRevenueReportList!=null&&CsvWriter.amcAssuredRevenueReportList.size()!=0){
			CsvWriter.amcAssuredRevenueReportList.clear();
			counter++;
		}
		if(CsvWriter.approvalList!=null&&CsvWriter.approvalList.size()!=0){
			CsvWriter.approvalList.clear();
			counter++;
		}
		if(CsvWriter.assessmentReportList!=null&&CsvWriter.assessmentReportList.size()!=0){
			CsvWriter.assessmentReportList.clear();
			counter++;
		}
		if(CsvWriter.assetallocationList!=null&&CsvWriter.assetallocationList.size()!=0){
			CsvWriter.assetallocationList.clear();
			counter++;
		}
		
		if(CsvWriter.attendanceErrorList!=null&&CsvWriter.attendanceErrorList.size()!=0){
			CsvWriter.attendanceErrorList.clear();
			counter++;
		}
		
		if(CsvWriter.attendanceList!=null&&CsvWriter.attendanceList.size()!=0){
			CsvWriter.attendanceList.clear();
			counter++;
		}
		
		if(CsvWriter.attendanceReportList!=null&&CsvWriter.attendanceReportList.size()!=0){
			CsvWriter.attendanceReportList.clear();
			counter++;
		}
		
		
		if(CsvWriter.billedUnbilledList!=null&&CsvWriter.billedUnbilledList.size()!=0){
			CsvWriter.billedUnbilledList.clear();
			counter++;
		}
		
		if(CsvWriter.billingDetailType!=null&&CsvWriter.billingDetailType.size()!=0){
			CsvWriter.billingDetailType.clear();
			counter++;
		}
		
		if(CsvWriter.billprod!=null&&CsvWriter.billprod.size()!=0){
			CsvWriter.billprod.clear();
			counter++;
		}
		
		if(CsvWriter.bills!=null&&CsvWriter.bills.size()!=0){
			CsvWriter.bills.clear();
			counter++;
		}
		
		if(CsvWriter.cancelSummarylist!=null&&CsvWriter.cancelSummarylist.size()!=0){
			CsvWriter.cancelSummarylist.clear();
			counter++;
		}
		if(CsvWriter.clientsideasset!=null&&CsvWriter.clientsideasset.size()!=0){
			CsvWriter.clientsideasset.clear();
			counter++;
		}
		
		if(CsvWriter.closingStockReport!=null&&CsvWriter.closingStockReport.size()!=0){
			CsvWriter.closingStockReport.clear();
			counter++;
		}
		
		if(CsvWriter.cncList!=null&&CsvWriter.cncList.size()!=0){
			CsvWriter.cncList.clear();
			counter++;
		}
		
		if(CsvWriter.company!=null&&CsvWriter.company.size()!=0){
			CsvWriter.company.clear();
			counter++;
		}
		
		if(CsvWriter.companyAssetList!=null&&CsvWriter.companyAssetList.size()!=0){
			CsvWriter.companyAssetList.clear();
			counter++;
		}
		if(CsvWriter.companycontributionList!=null&&CsvWriter.companycontributionList.size()!=0){
			CsvWriter.companycontributionList.clear();
			counter++;
		}
		
		if(CsvWriter.complainList!=null&&CsvWriter.complainList.size()!=0){
			CsvWriter.complainList.clear();
			counter++;
		}
		
		if(CsvWriter.complainReportlist!=null&&CsvWriter.complainReportlist.size()!=0){
			CsvWriter.complainReportlist.clear();
			counter++;
		}
		
		if(CsvWriter.comppayment!=null&&CsvWriter.comppayment.size()!=0){
			CsvWriter.comppayment.clear();
			counter++;
		}
		if(CsvWriter.consumptionList!=null&&CsvWriter.consumptionList.size()!=0){
			CsvWriter.consumptionList.clear();
			counter++;
		}
		if(CsvWriter.contactpersontype!=null&&CsvWriter.contactpersontype.size()!=0){
			CsvWriter.contactpersontype.clear();
			counter++;
		}
		if(CsvWriter.contract!=null&&CsvWriter.contract.size()!=0){
			CsvWriter.contract.clear();
			counter++;
		}
		if(CsvWriter.contractRenewalList!=null&&CsvWriter.contractRenewalList.size()!=0){
			CsvWriter.contractRenewalList.clear();
			counter++;
		}
		if(CsvWriter.ctc!=null&&CsvWriter.ctc.size()!=0){
			CsvWriter.ctc.clear();
			counter++;
		}
		if(CsvWriter.ctcttemplateList!=null&&CsvWriter.ctcttemplateList.size()!=0){
			CsvWriter.ctcttemplateList.clear();
			counter++;
		}
		if(CsvWriter.customerBranchErrorList!=null&&CsvWriter.customerBranchErrorList.size()!=0){
			CsvWriter.customerBranchErrorList.clear();
			counter++;
		}
		if(CsvWriter.customerbranchlist!=null&&CsvWriter.customerbranchlist.size()!=0){
			CsvWriter.customerbranchlist.clear();
			counter++;
		}
		if(CsvWriter.customerCallLogList!=null&&CsvWriter.adhocdownloadList.size()!=0){
			CsvWriter.customerCallLogList.clear();
			counter++;
		}
		if(CsvWriter.customerHelpDeskList!=null&&CsvWriter.customerHelpDeskList.size()!=0){
			CsvWriter.customerHelpDeskList.clear();
			counter++;
		}
		if(CsvWriter.custStatusList!=null&&CsvWriter.custStatusList.size()!=0){
			CsvWriter.custStatusList.clear();
			counter++;
		}
		if(CsvWriter.custtable!=null&&CsvWriter.custtable.size()!=0){
			CsvWriter.custtable.clear();
			counter++;
		}
		
		if(CsvWriter.deliverynote!=null&&CsvWriter.deliverynote.size()!=0){
			CsvWriter.deliverynote.clear();
			counter++;
		}
		if(CsvWriter.dochistory!=null&&CsvWriter.dochistory.size()!=0){
			CsvWriter.dochistory.clear();
			counter++;
		}
		
		if(CsvWriter.empAddDetList!=null&&CsvWriter.empAddDetList.size()!=0){
			CsvWriter.empAddDetList.clear();
			counter++;
		}
		if(CsvWriter.empDetails!=null&&CsvWriter.empDetails.size()!=0){
			CsvWriter.empDetails.clear();
			counter++;
		}
		if(CsvWriter.employeeAssetErrorList!=null&&CsvWriter.employeeAssetErrorList.size()!=0){
			CsvWriter.employeeAssetErrorList.clear();
			counter++;
		}
		
		if(CsvWriter.employeeAssetList!=null&&CsvWriter.employeeAssetList.size()!=0){
			CsvWriter.employeeAssetList.clear();
			counter++;
		}
		
		if(CsvWriter.employeeLeaveList!=null&&CsvWriter.employeeLeaveList.size()!=0){
			CsvWriter.employeeLeaveList.clear();
			counter++;
		}
		
		if(CsvWriter.employeeOtList!=null&&CsvWriter.employeeOtList.size()!=0){
			CsvWriter.employeeOtList.clear();
			counter++;
		}
		
		if(CsvWriter.employees!=null&&CsvWriter.employees.size()!=0){
			CsvWriter.employees.clear();
			counter++;
		}
		
		if(CsvWriter.errorMinList!=null&&CsvWriter.errorMinList.size()!=0){
			CsvWriter.errorMinList.clear();
			counter++;
		}
		
		if(CsvWriter.expensecostlist!=null&&CsvWriter.expensecostlist.size()!=0){
			CsvWriter.expensecostlist.clear();
			counter++;
		}
		
		if(CsvWriter.expenseincome!=null&&CsvWriter.expenseincome.size()!=0){
			CsvWriter.expenseincome.clear();
			counter++;
		}
		
		if(CsvWriter.fumigationAusList!=null&&CsvWriter.fumigationAusList.size()!=0){
			CsvWriter.fumigationAusList.clear();
			counter++;
		}
		
		if(CsvWriter.fumigationList!=null&&CsvWriter.fumigationList.size()!=0){
			CsvWriter.fumigationList.clear();
			counter++;
		}
		
		if(CsvWriter.grn!=null&&CsvWriter.grn.size()!=0){
			CsvWriter.grn.clear();
			counter++;
		}
		if(CsvWriter.hrprojectList!=null&&CsvWriter.hrprojectList.size()!=0){
			CsvWriter.hrprojectList.clear();
			counter++;
		}
		
		if(CsvWriter.inspList!=null&&CsvWriter.inspList.size()!=0){
			CsvWriter.inspList.clear();
			counter++;
		}
		if(CsvWriter.interfacetally!=null&&CsvWriter.interfacetally.size()!=0){
			CsvWriter.interfacetally.clear();
			counter++;
		}
		if(CsvWriter.intertype!=null&&CsvWriter.intertype.size()!=0){
			CsvWriter.intertype.clear();
			counter++;
		}
		if(CsvWriter.invListArray!=null&&CsvWriter.invListArray.size()!=0){
			CsvWriter.invListArray.clear();
			counter++;
		}
		if(CsvWriter.invoicelis!=null&&CsvWriter.invoicelis.size()!=0){
			CsvWriter.invoicelis.clear();
			counter++;
		}
		if(CsvWriter.itemproduct!=null&&CsvWriter.itemproduct.size()!=0){
			CsvWriter.itemproduct.clear();
			counter++;
		}
		
		if(CsvWriter.labourcostlist!=null&&CsvWriter.labourcostlist.size()!=0){
			CsvWriter.labourcostlist.clear();
			counter++;
		}
		if(CsvWriter.leadtable!=null&&CsvWriter.leadtable.size()!=0){
			CsvWriter.leadtable.clear();
			counter++;
		}
		if(CsvWriter.leaveapplication!=null&&CsvWriter.leaveapplication.size()!=0){
			CsvWriter.leaveapplication.clear();
			counter++;
		}
		if(CsvWriter.leavebalance!=null&&CsvWriter.leavebalance.size()!=0){
			CsvWriter.leavebalance.clear();
			counter++;
		}
		if(CsvWriter.leaveCalendar!=null&&CsvWriter.leaveCalendar.size()!=0){
			CsvWriter.leaveCalendar.clear();
			counter++;
		}
		if(CsvWriter.liaisonlist!=null&&CsvWriter.liaisonlist.size()!=0){
			CsvWriter.liaisonlist.clear();
			counter++;
		}
		if(CsvWriter.logindetails!=null&&CsvWriter.logindetails.size()!=0){
			CsvWriter.logindetails.clear();
			counter++;
		}
		if(CsvWriter.loi!=null&&CsvWriter.loi.size()!=0){
			CsvWriter.loi.clear();
			counter++;
		}
		
		if(CsvWriter.materialInfoList!=null&&CsvWriter.materialInfoList.size()!=0){
			CsvWriter.materialInfoList.clear();
			counter++;
		}
		
		if(CsvWriter.min!=null&&CsvWriter.min.size()!=0){
			CsvWriter.min.clear();
			counter++;
		}
		if(CsvWriter.mmn!=null&&CsvWriter.mmn.size()!=0){
			CsvWriter.mmn.clear();
			counter++;
		}
		
		if(CsvWriter.mrn!=null&&CsvWriter.mrn.size()!=0){
			CsvWriter.mrn.clear();
			counter++;
		}
		if(CsvWriter.multipleExp!=null&&CsvWriter.multipleExp.size()!=0){
			CsvWriter.multipleExp.clear();
			counter++;
		}
		
		if(CsvWriter.newTallyInterfaceList!=null&&CsvWriter.newTallyInterfaceList.size()!=0){
			CsvWriter.newTallyInterfaceList.clear();
			counter++;
		}
		if(CsvWriter.paymentlis!=null&&CsvWriter.paymentlis.size()!=0){
			CsvWriter.paymentlis.clear();
			counter++;
		}
		if(CsvWriter.paySlipList!=null&&CsvWriter.paySlipList.size()!=0){
			CsvWriter.paySlipList.clear();
			counter++;
		}
		if(CsvWriter.pettycash!=null&&CsvWriter.pettycash.size()!=0){
			CsvWriter.pettycash.clear();
			counter++;
		}
		if(CsvWriter.pettycashTransactionList!=null&&CsvWriter.pettycashTransactionList.size()!=0){
			CsvWriter.pettycashTransactionList.clear();
			counter++;
		}
		if(CsvWriter.physicalInvList!=null&&CsvWriter.physicalInvList.size()!=0){
			CsvWriter.physicalInvList.clear();
			counter++;
		}
		if(CsvWriter.pitl!=null&&CsvWriter.pitl.size()!=0){
			CsvWriter.pitl.clear();
			counter++;
		}
		if(CsvWriter.piv!=null&&CsvWriter.piv.size()!=0){
			CsvWriter.piv.clear();
			counter++;
		}
		if(CsvWriter.priceList!=null&&CsvWriter.priceList.size()!=0){
			CsvWriter.priceList.clear();
			counter++;
		}
		if(CsvWriter.prodGrpList!=null&&CsvWriter.prodGrpList.size()!=0){
			CsvWriter.prodGrpList.clear();
			counter++;
		}
		if(CsvWriter.profitlosssummarylist!=null&&CsvWriter.profitlosssummarylist.size()!=0){
			CsvWriter.profitlosssummarylist.clear();
			counter++;
		}
		if(CsvWriter.projectallocationList!=null&&CsvWriter.projectallocationList.size()!=0){
			CsvWriter.projectallocationList.clear();
			counter++;
		}
		if(CsvWriter.PO!=null&&CsvWriter.PO.size()!=0){
			CsvWriter.PO.clear();
			counter++;
		}
		if(CsvWriter.PR!=null&&CsvWriter.PR.size()!=0){
			CsvWriter.PR.clear();
			counter++;
		}
		
		if(CsvWriter.quotaion!=null&&CsvWriter.quotaion.size()!=0){
			CsvWriter.quotaion.clear();
			counter++;
		}
		
		if(CsvWriter.registrationLis!=null&&CsvWriter.registrationLis.size()!=0){
			CsvWriter.registrationLis.clear();
			counter++;
		}
		
		if(CsvWriter.renewalContractDt!=null&&CsvWriter.renewalContractDt.size()!=0){
			CsvWriter.renewalContractDt.clear();
			counter++;
		}
		
		if(CsvWriter.requestforquolist!=null&&CsvWriter.requestforquolist.size()!=0){
			CsvWriter.requestforquolist.clear();
			counter++;
		}
		if(CsvWriter.revanueList!=null&&CsvWriter.revanueList.size()!=0){
			CsvWriter.revanueList.clear();
			counter++;
		}
		if(CsvWriter.roleDefList!=null&&CsvWriter.roleDefList.size()!=0){
			CsvWriter.roleDefList.clear();
			counter++;
		}
		
		if(CsvWriter.salesdeplist!=null&&CsvWriter.salesdeplist.size()!=0){
			CsvWriter.salesdeplist.clear();
			counter++;
		}
		
		if(CsvWriter.salesOrders!=null&&CsvWriter.salesOrders.size()!=0){
			CsvWriter.salesOrders.clear();
			counter++;
		}
		if(CsvWriter.salespersoncontractlist!=null&&CsvWriter.salespersoncontractlist.size()!=0){
			CsvWriter.salespersoncontractlist.clear();
			counter++;
		}
		if(CsvWriter.salespersoninfolist!=null&&CsvWriter.salespersoninfolist.size()!=0){
			CsvWriter.salespersoninfolist.clear();
			counter++;
		}
		if(CsvWriter.salespersonleadlist!=null&&CsvWriter.salespersonleadlist.size()!=0){
			CsvWriter.salespersonleadlist.clear();
			counter++;
		}
		if(CsvWriter.salespersonquotationlist!=null&&CsvWriter.salespersonquotationlist.size()!=0){
			CsvWriter.salespersonquotationlist.clear();
			counter++;
		}
		if(CsvWriter.salespersonSummarylist!=null&&CsvWriter.salespersonSummarylist.size()!=0){
			CsvWriter.salespersonSummarylist.clear();
			counter++;
		}
		if(CsvWriter.salespersontargetactuallist!=null&&CsvWriter.salespersontargetactuallist.size()!=0){
			CsvWriter.salespersontargetactuallist.clear();
			counter++;
		}
		if(CsvWriter.salesPersonTargetList!=null&&CsvWriter.salesPersonTargetList.size()!=0){
			CsvWriter.salesPersonTargetList.clear();
			counter++;
		}
		if(CsvWriter.salesquotation!=null&&CsvWriter.salesquotation.size()!=0){
			CsvWriter.salesquotation.clear();
			counter++;
		}
		if(CsvWriter.salesRegList!=null&&!CsvWriter.salesRegList.equals("")){
			CsvWriter.salesRegList="";
			counter++;
		}
		if(CsvWriter.scheduledServiceList!=null&&CsvWriter.scheduledServiceList.size()!=0){
			CsvWriter.scheduledServiceList.clear();
			counter++;
		}
		
		if(CsvWriter.serialNumberDeviation!=null&&CsvWriter.serialNumberDeviation.size()!=0){
			CsvWriter.serialNumberDeviation.clear();
			counter++;
		}if(CsvWriter.serInvoiceDt!=null&&CsvWriter.serInvoiceDt.size()!=0){
			CsvWriter.serInvoiceDt.clear();
			counter++;
		}if(CsvWriter.servicedeplist!=null&&CsvWriter.servicedeplist.size()!=0){
			CsvWriter.servicedeplist.clear();
			counter++;
		}if(CsvWriter.servicePoList!=null&&CsvWriter.servicePoList.size()!=0){
			CsvWriter.servicePoList.clear();
			counter++;
		}if(CsvWriter.serviceproduct!=null&&CsvWriter.serviceproduct.size()!=0){
			CsvWriter.serviceproduct.clear();
			counter++;
		}if(CsvWriter.services!=null&&CsvWriter.services.size()!=0){
			CsvWriter.services.clear();
			counter++;
		}if(CsvWriter.shifts!=null&&CsvWriter.shifts.size()!=0){
			CsvWriter.shifts.clear();
			counter++;
		}
		if(CsvWriter.smshistory!=null&&CsvWriter.smshistory.size()!=0){
			CsvWriter.smshistory.clear();
			counter++;
		}
		if(CsvWriter.srContList!=null&&CsvWriter.srContList.size()!=0){
			CsvWriter.srContList.clear();
			counter++;
		}
		if(CsvWriter.srCustList!=null&&CsvWriter.srCustList.size()!=0){
			CsvWriter.srCustList.clear();
			counter++;
		}
		if(CsvWriter.srCustPayList!=null&&CsvWriter.srCustPayList.size()!=0){
			CsvWriter.srCustPayList.clear();
			counter++;
		}
		if(CsvWriter.srSoList!=null&&CsvWriter.srSoList.size()!=0){
			CsvWriter.srSoList.clear();
			counter++;
		}
		if(CsvWriter.statoturyReportMap!=null&&CsvWriter.statoturyReportMap.size()!=0){
			CsvWriter.statoturyReportMap.clear();
			counter++;
		}
		if(CsvWriter.statuteryReport!=null&&CsvWriter.statuteryReport.size()!=0){
			CsvWriter.statuteryReport.clear();
			counter++;
		}if(CsvWriter.storageBinList!=null&&CsvWriter.storageBinList.size()!=0){
			CsvWriter.storageBinList.clear();
			counter++;
		}if(CsvWriter.storageLocList!=null&&CsvWriter.storageLocList.size()!=0){
			CsvWriter.storageLocList.clear();
			counter++;
		}
		
		if(CsvWriter.technicianInfolist!=null&&CsvWriter.technicianInfolist.size()!=0){
			CsvWriter.technicianInfolist.clear();
			counter++;
		}
		if(CsvWriter.technicianList!=null&&CsvWriter.technicianList.size()!=0){
			CsvWriter.technicianList.clear();
			counter++;
		}
		if(CsvWriter.technicianSerchInfolist!=null&&CsvWriter.technicianSerchInfolist.size()!=0){
			CsvWriter.technicianSerchInfolist.clear();
			counter++;
		}
		if(CsvWriter.technicianSummaryInfolist!=null&&CsvWriter.technicianSummaryInfolist.size()!=0){
			CsvWriter.technicianSummaryInfolist.clear();
			counter++;
		}
		if(CsvWriter.timeReportcsv!=null&&CsvWriter.timeReportcsv.size()!=0){
			CsvWriter.timeReportcsv.clear();
			counter++;
		}
		if(CsvWriter.tool!=null&&CsvWriter.tool.size()!=0){
			CsvWriter.tool.clear();
			counter++;
		}
		if(CsvWriter.toolSet!=null&&CsvWriter.toolSet.size()!=0){
			CsvWriter.toolSet.clear();
			counter++;
		}
		if(CsvWriter.unallocatedemployeeList!=null&&CsvWriter.unallocatedemployeeList.size()!=0){
			CsvWriter.unallocatedemployeeList.clear();
			counter++;
		}
		if(CsvWriter.userlist!=null&&CsvWriter.userlist.size()!=0){
			CsvWriter.userlist.clear();
			counter++;
		}
		if(CsvWriter.vendorInvoiceList!=null&&CsvWriter.vendorInvoiceList.size()!=0){
			CsvWriter.vendorInvoiceList.clear();
			counter++;
		}if(CsvWriter.vendorpayment!=null&&CsvWriter.vendorpayment.size()!=0){
			CsvWriter.vendorpayment.clear();
			counter++;
		}
		if(CsvWriter.vendorpricelist!=null&&CsvWriter.vendorpricelist.size()!=0){
			CsvWriter.vendorpricelist.clear();
			counter++;
		}
		if(CsvWriter.vendors!=null&&CsvWriter.vendors.size()!=0){
			CsvWriter.vendors.clear();
			counter++;
		}
		
		if(CsvWriter.voluntrypfhistoryList!=null&&CsvWriter.voluntrypfhistoryList.size()!=0){
			CsvWriter.voluntrypfhistoryList.clear();
			counter++;
		}
		if(CsvWriter.voluntrypfList!=null&&CsvWriter.voluntrypfList.size()!=0){
			CsvWriter.voluntrypfList.clear();
			counter++;
		}
		if(CsvWriter.warehouseList!=null&&CsvWriter.warehouseList.size()!=0){
			CsvWriter.warehouseList.clear();
			counter++;
		}
		if(CsvWriter.workOrderDtls!=null&&CsvWriter.workOrderDtls.size()!=0){
			CsvWriter.workOrderDtls.clear();
			counter++;
		}
		
		
		logger.log(Level.SEVERE,"No. of static list cleared : "+counter);
		logger.log(Level.SEVERE,"Total Memory : "+Runtime.getRuntime().totalMemory());
		logger.log(Level.SEVERE,"Free Memory : "+Runtime.getRuntime().freeMemory());
		logger.log(Level.SEVERE,"Max Memory : "+Runtime.getRuntime().maxMemory());
		Runtime.getRuntime().gc();
		
		String msg="Total Memory : "+((Runtime.getRuntime().totalMemory()/1024)/1024)+"\n"+
				"Free Memory : "+((Runtime.getRuntime().freeMemory()/1024)/1024)+"\n"+
				"Max Memory : "+((Runtime.getRuntime().maxMemory()/1024)/1024)+"\n"+
				"No. of static list cleared : "+counter;
				
		
		return msg;
	}

	@Override
	public String checkHeapMemoryStatus() {
		try{
			String GAE_MEMORY_MB = System.getenv("GAE_MEMORY_MB");
			logger.log(Level.SEVERE,"GAE_MEMORY_MB : "+GAE_MEMORY_MB);
			
			String HEAP_SIZE_RATIO = System.getenv("HEAP_SIZE_RATIO");
			logger.log(Level.SEVERE,"HEAP_SIZE_RATIO : "+HEAP_SIZE_RATIO);
			
			String HEAP_SIZE_MB = System.getenv("HEAP_SIZE_MB");
			logger.log(Level.SEVERE,"HEAP_SIZE_MB : "+HEAP_SIZE_MB);
			
			String JAVA_HEAP_OPTS = System.getenv("JAVA_HEAP_OPTS");
			logger.log(Level.SEVERE,"JAVA_HEAP_OPTS : "+JAVA_HEAP_OPTS);
			
			String JAVA_GC_OPTS = System.getenv("JAVA_GC_OPTS");
			logger.log(Level.SEVERE,"JAVA_GC_OPTS : "+JAVA_GC_OPTS);
		}catch(Exception e){
			e.printStackTrace();
		}
	
		logger.log(Level.SEVERE,"Total Memory : "+Runtime.getRuntime().totalMemory());
		logger.log(Level.SEVERE,"Free Memory : "+Runtime.getRuntime().freeMemory());
		logger.log(Level.SEVERE,"Max Memory : "+Runtime.getRuntime().maxMemory());
		Runtime.getRuntime().gc();
		String msg="Total Memory : "+((Runtime.getRuntime().totalMemory()/1024)/1024)+"\n"+
				"Free Memory : "+((Runtime.getRuntime().freeMemory()/1024)/1024)+"\n"+
				"Max Memory : "+((Runtime.getRuntime().maxMemory()/1024)/1024)+"\n";
		return msg;
	}

	@Override
	public String updateWMSServicesFlag(long companyId) {
		NumberGeneration numGen = ofy().load().type(NumberGeneration.class)
						.filter("processName", "Service").filter("companyId", companyId).first().now();
		if(numGen!=null){
			long number = numGen.getNumber();
			
			Queue queue = QueueFactory.getQueue("UpdateServices-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "Update WMS Flag Services")
					.param("companyId", companyId+"").param("Number", number+""));
			logger.log(Level.SEVERE, "Updating..... WMS Flag Servicess !!!"+number);
			return "Update process started.";
		}
		else{
			return "Number Generation loading failed";
		}
		
	}

	
	@Override
	public String updateStock(DeliveryNote model) {
		/**
		 * Date : 18-01-2017 By Anil
		 * New Stock Updation and Transaction creation Code
		 */
		logger.log(Level.SEVERE, "in update stock");
		
		try {
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(DeliveryLineItems product:model.getDeliveryItems()){
			InventoryTransactionLineItem item=new InventoryTransactionLineItem();
			item.setCompanyId(model.getCompanyId());
			item.setBranch(model.getBranch());
			item.setDocumentType(AppConstants.DELIVERYNOTE);
			item.setDocumentId(model.getCount());
			item.setDocumentDate(model.getCreationDate());
			item.setDocumnetTitle("");
			item.setProdId(product.getProdId());
			item.setUpdateQty(product.getQuantity());
			item.setOperation(AppConstants.SUBTRACT);
			item.setWarehouseName(product.getWareHouseName());
			item.setStorageLocation(product.getStorageLocName());
			item.setStorageBin(product.getStorageBinName());
			/**
			 * nidhi
			 * 27-08-2018
			 */
			item.setProSerialNoDetails(product.getProSerialNoDetails());
			/**
			 * end
			 */
			itemList.add(item);
		}
		UpdateStock.setProductInventory(itemList);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		/**
		 * End
		 */
		
		
		/**
		 * @author Vijay Chougule Date 21-09-2020
		 * Des :- Deliver Note SMS
		 */
		logger.log(Level.SEVERE, " Sending SMS");
		model.sendSMStoClient();
		
		
		return null;
	}
	
	/**
	 * @author Vijay Chougule Date :- 08-09-2020
	 * Des :- Calling Payment Gateway API with necessary Json Data
	 * and sending paymentGatewayuniqueId to customer
	 */
	@Override
	public String CallPaymentGatewayAPI(int documentId, String documentName,String AppURL,String appId,long companyId,int customerId,String pdfURLLink) {

		logger.log(Level.SEVERE, "This is where i will call Payment Gateway URL");
		final String paymentGatewayAPI = AppConstants.PAYMENETGATEWAYAPI;
		
		logger.log(Level.SEVERE, "paymentGatewayAPI ==" +paymentGatewayAPI);

		String data = "";
		

		Company company = ofy().load().type(Company.class).filter("companyId", companyId)
							.filter("accessUrl", "my").first().now();
		logger.log(Level.SEVERE, "company" + company);
		
		if(company == null){
			return "company does not exist kindly check URL"+"@Error";
		}
//		else{
//			if(company.getMerchantId()==null || company.getMerchantId().equals("")){
//				return "Payment Gateway id does not exist in company master"+"@Error";
//			}
//		}

		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId)
		.filter("count",customerId).first().now();
		logger.log(Level.SEVERE, "customer" + customer);
		if(customer ==null){
			return "Customer does not exist kindly check"+"@Error";
		}
		
		/**
		 * @author Vijay Date 16-03-2021 validating Internet Number range defined or not 
		 */
		String error ="";
		error = validateInternetNumberRange(companyId,null);
		if(!error.trim().equals("") && error.trim().length()>0){
			return error +"@Error";
		}
		SuperModel model = null;

		logger.log(Level.SEVERE, "11111");
		String jsonData ="";
//		SalesLineItem lineItem = new SalesLineItem();
		if(documentName.trim().equals(AppConstants.LEAD)){
			Lead leadEntity = ofy().load().type(Lead.class).filter("count", documentId).filter("companyId", companyId).first().now();
			logger.log(Level.SEVERE, "leadEntity =="+leadEntity);
			model = leadEntity;
			jsonData = createJsonData(model,AppConstants.LEAD, AppURL, appId, company, customer,pdfURLLink,"");
		}
		else if(documentName.trim().equals(AppConstants.QUOTATION)) {
			Quotation quotationEntity = ofy().load().type(Quotation.class).filter("count", documentId).filter("companyId", companyId).first().now();
			logger.log(Level.SEVERE, "quotationEntity =="+quotationEntity);
			model = quotationEntity;
			jsonData = createJsonData(model,AppConstants.QUOTATION, AppURL, appId, company, customer,pdfURLLink,"");
		}
		else if(documentName.trim().equals(AppConstants.CONTRACT)) {
			Contract contractEntity = ofy().load().type(Contract.class).filter("count", documentId).filter("companyId", companyId).first().now();
			logger.log(Level.SEVERE, "Contract =="+contractEntity);
			model = contractEntity;
			jsonData = createJsonData(model,AppConstants.CONTRACT, AppURL, appId, company, customer,pdfURLLink,"");
		}
		logger.log(Level.SEVERE, "11111"+jsonData);

		URL url = null;
		try {
			url = new URL(paymentGatewayAPI);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
//			writer.write(createLeadJSONObject(leadEntity,AppURL,appId,company,customer));
			writer.write(jsonData);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data::::::::::" + data);

		
		org.json.JSONObject jsonobj = null;
		
		String paymentGatewayuniqueId = "";
		
		try {
			jsonobj = new org.json.JSONObject(data);
			
	
			paymentGatewayuniqueId = jsonobj.optString("id");
	
			logger.log(Level.SEVERE, "paymentGatewayuniqueId"+ paymentGatewayuniqueId);
	
			if (!paymentGatewayuniqueId.equals("")) {
				if(documentName.trim().equals(AppConstants.LEAD)){
					Lead leadEntity = (Lead) model;
					leadEntity.setPaymentGatewayUniqueId(paymentGatewayuniqueId);
					ofy().save().entity(leadEntity);
					logger.log(Level.SEVERE,"lead entity updated succesfully with paymentGatewayuniqueId");
			
				}
				
			}

			String smvalidationsmsg = sendsmstoClient(customer,companyId,paymentGatewayuniqueId,customer.getFullname());
			if(!smvalidationsmsg.equals("")){
				return smvalidationsmsg +"@Error";
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE,"parsing Error!!");
		}
		
//		return "Sign Up Sucessfully";
		return paymentGatewayuniqueId;
	
		
	}

	
	public String createJsonData(SuperModel model, String documentName,String appURL, String appId, Company company, Customer customer, String pdfURLLink, String landingPageText) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
//		String finalUrl = appURL+"contractinvoicepayment?contractDetails=";
		String finalUrl = appURL+"contractinvoicepayment?";
		String paymentterms = "100% Advance";
		
		String field1="", field2="", field3="";
		

		if(documentName.trim().equals(AppConstants.LEAD)){
			Lead leadEntity = (Lead) model;
			
			JSONObject jObj = new JSONObject();
//			double netpayable = (leadEntity.getNetpayable()*100);
			double netpayable = leadEntity.getNetpayable();


			String transactionType = "Pay & Confirm Now";
			String transactionType2 = "Confirm Now & Pay Later";

			
			jObj = getJsonData(documentName,appId,company.getMerchantId(),customer.getEmail(),company.getBusinessUnitName(),company.getCurrency(),finalUrl,
					AppConstants.LEAD,leadEntity.getCount(),leadEntity.getPersonInfo().getFullName(),leadEntity.getCreationDate(),
					customer.getCellNumber1(),customer.getSecondaryAdress().getCompleteAddress(),field1,field2,field3,leadEntity.getLeadProducts(),
					leadEntity.getDescription(),paymentterms,leadEntity.getTotalAmount(),leadEntity.getDiscountAmt(),leadEntity.getFinalTotalAmt(),
					leadEntity.getProductTaxes(),netpayable,company.getWebsiteServiceTermsAndConditions(),transactionType,transactionType2,company.getCompanyId(),null,null,company.getEmail(),landingPageText,
					company.getPaymentGatewayId(),company.getPaymentGatewayKey());
			
			
			String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Payment Gateway Lead Data :::" + jsonString);
			return jsonString;
			
		
		
		}
		else if(documentName.trim().equals(AppConstants.QUOTATION)) {
			Quotation quotationEntity = (Quotation) model;
			JSONObject jObj = new JSONObject();
			
			double netpayable = quotationEntity.getNetpayable();

			String transactionType = "Pay & Confirm Now";
			String transactionType2 = "Confirm Now & Pay Later";
			
			if(quotationEntity.getPayTerms()!=null && !quotationEntity.getPayTerms().equals("")){
				paymentterms = quotationEntity.getPayTerms();
			}
			
			jObj = getJsonData(documentName,appId,company.getMerchantId(),customer.getEmail(),company.getBusinessUnitName(),company.getCurrency(),finalUrl,
					AppConstants.QUOTATION,quotationEntity.getCount(),quotationEntity.getCinfo().getFullName(),quotationEntity.getCreationDate(),
					customer.getCellNumber1(),customer.getSecondaryAdress().getCompleteAddress(),field1,field2,field3,quotationEntity.getItems(),
					quotationEntity.getDescription(),paymentterms,quotationEntity.getTotalAmount(),quotationEntity.getDiscountAmt(),quotationEntity.getFinalTotalAmt(),
					quotationEntity.getProductTaxes(),netpayable,company.getWebsiteServiceTermsAndConditions(),transactionType,transactionType2,company.getCompanyId(),quotationEntity.getValidUntill(),pdfURLLink,company.getEmail(),landingPageText
					,company.getPaymentGatewayId(),company.getPaymentGatewayKey());
			
			
			String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Payment Gateway QUOTATION Data :::" + jsonString);
			return jsonString;
			
		}
		else if(documentName.trim().equals(AppConstants.CONTRACT)) {
			
			Contract contractEntity = (Contract) model;
			JSONObject jObj = new JSONObject();
			
			double netpayable = contractEntity.getNetpayable();

			String transactionType = "Pay & Confirm Now";
			String transactionType2 = "Confirm Now & Pay Later";

			if(contractEntity.getPayTerms()!=null && !contractEntity.getPayTerms().equals("")){
				paymentterms = contractEntity.getPayTerms();
			}
			
			jObj = getJsonData(documentName,appId,company.getMerchantId(),customer.getEmail(),company.getBusinessUnitName(),company.getCurrency(),finalUrl,
					AppConstants.CONTRACT,contractEntity.getCount(),contractEntity.getCinfo().getFullName(),contractEntity.getCreationDate(),
					customer.getCellNumber1(),customer.getSecondaryAdress().getCompleteAddress(),field1,field2,field3,contractEntity.getItems(),
					contractEntity.getDescription(),paymentterms,contractEntity.getTotalAmount(),contractEntity.getDiscountAmt(),contractEntity.getFinalTotalAmt(),
					contractEntity.getProductTaxes(),netpayable,company.getWebsiteServiceTermsAndConditions(),transactionType,transactionType2,company.getCompanyId(),null,pdfURLLink,company.getEmail(),landingPageText
					,company.getPaymentGatewayId(),company.getPaymentGatewayKey());
			
			
			String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Payment Gateway CONTRACT Data :::" + jsonString);
			return jsonString;
			
		}
		
		else if(documentName.trim().equals(AppConstants.CONTRACT_RENEWAL)) {
			
			ContractRenewal contractRenewalEntity = (ContractRenewal) model;
			JSONObject jObj = new JSONObject();
			
			double netpayable = contractRenewalEntity.getNetPayable();

			String transactionType = "Pay & Confirm Now";
			String transactionType2 = "Confirm Now & Pay Later";
			
			ContractInvoicePayment coninvoicepayment = new ContractInvoicePayment();
			
			double totalAmount =0;
			for(SalesLineItem item : contractRenewalEntity.getItems()){
				totalAmount += item.getPrice();
				logger.log(Level.SEVERE, "item.getTotalAmount" + totalAmount);
				item.setTotalAmount(item.getPrice());
				logger.log(Level.SEVERE, "item.getStartDate() " + item.getStartDate());
				logger.log(Level.SEVERE, "item.getEndDate() " + item.getEndDate());

				if(item.getStartDate()==null && item.getEndDate()==null){
					item.setStartDate(DateUtility.getDateWithTimeZone("IST", new Date()));
					item.setEndDate(coninvoicepayment.getProductEndDate(item.getStartDate(),item.getDuration()));
				}
				
			}
			ContractInvoicePayment contractPayment = new ContractInvoicePayment();
			
			ArrayList<ProductOtherCharges> prodTaxeslist = new ArrayList<ProductOtherCharges>();
			prodTaxeslist = contractPayment.getproductTaxeslist(contractRenewalEntity.getItems());
			DecimalFormat decimalformat = new DecimalFormat();
			for(ProductOtherCharges othercharges : prodTaxeslist){
				double calcAmt=othercharges.getAssessableAmount()*othercharges.getChargePercent()/100;
				decimalformat.format(calcAmt);
				othercharges.setChargePayable(calcAmt);
				logger.log(Level.SEVERE, "othercharges.getAssessableAmount()" + othercharges.getAssessableAmount());
				logger.log(Level.SEVERE, "othercharges.getChargePercent()" + othercharges.getChargePercent());

				logger.log(Level.SEVERE, "calcAmt" + calcAmt);
			}
			
			logger.log(Level.SEVERE, "prodTaxeslist size " + prodTaxeslist.size());
			System.out.println("appId "+appId);
			System.out.println("company.getMerchantId() "+company.getMerchantId());
			System.out.println("customer.getEmail()"+customer.getEmail());
			System.out.println("finalUrl"+finalUrl);
			System.out.println("company.getCurrency()"+company.getCurrency());

			
			jObj = getJsonData(documentName,appId,company.getMerchantId(),customer.getEmail(),company.getBusinessUnitName(),company.getCurrency(),finalUrl,
					AppConstants.CONTRACT_RENEWAL,contractRenewalEntity.getContractId(),contractRenewalEntity.getCustomerName(),contractRenewalEntity.getDate(),
					customer.getCellNumber1(),customer.getSecondaryAdress().getCompleteAddress(),field1,field2,field3,contractRenewalEntity.getItems(),
					contractRenewalEntity.getDescription(),paymentterms,totalAmount,0,totalAmount,
					prodTaxeslist,netpayable,company.getWebsiteServiceTermsAndConditions(),transactionType,transactionType2,company.getCompanyId(),null,pdfURLLink,company.getEmail(),landingPageText
					,company.getPaymentGatewayId(),company.getPaymentGatewayKey());
			
			
			String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Payment Gateway CONTRACT_RENEWAL Data :::" + jsonString);
			return jsonString;
			
		}
		else if(documentName.trim().equals("Invoice")) {
			
			Invoice invoiceEntity = (Invoice) model;
			JSONObject jObj = new JSONObject();
			
			double netpayable = invoiceEntity.getNetPayable();

			String transactionType = "Pay & Confirm Now";
			String transactionType2 = "";
			
			List<SalesLineItem> productlist = getInvoiceData(invoiceEntity.getSalesOrderProducts());
			List<ProductOtherCharges> prodTaxes = getprodTaxes(invoiceEntity);
			
			if(invoiceEntity.getArrPayTerms()!=null && invoiceEntity.getArrPayTerms().size()>0){
				paymentterms = invoiceEntity.getArrPayTerms().get(0).getPayTermPercent() + " "+ invoiceEntity.getArrPayTerms().get(0).getPayTermComment();
			}
			
			jObj = getJsonData(documentName,appId,company.getMerchantId(),customer.getEmail(),company.getBusinessUnitName(),company.getCurrency(),finalUrl,
					"Invoice",invoiceEntity.getCount(),invoiceEntity.getPersonInfo().getFullName(),invoiceEntity.getInvoiceDate(),
					customer.getCellNumber1(),customer.getSecondaryAdress().getCompleteAddress(),field1,field2,field3,productlist,
					invoiceEntity.getComment(),paymentterms,invoiceEntity.getTotalAmtExcludingTax(),invoiceEntity.getDiscountAmt(),invoiceEntity.getFinalTotalAmt(),
					prodTaxes,netpayable,company.getWebsiteServiceTermsAndConditions(),transactionType,transactionType2,company.getCompanyId(),null,pdfURLLink,company.getEmail(),landingPageText
					,company.getPaymentGatewayId(),company.getPaymentGatewayKey());
			
			
			String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Payment Gateway Invoice Data :::" + jsonString);
			return jsonString;
			
		}
		
		return null;
	}

	
	private List<ProductOtherCharges> getprodTaxes(Invoice invoiceEntity) {

		List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
		for(ContractCharges contractChanges : invoiceEntity.getBillingTaxes()){
			ProductOtherCharges prodCharges = new ProductOtherCharges();
			prodCharges.setChargeName(contractChanges.getTaxChargeName());
			prodCharges.setChargePercent(contractChanges.getTaxChargePercent());
			prodCharges.setAssessableAmount(contractChanges.getTaxChargeAssesVal());
			prodCharges.setChargePayable(contractChanges.getPayableAmt());
			list.add(prodCharges);
		}
			
		return list;
	}

	private List<SalesLineItem> getInvoiceData(ArrayList<SalesOrderProductLineItem> salesOrderProducts) {
		
		List<SalesLineItem> list = new ArrayList<SalesLineItem>();
		
		for(SalesOrderProductLineItem lineItem : salesOrderProducts){
			SalesLineItem saleslineitem = new SalesLineItem();
			saleslineitem.setPrduct(lineItem.getPrduct());
			saleslineitem.setProductName(lineItem.getProdName());
			saleslineitem.setProductCode(lineItem.getProdCode());
			saleslineitem.setPremisesDetails(""); //from contract
			saleslineitem.setStartDate(null); //billing period
			saleslineitem.setEndDate(null);
			saleslineitem.setNumberOfService(0); //from contract
			saleslineitem.setTotalAmount(lineItem.getTotalAmount());
			saleslineitem.setProductSrNo(lineItem.getProductSrNumber());
			list.add(saleslineitem);
		}
		
		return list;
	}

	private JSONObject getJsonData(String documentName, String appId,
			String merchantId, String customerEmail, String companyName,
			String currency, String finalUrl, String documentType, int count, String customerName,
			Date creationDate, Long cellNumber1, String completeAddress,
			String field1, String field2, String field3,
			List<SalesLineItem> productlist, String description,
			String paymentterms, double totalAmount, double discountAmt,
			double finalTotalAmt, List<ProductOtherCharges> productTaxes,
			double netpayable, String websiteServiceTermsAndConditions,
			String transactionType, String transactionType2, Long companyId, Date validUntil,String pdfLink, String companyEmailId, String landingPageText, String paymentGatewayId, String paymentGatewayKey) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		ServerAppUtility serverUtility = new ServerAppUtility();
		if(paymentGatewayId!=null && !paymentGatewayId.equals("")){
			paymentGatewayId = serverUtility.encodeText(paymentGatewayId);
		}
		if(paymentGatewayKey!=null && !paymentGatewayKey.equals("")){
			paymentGatewayKey = serverUtility.encodeText(paymentGatewayKey);
		}
		
		Contract contractEntity = null;
		Invoice invoiceEntity = null;
		if(documentName.trim().equals("Invoice")){
			
			invoiceEntity = ofy().load().type(Invoice.class).filter("count", count)
					.filter("companyId", companyId).first().now();
			
			if(invoiceEntity!=null){
				contractEntity = ofy().load().type(Contract.class).filter("count", invoiceEntity.getContractCount())
						.filter("companyId", companyId).first().now();
			}

		}
		
		JSONObject jObj = new JSONObject();

		jObj.put("Title", documentName);

		jObj.put("partnerId", appId);
		if(merchantId!=null){
			jObj.put("paymentGatewayid", merchantId);
		}
		else{
			jObj.put("paymentGatewayid", "");
		}
		jObj.put("customerEmail", customerEmail);
		if(companyName!=null){
			jObj.put("Company", companyName);
		}
		else{
			jObj.put("Company", "");
		}
		jObj.put("URL", finalUrl);
		jObj.put("companyId", companyId);
		if(companyEmailId!=null){
			jObj.put("companyEmailId", companyEmailId);
		}
		else{
			jObj.put("companyEmailId", "");
		}
		if(paymentGatewayId!=null){
			jObj.put("paymentGatewaySecretId", paymentGatewayId);
		}
		else{
			jObj.put("paymentGatewaySecretId", paymentGatewayId);
		}
		if(paymentGatewayKey!=null){
			jObj.put("paymentgatewayKey", paymentGatewayKey);
		}
		else{
			jObj.put("paymentgatewayKey", paymentGatewayKey);
		}
		
		jObj.put("bodyIntro", landingPageText);

		if(currency!=null){
			jObj.put("Currency", currency);
		}
		else{
			jObj.put("Currency", "");
		}
		jObj.put("DocumentType", documentType);

		JSONArray headerPart = new JSONArray();
		int headerSequence =0;
		JSONObject Obj = new JSONObject();
		Obj.put("sequence", ++headerSequence);
		Obj.put("fieldname", "ID");
		Obj.put("fdesc", "ID");
		Obj.put("fvalue", count);
		headerPart.put(Obj);
		
		
		JSONObject ObjcustName = new JSONObject();
		ObjcustName.put("sequence", ++headerSequence);
		ObjcustName.put("fieldname", "CustomerName");
		ObjcustName.put("fdesc", "Customer Name");
		ObjcustName.put("fvalue", customerName);
		headerPart.put(ObjcustName);
		

		JSONObject objDate = new JSONObject();
		objDate.put("sequence", ++headerSequence);
		objDate.put("fieldname", "Date");
		objDate.put("fdesc", "Date");
		if(creationDate!=null)
			objDate.put("fvalue",sdf.format(creationDate));
		else
			objDate.put("fvalue","");
		headerPart.put(objDate);
		
		JSONObject objPhone = new JSONObject();
		objPhone.put("sequence", ++headerSequence);
		objPhone.put("fieldname", "Phone");
		objPhone.put("fdesc", "Phone");
		objPhone.put("fvalue",cellNumber1);
		headerPart.put(objPhone);
		
		if(documentName.trim().equals("Quotation")){
			JSONObject objvalidUntil = new JSONObject();
			objvalidUntil.put("sequence", ++headerSequence);
			objvalidUntil.put("fieldname", "ValidUntil");
			objvalidUntil.put("fdesc", "Valid Until");
			objvalidUntil.put("fvalue",sdf.format(validUntil));
			headerPart.put(objvalidUntil);
		}
		else{
			JSONObject objvalidUntil = new JSONObject();
			objvalidUntil.put("sequence", ++headerSequence);
			objvalidUntil.put("fieldname", "ValidUntil");
			objvalidUntil.put("fdesc", "Valid Until");
			objvalidUntil.put("fvalue","");
			headerPart.put(objvalidUntil);
		}
		
		JSONObject ObjcustAddress = new JSONObject();
		ObjcustAddress.put("sequence", ++headerSequence);
		ObjcustAddress.put("fieldname", "Address");
		ObjcustAddress.put("fdesc", "Address");
		if(completeAddress!=null)
			ObjcustAddress.put("fvalue", serverUtility.getstringWithoutSpecialCharacter(completeAddress));
		else
			ObjcustAddress.put("fvalue", "");

		headerPart.put(ObjcustAddress);
		
		
		JSONObject objField1 = new JSONObject();
		objField1.put("sequence", ++headerSequence);
		objField1.put("fieldname", "Field1");
		objField1.put("fdesc", "Field1");
		objField1.put("fvalue", field1);
		headerPart.put(objField1);
		
		JSONObject objField2 = new JSONObject();
		objField2.put("sequence", ++headerSequence);
		objField2.put("fieldname", "Field2");
		objField2.put("fdesc", "Field2");
		objField2.put("fvalue", field2);
		headerPart.put(objField2);
		
		JSONObject objField3 = new JSONObject();
		objField3.put("sequence", ++headerSequence);
		objField3.put("fieldname", "Field3");
		objField3.put("fdesc", "Field3");
		objField3.put("fvalue", field3);
		headerPart.put(objField3);
		
		JSONObject customerFirstName = new JSONObject();
		customerFirstName.put("sequence", ++headerSequence);
		customerFirstName.put("fieldname", "customerFirstName");
		customerFirstName.put("fdesc", "First Name");
		customerFirstName.put("fvalue", getCustomerFirstName(customerName));
		headerPart.put(customerFirstName);
		
		jObj.put("headerPart", headerPart);

		ArrayList<String> strproductCategorylist = new ArrayList<String>();
		for(SalesLineItem saleslineitem : productlist) {
			if(saleslineitem.getPrduct().getProductCategory()!=null)
			strproductCategorylist.add(saleslineitem.getPrduct().getProductCategory());
		}
		List<Category> productcategoryList = ofy().load().type(Category.class).filter("companyId", companyId)
								.filter("catName IN", strproductCategorylist).filter("isServiceCategory", true).list();
		logger.log(Level.SEVERE, "productcategoryList"+productcategoryList.size());

		JSONArray bodyPart = new JSONArray();
		
		int sequence = 0;
		int srNO=0;
		for(SalesLineItem saleslineitem : productlist) {
			System.out.println("product terms n condition link " +saleslineitem.getPrduct().getTermsAndConditionLink());
			JSONObject objsrNo = new JSONObject();
			objsrNo.put("sequence", ++sequence);
			objsrNo.put("fieldname", "srNo");
			objsrNo.put("fdesc", "Sr");
			objsrNo.put("fvalue", ++srNO);
			bodyPart.put(objsrNo);
			
			if(documentName.trim().equals("Invoice")){
				
				JSONObject Obj1 = new JSONObject();
				Obj1.put("sequence", ++sequence);
				Obj1.put("fieldname", "PerticularsOfTreatments");
				Obj1.put("fdesc", "Particulars of Treatments");
				if(saleslineitem.getPremisesDetails()!=null){
					Obj1.put("fvalue", getDataFromContract(saleslineitem.getPrduct().getCount(),saleslineitem.getProductSrNo(), contractEntity,"Primices"));
				}
				else{
					Obj1.put("fvalue", "");
				}
				bodyPart.put(Obj1);
				
			}
			else{
				JSONObject Obj1 = new JSONObject();
				Obj1.put("sequence", ++sequence);
				Obj1.put("fieldname", "PerticularsOfTreatments");
				Obj1.put("fdesc", "Particulars of Treatments");
				if(saleslineitem.getPremisesDetails()!=null){
					Obj1.put("fvalue", serverUtility.getstringWithoutSpecialCharacter(saleslineitem.getPremisesDetails()));
				}
				else{
					Obj1.put("fvalue", "");
				}
				bodyPart.put(Obj1);
			}
			
			JSONObject objTreatment = new JSONObject();
			objTreatment.put("sequence", ++sequence);
			objTreatment.put("fieldname", "Treatment");
			objTreatment.put("fdesc", "Treatment");
//			if(saleslineitem.getPrduct().getTermsAndConditionLink()!=null){
//				objTreatment.put("hyperlink", saleslineitem.getPrduct().getTermsAndConditionLink());
//			}
//			else{
//				objTreatment.put("hyperlink", "");
//			}
			objTreatment.put("hyperlink", getProductCategoryhyperLink(saleslineitem.getPrduct().getProductCategory(), productcategoryList));
			
			objTreatment.put("fvalue", saleslineitem.getProductName()+"");
			bodyPart.put(objTreatment);
			
			if(documentName.trim().equals("Invoice") && invoiceEntity!=null && invoiceEntity.getBillingPeroidFromDate()!=null
							&& invoiceEntity.getBillingPeroidToDate()!=null ){
				JSONObject Obj3 = new JSONObject();
				Obj3.put("sequence", ++sequence);
				Obj3.put("fieldname", "StartDate");
				Obj3.put("fdesc", "Billing Period From Date");
				Obj3.put("fvalue", sdf.format(invoiceEntity.getBillingPeroidFromDate()));
				bodyPart.put(Obj3);
				
				JSONObject Obj4 = new JSONObject();
				Obj4.put("sequence", ++sequence);
				Obj4.put("fieldname", "EndDate");
				Obj4.put("fdesc", "Billing Period End Date");
				Obj4.put("fvalue", sdf.format(invoiceEntity.getBillingPeroidToDate()));
				bodyPart.put(Obj4);
			}
			else{
				JSONObject Obj3 = new JSONObject();
				Obj3.put("sequence", ++sequence);
				Obj3.put("fieldname", "StartDate");
				Obj3.put("fdesc", "Start Date");
				Obj3.put("fvalue", sdf.format(saleslineitem.getStartDate()));
				bodyPart.put(Obj3);
				
				JSONObject Obj4 = new JSONObject();
				Obj4.put("sequence", ++sequence);
				Obj4.put("fieldname", "EndDate");
				Obj4.put("fdesc", "End Date");
				Obj4.put("fvalue", sdf.format(saleslineitem.getEndDate()));
				bodyPart.put(Obj4);
			}

			
			if(documentName.trim().equals("Invoice")){
				JSONObject Obj2 = new JSONObject();
				Obj2.put("sequence", ++sequence);
				Obj2.put("fieldname", "NoOfServices");
				Obj2.put("fdesc", "No.of Services");
//				Obj2.put("fvalue", saleslineitem.getNumberOfServices()+"");
				Obj2.put("fvalue", getDataFromContract(saleslineitem.getPrduct().getCount(),saleslineitem.getProductSrNo(), contractEntity,"NoOfServices"));

				bodyPart.put(Obj2);
			}
			else{
				JSONObject Obj2 = new JSONObject();
				Obj2.put("sequence", ++sequence);
				Obj2.put("fieldname", "NoOfServices");
				Obj2.put("fdesc", "No.of Services");
				Obj2.put("fvalue", saleslineitem.getNumberOfServices()+"");
				bodyPart.put(Obj2);
			}
			
			
			
			if(documentName.trim().equals(AppConstants.QUOTATION)){
				JSONObject Obj5 = new JSONObject();
				Obj5.put("sequence", ++sequence);
				Obj5.put("fieldname", "Total");
				Obj5.put("fdesc", "Total");
				Obj5.put("fvalue", getTotalAmount(saleslineitem));
				bodyPart.put(Obj5);
				logger.log(Level.SEVERE, "Quotation "+getTotalAmount(saleslineitem));

			}
			else{
				JSONObject Obj5 = new JSONObject();
				Obj5.put("sequence", ++sequence);
				Obj5.put("fieldname", "Total");
				Obj5.put("fdesc", "Total");
				Obj5.put("fvalue", saleslineitem.getTotalAmount()+"");
				bodyPart.put(Obj5);
			
			}
			logger.log(Level.SEVERE, "saleslineitem.getTotalAmount() "+saleslineitem.getTotalAmount());
			
		}
		
		JSONObject objRemark = new JSONObject();
		objRemark.put("sequence", ++sequence);
		objRemark.put("fieldname", "Remark");
		objRemark.put("fdesc", "Remark");
		if(description!=null){
			objRemark.put("fvalue", serverUtility.getstringWithoutSpecialCharacter(description));
		}
		else{
			objRemark.put("fvalue", "");
		}
		bodyPart.put(objRemark);
		
		JSONObject objPaymentTerms = new JSONObject();
		objPaymentTerms.put("sequence", ++sequence);
		objPaymentTerms.put("fieldname", "PaymentTerms");
		objPaymentTerms.put("fdesc", "Payment Terms");
		objPaymentTerms.put("fvalue", paymentterms);
		bodyPart.put(objPaymentTerms);
		
		
		JSONObject objTotal = new JSONObject();
		objTotal.put("sequence", ++sequence);
		objTotal.put("fieldname", "Total");
		objTotal.put("fdesc", "Total");
		objTotal.put("fvalue", totalAmount);
		bodyPart.put(objTotal);
		
		JSONObject objDiscount = new JSONObject();
		objDiscount.put("sequence", ++sequence);
		objDiscount.put("fieldname", "Discount");
		objDiscount.put("fdesc", "Discount");
		objDiscount.put("fvalue", discountAmt);
		bodyPart.put(objDiscount);
		
		JSONObject objFinalTotalAmt = new JSONObject();
		objFinalTotalAmt.put("sequence", ++sequence);
		objFinalTotalAmt.put("fieldname", "TotalAfterDiscount");
		objFinalTotalAmt.put("fdesc", "Total After Discount");
		objFinalTotalAmt.put("fvalue", finalTotalAmt);
		bodyPart.put(objFinalTotalAmt);
		
		
//		JSONObject objtaxesinfo = new JSONObject();
//		JSONArray taxesPart = new JSONArray();
//		for(ProductOtherCharges prodTaxes : productTaxes){
//			JSONObject objCGST = new JSONObject();
//			objCGST.put("sequence", ++sequence);
//			objCGST.put("fieldname", prodTaxes.getChargeName());
//			objCGST.put("fdesc", prodTaxes.getChargeName()+" "+prodTaxes.getChargePercent()+"%");
//			objCGST.put("fvalue", prodTaxes.getChargePayable());
//			taxesPart.put(objCGST);
//		}
//		objtaxesinfo.put("taxesInfo", taxesPart);
//		bodyPart.put(objtaxesinfo);

		DecimalFormat df = new DecimalFormat("0.00");
		
		boolean CGSTSGSTFlag = false;
		boolean IGSTFlag = false;
		JSONArray taxesPart = new JSONArray();
		for(ProductOtherCharges prodTaxes : productTaxes){
			if(prodTaxes.getChargeName().contains("CGST") || prodTaxes.getChargeName().contains("SGST")){
				CGSTSGSTFlag = true;
			}
			if(prodTaxes.getChargeName().contains("IGST")){
				IGSTFlag = true;
			}
			JSONObject objtax = new JSONObject();
			objtax.put("sequence", ++sequence);
			objtax.put("fieldname", prodTaxes.getChargeName());
			objtax.put("fdesc", prodTaxes.getChargeName()+" "+prodTaxes.getChargePercent()+"%");
			objtax.put("fvalue", df.format(prodTaxes.getChargePayable())); 
			bodyPart.put(objtax);

		}
		if(!CGSTSGSTFlag){
			JSONObject objtax = new JSONObject();
			objtax.put("sequence", ++sequence);
			objtax.put("fieldname", "CGST");
			objtax.put("fdesc", "%");
			objtax.put("fvalue", "");
			bodyPart.put(objtax);
			
			JSONObject objtax1 = new JSONObject();
			objtax1.put("sequence", ++sequence);
			objtax1.put("fieldname", "SGST");
			objtax1.put("fdesc", "%");
			objtax1.put("fvalue", "");
			bodyPart.put(objtax1);
		}
		if(!IGSTFlag){
			JSONObject objtax = new JSONObject();
			objtax.put("sequence", ++sequence);
			objtax.put("fieldname", "IGST");
			objtax.put("fdesc", "%");
			objtax.put("fvalue", "");
			bodyPart.put(objtax);
		}
		
		JSONObject objNetpayable = new JSONObject();
		objNetpayable.put("sequence", ++sequence);
		objNetpayable.put("fieldname", "NetPayable");
		objNetpayable.put("fdesc", "Net Payable");
		objNetpayable.put("fvalue", netpayable);
		bodyPart.put(objNetpayable);
		
		jObj.put("bodyPart", bodyPart);
		
		
		JSONArray footerPart = new JSONArray();
		
		int footerSequence=1;
		JSONObject objTermsCondition = new JSONObject();
		objTermsCondition.put("sequence", ++footerSequence);
		objTermsCondition.put("fieldname", "TermsAndCondition");
		objTermsCondition.put("fdesc", "I accept Contract Terms and Conditions");
		if(websiteServiceTermsAndConditions!=null){
			objTermsCondition.put("fvalue", websiteServiceTermsAndConditions);
		}
		else{
			objTermsCondition.put("fvalue", "");
		}
		footerPart.put(objTermsCondition);
		
		JSONObject objTransactionType = new JSONObject();
		objTransactionType.put("sequence", ++footerSequence);
		objTransactionType.put("fieldname", "PayAndConfirmNow");
		objTransactionType.put("fdesc", transactionType);
		objTransactionType.put("fvalue", "yes");
		footerPart.put(objTransactionType);
		
		JSONObject objTransactionType2 = new JSONObject();
		objTransactionType2.put("sequence", ++footerSequence);
		objTransactionType2.put("fieldname", "ConfirmNowAndPayLater");
		objTransactionType2.put("fdesc", transactionType2);
		objTransactionType2.put("fvalue", "yes");
		footerPart.put(objTransactionType2);
		
		JSONObject objpdfLink = new JSONObject();
		objpdfLink.put("sequence", ++footerSequence);
		objpdfLink.put("fieldname", "PDFLink");
		objpdfLink.put("fdesc", "Download");
		if(pdfLink!=null){
			objpdfLink.put("fvalue", pdfLink);
		}
		else{
			objpdfLink.put("fvalue", "");
		}
		footerPart.put(objpdfLink);
		
		jObj.put("footerPart", footerPart);

		return jObj;
	}

	private Object getProductCategoryhyperLink(String productCategory,	List<Category> productcategoryList) {
		String productCategorylink = "";
		try {
			if(productCategory!=null){
				for(Category categoryEntity : productcategoryList) {
					if(productCategory.trim().equals(categoryEntity.getCatName().trim())){
						if(categoryEntity.getDescription()!=null){
							productCategorylink = categoryEntity.getDescription();
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return productCategorylink;
	}

	public String getTotalAmount(SalesLineItem saleslineitem) {

		DecimalFormat df = new DecimalFormat("0.00");


		double total = 0;
		SuperProduct product = saleslineitem.getPrduct();
		double tax = removeAllTaxes(product);
		double origPrice = saleslineitem.getPrice() - tax;
		total = origPrice;
		/**
		 * @author Anil
		 * @since 30-06-2020
		 * For sai care by Vaishnavi and Nitin Sir
		 * If Area is null then total price will be multiplication of rate and no. of services
		 */
		if (saleslineitem.isServiceRate()&&(saleslineitem.getArea()==null||saleslineitem.getArea().equals("")||saleslineitem.getArea().equalsIgnoreCase("NA"))) {
			origPrice=origPrice*saleslineitem.getNumberOfServices();
		}


		if ((saleslineitem.getPercentageDiscount() == null && saleslineitem.getPercentageDiscount() == 0)&& (saleslineitem.getDiscountAmt() == 0)) {

			System.out.println("inside both 0 condition");
			/********************
			 * Square Area Calculation code added in if condition and
			 * without square area calculation code in else block
			 ***************************/
			System.out.println("Get value from area =="+ saleslineitem.getArea());
			System.out.println("total amount before area calculation =="+ total);
			if (!saleslineitem.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(saleslineitem.getArea());
				total = origPrice * area;
				System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+ total);
			} else {
				total = origPrice;
			}
		}
		else if ((saleslineitem.getPercentageDiscount() != null)&& (saleslineitem.getDiscountAmt() != 0)) {

			if (!saleslineitem.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(saleslineitem.getArea());
				total = origPrice * area;
				total = total- (total * saleslineitem.getPercentageDiscount() / 100);
				total = total - saleslineitem.getDiscountAmt();
			} else {
				total = origPrice;
				total = total- (total * saleslineitem.getPercentageDiscount() / 100);
				total = total - saleslineitem.getDiscountAmt();
			}

		} else {
			System.out.println("inside oneof the null condition");

			if (saleslineitem.getPercentageDiscount() != null) {
				// System.out.println("inside getPercentageDiscount oneof the null condition");
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);

				if (!saleslineitem.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(saleslineitem.getArea());
					total = origPrice * area;
					total = total- (total * saleslineitem.getPercentageDiscount() / 100);
				} else {
					total = origPrice;
					total = total- (total * saleslineitem.getPercentageDiscount() / 100);
				}
			} else {
				// System.out.println("inside getDiscountAmt oneof the null condition");
				// total=origPrice-object.getDiscountAmt();

				if (!saleslineitem.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(saleslineitem.getArea());
					total = origPrice * area;
					total = total - saleslineitem.getDiscountAmt();

				} else {
					total = total - saleslineitem.getDiscountAmt();
				}
			}

			// total=total*object.getQty();

		}

		return df.format(total);
	
		
	}
	
	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			/**
			 * Date 13-06-2018
			 * By Vijay 
			 * Des :- above old code commented and new code added below
			 */
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			/**
			 * ends here
			 */
		}
		tax = retrVat + retrServ;
		return tax;
	}

	private String getDataFromContract(int productId, int productSrNo,	Contract contractEntity, String fieldName) {
		
		for(SalesLineItem lineItem : contractEntity.getItems()){
			if(lineItem.getPrduct().getCount()==productId && productSrNo==lineItem.getProductSrNo()){
				if(fieldName.equals("Primices")){
					if(lineItem.getPremisesDetails()!=null){
						return lineItem.getPremisesDetails();
					}
					else{
						return "";
					}
				}
				else if(fieldName.equals("NoOfServices")){
					return lineItem.getNumberOfServices()+"";
				}
				else{
					return "";
				}
			}
		}
		
		return "";
	}


	private String getCustomerFirstName(String fullName) {
		String [] customerName = fullName.split("\\s+");
		logger.log(Level.SEVERE, "customerName first Name" + customerName[0]);

		return customerName[0];
	}

	private String sendsmstoClient(Customer customer, Long companyId, String paymentGatewayuniqueId, String customerName) {

		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId).filter("status", true).first().now();
		logger.log(Level.SEVERE, "smsconfig" + smsconfig);

		if(smsconfig!=null){
			
			if(smsconfig.getStatus()){
				String accountSid = smsconfig.getAccountSID();
				String authoToken = smsconfig.getAuthToken();
				String password = smsconfig.getPassword();
				
				SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",companyId).filter("event","PaymentGateway").first().now();
				logger.log(Level.SEVERE, "smsEntity" + smsEntity);

				if(smsEntity!=null){
					
					if(smsEntity.getStatus()){
						String templateMsgWithBraces = smsEntity.getMessage();
//						Dear <First Name>, Thank you for signing with us! Complete your payment using the digital link - <Short Link>
//						https://us-east1-oneclickventure.cloudfunctions.net/razorPay/razorPayment/callRazorPayGateway/{PaymentGatewayUniqId}
//						String paymentGatewaylink = "https://us-east1-oneclickventure.cloudfunctions.net/razorPay/razorPayment/callRazorPayGateway/"+paymentGatewayuniqueId;
						
						String paymentGatewaylink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayuniqueId;
						String custName = templateMsgWithBraces.replace("{CustomerName}", customerName);
						String actualmsg = custName.replace("{ShortLink}", paymentGatewaylink);
						SmsServiceImpl smsimpl = new SmsServiceImpl();
						long customercell = customer.getCellNumber1();
						logger.log(Level.SEVERE, "customercell" + customercell);
						if(customercell!=0){
							int value =  smsimpl.sendSmsToClient(actualmsg, customer.getCellNumber1(), companyId);
						}
						else{
							return "Customer cell should not be 0";
						}
					}
					else{
						return "PaymentGateway SMS Template is inactive";
					}
					

				}
				else{
					return "Payment Gateway SMS Template does not defined";
				}
			}
			else{
				return "SMS Configuration status is inactive";
			}
			
		
		}
		else{
			return "SMS Configuration does not defined or inactive";
		}
		
		return "";
	}

	private String createLeadJSONObject(Lead leadEntity,String appUrl, String appId, Company company, Customer customer) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		String finalUrl = appUrl+"contractinvoicepayment?contractDetails=";

		JSONObject jObj = new JSONObject();
		jObj.put("partnerId", appId);
		jObj.put("paymentGatewayid", company.getMerchantId());
		jObj.put("customerPhoneNo", leadEntity.getPersonInfo().getCellNumber());
		jObj.put("customerEmail", customer.getEmail());
		double netpayable = (leadEntity.getNetpayable()*100);
		jObj.put("invoiceAmount", netpayable);
		if(company!=null){
			jObj.put("Company", company.getBusinessUnitName());
		}
		else{
			jObj.put("Company", "");
		}
		jObj.put("URL", finalUrl);
//		jObj.put("customerName", leadEntity.getPersonInfo().getFullName());
//		jObj.put("customerAddress", customer.getSecondaryAdress().getCompleteAddress());
		
		JSONArray jsonproductarray = new JSONArray();

		JSONObject Obj = new JSONObject();
		Obj.put("sequence", "1");
		Obj.put("fieldname", "PageTitle");
		Obj.put("fdesc", "Title");
		Obj.put("fvalue", "Sign Up");
		Obj.put("feditable", "False");
		jsonproductarray.put(Obj);
		
		JSONObject ObjcustName = new JSONObject();
		ObjcustName.put("sequence", "2");
		ObjcustName.put("fieldname", "CustomerName");
		ObjcustName.put("fdesc", "Customer Name");
		ObjcustName.put("fvalue", leadEntity.getPersonInfo().getFullName());
		ObjcustName.put("feditable", "True");
		jsonproductarray.put(ObjcustName);
		
		JSONObject ObjcustAddress = new JSONObject();
		ObjcustAddress.put("sequence", "3");
		ObjcustAddress.put("fieldname", "customerAddress");
		ObjcustAddress.put("fdesc", "Address");
		ObjcustAddress.put("fvalue", customer.getSecondaryAdress().getCompleteAddress());
		ObjcustAddress.put("feditable", "True");
		jsonproductarray.put(ObjcustAddress);
		
		int sequence = 3;
		for(SalesLineItem saleslineitem : leadEntity.getLeadProducts()) {
			JSONObject Obj1 = new JSONObject();
			Obj1.put("sequence", ++sequence);
			Obj1.put("fieldname", "Product");
			Obj1.put("fdesc", "Service");
			Obj1.put("fvalue", saleslineitem.getProductName()+"");
			Obj1.put("feditable", "False");
			jsonproductarray.put(Obj1);
			
			JSONObject Obj2 = new JSONObject();
			Obj2.put("sequence", ++sequence);
			Obj2.put("fieldname", "Services");
			Obj2.put("fdesc", "# Service");
			Obj2.put("fvalue", saleslineitem.getNumberOfServices()+"");
			Obj2.put("feditable", "False");
			jsonproductarray.put(Obj2);
			
			JSONObject Obj3 = new JSONObject();
			Obj3.put("sequence", ++sequence);
			Obj3.put("fieldname", "StartDate");
			Obj3.put("fdesc", "Start Date");
			Obj3.put("fvalue", sdf.format(saleslineitem.getStartDate()));
			Obj3.put("feditable", "False");
			jsonproductarray.put(Obj3);
			
			JSONObject Obj4 = new JSONObject();
			Obj4.put("sequence", ++sequence);
			Obj4.put("fieldname", "EndDate");
			Obj4.put("fdesc", "End Date");
			Obj4.put("fvalue", sdf.format(saleslineitem.getEndDate()));
			Obj4.put("feditable", "False");
			jsonproductarray.put(Obj4);
			
			JSONObject Obj5 = new JSONObject();
			Obj5.put("sequence", ++sequence);
			Obj5.put("fieldname", "Duration");
			Obj5.put("fdesc", "Duration");
			Obj5.put("fvalue", saleslineitem.getDuration()+"");
			Obj5.put("feditable", "False");
			jsonproductarray.put(Obj5);
		}
		
		
		jObj.put("dynamicData", jsonproductarray);
		
		String jsonString = jObj.toJSONString().replaceAll("\\\\", "");
		logger.log(Level.SEVERE, "Payment Gateway Lead Data :::" + jsonString);
		return jsonString;
		
	}

	@Override
	public Quotation updateAddress(Quotation quotationEntity) {
		logger.log(Level.SEVERE,"count == "+quotationEntity.getCount());
		HashSet<String> hsbranchName = new HashSet<String>();
		for(SalesLineItem item : quotationEntity.getItems()) {
			if(item.getCustomerBranchSchedulingInfo() != null && item.getCustomerBranchSchedulingInfo().size() > 0){
				for(BranchWiseScheduling branch : item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
					if(branch.isCheck()){
						hsbranchName.add(branch.getBranchName());
					}
				}
			}	
		}
		ArrayList<String> arraylist = new ArrayList<String>(hsbranchName);
		if(arraylist.size()==1){
			if(arraylist.get(0).trim().equals("Service Address")){
				Customer customer = ofy().load().type(Customer.class).filter("companyId", quotationEntity.getCompanyId())
									.filter("count", quotationEntity.getCinfo().getCount()).first().now();
				if(customer!=null){
					quotationEntity.setServiceAddress(customer.getSecondaryAdress());
					quotationEntity.setBillingAddress(customer.getAdress());
				}
			}
			else{
				CustomerBranchDetails customerBranch = ofy().load().type(CustomerBranchDetails.class).filter("companyId", quotationEntity.getCompanyId())
														.filter("cinfo.count", quotationEntity.getCinfo().getCount()).filter("buisnessUnitName", arraylist.get(0)).first().now();
				if(customerBranch!=null){
					quotationEntity.setServiceAddress(customerBranch.getAddress());
					quotationEntity.setBillingAddress(customerBranch.getBillingAddress());
//					quotationModel.setCustomerBranch(customerBranch.getBusinessUnitName());
				}
			}
		}
		else if(arraylist.size()>1){
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", quotationEntity.getCompanyId())
					.filter("count", quotationEntity.getCinfo().getCount()).first().now();
			if(customer!=null){
				quotationEntity.setServiceAddress(customer.getSecondaryAdress());
				quotationEntity.setBillingAddress(customer.getAdress());
				
				
			}
		}
		
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(quotationEntity);
//		ofy().save().entity(quotationEntity);
		
		return quotationEntity;
	}

	@Override
	public ArrayList<SuperModel> getSalesRegisterData(MyQuerry querry, Date fromDate, Date toDate, int invoiceid, long companyId) {
		ArrayList<SuperModel> list = new ArrayList<SuperModel>();
		GenricServiceImpl impl = new  GenricServiceImpl();
		list.addAll(impl.getSearchResult(querry));

		List<CreditNote> creditnotelist = null;
		if(fromDate!=null && toDate!=null){
			creditnotelist = ofy().load().type(CreditNote.class).filter("companyId", companyId)
								.filter("creditNoteDate >=", fromDate).filter("creditNoteDate <=", toDate).list();
			
		}
		else{
			if(invoiceid!=0){
				creditnotelist = ofy().load().type(CreditNote.class).filter("companyId", companyId)
						.filter("invoiceId", invoiceid).list();
			}
		}
		if(creditnotelist!=null && creditnotelist.size()>0){
			logger.log(Level.SEVERE, "creditnotelist size"+creditnotelist.size());
			HashSet<Integer> hsinvoiceid = new HashSet<Integer>();
			for(CreditNote creditnoteEntity : creditnotelist){
				if(creditnoteEntity.getInvoiceId()!=0)
				hsinvoiceid.add(creditnoteEntity.getInvoiceId());
			}
			if(hsinvoiceid.size()>0){
				ArrayList<Integer> arrayinvoiceid = new ArrayList<Integer>(hsinvoiceid);
				List<Invoice> invoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("count IN", arrayinvoiceid)
											.list();
				logger.log(Level.SEVERE, "creditnotelist invoicelist size"+creditnotelist.size());

				if(invoicelist.size()>0){
					for(Invoice invoiceEntity : invoicelist){
						invoiceEntity.setCreditnoteflag(true);
					}
					
					list.addAll(invoicelist);
				}
			}
		
		}
		
		return list;
	}

	@Override
	public String updateStationedContractIntoNormalContract(int contractId,long companyId) {
		
		Queue queue = QueueFactory.getQueue("UpdateServices-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "Update Station Technician Contract Into Normal Contract")
				.param("companyId", companyId+"").param("ContractId", contractId+""));
		logger.log(Level.SEVERE, "Task queue called for converting station technician into normal contracts "+contractId);
		
		return "It will take some time to reflect new service status in the system based on number of services, kindly check after some time";
	}

	@Override
	public String updateNormalContractIntoTechnicianContract(int contractId,
			long companyId) {
		Queue queue = QueueFactory.getQueue("UpdateServices-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "Update Normal Contract Into Station Technician Contract")
				.param("companyId", companyId+"").param("ContractId", contractId+""));
		logger.log(Level.SEVERE, "Task queue called for converting station technician into normal contracts "+contractId);
		
		return "It will take some time to reflect new service status in the system based on number of services, kindly check after some time";
	
	}

	
	public String validateInternetNumberRange(long companyId, String documentName) {
		String error ="";
		boolean flag = false;
		ServerAppUtility serverapputility = new ServerAppUtility();
		List<Config> numberRangeConfig = serverapputility.loadallConfigsData(91, companyId);
		logger.log(Level.SEVERE, "numberRangeConfig" + numberRangeConfig.size());
		for(Config confiEntity : numberRangeConfig){
			if(confiEntity.getName().equals("Internet")){
				flag = true;
				if(confiEntity.isStatus()==false){
					flag = false;
					return "Number Range - Internet status is InActive."+"@Error";
				}
			}
			
		}
		if(flag){
			ArrayList<String> internetnumgenlist = new ArrayList<String>();
			internetnumgenlist.add("C_Internet");
			internetnumgenlist.add("S_Internet");
			internetnumgenlist.add("B_Internet");
			internetnumgenlist.add("I_Internet");
			internetnumgenlist.add("P_Internet");
			if(documentName!=null && documentName.equals(AppConstants.LEAD)){
				internetnumgenlist.add("L_Internet");
			}
			if(documentName!=null && documentName.equals(AppConstants.QUOTATION)){
				internetnumgenlist.add("Q_Internet");

			}
			logger.log(Level.SEVERE, "internetnumgenlist size"+internetnumgenlist.size());
			logger.log(Level.SEVERE, "internetnumgenlist"+internetnumgenlist);

			System.out.println("internetnumgenlist size"+internetnumgenlist);
			List<NumberGeneration> numGenlist = ofy().load().type(NumberGeneration.class).filter("companyId",companyId)
					.filter("processName IN",internetnumgenlist).filter("status", true).list();
			
			if(numGenlist.size()!=0){
				error += " Internet Number Range does not exist";
				boolean validationflag = false;
				for(String numbergenName : internetnumgenlist){
					validationflag = false;
					for(NumberGeneration numgen : numGenlist){
						if(numgen.getProcessName().trim().equals(numbergenName)){
							validationflag = false;
							break;
						}
						else{
							validationflag = true;
						}
					}
					logger.log(Level.SEVERE, "validationflag"+validationflag);

					if(validationflag){
						System.out.println("numbergenName  =="+numbergenName);
						if(numbergenName.equals("C_Internet")){
							error += " contract ";
						}
						else if(numbergenName.equals("S_Internet")){
							error += " Service ";
						}
						else if(numbergenName.equals("B_Internet")){
							error += " Billing";
						}
						else if(numbergenName.equals("I_Internet")){
							error += " Invoice";
						}
						else if(numbergenName.equals("P_Internet")){
							error += " Payment";
						}
						else if(numbergenName.equals("L_Internet")){
							error += " Lead";
						}
						else if(numbergenName.equals("Q_Internet")){
							error += " Quotation";
						}
					}
					
					
				}
				if(validationflag){
					error += " in the Number Generation.";
				}
				else{
					logger.log(Level.SEVERE, "validationflag false else block"+validationflag);

					error ="";
				}
			}
			else{
				error +=" Number Range - Internet does not exist in the Number Generation!";
			}
			
		}
		else{
			return "Number Range - Internet does not exist in the system! Please create in the system"+"@Error";
		}
		logger.log(Level.SEVERE, "error == "+error);
		return error;
	}

	@Override
	public String getPDFURL(String documentName,SuperModel model, int documentId,long companyId,Company company, String moduleURL, String salarySlipMonth){
		String finalURL = "";
		finalURL = finalURL+moduleURL;
		boolean companyheaderFooterFlag = false;

		try {
			
		if(company==null){
			company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		}
		
		if(company.getUploadHeader()!=null && company.getUploadFooter()!=null){
			companyheaderFooterFlag = true;
		}
		logger.log(Level.SEVERE, "PDF Link companyheaderFooterFlag "+companyheaderFooterFlag);
		logger.log(Level.SEVERE, "documentName"+documentName);

		if(documentName.trim().equals(AppConstants.QUOTATION)){
			logger.log(Level.SEVERE, "Inside Quotation for PDF Link");

			Quotation quotationEntity = null;
			if(model!=null){
				quotationEntity = (Quotation) model;
			}
			else{
				quotationEntity = ofy().load().type(Quotation.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			

			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
									.filter("companyId", companyId).filter("processName", AppConstants.QUOTATION)
									.filter("processList.status", true).list();
			
			logger.log(Level.SEVERE, "processConfiglist size"+processConfiglist.size());

				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				if(processConfiglist.size()==0){
					
					return finalURL += "pdfservice"+"?Id="+quotationEntity.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
				}
				else{
					
					for(SuperModel model1:processConfiglist)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model1;
						processList.addAll(processConfig.getProcessList());
						
					}
					logger.log(Level.SEVERE, "processList size"+processList.size());
	
				for(int k=0;k<processList.size();k++){	
					
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
//					
//					if(companyheaderFooterFlag){
//						finalURL +="pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
//					}
//					else{
//						finalURL +="pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
//					}
//				
//				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PrintCustomQuotation")&&processList.get(k).isStatus()==true){
					
					if(quotationEntity.getGroup().equalsIgnoreCase("Five Years"))
					{
						if(companyheaderFooterFlag){
							finalURL +="customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"no";
						}
						else{
							finalURL +="customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"Five Years"+"&"+"preprint="+"yes";
						}
					}
					else if(quotationEntity.getGroup().equalsIgnoreCase("Direct Quotation"))
					{
						if(companyheaderFooterFlag){
							finalURL +="customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"no";
						}
						else{
							finalURL +="customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"Direct Quotation"+"&"+"preprint="+"yes";
						}
	
					}
					else if(quotationEntity.getGroup().equalsIgnoreCase("Building"))
					{
						if(companyheaderFooterFlag){
							finalURL +="customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"Building"+"&"+"preprint="+"no";
						}
						else{
							finalURL +="customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"Building"+"&"+"preprint="+"yes";
						}
						 
					}
					else if(quotationEntity.getGroup().equalsIgnoreCase("After Inspection"))
					{
						if(companyheaderFooterFlag){
							finalURL += "customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"no";
						}
						else{
							finalURL += "customQuotation"+"?Id="+quotationEntity.getId()+"&"+"group="+"After Inspection"+"&"+"preprint="+"yes";
						}
	
					}
				
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestleQuotations")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL +="pdfpestleservice"+"?Id="+quotationEntity.getId()+"&"+"group="+quotationEntity.getGroup()+"&"+"preprint="+"no";//1st PDF
					}
					else{
						finalURL +="pdfpestleservice"+"?Id="+quotationEntity.getId()+"&"+"group="+quotationEntity.getGroup()+"&"+"preprint="+"yes";//1st PDF
					}
	
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL +="pdfpestoIndiaservice"+"?Id="+quotationEntity.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
					}
					else{
						finalURL +="pdfpestoIndiaservice"+"?Id="+quotationEntity.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
					}
				
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("NBHCQuotation")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL += "nbhcServiceQuotation"+"?Id="+quotationEntity.getId()+"&"+"preprint="+"no"; 
					}
					else{
						finalURL += "nbhcServiceQuotation"+"?Id="+quotationEntity.getId()+"&"+"preprint="+"yes"; 
					}
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL += "universalServiceQuotation"+"?Id="+quotationEntity.getId()+"&"+"preprint="+"no"+"&"+"type="+"q"; 
	
					}
					else{
						finalURL += "universalServiceQuotation"+"?Id="+quotationEntity.getId()+"&"+"preprint="+"yes"+"&"+"type="+"q"; 
					}
	
				}
				

				}
				
				if(finalURL.contains("?Id=")){
					return finalURL;
				}
				else{
					logger.log(Level.SEVERE, "Quotation For Standard PDF");
					if(companyheaderFooterFlag){
						return finalURL += "pdfservice"+"?Id="+quotationEntity.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
					}
					else{
						return finalURL += "pdfservice"+"?Id="+quotationEntity.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";//Ashwini Patil Date:8-09-2022 yes changed to no
					}
				}
			}
			
		}
		else if(documentName.trim().equals(AppConstants.CONTRACT)){
			
			Contract contractEntity = null;
			if(model!=null){
				contractEntity = (Contract) model;
			}
			else{
				contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			
			
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", AppConstants.CONTRACT)
					.filter("processList.status", true).list();
			
			logger.log(Level.SEVERE, "Contract processConfiglist size"+processConfiglist.size());

			
			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

			if (processConfiglist.size() == 0) {

				return finalURL += "pdfservice" + "?Id=" + contractEntity.getId()+ "&" + "type=" + "c" + "&" + "preprint="+ "yes";
				
			} else {

				for (SuperModel model1 : processConfiglist) {
					ProcessConfiguration processConfig = (ProcessConfiguration) model1;
					processList.addAll(processConfig
							.getProcessList());

				}

//				int pecopp=0;
//				
//				for (int k = 0; k < processList.size(); k++) {
//					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")
//							&& processList.get(k).isStatus() == true) {
//						pecopp++;
//						break;
//					}
//
//				}
//				logger.log(Level.SEVERE, "pecopp "+pecopp);
				
				for (int k = 0; k < processList.size(); k++) {
//					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")
//							&& processList.get(k).isStatus() == true) {
//						logger.log(Level.SEVERE, "inside CompanyAsLetterHead");
//						
//						
//						if(pecopp>0){
//							return finalURL += "contractserviceone"+"?Id="+model.getId()+"&"+"preprint=yes"+"&"+"type="+"Contract";
//
//						}
//						else if(companyheaderFooterFlag){
//							return finalURL +="pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"no";
//						}
//						else{
//							return finalURL +="pdfservice"+"?Id="+model.getId()+"&"+"type="+"q"+"&"+"preprint="+"yes";
//						}
//
//					}

					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")
							&& processList.get(k).isStatus() == true) {
						logger.log(Level.SEVERE, "inside PestoIndiaQuotations");

						if(companyheaderFooterFlag){
							finalURL += "pdfpestoIndiaservice" + "?Id=" + contractEntity.getId() + "&"+ "type=" + "c" + "&" + "preprint=" + "no";

						}
						else{
							finalURL += "pdfpestoIndiaservice" + "?Id=" + contractEntity.getId() + "&"+ "type=" + "c" + "&" + "preprint=" + "yes";
						}
						

					}

					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")
							&& processList.get(k).isStatus() == true) {
						logger.log(Level.SEVERE, "inside VCareCustomization");

						if(companyheaderFooterFlag){
							finalURL +="Contract" + "?Id="+ contractEntity.getId() + "&" + "preprint=" + "no";

						}
						else{
							finalURL +="Contract" + "?Id="+ contractEntity.getId() + "&" + "preprint=" + "yes";
						}

					}

					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processList.get(k).isStatus() == true) {
							logger.log(Level.SEVERE, "inside VCareCustomization");
							finalURL += "pdfcon" + "?Id=" + contractEntity.getId();
					}

					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("AMCForGenesis")
							&& processList.get(k).isStatus() == true) {
						finalURL += "pdfamc" + "?Id=" + contractEntity.getId();

					}
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("UniversalPestCustomization")
							&& processList.get(k).isStatus() == true) {
						logger.log(Level.SEVERE, "inside UniversalPestCustomization");

						if(companyheaderFooterFlag){
							finalURL +="universalServiceQuotation" + "?Id=" + contractEntity.getId()
									+ "&" + "preprint=" + "no" + "&" + "type=" + "c";
						}
						else{
							finalURL +="universalServiceQuotation" + "?Id=" + contractEntity.getId()
									+ "&" + "preprint=" + "yes" + "&" + "type=" + "c";
						}

					}
					
					if (processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")
							&& processList.get(k).isStatus() == true) {
						logger.log(Level.SEVERE, "inside OnlyForPecopp");

						if(companyheaderFooterFlag){
							finalURL += "contractserviceone"+"?Id="+contractEntity.getId()+"&"+"preprint=no"+"&"+"type="+"Contract";
						}
						else{
							finalURL += "contractserviceone"+"?Id="+contractEntity.getId()+"&"+"preprint=yes"+"&"+"type="+"Contract";
						}
					}

				}
			
				if(finalURL.contains("?Id=")){
					return finalURL;
				}
				else{
					logger.log(Level.SEVERE, "Contract For Standard PDF");
					if(companyheaderFooterFlag){
						return finalURL +="pdfservice"+"?Id="+contractEntity.getId()+"&"+"type="+"c"+"&"+"preprint="+"no";
					}
					else{
						return finalURL +="pdfservice"+"?Id="+contractEntity.getId()+"&"+"type="+"c"+"&"+"preprint="+"no";//Ashwini Patil Date:8-09-2022 yes changed to no
					}
				}
				
			}
		}
		else if(documentName.trim().equals("Invoice")){
			
			Invoice invoiceEntity = null;
			if(model!=null){
				invoiceEntity = (Invoice) model;
			}
			else{
				invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "Invoice")
					.filter("processList.status", true).list();
			logger.log(Level.SEVERE, "Invoice processConfiglist size"+processConfiglist.size());

			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();


			if(processConfiglist.size()==0){

				/**
				 * Date 28-3-2018
				 * By jayshree
				 * Des.to call the multiple billing invoice
				 */
				
				if(invoiceEntity.isMultipleOrderBilling()==true){
					return finalURL += "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type="+"MultipleBilling";
				}else{
					return finalURL += "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"plane"+"&"+"type="+"SingleBilling";
				}
				
			}
			else{
			
				for(SuperModel model1:processConfiglist)
				{
					ProcessConfiguration processConfig=(ProcessConfiguration)model1;
					processList.addAll(processConfig.getProcessList());
					
				}
			
			for(int k=0;k<processList.size();k++){	
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
//					
//					logger.log(Level.SEVERE, "CompanyAsLetterHead");
//
//					if(companyheaderFooterFlag){
//						if(invoiceEntity.isMultipleOrderBilling()==true){
//							return finalURL += "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"MultipleBilling";
//						}
//						else{
//							return finalURL += "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
//						}
//	
//					}
//					else{
//						if(invoiceEntity.isMultipleOrderBilling()==true){
//							return finalURL += "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"no"+"&"+"type="+"MultipleBilling";
//						}
//						else{
//							return finalURL += "serviceinvoiceprint"+"?Id="+model.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
//						}
//					}
//				
//				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PestoIndiaQuotations")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "PestoIndiaQuotations");

					if(companyheaderFooterFlag){
						finalURL += "pdfpestoIndiaservice"+"?Id="+invoiceEntity.getId()+"&"+"type="+"i"+"&"+"preprint="+"no"; 
					}
					else{
						finalURL += "pdfpestoIndiaservice"+"?Id="+invoiceEntity.getId()+"&"+"type="+"i"+"&"+"preprint="+"yes"; 
					}
	
				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("VCareCustomization")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "VCareCustomization");

					if(companyheaderFooterFlag){
						finalURL += "Invoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
					}
					else{
						finalURL += "Invoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
					}
	
				
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("InvoiceAndRefDocCustmization")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "InvoiceAndRefDocCustmization");

					if(companyheaderFooterFlag){
						finalURL +="invoicepdf"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
					}
					else{
						finalURL +="invoicepdf"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
	
					}
				}
				
				if (processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForOrionPestSolutions")&& processList.get(k).isStatus() == true) {
					logger.log(Level.SEVERE, "OnlyForOrionPestSolutions");

					if(companyheaderFooterFlag){
						finalURL += "pdfOrionInvoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no";
	
					}
					else{
						finalURL += "pdfOrionInvoice"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes";
					}
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ReddysPestControlQuotations")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "ReddysPestControlQuotations");

					finalURL += "pdfrinvoice"+"?Id="+invoiceEntity.getId();
					
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "OnlyForPecopp");
					if(companyheaderFooterFlag){
						finalURL += "contractserviceone"+"?Id="+invoiceEntity.getId()+"&"+"preprint=no"+"&"+"type="+"Invoice";
					}
					else {
						finalURL += "contractserviceone"+"?Id="+invoiceEntity.getId()+"&"+"preprint=plane"+"&"+"type="+"Invoice";

					}
				}
				
			}
			
			logger.log(Level.SEVERE, "finalURL == "+finalURL);

			if(finalURL.contains("?Id=")){
				return finalURL;
			}
			else{
				
				logger.log(Level.SEVERE, "Invoice For Standard PDF");
				if(companyheaderFooterFlag){
					if(invoiceEntity.isMultipleOrderBilling()==true){
						return finalURL += "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no"+"&"+"type="+"MultipleBilling";
					}
					else{
						return finalURL += "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no"+"&"+"type="+"SingleBilling";//Ashwini Patil Date:8-09-2022 yes changed to no
					}

				}
				else{
					if(invoiceEntity.isMultipleOrderBilling()==true){
						return finalURL += "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"no"+"&"+"type="+"MultipleBilling";
					}
					else{
						return finalURL += "serviceinvoiceprint"+"?Id="+invoiceEntity.getId()+"&"+"preprint="+"yes"+"&"+"type="+"SingleBilling";
					}
				}
			}
			
			}
		}
		else if(documentName.trim().equals(AppConstants.CONTRACT_RENEWAL)){
			
			ContractRenewal contractRenewal = null;
			if(model!=null){
				contractRenewal = (ContractRenewal) model;
			}
			else{
				contractRenewal = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", documentId)
									.first().now();
			}
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "ContractRenewal")
					.filter("processList.status", true).list();
			logger.log(Level.SEVERE, "Contract Renewal processConfiglist size"+processConfiglist.size());

			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();


			if(processConfiglist.size()==0){
				return finalURL += "pdcontractrenewal" + "?ContractId="+ contractRenewal.getContractId()+ "&CompanyId="+ companyId+"&"+"preprint="+"plane&flag="+false;
				
			}
			else{
			
				for(SuperModel model1:processConfiglist)
				{
					ProcessConfiguration processConfig=(ProcessConfiguration)model1;
					processList.addAll(processConfig.getProcessList());
					
				}
			
			for(int k=0;k<processList.size();k++){	
//				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processList.get(k).isStatus()==true){
//					
//				
//				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForNBHC")&&processList.get(k).isStatus()==true){
						
					if(companyheaderFooterFlag){
						finalURL += "pdcontractrenewal" + "?ContractId="+ contractRenewal.getContractId()+ "&"+"CompanyId="+ companyId+"&preprint="+"no"+"&flag="+true;
					}
					else{
						finalURL += "pdcontractrenewal" + "?ContractId="+ contractRenewal.getContractId()+ "&"+"CompanyId="+companyId+"&preprint="+"yes"+"&flag="+true;
					}

	
				}
//				else{
//					if(companyheaderFooterFlag){
//						finalURL += "pdcontractrenewal"	+ "?ContractId=" + contractRenewal.getContractId() + "&"+ "CompanyId=" + model.getCompanyId() + "&"+ "preprint=" + "no&flag=" + false;
//					}
//					else{
//						finalURL += "pdcontractrenewal"	+ "?ContractId=" + contractRenewal.getContractId() + "&"+ "CompanyId=" + model.getCompanyId() + "&"+ "preprint=" + "yes&flag=" + false;
//					}
//
//				}
				
			}
			
			if(finalURL.contains("?ContractId=")){
				return finalURL;
			}
			else{
				logger.log(Level.SEVERE, "Contract Renewal For Standard PDF");

				if(companyheaderFooterFlag){
					return finalURL += "pdcontractrenewal" + "?ContractId="+ contractRenewal.getContractId()+ "&CompanyId="+ companyId+"&"+"preprint="+"no&flag="+false;//Ashwini Patil Date:8-09-2022 yes changed to no
				}
				else{
					return finalURL += "pdcontractrenewal" + "?ContractId="+ contractRenewal.getContractId()+ "&CompanyId="+ companyId+"&"+"preprint="+"no&flag="+false;
				}
			}

			
			}
		}
		
		/**
		 * @author Anil @since 20-09-2021
		 * Adding server side email method for printing pdfs
		 */
		else if(documentName.trim().equals("Sales Quotation")){
			SalesQuotation contractEntity = null;
			if(model!=null){
				contractEntity = (SalesQuotation) model;
			}else{
				contractEntity = ofy().load().type(SalesQuotation.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			
			
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "SalesQuotation")
					.filter("processList.status", true).list();
			
			logger.log(Level.SEVERE, "SalesQuotation processConfiglist size"+processConfiglist.size());

			
			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

			if (processConfiglist.size() == 0) {
				return finalURL +="pdfsales"+"?Id="+contractEntity.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
			}else {
				for (SuperModel model1 : processConfiglist) {
					ProcessConfiguration processConfig = (ProcessConfiguration) model1;
					processList.addAll(processConfig
							.getProcessList());

				}
				
//				for (int k = 0; k < processList.size(); k++) {
//					
//				}
				
				return finalURL +="pdfsales"+"?Id="+contractEntity.getId()+"&"+"type="+"sq"+"&"+"preprint="+"plane";
			}
		}else if(documentName.trim().equals("Sales Order")){
			SalesOrder salesOrderEntity = null;
			if(model!=null){
				salesOrderEntity = (SalesOrder) model;
			}else{
				salesOrderEntity = ofy().load().type(SalesOrder.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			
			
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "SalesOrder")
					.filter("processList.status", true).list();
			
			logger.log(Level.SEVERE, "SalesOrder processConfiglist size"+processConfiglist.size());

			
			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

			if (processConfiglist.size() == 0) {
				return finalURL += "pdfsales"+"?Id="+salesOrderEntity.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane";
			}else {
				for (SuperModel model1 : processConfiglist) {
					ProcessConfiguration processConfig = (ProcessConfiguration) model1;
					processList.addAll(processConfig
							.getProcessList());

				}
				
				for (int k = 0; k < processList.size(); k++) {
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("POPDFV1")&&processList.get(k).isStatus()==true){
						return finalURL +=  "pdfsales"+"?Id="+salesOrderEntity.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane"+"&"+"subtype="+"POPDFV1";
					}
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("SalesOrderUpdatedPdf")&&processList.get(k).isStatus()==true){
						return finalURL +=  "pdfsales"+"?Id="+salesOrderEntity.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane"+"&"+"subtype="+"SalesPdfUpdated";
					}
				}
				
				return finalURL += "pdfsales"+"?Id="+salesOrderEntity.getId()+"&"+"type="+"so"+"&"+"preprint="+"plane";
			}
			
			
		}else if(documentName.trim().equals("Delivery Note")){
			
			DeliveryNote contractEntity = null;
			if(model!=null){
				contractEntity = (DeliveryNote) model;
			}else{
				contractEntity = ofy().load().type(DeliveryNote.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			
			
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "DeliveryNote")
					.filter("processList.status", true).list();
			
			logger.log(Level.SEVERE, "SalesQuotation processConfiglist size"+processConfiglist.size());

			
			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();

			if (processConfiglist.size() == 0) {
				return finalURL +="deliverynotepdf"+"?Id="+contractEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote";
			}else {
				for (SuperModel model1 : processConfiglist) {
					ProcessConfiguration processConfig = (ProcessConfiguration) model1;
					processList.addAll(processConfig
							.getProcessList());

				}
				
				for (int k = 0; k < processList.size(); k++) {
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("POPDFV1")&&processList.get(k).isStatus()==true){
						return finalURL +="deliverynotepdf"+"?Id="+contractEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote"+"&"+"subtype=DELIVERYNOTEV1";
					}
					if(processList.get(k).getProcessType().trim().equalsIgnoreCase("DeliveryNoteUpdatedpdf")&&processList.get(k).isStatus()==true){
						return finalURL += "deliverynotepdf"+"?Id="+contractEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote"+"&"+"subtype=DeliveryNoteUpdated";
					}
				}
				
				return finalURL +="deliverynotepdf"+"?Id="+contractEntity.getId()+"&"+"preprint="+"plane"+"&"+"type=DeliveryNote";
			}
		}
		else if(documentName.trim().equals("Service")){
			

			logger.log(Level.SEVERE, "Inside Service for PDF Link");

			Service serviceEntity = null;
			if(model!=null){
				serviceEntity = (Service) model;
			}
			else{
				serviceEntity = ofy().load().type(Service.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			

			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
									.filter("companyId", companyId).filter("processName", "Service")
									.filter("processList.status", true).list();
			
			logger.log(Level.SEVERE, "processConfiglist size"+processConfiglist.size());

				List<ProcessTypeDetails> processList =new ArrayList<ProcessTypeDetails>();
				if(processConfiglist.size()==0){
					
					return finalURL += "pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId();
					
					
				}
				else{
					
					for(SuperModel model1:processConfiglist)
					{
						ProcessConfiguration processConfig=(ProcessConfiguration)model1;
						processList.addAll(processConfig.getProcessList());
						
					}
					logger.log(Level.SEVERE, "processList size"+processList.size());
	
				for(int k=0;k<processList.size();k++){	
					
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForPecopp")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL +="pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId()+"&"+"preprint="+"no";
					}
					else{
						finalURL +="pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId()+"&"+"preprint="+"yes";;
					}
				
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("OnlyForNBHC")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL +="pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId();
					}
					else{
						finalURL += "serviceRecord"+"?Id="+serviceEntity.getId()+"&"+"preprint="+"yes";
					}
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("ComplainServiceWithTurnAroundTime")&&processList.get(k).isStatus()==true){
					if(companyheaderFooterFlag){
						finalURL +="pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId()+"&"+"preprint="+"no";
					}
					else{
						finalURL +="pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId()+"&"+"preprint="+"yes";;
					}
				}
				
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PrintServiceJobCardPdf")&&processList.get(k).isStatus()==true){
						finalURL += "pdfserjob"+ "?Id="+ serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId(); 
				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("JobCompletionCertificate")&&processList.get(k).isStatus()==true){
						finalURL += "pdfjobcerti"+ "?Id="+ serviceEntity.getId();
	
				}
				

				}
				
				if(finalURL.contains("?Id=")){
					return finalURL;
				}
				else{
					logger.log(Level.SEVERE, "Service For Standard PDF");
					if(companyheaderFooterFlag){
						return finalURL += "pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId()+"&"+"preprint="+"no";
					}
					else{
						return finalURL += "pdfCustserjob"+"?Id="+serviceEntity.getId()+"&"+"companyId="+serviceEntity.getCompanyId()+"&"+"preprint="+"yes";
					}
				}
			}
			
		
		}
		else if(documentName.trim().equals("Payment")){

			
			CustomerPayment paymentEntity = null;
			if(model!=null){
				paymentEntity = (CustomerPayment) model;
			}
			else{
				paymentEntity = ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "CustomerPayment")
					.filter("processList.status", true).list();
			logger.log(Level.SEVERE, "payment processConfiglist size"+processConfiglist.size());

			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();


			if(processConfiglist.size()==0){
				logger.log(Level.SEVERE, "inside no process config condition "+processConfiglist.size());
				 finalURL += "customerpayslip"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"plane";
					logger.log(Level.SEVERE, "finalURL"+finalURL);

				 return finalURL;
				
			}
			else{
			
				for(SuperModel model1:processConfiglist)
				{
					ProcessConfiguration processConfig=(ProcessConfiguration)model1;
					processList.addAll(processConfig.getProcessList());
					
				}
			
			for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("EnableCustomPaymentReciept")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "EnableCustomPaymentReciept");

					if(companyheaderFooterFlag){
						finalURL += "custompayslip"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"no"; 
					}
					else{
						finalURL += "custompayslip"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"yes"; 
					}
	
				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("HygeiaPestManagement")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "HygeiaPestManagement");

					if(companyheaderFooterFlag){
						finalURL += "pdfreceipt"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"no";
					}
					else{
						finalURL += "pdfreceipt"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"yes";
					}
	
				
				}

				
			}
			
			logger.log(Level.SEVERE, "finalURL == "+finalURL);

			if(finalURL.contains("?Id=")){
				return finalURL;
			}
			else{
				
				logger.log(Level.SEVERE, "Customer Payment For Standard PDF");
				if(companyheaderFooterFlag){
					return finalURL += "customerpayslip"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"no"; 
				}
				else{
					return finalURL += "customerpayslip"+"?Id="+paymentEntity.getId()+"&"+"preprint="+"yes"; 
				}
			}
			
			}
		
		}
		else if(documentName.trim().equals("Salary Slip")){
			logger.log(Level.SEVERE, "Inside Salary slip condition ");

			PaySlip salarySlip = null;
			if(model!=null){
				salarySlip = (PaySlip) model;
			}
			else{
				salarySlip = ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", documentId).filter("salaryPeriod", salarySlipMonth)
									.first().now();
			}
			if(salarySlip==null) {
				return " Payroll not found or not generated for "+salarySlipMonth;
			}
			finalURL += "processpayslip" + "?Id="+ salarySlip.getId()+ "&" + "status=" + "false";
			logger.log(Level.SEVERE, "Salary Slip finalURL"+finalURL);
			return finalURL;
			
		}

		else if(documentName.trim().equals("Salary Slip")){
			logger.log(Level.SEVERE, "Inside Salary slip condition ");

			PaySlip salarySlip = null;
			if(model!=null){
				salarySlip = (PaySlip) model;
			}
			else{
				salarySlip = ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("empid", documentId).filter("salaryPeriod", salarySlipMonth)
									.first().now();
			}
			if(salarySlip==null) {
				return " Payroll not found or not generated for "+salarySlipMonth;
			}
			finalURL += "processpayslip" + "?Id="+ salarySlip.getId()+ "&" + "status=" + "false";
			logger.log(Level.SEVERE, "Salary Slip finalURL"+finalURL);
			return finalURL;
			
		}
		
		else if(documentName.trim().equals("Assessment Report")){
			logger.log(Level.SEVERE, "Inside AssesmentReport condition ");

			AssesmentReport assessmentReport = null;
			if(model!=null){
				assessmentReport = (AssesmentReport) model;
			}
			else{
				assessmentReport = ofy().load().type(AssesmentReport.class).filter("companyId", companyId).filter("count", documentId).first().now();
			}
			if(assessmentReport==null) {
				return " Assessment not found or not generated for "+documentId;
			}
			
			if(companyheaderFooterFlag){
				finalURL += "assessmentReport"+"?Id="+assessmentReport.getId()+"&"+"preprint="+"no"; 
			}
			else{
				finalURL += "assessmentReport"+"?Id="+assessmentReport.getId()+"&"+"preprint="+"yes"; 
			}
			logger.log(Level.SEVERE, "Salary Slip finalURL"+finalURL);
			return finalURL;
			
			
		}
		else if(documentName.trim().equals("Purchase Order")){
			
			PurchaseOrder purchaseorder = null;
			if(model!=null){
				purchaseorder = (PurchaseOrder) model;
			}
			else{
				purchaseorder = ofy().load().type(PurchaseOrder.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "PurchaseOrder")
					.filter("processList.status", true).list();
			logger.log(Level.SEVERE, "payment processConfiglist size"+processConfiglist.size());

			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();


			if(processConfiglist.size()==0){
				logger.log(Level.SEVERE, "inside no process config condition "+processConfiglist.size());
//					finalURL = "pdfpurchase" + "?Id=" + purchaseorder.getId()+"&"+"preprint="+"plane";
				if(companyheaderFooterFlag){
					finalURL = "pdfpurchase" + "?Id=" + purchaseorder.getId()+"&"+"preprint="+"no";
				}
				else{
					finalURL = "pdfpurchase" + "?Id=" + purchaseorder.getId()+"&"+"preprint="+"yes";
				}
				logger.log(Level.SEVERE, "finalURL"+finalURL);
				 return finalURL;
				
			}
			else{
			
				for(SuperModel model1:processConfiglist)
				{
					ProcessConfiguration processConfig=(ProcessConfiguration)model1;
					processList.addAll(processConfig.getProcessList());
					
				}
			
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("POPDFV1")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "POPDFV1");

					if(companyheaderFooterFlag){
						finalURL += "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"POPDFV1"+"&"+"preprint=no";
					}
					else{
						finalURL += "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"POPDFV1"+"&"+"preprint=yes";
					}
	
				}
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("PurchaseOrderUpdatedPdf")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "PurchaseOrderUpdatedPdf");

					if(companyheaderFooterFlag){
						finalURL += "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"PurchaseOrderUpdatedPdf"+"&"+"preprint=no";;
					}
					else{
						finalURL += "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"PurchaseOrderUpdatedPdf"+"&"+"preprint=yes";
					}
	
				
				}
				
			  }
			
				logger.log(Level.SEVERE, "finalURL == "+finalURL);
	
				if(finalURL.contains("?Id=")){
					return finalURL;
				}
				else{
					
					logger.log(Level.SEVERE, "purchase order For Standard PDF");
					if(companyheaderFooterFlag){
						 finalURL += "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"POPDFV1"+"&"+"preprint=no";
					}
					else{
						 finalURL += "pdfpurchase" + "?Id=" + model.getId()+"&"+"type="+"POPDFV1"+"&"+"preprint=yes";
					}
					logger.log(Level.SEVERE, "finalURL"+finalURL);
					 return finalURL;

				}
			
		}
		
		
		}
		else if(documentName.trim().equals("GRN")){
			
			GRN grn = null;
			if(model!=null){
				grn = (GRN) model;
			}
			else{
				grn = ofy().load().type(GRN.class).filter("companyId", companyId).filter("count", documentId)
									.first().now();
			}
			List<ProcessConfiguration> processConfiglist = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", companyId).filter("processName", "GRN")
					.filter("processList.status", true).list();
			logger.log(Level.SEVERE, "payment processConfiglist size"+processConfiglist.size());

			List<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();


			if(processConfiglist.size()==0){
				logger.log(Level.SEVERE, "inside no process config condition "+processConfiglist.size());
//					finalURL = "pdfpurchase" + "?Id=" + purchaseorder.getId()+"&"+"preprint="+"plane";
				if(companyheaderFooterFlag){
					finalURL = "grn" + "?Id=" + model.getId()+"&"+"preprint="+"no";;
				}
				else{
					finalURL = "grn" + "?Id=" + model.getId()+"&"+"preprint="+"yes";;
				}
				logger.log(Level.SEVERE, "finalURL"+finalURL);
				 return finalURL;
				
			}
			else{
			
				for(SuperModel model1:processConfiglist)
				{
					ProcessConfiguration processConfig=(ProcessConfiguration)model1;
					processList.addAll(processConfig.getProcessList());
					
				}
			
				for(int k=0;k<processList.size();k++){	
				if(processList.get(k).getProcessType().trim().equalsIgnoreCase("GRNUpdated")&&processList.get(k).isStatus()==true){
					logger.log(Level.SEVERE, "POPDFV1");

					if(companyheaderFooterFlag){
						finalURL += "grn" + "?Id=" + model.getId()+"&type=GRNUpdated"+"&"+"preprint=no";
					}
					else{
						finalURL += "grn" + "?Id=" + model.getId()+"&type=GRNUpdated"+"&"+"preprint=yes";
					}
	
				}
				
			  }
			
				logger.log(Level.SEVERE, "finalURL == "+finalURL);
	
				if(finalURL.contains("?Id=")){
					return finalURL;
				}
				else{
					
					logger.log(Level.SEVERE, "GRN For Standard PDF");
					if(companyheaderFooterFlag){
						finalURL += "grn" + "?Id=" + model.getId()+"&"+"preprint="+"no";
					}
					else{
						finalURL += "grn" + "?Id=" + model.getId()+"&"+"preprint="+"yes";
					}
					logger.log(Level.SEVERE, "finalURL"+finalURL);
					 return finalURL;

				}
			
		}
		
		
		}

		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "Document id does not exist";
	}
	
	public String callPaymnetGatewayUniqueIdAPI(String jsonData, SuperModel model, String entityName){
		
		logger.log(Level.SEVERE,"Inside digital payment request condition");

		final String paymentGatewayAPI = AppConstants.PAYMENETGATEWAYAPI;
		String data = "";
		
		URL url = null;
		try {
			url = new URL(paymentGatewayAPI);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "MalformedURLException ERROR::::::::::"
					+ e);
			e.printStackTrace();
		}
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestProperty("Content-Type",
				"application/json; charset=UTF-8");
		try {
			con.setRequestMethod("POST");
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "ProtocolException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		OutputStream os = null;
		try {
			os = con.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
		// Log.d("Test", "From Get Post Method" + getPostData(values));
		try {
//			writer.write(createLeadJSONObject(leadEntity,AppURL,appId,company,customer));
			writer.write(jsonData);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException2 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		try {
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException3 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		InputStream is = null;
		try {
			is = con.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException4 ERROR::::::::::" + e);
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String temp;

		try {
			while ((temp = br.readLine()) != null) {
				data = data + temp;
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
			e.printStackTrace();
		}

		logger.log(Level.SEVERE, "Data:::::::::: Unique Id " + data);
		data = data.replace("\"", "");

		logger.log(Level.SEVERE, "Data::::: Unique Id  = " + data);
		/**
		 * @author Vijay  Date :- 10-12-2021
		 * Des :- if there is any data issue to handle error updated the code
		 *
		 */
		if(data.contains("SyntaxError: Unexpected")){
			return null;
		}
		/**
		 * ends here
		 */
		String paymentGatewayUniqueId = "";
		try {
			
			paymentGatewayUniqueId = data.replace("\"", "");

	
			logger.log(Level.SEVERE, "paymentGatewayuniqueId"+ paymentGatewayUniqueId);
	
			if (!paymentGatewayUniqueId.equals("")) {
				if(entityName.trim().equals(AppConstants.LEAD)){
					Lead leadEntity = (Lead) model;
					leadEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
					ofy().save().entity(leadEntity);
					logger.log(Level.SEVERE,"lead entity updated succesfully with paymentGatewayuniqueId");
				}
				else if(entityName.trim().equals(AppConstants.QUOTATION)){
					Quotation quotationEntity = (Quotation) model;
					quotationEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
					ofy().save().entity(quotationEntity);
					logger.log(Level.SEVERE,"Quotation entity updated succesfully with paymentGatewayuniqueId");
				}
				else if(entityName.trim().equals(AppConstants.CONTRACT)){
					Contract contractEntity = (Contract) model;
					contractEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
					ofy().save().entity(contractEntity);
					logger.log(Level.SEVERE,"Contract entity updated succesfully with paymentGatewayuniqueId");
				}
				else if(entityName.trim().equals(AppConstants.CONTRACT_RENEWAL)){
					ContractRenewal contractRenewal = (ContractRenewal) model;
					contractRenewal.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
					ofy().save().entity(contractRenewal);
					logger.log(Level.SEVERE,"Contract renewal entity updated succesfully with paymentGatewayuniqueId");
				}
				else if(entityName.trim().equals("Invoice")){
					Invoice invoiceEntity = (Invoice) model;
					invoiceEntity.setPaymentGatewayUniqueId(paymentGatewayUniqueId);
					ofy().save().entity(invoiceEntity);
					logger.log(Level.SEVERE,"Invoice entity updated succesfully with paymentGatewayuniqueId");
				}
				
			}

			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE,"parsing Error!!");
		}
		
		return data;

	}
	
	public String validateNumberRange(long companyId, String documentName) {
		CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
		String error ="";
		error = commonserviceimpl.validateInternetNumberRange(companyId,documentName);
		if(!error.trim().equals("") && error.trim().length()>0){
			return error;
		}
		return error;
	}

	public String validatePaymentDocument(String entityName, int documentId, long companyId) {
		int contractId = 0;
		logger.log(Level.SEVERE,"entityName "+entityName);
		logger.log(Level.SEVERE,"documentId "+documentId);

		if(entityName.trim().equals(AppConstants.LEAD)){
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
						.filter("leadCount", documentId).first().now();
			if(contractEntity!=null){
				contractId = contractEntity.getCount();
				if(contractEntity.getStatus().equals(Contract.CREATED)){
					return "Can not send payment gateway email. Contract already created for this lead contract id - "+contractEntity.getCount();
				}
			}
		}
		else if(entityName.trim().equals(AppConstants.QUOTATION)){
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
						.filter("quotationCount", documentId).first().now();
			if(contractEntity!=null){
				contractId = contractEntity.getCount();
				if(contractEntity.getStatus().equals(Contract.CREATED)){
					return "Can not send payment gateway email. Contract already created for this quotation contract id - "+contractEntity.getCount();
				}
			}
		}
		else if(entityName.trim().equals(AppConstants.CONTRACT)){
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
						.filter("count", documentId).first().now();
			if(contractEntity!=null){
				contractId = contractEntity.getCount();
				if(contractEntity.getStatus().equals(Contract.CREATED)){
					return "Can not send payment gateway email. Contract already created for this contract contract id - "+contractEntity.getCount();
				}
			}
		}
		else if(entityName.trim().equals(AppConstants.CONTRACT_RENEWAL)){
			Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
						.filter("refContractCount", documentId).first().now();
			if(contractEntity!=null){
				contractId = contractEntity.getCount();
				if(contractEntity.getStatus().equals(Contract.CREATED)){
					return "Can not send payment gateway email. Contract already created for this contract contract id - "+contractEntity.getCount();
				}
			}
		}
		else if(entityName.trim().equals("Invoice")){
			CustomerPayment customerPayment = ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
					.filter("invoiceCount", documentId).first().now();
			logger.log(Level.SEVERE,"customerPayment "+customerPayment);
			logger.log(Level.SEVERE,"customerPayment "+customerPayment.getAmountTransferDate());
			if(customerPayment!=null && customerPayment.getAmountTransferDate()!=null){
					return "Can not send payment gateway email. Payment already processed for this invoice invoice id - "+documentId;
			}
		}

		else if(entityName.trim().equals("CustomerPayment")){
			CustomerPayment customerPayment = ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
					.filter("count", documentId).first().now();
			logger.log(Level.SEVERE,"customerPayment "+customerPayment);
			logger.log(Level.SEVERE,"customerPayment "+customerPayment.getAmountTransferDate());
			if(customerPayment!=null && customerPayment.getAmountTransferDate()!=null){
					return "Can not process payment. Payment already processed for this payment document id - "+documentId;
			}
		}
		logger.log(Level.SEVERE,"contractId "+contractId);

		if(contractId!=0){
			List<CustomerPayment> customerpaymentlist = ofy().load().type(CustomerPayment.class)
					.filter("companyId", companyId).filter("contractCount", contractId).list();
			if(customerpaymentlist.size()>0){
				return "Can not send payment gateway email. Payment document already exist for this "+entityName;

			}
		}
		

		return null;
	}
	

	@Override
	public String implementationScreenOperation(String actionTask,long companyId,boolean status) {
		System.out.println("inside implementation operation"+actionTask);
		if(actionTask.trim().equals("Define Branch As Company")){
			ProcessConfiguration processConfig = ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", companyId)
					.filter("processName", "Branch").first().now();
			boolean flag = false;
			if(processConfig!=null){
					for(ProcessTypeDetails procesType : processConfig.getProcessList()){
						if(procesType.getProcessName().equals("Branch") && procesType.getProcessType().equalsIgnoreCase("BranchAsCompany")){
							flag = true;
							break;
						}
					}
			}
			if(flag){
				processConfig.setConfigStatus(true);
				for(ProcessTypeDetails procesType : processConfig.getProcessList()){
					if(procesType.getProcessName().equals("Branch") && procesType.getProcessType().equalsIgnoreCase("BranchAsCompany")){
						procesType.setStatus(status);
					}
				}
				ofy().save().entities(processConfig);
				if(status){
					return "Branch as company activated sucessfully";
				}
				else{
					return "Branch as company deactivated sucessfully";
				}

			}
			else{
				if(processConfig!=null && !flag){
					List<ProcessTypeDetails> list = processConfig.getProcessList();
					ProcessTypeDetails processtype = new ProcessTypeDetails();
					processtype.setProcessName("Branch");
					processtype.setProcessType("BranchAsCompany");
					processtype.setStatus(status);
					list.add(processtype);
					processConfig.setConfigStatus(true);
					processConfig.setProcessList(list);
					ofy().save().entities(processConfig);
					
				}
				else{
					ProcessConfiguration processconfig = new ProcessConfiguration();
					processconfig.setProcessName("Branch");
					processconfig.setConfigStatus(true);
					
					List<ProcessTypeDetails> list = processconfig.getProcessList();
					ProcessTypeDetails processtype = new ProcessTypeDetails();
					processtype.setProcessName("Branch");
					processtype.setProcessType("BranchAsCompany");
					processtype.setStatus(true);
					list.add(processtype);
					processconfig.setConfigStatus(true);
					processconfig.setProcessList(list);
					ofy().save().entities(processconfig);

				}
			}	
			if(status){
				return "Branch as company activated sucessfully";
			}
			else{
				return "Branch as company deActivated sucessfully";
			}
		}
		
		/**
		 * @author Ashwini
		 * @since 12-1-2022
		 * to Manage PC_CITY_NOT_MANDATORY process configuration via service/Implementation screen
		 */
		else if(actionTask.trim().equals("City Not Mandatory")){
			System.out.println("in if (actionTask.trim().equals(City Not Mandatory))");
			ProcessConfiguration processConfig = ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", companyId)
					.filter("processName", "Customer").first().now();
			boolean flag = false;
			if(processConfig!=null){
					for(ProcessTypeDetails procesType : processConfig.getProcessList()){
						if(procesType.getProcessName().equals("Customer") && procesType.getProcessType().equalsIgnoreCase("PC_CITY_NOT_MANDATORY")){
							flag = true;
							break;
						}
					}
			}
			if(flag){
				System.out.println("in if(flag)");
				processConfig.setConfigStatus(true);
				for(ProcessTypeDetails procesType : processConfig.getProcessList()){
					if(procesType.getProcessName().equals("Customer") && procesType.getProcessType().equalsIgnoreCase("PC_CITY_NOT_MANDATORY")){
						procesType.setStatus(status);
					}
				}
				ofy().save().entities(processConfig);
				if(status){
					return "City Not Mandatory activated sucessfully";
				}
				else{
					return "City Not Mandatory deactivated sucessfully";
				}

			}
			else{
				if(processConfig!=null && !flag){
					System.out.println("in if(processConfig!=null && !flag)");
					List<ProcessTypeDetails> list = processConfig.getProcessList();
					ProcessTypeDetails processtype = new ProcessTypeDetails();
					processtype.setProcessName("Customer");
					processtype.setProcessType("PC_CITY_NOT_MANDATORY");
					processtype.setStatus(status);
					list.add(processtype);
					processConfig.setConfigStatus(true);
					processConfig.setProcessList(list);
					ofy().save().entities(processConfig);
					
				}
				else{
					System.out.println("in else of if(processConfig!=null && !flag)");
					ProcessConfiguration processconfig = new ProcessConfiguration();
					processconfig.setProcessName("Customer");
					processconfig.setConfigStatus(true);
					
					List<ProcessTypeDetails> list = processconfig.getProcessList();
					ProcessTypeDetails processtype = new ProcessTypeDetails();
					processtype.setProcessName("Customer");
					processtype.setProcessType("PC_CITY_NOT_MANDATORY");
					processtype.setStatus(true);
					list.add(processtype);
					processconfig.setConfigStatus(true);
					processconfig.setProcessList(list);
					ofy().save().entities(processconfig);

				}
			}	
			if(status){
				return "City Not Mandatory activated sucessfully";
			}
			else{
				return "City Not Mandatory deactivated sucessfully";
			}
				
		}else if(actionTask.trim().equals("Tax Number Range")){
			
			checkAndCreateBillingNonBillingConfigs(companyId);
			
			activateDeactivateProcessConfiguration(companyId,"NumberRange","MakeNumberRangeMandatory",true);
			
			ArrayList<String> list = new ArrayList<String>();
			list.add("C_Billing");
			list.add("S_Billing");
			list.add("B_Billing");
			list.add("I_Billing");
			list.add("P_Billing");
			List<NumberGeneration> numbergenlist = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
													.filter("processName IN", list).list();
			logger.log(Level.SEVERE,"numbergenlist size"+numbergenlist.size());
			int year = Calendar.getInstance().get(Calendar.YEAR);
			String strcurrentYearNumber = 1+""+year+""+"0000";
			long currentYearNumber = Long.parseLong(strcurrentYearNumber);
			logger.log(Level.SEVERE,"strcurrentYearNumber"+strcurrentYearNumber);

			if(numbergenlist.size()!=0){
				for(NumberGeneration numGen:numbergenlist){
					if(numGen.getProcessName().contains("C_Billing")){
						long number = numGen.getNumber();
						int lastDigit = (int) (number%10);
						if(lastDigit >0){
							return "Existing number range used for contracts. Can not update number range with 1YYYY0000";
							
						}
					}
				}
				for(NumberGeneration numGen:numbergenlist){
					if(numGen.getProcessName().equals("C_Billing") || numGen.getProcessName().equals("S_Billing") ||
							 numGen.getProcessName().equals("B_Billing") || numGen.getProcessName().equals("I_Billing") ||
							 numGen.getProcessName().equals("P_Billing") ){
						if(numGen.getNumber()==200000000){
							numGen.setNumber(currentYearNumber);
						}
					}
					
				}
				
			}
			else{
				ArrayList<NumberGeneration> numberGenration = new ArrayList<NumberGeneration>();
				for(String BillingNumberGen :list){
					NumberGeneration numgenration = new NumberGeneration();
					numgenration.setProcessName(BillingNumberGen);
					numgenration.setNumber(currentYearNumber);
					numgenration.setStatus(true);
					numgenration.setCompanyId(companyId);
					numberGenration.add(numgenration);
				}
				ArrayList<SuperModel> modellist = new ArrayList<SuperModel>();
				modellist.addAll(numberGenration);
				
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(modellist);
			}
			return "Tax Number range activated sucessfully";

		}
		else if(actionTask.trim().equals("Non Tax Number Range")){
			checkAndCreateBillingNonBillingConfigs(companyId);
			activateDeactivateProcessConfiguration(companyId,"NumberRange","MakeNumberRangeMandatory",true);

			ArrayList<String> list = new ArrayList<String>();
			list.add("C_NonBilling");
			list.add("S_NonBilling");
			list.add("B_NonBilling");
			list.add("I_NonBilling");
			list.add("P_NonBilling");
			List<NumberGeneration> numbergenlist = ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
													.filter("processName IN", list).list();
			logger.log(Level.SEVERE,"numbergenlist size"+numbergenlist.size());
			int year = Calendar.getInstance().get(Calendar.YEAR);
			String strcurrentYearNumber = 8+""+year+""+"0000";
			long currentYearNumber = Long.parseLong(strcurrentYearNumber);
			logger.log(Level.SEVERE,"strcurrentYearNumber"+strcurrentYearNumber);

			if(numbergenlist.size()!=0){
				for(NumberGeneration numGen:numbergenlist){
					if(numGen.getProcessName().contains("C_NonBilling")){
						long number = numGen.getNumber();
						int lastDigit = (int) (number%10);
						if(lastDigit >0){
							return "Existing number range used for contracts. Can not update number range with 8YYYY0000";
							
						}
					}
				}
				for(NumberGeneration numGen:numbergenlist){
					if(numGen.getProcessName().equals("C_NonBilling") || numGen.getProcessName().equals("S_NonBilling") ||
							 numGen.getProcessName().equals("B_NonBilling") || numGen.getProcessName().equals("I_NonBilling") ||
							 numGen.getProcessName().equals("P_NonBilling") ){
						if(numGen.getNumber()==200000000){
							numGen.setNumber(currentYearNumber);
						}
					}
					
				}
				
			}
			else{
				ArrayList<NumberGeneration> numberGenration = new ArrayList<NumberGeneration>();
				for(String BillingNumberGen :list){
					NumberGeneration numgenration = new NumberGeneration();
					numgenration.setProcessName(BillingNumberGen);
					numgenration.setNumber(currentYearNumber);
					numgenration.setStatus(true);
					numgenration.setCompanyId(companyId);
					numberGenration.add(numgenration);
				}
				ArrayList<SuperModel> modellist = new ArrayList<SuperModel>();
				modellist.addAll(numberGenration);
				
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(modellist);
			}

			return "Non Tax number range activated sucessfully";

		}
		else if(actionTask.trim().equals("One Time Service Contract")){
			System.out.println("inside one time service");
			String str = createContractGroupConfigs(companyId,status);
			return str;
		}
		else if(actionTask.trim().equals("Renewal Reminder To Client(SMS)")){
			String msg = "";
			msg =  validateAndActivateSMSRenewalReminder(companyId,status);
			return msg;
		}
		else if(actionTask.trim().equals("Enable Pedio")){
			return reactonEnablePedio(companyId,status);
		}
		
		return null;
	}

	private String reactonEnablePedio(long companyId, boolean status) {
		ArrayList<String> strlist = new ArrayList<String>();
		strlist.add("Technician");
		strlist.add("Operator");

		List<UserRole> userroleEntitylist = ofy().load().type(UserRole.class).filter("companyId", companyId)
						.filter("roleName IN", strlist).list();
		logger.log(Level.SEVERE, "userrolelist size "+userroleEntitylist.size());
		
		boolean flag = false;
		
		if(userroleEntitylist.size()!=0){
			
				for(UserRole userrole : userroleEntitylist){
					if(strlist.contains(userrole.getRoleName())){
						strlist.remove(userrole.getRoleName());
					}
				}
				
				if(strlist.size()>0){
					for(String userrolename : strlist){
						flag = true;
						
						UserRole userRole = new UserRole();
						userRole.setCompanyId(companyId);
						userRole.setRoleName(userrolename);
						userRole.setRoleStatus(status);
						ofy().save().entity(userRole);
					}
				}
				else {
					// updating status existing user role Technician/Operator
					for(UserRole userRole : userroleEntitylist) {
						userRole.setRoleStatus(status);
					}
					ofy().save().entities(userroleEntitylist);
				}
				
		}
		else{
			for(String userrolename : strlist){
				flag = true;
				UserRole userRole = new UserRole();
				userRole.setCompanyId(companyId);
				userRole.setRoleName(userrolename);
				userRole.setRoleStatus(status);
				ofy().save().entity(userRole);
			}
		}
		
		if(userroleEntitylist.size()==2 && status==false){
			return "User role technician/operator status deactiavted successfully";
		}
		
		if(flag){
			return "User role technician/operator created successfully";
		}
		else{
			return "User role technician/operator already exist";
		}
	}

	private String validateAndActivateSMSRenewalReminder(long companyId,boolean status) {
		
		List<SmsConfiguration> smsconfiglist = ofy().load().type(SmsConfiguration.class).filter("status", true).filter("companyId", companyId).list();
		if(smsconfiglist.size()==0){
			return "SMS Configuration not activated. kindly contact to EVA support team";
		}
		activateDeactivateProcessConfigs("CronJob", "ContractRenewalSMS", status, companyId, "Contract renewal due SMS");
		if(status){
			return "Contract renewal SMS process configs activated successfully";
		}
		else{
			return "Contract renewal SMS process configs deactivated successfully";
		}
	}

	private String createContractGroupConfigs(long companyId, boolean status) {
		try {
			
		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Config").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	int count = (int) lastNumber;
    	
		List<String> contractGrouplist = getOneTimeConfigList();
		List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
				.filter("type", 31).list();
		System.out.println("configEntity size"+configEntity.size());
		if(configEntity!=null && configEntity.size()!=0){
			for(int i=0;i<configEntity.size();i++){
	    			if(contractGrouplist.contains(configEntity.get(i).getName().trim())){
	    				contractGrouplist.remove(configEntity.get(i).getName().trim());
	    				configEntity.get(i).setStatus(status);
	    			}
	    	}
		}
		if(configEntity!=null && !status){
			ofy().save().entities(configEntity);
		}
		else{
			for(String articleType : contractGrouplist){
	    		int setcount = ++count;
	    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.CONTRACTGROUP), articleType, companyId,setcount);
	    		
	    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
	    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
	    	}
			
			numbergen.setNumber(count);
	  		GenricServiceImpl impl = new GenricServiceImpl();
	  		impl.save(numbergen);
		}
  		
		} catch (Exception e) {
			e.printStackTrace();
			return "Please try again "+e.getMessage();
		}

		if(status){
			return "Single Or One Time service contract activated successfully";
		}
		else{
			return "Single Or One Time service contract deactivated successfully";
		}
	
	}

	private List<String> getOneTimeConfigList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("One Time");
		return list;
	}

	private void activateDeactivateProcessConfiguration(long companyId,String processName, String processType, boolean status) {

		ProcessConfiguration processConfig = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName", processName).first().now();
		logger.log(Level.SEVERE, "processConfig "+processConfig);
		boolean flag = false;
		if(processConfig!=null){
				for(ProcessTypeDetails procesType : processConfig.getProcessList()){
					if(procesType.getProcessName().equals(processName) && procesType.getProcessType().equalsIgnoreCase(processType)){
						flag = true;
						break;
					}
				}
		}
		if(flag){
			processConfig.setConfigStatus(true);
			for(ProcessTypeDetails procesType : processConfig.getProcessList()){
				if(procesType.getProcessName().equals(processName) && procesType.getProcessType().equalsIgnoreCase(processType)){
					procesType.setStatus(status);
				}
			}
			ofy().save().entities(processConfig);
			logger.log(Level.SEVERE, "process Config updated successfully ");

		}
		else{
			if(processConfig!=null && !flag){
				List<ProcessTypeDetails> list = processConfig.getProcessList();
				ProcessTypeDetails processtype = new ProcessTypeDetails();
				processtype.setProcessName(processName);
				processtype.setProcessType(processType);
				processtype.setStatus(status);
				list.add(processtype);
				processConfig.setConfigStatus(true);
				processConfig.setProcessList(list);
				processConfig.setCompanyId(companyId);

				ofy().save().entities(processConfig);
				logger.log(Level.SEVERE, "process Config updated successfully. ");

			}
			else{
				ProcessConfiguration processconfig = new ProcessConfiguration();
				processconfig.setProcessName(processName);
				processconfig.setConfigStatus(true);
				
				List<ProcessTypeDetails> list = processconfig.getProcessList();
				ProcessTypeDetails processtype = new ProcessTypeDetails();
				processtype.setProcessName(processName);
				processtype.setProcessType(processType);
				processtype.setStatus(true);
				list.add(processtype);
				processconfig.setConfigStatus(true);
				processconfig.setProcessList(list);
				processconfig.setCompanyId(companyId);
				
				ofy().save().entities(processconfig);
				logger.log(Level.SEVERE, "process Config updated successfully.. ");


			}
		}	
		
		if(processName.equals("Service") && processType.equalsIgnoreCase("UpdateFindingEntity")){
			logger.log(Level.SEVERE, "In ServiceFinding process creation");
			NumberGeneration ng=ofy().load()
					.type(NumberGeneration.class)
					.filter("companyId", companyId)
					.filter("processName", "ServiceFinding").first().now();
			
			if(ng==null) {
				ProcessName pname=new ProcessName();
				pname.setCompanyId(companyId);
				pname.setStatus(true);
				pname.setProcessName("ServiceFinding");
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
					
				int count=(int) numGen.getLastUpdatedNumber("ProcessName", companyId, (long)1);
				pname.setCount(count);
				ofy().save().entity(pname);
				
				NumberGeneration finding=new NumberGeneration();
				finding.setCompanyId(companyId);
				finding.setStatus(true);
				finding.setProcessName("ServiceFinding");
				finding.setNumber(100000000);
				ofy().save().entity(finding);
				logger.log(Level.SEVERE, "ServiceFinding process name and number generation created!");
			}
		}
	}

	private void checkAndCreateBillingNonBillingConfigs(long companyId) {

		NumberGeneration numbergen = null;	
		numbergen = ofy().load().type(NumberGeneration.class).filter("companyId",companyId).filter("processName", "Config").filter("status", true).first().now();
		long lastNumber = numbergen.getNumber();
		logger.log(Level.SEVERE,"Last Number===="+lastNumber);
    	int count = (int) lastNumber;
    	
		List<String> numberRangelist = getNumberRangeConfigList();
		List<Config> configEntity = ofy().load().type(Config.class).filter("companyId", companyId)
				.filter("type", 91).list();

		if(configEntity.size()!=0){
			for(int i=0;i<configEntity.size();i++){
	    			if(numberRangelist.contains(configEntity.get(i).getName().trim())){
	    				numberRangelist.remove(configEntity.get(i).getName().trim());
	    			}
	    	}
		}
		for(String articleType : numberRangelist){
    		int setcount = ++count;
    		String configInstring = getConfigDataForTaskQueue(ConfigTypes.getConfigType(Screen.NUMBERRANGE), articleType, companyId,setcount);
    		
    		Queue queue = QueueFactory.getQueue("ConfigValue-queue");
    		queue.add(TaskOptions.Builder.withUrl("/slick_erp/configvaluetaskqueue").param("configValuekey", configInstring));
    	}
		
		numbergen.setNumber(count);
  		GenricServiceImpl impl = new GenricServiceImpl();
  		impl.save(numbergen);
	}

	private String getConfigDataForTaskQueue(int type, String name, Long companyId, int setcount) {

		 String configName = new String();
		 configName = type +"$" +name +"$"+companyId+"$"+setcount;
		 return configName;
	
	}

	private List<String> getNumberRangeConfigList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Billing");
		list.add("NonBilling");
		return list;
	}

	@Override
	public String enableDisableSelfApproval(ArrayList<ProcessName> list, long companyId, boolean status,String processType) {
		for(ProcessName name : list){
			activateDeactivateProcessConfiguration(companyId, name.getProcessName(), processType, name.isStatus());
		}
		return processType+" updated sucessfully";
	}

	@Override
	public String activateDeactivateProcessConfiguration(ArrayList<ProcessTypeDetails> processlist, long companyId) {
		
		HashSet<String> hsprocessNamelist = new HashSet<String>();
		for(ProcessTypeDetails processType : processlist){
			hsprocessNamelist.add(processType.getProcessName());
		}
		String processName ="";
		if(hsprocessNamelist.size()>1){
			
			updateProcessConfiguration(hsprocessNamelist,processlist,companyId);
			return "Process configuration updated successfully";

		}
		else{
			processName = processlist.get(0).getProcessName();
	
		}
		System.out.println("processName == "+processName);
		System.out.println("company id"+companyId);
		
		ProcessConfiguration processConfig = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName", processName).first().now();
		System.out.println("processConfig =="+processConfig);
		for(ProcessTypeDetails processNamelist : processlist){
			boolean flag = false;
			if(processConfig!=null){
					for(ProcessTypeDetails procesType : processConfig.getProcessList()){
						if(procesType.getProcessName().equals(processName) && procesType.getProcessType().equalsIgnoreCase(processNamelist.getProcessType())){
							flag = true;
							break;
						}
					}
			}
			if(flag){
				processConfig.setConfigStatus(true);
				for(ProcessTypeDetails procesType : processConfig.getProcessList()){
					if(procesType.getProcessName().equals(processName) && procesType.getProcessType().equalsIgnoreCase(processNamelist.getProcessType())){
						procesType.setStatus(processNamelist.isStatus());
					}
				}
				ofy().save().entities(processConfig);

			}
			else{
				if(processConfig!=null && !flag){
					List<ProcessTypeDetails> list = processConfig.getProcessList();
					ProcessTypeDetails processtype = new ProcessTypeDetails();
					processtype.setProcessName(processName);
					processtype.setProcessType(processNamelist.getProcessType());
					processtype.setStatus(processNamelist.isStatus());
					list.add(processtype);
					processConfig.setConfigStatus(true);
					processConfig.setProcessList(list);
					ofy().save().entities(processConfig);

				}
				else{
					ProcessConfiguration processconfig = new ProcessConfiguration();
					processconfig.setProcessName(processName);
					processconfig.setConfigStatus(true);
					
					List<ProcessTypeDetails> list = new ArrayList<ProcessTypeDetails>();
					ProcessTypeDetails processtype = new ProcessTypeDetails();
					processtype.setProcessName(processName);
					processtype.setProcessType(processNamelist.getProcessType());
					processtype.setStatus(true);
					list.add(processtype);
					processconfig.setConfigStatus(true);
					processconfig.setProcessList(list);
					ofy().save().entities(processconfig);
				}
			}	
			
		}
		return "Process configuration updated successfully";
	}

	private void updateProcessConfiguration(HashSet<String> hsprocessNamelist, ArrayList<ProcessTypeDetails> processlist, long companyId) {
		
		ArrayList<String> processName = new ArrayList<String>(hsprocessNamelist);
		
		List<ProcessConfiguration> processConfiglist = ofy().load()
				.type(ProcessConfiguration.class)
				.filter("companyId", companyId)
				.filter("processName IN", processName).list();
		
		System.out.println("processConfiglist =="+processConfiglist.size());
		for(ProcessTypeDetails processNamelist : processlist){
			for(ProcessConfiguration processConfig : processConfiglist){
				
				boolean flag = false;
				if(processConfig!=null){
						for(ProcessTypeDetails procesType : processConfig.getProcessList()){
							if(procesType.getProcessName().equals(processName) && procesType.getProcessType().equalsIgnoreCase(processNamelist.getProcessType())){
								flag = true;
								break;
							}
						}
				}
				if(flag){
					processConfig.setConfigStatus(true);
					for(ProcessTypeDetails procesType : processConfig.getProcessList()){
						if(procesType.getProcessName().equals(processName) && procesType.getProcessType().equalsIgnoreCase(processNamelist.getProcessType())){
							procesType.setStatus(processNamelist.isStatus());
						}
					}
					ofy().save().entities(processConfig);

				}
				else{
					if(processConfig!=null && !flag){
						List<ProcessTypeDetails> list = processConfig.getProcessList();
						ProcessTypeDetails processtype = new ProcessTypeDetails();
						processtype.setProcessName(processConfig.getProcessName());
						processtype.setProcessType(processNamelist.getProcessType());
						processtype.setStatus(processNamelist.isStatus());
						list.add(processtype);
						processConfig.setConfigStatus(true);
						processConfig.setProcessList(list);
						ofy().save().entities(processConfig);

					}
					else{
						ProcessConfiguration processconfig = new ProcessConfiguration();
						processconfig.setProcessName(processConfig.getProcessName());
						processconfig.setConfigStatus(true);
						
						List<ProcessTypeDetails> list = new ArrayList<ProcessTypeDetails>();
						ProcessTypeDetails processtype = new ProcessTypeDetails();
						processtype.setProcessName(processConfig.getProcessName());
						processtype.setProcessType(processNamelist.getProcessType());
						processtype.setStatus(true);
						list.add(processtype);
						processconfig.setConfigStatus(true);
						processconfig.setProcessList(list);
						ofy().save().entities(processconfig);
					}
				}
			}
				
			
		}
		
	}

	@Override
	public String activateDeactivateProcessConfigs(String processName, String processType, boolean status, long companyId,String name) {
		
		activateDeactivateProcessConfiguration(companyId, processName, processType, status);
		if(status){
			return name + " activated successfully";
		}
		else{
			return name + " deactivated successfully";
		}

	}

	@Override
	public String validateMaterialQuantity(long companyId, List<ProductGroupList> groupList) {
		
		logger.log(Level.SEVERE, " inside validateMaterialQuantity ");

		String error ="";
		if(groupList.size()>0){
			
			GeneralServiceImpl generalserviceimpl = new GeneralServiceImpl();
			
			HashSet<Integer> hsprodId = new HashSet<Integer>();
			
			HashMap<Integer, Double> hsproductQty = new HashMap<Integer, Double>();

			for(ProductGroupList prodId : groupList){
				hsprodId.add(prodId.getProduct_id());
				if(hsproductQty.containsKey(prodId.getProduct_id())){
					double materialQty = hsproductQty.get(prodId.getProduct_id()) +  prodId.getQuantity();
					logger.log(Level.SEVERE, "prod materialQty "+materialQty);
					
					hsproductQty.put(prodId.getProduct_id(), materialQty);
				}
				else{
					logger.log(Level.SEVERE, "prod materialQty == "+prodId.getQuantity());

					hsproductQty.put(prodId.getProduct_id(), prodId.getQuantity());
				}

			}
			ArrayList<Integer> prodIdList = new ArrayList<Integer>(hsprodId);

			ArrayList<SuperModel> productinventoruviewlist = generalserviceimpl.getProductInventoryDetails(prodIdList,companyId);


			for (Map.Entry<Integer, Double> entryset : hsproductQty.entrySet()) {

				int count=0;
				
			for(ProductGroupList productgroup : groupList){
				boolean productStockMasterFlag = false;
				
				if(entryset.getKey().equals(productgroup.getProduct_id())){
					count++;
				}
				logger.log(Level.SEVERE, "count"+count);
				if(count>1){
					continue;
				}
				
				for(SuperModel model : productinventoruviewlist){
					ProductInventoryView prodView = (ProductInventoryView) model;
					for(ProductInventoryViewDetails proddetails : prodView.getDetails()){
						productStockMasterFlag = true;
						
						logger.log(Level.SEVERE, "proddetails.getProdid() "+proddetails.getProdid()+" productgroup.getProduct_id()"+productgroup.getProduct_id());

						if(proddetails.getProdid() == productgroup.getProduct_id() &&  proddetails.getWarehousename().trim().equals(productgroup.getWarehouse().trim()) &&
								proddetails.getStoragelocation().trim().equals(productgroup.getStorageLocation().trim()) && 
								proddetails.getStoragebin().trim().equals(productgroup.getStorageBin().trim())){
							
							double materialQty = 0;
							logger.log(Level.SEVERE,"entryset.getKey() "+entryset.getKey());
							logger.log(Level.SEVERE,"productgroup.getProduct_id()"+productgroup.getProduct_id());
							logger.log(Level.SEVERE,"entryset.getKey().equals(productgroup.getProduct_id())"+(entryset.getKey().equals(productgroup.getProduct_id())));

							if(entryset.getKey().equals(productgroup.getProduct_id())){
								materialQty = entryset.getValue();
								logger.log(Level.SEVERE, "Material Id -  "+entryset.getKey()+" Material Qty "+entryset.getValue());

							}
							
							logger.log(Level.SEVERE,"proddetails.getAvailableqty()"+proddetails.getAvailableqty());
							logger.log(Level.SEVERE,"materialQty"+materialQty);

							if(proddetails.getAvailableqty()<materialQty){
								error += "  Insufficient available quantity warehouse - "+proddetails.getWarehousename() +" product name - "+proddetails.getProdname();
							}
						}
					}
				}
				
				if(productStockMasterFlag==false){
					error += "Please Define Product Inventory Master for Warehouse "+ productgroup.getWarehouse()+ " of product "+productgroup.getName();
				}
				
				
			}
				
			}
			
		}
		return error;
	}

	@Override
	public String validateUploadedFileSize(DocumentUpload uploadedFile,	long companyId) {
		
		
		BlobstoreService blobstoreservice=BlobstoreServiceFactory.getBlobstoreService();

		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

		String urlData = "";
		String fileName = "";

		urlData =uploadedFile.getUrl();
		logger.log(Level.SEVERE, "Doc URL :::: " + urlData);

		
//		fileName = emailDetails.getUploadedDocument().getName();
//		logger.log(Level.SEVERE, "Document Name :::: " + fileName);

		String[] res = urlData.split("blob-key=", 0);;
		
		logger.log(Level.SEVERE, "Splitted Url :::: " + res);

		String blob = res[1].trim();
		logger.log(Level.SEVERE,"Splitted Url Assigned to string  :::: " + blob);
		
		BlobKey blobKey = null;
		try {
			blobKey = new BlobKey(blob);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);
		logger.log(Level.SEVERE, "BlobKey  size :::: " + blobKey.toString().length());

		BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
		BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);
		Long blobSize = blobInfo.getSize();
		
		long blobsizeInKb = Math.round(blobSize/1024);
		logger.log(Level.SEVERE, "blobSize == " + blobSize);
		if(blobsizeInKb>1024) {
			return "Attachment uploaded file size must be leass than 1 MB";
		}
		
		return "";
	}


	@Override
	public void printSummarySRCopy(Long companyId, Date fromDate, Date toDate, String branch, int serviceId,
			ArrayList<String> emailIdList, Contract contract,String reportName, String reportFormat, Boolean completedFlag,
			Boolean cancelledFlag, Boolean otherFlag) {

		

		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "sr" + companyId);
		Gson gson = new Gson();
		String str = gson.toJson(emailIdList);
	//	String str1 = gson.toJson(contract , Contract.class);
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "SendSummarySrCopies")
					.param("companyId", companyId+ "")
					.param("fromDate", fromDate+"" )
					.param("toDate", toDate+"")
					.param("branch", branch)
					.param("serviceId", serviceId+"")
					.param("contract", contract.getCount()+"")
					.param("email", str)
					.param("reportName", reportName)
					.param("reportFormat", reportFormat)
					.param("completedFlag", completedFlag+"")
					.param("cancelledFlag", cancelledFlag+"")
					.param("otherFlag", otherFlag+""));
			logger.log(Level.SEVERE, "sr" + queue);
	
	}




	@Override
	public void sendCTCEmail(Long companyId, ArrayList<String> emailIdList) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "in sendCTCEmail method of commonservice" + companyId);
		Gson gson = new Gson();
		String str = gson.toJson(emailIdList);
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "SendCTCEmail")
					.param("companyId", companyId+ "")
					.param("email", str));
			logger.log(Level.SEVERE, "calling DocumentQueue" + queue);
		
	}



	/**
	 * @author Ashwini PAtil
	 * @since 31-03-2022
	 * Sunrise client has employees in thousands which are not getting loaded in a minute so sending employee details through email using taskqueue.
	 * Email process level option is available on CTC screen. 
	 * Excel report of all active CTC will be mailed through this method.
	 */
	public void createAndSendCTCEmail(long companyId,ArrayList<String> emailList) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "in createAndSendCTCEmail");
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		List<CTC> ctcList = new ArrayList<CTC>();
		if(company != null){
			logger.log(Level.SEVERE, "Loading CTC");
			ctcList = ofy().load().type(CTC.class).filter("companyId", companyId)
					 .filter("status", CTC.ACTIVE).list();		
		}
		logger.log(Level.SEVERE, "CTC list size="+ctcList.size());
		if(ctcList.size()>0){
			XSSFWorkbook workbook=new XSSFWorkbook();
			try {		
				createCTCWorkbook(ctcList,workbook);
		    	workbook.write(bytestream);
				logger.log(Level.SEVERE,"bytestream created");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	
		String mailSub = "Active CTC report";
		
		String mailMsg = "Dear Sir/Ma'am ,"+"\n"+"\n"+"\n"+ " <BR>"+"Please find the active CTC details report.";
		Date d=new Date();
		String filename="CTC "+d+".xlsx";
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(company.getEmail(), emailList, null, null, mailSub, mailMsg, "text/html",bytestream,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",company.getDisplayNameForCompanyEmailId());
			logger.log(Level.SEVERE,"attached xlsx");
		}else{
			Email email = new Email();
			email.sendMailWithGmail(company.getEmail(), emailList, null, null, mailSub, mailMsg, "text/html",bytestream,filename,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			logger.log(Level.SEVERE,"attached xlsx");
		}
					
	}




	private void createCTCWorkbook(List<CTC> ctcList, XSSFWorkbook workbook) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"Inside createCTCWorkbook");
    	
		XSSFCellStyle boldStyle = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont boldFont = (XSSFFont) workbook.createFont();
	    boldFont.setFontHeightInPoints((short) 14);
	    boldFont.setFontName("Times New Roman");
	    boldFont.setBold(true);
	    boldStyle.setFont(boldFont);
	    boldStyle.setBorderBottom(BorderStyle.THIN);
	    boldStyle.setBorderLeft(BorderStyle.THIN);
	    boldStyle.setBorderRight(BorderStyle.THIN);
	    boldStyle.setBorderTop(BorderStyle.THIN);
	    boldStyle.setWrapText(true);
	   
	    XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont font = (XSSFFont) workbook.createFont();
	    font.setFontHeightInPoints((short) 14);
	    font.setFontName("Times New Roman");
	    font.setBold(false);
	    style.setFont(font);
	    style.setBorderBottom(BorderStyle.THIN);
	    style.setBorderLeft(BorderStyle.THIN);
	    style.setBorderRight(BorderStyle.THIN);
	    style.setBorderTop(BorderStyle.THIN);
	    style.setWrapText(true);
	    style.setAlignment(HorizontalAlignment.CENTER);
	    style.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    XSSFCellStyle leftStyle = (XSSFCellStyle) workbook.createCellStyle();
	    leftStyle.setFont(font);
	    leftStyle.setBorderBottom(BorderStyle.THIN);
	    leftStyle.setBorderLeft(BorderStyle.THIN);
	    leftStyle.setBorderRight(BorderStyle.THIN);
	    leftStyle.setBorderTop(BorderStyle.THIN);
	    leftStyle.setWrapText(true);
	    leftStyle.setAlignment(HorizontalAlignment.LEFT);
	    leftStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    
	    XSSFSheet sheet = (XSSFSheet) workbook.createSheet("Sheet1");
	    sheet.setDefaultColumnWidth(17);
	    sheet.setColumnWidth(0,5);
	    
	    Row row=sheet.createRow(0);
	    createCell(row, 0, "CTC ID", boldStyle);
	    createCell(row, 1, "CTC TEMPLATE ID", boldStyle);
	    createCell(row, 2, "CTC TEMPLATE NAME", boldStyle);
	    createCell(row, 3, "EMPLOYEE ID", boldStyle);
	    createCell(row, 4, "NAME", boldStyle);
	    createCell(row, 5, "PHONE NUMBER", boldStyle);
	    createCell(row, 6, "CALENDAR", boldStyle);
	    createCell(row, 7, "BRANCH", boldStyle);
	    createCell(row, 8, "DEPARTMENT", boldStyle);
	    createCell(row, 9, "TYPE", boldStyle);
	    createCell(row, 10, "EMPLOYEE DESIGNATION", boldStyle);
	    createCell(row, 11, "EMPLOYEE ROLE", boldStyle);
	    createCell(row, 12, "PROJECT", boldStyle);
	    
	    int colCount=13;
	    int rowCount=1;
	    LinkedHashSet<String> hscomponentlist = new LinkedHashSet<String>();
		for(CTC ctc:ctcList)
		{
			ArrayList<CtcComponent> ctcEarninglist =  ctc.getEarning();
			Comparator<CtcComponent> ctccomponentSrNocomparater = new Comparator<CtcComponent>() {
				public int compare(CtcComponent s1, CtcComponent s2) {
				
				Integer srnumber1 = s1.getSrNo();
				Integer srnumber2 = s2.getSrNo();
				
				return srnumber1.compareTo(srnumber2);
				}
				
				};
				Collections.sort(ctcEarninglist, ctccomponentSrNocomparater);
				
			for(CtcComponent ctcomp : ctcEarninglist){
				hscomponentlist.add(removeCommaFromStringValue(ctcomp.getName()));
			}
		}
		ArrayList<String> componentlist = new ArrayList<String>(hscomponentlist);
		for(String strcomponentName : componentlist){
			createCell(row, colCount, strcomponentName, boldStyle);
			colCount++;
		}
		createCell(row, colCount, "Paid Leave Amount", boldStyle);colCount++;
		createCell(row, colCount, "Bonus Amount", boldStyle);colCount++;
		createCell(row, colCount, "Gross Amount", boldStyle);
		    
		for(CTC ctc:ctcList){
			
			row=sheet.createRow(rowCount);
			createCell(row, 0,ctc.getCount()+"", leftStyle);
			createCell(row, 1,ctc.getCtcTemplateId()+"", leftStyle);		
			
			if(ctc.getCtcTemplateName()!=null){
				createCell(row, 2,ctc.getCtcTemplateName(), leftStyle);
			}else{
				createCell(row, 2," ", leftStyle);
			}
			
			createCell(row, 3,ctc.getEmpid()+"", leftStyle);
			createCell(row, 4,ctc.getEmployeeName(), leftStyle);
			createCell(row, 5,ctc.getEmpCellNo()+"", leftStyle);
			createCell(row, 6,ctc.getCalendar()+"", leftStyle);
			createCell(row, 7,ctc.getBranch(), leftStyle);
			createCell(row, 8,ctc.getDepartment(), leftStyle);
			createCell(row, 9,ctc.getEmployeeType(), leftStyle);
			createCell(row, 10,ctc.getEmployeedDesignation(), leftStyle);
			createCell(row, 11,ctc.getEmployeeRole(), leftStyle);


			/**
			 * @author Anil , Date : 13-07-2019
			 */
			if(ctc.getProjectName()!=null){
				createCell(row, 12,ctc.getProjectName(), leftStyle);
			}else{
				createCell(row, 12," ", leftStyle);
			}

			colCount=13;
			
			for(String strcomponentName : componentlist){
				boolean flag=false;
				for(CtcComponent ctccomponent : ctc.getEarning())
				{
					if(strcomponentName.trim().equals(ctccomponent.getName().trim())){
						double monthly=round(ctccomponent.getAmount()/12,2);
						createCell(row, colCount,monthly+"", leftStyle);	
						flag=true;
						break;
					}
					
				}
				if(flag==false)
					createCell(row, colCount," ", leftStyle);
				colCount++;
				
			}
			
			createCell(row, colCount,ctc.getPaidLeaveAmt()+"", leftStyle);colCount++;
			createCell(row, colCount,ctc.getBonusAmt()+"", leftStyle);colCount++;
			double ctcAmount= 0;
			ctcAmount = round(ctc.getCtcAmount()/12,2); 
			ctcAmount += ctc.getPaidLeaveAmt() + ctc.getBonusAmt();
			createCell(row, colCount,ctcAmount +"", leftStyle);	
			rowCount++;
		}
		
	}
	
	public void createCell(Row row, int column,String value,XSSFCellStyle style) {
	    	Cell cell=row.createCell(column);
	    	cell.setCellValue(value);
	    	cell.setCellStyle(style);
	    }
	
	public String removeCommaFromStringValue(String data){	
		System.out.println("CSV WRITER : "+data);
		data=data.replaceAll(","," ");
		data = data.replaceAll("\n", " ");
		return data;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}





	@Override
	public String updateServiceValue(int contractId, long companyId) {
		
		Queue queue = QueueFactory.getQueue("UpdateServices-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "Update Service Value")
				.param("companyId", companyId+"").param("ContractId", contractId+""));
		logger.log(Level.SEVERE, "Task queue called for Updating service value for contract "+contractId);
		
		return "Service value updation process started. It will take some time to update service value based on number of services, kindly check after some time";

	}



	/**
	 * @author :- Vijay Chougule
	 * Des :- Contract Reset
	 */
	
	@Override
	public ArrayList<CancelContract> getServiceInvoiceDocument(int contractId,
			long companyId) {
		
		ArrayList<CancelContract> list = new ArrayList<CancelContract>();
		
		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractId)
									.filter("status", Service.SERVICESTATUSCOMPLETED).list();
		if(servicelist.size()>0){
			CancelContract cancelContract = new CancelContract();
			cancelContract.setDocumnetName(AppConstants.SERVICE);
			cancelContract.setDocumnetId(servicelist.size());
			list.add(cancelContract);
		}
		
		 ArrayList<String> statuslist = new ArrayList<String>();
		 statuslist.add(BillingDocument.CREATED);
		 statuslist.add(BillingDocument.REQUESTED);
		 statuslist.add(BillingDocument.APPROVED);
		 
		List<Invoice> invoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", contractId)
									.filter("status IN", statuslist).list();
		
		if(invoicelist.size()>0){
			CancelContract cancelContract = new CancelContract();
			cancelContract.setDocumnetName(AppConstants.Invoice);
			cancelContract.setDocumnetId(invoicelist.size());
			list.add(cancelContract);
		}
		
		return list;
	}



	
	/**
	 * @author :- Vijay Chougule
	 * Des :- Contract Reset
	 */
	@Override
	public String updateContractDocuments(int contractId, long companyId,
			boolean serviceCancelFlag, boolean serviceDeleteFlag,
			boolean invoiceCancelFlag,boolean deleteinvoiceFlag) {
		
		logger.log(Level.SEVERE, "serviceCancelFlag "+serviceCancelFlag);
		logger.log(Level.SEVERE, "serviceDeleteFlag "+serviceDeleteFlag);
		logger.log(Level.SEVERE, "invoiceCancelFlag "+invoiceCancelFlag);
		logger.log(Level.SEVERE, "deleteinvoiceFlag "+deleteinvoiceFlag);

		try {
			
		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractId).list();
		logger.log(Level.SEVERE, "servicelist size "+servicelist.size());
		if(servicelist.size()>0){
			if(serviceCancelFlag){
				for(Service service : servicelist){
					if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED) || service.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)){
						service.setStatus(Service.CANCELLED);
					}
				}
				ofy().save().entities(servicelist);

				ArrayList<String> statuslist = new ArrayList<String>();
				statuslist.add(Service.SERVICESTATUSOPEN);
				statuslist.add(Service.SERVICESTATUSPENDING);
				statuslist.add(Service.SERVICESTATUSPLANNED);
				statuslist.add(Service.SERVICESTATUSREOPEN);
				statuslist.add(Service.SERVICESTATUSREPORTED);
				statuslist.add(Service.SERVICESTATUSRESCHEDULE);
				statuslist.add(Service.SERVICESTATUSSCHEDULE);
				statuslist.add(Service.SERVICESTATUSSCHEDULERESCHEDULE);
				statuslist.add(Service.SERVICESTATUSSTARTED);

				List<Service> openservicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("contractCount", contractId)
												.filter("status IN", statuslist).list();
				logger.log(Level.SEVERE, "open services size "+openservicelist.size());
				if(openservicelist.size()>0) {
					ofy().delete().entities(openservicelist).now();
					logger.log(Level.SEVERE, "All open services deleted ");
				}
				

			}else{
				if(serviceDeleteFlag){
					ofy().delete().entities(servicelist).now();
					logger.log(Level.SEVERE, "All services deleted ");
				}
			}
		}
		List<Invoice> approvedInvoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", contractId)
									.filter("status", Invoice.APPROVED).list();
		logger.log(Level.SEVERE, "approved Invoicelist size"+approvedInvoicelist.size());

		HashSet<Integer> hsbillingidlist = new HashSet<Integer>();
		HashSet<Integer> hsInvoieidlist = new HashSet<Integer>();

		ArrayList<String> statuslist = new ArrayList<String>();
		statuslist.add(Invoice.CREATED);
		statuslist.add(Invoice.REQUESTED);
		statuslist.add(Invoice.PROFORMAINVOICE);

			
			if(deleteinvoiceFlag){
				logger.log(Level.SEVERE, "for open invoice billing document deletion ");
				
				List<Invoice> openInvoicelist = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("contractCount", contractId)
											.filter("status IN", statuslist).list();
				logger.log(Level.SEVERE, "open invoicelist size "+openInvoicelist.size());
				if(openInvoicelist.size()>0) {
					
					for(Invoice invoiceEntity : openInvoicelist ){
						for(BillingDocumentDetails billingdocument : invoiceEntity.getArrayBillingDocument()){
							hsbillingidlist.add(billingdocument.getBillId());
						}
						hsInvoieidlist.add(invoiceEntity.getCount());
					}
					ofy().delete().entities(openInvoicelist);
					
					if(hsbillingidlist.size()>0){
						ArrayList<Integer> billingidlist = new ArrayList<Integer>(hsbillingidlist);
						List<BillingDocument> billinglist = ofy().load().type(BillingDocument.class).filter("companyId", companyId)
								.filter("count IN", billingidlist).list();
							if (billinglist != null && billinglist.size() > 0) {
								ofy().delete().entities(billinglist);
							}
					}
					
					
				}
			
				
			}
				
				if(approvedInvoicelist.size()>0){
					
					if(invoiceCancelFlag) {
						
						ArrayList<Integer> invoiceidarray = new ArrayList<Integer>();
						
						for(Invoice invoiceEntity : approvedInvoicelist ){
								invoiceEntity.setStatus(Invoice.CANCELLED);
								invoiceidarray.add(invoiceEntity.getCount());
								
							for(BillingDocumentDetails billingdocument : invoiceEntity.getArrayBillingDocument()){
								hsbillingidlist.add(billingdocument.getBillId());
							}
							hsInvoieidlist.add(invoiceEntity.getCount());
						}
						ofy().save().entities(approvedInvoicelist);
						
						if(hsbillingidlist.size()>0){
							ArrayList<Integer> billingidlist = new ArrayList<Integer>(hsbillingidlist);
							List<BillingDocument> billinglist = ofy().load().type(BillingDocument.class).filter("companyId", companyId)
									.filter("count IN", billingidlist).list();
								if (billinglist != null && billinglist.size() > 0) {
									for (BillingDocument billingentity : billinglist) {
										billingentity.setStatus(BillingDocument.CANCELLED);
									}
									ofy().save().entities(billinglist);
								}
							
						}
						if(hsInvoieidlist.size()>0){
							ArrayList<Integer> invoiceidlist = new ArrayList<Integer>(hsInvoieidlist);
							List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("companyId", companyId)
																.filter("invoiceCount IN", invoiceidlist).list();
							if(paymentlist!=null && paymentlist.size()>0){
								for(CustomerPayment payment : paymentlist ){
									payment.setStatus(CustomerPayment.CANCELLED);
								}
								ofy().save().entities(paymentlist);

							}
						}
						
						try {
							List<AccountingInterface> accountingInterfacelist = ofy().load().type(AccountingInterface.class).filter("companyId", companyId)
									.filter("documentType", "Invoice").filter("documentID IN", invoiceidarray).list();
							if(accountingInterfacelist.size()>0){
								for(AccountingInterface accoutinginterface : accountingInterfacelist){
									accoutinginterface.setDocumentStatus(BillingDocument.CANCELLED);// Ashwini Patil Date:8-08-2024 ultra reported that when we reset contract invoice gets cancelled but accounting interface still shows invoice approved
									accoutinginterface.setStatus(BillingDocument.CANCELLED);
									accoutinginterface.setRemark("Invoice Id = "+accoutinginterface.getDocumentID()+" has been cancelled due to contract reset");
								}
								ofy().save().entities(accountingInterfacelist);
							}
						} catch (Exception e) {
							
						}
						
						
					}
					logger.log(Level.SEVERE, "invoice document cancelled");

					}
					

		ArrayList<String> billingstatuslist = new ArrayList<String>();
		billingstatuslist.add(BillingDocument.CREATED);
		billingstatuslist.add(BillingDocument.REQUESTED);
		billingstatuslist.add(BillingDocument.APPROVED);
		
		List<BillingDocument> billinglist = ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("contractCount", contractId)
											.filter("status IN", billingstatuslist).list();
		if(billinglist.size()>0){
			ofy().delete().entities(billinglist).now();
			logger.log(Level.SEVERE, "open Billing document deleted");
		}
		
		
		} catch (Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		return "success";
	}

	@Override
	public ArrayList<HrProjectOvertime> getHrProjectOvertimelist(int projectId,	long companyId) {
		ArrayList<HrProjectOvertime> arraylist = new ArrayList<HrProjectOvertime>();

		try {
			List<HrProjectOvertime> list = ofy().load().type(HrProjectOvertime.class).filter("companyId", companyId).filter("projectId", projectId).list();
			logger.log(Level.SEVERE, "list size "+list.size());
			if(list.size()>0){
				ArrayList<HrProjectOvertime> arraylist2 = new ArrayList<HrProjectOvertime>(list);
				return arraylist2;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "returning here");
		return arraylist;
	}

	@Override
	public String updateHrProjectOvertime(ArrayList<HrProjectOvertime> list,String oprationName) {
		try {
			
			ofy().clear();
			logger.log(Level.SEVERE,"LIST SIZE "+list.size());
		    logger.log(Level.SEVERE,"oprationName "+oprationName);
			if(oprationName.equals("Delete")){
				ofy().delete().entities(list);
			}else if(oprationName.equalsIgnoreCase("Save")){
				logger.log(Level.SEVERE,"SAVE OPERATION");
//				ofy().save().entities(list).now();
				ArrayList<SuperModel> otlist = new ArrayList<SuperModel>();
				otlist.addAll(list);
				GenricServiceImpl genimpl = new GenricServiceImpl();
				genimpl.save(otlist);
			}
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
		    logger.log(Level.SEVERE,"Updated Successfully!!!");
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<Overtime> getHRProjectOvertimelist(HrProject hrproject) {

		try {
			List<Overtime> overtimelist = hrproject.getOtList();
			if(overtimelist.size()==0){
				List<HrProjectOvertime> list = ofy().load().type(HrProjectOvertime.class).filter("companyId", hrproject.getCompanyId()).filter("projectId", hrproject.getCount()).list();
				ArrayList<Overtime> otlist = new ArrayList<Overtime>();
				for(HrProjectOvertime  ot : list){
					otlist.add(ot);
				}
				return otlist;
			}
			else{
				ArrayList<Overtime> otlist = new ArrayList<Overtime>();
				otlist.addAll(overtimelist);
				return otlist;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "returning here");
		return null;
	}

	@Override
	public ArrayList<HrProjectOvertime> getHrProjectOvertimelist(ArrayList<Integer> projectIdlist,	long companyId) {
		ArrayList<HrProjectOvertime> arraylist = new ArrayList<HrProjectOvertime>();

		try {
			List<HrProjectOvertime> list = ofy().load().type(HrProjectOvertime.class).filter("companyId", companyId).filter("projectId IN", projectIdlist).list();
			logger.log(Level.SEVERE, "list size "+list.size());
			if(list.size()>0){
				ArrayList<HrProjectOvertime> arraylist2 = new ArrayList<HrProjectOvertime>(list);
				return arraylist2;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "returning here");
		return arraylist;
	}

	@Override
	public String updateHrProjectOvertimelist(ArrayList<Integer> hrprojectOtIdlist, long companyId,
			String operationName, int projectId) {
		logger.log(Level.SEVERE, "hrprojectOtIdlist "+hrprojectOtIdlist);

		List<HrProjectOvertime> list = ofy().load().type(HrProjectOvertime.class).filter("companyId", companyId)
							.filter("count IN", hrprojectOtIdlist).filter("projectId", projectId).list();
		if(list.size()>0){
			ofy().delete().entities(list);
		}

		return "success";
	}

	@Override
	public String updateEmployeeProjectAllocationList(ArrayList<EmployeeProjectAllocation> list, String oprationName) {
		

		try {
			
			ofy().clear();
			logger.log(Level.SEVERE,"LIST SIZE "+list.size());
		    logger.log(Level.SEVERE,"oprationName "+oprationName);
			if(oprationName.equals("Delete")){
				ofy().delete().entities(list);
			}else if(oprationName.equalsIgnoreCase("Save")){
				logger.log(Level.SEVERE,"SAVE OPERATION");
				ArrayList<SuperModel> empprojectallocationlist = new ArrayList<SuperModel>();
				empprojectallocationlist.addAll(list);
				GenricServiceImpl genimpl = new GenricServiceImpl();
				genimpl.save(empprojectallocationlist);
			}
			ObjectifyService.reset();
		    ofy().consistency(Consistency.STRONG);
		    ofy().clear();
		    logger.log(Level.SEVERE,"Updated Successfully!!!");
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "success";
	}
	
	public ArrayList<EmployeeProjectAllocation> geteEmployeeProjectAllocationList(int projectAllocationId, long companyId){
		

		try {
			List<EmployeeProjectAllocation> list = ofy().load().type(EmployeeProjectAllocation.class).filter("companyId", companyId).filter("projectAllocationId", projectAllocationId).list();
			logger.log(Level.SEVERE, "list size "+list.size());
			if(list.size()>0){
				ArrayList<EmployeeProjectAllocation> EmployeeProjectAllocationlist = new ArrayList<EmployeeProjectAllocation>(list);
				return EmployeeProjectAllocationlist;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "returning here");
		return null;
		
	}
	
	public ArrayList<EmployeeProjectAllocation> getEmployeeProjectAllocationlist(ProjectAllocation projectAllocationEntity){
	
			ArrayList<EmployeeProjectAllocation> employeeprojectallocationlist = new ArrayList<EmployeeProjectAllocation>();
			logger.log(Level.SEVERE, "projectAllocationEntity.getEmployeeProjectAllocationList() size ==  "+projectAllocationEntity.getEmployeeProjectAllocationList().size());
		   if(projectAllocationEntity.getEmployeeProjectAllocationList()!=null && projectAllocationEntity.getEmployeeProjectAllocationList().size()>0){
			   employeeprojectallocationlist = projectAllocationEntity.getEmployeeProjectAllocationList();
		   }
		   else{
			   if(projectAllocationEntity.isSeperateEntityFlag()){
				   employeeprojectallocationlist = geteEmployeeProjectAllocationList(projectAllocationEntity.getCount(), projectAllocationEntity.getCompanyId());
					logger.log(Level.SEVERE, "from seperate entity"+employeeprojectallocationlist.size());
			   }
		   }
		
		return employeeprojectallocationlist;
		
		
	}

	@Override
	public String genrateHRProjectOnProcejectAllocationSave(ProjectAllocation projectAllocation) {
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.updateonProjectAllocationSave(projectAllocation);
		
		CncBillServiceImpl impl = new CncBillServiceImpl();
		impl.generateHRProject(projectAllocation);
		
		return "success";
	}
	
	//Ashwini Patil 15-09-2022 to delete payment documents at the time of Invoice reset
	@Override
	public String deletePaymentDocumentsOnInvoiceReset(int invoiceId, long companyId, String username) {
		// TODO Auto-generated method stub
		try {
			List<CustomerPayment> paymentList = ofy().load().type(CustomerPayment.class).filter("companyId", companyId).filter("invoiceCount", invoiceId).list();
			logger.log(Level.SEVERE, "paymentList size "+paymentList.size());
			if(paymentList.size()>0) {
				ofy().delete().entities(paymentList);
			}
			
			List<AccountingInterface> aiInvoiceList = ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("documentID", invoiceId).filter("documentType", "Invoice").list();
			logger.log(Level.SEVERE, "aiInvoiceList size "+aiInvoiceList.size());
			if(aiInvoiceList.size()>0) {
				ofy().delete().entities(aiInvoiceList);
			}
			
			
			List<AccountingInterface> aiPaymentList = ofy().load().type(AccountingInterface.class).filter("companyId", companyId).filter("referenceDocumentNo1", invoiceId+"").filter("documentType", "Customer Payment").list();
			logger.log(Level.SEVERE, "aiPaymentList size "+aiPaymentList.size());
			if(aiPaymentList.size()>0) {
				ofy().delete().entities(aiPaymentList);
			}
			
			Employee user = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", username).first().now();
			
			Invoice invoice = ofy().load().type(Invoice.class).filter("companyId", companyId).filter("count", invoiceId).first().now();
			invoice.setStatus("Created");
			invoice.setLog(invoice.getLog()+"\n"+username+" "+user.getCount()+" reset the Invoice on "+new Date());
			ofy().save().entity(invoice);
			logger.log(Level.SEVERE, "saved Invoice");
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "error";
		}
			
		return "success";
	}
	
	
	
	@Override
	public AuditObservations saveNewObservationImpactRecommendation(long companyId,
			ObservationImpactRecommendation observation) {
		
		
//		CreateUpdateObservationApi createnewobservation = new CreateUpdateObservationApi();
//		String msg = createnewobservation.saveAndUpdateNewObservation(observation.getObservationName(), 
//				observation.getImpact(), observation.getAction(), observation.getRecommendation(), companyId);
//		logger.log(Level.SEVERE, "msg "+msg);
//
//		AuditObservations auditobservation = ofy().load().type(AuditObservations.class).filter("companyId", companyId)
//				.filter("observationName", observation.getObservationName()).first().now();
//		logger.log(Level.SEVERE, "auditobservation "+auditobservation);
		
		AuditObservations auditobservations = saveAndUpdateNewObservation(observation.getObservationName(), 
				observation.getImpact(), observation.getAction(), observation.getRecommendation(), companyId);
		return auditobservations;
	}

	private AuditObservations saveAndUpdateNewObservation(String observationName, String impact, String action,
			String recommendation, long companyId) {
		
		AuditObservations auditobservation = ofy().load().type(AuditObservations.class).filter("companyId", companyId)
				.filter("observationName", observationName).first().now();
		logger.log(Level.SEVERE, "auditobservation "+auditobservation);
		
		if(auditobservation!=null){
			ArrayList<ObservationImpactRecommendation> list  = new ArrayList<ObservationImpactRecommendation>();
			list.addAll(auditobservation.getObservationlist());
			
			ObservationImpactRecommendation observation = new ObservationImpactRecommendation();
			observation.setObservationName(observationName);
			observation.setImpact(impact);
			observation.setAction(action);
			observation.setRecommendation(recommendation);
			list.add(observation);
			
			auditobservation.setObservationlist(list);
			auditobservation.setStatus(true);
			
			ofy().save().entities(auditobservation);
			logger.log(Level.SEVERE, "observation updated successfully");
			return auditobservation;

		}
		else{
			
			logger.log(Level.SEVERE, "First time new observation entry in the entity");
		
			AuditObservations auditobservationimpact = new AuditObservations();
			auditobservationimpact.setObservationName(observationName);
			
			auditobservationimpact.setCompanyId(companyId);
			
			ArrayList<ObservationImpactRecommendation> list  = new ArrayList<ObservationImpactRecommendation>();
			ObservationImpactRecommendation observation = new ObservationImpactRecommendation();
			observation.setObservationName(observationName);
			observation.setImpact(impact);
			observation.setAction(action);
			observation.setRecommendation(recommendation);
			observation.setNewentry("yes");
			list.add(observation);
			
			auditobservationimpact.setObservationlist(list);
			auditobservationimpact.setStatus(true);
		
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(auditobservationimpact);
			logger.log(Level.SEVERE, "observation created successfully");
			return auditobservationimpact;
		}
		
		
	}
	
	
	@Override
	public String setupPedio(long companyId,String fromLink,String tolink) {
		
		

		String referenceLink=tolink+"/slick_erp/copypedioconfigurationService";
		logger.log(Level.SEVERE,"referenceLink URL : "+referenceLink);
		
		URL url = null;
		try {
			url = new URL("https://"+tolink);
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE,"MalformedURLException ERROR::::::::::"+e);
			e.printStackTrace();
			return "Please add valid url. "+e.getMessage();
		}
		logger.log(Level.SEVERE,"STAGE 1");
        HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(60000);
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IOException ERROR::::::::::"+e);
			e.printStackTrace();
			return "Error occured in establishing connection. "+e.getMessage();
		}
		
		Gson gson = new Gson();
//		String entity = gson.toJson(entityList);
		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "copyPedioConfiguration")
				.param("companyId", companyId+"")
				.param("fromLink", fromLink )
				.param("toLink", referenceLink ));
		
		
		return "Process has been started, please check after some time.";
	
	}
	
	public String getCompleteURL(Company company){
		return "https://"+company.getCompanyURL()+"/slick_erp/";
	}

	@Override
	public String deleteInvalidLogin(String userName, long companyId) {
		
		List<LoggedIn> loggedInlist = ofy().load().type(LoggedIn.class).filter("companyId", companyId).filter("userName", userName)
				.filter("remark", "Invalid Login").filter("status", AppConstants.INACTIVE).list();
		if(loggedInlist.size()>0) {
			ofy().delete().entities(loggedInlist);
		}
		
		return "Invalid login data cleared";
	}
	
	@Override
	public void shareCustomerPortalLink(Long companyId,boolean sms,boolean whatsapp,boolean email) {
		// TODO Auto-generated method stub
		Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
		queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
				.param("OperationName", "shareCustomerPortalLink")
				.param("companyId", companyId+ "")
				.param("smsflag", sms+ "")
				.param("whatsappflag", whatsapp+ "")
				.param("emailflag", email+ "")
				);
		logger.log(Level.SEVERE, "calling DocumentQueue" + queue);
	}

	@Override
	public String checkCustomerPortalLicense(Long companyId,boolean sms,boolean whatsapp,boolean email) {
		// TODO Auto-generated method stub
		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(company!=null) {
			GetUserRegistrationOtp api=new GetUserRegistrationOtp();
			String result=api.validateLicense(company,AppConstants.LICENSETYPELIST.CUSTOMERPORTAL);
			if(!result.equalsIgnoreCase("Success"))
				return result;
			else {
				if(sms||whatsapp) {
					SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE ).filter("status",true).first().now();
					if(smsEntity==null) 
						return "Communication template 'Customer Portal Link Communication' not found or Inactive";
				
				
					if(sms) {
						SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId)
								.filter("status", true).first().now();
						logger.log(Level.SEVERE,"smsConfig"+ smsConfig);
						if(smsConfig!=null){
							if(smsConfig.getStatus()){
								
								if(smsConfig.getAccountSID().equals("") ){
									return "Sender id does not exist in sms configuration!";
			
								}
								if(smsConfig.getAuthToken().equals("") ){
									return "User name does not exist in sms configuration!";
			
								}
								if(smsConfig.getPassword().equals("") ){
									return "Password does not exist in sms configuration!";
			
								}
							}else
								return "SMS configuration status is inactive!";
						}
						else{
							return "SMS configuration is not defined!";
						}
					}
					if(whatsapp) {
						if(company.getWhatsAppApiUrl()!=null && !company.getWhatsAppApiUrl().equals("") &&
								company.getWhatsAppInstaceId()!=null && !company.getWhatsAppInstaceId().equals("")){
							if(!company.isWhatsAppApiStatus()){
								return "WhatsApp integration status is inactive";
							}
						}
						else{
							return "WhatsApp integration is not defined";
						}
					}
				}
						
				
				if(email) {
					EmailTemplate emailTemplate = null;
					emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
								.filter("templateName",AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE)	
								.filter("templateStatus", true).first().now();
					if(emailTemplate==null)
						return "Email template 'Customer Portal Link Communication' not found or Inactive";
				}
				if(company.getEmail()==null||company.getEmail().equals("")) {
					return "Email id missing in company information";
				}
				if(company.getCompanyURL()==null||company.getCompanyURL().equals("")) {					
					return "Company url missing in company information.";
				}
				return "Success";
			}
		
		}else {
			return "Unable to load Company details";
		}
		
	}

	@Override
	public String SendContractDataOnEmail(Long companyId, Date fromDate,
			Date toDate, boolean activeContractsFlag,  ArrayList<String> emailIdlist, String url) {
		 SimpleDateFormat format= new SimpleDateFormat("MM/dd/yyyy");

		Gson gson = new Gson();
		String str = gson.toJson(emailIdlist);
		String activeContracts = "No";
		if(activeContractsFlag){
			activeContracts = "Yes";
		}
		else{
			activeContracts = "No";
		}
		String strfromDate = "null"; 
		String strtodate = "null";
		
		if(fromDate!=null){
			strfromDate = format.format(fromDate);
		}
		if(toDate!=null){
			strtodate = format.format(toDate);

		}
		
			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
					.param("OperationName", "SendContractDataOnEmail")
					.param("companyId", companyId+ "")
					.param("fromDate", strfromDate)
					.param("toDate", strtodate)
					.param("activeContracts", activeContracts)
					.param("email", str)
					.param("link", url));
			logger.log(Level.SEVERE, "sr" + queue);
		
		return "Data download process started please check email after 5 to 10 minutes";
	}

	@Override
	public String sendSRFormatVersion1CopyOnEmail(Date fromDate, Date toDate,
			String team, String technicianName, ArrayList<String> toemaillst , long companyId) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		logger.log(Level.SEVERE, "fromDate" + fromDate);
		logger.log(Level.SEVERE, "toDate" + toDate);

		Calendar cal=Calendar.getInstance();
		cal.setTime(fromDate);
		cal.add(Calendar.DATE, 0);
		
		Date formDatewithtime=null;
		
		try {
			formDatewithtime=dateFormat.parse(dateFormat.format(cal.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "formDatewithtime" + formDatewithtime);

		Calendar cal3=Calendar.getInstance();
		cal3.setTime(toDate);
		cal3.add(Calendar.DATE, 0);
		
		Date todatewithtime=null;
		
		try {
			todatewithtime=dateFormat.parse(dateFormat.format(cal3.getTime()));
			cal3.set(Calendar.HOUR_OF_DAY,23);
			cal3.set(Calendar.MINUTE,59);
			cal3.set(Calendar.SECOND,59);
			cal3.set(Calendar.MILLISECOND,999);
			todatewithtime=cal3.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "todatewithtime" + todatewithtime);

		List<Service> servicelist = ofy().load().type(Service.class)
				.filter("companyId", companyId)
				.filter("serviceDate >=", formDatewithtime)
				.filter("serviceDate <=", todatewithtime)
				.filter("employee", technicianName)
				.list();
		if(servicelist.size()>0){
			
			logger.log(Level.SEVERE, "sr" + companyId);
			Gson gson = new Gson();
			String str = gson.toJson(toemaillst);
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
						.param("OperationName", "sendSRFormatVersion1OnEmail")
						.param("companyId", companyId+ "")
						.param("fromDate", fromDate+"" )
						.param("toDate", toDate+"")
						.param("technicianName", technicianName+"")
						.param("team",team)
						.param("email", str));
				logger.log(Level.SEVERE, "sr" + queue);
				
			return "Email sending process started. Please check your inbox after few minutes.";
		}
		else{
			return "No data found";
		}
		
	}

	public void SRFormatVersion1CopyOnEmail(Date fromDate, Date toDate,
			String team, String technicianName, ArrayList<String> toemaillst, long companyId) {
		
		
		List<Service> servicelist = ofy().load().type(Service.class)
				.filter("companyId", companyId)
				.filter("serviceDate >=", fromDate)
				.filter("serviceDate <=", toDate)
				.filter("employee", technicianName)
				.list();
		
		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
		
		Company compEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();

		Comparator<Service> servDateComp=new Comparator<Service>() {
			@Override
			public int compare(Service arg0, Service arg1) {
				return arg0.getServiceDate().compareTo(arg1.getServiceDate());
			}
		};
		Collections.sort(servicelist, servDateComp);
		
		
		ByteArrayOutputStream pdfstream = new ByteArrayOutputStream();

		SRFormatVersion1 srformatv1 = new SRFormatVersion1();
		srformatv1.document = new Document();
		
		try {
			PdfWriter writer = PdfWriter.getInstance(srformatv1.document, pdfstream);
		
		
		srformatv1.document.setPageSize(PageSize.A4);
		srformatv1.document.setMargins(20, 20, 120, 100);
		srformatv1.document.setMarginMirroring(false);
			
  	    HeaderFooterPageEvent event = new HeaderFooterPageEvent(compEntity,null,"no");
  	    writer.setPageEvent(event);
		
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		srformatv1.document.open();
		srformatv1.loadMultipleSRCopies(servicelist,"no");
		srformatv1.document.close();
		

		
		String compName="SR - ";
		
		String mailSub = compName;
		
		String mailMsg = "Dear Sir/Ma'am ,"+"\n"+"\n"+"\n"+ " <BR>"+"Please find the service record copies.";

		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", "EnableSendGridEmailApi", companyId)){
			SendGridEmailServlet sdEmail=new SendGridEmailServlet();
			sdEmail.sendMailWithSendGrid(compEntity.getEmail(), toemaillst, null, null, mailSub, mailMsg, "text/html",pdfstream,"SR Copies.pdf","application/pdf",compEntity.getDisplayNameForCompanyEmailId());
		}else{
			Email email = new Email();
			email.sendMailWithGmail(compEntity.getEmail(), toemaillst, null, null, mailSub, mailMsg, "text/html",pdfstream,"SR Copies.pdf","application/pdf");
		}
		
		logger.log(Level.SEVERE, "SR copy email sending process completed here ");
		
	}

	@Override
	public String createAccountingInterface(SuperModel model) {
		
		if(model instanceof Invoice) {
			Invoice invoiceEntity = (Invoice) model;
			invoiceEntity.accountingInterface(invoiceEntity,"CommonServiceImpl");
			List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("companyId", invoiceEntity.getCompanyId())
										.filter("status", "Closed").filter("invoiceCount", invoiceEntity.getCount()).list();
			logger.log(Level.SEVERE, "accountingInterface size"+paymentlist.size());
			
			for(CustomerPayment custpayment : paymentlist){
				custpayment.accountingInterface("CommonServiceImpl");
			}
		}	
		return "Accounting interface created successfully";
	}

	@Override
	public String createSpecificDocumentAccountingInterface(SuperModel model) {
		if(model instanceof Invoice) {
			Invoice invoiceEntity = (Invoice) model;
			invoiceEntity.accountingInterface(invoiceEntity,"CommonServiceImpl");
			
//			if(invoiceEntity.getStatus().equals(Invoice.CANCELLED)) {
//				List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class).filter("companyId", invoiceEntity.getCompanyId())
//							.filter("invoiceCount", invoiceEntity.getCount()).list();
//				logger.log(Level.SEVERE, "paymentlist size"+paymentlist.size());
//				ArrayList<Integer> paymentlistid = new  ArrayList<Integer>();
//				for(CustomerPayment payment : paymentlist) {
//					paymentlistid.add(payment.getCount());
//				}
//				List<AccountingInterface> acclistpaymentlist = ofy().load().type(AccountingInterface.class).filter("documentID IN", paymentlistid) 
//							.filter("documentType", "Customer Payment").list();
//				
//			}
		}
		if(model instanceof CustomerPayment) {
			CustomerPayment paymentEntity = (CustomerPayment) model;
			paymentEntity.accountingInterface("CommonServiceImpl");
		}
		return "Accounting interface created successfully";
	}

	@Override
	public Invoice updateInvoiceOrderTypeInProduct(Invoice invoiceEntity) {
		
		ArrayList<SalesOrderProductLineItem> list = invoiceEntity.getSalesOrderProducts();
		for(SalesOrderProductLineItem salesline : list){
			salesline.setTypeOfOrder(AppConstants.ORDERTYPESALES);
		}
		invoiceEntity.setSalesOrderProducts(list);
		ofy().save().entity(invoiceEntity);
		
		return invoiceEntity;
	}

	@Override
	public String deleteEntryFromAccountingInterface(SuperModel model) {
		
		if(model instanceof Invoice) {
			Invoice invoiceEntity = (Invoice) model;
			List<AccountingInterface> accountinginterfacelist = ofy().load().type(AccountingInterface.class).filter("companyId", invoiceEntity.getCompanyId())
							.filter("documentStatus", Invoice.APPROVED).filter("documentID", invoiceEntity.getCount()).list();
			logger.log(Level.SEVERE, "accountingInterface size"+accountinginterfacelist.size());
			if(accountinginterfacelist.size()>0){
				ofy().delete().entities(accountinginterfacelist);
			}
		}
		if(model instanceof CustomerPayment) {
			CustomerPayment paymentEntity = (CustomerPayment) model;
			List<AccountingInterface> accountinginterfacelist = ofy().load().type(AccountingInterface.class).filter("companyId", paymentEntity.getCompanyId())
					.filter("documentStatus", CustomerPayment.CLOSED).filter("documentID", paymentEntity.getCount()).list();
			logger.log(Level.SEVERE, "accountingInterface size"+accountinginterfacelist.size());
			if(accountinginterfacelist.size()>0){
				ofy().delete().entities(accountinginterfacelist);
			}
		
		}
		return "Accounting interface entry deleted successfully";
	}

	@Override
	public String suspendServices(List<Service> servicelist, String suspendRemark) {
		
		logger.log(Level.SEVERE, "Services list size"+servicelist.size());
		if (servicelist.size() != 0 && servicelist.size() <= 100) {
			suspendAllServices(servicelist, suspendRemark);
			logger.log(Level.SEVERE, "Services cancelled successfully.");
			return "Services cancelled successfully.";
		} else if(servicelist.size() != 0 && servicelist.size() > 100){
			long companyId = 0;
			List<Integer> serviceIdList = new ArrayList<Integer>();
			for(Service ser : servicelist){
				serviceIdList.add(ser.getCount());
				companyId = ser.getCompanyId();
			}
			Gson gson = new Gson();
			String str = gson.toJson(serviceIdList);

			Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "Suspend Services").param("Remark", suspendRemark)
					.param("serviceId", str).param("companyId", companyId+""));
			return "Services cancellation process started. It may take few minutes";
		}else {
			return "No service is selected for cancellation.";
		}
		
	}

	public void suspendAllServices(List<Service> servicelist, String suspendRemark) {
		
		for(Service ser:servicelist){
			ser.setStatus(Service.SERVICESUSPENDED);
			ser.setRecordSelect(false);
			ser.setRemark(suspendRemark);
		}
		ofy().save().entities(servicelist);
	}

	@Override
	public String validateNumberRange(String className, long numberrange, long companyId) {
		logger.log(Level.SEVERE, "numberrange "+numberrange);
		logger.log(Level.SEVERE, "className "+className);

		long tonumberRange = numberrange+100000;
		logger.log(Level.SEVERE, "tonumberRange "+tonumberRange);
		
		if(className.contains("C_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				logger.log(Level.SEVERE,"i"+i);
				logger.log(Level.SEVERE,"nextnumber"+nextnumber);

				List<Contract> contractlist = ofy().load().type(Contract.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "contractlist size "+contractlist.size());
				
				if(contractlist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		else if(className.contains("S_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				List<Service> servicelist = ofy().load().type(Service.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "Service size "+servicelist.size());
				
				if(servicelist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		else if(className.contains("B_") || className.contains("SOB_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				List<BillingDocument> billingDocumentlist = ofy().load().type(BillingDocument.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "billingDocumentlist size "+billingDocumentlist.size());
				
				if(billingDocumentlist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		else if(className.contains("I_") || className.contains("SOI_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				List<Invoice> invoicelist = ofy().load().type(Invoice.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "invoicelist size "+invoicelist.size());
				
				if(invoicelist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		else if(className.contains("P_") || className.contains("SOP_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				List<CustomerPayment> paymentlist = ofy().load().type(CustomerPayment.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "paymentlist size "+paymentlist.size());
				
				if(paymentlist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		else if(className.contains("SO_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				List<SalesOrder> salesorderlist = ofy().load().type(SalesOrder.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "salesorderlist size "+salesorderlist.size());
				
				if(salesorderlist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		else if(className.contains("D_")){
			for(long i=numberrange;i<tonumberRange;i+=1000 ){
				long nextnumber=numberrange+1000;
				List<DeliveryNote> deliverynotelist = ofy().load().type(DeliveryNote.class)
						.filter("companyId", companyId).filter("count >=", i).filter("count <=", nextnumber).list();
				logger.log(Level.SEVERE, "deliverynotelist size "+deliverynotelist.size());
				
				if(deliverynotelist.size()>0){
					return "Number already used from "+i+" to "+nextnumber +" please add number range which is not used before in the system";
				}
			}
		}
		
		return "";
	}

	@Override
	public String updateAccountingInterface(SuperModel model) {
		
		deleteEntryFromAccountingInterface(model);
		
		createSpecificDocumentAccountingInterface(model);
		
		return "updated successfully";
	}

	@Override
	public String deleteAndCreateAccountingInterfaceData(Date fromDate, Date toDate, long companyId,boolean paymentDocflag) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		logger.log(Level.SEVERE, "fromDate" + fromDate);
		logger.log(Level.SEVERE, "toDate" + toDate);

		Calendar cal=Calendar.getInstance();
		cal.setTime(fromDate);
		cal.add(Calendar.DATE, 0);
		
		Date formDatewithtime=null;
		
		try {
			formDatewithtime=dateFormat.parse(dateFormat.format(cal.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "formDatewithtime" + formDatewithtime);

		Calendar cal3=Calendar.getInstance();
		cal3.setTime(toDate);
		cal3.add(Calendar.DATE, 0);
		
		Date todatewithtime=null;
		
		try {
			todatewithtime=dateFormat.parse(dateFormat.format(cal3.getTime()));
			cal3.set(Calendar.HOUR_OF_DAY,23);
			cal3.set(Calendar.MINUTE,59);
			cal3.set(Calendar.SECOND,59);
			cal3.set(Calendar.MILLISECOND,999);
			todatewithtime=cal3.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "todatewithtime" + todatewithtime);
		logger.log(Level.SEVERE, "paymentDocflag" + paymentDocflag);

		if(paymentDocflag){
			try {
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
						.param("OperationName", "DeleteAndCreateAccountingInterfacePaymentDocuments")
						.param("companyId", companyId+ "")
						.param("fromDate", fromDate+"" )
						.param("toDate", toDate+""));
				logger.log(Level.SEVERE, "for payment document" + queue);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else{
			try {
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue")
						.param("OperationName", "DeleteAndCreateAccountingInterface")
						.param("companyId", companyId+ "")
						.param("fromDate", fromDate+"" )
						.param("toDate", toDate+""));
				logger.log(Level.SEVERE, "sr" + queue);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		
		return "Update process started";
	}

	@Override
	public String cancelCustomerBranchServices(
			ArrayList<CustomerBranchDetails> customerbranchlist, long companyId,String Cancellationremark,String loggedinuser, int customerId) {
		logger.log(Level.SEVERE, "customerbranchlist size" + customerbranchlist.size());

		try {
			List<String> strcustomerbranchlist = new ArrayList<String>();
			for(CustomerBranchDetails customerbranch : customerbranchlist){
//				strcustomerbranchlist.add(customerbranch.getBusinessUnitName());
				
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "Cancel Services").param("Remark", Cancellationremark)
						.param("customerbranchName", customerbranch.getBusinessUnitName()).param("loggedinuser", loggedinuser)
						.param("customerId",customerId+"").param("companyId", companyId+""));
			}
//			Gson gson = new Gson();
//			String str = gson.toJson(strcustomerbranchlist);

			
			return "Services cancellation process started. It may take few minutes";
		} catch (Exception e) {
			return "Failed please try again or contact to eva support";
		}
//		
	}

	@Override
	public String updateCustomerBranchServiceAddressInServices(int customerId, long companyId) {

		List<CustomerBranchDetails> customerbranchlist = ofy().load().type(CustomerBranchDetails.class).filter("companyId", companyId)
						.filter("cinfo.count", customerId).filter("status", true).list();
		logger.log(Level.SEVERE,"customerbranchlist size"+customerbranchlist.size());
		
		if(customerbranchlist.size()>0){
			for(CustomerBranchDetails customerbranch : customerbranchlist){
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "Update Customer Branch Service Address")
						.param("customerId",customerId+"").param("customerbranchName", customerbranch.getBusinessUnitName())
						.param("companyId", companyId+""));
			}
		}
		
		return "Service updation process started. It may take few minutes";
	}
	
	@Override
	public Integer getNumberOfHRLicense(long companyId) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"in getNumberOfHRLicense");
		ServerAppUtility utility=new ServerAppUtility();
		Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();			
		int count= utility.getNumberOfActiveLicense(comp,AppConstants.LICENSETYPELIST.HRLICENSE);
		return count;
	}

	@Override
	public Integer getNumberOfPayrollProcessedInMonth(long companyId, String salaryPeriod) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE,"in getNumberOfPayrollProcessedInMonth");
		List<PaySlip> salSlipList = ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", salaryPeriod).list();			
		if(salSlipList!=null)
			return salSlipList.size();
		else
			return 0;
	}
	
}
