package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class CNCBillAnnexureBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5458022469769230097L;
	/**
	 * 
	 */
	

	int srNo;
	String productName;
	double noOfStaff;
	String month;
	double oldCTC;
	double newCTC;
	double arrearsCTC;
	double manDays;
	double perDayRate;
	double total;
	double taxableAmount;
	
	public CNCBillAnnexureBean() {
		this.productName = "";
		this.month = "";		
	}

	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getNoOfStaff() {
		return noOfStaff;
	}

	public void setNoOfStaff(double noOfStaff) {
		this.noOfStaff = noOfStaff;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public double getOldCTC() {
		return oldCTC;
	}

	public void setOldCTC(double oldCTC) {
		this.oldCTC = oldCTC;
	}

	public double getNewCTC() {
		return newCTC;
	}

	public void setNewCTC(double newCTC) {
		this.newCTC = newCTC;
	}

	public double getArrearsCTC() {
		return arrearsCTC;
	}

	public void setArrearsCTC(double arrearsCTC) {
		this.arrearsCTC = arrearsCTC;
	}

	public double getManDays() {
		return manDays;
	}

	public void setManDays(double manDays) {
		this.manDays = manDays;
	}

	public double getPerDayRate() {
		return perDayRate;
	}

	public void setPerDayRate(double perDayRate) {
		this.perDayRate = perDayRate;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	
	
}
