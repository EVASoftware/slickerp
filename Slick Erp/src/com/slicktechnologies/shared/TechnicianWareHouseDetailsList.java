package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class TechnicianWareHouseDetailsList extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1885655366034352738L;
	
	@Index
	protected String parentWareHouse;
	@Index
	protected String parentStorageLocation;
	@Index
	protected String pearentStorageBin;
	@Index
	protected ArrayList<TechnicianWarehouseDetails> wareHouseList;
	
	public TechnicianWareHouseDetailsList() {
		parentWareHouse = "";
		parentStorageLocation = "";
		pearentStorageBin = "";
		wareHouseList = new ArrayList<TechnicianWarehouseDetails>();
	}
	public String getParentWareHouse() {
		return parentWareHouse;
	}

	public void setParentWareHouse(String parentWareHouse) {
		this.parentWareHouse = parentWareHouse;
	}

	public String getParentStorageLocation() {
		return parentStorageLocation;
	}

	public void setParentStorageLocation(String parentStorageLocation) {
		this.parentStorageLocation = parentStorageLocation;
	}

	public String getPearentStorageBin() {
		return pearentStorageBin;
	}

	public void setPearentStorageBin(String pearentStorageBin) {
		this.pearentStorageBin = pearentStorageBin;
	}
	
	public ArrayList<TechnicianWarehouseDetails> getWareHouseList() {
		return wareHouseList;
	}
	public void setWareHouseList(List<TechnicianWarehouseDetails> wareHouseList) {
		if(wareHouseList != null){			
			this.wareHouseList =new ArrayList<TechnicianWarehouseDetails>();
			this.wareHouseList.addAll(wareHouseList);
		}		
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
}
