package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

// TODO: Auto-generated Javadoc
/**
 * Represents all Sales Line Items.A sales Line Item can be {@link ServiceProduct} or 
 * {@link ItemProduct}. Corresponding table should have capability to display both types of 
 * {@link ServiceProduct} and {@link ItemProduct}.
 */
@Embed
public class SalesLineItem implements Serializable
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5841111129485718832L;
	
	/** ************************************************Entity Attributes******************************. */
	/** The service product */
	private ServiceProduct serviceProduct;
	
	/** The item product */
	private ItemProduct itemProduct;
	
	/** The start date given in quotation or contract for this sales line item */
	private Date startDate;
  
	  /**  The quantity represents no of products in one sales line item. */
	  protected double quantity;
	  
	  /** The % discount given on one sales line item */
	  protected double percentageDiscount;
	  
	  /** The flat discount given on one sales line item */
	  protected double flatDiscount;
	  
	  
	  /** This field is used at the time of Contract renewal to store old price of product */
	  /** update made by Anil 24/09/2015 */
	  
	  protected double oldProductPrice;
	  
	  
	  /**  This field used for warranty until for product */ 
	  protected Date warrantyUntil;
	  
	  String serviceTaxEdit;
	  String vatTaxEdit;
	  
	  double discountAmt; 
	  
	  
	  protected String termsoftreatment;
	  
	  protected String premisesDetails;
	  
	  
	  // ******rohan added ***********************
	  
	  int productSrNo;
	  
	  
	// vijay adeed this column for area 
		  String area = "NA";
		  
    // Date 7 feb 2017 added by vijay for adding remark
			
	  protected String remark;
	  
		 
	  /** This field is used to store the typeoftreatment value as per sonu sir requirement */
	  /** update made by Manisha 17/02/2018 */
	  
      protected String profrequency;
      
	  protected String typeoftreatment;
	  /**End**/		 
	  
	  
	  /**
	   * This variable is used in Service Po
	   * Date : 17-10-2016 By Anil
	   * Release : 30 Sept 2016 
	   * Project : PURCHASE MODIFICATION(NBHC)
	   */
	  
	  String warehouseName;
	  
	  /**
	   * End
	   */
	  
	  /**
	   * Date : 03-04-2017 By ANIL
	   */
	  
	  String branchSchedulingInfo;
	  String branchSchedulingInfo1;
	  String branchSchedulingInfo2;
	  String branchSchedulingInfo3;
	  String branchSchedulingInfo4;
	  
	  /**
	   * Date:12-06-2017 By ANIL
	   */
	  
	  String branchSchedulingInfo5;
	  String branchSchedulingInfo6;
	  String branchSchedulingInfo7;
	  String branchSchedulingInfo8;
	  String branchSchedulingInfo9;
	  
	  @Index
	  Date endDate;
	  
	  
	  /** 28-09-2107 sagar sore [totalAmount variable added with setters and getters for storing total amount instead of calculating each time] **/
	  protected double totalAmount;
	  
	  /**
	   * Date : 17-04-2018 By ANIL
	   * this field stores whether the tax applies to product is inclusive or not
	   * for Sales order/HVAC/Rohan/Nitin Sir
	   */
	  
	  boolean isInclusive;
	  
	  /** date 23/11/2018 added by komal for hvac amc number **/
	  protected String amcNumber;
	  /**
	   * end
	*/
	   /** date 29.5.218 added by komal added by komal **/
	   protected boolean isComplainService;
	  
	  /** 30-04-2018 BY vijay for customer branches scheduling updated String to hashmap ***/
	  @EmbedMap
	  @Serialize
	  protected HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchSchedulingInfo;

	  /**
	   * nidhi
	   * 8-08-2018
	   * for map model no , serial no
	   */
	  protected String proModelNo;
	  protected String proSerialNo;
	  /**
	   * end
	   */
	  /**
  	 * ************************************************Default Ctor*********************************************.
  	 */
	  /**
	   * nidhi
	   * 11-09-2018
	   *  *:*:*
	   */
	  protected String areaUnit;
	  protected String areaUnitEditValue;
	  @EmbedMap
	  @Serialize
	  protected HashMap<String, ArrayList<ProductGroupList>> serviceProdGroupList;
	  
	  /**
		 * nidhi 21-08-2018 for map serial number details
		 */
			@EmbedMap
		  @Serialize
		  protected HashMap<Integer, ArrayList<ProductSerialNoMapping>>  proSerialNoDetails  = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

	  /**
	/**
	 * Instantiates a new sales line item.
	 */
	  /** date 23.11.2018 added by komal for previous tax storage**/
	  protected Tax previousTax1;
	  protected Tax previousTax2;
	  /**
	   * end komal
	   */
	  /** date 11.12.2018 added by komal for nbhc service branches save**/
	  @EmbedMap
	  @Serialize
	  protected HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchesInfo; 
	  
	/**
	 * @author Anil
	 * @since 30-06-2020
	 * Added service wise bill flag for calculating total amount (Rate*no. of services)
	 * for Sai Care raised by Vaishnavi and Nitin Sir
	 */
	  boolean isServiceRate;
	  
	  /**
	   * @author Anil
	   * @since 20-07-2020
	   * for PTSPL raised by Rahul Tiwari
	   */
	  boolean isReadOnly;
	  
	protected Tax refproductmasterTax1;
	protected Tax refproductmasterTax2;
		
	/**
	 * @author Vijay Date :- 25-05-2023
	 * Des :- added flag to manage price data on contract renewal popup screen revised price
	 */
	boolean revisedpriceflag;
	double contractRenewalRevisedPercentage;
	double renewalProductNetpayable;
	
	public SalesLineItem() 
	{
		super();
		quantity=0;
		warehouseName="";
		remark="";
		
//		branchSchedulingInfo="";
//		branchSchedulingInfo1="";
//		branchSchedulingInfo2="";
//		branchSchedulingInfo3="";
//		branchSchedulingInfo4="";
//		
//		branchSchedulingInfo5="";
//		branchSchedulingInfo6="";
//		branchSchedulingInfo7="";
//		branchSchedulingInfo8="";
//		branchSchedulingInfo9="";
		profrequency="";
		typeoftreatment="";
		/** date 29.5.218 added by komal added by komal **/
		isComplainService = false;
		/**
		 * nidhi
		 * 8-08-2018
		 * 
		 */
		proSerialNo = "";
		proModelNo = "";
		/**
		   * nidhi
		   * 11-09-2018
		   *  *:*:*
		   */
		 areaUnit = "";
		 areaUnitEditValue ="";
		 /** date 23.11.2018 added by komal for previous tax storage**/
		  previousTax1 = new Tax();
		  previousTax2 = new Tax();
		  /**
		   * end komal
		   */
		  /** date 11.12.2018 added by komal for nbhc service branches save**/
		  serviceBranchesInfo = new HashMap<Integer, ArrayList<BranchWiseScheduling>>(); 
		  
		  refproductmasterTax1 = new Tax();
		  refproductmasterTax2 = new Tax();
		  
		  revisedpriceflag = false;
	}
	
	
	public HashMap<Integer, ArrayList<BranchWiseScheduling>> getCustomerBranchSchedulingInfo() {
		return customerBranchSchedulingInfo;
	}


	public void setCustomerBranchSchedulingInfo(
			HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchSchedulingInfo) {
		this.customerBranchSchedulingInfo = customerBranchSchedulingInfo;
	}

	  public String getTermsoftreatment() {
		return serviceProduct.getTermsoftreatment();
	  }

	  public void setTermsoftreatment(String termsoftreatment) {
		  serviceProduct.setTermsoftreatment(termsoftreatment);
	  }

	  
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBranchSchedulingInfo1() {
		return branchSchedulingInfo1;
	}

	public void setBranchSchedulingInfo1(String branchSchedulingInfo1) {
		this.branchSchedulingInfo1 = branchSchedulingInfo1;
	}

	public String getBranchSchedulingInfo2() {
		return branchSchedulingInfo2;
	}

	public void setBranchSchedulingInfo2(String branchSchedulingInfo2) {
		this.branchSchedulingInfo2 = branchSchedulingInfo2;
	}

	public String getBranchSchedulingInfo3() {
		return branchSchedulingInfo3;
	}

	public void setBranchSchedulingInfo3(String branchSchedulingInfo3) {
		this.branchSchedulingInfo3 = branchSchedulingInfo3;
	}

	public String getBranchSchedulingInfo4() {
		return branchSchedulingInfo4;
	}

	public void setBranchSchedulingInfo4(String branchSchedulingInfo4) {
		this.branchSchedulingInfo4 = branchSchedulingInfo4;
	}

	public String getBranchSchedulingInfo() {
		return branchSchedulingInfo;
	}

	public void setBranchSchedulingInfo(String branchSchedulingInfo) {
		this.branchSchedulingInfo = branchSchedulingInfo;
	}
	
	
	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	/**
	 * ************************************************Getter/Setter*********************************************.
	 *
	 * @return the prduct
	 */
	/**
	 * Gets the prduct.
	 * @return the prduct
	 */
	public SuperProduct getPrduct()
	{

		if(serviceProduct==null)
			return itemProduct;
		
		else if(itemProduct==null)
			return serviceProduct;
		else
		{
			return null;
		}	
	}
	
	/**
	 * Sets the prduct.
	 *
	 * @param product the new prduct
	 */
	public void setPrduct(SuperProduct product) 
	{
		if(product instanceof ServiceProduct)
			serviceProduct=(ServiceProduct) product;
		else if(product instanceof ItemProduct)
			itemProduct=(ItemProduct) product;
		System.out.println("Service Product IS "+serviceProduct);
		System.out.println("Item Product IS "+itemProduct);
	}
	
	/**
	 * Gets the qty.
	 * @return the qty
	 */
	public double getQty() 
	{
		return quantity;
	}
	
	
	/**
	 * Sets the quantity.
	 * @param quantity the new quantity
	 */
	public void setQuantity(Double quantity)
	{
		if(quantity!=null)
		   this.quantity = quantity;
	}
	/**
	 * Gets the percentage discount.
	 * @return the percentage discount
	 */
	public Double getPercentageDiscount()
	{
			return percentageDiscount;
	}
	/**
	 * Sets the percentage discount.
	 * @param percentageDiscount the new percentage discount
	 */
	public void setPercentageDiscount(Double percentageDiscount) 
	{
		if(percentageDiscount!=null)
		   this.percentageDiscount = percentageDiscount;
	}
	/**
	 * Gets the flat discount.
	 * @return the flat discount
	 */
	public double getFlatDiscount()
	{
		return flatDiscount;
	}
	
	/**
	 * Sets the flat discount.
	 * @param flatDiscount the new flat discount
	 */
	public void setFlatDiscount(Double flatDiscount) 
	{
		if(flatDiscount!=null)
		  this.flatDiscount = flatDiscount;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		if(serviceProduct==null)
			return itemProduct.getPrice();
		else if(itemProduct==null)
			return serviceProduct.getPrice();
		else 
			return 0;
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public int getDuration() {
		if(serviceProduct!=null)
		  return serviceProduct.getDuration();
		else
			return -1;
	}

	/**
	 * Gets the number of services.
	 *
	 * @return the number of services
	 */
	public int getNumberOfServices() {
		if(serviceProduct!=null)
			return serviceProduct.getNumberOfServices();
		else 
			return -1;
	}

	/**
	 * Gets the service tax.
	 *
	 * @return the service tax
	 */
	public Tax getServiceTax() {
		if(serviceProduct!=null)
			return serviceProduct.getServiceTax();
		else
			return itemProduct.getServiceTax();
	}

	/**
	 * Sets the service tax.
	 *
	 * @param serviceTax the new service tax
	 */
	public void setServiceTax(Tax serviceTax) {
		if(serviceProduct!=null)
			serviceProduct.setServiceTax(serviceTax);
		else
			itemProduct.setServiceTax(serviceTax);
	}

	/**
	 * Sets the number of service.
	 *
	 * @param val1 the new number of service
	 */
	public void setNumberOfService(Integer val1) {
		serviceProduct.setNumberOfService(val1);
	}

	/**
	 * Sets the count.
	 *
	 * @param long1 the new count
	 */
	public void setCount(int long1) {
		if(itemProduct!=null)
			itemProduct.setCount(long1);
		else
			 serviceProduct.setCount(long1);
		
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		if(itemProduct!=null)
			return itemProduct.getId();
		else 
			return serviceProduct.getId();
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		itemProduct.setId(id);
	}

	/**
	 * Sets the deleted.
	 *
	 * @param deleted the new deleted
	 */
	public void setDeleted(boolean deleted) {
		itemProduct.setDeleted(deleted);
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(Long companyId) {
		itemProduct.setCompanyId(companyId);
	}

	/**
	 * Checks if is sell product.
	 *
	 * @return true, if is sell product
	 */
	public boolean isSellProduct() {
		return itemProduct.isSellProduct();
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		if(itemProduct!=null)
			return itemProduct.getProductCode();
		else 
			return serviceProduct.getProductCode();
	}

	/**
	 * Sets the sell product.
	 *
	 * @param isSellProduct the new sell product
	 */
	public void setSellProduct(boolean isSellProduct) {
		itemProduct.setSellProduct(isSellProduct);
	}

	/**
	 * Checks if is purchase product.
	 *
	 * @return true, if is purchase product
	 */
	public boolean isPurchaseProduct() {
		return itemProduct.isPurchaseProduct();
	}

	/**
	 * Sets the product code.
	 *
	 * @param prodCode the new product code
	 */
	public void setProductCode(String prodCode) {
		
		if(itemProduct!=null)
			itemProduct.setProductCode(prodCode);
		else
			 serviceProduct.setProductCode(prodCode);
		
	}

	/**
	 * Sets the purchase product.
	 *
	 * @param isPurchaseProduct the new purchase product
	 */
	public void setPurchaseProduct(boolean isPurchaseProduct) {
		itemProduct.setPurchaseProduct(isPurchaseProduct);
	}

	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		if(itemProduct!=null)
			return itemProduct.getProductName();
		else
			return serviceProduct.getProductName();
	}

	/**
	 * Gets the vat tax.
	 *
	 * @return the vat tax
	 */
	public Tax getVatTax() {
		if(itemProduct!=null)
			return itemProduct.getVatTax();
		else
			return serviceProduct.getVatTax();
	}

	/**
	 * Sets the product name.
	 *
	 * @param prodName the new product name
	 */
	public void setProductName(String prodName) {
		if(itemProduct!=null)
			itemProduct.setProductName(prodName);
		else
			serviceProduct.setProductName(prodName);
	}

	/**
	 * Sets the vat tax.
	 *
	 * @param vatTaxEntity the new vat tax
	 */
	public void setVatTax(Tax vatTaxEntity) {
		if(itemProduct!=null)
			itemProduct.setVatTax(vatTaxEntity);
		else
			serviceProduct.setVatTax(vatTaxEntity);
	}
	
	

	/**
	 * Gets the unit of measurement.
	 *
	 * @return the unit of measurement
	 */
	public String getUnitOfMeasurement() {
		if(itemProduct!=null)
			return itemProduct.getUnitOfMeasurement();
		else
			return serviceProduct.getUnitOfMeasurement();
	}

	/**
	 * Gets the barcode.
	 *
	 * @return the barcode
	 */
	public String getBarcode() {
		if(itemProduct!=null)
			return itemProduct.getBarcode();
		else
			return null; 
	}

	/**
	 * Sets the barcode.
	 *
	 * @param barcode the new barcode
	 */
	public void setBarcode(String barcode) {
		itemProduct.setBarcode(barcode);
	}

	/**
	 * Sets the unit of measurement.
	 *
	 * @param unitOfMeasurement the new unit of measurement
	 */
	public void setUnitOfMeasurement(String unitOfMeasurement) {
		if(itemProduct!=null){
			itemProduct.setUnitOfMeasurement(unitOfMeasurement);
		}else{
			serviceProduct.setUnitOfMeasurement(unitOfMeasurement);
		}
	}

	/**
	 * Gets the product category.
	 *
	 * @return the product category
	 */
	public String getProductCategory() {
		if(itemProduct!=null)
			return itemProduct.getProductCategory();
		else
			return serviceProduct.getProductCategory();
	}

	/**
	 * Sets the product category.
	 *
	 * @param productCategory the new product category
	 */
	public void setProductCategory(String productCategory) {
		
		if(itemProduct!=null)
			 itemProduct.setProductCategory(productCategory);
		else
			 serviceProduct.setProductCategory(productCategory);
		
	}

	
	/**
	 * Gets the specification.
	 *
	 * @return the specification
	 */
	public String getSpecification() {
		if(itemProduct!=null)
			return itemProduct.getSpecification();
		else
			return serviceProduct.getSpecification();
	}

	/**
	 * Sets the spec.
	 *
	 * @param spec the new spec
	 */
	public void setSpec(String spec) {
		itemProduct.setSpec(spec);
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		if(itemProduct!=null)
			return itemProduct.getComment();
		else
			return serviceProduct.getComment();
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		itemProduct.setComment(comment);
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(double price) {
		if(itemProduct!=null)
		     itemProduct.setPrice(price);
		else
			serviceProduct.setPrice(price);
	}

	/**
	 * Gets the lbt tax.
	 *
	 * @return the lbt tax
	 */
/*	public Tax getLbtTax() {
		if(itemProduct!=null)
			return itemProduct.getLbtTax();
		else
			return serviceProduct.getLbtTax();
	}
*/
	/**
	 * Sets the lbt tax.
	 *
	 * @param lbtTaxEntity the new lbt tax
	 */
	/*public void setLbtTax(Tax lbtTaxEntity) {
		itemProduct.setLbtTax(lbtTaxEntity);
	}*/

	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		if(itemProduct!=null)
			return itemProduct.isStatus();
		else
			return serviceProduct.isStatus();
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		itemProduct.setStatus(status);
	}

	/**
	 * Gets the terms and conditions.
	 *
	 * @return the terms and conditions
	 */
	public DocumentUpload getTermsAndConditions() {
		if(itemProduct!=null)
		  return itemProduct.getTermsAndConditions();
		else
			return serviceProduct.getTermsAndConditions();
	}

	/**
	 * Sets the terms and conditions.
	 *
	 * @param termsAndConditions the new terms and conditions
	 */
	public void setTermsAndConditions(DocumentUpload termsAndConditions) {
		if(itemProduct!=null){
			itemProduct.setTermsAndConditions(termsAndConditions);
		}
		else{
			serviceProduct.setTermsAndConditions(termsAndConditions);
		}
	}

	/**
	 * Gets the product image.
	 *
	 * @return the product image
	 */
	public DocumentUpload getProductImage() {
		if(itemProduct!=null)
			return itemProduct.getProductImage();
		else 
			return serviceProduct.getProductImage();
	}

	/**
	 * Sets the product image.
	 *
	 * @param productImage the new product image
	 */
	public void setProductImage(DocumentUpload productImage) {
		if(itemProduct!=null){
			itemProduct.setProductImage(productImage);
		}
		else{
			serviceProduct.setProductImage(productImage);
		}
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		if(serviceProduct!=null)
		   return startDate;
		else
			return null;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		if(serviceProduct!=null)
		  this.startDate = startDate;
	}

	public void setDuration(Integer duration) {
		if(serviceProduct!=null)
			serviceProduct.setDuration(duration);
		
	}

	public double getOldProductPrice() {
		return oldProductPrice;
	}

	public void setOldProductPrice(double oldProductPrice) {
		this.oldProductPrice = oldProductPrice;
	}

	public String getServiceTaxEdit() {
		return serviceTaxEdit;
	}

	public void setServiceTaxEdit(String serviceTaxEdit) {
		this.serviceTaxEdit = serviceTaxEdit;
	}

	public String getVatTaxEdit() {
		return vatTaxEdit;
	}

	public void setVatTaxEdit(String vatTaxEdit) {
		this.vatTaxEdit = vatTaxEdit;
	}
	
	
	public void setServicingTime(Double servicingTime) {
		if(serviceProduct!=null)
			serviceProduct.setServiceTime(servicingTime);;
	}
	
	
	public Double getServicingTIme(){
		if(serviceProduct!=null){
			return serviceProduct.getServiceTime();
		}else
			return 0d;
	}

	public double getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public Date getWarrantyUntil() {
		return warrantyUntil;
	}

	public void setWarrantyUntil(Date warrantyUntil) {
		this.warrantyUntil = warrantyUntil;
	}
	
	
	public int getWarrantyInMonth(){
		return this.itemProduct.getWarrantyPeriodInMonths();
	}
	
	public void setWarrantyInMonth(int warrantymonth){
		this.itemProduct.setWarrantyPeriodInMonths(warrantymonth);
	}

	public String getPremisesDetails() {
		return premisesDetails;
	}

	public void setPremisesDetails(String premisesDetails) {
		this.premisesDetails = premisesDetails;
	}

	public int getProductSrNo() {
		return productSrNo;
	}

	public void setProductSrNo(int productSrNo) {
		this.productSrNo = productSrNo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getBranchSchedulingInfo5() {
		return branchSchedulingInfo5;
	}

	public void setBranchSchedulingInfo5(String branchSchedulingInfo5) {
		this.branchSchedulingInfo5 = branchSchedulingInfo5;
	}

	public String getBranchSchedulingInfo6() {
		return branchSchedulingInfo6;
	}

	public void setBranchSchedulingInfo6(String branchSchedulingInfo6) {
		this.branchSchedulingInfo6 = branchSchedulingInfo6;
	}

	public String getBranchSchedulingInfo7() {
		return branchSchedulingInfo7;
	}

	public void setBranchSchedulingInfo7(String branchSchedulingInfo7) {
		this.branchSchedulingInfo7 = branchSchedulingInfo7;
	}

	public String getBranchSchedulingInfo8() {
		return branchSchedulingInfo8;
	}

	public void setBranchSchedulingInfo8(String branchSchedulingInfo8) {
		this.branchSchedulingInfo8 = branchSchedulingInfo8;
	}

	public String getBranchSchedulingInfo9() {
		return branchSchedulingInfo9;
	}

	public void setBranchSchedulingInfo9(String branchSchedulingInfo9) {
		this.branchSchedulingInfo9 = branchSchedulingInfo9;
	}
	
	

	/**
	 * Date 21 jun 2017 added by vijay for Quick Sales Order
	 * @return
	 */
	public double getItemProductAvailableQty(){
		return itemProduct.getAvailableQuantity();
	}
	
	public void setItemProductAvailableQty(double availableQty){
		this.itemProduct.setAvailableQuantity(availableQty);
	}
	
	public String getItemProductWarehouseName(){
		return itemProduct.getWarehouse();
	}
	
	public void setItemProductWarehouoseName(String WarehouseName){
		this.itemProduct.setWarehouse(WarehouseName);
	}
	
	public String getItemProductStrorageLocation(){
		return itemProduct.getStorageLocation();
	}
	
	public void setItemProductStorageLocation(String storageLocationName){
		this.itemProduct.setStorageLocation(storageLocationName);
	}
	
	public String getItemProductStorageBin(){
		return itemProduct.getStorageBin();
	}
	
	public void setItemStorageBin(String storageBin){
		this.itemProduct.setStorageBin(storageBin);
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public String getProfrequency() {
		return profrequency;
	}

    public void setProfrequency(String profrequency) {
		this.profrequency = profrequency;
	}
 

    public String getTypeoftreatment() {
	return typeoftreatment;
   }


    public void setTypeoftreatment(String typeoftreatment) {
	this.typeoftreatment = typeoftreatment;
    }
    
    
    
    /**
     * Date : 17-04-2018 By ANIL
     * This getter/setter are used in salslineitem of sale order and used at the time of creating AMC Contract
     */


	public boolean isInclusive() {
		return isInclusive;
	}

	public void setInclusive(boolean isInclusive) {
		this.isInclusive = isInclusive;
	}
	
	/**
	 * Date :18-04-2018 BY ANIL
	 */
	
	public int getNo_Of_Services(){
    	if(itemProduct!=null){
    		return itemProduct.getNoOfServices();
    	}
    	return 0;
    }
    
    public void setNo_Of_Services(int no_Of_Services){
    	if(itemProduct!=null){
    		itemProduct.setNoOfServices(no_Of_Services);
    	}
    }
    
    public int getWarrantyDuration(){
    	if(itemProduct!=null){
    		return itemProduct.getWarrantyDuration();
    	}
    	return 0;
    }
    
    public void setWarrantyDuration(int warrantyDuration){
    	if(itemProduct!=null){
    		itemProduct.setWarrantyDuration(warrantyDuration);
    	}
    }
    
    
    public String getServiceProductName(){
    	if(itemProduct!=null){
    		return itemProduct.getSerProdName();
    	}
    	return null;
    }
    public String getServiceProductCode(){
    	if(itemProduct!=null){
    		return itemProduct.getSerProdCode();
    	}
    	return null;
    }
    public int getServiceProductId(){
    	if(itemProduct!=null){
    		return itemProduct.getSerProdId();
    	}
    	return 0;
    }



	public String getAmcNumber() {
		return amcNumber;
	}



	public void setAmcNumber(String amcNumber) {
		this.amcNumber = amcNumber;
	}


	public boolean isComplainService() {
		return isComplainService;
	}


	public void setComplainService(boolean isComplainService) {
		this.isComplainService = isComplainService;
	}
    
    
    
    /**
     * END
     */
    
    
    
    
	/**
	 * ends here
	 */
	
	
	/**
	 * Date 06/06/2018 by vijay
	 * Des :- Warranty Period of service product
	 * Requirement :- Neatedge Services
	 */
	public int getWarrantyPeriod(){
		if(serviceProduct!=null){
			return serviceProduct.getWarrantyPeriod();
		}else{
			return 0;
		}
	}
	
	public void setWarrantyPeriod(int warrantyPeriod){
			 serviceProduct.setWarrantyPeriod(warrantyPeriod);
	}

	/**
	 * nidhi
	 * 8-08-2018
	 * 
	 * @return
	 */
	public String getProModelNo() {
		if(this.proModelNo != null){
			return this.proModelNo;
		}else{
			return "";
		}
	}


	public void setProModelNo(String proModelNo) {
		this.proModelNo = proModelNo;
	}


	public String getProSerialNo() {
		if(this.proSerialNo != null){
			return proSerialNo;
		}else{
			return "";
		}
	}


	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	
	/**
	 * ends here
	 */

	public String getAreaUnitEditValue() {
		return areaUnitEditValue;
	}


	public String getAreaUnit() {
		return areaUnit;
	}


	public void setAreaUnit(String areaUnit) {
		this.areaUnit = areaUnit;
	}


	public void setAreaUnitEditValue(String areaUnitEditValue) {
		this.areaUnitEditValue = areaUnitEditValue;
	}


	public HashMap<String, ArrayList<ProductGroupList>> getServiceProdGroupList() {
		return serviceProdGroupList;
	}


	public void setServiceProdGroupList(
			HashMap<String, ArrayList<ProductGroupList>> serviceProdGroupList) {
		this.serviceProdGroupList = serviceProdGroupList;
	}


	public Tax getPreviousTax1() {
		return previousTax1;
	}


	public void setPreviousTax1(Tax previousTax1) {
		this.previousTax1 = previousTax1;
	}


	public Tax getPreviousTax2() {
		return previousTax2;
	}


	public void setPreviousTax2(Tax previousTax2) {
		this.previousTax2 = previousTax2;
	}
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}


	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}


	public HashMap<Integer, ArrayList<BranchWiseScheduling>> getServiceBranchesInfo() {
		return serviceBranchesInfo;
	}


	public void setServiceBranchesInfo(
			HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchesInfo) {
		this.serviceBranchesInfo = serviceBranchesInfo;
	}

	
	
	public boolean isServiceRate() {
		return isServiceRate;
	}


	public void setServiceRate(boolean isServiceCal) {
		this.isServiceRate = isServiceCal;
	}


	@Override
	public String toString() {
		return "SalesLineItem [itemProduct=" + itemProduct + "]";
	}


	public boolean isReadOnly() {
		return isReadOnly;
	}


	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}


	public Tax getRefproductmasterTax1() {
		return refproductmasterTax1;
	}


	public void setRefproductmasterTax1(Tax refproductmasterTax1) {
		this.refproductmasterTax1 = refproductmasterTax1;
	}


	public Tax getRefproductmasterTax2() {
		return refproductmasterTax2;
	}


	public void setRefproductmasterTax2(Tax refproductmasterTax2) {
		this.refproductmasterTax2 = refproductmasterTax2;
	}
	
	/**
	 * ends here
	 */
	
	
	public boolean isRevisedpriceflag() {
		return revisedpriceflag;
	}



	public void setRevisedpriceflag(boolean revisedpriceflag) {
		this.revisedpriceflag = revisedpriceflag;
	}


	public double getContractRenewalRevisedPercentage() {
		return contractRenewalRevisedPercentage;
	}


	public void setContractRenewalRevisedPercentage(
			double contractRenewalRevisedPercentage) {
		this.contractRenewalRevisedPercentage = contractRenewalRevisedPercentage;
	}


	public double getRenewalProductNetpayable() {
		return renewalProductNetpayable;
	}


	public void setRenewalProductNetpayable(double renewalProductNetpayable) {
		this.renewalProductNetpayable = renewalProductNetpayable;
	}
	
	
	/*************************************************************************************************************/
	 
	
}
