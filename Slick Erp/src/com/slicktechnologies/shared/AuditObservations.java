package com.slicktechnologies.shared;

import java.util.ArrayList;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class AuditObservations extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7363288489146765122L;

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	@Index
	String observationName;
	
	@Index
	boolean status;
	
	ArrayList<ObservationImpactRecommendation> observationlist;
	
	public AuditObservations(){
		super();
		observationName ="";
		observationlist = new ArrayList<ObservationImpactRecommendation>();
	}

	public String getObservationName() {
		return observationName;
	}

	public void setObservationName(String observationName) {
		this.observationName = observationName;
	}

	public ArrayList<ObservationImpactRecommendation> getObservationlist() {
		return observationlist;
	}

	public void setObservationlist(ArrayList<ObservationImpactRecommendation> observationlist) {
		this.observationlist = observationlist;
	}

	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
	@Override
	public String toString() {
		return observationName;
	}
	
	
	
	
}
