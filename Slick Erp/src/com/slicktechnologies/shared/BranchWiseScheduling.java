package com.slicktechnologies.shared;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Serialize
public class BranchWiseScheduling implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1009668895452583202L;


	boolean check;
	String branchName;
	String day;
	String servicingBranch;
	/** date 14.04.2018 added by komal for orion(area column)**/
	double area;
	
	/** Nidhi 18-09-2018 by nidhi */
	String unitOfMeasurement;
	String setUOMEditValue;
	
	/**
	 * @author Anil
	 * @since 04-06-2020
	 * Adding component and asset details for ups maintaince
	 * for premium tech raised by Rahul Tiwari
	 */
	
	String tierName;
	String tat;
	int assetQty;
	String componentName;
	int durationForReplacement;
	ArrayList<ComponentDetails> componentList=new ArrayList<ComponentDetails>();
	/**
	 * @author Vijay Chougule
	 * Des :- Adding Service duration to map Service Duration in service 
	 * for Life Line Services
	 */
	double serviceDuration;
	
	double amount;
	
	public BranchWiseScheduling() {
		check=false;
		branchName="";
		day="";
		servicingBranch="";
		unitOfMeasurement =  "";
	}

	public boolean isCheck() {
		return check;
	}



	public void setCheck(boolean check) {
		this.check = check;
	}



	public String getBranchName() {
		return branchName;
	}



	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}



	public String getDay() {
		return day;
	}



	public void setDay(String day) {
		this.day = day;
	}

	


	public String getServicingBranch() {
		return servicingBranch;
	}

	public void setServicingBranch(String servicingBranch) {
		this.servicingBranch = servicingBranch;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public String getSetUOMEditValue() {
		return setUOMEditValue;
	}

	public void setSetUOMEditValue(String setUOMEditValue) {
		this.setUOMEditValue = setUOMEditValue;
	}
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	        BranchWiseScheduling customer = (BranchWiseScheduling) o;
	        return branchName == customer.branchName;
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hash(branchName);
	    }

		public String getTierName() {
			return tierName;
		}

		public void setTierName(String tierName) {
			this.tierName = tierName;
		}

		public String getTat() {
			return tat;
		}

		public void setTat(String tat) {
			this.tat = tat;
		}

		public int getAssetQty() {
			return assetQty;
		}

		public void setAssetQty(int assetQty) {
			this.assetQty = assetQty;
		}

		public ArrayList<ComponentDetails> getComponentList() {
			return componentList;
		}

		public void setComponentList(List<ComponentDetails> componentList) {
			ArrayList<ComponentDetails> list=new ArrayList<ComponentDetails>();
			if(componentList!=null&&componentList.size()!=0){
				list.addAll(componentList);
			}
			this.componentList = list;
		}

		public String getComponentName() {
			return componentName;
		}

		public void setComponentName(String componentName) {
			this.componentName = componentName;
		}

		public int getDurationForReplacement() {
			return durationForReplacement;
		}

		public void setDurationForReplacement(int durationForReplacement) {
			this.durationForReplacement = durationForReplacement;
		}

		@Override
		public String toString() {
			return branchName;
		}

		public double getServiceDuration() {
			return serviceDuration;
		}

		public void setServiceDuration(double serviceDuration) {
			this.serviceDuration = serviceDuration;
		}

		public double getAmount() {
			return amount;
		}

		public void setAmount(double amount) {
			this.amount = amount;
		}

		

	    
	    
	
}
