package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

@Embed
public class ContractUploadDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1401595982306868370L;

	HashSet<String> empNameList;
	HashSet<String> paymentTerms;
	HashMap<Integer, String> customerDetail;
	HashMap<Integer, String> productDetail;
	List<CustomerContractDetails> contractDetails;
	List<Customer> custDetails;
//	List<ServiceProduct> serProDetails;
	List<ConfigCategory> payTerms;
	List<Employee> empDetails;
	public ArrayList<String> errorList;
	long companyId;
	String loginName;
	ArrayList<String> exceldatalist = new ArrayList<String>();
	public ContractUploadDetails(){
		empNameList = new HashSet<String>();
		paymentTerms = new HashSet<String>();
		customerDetail = new HashMap<Integer, String>();
		contractDetails = new ArrayList<CustomerContractDetails>();
		custDetails = new ArrayList<Customer>();
//		serProDetails = new ArrayList<ServiceProduct>();
		payTerms = new ArrayList<ConfigCategory>();
		empDetails = new ArrayList<Employee>();
		errorList = new ArrayList<String>();
	}

	public HashSet<String> getEmpNameList() {
		return empNameList;
	}

	public void setEmpNameList(HashSet<String> empNameList) {
		this.empNameList = empNameList;
	}

	public HashSet<String> getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(HashSet<String> paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public HashMap<Integer, String> getCustomerDetail() {
		return customerDetail;
	}

	public void setCustomerDetail(HashMap<Integer, String> customerDetail) {
		this.customerDetail = customerDetail;
	}

	public HashMap<Integer, String> getProductDetail() {
		return productDetail;
	}

	public void setProductDetail(HashMap<Integer, String> productDetail) {
		this.productDetail = productDetail;
	}

	public List<CustomerContractDetails> getContractDetails() {
		return contractDetails;
	}

	public void setContractDetails(List<CustomerContractDetails> contractDetails) {
		this.contractDetails = contractDetails;
	}

	public List<Customer> getCustDetails() {
		return custDetails;
	}

	public void setCustDetails(List<Customer> custDetails) {
		this.custDetails = custDetails;
	}

//	public List<ServiceProduct> getSerProDetails() {
//		return serProDetails;
//	}
//
//	public void setSerProDetails(List<ServiceProduct> serProDetails) {
//		this.serProDetails = serProDetails;
//	}

	public List<ConfigCategory> getPayTerms() {
		return payTerms;
	}

	public void setPayTerms(List<ConfigCategory> payTerms) {
		this.payTerms = payTerms;
	}

	public List<Employee> getEmpDetails() {
		return empDetails;
	}

	public void setEmpDetails(List<Employee> empDetails) {
		this.empDetails = empDetails;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public ArrayList<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(ArrayList<String> errorList) {
		this.errorList = errorList;
	}
	
	public ArrayList<String> getExceldatalist() {
		return exceldatalist;
	}
	public void setExceldatalist(ArrayList<String> exceldatalist) {
		if(exceldatalist!=null){
			this.exceldatalist=new ArrayList<String>();
			this.exceldatalist.addAll(exceldatalist);
//			System.out.println("Size of Payment"+this.exceldatalist.size());
		}
	}
	
}
