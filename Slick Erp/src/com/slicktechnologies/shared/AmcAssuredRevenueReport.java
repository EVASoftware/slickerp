package com.slicktechnologies.shared;

import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class AmcAssuredRevenueReport extends SuperModel {

	private static final long serialVersionUID = -6239431315487671330L;
	
	public AmcAssuredRevenueReport() {
		 serviceValueMap = new HashMap<String,Double>();
	}

	@Index
	protected int contractCount;
	
	@Index
	protected String custName;

	@Index	
	protected String branch;
	
    @Index
    protected String contractType;
    
    @Index
    protected double contractValue;
    
    @Index
	protected Date contStartDate;
   
	@Index
	protected Date contEndDate;
	
	@EmbedMap
	HashMap<String , Double> serviceValueMap;
	
	

	/**
	 * Date 13-06-2019 by Vijay for NBHC CCPM 
	 */ 
	protected String month;
	
	public int getContractCount() {
		return contractCount;
	}


	public void setContractCount(int contractCount) {
		this.contractCount = contractCount;
	}


	public String getCustName() {
		return custName;
	}


	public void setCustName(String custName) {
		this.custName = custName;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public double getContractValue() {
		return contractValue;
	}


	public void setContractValue(double contractValue) {
		this.contractValue = contractValue;
	}


	public Date getContStartDate() {
		return contStartDate;
	}


	public void setContStartDate(Date contStartDate) {
		this.contStartDate = contStartDate;
	}


	public Date getContEndDate() {
		return contEndDate;
	}


	public void setContEndDate(Date contEndDate) {
		this.contEndDate = contEndDate;
	}


	public HashMap<String, Double> getServiceValueMap() {
		return serviceValueMap;
	}


	public void setServiceValueMap(HashMap<String, Double> serviceValueMap) {
		this.serviceValueMap = serviceValueMap;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public String getMonth() {
		return month;
	}


	public void setMonth(String month) {
		this.month = month;
	}
	
}
