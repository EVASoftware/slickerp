package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class TechnicianWarehouseDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5011560198701454006L;
	@Index
	private String parentWareHouse;
	@Index
	private String parentStorageLocation;
	@Index
	private String pearentStorageBin;
	@Index
	private String technicianName;
	@Index
	private String technicianWareHouse;
	@Index
	private String technicianStorageLocation;
	@Index
	private String technicianStorageBin;
	
	public TechnicianWarehouseDetails(){
		parentWareHouse = "";
		parentStorageLocation = "";
		pearentStorageBin = "";
		technicianName = "";
		technicianWareHouse = "";
		technicianStorageLocation = "";
		technicianStorageBin = "";
	}

	public String getParentWareHouse() {
		return parentWareHouse;
	}

	public void setParentWareHouse(String parentWareHouse) {
		this.parentWareHouse = parentWareHouse;
	}

	public String getParentStorageLocation() {
		return parentStorageLocation;
	}

	public void setParentStorageLocation(String parentStorageLocation) {
		this.parentStorageLocation = parentStorageLocation;
	}

	public String getPearentStorageBin() {
		return pearentStorageBin;
	}

	public void setPearentStorageBin(String pearentStorageBin) {
		this.pearentStorageBin = pearentStorageBin;
	}

	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}

	public String getTechnicianWareHouse() {
		return technicianWareHouse;
	}

	public void setTechnicianWareHouse(String technicianWareHouse) {
		this.technicianWareHouse = technicianWareHouse;
	}

	public String getTechnicianStorageLocation() {
		return technicianStorageLocation;
	}

	public void setTechnicianStorageLocation(String technicianStorageLocation) {
		this.technicianStorageLocation = technicianStorageLocation;
	}

	public String getTechnicianStorageBin() {
		return technicianStorageBin;
	}

	public void setTechnicianStorageBin(String technicianStorageBin) {
		this.technicianStorageBin = technicianStorageBin;
	}
	
}
