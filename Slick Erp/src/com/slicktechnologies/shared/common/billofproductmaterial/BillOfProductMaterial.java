package com.slicktechnologies.shared.common.billofproductmaterial;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;

@Entity
public class BillOfProductMaterial extends SuperModel{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7304051426040032152L;
	
	/*****************************************Applicability Attributes******************************/
	
	@Index
	private String bomTitle;
	@Index
	private int bomProductId;
	@Index
	private String bomProductCode;
	@Index
	private String bomProductName;
	@Index
	protected boolean bomStatus;
	@Index
	private ArrayList<BillOfProductMaterialDetails> bomProductList;
	
	/**
	 * Date : 19-05-2017 By ANIL
	 * This list stores the cost of the man power used to do service 
	 */
	private ArrayList<ManPowerCostStructure> manPowerCostStructureList;
	
	/********************************************Constructor*************************************/
	
	public BillOfProductMaterial() {
		super();
		bomTitle="";
		bomProductCode="";
		bomProductName="";
		bomProductList=new ArrayList<BillOfProductMaterialDetails>();
		manPowerCostStructureList=new ArrayList<ManPowerCostStructure>();
	}
	
	/****************************************Getters And Setters**************************************/
	public ArrayList<ManPowerCostStructure> getManPowerCostStructureList() {
		return manPowerCostStructureList;
	}

	public void setManPowerCostStructureList(List<ManPowerCostStructure> manPowerCostStructureList) {
		ArrayList<ManPowerCostStructure> arrBillItems=new ArrayList<ManPowerCostStructure>();
		arrBillItems.addAll(manPowerCostStructureList);
		this.manPowerCostStructureList = arrBillItems;
	}
	
	public String getBomTitle() {
		return bomTitle;
	}

	public void setBomTitle(String bomTitle) {
		if(bomTitle!=null){
			this.bomTitle = bomTitle.trim();
		}
	}

	public int getBomProductId() {
		return bomProductId;
	}

	public void setBomProductId(int bomProductId) {
		this.bomProductId = bomProductId;
	}

	public String getBomProductCode() {
		return bomProductCode;
	}

	public void setBomProductCode(String bomProductCode) {
		if(bomProductCode!=null){
			this.bomProductCode = bomProductCode.trim();
		}
	}

	public String getBomProductName() {
		return bomProductName;
	}

	public void setBomProductName(String bomProductName) {
		if(bomProductName!=null){
			this.bomProductName = bomProductName.trim();
		}
	}

	public boolean isBomStatus() {
		return bomStatus;
	}

	public void setBomStatus(boolean bomStatus) {
		this.bomStatus = bomStatus;
	}

	public List<BillOfProductMaterialDetails> getBomProductList() {
		return bomProductList;
	}

	public void setBomProductList(List<BillOfProductMaterialDetails> bomProductList) {
		ArrayList<BillOfProductMaterialDetails> arrBillItems=new ArrayList<BillOfProductMaterialDetails>();
		arrBillItems.addAll(bomProductList);
		this.bomProductList = arrBillItems;
	}

	
	/******************************************Overridden Method****************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
