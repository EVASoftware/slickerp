package com.slicktechnologies.shared.common.billofproductmaterial;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class BillOfProductMaterialDetails implements Serializable {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2380239011527968996L;

	/*****************************************Applicability Attributes******************************/
	private int compProdId;
	private int productId;
	private String productCode;
	private String productName;
	private String productCategory;
	private double productQty;
	private String productUOM;
	private double productPrice;
	
	
	public BillOfProductMaterialDetails() {
		
	}
	
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public double getProductQty() {
		return productQty;
	}

	public void setProductQty(double productQty) {
		this.productQty = productQty;
	}

	public String getProductUOM() {
		return productUOM;
	}

	public void setProductUOM(String productUOM) {
		this.productUOM = productUOM;
	}
	
	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public int getCompProdId() {
		return compProdId;
	}

	public void setCompProdId(int compProdId) {
		this.compProdId = compProdId;
	}
	
	

}
