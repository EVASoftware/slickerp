package com.slicktechnologies.shared.common;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class AssessmentReportUnit extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4371081515345077891L;

	@Index
	String Deficiency;
	@Index
	String Unit;
	@Index
	protected Boolean status;
	
	public AssessmentReportUnit(){
		Deficiency ="";
		Unit ="";
	}

	
	public String getDeficiency() {
		return Deficiency;
	}

	public void setDeficiency(String deficiency) {
		Deficiency = deficiency;
	}

	public String getUnit() {
		return Unit;
	}

	public void setUnit(String unit) {
		Unit = unit;
	}

	

	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Unit;
	}

}
