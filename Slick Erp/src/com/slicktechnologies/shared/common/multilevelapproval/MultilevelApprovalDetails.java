package com.slicktechnologies.shared.common.multilevelapproval;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class MultilevelApprovalDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3995855478839772608L;

	/*************************************Applicability Attributes***********************************/
	
	@Index
	protected String documentName;
	@Index
	protected String employeeRole;
	@Index
	protected String level;
	protected String remark;
	protected boolean status;

	@Index
	protected String employeeName;
	Double amountUpto;
	/************************************Getters And Setters*****************************************/
	
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	public String getEmployeeRole() {
		return employeeRole;
	}
	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public Double getAmountUpto() {
		return amountUpto;
	}
	public void setAmountUpto(Double amountUpto) {
		this.amountUpto = amountUpto;
	}
	
	
}
