package com.slicktechnologies.shared.common.multilevelapproval;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class MultilevelApproval extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2689202563517391946L;
	
	/************************************Applicability Attributes**************************************/
	
	@Index
	protected String documentType;
	protected String remark;
	@Index
	protected boolean status;
	protected ArrayList<MultilevelApprovalDetails> approvalLevelDetails;
	
	
	@Index
	boolean docStatus;

	/****************************************Constructor*******************************************/
	
	public MultilevelApproval() {
		super();
		documentType="";
		remark="";
		approvalLevelDetails=new ArrayList<MultilevelApprovalDetails>();
		
		docStatus=true;
	}
	
	/*****************************************Getters And Setters****************************************/
	
	
	public boolean isDocStatus() {
		return docStatus;
	}

	public void setDocStatus(boolean docStatus) {
		this.docStatus = docStatus;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		if(documentType!=null){
			this.documentType = documentType.trim();
		}
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		if(remark!=null){
			this.remark = remark.trim();
		}
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<MultilevelApprovalDetails> getApprovalLevelDetails() {
		return approvalLevelDetails;
	}

	public void setApprovalLevelDetails(List<MultilevelApprovalDetails> approvalLevelDetails) {
		ArrayList<MultilevelApprovalDetails> arrApproval=new ArrayList<MultilevelApprovalDetails>();
		arrApproval.addAll(approvalLevelDetails);
		this.approvalLevelDetails = arrApproval;
	}

	
	
	
	/*****************************************Overridden Method******************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return isDuplicateDocumentName(m);
	}
	
	public boolean isDuplicateDocumentName(SuperModel model){
		MultilevelApproval entity=(MultilevelApproval) model;
		String name = entity.getDocumentType().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=getDocumentType().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(entity.id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else
		{
			if(entity.id.equals(id))
				return false;	
			if(name.equals(curname)==true)
				return true;
		}
	
	return false;
	}

}
