package com.slicktechnologies.shared.common.supportlayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Entity
public class RaiseTicket extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4961573826701408844L;
	
	/**********************************Constants**************************************************/
	
	public final static String TICKETCREATED="Created";
	public final static String TICKETSUBMITTED="Submitted";
	public final static String TICKETREJECTED="Rejected";
	public final static String TICKETFIXED="Fixed";
	public final static String TICKETCLOSED="Closed";
	
	/*************************************Applicability Attributes********************************************/
@Index	
protected String raiseTicketTitle;

protected String raiseTicketCompanyName;

@Index
protected Date raiseTicketDate;

protected Date raiseTicketSubmittedDate;

protected Date raiseTicketFixedDate;

protected Date raiseTicketClosedDate;

protected String raiseTicketAssignTo;

protected String raiseTicketSubmittedBy;

protected String raiseTicketFixedBy;

protected String raiseTicketClosedBy;

protected String raiseTicketEmpName;

protected String raiseTicketEmailId;

protected Long raiseTicketCompanyId;

protected Long raiseTicketCell;

protected String raiseTicketBranch;
@Index
protected String raiseTicketReferneceNo;
@Index
protected String raiseTicketPriority;
@Index
protected String raiseTicketLevel;
@Index
protected String raiseTicketCategory;
@Index
protected String raiseTicketType;
@Index
protected String raiseTicketStatus;
@Index
protected String raiseTicketModule;
@Index
protected String raiseTicketDocumentName;
@Index
protected int raiseTicketDocumentId;
@Index
protected DocumentUpload uploadDocument;

protected String raiseTicketDescription;


/*******************************************Constructor***************************************************/


public RaiseTicket()
{
	super();
	raiseTicketCompanyName="";
	raiseTicketTitle="";
	raiseTicketEmpName="";
	raiseTicketBranch="";
	raiseTicketLevel="";
	raiseTicketPriority="";
	raiseTicketType="";
	raiseTicketStatus="";
	raiseTicketModule="";
	raiseTicketDocumentName="";
	raiseTicketCategory="";
	raiseTicketDescription="";
	raiseTicketReferneceNo="";
	raiseTicketAssignTo="";
	raiseTicketSubmittedBy="";
	raiseTicketFixedBy="";
	raiseTicketClosedBy="";
	uploadDocument=new DocumentUpload();
	raiseTicketDate=new Date();
//	raiseTicketSubmittedDate=new Date();
//	raiseTicketFixedDate=new Date();
	//raiseTicketClosedDate=new Date();
}
	
/*******************************************************Getters And Setters************************************/
	
	
	public String getRaiseTicketTitle() {
		return raiseTicketTitle;
	}




	public void setRaiseTicketTitle(String raiseTicketTitle) {
		if(raiseTicketTitle!=null)
		this.raiseTicketTitle = raiseTicketTitle.trim();
	}

	public String getRaiseTicketEmpName() {
		return raiseTicketEmpName;
	}




	public void setRaiseTicketEmpName(String raiseTicketEmpName) {
		if(raiseTicketEmpName!=null)
		this.raiseTicketEmpName = raiseTicketEmpName.trim();
	}

	
	

	public Long getRaiseTicketCell() {
		return raiseTicketCell;
	}

	public void setRaiseTicketCell(Long raiseTicketCell) {
		this.raiseTicketCell = raiseTicketCell;
	}

	public String getRaiseTicketBranch() {
		return raiseTicketBranch;
	}




	public void setRaiseTicketBranch(String raiseTicketBranch) {
		if(raiseTicketBranch!=null)
		this.raiseTicketBranch = raiseTicketBranch.trim();
	}




	public String getRaiseTicketPriority() {
		return raiseTicketPriority;
	}




	public void setRaiseTicketPriority(String raiseTicketPriority) {
		if(raiseTicketPriority!=null)
		this.raiseTicketPriority = raiseTicketPriority.trim();
	}




	public String getRaiseTicketLevel() {
		return raiseTicketLevel;
	}




	public void setRaiseTicketLevel(String raiseTicketLevel) {
		if(raiseTicketLevel!=null)
		this.raiseTicketLevel = raiseTicketLevel.trim();
	}




	public String getRaiseTicketCategory() {
		return raiseTicketCategory;
	}




	public void setRaiseTicketCategory(String raiseTicketCategory) {
		if(raiseTicketCategory!=null)
		this.raiseTicketCategory = raiseTicketCategory.trim();
	}




	public String getRaiseTicketType() {
		return raiseTicketType;
	}




	public void setRaiseTicketType(String raiseTicketType) {
		if(raiseTicketType!=null)
		this.raiseTicketType = raiseTicketType.trim();
	}




	public String getRaiseTicketStatus() {
		return raiseTicketStatus;
	}




	public void setRaiseTicketStatus(String raiseTicketStatus) {
		if(raiseTicketStatus!=null)
		this.raiseTicketStatus = raiseTicketStatus.trim();
	}




	public String getRaiseTicketModule() {
		return raiseTicketModule;
	}




	public void setRaiseTicketModule(String raiseTicketModule) {
		if(raiseTicketModule!=null)
		this.raiseTicketModule = raiseTicketModule.trim();
	}




	public String getRaiseTicketDocumentName() {
		return raiseTicketDocumentName;
	}




	public void setRaiseTicketDocumentName(String raiseTicketDocumentName) {
		if(raiseTicketDocumentName!=null)
		this.raiseTicketDocumentName = raiseTicketDocumentName.trim();
	}


	public int getRaiseTicketDocumentId() {
		return raiseTicketDocumentId;
	}

	public void setRaiseTicketDocumentId(int raiseTicketDocumentId) {
		this.raiseTicketDocumentId = raiseTicketDocumentId;
	}

	public Date getRaiseTicketDate() {
		return raiseTicketDate;
	}

	public void setRaiseTicketDate(Date raiseTicketDate) {
		if(raiseTicketDate!=null)
		this.raiseTicketDate = raiseTicketDate;
	}

	public DocumentUpload getUploadDocument() {
		return uploadDocument;
	}

	public void setUploadDocument(DocumentUpload uploadDocument) {
		this.uploadDocument = uploadDocument;
	}

	public String getRaiseTicketDescription() {
		return raiseTicketDescription;
	}

	public void setRaiseTicketDescription(String raiseTicketDescription) {
		if(raiseTicketDescription!=null)
		this.raiseTicketDescription = raiseTicketDescription.trim();
	}

	
	public String getRaiseTicketReferneceNo() {
		return raiseTicketReferneceNo;
	}

	public void setRaiseTicketReferneceNo(String raiseTicketReferneceNo) {
		if(raiseTicketReferneceNo!=null)
		this.raiseTicketReferneceNo = raiseTicketReferneceNo.trim();
	}
	
	public String getRaiseTicketEmailId() {
		return raiseTicketEmailId;
	}

	public void setRaiseTicketEmailId(String raiseTicketEmailId) {
		this.raiseTicketEmailId = raiseTicketEmailId;
	}

	public Long getRaiseTicketCompanyId() {
		return raiseTicketCompanyId;
	}

	public void setRaiseTicketCompanyId(Long raiseTicketCompanyId) {
		this.raiseTicketCompanyId = raiseTicketCompanyId;
	}

	
	public Date getRaiseTicketSubmittedDate() {
		return raiseTicketSubmittedDate;
	}

	public void setRaiseTicketSubmittedDate(Date raiseTicketSubmittedDate) {
		this.raiseTicketSubmittedDate = raiseTicketSubmittedDate;
	}

	public Date getRaiseTicketFixedDate() {
		return raiseTicketFixedDate;
	}

	public void setRaiseTicketFixedDate(Date raiseTicketFixedDate) {
		this.raiseTicketFixedDate = raiseTicketFixedDate;
	}

	public Date getRaiseTicketClosedDate() {
		return raiseTicketClosedDate;
	}

	public void setRaiseTicketClosedDate(Date raiseTicketClosedDate) {
		this.raiseTicketClosedDate = raiseTicketClosedDate;
	}

	public String getRaiseTicketAssignTo() {
		return raiseTicketAssignTo;
	}

	public void setRaiseTicketAssignTo(String raiseTicketAssignTo) {
		this.raiseTicketAssignTo = raiseTicketAssignTo;
	}

	public String getRaiseTicketSubmittedBy() {
		return raiseTicketSubmittedBy;
	}

	public void setRaiseTicketSubmittedBy(String raiseTicketSubmittedBy) {
		this.raiseTicketSubmittedBy = raiseTicketSubmittedBy;
	}

	public String getRaiseTicketFixedBy() {
		return raiseTicketFixedBy;
	}

	public void setRaiseTicketFixedBy(String raiseTicketFixedBy) {
		this.raiseTicketFixedBy = raiseTicketFixedBy;
	}

	public String getRaiseTicketClosedBy() {
		return raiseTicketClosedBy;
	}

	public void setRaiseTicketClosedBy(String raiseTicketClosedBy) {
		this.raiseTicketClosedBy = raiseTicketClosedBy;
	}
	
	

	public String getRaiseTicketCompanyName() {
		return raiseTicketCompanyName;
	}

	public void setRaiseTicketCompanyName(String raiseTicketCompanyName) {
		this.raiseTicketCompanyName = raiseTicketCompanyName;
	}

	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(TICKETCREATED);
		statuslist.add(TICKETSUBMITTED);
		statuslist.add(TICKETREJECTED);
		statuslist.add(TICKETFIXED);
		statuslist.add(TICKETCLOSED);
		
		return statuslist;
	}


	
	@Override
	public boolean isDuplicate(SuperModel m) {
	
		return false;
	}

}
