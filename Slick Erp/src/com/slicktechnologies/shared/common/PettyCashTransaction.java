package com.slicktechnologies.shared.common;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import java.util.Date;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
@Entity
public class PettyCashTransaction extends ConcreteBusinessProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = 295210661109161493L;


		protected int documentId;
		protected String documentName;
		@Index
		protected Date transactionDate;
		@Index
		protected String personResponsible;
		@Index
		protected String appoverName;
		
		/**Added by sheetal:24-12-2021
		 * storing expense category**/
		protected String expenseCategory;
		protected double openingStock;
		protected double closingStock;
		protected String action;
		protected double transactionAmount;
		@Index
		protected String documentTitle;
		protected String transactionTime;
		@Index
		protected String pettyCashName;
		
		
		/**
		 * @author Anil , Date : 24-08-2019
		 * Storing expense date
		 */
		@Index
		protected Date documentDate;
		
		/** By Sheetal : 07-04-2022
		 *  Storing desription of expenses**/
		protected String description;
		
		//    getters and setters 
		
		public int getDocumentId() {
			return documentId;
		}
		public void setDocumentId(int documentId) {
			this.documentId = documentId;
		}
		public String getDocumentName() {
			return documentName;
		}
		public void setDocumentName(String documentName) {
			this.documentName = documentName;
		}
		public Date getTransactionDate() {
			return transactionDate;
		}
		public void setTransactionDate(Date transactionDate) {
			this.transactionDate = transactionDate;
		}
		public String getPersonResponsible() {
			return personResponsible;
		}
		public void setPersonResponsible(String personResponsible) {
			this.personResponsible = personResponsible;
		}
		public String getAppoverName() {
			return appoverName;
		}
		public void setAppoverName(String appoverName) {
			this.appoverName = appoverName;
		}
		public double getOpeningStock() {
			return openingStock;
		}
		public void setOpeningStock(double openingStock) {
			this.openingStock = openingStock;
		}
		public double getClosingStock() {
			return closingStock;
		}
		public void setClosingStock(double closingStock) {
			this.closingStock = closingStock;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public double getTransactionAmount() {
			return transactionAmount;
		}
		public void setTransactionAmount(double transactionAmount) {
			this.transactionAmount = transactionAmount;
		}
		public String getDocumentTitle() {
			return documentTitle;
		}
		public void setDocumentTitle(String documentTitle) {
			this.documentTitle = documentTitle;
		}
		public String getTransactionTime() {
			return transactionTime;
		}
		public void setTransactionTime(String transactionTime) {
			this.transactionTime = transactionTime;
		}
		public String getPettyCashName() {
			return pettyCashName;
		}
		public void setPettyCashName(String pettyCashName) {
			this.pettyCashName = pettyCashName;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		public Date getDocumentDate() {
			return documentDate;
		}
		public void setDocumentDate(Date documentDate) {
			this.documentDate = documentDate;
		}
		public String getexpenseCategory() {
			return expenseCategory;
		}
	    public void setexpenseCategory(String expenseCategory){
	    	this.expenseCategory = expenseCategory;
	    }
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
	    
		
}
