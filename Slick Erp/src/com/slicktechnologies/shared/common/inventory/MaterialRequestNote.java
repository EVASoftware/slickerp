package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

@Entity
public class MaterialRequestNote extends ConcreteBusinessProcess {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1117432386125038079L;
	
	/*****************************************Constants********************************************/
	
	public static final String CREATED = "Created";
	public static final String REQUESTED = "Requested";
	public static final String APPROVED = "Approved";
	public static final String REJECTED = "Rejected";
	public static final String CANCELLED = "Cancelled";
	
	/**********************************Applicability Attributes******************************************/

	@Index
	protected String mrnTitle;
	@Index
	protected Date mrnDate;
	@Index
	protected Date mrnRequiredDate;
	@Index
	protected int mrnSoId;
	protected Date mrnSoDate;
	@Index
	protected int mrnWoId;
	protected Date mrnWoDate;
	@Index
	protected Date mrnDeliveryDate;
	@Index
	protected String mrnCategory;
	@Index
	protected String mrnType;
	@Index
	protected String mrnProject;
	@Index
	protected String mrnSalesPerson;
	protected String mrnDescription;
	protected DocumentUpload upload;
	protected Date mrnRefDate;
	protected String mrnRefId;
	
	ArrayList<SalesLineItem> productTableMrn;
	/**** Date 06-02-2019 by Vijay commneted Index MRN not saving due to Index inside product tables used Map for companyAsset ***/
//	@Index
	ArrayList<MaterialProduct> subProductTableMrn;
	/**
	 * Date 02-09-2019 by vijay
	 * Des :- NBHC Inventory Management Auto MRN flag
	 */
	@Index
	protected boolean autoMRN;
	
	/*****************************************Constructor*******************************************/
	
	public MaterialRequestNote() {
		productTableMrn = new ArrayList<SalesLineItem>();
		subProductTableMrn = new ArrayList<MaterialProduct>();
		mrnRefId="";
		autoMRN=false;
	}

	
	/*******************************************Getters And Setters**************************************/
	

	public Date getMrnDate() {
		return mrnDate;
	}

	public void setMrnDate(Date mrnDate) {
		this.mrnDate = mrnDate;
	}

	public int getMrnSoId() {
		return mrnSoId;
	}

	public void setMrnSoId(int mrnSoId) {
		this.mrnSoId = mrnSoId;
	}

	public Date getMrnSoDate() {
		return mrnSoDate;
	}

	public void setMrnSoDate(Date mrnSoDate) {
		this.mrnSoDate = mrnSoDate;
	}

	public int getMrnWoId() {
		return mrnWoId;
	}

	public void setMrnWoId(int mrnWoId) {
		this.mrnWoId = mrnWoId;
	}

	public Date getMrnWoDate() {
		return mrnWoDate;
	}

	public void setMrnWoDate(Date mrnWoDate) {
		this.mrnWoDate = mrnWoDate;
	}

	public Date getMrnDeliveryDate() {
		return mrnDeliveryDate;
	}

	public void setMrnDeliveryDate(Date mrnDeliveryDate) {
		this.mrnDeliveryDate = mrnDeliveryDate;
	}

	public String getMrnProject() {
		return mrnProject;
	}

	public void setMrnProject(String mrnProject) {
		this.mrnProject = mrnProject;
	}

	public String getMrnSalesPerson() {
		return mrnSalesPerson;
	}

	public void setMrnSalesPerson(String mrnSalesPerson) {
		this.mrnSalesPerson = mrnSalesPerson;
	}

	public ArrayList<SalesLineItem> getProductTableMrn() {
		return productTableMrn;
	}

	public void setProductTableMrn(List<SalesLineItem> productTableMrn) {
		if (productTableMrn != null) {
			this.productTableMrn = new ArrayList<SalesLineItem>();
			this.productTableMrn.addAll(productTableMrn);
		}
	}

	public ArrayList<MaterialProduct> getSubProductTableMrn() {
		return subProductTableMrn;
	}

	public void setSubProductTableMrn(List<MaterialProduct> subProductTableMrn) {
		if (subProductTableMrn != null) {
			this.subProductTableMrn = new ArrayList<MaterialProduct>();
			this.subProductTableMrn.addAll(subProductTableMrn);
		}
	}
	
	public String getMrnTitle() {
		return mrnTitle;
	}

	public void setMrnTitle(String mrnTitle) {
		this.mrnTitle = mrnTitle;
	}

	public Date getMrnRequiredDate() {
		return mrnRequiredDate;
	}

	public void setMrnRequiredDate(Date mrnRequiredDate) {
		this.mrnRequiredDate = mrnRequiredDate;
	}

	public String getMrnCategory() {
		return mrnCategory;
	}

	public void setMrnCategory(String mrnCategory) {
		this.mrnCategory = mrnCategory;
	}

	public String getMrnType() {
		return mrnType;
	}

	public void setMrnType(String mrnType) {
		this.mrnType = mrnType;
	}

	public String getMrnDescription() {
		return mrnDescription;
	}

	public void setMrnDescription(String mrnDescription) {
		this.mrnDescription = mrnDescription;
	}

	public DocumentUpload getUpload() {
		return upload;
	}

	public void setUpload(DocumentUpload upload) {
		this.upload = upload;
	}
	
	public Date getMrnRefDate() {
		return mrnRefDate;
	}

	public void setMrnRefDate(Date mrnRefDate) {
		if(mrnRefDate!=null){
			this.mrnRefDate = mrnRefDate;
		}
	}


	public String getMrnRefId() {
		return mrnRefId;
	}

	public void setMrnRefId(String mrnRefId) {
		if(mrnRefId!=null){
			this.mrnRefId = mrnRefId.trim();
		}
	}
	
	

	/***************************************************************************************************/
	
	/***************************************On Save Logic****************************************/
	
	@OnSave
	@GwtIncompatible
	public void reactOnDataSave(){
		if(this.getMrnDate()!=null)
		{
			setMrnDate(DateUtility.getDateWithTimeZone("IST", this.getMrnDate()));
		}
		/**
		 * @author Vijay Chougule Date 14-09-2019 
		 * Des :- NBHC INventory Management LOCK SEAL MRN must be close
		 * when  MRN balanced  Qty <=0
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote", "EnableCloseMRNBasedOnMMN", this.getCompanyId())){
			for(MaterialProduct materialprod : this.getSubProductTableMrn()){
				if(materialprod.getMaterialProductBalanceQty()<=0){
					this.setStatus(MaterialRequestNote.CLOSED);
				}
			}	
		}
		/**
		 * ends here
		 */
		
		/**
		 * @author Anil , Date : 12-05-2021
		 * While doing simplification of MRN screen, MRN date field is removed from Screen 
		 * So here we are storing MRN Date as current date
		 */
		if(id==null){
			if(getMrnDate()==null){
				setMrnDate(DateUtility.getDateWithTimeZone("IST",new Date()));
			}
		}else{
			if(getMrnDate()==null){
				setMrnDate(DateUtility.getDateWithTimeZone("IST",creationDate));
			}
		}
		
	}
	
	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		if(this.getCount()!=0){
//			createMIN();
			
			/***
			 * Date 27-12-2018 By Vijay
			 * Des :- Inventory Management NBHC
			 * create Material Movement note
			 * and if this process config is active then MIN will not create
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote",
					"EnableCreateMMN", this.getCompanyId())){
				createMMN();
			}
			else{
				createMIN();
			}
			/**
			 * ends here
			 */
		}
		/**
		 * Added by Rahul Verma 19 July 2017
		 */
		accountingInterface();
		/**
		 * Ends
		 */
	}
	
	
	@GwtIncompatible
	private void createMIN() {
		MaterialIssueNote min = new MaterialIssueNote();
		
		min.setMinMrnId(this.getCount());
		min.setMinMrnDate(this.getMrnDate());
		min.setMinWoId(this.getMrnWoId());
		min.setMinWoDate(this.getMrnWoDate());
		min.setMinSoId(this.getMrnSoId());
		min.setMinSoDate(this.getMrnSoDate());
		min.setMinSoDeliveryDate(this.getMrnDeliveryDate());
		min.setMinSalesPerson(this.getMrnSalesPerson());
		min.setMinProject(this.getMrnProject());
		
		min.setBranch(this.getBranch());
		min.setEmployee(this.getEmployee());
		min.setApproverName(this.getApproverName());
		
		ArrayList<SalesLineItem> productList = this.getProductTableMrn();
		min.setProductTablemin(productList);
		
		ArrayList<MaterialProduct> materialList = this.getSubProductTableMrn();
		min.setSubProductTablemin(materialList);
		min.setCompanyId(this.getCompanyId());
		
		min.setMinTitle(this.getMrnTitle());
		min.setMinDate(new Date());
		
		
		GenricServiceImpl genimpl=new GenricServiceImpl();
		genimpl.save(min);
	}
	
	/*************************************Status List for Search***************************************/
	
	public static List<String> getStatusList() {
		ArrayList<String> statusListBox = new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(REQUESTED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		statusListBox.add(CANCELLED);
		return statusListBox;
	}

	
	/******************************************Overridden Method***************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	
	/**************Accounting Interface*********************
	 * Created by Rahul Verma on 24 April 2017
	 * Created for Interface with other system
	 */
	@GwtIncompatible
	protected void accountingInterface(){

		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", this.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGMRN)
					.filter("companyId", getCompanyId())
					.filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag = 0;

		if (processConfig != null) {
			System.out.println("Process Config Not Null");
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				System.out
						.println("----------------------------------------------------------");
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i)
								.getProcessType());
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i).isStatus());
				System.out
						.println("----------------------------------------------------------");
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {
			System.out.println("ExecutingAccInterface");
		if(this.getCount()!=0){
			
			System.out.println("Product List Size :: "+this.getProductTableMrn().size());
			
			/*************************************************************************************************************/
			for(int  i = 0;i<this.getSubProductTableMrn().size();i++){
				
				
				
		/*************************************************Start************************************************/
				
				String unitofmeasurement = this.getSubProductTableMrn().get(i).getMaterialProductUOM();
				int prodId=this.getSubProductTableMrn().get(i).getMaterialProductId();
				 String productCode = this.getSubProductTableMrn().get(i).getMaterialProductCode();
				 String productName = this.getSubProductTableMrn().get(i).getMaterialProductName();
				 double productQuantity = this.getSubProductTableMrn().get(i).getMaterialProductRequiredQuantity();
				 double availableQuantity=this.getSubProductTableMrn().get(i).getMaterialProductAvailableQuantity();
				 String remarks=this.getSubProductTableMrn().get(i).getMaterialProductRemarks().trim();
				 String warehouseName=this.getSubProductTableMrn().get(i).getMaterialProductWarehouse().trim();
				 System.out.println("warehouseName"+warehouseName);
				 WareHouse warehouse=ofy().load().type(WareHouse.class).filter("companyId", this.companyId).filter("buisnessUnitName",warehouseName).first().now();
				 System.out.println("warehouse"+warehouse);
				 String warehouseCode="";
				 if(warehouse.getWarehouseCode().trim()!=null){
					 warehouseCode=warehouse.getWarehouseCode().trim();
				 }
				 String storageLocation=this.getSubProductTableMrn().get(i).getMaterialProductStorageLocation().trim();
				 String storageBin=this.getSubProductTableMrn().get(i).getMaterialProductStorageBin().trim();
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
													 this.getEmployee(),//accountingInterfaceCreatedBy
													 this.getStatus(),
													 AppConstants.STATUS ,//Status
													 "",//remark
													 "Inventory",//module
													 "MRN",//documentType
				   									 this.getCount(),//docID
													 this.getMrnTitle(),//DOCtile
													 this.getMrnDate(), //docDate
													 AppConstants.DOCUMENTGL,//docGL
														this.getMrnWoId()+"",//ref doc no.1  (Work Order Id)
														this.getMrnWoDate(),//ref doc date.1 (Work Order Date)
														AppConstants.REFDOCWO,//ref doc type 1
														this.getMrnSoId()+"",//ref doc no 2 (Order ID)
														this.getMrnSoDate(),//ref doc date 2 (Order Date)
														AppConstants.REFDOCSO,//ref doc type 2
														"",//accountType
														0,//custID
														"",//custName
														0l,//custCell
														0,//vendor ID
														"",//vendor NAme
														0l,//vendorCell
														0,//empId
														" ",//empNAme
														this.getBranch(),//branch
														this.getEmployee(),//personResponsible
														this.getMrnSalesPerson(),//requestedBY
														this.getApproverName(),//approvedBY
														"",//paymentmethod
														null,//paymentdate
														"",//cheqno.
														null,//cheqDate
														null,//bankName
														null,//bankAccount
														0,//transferReferenceNumber
														null,//transactionDate
														null,//contract start date
														null, // contract end date
														prodId,//prod ID
														productCode,//prod Code
														productName,//productNAme
														productQuantity,//prodQuantity
														null,//productDate
														0,//duration
														0,//services
														unitofmeasurement,//unit of measurement
														0.0,//productPrice
														0.0,//VATpercent
														0.0,//VATamt
														0,//VATglAccount
														0.0,//serviceTaxPercent
														0.0,//serviceTaxAmount
														0,//serviceTaxGLaccount
														"",//cform
														0,//cformpercent
														0,//cformamount
														0,//cformGlaccount	
														0.0,//totalamouNT
														0.0,//NET PAPAYABLE
														"",//Contract Category
														0,//amount Recieved
														0.0,// base Amt for tdscal in pay  by rohan  
														0.0, //  tds percentage by rohan 
														0.0,//  tds amount  by rohan 
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														0.0,
														"",
														"",
														"",
														"",
														"",
														"",
														0l,
														this.getCompanyId(),
														null,				//  billing from date (rohan)
														null,			//  billing to date (rohan)
														warehouseName, //Warehouse
														warehouseCode,				//warehouseCode
														"",				//ProductRefId
														AccountingInterface.OUTBOUNDDIRECTION,				//Direction
														AppConstants.CCPM,				//sourceSystem
														AppConstants.NBHC,				//Destination System
														null,null,null
						 );

		}
		
	}

	
		
	}
	}
	
	
	/***
	 * Date 27-12-2018 By Vijay
	 * Des :- Inventory Management NBHC
	 * create Material Movement note
	 */
	@GwtIncompatible	
	private void createMMN() {
		MaterialMovementNote mmn = new MaterialMovementNote();
		mmn.setCompanyId(this.getCompanyId());
		mmn.setStatus(MaterialMovementNote.CREATED);
		mmn.setCreationDate(new Date());
		mmn.setMmnDate(new Date());
		
		mmn.setMmnTransactionType("TransferOUT");
		
		mmn.setTransDirection("TRANSFEROUT");
		
		WareHouse warehouse = ofy().load().type(WareHouse.class).filter("companyId", this.getCompanyId())
				.filter("buisnessUnitName", this.getSubProductTableMrn().get(0).getMaterialProductWarehouse()).first()
				.now();
		ProductInventoryViewDetails productinventoryView = null;
		if (warehouse != null) {
			WareHouse parentWarehouse = ofy().load().type(WareHouse.class).filter("companyId", this.getCompanyId())
					.filter("buisnessUnitName", warehouse.getParentWarehouse()).first().now();
			if (parentWarehouse != null) {
				mmn.setBranch(parentWarehouse.getBranchdetails().get(0).getBranchName());
				productinventoryView = ofy().load().type(ProductInventoryViewDetails.class).filter("warehousename", parentWarehouse.getBusinessUnitName())
						.filter("prodid", this.getSubProductTableMrn().get(0).getMaterialProductId())
						.filter("companyId", this.getCompanyId()).first().now();
			}
		}
		
		ArrayList<MaterialProduct> materiallist = new ArrayList<MaterialProduct>();

		if(this.getSubProductTableMrn().size()!=0){
		mmn.setTransToWareHouse(this.getSubProductTableMrn().get(0).getMaterialProductWarehouse());
		mmn.setTransToStorLoc(this.getSubProductTableMrn().get(0).getMaterialProductStorageLocation());
		mmn.setTransToStorBin(this.getSubProductTableMrn().get(0).getMaterialProductStorageBin());
		mmn.setMmnMrnId(this.getCount());
		for(MaterialProduct materialProdMRN : this.getSubProductTableMrn()){
			MaterialProduct materialProd = new MaterialProduct();
			materialProd.setMaterialProductId(materialProdMRN.getMaterialProductId());
			materialProd.setMaterialProductCode(materialProdMRN.getMaterialProductCode());
			materialProd.setMaterialProductName(materialProdMRN.getMaterialProductName());
			materialProd.setMaterialProductRequiredQuantity(materialProdMRN.getMaterialProductRequiredQuantity());
			materialProd.setMaterialProductRemarks(materialProdMRN.getMaterialProductRemarks());
			materialProd.setMaterialProductUOM(materialProdMRN.getMaterialProductUOM());
			materialProd.setMaterialProductBalanceQty(materialProdMRN.getMaterialProductRequiredQuantity());
			if(productinventoryView!=null){
				materialProd.setMaterialProductWarehouse(productinventoryView.getWarehousename());
				materialProd.setMaterialProductStorageLocation(productinventoryView.getStoragelocation());
				materialProd.setMaterialProductStorageBin(productinventoryView.getStoragebin());
				materialProd.setMaterialProductAvailableQuantity(productinventoryView.getAvailableqty());
			}
			materiallist.add(materialProd);
		}
		}
		mmn.setSubProductTableMmn(materiallist);
		mmn.setEmployee(this.getEmployee());
		
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(mmn);
	}
	/**
	 * ends here
	 */


	public boolean isAutoMRN() {
		return autoMRN;
	}


	public void setAutoMRN(boolean autoMRN) {
		this.autoMRN = autoMRN;
	}

	
}
