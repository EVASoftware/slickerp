package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;
@Entity
public class MaterialIssueNote extends ConcreteBusinessProcess {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8756781478585720541L;
	
	@Index
	protected String minTitle;
	@Index
	protected Date minDate;
	@Index
	protected int minSoId;
	
	protected Date minSoDate;
	@Index
	protected int minWoId;
	
	protected Date minWoDate;
	@Index
	protected int minMrnId;
	
	protected Date minMrnDate;
	@Index
	protected Date minSoDeliveryDate;
	@Index
	protected String minCategory;
	@Index
	protected String minType;
	@Index
	protected String minProject;
	@Index
	protected String minSalesPerson;
	@Index
	protected String minIssuedBy;
	@Index
	protected String minWarehouse;

	protected String minCommentOnStatusChange;
	@Index
	protected int serviceId;
	protected String minDescription;
	
	protected DocumentUpload upload;

	ArrayList<SalesLineItem> productTablemin;
	
	ArrayList<MaterialProduct> subProductTablemin;
	
	
	// rohan added this code for adding customer in MIN, MMN ,MRN ,GRN
	@Index 
	protected PersonInfo cinfo;
	/*** Date 17-06-2019 by Vijay for NBHC Invnetory Management lock seal number ****/
	@Index 
	String referenceNo;
	

	public static final String CREATED = "Created";
	public static final String REQUESTED = "Requested";
	public static final String APPROVED = "Approved";
	public static final String REJECTED = "Rejected";
	public static final String CANCELLED = "Cancelled";

	@Index
	boolean createdFromApp;
	
	public MaterialIssueNote() {
		productTablemin = new ArrayList<SalesLineItem>();
		subProductTablemin = new ArrayList<MaterialProduct>();
		
		cinfo = new PersonInfo();
		referenceNo ="";
		createdFromApp = false;
		
	}

	public String getMinTitle() {
		return minTitle;
	}

	public void setMinTitle(String minTitle) {
		this.minTitle = minTitle;
	}

	public Date getMinDate() {
		return minDate;
	}

	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}

	public int getMinSoId() {
		return minSoId;
	}

	public void setMinSoId(int minSoId) {
		this.minSoId = minSoId;
	}

	public Date getMinSoDate() {
		return minSoDate;
	}

	public void setMinSoDate(Date minSoDate) {
		this.minSoDate = minSoDate;
	}

	public int getMinWoId() {
		return minWoId;
	}

	public void setMinWoId(int minWoId) {
		this.minWoId = minWoId;
	}

	public Date getMinWoDate() {
		return minWoDate;
	}

	public void setMinWoDate(Date minWoDate) {
		this.minWoDate = minWoDate;
	}

	public int getMinMrnId() {
		return minMrnId;
	}

	public void setMinMrnId(int minMrnId) {
		this.minMrnId = minMrnId;
	}

	public Date getMinMrnDate() {
		return minMrnDate;
	}

	public void setMinMrnDate(Date minMrnDate) {
		this.minMrnDate = minMrnDate;
	}

	public Date getMinSoDeliveryDate() {
		return minSoDeliveryDate;
	}

	public void setMinSoDeliveryDate(Date minSoDeliveryDate) {
		this.minSoDeliveryDate = minSoDeliveryDate;
	}

	public String getMinCategory() {
		return minCategory;
	}

	public void setMinCategory(String minCategory) {
		this.minCategory = minCategory;
	}

	public String getMinType() {
		return minType;
	}

	public void setMinType(String minType) {
		this.minType = minType;
	}

	public String getMinProject() {
		return minProject;
	}

	public void setMinProject(String minProject) {
		this.minProject = minProject;
	}

	public String getMinSalesPerson() {
		return minSalesPerson;
	}

	public void setMinSalesPerson(String minSalesPerson) {
		this.minSalesPerson = minSalesPerson;
	}
	
	public String getMinIssuedBy() {
		return minIssuedBy;
	}

	public void setMinIssuedBy(String minIssuedBy) {
		this.minIssuedBy = minIssuedBy;
	}


	public String getMinWarehouse() {
		return minWarehouse;
	}

	public void setMinWarehouse(String minWarehouse) {
		this.minWarehouse = minWarehouse;
	}

	public String getMinCommentOnStatusChange() {
		return minCommentOnStatusChange;
	}

	public void setMinCommentOnStatusChange(String minCommentOnStatusChange) {
		this.minCommentOnStatusChange = minCommentOnStatusChange;
	}

	public String getMinDescription() {
		return minDescription;
	}

	public void setMinDescription(String minDescription) {
		this.minDescription = minDescription;
	}

	public DocumentUpload getUpload() {
		return upload;
	}

	public void setUpload(DocumentUpload upload) {
		this.upload = upload;
	}

	public ArrayList<SalesLineItem> getProductTablemin() {
		return productTablemin;
	}

	public void setProductTablemin(List<SalesLineItem> productTablemin) {
		if (productTablemin != null) {
			this.productTablemin = new ArrayList<SalesLineItem>();
			this.productTablemin.addAll(productTablemin);
		}
	}

	public ArrayList<MaterialProduct> getSubProductTablemin() {
		return subProductTablemin;
	}

	public void setSubProductTablemin(List<MaterialProduct> subProductTablemin) {
		if (subProductTablemin != null) {
			this.subProductTablemin = new ArrayList<MaterialProduct>();
			this.subProductTablemin.addAll(subProductTablemin);
		}
	}
	
	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}


	public PersonInfo getCinfo() {
		return cinfo;
	}

	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}
	
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public boolean isCreatedFromApp() {
		return createdFromApp;
	}

	public void setCreatedFromApp(boolean createdFromApp) {
		this.createdFromApp = createdFromApp;
	}

	public static List<String> getStatusList() {
		ArrayList<String> statusListBox = new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(REQUESTED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		statusListBox.add(CANCELLED);
		return statusListBox;
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		System.out.println("Inside Approval..................");
		
		if(this.getStatus().equals(MaterialIssueNote.APPROVED))
		{
			System.out.println("Inside Condition ..............");
			
//			for(int i=0;i<getSubProductTablemin().size();i++){
//				
//				int productId=getSubProductTablemin().get(i).getMaterialProductId();
//				String storageLocation=getSubProductTablemin().get(i).getMaterialProductStorageLocation();
//				String storageBin=getSubProductTablemin().get(i).getMaterialProductStorageBin();
//				String warehouse=getSubProductTablemin().get(i).getMaterialProductWarehouse();
//				double productQty=getSubProductTablemin().get(i).getMaterialProductRequiredQuantity();
//				Date date=getCreationDate();
//				long compId=getCompanyId();
//				int idCount=this.getCount();
//				String minTitle1=this.getMinTitle();
//				System.out.println("Id :: "+productId);
//				
//				
//				UpdateStock.setProductInventory(productId,productQty, idCount,date,AppConstants.MIN,minTitle1,
//					warehouse, storageLocation, storageBin,AppConstants.SUBTRACT,compId);
//			}
			
//			updateInventoryViewList();
			
			
			/**
			 * Date : 18-01-2017 By Anil
			 * New Stock Updation and Transaction creation Code
			 */
			
			ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
			for(MaterialProduct product:getSubProductTablemin()){
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(this.getCompanyId());
				item.setBranch(this.getBranch());
				item.setDocumentType(AppConstants.MIN);
				item.setDocumentId(this.getCount());
				item.setDocumentDate(this.getCreationDate());
				item.setDocumnetTitle(this.getMinTitle());
				if(this.getServiceId()>0)
					item.setServiceId(this.getServiceId()+"");
				item.setProdId(product.getMaterialProductId());
				item.setUpdateQty(product.getMaterialProductRequiredQuantity());
				item.setOperation(AppConstants.SUBTRACT);
				item.setWarehouseName(product.getMaterialProductWarehouse());
				item.setStorageLocation(product.getMaterialProductStorageLocation());
				item.setStorageBin(product.getMaterialProductStorageBin());
				/*** Date 30-08-2019 by Vijay for Inventory Management Lock seal ***/
				item.setProductCode(product.getMaterialProductCode());
				/**
				 * nidhi ||*||
				 */
				item.setPlannedQty(product.getMaterialProductPlannedQuantity());
				/**
				 * end
				 */
				
				/**
				 * Date 02-05-2019 by Vijay for NBHC IM Serial No mapping
				 */
				HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					
				if(product.getProSerialNoDetails() !=null &&
						product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
					prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
					for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
						 ProductSerialNoMapping element = new ProductSerialNoMapping();
						 element.setAvailableStatus(pr.isAvailableStatus());
						 element.setNewAddNo(pr.isNewAddNo());
						 element.setStatus(pr.isStatus());
						 element.setProSerialNo(pr.getProSerialNo());
						 prserdt.get(0).add(element);
					}
					for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
				      {
						ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
						if(!pr.isStatus()){
								itr.remove();
						}
				      }
				}
				item.setProSerialNoDetails(prserdt);
				/**
				 * ends here
				 */
				item.setProSerialNoDetails(prserdt);
				/** Date 29-11-2019 by vijay for Inventory management lock seal Number ***/
				if(product.getLockSealSerialNo()!=null){
				item.setSerialNumber(product.getLockSealSerialNo());
				}
				
				itemList.add(item);
			}
			
			/**
			 * Date 29-11-2019 By Vijay
			 * Des :- NBHC Inventory Management Lock Seal Stock Updation from ProductInventoryDetails master
			 */
			 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryViewDetails", "EnableStockUpdation", this.getCompanyId())){	
				 UpdateStock.setProductInventoryViewDetails(itemList);
			 }
			 else{
				 UpdateStock.setProductInventory(itemList);
			 }
				
			
			/**
			 * End
			 */
			
			/**
			 * Rahul Added this
			 */
			accountingInterface();
			/**
			 * Ends
			 */
			boolean bomProcessActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", getCompanyId());
			if(bomProcessActive && getServiceId()!=0){
				String jsonProDetails="";
				
					Gson gson = new Gson();
					if(getSubProductTablemin()!=null && getSubProductTablemin().size()>0){
						jsonProDetails = gson.toJson(this);
					
					
						Queue queue = QueueFactory.getQueue("BOMProductListMappingTaskQueue-Queue");
						queue.add(TaskOptions.Builder.withUrl("/slick_erp/bomproductlisttaskqueue")
								.param("minObject", jsonProDetails).param("processName","minMapping"));
					
					}
					
	
			}
		}
	}
	
	@GwtIncompatible
	protected void updateInventoryViewList()
	{
		List<MaterialProduct> materialLis=this.getSubProductTablemin();
		for(int i=0;i<materialLis.size();i++)
		{
			int prodId=materialLis.get(i).getMaterialProductId();
			String wareHouse=materialLis.get(i).getMaterialProductWarehouse();
			String storageLoc=materialLis.get(i).getMaterialProductStorageLocation();
			String storageBin=materialLis.get(i).getMaterialProductStorageBin();
			
			ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class).filter("count",prodId).filter("warehousename", wareHouse.trim()).filter("storagelocation", storageLoc.trim()).filter("storagebin", storageBin.trim()).filter("companyId", getCompanyId()).first().now();
			
			if(prodInvDtls!=null){
				prodInvDtls.setAvailableqty(prodInvDtls.getAvailableqty()+materialLis.get(i).getMaterialProductRequiredQuantity());
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(prodInvDtls);
			}
		}
	}
	
	

	/**************Accounting Interface*********************
	 * Created by Rahul Verma on 24 April 2017
	 * Created for Interface with other system
	 */
	@GwtIncompatible
	private void accountingInterface() {

		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", this.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGMIN)
					.filter("companyId", getCompanyId())
					.filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag = 0;

		if (processConfig != null) {
			System.out.println("Process Config Not Null");
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				System.out
						.println("----------------------------------------------------------");
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i)
								.getProcessType());
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i).isStatus());
				System.out
						.println("----------------------------------------------------------");
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {
			System.out.println("ExecutingAccInterface");

			if (this.getCount() != 0) {

				System.out.println("Product List Size :: "
						+ this.getProductTablemin().size());

				/*************************************************************************************************************/
				for (int i = 0; i < this.getSubProductTablemin().size(); i++) {

					/************************************************* Start ************************************************/

					String unitofmeasurement = this.getSubProductTablemin()
							.get(i).getMaterialProductUOM();
					int prodId = this.getSubProductTablemin().get(i)
							.getMaterialProductId();
					String productCode = this.getSubProductTablemin().get(i)
							.getMaterialProductCode();
					String productName = this.getSubProductTablemin().get(i)
							.getMaterialProductName();
					double productQuantity = this.getSubProductTablemin()
							.get(i).getMaterialProductRequiredQuantity();
					double availableQuantity = this.getSubProductTablemin()
							.get(i).getMaterialProductAvailableQuantity();
					/**
					 * Date : 30-10-2017 BY ANIL
					 */
					String remarks ="";
					if(this.getSubProductTablemin().get(i).getMaterialProductRemarks()!=null){
						remarks = this.getSubProductTablemin().get(i).getMaterialProductRemarks().trim();
					}
					String warehouseName = this.getSubProductTablemin().get(i)
							.getMaterialProductWarehouse().trim();
					System.out.println("warehouseName" + warehouseName);
					WareHouse warehouse = ofy().load().type(WareHouse.class)
							.filter("companyId", this.companyId)
							.filter("buisnessUnitName", warehouseName).first()
							.now();
					System.out.println("warehouse" + warehouse);
					String warehouseCode = "";
					if (warehouse.getWarehouseCode().trim() != null) {
						warehouseCode = warehouse.getWarehouseCode().trim();
					}
					String storageLocation = this.getSubProductTablemin()
							.get(i).getMaterialProductStorageLocation().trim();
					String storageBin = this.getSubProductTablemin().get(i)
							.getMaterialProductStorageBin().trim();

					UpdateAccountingInterface.updateTally(
							new Date(),// accountingInterfaceCreationDate
							this.getEmployee(),// accountingInterfaceCreatedBy
							this.getStatus(),
							AppConstants.STATUS,// Status
							"",// remark
							"Inventory",// module
							"MIN",// documentType
							this.getCount(),// docID
							this.getMinTitle(),// DOCtile
							this.getMinDate(), // docDate
							AppConstants.DOCUMENTGL,// docGL
							this.getMinMrnId() + "",// ref doc no.1 (Work Order
													// Id)
							this.getMinMrnDate(),// ref doc date.1 (Work Order
													// Date)
							AppConstants.REFDOCMRN,// ref doc type 1
							this.getMinSoId() + "",// ref doc no 2 (Order ID)
							this.getMinSoDate(),// ref doc date 2 (Order Date)
							AppConstants.REFDOCMIN,// ref doc type 2
							"",// accountType
							0,// custID
							"",// custName
							0l,// custCell
							0,// vendor ID
							"",// vendor NAme
							0l,// vendorCell
							0,// empId
							" ",// empNAme
							this.getBranch(),// branch
							this.getMinIssuedBy(),// personResponsible (Issued
													// By)
							this.getEmployee(),// requestedBY
							this.getApproverName(),// approvedBY
							"",// paymentmethod
							null,// paymentdate
							"",// cheqno.
							null,// cheqDate
							null,// bankName
							null,// bankAccount
							0,// transferReferenceNumber
							null,// transactionDate
							null,// contract start date
							null, // contract end date
							prodId,// prod ID
							productCode,// prod Code
							productName,// productNAme
							productQuantity,// prodQuantity
							null,// productDate
							0,// duration
							0,// services
							unitofmeasurement,// unit of measurement
							0.0,// productPrice
							0.0,// VATpercent
							0.0,// VATamt
							0,// VATglAccount
							0.0,// serviceTaxPercent
							0.0,// serviceTaxAmount
							0,// serviceTaxGLaccount
							"",// cform
							0,// cformpercent
							0,// cformamount
							0,// cformGlaccount
							0.0,// totalamouNT
							0.0,// NET PAPAYABLE
							"",// Contract Category
							0,// amount Recieved
							0.0,// base Amt for tdscal in pay by rohan
							0.0, // tds percentage by rohan
							0.0,// tds amount by rohan
							"", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "",
							0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0,
							"", 0.0, "", "", "", "", "", "", 0l,
							this.getCompanyId(), null, // billing from date
														// (rohan)
							null, // billing to date (rohan)
							warehouseName, // Warehouse
							warehouseCode, // warehouseCode
							"", // ProductRefId
							AccountingInterface.OUTBOUNDDIRECTION, // Direction
							AppConstants.CCPM, // sourceSystem
							AppConstants.NBHC, // Destination System
							null,null,null
							);

				}

			}
		}
	}

}
