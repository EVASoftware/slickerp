package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

@Entity
public class BillOfMaterial extends SuperModel{

	private static final long serialVersionUID = 3125739421289632211L;

	/**
	 * 
	 */
	
	/******************************************Applicability Attributes************************************/
	
	
	@Index
	protected boolean status;
	@Index
	protected int productGroupId;
	@Index
	protected int product_id;
	@Index
	protected String code;
	@Index
	protected String name;
	@Index
	protected String title;
	@Index
	protected ArrayList<BillProductDetails>billProdItems;
	@Index
	protected ArrayList<CompanyAsset>toolItems;
 	
	/*****************************************Constructor******************************************/
	
	public BillOfMaterial() {
		super();
		title="";
		billProdItems = new ArrayList<BillProductDetails>();
		toolItems=new ArrayList<CompanyAsset>();
	}

	/********************************************Getters And Setters*****************************************/
	
	public int getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(int productGroupId) {
		this.productGroupId = productGroupId;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		if(code!=null){
			this.code = code.trim();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name!=null){
			this.name = name.trim();
		}
	}
	
	
	public String gettitle() {
		return title;
	}
	public void setTitle(String title) {
		if(title!=null){
			this.title = title.trim();
		}
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public List<BillProductDetails> getBillProdItems() {
		return billProdItems;
	}

	public void setBillProdItems(List<BillProductDetails> billProdItems) {
		ArrayList<BillProductDetails> arrBillItems=new ArrayList<BillProductDetails>();
		arrBillItems.addAll(billProdItems);
		this.billProdItems = arrBillItems;
	}

	public List<CompanyAsset> getToolItems() {
		return toolItems;
	}

	public void setToolItems(List<CompanyAsset> toolItems) {
		ArrayList<CompanyAsset> arrTools=new ArrayList<CompanyAsset>();
		arrTools.addAll(toolItems);
		this.toolItems = arrTools;
	}

	/*****************************************Overridden Method*****************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}



	
}
