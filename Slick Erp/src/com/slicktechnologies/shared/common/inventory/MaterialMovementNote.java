package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.inventory.materialmovementnote.MaterialMovementNoteForm;
import com.slicktechnologies.client.views.inventory.materialmovementtype.MaterialMovementTypeForm;
import com.slicktechnologies.server.ApprovableServiceImplementor;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;
@Embed
@Entity
public class MaterialMovementNote extends ConcreteBusinessProcess {

	/**
	 * 
	 */
	private static final long serialVersionUID = -921651502098619552L;
	
	/**********************************************Constants**********************************************/
	
	public static final String CREATED = "Created";
	public static final String REQUESTED = "Requested";
	public static final String APPROVED = "Approved";
	public static final String REJECTED = "Rejected";
	public static final String CANCELLED = "Cancelled";
	
	
	/*****************************************Applicability Attributes************************************/
	
	@Index
	protected String mmnTitle;
	@Index
	protected Date mmnDate;
	@Index
	protected int mmnWoId;
	protected Date mmnWoDate;
	@Index
	protected int mmnMrnId;
	protected Date mmnMrnDate;
	@Index
	protected int mmnMinId;
	protected Date mmnMinDate;
	@Index
	protected String mmnCategory;
	@Index
	protected String mmnType;
	@Index
	protected String mmnTransactionType;
//	@Index
//	protected String mmnDirection;
	@Index
	protected String mmnPersonResposible;
	@Index
	protected String mmnWarehouse;

	protected String mmnCommentOnStatusChange;

	protected String mmnDescription;
	
	protected DocumentUpload upload;
	@Index
	protected int serviceId;

	ArrayList<MaterialProduct> subProductTableMmn;
	
	@Index
	protected String orderID;
	
	@Index
	protected String transToWareHouse;
	@Index
	protected String transToStorBin;
	@Index
	protected String transToStorLoc;
	@Index
	protected String transDirection;
	/** date 25/11/2017 added by komal for hvac  to select defective **/
	@Index
	boolean recordSelect;
	@Index
	protected String mmnGRNStatus;
	
	/**
	 * Date 05-04-2019 by Vijay for NBHC CCPM SAP Number
	 */
	@Index
	protected String referenceNo;
	/** date 31.05.2019 added by komal to store service id of all services for multiple service scheduling**/
	@Index
	protected ArrayList<Integer> serviceIdList;
//	@Index
//	protected boolean isAccountingInterface;
	/******************************************Constructor*********************************************/
	
	public MaterialMovementNote() {
		subProductTableMmn = new ArrayList<MaterialProduct>();
		mmnTitle="";
		mmnCategory="";
		mmnType="";
		mmnTransactionType="";
		mmnPersonResposible="";
		mmnWarehouse="";
		mmnCommentOnStatusChange="";
		mmnDescription="";
		upload=new DocumentUpload();
		transToWareHouse="";
		transToStorLoc="";
		transToStorBin="";
		transDirection="";
		orderID="";
		referenceNo="";
		/** date 31.05.2019 added by komal to store service id of all services for multiple service scheduling**/
		serviceIdList = new ArrayList<Integer>();
	//	isAccountingInterface  = false;
	}
	
	/************************************Getters And Setters*******************************************/
	
	
	public String getMmnTitle() {
		return mmnTitle;
	}

	public void setMmnTitle(String mmnTitle) {
		if(mmnTitle!=null){
			this.mmnTitle = mmnTitle.trim();
		}
	}

	public Date getMmnDate() {
		return mmnDate;
	}

	public void setMmnDate(Date mmnDate) {
		if(mmnDate!=null){
			this.mmnDate = mmnDate;
		}
	}

	public int getMmnWoId() {
		return mmnWoId;
	}

	public void setMmnWoId(int mmnWoId) {
		this.mmnWoId = mmnWoId;
	}

	public Date getMmnWoDate() {
		return mmnWoDate;
	}

	public void setMmnWoDate(Date mmnWoDate) {
		if(mmnWoDate!=null){
			this.mmnWoDate = mmnWoDate;
		}
	}

	public int getMmnMrnId() {
		return mmnMrnId;
	}

	public void setMmnMrnId(int mmnMrnId) {
		this.mmnMrnId = mmnMrnId;
	}

	public Date getMmnMrnDate() {
		return mmnMrnDate;
	}

	public void setMmnMrnDate(Date mmnMrnDate) {
		if(mmnMrnDate!=null){
			this.mmnMrnDate = mmnMrnDate;
		}
	}

	public int getMmnMinId() {
		return mmnMinId;
	}

	public void setMmnMinId(int mmnMinId) {
		this.mmnMinId = mmnMinId;
	}

	public Date getMmnMinDate() {
		return mmnMinDate;
	}

	public void setMmnMinDate(Date mmnMinDate) {
		if(mmnMinDate!=null){
			this.mmnMinDate = mmnMinDate;
		}
	}

	public String getMmnCategory() {
		return mmnCategory;
	}

	public void setMmnCategory(String mmnCategory) {
		if(mmnCategory!=null){
			this.mmnCategory = mmnCategory.trim();
		}
	}

	public String getMmnType() {
		return mmnType;
	}

	public void setMmnType(String mmnType) {
		if(mmnType!=null){
			this.mmnType = mmnType.trim();
		}
	}

	public String getMmnTransactionType() {
		return mmnTransactionType;
	}

	public void setMmnTransactionType(String mmnTransactionType) {
		if(mmnTransactionType!=null){
			this.mmnTransactionType = mmnTransactionType.trim();
		}
	}

	public String getMmnPersonResposible() {
		return mmnPersonResposible;
	}

	public void setMmnPersonResposible(String mmnPersonResposible) {
		if(mmnPersonResposible!=null){
			this.mmnPersonResposible = mmnPersonResposible.trim();
		}
	}

	public String getMmnWarehouse() {
		return mmnWarehouse;
	}

	public void setMmnWarehouse(String mmnWarehouse) {
		if(mmnWarehouse!=null){
			this.mmnWarehouse = mmnWarehouse.trim();
		}
	}

	public String getMmnCommentOnStatusChange() {
		return mmnCommentOnStatusChange;
	}

	public void setMmnCommentOnStatusChange(String mmnCommentOnStatusChange) {
		if(mmnCommentOnStatusChange!=null){
			this.mmnCommentOnStatusChange = mmnCommentOnStatusChange.trim();
		}
	}

	public String getMmnDescription() {
		return mmnDescription;
	}

	public void setMmnDescription(String mmnDescription) {
		if(mmnDescription!=null){
			this.mmnDescription = mmnDescription.trim();
		}
	}

	public DocumentUpload getUpload() {
		return upload;
	}

	public void setUpload(DocumentUpload upload) {
		if(upload!=null){
			this.upload = upload;
		}
	}

	public List<MaterialProduct> getSubProductTableMmn() {
		return subProductTableMmn;
	}

	public void setSubProductTableMmn(List<MaterialProduct> subProductTableMmn) {
		if (subProductTableMmn != null) {
			ArrayList<MaterialProduct> materialLis=new ArrayList<MaterialProduct>();
			materialLis.addAll(subProductTableMmn);
			this.subProductTableMmn=materialLis;
		}
	}
	
	public String getTransToWareHouse() {
		return transToWareHouse;
	}

	public void setTransToWareHouse(String transToWareHouse) {
		if(transToWareHouse!=null){
			this.transToWareHouse = transToWareHouse.trim();
		}
	}

	public String getTransToStorBin() {
		return transToStorBin;
	}

	public void setTransToStorBin(String transToStorBin) {
		if(transToStorBin!=null){
			this.transToStorBin = transToStorBin.trim();
		}
	}

	public String getTransToStorLoc() {
		return transToStorLoc;
	}

	public void setTransToStorLoc(String transToStorLoc) {
		if(transToStorLoc!=null){
			this.transToStorLoc = transToStorLoc.trim();
		}
	}

	public String getTransDirection() {
		return transDirection;
	}

	public void setTransDirection(String transDirection) {
		if(transDirection!=null){
			this.transDirection = transDirection.trim();
		}
	}
	
	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		if(orderID!=null){
			this.orderID = orderID.trim();
		}
	}
	
	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

//	public boolean isAccountingInterface() {
//		return isAccountingInterface;
//	}
//
//	public void setAccountingInterface(boolean isAccountingInterface) {
//		this.isAccountingInterface = isAccountingInterface;
//	}

	/**************************************Status List Box for Search***************************************/


	public static List<String> getStatusList() {
		ArrayList<String> statusListBox = new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(REQUESTED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		statusListBox.add(CANCELLED);
		return statusListBox;
	}
	
	/*******************************************Overridden Method****************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	
	/********************************************Approval Logic**************************************/
	
	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		System.out.println("Inside Approval..................");
		
		if(this.getStatus().equals(ConcreteBusinessProcess.APPROVED))
		{
			System.out.println("Inside Condition ..............");
			
//			for(int i=0;i<getSubProductTableMmn().size();i++){
//				
//				int productId=getSubProductTableMmn().get(i).getMaterialProductId();
//				String storageLocation=getSubProductTableMmn().get(i).getMaterialProductStorageLocation();
//				String storageBin=getSubProductTableMmn().get(i).getMaterialProductStorageBin();
//				String warehouse=getSubProductTableMmn().get(i).getMaterialProductWarehouse();
//				double productQty=getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity();
//				Date date=getCreationDate();
//				long compId=getCompanyId();
//				int idCount=this.getCount();
//				String mmnTitle1=this.getMmnTitle();
//				String transactionType=this.getMmnTransactionType().trim();
//				
//				MaterialMovementType mmt=ofy().load().type(MaterialMovementType.class).filter("companyId", compId).filter("mmtName", transactionType).first().now();
//				
//				String transaction=this.getTransDirection().trim();
//				String operation;
//				if(transaction.equals("IN")){
//					 operation=AppConstants.ADD;
//				}
//				else if(transaction.equals("OUT")){
//					operation=AppConstants.SUBTRACT;
//				}
//				
//				else if(transaction.equals("TRANSFEROUT")){
//					operation=AppConstants.SUBTRACT;
//					if(i==this.getSubProductTableMmn().size()-1){
//					createNewMMNForSelectedWareHouse();
//					}
//				}
//				
//				else if(transaction.equals("TRANSFERIN")){
//					
//					System.out.println("transfer in start");
//					
//					 storageLocation=this.getTransToStorLoc();
//					 storageBin=this.getTransToStorBin();
//					 warehouse=this.getTransToWareHouse();
//					 
//					operation=AppConstants.ADD;
//					
//					System.out.println("transfer in end");
//				}
//				else{
//					operation=" ";
//				}
//				
//				System.out.println("Id :: "+productId);
//				
//				
//				UpdateStock.setProductInventory(productId,productQty, idCount,date,AppConstants.MMN+"-"+transactionType,mmnTitle1,
//					warehouse, storageLocation, storageBin,operation,compId);
//			}
			
//			updateInventoryViewList();
			
			
			/**
			 * Date : 18-01-2017 By Anil
			 * New Stock Updation and Transaction creation Code
			 */
			
			ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
			for(MaterialProduct product:getSubProductTableMmn()){
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(this.getCompanyId());
				item.setBranch(this.getBranch());
				item.setDocumentType(AppConstants.MMN);
				item.setDocumentId(this.getCount());
				item.setDocumentDate(this.getCreationDate());
				item.setDocumnetTitle(this.getMmnTitle());
				if(this.getServiceId()>0)
					item.setServiceId(this.getServiceId()+"");
				item.setProdId(product.getMaterialProductId());
				item.setUpdateQty(product.getMaterialProductRequiredQuantity());
				/*** Date 30-08-2019 by Vijay for Inventory Management Lock seal ***/
				item.setProductCode(product.getMaterialProductCode());
				item.setWarehouseName(product.getMaterialProductWarehouse());
				item.setStorageLocation(product.getMaterialProductStorageLocation());
				item.setStorageBin(product.getMaterialProductStorageBin());
				
				String transaction=this.getTransDirection().trim();
				String operation = null;
				if(transaction.equals("IN")){
					 operation=AppConstants.ADD;
				}else if(transaction.equals("OUT")){
					operation=AppConstants.SUBTRACT;
				}else if(transaction.equals("TRANSFEROUT")){
					operation=AppConstants.SUBTRACT;
				}else if(transaction.equals("TRANSFERIN")){
					operation = AppConstants.ADD;
					item.setWarehouseName(this.getTransToWareHouse());
					item.setStorageLocation(this.getTransToStorLoc());
					item.setStorageBin(this.getTransToStorBin());
					System.out.println("transfer in end");
				}
				item.setOperation(operation);
				
				/**
				 * Date : 01-03-2017 By ANIL
				 */
				item.setDocumentSubType(transaction);
				/*** Date 17-04-2019 by Vijay for Scrap Transaction Type must display as Scrap. it was showing as OUT ***/
				if(transaction.equals("OUT")){
					item.setDocumentSubType("Scrap");
				}
				
				/**
				 * Date 02-05-2019 by Vijay for NBHC IM Serial No mapping
				 */
				HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					
				if(product.getProSerialNoDetails() !=null &&
						product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
					prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
					for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
						 ProductSerialNoMapping element = new ProductSerialNoMapping();
						 element.setAvailableStatus(pr.isAvailableStatus());
						 element.setNewAddNo(pr.isNewAddNo());
						 element.setStatus(pr.isStatus());
						 element.setProSerialNo(pr.getProSerialNo());
						 prserdt.get(0).add(element);
					}
					for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
				      {
						ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
						if(!pr.isStatus()){
								itr.remove();
						}
				      }
				}
				item.setProSerialNoDetails(prserdt);

				/**
				 * ends here
				 */
				
				itemList.add(item);
				/** date 14/12/2017 added by komal for mmn approval for hvac **/
				if(product.getMaterialProductCategory()!=null){
				if(product.getMaterialProductCategory().equalsIgnoreCase("ASSET")){
					ArrayList<SuperModel> list = new ArrayList<SuperModel>();
					if(product.getCompanyAsset().get(product.getMaterialProductId()+"").size()!=0){
						for(CompanyAsset c : product.getCompanyAsset().get(product.getMaterialProductId()+"")){
							c.setRecordSelect(false);
							c.setReferenceNumber(this.getTransToStorBin());
							list.add(c);
						}
						GenricServiceImpl service = new GenricServiceImpl();
						service.save(list);
					}
				}
				}
				/**
				 *  end
				 */
				
				/** Date 04-12-2019 by vijay for Inventory management lock seal Number ***/
				if(product.getLockSealSerialNo()!=null){
				item.setSerialNumber(product.getLockSealSerialNo());
				}

			}
//			UpdateStock.setProductInventory(itemList);
			/**
			 * Date 29-11-2019 By Vijay
			 * Des :- NBHC Inventory Management Lock Seal Stock Updation from ProductInventoryDetails master
			 */
			 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryViewDetails", "EnableStockUpdation", this.getCompanyId())){
				 UpdateStock.setProductInventoryViewDetails(itemList);
			 }
			 else{
				 UpdateStock.setProductInventory(itemList); 
			 }
			 
			if(this.getTransDirection().trim().equals("TRANSFEROUT")){
				createNewMMNForSelectedWareHouse();
			}
			/**
			 * End
			 */
			/**
			 * Rahul added this on 19 July 2017 
			 */
			if(this.mmnType.equalsIgnoreCase("NoAccountingInterface")){
			}else{
				accountingInterface();
			}
			/**
			 * Ends
			 */
			boolean bomProcessActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillOfMaterial","ApplyBillOfMaterial", getCompanyId());
			if(this.getMmnMinId()!=0 && bomProcessActive){
				if(getSubProductTableMmn().size()>0){

					String jsonProDetails="";
					
						Gson gson = new Gson();
						if(getSubProductTableMmn()!=null && getSubProductTableMmn().size()>0){
							jsonProDetails = gson.toJson(this);
						
						
//							Queue queue = QueueFactory.getQueue("BOMProductListMapping-queue");
//							queue.add(TaskOptions.Builder.withUrl("/slick_erp/bomProductListtaskqueue")
//									.param("mmnObject", jsonProDetails).param("processName","mmnMapping"));
							Queue queue = QueueFactory.getQueue("BOMProductListMappingTaskQueue-Queue");
							queue.add(TaskOptions.Builder.withUrl("/slick_erp/bomproductlisttaskqueue")
									.param("mmnObject", jsonProDetails).param("processName","mmnMapping"));
						
						}
						
		
				
				}
			}
			
//			/**
//			 * Date 27-12-2018 By Vijay
//			 * Des :- NBHC Inventory Management (This is in UAT so code commented )
//			 */
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote",
//					"EnableAutomaticMRNOrderQty", this.getCompanyId())){
//				updateMRNDoc(this);
//			}
//			/**
//			 * ends here
//			 */
		}
	}
	
	
	
	@GwtIncompatible
	private void sendApprovalRequest(MaterialMovementNote mmn) 
	{
		System.out.println("before setting data to approval");
		Approvals approval=new Approvals();
		approval.setApproverName(mmn.getApproverName());
		System.out.println(" Hi approver name ============= "+mmn.getApproverName());
		approval.setBranchname(mmn.getBranch());
		approval.setBusinessprocessId(mmn.getCount());
		approval.setCreationDate(new Date());
		approval.setRequestedBy(mmn.getEmployee());
//		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.PENDING);
		approval.setBusinessprocesstype("MMN");
		approval.setPersonResponsible(mmn.getMmnPersonResposible());
		approval.setCompanyId(mmn.getCompanyId());
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(approval);
		System.out.println("after saving data to approval");
	}

	@GwtIncompatible
	protected void updateInventoryViewList()
	{
		List<MaterialProduct> materialLis=this.getSubProductTableMmn();
		for(int i=0;i<materialLis.size();i++)
		{
			int prodId=materialLis.get(i).getMaterialProductId();
			String wareHouse=materialLis.get(i).getMaterialProductWarehouse();
			String storageLoc=materialLis.get(i).getMaterialProductStorageLocation();
			String storageBin=materialLis.get(i).getMaterialProductStorageBin();
			
			ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class).filter("count",prodId).filter("warehousename", wareHouse.trim()).filter("storagelocation", storageLoc.trim()).filter("storagebin", storageBin.trim()).filter("companyId", getCompanyId()).first().now();
			
			if(prodInvDtls!=null){
				prodInvDtls.setAvailableqty(prodInvDtls.getAvailableqty()+materialLis.get(i).getMaterialProductRequiredQuantity());
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(prodInvDtls);
			}
		}
	}
	

	/**************Accounting Interface*********************
	 * Created by Rahul Verma on 24 April 2017
	 * Created for Interface with other system
	 */
	@GwtIncompatible
	public void accountingInterface() {

		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", this.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGMMN)
					.filter("companyId", getCompanyId())
					.filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag = 0;

		if (processConfig != null) {
			System.out.println("Process Config Not Null");
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				System.out
						.println("----------------------------------------------------------");
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i)
								.getProcessType());
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i).isStatus());
				System.out
						.println("----------------------------------------------------------");
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {
			System.out.println("ExecutingAccInterface");
			if (this.getCount() != 0) {

				System.out.println("Product List Size :: "
						+ this.getSubProductTableMmn().size());

				/*************************************************************************************************************/
				for (int i = 0; i < this.getSubProductTableMmn().size(); i++) {

					/************************************************* Start ************************************************/

					String unitofmeasurement = this.getSubProductTableMmn()
							.get(i).getMaterialProductUOM();
					int prodId = this.getSubProductTableMmn().get(i)
							.getMaterialProductId();
					String productCode = this.getSubProductTableMmn().get(i)
							.getMaterialProductCode();
					String productName = this.getSubProductTableMmn().get(i)
							.getMaterialProductName();
					double productQuantity = this.getSubProductTableMmn()
							.get(i).getMaterialProductRequiredQuantity();
					double availableQuantity = this.getSubProductTableMmn()
							.get(i).getMaterialProductAvailableQuantity();
					String remarks = this.getSubProductTableMmn().get(i)
							.getMaterialProductRemarks().trim();
					String warehouseName = this.getSubProductTableMmn().get(i)
							.getMaterialProductWarehouse().trim();
					String toWarehouseName=this.getTransToWareHouse();
					System.out.println("warehouseName" + warehouseName);
					WareHouse warehouse = ofy().load().type(WareHouse.class)
							.filter("companyId", this.companyId)
							.filter("buisnessUnitName", warehouseName).first()
							.now();
					System.out.println("warehouse" + warehouse);
					String warehouseCode = "";
					if (warehouse.getWarehouseCode().trim() != null) {
						warehouseCode = warehouse.getWarehouseCode().trim();
					}
					WareHouse towarehouse = ofy().load().type(WareHouse.class)
							.filter("companyId", this.companyId)
							.filter("buisnessUnitName", toWarehouseName.trim()).first()
							.now();
					System.out.println("warehouse" + towarehouse);
					String towarehouseCode = "";
					if (towarehouse.getWarehouseCode().trim() != null) {
						towarehouseCode = towarehouse.getWarehouseCode().trim();
					}
					String storageLocation = this.getSubProductTableMmn()
							.get(i).getMaterialProductStorageLocation().trim();
					String storageBin = this.getSubProductTableMmn().get(i)
							.getMaterialProductStorageBin().trim();

					UpdateAccountingInterface.updateTally(
							new Date(),// accountingInterfaceCreationDate
							this.getEmployee(),// accountingInterfaceCreatedBy
							this.getStatus(),
							AppConstants.STATUS,// Status
							"",// remark
							"Inventory",// module
							"MMN",// documentType
							this.getCount(),// docID
							this.getMmnTitle(),// DOCtile
							this.getMmnDate(), // docDate
							AppConstants.DOCUMENTGL,// docGL
							this.getMmnMinId() + "",// ref doc no.1 (Work Order
													// Id)
							this.getMmnMinDate(),// ref doc date.1 (Work Order
													// Date)
							AppConstants.REFDOCMIN,// ref doc type 1
							this.getMmnWoId() + "",// ref doc no 2 (Order ID)
							this.getMmnWoDate(),// ref doc date 2 (Order Date)
							AppConstants.REFDOCWO,// ref doc type 2
							"",// accountType
							0,// custID
							"",// custName
							0l,// custCell
							0,// vendor ID
							"",// vendor NAme
							0l,// vendorCell
							0,// empId
							" ",// empNAme
							this.getBranch(),// branch
							this.getMmnPersonResposible(),// personResponsible
															// (Issued By)
							this.getEmployee(),// requestedBY
							this.getApproverName(),// approvedBY
							"",// paymentmethod
							null,// paymentdate
							"",// cheqno.
							null,// cheqDate
							null,// bankName
							null,// bankAccount
							0,// transferReferenceNumber
							null,// transactionDate
							null,// contract start date
							null, // contract end date
							prodId,// prod ID
							productCode,// prod Code
							productName,// productNAme
							productQuantity,// prodQuantity
							null,// productDate
							0,// duration
							0,// services
							unitofmeasurement,// unit of measurement
							0.0,// productPrice
							0.0,// VATpercent
							0.0,// VATamt
							0,// VATglAccount
							0.0,// serviceTaxPercent
							0.0,// serviceTaxAmount
							0,// serviceTaxGLaccount
							"",// cform
							0,// cformpercent
							0,// cformamount
							0,// cformGlaccount
							0.0,// totalamouNT
							0.0,// NET PAPAYABLE
							"",// Contract Category
							0,// amount Recieved
							0.0,// base Amt for tdscal in pay by rohan
							0.0, // tds percentage by rohan
							0.0,// tds amount by rohan
							"", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "",
							0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0, "", 0.0,
							"", 0.0, "", "", "", "", "", "", 0l,
							this.getCompanyId(), null, // billing from date
														// (rohan)
							null, // billing to date (rohan)
							warehouseName+"$"+toWarehouseName, // Warehouse
							warehouseCode+"$"+towarehouseCode, // warehouseCode
							"", // ProductRefId
							AccountingInterface.OUTBOUNDDIRECTION, // Direction
							AppConstants.CCPM, // sourceSystem
							AppConstants.NBHC, // Destination System
							null,null,null
							);

				}

			}

		}

	}

	@GwtIncompatible
	public void createNewMMNForSelectedWareHouse() {

		/**
		 * Date : 27-02-2017 By Anil
		 * Loading Transfer out warehouse for mapping its branch in MMN TransferIn Document
		 */
		String tranferWarehousBranch="";
		WareHouse warehouse=ofy().load().type(WareHouse.class).filter("companyId", this.getCompanyId()).filter("buisnessUnitName", this.getTransToWareHouse()).first().now();
		if(warehouse!=null){
			if(warehouse.getBranchdetails()!=null&&warehouse.getBranchdetails().size()!=0){
				tranferWarehousBranch=warehouse.getBranchdetails().get(0).getBranchName();
			}
		}
		/**
		 * End
		 */
		
		final MaterialMovementNote mmn=new MaterialMovementNote();
		
		if(this.getMmnMinId()!=0){
			mmn.setMmnMinId(this.getMmnMinId());
			System.out.println("getMmnMinId===="+this.getMmnMinId());
		}
		
		if(this.getMmnMinDate()!=null){
			mmn.setMmnMinDate(this.getMmnMinDate());
		}
		
		if(this.getMmnWoId()!=0){
			mmn.setMmnWoId(this.getCount());
		}
		
		if(this.getMmnWoDate()!=null){
			mmn.setMmnWoDate(this.getMmnWoDate());
		}
		
		
		
		if(this.getMmnTitle()!=null){
			mmn.setMmnTitle(this.getMmnTitle());
		}
		
		if(this.getMmnDate()!=null){
			mmn.setMmnDate(this.getMmnDate());
		}
		
		
		mmn.setMmnTransactionType("TransferIN");
		
		mmn.setTransDirection("TRANSFERIN");
		
		
		
		
		/**
		 * Date :27-02-2017 By Anil
		 */
		if(this.getBranch()!=null){
			mmn.setBranch(tranferWarehousBranch);
		}
		/**
		 * End
		 */
		
		if(this.getMmnCategory()!=null){
			mmn.setMmnCategory(this.getMmnCategory());
		}
		
		if(this.getMmnType()!=null){
			mmn.setMmnType(this.getMmnType());
		}
		
		
		
		if(this.getMmnPersonResposible()!=null){
			mmn.setMmnPersonResposible(this.getMmnPersonResposible());
		}
		
		if(this.getApproverName()!=null){
			mmn.setApproverName(this.getEmployee());
		}
		/**
		 * @author Vijay 
		 * Des :- Inventory Management App MMN OUT Approver name mapping in MMN in as requested by
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", AppConstants.PC_MMNOUTAPPROVERASMMNINREQUESTEDBY, this.getCompanyId())) {
			if(this.getApproverName()!=null){
				mmn.setEmployee(this.getApproverName());
			}
		}
		else {
			if(this.getEmployee()!=null){
				mmn.setEmployee(this.getEmployee());
			}
		}
		
		
		mmn.setStatus(MaterialMovementNote.REQUESTED);
		
		if(this.getMmnCommentOnStatusChange()!=null){
			mmn.setMmnCommentOnStatusChange(this.getMmnCommentOnStatusChange());
		}
		
		if(this.getTransToStorBin()!=null){
			mmn.setTransToStorBin(this.getTransToStorBin());
		}
		System.out.println("TransToStorBin******************"+this.getTransToStorBin());
		
		
		if(this.getTransToStorLoc()!=null){
			mmn.setTransToStorLoc(this.getTransToStorLoc());
		}
		System.out.println("TransToStorLoc*****************"+this.getTransToStorLoc());
		
		
		if(this.getTransToWareHouse()!=null){
			mmn.setTransToWareHouse(this.getTransToWareHouse());
		}
		System.out.println("TransToWareHouse*************"+this.getTransToWareHouse());
		
		
		if(this.getUpload()!=null){
			mmn.setUpload(this.getUpload());
		}
		
		if(this.getMmnDescription()!=null){
			mmn.setMmnDescription("Ref MMN-"+this.getCount()+"\n"+this.getMmnDescription());
		}
		
		mmn.setSubProductTableMmn(this.getSubProductTableMmn());
		//vijay
		mmn.setMmnMrnId(this.getMmnMrnId());
		
		mmn.setCompanyId(this.getCompanyId());
		
		/** date 18.03.2019 added by komal to store service id in transferin mmn **/
		mmn.setServiceId(this.getServiceId());
		if(this.getServiceIdList() != null){
			mmn.setServiceIdList(this.getServiceIdList());
		}
		GenricServiceImpl gsimpl= new GenricServiceImpl();
		ReturnFromServer entity = gsimpl.save(mmn);
		Logger logger = Logger.getLogger("MaterialMovementNote.class");
		/*** Date 31-01-2019 by Vijay for NBHC Inventory Management Approve MMN directly with process config ***/
		/** date 14/12/2017 added by komal for mmn approval for hvac **/
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
				"QuickSalesOrder", "SiddhiServices", this.getCompanyId())
				|| ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
						"MaterialRequestNote", "EnableAutomaticMRNOrderQty", this.getCompanyId())
					|| 	mmn.getMmnCategory().equalsIgnoreCase("Android")) {
			mmn.setCount(entity.count);
			for(Integer ser : mmn.getServiceIdList()){
				logger.log(Level.SEVERE, "Id 1: " +ser+"");
				//ser.setMmn(mmn);
			}
			if(mmn.getServiceIdList() != null && mmn.getServiceIdList().size() > 0){
				mmn.setMmnType("NoAccountingInterface");
				ObjectifyService.reset();
				ofy().consistency(Consistency.STRONG);
				ofy().clear();
				List<Service> serList = ofy().load().type(Service.class).filter("companyId", this.getCompanyId())
							.filter("count IN", this.getServiceIdList()).list();
			
				
				for(Service ser : serList){
					logger.log(Level.SEVERE, "Id : " +ser.getCount()+" status="+ser.isServiceScheduled());
					ser.setServiceScheduled(true);//Ashwini Patil Date:20-03-2023
					ser.setMmn(mmn);
				}
				if(serList != null && serList.size() > 0){
					ofy().save().entities(serList);
				}
				
			}
			System.out.println("second mmn approve :" + entity.count);
			// sendApprovalRequest(mmn);
			ApproveMMN(mmn);
		} else {
			sendApprovalRequest(mmn);
		}
		
	}

	public boolean isRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}

	public String getMmnGRNStatus() {
		return mmnGRNStatus;
	}

	public void setMmnGRNStatus(String mmnGRNStatus) {
		this.mmnGRNStatus = mmnGRNStatus;
	}
	
	/** date 14/12/2017 added by komal for mmn approval for hvac **/
	@GwtIncompatible
	private void ApproveMMN(MaterialMovementNote mmn) {
		System.out.println("before setting data to approval");
		Approvals approval = new Approvals();
		approval.setApproverName(mmn.getApproverName());
		System.out.println(" Hi approver name ============= "
				+ mmn.getApproverName());
		approval.setBranchname(mmn.getBranch());
		approval.setBusinessprocessId(mmn.getCount());
		approval.setCreationDate(new Date());
		approval.setRequestedBy(mmn.getMmnPersonResposible());
		approval.setDocumentCreatedBy(mmn.getMmnPersonResposible());
		approval.setApprovalLevel(1);
		approval.setStatus(Approvals.APPROVED);
		approval.setBusinessprocesstype("MMN");
		approval.setPersonResponsible(mmn.getMmnPersonResposible());
		approval.setCompanyId(mmn.getCompanyId());
		approval.setApprovalOrRejectDate(new Date());
		GenricServiceImpl impl = new GenricServiceImpl();
		ReturnFromServer entity = impl.save(approval);
		System.out.println("after saving data to approval mmn2" + entity.count);
		ApprovableServiceImplementor approvals = new ApprovableServiceImplementor();
		// System.out.println("after saving data to approval mmn2");
		System.out.println("Buisness Process Type "
				+ approval.getBusinessprocesstype());
		ApprovableProcess process = null;
		ArrayList<ApprovableProcess> processList = approvals
				.retrieveApprovableProcess(approval.getBusinessprocesstype(),
						approval.getBusinessprocessId(), approval.getStatus(),
						approval.getCompanyId());
		if (processList.size() == 0) {
			System.out.println("No document found for approval!");
		} else if (processList.size() > 1) {
			System.out.println("More than one document exist with same id!");
		} else {
			process = processList.get(0);
		}

		ArrayList<InventoryTransactionLineItem> itemList = new ArrayList<InventoryTransactionLineItem>();

		for (MaterialProduct product : mmn.getSubProductTableMmn()) {
			InventoryTransactionLineItem item = new InventoryTransactionLineItem();
			item.setCompanyId(mmn.getCompanyId());
			item.setBranch(mmn.getBranch());
			item.setDocumentType(AppConstants.MMN);
			item.setDocumentId(mmn.getCount());
			item.setDocumentDate(mmn.getCreationDate());
			item.setDocumnetTitle(mmn.getMmnTitle());
			item.setProdId(product.getMaterialProductId());
			item.setUpdateQty(product.getMaterialProductRequiredQuantity());
			item.setWarehouseName(product.getMaterialProductWarehouse());
			item.setStorageLocation(product.getMaterialProductStorageLocation());
			item.setStorageBin(product.getMaterialProductStorageBin());
			String transaction = mmn.getTransDirection().trim();
			String operation = null;
			if (transaction.equals("IN")) {
				operation = AppConstants.ADD;
			} else if (transaction.equals("OUT")) {
				operation = AppConstants.SUBTRACT;
			} else if (transaction.equals("TRANSFEROUT")) {
				operation = AppConstants.SUBTRACT;
			} else if (transaction.equals("TRANSFERIN")) {
				operation = AppConstants.ADD;
				item.setWarehouseName(mmn.getTransToWareHouse());
				item.setStorageLocation(mmn.getTransToStorLoc());
				item.setStorageBin(mmn.getTransToStorBin());
				System.out.println("transfer in end");
			}
			item.setOperation(operation);
			item.setDocumentSubType(transaction);
			/*** Date 09-05-2019 by Vijay for NBHC IM Serial No mapping ***/
			if(product.getProSerialNoDetails()!=null){
				item.setProSerialNoDetails(product.getProSerialNoDetails());
			}
			/**** Date 24-06-2019 by Vijay Setting Product Name if stock master not define then update with transaction product name display to be display in transaction ***/
			item.setProductName(product.getMaterialProductName());
			item.setProductCode(product.getMaterialProductCode());

			/** Date 04-12-2019 by vijay for Inventory management lock seal Number ***/
			if(product.getLockSealSerialNo()!=null){
			item.setSerialNumber(product.getLockSealSerialNo());
			}
			
			itemList.add(item);
		}
		/***
		 *  Date 09-05-2019 by Vijay for NBHC Inventory Management
		 *  Des :- If Stock Master is not defined for product then update stock master and also update transction acoordingly 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "EnableUpdateStockMatserWithTransaction", this.getCompanyId())){
//			UpdateStock.setProductInventory(itemList,true);
			 UpdateStock.setProductInventoryViewDetails(itemList,true);
		}else{
			UpdateStock.setProductInventory(itemList);
		}
		process.setStatus(approval.getStatus());
		process.setApprovalDate(DateUtility.getDateWithTimeZone("IST",
				new Date()));
		System.out.println("");
		SuperModel mod = (SuperModel) process;
		ofy().save().entity(approval);
		ofy().save().entity(mod);

		/**
		 * Date 27-12-2018 By Vijay
		 * Des :- NBHC Inventory Management
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialRequestNote",
				"EnableAutomaticMRNOrderQty", this.getCompanyId())){
			updateMRNDoc(mmn);
		}
	}

//	public int getMrnId() {
//		return mrnId;
//	}
//
//	public void setMrnId(int mrnId) {
//		this.mrnId = mrnId;
//	}
	
	/**
	 * Date 11-01-2019 by Vijay
	 * Des :- For NBHC Inventory Management MMN Qty updating in MRN
	 * because when we created full qty MMN against MRN then only MRN will close or MRN Open for create MMN against it
	 * and balance MMN qty document will create	
	 * @param mmn 
	 */
	@GwtIncompatible
	private void updateMRNDoc(MaterialMovementNote mmn) {
		Logger logger = Logger.getLogger("Logger");

		if(mmn.getTransDirection().trim().equals("TRANSFERIN")){
		MaterialRequestNote mrn = ofy().load().type(MaterialRequestNote.class).filter("companyId", mmn.getCompanyId())
								.filter("count", mmn.getMmnMrnId()).first().now();
		if(mrn!=null){
			boolean flag = true;
			for(MaterialProduct materialprod : mmn.getSubProductTableMmn()){
				for(MaterialProduct mrnMaterialProd : mrn.getSubProductTableMrn()){
					if(materialprod.getMaterialProductId()==mrnMaterialProd.getMaterialProductId()
						&& materialprod.getMaterialProductCode().trim().equals(mrnMaterialProd.getMaterialProductCode().trim())	){
						System.out.println("materialprod.getMaterialProductBalanceQty() =="+materialprod.getMaterialProductBalanceQty());
						double balanceQty = materialprod.getMaterialProductBalanceQty()-materialprod.getMaterialProductRequiredQuantity();
						if(balanceQty>0){
							mrnMaterialProd.setMaterialProductBalanceQty(balanceQty);
							flag=false;
						}
						else{
							mrnMaterialProd.setMaterialProductBalanceQty(0);
						}
					}
				}
			}
			if(flag){
				mrn.setStatus(MaterialRequestNote.CLOSED);
			}
			ofy().save().entity(mrn);
			
			/**
			 * if MMN Qty Less than MRN Qty then create new MMN
			 */
			checkandCreateMMN(mmn);
			/**
			 * ends here
			 */
			
		}
		/**
		 * Date 13-09-2019 by Vijay
		 * Des :- NBHC Inventory Management Lock seal App MRN Close 
		 */
		else{
			
			List<MaterialRequestNote> materialRequestNotelist = ofy().load().type(MaterialRequestNote.class)
					.filter("companyId", this.getCompanyId()).filter("branch", this.getBranch())
					.filter("status", MaterialRequestNote.APPROVED).list();
			logger.log(Level.SEVERE, "materialRequestNotelist "+materialRequestNotelist.size());
			if(materialRequestNotelist.size()!=0){
				
				Comparator<MaterialRequestNote> materialRequestComparator = new Comparator<MaterialRequestNote>() {
					
					@Override
					public int compare(MaterialRequestNote mrn, MaterialRequestNote mrn2) {
						Date date = mrn.getCreationDate();
						Date date2 = mrn2.getCreationDate();
						return date.compareTo(date2);
					}
				};
				Collections.sort(materialRequestNotelist,materialRequestComparator);
				boolean flag = true;
				for(MaterialProduct materialprod : mmn.getSubProductTableMmn()){
					double mmnQty = materialprod.getMaterialProductRequiredQuantity();
					for(MaterialRequestNote mrn1 : materialRequestNotelist){
						for(MaterialProduct mrnMaterialProd : mrn1.getSubProductTableMrn()){
							if(materialprod.getMaterialProductId()==mrnMaterialProd.getMaterialProductId()
								&& materialprod.getMaterialProductCode().trim().equals(mrnMaterialProd.getMaterialProductCode().trim())	){
								
								logger.log(Level.SEVERE, "mmnQty"+mmnQty);

								if (mrnMaterialProd.getMaterialProductBalanceQty() >= mmnQty) {
									double mrnBalancedQty = Math
											.abs(mrnMaterialProd.getMaterialProductBalanceQty() - mmnQty);
									mrnMaterialProd.setMaterialProductBalanceQty(mrnBalancedQty);
									flag = true;
									ofy().save().entity(mrn1);
									logger.log(Level.SEVERE, "MRN updated successfully");
									break;
								} else {
									 mmnQty = Math.abs(mrnMaterialProd.getMaterialProductBalanceQty() - mmnQty);
									System.out.println("mrnBalancedQty" + mmnQty);
									if (mmnQty > 0) {
										mrnMaterialProd.setMaterialProductBalanceQty(0);
										ofy().save().entity(mrn1);
										logger.log(Level.SEVERE, "MRN updated successfully");
									} else {
										flag = true;
										break;
									}

								}
							
							}
						}
					}
					if(flag){
						break;
					}
					
				}
			}
		}
		
		}
	}

	@GwtIncompatible
	private void checkandCreateMMN(MaterialMovementNote materialMovementnote) {
		MaterialMovementNote mmn = new MaterialMovementNote();
		mmn.setCompanyId(materialMovementnote.getCompanyId());
		mmn.setStatus(MaterialMovementNote.CREATED);
		mmn.setCreationDate(new Date());
		mmn.setMmnDate(new Date());
		
		ArrayList<MaterialProduct> materiallist = new ArrayList<MaterialProduct>();

		if(this.getSubProductTableMmn().size()!=0){
		mmn.setTransToWareHouse(materialMovementnote.getTransToWareHouse());
		mmn.setTransToStorLoc(materialMovementnote.getTransToStorLoc());
		mmn.setTransToStorBin(materialMovementnote.getTransToStorBin());
		System.out.println("New MMN MRN ID=="+materialMovementnote.getMmnMrnId());
		mmn.setMmnMrnId(this.getMmnMrnId());
		for(MaterialProduct materialProdMMN : materialMovementnote.getSubProductTableMmn()){
			if(materialProdMMN.getMaterialProductBalanceQty()>0){
			MaterialProduct materialProd = new MaterialProduct();
			materialProd.setMaterialProductId(materialProdMMN.getMaterialProductId());
			materialProd.setMaterialProductCode(materialProdMMN.getMaterialProductCode());
			materialProd.setMaterialProductName(materialProdMMN.getMaterialProductName());
			if(materialProdMMN.getMaterialProductRequiredQuantity()>materialProdMMN.getMaterialProductBalanceQty()){
				materialProd.setMaterialProductRequiredQuantity(materialProdMMN.getMaterialProductRequiredQuantity());
			}
			else{
				materialProd.setMaterialProductRequiredQuantity(materialProdMMN.getMaterialProductBalanceQty()-materialProdMMN.getMaterialProductRequiredQuantity());
			}
			materialProd.setMaterialProductRemarks(materialProdMMN.getMaterialProductRemarks());
			materialProd.setMaterialProductUOM(materialProdMMN.getMaterialProductUOM());
			if(materialProdMMN.getMaterialProductRequiredQuantity()>materialProdMMN.getMaterialProductBalanceQty()){
				materialProd.setMaterialProductBalanceQty(0);
			}
			else{
				materialProd.setMaterialProductBalanceQty(materialProd.getMaterialProductRequiredQuantity());

			}
			if(materialProd.getMaterialProductRequiredQuantity()!=0){
			materiallist.add(materialProd);
			}
			}
		}
		}
		mmn.setSubProductTableMmn(materiallist);
		mmn.setEmployee(materialMovementnote.getEmployee());
		mmn.setBranch(materialMovementnote.getBranch());	

		if(materiallist.size()>0){
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(mmn);
		}
	
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public ArrayList<Integer> getServiceIdList() {
		return serviceIdList;
	}

	public void setServiceIdList(List<Integer> serviceIdList) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(serviceIdList != null){
			list.addAll(serviceIdList);
		}
		this.serviceIdList = list;
	}

	
	
}
