package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

@Embed
/**
 * Class Repersenting Issue Slip Details.
 * @author Kamala
 *
 */
public class IssueSlipDetails extends InventoryProductDetail implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758706000876446076L;
	/****************************************Attributes************************************************/
	/**
	 * Quantity asked
	 */
	double quantityAsked;
	/**
	 * Actual Quantity Issued
	 */
	double quantityIssued;
	/**
	 * Quantity in Ware House when  Issue Slip was Created
	 */
	double wareHouseQty;
	/****************************************Constructor************************************************/
	public IssueSlipDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	/*****************************Getter Setters**************************************************/
	
	public double getQuantityAsked() {
		return quantityAsked;
	}
	public void setQuantityAsked(double quantityAsked) {
		this.quantityAsked = quantityAsked;
	}
	
	public double getQuantityIssued() {
		return quantityIssued;
	}
	public void setQuantityIssued(double quantityIssued) {
		this.quantityIssued = quantityIssued;
	}
	public double getWareHouseQty() {
		return wareHouseQty;
	}
	public void setWareHouseQty(double wareHouseQty) {
		this.wareHouseQty = wareHouseQty;
	}
	@Override
	public Double getQty() {
		// TODO Auto-generated method stub
		return quantityIssued;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj==null)
			return false;
		if(obj instanceof IssueSlipDetails)
		{
			IssueSlipDetails details=(IssueSlipDetails) obj;
			String lpcodeincoming=details.getWarehouseLocation()+details.getProductCode();
			String lpcodeOwn=this.getWarehouseLocation()+this.getProductCode();
			if(lpcodeOwn!=null&&lpcodeincoming!=null)
			{
				if(lpcodeOwn.equals(lpcodeincoming))
				{
					System.out.println("********************");
					return true;
				}
			}
			else 
				return false;
		}
		
		return false;
	}
	
   	
	
	

}
