package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.ProductSerialNoMapping;


@Embed
public class PhysicalInventoryProducts implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -968480984086462110L;
	
	/*********************************************Applicability Attributes**************************************/

	@Index
	protected Integer productid;
	@Index
	protected String productname;
	protected String unitofmeorment;
	@Index
	protected String warehousename;
	@Index
	protected String storagelocation;
	@Index
	protected String storagebin;
	@Index
	protected String branch;//Ashwini Patil Date:14-06-2023
	protected List<String> branchlist;//Ashwini Patil Date:14-06-2023 warehousewise branchlist getting stored for selection
	protected double minqty;
	protected double maxqty;
	protected double normallevel;
	protected double reorderlevel;
	protected double reorderqty;
	protected double availableqty;
	protected double variance;
	protected String remark;
	
	
	/**
	 * nidhi 30-11-2018
	 */
	protected double actualQty;
	
	/**
	 * nidhi 21-08-2018 for map serial number details
	 * @author Anil, Date : 29-04-2019
	 * copied nidhis logic for storing model sr. no.
	 */
	@EmbedMap
	@Serialize
	protected HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

	/*********************************************Getters And Setters****************************************/
	
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}

	public void setProSerialNoDetails(HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}
	
	public Integer getProductid() {
		return productid;
	}
	
	public void setProductid(Integer productid) {
		this.productid = productid;
	}
	
	public String getProductname() {
		return productname;
	}
	
	public void setProductname(String productname) {
		this.productname = productname;
	}
	
	public String getUnitofmeorment() {
		return unitofmeorment;
	}
	
	public void setUnitofmeorment(String unitofmeorment) {
		this.unitofmeorment = unitofmeorment;
	}
	
	public String getWarehousename() {
		return warehousename;
	}
	
	public void setWarehousename(String warehousename) {
		this.warehousename = warehousename;
	}
	
	public String getStoragelocation() {
		return storagelocation;
	}
	
	public void setStoragelocation(String storagelocation) {
		this.storagelocation = storagelocation;
	}
	
	public String getStoragebin() {
		return storagebin;
	}
	
	public void setStoragebin(String storagebin) {
		this.storagebin = storagebin;
	}
	
	public double getAvailableqty() {
		return availableqty;
	}
	
	public void setAvailableqty(double availableqty) {
		this.availableqty = availableqty;
	}
	
	public double getVariance() {
		return variance;
	}
	
	public void setVariance(double variance) {
		this.variance = variance;
	}
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getMinqty() {
		return minqty;
	}

	public void setMinqty(double minqty) {
		this.minqty = minqty;
	}

	public double getMaxqty() {
		return maxqty;
	}

	public void setMaxqty(double maxqty) {
		this.maxqty = maxqty;
	}

	public double getNormallevel() {
		return normallevel;
	}

	public void setNormallevel(double normallevel) {
		this.normallevel = normallevel;
	}

	public double getReorderlevel() {
		return reorderlevel;
	}

	public void setReorderlevel(double reorderlevel) {
		this.reorderlevel = reorderlevel;
	}

	public double getReorderqty() {
		return reorderqty;
	}

	public void setReorderqty(double reorderqty) {
		this.reorderqty = reorderqty;
	}

	public double getActualQty() {
		return actualQty;
	}

	public void setActualQty(double actualQty) {
		this.actualQty = actualQty;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public List<String> getBranchlist() {
		return branchlist;
	}

	public void setBranchlist(List<String> branchlist) {
		this.branchlist = branchlist;
	}
	
	
	
	
	

}
