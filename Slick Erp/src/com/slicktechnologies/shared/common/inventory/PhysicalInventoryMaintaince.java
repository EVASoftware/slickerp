package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;

@Entity
public class PhysicalInventoryMaintaince extends ConcreteBusinessProcess {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
/*******************************************Applicability Attributes***********************************/

	@Index
	protected String title;
	@Index
	protected Date date;
	protected ArrayList<PhysicalInventoryProducts> productsLis;
	
	/***************************************Constructor***********************************************/
	
	public PhysicalInventoryMaintaince() {
		super();
		title="";
		productsLis=new ArrayList<PhysicalInventoryProducts>();
	}

	
	/******************************************Getters And Setters*************************************/

	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		if(title!=null){
			this.title = title.trim();
		}
	}
	
	
	public Date getDate() {
		return date;
	}
	
	
	public void setDate(Date date) {
		if(date!=null){
			this.date = date;
		}
	}
	
	public List<PhysicalInventoryProducts> getProductsLis() {
		return productsLis;
	}


	public void setProductsLis(List<PhysicalInventoryProducts> productsLis) {
		ArrayList<PhysicalInventoryProducts> arrProdLis=new ArrayList<PhysicalInventoryProducts>();
		arrProdLis.addAll(productsLis);
		this.productsLis = arrProdLis;
	}
	

	/**
	 * Called on save for saving variance
	 */
	
	@OnSave
	@GwtIncompatible
	protected void reactingOnSave()
	{
		if(this.getDate()!=null){
			setDate(DateUtility.getDateWithTimeZone("IST",this.getDate()));
		}
		
		saveVariance();
		
	}
	
	/**
	 * This method is called on save of physical inventory.
	 * If variance in product information table is greater than 0 then Stock is updated with add operation.
	 * If variance is less than 0 then it is multipled with -1 to get minus value and then stock is updated.
	 */
	@GwtIncompatible
	protected void saveVariance()
	{
		
		boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PhysicalInventoryProducts", "ActualProductQtyMapActive",this.getCompanyId());
		
		if(flag){
			return;
		}
//		for(int i=0;i<getProductsLis().size();i++){
//			
//			int productId=getProductsLis().get(i).getProductid();
//			String warehouse=getProductsLis().get(i).getWarehousename();
//			String storageLocation=getProductsLis().get(i).getStoragelocation();
//			String storageBin=getProductsLis().get(i).getStoragebin();
//			double varaince=getProductsLis().get(i).getVariance();
//			Date docDate=getDate();
//			int docId=getCount();
//			String title=getTitle();
//			long comID=getCompanyId();
//			
//			
//		if(varaince>0){
//			UpdateStock.setProductInventory( productId, varaince, docId, docDate, "Phy. Inv Variance", title,
//					 warehouse, storageLocation, storageBin, AppConstants.ADD, comID);
//		}
//		else
//		{
//			varaince=varaince*(-1);
//			UpdateStock.setProductInventory( productId, varaince, docId, docDate, "Phy. Inv Variance", title,
//					 warehouse, storageLocation, storageBin, AppConstants.SUBTRACT, comID);
//		}
//		}
		
		/**
		 * Date : 18-01-2017 By Anil
		 * New Stock Updation and Transaction creation Code
		 */
		
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(PhysicalInventoryProducts product:getProductsLis()){
			InventoryTransactionLineItem item=new InventoryTransactionLineItem();
			item.setCompanyId(this.getCompanyId());
			item.setBranch(product.getBranch());//Ashwini Patil 14-06-2023
			item.setDocumentType(AppConstants.PHYSICALINVENTORY);
			item.setDocumentId(this.getCount());
			item.setDocumentDate(this.getDate());
			item.setDocumnetTitle("");
			item.setProdId(product.getProductid());
			double varaince=product.getVariance();
			String operations=null;
			if(varaince==0){
				return;
			}else if(varaince>0){
				operations=AppConstants.ADD;
			}else{
				varaince=varaince*(-1);
				operations=AppConstants.SUBTRACT;
			}
			item.setUpdateQty(varaince);
			item.setOperation(operations);
			item.setWarehouseName(product.getWarehousename());
			item.setStorageLocation(product.getStoragelocation());
			item.setStorageBin(product.getStoragebin());
			
			
			/**
			 * nidhi
			 * 23-08-2018
			 * for map serial number
			 * @author Anil , Date : 30-04-2019
			 */
			HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				
			if(product.getProSerialNoDetails() !=null 
					&&product.getProSerialNoDetails().containsKey(0) 
					&& product.getProSerialNoDetails().get(0)!=null){
				
				prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
				for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
					
					 ProductSerialNoMapping element = new ProductSerialNoMapping();
					 element.setAvailableStatus(pr.isAvailableStatus());
					 element.setNewAddNo(pr.isNewAddNo());
					 element.setStatus(pr.isStatus());
					 element.setProSerialNo(pr.getProSerialNo());
					 if(!pr.isStatus()){
						 prserdt.get(0).add(element);
					 }
				}
//				for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();){
//					ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
//					pr.setStatus(true);
//			    }
			}
			item.setProSerialNoDetails(prserdt);
			
			itemList.add(item);
		}
		UpdateStock.setProductInventory(itemList);
		
		/**
		 * End
		 */
		
		
	}
	
	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		Logger logger=Logger.getLogger("Starting Approval Logger ");
		logger.log(Level.SEVERE,"Starting Approval Process");
	
		/**
		 * Date : 18-01-2017 By Anil
		 * New Stock Updation and Transaction creation Code
		 */
		
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(PhysicalInventoryProducts product:getProductsLis()){
			InventoryTransactionLineItem item=new InventoryTransactionLineItem();
			item.setCompanyId(this.getCompanyId());
			item.setBranch(product.getBranch());//Ashwini Patil 14-06-2023
			item.setDocumentType(AppConstants.PHYSICALINVENTORY);
			item.setDocumentId(this.getCount());
			item.setDocumentDate(this.getDate());
			item.setDocumnetTitle("");
			item.setProdId(product.getProductid());
			double varaince=product.getVariance();
			String operations=null;
			if(varaince==0){
				return;
			}else if(varaince>0){
				operations=AppConstants.ADD;
			}else{
				varaince=varaince*(-1);
				operations=AppConstants.SUBTRACT;
			}
			item.setUpdateQty(varaince);
			item.setOperation(operations);
			item.setWarehouseName(product.getWarehousename());
			item.setStorageLocation(product.getStoragelocation());
			item.setStorageBin(product.getStoragebin());
			
			/**
			 * nidhi
			 * 23-08-2018
			 * for map serial number
			 * @author Anil , Date : 30-04-2019
			 */
			HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				
			if(product.getProSerialNoDetails() !=null 
					&&product.getProSerialNoDetails().containsKey(0) 
					&& product.getProSerialNoDetails().get(0)!=null){
				
				prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
				for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
					 ProductSerialNoMapping element = new ProductSerialNoMapping();
					 element.setAvailableStatus(pr.isAvailableStatus());
					 element.setNewAddNo(pr.isNewAddNo());
					 element.setStatus(pr.isStatus());
					 element.setProSerialNo(pr.getProSerialNo());
					 if(!pr.isStatus()){
						 prserdt.get(0).add(element);
					 }
				}
//				for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
//			    {
//					ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
//					if(!pr.isStatus()){
//						itr.remove();
//					}
//			    }
			}
			item.setProSerialNoDetails(prserdt);
			
			itemList.add(item);
		}
		UpdateStock.setProductInventory(itemList);
	}
	/****************************************Overridden Method********************************************/


	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
}
