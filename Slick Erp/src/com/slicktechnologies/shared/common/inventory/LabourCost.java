package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class LabourCost implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5331464935128943186L;

	/******************************************Applicability Attributes****************************************/

	int contractId;
	int serviceId;
	int employeeId;
	String employeeName;
	String employeeCell;
	String serviceHours;
	String empHourSalary;
	double labourCost;
	double labourcostNetAmount;
	String docType;
	int docID;
	int prodId;
	double price;
	double total;
	int custId;
	String custName;
	String custCellNo;
	String poc;
	String branch;
	double contTotal =0;
	String segment="";
	/*******************************************Getters And Setters******************************************/
	
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeCell() {
		return employeeCell;
	}
	public void setEmployeeCell(String employeeCell) {
		this.employeeCell = employeeCell;
	}
	public String getServiceHours() {
		return serviceHours;
	}
	public void setServiceHours(String serviceHours) {
		this.serviceHours = serviceHours;
	}
	public String getEmpHourSalary() {
		return empHourSalary;
	}
	public void setEmpHourSalary(String empHourSalary) {
		this.empHourSalary = empHourSalary;
	}
	public double getLabourCost() {
		return labourCost;
	}
	public void setLabourCost(double labourCost) {
		this.labourCost = labourCost;
	}
	public double getLabourcostNetAmount() {
		return labourcostNetAmount;
	}
	public void setLabourcostNetAmount(double labourcostNetAmount) {
		this.labourcostNetAmount = labourcostNetAmount;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public int getDocID() {
		return docID;
	}
	public void setDocID(int docID) {
		this.docID = docID;
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustCellNo() {
		return custCellNo;
	}
	public void setCustCellNo(String custCellNo) {
		this.custCellNo = custCellNo;
	}
	public String getPoc() {
		return poc;
	}
	public void setPoc(String poc) {
		this.poc = poc;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public double getContTotal() {
		return contTotal;
	}
	public void setContTotal(double contTotal) {
		this.contTotal = contTotal;
	}

}
