package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class SerialNumberDeviation extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -856718687857389869L;

	
	@Index
	protected String warehouseName;
	@Index
	protected String productCode;
	@Index
	protected boolean status;
	@Index
	protected long startSerialNumber;
	@Index
	protected long endSerialNumber;
	@Index
	protected String DocumentType;
	@Index
	protected Date transactionDate;
	
	public SerialNumberDeviation(){
		super();
		warehouseName="";
		productCode="";
		DocumentType="";
	}
	
	
	
	
	public String getWarehouseName() {
		return warehouseName;
	}






	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}






	public String getProductCode() {
		return productCode;
	}






	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}








	public Date getTransactionDate() {
		return transactionDate;
	}




	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}




	public boolean isStatus() {
		return status;
	}






	public void setStatus(boolean status) {
		this.status = status;
	}






	public long getStartSerialNumber() {
		return startSerialNumber;
	}






	public void setStartSerialNumber(long startSerialNumber) {
		this.startSerialNumber = startSerialNumber;
	}






	public long getEndSerialNumber() {
		return endSerialNumber;
	}






	public void setEndSerialNumber(long endSerialNumber) {
		this.endSerialNumber = endSerialNumber;
	}






	public String getDocumentType() {
		return DocumentType;
	}






	public void setDocumentType(String documentType) {
		DocumentType = documentType;
	}






	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
