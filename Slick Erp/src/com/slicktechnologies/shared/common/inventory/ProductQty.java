package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.ProductSerialNoMapping;
@Embed
public class ProductQty  implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8436814073380607477L;
	protected int prodId;
	protected double prodQty;
	
	
	/**
	 * nidhi
	 * 3-10-2018
	 * @return
	 */
	protected HashMap<Integer, ArrayList<ProductSerialNoMapping>> prListSr;
	
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public double getProdQty() {
		return prodQty;
	}
	public void setProdQty(double prodQty) {
		this.prodQty = prodQty;
	}
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getPrListSr() {
		return prListSr;
	}
	public void setPrListSr(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> prListSr) {
		this.prListSr = prListSr;
	}
	
	
}