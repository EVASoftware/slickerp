package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;

@Entity
@Cache
public class WareHouse extends BusinessUnit implements Serializable {

	/**
	 * 
	 */

	/*********************************************** Entity Attributes ****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 982013130256104959L;
	/** The status of this branch can be active or inactive */
	@Index
	protected boolean status;

	@Index
	protected ArrayList<BranchDetails> branchdetails;

	/*********************************************** Join Kind Attribute *****************************************************/
	@Index
	protected Key<BranchRelation> keyName;
	@Ignore
	protected boolean nameChanged = false;

	/**
	 * This fields stores warehouse code and warehouse expiry date Date
	 * 06-10-2016 By Anil Release 30 Sept 2016 Project : Inventory Modification
	 * (NBHC)
	 */

	@Index
	protected String warehouseCode;
	@Index
	protected Date warehouseExpiryDate;
	/**
	 * End
	 */

	/**
	 * This fields are used to maintain the hierarchy between warehouses Date :
	 * 21-10-2016 By ANil Release : 30 Sept 2016 Project : Purchase Modification
	 * (NBHC)
	 */
	@Index
	protected String warehouseType;
	@Index
	protected String parentWarehouse;

	protected boolean isFreez=false;
	
	/**
	 * End
	 */

	/******************************************* Default Ctor ********************************************************/
	/**
	 * Instantiates a new branch.
	 */
	public WareHouse() {
		super();
		nameChanged = false;
		warehouseCode = "";
		warehouseType = "";
		parentWarehouse = "";
	}

	public String getWarehouseType() {
		return warehouseType;
	}

	public void setWarehouseType(String warehouseType) {
		this.warehouseType = warehouseType;
	}

	public String getParentWarehouse() {
		return parentWarehouse;
	}

	public void setParentWarehouse(String parentWarehouse) {
		this.parentWarehouse = parentWarehouse;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public Date getWarehouseExpiryDate() {
		return warehouseExpiryDate;
	}

	public void setWarehouseExpiryDate(Date warehouseExpiryDate) {
		this.warehouseExpiryDate = warehouseExpiryDate;
	}

	public boolean isFreez() {
		return isFreez;
	}

	public void setFreez(boolean isFreez) {
		this.isFreez = isFreez;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getstatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setstatus(boolean status) {
		this.status = status;
	}

	@Override
	public void setBusinessUnitName(String buisnessunitName) {

		if (buisnessunitName != null) {

			if (!(this.buisnessUnitName.equals(buisnessunitName.trim())))
				nameChanged = true;
			this.buisnessUnitName = buisnessunitName;
		}
	}

	public ArrayList<BranchDetails> getBranchdetails() {
		return branchdetails;
	}

	public void setBranchdetails(List<BranchDetails> branchdetails) {
		if (branchdetails != null) {
			this.branchdetails = new ArrayList<BranchDetails>();
			this.branchdetails.addAll(branchdetails);
		}

	}

	/************************************************** Relation Managment Part **********************************************/

	protected Key<LocationRelation> locationKey;

	@OnSave
	@GwtIncompatible
	public void onSave() {
		//
		// if(id==null)
		// {
		// LocationRelation branchRelation=new LocationRelation();
		// branchRelation.setName(buisnessUnitName);
		// branchRelation.setCompanyId(getCompanyId());
		//
		// locationKey=(Key<LocationRelation>)
		// MyUtility.createRelationalKey(branchRelation);
		//
		// }
		// else
		// {
		//
		// if(nameChanged==true)
		// {
		// if(locationKey!=null)
		// {
		// LocationRelation branchRelation=new LocationRelation();
		// branchRelation.setName(buisnessUnitName);
		// branchRelation.setCompanyId(getCompanyId());
		// branchRelation.setStatus(getstatus());
		//
		// MyUtility.updateRelationalEntity(branchRelation,locationKey);
		//
		// }
		// }
		// }
		//

	}

	@Override
	public String toString() {
		return this.buisnessUnitName;
	}

}
