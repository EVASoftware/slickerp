package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class ProductInventoryView extends SuperModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7177854028207924871L;
	@Index
	ProductInfo productinfo;
	
	@Index
	String inspectionRequired;
	@Index
	String inspectionMethod;
	
	
	/**  this flag is used for capturing transactions when available qty gets change  
	 * This is done by Anil on 7/9/2016 **/
	boolean editFlag;
	
//	@Index
	ArrayList<ProductInventoryViewDetails> details;
	
	String comment ; //Added by Ashwini
/**
	 * Date : 27-02-2018 BY ANIL
	 * this flag is used when we are creating stock master by auto process
	 * default value is false 
	 * this is used to avoid extra creation of inventory list and transaction list
	 */
	@Index
	boolean invFlag;
	
	protected boolean isFreez=false;
	
	/*************************************************************************************/
	
	public ProductInventoryView() {
		super();
		productinfo=new ProductInfo();
		inspectionRequired="";
		inspectionMethod="";
		details=new ArrayList<ProductInventoryViewDetails>();
		editFlag=false;
		invFlag=false;
	}
	
	
	/**************************Getters and Setters******************************************/
	
	
	public int getProdID() {
		return productinfo.getProdID();
	}
	public void setProdID(int productID) {
		productinfo.setProdID(productID);
	}

	public String getProductCode() {
		return productinfo.getProductCode();
	}


	public void setProductCode(String productCode) {
		productinfo.setProductCode(productCode);
	}


	public String getProductName() {
		return productinfo.getProductName();
	}

	public void setProductName(String productName) {
		productinfo.setProductName(productName);
	}


	public String getProductCategory() {
		return productinfo.getProductCategory();
	}


	public void setProductCategory(String productCategory) {
		productinfo.setProductCategory(productCategory);
	}
	
	public double getProductPrice() {
		return productinfo.getProductPrice();
	}


	public void setProductPrice(double productPrice) {
		productinfo.setProductPrice(productPrice);
	}

	public String getProductUOM() {
		return productinfo.getUnitofMeasure();
	}


	public void setProductUOM(String productUom) {
		productinfo.setUnitofMeasure(productUom);
	}
	
	
	
	public ArrayList<ProductInventoryViewDetails> getDetails() {
		return details;
	}


	public void setDetails(List<ProductInventoryViewDetails> details) {
		if(details!=null)
			this.details = new ArrayList<ProductInventoryViewDetails>();
			this.details.addAll(details);
	}

	public String getInspectionRequired() {
		return inspectionRequired;
	}


	public void setInspectionRequired(String inspectionRequired) {
		this.inspectionRequired = inspectionRequired;
	}
    
	/*
	 * Date:20-12-2018
	 * @author Ashwini
	 */
	
	public String getComment(){
		return comment;
	}
   
	public void setComment(String comment){
		if(comment!=null){
			this.comment = comment.trim();
		}
	}
	 
	/*
	 * end by Ashwini
	 */
	public boolean isFreez() {
		return isFreez;
	}

	public void setFreez(boolean isFreez) {
		this.isFreez = isFreez;
	}
	public String getInspectionMethod() {
		return inspectionMethod;
	}


	public void setInspectionMethod(String inspectionMethod) {
		this.inspectionMethod = inspectionMethod;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	
	public boolean getEditFlag() {
		return editFlag;
	}


	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}
	
	
	public boolean isInvFlag() {
		return invFlag;
	}


	public void setInvFlag(boolean invFlag) {
		this.invFlag = invFlag;
	}
  
	
	

	@OnSave
	@GwtIncompatible
	public void reactOnSave()
	{
		System.out.println("reactOnSave "+isFreez);
		if(id==null)
		{
			System.out.println("inside on save id is null "+isFreez);
			if(!invFlag){
				saveInventoryList();
				addToTransaction();
			}else{
				setInvFlag(false);
			}
		}
		if(id!=null)
		{
			/**
			 * Date 16/1/2017
			 * This code is only executed if product inventory view is updated.
			 */
			if(editFlag)
			{
				updateInventoryList();
				setEditFlag(false);
			}
			
			System.out.println("inside on save "+isFreez);
			
			
			
		}
		if(!isFreez){
			System.out.println("inside my condition isFreez"+isFreez);
			ItemProduct itemProd=ofy().load().type(ItemProduct.class).filter("companyId", getCompanyId()).filter("count",getCount()).first().now();
			setFreez(true);
			if(itemProd!=null){
				itemProd.setFreez(true);
			}
		  ofy().save().entities(itemProd);
		}
			
		
	}

	
	@GwtIncompatible
	public void saveInventoryList()
	{
		ArrayList<SuperModel> saveArray=new ArrayList<SuperModel>();
		
		for(int i=0;i<this.getDetails().size();i++)
		{
			ProductInventoryViewDetails pivd=new ProductInventoryViewDetails();
			pivd.setProdid(this.getDetails().get(i).getProdid());
			pivd.setCompanyId(getCompanyId());
			pivd.setProdname(this.getDetails().get(i).getProdname());
			pivd.setProdcode(this.getProductCode());
			pivd.setWarehousename(this.getDetails().get(i).getWarehousename());
			pivd.setStoragelocation(this.getDetails().get(i).getStoragelocation());
			pivd.setStoragebin(this.getDetails().get(i).getStoragebin());
			pivd.setMinqty(this.getDetails().get(i).getMinqty());
			pivd.setMaxqty(this.getDetails().get(i).getMaxqty());
			pivd.setNormallevel(this.getDetails().get(i).getNormallevel());
			pivd.setReorderlevel(this.getDetails().get(i).getReorderlevel());
			pivd.setReorderqty(this.getDetails().get(i).getReorderqty());
			pivd.setAvailableqty(this.getDetails().get(i).getAvailableqty());
			pivd.setQty(this.getDetails().get(i).getQty());
			pivd.setTotalwarehouseQty(this.getDetails().get(i).getTotalwarehouseQty());
			pivd.setProdInvViewCount(this.getCount());
			/**Date 19-8-2019 by Amol Set Category**/
			pivd.setProductCategory(this.getProductCategory());
			
			saveArray.add(pivd);
		}
		
		if(saveArray.size()!=0)
		{
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(saveArray);
		}
	}
	
	
	
	
	/**
	 * this code is updated/corrected by Anil On 26-08-2016(For NBHC INV MGMT) 
	 */
	@GwtIncompatible
	private void updateInventoryList()
	{

		List<ProductInventoryViewDetails> prodInvDetList=new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		List<ProductInventoryViewDetails> prodInvDetailsList=ofy().load().type(ProductInventoryViewDetails.class).filter("prodInvViewCount", this.getCount()).filter("companyId", getCompanyId()).list();
		System.out.println("INTIAL PROD INV VIEW DET LIST SIZE : "+prodInvDetailsList.size());
		
		for(ProductInventoryViewDetails prodInvDet:getDetails()){
			
			ProductInventoryViewDetails prodInvDetails=getProdInvViewDetails(prodInvDetailsList,prodInvDet.getWarehousename(),prodInvDet.getStoragelocation(),prodInvDet.getStoragebin());
			
			System.out.println();
			System.out.println("PROD INV LIS SIZE :: "+prodInvDetailsList.size());
			System.out.println();
			
			
			if(prodInvDetails!=null){
				
				boolean updateFlag=false;
				
				if(prodInvDet.getAvailableqty()!=prodInvDetails.getAvailableqty()){
					updateFlag=true;
					double openingStock=0;
					double closingStock=0;
					double updatedQty=0;
					String operation="";
					
					openingStock=prodInvDetails.getAvailableqty();
					closingStock=prodInvDet.getAvailableqty();
					
					if(closingStock>openingStock){
						updatedQty=closingStock-openingStock;
						operation=AppConstants.ADD;
					}else{
						updatedQty=openingStock-closingStock;
						operation=AppConstants.SUBTRACT;
					}
					
					/**
					 * Date : 18-01-2017 By Anil
					 */
					ProductInventoryTransaction pit=UpdateStock.getProductInventoryTransaction(prodInvDetails.getProdid(), prodInvDetails.getProdname()
							,updatedQty,this.getProductUOM(), this.getProductPrice(), prodInvDetails.getWarehousename(), prodInvDetails.getStoragelocation(), prodInvDetails.getStoragebin(), "Product Inventory View", this.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date())
							,operation, openingStock, closingStock, getCompanyId(),"","");
					
					transactionList.add(pit);
					/**
					 * End
					 */
					
//					if(editFlag){
//					UpdateStock.setProductInventoryTransaction(prodInvDetails.getProdid(), prodInvDetails.getProdname()
//							,updatedQty,this.getProductUOM(), this.getProductPrice(), prodInvDetails.getWarehousename(), prodInvDetails.getStoragelocation(), prodInvDetails.getStoragebin(), "Product Inventory View", this.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date())
//							,operation, openingStock, closingStock, getCompanyId());
//					}
					
					prodInvDetails.setAvailableqty(closingStock);
					
				}
				if(prodInvDet.getMinqty()!=prodInvDetails.getMinqty()){
					updateFlag=true;
					prodInvDetails.setMinqty(prodInvDet.getMinqty());
				}
				if(prodInvDet.getMaxqty()!=prodInvDetails.getMaxqty()){
					updateFlag=true;
					prodInvDetails.setMaxqty(prodInvDet.getMaxqty());
				}
				if(prodInvDet.getNormallevel()!=prodInvDetails.getNormallevel()){
					updateFlag=true;
					prodInvDetails.setNormallevel(prodInvDet.getNormallevel());
				}
				if(prodInvDet.getReorderlevel()!=prodInvDetails.getReorderlevel()){
					updateFlag=true;
					prodInvDetails.setReorderlevel(prodInvDet.getReorderlevel());
				}
				if(prodInvDet.getReorderqty()!=prodInvDetails.getReorderqty()){
					updateFlag=true;
					prodInvDetails.setReorderqty(prodInvDet.getReorderqty());
				}
				
				/**Date 19-8-2019 set Product Category By Amol**/
				if(prodInvDetails.getProductCategory()==null||prodInvDetails.getProductCategory().equals("")){
					updateFlag=true;
					prodInvDetails.setProductCategory(prodInvDet.getProductCategory());
				}
				
				/**
				 * @author Anil @since 21-01-2021
				 * updating in use stock
				 */
				if(prodInvDet.getInUseStock()!=null){
					
					if(prodInvDetails.getInUseStock()==null){
						updateFlag=true;
						prodInvDetails.setInUseStock(prodInvDet.getInUseStock());
					}else{
						if(!prodInvDet.getInUseStock().equals(prodInvDetails.getInUseStock())){
							updateFlag=true;
							prodInvDetails.setInUseStock(prodInvDet.getInUseStock());
						}
					}
				}
				
				if(updateFlag){
//					ofy().save().entity(prodInvDetails).now();
					/**
					 * Date : 18-01-2017 By Anil
					 */
					prodInvDetList.add(prodInvDetails);
				}
				
				
				
			}else{
				
				
				/**
				 * if we enter stock of product for new warehouse/location/bin 
				 * it creates product inventory view details and transaction
				 */
				prodInvDet.setCompanyId(this.getCompanyId());
				prodInvDet.setProdInvViewCount(this.getCount());
				prodInvDet.setProdcode(this.getProductCode());
				prodInvDet.setProductCategory(this.getProductCategory());
//				ofy().save().entity(prodInvDet).now();
//				if(editFlag){
//				UpdateStock.setProductInventoryTransaction(prodInvDet.getProdid(), prodInvDet.getProdname(),prodInvDet.getAvailableqty(),
//						this.getProductUOM(), this.getProductPrice(), prodInvDet.getWarehousename(), prodInvDet.getStoragelocation(), 
//						prodInvDet.getStoragebin(), "Product Inventory View", this.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date()),AppConstants.ADD, 0, prodInvDet.getAvailableqty(), getCompanyId());
//				}
				
				/**
				 * Date : 18-01-2017 By Anil
				 */
				prodInvDetList.add(prodInvDet);
				
				/**
				 * Date : 18-01-2017 By Anil
				 */
				ProductInventoryTransaction pit=UpdateStock.getProductInventoryTransaction(prodInvDet.getProdid(), prodInvDet.getProdname(),prodInvDet.getAvailableqty(),
						this.getProductUOM(), this.getProductPrice(), prodInvDet.getWarehousename(), prodInvDet.getStoragelocation(), 
						prodInvDet.getStoragebin(), "Product Inventory View", this.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date()),AppConstants.ADD, 0, prodInvDet.getAvailableqty(), getCompanyId(),"","");
				
				transactionList.add(pit);
				
			}
		}
		
		/**
		 * Date : 18-01-2017 By Anil
		 */
		ArrayList<SuperModel> transactionModelList=new ArrayList<SuperModel>();
		ArrayList<SuperModel> pivdModelList=new ArrayList<SuperModel>();
		
		for(ProductInventoryTransaction pit:transactionList){
			SuperModel model=pit;
			transactionModelList.add(model);
		}
		for(ProductInventoryViewDetails pivd:prodInvDetList){
			SuperModel model=pivd;
			pivdModelList.add(model);
		}
		
		GenricServiceImpl impl=new GenricServiceImpl();
		if(pivdModelList.size()!=0){
			impl.save(pivdModelList);
		}
		if(transactionModelList.size()!=0){
			impl.save(transactionModelList);
		}
		
		/**
		 * End
		 */
		
		System.out.println("AFTER ALL PROD INV VIEW DET LIST SIZE :: "+prodInvDetailsList.size());
		if(prodInvDetailsList.size()!=0){
			ofy().delete().entities(prodInvDetailsList).now();
		}
	}
	
	
	/**
	 *  This method is used for adding data to transaction when product is added to inventory.
	 *  This method is called when product is added to inventory for first time entry.
	 */
	
	@GwtIncompatible
	public void addToTransaction()
	{
		String unitOfMeasurement="";
		if(this.getProductUOM()!=null){
			unitOfMeasurement=this.getProductUOM();
		}
		for(int i=0;i<this.getDetails().size();i++)
		{
			UpdateStock.setProductInventoryTransaction(this.getDetails().get(i).getProdid(), this.getDetails().get(i).getProdname(), this.getDetails().get(i).getAvailableqty(), unitOfMeasurement, this.getProductPrice(), this.getDetails().get(i).getWarehousename(), this.getDetails().get(i).getStoragelocation(), this.getDetails().get(i).getStoragebin(), "Product Inventory View", this.getCount(), "", DateUtility.getDateWithTimeZone("IST", new Date()),AppConstants.ADD, 0, this.getDetails().get(i).getAvailableqty(), getCompanyId(),"","");
		}
	}
	
	
	@GwtIncompatible
	public ProductInventoryViewDetails getProdInvViewDetails(List<ProductInventoryViewDetails> prodInvDetailsList,
			String warehousename, String storagelocation, String storagebin) {
		ProductInventoryViewDetails prodInvDet=null;
		if(prodInvDetailsList.size()!=0){
			for(int i=0;i<prodInvDetailsList.size();i++){
				if(prodInvDetailsList.get(i).getWarehousename().trim().equals(warehousename.trim())&&
						prodInvDetailsList.get(i).getStoragelocation().trim().equals(storagelocation.trim())&&
						prodInvDetailsList.get(i).getStoragebin().trim().equals(storagebin.trim())){
					
					prodInvDet=prodInvDetailsList.get(i);
					prodInvDetailsList.remove(i);
					
					return prodInvDet;
				}
				
			}
		}
		
		return null;
	}
	
 }
