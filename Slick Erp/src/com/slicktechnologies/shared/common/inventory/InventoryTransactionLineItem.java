package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.ProductSerialNoMapping;

public class InventoryTransactionLineItem {

	Long companyId;
	String documentType;
	int documentId;
	Date documentDate;
	String documnetTitle;
	String branch;
	String remark;
	
	int prodId;
	double updateQty;
	String operation;
	String warehouseName;
	String storageLocation;
	String storageBin;
	String user;
	
	String productName;
	String productUOM;
	double productPrice;
	
	double openingStock;
	double closingStock;
	
	/**
	 * Date : 01-03-2017 BY ANil
	 * added sub type for mmn transaction
	 */
	String documentSubType;
	
	/**
	 * Date : 02-03-2017 By Anil
	 * Added product Code
	 */
	String productCode;
	
	/** date 24/11/2017 added by komal for min and mmn number generation **/
	int count;
	/**
	 * nidhi 21-08-2018 for map serial number details
	 */
		@EmbedMap
	  @Serialize
	  protected HashMap<Integer, ArrayList<ProductSerialNoMapping>>  proSerialNoDetails = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
	  /** date 30.11.2018 added by komal for product category **/
	  protected String productCategory;
	  
	/**
	 * nidhi ||*||
	 * @return
	 */
	  double plannedQty = 0;
	  
	  /**
	   * Date 29-11-2019
	   * Des :- NBHC Inventory Management Lock Seal Serial Number
	   */
	  protected String serialNumber;
	  
	  protected String serviceId; //Ashwini Patil Date:28-06-2024 added for Rex
	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public String getDocumentSubType() {
		return documentSubType;
	}


	public void setDocumentSubType(String documentSubType) {
		this.documentSubType = documentSubType;
	}
	
	
	public InventoryTransactionLineItem() {
		// TODO Auto-generated constructor stub
		productCategory = "";
	}


	public Long getCompanyId() {
		return companyId;
	}


	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}


	public String getDocumentType() {
		return documentType;
	}


	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}


	public int getDocumentId() {
		return documentId;
	}


	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}


	public Date getDocumentDate() {
		return documentDate;
	}


	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}


	public String getDocumnetTitle() {
		return documnetTitle;
	}


	public void setDocumnetTitle(String documnetTitle) {
		this.documnetTitle = documnetTitle;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public int getProdId() {
		return prodId;
	}


	public void setProdId(int prodId) {
		this.prodId = prodId;
	}


	public double getUpdateQty() {
		return updateQty;
	}


	public void setUpdateQty(double updateQty) {
		this.updateQty = updateQty;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public String getWarehouseName() {
		return warehouseName;
	}


	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}


	public String getStorageLocation() {
		return storageLocation;
	}


	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}


	public String getStorageBin() {
		return storageBin;
	}


	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductUOM() {
		return productUOM;
	}


	public void setProductUOM(String productUOM) {
		this.productUOM = productUOM;
	}


	public double getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}


	public double getOpeningStock() {
		return openingStock;
	}


	public void setOpeningStock(double openingStock) {
		this.openingStock = openingStock;
	}


	public double getClosingStock() {
		return closingStock;
	}


	public void setClosingStock(double closingStock) {
		this.closingStock = closingStock;
	}


	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}
	
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}


	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}


	public String getProductCategory() {
		return productCategory;
	}


	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}


	public double getPlannedQty() {
		return plannedQty;
	}


	public void setPlannedQty(double plannedQty) {
		this.plannedQty = plannedQty;
	}


	public String getSerialNumber() {
		return serialNumber;
	}


	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}


	public String getServiceId() {
		return serviceId;
	}


	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}



	
}
