package com.slicktechnologies.shared.common.inventory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.text.StrTokenizer;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductSerialNoMapping;

import sun.print.resources.serviceui;
import sun.security.jca.ServiceId;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class UpdateStock {

//	static double requestedProductQty;
//	static double openingStock;
//	static double closingStock=0;
	
	public Logger logger = Logger.getLogger("UpdateStock Process");
	
	@GwtIncompatible
	public static void setProductInventory(int productId,double productQty,int docId,Date docDate,String docType,String docTitle,
											String warehouseName,String storageLocation,String storageBin,
											String operation,long compId,String branch,String serviceId){
		
		DecimalFormat df=new DecimalFormat("0.0000");
		double requestedProductQty=0;
		double openingStock=0;
		double closingStock=0;
		
		final String warehouse=warehouseName;
		final String storLoc=storageLocation;
		final String storBin=storageBin;
		
		requestedProductQty=productQty;
		
		ProductInventoryView prodInvView=ofy().load().type(ProductInventoryView.class).filter("companyId", compId).filter("productinfo.prodID", productId).first().now();
		prodInvView.setEditFlag(false);
		
		ArrayList<ProductInventoryViewDetails> inventoryProductList=new ArrayList<ProductInventoryViewDetails>();
		inventoryProductList=prodInvView.getDetails();
				
			if(!inventoryProductList.isEmpty()&&inventoryProductList!=null){
				for(int i=0;i<inventoryProductList.size();i++){
					
					if(inventoryProductList.get(i).getWarehousename().equals(warehouse)&&
						inventoryProductList.get(i).getStoragelocation().equals(storLoc)&&
						inventoryProductList.get(i).getStoragebin().equals(storBin)){
						
						openingStock=inventoryProductList.get(i).getAvailableqty();
							
						System.out.println("Opening Stock :: "+openingStock);
						
						if(operation.equals("Add")){
							closingStock=openingStock+requestedProductQty;
							
							inventoryProductList.get(i).setAvailableqty(Double.parseDouble(df.format(closingStock)));
							prodInvView.setDetails(inventoryProductList);
	//							GenricServiceImpl impl = new GenricServiceImpl();
	//							impl.save(prodInvView);
							
						}
						if(operation.equals("Subtract")){
							closingStock=openingStock-requestedProductQty;
							inventoryProductList.get(i).setAvailableqty(Double.parseDouble(df.format(closingStock)));
							prodInvView.setDetails(inventoryProductList);
	//							GenricServiceImpl impl = new GenricServiceImpl();
	//							impl.save(prodInvView);
						}
						
						/**
						 * date : 16/1/2017
						 * This code update the stock in inventory list entity 
						 */
						
						ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class).filter("prodInvViewCount",inventoryProductList.get(i).getProdid()).filter("warehousename", warehouse.trim()).filter("storagelocation", storLoc.trim()).filter("storagebin", storBin.trim()).filter("companyId", prodInvView.getCompanyId()).first().now();
						
						if(prodInvDtls!=null){
							prodInvDtls.setAvailableqty(Double.parseDouble(df.format(closingStock)));
	//							GenricServiceImpl impl=new GenricServiceImpl();
	//							impl.save(prodInvDtls);
							ofy().save().entity(prodInvView).now();
							ofy().save().entity(prodInvDtls).now();
						}
						
						
						String productName=prodInvView.getProductName();
						String productUOM=prodInvView.getProductUOM();
						double productPrice=prodInvView.getProductPrice();
								
						setProductInventoryTransaction(productId, productName, productQty, productUOM, productPrice,
								warehouseName, storageLocation, storageBin, docType, docId, docTitle, docDate,
								operation, openingStock,Double.parseDouble(df.format(closingStock)),compId,branch,serviceId);
						
						break;
						
					}
				}
			}
				
		}
	
	
	public static void setProductInventoryTransaction(int productId,String productName,double productQty,String productUOM,
			double productPrice,String warehouseName,String storageLocation,String storageBin,
			String documentType,int documentId,String documentTitle,Date documentDate,
			String operation,double openingStock,double closingStock,long compId,String branch,String serviceId){
			
			System.out.println("Inside Transaction.....");
			ProductInventoryTransaction prodInvTran=new ProductInventoryTransaction();
			
			prodInvTran.setProductId(productId);
			prodInvTran.setProductName(productName);
			prodInvTran.setProductPrice(productPrice);
			prodInvTran.setProductQty(productQty);
			prodInvTran.setProductUOM(productUOM);
			prodInvTran.setWarehouseName(warehouseName);
			prodInvTran.setStorageLocation(storageLocation);
			prodInvTran.setStorageBin(storageBin);
			prodInvTran.setDocumentType(documentType);
			prodInvTran.setDocumentId(documentId);
			prodInvTran.setDocumentTitle(documentTitle);
			prodInvTran.setDocumentDate(documentDate);
			prodInvTran.setOperation(operation);
			prodInvTran.setOpeningStock(openingStock);
			prodInvTran.setClosingStock(closingStock);
			prodInvTran.setCompanyId(compId);
			prodInvTran.setBranch(branch);
			prodInvTran.setInventoryTransactionDateTime(new Date());
			if(serviceId!=null&&!serviceId.equals(""))
				prodInvTran.setServiceId(serviceId);
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(prodInvTran);
	}
	
	/**
	 * Date : 18-01-2017 By Anil
	 * Project: NBHC CCPM
	 * This method is used to update stock in ProductInventoryView and  ProductInventoryViewDetails .
	 * also creates transaction for given updation.
	 * 
	 */
	
	@GwtIncompatible
	public static void setProductInventory(ArrayList<InventoryTransactionLineItem> itemList){/*
		
		DecimalFormat df=new DecimalFormat("0.00");
		HashSet<Integer> hset=new HashSet<Integer>();
		for(InventoryTransactionLineItem obj:itemList){
			hset.add(obj.getProdId());
		}
		List<Integer> prodIdList=new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList=ofy().load().type(ProductInventoryView.class).filter("companyId", itemList.get(0).getCompanyId()).filter("productinfo.prodID IN", prodIdList).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		List<ProductInventoryViewDetails> prodList=new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		
		for(ProductInventoryView piv:prodInvViewList){
			for(InventoryTransactionLineItem item:itemList){
				if(piv.getProdID()==item.getProdId()){
					piv.setEditFlag(false);
					double requestedProductQty=0;
					double openingStock=0;
					double closingStock=0;
					for(ProductInventoryViewDetails prod:piv.getDetails()){
						if(prod.getWarehousename().equals(item.getWarehouseName())
								&&prod.getStoragelocation().equals(item.getStorageLocation())
								&&prod.getStoragebin().equals(item.getStorageBin())){
							requestedProductQty=item.getUpdateQty();
							openingStock=prod.getAvailableqty();
							if(item.getOperation().equals("Add")){
								closingStock=openingStock+requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								
								*//**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 *//*
								if(!prod.getProSerialNoDetails().containsKey(0)){
									prod.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
								}
								if(item.getProSerialNoDetails().size()>0){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									
									proSerList = item.getProSerialNoDetails().get(0);
									
									if(proSerList.size()>0){
										for(ProductSerialNoMapping proSer : proSerList){
											if(proSer.getProSerialNo().trim().length()>0){
												proSer.setStatus(false);
												proSer.setNewAddNo(false);
												proSer.setAvailableStatus(true);
												prod.getProSerialNoDetails().get(0).add(proSer);
											}
										}
									}
								}
								*//**
								 * end
								 *//*
							}
							if(item.getOperation().equals("Subtract")){
								closingStock=openingStock-requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								*//**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 *//*
								if(prod.getProSerialNoDetails().size() >0 && prod.getProSerialNoDetails().containsKey(0)){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									proSerList.addAll(prod.getProSerialNoDetails().get(0));
									System.out.println("get new number == " + item.getProSerialNoDetails().keySet());
									System.out.println("get status-- " +item.getProSerialNoDetails().containsKey(0));
									if(item.getProSerialNoDetails().containsKey(0) && item.getProSerialNoDetails().get(0).size()> 0){
										ArrayList<ProductSerialNoMapping> proInvList = new ArrayList<ProductSerialNoMapping>();
										for(ProductSerialNoMapping itr : item.getProSerialNoDetails().get(0))
									      {
											 ProductSerialNoMapping element = new ProductSerialNoMapping();
											 element.setAvailableStatus(itr.isAvailableStatus());
											 element.setNewAddNo(itr.isNewAddNo());
											 element.setStatus(itr.isStatus());
											 element.setProSerialNo(itr.getProSerialNo());
											 
											 proInvList.add(element);
											 
									      }
									 for(Iterator<ProductSerialNoMapping> itr = proInvList.iterator();itr.hasNext();)
								      {
										 boolean get = false;
										 ProductSerialNoMapping element21 = itr.next();
										 
										 for(Iterator<ProductSerialNoMapping> itr1 = prod.getProSerialNoDetails().get(0).iterator() ; itr1.hasNext();){
											 ProductSerialNoMapping element1 = itr1.next();
											 if(element21.getProSerialNo().equals(element1.getProSerialNo())){
												 itr1.remove();
												 itr.remove();
												 get =true;
												break;
											 }
									           
									       }
										 
										 if(!get && (element21.isNewAddNo() || !element21.isAvailableStatus())){
											 itr.remove();
											 System.out.println("get new number == " + element21.getProSerialNo());
											 continue;
										 }
									}
								}
								}
								*//**
								 * end
								 *//*
								
								
								try {
									
									
									validateStock(closingStock);
								} catch (InvalidTransactionException e) {
									e.printStackTrace();
									prod.setWarehousename(null);
								}
							}
							
							ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class)
									.filter("companyId", piv.getCompanyId())
									.filter("prodInvViewCount",prod.getProdid())
									.filter("warehousename", prod.getWarehousename().trim())
									.filter("storagelocation", prod.getStoragelocation().trim())
									.filter("storagebin", prod.getStoragebin().trim()).first().now();
							if(prodInvDtls!=null){
								prodInvDtls.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								prodList.add(prodInvDtls);
							}
							item.setOpeningStock(openingStock);
							item.setClosingStock(Double.parseDouble(df.format(closingStock)));
							item.setProductName(piv.getProductName());
							item.setProductPrice(piv.getProductPrice());
							item.setProductUOM(piv.getProductUOM());
							
							*//**
							 * Date : 02-03-2017 By Anil
							 * setting product code
							 *//*
							item.setProductCode(piv.getProductCode());
							
							ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item);
							transactionList.add(prodInvTran);
							break;
						}
					}
					break;
				}
			}
		}
		ArrayList<SuperModel>modelList=new ArrayList<SuperModel>();
		for(ProductInventoryTransaction obj:transactionList){
			SuperModel model=obj;
			modelList.add(model);
		}
		*//**
		 * Date 08-09-2018 By Vijay
		 * Des :- Bug transfer in if product inventory view does not defined then getting exception so added if conditon
		 *//*
		if(modelList.size()!=0){
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(modelList);
		}
		
		*//**
		 * Date : 21-11-2017 BY ANIL
		 *//*
		
//		ofy().save().entities(prodInvViewList);
//		ofy().save().entities(prodList);
		
		for(ProductInventoryView piv:prodInvViewList){
			ofy().save().entity(piv).now();
		}
		for(ProductInventoryViewDetails pivd:prodList){
			ofy().save().entity(pivd).now();
		}
		*//**
		 * End
		 *//*
		
	*/
		
		Logger logger1 = Logger.getLogger("UpdateStock Process");
		try {
		DecimalFormat df=new DecimalFormat("0.0000");
		HashSet<Integer> hset=new HashSet<Integer>();
		for(InventoryTransactionLineItem obj:itemList){
			hset.add(obj.getProdId());
		}
		List<Integer> prodIdList=new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList=ofy().load().type(ProductInventoryView.class).filter("companyId", itemList.get(0).getCompanyId()).filter("productinfo.prodID IN", prodIdList).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		List<ProductInventoryViewDetails> prodList=new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		
		for(ProductInventoryView piv:prodInvViewList){
			for(InventoryTransactionLineItem item:itemList){
				if(piv.getProdID()==item.getProdId()){
					piv.setEditFlag(false);
					double requestedProductQty=0;
					double openingStock=0;
					double closingStock=0;
					/**
					 * @author Anil @since 21-01-2021
					 */
					Double inUseStock=null;
					
					for(ProductInventoryViewDetails prod:piv.getDetails()){
						if(prod.getWarehousename()!=null && prod.getWarehousename().equals(item.getWarehouseName())
								&&prod.getStoragelocation().equals(item.getStorageLocation())
								&&prod.getStoragebin().equals(item.getStorageBin())){
							requestedProductQty=item.getUpdateQty();
							openingStock=prod.getAvailableqty();
							if(item.getOperation().equals("Add")){
								closingStock=openingStock+requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * @author Anil , Date : 02-05-2019
								 * while updating serial number for other transaction then we are adding serial number to existing but for physical inventory it should be cleared and added again
								 */
								if(!prod.getProSerialNoDetails().containsKey(0)||item.getDocumentType().equals(AppConstants.PHYSICALINVENTORY)){
									prod.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
								}
								if(item.getProSerialNoDetails()!=null
										&& item.getProSerialNoDetails().size()>0){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									
									proSerList = item.getProSerialNoDetails().get(0);
									
									if(proSerList.size()>0){
										for(ProductSerialNoMapping proSer : proSerList){
											if(proSer.getProSerialNo().trim().length()>0){
												proSer.setStatus(false);
												proSer.setNewAddNo(false);
												proSer.setAvailableStatus(true);
												prod.getProSerialNoDetails().get(0).add(proSer);
											}
										}
									}
								}
								/**
								 * end
								 */
								
								/**
								 * Date 13-Dec-2018
								 * @author Vijay Chougule
								 * Requirements :- NBHC Inventory Management 
								 * Des :- based on available Qty Order Qty and Expected Qty calculation for Automatic PR 
								 * Generation with process config 
								 */
								System.out.println("Condition "+(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())));

								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())){
									 prod.setOrderQty(getOrderQty(prod.getAvailableqty(),prod.getForecastDemand(),
											 prod.getConsumptiontillStockReceived(),prod.getReorderlevel(),
											 prod.getMinOrderQty(),prod.getSafetyStockLevel()));
									 prod.setExpectedStock(getExpectedStock(prod.getAvailableqty(), prod.getOrderQty(), prod.getConsumptiontillStockReceived()));
									 System.out.println("expected stock =="+prod.getExpectedStock());
								 }
								/**
								 * ends here
								 */
								/**
								 * Date 26-06-2019 By Vijay
								 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
								 *  with process config   
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", piv.getCompanyId())){
									 updateSerialNumberMasterForStockIN(prod,item);
								 }
								 /**
								  * ends here
								  */
								 
								 /**
								  * @author Anil @since 21-01-2021
								  * Updating in use stock
								  */
								 if(prod.getInUseStock()!=null){
									 inUseStock=prod.getInUseStock()+requestedProductQty;
									 prod.setInUseStock(inUseStock);
								 }
								 
							}
							if(item.getOperation().equals("Subtract")){
								closingStock=openingStock-requestedProductQty;
								
								
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 */
								if(prod.getProSerialNoDetails()!=null &&
										prod.getProSerialNoDetails().size() >0 && prod.getProSerialNoDetails().containsKey(0)){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									proSerList.addAll(prod.getProSerialNoDetails().get(0));
									System.out.println("get new number == " + item.getProSerialNoDetails().keySet());
									System.out.println("get status-- " +item.getProSerialNoDetails().containsKey(0));
									if(item.getProSerialNoDetails().containsKey(0) && item.getProSerialNoDetails().get(0).size()> 0){
										
										if(item.getDocumentType().equals(AppConstants.PHYSICALINVENTORY)){
											prod.setProSerialNoDetails(item.getProSerialNoDetails());
										}else{
										
										ArrayList<ProductSerialNoMapping> proInvList = new ArrayList<ProductSerialNoMapping>();
										for(ProductSerialNoMapping itr : item.getProSerialNoDetails().get(0))
									      {
											 ProductSerialNoMapping element = new ProductSerialNoMapping();
											 element.setAvailableStatus(itr.isAvailableStatus());
											 element.setNewAddNo(itr.isNewAddNo());
											 element.setStatus(itr.isStatus());
											 element.setProSerialNo(itr.getProSerialNo());
											 
											 proInvList.add(element);
											 
									      }
									 for(Iterator<ProductSerialNoMapping> itr = proInvList.iterator();itr.hasNext();)
								      {
										 boolean get = false;
										 ProductSerialNoMapping element21 = itr.next();
										 
										 for(Iterator<ProductSerialNoMapping> itr1 = prod.getProSerialNoDetails().get(0).iterator() ; itr1.hasNext();){
											 ProductSerialNoMapping element1 = itr1.next();
											 if(element21.getProSerialNo().equals(element1.getProSerialNo())){
												 itr1.remove();
												 itr.remove();
												 get =true;
												break;
											 }
									           
									       }
										 
										 if(!get && (element21.isNewAddNo() || !element21.isAvailableStatus())){
											 itr.remove();
											 System.out.println("get new number == " + element21.getProSerialNo());
											 continue;
										 }
									}
									}
								}
								}
								/**
								 * end
								 */
								
								
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								
								/**
								 * Date 13-Dec-2018
								 * @author Vijay Chougule
								 * Requirements :- NBHC Inventory Management 
								 * Des :- based on available Qty Order Qty and Expected Qty calculation for Automatic PR 
								 * Generation with process config 
								 */
								System.out.println("Condition "+(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())));

								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())){
									 prod.setOrderQty(getOrderQty(prod.getAvailableqty(),prod.getForecastDemand(),
											 prod.getConsumptiontillStockReceived(),prod.getReorderlevel(),
											 prod.getMinOrderQty(),prod.getSafetyStockLevel()));
									 prod.setExpectedStock(getExpectedStock(prod.getAvailableqty(), prod.getOrderQty(), prod.getConsumptiontillStockReceived()));
									 System.out.println("expected stock =="+prod.getExpectedStock());
								 }
								/**
								 * ends here
								 */
								 
							 	/**
								 * Date 26-06-2019 By Vijay
								 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
								 *  with process config   
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", piv.getCompanyId())){
									 updateSerialNumberMasterForStockOUT(prod,item);
								 }
								 /**
								  * ends here
								  */
								 
								try {
									validateStock(closingStock);
								} catch (InvalidTransactionException e) {
									e.printStackTrace();
									prod.setWarehousename(null);
									/**
									 * @author Anil , Date : 06-03-2020
									 * When stock goes negative we are setting warehouse name to null
									 * which throws exception
									 */
									return;
								}
								
								 /**
								  * @author Anil @since 21-01-2021
								  * Updating in use stock
								  */
								 if(prod.getInUseStock()!=null){
									 inUseStock=prod.getInUseStock()-requestedProductQty;
									 if(inUseStock<0){
										 inUseStock=0.0;
									 }
									 prod.setInUseStock(inUseStock);
								 }
							}
							
							ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class)
									.filter("companyId", piv.getCompanyId())
									.filter("prodInvViewCount",prod.getProdid())
									.filter("warehousename", prod.getWarehousename().trim())
									.filter("storagelocation", prod.getStoragelocation().trim())
									.filter("storagebin", prod.getStoragebin().trim()).first().now();
							if(prodInvDtls!=null){
								prodInvDtls.setAvailableqty(Double.parseDouble(df.format(closingStock)));
							
								
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 */
								if(!prodInvDtls.getProSerialNoDetails().containsKey(0)){
									prodInvDtls.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
								}
								if(item.getProSerialNoDetails().size()>0){
									ArrayList<ProductSerialNoMapping> proSerList1 = new ArrayList<ProductSerialNoMapping>();
									
									proSerList1 = item.getProSerialNoDetails().get(0);
									
									if(proSerList1.size()>0){
										for(ProductSerialNoMapping proSer : proSerList1){
											if(proSer.getProSerialNo().trim().length()>0){
												prodInvDtls.getProSerialNoDetails().get(0).add(proSer);
											}
										}
									}
								}
								/**
								 * end
								 */
								
								 /**
								  * @author Anil @since 21-01-2021
								  * Updating in use stock
								  */
								prodInvDtls.setInUseStock(inUseStock);
								
								
								prodList.add(prodInvDtls);
							}
							item.setOpeningStock(openingStock);
							item.setClosingStock(Double.parseDouble(df.format(closingStock)));
							item.setProductName(piv.getProductName());
							item.setProductPrice(piv.getProductPrice());
							item.setProductUOM(piv.getProductUOM());
							
							/**
							 * Date : 02-03-2017 By Anil
							 * setting product code
							 */
							item.setProductCode(piv.getProductCode());
							
							ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item);
							transactionList.add(prodInvTran);
							break;
						}
					}
					logger1.log(Level.SEVERE,"Get pro ID -- " + item.getProdId() + " ware house -- " + item.getWarehouseName());
//					break;
				}
			}
		}
		ArrayList<SuperModel>modelList=new ArrayList<SuperModel>();
		for(ProductInventoryTransaction obj:transactionList){
			SuperModel model=obj;
			modelList.add(model);
		}
		/**
		 * Date 08-09-2018 By Vijay
		 * Des :- Bug transfer in if product inventory view does not defined then getting exception so added if conditon
		 */
		if(modelList.size()!=0){
			for(int j=0;j<modelList.size();j++){
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(modelList.get(j));
			}
		
		}
		
		/**
		 * Date : 21-11-2017 BY ANIL
		 */
		
//		ofy().save().entities(prodInvViewList);
//		ofy().save().entities(prodList);
		
		for(ProductInventoryView piv:prodInvViewList){
			ofy().save().entity(piv).now();
		}
		for(ProductInventoryViewDetails pivd:prodList){
			ofy().save().entity(pivd).now();
		}
		/**
		 * End
		 * 
		 */
		
	}catch(Exception e) {
			e.printStackTrace();
		}
		logger1.log(Level.SEVERE, "update stock completed.");
	}
	


	


	public static ProductInventoryTransaction getProductInventoryTransaction(InventoryTransactionLineItem item){
		ProductInventoryTransaction prodInvTran=new ProductInventoryTransaction();
		
		prodInvTran.setProductId(item.getProdId());
		prodInvTran.setProductName(item.getProductName());
		prodInvTran.setProductPrice(item.getProductPrice());
		prodInvTran.setProductQty(item.getUpdateQty());
		prodInvTran.setProductUOM(item.getProductUOM());
		prodInvTran.setWarehouseName(item.getWarehouseName());
		prodInvTran.setStorageLocation(item.getStorageLocation());
		prodInvTran.setStorageBin(item.getStorageBin());
		prodInvTran.setDocumentType(item.getDocumentType());
		prodInvTran.setDocumentId(item.getDocumentId());
		prodInvTran.setDocumentTitle(item.getDocumnetTitle());
		prodInvTran.setDocumentDate(item.getDocumentDate());
		prodInvTran.setOperation(item.getOperation());
		prodInvTran.setOpeningStock(item.getOpeningStock());
		prodInvTran.setClosingStock(item.getClosingStock());
		prodInvTran.setCompanyId(item.getCompanyId());
		prodInvTran.setBranch(item.getBranch());
		prodInvTran.setInventoryTransactionDateTime(new Date());
		if(item.getServiceId()!=null&&!item.getServiceId().equals(""))
			prodInvTran.setServiceId(item.getServiceId());
		
		/**
		 * Date :01-03-2017 By ANIL
		 */
		if(item.getDocumentSubType()!=null){
			prodInvTran.setDocumentSubType(item.getDocumentSubType());
		}
		
		/**
		 * Date : 02-03-2017 By ANIL
		 */
		
		if(item.getProductCode()!=null){
			prodInvTran.setProductCode(item.getProductCode());
		}
		/**
		 * nidhi
		 * 23-08-2018
		 */
		if(prodInvTran.getProSerialNoDetails()!= null &&
				!prodInvTran.getProSerialNoDetails().containsKey(0)){
			prodInvTran.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
			System.out.println("111111111111");
		}
		if(item.getProSerialNoDetails()!=null
				&& item.getProSerialNoDetails().size()>0){
			ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
			
			proSerList = item.getProSerialNoDetails().get(0);
			
			if(proSerList.size()>0){
				for(ProductSerialNoMapping proSer : proSerList){
					if(proSer.getProSerialNo().trim().length()>0){
						prodInvTran.getProSerialNoDetails().get(0).add(proSer);
						System.out.println("22222222");
						System.out.println("proSer =="+proSer);

					}
				}
			}
			
		}
		/** Date 29-11-2019 by vijay 
		 * Des :- NBHC Inventory Management Lock seal number
		 */
		if(item.getSerialNumber()!=null){
			prodInvTran.setSerialNumber(item.getSerialNumber());
		}
		/**
		 * ends here
		 */
		
		return prodInvTran;
		
	}
	
	
	
	public static ProductInventoryTransaction getProductInventoryTransaction(int productId,String productName,double productQty,String productUOM,
			double productPrice,String warehouseName,String storageLocation,String storageBin,
			String documentType,int documentId,String documentTitle,Date documentDate,
			String operation,double openingStock,double closingStock,long compId,String branch,String serviceId){
			
			System.out.println("Inside Transaction.....");
			ProductInventoryTransaction prodInvTran=new ProductInventoryTransaction();
			
			prodInvTran.setProductId(productId);
			prodInvTran.setProductName(productName);
			prodInvTran.setProductPrice(productPrice);
			prodInvTran.setProductQty(productQty);
			prodInvTran.setProductUOM(productUOM);
			prodInvTran.setWarehouseName(warehouseName);
			prodInvTran.setStorageLocation(storageLocation);
			prodInvTran.setStorageBin(storageBin);
			prodInvTran.setDocumentType(documentType);
			prodInvTran.setDocumentId(documentId);
			prodInvTran.setDocumentTitle(documentTitle);
			prodInvTran.setDocumentDate(documentDate);
			prodInvTran.setOperation(operation);
			prodInvTran.setOpeningStock(openingStock);
			prodInvTran.setClosingStock(closingStock);
			prodInvTran.setCompanyId(compId);
			prodInvTran.setBranch(branch);
			prodInvTran.setInventoryTransactionDateTime(new Date());
			if(serviceId!=null&&!serviceId.equals(""))
				prodInvTran.setServiceId(serviceId);
			
			return prodInvTran;
	}
	
	
	
	static class InvalidTransactionException extends Exception{
		public InvalidTransactionException(String message) {
			super(message);
		}
	}
	
	public static void validateStock(double closingStock) throws InvalidTransactionException{
		System.out.println("Validating STOCK....!!!!");
		if(closingStock<0){  
		  throw new InvalidTransactionException("CLOSING STOCK IS GOING NEGATIVE....!!!!");  
		}
	}
	
	/**
	 * End
	 */
	/**
	 * Date : 18-01-2017 By Anil
	 * Project: NBHC CCPM
	 * This method is used to update stock in ProductInventoryView and  ProductInventoryViewDetails .
	 * also creates transaction for given updation.
	 * 
	 */
	
	@GwtIncompatible
	public static void setProductInventory(ArrayList<InventoryTransactionLineItem> itemList , boolean flag){
		System.out.println("INSIDE INVETORY ...!");
		DecimalFormat df=new DecimalFormat("0.0000");
		HashSet<Integer> hset=new HashSet<Integer>();
		for(InventoryTransactionLineItem obj:itemList){
			hset.add(obj.getProdId());
		}
		List<Integer> prodIdList=new ArrayList<Integer>(hset);
		List<ProductInventoryView> prodInvViewList=ofy().load().type(ProductInventoryView.class).filter("companyId", itemList.get(0).getCompanyId()).filter("productinfo.prodID IN", prodIdList).list();
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		/**
		 * Date : 12-02-2018 BY ANIL 
		 * For OPTICALC
		 * here we get all those product whose stock is not already defined
		 * and we will generate the the stock entry with current stock.
		 */
		ArrayList<InventoryTransactionLineItem> newStockList=new ArrayList<InventoryTransactionLineItem>();
		
		ArrayList<InventoryTransactionLineItem> updateExistingStockList=new ArrayList<InventoryTransactionLineItem>();
		
		/**
		 * Date : 26-02-2018 BY ANIL
		 */
		boolean createStockFlag=false;
		boolean updateStockFlag=false;
		
		for(int i=0;i<itemList.size();i++){
			System.out.println("LOOOP : :  "+i);
			createStockFlag=true;
			for(ProductInventoryView piv:prodInvViewList){
				for(ProductInventoryViewDetails pivd : piv.getDetails()){
					if(pivd.getProdid()==itemList.get(i).getProdId()){
						updateStockFlag=true;
						createStockFlag=false;
						
						/**
						 * Date : 26-02-2018 BY ANIL
						 */
						if(itemList.get(i).getWarehouseName().trim().equals(pivd.getWarehousename().trim())
								&&itemList.get(i).getStorageLocation().trim().equals(pivd.getStoragelocation().trim())
								&&itemList.get(i).getStorageBin().trim().equals(pivd.getStoragebin().trim())){
							updateStockFlag=false;
							/*** Date 17-12-2018 By Vijay
							 * Des :- Bug From NBHC CCPM :- creating duplicate entry product inventory view master
							 * due to updateStockFlag getting overwrite to true even if master defined.
							 * so i have added break statement for  updateStockFlag will  not overwrite
							 */
							break;
						}
						/**
						 * End
						 */
					}
				}
				
			}
			if(createStockFlag){
				System.out.println("CREATE STOCK FLAG TRUE ");
				newStockList.add(itemList.get(i));
				itemList.remove(i);
				i--;
			}
			
			/**
			 * Date : 26-02-2018 BY ANIL
			 */
			if(updateStockFlag){
				System.out.println("UPDATE STOCK FLAG TRUE ");
				updateExistingStockList.add(itemList.get(i));
				itemList.remove(i);
				i--;
			}
			
			/**
			 * End
			 */
		}
		
		System.out.println("CREATE STOCK LIST SIZE : "+newStockList.size());
		if(newStockList.size()!=0){
			createAndUpdateStock(newStockList);
		}
		
		System.out.println("UPDATE STOCK LIST SIZE : "+updateExistingStockList.size());
		if(updateExistingStockList.size()!=0){
			updateExistingStock(updateExistingStockList,prodInvViewList);
		}
		
		/**
		 * End
		 */
		
		
		List<ProductInventoryViewDetails> prodList=new ArrayList<ProductInventoryViewDetails>();
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		System.out.println("PIV SIZE : "+prodInvViewList.size());
		for(ProductInventoryView piv:prodInvViewList){
			for(InventoryTransactionLineItem item:itemList){
				if(piv.getProdID()==item.getProdId()){
					piv.setEditFlag(false);
					double requestedProductQty=0;
					double openingStock=0;
					double closingStock=0;
					for(ProductInventoryViewDetails prod:piv.getDetails()){
						
						if(prod.getWarehousename()!=null && prod.getWarehousename().equals(item.getWarehouseName())
								&&prod.getStoragelocation().equals(item.getStorageLocation())
								&&prod.getStoragebin().equals(item.getStorageBin())){
						
//						if(prod.getBranch().equals(item.getBranch())){
							
						//	System.out.println("BRANCH MATCHED .. "+prod.getBranch()+" : "+item.getBranch());
							requestedProductQty=item.getUpdateQty();
							openingStock=prod.getAvailableqty();
							if(item.getOperation().equals("Add")){
								closingStock=openingStock+requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 */
								if(prod.getProSerialNoDetails()!=null &&
										!prod.getProSerialNoDetails().containsKey(0)){
									prod.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
								}
								if(item.getProSerialNoDetails()!=null
										&& item.getProSerialNoDetails().size()>0){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									
									proSerList = item.getProSerialNoDetails().get(0);
									
									if(proSerList.size()>0){
										for(ProductSerialNoMapping proSer : proSerList){
											if(proSer.getProSerialNo().trim().length()>0){
												proSer.setStatus(false);
												proSer.setNewAddNo(false);
												proSer.setAvailableStatus(true);
												prod.getProSerialNoDetails().get(0).add(proSer);
											}
										}
									}
								}
								/**
								 * end
								 */
								
								/**
								 * Date 13-Dec-2018
								 * @author Vijay Chougule
								 * Requirements :- NBHC Inventory Management 
								 * Des :- based on available Qty Order Qty and Expected Qty calculation for Automatic PR 
								 * Generation with process config 
								 */
								System.out.println("Condition "+(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())));

								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())){
									 prod.setOrderQty(getOrderQty(prod.getAvailableqty(),prod.getForecastDemand(),
											 prod.getConsumptiontillStockReceived(),prod.getReorderlevel(),
											 prod.getMinOrderQty(),prod.getSafetyStockLevel()));
									 prod.setExpectedStock(getExpectedStock(prod.getAvailableqty(), prod.getOrderQty(), prod.getConsumptiontillStockReceived()));
									 System.out.println("expected stock =="+prod.getExpectedStock());
								 }
								/**
								 * ends here
								 */
							 	/**
								 * Date 26-06-2019 By Vijay
								 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
								 *  with process config   
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", piv.getCompanyId())){
									 updateSerialNumberMasterForStockIN(prod,item);
								 }
								 /**
								  * ends here
								  */
							}
							if(item.getOperation().equals("Subtract")){
								closingStock=openingStock-requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 */
								if(prod.getProSerialNoDetails()!=null &&
										prod.getProSerialNoDetails().size() >0 && prod.getProSerialNoDetails().containsKey(0)){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									proSerList.addAll(prod.getProSerialNoDetails().get(0));
									System.out.println("get new number == " + item.getProSerialNoDetails().keySet());
									System.out.println("get status-- " +item.getProSerialNoDetails().containsKey(0));
									if(item.getProSerialNoDetails().containsKey(0) && item.getProSerialNoDetails().get(0).size()> 0){
										ArrayList<ProductSerialNoMapping> proInvList = new ArrayList<ProductSerialNoMapping>();
										for(ProductSerialNoMapping itr : item.getProSerialNoDetails().get(0))
									      {
											 ProductSerialNoMapping element = new ProductSerialNoMapping();
											 element.setAvailableStatus(itr.isAvailableStatus());
											 element.setNewAddNo(itr.isNewAddNo());
											 element.setStatus(itr.isStatus());
											 element.setProSerialNo(itr.getProSerialNo());
											 
											 proInvList.add(element);
											 
									      }
									 for(Iterator<ProductSerialNoMapping> itr = proInvList.iterator();itr.hasNext();)
								      {
										 boolean get = false;
										 ProductSerialNoMapping element21 = itr.next();
										 
										 for(Iterator<ProductSerialNoMapping> itr1 = prod.getProSerialNoDetails().get(0).iterator() ; itr1.hasNext();){
											 ProductSerialNoMapping element1 = itr1.next();
											 if(element21.getProSerialNo().equals(element1.getProSerialNo())){
												 itr1.remove();
												 itr.remove();
												 get =true;
												break;
											 }
									           
									       }
										 
										 if(!get && (element21.isNewAddNo() || !element21.isAvailableStatus())){
											 itr.remove();
											 System.out.println("get new number == " + element21.getProSerialNo());
											 continue;
										 }
									}
								}
								}
								/**
								 * end
								 */
								
								/**
								 * Date : 07-03-2018 BY  ANIL
								 * Commented this code as negative stock is allowed in Opticalc 
								 */
//								try {
//									validateStock(closingStock);
//								} catch (InvalidTransactionException e) {
//									e.printStackTrace();
//									prod.setWarehousename(null);
//								}
								
								/**
								 * Date 13-Dec-2018
								 * @author Vijay Chougule
								 * Requirements :- NBHC Inventory Management 
								 * Des :- based on available Qty Order Qty and Expected Qty calculation for Automatic PR 
								 * Generation with process config 
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", piv.getCompanyId())){
									 prod.setOrderQty(getOrderQty(prod.getAvailableqty(),prod.getForecastDemand(),
											 prod.getConsumptiontillStockReceived(),prod.getReorderlevel(),
											 prod.getMinOrderQty(),prod.getSafetyStockLevel()));
									 prod.setExpectedStock(getExpectedStock(prod.getAvailableqty(), prod.getOrderQty(), prod.getConsumptiontillStockReceived()));
								 }
								/**
								 * ends here
								 */
							 
							   /**
								 * Date 26-06-2019 By Vijay
								 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
								 *  with process config   
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", piv.getCompanyId())){
									 updateSerialNumberMasterForStockOUT(prod,item);
								 }
								 /**
								  * ends here
								  */ 
							}
							ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class)
									.filter("companyId", piv.getCompanyId())
									.filter("prodInvViewCount",prod.getProdid())
//									.filter("branch", prod.getBranch().trim()).first().now();
									.filter("warehousename", prod.getWarehousename().trim())
									.filter("storagelocation", prod.getStoragelocation().trim())
									.filter("storagebin", prod.getStoragebin().trim()).first().now();
							if(prodInvDtls!=null){
								System.out.println("pivd not null");
								prodInvDtls.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 */
								if(!prodInvDtls.getProSerialNoDetails().containsKey(0)){
									prodInvDtls.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
								}
								if(item.getProSerialNoDetails().size()>0){
									ArrayList<ProductSerialNoMapping> proSerList1 = new ArrayList<ProductSerialNoMapping>();
									
									proSerList1 = item.getProSerialNoDetails().get(0);
									
									if(proSerList1.size()>0){
										for(ProductSerialNoMapping proSer : proSerList1){
											if(proSer.getProSerialNo().trim().length()>0){
												prodInvDtls.getProSerialNoDetails().get(0).add(proSer);
											}
										}
									}
								}
								/**
								 * end
								 */
								prodList.add(prodInvDtls);
							}
							item.setOpeningStock(openingStock);
							item.setClosingStock(Double.parseDouble(df.format(closingStock)));
							item.setProductName(piv.getProductName());
							item.setProductPrice(piv.getProductPrice());
							item.setProductUOM(piv.getProductUOM());
							
							/**
							 * Date : 02-03-2017 By Anil
							 * setting product code
							 */
							item.setProductCode(piv.getProductCode());
							
							ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item);
							transactionList.add(prodInvTran);
							
							break;
						}
					}
					break;
				}
			}
		}
		ArrayList<SuperModel>modelList=new ArrayList<SuperModel>();
		for(ProductInventoryTransaction obj:transactionList){
			SuperModel model=obj;
			modelList.add(model);
		}
		
		GenricServiceImpl impl=new GenricServiceImpl();
		if(modelList.size()!=0){
			impl.save(modelList);
		}
		
		/**
		 * Date : 21-11-2017 BY ANIL
		 */
		
//		ofy().save().entities(prodInvViewList);
//		ofy().save().entities(prodList);
		
		for(ProductInventoryView piv:prodInvViewList){
			ofy().save().entity(piv).now();
		}
		for(ProductInventoryViewDetails pivd:prodList){
			ofy().save().entity(pivd).now();
		}
		/**
		 * End
		 */
		
	}
	private static void updateExistingStock(ArrayList<InventoryTransactionLineItem> updateExistingStockList, List<ProductInventoryView> prodInvViewList) {
		// TODO Auto-generated method stub
		for(InventoryTransactionLineItem itli:updateExistingStockList){
//			ProductInventoryView piv=ofy().load().type(ProductInventoryView.class).filter("companyId", newStockList.get(0).getCompanyId()).filter("productinfo.prodID", itli.getProdId()).first().now();
			
			ProductInventoryView piv=null;
			for(ProductInventoryView obj:prodInvViewList){
				if(obj.getCount()==itli.getProdId()){
					piv=obj;
					break;
				}
			}
			
			
			if(piv!=null){
				System.out.println("PIV NOT NULL");
				piv.getDetails().add(getPivLineItem(itli,true));
				
				ProductInventoryViewDetails pivd=getPivLineItem(itli,false);
				pivd.setCompanyId(piv.getCompanyId());
				pivd.setProdInvViewCount(piv.getCount());
				
//				itli.setClosingStock(0-itli.getOpeningStock());
				
				ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(itli);
				
				ofy().save().entity(piv).now();
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(pivd);
				impl.save(prodInvTran);
				
			}
//			else{
//				System.out.println("PIV NULL");
//				ProductInventoryView newPiv=getPIV(itli);
//				newPiv.getDetails().add(getPivLineItem(itli));
//				
//				ProductInventoryViewDetails pivd=getPivLineItem(itli);
//				pivd.setCompanyId(newPiv.getCompanyId());
//				pivd.setProdInvViewCount(newPiv.getCount());
//				
//				ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(itli);
//				
//				
//				GenricServiceImpl impl=new GenricServiceImpl();
//				impl.save(newPiv);
//				impl.save(pivd);
//				impl.save(prodInvTran);
//			}
		}
	}


	private static void createAndUpdateStock(ArrayList<InventoryTransactionLineItem> newStockList) {
		System.out.println("Creating Stock Master ..");
		
		for(InventoryTransactionLineItem item:newStockList){
//			ProductInventoryView piv=ofy().load().type(ProductInventoryView.class).filter("companyId", newStockList.get(0).getCompanyId()).filter("productinfo.prodID", itli.getProdId()).first().now();
//			if(piv!=null){
//				System.out.println("PIV NOT NULL");
//				piv.getDetails().add(getPivLineItem(itli));
//				
//				ProductInventoryViewDetails pivd=getPivLineItem(itli);
//				pivd.setCompanyId(piv.getCompanyId());
//				pivd.setProdInvViewCount(piv.getCount());
//				
//				ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(itli);
//				
//				ofy().save().entity(piv).now();
//				GenricServiceImpl impl=new GenricServiceImpl();
//				impl.save(pivd);
//				impl.save(prodInvTran);
//				
//			}else{
			
				System.out.println("PIV NULL");
				
				
				ProductInventoryView newPiv=getPIV(item);
				newPiv.setEditFlag(false);
				newPiv.setInvFlag(true);
				newPiv.getDetails().add(getPivLineItem(item,true));
				
				
				
				ProductInventoryViewDetails pivd=getPivLineItem(item,false);
				pivd.setCompanyId(newPiv.getCompanyId());
				pivd.setProdInvViewCount(newPiv.getCount());
				
				ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item);
				
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(newPiv);
				impl.save(pivd);
				impl.save(prodInvTran);
//			}
		}
	}

	private static ProductInventoryView getPIV(InventoryTransactionLineItem itli) {
		ProductInventoryView piv=new ProductInventoryView();
		piv.setCompanyId(itli.getCompanyId());
		piv.setProdID(itli.getProdId());
		piv.setProductCode(itli.getProductCode());
		piv.setProductName(itli.getProductName());
		piv.setCount(itli.getProdId());
        if(itli.getProductCategory() != null){
			piv.setProductCategory(itli.getProductCategory());
		}
		if(itli.getProductUOM() != null){
			piv.setProductUOM(itli.getProductUOM());
		}	
		return piv;
	}


	private static ProductInventoryViewDetails getPivLineItem(InventoryTransactionLineItem item, boolean lockSealFlag) {
		DecimalFormat df=new DecimalFormat("0.0000");
		
		ProductInventoryViewDetails pivd=new ProductInventoryViewDetails();
		pivd.setProdid(item.getProdId());
		pivd.setCompanyId(item.getCompanyId());
		pivd.setProdname(item.getProductName());
		pivd.setProdcode(item.getProductCode());
		pivd.setAvailableqty(0-item.getUpdateQty());
		pivd.setProdInvViewCount(item.getProdId());
		//pivd.setBranch(item.getBranch());
		
		/**
		 * Date : 26-02-2018 BY ANIL
		 */
		pivd.setWarehousename(item.getWarehouseName());
		pivd.setStoragelocation(item.getStorageLocation());
		pivd.setStoragebin(item.getStorageBin());
		
		/**
		 * End
		 */
		
		/**
		 * Date 31-08-2019 by Vijay
		 * Des :- NBHC Inventory Management Warehouse type
		 */
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView",
				"EnableUpdateWarehouseType", item.getCompanyId())) {
			WareHouse warehouseEntity = ofy().load().type(WareHouse.class)
					.filter("buisnessUnitName", item.getWarehouseName()).filter("companyId", item.getCompanyId())
					.first().now();
			if (warehouseEntity != null && warehouseEntity.getWarehouseType() != null) {
				pivd.setWarehouseType(warehouseEntity.getWarehouseType());
			}
		}
		/**
		 * Ends here
		 */
		
		
		
		double requestedProductQty=0;
		double openingStock=0;
		double closingStock=0;
		
		
		requestedProductQty=item.getUpdateQty();
		openingStock=0;
		if(item.getOperation().equals("Add")){
			closingStock=openingStock+requestedProductQty;
			pivd.setAvailableqty(Double.parseDouble(df.format(closingStock)));
		}
		if(item.getOperation().equals("Subtract")){
			closingStock=openingStock-requestedProductQty;
			pivd.setAvailableqty(Double.parseDouble(df.format(closingStock)));
//			try {
//				validateStock(closingStock);
//			} catch (InvalidTransactionException e) {
//				e.printStackTrace();
//				pivd.setWarehousename(null);
//			}
		}
		
		
		
		item.setOpeningStock(openingStock);
		item.setClosingStock(Double.parseDouble(df.format(closingStock)));
		
		
		
		
		
//		pivd.setMinqty(itli.getMinqty());
//		pivd.setMaxqty(itli.getMaxqty());
//		pivd.setNormallevel(itli.getNormallevel());
//		pivd.setReorderlevel(itli.getReorderlevel());
//		pivd.setReorderqty(itli.getReorderqty());
//		pivd.setQty(itli.getQty());
//		pivd.setTotalwarehouseQty(itli.getTotalwarehouseQty());
		
		
		/**
		 * Date 18-06-2019 by Vijay
		 * Des :- For Inventory management Lock Seal Serial Number mapping
		 */
		if(pivd.getProSerialNoDetails()!= null &&
				!pivd.getProSerialNoDetails().containsKey(0)){
			pivd.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
		}
		if(item.getProSerialNoDetails()!=null
				&& item.getProSerialNoDetails().size()>0){
			ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
			
			proSerList = item.getProSerialNoDetails().get(0);
			
			if(proSerList.size()>0){
				for(ProductSerialNoMapping proSer : proSerList){
					if(proSer.getProSerialNo().trim().length()>0){
						pivd.getProSerialNoDetails().get(0).add(proSer);
					}
				}
			}
			
		}
		/**
		 * ends here
		 */
		
		
		/**
		 * Date 26-06-2019 By Vijay
		 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
		 *  with process config   
		 */
		 if(lockSealFlag && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", pivd.getCompanyId())){
			 updateSerialNumberMasterForStockIN(pivd,item);
		 }
		 /**
		  * ends here
		  */
		
		return pivd;
	}


	/**
	 * @author Vijay Chougule
	 * Date :- 13-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty calculations
	 */
	public static int getOrderQty(double stockInHand,double forecastDemand,
			double futherConsmptntillStockReceived,double reorderLevel,double minOrderQty,
			double safetyStock){
			double orderQty = 0;
			int finalOrderQty = 0;
			if(forecastDemand>0){
				if(stockInHand<=forecastDemand){
					orderQty = (forecastDemand-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
					if(orderQty!=0){
						orderQty = getRoundUpNumber(orderQty)*minOrderQty;
					}
				}
			}	
				else if(stockInHand<=reorderLevel){
					orderQty = (reorderLevel-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
					if(orderQty!=0){
						orderQty = getRoundUpNumber(orderQty)*minOrderQty;
					}
				}
				else if(stockInHand<=safetyStock){
					orderQty = (safetyStock-(stockInHand-futherConsmptntillStockReceived))/minOrderQty;
					if(orderQty!=0){
						orderQty = getRoundUpNumber(orderQty)*minOrderQty;
					}

				}
//			}//commented by VIjay
			
			finalOrderQty = (int) orderQty;
		return finalOrderQty;
	}
	/**
	 * ends here
	 */

	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty calculations
	 * Des :- below Method is reusable for get roundup if number contains decimal points which is
	 * greater than 1 then number will increased by 1
	 */
	public static int getRoundUpNumber(double number){
		DecimalFormat decimalFormat= new DecimalFormat("0.00");
		System.out.println("number =="+number);
		try {
			String strNumber = String.valueOf(decimalFormat.format(number));
			String num = strNumber.substring(strNumber.indexOf(".")).substring(1);
			int num1 = Integer.parseInt(num);
			if(num1>0){
				number = number+1;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return (int)number;
	}
	
	public static double getExpectedStock(double availableqty,
			double orderQty, double consumptiontillStockReceived) {
		
			System.out.println("orderQty == "+orderQty);
			System.out.println("availableqty "+availableqty);
			System.out.println("consumptiontillStockReceived "+consumptiontillStockReceived);
		return orderQty+availableqty-consumptiontillStockReceived;
	}
	
	
	/**
	 * Date 26-06-2019 By Vijay
	 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
	 *  with process config   
	 */
	private static void updateSerialNumberMasterForStockIN(ProductInventoryViewDetails prod, InventoryTransactionLineItem item) {
		Logger logger = Logger.getLogger("logger");
		logger.log(Level.SEVERE, "Product Code"+item.getProductCode().trim());
		if(item.getProductCode().trim().equals("P100000065")){
			SerialNumberStockMaster serialNumberMaster = new SerialNumberStockMaster();
			serialNumberMaster.setWarehouseName(prod.getWarehousename());
			serialNumberMaster.setStorageLocation(prod.getStoragelocation());
			serialNumberMaster.setStorageBin(prod.getStoragebin());
			serialNumberMaster.setProductId(prod.getProdid());
			serialNumberMaster.setProductCode(prod.getProdcode());
			serialNumberMaster.setProductName(prod.getProdname());
			serialNumberMaster.setCompanyId(prod.getCompanyId());
			serialNumberMaster.setProductCode(item.getProductCode());
//			String serialNumber = getSerialNumber(item);
			String serialNumber =item.getSerialNumber();


			if(serialNumber!=null && !serialNumber.equals("")){
				String arrayserialNumber[] = serialNumber.split("-");
				String startNumber = arrayserialNumber[0].trim();
				String endNumber = arrayserialNumber[1].trim();
				long startSerialNumber = Long.parseLong(startNumber);
				long endSerialNumber = Long.parseLong(endNumber);
				ArrayList<SerialNumberMapping> serailNumberlist = new ArrayList<SerialNumberMapping>();

				for(long i=startSerialNumber;i<=endSerialNumber;i++){
					SerialNumberMapping serialNumberMapping = new SerialNumberMapping();
					serialNumberMapping.setStatus(true);
					serialNumberMapping.setSerialNumber(i);
					serailNumberlist.add(serialNumberMapping);
				}
				serialNumberMaster.setSerailNumberlist(serailNumberlist);
				serialNumberMaster.setStartSerialNumber(startSerialNumber);
				serialNumberMaster.setEndSerialNumber(endSerialNumber);
			}
			serialNumberMaster.setStatus(true);
			serialNumberMaster.setSerialNumberRange(serialNumber);
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(serialNumberMaster);
			logger.log(Level.SEVERE, "Stock Master updated successfully");
		}
	}
	
	


	private static void updateSerialNumberMasterForStockOUT(ProductInventoryViewDetails prod,
			InventoryTransactionLineItem item) {
		Logger logger = Logger.getLogger("logger");
		logger.log(Level.SEVERE, "Product Code"+item.getProductCode().trim());
		if(item.getProductCode().trim().equals("P100000065")){
//			String serialNumber = getSerialNumber(item);
			String serialNumber = item.getSerialNumber();

			SerialNumberStockMaster serialStockMaster = ofy().load().type(SerialNumberStockMaster.class)
					.filter("warehouseName", prod.getWarehousename())
					.filter("storageLocation", prod.getStoragelocation())
					.filter("storageBin", prod.getStoragebin())
					.filter("productCode", item.getProductCode())
					.filter("companyId", prod.getCompanyId())
					.filter("serialNumberRange", serialNumber)
					.filter("status", true).first().now();

			logger.log(Level.SEVERE, "serialStockMaster == "+serialStockMaster);
			logger.log(Level.SEVERE, "serialNumberRange == "+serialNumber);

			if(serialStockMaster!=null){
				updateStockMaster(serialStockMaster);
			}
			else{
				/**
				 * Date :- 22-04-2020
				 * Des :- Earlier NBHC decided to return single single lock seal number so below code 
				 * implemented to return single lock from warehouse to cluster
				 * But they raised new requirement they will no do the single single lock seal
				 * MMN. So below code is not useful based on new requirement for return lock seal number.
				 * 
				 * But below code executed for MIN from NBHC EOD APP.
				 */
				String arrayserialNumber[] = serialNumber.split("-");
				String startNumber = arrayserialNumber[0].trim();
				String endNumber = arrayserialNumber[1].trim();
				long startSerialNumber = Long.parseLong(startNumber);
				long endSerialNumber = Long.parseLong(endNumber);
				
				List<SerialNumberStockMaster> serialStockMasterlist = ofy().load().type(SerialNumberStockMaster.class)
						.filter("warehouseName", prod.getWarehousename())
						.filter("storageLocation", prod.getStoragelocation())
						.filter("storageBin", prod.getStoragebin())
						.filter("productCode", item.getProductCode())
						.filter("companyId", prod.getCompanyId())
//						.filter("startSerialNumber >=", startSerialNumber)
//						.filter("endSerialNumber <=", endSerialNumber)
						.filter("status", true).list();
				logger.log(Level.SEVERE, "warehouseName "+prod.getWarehousename());
				logger.log(Level.SEVERE, "storageLocation "+prod.getStoragelocation());
				logger.log(Level.SEVERE, "storageBin "+prod.getStoragebin());
				logger.log(Level.SEVERE, "productCode "+item.getProductCode());
				logger.log(Level.SEVERE, "companyId "+prod.getCompanyId());

				logger.log(Level.SEVERE, "serialStockMasterlist size == "+serialStockMasterlist.size());
				
				if(serialStockMasterlist.size()!=0){
					for(SerialNumberStockMaster serialNumberStockMaster : serialStockMasterlist){
						if(serialNumberStockMaster.getInActiveserailNumberlist()!=null && serialNumberStockMaster.getInActiveserailNumberlist().size()!=0){
							 boolean flag = false;
							for(SerialNumberMapping srmappingNumber : serialNumberStockMaster.getInActiveserailNumberlist()){
								 String serialNumberRange[] =  srmappingNumber.getProSerialNo().split("-"); 
								 long fromSerialNumber = Long.parseLong(serialNumberRange[0].trim());
								 long toSerialNumber = Long.parseLong(serialNumberRange[1].trim());
								if(fromSerialNumber>=startSerialNumber && toSerialNumber<=endSerialNumber){
									flag  = true;
									break;
								}
								else{
									for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
										if(srNumber.isStatus() && srNumber.getSerialNumber()>=startSerialNumber
														&& srNumber.getSerialNumber() <= endSerialNumber){
											srNumber.setStatus(false);
										}
									}
								}
							}
							if(flag){
								continue;
							}
						}
						else{
							for(SerialNumberMapping srNumber : serialNumberStockMaster.getSerailNumberlist()){
								if(srNumber.isStatus() && srNumber.getSerialNumber()>=startSerialNumber
												&& srNumber.getSerialNumber() <= endSerialNumber){
									srNumber.setStatus(false);
								}
							}
						}
						boolean activeSerialNumber = checkisAnyActiveSerialNumber(serialNumberStockMaster);
						if(activeSerialNumber==false){
							serialNumberStockMaster.setStatus(false);
						}
						if(serialNumberStockMaster.getInActiveserailNumberlist()!=null && serialNumberStockMaster.getInActiveserailNumberlist().size()!=0){
							SerialNumberMapping srNumberMapping = new SerialNumberMapping();
							srNumberMapping.setProSerialNo(serialNumber);
							serialNumberStockMaster.getInActiveserailNumberlist().add(srNumberMapping);
						}
						else{
							ArrayList<SerialNumberMapping> inactiveSRNoMapping = new ArrayList<SerialNumberMapping>();
							SerialNumberMapping srNumberMapping = new SerialNumberMapping();
							srNumberMapping.setProSerialNo(serialNumber);
							inactiveSRNoMapping.add(srNumberMapping);
							serialNumberStockMaster.setInActiveserailNumberlist(inactiveSRNoMapping);

						}

//						serialNumberStockMaster.setInActiveserailNumberlist(getInActiveList(serialNumberStockMaster,serialNumber));
						ofy().save().entity(serialNumberStockMaster);
						logger.log(Level.SEVERE, "Stock updated sucessfully");

					}
					
				}
			}
		}
		
	}
	
	
	private static ArrayList<SerialNumberMapping> getInActiveList(SerialNumberStockMaster serialNumberStockMaster, String serialNumber) {
		if(serialNumberStockMaster.getInActiveserailNumberlist()!=null && serialNumberStockMaster.getInActiveserailNumberlist().size()!=0){
			
		}
		else{
			
		}
		
		return null;
	}


	private static boolean checkisAnyActiveSerialNumber(SerialNumberStockMaster serialNumberStockMaster) {
		for(SerialNumberMapping serialNumberMapping : serialNumberStockMaster.getSerailNumberlist()){
			if(serialNumberMapping.isStatus()){
				return true;
			}
		}
		return false;
	}


	private static void updateStockMaster(SerialNumberStockMaster serialStockMaster) {
		serialStockMaster.setStatus(false);
		for(SerialNumberMapping serialNumberMapping : serialStockMaster.getSerailNumberlist()){
			serialNumberMapping.setStatus(false);
		}
		ofy().save().entity(serialStockMaster);
		Logger logger = Logger.getLogger("logger");
		logger.log(Level.SEVERE, "Stock Updated Sucessfully");
	}


//	private static String getSerialNumber(InventoryTransactionLineItem item) {
//		
//		if(item.getProSerialNoDetails()!=null
//				&& item.getProSerialNoDetails().size()>0){
//			ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
//			
//			proSerList = item.getProSerialNoDetails().get(0);
//			
//			if(proSerList.size()>0){
//				for(ProductSerialNoMapping proSer : proSerList){
//					if(proSer.getProSerialNo().trim().length()>0){
//						return proSer.getProSerialNo();
//					}
//				}
//			}
//		}
//		return null;
//	}
	
	
	/**
	 * Date :- 30-11-2019
	 * Project: NBHC Lock Seal with process configuration
	 * This method is used to update stock in ProductInventoryViewDetails .
	 * also creates transaction for given updation.
	 */
	
	@GwtIncompatible
	public static void setProductInventoryViewDetails(ArrayList<InventoryTransactionLineItem> itemList){
		
		Logger logger1 = Logger.getLogger("UpdateStock Process");
		DecimalFormat df=new DecimalFormat("0.0000");
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HashSet<Integer> hset=new HashSet<Integer>();
		for(InventoryTransactionLineItem obj:itemList){
			hset.add(obj.getProdId());
		}
		List<Integer> prodIdList=new ArrayList<Integer>(hset);
		List<ProductInventoryViewDetails> prodInvViewDetailsList=ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", itemList.get(0).getCompanyId()).filter("prodid IN", prodIdList).list();
		System.out.println("prodInvViewDetailsList"+prodInvViewDetailsList.size());
		
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		try {
//			if(prodInvViewDetailsList.size()>0){
//				logger1.log(Level.SEVERE, "Opening Stock Before"+prodInvViewDetailsList.get(0).getAvailableqty());
//			}
		
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		
			for(InventoryTransactionLineItem item:itemList){
					double requestedProductQty=0;
					double openingStock=0;
					double closingStock=0;
					for(ProductInventoryViewDetails prod:prodInvViewDetailsList){
						if(prod.getProdid()==item.getProdId()){
						if(prod.getWarehousename()!=null && prod.getWarehousename().equals(item.getWarehouseName())
								&&prod.getStoragelocation().equals(item.getStorageLocation())
								&&prod.getStoragebin().equals(item.getStorageBin())){
							requestedProductQty=item.getUpdateQty();
							openingStock=prod.getAvailableqty();
//							logger1.log(Level.SEVERE, "openingStock "+openingStock);
							if(item.getOperation().equals("Add")){
								closingStock=openingStock+requestedProductQty;
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * @author Anil , Date : 02-05-2019
								 * while updating serial number for other transaction then we are adding serial number to existing but for physical inventory it should be cleared and added again
								 */
								if(!prod.getProSerialNoDetails().containsKey(0)||item.getDocumentType().equals(AppConstants.PHYSICALINVENTORY)){
									prod.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
								}
								if(item.getProSerialNoDetails()!=null
										&& item.getProSerialNoDetails().size()>0){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									
									proSerList = item.getProSerialNoDetails().get(0);
									
									if(proSerList.size()>0){
										for(ProductSerialNoMapping proSer : proSerList){
											if(proSer.getProSerialNo().trim().length()>0){
												proSer.setStatus(false);
												proSer.setNewAddNo(false);
												proSer.setAvailableStatus(true);
												prod.getProSerialNoDetails().get(0).add(proSer);
											}
										}
									}
								}
								/**
								 * end
								 */
								
								/**
								 * Date 13-Dec-2018
								 * @author Vijay Chougule
								 * Requirements :- NBHC Inventory Management 
								 * Des :- based on available Qty Order Qty and Expected Qty calculation for Automatic PR 
								 * Generation with process config 
								 */
								System.out.println("Condition "+(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", prod.getCompanyId())));

								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", prod.getCompanyId())){
									 prod.setOrderQty(getOrderQty(prod.getAvailableqty(),prod.getForecastDemand(),
											 prod.getConsumptiontillStockReceived(),prod.getReorderlevel(),
											 prod.getMinOrderQty(),prod.getSafetyStockLevel()));
									 prod.setExpectedStock(getExpectedStock(prod.getAvailableqty(), prod.getOrderQty(), prod.getConsumptiontillStockReceived()));
									 System.out.println("expected stock =="+prod.getExpectedStock());
								 }
								/**
								 * ends here
								 */
								/**
								 * Date 26-06-2019 By Vijay
								 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
								 *  with process config   
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", prod.getCompanyId())){
									 updateSerialNumberMasterForStockIN(prod,item);
								 }
								 /**
								  * ends here
								  */
								 
							}
							if(item.getOperation().equals("Subtract")){
								closingStock=openingStock-requestedProductQty;
								System.out.println("substract");
//								logger1.log(Level.SEVERE, "closingStock "+closingStock);

								/**
								 * nidhi
								 * 23-08-2018
								 * for map create serial number mapping
								 * 
								 */
								if(prod.getProSerialNoDetails()!=null &&
										prod.getProSerialNoDetails().size() >0 && prod.getProSerialNoDetails().containsKey(0)){
									ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
									proSerList.addAll(prod.getProSerialNoDetails().get(0));
									System.out.println("get new number == " + item.getProSerialNoDetails().keySet());
									System.out.println("get status-- " +item.getProSerialNoDetails().containsKey(0));
									if(item.getProSerialNoDetails().containsKey(0) && item.getProSerialNoDetails().get(0).size()> 0){
										
										if(item.getDocumentType().equals(AppConstants.PHYSICALINVENTORY)){
											prod.setProSerialNoDetails(item.getProSerialNoDetails());
										}else{
										
										ArrayList<ProductSerialNoMapping> proInvList = new ArrayList<ProductSerialNoMapping>();
										for(ProductSerialNoMapping itr : item.getProSerialNoDetails().get(0))
									      {
											 ProductSerialNoMapping element = new ProductSerialNoMapping();
											 element.setAvailableStatus(itr.isAvailableStatus());
											 element.setNewAddNo(itr.isNewAddNo());
											 element.setStatus(itr.isStatus());
											 element.setProSerialNo(itr.getProSerialNo());
											 
											 proInvList.add(element);
											 
									      }
									 for(Iterator<ProductSerialNoMapping> itr = proInvList.iterator();itr.hasNext();)
								      {
										 boolean get = false;
										 ProductSerialNoMapping element21 = itr.next();
										 
										 for(Iterator<ProductSerialNoMapping> itr1 = prod.getProSerialNoDetails().get(0).iterator() ; itr1.hasNext();){
											 ProductSerialNoMapping element1 = itr1.next();
											 if(element21.getProSerialNo().equals(element1.getProSerialNo())){
												 itr1.remove();
												 itr.remove();
												 get =true;
												break;
											 }
									           
									       }
										 
										 if(!get && (element21.isNewAddNo() || !element21.isAvailableStatus())){
											 itr.remove();
											 System.out.println("get new number == " + element21.getProSerialNo());
											 continue;
										 }
									}
									}
								}
								}
								/**
								 * end
								 */
								
								
								prod.setAvailableqty(Double.parseDouble(df.format(closingStock)));
								
								/**
								 * Date 13-Dec-2018
								 * @author Vijay Chougule
								 * Requirements :- NBHC Inventory Management 
								 * Des :- based on available Qty Order Qty and Expected Qty calculation for Automatic PR 
								 * Generation with process config 
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableAutomaticPROrderQty", prod.getCompanyId())){
									 prod.setOrderQty(getOrderQty(prod.getAvailableqty(),prod.getForecastDemand(),
											 prod.getConsumptiontillStockReceived(),prod.getReorderlevel(),
											 prod.getMinOrderQty(),prod.getSafetyStockLevel()));
									 prod.setExpectedStock(getExpectedStock(prod.getAvailableqty(), prod.getOrderQty(), prod.getConsumptiontillStockReceived()));
									 System.out.println("expected stock =="+prod.getExpectedStock());
								 }
								/**
								 * ends here
								 */
								 
							 	/**
								 * Date 26-06-2019 By Vijay
								 * Des :- For Inventory Management Lock Seal Serial Number storing in another entity 
								 *  with process config   
								 */
								 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryView", "EnableStoreSerialNumberInDetails", prod.getCompanyId())){
									 updateSerialNumberMasterForStockOUT(prod,item);
								 }
								 /**
								  * ends here
								  */
								
								try {
									validateStock(closingStock);
								} catch (InvalidTransactionException e) {
									e.printStackTrace();
									prod.setWarehousename(null);
								}
							}
							
							item.setOpeningStock(openingStock);
							item.setClosingStock(Double.parseDouble(df.format(closingStock)));
//							logger1.log(Level.SEVERE, "openingStock $$ "+item.getOpeningStock());
//							logger1.log(Level.SEVERE, "ClosingStock $$ "+item.getClosingStock());

							item.setProductName(prod.getProdname());
							item.setProductPrice(prod.getPrice());
//							item.setProductUOM(prod.getProductUOM());
							
							/**
							 * Date : 02-03-2017 By Anil
							 * setting product code
							 */
							item.setProductCode(prod.getProdcode());
							
							ofy().save().entity(prod); 
							
							ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item);
							transactionList.add(prodInvTran);
							break;
						}
					  }
					}
					logger1.log(Level.SEVERE,"Get pro ID -- " + item.getProdId() + " ware house -- " + item.getWarehouseName());
			}
		ArrayList<SuperModel>modelList=new ArrayList<SuperModel>();
		for(ProductInventoryTransaction obj:transactionList){
			SuperModel model=obj;
			modelList.add(model);
		}
		/**
		 * Date 08-09-2018 By Vijay
		 * Des :- Bug transfer in if product inventory view does not defined then getting exception so added if conditon
		 */
		if(modelList.size()!=0){
			for(int j=0;j<modelList.size();j++){
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(modelList.get(j));
			}
		
		}
		
//		for(ProductInventoryViewDetails pivd:prodInvViewDetailsList){
//			ofy().save().entity(pivd).now();
//		}
//		logger1.log(Level.SEVERE, "product inventory view details stock updated");
		/**
		 * End
		 */
		/** Date 08-01-2019 by vijay to clear cache **/
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		logger1.log(Level.SEVERE, "cache cleared");

		} catch (Exception e) {
			logger1.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		
	}
	
	/**
	 * Date : 04-12-2019
	 * Project: NBHC Inventory Management Lock Seal
	 * This method is used to update stock in ProductInventoryViewDetails if product and warehouse not exist in productInventoryViewDetails  .
	 * also creates transaction for given updation.
	 * 
	 */
	
	@GwtIncompatible
	public static void setProductInventoryViewDetails(ArrayList<InventoryTransactionLineItem> itemList , boolean flag){
		System.out.println("INSIDE INVETORY ...!");
		DecimalFormat df=new DecimalFormat("0.0000");
		Logger logger1 = Logger.getLogger("UpdateStock Process");

		HashSet<Integer> hset=new HashSet<Integer>();
		for(InventoryTransactionLineItem obj:itemList){
			hset.add(obj.getProdId());
		}
		List<Integer> prodIdList=new ArrayList<Integer>(hset);
		
		List<ProductInventoryViewDetails> prodInvViewDetailsList=ofy().load().type(ProductInventoryViewDetails.class).filter("companyId", itemList.get(0).getCompanyId()).filter("prodid IN", prodIdList).list();
		System.out.println("prodInvViewDetailsList"+prodInvViewDetailsList.size());
	
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		try {
			
		
		
		/**
		 * Date : 12-02-2018 BY ANIL 
		 * For OPTICALC
		 * here we get all those product whose stock is not already defined
		 * and we will generate the the stock entry with current stock.
		 */
		ArrayList<InventoryTransactionLineItem> newStockList=new ArrayList<InventoryTransactionLineItem>();
		
		ArrayList<InventoryTransactionLineItem> updateExistingStockList=new ArrayList<InventoryTransactionLineItem>();
		
		/**
		 * Date : 26-02-2018 BY ANIL
		 */
		boolean createStockFlag=false;
		boolean updateStockFlag=false;
		
		for(int i=0;i<itemList.size();i++){
			System.out.println("LOOOP : :  "+i);
			createStockFlag=true;
				for(ProductInventoryViewDetails pivd : prodInvViewDetailsList){
					if(pivd.getWarehousename()!=null && pivd.getProdid()==itemList.get(i).getProdId() && 
							itemList.get(i).getWarehouseName().trim().equals(pivd.getWarehousename().trim())
							&&itemList.get(i).getStorageLocation().trim().equals(pivd.getStoragelocation().trim())
							&&itemList.get(i).getStorageBin().trim().equals(pivd.getStoragebin().trim())){
						updateStockFlag=true;
						createStockFlag=false;
						break;
//						/**
//						 * Date : 26-02-2018 BY ANIL
//						 */
//						if(itemList.get(i).getWarehouseName().trim().equals(pivd.getWarehousename().trim())
//								&&itemList.get(i).getStorageLocation().trim().equals(pivd.getStoragelocation().trim())
//								&&itemList.get(i).getStorageBin().trim().equals(pivd.getStoragebin().trim())){
//							updateStockFlag=false;
//							/*** Date 17-12-2018 By Vijay
//							 * Des :- Bug From NBHC CCPM :- creating duplicate entry product inventory view master
//							 * due to updateStockFlag getting overwrite to true even if master defined.
//							 * so i have added break statement for  updateStockFlag will  not overwrite
//							 */
//							break;
//						}
//						/**
//						 * End
//						 */
					}
				}
				
			if(createStockFlag){
				System.out.println("CREATE STOCK FLAG TRUE ");
				newStockList.add(itemList.get(i));
				itemList.remove(i);
				i--;
			}
			
			/**
			 * Date : 26-02-2018 BY ANIL
			 */
			if(updateStockFlag){
				System.out.println("UPDATE STOCK FLAG TRUE ");
				updateExistingStockList.add(itemList.get(i));
				itemList.remove(i);
				i--;
			}
			
			/**
			 * End
			 */
		}
		
		System.out.println("CREATE STOCK LIST SIZE : "+newStockList.size());
		if(newStockList.size()!=0){
			createAndUpdateStockDetails(newStockList);
		}
		
		System.out.println("UPDATE STOCK LIST SIZE : "+updateExistingStockList.size());
		if(updateExistingStockList.size()!=0){
			setProductInventoryViewDetails(updateExistingStockList);
		}
		
		/**
		 * End
		 */
		
		} catch (Exception e) {
			logger1.log(Level.SEVERE, "Exception"+e.getMessage());
		}
		
	}


	/**
	 * Date 05-12-2019 by Vijay
	 * Des :- NBHC Inventory Management if productInventoryViewDetails there is not entry while(GRN/MMN stock IN)
	 * Then here i m creating its entry with 0 balance in productInventoryViewDeatils and then calling stock updation common method as below mentioned
	 * @param newStockList
	 */
	private static void createAndUpdateStockDetails(ArrayList<InventoryTransactionLineItem> newStockList) {
		List<ProductInventoryTransaction> transactionList=new ArrayList<ProductInventoryTransaction>();
		ArrayList<SuperModel> list=new ArrayList<SuperModel>();
		for(InventoryTransactionLineItem item : newStockList){
			ProductInventoryViewDetails pivd=new ProductInventoryViewDetails();
			pivd.setProdid(item.getProdId());
			pivd.setCompanyId(item.getCompanyId());
			pivd.setProdname(item.getProductName());
			pivd.setProdcode(item.getProductCode());
			pivd.setWarehousename(item.getWarehouseName());
			pivd.setStoragelocation(item.getStorageLocation());
			pivd.setStoragebin(item.getStorageBin());
			pivd.setMinqty(0);
			pivd.setMaxqty(0);
			pivd.setNormallevel(0);
			pivd.setReorderlevel(0);
			pivd.setReorderqty(0);
			pivd.setAvailableqty(0);
			pivd.setQty(0);
			pivd.setTotalwarehouseQty(0);
			/**Date 19-8-2019 by Amol Set Category**/
			pivd.setProductCategory(item.getProductCategory());
			list.add(pivd);
			ProductInventoryTransaction prodInvTran=getProductInventoryTransaction(item,true);
			transactionList.add(prodInvTran);
		}
		
		if(list.size()!=0)
		{
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(list);
		}
		ArrayList<SuperModel>modelList=new ArrayList<SuperModel>();
		for(ProductInventoryTransaction obj:transactionList){
			SuperModel model=obj;
			modelList.add(model);
		}
		if(modelList.size()!=0){
			for(int j=0;j<modelList.size();j++){
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(modelList.get(j));
			}
		
		}
		/** Date 05-12-2019 by vijay here calling common method of stock updation ***/
		setProductInventoryViewDetails(newStockList);

	}


	private static ProductInventoryTransaction getProductInventoryTransaction(InventoryTransactionLineItem item,
			boolean flag) {

		ProductInventoryTransaction prodInvTran=new ProductInventoryTransaction();
		prodInvTran.setProductId(item.getProdId());
		prodInvTran.setProductName(item.getProductName());
		prodInvTran.setProductPrice(item.getProductPrice());
		if(flag){
			prodInvTran.setProductQty(0);
		}
		else{
			prodInvTran.setProductQty(item.getUpdateQty());
		}
		prodInvTran.setProductUOM(item.getProductUOM());
		prodInvTran.setWarehouseName(item.getWarehouseName());
		prodInvTran.setStorageLocation(item.getStorageLocation());
		prodInvTran.setStorageBin(item.getStorageBin());
		if(flag){
			prodInvTran.setDocumentType("");
		}
		else{
			prodInvTran.setDocumentType(item.getDocumentType());

		}
		prodInvTran.setDocumentId(item.getDocumentId());
		prodInvTran.setDocumentTitle(item.getDocumnetTitle());
		prodInvTran.setDocumentDate(item.getDocumentDate());
		prodInvTran.setOperation(item.getOperation());
		prodInvTran.setOpeningStock(item.getOpeningStock());
		prodInvTran.setClosingStock(item.getClosingStock());
		prodInvTran.setCompanyId(item.getCompanyId());
		prodInvTran.setBranch(item.getBranch());
		prodInvTran.setInventoryTransactionDateTime(new Date());
		if(item.getServiceId()!=null&&!item.getServiceId().equals(""))
			prodInvTran.setServiceId(item.getServiceId());
		/**
		 * Date :01-03-2017 By ANIL
		 */
		if(item.getDocumentSubType()!=null){
			prodInvTran.setDocumentSubType(item.getDocumentSubType());
		}
		/**
		 * Date : 02-03-2017 By ANIL
		 */
		
		if(item.getProductCode()!=null){
			prodInvTran.setProductCode(item.getProductCode());
		}
		/**
		 * nidhi
		 * 23-08-2018
		 */
		if(prodInvTran.getProSerialNoDetails()!= null &&
				!prodInvTran.getProSerialNoDetails().containsKey(0)){
			prodInvTran.getProSerialNoDetails().put(0, new ArrayList<ProductSerialNoMapping>());
		}
		if(item.getProSerialNoDetails()!=null
				&& item.getProSerialNoDetails().size()>0){
			ArrayList<ProductSerialNoMapping> proSerList = new ArrayList<ProductSerialNoMapping>();
			
			proSerList = item.getProSerialNoDetails().get(0);
			
			if(proSerList.size()>0){
				for(ProductSerialNoMapping proSer : proSerList){
					if(proSer.getProSerialNo().trim().length()>0){
						prodInvTran.getProSerialNoDetails().get(0).add(proSer);
					}
				}
			}
			
		}
		/** Date 29-11-2019 by vijay 
		 * Des :- NBHC Inventory Management Lock seal number
		 */
		if(!flag){
			if(item.getSerialNumber()!=null){
				prodInvTran.setSerialNumber(item.getSerialNumber());
			}
		}
		
		/**
		 * ends here
		 */
		
		return prodInvTran;
	
	}

	
}
