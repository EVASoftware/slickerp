package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
/**
 * Class Representing Issue Slip.
 * @author Kamala
 *
 */
public class IssueSlip extends InventoryTransaction{

	/***********************Attributes************************************************************/
	private static final long serialVersionUID = 8617847128595318206L;
	/**
	 * Person to which this Issue Slip is being Issued.
	 */
	@Index
	protected PersonInfo customerInfo;
	/**
	 * Type of Issue Slip.
	 */
	@Index
	protected String issueType;
	
    
    /**
     * Refrence Number 
     */
	protected String refNo;
   /**
    * Details item corresponding to Issue Slip.
    */
	@Serialize
	 protected ArrayList<IssueSlipDetails>items;
	/**
	 * Description of this Issue Slip.
	 */
	String description;
	
	


	/**
	 * Project for which Issue Slip is Being used.
	 */
	@Index
	String proJectName;
	/***********************Relational Part************************************************************/
	Key<Config> issueTypeKey;
	Key<HrProject>hrProjectKey;
	/***********************Constructor************************************************************/	
	
	
	
	
	public IssueSlip() 
	{
		super();
		customerInfo=new PersonInfo();
		issueType="";
		refNo="";
		items=new ArrayList<IssueSlipDetails>();
		proJectName="";
	}


	public Long getCellNumber() {
		return customerInfo.getCellNumber();
	}


	public void setCellNumber(Long cellNumber) {
		customerInfo.setCellNumber(cellNumber);
	}


	public String getFullName() {
		return customerInfo.getFullName();
	}


	public void setFullName(String fullName) {
		customerInfo.setFullName(fullName);
	}


	public int getReceiverCount() {
		return customerInfo.getCount();
	}


	public void setReceiverCount(int id) {
		customerInfo.setCount(id);
	}


	public String getEmail() {
		return customerInfo.getEmail();
	}


	public void setEmail(String email2) {
		customerInfo.setEmail(email2);
	}

	@SearchAnnotation(Datatype = ColumnDatatype.PERSONINFO, flexFormNumber = "10", title = "Receiver", variableName = "compositeReceiver",extra="EmployeeInfo")
	public PersonInfo getCustomerInfo() {
		return customerInfo;
	}


	public void setCustomerInfo(PersonInfo customerInfo) {
		this.customerInfo = customerInfo;
	}



	public String getRefNo() {
		return refNo;
	}


	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}


	


	public ArrayList<IssueSlipDetails> getInventoryProductItem() {
		return items;
	}


	public void setInventoryProductItem(
			List<IssueSlipDetails> list) {
		if(list!=null)
		{
			this.items=new ArrayList<IssueSlipDetails>();
			items.addAll(list);
		}
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	


	public String getProJectName() {
		return proJectName;
	}


	public void setProJectName(String proJectName) {
		this.proJectName = proJectName;
	}


	


	


	public Key<Config> getIssueTypeKey() {
		return issueTypeKey;
	}


	public void setIssueTypeKey(Key<Config> issueTypeKey) {
		this.issueTypeKey = issueTypeKey;
	}


	public String getIssueType() {
		return issueType;
	}


	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public static List<String> getStatusList() {
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		statusListBox.add(REQUESTED);
		return statusListBox;
	}


	@Override
	public ArrayList<IssueSlipDetails> getProductDetails() {
		// TODO Auto-generated method stub
		return  this.items;
	}


	@Override
	public int getTransectionType() {
		// TODO Auto-generated method stub
		return OUTTRANSECTION;
	}
	
	/*****************************************************Relation Managment Part***********************/
	/*
	@GwtIncompatible
	private void manageRelationSave()
	{
		issueTypeKey=MyUtility.getConfigKeyFromCondition(issueType,ConfigTypes.ISSUENOTETYPE.getValue());
		if(this.proJectName!=null)
		{
			MyQuerry querry=new MyQuerry();
			querry.setQuerryObject(new HrProject());
			Filter filter=new Filter();
			filter.setQuerryString("projectName");
			filter.setStringValue(proJectName.trim());
			querry.getFilters().add(filter);
			hrProjectKey=(Key<HrProject>) MyUtility.getRelationalKeyFromCondition(querry);
		}
		
	}*/
	
	@GwtIncompatible
	private void manageRelationLoad()
	{
		String issueTypeName=MyUtility.getConfigNameFromKey(issueTypeKey);
		if(!(issueTypeName.trim().equals("")))
		{
			this.issueType=issueTypeName;
		}
		
	   if(hrProjectKey!=null)
	   {
		  HrProject proj= ofy().load().key(hrProjectKey).now();
		  if(proj!=null)
		  {
			  this.proJectName=proj.getProjectName();
		  }
	   }
		
	}


	@Override
	public String getDocType() {
		// TODO Auto-generated method stub
		return InventoryEntries.ISSUENOTE;
	}
}
