package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.ProductSerialNoMapping;

/**
 * 
 * @author Vijay Chougule
 * Des :- Every Warehouse Serial Stock Number maintaining in this entity
 * Requirement :- Inventory Management for Lock Seal
 */
@Entity
public class SerialNumberStockMaster extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -274059210881309509L;
	
	@Index
	protected String warehouseName;
	@Index
	protected String storageLocation;
	@Index
	protected String storageBin;
	@Index
	protected String productCode;
	@Index
	protected int productId;
	@Index
	protected String productName;
	
	ArrayList<SerialNumberMapping> serailNumberlist;
	
	@Index
	protected String serialNumberRange;
	@Index
	protected boolean status;
	@Index
	protected long startSerialNumber;
	@Index
	protected long endSerialNumber;
	
//	ArrayList<ProductSerialNoMapping> inActiveserailNumberlist;

	ArrayList<SerialNumberMapping> inActiveserailNumberlist;

	public SerialNumberStockMaster(){
		super();
		warehouseName="";
		storageLocation="";
		storageBin="";
		productCode="";
		serialNumberRange="";
		status = false;
	}
	

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public int getProductId() {
		return productId;
	}


	public void setProductId(int productId) {
		this.productId = productId;
	}


	public ArrayList<SerialNumberMapping> getSerailNumberlist() {
		return serailNumberlist;
	}


	public void setSerailNumberlist(ArrayList<SerialNumberMapping> serailNumberlist) {
		this.serailNumberlist = serailNumberlist;
	}


	



	public String getSerialNumberRange() {
		return serialNumberRange;
	}


	public void setSerialNumberRange(String serialNumberRange) {
		this.serialNumberRange = serialNumberRange;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}



	public ArrayList<SerialNumberMapping> getInActiveserailNumberlist() {
		return inActiveserailNumberlist;
	}


	public void setInActiveserailNumberlist(ArrayList<SerialNumberMapping> inActiveserailNumberlist) {
		this.inActiveserailNumberlist = inActiveserailNumberlist;
	}


	public long getStartSerialNumber() {
		return startSerialNumber;
	}


	public void setStartSerialNumber(long startSerialNumber) {
		this.startSerialNumber = startSerialNumber;
	}


	public long getEndSerialNumber() {
		return endSerialNumber;
	}


	public void setEndSerialNumber(long endSerialNumber) {
		this.endSerialNumber = endSerialNumber;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
