package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class BillProductDetails implements Serializable{
	
	private static final long serialVersionUID = 5774371168403066421L;
	
	/*************************************Applicability Attributes******************************************/
	
	int serviceNo;
	int prodId;
	String prodName;
	String prodCode;
	String prodGroupTitle;
	int prodGroupId;
	
	/****************************************Getters And Setters**********************************************/
	
	public int getServiceNo() {
		return serviceNo;
	}
	public void setServiceNo(int serviceNo) {
		this.serviceNo = serviceNo;
	}
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	public String getProdGroupTitle() {
		return prodGroupTitle;
	}
	public void setProdGroupTitle(String prodGroupTitle) {
		this.prodGroupTitle = prodGroupTitle;
	}
	public int getProdGroupId() {
		return prodGroupId;
	}
	public void setProdGroupId(int prodGroupId) {
		this.prodGroupId = prodGroupId;
	}
	

	
}
