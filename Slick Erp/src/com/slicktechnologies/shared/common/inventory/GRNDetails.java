package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.shared.common.productlayer.Tax;
/**
 * Detail class of GRN
 * @author Kamala
 *
 */
@Embed
public class GRNDetails extends InventoryProductDetail
{
	private static final long serialVersionUID = 5486228249860818993L;
	
	/*******************************Attributes*************************************************/
	/**
	 * Quantity in PO
	 */
	Double poQty;
	/**
	 * RecieveD QTY.
	 */
	Double receivedQty;
	/**
	 * LBT Percentage
	 */
	Tax LBT;
	/**
	 * Vat Percentage.
	 */
	Tax VAT;
	
	
	/**
	 * Date 26-04-2017
	 * added by vijay for NBHC Deadstock
	 * total  product price into quantity
	 * and Warehouse Address
	 */
	private double total;
	private String warehouseAddress;
	/**
	 * Ends here
	 */
	/**
	 * Date 05-04-2019 by Vijay 
	 * NBHC CCPM Grn interface acknowledgement SAP NO 
	 * GRN entry sap number 
	 */
	@Index
	 private String referenceNo;
	 /*** GRN cancellation SAP no ****/
	@Index
	 private String referenceNo2;
	
	/*** Date 13-04-2019 by Vijay for NBHC CCPM partial cancellation ***/
	private boolean recordSelect;
	private String status;
	private String partialCancellationRemark;

	/** Date 04-12-2019 by Vijay for NBHC Inventory Management Lock seal serial number **/
	private String serialNumber;
	
	public GRNDetails() {
		super();
		warehouseAddress="";
		referenceNo ="";
		referenceNo2 ="";
		partialCancellationRemark ="";
		serialNumber="";
	}
/****************************************Getter/Setter*************************************************/
	public Double getPoQty() {
		return poQty;
	}

	public void setPoQty(Double poQty) {
		this.poQty = poQty;
	}
	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}
	
	
	public Tax getLBT() {
		return LBT;
	}
	public void setLBT(Tax lBT) {
		LBT = lBT;
	}
	public Tax getVAT() {
		return VAT;
	}
	public void setVAT(Tax vAT) {
		VAT = vAT;
	}
	
	@Override
	public Double getQty() {
		return this.receivedQty;
	}
	
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj==null)
			return false;
		if(obj instanceof GRNDetails)
		{
			GRNDetails details=(GRNDetails) obj;
			String lpcodeincoming=details.getWarehouseLocation()+details.getProductCode();
			String lpcodeOwn=this.getWarehouseLocation()+this.getProductCode();
			if(lpcodeOwn!=null&&lpcodeincoming!=null)
			{
				if(lpcodeOwn.equals(lpcodeincoming))
				{
					System.out.println("********************");
					return true;
				}
			}
			else 
				return false;
		}
		
		return false;
	}
	
   	
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getWarehouseAddress() {
		return warehouseAddress;
	}
	public void setWarehouseAddress(String warehouseAddress) {
		this.warehouseAddress = warehouseAddress;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getReferenceNo2() {
		return referenceNo2;
	}
	public void setReferenceNo2(String referenceNo2) {
		this.referenceNo2 = referenceNo2;
	}
	public boolean isRecordSelect() {
		return recordSelect;
	}
	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPartialCancellationRemark() {
		return partialCancellationRemark;
	}
	public void setPartialCancellationRemark(String partialCancellationRemark) {
		this.partialCancellationRemark = partialCancellationRemark;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	

}
