package com.slicktechnologies.shared.common.inventory;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;

@Entity
public class Storagebin extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3933889813371793992L;
	
	/**************************************Applicability Attributes************************************/
	
	@Index
	protected String binName;
	@Index
	protected String description;
	@Index
	protected String xcoordinate;
	@Index
	protected String ycoordinate;
	@Index
	protected String storagelocation;
	@Index
	protected boolean status;
	
	
	/**
	 * Added by Anil Date : 23-11-2016
	 * Project: DEADSTOCK NBHC
	 */
	@Index
	protected String warehouseName;
	/**
	 * End
	 */
	/** date 10.09.2019 added by komal to store main branch warehouse bin as per nitin sir's instruction**/
	@Index
	boolean isMainWareHouseBin;
	
	public Storagebin() {
		super();
		
		binName="";
		description="";
		xcoordinate="";
		ycoordinate="";
		storagelocation="";
		
		warehouseName="";
		isMainWareHouseBin =false;
	}

	
	
	
	/****************************************Getters And Setters*************************************/
	
	
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	
	public String getBinName() {
		return binName;
	}
	
	public void setBinName(String binName) {
		this.binName = binName;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getXcoordinate() {
		return xcoordinate;
	}
	public void setXcoordinate(String xcoordinate) {
		this.xcoordinate = xcoordinate;
	}
	
	
	public String getYcoordinate() {
		return ycoordinate;
	}
	public void setYcoordinate(String ycoordinate) {
		this.ycoordinate = ycoordinate;
	}
	
	
	public String getStoragelocation() {
		return storagelocation;
	}
	public void setStoragelocation(String storagelocation) {
		this.storagelocation = storagelocation;
	}
	
	
	public Boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	/*******************************************Overridden Method****************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		Storagebin entity = (Storagebin) m;
		
		String name = entity.getBinName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String locName = entity.getStoragelocation().trim();
		locName=locName.replaceAll("\\s","");
		locName=locName.toLowerCase();
		
		String whName = entity.getWarehouseName().trim();
		whName=whName.replaceAll("\\s","");
		whName=whName.toLowerCase();
		
		
		
		String curname=this.binName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		String curLocName=this.storagelocation.trim();
		curLocName=curLocName.replaceAll("\\s","");
		curLocName=curLocName.toLowerCase();
		
		String curWhName=this.warehouseName.trim();
		curWhName=curWhName.replaceAll("\\s","");
		curWhName=curWhName.toLowerCase();
		
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname)&&locName.equals(curLocName)&&whName.equals(curWhName))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true)&&locName.equals(curLocName)==true&&whName.equals(curWhName)==true)
			  return true;
			else
				return false;
				
		}
	}
	
		@Override
		public String toString() {
			return this.binName;
		}

		public boolean isMainWareHouseBin() {
			return isMainWareHouseBin;
		}
		public void setMainWareHouseBin(boolean isMainWareHouseBin) {
			this.isMainWareHouseBin = isMainWareHouseBin;
		}


}
