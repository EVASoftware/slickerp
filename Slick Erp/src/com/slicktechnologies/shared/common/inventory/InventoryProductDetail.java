package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
/**
 * The Class ProductDetails. SuperClass of product related details of inventory.
 */
@Embed
@Entity
public class InventoryProductDetail extends ProductDetailsPO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6895583181107678650L;

	@Index
	String warehouseLocation;
	@Index
	protected String storageLoc;
	@Index
	protected String storageBin;

	public InventoryProductDetail() {
		super();
		warehouseLocation = "";
		storageLoc="";
		storageBin="";
	}

	public String getWarehouseLocation() {
		return warehouseLocation;
	}
	
	public void setWarehouseLocation(String warehouseLocation) {
		if (warehouseLocation != null)
			this.warehouseLocation = warehouseLocation;
	}
	
	public String getStorageLoc() {
		return storageLoc;
	}

	public void setStorageLoc(String storageLoc) {
		this.storageLoc = storageLoc;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}


	public Double getQty() {
		return 0d;
	}
	public static void makeInventoryProductDetailListBoxLive(ObjectListBox<InventoryProductDetail> olbProductName) {
		MyQuerry myQuerry = new MyQuerry();
		Filter filter = new Filter();
		myQuerry.getFilters().add(filter);
		myQuerry.setQuerryObject(new InventoryProductDetail());
		olbProductName.MakeLive(myQuerry);

	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
