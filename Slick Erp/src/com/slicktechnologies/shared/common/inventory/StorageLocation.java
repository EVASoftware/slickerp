package com.slicktechnologies.shared.common.inventory;
import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;

@Entity
public class StorageLocation extends BusinessUnit {

	/**
	 * 
	 */
	
	/***********************************************Entity Attributes****************************************************/
	/**The Constant serialVersionUID. */
	private static final long serialVersionUID = 982013130256104959L;
	
	/**The status of this branch can be active or inactive */
	@Index
	protected boolean status;
	
	/***********************************************Join Kind Attribute*****************************************************/
	@Index
	protected Key<BranchRelation> keyName;
	@Ignore
	protected boolean nameChanged=false;
	@Index
	protected String warehouseName;

	/*******************************************Default Ctor********************************************************/
	/**
	 * Instantiates a new branch.
	 */
	public StorageLocation() 
	{
		super();
		nameChanged=false;	
		
	}
	
	/**
	 * Gets the status.
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getstatus() 
	{
		return status;
	}

	/**
	 * Sets the status.
	 * @param status the new status
	 */
	public void setstatus(boolean status) 
	{
		this.status=status;	
	}

	@Override
	public void setBusinessUnitName(String buisnessunitName) 
	{
		
		if(buisnessunitName!=null)
		{
		
		if(!(this.buisnessUnitName.equals(buisnessunitName.trim())))
			nameChanged=true;
		this.buisnessUnitName=buisnessunitName;
		}
	}

	
	
	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}



	/**************************************************Relation Managment Part**********************************************/
	
	
	

	

	protected Key<LocationRelation> locationKey;


  
	@OnSave
	@GwtIncompatible
	public void onSave()
	{
		
//	   if(id==null)
//		{
//			LocationRelation branchRelation=new LocationRelation();
//			branchRelation.setName(buisnessUnitName);
//			branchRelation.setCompanyId(getCompanyId());
//			
//			locationKey=(Key<LocationRelation>) MyUtility.createRelationalKey(branchRelation);
//			
//		}
//		else
//		{
//			
//			if(nameChanged==true)
//			{
//				if(locationKey!=null)
//				{
//					LocationRelation branchRelation=new LocationRelation();
//					branchRelation.setName(buisnessUnitName);
//					branchRelation.setCompanyId(getCompanyId());
//					branchRelation.setStatus(getstatus());
//					
//					MyUtility.updateRelationalEntity(branchRelation,locationKey);
//					
//				}
//			}
//		}
		
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.buisnessUnitName.toString();
	}

	@Override
	public boolean isDuplicate(SuperModel model) {
		// TODO Auto-generated method stub
//		return super.isDuplicate(model);
		
		System.out.println("DUPLICATE CHECK METHOD");
		StorageLocation entity=(StorageLocation) model;
		String name = entity.getBusinessUnitName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		
		
		String whName = entity.getWarehouseName().trim();
		whName=whName.replaceAll("\\s","");
		whName=whName.toLowerCase();
		
		
		
		String curname=buisnessUnitName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		String currWhName = warehouseName.trim();
		currWhName=currWhName.replaceAll("\\s","");
		currWhName=currWhName.toLowerCase();
		
		System.out.println(whName+" - "+name+"\t"+currWhName+" - "+curname);
		
		//New Object is being added
		if(entity.id==null)
		{
			System.out.println("ID NULL");
			if(name.equals(curname)&&whName.equals(currWhName)){
				System.out.println("1");
				return true;
			}else{
				System.out.println("0");
				return false;
			}
		}
		// Old object is being updated
		else
		{
			System.out.println("ID NOT NULL");
			if(entity.id.equals(id)){
				System.out.println("0");
				return false;	
			}
			if(name.equals(curname)==true&&whName.equals(currWhName)==true){
				System.out.println("1");
				return true;
			}
		}
		System.out.println("00");
		return false;
	}


}
