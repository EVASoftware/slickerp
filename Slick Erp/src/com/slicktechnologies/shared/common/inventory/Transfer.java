package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;

// TODO: Auto-generated Javadoc
/**
 * Entity Repersenting the Transfer of Goods from one Location  to Another.
 */
@Entity
public class Transfer extends InventoryTransaction
{
  
  /** The Constant serialVersionUID. */
	/***********************Attributes************************************************************/
	private static final long serialVersionUID = 4525807832779710493L;
    
    /** The from location. */
	@Index
    protected String fromLocation;
    
    /** The to location. */
	@Index
    protected String toLocation;
	/**
	 * Label of Transfer.
	 */
	 protected   int MODE=OUTTRANSECTION;
	protected  @Index String transferLabel;
     
     @Serialize
     /** The items. */
     protected  ArrayList<TransferDetails>items;
     
   
    
  /***********************Relational Part************************************************************/
    
    protected Key<LocationRelation>fromLocationKey;
    protected Key<LocationRelation>toLocationKey;
    
   

	public Transfer() {
		super();
		fromLocation="";
		toLocation="";
		transferLabel="";
		items=new ArrayList<TransferDetails>();
	}
	
	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		if(fromLocation!=null)
		  this.fromLocation = fromLocation.trim();
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(String toLocation) {
		if(toLocation!=null)
		   this.toLocation = toLocation.trim();
	}

	public ArrayList<TransferDetails> getItems() {
		return items;
	}

	public void setItems(List<TransferDetails> list) {
		if(list!=null)
		{
		    this.items=new ArrayList<TransferDetails>();
			this.items.addAll(list);
		  
		}
	}
	
	public String getTransferLabel() {
		return transferLabel;
	}

	public void setTransferLabel(String transferLabel) {
		this.transferLabel = transferLabel;
	}

	public Key<LocationRelation> getFromLocationKey() {
		return fromLocationKey;
	}

	public void setFromLocationKey(Key<LocationRelation> fromLocationKey) {
		this.fromLocationKey = fromLocationKey;
	}

	public Key<LocationRelation> getToLocationKey() {
		return toLocationKey;
	}

	public void setToLocationKey(Key<LocationRelation> toLocationKey) {
		this.toLocationKey = toLocationKey;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public List<? extends InventoryProductDetail> getProductDetails() {
		// TODO Auto-generated method stub
		return this.items;
	}

	@Override
	public int getTransectionType() {
		// TODO Auto-generated method stub
		return MODE;
	}
  
	 
/***********************Relational Managment************************************************************/  
    
	@GwtIncompatible
	@OnSave
	public void manageRelationonSave()
	{
		if(fromLocation!=null&&companyId!=null)
			fromLocationKey=ofy().load().type(LocationRelation.class).filter("locationName",fromLocation).
			filter("companyId",getCompanyId()).keys().first().now();
		if(toLocation!=null&&companyId==null)
				toLocationKey=ofy().load().type(LocationRelation.class).filter("locationName",toLocation).
				keys().first().now();
		
		
		
	}
	
	@GwtIncompatible
	@OnLoad
	public void manageRelationonLoad()
	{
		if(fromLocationKey!=null)
		{
			String temp=ofy().load().key(fromLocationKey).now().getName();
			if(temp!=null&&temp.trim().equals("")==false)
				fromLocation=temp;
			
		}
		
		if(toLocationKey!=null)
		{
			String temp=ofy().load().key(toLocationKey).now().getName();
			if(temp!=null&&temp.trim().equals("")==false)
				toLocation=temp;
			
		}
		
	}
	
	public static List<String> getStatusList() {
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		
		return statusListBox;
	}

	@Override
	public String getDocType() {
		// TODO Auto-generated method stub
		return InventoryEntries.TRANSFER;
	}
    @GwtIncompatible
	@Override
	public void reactOnApproval() {
		//Perform Out transection for From Location
		super.reactOnApproval();
		setStatus(Transfer.APPROVED);
		setApprovalDate(GenricServiceImpl.parseDate(new Date()));
		//Perform In Transection
		Transfer toLocation=new Transfer();
		//Create a new List of TransferDetails
		List<TransferDetails>details=new ArrayList<TransferDetails>();
		for(TransferDetails i:this.items)
		{
			TransferDetails temp=new TransferDetails();
			temp.setCompanyId(i.getCompanyId());
			temp.setProductID(i.getProductID());
			temp.setProductCategory(i.getProductCategory());
			temp.setProductCode(i.getProductCode());
			temp.setProductName(i.getProductName());
			temp.setUnitOfmeasurement(i.getUnitOfmeasurement());
		//	temp.setLbtTax(i.getLbtTax());
		//	temp.setVatTax(i.getVatTax());
			temp.setProdPrice(i.getProdPrice());
			temp.setWarehouseLocation(this.toLocation);
			temp.setQuantityInInventory(i.getQuantityInInventory());
			temp.setTransferQuantity(i.getTransferQuantity());
			details.add(temp);
		}
		
		toLocation.setItems(details);
		toLocation.MODE=INTRANSECTION;
	
		toLocation.setApprovalDate(GenricServiceImpl.parseDate(new Date()));
		toLocation.setCompanyId(getCompanyId());
		
		toLocation.createAndChangeInventoryItem();
		
		
		
	}
	
	

	
}
