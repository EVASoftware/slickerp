package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class BranchDetails extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2769562653916907778L;

	protected String branchName;
	
	protected int branchID;
	
	protected String branchAdds;
	
	
	
	
	/*****************************Getters and setters*******************************************/
	
	
	
	public String getBranchName() {
		return branchName;
	}








	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}








	public int getBranchID() {
		return branchID;
	}








	public void setBranchID(int branchID) {
		this.branchID = branchID;
	}








	public String getBranchAdds() {
		return branchAdds;
	}








	public void setBranchAdds(String branchAdds) {
		this.branchAdds = branchAdds;
	}








	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
