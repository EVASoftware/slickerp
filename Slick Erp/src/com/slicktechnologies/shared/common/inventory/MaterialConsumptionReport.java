package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;

@Embed
public class MaterialConsumptionReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5031023879292006479L;
	
	/******************************************Applicability Attributes****************************************/
	
	int contractId;
	int serviceId;
	String docType;
	int docID;
	int prodId;
	String prodName;
	String prodCode;
	double quantity;
	String prodUOM;
	double price;
	double total;
	int custId;
	String custName;
	String custCellNo;
	String poc;
	String branch;
	String segment;
	double netAmount;
	double contAmount;
	double minCost;
	double operatorCost;
	double otherCost;
	double profitLoss;
	double contTotal;
	/**
	 * nidhi 10-01-2019 |*)
	 */
	double plannedminCost;
	double plannedLabourCost;
	double targetRevenue;
	double billingAmt;
	String contType;
	String contGroup;
	ArrayList<Integer> quotationId = new ArrayList<Integer>();
	HashMap<Integer, Double> billingAmtDt = new HashMap<Integer, Double>();	
	ArrayList<Contract> conList = new ArrayList<Contract>();
	ArrayList<Quotation> quoListDt = new ArrayList<Quotation>();
	ArrayList<Integer> contDtList = new ArrayList<Integer>();
	HashMap<Integer,HashSet<Integer>> serContDt = new HashMap<Integer, HashSet<Integer>>();
	/*******************************************Getters And Setters******************************************/
	HashMap<Integer, Double> plannedQuoCost =  new HashMap<Integer, Double>();
	double actualMinCost;
	double actualLabourCost;
	/** date 7.5.2019 added by komal to store SAP code for NBHC**/
	String referenceNumber;
	
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	
	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
	public int getDocID() {
		return docID;
	}
	public void setDocID(int docID) {
		this.docID = docID;
	}
	
	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	public String getProdCode() {
		return prodCode;
	}
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public String getProdUOM() {
		return prodUOM;
	}
	public void setProdUOM(String prodUOM) {
		this.prodUOM = prodUOM;
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
	public String getCustCellNo() {
		return custCellNo;
	}
	public void setCustCellNo(String custCellNo) {
		this.custCellNo = custCellNo;
	}
	
	public String getPoc() {
		return poc;
	}
	public void setPoc(String poc) {
		this.poc = poc;
	}
	public double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}
	public double getContAmount() {
		return contAmount;
	}
	public void setContAmount(double contAmount) {
		this.contAmount = contAmount;
	}
	public double getMinCost() {
		return minCost;
	}
	public void setMinCost(double minCost) {
		this.minCost = minCost;
	}
	public double getOperatorCost() {
		return operatorCost;
	}
	public void setOperatorCost(double operatorCost) {
		this.operatorCost = operatorCost;
	}
	public double getProfitLoss() {
		return profitLoss;
	}
	public void setProfitLoss(double profitLoss) {
		this.profitLoss = profitLoss;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getSegment() {
		return segment;
	}
	public void setSegment(String segment) {
		this.segment = segment;
	}
	public double getOtherCost() {
		return otherCost;
	}
	public void setOtherCost(double otherCost) {
		this.otherCost = otherCost;
	}
	public double getContTotal() {
		return contTotal;
	}
	public void setContTotal(double contTotal) {
		this.contTotal = contTotal;
	}
	public double getPlannedminCost() {
		return plannedminCost;
	}
	public void setPlannedminCost(double plannedminCost) {
		this.plannedminCost = plannedminCost;
	}
	public double getPlannedLabourCost() {
		return plannedLabourCost;
	}
	public void setPlannedLabourCost(double plannedLabourCost) {
		this.plannedLabourCost = plannedLabourCost;
	}
	public double getTargetRevenue() {
		return targetRevenue;
	}
	public void setTargetRevenue(double targetRevenue) {
		this.targetRevenue = targetRevenue;
	}
	public double getBillingAmt() {
		return billingAmt;
	}
	public void setBillingAmt(double billingAmt) {
		this.billingAmt = billingAmt;
	}
	public String getContType() {
		return contType;
	}
	public void setContType(String contType) {
		this.contType = contType;
	}
	public String getContGroup() {
		return contGroup;
	}
	public void setContGroup(String contGroup) {
		this.contGroup = contGroup;
	}
	public ArrayList<Integer> getQuotationId() {
		return quotationId;
	}
	public void setQuotationId(ArrayList<Integer> quotationId) {
		this.quotationId = quotationId;
	}
	public HashMap<Integer, Double> getBillingAmtDt() {
		return billingAmtDt;
	}
	public void setBillingAmtDt(HashMap<Integer, Double> billingAmtDt) {
		this.billingAmtDt = billingAmtDt;
	}
	public List<Contract> getConList() {
		return conList;
	}
	public void setConList(ArrayList<Contract> conList) {
		this.conList = conList;
	}
	public List<Quotation> getQuoListDt() {
		return quoListDt;
	}
	public void setQuoListDt(ArrayList<Quotation> quoListDt) {
		this.quoListDt = quoListDt;
	}
	public HashMap<Integer, Double> getPlannedQuoCost() {
		return plannedQuoCost;
	}
	public void setPlannedQuoCost(HashMap<Integer, Double> plannedQuoCost) {
		this.plannedQuoCost = plannedQuoCost;
	}
	public ArrayList<Integer> getContDtList() {
		return contDtList;
	}
	public void setContDtList(ArrayList<Integer> contDtList) {
		this.contDtList = contDtList;
	}
	public HashMap<Integer, HashSet<Integer>> getSerContDt() {
		return serContDt;
	}
	public void setSerContDt(HashMap<Integer, HashSet<Integer>> serContDt) {
		this.serContDt = serContDt;
	}
	public double getActualMinCost() {
		return actualMinCost;
	}
	public void setActualMinCost(double actualMinCost) {
		this.actualMinCost = actualMinCost;
	}
	public double getActualLabourCost() {
		return actualLabourCost;
	}
	public void setActualLabourCost(double actualLabourCost) {
		this.actualLabourCost = actualLabourCost;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
}
