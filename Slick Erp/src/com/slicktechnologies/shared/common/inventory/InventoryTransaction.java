package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Contract Provider for all Inventory Transection.All other Inventory
 * Transection Should extend this class.
 */
public abstract class InventoryTransaction extends ConcreteBusinessProcess {

	ProductInventoryView view;

	private static final long serialVersionUID = 7260224090002891415L;

	/** The document Type of this Inventory Transaction */

	String docType;

	/** Document Key of this InventoryTransaction */
	Key<?> docKey;

	/**
	 * Repersents weather type of transaction used to determine impact on
	 * Inventory Quantity. i'e weather it will decrease or increase
	 * {@link InventoryInHand} quantity
	 */

	String transType;
	/** Constant Denotes In Inventory Transection **/
	public static final int INTRANSECTION = 1;
	/** Constant Denotes Out Inventory Transection **/
	public static final int OUTTRANSECTION = 2;

	public static final int UPDATETRANSECTION = 3;

	/**
	 * Returns the Products associated with this Inventory Transaction.
	 * 
	 * @return List of Products associated with this Transection.
	 */
	abstract public List<? extends InventoryProductDetail> getProductDetails();

	/**
	 * Returns the type of this Inventory Transection.
	 * 
	 * @return type of transaction
	 */
	abstract public int getTransectionType();

	/**
	 * Inserts objects in {@link IncomingInventory} or {@link OutgoingInventory}
	 * depending upon transection type.Also changes Inventory In Hand.
	 */
	@GwtIncompatible
	public void createAndChangeInventoryItem() {
		// get Product Details of this Transection
		List<? extends InventoryProductDetail> productDetail = getProductDetails();

		// Array List to Store newly created In or Out Transaction
		List<SuperModel> temp = new ArrayList<SuperModel>();
		// List to hold primary key lPcode.
		List<String> inHandPrimary = new ArrayList<String>();
		// Hash Map to minimize looping.
		HashMap<String, InventoryInHand> map = new HashMap<String, InventoryInHand>();
		HashMap<String, ProductInventoryViewDetails> map1 = new HashMap<String, ProductInventoryViewDetails>();
		// Loop the Product Detail and make the object of InventoryIn or Out,
		// Put the objects
		// in temp

		// HashMap<String,InventoryInHand>map=new
		// HashMap<String,InventoryInHand>();

		for (InventoryProductDetail detail : productDetail) {

			if (getTransectionType() == INTRANSECTION) {
				System.out.println("InTransaction$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
				IncomingInventory in = new IncomingInventory();
				in.setItemProduct(detail);
				in.setDocKey(getDocKey());
				in.setDocType(getDocType());
				in.setDocId(getCount());
				in.setCreationDate(getApprovalDate());
				in.setCompanyId(getCompanyId());
				System.out.println("Qty" + detail.getProductQuantity());
				in.setQty(detail.getProductQuantity());
				// in.setQty(2);
				if (!detail.getWarehouseLocation().equals(""))
					in.setWarehouseLocation(detail.getWarehouseLocation());
				else
					in.setWarehouseLocation("N/A");
				in.setStorageBin(detail.getStorageBin());
				in.setStorageLoc(detail.getStorageLoc());

				SuperModel model = in;

				temp.add(model);

			}

			else if (getTransectionType() == OUTTRANSECTION) {
				System.out.println("OUTTRANSECTION");
				OutgoingInventory out = new OutgoingInventory();
				out.setItemProduct(detail);
				out.setDocKey(getDocKey());
				out.setDocType(getDocType());
				out.setDocId(getCount());
				out.setCreationDate(getApprovalDate());
				out.setCompanyId(getCompanyId());
				out.setQty(detail.getProductQuantity());
				out.setWarehouseLocation(detail.getWarehouseLocation());
				SuperModel model = out;
				temp.add(model);

			}

			else if (getTransectionType() == UPDATETRANSECTION) {
				// A Patch as We are taking Count Sheet Items as Out only they
				// can be in also
				System.out.println("UPDATETRANSECTION");
				OutgoingInventory out = new OutgoingInventory();
				out.setItemProduct(detail);
				out.setDocKey(getDocKey());
				out.setDocType(getDocType());
				out.setDocId(getCount());
				out.setCreationDate(getApprovalDate());
				out.setCompanyId(getCompanyId());
				out.setQty(detail.getProductQuantity());
				out.setWarehouseLocation(detail.getWarehouseLocation());

				SuperModel model = out;
				temp.add(model);

			}
			String tempPrimary = detail.getWarehouseLocation()
					+ detail.getProductCode();
			inHandPrimary.add(tempPrimary);

		}
		for (String s : inHandPrimary) {
			System.out.println("Value of inhand primary is " + s);
		}

		List<InventoryInHand> inHand = ofy().load().type(InventoryInHand.class)
				.filter("lPcode in", inHandPrimary)
				.filter("companyId", getCompanyId()).list();

		List<ProductInventoryViewDetails> inHand1 = ofy().load()
				.type(ProductInventoryViewDetails.class)
				.filter("lPcode in", inHandPrimary)
				.filter("companyId", getCompanyId()).list();

		System.out.println("Size of List" + inHand.size());

		for (InventoryInHand obj : inHand) {
			map.put(obj.getlPcode(), obj);
		}
		/*
		 * for(ProductInventoryViewDetails obj:inHand1) {
		 * map1.put(obj.getProdname(), obj); }
		 */

		for (SuperModel model : temp) {
			String lpCode = "";
			InventoryProductDetail prodDetail = null;
			double qty = 0;
			if (model instanceof IncomingInventory) {
				IncomingInventory in = (IncomingInventory) model;
				lpCode = in.getWarehouseLocation() + in.getProductCode();
				prodDetail = in.getItemProduct();
				qty = in.getQty();

			} else if (model instanceof OutgoingInventory) {
				OutgoingInventory out = (OutgoingInventory) model;
				lpCode = out.getWarehouseLocation() + out.getProductCode();
				prodDetail = out.getItemProduct();
				qty = out.getQty();

			}

			InventoryInHand item = map.get(lpCode);
			if (item == null && getTransectionType() == INTRANSECTION) {
				InventoryInHand banana = new InventoryInHand();
				banana.setItem(prodDetail);
				banana.setQty(qty);
				banana.setCompanyId(getCompanyId());
				banana.setlPcode(lpCode);
				// banana.setWareHouseLocationName(lpCode);
				// banana.setCostOfInventory(costOfInventory);

				inHand.add(banana);

				ProductInventoryViewDetails bana = new ProductInventoryViewDetails();
				bana.setAvailableqty((int) qty);

			} else {
				if (getTransectionType() == INTRANSECTION) {
					item.qty = item.qty + qty;
				} else if (getTransectionType() == OUTTRANSECTION) {
					System.out.println("Loc Code " + item.getlPcode());
					System.out.println("Product Name " + item.getProductName());
					System.out.println("Product Qty before update "
							+ item.getQty());
					item.qty = item.qty - qty;
					System.out.println("Product Qty After update "
							+ item.getQty());

				}

				else if (getTransectionType() == UPDATETRANSECTION) {
					item.qty = qty;
					System.out.println("Inside Update Transection " + item.qty);
				}

			}
		}

		GenricServiceImpl impl = new GenricServiceImpl();

		ArrayList<SuperModel> array = new ArrayList<SuperModel>();

		array.addAll(temp);
		if (array.size() != 0)
			impl.save(array);

		array = new ArrayList<SuperModel>();
		array.addAll(inHand);
		if (array.size() != 0)
			impl.save(array);

	}

	@GwtIncompatible
	public void saveInventoryEntries() {
		InventoryEntries entries = new InventoryEntries();
		entries.setBranchname(getBranch());
		entries.setEmployee(getEmployee());
		entries.setApprovedBy(getApproverName());
		System.out.println("Approval Date is " + this.approvalDate);
		entries.setTransectionDate(getApprovalDate());
		entries.setDocType(getDocType());
		entries.setDocId(getCount());
		entries.setCompanyId(getCompanyId());
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(entries);
	}

	public abstract String getDocType();

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public Key<?> getDocKey() {
		return docKey;
	}

	public void setDocKey(Key<?> docKey) {
		this.docKey = docKey;
	}

	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		// super.reactOnApproval();
//		createAndChangeInventoryItem();
//		saveInventoryEntries();
//		retriveInventoryView();
	}

	/********************************** Update inventory view and list ***************************************/

	@GwtIncompatible
	public void retriveInventoryView() {

		ArrayList<ProductInventoryViewDetails> inarray = new ArrayList<ProductInventoryViewDetails>();
		ArrayList<InventoryInHand> inha = new ArrayList<InventoryInHand>();

		ProductInventoryView tempViewlist = ofy().load()
				.type(ProductInventoryView.class)
				.filter("companyId", getCompanyId()).filter("count", 1).first()
				.now();
		System.out.println("1***********" + "size"
				+ tempViewlist.getDetails().size());

		System.out.println("2*980************");
		inarray.addAll(tempViewlist.getDetails());

		List<InventoryInHand> tempInhandlist = ofy().load()
				.type(InventoryInHand.class)
				.filter("companyId", getCompanyId()).list();

		System.out.println("2*************");

		System.out.println("3****************");

		for (SuperModel model : tempInhandlist) {
			InventoryInHand entity = (InventoryInHand) model;
			InventoryInHand temp = new InventoryInHand();

			temp.setProductId(entity.getProductId());
			temp.setProductCategory(entity.getProductCategory());
			temp.setProductCode(entity.getProductCode());
			temp.setProductName(entity.getProductName());
			temp.setUnitOfMeasurment(entity.getUnitOfMeasurment());
			temp.setWareHouseLocationName(entity.getWareHouseLocationName());
			temp.setStorageLoc(entity.getStorageLoc());
			temp.setStorageBin(entity.getStorageBin());
			temp.setQty(entity.getQty());
			inha.add(temp);

		}

		System.out.println("4************");

		for (InventoryInHand temp : inha) {
			System.out.println("5*************");
			for (ProductInventoryViewDetails templist : inarray) {
				if (temp.getProductId() == templist.getProdid()) {
					System.out.println("7************");
					if (temp.getQty() != templist.getAvailableqty())
						templist.setAvailableqty((int) temp.getQty());
				}

			}
		}
		System.out.println("8");

		tempViewlist.setDetails(inarray);
		ofy().save().entities(tempViewlist);

	}

}
