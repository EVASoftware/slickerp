package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Entity Repersenting the Transfer of Goods from one Location  to Another.
 */
@Embed
public class TransferDetails extends InventoryProductDetail
{
  
 
	/***********************Attributes************************************************************/
	private static final long serialVersionUID = 4525807832779710493L;
    
	protected double quantityInInventory;
    protected double transferQuantity;
  
    /***********************Attributes************************************************************/
    
   public TransferDetails() {
		super();
		
	}

   @TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Quantity in Inventory")
	public double getQuantityInInventory() {
		return quantityInInventory;
	}

	public void setQuantityInInventory(double quantityInInventory) {
		this.quantityInInventory = quantityInInventory;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 9, isFieldUpdater = false, isSortable = true, title = "Transfer Quantity")
	public Double getTransferQuantity() {
		return transferQuantity;
	}

	public void setTransferQuantity(double transferQuantity) {
		this.transferQuantity = transferQuantity;
	}

	@Override
	public Double getQty() {
		// TODO Auto-generated method stub
		return this.transferQuantity;
	}
   
   
	@Override
	public boolean equals(Object obj) 
	{
		if(obj==null)
			return false;
		if(obj instanceof TransferDetails)
		{
			TransferDetails details=(TransferDetails) obj;
			String lpcodeincoming=details.getWarehouseLocation()+details.getProductCode();
			String lpcodeOwn=this.getWarehouseLocation()+this.getProductCode();
			if(lpcodeOwn!=null&&lpcodeincoming!=null)
			{
				if(lpcodeOwn.equals(lpcodeincoming))
				{
					System.out.println("********************");
					return true;
				}
			}
			else 
				return false;
		}
		
		return false;
	}
	

	
}
