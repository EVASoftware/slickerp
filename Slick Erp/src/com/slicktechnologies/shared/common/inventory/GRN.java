package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.LoginServiceImpl;
import com.slicktechnologies.server.PurchaseOrderServiceImplementor;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseRequisition;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.relationalLayer.VendorRelation;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;
import com.slicktechnologies.shared.common.sms.SMSDetails;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

@SuppressWarnings("unused")
@Entity
/**
 * Repersents Goods Recieved Entry Note.It can Come Corresponding to a PO or Can be without 
 * PO.
 * @author Kamala
 *
 */

public class GRN extends InventoryTransaction{
	
	/*****************************************Constants***************************************************/

	public static final String CREATED="Created";
	public static final String REQUESTED="Requested";
	public static final String APPROVED="Approved";
	public static final String REJECTED="Rejected";
	public static final String INSPECTIONREQUESTED="Inspection Requested";
	public static final String INSPECTIONAPPROVED="Inspection Approved";
	public static final String CLOSED="Closed";

	/***********************Attributes************************************************************/
	private static final long serialVersionUID = 8617847128595318206L;
	
    /**
     * Represents a Vendor from which the Goods is being Recieved.
     */
	@Index
	PersonInfo vendorInfo;
	/**
	 * Refrence Number corresponding to this GRN.
	 */
	@Index
	String refNo;
	/**
	 * Payment terms will come from PO.
	 */
	String paymentTerms;
	/**
	 * Items of this Receipt
	 */
	
	String paymentMethod;
	/**
	 * Items of this Receipt
	 */
	//@Serialize
//	@Index nidhi 4-10-2018  for store hash map in array list
	ArrayList<GRNDetails>items;
	/**
	 * Description of this Receipt.
	 */
	String description;
	
	@Index
	protected String Title;
	@Index
	protected String poTitle;
	@Index
	protected int poNo;
	@Index
	protected Date grnCreationDate;
	
	protected Date poCreationDate;
	
	protected Date grnReferenceDate;
	@Index
	protected String grnCategory;
	@Index
	protected String grnGroup;
	@Index
	protected String grnType;
	@Index
	protected String grnWarehouse;
	
	protected String inspectionRequired;
	protected int inspectionId;
	protected DocumentUpload upload;
	
	/**
	 * Added by Rahul Verma
	 * Date: 19 june 2017
	 * Description : This flag will check whether GRN document is partially cancelled or not.
	 */
	@Index
	protected boolean partialCancellationFlag;
	@Index
	protected String refNo2;
	
	/**
	 * Address of Vendor
	 */
	Address adress;
	
	/**
	 * Project for which this Delivery is coming.
	 */
	
	String proJectName;
	/**
	 * Po Number corresponding to this GRN,If Grn is from PO.
	 */
	@Index
	String poNumber;
	
	/**
	 * Warehouse name stores the name of warehouse for whom GRN is Created.
	 * it may be blank if delivery address is other than warehouse address in PO
	 * Date : 13-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFIACTION (NBHC)
	 */
	@Index 
	String warehouseName;
	
	/**
	 * END
	 */
	
	/**
	 * This store the asset excel if product in the GRN is of asset category.
	 * Date : 22-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	protected DocumentUpload assetExcelUplod;
	/**
	 * End
	 */
	
	
	/***********************Relational Attributes************************************************************/
	/**
	 * Key of Po.
	 */
	Key<?>poKey;
	Key<HrProject>projecKey;
	Key<Config>paymentTermKey;
	Key<Config>paymentMethodKey;
	Key<VendorRelation>vendorRelationKey;
	@Index
	protected Key<CheckListType>checklistKey;	
	
	/*
	 * Added by Rahul Verma
	 * Date: 22 May 2017
	 */
	@Index
	protected String vendorReferenceId;
	ArrayList<ProductQty> poList;
	ArrayList<ProductQty> grnList;
	ArrayList<ProductQty> compList;
	/***23-1-2019 added by amol***/
	protected String typeOfOrder;
	/** Date 20-12-2018 By Vijay
	 * Des :- to maintain PR number
	 * needed for NBHC Automatic PR Generation 
	 */
	private int prNumber;
	/**
	 * ends here
	 */
	/** Date 12-08-2020 by Vijay for sasha PO's Supplier's Ref./Order No in GRN then into Billingdocument the same ***/
	protected String refNo3;
	
	/******************************************Constructor**********************************************/
	
	public GRN()
	{
		vendorInfo=new PersonInfo();
		//refNo="";
		paymentTerms="";
		description="";
		adress=new Address();
		paymentMethod="";
		proJectName="";
		items=new ArrayList<GRNDetails>();
		inspectionRequired="";
		upload=new DocumentUpload();
		
		warehouseName="";
		assetExcelUplod=new DocumentUpload();
		
		vendorReferenceId="";
		refNo2="";
		refNo3 = "";
	}
	
	
	/**************************************Getters And Setters*******************************************/

	public boolean isPartialCancellationFlag() {
		return partialCancellationFlag;
	}


	public void setPartialCancellationFlag(boolean partialCancellationFlag) {
		this.partialCancellationFlag = partialCancellationFlag;
	}
	
	public String getRefNo2() {
		return refNo2;
	}


	public void setRefNo2(String refNo2) {
		this.refNo2 = refNo2;
	}
	
	public String getVendorReferenceId() {
		return vendorReferenceId;
	}


	public void setVendorReferenceId(String vendorReferenceId) {
		this.vendorReferenceId = vendorReferenceId;
	}
	
	public DocumentUpload getAssetExcelUplod() {
		return assetExcelUplod;
	}


	public void setAssetExcelUplod(DocumentUpload assetExcelUplod) {
		this.assetExcelUplod = assetExcelUplod;
	}

	
	
	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	
	public PersonInfo getVendorInfo() {
		return vendorInfo;
	}

	public void setVendorInfo(PersonInfo vendorInfo) {
		this.vendorInfo = vendorInfo;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public Date getGrnReferenceDate() {
		return grnReferenceDate;
	}


	public void setGrnReferenceDate(Date grnReferenceDate) {
		this.grnReferenceDate = grnReferenceDate;
	}


	public String getPaymentTerms() {
		return paymentTerms;
	}


	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public ArrayList<GRNDetails> getInventoryProductItem() {
		return items;
	}


	public void setInventoryProductItem(List<GRNDetails> list)
	{
		if(list!=null)
		{
			this.items=new ArrayList<GRNDetails>();
			items.addAll(list);
		}
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public Address getAdress() {
		return adress;
	}

	public void setAdress(Address adress) {
		this.adress = adress;
	}

	public String getProJectName() {
		return proJectName;
	}


	public void setProJectName(String proJectName) {
		this.proJectName = proJectName;
	}
	

	public String getGrnWarehouse() {
		return grnWarehouse;
	}


	public void setGrnWarehouse(String grnWarehouse) {
		this.grnWarehouse = grnWarehouse;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Purchase Order Id")
	@SearchAnnotation(Datatype = ColumnDatatype.INTEGERBOX, flexFormNumber = "30", title = "Purchase Order Id", variableName = "ibPOId")
		
		public String getPoNumber() {
			return poNumber;
		}



	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}


	public Key<?> getPoKey() {
		return poKey;
	}


	public void setPoKey(Key<?> poKey) {
		this.poKey = poKey;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public List<? extends InventoryProductDetail> getProductDetails() {
		return this.items;
	}


	@Override
	public int getTransectionType() {
		return INTRANSECTION;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}


	public void setPaymentMethod(String paymentMethod) {
		if(paymentMethod!=null)
		   this.paymentMethod = paymentMethod.trim();
	}
	
	
	public String getTitle() {
		return Title;
	}


	public void setTitle(String title) {
		Title = title;
	}


	public String getPoTitle() {
		return poTitle;
	}


	public void setPoTitle(String poTitle) {
		this.poTitle = poTitle;
	}


	public int getPoNo() {
		return poNo;
	}


	public void setPoNo(int poNo) {
		this.poNo = poNo;
	}


	public Date getGrnCreationDate() {
		return grnCreationDate;
	}


	public void setGrnCreationDate(Date grnCreationDate) {
		this.grnCreationDate = grnCreationDate;
	}


	public Date getPoCreationDate() {
		return poCreationDate;
	}


	public void setPoCreationDate(Date poCreationDate) {
		this.poCreationDate = poCreationDate;
	}

	public String getGrnCategory() {
		return grnCategory;
	}


	public void setGrnCategory(String grnCategory) {
		this.grnCategory = grnCategory;
	}


	public String getGrnGroup() {
		return grnGroup;
	}


	public void setGrnGroup(String grnGroup) {
		this.grnGroup = grnGroup;
	}


	public String getGrnType() {
		return grnType;
	}


	public void setGrnType(String grnType) {
		this.grnType = grnType;
	}

	@Override
	public String getDocType() {
		return InventoryEntries.GRN;
	}
	
	public String getInspectionRequired() {
		return inspectionRequired;
	}

	public void setInspectionRequired(String inspectionRequired) {
		if(inspectionRequired!=null){
			this.inspectionRequired = inspectionRequired.trim();
		}
	}

	public int getInspectionId() {
		return inspectionId;
	}

	public void setInspectionId(int inspectionId) {
		this.inspectionId = inspectionId;
	}

	public DocumentUpload getUpload() {
		return upload;
	}

	public void setUpload(DocumentUpload upload) {
		if(upload!=null){
			this.upload = upload;
		}
	}


	/******************************************OnLoad Management***************************************/
	
	
	@OnLoad
	   @GwtIncompatible
	   public void onLoad()
	   {
		   if(paymentMethodKey!=null)
		   {
			   String paymentMethodName=MyUtility.getConfigNameFromKey(paymentMethodKey);
			   if(paymentMethodName.equals("")==false)
				   setPaymentTerms(paymentMethodName);
		   }
		   if(paymentTermKey!=null)
		   {
			   String paymentTermName=MyUtility.getConfigNameFromKey(paymentTermKey);
			   if(paymentTermName.equals("")==false)
				   setPaymentTerms(paymentTermName);
		   }
	   }
	
	/***************************************Status List***********************************************/
	
	public static List<String> getStatusList() {
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(REQUESTED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		statusListBox.add(INSPECTIONAPPROVED);
		statusListBox.add(INSPECTIONREQUESTED);
		statusListBox.add(CLOSED);
		return statusListBox;
	}
	
	/****************************************Business Logic********************************************/

	
//	@OnSave
//	@GwtIncompatible
//	private void updateGrnStatus(){
//		Inspection inspection=ofy().load().type(Inspection.class).filter("companyId", this.getCompanyId()).filter("count", this.getInspectionId()).first().now();
//		if(inspection!=null){
//			if(inspection.getStatus().equals(APPROVED)&&this.getStatus().equals(CREATED)){
//				this.setStatus(INSPECTIONAPPROVED);
//			}
//		}
//	}
	
	@Override
	@GwtIncompatible
	public void reactOnRejected() {
		super.reactOnRejected();
		Inspection insp = null;
		if(this.getInspectionId()!=0){
			insp=ofy().load().type(Inspection.class).filter("companyId", this.getCompanyId()).filter("count", this.getInspectionId()).first().now();
			if(getStatus().equals(ConcreteBusinessProcess.REJECTED))
			{
				this.setStatus(GRN.CREATED);
				this.setInspectionId(0);
				if(insp!=null){
					for(int i=0;i<this.getInventoryProductItem().size();i++){
						for(int j=0;j<insp.getItems().size();j++){
							if(this.getInventoryProductItem().get(i).getProductID()==insp.getItems().get(j).getProductID()){
								this.getInventoryProductItem().get(i).setProductQuantity(insp.getItems().get(j).getAcceptedQty());
							}
						}
					}
					
					insp.setStatus(REJECTED);
					ofy().save().entity(insp).now();
				}
			}
		}
	}
	
	
	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		
		
		if(getStatus().equals(ConcreteBusinessProcess.APPROVED))
		{
			/**
			 * @author Anil ,Date : 15-03-2019
			 */
			reactOnSave();
			/**
			 * End
			 */
			
			/*** Date 06-02-2019 by Vijay NBHC CCPM not required to create bills against GRN ****/
			if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "EnableDoNotCreateBillDocuments", this.getCompanyId())){
				/** date 20.9.2018 added by komal**/
				createBilling(this);
			}
			

//		for(int i=0;i<getInventoryProductItem().size();i++){
//			
//			if(getInventoryProductItem().get(i).getProductQuantity()!=0)
//			{
//				int productId=getInventoryProductItem().get(i).getProductID();
//				String storageLocation=getInventoryProductItem().get(i).getStorageLoc();
//				String storageBin=getInventoryProductItem().get(i).getStorageBin();
//				String warehouse=getInventoryProductItem().get(i).getWarehouseLocation();
//				double productQty=getInventoryProductItem().get(i).getProductQuantity();
//				Date date=getCreationDate();
//				long compId=getCompanyId();
//			
//				UpdateStock.setProductInventory(productId,productQty, count,date,"GRN",Title,
//						warehouse, storageLocation, storageBin, "Add",compId);
//			}
//			
//		}
		
//		updateInventoryViewList();
			
			
			/**
			 * Date : 18-01-2017 By Anil
			 * New Stock Updation and Transaction creation Code
			 */
			
			ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
			for(GRNDetails product:getInventoryProductItem()){
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(this.getCompanyId());
				item.setBranch(this.getBranch());
				item.setDocumentType(AppConstants.GRN);
				item.setDocumentId(this.getCount());
				item.setDocumentDate(this.getCreationDate());
				item.setDocumnetTitle("");
				item.setProdId(product.getProductID());
				item.setUpdateQty(product.getProductQuantity());
				item.setOperation(AppConstants.ADD);
				item.setWarehouseName(product.getWarehouseLocation());
				item.setStorageLocation(product.getStorageLoc());
				item.setStorageBin(product.getStorageBin());
				/** date 30.11.2018 added by komal **/
				item.setProductName(product.getProductName());
				item.setProductCode(product.getProductCode());
				if(product.getPrduct() != null){
					if(product.getPrduct().getUnitOfMeasurement() != null){
						item.setProductUOM(product.getPrduct().getUnitOfMeasurement());
					}
					if(product.getPrduct().getProductCategory() != null){
						item.setProductCategory(product.getPrduct().getProductCategory());
					}
				}
				
				/**
				 * nidhi
				 * 23-08-2018
				 * for map serial number
				 */
					HashMap<Integer , ArrayList<ProductSerialNoMapping>> prserdt = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					
				if(product.getProSerialNoDetails() !=null &&
						product.getProSerialNoDetails().containsKey(0) && product.getProSerialNoDetails().get(0)!=null){
					prserdt.put(0, new ArrayList<ProductSerialNoMapping>());
					for(ProductSerialNoMapping pr : product.getProSerialNoDetails().get(0)){
						 ProductSerialNoMapping element = new ProductSerialNoMapping();
						 element.setAvailableStatus(pr.isAvailableStatus());
						 element.setNewAddNo(pr.isNewAddNo());
						 element.setStatus(pr.isStatus());
						 element.setProSerialNo(pr.getProSerialNo());
						 prserdt.get(0).add(element);
					}
					for(Iterator<ProductSerialNoMapping> itr = prserdt.get(0).iterator();itr.hasNext();)
				      {
						ProductSerialNoMapping pr = (ProductSerialNoMapping) itr.next();
						if(!pr.isStatus()){
								itr.remove();
						}
				      }
				}
				
				/** Date 04-12-2019 by vijay for Inventory management lock seal Number ***/
				if(product.getSerialNumber()!=null){
				item.setSerialNumber(product.getSerialNumber());
				}
				
				item.setProSerialNoDetails(prserdt);
				itemList.add(item);
			}
//			UpdateStock.setProductInventory(itemList , true);
			/**
			 * Date 29-11-2019 By Vijay
			 * Des :- NBHC Inventory Management Lock Seal Stock Updation from ProductInventoryDetails master
			 */
			 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ProductInventoryViewDetails", "EnableStockUpdation", this.getCompanyId())){
				 UpdateStock.setProductInventoryViewDetails(itemList,true);
			 }
			 else{
				 UpdateStock.setProductInventory(itemList , true);
			 }
			 /**
			  * ends here
			  */

			
			
			
			/**
			 * End
			 */
			
			/**
			 * This Method Execute When you Save GRN
			 * Date : 23 May 2017 By Rahul
			 * Project : PO and GRN Interface (NBHC)
			 */

			checkQuantityNCreateGRN(this.getPoNo(),this.getCount());
			
			/**
			 * Added by Rahul Verma
			 * for Creating GRN Interface Link
			 * 
			 */
			accountingInterface();
			
			if(isCreateAsset()){
				PurchaseOrderServiceImplementor impl=new PurchaseOrderServiceImplementor();
				impl.createAsset(this);
			}
			
			/**
			 * Date 20-12-2018 By Vijay
			 * Des :- NBHC Inventory Management GRN Qty update in PR
			 */
			updatePR(this.getPrNumber());
			
			/**
			 * ends here
			 */
			
			reactOnSendMessage();
		}
	}
	
	

	@GwtIncompatible
	private void checkQuantityNCreateGRN(final int poId,final int grnId) {
	

		// TODO Auto-generated method stub
		List<PurchaseOrder> purchaseOrderList=ofy().load().type(PurchaseOrder.class).filter("companyId", this.getCompanyId()).filter("count", poId).list();
			
		poList=new ArrayList<ProductQty>();
		HashMap<Integer, ArrayList<ProductSerialNoMapping>> prListSr = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
		HashMap<Integer, ArrayList<ProductSerialNoMapping>> drprListSr = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
		
		ArrayList<ProductQty> mergedQty=new ArrayList<ProductQty>();
		HashSet<Integer> prodQtySet=new HashSet<Integer>();
		for(PurchaseOrder entity : purchaseOrderList) {
				if(purchaseOrderList.size()!=0){
					for(int i=0;i<entity.getProductDetails().size();i++){
						
						if(entity.getProductDetails().get(i).getProSerialNoDetails()!= null &&
								entity.getProductDetails().get(i).getProSerialNoDetails().containsKey(0) 
								&& entity.getProductDetails().get(i).getProSerialNoDetails().get(0) != null){
							if(prListSr.containsKey(entity.getProductDetails().get(i).getPrduct().getCount())){
								prListSr.get(entity.getProductDetails().get(i).getPrduct().getCount()).addAll(entity.getProductDetails().get(i).getProSerialNoDetails().get(0));
							}else{
								prListSr.put(entity.getProductDetails().get(i).getPrduct().getCount(),entity.getProductDetails().get(i).getProSerialNoDetails().get(0));
							}
							
						}
//						if(warehouseName.trim().equals(entity.getProductDetails().get(i).getWarehouseName().trim())){ // stop for merge qty of product bcoz dupliction of product no allowed now
							System.out.println("WAREHOUSE NAME : "+warehouseName+" --- "+entity.getProductDetails().get(i).getWarehouseName());
							prodQtySet.add(entity.getProductDetails().get(i).getProductID());
							ProductQty pq=new ProductQty();
							pq.setProdId(entity.getProductDetails().get(i).getProductID());
							pq.setProdQty(entity.getProductDetails().get(i).getProductQuantity());
//							pq.setPrListSr().put(entity.getProductDetails().get(i).getProSerialNoDetails().get(0));
							poList.add(pq);
							
//						}
					}
				}
			}
			System.out.println("HS WH prod lis SIZE "+prodQtySet.size());
			System.out.println("PO prod lis Wh wise Size "+poList.size());
			/*if(prodQtySet.size()<mergedQty.size()){
				for(Integer obj:prodQtySet){
					double sum=0;
					for(ProductQty qtyObj:mergedQty){
						if(qtyObj.getProdId()==obj){
							sum=sum+qtyObj.getProdQty();
						}
					}
					ProductQty pq=new ProductQty();
					pq.setProdId(obj);
					pq.setProdQty(sum);
					poList.add(pq);
				}
				System.out.println("MODIFIES PO LIST SIZE "+poList.size());
			}else{
				poList=mergedQty;
				System.out.println("UNCHANGED PO LIS SIZE "+poList.size());
			}*/
			String warehouseNameStr="";
			if(!this.getWarehouseName().equals("")){
				warehouseNameStr=getWarehouseName();
			}
		/****************Querry in GRN**********************/
			
		List<GRN> grnDataList=null;
		if( this.getPoNo()!=0){
			if(warehouseNameStr!=""){
				grnDataList=ofy().load().type(GRN.class).filter("companyId",this.getCompanyId()).filter("poNo", this.getPoNo()).filter("warehouseName", warehouseNameStr.trim()).list();
			}else{
				grnDataList=ofy().load().type(GRN.class).filter("companyId",this.getCompanyId()).filter("poNo", this.getPoNo()).list();
			}
		}
		
		
		
		grnList=new ArrayList<ProductQty>();
		double qty=0;
		boolean flag=false;
		if(grnDataList!=null){
			for (GRN entity : grnDataList) {
				if (grnDataList.size() != 0) {
					{
						for (int i = 0; i < entity.getInventoryProductItem().size(); i++) {
							
							if(drprListSr.containsKey(entity.getInventoryProductItem().get(i).getProductID()) &&
									entity.getInventoryProductItem().get(i).getProSerialNoDetails() != null 
									&& entity.getInventoryProductItem().get(i).getProSerialNoDetails().containsKey(0) &&
									entity.getInventoryProductItem().get(i).getProSerialNoDetails().get(0) != null 
									){
								drprListSr.get(entity.getInventoryProductItem().get(i).getProductID()).addAll(entity.getInventoryProductItem().get(i).getProSerialNoDetails().get(0));
							}else if(entity.getInventoryProductItem().get(i).getProSerialNoDetails() !=null 
									&& entity.getInventoryProductItem().get(i).getProSerialNoDetails().get(0) != null 
									&& entity.getInventoryProductItem().get(i).getProSerialNoDetails().containsKey(0))
							{
								drprListSr.put(entity.getInventoryProductItem().get(i).getProductID(), entity.getInventoryProductItem().get(i).getProSerialNoDetails().get(0));
							}
							
							
							if (!grnList.isEmpty()) {
								for (int j = 0; j < grnList.size(); j++) {
									if (grnList.get(j).getProdId() == entity .getInventoryProductItem().get(i)
											.getProductID()) {
										qty = grnList.get(j).getProdQty();
										qty = qty + entity.getInventoryProductItem() .get(i).getProductQuantity();
										grnList.get(j).setProdQty(qty);
										flag = true;
									}
								}
								if (flag == false) {
									ProductQty pq = new ProductQty();
									pq.setProdId(entity.getInventoryProductItem()
											.get(i).getProductID());
									pq.setProdQty(entity.getInventoryProductItem()
											.get(i).getProductQuantity());

									grnList.add(pq);
								}
								
							} else {
								ProductQty pq = new ProductQty();
								pq.setProdId(entity.getInventoryProductItem()
										.get(i).getProductID());
								pq.setProdQty(entity.getInventoryProductItem()
										.get(i).getProductQuantity());

								grnList.add(pq);
							}
						}
					}
				} else {
					System.out.println("No Result Found !!!!");
				}
			}
			
		int  counter = validateGrnQty(poList,grnList,prListSr,drprListSr);
		}
		
	
	}

	@GwtIncompatible
	private void actionOnQuantity() {
		int counter = 0;
		List<ProductQty> updatedQtyLis = new ArrayList<ProductQty>();
		double qty = 0;
		for (int i = 0; i < this.getProductDetails().size(); i++) {
			for (int j = 0; j < compList.size(); j++) {
				if (this.getProductDetails().get(i).getProductID() == compList
						.get(j).getProdId()) {

					if (this.getProductDetails().get(i)
							.getProductQuantity() == compList.get(j)
							.getProdQty()) {
						counter++;
					} else {
						qty = compList.get(j).getProdQty()
								- this.getProductDetails().get(i)
										.getProductQuantity();

						ProductQty pq = new ProductQty();
						pq.setProdId(this.getProductDetails().get(i)
								.getProductID());
						pq.setProdQty(qty);
						updatedQtyLis.add(pq);
					}
				}
			}
		}

		if (counter != this.getProductDetails().size()) {
			createGRN(getUpdatedList(updatedQtyLis));
		}

	}

	@GwtIncompatible
	private void createGRN(List<GRNDetails> updList) {
		// TODO Auto-generated method stub

		System.out.println("Entity Enter Here");
		GRN grnEntity=new GRN();
		grnEntity.setCompanyId(this.getCompanyId());
		if(this.getPoNo()!=0){
			grnEntity.setPoNo(this.getPoNo());
		}
		if(this.getPoTitle()!=null){
			grnEntity.setPoTitle(this.getPoTitle());
		}
		if(this.getPoCreationDate()!=null){
			grnEntity.setPoCreationDate(this.getPoCreationDate());
		}
		if(this.getProJectName()!=null){
			grnEntity.setProJectName(this.getProJectName());
		}
		if(this.getTitle()!=null){
			grnEntity.setTitle(this.getTitle());
		}
		if(this.getEmployee()!=null){
			grnEntity.setEmployee(this.getEmployee());
		}
		if(this.getGrnCreationDate()!=null){
			grnEntity.setGrnCreationDate(this.getGrnCreationDate());
		}
		grnEntity.setStatus(ConcreteBusinessProcess.CREATED);
		if(this.getBranch()!=null){
			grnEntity.setBranch(this.getBranch());
		}
		if(this.getGrnCategory()!=null){
			grnEntity.setGrnCategory(this.getGrnCategory());
		}
		if(this.getGrnType()!=null){
			grnEntity.setGrnType(this.getGrnType());
		}
		if(this.getGrnGroup()!=null){
			grnEntity.setGrnGroup(this.getGrnGroup());
		}
		if(this.getApproverName()!=null){
			grnEntity.setApproverName(this.getApproverName());
		}
		if(this.getRefNo()!=null){
			grnEntity.setRefNo(this.getRefNo());
		}
		if(this.getGrnReferenceDate()!=null){
			grnEntity.setGrnReferenceDate(this.getGrnReferenceDate());
		}
		if(this.getRemark()!=null){
			grnEntity.setRemark(this.getRemark());
		}
		if(this.getInventoryProductItem().size()!=0){
			grnEntity.setInventoryProductItem(updList);
		}
		if(this.getVendorInfo()!=null){
			grnEntity.setVendorInfo(this.getVendorInfo());
		}
		if(this.getInspectionRequired()!=null){
			grnEntity.setInspectionRequired(this.getInspectionRequired());
		}
		////////////////////
		if(this.getWarehouseName()!=null){
			grnEntity.setWarehouseName(this.getWarehouseName());
		}
		/**
		 * Date 20-12-2018 by Vijay
		 * Des :- NBHC Inventory Management 
		 * for Automatic PR creation
		 */
		grnEntity.setPrNumber(this.getPrNumber());
		/**
		 * ends here
		 */

		/** Date 12-08-2020 by Vijay for sasha PO's Supplier's Ref./Order No mapping into Billingdocument the same for the report***/
		grnEntity.setRefNo3(this.getRefNo3());
			
		  
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(grnEntity);
		
	}

	@GwtIncompatible
	private List<GRNDetails> getUpdatedList(List<ProductQty> qtyLis) {
		ArrayList<GRNDetails> arrGrn=new ArrayList<GRNDetails>();
		
		for(int i=0;i<this.getProductDetails().size();i++)
		{
			for(int j=0;j<qtyLis.size();j++){
				if(this.getProductDetails().get(i).getProductID()==qtyLis.get(j).getProdId()){
					GRNDetails dli=new GRNDetails();
					dli.setProductID(this.getProductDetails().get(i).getProductID());
					dli.setProductCategory(this.getProductDetails().get(i).getProductCategory());
					dli.setProductCode(this.getProductDetails().get(i).getProductCode());
					dli.setProductName(this.getProductDetails().get(i).getProductName());
					dli.setProductQuantity(qtyLis.get(j).getProdQty());
					dli.setProdPrice(this.getProductDetails().get(i).getProdPrice());
					dli.setUnitOfmeasurement(this.getProductDetails().get(i).getUnitOfmeasurement());
					dli.setVat(this.getProductDetails().get(i).getVat());
					dli.setTax(this.getProductDetails().get(i).getTax());
					dli.setDiscount(this.getProductDetails().get(i).getDiscount());
					dli.setTotal(this.getProductDetails().get(i).getTotal());
					dli.setWarehouseLocation(this.getProductDetails().get(i).getWarehouseLocation());
					dli.setStorageLoc(this.getProductDetails().get(i).getStorageLoc());
					dli.setStorageBin(this.getProductDetails().get(i).getStorageBin());
					/** date 1.10.2018 added by komal**/
					dli.setPrduct(this.getProductDetails().get(i).getPrduct());
					/**
					 * nidhi
					 * 3-10-2018
					 */
					dli.setProSerialNoDetails(qtyLis.get(j).getPrListSr());
					/**
					 * end
					 */
					if(this.getProductDetails().get(i).getPurchaseTax1()!=null){
						dli.setPurchaseTax1(this.getProductDetails().get(i).getPurchaseTax1());
					}
					if(this.getProductDetails().get(i).getPurchaseTax2()!=null){
					dli.setPurchaseTax2(this.getProductDetails().get(i).getPurchaseTax2());
					}
					/**
					 * Date 06-04-2019 by Vijay for NBHC CCPM partial GRN product Reference mapping 
					 */
					if(this.getProductDetails().get(i).getProductRefId()!=null)
					dli.setProductRefId(this.getProductDetails().get(i).getProductRefId());
					/**
					 * ends here
					 */
					arrGrn.add(dli);
				}
			}
		}
		return arrGrn;
	}

	@GwtIncompatible
	private int validateGrnQty(ArrayList<ProductQty> poList,
			ArrayList<ProductQty> grnList ,HashMap<Integer, ArrayList<ProductSerialNoMapping>> prDt,HashMap<Integer, ArrayList<ProductSerialNoMapping>> grnDt) {
		ArrayList<ProductQty> compList1=new ArrayList<ProductQty>();
		int counter = 0;
		double qty=0;
		if(!grnList.isEmpty()){
			for(int i=0;i<poList.size();i++){
				for(int j=0;j<grnList.size();j++){
					if(poList.get(i).getProdId()==grnList.get(j).getProdId() && poList.get(i).getProdQty() == grnList.get(j).getProdQty()){
						counter++;
					}else if(poList.get(i).getProdId()==grnList.get(j).getProdId()){
						
						qty=poList.get(i).getProdQty()- grnList.get(j).getProdQty();
					
						
						ArrayList<ProductSerialNoMapping> prMapingRm = new ArrayList<ProductSerialNoMapping>();
						if(prDt.containsKey(poList.get(i).getProdId())){
							prMapingRm = createDeliveryNoteOfRemaining(prDt,grnDt,poList.get(i).getProdId());
//							rmDlSrList.put(srOrder.getItems().get(i).getPrduct().getCount(), prMapingRm);
						}
						
						
						ProductQty pq=new ProductQty();
						pq.setProdId(poList.get(i).getProdId());
						pq.setProdQty(qty);
						HashMap<Integer, ArrayList<ProductSerialNoMapping> > prSrDt  = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
						prSrDt.put(0, prMapingRm);
						pq.setPrListSr(prSrDt);
						
						compList1.add(pq);
					}
				}
			}
		}
		else{
			for(int i=0;i<poList.size();i++){
				qty=poList.get(i).getProdQty();
				
				ArrayList<ProductSerialNoMapping> prMapingRm = new ArrayList<ProductSerialNoMapping>();
				if(prDt.containsKey(poList.get(i).getProdId())){
					prMapingRm = createDeliveryNoteOfRemaining(prDt,grnDt,poList.get(i).getProdId());
//					rmDlSrList.put(srOrder.getItems().get(i).getPrduct().getCount(), prMapingRm);
				}
				ProductQty pq=new ProductQty();
				pq.setProdId(poList.get(i).getProdId());
				pq.setProdQty(qty);
				pq.getPrListSr().put(0, prMapingRm);
				
				
				compList1.add(pq);
				
			}
//			compList.addAll(poList);
		}
		
		if (counter != poList.size()) {
			createGRN(getUpdatedList(compList1));
		}
		/**
		 * Date 20-12-2018 By Vijay
		 * Des :- NBHC Inventory Management when full qty grn received approved then
		 * updating PR doc status to closed PR doc for Automatic PR Creation
		 */
		else{
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPROrderQty", this.getCompanyId())){
			updatePRStatus(this.getPrNumber(),this.getPoNo());
			}
		}
		/**
		 * ends here
		 */

		
		return counter;
	}
	
	private ArrayList<ProductSerialNoMapping> createDeliveryNoteOfRemaining(HashMap<Integer, ArrayList<ProductSerialNoMapping>> srPrDtl,HashMap<Integer, ArrayList<ProductSerialNoMapping>> dlPrDtl, Integer prCode){
		ArrayList<ProductSerialNoMapping> prDt  = new ArrayList<ProductSerialNoMapping>();
		
		for(Iterator<ProductSerialNoMapping> itr = srPrDtl.get(prCode).iterator();itr.hasNext();)
	      {
			boolean get = false;
			ProductSerialNoMapping dPrDl = (ProductSerialNoMapping) itr.next();
			for(Iterator<ProductSerialNoMapping> itr1 = dlPrDtl.get(prCode).iterator();itr1.hasNext();)
		      {
				ProductSerialNoMapping srPrDl = (ProductSerialNoMapping) itr1.next();
				
				if(dPrDl.getProSerialNo().equals(srPrDl.getProSerialNo()) && srPrDl.isStatus()){
					itr.remove();
					get = true;
					break;
				}
				
		      }
			if(!get){
				ProductSerialNoMapping prDtl = new ProductSerialNoMapping();
				prDtl.setProSerialNo(dPrDl.getProSerialNo());
				prDtl.setNewAddNo(dPrDl.isNewAddNo());
				prDtl.setAvailableStatus(dPrDl.isNewAddNo());
				prDtl.setStatus(dPrDl.isStatus());
				prDt.add(prDtl);
			}
	      }
		if(srPrDtl.size()==0 && dlPrDtl.size()>0){
			boolean get = false;
//			ProductSerialNoMapping dPrDl = (ProductSerialNoMapping) itr.next();
			for(Iterator<ProductSerialNoMapping> itr1 = dlPrDtl.get(prCode).iterator();itr1.hasNext();)
		      {
				ProductSerialNoMapping srPrDl = (ProductSerialNoMapping) itr1.next();
				
				if( srPrDl.isStatus()){
					itr1.remove();
					get = true;
					break;
				}
				
		      }
			if(dlPrDtl.size()>0){
				
				for(ProductSerialNoMapping prd1 : dlPrDtl.get(prCode)){
					ProductSerialNoMapping prDtl = new ProductSerialNoMapping();
					prDtl.setProSerialNo(prd1.getProSerialNo());
					prDtl.setNewAddNo(prd1.isNewAddNo());
					prDtl.setAvailableStatus(prd1.isNewAddNo());
					prDtl.setStatus(prd1.isStatus());
					prDt.add(prDtl);
				}
				
			}
		}
		
		return prDt;
	}
	
	@GwtIncompatible
	private boolean isCreateAsset(){
		boolean assetFlag=false;
		for(GRNDetails obj:getInventoryProductItem()){
			if(obj.getProductCategory().equals("ASSET")){
				assetFlag=true;
				break;
			}
		}
		System.out.println("ASSET FLAG "+assetFlag);
//		logger.log(Level.SEVERE,"ASSET FLAG  :::: "+assetFlag);
		if(assetFlag){
			if(this.getAssetExcelUplod()!=null&&!this.getAssetExcelUplod().getName().equals("")
					&&!this.getAssetExcelUplod().getUrl().equals("")
					){
				System.out.println("ASSET EXCEL");
//				logger.log(Level.SEVERE,"ASSET EXCEL :::: "+assetFlag);
				return true;
			}
		}
		
		return false;
	}
	
	@GwtIncompatible
	protected void updateInventoryViewList()
	{
		List<GRNDetails> grnprodlis=this.getInventoryProductItem();
		for(int i=0;i<grnprodlis.size();i++)
		{
			int prodId=grnprodlis.get(i).getProductID();
			String wareHouse=grnprodlis.get(i).getWarehouseLocation();
			String storageLoc=grnprodlis.get(i).getStorageLoc();
			String storageBin=grnprodlis.get(i).getStorageBin();
			
			ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class).filter("count",prodId).filter("warehousename", wareHouse.trim()).filter("storagelocation", storageLoc.trim()).filter("storagebin", storageBin.trim()).filter("companyId", getCompanyId()).first().now();
			
			if(prodInvDtls!=null){
				prodInvDtls.setAvailableqty(prodInvDtls.getAvailableqty()+grnprodlis.get(i).getProductQuantity());
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(prodInvDtls);
			}
		}
	}
	
	/*************************************Overridden Method**********************************************/
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


	/*********************************Accounting Interface***************************************/
	@GwtIncompatible
	protected void accountingInterface() {
		ProcessName processName = ofy().load().type(ProcessName.class)
				.filter("companyId", this.getCompanyId())
				.filter("processName", AppConstants.PROCESSNAMEACCINTERFACE)
				.filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig = null;
		if (processName != null) {
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("processName", AppConstants.PROCESSCONFIGGRN)
					.filter("companyId", getCompanyId())
					.filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag = 0;

		if (processConfig != null) {
			System.out.println("Process Config Not Null");
			for (int i = 0; i < processConfig.getProcessList().size(); i++) {
				System.out
						.println("----------------------------------------------------------");
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i)
								.getProcessType());
				System.out.println("One===== "
						+ processConfig.getProcessList().get(i).isStatus());
				System.out
						.println("----------------------------------------------------------");
				if (processConfig.getProcessList().get(i).getProcessType()
						.trim()
						.equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)
						&& processConfig.getProcessList().get(i).isStatus() == true) {
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}

		if (flag == 1 && processConfig != null) {
			System.out.println("ExecutingAccInterface");

			if (this.getCount() != 0) {

				System.out.println("Product List Size :: "
						+ this.getProductDetails().size());

				/*************************************************************************************************************/
				for (int i = 0; i < this.getInventoryProductItem().size(); i++) {

					/************************************************* Start ************************************************/

					String unitofmeasurement = this.getInventoryProductItem()
							.get(i).getUnitOfmeasurement();
					double productprice = this.getInventoryProductItem().get(i)
							.getProdPrice();
					int prodId = this.getInventoryProductItem().get(i)
							.getProductID();
					String productCode = this.getInventoryProductItem().get(i)
							.getProductCode();
					String productName = this.getInventoryProductItem().get(i)
							.getProductName();
					double productQuantity = this.getInventoryProductItem()
							.get(i).getProductQuantity();
					double totalAmount = this.getInventoryProductItem().get(i)
							.getProdPrice()
							* this.getProductDetails().get(i)
									.getProductQuantity();
					String prodRefId = "";
					ProductDetailsPO po = this.getInventoryProductItem().get(i);
					if (this.getInventoryProductItem().get(i).getProductRefId()
							.trim() != null) {
						prodRefId = this.getInventoryProductItem().get(i)
								.getProductRefId().trim();
					}
					String vName = this.getVendorInfo().getFullName().trim();
					long vCell = this.getVendorInfo().getCellNumber();
					int vId = this.getVendorInfo().getCount();
					String vendorRefId = "";
					if (this.getVendorReferenceId() != null
							&& this.getVendorReferenceId().trim().length() > 0) {
						vendorRefId = this.getVendorReferenceId();
					} else {
						vendorRefId = 0 + "";
					}
					String warehouseName = this.getInventoryProductItem()
							.get(i).getWarehouseLocation().trim();
					System.out.println("warehouseName" + warehouseName);
					WareHouse warehouse = ofy().load().type(WareHouse.class)
							.filter("companyId", this.companyId)
							.filter("buisnessUnitName", warehouseName).first()
							.now();
					System.out.println("warehouse" + warehouse);
					String warehouseCode = "";
					if (warehouse.getWarehouseCode().trim() != null) {
						warehouseCode = warehouse.getWarehouseCode().trim();
					}
					UpdateAccountingInterface.updateTally(
							new Date(),// accountingInterfaceCreationDate
							this.getEmployee(),// accountingInterfaceCreatedBy
							this.getStatus(),
							AppConstants.STATUS,// Status
							this.getRemark(),
							"Inventory",// module
							"GRN",// documentType
							this.getCount(),// docID
							this.getTitle(),// DOCtile
							this.getCreationDate(), // docDate
							AppConstants.DOCUMENTGL,// docGL
							this.getPoNumber(),// ref doc no.1
							this.getPoCreationDate(),// ref doc date.1
							AppConstants.REFDOCPO,// ref doc type 1
							this.getRefNo(),// ref doc no 2
							this.getGrnReferenceDate(),// ref doc date 2
							AppConstants.REFDOCPO,// ref doc type 2
							AppConstants.ACCOUNTTYPEAP,// accountType
							0,// custID
							"",// custName
							0,// custCell
							Integer.parseInt(vendorRefId.trim()),// vendor ID
							vName,// vendor NAme
							vCell,// vendorCell
							0,// empId
							" ",// empNAme
							this.getBranch(),// branch
							this.getEmployee(),// personResponsible
							"",// requestedBY
							this.getApproverName(),// approvedBY
							"",// paymentmethod
							null,// paymentdate
							"",// cheqno.
							null,// cheqDate
							null,// bankName
							null,// bankAccount
							0,// transferReferenceNumber
							null,// transactionDate
							null,// contract start date
							null, // contract end date
							prodId,// prod ID
							productCode,// prod Code
							productName,// productNAme
							productQuantity,// prodQuantity
							null,// productDate
							0,// duration
							0,// services
							unitofmeasurement,// unit of measurement
							productprice,// productPrice
							0,// VATpercent
							0,// VATamt
							0,// VATglAccount
							0,// serviceTaxPercent
							0,// serviceTaxAmount
							0,// serviceTaxGLaccount
							"",// cform
							0,// cformpercent
							0,// cformamount
							0,// cformGlaccount
							0,// totalamouNT
							0,// NET PAPAYABLE
//							"",// Contract Category
							this.getRefNo2(),// this was extra field for contract category so i used this for GRN refNo2  /*** Date 15-04-2019 by Vijay for NBHC CCPM need Bill number to map in SAP **/
							0,// amount Recieved
							0.0,// base Amt for tdscal in pay by rohan
							0, // tds percentage by rohan
							0,// tds amount by rohan
							"", 0, "", 0, "", 0, "", 0, "", 0, "", 0, "", 0,
							"", 0, "", 0, "", 0, "", 0, "", 0, "", "", "", "",
							"", "", 0, this.getCompanyId(), null, // billing
																	// from date
																	// (rohan)
							null, // billing to date (rohan)
							warehouseName, // Warehouse
							warehouseCode, // warehouseCode
							prodRefId, // ProductRefId
							AccountingInterface.OUTBOUNDDIRECTION, // Direction
							AppConstants.CCPM, // sourceSystem
							AppConstants.NBHC, // Destination System
							null,null,null
							);

				}

			}

		}
	}
	/** date 13.7.2018 added by komal to create invoice on grn approval**/
	@GwtIncompatible
	private void createBilling(GRN grn){
		Logger logger = Logger.getLogger("GRN logger");
		BillingDocument  billingDocument = null;
		if(grn.getCount()!=0){
			billingDocument = ofy().load().type(BillingDocument.class).filter("companyId", companyId).filter("refNumber", grn.getCount()).first().now();
		}
		  PurchaseOrder purchaseOrder = ofy().load().type(PurchaseOrder.class).filter("companyId", companyId).filter("count", grn.getPoNo()).first().now();	
		  double otherCharges = 0;
		  List<OtherCharges> otherChargeDetails = new ArrayList<OtherCharges>();
		 
		if(billingDocument == null){
			billingDocument = new BillingDocument();
		}
		 if(purchaseOrder != null){
			  otherCharges = purchaseOrder.getGtotal();
			  if(purchaseOrder.getOtherCharges() != null){
				  billingDocument.setOtherCharges(purchaseOrder.getOtherCharges());
				  otherChargeDetails = purchaseOrder.getOtherCharges();
			  }
			
		  }
		ServerAppUtility serverapp = new ServerAppUtility();
		Vendor cust = ofy().load().type(Vendor.class).filter("companyId", companyId).filter("count", grn.getVendorInfo().getCount()).first().now();
		
		ArrayList<SalesOrderProductLineItem> productlist = new ArrayList<SalesOrderProductLineItem>();
		ArrayList<SalesLineItem> salesItem = new ArrayList<SalesLineItem>();
		SalesOrderProductLineItem salesLineItem=new SalesOrderProductLineItem();
		ArrayList<GRNDetails> grnDetailsList = grn.items;
		double total = 0;
		for(GRNDetails details : grnDetailsList){
			salesLineItem = new SalesOrderProductLineItem();
			/**
			 * Date : 27-12-2018 BY ANIL
			 * No need to load super product,we can directly pic it from grn
			 */
//			SuperProduct product = ofy().load().type(SuperProduct.class).filter("companyId", companyId).filter("count", details.getProductID()).filter("status", true).first().now();
			if(details.getProductQuantity() >0){
			/**
			 * Date : 27-12-2018 BY ANIL
			 * commented below code ,because of it GRN's line items are getting updated and data get corrupted 
			 */
			salesLineItem.setPrduct(details.getPrduct());
//			salesLineItem.setPrduct(product);
//			details.setPrduct(product);
			
			/**
			 * End
			 */
			salesLineItem.setTotalAmount(details.getProductQuantity() * details.getProdPrice());
			salesLineItem.setProdId(details.getProductID());
			salesLineItem.setProdCode(details.getProductCode());
			salesLineItem.setProdCategory(details.getProductCategory());
			salesLineItem.setProdName(details.getProductName());
			salesLineItem.setPrice(details.getProdPrice());
		//	salesLineItem.setQuantity(details.getProductQuantity());
			salesLineItem.setDiscountAmt(details.getDiscount());
			salesLineItem.setVatTax(details.getPurchaseTax1());
			salesLineItem.setServiceTax(details.getPurchaseTax2());
			
			salesLineItem.setBaseBillingAmount(salesLineItem.getTotalAmount());
			salesLineItem.setPaymentPercent(100.0);
			salesLineItem.setBasePaymentAmount(salesLineItem.getTotalAmount());
			/**
			 * Updated By: Viraj 
			 * Date: 13-02-2019
			 * Description: To solve null pointer error when we create grn through PR(Purchase Register)
			 */
			if(details.getPrduct() != null && details.getPrduct().getHsnNumber()!=null) {
				salesLineItem.setHsnCode(details.getPrduct().getHsnNumber());
			}
			salesLineItem.setArea(details.getProductQuantity()+"");
			if(salesLineItem.getUnitOfMeasurement()!=null)
			salesLineItem.setUnitOfMeasurement(salesLineItem.getUnitOfMeasurement());
			//salesLineItem.set
			productlist.add(salesLineItem);
			
			SalesLineItem item=new SalesLineItem();
			item.setPrduct(details.getPrduct());
			//item.setQuantity(details.getQty());
			item.setArea(details.getProductQuantity()+"");
			item.setPrice(details.getProdPrice());
			item.setTotalAmount(details.getProductQuantity() * details.getProdPrice());					
			item.setVatTax(details.getPurchaseTax1());
			item.setServiceTax(details.getPurchaseTax2());
			total += item.getTotalAmount();
			logger.log(Level.SEVERE , "Total : " + total);
			salesItem.add(item);
			}
		}
		if(otherChargeDetails.size() > 0){
			for(OtherCharges ot : otherChargeDetails){
			SalesLineItem item1=new SalesLineItem();
			ItemProduct product = new ItemProduct();
//			product = ofy().load().type(ItemProduct.class).first().now();
//			logger.log(Level.SEVERE , "product :"+ product.getProductName());
//			product.setProductName(ot.getOtherChargeName());
//			product.setPurchaseTax1(ot.getTax1());
//			product.setPurchaseTax2(ot.getTax2());
			item1.setPrduct(product);
			item1.setProductName(ot.getOtherChargeName());
			item1.setArea(1+"");
			item1.setTotalAmount(ot.getAmount());		
			item1.setPrice(ot.getAmount());
			item1.setVatTax(ot.getTax1());
			item1.setServiceTax(ot.getTax2());
			//total += ot.getAmount();
			logger.log(Level.SEVERE , "Total : " + total);
			salesItem.add(item1);
			}
		}
		for(SalesLineItem it :  salesItem){

			logger.log(Level.SEVERE , "price 1: " + it.getPrice());
		}
		billingDocument.setContractCount(grn.getPoNo());
		billingDocument.setRefNumber(grn.getCount()+"");
		//billingDocument.setSalesOrderProductFromBilling(productlist);
		billingDocument.setSalesOrderProducts(productlist);
		logger.log(Level.SEVERE , "size : " + salesItem.size());
		  ArrayList<PaymentTerms> arrPayTerms = new ArrayList<PaymentTerms>();
		  PaymentTerms pt = new PaymentTerms();
		  pt.setPayTermDays(0);
		  pt.setPayTermPercent(100.0d);
		  pt.setPayTermComment("Payment");
		  arrPayTerms.add(pt);
		  billingDocument.setArrPayTerms(arrPayTerms);
		  
		  if(cust!=null){
				if(cust.getArticleTypeDetails()!=null){  
					for(ArticleType object:cust.getArticleTypeDetails()){
						if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
							billingDocument.setGstinNumber(object.getArticleTypeValue());
							break;
						}
					}
				}
			}
		  PersonInfo personInfo =new PersonInfo();
		  personInfo.setCount(grn.getVendorInfo().getCount());
		  personInfo.setCellNumber(grn.getVendorInfo().getCellNumber());
		  personInfo.setFullName(grn.getVendorInfo().getFullName());
		  personInfo.setPocName(grn.getVendorInfo().getPocName());
		  billingDocument.setPersonInfo(personInfo);
		  billingDocument.setStatus(GRN.CREATED);
		  billingDocument.setCompanyId(companyId);
		  billingDocument.setBranch(grn.getBranch());
		  billingDocument.setAccountType("AP");
		 // billingDocument.setInvoiceType("Tax Invoice");
		  billingDocument.setApproverName(grn.getApproverName());
		  List<ProductOtherCharges> list = new ArrayList<ProductOtherCharges>();
		  List<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
		  try {
			list.addAll(serverapp.addProdTaxes(salesItem , taxList , AppConstants.ACCOUNTTYPEAP));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
			double assessValue=0;
			double tax = 0;
			for(int i=0;i<list.size();i++){
				ContractCharges taxDetails=new ContractCharges();
				taxDetails.setTaxChargeName(list.get(i).getChargeName());
				taxDetails.setTaxChargePercent(list.get(i).getChargePercent());
				assessValue=list.get(i).getAssessableAmount();//*paymentRecieved/100;
				taxDetails.setTaxChargeAssesVal(assessValue);				
				taxDetails.setIdentifyTaxCharge(list.get(i).getIndexCheck());
				tax = tax + taxDetails.getTaxChargeAssesVal()*taxDetails.getTaxChargePercent()/100;
				arrBillTax.add(taxDetails);
			}
		
		  billingDocument.setTotalOtherCharges(otherCharges);
		  billingDocument.setBillingTaxes(arrBillTax);
		  billingDocument.setTotalBillingAmount(Math.round(total+tax+otherCharges));
		  billingDocument.setFinalTotalAmt(Math.round(total));
		  billingDocument.setTotalAmtIncludingTax(Math.round(total+tax+otherCharges));
		  billingDocument.setTotalSalesAmount(Math.round(total+tax+otherCharges));
		//  billingDocument.setTotalAmtExcludingTax(Math.round(salesLineItem.getTotalAmount()));
		  //billingDocument.setNetPayable(Math.round(total+tax));
		  billingDocument.setGrandTotalAmount(Math.round(total+tax+otherCharges));
		  billingDocument.setTotalAmount(Math.round(total));
		  billingDocument.setPaymentDate(new Date());
		  billingDocument.setInvoiceDate(new Date());
		  billingDocument.setBillingDate(new Date());
		  billingDocument.setInvoiceAmount(Math.round(total+tax+otherCharges));
		  billingDocument.setTypeOfOrder(AppConstants.ORDERTYPEPURCHASE);
		  logger.log(Level.SEVERE , "Total  : " + total +"  "+ (total+tax));
	
		  /** Date 12-08-2020 by Vijay for sasha PO's Supplier's Ref./Order No mapping into Billingdocument the same for the report***/
		  billingDocument.setReferenceNumer(grn.getRefNo3());
		  /** Date 12-08-2020 by Vijay for sasha GRN's Receipt Note No into Billingdocument the same for the report***/
		  billingDocument.setReferenceNumber2(grn.getRefNo());
		  /**
		   * ends here
		   */
		  if(grn.getEmployee()!=null){
			  billingDocument.setEmployee(grn.getEmployee());
		  }
		  
		GenricServiceImpl service = new GenricServiceImpl();
		service.save(billingDocument);
	}
//	@OnSave
	@GwtIncompatible
	private void reactOnSave(){
		/** date 14.10.2018 added by komal for sasha (auto generate po number)**/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("GRN", "MonthwiseNumberGeneration", this.getCompanyId())){
//		if(id == null){
			DecimalFormat df=new DecimalFormat("000");
			ServerAppUtility utility = new ServerAppUtility();
			SimpleDateFormat mmm = new SimpleDateFormat("MMM");
			mmm.setTimeZone(TimeZone.getTimeZone("IST"));
			String month = mmm.format(this.getCreationDate());
			String year = "";
			try {
				year = utility.getFinancialYearFormat(this.getCreationDate(), true);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String number = "MRN/"+month+"/"+year;
			ProcessConfiguration config = new ProcessConfiguration();
			config= ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", companyId)
					.filter("processName", number).first().now();
			int cnt = 1;
				if (config !=null) {

					for (int i = 0; i < config.getProcessList().size(); i++) {
						try{
							cnt = Integer.parseInt(config.getProcessList().get(i).getProcessType());
							cnt = cnt +1;
							config.getProcessList().get(i).setProcessType(cnt+"");
						}catch(Exception e){
							cnt = 0;
						}
					}

				}
				else{
					config = new ProcessConfiguration();
					config.setCompanyId(this.companyId);
					config.setProcessName(number);
					config.setConfigStatus(true);
					ArrayList<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
					ProcessTypeDetails type = new ProcessTypeDetails();
					type.setProcessName(number);
					type.setProcessType(cnt+"");
					type.setStatus(true);
					processList.add(type);		
					config.setProcessList(processList);
				}
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(config);
				if(cnt <= 999){
					/********30-1-2019  added by amol for MRN to GRN change************/
					this.setRefNo("GRN/"+month.toUpperCase()+"/"+df.format(cnt)+"/"+year);
				}else{
					this.setRefNo("GRN/"+month.toUpperCase()+"/"+cnt+"/"+year);
				}
//			}
		}

	}

	public int getPrNumber() {
		return prNumber;
	}


	public void setPrNumber(int prNumber) {
		this.prNumber = prNumber;
	}

	@GwtIncompatible
	public void updatePR(int prNumber) {
		Logger logger = Logger.getLogger("Logger");
		System.out.println("GRN Recieved");
		logger.log(Level.SEVERE, "GRN Recieved Update PR Status");

		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableAutomaticPROrderQty", this.getCompanyId())){
			PurchaseRequisition purchaseReq = ofy().load().type(PurchaseRequisition.class).filter("count", prNumber)
					.filter("companyId", this.getCompanyId()).first().now();
			logger.log(Level.SEVERE, "purchaseReq for PR");

			if(purchaseReq!=null){
				for(GRNDetails grndetails: this.getInventoryProductItem()){
					for(ProductDetails prDetails : purchaseReq.getPrproduct())
					if(grndetails.getProductID()==prDetails.getProductID()
					  && grndetails.getWarehouseName().equals(prDetails.getWarehouseName())){
						prDetails.setGrnQtyReceived(prDetails.getGrnQtyReceived()+grndetails.getProductQuantity());
					}
				}
				ofy().save().entity(purchaseReq);
			}
			/**
			 * Date 13-09-2019 by Vijay
			 * Des :- NBHC Inventory Management Lock seal App GRN PR Close 
			 */
			else{
				List<PurchaseRequisition> purchaseReqlist = ofy().load().type(PurchaseRequisition.class)
						.filter("companyId", this.getCompanyId()).filter("purchasedetails.branch", this.getBranch())
						.filter("purchasedetails.status", PurchaseRequisition.APPROVED).list();
				logger.log(Level.SEVERE, "purchaseReqlist "+purchaseReqlist.size());
				if(purchaseReqlist.size()!=0){
					Comparator<PurchaseRequisition> purchaseReqcomparator = new Comparator<PurchaseRequisition>() {
						
						@Override
						public int compare(PurchaseRequisition purchase, PurchaseRequisition purchase2) {
							Date date = purchase.getCreationDate();
							Date date2 = purchase2.getCreationDate();
							return date.compareTo(date2);
						}
					};
					Collections.sort(purchaseReqlist,purchaseReqcomparator);
					
					for (GRNDetails grndetails : this.getInventoryProductItem()) {
						boolean flag = false;
						double grnQty = grndetails.getProductQuantity();
						for (PurchaseRequisition purchaseReqn : purchaseReqlist) {

							for (ProductDetails prDetails : purchaseReqn.getPrproduct()) {

								if (grndetails.getProductID() == prDetails.getProductID()
										&& grndetails.getWarehouseName().equals(prDetails.getWarehouseName())) {
									System.out.println("grnQty"+grnQty);
									if (prDetails.getGrnBalancedQty() >= grnQty) {
										double grnBalancedQty = Math
												.abs(prDetails.getGrnBalancedQty() - grnQty);
										prDetails.setGrnBalancedQty(grnBalancedQty);
										System.out.println("grnBalancedQty =="+grnBalancedQty);
										flag = true;
										ofy().save().entity(purchaseReqn);
										logger.log(Level.SEVERE, "Purchase updated successfully");
										break;
									} else {
										grnQty = Math.abs(prDetails.getGrnBalancedQty() - grnQty);
										System.out.println("grnBalancedQty" + grnQty);
										if (grnQty > 0) {
											prDetails.setGrnBalancedQty(0);
											ofy().save().entity(purchaseReqn);
											logger.log(Level.SEVERE, "Purchase updated successfully");
										} else {
											flag = true;
											break;
										}

									}

								}
							}

							if (flag) {
								break;
							}
						}

					}
					
				}
			}
		}
	}
	
	
	@GwtIncompatible
	public void updatePRStatus(int prNumber, int poNumber) {
		
		PurchaseRequisition purchaserequision = ofy().load().type(PurchaseRequisition.class)
				.filter("companyId", this.getCompanyId()).filter("count", prNumber)
				.first().now();
		
		boolean flag = false;
		
		if(purchaserequision!=null){
			if(purchaserequision.getPrproduct().size()==1){
				purchaserequision.setStatus(PurchaseRequisition.CLOSE);
			ofy().save().entity(purchaserequision);
			}else{
				ArrayList<String> statuslist = new ArrayList<String>();
				statuslist.add(GRN.CREATED);
				statuslist.add(GRN.REQUESTED);
				List<GRN> grnlist = ofy().load().type(GRN.class).filter("companyId", this.getCompanyId())
						.filter("poNo", poNumber).filter("status IN", statuslist).list();
				System.out.println("grnlist =="+grnlist.size());
				if(grnlist.size()==1){
					purchaserequision.setStatus(PurchaseRequisition.CLOSE);
					ofy().save().entity(purchaserequision);
				}
			}
		}
		
		
	}
	
	/**
	 * @author Vijay Chougule Date 22-06-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getPoCreationDate()!=null){
			this.setPoCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getPoCreationDate()));
		}
	}
	/**
	 * ends here	
	 */


	public String getRefNo3() {
		return refNo3;
	}


	public void setRefNo3(String refNo3) {
		this.refNo3 = refNo3;
	}
	
	
	@GwtIncompatible
	private void reactOnSendMessage() {
		
		 Logger logger = Logger.getLogger("NameOfYourLogger");
		 SMSDetails smsdetails = new SMSDetails();
		 smsdetails.setModel(this);
		 SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", AppConstants.GRNAPPROVAL).filter("status",true).first().now();
		 
		 if(smsEntity!=null) {
			 smsdetails.setSmsTemplate(smsEntity);
			 smsdetails.setEntityName("GRN");
			 
			 String mobileNo=Long.toString(this.getVendorInfo().getCellNumber());
			 smsdetails.setMobileNumber(mobileNo);
			 smsdetails.setCommunicationChannel(AppConstants.SMS);
			 smsdetails.setModuleName(AppConstants.INVENTORYMODULE);
			 smsdetails.setDocumentName(AppConstants.GRN);
			 smsdetails.setModel(this);
			 SmsServiceImpl sendImpl = new SmsServiceImpl();
			 String smsmsg = sendImpl.validateAndSendSMS(smsdetails);
			 logger.log(Level.SEVERE," inside validateAndSendSMS"+smsmsg);
		 }
		 
	}



	
}
