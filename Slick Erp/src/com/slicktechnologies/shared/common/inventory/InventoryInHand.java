package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Saves Inventory Current State.At any time for a Given Location and For a Given Product
 * Value of {@link InventoryInHand} is Current qty of that Product.
 */
@Entity
public class InventoryInHand extends SuperModel
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5729740864539950340L;
	
	/** Product of Inventory */
	@Index
	protected InventoryProductDetail item;
	
	/** The qty. */
	protected double qty;
	
	/** The cost of inventory. */
	protected double costOfInventory;
	
	/** Used as Primary Key to distinguish objects.*/
	@Index
	protected String lPcode;
	
	/**
	 * Instantiates a new inventory in hand.
	 */
	public InventoryInHand() {
		super();
		lPcode="";
		item=new InventoryProductDetail();
		
	}

	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public InventoryProductDetail getItem() {
		return item;
	}

	/**
	 * Sets the item.
	 *
	 * @param item the new item
	 */
	public void setItem(InventoryProductDetail item) {
		this.item = item;
	}

	
	/**
	 * Gets the qty.
	 *
	 * @return the qty
	 */
	public double getQty() {
		return qty;
	}

	
	/**
	 * Sets the qty.
	 *
	 * @param qty the new qty
	 */
	public void setQty(double qty) {
		this.qty = qty;
	}

	
	/**
	 * Gets the cost of inventory.
	 *
	 * @return the cost of inventory
	 */
	public double getCostOfInventory() {
		return costOfInventory;
	}

	/**
	 * Sets the cost of inventory.
	 *
	 * @param costOfInventory the new cost of inventory
	 */
	public void setCostOfInventory(double costOfInventory) {
		this.costOfInventory = costOfInventory;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object arg0) {
		return item.equals(arg0);
	}

	/**
	 * Sets the ware house location name.
	 *
	 * @param buisnessunitName the new ware house location name
	 */
	public void setWareHouseLocationName(String buisnessunitName) {
		item.setWarehouseLocation(buisnessunitName);
	}
	
	

	
	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public int getProductId() {
		return item.getProductID();
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId the new product id
	 */
	public void setProductId(int productId) {
		item.setProductID(productId);
	}

	
	/**
	 * Gets the product category.
	 *
	 * @return the product category
	 */
	public String getProductCategory() {
		return item.getProductCategory();
	}

	/**
	 * Sets the product category.
	 *
	 * @param productCategory the new product category
	 */
	public void setProductCategory(String productCategory) {
		item.setProductCategory(productCategory);
	}

	
	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return item.getProductCode();
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		item.setProductCode(productCode);
	}

	
	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return item.getProductName();
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		item.setProductName(productName);
	}

	
	/**
	 * Gets the unit of measurment.
	 *
	 * @return the unit of measurment
	 */
	public String getUnitOfMeasurment() {
		return item.getUnitOfmeasurement();
	}

	/**
	 * Sets the unit of measurment.
	 *
	 * @param unitOfMeasurment the new unit of measurment
	 */
	public void setUnitOfMeasurment(String unitOfMeasurment) {
		item.setUnitOfmeasurement(unitOfMeasurment);
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Double getPrice() {
		return item.getProdPrice();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return item.hashCode();
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(Double price) {
		item.setProdPrice(price);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return item.toString();
	}

	

	/**
	 * Gets the ware house location name.
	 *
	 * @return the ware house location name
	 */
	public String getWareHouseLocationName() {
		return item.getWarehouseLocation();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Gets the l pcode.
	 *
	 * @return the l pcode
	 */
	public String getlPcode() {
		return lPcode;
	}

	/**
	 * Sets the l pcode.
	 *
	 * @param lPcode the new l pcode
	 */
	public void setlPcode(String lPcode) 
	{
		if(this.lPcode!=null)
		   this.lPcode = lPcode.trim();
	}

	public String getStorageBin() {
		return item.getStorageBin();
	}

	public void setStorageBin(String storageBin) {
		item.setStorageBin(storageBin);
	}

	public String getStorageLoc() {
		return item.getStorageLoc();
	}

	public void setStorageLoc(String storageLoc) {
		item.setStorageLoc(storageLoc);
	}
	
	
	
	
	

}
