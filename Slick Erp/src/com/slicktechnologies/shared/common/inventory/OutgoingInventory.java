package com.slicktechnologies.shared.common.inventory;

import java.util.Date;


import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;

import static  com.googlecode.objectify.ObjectifyService.ofy;


/**
 * The Class IncomingInventory.Repersents the incoming inventory item.It has doc reference 
 * to distinguish between type of document to which the item corresponds.
 */
@SuppressWarnings("unused")
@Entity
public class OutgoingInventory extends SuperModel
{

	/***********************Attributes************************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6346310095102561964L;
	/** Product of the inventory Item */
	@Index
	protected InventoryProductDetail itemProduct;
	
	/** Quantity of Inventory Item */
	double qty;
	/** Repersents the type of Doc corresponding to this inventory item */
	protected String docType;
	
	@Index
	protected Date creationDate;
	/** Document Id */
	int docId;
	
	/***********************Relational Part************************************************************/
	/** Key of The Document corresponding to this Inventory Item */
	Key<?>docKey;
	/** Key of The Product corresponding to this Inventory Item */
	Key<InventoryProductDetail>prodKey;
	
	Key<LocationRelation>wareHouseKey;
	
	/***********************Constructor************************************************************/
	/**
	 * Instantiates a new in inventory.
	 */
	public OutgoingInventory() {
		super();
		docType="";
		
		itemProduct=new InventoryProductDetail();
	}
	
	/*************************************Getters Setters***************************************/


	/**
	 * Gets the item product.
	 *
	 * @return the item product
	 */
	public InventoryProductDetail getItemProduct() {
		return itemProduct;
	}










	/**
	 * Sets the item product.
	 *
	 * @param itemProduct the new item product
	 */
	public void setItemProduct(InventoryProductDetail itemProduct) {
		this.itemProduct = itemProduct;
	}










	/**
	 * Gets the qty.
	 *
	 * @return the qty
	 */
	public double getQty() {
		return qty;
	}










	/**
	 * Sets the qty.
	 *
	 * @param qty the new qty
	 */
	public void setQty(double qty) {
		this.qty = qty;
	}










	public int getProductId() {
		return itemProduct.getProductID();
	}

	public void setProductId(int productId) {
		itemProduct.setProductID(productId);
	}

	public String getProductCategory() {
		return itemProduct.getProductCategory();
	}

	public void setProductCategory(String productCategory) {
		itemProduct.setProductCategory(productCategory);
	}

	public String getProductCode() {
		return itemProduct.getProductCode();
	}

	public void setProductCode(String productCode) {
		itemProduct.setProductCode(productCode);
	}

	public String getProductName() {
		return itemProduct.getProductName();
	}

	public void setProductName(String productName) {
		itemProduct.setProductName(productName);
	}

	public String getUnitOfMeasurment() {
		return itemProduct.getUnitOfmeasurement();
	}

	public void setUnitOfMeasurment(String unitOfMeasurment) {
		itemProduct.setUnitOfmeasurement(unitOfMeasurment);
	}

	public Double getPrice() {
		return itemProduct.getProdPrice();
	}

	public void setPrice(Double price) {
		itemProduct.setProdPrice(price);
	}

	/**
	 * Gets the doc key.
	 *
	 * @return the doc key
	 */
	public Key<?> getDocKey() {
		return docKey;
	}










	public String getWarehouseLocation() {
		return itemProduct.getWarehouseLocation();
	}

	public void setWarehouseLocation(String warehouseLocation) {
		itemProduct.setWarehouseLocation(warehouseLocation);
	}

	/**
	 * Sets the doc key.
	 *
	 * @param docKey the new doc key
	 */
	public void setDocKey(Key<?> docKey) {
		this.docKey = docKey;
	}










	/**
	 * Gets the doc type.
	 *
	 * @return the doc type
	 */
	public String getDocType() {
		
		return docType;
	}

	/**
	 * Sets the doc type.
	 *
	 * @param docType the new doc type
	 */
	public void setDocType(String docType) {
		if(docType!=null)
		this.docType = docType.trim();
	}
    /* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
	
		return false;
	}
	
	
	
	

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Key<InventoryProductDetail> getProdKey() {
		return prodKey;
	}

	public void setProdKey(Key<InventoryProductDetail> prodKey) {
		this.prodKey = prodKey;
	}

	public Key<LocationRelation> getWareHouseKey() {
		return wareHouseKey;
	}

	public void setWareHouseKey(Key<LocationRelation> wareHouseKey) {
		this.wareHouseKey = wareHouseKey;
	}
	
	
	

	public int getDocId() {
		return docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	/*************************************Relation Managment***************************************/
  
	
	@GwtIncompatible
	@OnSave
	public void manageRelationonSave()
	{
		if(itemProduct.getWarehouseLocation()!=null&&companyId!=null)
			wareHouseKey=ofy().load().type(LocationRelation.class).filter("locationName",itemProduct.getWarehouseLocation()).
			filter("companyId",getCompanyId()).keys().first().now();
		if(itemProduct.getWarehouseLocation()!=null&&companyId==null)
				wareHouseKey=ofy().load().type(LocationRelation.class).filter("locationName",itemProduct.getWarehouseLocation()).
				keys().first().now();
		
		
		
	}
	
	@GwtIncompatible
	@OnLoad
	public void manageRelationonLoad()
	{
		if(wareHouseKey!=null)
		{
          String loc=ofy().load().key(wareHouseKey).now().getName();
          itemProduct.setWarehouseLocation(loc);
		}
		
	}
}
