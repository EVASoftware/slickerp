package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;


/**
 * Used for manually adjusting the content of Inventory.
 */
@Entity
public class CountSheet extends InventoryTransaction
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8231469176482639981L;
	
	/***********************Attributes************************************************************/
	/** The count sheet name. */
	@Index
	protected String countSheetName;
	
	@Index
	protected String wareHouseName;
	
	@Serialize
	ArrayList<CountSheetDetails>items;
	
	public static final String CANCEL="Cancelled";
	public static final String SUBMITED="Submitted";
	public static final String CREATED="Created";
	
	/***********************Relational Part************************************************************/
	Key<LocationRelation>wareHouseKey;
	ArrayList<Key<InventoryProductDetail>>productDetailsKey;
	
	/***********************Constructor************************************************************/
	
public CountSheet() {
		super();
		wareHouseName="";
		countSheetName="";
		items=new ArrayList<CountSheetDetails>();
		
	}


	public String getCountSheetName() {
		return countSheetName;
	}

	public void setCountSheetName(String countSheetName) {
		if(countSheetName!=null)
		this.countSheetName = countSheetName.trim();
	}

	
	public String getWareHouseName() {
		return wareHouseName;
	}

	public void setWareHouseName(String wareHouseName) {
		if(wareHouseName!=null)
		    this.wareHouseName = wareHouseName.trim();
	}

	public ArrayList<CountSheetDetails> getItems() {
		return items;
	}

	public void setItems(List<CountSheetDetails> list) {
		if(list!=null)
		{
			this.items=new ArrayList<CountSheetDetails>();
			this.items.addAll(list);
		
		}
	}
	
	@Override
	public List<? extends InventoryProductDetail> getProductDetails() {
		
		return this.items;
	}


	@Override
	public int getTransectionType() {
		// TODO Auto-generated method stub
		return InventoryTransaction.UPDATETRANSECTION;
	}
	


	/*************************************Relation Managment***************************************/

	
	@GwtIncompatible
	@OnSave
	public void manageRelationonSave()
	{
		if(wareHouseName!=null&&companyId!=null)
			wareHouseKey=ofy().load().type(LocationRelation.class).filter("locationName",wareHouseName).
			filter("companyId",getCompanyId()).keys().first().now();
		if(wareHouseName!=null&&companyId==null)
			wareHouseKey=ofy().load().type(LocationRelation.class).filter("locationName",wareHouseName).
				keys().first().now();
		
		
		
	}
	
	@GwtIncompatible
	@OnLoad
	public void manageRelationonLoad()
	{
		if(wareHouseKey!=null)
		{
			wareHouseName=ofy().load().key(wareHouseKey).now().getName();
		}
		
		
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		CountSheet entity = (CountSheet) m;
		String name = entity.getCountSheetName();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.getCountSheetName().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(this.id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	
		
	public static List<String> getStatusList() {
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(CREATED);
		
		statusListBox.add(SUBMITED);
		statusListBox.add(CANCEL);
		return statusListBox;
	}


	@Override
	public String toString() 
	{
		return countSheetName;
	}


	@Override
	public String getDocType() {
		// TODO Auto-generated method stub
		return InventoryEntries.COUNTSHEET;
	}


	
	

}
