package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.relationalLayer.LocationRelation;


/**
 * Class Which Keeps track record of Inventory Transections.
 */
@SuppressWarnings("unused")
@Entity

public class InventoryEntries extends SuperModel
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1135573133968558261L;

	
	/***********************Attributes************************************************************/
	/** ID OF Document */
	@Index
	int docId;
	
	/** Type of Inventory Transaction Document */
	@Index
	String docType;
	
	/** Approver Name  */
	@Index
	String approvedBy;
	
	
	
	/** The transection date. */
	@Index
	Date transectionDate;
	@Index
	String employee;
	@Index
	String branchName;
	
	public static final String GRN="Grn";
	public static final String DELIVERYNOTE="Delivery Note";
	public static final String ISSUENOTE="Issue Note";
	public static final String COUNTSHEET="Count Sheet";


	public static final String TRANSFER ="Transfer";
	/***********************Relational Part************************************************************/
	
	Key<EmployeeRelation>approvalKey;
	
	Key<EmployeeRelation> employeeKey;
	
	
	Key<BranchRelation> branchKey;
	
	
	public InventoryEntries() {
		super();
		branchName="";
		employee="";
		approvedBy="";
		docType="";
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
	
		return false;
	}
	
/***************************************Getter/Setters*****************************************/
	public int getDocId() {
		return docId;
	}

	
	public void setDocId(int docId) {
		this.docId = docId;
	}

	public String getDocType() 
	{
		return docType;
	}


	public void setDocType(String docType) {
		if(docType!=null)
		this.docType = docType.trim();
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	
	
	public void setApprovedBy(String approvedBy) {
		if(approvedBy!=null)
		this.approvedBy = approvedBy.trim();
	}


	

	
	
	
	public Date getTransectionDate() {
		return transectionDate;
	}


	public void setTransectionDate(Date transectionDate) {
		this.transectionDate = transectionDate;
	}

	
	
	public String getEmployee() {
		return employee;
	}


	public void setEmployee(String employee) {
		if(employee!=null)
		this.employee = employee.trim();
	}

	
	public String getBranchname() {
		return branchName;
	}


	public void setBranchname(String branchname) {
		if(branchname!=null)
		this.branchName = branchname.trim();
	}


	public Key<EmployeeRelation> getApprovalKey() {
		return approvalKey;
	}


	public void setApprovalKey(Key<EmployeeRelation> approvalKey) {
		this.approvalKey = approvalKey;
	}


	public Key<EmployeeRelation> getEmployeeKey() {
		return employeeKey;
	}


	public void setEmployeeKey(Key<EmployeeRelation> employeeKey) {
		this.employeeKey = employeeKey;
	}


	public Key<BranchRelation> getBranchRelation() {
		return branchKey;
	}


	public void setBranchRelation(Key<BranchRelation> branchRelation) {
		this.branchKey = branchRelation;
	}

/*************************************Relation Managment***************************************/
  
	
	@GwtIncompatible
	@OnSave
	public void manageRelationonSave()
	{
		if(approvedBy!=null&&companyId!=null)
			approvalKey=ofy().load().type(EmployeeRelation.class).filter("employeeName",approvedBy).
			filter("companyId",getCompanyId()).keys().first().now();
		
		
		
	}
	
	

	@GwtIncompatible
	@OnLoad
	public void manageRelationonLoad()
	{
		if(approvalKey!=null)
		{
			EmployeeRelation emp=ofy().load().key(approvalKey).now();
			if(emp!=null)
				emp.getEmployeeName();
		}
		
	}
	public static List<String> getDocumnetTypeList() {
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(COUNTSHEET);
		statusListBox.add(ISSUENOTE);
		statusListBox.add(DELIVERYNOTE);
		statusListBox.add(GRN);
		return statusListBox;
	}
	
	
  
}
