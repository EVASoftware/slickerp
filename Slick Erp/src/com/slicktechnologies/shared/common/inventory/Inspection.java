package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class Inspection extends ConcreteBusinessProcess {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5373195185797081438L;
	
	/**************************************Constants*************************************************/
	
	public static final String CREATED="Created";
	public static final String REQUESTED = "Requested";
	public static final String APPROVED="Approved";
	public static final String REJECTED="Rejected";
	
	/****************************************Applicability Attributes***********************************/
	
	@Index
	protected String inspectionTitle;
	@Index
	protected String inspectionProject;
	@Index
	protected String inspectionRefNo;
	@Index
	protected Date inspectionRefDate;
	@Index
	protected int inspectionGrnId;
	@Index
	protected String inspectionGrnTitle;
	@Index
	protected Date inspectionGrnDate;
	@Index
	protected int inspectionPOid;
	@Index
	protected DocumentUpload upload;
	@Index
	PersonInfo vendorInfo;
	ArrayList<InspectionDetails>items;
	protected String inspectionDescription;
	
	
	/****************************************Constructor*************************************************/
	
	public Inspection() {
		inspectionTitle="";
		inspectionProject="";
		inspectionRefNo="";
		inspectionGrnTitle="";
		upload=new DocumentUpload();
		items=new ArrayList<InspectionDetails>();
		inspectionDescription="";
		vendorInfo=new PersonInfo();
	}
	
	/****************************************Getters And Setters*****************************************/

	public String getInspectionTitle() {
		return inspectionTitle;
	}

	public void setInspectionTitle(String inspectionTitle) {
		if(inspectionTitle!=null){
			this.inspectionTitle = inspectionTitle.trim();
		}
	}

	public String getInspectionProject() {
		return inspectionProject;
	}

	public void setInspectionProject(String inspectionProject) {
		if(inspectionProject!=null){
			this.inspectionProject = inspectionProject.trim();
		}
	}

	public String getInspectionRefNo() {
		return inspectionRefNo;
	}

	public void setInspectionRefNo(String inspectionRefNo) {
		if(inspectionRefNo!=null){
			this.inspectionRefNo = inspectionRefNo.trim();
		}
	}

	public Date getInspectionRefDate() {
		return inspectionRefDate;
	}

	public void setInspectionRefDate(Date inspectionRefDate) {
		if(inspectionRefDate!=null){
			this.inspectionRefDate = inspectionRefDate;
		}
	}

	public int getInspectionGrnId() {
		return inspectionGrnId;
	}

	public void setInspectionGrnId(int inspectionGrnId) {
		this.inspectionGrnId = inspectionGrnId;
	}

	public String getInspectionGrnTitle() {
		return inspectionGrnTitle;
	}

	public void setInspectionGrnTitle(String inspectionGrnTitle) {
		if(inspectionGrnTitle!=null){
			this.inspectionGrnTitle = inspectionGrnTitle;
		}
	}

	public Date getInspectionGrnDate() {
		return inspectionGrnDate;
	}

	public void setInspectionGrnDate(Date inspectionGrnDate) {
		if(inspectionGrnDate!=null){
			this.inspectionGrnDate = inspectionGrnDate;
		}
	}

	public PersonInfo getVendorInfo() {
		return vendorInfo;
	}

	public void setVendorInfo(PersonInfo vendorInfo) {
		this.vendorInfo = vendorInfo;
	}

	public ArrayList<InspectionDetails> getItems() {
		return items;
	}

	public void setItems(List<InspectionDetails> list) {
		
		ArrayList<InspectionDetails> arrSSteps=new ArrayList<InspectionDetails>();
		arrSSteps.addAll(list);
		this.items = arrSSteps;
	}

	public String getInspectionDescription() {
		return inspectionDescription;
	}

	public void setInspectionDescription(String inspectionDescription) {
		if(inspectionDescription!=null){
			this.inspectionDescription = inspectionDescription.trim();
		}
	}

	public DocumentUpload getUpload() {
		return upload;
	}

	public void setUpload(DocumentUpload upload) {
		if(upload!=null){
			this.upload = upload;
		}
	}
	
	public int getInspectionPOid() {
		return inspectionPOid;
	}

	public void setInspectionPOid(int inspectionPOid) {
		this.inspectionPOid = inspectionPOid;
	}

/***************************************Status List***********************************************/
	
	public static List<String> getStatusList() {
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(REQUESTED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		return statusListBox;
	}
	
	/*******************************************Business Logic*********************************************/

	@Override
	@GwtIncompatible
	public void reactOnRejected() {
		super.reactOnRejected();
		GRN grn=ofy().load().type(GRN.class).filter("companyId", this.getCompanyId()).filter("count", this.getInspectionGrnId()).first().now();
		if(getStatus().equals(ConcreteBusinessProcess.REJECTED))
		{
			System.out.println("Inside Reject.......");
			if(grn!=null){
				System.out.println("Inside Reject GRN .......");
				grn.setStatus(GRN.CREATED);
				grn.setInspectionId(0);
				ofy().save().entity(grn).now();
			}
		}
	}

	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		GRN grn=ofy().load().type(GRN.class).filter("companyId", this.getCompanyId()).filter("count", this.getInspectionGrnId()).first().now();
		if(getStatus().equals(ConcreteBusinessProcess.APPROVED))
		{
			double productQty=0;
			double rejectedQty=0;
			
			int counter=0;
			int rejectionCounter=0;
			
			for(int i=0;i<getItems().size();i++){
				productQty=getItems().get(i).getAcceptedQty();
				rejectedQty=getItems().get(i).getRejectedQty();
				
				if(getItems().get(i).getAcceptedQty()==0){
					counter++;
				}
				if(getItems().get(i).getRejectedQty()!=0){
					rejectionCounter++;
				}
				if(grn!=null){
					for(int j=0;j<grn.getInventoryProductItem().size();j++){
						if(getItems().get(i).getProductID()==grn.getInventoryProductItem().get(j).getProductID()){
							grn.getInventoryProductItem().get(j).setProductQuantity(productQty);
						}
					}
				}
			}
			
			if(grn!=null){
				if(counter==getItems().size()){
					grn.setStatus(GRN.CLOSED);
					ofy().save().entity(grn).now();
				}else{
					grn.setStatus(GRN.INSPECTIONAPPROVED);
					ofy().save().entity(grn).now();
				}
			}
			
			if(rejectionCounter!=0){
				GRN grnEntity=new GRN();
				ArrayList<GRNDetails> arrGrn=new ArrayList<GRNDetails>();
				for(int i=0;i<getItems().size();i++)
				{
					if(getItems().get(i).getRejectedQty()!=0){
						GRNDetails dli=new GRNDetails();
						dli.setProductID(getItems().get(i).getProductID());
						dli.setProductCategory(getItems().get(i).getProductCategory());
						dli.setProductCode(getItems().get(i).getProductCode());
						dli.setProductName(getItems().get(i).getProductName());
						dli.setProductQuantity(getItems().get(i).getRejectedQty());
						dli.setProdPrice(getItems().get(i).getProdPrice());
						dli.setUnitOfmeasurement(getItems().get(i).getUnitOfmeasurement());
						dli.setVat(getItems().get(i).getVat());
						dli.setTax(getItems().get(i).getTax());
						dli.setDiscount(getItems().get(i).getDiscount());
						dli.setTotal(getItems().get(i).getTotal());
						dli.setWarehouseLocation(getItems().get(i).getWarehouseLocation());
						dli.setStorageLoc(getItems().get(i).getStorageLoc());
						dli.setStorageBin(getItems().get(i).getStorageBin());
						arrGrn.add(dli);
					}
				}
				
				grnEntity.setCompanyId(grn.getCompanyId());
				grnEntity.setPoNo(grn.getPoNo());
				grnEntity.setPoTitle(grn.getPoTitle());
				grnEntity.setPoCreationDate(grn.getPoCreationDate());
				grnEntity.setProJectName(grn.getProJectName());
				grnEntity.setTitle(grn.getTitle());
				grnEntity.setEmployee(grn.getEmployee());
				grnEntity.setGrnCreationDate(grn.getGrnCreationDate());
				grnEntity.setStatus(ConcreteBusinessProcess.CREATED);
				grnEntity.setBranch(grn.getBranch());
				grnEntity.setGrnCategory(grn.getGrnCategory());
				grnEntity.setGrnType(grn.getGrnType());
				grnEntity.setGrnGroup(grn.getGrnGroup());
				grnEntity.setApproverName(grn.getApproverName());
				grnEntity.setRefNo(grn.getRefNo());
				grnEntity.setGrnReferenceDate(grn.getGrnReferenceDate());
				grnEntity.setRemark(grn.getRemark());
				grnEntity.setInventoryProductItem(arrGrn);
				grnEntity.setVendorInfo(grn.getVendorInfo());
				grnEntity.setInspectionRequired(grn.getInspectionRequired());
				
				// vijay
				grnEntity.setTitle(grn.getTitle());
				
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(grnEntity);
				
			}
			
		}
		
	}
	

}
