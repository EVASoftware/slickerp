package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.Config;

@Entity
public class MaterialMovementType extends SuperModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8338800537878174360L;
	@Index
	protected String mmtName;
	@Index
	protected String mmtType;
	@Index
	protected boolean mmtStatus;
	
	public MaterialMovementType() {
		
	}
	
	
	public String getMmtName() {
		return mmtName;
	}


	public void setMmtName(String mmtName) {
		this.mmtName = mmtName;
	}


	public String getMmtType() {
		return mmtType;
	}


	public void setMmtType(String mmtType) {
		this.mmtType = mmtType;
	}


	public boolean isMmtStatus() {
		return mmtStatus;
	}


	public void setMmtStatus(boolean mmtStatus) {
		this.mmtStatus = mmtStatus;
	}


	@Override
	public boolean isDuplicate(SuperModel model) {
		MaterialMovementType obj=(MaterialMovementType) model;
		String name = obj.getMmtName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.mmtName.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
		if(obj.id==null)
		{
			if(name.equals(currentName))
				return true;
			else
				return false;
		}
		
		//while updating a Old object
		else
		{
			if(obj.id.equals(id))
				return false;
			else if (name.equals(currentName))
				return true;
			else
				return false;	
		}	
	}


	@Override
	public String toString() {
		return mmtName;
	}
	
	

}
