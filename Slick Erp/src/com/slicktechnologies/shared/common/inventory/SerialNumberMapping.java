package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class SerialNumberMapping implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8008165357946641743L;

	boolean status;
	String proSerialNo;
//	int serialNumber;
	long serialNumber;
	
	public SerialNumberMapping(){
		proSerialNo ="";
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public long getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(long serialNumber) {
		this.serialNumber = serialNumber;
	}

//	public int getSerialNumber() {
//		return serialNumber;
//	}
//
//	public void setSerialNumber(int serialNumber) {
//		this.serialNumber = serialNumber;
//	}
	
	
}
