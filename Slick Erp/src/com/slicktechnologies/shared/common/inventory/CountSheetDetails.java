package com.slicktechnologies.shared.common.inventory;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Repersents Count Sheet Line Items.
 */
@Embed
public class CountSheetDetails extends InventoryProductDetail
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2867878328545994732L;
	
	/** ********************************Attributes*************************************. */
	/** The value of Inventory in System **/
	Double theoreticalInventory;
	
	/** Actual Value Of Inventory  */
	Double countedInventory;
	
	/** The comments. */
	String comments;
	
	/**
	 * Instantiates a new count sheet details.
	 */
	public CountSheetDetails() 
	{
		super();
		comments="";
	}

	/**
	 * ************************Getter/Setters**********************************************.
	 *
	 * @return the theoretical inventory
	 */
	
	public Double getTheoreticalInventory() {
		return theoreticalInventory;
	}

	/**
	 * Sets the theoretical inventory.
	 *
	 * @param theoreticalInventory the new theoretical inventory
	 */
	public void setTheoreticalInventory(Double theoreticalInventory) {
		this.theoreticalInventory = theoreticalInventory;
	}

	/**
	 * Gets the counted inventory.
	 *
	 * @return the counted inventory
	 */
	public Double getCountedInventory() {
		return countedInventory;
	}

	/**
	 * Sets the counted inventory.
	 *
	 * @param countedInventory the new counted inventory
	 */
	public void setCountedInventory(Double countedInventory) {
		this.countedInventory = countedInventory;
		
	}

	
	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Override
	public Double getQty()
	{
		return this.countedInventory;
	}

}
