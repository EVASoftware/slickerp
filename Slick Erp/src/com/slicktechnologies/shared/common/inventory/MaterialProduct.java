package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
@Embed
public class MaterialProduct extends SuperModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3127001135469475155L;
	@Index
	protected int materialProductId;
	@Index
	protected String materialProductCode;
	@Index
	protected String materialProductName;
	@Index
	protected String materialProductCategory;
	@Index
	protected double materialProductAvailableQuantity;
	@Index
	protected double materialProductRequiredQuantity;
	@Index
	protected String materialProductUOM;
	@Index
	protected String materialProductRemarks;
	@Index
	protected String materialProductWarehouse;
	@Index
	protected String materialProductStorageLocation;
	@Index
	protected String materialProductStorageBin;
	
	@Index
	protected double matReqReOdrLvlQty;
	
	 /**date 1/1/2018 added by komal for defective material credit **/
		@Index
		protected boolean isCreditReceived;
		 /**date 22/1/2018 added by komal for hvac **/	
		@EmbedMap
		@Serialize
		protected Map<String ,ArrayList<CompanyAsset>> companyAsset;
		
		/**
		 * nidhi 21-08-2018 for map serial number details
	 */
		@EmbedMap
	  @Serialize
	  protected HashMap<Integer, ArrayList<ProductSerialNoMapping>>  proSerialNoDetails  = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

		/**
		 * nidhi ||*||
		 * @return for map planned qty
		 */
		protected double materialProductPlannedQuantity = 0;	
		
		public boolean isCreditReceived() {
			return isCreditReceived;
		}
		public void setCreditReceived(boolean isCreditReceived) {
			this.isCreditReceived = isCreditReceived;
		}
		
		/**
		 * Date 11-01-2019 by Vijay
		 * Des :- For NBHC Inventory management to create MMN doc for balance mmn qty
		 * maintaining in MMN 
		 */
		double materialProductBalanceQty;
		/**
		 * ends here
		 */
		
		/**
		 * Date 16-04-2019 by Vijay
		 * Des :- NBHC CCPM Material Consumption Data to NBHC DB through interface with branch
		 * so i need this field for logic in MINConsumptionCronJobImpl this field value set their only. 
		 */
		String materialBranch;
		
		/**
		 * Date 09-05-2019 by Vijay
		 * Des :- NBHC Inventory Management lock seal No
		 */
		@Index
		String lockSealSerialNo;
		
	public MaterialProduct() {
		super();
		materialProductRequiredQuantity=1;
		materialProductRemarks="";
		/**date 22/1/2018 added by komal for hvac **/
		companyAsset = new HashMap<String ,ArrayList<CompanyAsset>>();
		materialBranch = "";
		lockSealSerialNo="";
	}

	
	
	
	public String getMaterialProductWarehouse() {
		return materialProductWarehouse;
	}



	public void setMaterialProductWarehouse(String materialProductWarehouse) {
		this.materialProductWarehouse = materialProductWarehouse;
	}



	public String getMaterialProductStorageLocation() {
		return materialProductStorageLocation;
	}



	public void setMaterialProductStorageLocation(
			String materialProductStorageLocation) {
		this.materialProductStorageLocation = materialProductStorageLocation;
	}



	public String getMaterialProductStorageBin() {
		return materialProductStorageBin;
	}



	public void setMaterialProductStorageBin(String materialProductStorageBin) {
		this.materialProductStorageBin = materialProductStorageBin;
	}



	public int getMaterialProductId() {
		return materialProductId;
	}


	public void setMaterialProductId(int materialProductId) {
		this.materialProductId = materialProductId;
	}


	public String getMaterialProductCode() {
		return materialProductCode;
	}


	public void setMaterialProductCode(String materialProductCode) {
		this.materialProductCode = materialProductCode;
	}


	public String getMaterialProductName() {
		return materialProductName;
	}


	public void setMaterialProductName(String materialProductName) {
		this.materialProductName = materialProductName;
	}


	public String getMaterialProductCategory() {
		return materialProductCategory;
	}


	public void setMaterialProductCategory(String materialProductCategory) {
		this.materialProductCategory = materialProductCategory;
	}


	public double getMaterialProductAvailableQuantity() {
		return materialProductAvailableQuantity;
	}


	public void setMaterialProductAvailableQuantity(double materialProductAvailableQuantity) {
		this.materialProductAvailableQuantity = materialProductAvailableQuantity;
	}


	public double getMaterialProductRequiredQuantity() {
		return materialProductRequiredQuantity;
	}


	public void setMaterialProductRequiredQuantity(
			double materialProductRequiredQuantity) {
		this.materialProductRequiredQuantity = materialProductRequiredQuantity;
	}


	public String getMaterialProductUOM() {
		return materialProductUOM;
	}


	public void setMaterialProductUOM(String materialProductUOM) {
		this.materialProductUOM = materialProductUOM;
	}


	public String getMaterialProductRemarks() {
		return materialProductRemarks;
	}


	public void setMaterialProductRemarks(String materialProductRemarks) {
		this.materialProductRemarks = materialProductRemarks;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}



	public double getMatReqReOdrLvlQty() {
		return matReqReOdrLvlQty;
	}



	public void setMatReqReOdrLvlQty(double matReqReOdrLvlQty) {
		this.matReqReOdrLvlQty = matReqReOdrLvlQty;
	}
	public Map<String, ArrayList<CompanyAsset>> getCompanyAsset() {
		return companyAsset;
	}
	public void setCompanyAsset(Map<String, ArrayList<CompanyAsset>> companyAsset) {
		this.companyAsset = companyAsset;
	}
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}
	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}

	public double getMaterialProductPlannedQuantity() {
		return materialProductPlannedQuantity;
	}
	
	public void setMaterialProductPlannedQuantity(
			double materialProductPlannedQuantity) {
		this.materialProductPlannedQuantity = materialProductPlannedQuantity;
	}
	public double getMaterialProductBalanceQty() {
		return materialProductBalanceQty;
	}
	public void setMaterialProductBalanceQty(double materialProductBalanceQty) {
		this.materialProductBalanceQty = materialProductBalanceQty;
	}
	public String getMaterialBranch() {
		return materialBranch;
	}
	public void setMaterialBranch(String materialBranch) {
		this.materialBranch = materialBranch;
	}
	public String getLockSealSerialNo() {
		return lockSealSerialNo;
	}
	public void setLockSealSerialNo(String lockSealSerialNo) {
		this.lockSealSerialNo = lockSealSerialNo;
	}
	
	
	
	
}
