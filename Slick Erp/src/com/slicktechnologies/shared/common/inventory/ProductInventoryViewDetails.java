package com.slicktechnologies.shared.common.inventory;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

@Entity
@Embed
public class ProductInventoryViewDetails extends SuperModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6829325609921091678L;
	ProductInfo productinfo;
	
	/*****************************************Applicability Attributes*******************************************/
	
	@Index
	protected int prodInvViewCount;
	@Index
	protected int prodid;
	@Index
	protected String prodname;
	@Index
	protected String prodcode;
	@Index
	protected String warehousename;
	@Index
	protected String storagelocation;
	@Index
	protected String storagebin;
	protected double minqty;
	protected double maxqty;
	protected double normallevel;
	protected double reorderlevel;
	protected double reorderqty;
	protected double availableqty;
	
	protected double qty;
	protected double totalwarehouseQty;
	
	/** date 22/02.2018 added by komal for hvac product purchase price **/
	protected double price;
	protected double purchasePrice;
	/**
	 * nidhi 21-08-2018 for map serial number details
	 */
	@EmbedMap
	@Serialize
	  protected HashMap<Integer, ArrayList<ProductSerialNoMapping>>  proSerialNoDetails = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

	/**
	 * @author Vijay Chougule
	 * Date :- 11-Dec-2018
	 * Des :- For NBHC Inventory Management for Automatic PR Order Qty 
	 */
	protected double avgConsumption;
	protected int leadDays;
	protected double safetyStockLevel;
	protected double minOrderQty;
	protected double forecastDemand;
	protected double orderQty;
	protected double expectedStock;
	protected double consumptiontillStockReceived;
	/**
	 * Des :- Normally we should not maintain branch name in
	 * this entity but i need the branch Name while creating 
	 * Automatic PR. So this field will not store branch name for
	 * embeded purpose i have used it
	 */
	protected String branchName;
	/**
	 * ends here
	 */
	/**
	 * Date 28-01-2019 By Vijay
	 * Des :- NBHC Inventory Management need to identify warehouse and cluster warehouse
	 */
	protected String warehouseType;
	
	   /** Date 19-8-2019 by Amol Added  a Product Category***/
	@Index
	protected String productCategory;
	
	
	boolean isFreeze=false;
	
	/**
	 * @author Anil
	 * @since 21-01-2021
	 * it  stores the inUseStock details
	 * for improving the scheduling performance will store and update inUseStock
	 * right now we are calculating in used stock every time of scheduling of services
	 * Raised by Nitin sir, for NBHC
	 */
	Double inUseStock;
	protected String prodUOM;
	
	
	/**
	 * ends here
	 */
	
	/**********************************************Constructor***********************************************/

	public ProductInventoryViewDetails() {
		super();
		prodname="";
		prodcode="";
		warehousename="";
		storagelocation="";
		storagebin="";
		warehouseType ="";
			productCategory="";
			prodUOM="";
			productinfo=new ProductInfo();
	}

	/*********************************************Getters And Setters**************************************/

	public int getProdInvViewCount() {
		return prodInvViewCount;
	}

	public void setProdInvViewCount(int prodInvViewCount) {
		this.prodInvViewCount = prodInvViewCount;
	}

	public int getProdid() {
		return prodid;
	}

	public void setProdid(int prodid) {
		this.prodid = prodid;
	}

	public String getProdname() {
		return prodname;
	}

	public void setProdname(String prodname) {
		this.prodname = prodname;
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public String getWarehousename() {
		return warehousename;
	}

	public void setWarehousename(String warehousename) {
			this.warehousename = warehousename;
	}

	public String getStoragelocation() {
		return storagelocation;
	}

	public void setStoragelocation(String storagelocation) {
		this.storagelocation = storagelocation;
	}

	public String getStoragebin() {
		return storagebin;
	}

	public void setStoragebin(String storagebin) {
		this.storagebin = storagebin;
	}

	public double getMinqty() {
		return minqty;
	}

	public void setMinqty(double minqty) {
		this.minqty = minqty;
	}

	public double getMaxqty() {
		return maxqty;
	}

	public void setMaxqty(double maxqty) {
		this.maxqty = maxqty;
	}

	public double getNormallevel() {
		return normallevel;
	}

	public void setNormallevel(double normallevel) {
		this.normallevel = normallevel;
	}

	public double getReorderlevel() {
		return reorderlevel;
	}

	public void setReorderlevel(double reorderlevel) {
		this.reorderlevel = reorderlevel;
	}

	public double getReorderqty() {
		return reorderqty;
	}

	public void setReorderqty(double reorderqty) {
		this.reorderqty = reorderqty;
	}

	public double getAvailableqty() {
		return availableqty;
	}

	public void setAvailableqty(double availableqty) {
		this.availableqty = availableqty;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public double getTotalwarehouseQty() {
		return totalwarehouseQty;
	}

	public void setTotalwarehouseQty(double totalwarehouseQty) {
		this.totalwarehouseQty = totalwarehouseQty;
	}
	
	
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	public String getProdUOM(){
		return prodUOM;
	}
	
	public void setProdUOM(String prodUOM){
		this.prodUOM = prodUOM;
	}

	/****************************************Overridden Method***********************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}

	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}
	
	public double getAvgConsumption() {
		return avgConsumption;
	}

	public void setAvgConsumption(double avgConsumption) {
		this.avgConsumption = avgConsumption;
	}

	public double getSafetyStockLevel() {
		return safetyStockLevel;
	}

	public void setSafetyStockLevel(double safetyStockLevel) {
		this.safetyStockLevel = safetyStockLevel;
	}

	public double getMinOrderQty() {
		return minOrderQty;
	}

	public void setMinOrderQty(double minOrderQty) {
		this.minOrderQty = minOrderQty;
	}

	public double getForecastDemand() {
		return forecastDemand;
	}

	public void setForecastDemand(double forecastDemand) {
		this.forecastDemand = forecastDemand;
	}

	public double getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(double orderQty) {
		this.orderQty = orderQty;
	}

	public double getExpectedStock() {
		return expectedStock;
	}

	public void setExpectedStock(double expectedStock) {
		this.expectedStock = expectedStock;
	}

	public int getLeadDays() {
		return leadDays;
	}

	public void setLeadDays(int leadDays) {
		this.leadDays = leadDays;
	}

	public double getConsumptiontillStockReceived() {
		return consumptiontillStockReceived;
	}

	public void setConsumptiontillStockReceived(double consumptiontillStockReceived) {
		this.consumptiontillStockReceived = consumptiontillStockReceived;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getWarehouseType() {
		return warehouseType;
	}

	public void setWarehouseType(String warehouseType) {
		this.warehouseType = warehouseType;
	}
	
	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	
	public boolean isFreeze() {
		return isFreeze;
	}

	public void setFreeze(boolean isFreeze) {
		this.isFreeze = isFreeze;
	}

	public Double getInUseStock() {
		return inUseStock;
	}

	public void setInUseStock(Double inUseStock) {
		this.inUseStock = inUseStock;
	}

	/**
	 * @author Anil @since 16-04-2021
	 * While creating approving GRN stock of some product is not getting updated
	 * transaction get created
	 * this may be happening because on save event we are updating freeze status on warehouse master
	 */
//	@OnSave
//	@GwtIncompatible
//	public void reactOnSave()
//	{
//		if(!isFreeze){
//			System.out.println("inside my condition isFreez in product inventory view details"+isFreeze);
//			WareHouse wareHouse=ofy().load().type(WareHouse.class).filter("companyId", getCompanyId()).filter("buisnessUnitName",getWarehousename()).first().now();
//			setFreeze(true);
//			if(wareHouse!=null){
//				wareHouse.setFreez(true);
//				ofy().save().entities(wareHouse);
//			}
//		  
//		}
//	}
}
