package com.slicktechnologies.shared.common.inventory;

import java.io.Serializable;
import java.io.Serializable;
import java.util.Date;
import com.googlecode.objectify.annotation.Embed;

@Embed
public class ExpenseCost implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 475092522352300994L;

/******************************************Applicability Attributes****************************************/
	
	int contractId;
	int expenseId;
	String expenseDate;
	String approvarName;
	double expenseAmt;
	
	String expenseCategory;
	String expenseType;
	String empployeeName;
	double expensenetAmount;
	int serviceId;
	
	double contCost;
public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
/***************************************getters and setters *****************************************************************/	
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	public int getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(int expenseId) {
		this.expenseId = expenseId;
	}
	public String getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}
	public String getApprovarName() {
		return approvarName;
	}
	public void setApprovarName(String approvarName) {
		this.approvarName = approvarName;
	}
	public double getExpenseAmt() {
		return expenseAmt;
	}
	public void setExpenseAmt(double expenseAmt) {
		this.expenseAmt = expenseAmt;
	}
	public String getExpenseCategory() {
		return expenseCategory;
	}
	public void setExpenseCategory(String expenseCategory) {
		this.expenseCategory = expenseCategory;
	}
	public String getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}
	public String getEmpployeeName() {
		return empployeeName;
	}
	public void setEmpployeeName(String empployeeName) {
		this.empployeeName = empployeeName;
	}
	public double getExpensenetAmount() {
		return expensenetAmount;
	}
	public void setExpensenetAmount(double expensenetAmount) {
		this.expensenetAmount = expensenetAmount;
	}
	public double getContCost() {
		return contCost;
	}
	public void setContCost(double contCost) {
		this.contCost = contCost;
	}
	
	
}
