package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.Config;

@Entity
public class ProductInventoryTransaction extends ConcreteBusinessProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1104154244569898656L;
	
	@Index
	protected Date inventoryTransactionDateTime;
	@Index
	protected int productId;
	@Index
	protected String productName;
	protected double productQty;
	protected double productPrice;
	protected String productUOM;
	@Index
	protected String warehouseName;
	@Index
	protected String storageLocation;
	@Index
	protected String storageBin;
	@Index
	protected String documentType;
	@Index
	protected int documentId;
	@Index
	protected String documentTitle;
	@Index
	protected Date documentDate;
	
	protected String operation;
	protected double openingStock;
	protected double closingStock;
	
	@Index
	protected String user;
	
	/**
	 * Date : 01/03/2017 By Anil
	 * This field stores MMN Sub type i.e TransferIn,TransferOut,IN,OUT
	 */
	
	@Index
	protected String documentSubType;
	
	/**
	 * End
	 */
	
	/**
	 * Date : 02/03/2017 By Anil
	 * Added product code 
	 */
	@Index 
	String productCode;
	/**
	 * nidhi 21-08-2018 for map serial number details
	 */
	  @EmbedMap
	  @Serialize
	  protected HashMap<Integer, ArrayList<ProductSerialNoMapping>>  proSerialNoDetails = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();

  /**
	 * nidhi ||*||
	 * @return
	 */
	  double plannedQty = 0;
	  
	  /**
	   * Date 29-11-2019
	   * Des :- NBHC Inventory Management Lock Seal Serial Number
	   */
	  protected String serialNumber;
	  
	  @Index
	  protected String serviceId; //Ashwini Patil Date:28-06-2024 added for Rex
	  
		  
	public ProductInventoryTransaction() {
		super();
		inventoryTransactionDateTime = new Date();
		documentSubType="";
		productCode="";
		serialNumber = "";
		serviceId="";
	}

	
	
	
	public Date getInventoryTransactionDateTime() {
		return inventoryTransactionDateTime;
	}

	public void setInventoryTransactionDateTime(Date inventoryTransactionDateTime) {
//		inventoryTransactionDateTime=new Date();
		this.inventoryTransactionDateTime = inventoryTransactionDateTime;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductQty() {
		return productQty;
	}

	public void setProductQty(double productQty) {
		this.productQty = productQty;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductUOM() {
		return productUOM;
	}

	public void setProductUOM(String productUOM) {
		this.productUOM = productUOM;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public double getOpeningStock() {
		return openingStock;
	}

	public void setOpeningStock(double openingStock) {
		this.openingStock = openingStock;
	}

	public double getClosingStock() {
		return closingStock;
	}

	public void setClosingStock(double closingStock) {
		this.closingStock = closingStock;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	

	public String getDocumentSubType() {
		return documentSubType;
	}

	public void setDocumentSubType(String documentSubType) {
		this.documentSubType = documentSubType;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}



	@Override
	public int compareTo(SuperModel arg0) {
//		return super.compareTo(arg0);
		ProductInventoryTransaction entityToCompare=(ProductInventoryTransaction) arg0;
		Integer id=this.count;
		Integer objName=entityToCompare.count;
		System.out.println("ID 1 : "+id+" ID 2 : "+objName);
		return id.compareTo(objName);
	}







	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}


	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}

	public double getPlannedQty() {
		return plannedQty;
	}

	public void setPlannedQty(double plannedQty) {
		this.plannedQty = plannedQty;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}




	public String getServiceId() {
		return serviceId;
	}




	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	


	
	
}
