package com.slicktechnologies.shared.common.inventory;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.common.deliverynote.ShippingSteps;

@Embed
public class InspectionDetails extends GRNDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7991546531836429470L;
	
	/********************************************Applicability Attributes**********************************/
	
	double rejectedQty;
	double acceptedQty;
	String reasonForReject;
	String inspectionRequired;
	
	@Serialize
	ArrayList<ShippingSteps> inspectionStepsLis;
	
	
	/******************************************Constructor*****************************************/
	
	public InspectionDetails() {
		super();
		inspectionRequired="";
		inspectionStepsLis=new ArrayList<ShippingSteps>();
	}
	
	/****************************************Getters And Setters*****************************************/

	public ArrayList<ShippingSteps> getInspectionStepsLis() {
		return inspectionStepsLis;
	}

	public void setInspectionStepsLis(List<ShippingSteps> inspectionStepsLis) {
		
		ArrayList<ShippingSteps> arrSSteps=new ArrayList<ShippingSteps>();
		arrSSteps.addAll(inspectionStepsLis);
		this.inspectionStepsLis = arrSSteps;
	}

	public double getRejectedQty() {
		return rejectedQty;
	}

	public void setRejectedQty(double rejectedQty) {
		this.rejectedQty = rejectedQty;
	}

	public double getAcceptedQty() {
		return acceptedQty;
	}

	public void setAcceptedQty(double acceptedQty) {
		this.acceptedQty = acceptedQty;
	}

	public String getReasonForReject() {
		return reasonForReject;
	}

	public void setReasonForReject(String reasonForReject) {
		if(reasonForReject!=null){
			this.reasonForReject = reasonForReject.trim();
		}
	}

	public String getInspectionRequired() {
		return inspectionRequired;
	}

	public void setInspectionRequired(String inspectionRequired) {
		this.inspectionRequired = inspectionRequired;
	}
	
	

}
