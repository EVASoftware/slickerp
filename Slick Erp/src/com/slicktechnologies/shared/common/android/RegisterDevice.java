package com.slicktechnologies.shared.common.android;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Contract;

/*
 * Rahul created this entity this on 17/Feb/2017
 * This entity is used for FCM(Firebase Cloud Messaging) 
 */
@Entity
public class RegisterDevice extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5752932535752761L;
	/**
	 * 
	 */
	
	@Index
	String regId; //From Mobile
	@Index
	String userName; //From Mobile
	@Index
	String employeeName; //From User (Querry)
	@Index
	int employeeId; //From Employee(Querry)
	@Index
	long imeiNumber; //From Mobile
	/*
	 * Date: 1 Jul 2017
	 * By: Apeksha Gunjal
	 * Adding companyCode and applicationName
	 */
	@Index 
	String companyCode; //From CustmerApp
	@Index 
	String applicationName; //From CustmerApp and TechnicianApp
	/** date 20.4.2019 added by komal to store status , creation date , **/
	@Index
	boolean status;
	@Index
	Date creationDate;
	@Index
	Date deactivationDate;
	@Index
	String deactivatedBy;
	
	/*************************Getters and Setter*************/
	
	

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public RegisterDevice() {
		super();
		/** date 20.4.2019 added by komal to store status , creation date**/
		status = true;
		creationDate = new Date();
		deactivatedBy = "";
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	

	public long getImeiNumber() {
		return imeiNumber;
	}

	public void setImeiNumber(long imeiNumber) {
		this.imeiNumber = imeiNumber;
	}

	public String isApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getDeactivationDate() {
		return deactivationDate;
	}

	public void setDeactivationDate(Date deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}
	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add("EVA Pedio");
		statuslist.add("EVA Priora");		
		statuslist.add("EVA Attendance");
		statuslist.add("EVA Kreto");
		
		return statuslist;
	}

	

	
}
