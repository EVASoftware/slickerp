package com.slicktechnologies.shared.common.android;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class EmployeeFingerDB  extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 665474688046280249L;
	/**
	 * 
	 */

	@Index
	String quality;
	@Index
	String nfiq;
	/* byte[] */String rawdataUrl;
	/* byte[] */String isoTemplateUrl;
	@Index
	String inWidth;
	@Index
	String inHeight;
	@Index
	String inArea;
	@Index
	String resolution;
	@Index
	String grayScale;
	@Index
	String bpp;
	@Index
	String wsqCompressRatio;
	@Index
	String wsqInfo;
	@Index
	int empId;

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	/************************* Setters and Getters **************************/
	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getNfiq() {
		return nfiq;
	}

	public void setNfiq(String nfiq) {
		this.nfiq = nfiq;
	}

	// public byte[] getRawdata() {
	// return rawdata;
	// }
	//
	//
	// public void setRawdata(byte[] rawdata) {
	// this.rawdata = rawdata;
	// }
	//
	//
	// public byte[] getIsoTemplate() {
	// return isoTemplate;
	// }
	//
	//
	// public void setIsoTemplate(byte[] isoTemplate) {
	// this.isoTemplate = isoTemplate;
	// }

	public String getRawdataUrl() {
		return rawdataUrl;
	}

	public void setRawdataUrl(String rawdataUrl) {
		this.rawdataUrl = rawdataUrl;
	}

	public String getIsoTemplateUrl() {
		return isoTemplateUrl;
	}

	public void setIsoTemplateUrl(String isoTemplateUrl) {
		this.isoTemplateUrl = isoTemplateUrl;
	}

	public String getInWidth() {
		return inWidth;
	}

	public void setInWidth(String inWidth) {
		this.inWidth = inWidth;
	}

	public String getInHeight() {
		return inHeight;
	}

	public void setInHeight(String inHeight) {
		this.inHeight = inHeight;
	}

	public String getInArea() {
		return inArea;
	}

	public void setInArea(String inArea) {
		this.inArea = inArea;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getGrayScale() {
		return grayScale;
	}

	public void setGrayScale(String grayScale) {
		this.grayScale = grayScale;
	}

	public String getBpp() {
		return bpp;
	}

	public void setBpp(String bpp) {
		this.bpp = bpp;
	}

	public String getWsqCompressRatio() {
		return wsqCompressRatio;
	}

	public void setWsqCompressRatio(String wsqCompressRatio) {
		this.wsqCompressRatio = wsqCompressRatio;
	}

	public String getWsqInfo() {
		return wsqInfo;
	}

	public void setWsqInfo(String wsqInfo) {
		this.wsqInfo = wsqInfo;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

}
