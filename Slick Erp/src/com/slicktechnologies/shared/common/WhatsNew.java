package com.slicktechnologies.shared.common;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class WhatsNew extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5780364292447562346L;
	/**************************************Applicability Attributes************************************/
	@Index
	protected String Title;
	@Index
	protected String subject;
	@Index
	protected String path;
	@Index
	protected String descriptionone;
	@Index
	protected String descriptiontwo;
	@Index
	protected String descriptionthree;
	@Index
	protected String descriptionfour;
	@Index
	protected String descriptionfive;
	@Index
	protected String moreDetails;
	@Index
	protected String googleDriveLink;
	/*******************************************Constructor******************************************/
	
	public WhatsNew() {
		super();
		Title="";
		subject="";
		path="";
		descriptionone="";
		descriptiontwo="";
		descriptionthree="";
		descriptionfour="";
		descriptionfive="";
		moreDetails="";
		googleDriveLink="";
	}
	
	/***************************************Getters And Setters****************************************/
	


	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}
	
	

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getGoogleDriveLink() {
		return googleDriveLink;
	}

	public void setGoogleDriveLink(String googleDriveLink) {
		this.googleDriveLink = googleDriveLink;
	}

	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDescriptionone() {
		return descriptionone;
	}

	public void setDescriptionone(String descriptionone) {
		this.descriptionone = descriptionone;
	}

	public String getDescriptiontwo() {
		return descriptiontwo;
	}

	public void setDescriptiontwo(String descriptiontwo) {
		this.descriptiontwo = descriptiontwo;
	}

	public String getDescriptionthree() {
		return descriptionthree;
	}

	public void setDescriptionthree(String descriptionthree) {
		this.descriptionthree = descriptionthree;
	}

	public String getDescriptionfour() {
		return descriptionfour;
	}

	public void setDescriptionfour(String descriptionfour) {
		this.descriptionfour = descriptionfour;
	}

	public String getDescriptionfive() {
		return descriptionfive;
	}

	public void setDescriptionfive(String descriptionfive) {
		this.descriptionfive = descriptionfive;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
