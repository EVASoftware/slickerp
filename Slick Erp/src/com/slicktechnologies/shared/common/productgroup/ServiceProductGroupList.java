package com.slicktechnologies.shared.common.productgroup;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Embed
public class ServiceProductGroupList extends ProductGroupList{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7455249127329319028L;
	@Index
	protected int productGroupId;
	@Index
	protected int issueProduct_id=0;
	@Index
	protected String issueCode="";
	@Index
	protected String issueName="";
	
	protected double issueQuantity=0.0;
	
	protected String issueUnit="";
	
	protected double issuePrice=0.0;
	
	/*** Date 05 Oct 2017 added by vijay for required from
	 * Technician Scheduling
	 */
	
	@Index
	protected String issueWarehouse="";
	@Index
	protected String issueStorageLocation="";
	@Index
	protected String issueStorageBin="";
	
	
	@Index
	protected int actulProduct_id=0;
	@Index
	protected String actulCode="";
	@Index
	protected String actulName="";
	
	protected double actulQuantity=0.0;
	
	protected String actulUnit="";
	
	
	/*** Date 05 Oct 2017 added by vijay for required from
	 * Technician Scheduling
	 */
	
	@Index
	protected String actulWarehouse="";
	@Index
	protected String actulStorageLocation="";
	@Index
	protected String actulStorageBin="";
	protected double actualPrice=0.0;
	
	@Index
	protected int minCount=0;
	@Index
	protected int serviceCount=0;
	
	protected String reportNote="";
	public ServiceProductGroupList(){
		
	}

	public int getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(int productGroupId) {
		this.productGroupId = productGroupId;
	}

	public int getIssueProduct_id() {
		return issueProduct_id;
	}

	public void setIssueProduct_id(int issueProduct_id) {
		this.issueProduct_id = issueProduct_id;
	}

	public String getIssueCode() {
		return issueCode;
	}

	public void setIssueCode(String issueCode) {
		this.issueCode = issueCode;
	}

	public String getIssueName() {
		return issueName;
	}

	public void setIssueName(String issueName) {
		this.issueName = issueName;
	}

	public double getIssueQuantity() {
		return issueQuantity;
	}

	public void setIssueQuantity(double issueQuantity) {
		this.issueQuantity = issueQuantity;
	}

	public String getIssueUnit() {
		return issueUnit;
	}

	public void setIssueUnit(String issueUnit) {
		this.issueUnit = issueUnit;
	}

	public double getIssuePrice() {
		return issuePrice;
	}

	public void setIssuePrice(double issuePrice) {
		this.issuePrice = issuePrice;
	}

	public String getIssueWarehouse() {
		return issueWarehouse;
	}

	public void setIssueWarehouse(String issueWarehouse) {
		this.issueWarehouse = issueWarehouse;
	}

	public String getIssueStorageLocation() {
		return issueStorageLocation;
	}

	public void setIssueStorageLocation(String issueStorageLocation) {
		this.issueStorageLocation = issueStorageLocation;
	}

	public String getIssueStorageBin() {
		return issueStorageBin;
	}

	public void setIssueStorageBin(String issueStorageBin) {
		this.issueStorageBin = issueStorageBin;
	}

	public int getActulProduct_id() {
		return actulProduct_id;
	}

	public void setActulProduct_id(int actulProduct_id) {
		this.actulProduct_id = actulProduct_id;
	}

	public String getActulCode() {
		return actulCode;
	}

	public void setActulCode(String actulCode) {
		this.actulCode = actulCode;
	}

	public String getActulName() {
		return actulName;
	}

	public void setActulName(String actulName) {
		this.actulName = actulName;
	}

	public double getActulQuantity() {
		return actulQuantity;
	}

	public void setActulQuantity(double actulQuantity) {
		this.actulQuantity = actulQuantity;
	}

	public String getActulUnit() {
		return actulUnit;
	}

	public void setActulUnit(String actulUnit) {
		this.actulUnit = actulUnit;
	}

	public String getActulWarehouse() {
		return actulWarehouse;
	}

	public void setActulWarehouse(String actulWarehouse) {
		this.actulWarehouse = actulWarehouse;
	}

	public String getActulStorageLocation() {
		return actulStorageLocation;
	}

	public void setActulStorageLocation(String actulStorageLocation) {
		this.actulStorageLocation = actulStorageLocation;
	}

	public String getActulStorageBin() {
		return actulStorageBin;
	}

	public void setActulStorageBin(String actulStorageBin) {
		this.actulStorageBin = actulStorageBin;
	}

	public double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public int getMinCount() {
		return minCount;
	}

	public void setMinCount(int minCount) {
		this.minCount = minCount;
	}

	public int getServiceCount() {
		return serviceCount;
	}

	public void setServiceCount(int serviceCount) {
		this.serviceCount = serviceCount;
	}

	public String getReportNote() {
		return reportNote;
	}

	public void setReportNote(String reportNote) {
		this.reportNote = reportNote;
	}
	
	
	
}
