package com.slicktechnologies.shared.common.productgroup;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;

@Entity
public class ProductGroupDetails extends SuperModel{
	
	private static final long serialVersionUID = 2208302163299162324L;
	
	@Index
 	protected String title;
 	@Index
 	protected boolean status;
 	
 	protected ArrayList<ProductGroupList> pitems;
 	
 	/**
 	 * nidhi
 	 * 10-09-2018
 	 *  *:*:*
 	 */
 	protected double area = 00;
 	@Index
	protected String unit = "";
	/**
	 * end
	 */
	public ProductGroupDetails() {
		super();
		title="";
		pitems = new ArrayList<ProductGroupList>();
	}


	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title.trim();
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public List<ProductGroupList> getPitems() 
	{
		return pitems;
	}

	public void setPitems(List<ProductGroupList> pitems) {
		ArrayList<ProductGroupList> arraypitems = new ArrayList<ProductGroupList>();
		arraypitems.addAll(pitems);
		this.pitems = arraypitems;
	}
	
	@OnSave
	@GwtIncompatible
	public void  addGroupDetails() {
		

		if(id==null)
		{
			saveGroupList();
		}
		if(id!=null)
		{
			updateGroupList();
		}
		
		
	}
	
	@GwtIncompatible
	protected void saveGroupList()
	{
		ArrayList<SuperModel> saveArray=new ArrayList<SuperModel>();
		for(int i=0;i<this.getPitems().size();i++)
		{
			ProductGroupList groupDetails = new ProductGroupList();
			groupDetails.setProductGroupId(getCount());
			groupDetails.setProduct_id(this.getPitems().get(i).getProduct_id());
			groupDetails.setCode(this.getPitems().get(i).getCode());
			groupDetails.setName(this.getPitems().get(i).getName());
			groupDetails.setQuantity(this.getPitems().get(i).getQuantity());
			groupDetails.setUnit(this.getPitems().get(i).getUnit());
			groupDetails.setCompanyId(getCompanyId());
	    	groupDetails.setTitle(this.getTitle());
	    	groupDetails.setStatus(this.getStatus());
	    	
	    	saveArray.add(groupDetails);
		}
		
		
		if(saveArray.size()!=0)
		{
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(saveArray);
		}
		
	}
	
	@GwtIncompatible
	private void updateGroupList()
	{
		int groupList=ofy().load().type(ProductGroupList.class).filter("productGroupId", this.getCount()).filter("companyId", getCompanyId()).count();
		if(groupList==1)
		{
			ProductGroupList groupLis=ofy().load().type(ProductGroupList.class).filter("productGroupId", this.getCount()).filter("companyId", getCompanyId()).first().now();
			
			if(groupLis!=null){
				ofy().delete().entity(groupLis);
				saveGroupList();
			}
		}
		
		if(groupList>1)
		{
			List<ProductGroupList> groupListAll=ofy().load().type(ProductGroupList.class).filter("productGroupId", this.getCount()).filter("companyId", getCompanyId()).list();
			
			for(int i=0;i<groupListAll.size();i++)
			{
				ofy().delete().entity(groupListAll.get(i));
			}
			saveGroupList();
		}
	}
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


	public double getArea() {
		return area;
	}


	public void setArea(double area) {
		this.area = area;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public void setPitems(ArrayList<ProductGroupList> pitems) {
		this.pitems = pitems;
	}

}
