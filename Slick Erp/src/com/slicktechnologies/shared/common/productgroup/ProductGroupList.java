package com.slicktechnologies.shared.common.productgroup;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
@Entity
public class ProductGroupList extends SuperModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4305136512649687495L;

	
	/******************************************Applicability Attributes****************************************/
	
	@Index
 	protected String title;
	@Index
	protected boolean status;
	@Index
	protected int productGroupId;
	@Index
	protected int product_id;
	@Index
	protected String code;
	@Index
	protected String name;
	
	protected double quantity;
	
	protected String unit;
	
	
	/*** Date 05 Oct 2017 added by vijay for required from
	 * Technician Scheduling
	 */
	
	@Index
	protected String warehouse;
	@Index
	protected String storageLocation;
	@Index
	protected String storageBin;
	
	protected double returnQuantity;
	
	
	/**
	 * ends here
	 */
	
	/****************************************Getters And Setters**********************************************/
	/**
	 * nidhi
	 *  *:*:*
	 * 18-09-2018
	 * for map service sequance in service scheduling
	 */
	@Index
	protected int serNo = 0;
	protected double price  = 0.0;
	protected double proActualQty  = 0.0;
	protected String proActualUnit = ""; // for area of bom pro
	/**
	 * nidhi ||*||
	 * 27-12-2018
	 */
	protected double plannedQty =0.0;
	protected String plannedUnit="";
	
	@Index
	protected String parentWarentwarehouse;
	@Index
	protected String parentStorageLocation;
	@Index
	protected String parentStorageBin;
	
	/**
	 * @author Anil @since 30-09-2021
	 * Adding Mixing no. and lot no. field for innovative raised by Rahul Tiwari
	 */
	@Index
	protected String mixingRatio;
	@Index
	protected String lotNo;
	
	public String getMixingRatio() {
		return mixingRatio;
	}

	public void setMixingRatio(String mixingNo) {
		this.mixingRatio = mixingNo;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	/**
	 * nidhi 
	 * please add new created variable in copyOfObject method for cloning of object
	 * @return
	 */
	public int getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(int productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Integer getProduct_id() {
		return product_id;
	}
	
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getQuantity() {
		return quantity;
	}
	
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}
	
	

	public double getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(double returnQuantity) {
		this.returnQuantity = returnQuantity;
	}
	
	/********************************************Overridden Method*********************************************/
	

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public int getSerNo() {
		return serNo;
	}

	public void setSerNo(int serNo) {
		this.serNo = serNo;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getProActualQty() {
		return proActualQty;
	}

	public void setProActualQty(double proActualQty) {
		this.proActualQty = proActualQty;
	}

	public String getProActualUnit() {
		return proActualUnit;
	}

	public void setProActualUnit(String proActualUnit) {
		this.proActualUnit = proActualUnit;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	/**
	 * nidhi ||*||
	 * @return
	 */
	
	
	public double getPlannedQty() {
		return plannedQty;
	}

	public void setPlannedQty(double plannedQty) {
		this.plannedQty = plannedQty;
	}

	public String getPlannedUnit() {
		return plannedUnit;
	}

	public void setPlannedUnit(String plannedUnit) {
		this.plannedUnit = plannedUnit;
	}

	public ProductGroupList  copyOfObject(){
		ProductGroupList progt = new ProductGroupList();
		
		progt.setProduct_id(this.getProduct_id());
		progt.setPrice(this.getPrice());
		progt.setCount(this.getCount());
		progt.setQuantity(this.getQuantity());
		progt.setSerNo(this.getSerNo());
		progt.setUnit(this.getUnit());
		progt.setName(this.getName());
		progt.setProductGroupId(this.getProductGroupId());
		progt.setCode(this.getCode());
		progt.setCompanyId(this.getCompanyId());
		progt.setTitle(this.getTitle());
		/**
		 * nidhi ||*||
		 * 
		 */
		progt.setPlannedQty(this.getPlannedQty());
		progt.setPlannedUnit(this.getPlannedUnit());
		/**
		 * end
		 */
		return progt;
	}

	public String getParentWarentwarehouse() {
		return parentWarentwarehouse;
	}

	public void setParentWarentwarehouse(String parentWarentwarehouse) {
		this.parentWarentwarehouse = parentWarentwarehouse;
	}

	public String getParentStorageLocation() {
		return parentStorageLocation;
	}

	public void setParentStorageLocation(String parentStorageLocation) {
		this.parentStorageLocation = parentStorageLocation;
	}

	public String getParentStorageBin() {
		return parentStorageBin;
	}

	public void setParentStorageBin(String parentStorageBin) {
		this.parentStorageBin = parentStorageBin;
	}
	
}
