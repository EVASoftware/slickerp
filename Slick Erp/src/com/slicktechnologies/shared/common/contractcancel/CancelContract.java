package com.slicktechnologies.shared.common.contractcancel;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class CancelContract implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5277726839613894918L;
	
	protected boolean recordSelect;
	
	public boolean isRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}

	
	
	@Index
	protected int documnetId;
	
	@Index
	protected String documnetName;
	
	protected String bpName;
	
	protected String branch;
	
	protected String documentStatus;

	public int getDocumnetId() {
		return documnetId;
	}

	public void setDocumnetId(int documnetId) {
		this.documnetId = documnetId;
	}

	public String getDocumnetName() {
		return documnetName;
	}

	public void setDocumnetName(String documnetName) {
		this.documnetName = documnetName;
	}

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}
	

}
