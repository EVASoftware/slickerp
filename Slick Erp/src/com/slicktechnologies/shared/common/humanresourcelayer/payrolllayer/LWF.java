package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class LWF extends SuperModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6426123266222930674L;
	/**
	 * 
	 */

	@Index
	String state;
	@Index
	String name;
	@Index
	String shortName;
	@Index
	boolean status;
	double employeeContribution;
	double employerContribution;
//	@Index
//	String period;
//	@Index
//	String deductionMonth;
//	@Index
	double fromAmt;
//	@Index
	double toAmt;
	ArrayList<LWFBean> lwfList;
	
	public void LWF(){
		state = "";
		name = "";
		shortName = "";
//		period = "";
//		deductionMonth= "";
//		lwfList= new ArrayList<LWFBean>();
	}
	
	
	
	
	public double getEmployeeContribution() {
		return employeeContribution;
	}




	public void setEmployeeContribution(double employeeContribution) {
		this.employeeContribution = employeeContribution;
	}




	public double getEmployerContribution() {
		return employerContribution;
	}




	public void setEmployerContribution(double employerContribution) {
		this.employerContribution = employerContribution;
	}




	public double getFromAmt() {
		return fromAmt;
	}




	public void setFromAmt(double fromAmt) {
		this.fromAmt = fromAmt;
	}




	public double getToAmt() {
		return toAmt;
	}




	public void setToAmt(double toAmt) {
		this.toAmt = toAmt;
	}




	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getShortName() {
		return shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return isDuplicateComponentName(m)&&isDuplicateStateName(m);
	}
	
	public boolean isDuplicateComponentName(SuperModel m) {
		LWF entity = (LWF) m;
		String name = entity.getName().trim();
		String curname = this.name.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();
		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}
	public boolean isDuplicateStateName(SuperModel m) {
		LWF entity = (LWF) m;
		String name = entity.getState().trim();
		String curname = this.state.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();
		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}


	public ArrayList<LWFBean> getLwfList() {
		return lwfList;
	}


	public void setLwfList(List<LWFBean> lwfList) {
		ArrayList<LWFBean> list = new ArrayList<LWFBean>();
		if(lwfList != null){
			list.addAll(lwfList);
		}
		this.lwfList = list;
	}

	

}
