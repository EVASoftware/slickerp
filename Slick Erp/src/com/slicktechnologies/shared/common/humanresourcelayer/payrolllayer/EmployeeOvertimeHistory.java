package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class EmployeeOvertimeHistory extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7077732491563848180L;
	
	int otId;
	String otName;
	double otHours;
	double otAmount;
	boolean payOtAsOther;
	String correspondanceName;

	
	public EmployeeOvertimeHistory() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	
	public int getOtId() {
		return otId;
	}


	public void setOtId(int otId) {
		this.otId = otId;
	}


	public String getOtName() {
		return otName;
	}


	public void setOtName(String otName) {
		this.otName = otName;
	}


	public double getOtHours() {
		return otHours;
	}


	public void setOtHours(double otHours) {
		this.otHours = otHours;
	}


	public double getOtAmount() {
		return otAmount;
	}


	public void setOtAmount(double otAmount) {
		this.otAmount = otAmount;
	}


	public boolean isPayOtAsOther() {
		return payOtAsOther;
	}


	public void setPayOtAsOther(boolean payOtAsOther) {
		this.payOtAsOther = payOtAsOther;
	}


	public String getCorrespondanceName() {
		return correspondanceName;
	}


	public void setCorrespondanceName(String correspondanceName) {
		this.correspondanceName = correspondanceName;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
