package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.google.gwt.core.shared.GwtIncompatible;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Represents Cost to Company for an Employee.This Component is Used to Genrate 
 * Pay Slip For Employees.Again Like {@link LeaveBalance} only one active CTC of an Employee should
 * exist active CTC means that the the valid till must be greater than current Date.
 */
@Entity
public class CTC extends EmployeeHRProcess
{
/************************************Attributes******************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5660813597577092360L;
	
	/**  Gross earning of CTC. */
	protected Double grossEarning;
	
	/**  Amount of CTC. */
	protected Double ctcAmount;
	
	/**  Calendar name for which CTC is applicable. */
	@Index
	protected String calendarName;
	
	/**  List of Earnings for CTC. */
	
	protected ArrayList<CtcComponent> earning;
	
	/**  Status of CTC. */
	protected boolean CTCstatus;
	
	/** The Constant ACTIVE. */
	public static final String ACTIVE="Active";
	
	/**
	 * Start Validity of CTC
	 */
	public Date validFrom;
	/**
	 * End Validity of CTC
	 */
	@Index
	public Date validTill;
	
	
	/**
	 * Date : 28-04-2018 By ANIL
	 * adding deduction component in CTC 
	 * for Sasha ERP/Facility Mgmt
	 */
	protected ArrayList<CtcComponent> deduction;
	
	/**
	 * Date : 03-05-2018 By ANIL
	 * adding companies contribution component in CTC
	 * for Sashsa ERP/Facility Mgmt
	 */
	protected ArrayList<CtcComponent> companiesContribution;
	double employeesTotalDeduction;
	double companiesTotalDeduction;
	
	/**
	 * Date : 04-05-2018 BY ANIL
	 * Added effective date ,from where it should be applicable in case of variance in salary structure
	 */
	
	@Index
	Date effectiveFromDate;
	
	
	/**
	 * Date : 11-05-2018 BY ANIL
	 * Adding new status Inactive, 
	 */
	public static final String INACTIVE="Inactive";
	
	/**
	 * Date : 12-05-2018 BY ANIL
	 * added arrears flag to the ctc
	 */
	@Index
	boolean isArrears;
	
	/**
	 * Rahul added on 16 July 2018
	 */
	@Index
	boolean paidLeaveApplicable;
	/**
	 * Ends
	 */
	/** date 20.7.2018 added by komal for  cumulative ctc**/
	@Index
	double cumulativeCTC;
	@Index
	double cumulativeTax;
	@Index
	double cumulativeDeduction;
	
	
	/**
	 * Date : 21-08-2018 BY ANIL
	 * storing the ctc allocation information
	 */
	@Index
	int ctcTemplateId;
	String ctcTemplateName;
	Date ctcTempAllocationDate;
	String ctcTempAllocatedBy;
	
	
	/**
	 * Date :27-08-2018 By Anil
	 */
	@Index
	boolean isMonthly;
	@Index
	boolean isAnnually;
	@Index
	boolean isBonus;
	
	double paidLeaveAmt;
	double bonusAmt;
	@Index
	String month;
	
	/**
	 * Date : 31-12-2018 BY ANIL
	 */
	double previousPaidLeaveAmt;
	double previousBonusAmt;
	boolean isPaidLeaveAndBonusRevised;
	
	/** @author Anil , Date :19-06-2019
     * Pl and Bonus by formula for riseon by RT 
     **/
    @Index
    String plName;
    @Index
    boolean isPlIncludedInEarning;
    String plCorresponadenceName;
   
    @Index
    String bonusName;
    @Index
    boolean isBonusIncludedInEarning;
    String bonusCorresponadenceName;
    
    /**
     * @author Anil , Date : 12-07-2019
     * Capturing project name for processing multiple site payroll
     * raised by Rahul tiwari for riseon
     */
    @Index
    String projectName;
    
    @Index
    Date effectiveToDate;
	
/************************************Attributes***********************************************************/	
	
	
/**************************************Constructor*****************************************************/

	/**
	 * Instantiates a new ctc.
	 */
	public CTC()
	{
		super();
		
		earning=new ArrayList<CtcComponent>();
		calendarName="";
		
		deduction=new ArrayList<CtcComponent>();
		
		companiesContribution=new ArrayList<CtcComponent>();
		
		paidLeaveApplicable=true;
		
		ctcTemplateName="";
		ctcTempAllocatedBy="";
		month="";
		
		
		
	}
	
/***********************************************************************************************************/	
	
	

	
/**
 * ************************Getters and Setters*********************************************.
 *
 * @return the gross earning
 */
	

	

	public String getProjectName() {
		return projectName;
	}
	
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}
	
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public String getPlName() {
		return plName;
	}


	public void setPlName(String plName) {
		this.plName = plName;
	}







	public boolean isPlIncludedInEarning() {
		return isPlIncludedInEarning;
	}







	public void setPlIncludedInEarning(boolean isPlIncludedInEarning) {
		this.isPlIncludedInEarning = isPlIncludedInEarning;
	}







	public String getPlCorresponadenceName() {
		return plCorresponadenceName;
	}







	public void setPlCorresponadenceName(String plCorresponadenceName) {
		this.plCorresponadenceName = plCorresponadenceName;
	}







	public String getBonusName() {
		return bonusName;
	}







	public void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}







	public boolean isBonusIncludedInEarning() {
		return isBonusIncludedInEarning;
	}







	public void setBonusIncludedInEarning(boolean isBonusIncludedInEarning) {
		this.isBonusIncludedInEarning = isBonusIncludedInEarning;
	}







	public String getBonusCorresponadenceName() {
		return bonusCorresponadenceName;
	}







	public void setBonusCorresponadenceName(String bonusCorresponadenceName) {
		this.bonusCorresponadenceName = bonusCorresponadenceName;
	}

	
	
	public double getPreviousPaidLeaveAmt() {
		return previousPaidLeaveAmt;
	}






	public void setPreviousPaidLeaveAmt(double previousPaidLeaveAmt) {
		this.previousPaidLeaveAmt = previousPaidLeaveAmt;
	}






	public double getPreviousBonusAmt() {
		return previousBonusAmt;
	}






	public void setPreviousBonusAmt(double previousBonusAmt) {
		this.previousBonusAmt = previousBonusAmt;
	}






	public boolean isPaidLeaveAndBonusRevised() {
		return isPaidLeaveAndBonusRevised;
	}






	public void setPaidLeaveAndBonusRevised(boolean isPaidLeaveAndBonusRevised) {
		this.isPaidLeaveAndBonusRevised = isPaidLeaveAndBonusRevised;
	}
	
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public boolean isMonthly() {
		return isMonthly;
	}
	public void setMonthly(boolean isMonthly) {
		this.isMonthly = isMonthly;
	}
	public boolean isAnnually() {
		return isAnnually;
	}
	public void setAnnually(boolean isAnnually) {
		this.isAnnually = isAnnually;
	}
	public boolean isBonus() {
		return isBonus;
	}
	public void setBonus(boolean isBonus) {
		this.isBonus = isBonus;
	}
	public double getPaidLeaveAmt() {
		return paidLeaveAmt;
	}
	public void setPaidLeaveAmt(double paidLeaveAmt) {
		this.paidLeaveAmt = paidLeaveAmt;
	}
	public double getBonusAmt() {
		return bonusAmt;
	}
	public void setBonusAmt(double bonusAmt) {
		this.bonusAmt = bonusAmt;
	}


	

	public int getCtcTemplateId() {
		return ctcTemplateId;
	}
	
	public void setCtcTemplateId(int ctcTemplateId) {
		this.ctcTemplateId = ctcTemplateId;
	}
	
	public String getCtcTemplateName() {
		return ctcTemplateName;
	}
	
	public void setCtcTemplateName(String ctcTemplateName) {
		this.ctcTemplateName = ctcTemplateName;
	}
	
	public Date getCtcTempAllocationDate() {
		return ctcTempAllocationDate;
	}
	
	public void setCtcTempAllocationDate(Date ctcTempAllocationDate) {
		this.ctcTempAllocationDate = ctcTempAllocationDate;
	}
	
	public String getCtcTempAllocatedBy() {
		return ctcTempAllocatedBy;
	}
	
	public void setCtcTempAllocatedBy(String ctcTempAllocatedBy) {
		this.ctcTempAllocatedBy = ctcTempAllocatedBy;
	}
	
		public boolean isPaidLeaveApplicable() {
		return paidLeaveApplicable;
	}
	
	public void setPaidLeaveApplicable(boolean paidLeaveApplicable) {
		this.paidLeaveApplicable = paidLeaveApplicable;
	}

	public boolean isArrears() {
		return isArrears;
	}
	public void setArrears(boolean isArrears) {
		this.isArrears = isArrears;
	}

	
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}
	
	
	public double getEmployeesTotalDeduction() {
		return employeesTotalDeduction;
	}
	
	public void setEmployeesTotalDeduction(double employeesTotalDeduction) {
		this.employeesTotalDeduction = employeesTotalDeduction;
	}
	
	public double getCompaniesTotalDeduction() {
		return companiesTotalDeduction;
	}
	
	public void setCompaniesTotalDeduction(double companiesTotalDeduction) {
		this.companiesTotalDeduction = companiesTotalDeduction;
	}
	

	public ArrayList<CtcComponent> getCompaniesContribution() {
		return companiesContribution;
	}

	public void setCompaniesContribution(List<CtcComponent> companiesContribution) {
		if(companiesContribution!=null){
		    this.companiesContribution=new ArrayList<CtcComponent>();
			this.companiesContribution.addAll(companiesContribution);
		}
	}
	
	
	
	public ArrayList<CtcComponent> getDeduction() {
		return deduction;
	}

	public void setDeduction(List<CtcComponent> deduction) {
		if(deduction!=null){
		    this.deduction=new ArrayList<CtcComponent>();
			this.deduction.addAll(deduction);
		}
	}

	
	
	

	public Double getGrossEarning() {
		return grossEarning;
	}
	/**
	 * Sets the gross earning.
	 *
	 * @param grossEarning the new gross earning
	 */
	public void setGrossEarning(Double grossEarning) {
		this.grossEarning = grossEarning;
	}
	
	/**
	 * Gets the ctc amount.
	 *
	 * @return the ctc amount
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "CTC Amount")
	public Double getCtcAmount() {
		return ctcAmount;
	}


	/**
	 * Sets the ctc amount.
	 *
	 * @param ctcAmount the new ctc amount
	 */
	public void setCtcAmount(Double ctcAmount) {
		this.ctcAmount = ctcAmount;
	}

	/**
	 * Gets the earning.
	 *
	 * @return the earning
	 */
	public ArrayList<CtcComponent> getEarning() {
		return earning;
	}


	/**
	 * Sets the earning.
	 *
	 * @param list the new earning
	 */
	public void setEarning(List<CtcComponent> list) {
		if(list!=null)
		{
		    this.earning=new ArrayList<CtcComponent>();
			this.earning.addAll(list);
		}
	}


	/**
	 * Gets the CT cstatus.
	 *
	 * @return the CT cstatus
	 */
	public Boolean getCTCstatus() {
		return CTCstatus;
	}



	/**
	 * Sets the CT cstatus.
	 *
	 * @param cTCstatus the new CT cstatus
	 */
	public void setCTCstatus(Boolean cTCstatus) {
		CTCstatus = cTCstatus;
	}



	/**
	 * Gets the calendar.
	 *
	 * @return the calendar
	 */
	public String getCalendar() {
		// TODO Auto-generated method stub
		return calendarName;
	}

	


	public Date getValidFrom() {
		return validFrom;
	}



	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}



	public Date getValidTill() {
		return validTill;
	}



	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}



	public void setEarning(ArrayList<CtcComponent> earning) {
		this.earning = earning;
	}



	/**
	 * Sets the calender.
	 *
	 * @param value the new calender
	 */
	public void setCalender(String value) {
		if(value!=null)
			calendarName=value.trim();
		
	}



	/**
	 * Gets the List of Statuses in Ctc
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList() 
	{
		ArrayList<String>list=new ArrayList<String>();
		list.add(CREATED);
		list.add(ACTIVE);
		list.add(INACTIVE);
		
		return list;
	}
	
	
	@OnSave
	@GwtIncompatible
	public void updateCtcStatusInEmployeeInfo(){
		EmployeeInfo empInfo=ofy().load().type(EmployeeInfo.class).filter("companyId", this.getCompanyId()).filter("empCount", this.getEmpid()).first().now();
		if(empInfo!=null){
			if(this.getStatus().equals(CTC.CREATED)){
				empInfo.setCtcCreated(true);
				empInfo.setCtcAmount(this.getCtcAmount());
				/**Date 19-4-2019  Added by Amol to Save The CTC Template Name**/
				empInfo.setCtcTemplateName(this.getCtcTemplateName());
			}else if(this.getStatus().equals(CTC.ACTIVE)){
				empInfo.setCtcAllocated(true);
				empInfo.setCtcAmount(this.getCtcAmount());
				empInfo.setCtcTemplateName(this.getCtcTemplateName());
			}
			ofy().save().entity(empInfo).now();
		}
		
		
	}

	public double getCumulativeCTC() {
		return cumulativeCTC;
	}

	public void setCumulativeCTC(double cumulativeCTC) {
		this.cumulativeCTC = cumulativeCTC;
	}

	public double getCumulativeTax() {
		return cumulativeTax;
	}

	public void setCumulativeTax(double cumulativeTax) {
		this.cumulativeTax = cumulativeTax;
	}

	public double getCumulativeDeduction() {
		return cumulativeDeduction;
	}

	public void setCumulativeDeduction(double cumulativeDeduction) {
		this.cumulativeDeduction = cumulativeDeduction;
	}


	
	
	

}
