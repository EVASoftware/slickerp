package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
@Embed
public class AvailedLeaves implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4615405293078755644L;


	private String leaveName;
	private double leaveHour;
	private boolean unPaid;
	
	public AvailedLeaves() {
		leaveName="";
		leaveHour=0;
		unPaid=false;
	}

	public String getLeaveName() {
		return leaveName;
	}

	public void setLeaveName(String leaveName) {
		this.leaveName = leaveName;
	}

	public double getLeavehour() {
		return leaveHour;
	}

	public void setLeavehour(double leavehour) {
		this.leaveHour = leavehour;
	}

	public boolean isUnPaid() {
		return unPaid;
	}

	public void setUnPaid(boolean unPaid) {
		this.unPaid = unPaid;
	}

}
