package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EducationalInfo;
import com.slicktechnologies.shared.common.helperlayer.PassportInformation;
import com.slicktechnologies.shared.common.helperlayer.PreviousCompanyHistory;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;

import static com.googlecode.objectify.ObjectifyService.ofy;



public class PaySlipPrintHelper{

	
	/**
	 * Helper Class used for printing of Pay Slip.
	 */
	private static final long serialVersionUID = 687714170426133291L;
  /*****************************************Attributes*********************************************************/
	
	
	protected Address companyAddress;
	protected DocumentUpload logo;
	protected PaySlip paySlip;

	public PaySlipPrintHelper() 
		{
			super();
			
			
		}

	public int compareTo(SuperModel o) {
		return paySlip.compareTo(o);
	}

	public boolean equals(Object obj) {
		return paySlip.equals(obj);
	}

	public final int getCount() {
		return paySlip.getCount();
	}

	public int hashCode() {
		return paySlip.hashCode();
	}

	public final void setCount(int long1) {
		paySlip.setCount(long1);
	}

	public Long getCompanyId() {
		return paySlip.getCompanyId();
	}

	public Long getId() {
		return paySlip.getId();
	}

	public void setId(Long id) {
		paySlip.setId(id);
	}

	public String getEmployeedDesignation() {
		return paySlip.getEmployeedDesignation();
	}

	public boolean isDeleted() {
		return paySlip.isDeleted();
	}

	public boolean isDuplicate(SuperModel m) {
		return paySlip.isDuplicate(m);
	}

	public void setEmployeedDesignation(String employeedDesignation) {
		paySlip.setEmployeedDesignation(employeedDesignation);
	}

	public void setDeleted(boolean deleted) {
		paySlip.setDeleted(deleted);
	}

	public void setCompanyId(Long companyId) {
		paySlip.setCompanyId(companyId);
	}

	public Date getFromdate() {
		return paySlip.getFromdate();
	}

	public void setEmpCellNo(Long empCellNo) {
		paySlip.setEmpCellNo(empCellNo);
	}

	public void setFromdate(Date fromdate) {
		paySlip.setFromdate(fromdate);
	}

	public String getApproverName() {
		return paySlip.getApproverName();
	}

	public Date getTodate() {
		return paySlip.getTodate();
	}

	public void setTodate(Date todate) {
		paySlip.setTodate(todate);
	}

	public String getBranch() {
		return paySlip.getBranch();
	}

	public void setApproverName(String approverName) {
		paySlip.setApproverName(approverName);
	}

	public void setBranch(String branch) {
		paySlip.setBranch(branch);
	}

	public Date getApprovalDate() {
		return paySlip.getApprovalDate();
	}

	public Long getEmpCellNo() {
		return paySlip.getEmpCellNo();
	}

	public void setApprovalDate(Date approvalDate) {
		paySlip.setApprovalDate(approvalDate);
	}

	public String getDepartment() {
		return paySlip.getDepartment();
	}

	public int getEmpid() {
		return paySlip.getEmpid();
	}

	public void setDepartment(String department) {
		paySlip.setDepartment(department);
	}

	public String getEmployeeName() {
		return paySlip.getEmployeeName();
	}

	public Date getCreationDate() {
		return paySlip.getCreationDate();
	}

	public void setEmpid(int empid) {
		paySlip.setEmpid(empid);
	}

	public void setCreationDate(Date creationDate) {
		paySlip.setCreationDate(creationDate);
	}

	public String getEmployeeType() {
		return paySlip.getEmployeeType();
	}

	public String getStatus() {
		return paySlip.getStatus();
	}

	public void setStatus(String status) {
		paySlip.setStatus(status);
	}

	public String getEmployeeRole() {
		return paySlip.getEmployeeRole();
	}

	public void setEmployeeName(String employeeName) {
		paySlip.setEmployeeName(employeeName);
	}

	public String getCountry() {
		return paySlip.getCountry();
	}

	public void setCountry(String country) {
		paySlip.setCountry(country);
	}

	public String getTbESICNo() {
		return paySlip.getTbESICNo();
	}

	public void setTbESICNo(String tbESICNo) {
		paySlip.setTbESICNo(tbESICNo);
	}

	public void setEmployeeType(String employeeType) {
		paySlip.setEmployeeType(employeeType);
	}

	public String getTbPanNo() {
		return paySlip.getTbPanNo();
	}

	public void setTbPanNo(String tbPanNo) {
		paySlip.setTbPanNo(tbPanNo);
	}

	public String getTbPFACNo() {
		return paySlip.getTbPFACNo();
	}

	public void setEmployeeRole(String employeeRole) {
		paySlip.setEmployeeRole(employeeRole);
	}

	public void setTbPFACNo(String tbPFACNo) {
		paySlip.setTbPFACNo(tbPFACNo);
	}

	public void reactOnApproval() {
		paySlip.reactOnApproval();
	}

	public void reactOnRejected() {
		paySlip.reactOnRejected();
	}

	public String getTbaccNo() {
		return paySlip.getTbaccNo();
	}

	public void setTbaccNo(String tbaccNo) {
		paySlip.setTbaccNo(tbaccNo);
	}

	public String getTbIFSC() {
		return paySlip.getTbIFSC();
	}

	public void setTbIFSC(String tbIFSC) {
		paySlip.setTbIFSC(tbIFSC);
	}

	public ArrayList<CtcComponent> getEarningList() {
		return paySlip.getEarningList();
	}

	public void setEarningList(List<CtcComponent> list) {
		paySlip.setEarningList(list);
	}

	public ArrayList<CtcComponent> getDeductionList() {
		return paySlip.getDeductionList();
	}

//	public void setDeductionList(List<CtcComponent> list) {
//		paySlip.setDeductionList(list);
//	}

	public double getEligibleDays() {
		return paySlip.getEligibleDays();
	}

	public void setEligibleDays(double eligibleDays) {
		paySlip.setEligibleDays(eligibleDays);
	}

	public double getPaidDays() {
		return paySlip.getPaidDays();
	}

	public void setPaidDays(double paidDays) {
		paySlip.setPaidDays(paidDays);
	}

	public String getSalaryPeriod() {
		return paySlip.getSalaryPeriod();
	}

	public void setSalaryPeriod(String salaryPeriod) {
		paySlip.setSalaryPeriod(salaryPeriod);
	}

	public List<PaidEmis> getLoneEmis() {
		return paySlip.getLoneEmis();
	}

	public void setLoneEmis(List<PaidEmis> loneEmis) {
		paySlip.setLoneEmis(loneEmis);
	}

	public void setDeductionList(ArrayList<CtcComponent> deductionList) {
		paySlip.setDeductionList(deductionList);
	}

	public Date getDateOfBirth() {
		return paySlip.getDateOfBirth();
	}

	public String getEmpBankBranch() {
		return paySlip.getEmpBankBranch();
	}

	public void setEmpBankBranch(String empBankBranch) {
		paySlip.setEmpBankBranch(empBankBranch);
	}

	public String getEmpBankName() {
		return paySlip.getEmpBankName();
	}

	public void setEmpBankName(String empBankName) {
		paySlip.setEmpBankName(empBankName);
	}

	public Date getDob() {
		return paySlip.getDob();
	}

	public void setDob(Date dob) {
		paySlip.setDob(dob);
	}

	public double getCtcAmount() {
		return paySlip.getCtcAmount();
	}

	public String getEsicNumber() {
		return paySlip.getEsicNumber();
	}

	public void setEsicNumber(String esicNumber) {
		paySlip.setEsicNumber(esicNumber);
	}

	public String getPanNumber() {
		return paySlip.getPanNumber();
	}

	public void setPanNumber(String panNumber) {
		paySlip.setPanNumber(panNumber);
	}

	public String getPfNumber() {
		return paySlip.getPfNumber();
	}

	public void setPfNumber(String pfNumber) {
		paySlip.setPfNumber(pfNumber);
	}

	public String getAccountNumber() {
		return paySlip.getAccountNumber();
	}

	public void setAccountNumber(String accountNumber) {
		paySlip.setAccountNumber(accountNumber);
	}

	public String getIfscNumber() {
		return paySlip.getIfscNumber();
	}

	public void setIfscNumber(String ifscNumber) {
		paySlip.setIfscNumber(ifscNumber);
	}

	public String getBankBranch() {
		return paySlip.getBankBranch();
	}

	public void setBankBranch(String bankBranch) {
		paySlip.setBankBranch(bankBranch);
	}

	public String getBankName() {
		return paySlip.getBankName();
	}

	public void setBankName(String bankName) {
		paySlip.setBankName(bankName);
	}

	public double getNetPayBeforeDeduction() {
		return paySlip.getNetPayBeforeDeduction();
	}

	public void setNetPayBeforeDeduction(double netPayBeforeDeduction) {
		paySlip.setNetPayBeforeDeduction(netPayBeforeDeduction);
	}

	public double getNetDeduction() {
		return paySlip.getNetDeduction();
	}

	public void setNetDeduction(double netDeduction) {
		paySlip.setNetDeduction(netDeduction);
	}

	public void setEarningList(ArrayList<CtcComponent> earningList) {
		paySlip.setEarningList(earningList);
	}

	public PaySlipPrintHelper getPaySlipPrintHelper() {
		return paySlip.getPaySlipPrintHelper();
	}

	public double getMonthlyEarnings() {
		return paySlip.getMonthlyEarnings();
	}

	public void setMonthlyEarnings(double monthlyEarnings) {
		paySlip.setMonthlyEarnings(monthlyEarnings);
	}

	public void setCtcAmount(double ctcAmount) {
		paySlip.setCtcAmount(ctcAmount);
	}

	public String toString() {
		return paySlip.toString();
	}

	public Address getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(Address companyAddress) {
		this.companyAddress = companyAddress;
	}

	public DocumentUpload getLogo() {
		return logo;
	}

	public void setLogo(DocumentUpload logo) {
		this.logo = logo;
	}

	public PaySlip getPaySlip() {
		return paySlip;
	}

	public void setPaySlip(PaySlip paySlip) {
		this.paySlip = paySlip;
	}

	
	
  
  

}
