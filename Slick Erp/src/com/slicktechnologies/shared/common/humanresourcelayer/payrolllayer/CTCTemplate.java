package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class CTCTemplate extends SuperModel{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5787539319262876351L;

	@Index
	String ctcTemplateName;
	@Index
	boolean status;
	ArrayList<CtcComponent> earning;
	ArrayList<CtcComponent> deduction;
	
	double totalCtcAmount;
	double totalDeductionAmount;
	
	double grossEarning;
	double ctcCopy;
	double difference;
	
	/**
	 * Date : 09-05-2018 By ANIL
	 * adding companies contribution component in CTC
	 * for Sashsa ERP/Facility Mgmt
	 */
	protected ArrayList<CtcComponent> companyContribution;
	double companyTotalDeduction;
	
	/**
	 * Date :29-05-2018 By ANIL
	 */
	String gender;
	String numberRange;
	@Index
	String State;
	
	/**
	 * Rahul added on 16 July 2018
	 */
	boolean paidLeavesAvailable;
	
	/**
	 * Date :27-08-2018 By Anil
	 */
	@Index
	boolean isMonthly;
	@Index
	boolean isAnnually;
	@Index
	boolean isBonus;
	
	double paidLeaveAmt;
	double bonusAmt;
	@Index
	String month;
	/** date 31.8.2018 added by komal**/
	@Index
	String category;
	
	/**
	 * Date : 31-12-2018 BY ANIL
	 */
	double previousPaidLeaveAmt;
	double previousBonusAmt;
	boolean isPaidLeaveAndBonusRevised;
	/*Date 6-3-2019 added by Amol
	 * 
	 */
   double monthlyCtcAmount;
   
   /** @author Anil , Date :19-06-2019
    * Pl and Bonus by formula for riseon by RT 
    **/
   @Index
   String plName;
   @Index
   boolean isPlIncludedInEarning;
   String plCorresponadenceName;
   
   @Index
   String bonusName;
   @Index
   boolean isBonusIncludedInEarning;
   String bonusCorresponadenceName;
   
	public CTCTemplate() {
		super();
		paidLeavesAvailable=true;
		month="";
		category = "";
	}
	
	
	
	
	
	
	
	public String getPlName() {
		return plName;
	}







	public void setPlName(String plName) {
		this.plName = plName;
	}







	public boolean isPlIncludedInEarning() {
		return isPlIncludedInEarning;
	}







	public void setPlIncludedInEarning(boolean isPlIncludedInEarning) {
		this.isPlIncludedInEarning = isPlIncludedInEarning;
	}







	public String getPlCorresponadenceName() {
		return plCorresponadenceName;
	}







	public void setPlCorresponadenceName(String plCorresponadenceName) {
		this.plCorresponadenceName = plCorresponadenceName;
	}







	public String getBonusName() {
		return bonusName;
	}







	public void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}







	public boolean isBonusIncludedInEarning() {
		return isBonusIncludedInEarning;
	}







	public void setBonusIncludedInEarning(boolean isBonusIncludedInEarning) {
		this.isBonusIncludedInEarning = isBonusIncludedInEarning;
	}







	public String getBonusCorresponadenceName() {
		return bonusCorresponadenceName;
	}







	public void setBonusCorresponadenceName(String bonusCorresponadenceName) {
		this.bonusCorresponadenceName = bonusCorresponadenceName;
	}







	public double getPreviousPaidLeaveAmt() {
		return previousPaidLeaveAmt;
	}






	public void setPreviousPaidLeaveAmt(double previousPaidLeaveAmt) {
		this.previousPaidLeaveAmt = previousPaidLeaveAmt;
	}






	public double getPreviousBonusAmt() {
		return previousBonusAmt;
	}






	public void setPreviousBonusAmt(double previousBonusAmt) {
		this.previousBonusAmt = previousBonusAmt;
	}






	public boolean isPaidLeaveAndBonusRevised() {
		return isPaidLeaveAndBonusRevised;
	}






	public void setPaidLeaveAndBonusRevised(boolean isPaidLeaveAndBonusRevised) {
		this.isPaidLeaveAndBonusRevised = isPaidLeaveAndBonusRevised;
	}






	public String getMonth() {
		return month;
	}






	public void setMonth(String month) {
		this.month = month;
	}






	public boolean isMonthly() {
		return isMonthly;
	}






	public void setMonthly(boolean isMonthly) {
		this.isMonthly = isMonthly;
	}






	public boolean isAnnually() {
		return isAnnually;
	}






	public void setAnnually(boolean isAnnually) {
		this.isAnnually = isAnnually;
	}






	public boolean isBonus() {
		return isBonus;
	}






	public void setBonus(boolean isBonus) {
		this.isBonus = isBonus;
	}






	public double getPaidLeaveAmt() {
		return paidLeaveAmt;
	}






	public void setPaidLeaveAmt(double paidLeaveAmt) {
		this.paidLeaveAmt = paidLeaveAmt;
	}






	public double getBonusAmt() {
		return bonusAmt;
	}






	public void setBonusAmt(double bonusAmt) {
		this.bonusAmt = bonusAmt;
	}






	public boolean isPaidLeavesAvailable() {
		return paidLeavesAvailable;
	}






	public void setPaidLeavesAvailable(boolean paidLeavesAvailable) {
		this.paidLeavesAvailable = paidLeavesAvailable;
	}






	public String getGender() {
		return gender;
	}






	public void setGender(String gender) {
		this.gender = gender;
	}






	public String getNumberRange() {
		return numberRange;
	}






	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}






	public ArrayList<CtcComponent> getCompanyContribution() {
		return companyContribution;
	}






	public void setCompanyContribution(List<CtcComponent> companyContribution) {
		if(companyContribution!=null){
		    this.companyContribution=new ArrayList<CtcComponent>();
			this.companyContribution.addAll(companyContribution);
		}
	}






	public double getCompanyTotalDeduction() {
		return companyTotalDeduction;
	}






	public void setCompanyTotalDeduction(double companyTotalDeduction) {
		this.companyTotalDeduction = companyTotalDeduction;
	}






	public double getGrossEarning() {
		return grossEarning;
	}






	public void setGrossEarning(double grossEarning) {
		this.grossEarning = grossEarning;
	}






	public double getCtcCopy() {
		return ctcCopy;
	}






	public void setCtcCopy(double ctcCopy) {
		this.ctcCopy = ctcCopy;
	}


	public double getDifference() {
		return difference;
	}

	public void setDifference(double difference) {
		this.difference = difference;
	}






	public double getTotalCtcAmount() {
		return totalCtcAmount;
	}

	public void setTotalCtcAmount(double totalCtcAmount) {
		this.totalCtcAmount = totalCtcAmount;
	}





	public double getTotalDeductionAmount() {
		return totalDeductionAmount;
	}





	public void setTotalDeductionAmount(double totalDeductionAmount) {
		this.totalDeductionAmount = totalDeductionAmount;
	}





	public String getCtcTemplateName() {
		return ctcTemplateName;
	}
	public void setCtcTemplateName(String ctcTemplateName) {
		this.ctcTemplateName = ctcTemplateName;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public ArrayList<CtcComponent> getEarning() {
		return earning;
	}
	public void setEarning(List<CtcComponent> list) {
		if(list!=null){
		    this.earning=new ArrayList<CtcComponent>();
			this.earning.addAll(list);
		}
	}
	public ArrayList<CtcComponent> getDeduction() {
		return deduction;
	}

	public void setDeduction(List<CtcComponent> deduction) {
		if(deduction!=null){
		    this.deduction=new ArrayList<CtcComponent>();
			this.deduction.addAll(deduction);
		}
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}






	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public double getMonthlyCtcAmount() {
		return monthlyCtcAmount;
	}
public void setMonthlyCtcAmount(double monthlyCtcAmount) {
		this.monthlyCtcAmount = monthlyCtcAmount;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
//		return super.toString();
		return ctcTemplateName;
	}


	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}






//	public double getDbMonthlyCtcAmount() {
//		return dbMonthlyCtcAmount;
//	}
//
//
//
//
//
//
//	public void setDbMonthlyCtcAmount(double dbMonthlyCtcAmount) {
//		this.dbMonthlyCtcAmount = dbMonthlyCtcAmount;
//	}
	

}
