package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class ProfessionalTax extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7140179017842768910L;
	/**
	 * 
	 */
	
	@Index
	String state;
	@Index
	String componentName;
	@Index
	String shortName;
	double ptAmount;
	double rangeFromAmt;
	double rangeToAmt;
	@Index
	String gender;
	@Index
	boolean status;
	@Index
	boolean isExceptionForSpecificMonth;
	@Index
	String specificMonth;
	double specificMonthAmount;
	
	
	public ProfessionalTax() {
		state="";                        
		componentName="";                
		shortName="";                    
		gender="";                       
		specificMonth="";                
	}
	
	
	
	
	public String getState() {
		return state;
	}




	public void setState(String state) {
		this.state = state;
	}




	public String getComponentName() {
		return componentName;
	}




	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}




	public String getShortName() {
		return shortName;
	}




	public void setShortName(String shortName) {
		this.shortName = shortName;
	}




	public double getPtAmount() {
		return ptAmount;
	}




	public void setPtAmount(double ptAmount) {
		this.ptAmount = ptAmount;
	}




	public double getRangeFromAmt() {
		return rangeFromAmt;
	}




	public void setRangeFromAmt(double rangeFromAmt) {
		this.rangeFromAmt = rangeFromAmt;
	}




	public double getRangeToAmt() {
		return rangeToAmt;
	}




	public void setRangeToAmt(double rangeToAmt) {
		this.rangeToAmt = rangeToAmt;
	}




	public String getGender() {
		return gender;
	}




	public void setGender(String gender) {
		this.gender = gender;
	}




	public boolean isStatus() {
		return status;
	}




	public void setStatus(boolean status) {
		this.status = status;
	}




	public boolean isExceptionForSpecificMonth() {
		return isExceptionForSpecificMonth;
	}




	public void setExceptionForSpecificMonth(boolean isExceptionForSpecificMonth) {
		this.isExceptionForSpecificMonth = isExceptionForSpecificMonth;
	}




	public String getSpecificMonth() {
		return specificMonth;
	}




	public void setSpecificMonth(String specificMonth) {
		this.specificMonth = specificMonth;
	}




	public double getSpecificMonthAmount() {
		return specificMonthAmount;
	}




	public void setSpecificMonthAmount(double specificMonthAmount) {
		this.specificMonthAmount = specificMonthAmount;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return isDuplicateComponentName(m)&&isDuplicateStateName(m);
	}
	
	public boolean isDuplicateComponentName(SuperModel m) {
		ProfessionalTax entity = (ProfessionalTax) m;
		String name = entity.getComponentName().trim();
		String curname = this.componentName.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();
		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}
	public boolean isDuplicateStateName(SuperModel m) {
		ProfessionalTax entity = (ProfessionalTax) m;
		String name = entity.getState().trim();
		String curname = this.state.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();
		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}

}
