package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

// TODO: Auto-generated Javadoc
/**
 * Created when a Pay Slip is created for Employee.
 * Used to keep record of Pay Slip Allocation Process of an
 * Employee.
 * 
 *
 */
@Entity
public class PaySlipRecord 
{
	
	@Id
	Long id;
	/** The employee key. */
	@Index
	protected Key<EmployeeInfo>employeeKey;
	
	/** The salary period. for which a Pay Slip is created. */
	@Index
	protected String salaryPeriod;
	/**
	 * start date of pay slip period.
	 */
	@Index
	Date fromDate;
	/**
	 * End date of pay slip period.
	 */
	@Index
	Date toDate;
	public Key<EmployeeInfo> getEmployeeKey() {
		return employeeKey;
	}
	public void setEmployeeKey(Key<EmployeeInfo> employeeKey) {
		this.employeeKey = employeeKey;
	}
	public String getSalaryPeriod() {
		return salaryPeriod;
	}
	public void setSalaryPeriod(String salaryPeriod) {
		this.salaryPeriod = salaryPeriod;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
}
