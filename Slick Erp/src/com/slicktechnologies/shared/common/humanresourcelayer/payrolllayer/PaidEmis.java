package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.Key;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Emi;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;

// TODO: Auto-generated Javadoc
/**
 * Helper Class to track {@link Emi} of a Loan in {@link PaySlip}
 */
public class PaidEmis implements Serializable
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6872588415319794245L;
	
	/** emi which has been paid */
	protected Emi emi;
	
	/** id of loan application corresponding to this class.  */
	protected Key<Loan> loanApplicationKey;
	
	/**
	 * Gets the emi.
	 *
	 * @return the emi
	 */
	public Emi getEmi() {
		return emi;
	}
	
	/**
	 * Sets the emi.
	 *
	 * @param emi the new emi
	 */
	public void setEmi(Emi emi) {
		this.emi = emi;
	}
	
	/**
	 * Gets the advance id.
	 *
	 * @return the advance id
	 */
	public Key<Loan> getAdvanceKey() {
		return loanApplicationKey;
	}
	
	/**
	 * Sets the advance id.
	 *
	 * @param advanceId the new advance id
	 */
	public void setAdvanceKey(Key<Loan> advanceId) {
		loanApplicationKey = advanceId;
	}
	
	/**
	 * Compare to.
	 *
	 * @param o the o
	 * @return the int
	 */
	public int compareTo(Emi o) {
		return emi.compareTo(o);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return emi.equals(obj);
	}
	
	/**
	 * Gets the no of months.
	 *
	 * @return the no of months
	 */
	public int getNoOfMonths() {
		return emi.getInstallmentNumber();
	}
	
	/**
	 * Gets the monthly status.
	 *
	 * @return the monthly status
	 */
	public String getMonthlyStatus() {
		return emi.getMonthlyStatus();
	}
	
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return emi.getDate();
	}
	
	/**
	 * Gets the monthly loan paidto date.
	 *
	 * @return the monthly loan paidto date
	 */
	public double getMonthlyLoanPaidtoDate() {
		return emi.getMonthlyLoanPaidtoDate();
	}
	
	/**
	 * Gets the monthlyprincipal amt.
	 *
	 * @return the monthlyprincipal amt
	 */
	public int getMonthlyprincipalAmt() {
		return emi.getMonthlyprincipalAmt();
	}
	
	/**
	 * Gets the monthly interest.
	 *
	 * @return the monthly interest
	 */
	public int getMonthlyInterest() {
		return emi.getMonthlyInterest();
	}
	
	/**
	 * Gets the monthly payment.
	 *
	 * @return the monthly payment
	 */
	public int getMonthlyPayment() {
		return emi.getMonthlyPayment();
	}
	
	/**
	 * Gets the monthly balance.
	 *
	 * @return the monthly balance
	 */
	public int getMonthlyBalance() {
		return emi.getMonthlyBalance();
	}
	
	/**
	 * Gets the principal amount.
	 *
	 * @return the principal amount
	 */
	public int getPrincipalAmount() {
		return emi.getPrincipalAmount();
	}
	
	/**
	 * Gets the intrest.
	 *
	 * @return the intrest
	 */
	public int getIntrest() {
		return emi.getIntrest();
	}
	
	/**
	 * Gets the emi amount.
	 *
	 * @return the emi amount
	 */
	public int getEmiAmount() {
		return emi.getEmiAmount();
	}
	
	/**
	 * Gets the loan balance.
	 *
	 * @return the loan balance
	 */
	public int getLoanBalance() {
		return emi.getLoanBalance();
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return emi.getStatus();
	}
	
	/**
	 * Gets the emi number.
	 *
	 * @return the emi number
	 */
	public int getEmiNumber() {
		return emi.getEmiNumber();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return emi.hashCode();
	}
	
	/**
	 * Sets the no of months.
	 *
	 * @param noOfMonths the new no of months
	 */
	public void setNoOfMonths(int noOfMonths) {
		emi.setInstallemtNumber(noOfMonths);
	}
	
	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		emi.setDate(date);
	}
	
	/**
	 * Sets the monthly status.
	 *
	 * @param monthlyStatus the new monthly status
	 */
	public void setMonthlyStatus(String monthlyStatus) {
		emi.setMonthlyStatus(monthlyStatus);
	}
	
	/**
	 * Sets the monthly loan paidto date.
	 *
	 * @param monthlyLoanPaidtoDate the new monthly loan paidto date
	 */
	public void setMonthlyLoanPaidtoDate(double monthlyLoanPaidtoDate) {
		emi.setMonthlyLoanPaidtoDate(monthlyLoanPaidtoDate);
	}
	
	/**
	 * Sets the monthlyprincipal amt.
	 *
	 * @param monthlyprincipalAmt the new monthlyprincipal amt
	 */
	public void setMonthlyprincipalAmt(int monthlyprincipalAmt) {
		emi.setMonthlyprincipalAmt(monthlyprincipalAmt);
	}
	
	/**
	 * Sets the monthly interest.
	 *
	 * @param monthlyInterest the new monthly interest
	 */
	public void setMonthlyInterest(int monthlyInterest) {
		emi.setMonthlyInterest(monthlyInterest);
	}
	
	/**
	 * Sets the monthly payment.
	 *
	 * @param monthlyPayment the new monthly payment
	 */
	public void setMonthlyPayment(int monthlyPayment) {
		emi.setMonthlyPayment(monthlyPayment);
	}
	
	/**
	 * Sets the monthly balance.
	 *
	 * @param monthlyBalance the new monthly balance
	 */
	public void setMonthlyBalance(int monthlyBalance) {
		emi.setMonthlyBalance(monthlyBalance);
	}
	
	/**
	 * Sets the principal amount.
	 *
	 * @param principalAmount the new principal amount
	 */
	public void setPrincipalAmount(int principalAmount) {
		emi.setPrincipalAmount(principalAmount);
	}
	
	/**
	 * Sets the intrest.
	 *
	 * @param intrest the new intrest
	 */
	public void setIntrest(int intrest) {
		emi.setIntrest(intrest);
	}
	
	/**
	 * Sets the emi amount.
	 *
	 * @param emiAmount the new emi amount
	 */
	public void setEmiAmount(int emiAmount) {
		emi.setEmiAmount(emiAmount);
	}
	
	/**
	 * Sets the loan balance.
	 *
	 * @param loanBalance the new loan balance
	 */
	public void setLoanBalance(int loanBalance) {
		emi.setLoanBalance(loanBalance);
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		emi.setStatus(status);
	}
	
	/**
	 * Sets the emi number.
	 *
	 * @param emiNumber the new emi number
	 */
	public void setEmiNumber(int emiNumber) {
		emi.setEmiNumber(emiNumber);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return emi.toString();
	}
	
	
	
	
	

}
