package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


/**
 * Saves the Pay Slip Allocation Process , saving this starts Processing of Pay roll.That is Pay Slip
 * Entitys are Created.
 */
@Entity
public class PaySlipAllocationProcess extends SuperModel {

	/********************************************Attributes***************************************************************/
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -85419010705327355L;
	
	/** User who started the pay roll process*/
	protected String user;
	
	/** Filter to which allocation process is assigned*/
	protected String allocatedTo;
	
	/** The system time. */
	protected Date systemTime;
	
	
	
	@Index
	protected Date payRollDate;
	@Index
	protected String stringPayRollPeriod;

	/*****************************************************************************************************************/
	
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	/********************************************Attributes***************************************************************/
	
	/********************************************Constructor***************************************************************/

	public PaySlipAllocationProcess() {
		super();
		// TODO Auto-generated constructor stub
	}
	/*****************************************************************************************************************/

	public String getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}



	/**
	 * Gets the allocated to.
	 *
	 * @return the allocated to
	 */
	public String getAllocatedTo() {
		return allocatedTo;
	}



	/**
	 * Sets the allocated to.
	 *
	 * @param allocatedTo the new allocated to
	 */
	public void setAllocatedTo(String allocatedTo) {
		this.allocatedTo = allocatedTo;
	}


	/**
	 * Gets the system time.
	 *
	 * @return the system time
	 */
	public Date getSystemTime() {
		return systemTime;
	}



	/**
	 * Sets the system time.
	 *
	 * @param systemTime the new system time
	 */
	public void setSystemTime(Date systemTime) {
		this.systemTime = systemTime;
	}
	
	






	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}



	public void setSalaryPeriod(Date value) {
		this.payRollDate=value;
		DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
		this.stringPayRollPeriod=format.format(payRollDate);
		
		
	}
	
	public Date getSalaryPeriod()
	{
		return this.payRollDate;
		
	}



	public Date getPayRollDate() {
		return payRollDate;
	}



	public void setPayRollDate(Date payRollDate) {
		this.payRollDate = payRollDate;
	}



	public String getStringPayRollPeriod() {
		return stringPayRollPeriod;
	}



	public void setStringPayRollPeriod(String stringPayRollPeriod) {
		this.stringPayRollPeriod = stringPayRollPeriod;
	}
	
	

}
