package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Represents  Component of a CTC
 */
@Entity
@Embed
public class CtcComponent extends SuperModel implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6463349459247776705L;
	
/****************************************** Attributes *******************************************************/
	
	/** The name of Component */
	@Index
	protected String name;
	/** The short name of Component */
	/**
	 * Date : 17-05-2018 BY ANIL
	 */
	@Index
	protected String shortName;
	/** The status of the component */
	@Index
	protected Boolean status;
	/** The Net Ctc Amount */
	protected Double amount;
	/** Maximum percentage of CTC */
	@Index
	protected double maxPerOfCTC;
	/** Maximum amount of CTC */
	protected Integer maxAmount;
	/** The Applicability. */
	protected boolean Aplicablity;
	/** Is this component a deduction component or not */
	@Index
	protected boolean isDeduction;
	
	
	/****** New Field Added by Anil
	 * 13-08-2015 For Deduction Component 
	 */
	
	@Index
	protected Boolean isRecord;
	@Index
	protected Boolean isNonMonthly;
	
	@Index
	protected String ctcComponentName;
	
	protected String condition;
	
	protected Integer conditionValue;

	/**
	 *  11-09-2017
	 *  nidhi
	 *  for ctcComponet data
	 *  for display component in sequence 
	 */
	@Index
	protected int srNo;
	/**
	 *  end
	 */
	
	/**
	 * Date : 04-05-2018 BY ANIL
	 * Adding gender field for the calculation of PT
	 */
	
	String gender;
	
	/**
	 * Date : 26 June 2018
	 * Dev : Rahul Verma
	 * Desc: Added this for frequency of calculations
	 */
	String specificMonthValue;
	boolean specificMonth;
	
	
	
	/**
	 * Date : 11-07-2018 BY  ANIL
	 * This field is used to store the actual earning of per month based on the no. of paid days
	 * as we are currently storing base rate in earning list of PaySlip.
	 * for deduction we are storing actual calculated amount in variable amount.
	 */
	double actualAmount;
	/**
	 * End
	 */
	/**
	 * Rahul added this
	 * Date: 14 July 2018
	 * Description : Fields to exclude in the payroll
	 */
	@Index
	boolean excludeInPayRoll;
	/**
	 * Rahul Verma added this on 17 July 2018.
	 * In salary slip we add components manualy so only this should be allowed to deleted
	 */
	boolean recordTobeDeletedinTable;
	/**
	 * Ends 
	 */
	/** date 20.7.2018 added by komal to check whether deduction is included in tds calculation***/
	boolean isTDS;
	
	
	/**
	 * Date : 21-08-2018 BY ANIL
	 * revised flag is added
	 */
	
	@Index
	boolean isRevised;
	double oldAmountMonthly;
	double oldAmountAnnually;
	
	/**
	 * Date : 28-08-2018 By ANIL
	 * Capturing arrears component here and it is of which components
	 */
	@Index
	boolean isArrears;
	String arrearOf;
	
	/**
	 * @author Anil , Date : 29-06-2019
	 * adding field for storing base amount on which deduction is applicable 
	 * also for bifurcation of pf percentage for the purpose of CNC
	 * In percent1 we will store Employer PF(12%) and In percent2 we will store EDLI(1%)
	 */
	double baseAmount;
	double percent1;
	double percent2;
	
	
	/**
	 * @author Anil , Date : 26-07-2019
	 * adding field for storing from and to range amount
	 * For ESIC
	 */
	
	double fromRangeAmount;
	double toRangeAmount;
	
	/************************************ Constructor ******************************************************/
	/**
	 * Instantiates a new salary head.
	 */


	/************************************ Constructor ******************************************************/
	/**
	 * Instantiates a new salary head.
	 */
	public CtcComponent() {
		name = "";
		shortName = "";
		amount = 0.0;
		isNonMonthly=false;
		isRecord=false;
//		condition="";
//		conditionValue=0;
		
		gender="";
		specificMonthValue="";
		specificMonth=false;

		excludeInPayRoll=false;
		recordTobeDeletedinTable=false;
		/** date 20.7.2018 added by komal to check whether deduction is included in tds calculation***/
		isTDS = false;
		
		isArrears=false;
		arrearOf="";
	}

	
	
	
	
	public double getFromRangeAmount() {
		return fromRangeAmount;
	}





	public void setFromRangeAmount(double fromRangeAmount) {
		this.fromRangeAmount = fromRangeAmount;
	}





	public double getToRangeAmount() {
		return toRangeAmount;
	}





	public void setToRangeAmount(double toRangeAmount) {
		this.toRangeAmount = toRangeAmount;
	}





	public double getBaseAmount() {
		return baseAmount;
	}





	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}





	public double getPercent1() {
		return percent1;
	}





	public void setPercent1(double percent1) {
		this.percent1 = percent1;
	}





	public double getPercent2() {
		return percent2;
	}





	public void setPercent2(double percent2) {
		this.percent2 = percent2;
	}





	public boolean isArrears() {
		return isArrears;
	}





	public void setArrears(boolean isArrears) {
		this.isArrears = isArrears;
	}





	public String getArrearOf() {
		return arrearOf;
	}





	public void setArrearOf(String arrearOf) {
		this.arrearOf = arrearOf;
	}





	public boolean isRevised() {
		return isRevised;
	}





	public void setRevised(boolean isRevised) {
		this.isRevised = isRevised;
	}





	public double getOldAmountMonthly() {
		return oldAmountMonthly;
	}





	public void setOldAmountMonthly(double oldAmountMonthly) {
		this.oldAmountMonthly = oldAmountMonthly;
	}





	public double getOldAmountAnnually() {
		return oldAmountAnnually;
	}





	public void setOldAmountAnnually(double oldAmountAnnually) {
		this.oldAmountAnnually = oldAmountAnnually;
	}





	public boolean isExcludeInPayRoll() {
		return excludeInPayRoll;
	}





	public void setExcludeInPayRoll(boolean excludeInPayRoll) {
		this.excludeInPayRoll = excludeInPayRoll;
	}





	public boolean isRecordTobeDeletedinTable() {
		return recordTobeDeletedinTable;
	}





	public void setRecordTobeDeletedinTable(boolean recordTobeDeletedinTable) {
		this.recordTobeDeletedinTable = recordTobeDeletedinTable;
	}





	public double getActualAmount() {
		return actualAmount;
	}





	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}





	public String getSpecificMonthValue() {
		return specificMonthValue;
	}



	public void setSpecificMonthValue(String specificMonthValue) {
		this.specificMonthValue = specificMonthValue;
	}



	public boolean isSpecificMonth() {
		return specificMonth;
	}



	public void setSpecificMonth(boolean specificMonth) {
		this.specificMonth = specificMonth;
	}



	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	/***********************************************************************************************************/

	public String getFrequency() {
		return specificMonthValue;
	}

	public void setFrequency(String frequency) {
		this.specificMonthValue = frequency;
	}
	
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		if (arg0 instanceof CtcComponent) {
			CtcComponent salconfig = (CtcComponent) arg0;
			if (salconfig.getName() != null) {
				return this.name.compareTo(salconfig.name);
			}
		}
		return 0;
	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate
	 * (com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		/**
		 * Date : 07-05-2018 BY ANIL 
		 * commenting code which checks for duplicate of short name
		 */
//		return isDuplicateName(m) || isDuplicateShortName(m);
		return isDuplicateName(m);
	}

	/**
	 * Checks if is duplicate name.
	 *
	 * @param m
	 *            the m
	 * @return true, if is duplicate name
	 */
	public boolean isDuplicateName(SuperModel m) {
		CtcComponent entity = (CtcComponent) m;
		String name = entity.getName().trim();
		String curname = this.name.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}

		// Old object is being updated
		else {

			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;

		}

	}

	/**
	 * Checks if is duplicate short name.
	 *
	 * @param m
	 *            the m
	 * @return true, if is duplicate short name
	 */
	public boolean isDuplicateShortName(SuperModel m) {
		CtcComponent entity = (CtcComponent) m;
		String name = entity.getShortName().trim();
		String curname = this.shortName.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}

		// Old object is being updated
		else {

			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;

		}

	}

	/**
	 * *****************************Getters and
	 * Setters****************************************.
	 *
	 * @return the name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		if (name != null)
			this.name = name.trim();
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * Gets the short name.
	 *
	 * @return the short name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Short Name")
	public String getShortName() {
		return shortName;
	}

	/**
	 * Sets the short name.
	 *
	 * @param shortName
	 *            the new short name
	 */
	public void setShortName(String shortName) {
		if (shortName != null)
			this.shortName = shortName.trim();
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * Gets the max per of ctc.
	 *
	 * @return the max per of ctc
	 */
	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 3, isFieldUpdater = false, isSortable = true, title = "% of CTC")
	public Double getMaxPerOfCTC() {

		return maxPerOfCTC;
	}

	/**
	 * Sets the max per of ctc.
	 *
	 * @param maxPerOfCTC
	 *            the new max per of ctc
	 */
	public void setMaxPerOfCTC(Double maxPerOfCTC) {
		if(maxPerOfCTC!=null){
			this.maxPerOfCTC = maxPerOfCTC;
		}
		else{
			this.maxPerOfCTC=0;
		}
	}

	/**
	 * Gets the max amount.
	 *
	 * @return the max amount
	 */
	public Integer getMaxAmount() {
		return maxAmount;
	}

	/**
	 * Sets the max amount.
	 *
	 * @param maxAmount
	 *            the new max amount
	 */
	public void setMaxAmount(int maxAmount) {
		this.maxAmount = maxAmount;
	}

	/**
	 * Checks if is aplicablity.
	 *
	 * @return true, if is aplicablity
	 */
	public boolean isAplicablity() {
		return Aplicablity;
	}

	/**
	 * Sets the aplicablity.
	 *
	 * @param aplicablity
	 *            the new aplicablity
	 */
	public void setAplicablity(boolean aplicablity) {
		Aplicablity = aplicablity;
	}

	/**
	 * Checks if is deduction.
	 *
	 * @return true, if is deduction
	 */
	public boolean isDeduction() {
		return isDeduction;
	}

	/**
	 * Sets the deduction.
	 *
	 * @param isDeduction
	 *            the new deduction
	 */
	public void setDeduction(boolean isDeduction) {
		this.isDeduction = isDeduction;
	}

	/**
	 * Sets the max amount.
	 *
	 * @param maxAmount
	 *            the new max amount
	 */
	public void setMaxAmount(Integer maxAmount) {
		this.maxAmount = maxAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof CtcComponent) {
				CtcComponent head = (CtcComponent) obj;
				return head.getName().equalsIgnoreCase(this.name);
			}
			return false;
		}
		return false;
	}

	@Override
	public String toString() {
		return name;
	}

	public Boolean getIsRecord() {
		return isRecord;
	}

	public void setIsRecord(Boolean isRecord) {
		this.isRecord = isRecord;
	}

	public Boolean getIsNonMonthly() {
		return isNonMonthly;
	}

	public void setIsNonMonthly(Boolean isNonMonthly) {
		this.isNonMonthly = isNonMonthly;
	}

	public String getCtcComponentName() {
		return ctcComponentName;
	}

	public void setCtcComponentName(String ctcComponentName) {
		this.ctcComponentName = ctcComponentName;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Integer getConditionValue() {
		return conditionValue;
	}

	public void setConditionValue(Integer conditionValue) {
		this.conditionValue = conditionValue;
	}

	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	public CtcComponent Myclone() {
		// TODO Auto-generated method stub
		CtcComponent temp=new CtcComponent();

		temp.id=null;
		temp.companyId=companyId;
		temp.setDeleted(isDeleted());
		temp.setStatus(status);
		temp.name =name;
		temp.shortName = shortName;
		temp.status = status;
		temp.amount = amount;
		temp.maxAmount = maxAmount;
		temp.Aplicablity =Aplicablity;
		temp.isDeduction = isDeduction;
		temp.isRecord = isRecord;
		temp.isNonMonthly = isNonMonthly;
		temp.ctcComponentName = ctcComponentName;
		temp.condition = condition;
		temp.conditionValue = conditionValue;
		temp.srNo = srNo;
		temp.gender = gender;
		temp.specificMonthValue = specificMonthValue;
		temp.specificMonth = specificMonth;
		temp.actualAmount =actualAmount;
		temp.excludeInPayRoll = excludeInPayRoll;
		temp.recordTobeDeletedinTable = recordTobeDeletedinTable;
		temp.isTDS = isTDS;
		
		temp.isRevised=isRevised;
		temp.oldAmountMonthly=oldAmountMonthly;
		temp.oldAmountAnnually=oldAmountAnnually;
		temp.isArrears=isArrears;
		temp.arrearOf=arrearOf;
		temp.baseAmount=baseAmount;
		temp.percent1=percent1;
		temp.percent2=percent2;
		temp.fromRangeAmount=fromRangeAmount;
		temp.toRangeAmount=toRangeAmount;
		
		return temp;
	}

	public boolean isTDS() {
		return isTDS;
	}

	public void setTDS(boolean isTDS) {
		this.isTDS = isTDS;
	}
	
}
