package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class LWFBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3253947649202048093L;
	/**
	 * 
	 */
	
	double employeeContribution;
	double employerContribution;
	@Index
	String period;
	@Index
	String deductionMonth;
	@Index
	double fromAmt;
	@Index
	double toAmt;
	
	public LWFBean(){
		
	}

	public double getEmployeeContribution() {
		return employeeContribution;
	}

	public void setEmployeeContribution(double employeeContribution) {
		this.employeeContribution = employeeContribution;
	}

	public double getEmployerContribution() {
		return employerContribution;
	}

	public void setEmployerContribution(double employerContribution) {
		this.employerContribution = employerContribution;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getDeductionMonth() {
		return deductionMonth;
	}

	public void setDeductionMonth(String deductionMonth) {
		this.deductionMonth = deductionMonth;
	}

	public double getFromAmt() {
		return fromAmt;
	}

	public void setFromAmt(double fromAmt) {
		this.fromAmt = fromAmt;
	}

	public double getToAmt() {
		return toAmt;
	}

	public void setToAmt(double toAmt) {
		this.toAmt = toAmt;
	}

}
