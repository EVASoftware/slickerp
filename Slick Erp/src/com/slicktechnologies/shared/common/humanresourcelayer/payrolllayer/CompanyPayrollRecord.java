package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.google.gwt.core.shared.GwtIncompatible;
@Entity
public class CompanyPayrollRecord extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2572584854971043891L;

	@Index
	protected int empId;
	@Index
	protected String empName;
	@Index
	protected String payrollMonth;
	
	protected String ctcComponentName;
	protected double ctcAmount;
	protected String userId;

	/**
	 * Date : 13-07-2018 BY ANIL
	 */
	@Index
	protected String ctcCompShortName;
	/**
	 * End
	 */
	
	/**
	 * @author Anil ,Date : 11-04-2019
	 */
	protected int amtRange;
	
	/**
	 * @author Anil , Date : 01-07-2019
	 * Storing base amount on which the deduction is applicable
	 */
	
	protected double baseAmt;
	
	/**
	 * @author Anil, Date : 18-07-2019
	 * added projectName;
	 */
	@Index
	String projectName;
	
	double ctcCompPercent;
	
	/**
	 * @author Anil @since 04-02-2021
	 * Adding sitelocation and state field for 
	 * updating employer's contributions as per the state and site 
	 */
	String siteLocation;
	String state;
	
	
	public CompanyPayrollRecord() {
		ctcCompShortName="";
	}
	
	
	
	
	

	public double getCtcCompPercent() {
		return ctcCompPercent;
	}






	public void setCtcCompPercent(double ctcCompPercent) {
		this.ctcCompPercent = ctcCompPercent;
	}






	public String getProjectName() {
		return projectName;
	}






	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}






	public double getBaseAmt() {
		return baseAmt;
	}




	public void setBaseAmt(double baseAmt) {
		this.baseAmt = baseAmt;
	}




	public String getCtcCompShortName() {
		return ctcCompShortName;
	}




	public void setCtcCompShortName(String ctcCompShortName) {
		this.ctcCompShortName = ctcCompShortName;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPayrollMonth() {
		return payrollMonth;
	}

	public void setPayrollMonth(String payrollMonth) {
		this.payrollMonth = payrollMonth;
	}

	public String getCtcComponentName() {
		return ctcComponentName;
	}

	public void setCtcComponentName(String ctcComponentName) {
		this.ctcComponentName = ctcComponentName;
	}

	public double getCtcAmount() {
		return ctcAmount;
	}

	public void setCtcAmount(double ctcAmount) {
		this.ctcAmount = ctcAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
	
	public int getAmtRange() {
		return amtRange;
	}




	public void setAmtRange(int amtRange) {
		this.amtRange = amtRange;
	}




	public String getSiteLocation() {
		return siteLocation;
	}






	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}






	public String getState() {
		return state;
	}






	public void setState(String state) {
		this.state = state;
	}






	@GwtIncompatible
	public static CompanyPayrollRecord  createCompPayrollRec(int empId,String empName,String payrollMonth,String componentName,double amount,long companyId,String shortName,int amtRange,double baseAmt,String projectName,double ctcCompPercent,String siteLocation,String state){
		
		CompanyPayrollRecord comPayRec=new CompanyPayrollRecord();
		comPayRec.setEmpId(empId);
		comPayRec.setEmpName(empName);
		comPayRec.setPayrollMonth(payrollMonth);
		comPayRec.setCtcComponentName(componentName);
		comPayRec.setCtcAmount(amount);
		comPayRec.setCompanyId(companyId);
		
		comPayRec.setCtcCompShortName(shortName);
		/**
		 * 
		 */
		comPayRec.setAmtRange(amtRange);
		
		comPayRec.setBaseAmt(baseAmt);
		comPayRec.setProjectName(projectName);
		comPayRec.setCtcCompPercent(ctcCompPercent);
		
		comPayRec.setSiteLocation(siteLocation);
		comPayRec.setState(state);
		
		return comPayRec;
	}
	
	
	
//	@GwtIncompatible
//	public static CompanyPayrollRecord  createCompPayrollRec(int empId,String empName,String payrollMonth,List<CtcComponent>deductionList,int monthlyDays,double paidDays,CTC ctc){
//		
//		CompanyPayrollRecord comPayRec=new CompanyPayrollRecord();
//		comPayRec.setEmpId(empId);
//		comPayRec.setEmpName(empName);
//		comPayRec.setPayrollMonth(payrollMonth);
//		
//		comPayRec.setCtcComponentDetails(deductionList,monthlyDays,paidDays,ctc);
//		
//		
//		
//		return comPayRec;
//	}

//	private void setCtcComponentDetails(List<CtcComponent> deductionList,int monthlyDays,double paidDays,CTC ctc) {
//		System.out.println("INSIDE COMPANY PAYROLL RECORD "+deductionList.size());
//		
//		for(int i=0;i<deductionList.size();i++){
//			System.out.println("Component Name :: " + i + " "+deductionList.get(i).getName() +"Depend ON "+" "+deductionList.get(i).getCtcComponentName()+" % "+ deductionList.get(i).getMaxPerOfCTC() );
//		}
//		
//		double actualSumDeadAmount=0;
//		
//		for (CtcComponent deductions : deductionList) {
//			double dedAmount = 0;
//			
//			if (deductions.getMaxPerOfCTC() != null) {
//				double dedutionAmountMonthly=getCtcComponentAmount(deductions.getCtcComponentName(),ctc.getEarning(), monthlyDays, paidDays);
//				
//				
//				if(deductions.getCondition()!=null){
//					ctcComponentName=deductions.getName();
//					int applicableAmountAnnualy=deductions.getConditionValue();
//					double applicableAmtMonthly=applicableAmountAnnualy/12;
//					
//					System.out.println("CONDITION  :: "+deductions.getCondition());
//					
//					if(deductions.getCondition().trim().equals("<")){
//							if(dedutionAmountMonthly<applicableAmtMonthly){
//								System.out.println("AMOUNT <");
//								dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
//							}
//					}
//					if(deductions.getCondition().trim().equals(">")){
//							if(dedutionAmountMonthly>applicableAmtMonthly){
//								System.out.println("AMOUNT >");
//								dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
//							}
//					}
//					if(deductions.getCondition().trim().equals("=")){
//							if(dedutionAmountMonthly==applicableAmtMonthly){
//								System.out.println("AMOUNT =");
//								dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
//							}
//					}
//					
//				}else{
//					dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
//				}
//			}
//			else {
//				dedAmount = deductions.getMaxAmount() / 12;
//			}
//
//			double oneDayDeduction=dedAmount/monthlyDays;
//			double actualDeduction=oneDayDeduction*paidDays;
//			
//			actualSumDeadAmount=actualSumDeadAmount+actualDeduction;
//		}
//		ctcAmount=Math.round(actualSumDeadAmount);
//		
//		System.out.println("CTC COMPONENT NAME : "+ctcComponentName);
//		System.out.println("CTC COMPONENT AMOUNT : "+ctcAmount);
//		
//	}
//	
//	public double getCtcComponentAmount(String ctcComponentName,List<CtcComponent> earningList,int monthlyDays,double paidDays){
//		double amount=0;
//		if(ctcComponentName.equals("Gross Earning")){
//			for (CtcComponent earnings : earningList) {
//				
//				double earning = earnings.getAmount() / 12;
//				
//				double oneDayEarning=earning/monthlyDays;
//				double actualearning=oneDayEarning*paidDays;
//				amount=amount+actualearning;
//			}
//		}else{
//			for(int i=0;i<earningList.size();i++){
//				if(earningList.get(i).getName().equals(ctcComponentName)){
//					amount=earningList.get(i).getAmount()/ 12;
//				}
//			}
//		}
//		return amount;
//	}


}
