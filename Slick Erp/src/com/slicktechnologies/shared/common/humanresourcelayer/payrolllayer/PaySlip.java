package com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPf;
import com.slicktechnologies.shared.common.humanresourcelayer.VoluntaryPfHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Emi;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Loan;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneEmi;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar.WeekleyOff;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Pay Slip Class representing the Pay Slip of an Employee.
 * Entity is created when Pay Roll Process is executed.
 * To Do :<b>Pay Slip Period is monthly right now but it should be configurable.</b>
 * 
 * 
 */
@Entity
public class PaySlip extends EmployeeHRProcess
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5545962378806784314L;
	
//	Logger logger=Logger.getLogger("Pay SLIP Logger");

	/** The earning list corresponding to Pay Slip will come from ctc. */
	@Serialize
	protected ArrayList<CtcComponent> earningList;

	/**
	 * The deduction list. corresponding to Pay Slip will Come from Company
	 * Settings
	 */
	@Serialize
	protected ArrayList<CtcComponent> deductionList;

	int monthlyDays;
	int weekDays;
	int holiday;
	double unpaidDay;
	double overTimehrs;
	double overtimeDay;
	double overtimeSalary;
	double earnedLeave;
	
	
	
	
	double grossEarning;
	double totalDeduction;
	double netEarning;

	protected ArrayList<AvailedLeaves> aviledLeaves;

	protected double ctcAmount;

	/**
	 * The eligible days in which employee is eleiginle to be paid in this pay
	 * slip period.
	 */
	protected double eligibleDays;

	/** Total Number of Paid Days. */
	protected double paidDays;

	/** Esic Number of Employee */
	protected String esicNumber;

	/** Pan Number of Employee */
	protected String panNumber;

	/** Pf Number of Employee */
	protected String pfNumber;

	/** Account number of Employee */
	protected String accountNumber;

	/** IIFSC code. of Employee */
	protected String ifscNumber;

	/** Branch of Employee */
	protected String bankBranch;
	/** Name of the Bank */
	protected String bankName;

	/** The salary period. A String repersenting Month-Year combination */
	@Index
	protected String salaryPeriod;

	protected Date dob;

	protected double monthlyEarnings;
	protected double netPayBeforeDeduction;
	protected double netDeduction;

	@Serialize
	protected List<PaidEmis> loneEmis;
	
	
	/**
	 * Date  : 11-07-2018 BY ANIL
	 */
	@Index 
	String branch;
	String uin;
	Date doj;
	String uanNo;
	String aadharNo;
	String fatherName;
	double grossEarningWithoutOT;
	double netEarningWithoutOT;
	String empEmail;

	/**
	 * Instantiates a new pay slip.
	 */
	
	/**
	 * Rahul Verma added this on 14 July 2018 Bonus and Paid Leaves
	 */
	@Index
	double bonus;
	@Index
	double paidLeaves;
	/**
	 * Ends
	 */
	/**
	 * Rahul Verma added on 16 July 2018
	 * 
	 */

	ArrayList<EmployeeCTCTemplate> employeeCTCList;

	/**
	 * Ends
	 */
	
	
	/**
	 * Date : 30-07-2018 By ANIL
	 * As per Nitin sir's instruction
	 * adding following fields
	 */
	String payMode;
	@Index
	String projectName;
	String shiftName;
	String shiftFrom;
	String shiftTo;
	@Index
	String gender;
	
	
	
	double ratePaidLeava;
	double rateBonus;
	
	int extraDay;
	
	/**
	 * Date : 11-09-2018 by ANIL
	 */
	@Index
	String happayCardNumber;
	
	@Index
	String spouseName;
	
	/**
	 * Date : 14-09-2018 BY ANIL
	 */
	@Index
	boolean isFreeze;
	@Index
	Date dateOfFreeze;
	String freezeBy;
	/** date 6.11.2018 added by komal for total companyContribution **/
	double totalCompanyContribution;
	
	/**
	 * @author Anil,Date : 22-02-2019
	 * Paid leave and bonus as additional allowance
	 */
	@Index
	boolean paidLeaveAndBonusAsAdditionalAllowance;
	
	/**
	 * @author Anil , Date : 09-03-2019
	 * Storing month wise leave details
	 */
	ArrayList<AllocatedLeaves> leaveBalList=new ArrayList<AllocatedLeaves>();
	
	/**
	 * @author Anil ,Date : 28-03-2019
	 * storing client info,raised by sonu and nitin sir for v allaince
	 */
	@Index
	PersonInfo clientInfo;
	
	/**
	 * @author Anil ,Date : 15-04-2019
	 * 
	 */
	@Index
	boolean isOtIncludedInGross;
	
	/**
	 * @author Anil , Date : 12-06-2019
	 * Storing OT wise bifurcation
	 */
	ArrayList<EmployeeOvertimeHistory> empOtHistoryList=new ArrayList<EmployeeOvertimeHistory>();

	/** @author Anil , Date :19-06-2019
     * Pl and Bonus by formula for riseon by RT 
     **/
    @Index
    String plName;
    @Index
    boolean isPlIncludedInEarning;
    String plCorresponadenceName;
   
    @Index
    String bonusName;
    @Index
    boolean isBonusIncludedInEarning;
    String bonusCorresponadenceName;
    
    
    /**
     * @author Anil , Date : 10-07-2019
     * Storing CNC Id 
     * for EVA raised by Nitin sir and abhinav
     * @author Anil , Date : 17-07-2019
     * changing datatype from int to String for google data studio report(GP sheet)
     */
    @Index
    String cncId;
    boolean designationFlag=false;
    
    /**
     * @author Anil , 02-12-2019
     * fixed day wise payroll
     */
    double woDayAsExtraDay;
    int actualDaysInMonth;
    boolean fixedDayWisePayroll;
    
    /**
	 * @author Anil
	 * @since 29-07-2020
	 * Storing payroll cycle start and end date
	 */
	@Index
	Date startDate;
	@Index
	Date endDate;
	
	/**
	 * @author Anil
	 * @since 01-09-2020
	 * site location
	 */
	@Index
	String siteLocation;
	
	/**
	 * @author Anil @since 04-02-2021
	 */
	String state;
	
	public PaySlip() {
		super();
		earningList = new ArrayList<CtcComponent>();
		deductionList = new ArrayList<CtcComponent>();
		salaryPeriod = "";
		loneEmis = new ArrayList<PaidEmis>();
		netDeduction = 0d;
		aviledLeaves=new ArrayList<AvailedLeaves>();
		branch="";
		uin="";
		uanNo="";
		aadharNo="";
		fatherName="";
		empEmail="";
		bonus = 0;
		paidLeaves = 0;
		employeeCTCList = new ArrayList<EmployeeCTCTemplate>();
		
		payMode="";
		projectName="";
		shiftName="";
		shiftFrom="";
		shiftTo="";
		gender="";
		happayCardNumber="";
		spouseName="";
		isFreeze=false;
		freezeBy="";
	
	}
	
	

	public String getCncId() {
		return cncId;
	}



	public void setCncId(String cncId) {
		this.cncId = cncId;
	}



	public String getPlName() {
		return plName;
	}



	public void setPlName(String plName) {
		this.plName = plName;
	}



	public boolean isPlIncludedInEarning() {
		return isPlIncludedInEarning;
	}



	public void setPlIncludedInEarning(boolean isPlIncludedInEarning) {
		this.isPlIncludedInEarning = isPlIncludedInEarning;
	}



	public String getPlCorresponadenceName() {
		return plCorresponadenceName;
	}



	public void setPlCorresponadenceName(String plCorresponadenceName) {
		this.plCorresponadenceName = plCorresponadenceName;
	}



	public String getBonusName() {
		return bonusName;
	}



	public void setBonusName(String bonusName) {
		this.bonusName = bonusName;
	}



	public boolean isBonusIncludedInEarning() {
		return isBonusIncludedInEarning;
	}



	public void setBonusIncludedInEarning(boolean isBonusIncludedInEarning) {
		this.isBonusIncludedInEarning = isBonusIncludedInEarning;
	}



	public String getBonusCorresponadenceName() {
		return bonusCorresponadenceName;
	}



	public void setBonusCorresponadenceName(String bonusCorresponadenceName) {
		this.bonusCorresponadenceName = bonusCorresponadenceName;
	}



	public ArrayList<EmployeeOvertimeHistory> getEmpOtHistoryList() {
		return empOtHistoryList;
	}

	public void setEmpOtHistoryList(List<EmployeeOvertimeHistory> empOtHistoryList) {
		ArrayList<EmployeeOvertimeHistory> empOtHistList=new ArrayList<EmployeeOvertimeHistory>();
		empOtHistList.addAll(empOtHistoryList);
		this.empOtHistoryList = empOtHistList;
	}

	public boolean isOtIncludedInGross() {
		return isOtIncludedInGross;
	}



	public void setOtIncludedInGross(boolean isOtIncludedInGross) {
		this.isOtIncludedInGross = isOtIncludedInGross;
	}



	public PersonInfo getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(PersonInfo clientInfo) {
		this.clientInfo = clientInfo;
	}


	public ArrayList<AllocatedLeaves> getLeaveBalList() {
		return leaveBalList;
	}




	public void setLeaveBalList(ArrayList<AllocatedLeaves> leaveBalList) {
		ArrayList<AllocatedLeaves>leaveList=new ArrayList<AllocatedLeaves>();
		leaveList.addAll(leaveBalList);
		this.leaveBalList = leaveList;
	}




	public Date getDateOfFreeze() {
		return dateOfFreeze;
	}




	public void setDateOfFreeze(Date dateOfFreeze) {
		this.dateOfFreeze = dateOfFreeze;
	}




	public String getFreezeBy() {
		return freezeBy;
	}




	public void setFreezeBy(String freezeBy) {
		this.freezeBy = freezeBy;
	}




	public boolean isFreeze() {
		return isFreeze;
	}




	public void setFreeze(boolean isFreeze) {
		this.isFreeze = isFreeze;
	}




	public String getSpouseName() {
		return spouseName;
	}




	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}




	public String getHappayCardNumber() {
		return happayCardNumber;
	}




	public void setHappayCardNumber(String happayCardNumber) {
		this.happayCardNumber = happayCardNumber;
	}




	public int getExtraDay() {
		return extraDay;
	}




	public void setExtraDay(int extraDay) {
		this.extraDay = extraDay;
	}




	public double getRatePaidLeava() {
		return ratePaidLeava;
	}




	public void setRatePaidLeava(double ratePaidLeava) {
		this.ratePaidLeava = ratePaidLeava;
	}




	public double getRateBonus() {
		return rateBonus;
	}




	public void setRateBonus(double rateBonus) {
		this.rateBonus = rateBonus;
	}




	public String getGender() {
		return gender;
	}




	public void setGender(String gender) {
		this.gender = gender;
	}




	public String getShiftName() {
		return shiftName;
	}




	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}




	public String getShiftFrom() {
		return shiftFrom;
	}




	public void setShiftFrom(String shiftFrom) {
		this.shiftFrom = shiftFrom;
	}




	public String getShiftTo() {
		return shiftTo;
	}




	public void setShiftTo(String shiftTo) {
		this.shiftTo = shiftTo;
	}




	public String getPayMode() {
		return payMode;
	}




	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}




	public String getProjectName() {
		return projectName;
	}




	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}




	public ArrayList<EmployeeCTCTemplate> getEmployeeCTCTemplate() {
		return employeeCTCList;
	}

	public void setEmployeeCTCTemplate(
			List<EmployeeCTCTemplate> employeeCTCArrayList) {
		ArrayList<EmployeeCTCTemplate> employeeProjectArrayList = new ArrayList<EmployeeCTCTemplate>();
		employeeProjectArrayList.addAll(employeeCTCArrayList);
		this.employeeCTCList = employeeProjectArrayList;
	}
	
	public double getBonus() {
		return bonus;
	}



	public void setBonus(double bonus) {
		this.bonus = bonus;
	}



	public double getPaidLeaves() {
		return paidLeaves;
	}



	public void setPaidLeaves(double paidLeaves) {
		this.paidLeaves = paidLeaves;
	}



	public ArrayList<EmployeeCTCTemplate> getEmployeeCTCList() {
		return employeeCTCList;
	}



	public void setEmployeeCTCList(ArrayList<EmployeeCTCTemplate> employeeCTCList) {
		this.employeeCTCList = employeeCTCList;
	}



	public double getGrossEarningWithoutOT() {
		/**
		 * @author Anil , Date : 27-04-2019
		 * if overtime is included in gross earning then we will return gross earning instead gross earning without ot
		 * used on all statutory reports
		 * raised by Rahul Tiwari for v-alliance
		 * 
		 * if ot included in gross earning flag is true 
		 * then it will return gross earning else gross earning without ot
		 */
		if(this.isOtIncludedInGross){
			return grossEarning;
		}else{
			return grossEarningWithoutOT;
		}
	}
	
	/**
	 * @author Anil , Date : 27-04-2019
	 * this methods return gross earning without ot
	 * if ot included in gross earning flag true or false
	 */
	public double getGrossEarningActualWithoutOT() {
		return grossEarningWithoutOT;
	}

	public void setGrossEarningWithoutOT(double grossEarningWithoutOT) {
		this.grossEarningWithoutOT = grossEarningWithoutOT;
	}



	public double getNetEarningWithoutOT() {
		return netEarningWithoutOT;
	}



	public void setNetEarningWithoutOT(double netEarningWithoutOT) {
		this.netEarningWithoutOT = netEarningWithoutOT;
	}



	public String getEmpEmail() {
		return empEmail;
	}



	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}



	public String getFatherName() {
		return fatherName;
	}



	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}



	public String getUanNo() {
		return uanNo;
	}



	public void setUanNo(String uanNo) {
		this.uanNo = uanNo;
	}



	public String getAadharNo() {
		return aadharNo;
	}



	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}



	public Date getDoj() {
		return doj;
	}



	public void setDoj(Date doj) {
		this.doj = doj;
	}



	public String getUin() {
		return uin;
	}



	public void setUin(String uin) {
		this.uin = uin;
	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}



	@Override
	public Long getEmpCellNo() {
		return super.getEmpCellNo();
	}

	@Override
	public int getEmpid() {
		return super.getEmpid();
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Employee Name")
	@Override
	public String getEmployeeName() {
		return super.getEmployeeName();
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Employee Type")
	@Override
	public String getEmployeeType() {
		return super.getEmployeeType();
	}

	@Override
	public String getEmployeeRole() {
		return super.getEmployeeRole();
	}

	/**
	 * Gets the tb esic no.
	 * @return the tb esic no
	 */

	public String getTbESICNo() {
		return esicNumber;
	}

	/**
	 * Sets the tb esic no.
	 * @param tbESICNo the new tb esic no
	 *            
	 */
	public void setTbESICNo(String tbESICNo) {
		if (tbESICNo != null)
			esicNumber = tbESICNo.trim();
	}

	/**
	 * Gets the tb pan no.
	 * @return the tb pan no
	 */

	public String getTbPanNo() {
		return panNumber;
	}

	/**
	 * Sets the tb pan no.
	 * @param tbPanNo the new tb pan no
	 *           
	 */
	public void setTbPanNo(String tbPanNo) {
		if (tbPanNo != null)
			this.panNumber = tbPanNo.trim();
	}

	/**
	 * Gets the tb pfac no.
	 * @return the tb pfac no
	 */

	public String getTbPFACNo() {
		return pfNumber;
	}

	/**
	 * Sets the tb pfac no.
	 * @param tbPFACNo the new tb pfac no
	 *            
	 */
	public void setTbPFACNo(String tbPFACNo) {
		if (tbPFACNo != null)
			this.pfNumber = tbPFACNo.trim();
	}

	/**
	 * Gets the tbacc no.
	 * @return the tbacc no
	 */
	public String getTbaccNo() {
		return accountNumber;
	}

	/**
	 * Sets the tbacc no.
	 * @param tbaccNo the new tbacc no
	 *            
	 */
	public void setTbaccNo(String tbaccNo) {
		this.accountNumber = tbaccNo;
	}

	/**
	 * Gets the tb ifsc.
	 * @return the tb ifsc
	 */
	public String getTbIFSC() {
		return ifscNumber;
	}

	/**
	 * Sets the tb ifsc.
	 *
	 * @param tbIFSC the new tb ifsc
	 *           
	 */
	public void setTbIFSC(String tbIFSC) {
		if (tbIFSC != null)
			this.ifscNumber = tbIFSC.trim();
	}

	// @TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater
	// = false, isSortable = true, title = "ECIS No")
	/**
	 * Gets the earning list.
	 *
	 * @return the earning list
	 */
	public ArrayList<CtcComponent> getEarningList() {
		return earningList;
	}

	/**
	 * Sets the earning list.
	 *
	 * @param list the new earning list
	 *            
	 */
	public void setEarningList(List<CtcComponent> list) {
		if (list != null) {
			this.earningList = new ArrayList<CtcComponent>();
			/**
			 * @author Anil, Date : 02-08-2019
			 * after manully updating salary slip
			 * arrear components were getting deleted
			 */
			for (CtcComponent comp : list) {
				if (comp.getAmount() != 0||comp.getActualAmount()!=0)
					this.earningList.add(comp);
			}

		}
	}

	/**
	 * Gets the deduction list.
	 *
	 * @return the deduction list
	 */
	public ArrayList<CtcComponent> getDeductionList() {
		return deductionList;
	}

	/**
	 * Sets the deduction list.
	 *
	 * @param deductionCompList
	 *            the new deduction list
	 * @param ptDeduction 
	 * @param ptDeduction 
	 * @param empGender 
	 * @param deductionTypeName 
	 * @param pfMaxFixedValue 
	 * @param numberRangeAsEmpTyp 
	 * @param salaryPeriod
	 * @param pf 
	 * @param esic 
	 */
	@GwtIncompatible
	public void setDeductionList(List<CtcComponent> deductionCompList, List<CtcComponent> ptDeduction, String empGender, 
			double pfMaxFixedValue, String deductionTypeName, String numberRangeAsEmpTyp, 
			String salaryPeriod,ArrayList<CtcComponent>arrearDeductionList,List<ProfessionalTax>ptList,String month,EmployeeInfo info,double applicableDisabiltyPercentage,Date date,boolean isProvisional, ProvidentFund pf, Esic esic) {
		
		ArrayList<CtcComponent> pfList=new ArrayList<CtcComponent>();
		
		System.out.println("");
		System.out.println("");
		System.out.println("INSIDE PAYSLIP SETTING DEDUCTION LIST " +deductionCompList.size());
		this.deductionList = new ArrayList<CtcComponent>();
		ArrayList<CtcComponent> updatedDeductionList=new ArrayList<CtcComponent>();
		
		/**
		 * Date : 28-08-2018 By ANIL
		 * calculating arrears in PF
		 */
		double pfArrearAmt=getPFArrearsAmount();
		
		/**
		 * Date : 31-08-2018 By ANIL
		 * calculating Other allowance and HRA
		 */
		double otherArrearsAmt=getOtherArrearsAmount();
		
		
		
		if (deductionCompList != null) {
			double actualSumDeadAmount=0;
			/**
			 * Date : 29-05-2018 By ANIL for Sasha ERP
			 * for calculating pf on capped amount
			 */
			boolean pfFlag=false;
			
			for(CtcComponent deductions:deductionCompList){
				
				if(deductions.isNonMonthly==false&&deductions.isRecord==false
						&&!deductions.getShortName().equalsIgnoreCase("PT")){
					/**
					 * Added By Rahul
					 * isSpecificMonth true then add flat amount in deduction
					 * 
					 */
					boolean isSpecificMonth=false;
					double dedAmount = 0;
					double specificDeductionValue=0;
					try{
						isSpecificMonth=deductions.isSpecificMonth();
					}catch(Exception e){
						isSpecificMonth=false;
					}
					
					if(isSpecificMonth==false){
						
						/**
						 * In this if condition we calculate PF,ESIC
						 * PF is depend on CTC component Basic/DA
						 * ESIC is depend on CTC component Gross Earning
						 */
					if(deductions.getCtcComponentName()!=null&&!deductions.getCtcComponentName().equals("")
							&&deductions.getMaxPerOfCTC()!=null&&deductions.getMaxPerOfCTC()!=0
							&&(deductions.getMaxAmount()==null||deductions.getMaxAmount()==0)
							&&deductions.getAmount()!=0){
	//					if (deductions.getMaxPerOfCTC() != null&&deductions.getMaxPerOfCTC()!=0) {
						
						double dedutionAmountMonthly=getCtcComponentAmount(deductions.getCtcComponentName());
						System.out.println("MONTHLY DEDUCTION AMOUNT :: "+dedutionAmountMonthly);
						
						/**
						 * Date : 29-08-2018 BY ANIL
						 * updates ESCI Calculation for direct employee of Sasha
						 */
						if(deductionTypeName!=null&&!deductionTypeName.equals("")&&deductions.getShortName().equalsIgnoreCase("ESIC")){
							dedutionAmountMonthly=dedutionAmountMonthly-getCtcComponentAmount(deductionTypeName);
						}
						
						System.out.println("MAX CTC % "+deductions.getMaxPerOfCTC());
						/**
						 * ESCI calculation If condition
						 */
						if(deductions.getCondition()!=null&&!deductions.getCondition().equals("")){
							/**
							 * Date : 14-05-2018 By ANIL
							 * Earlier we were putting annual value in conditional value field ,now we are storing monthly value in it.
							 */
	//							int applicableAmountAnnualy=deductions.getConditionValue();
	//							double applicableAmtMonthly=applicableAmountAnnualy/12;
							
							int applicableAmtMonthly=deductions.getConditionValue();
							double applicableAmountAnnualy=applicableAmtMonthly*12;
							/**
							 * End
							 */
							
							System.out.println("APPLICABLE AMOUNT ANNUALY :: "+applicableAmountAnnualy);
							System.out.println("APPLICABLE AMOUNT MONTHLY :: "+applicableAmtMonthly);
							System.out.println("CONDITION  :: "+deductions.getCondition());
							
							if(deductions.getCondition().trim().equals("<")){
								if(dedutionAmountMonthly<applicableAmtMonthly){
									System.out.println("AMOUNT <");
									dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
									deductions.setAmount(dedAmount);
									updatedDeductionList.add(deductions);
								}
							}
							if(deductions.getCondition().trim().equals(">")){
								if(dedutionAmountMonthly>applicableAmtMonthly){
									System.out.println("AMOUNT >");
									dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
									deductions.setAmount(dedAmount);
									updatedDeductionList.add(deductions);
								}
							}
							if(deductions.getCondition().trim().equals("=")){
								if(dedutionAmountMonthly==applicableAmtMonthly){
									System.out.println("AMOUNT =");
									dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
									deductions.setAmount(dedAmount);
									updatedDeductionList.add(deductions);
								}
							}
							
						}/** PF Calculation in Else **/
						else{
							System.out.println("INSIDE ELSE CONDITION NULL");
							
							/**
							 * Date : 29-05-2018 By ANIL
							 * added above code for PF Calculation
							 * this calculation is basically for direct employee whose pf is deducted on basic and Da
							 */
							if(pfMaxFixedValue!=0&&deductions.getShortName().equalsIgnoreCase("PF")&&pfFlag==false){
								pfFlag=true;
								dedutionAmountMonthly=0;
								for(CtcComponent ded:deductionCompList){
									if(ded.getShortName().equalsIgnoreCase("PF")){
										dedutionAmountMonthly+=getCtcComponentAmount(ded.getCtcComponentName());
									}
								}
								if(dedutionAmountMonthly>pfMaxFixedValue){
	//									pfMaxLimitFlag=true;
									dedutionAmountMonthly=pfMaxFixedValue;
								}
								pfList.add(deductions);
							}else if(pfMaxFixedValue!=0&&deductions.getShortName().equalsIgnoreCase("PF")&&pfFlag==true){
								dedutionAmountMonthly=0;
								pfList.add(deductions);
							}
							/**
							*  Date : 23-11-20018 BY ANIL
							*  if we do not defined max limit for PF ,PF is not calculated
							**/
							else if(pfMaxFixedValue==0&&deductions.getShortName().equalsIgnoreCase("PF")&&pfFlag==false){
								pfFlag=true;
								dedutionAmountMonthly=0;
								for(CtcComponent ded:deductionCompList){
									if(ded.getShortName().equalsIgnoreCase("PF")){
										dedutionAmountMonthly+=getCtcComponentAmount(ded.getCtcComponentName());
									}
								}
								pfList.add(deductions);
							}else if(pfMaxFixedValue==0&&deductions.getShortName().equalsIgnoreCase("PF")&&pfFlag==true){
								dedutionAmountMonthly=0;
								pfList.add(deductions);
							}
							
							/**
							 * End
							 */
							/**
							 * Here we are setting rate amount
							 */
							dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
							deductions.setAmount(dedAmount);
							updatedDeductionList.add(deductions);
						}
					}
					/**
					 * Date : 12-05-2018 By ANIL
					 * if PT( Flat amount) amount is not zero
					 * Date : 13-07-2018 By ANIL
					 * As we PT is deducted on gross earning ,if gross amount is changes its PT deduction amount also gets changed
					 * thats why we are not calculating PT here.
					 */
					else if((deductions.getCtcComponentName()!=null&&!deductions.getCtcComponentName().equals(""))
							&&(deductions.getMaxPerOfCTC()==null||deductions.getMaxPerOfCTC()==0)
							&&(deductions.getMaxAmount()!=null&&deductions.getMaxAmount()!=0)&&deductions.getAmount()!=0){
						System.out.println("MAX CTC % NULL");
	//						dedAmount = deductions.getMaxAmount() / 12;
						dedAmount = deductions.getAmount() / 12;
						deductions.setAmount(dedAmount);
						updatedDeductionList.add(deductions);
					}
				}else{
					/**
					 * Rahul added specificDeductionValue
					 * LWF calculated here
					 */
					System.out.println("DEDUCTION AMOUNT : "+dedAmount);
					String[] salarySplitData=salaryPeriod.split("-");
					if(deductions.isSpecificMonth()){
						if(salarySplitData[1].trim().equalsIgnoreCase(deductions.getSpecificMonthValue().trim())){
							/**
							 * Date : 11-07-2018 BY ANIL
							 */
//							dedAmount=deductions.getAmount();
//							deductions.setAmount(dedAmount);
							
//							specificDeductionValue=deductions.getAmount();
//							deductions.setAmount(specificDeductionValue);
							/**
							 * End
							 */
							
							/**
							 * @author Anil ,Date : 11-04-2019
							 */
//							double grossEarning=getCtcComponentAmount("Gross Earning");
//							if(grossEarning>=deductions.getConditionValue()){
							if(grossEarning>=deductions.getFromRangeAmount()&&grossEarning<=deductions.getToRangeAmount()){
								double amtRange=0;
//								int counter=0;
								boolean isValid=true;
								for(int i=0;i<updatedDeductionList.size();i++){
//								for(CtcComponent c:updatedDeductionList){
									CtcComponent c=updatedDeductionList.get(i);
									if(c.getShortName().equalsIgnoreCase("LWF")
											||c.getShortName().equalsIgnoreCase("MWF")
											||c.getShortName().equalsIgnoreCase("MLWF")){
										isValid=false;
										amtRange=c.getConditionValue();
//										if(deductions.getConditionValue()>amtRange){
										if(grossEarning>=deductions.getFromRangeAmount()&&grossEarning<=deductions.getToRangeAmount()){
											updatedDeductionList.remove(i);
											i--;
											isValid=true;
										}
									}
//									counter++;
								}
								if(isValid){
									specificDeductionValue=deductions.getAmount();
									updatedDeductionList.add(deductions);
								}
								
							}
							
						}
					}
				}
					
					//DAte  : 11-07-2018 BY ANIL for LWF
					if (deductions.isSpecificMonth()) {
						actualSumDeadAmount = actualSumDeadAmount+ Math.round(specificDeductionValue);
						deductions.setActualAmount(Math.round(specificDeductionValue));
					} else {
						double oneDayDeduction = dedAmount / getMonthlyDays();
//						double actualDeduction = oneDayDeduction* getPaidDays();
						
						/**
						 * Date : 28-08-2018 By ANIL
						 * For PF arrears
						 * 
						 */
						
						double actualDeduction=0;
						if(deductions.getShortName().equalsIgnoreCase("PF")&&deductions.getAmount()!=0){
							/**
							 * Date : 11-10-2018 By ANIL
							 * Date : 27-10-2018 BY ANIL
							 * checked pfmaxFixedValue
							 */
//							actualDeduction = (oneDayDeduction* getPaidDays())+(pfArrearAmt*deductions.getMaxPerOfCTC()/100);
							double actualPfAmount=getPFAmount();
							if(pfMaxFixedValue!=0&&actualPfAmount>pfMaxFixedValue){
								actualDeduction = (pfMaxFixedValue*deductions.getMaxPerOfCTC()/100)+(pfArrearAmt*deductions.getMaxPerOfCTC()/100);
							}else if(pfMaxFixedValue==0&&actualPfAmount>pfMaxFixedValue){
								actualDeduction = (actualPfAmount*deductions.getMaxPerOfCTC()/100)+(pfArrearAmt*deductions.getMaxPerOfCTC()/100);
							}else{
								actualDeduction = (actualPfAmount*deductions.getMaxPerOfCTC()/100)+(pfArrearAmt*deductions.getMaxPerOfCTC()/100);
							}
							/**
							 * End
							 */
						}
						else{
							actualDeduction = oneDayDeduction* getPaidDays();
						}
						/**
						 * Ends
						 */
						
						

						/**
						 * Date : 11-07-2018 By ANIL Setting actual deduction to
						 * deduction list
						 * Date : 21-07-2018 BY ADDED ESIC ROUND OFF CODE
						 */
						if(deductions.getShortName().equalsIgnoreCase("ESIC")){
							actualDeduction +=((pfArrearAmt+otherArrearsAmt)*deductions.getMaxPerOfCTC()/100);
//							PaySlipServiceImpl impl=new PaySlipServiceImpl();
//							actualDeduction=impl.getEsicRoundOff(actualDeduction);
							actualDeduction=getEsicRoundOff(actualDeduction);
							deductions.setActualAmount(actualDeduction);
						}else{
							deductions.setActualAmount(Math.round(actualDeduction));
						}
						/**
						 * End
						 */
						actualSumDeadAmount = actualSumDeadAmount+ Math.round(actualDeduction) + Math.round(specificDeductionValue);
					}
				}
			}
			//Outer For loop terminates here
			                                                                                                           
			
			/**
			 * Date : 24-08-2018 By ANIL
			 * New logic for calculating pt state wise 
			 * Date : 14-11-2018 By  ANIL
			 * updated PT calculation logic for person with applicable disability
			 */
//			List<ProfessionalTax> ptList=new ArrayList<ProfessionalTax>();
//			String month = null;
			if(applicableDisabiltyPercentage!=0){
				if(!info.isDisabled()){
					CtcComponent ptComponent=getPtDetailsForEmployee(ptList,grossEarningWithoutOT,month,empGender);
					if(ptComponent!=null){
						actualSumDeadAmount=actualSumDeadAmount+ptComponent.getActualAmount();
						updatedDeductionList.add(ptComponent);
					}
				}else if(info.isDisabled()==true&&info.getDisablityPercentage()<applicableDisabiltyPercentage){
					CtcComponent ptComponent=getPtDetailsForEmployee(ptList,grossEarningWithoutOT,month,empGender);
					if(ptComponent!=null){
						actualSumDeadAmount=actualSumDeadAmount+ptComponent.getActualAmount();
						updatedDeductionList.add(ptComponent);
					}
				}
			}else{
				CtcComponent ptComponent=getPtDetailsForEmployee(ptList,grossEarningWithoutOT,month,empGender);
				if(ptComponent!=null){
					actualSumDeadAmount=actualSumDeadAmount+ptComponent.getActualAmount();
					updatedDeductionList.add(ptComponent);
				}
			}
			
			/**
			 * @author Anil , Date : 27-06-2019
			 * calculating PF on the basis of updated logic
			 */
			if(pf!=null){
				double pfRateAmt=calculatePfAmt(pf,earningList,pfMaxFixedValue,monthlyDays,paidDays,true,false,false,this,false);
				double pfActualAmt=calculatePfAmt(pf,earningList,pfMaxFixedValue,monthlyDays,paidDays,false,true,false,this,false);
				double pfBaseAmt=calculatePfAmt(pf,earningList,pfMaxFixedValue,monthlyDays,paidDays,false,true,false,this,true);
				
				
				CtcComponent comp=new CtcComponent();
				comp.setDeduction(true);
				comp.setName(pf.getPfName());
				comp.setShortName(pf.getPfShortName());
				comp.setActualAmount(pfActualAmt);
				comp.setAmount(pfRateAmt);
				comp.setBaseAmount(pfBaseAmt);
				comp.setMaxPerOfCTC(pf.getEmployeeContribution());
				
				actualSumDeadAmount=actualSumDeadAmount+pfActualAmt;
				updatedDeductionList.add(comp);
			
			}
			
			
			/**
			 * Date : 23-11-2018 By ANIL
			 * Voluntary PF Calculation
			 */
			CtcComponent voluntaryPf=getVoluntaryPf(info,pfList,date,isProvisional);
			if(voluntaryPf!=null){
				updatedDeductionList.add(voluntaryPf);
			}
			
			
			
			/**
			 * @author Anil , Date : 25-07-2019
			 * calculating ESIC on the basis of updated logic
			 */
			if(esic!=null){
				//ESIC,PAYSLIP,EMP CONT,EMPLR CONT,RATE,BASE
				double esicRateAmt=calculateEsicAmt(esic,this,false,false,true,false);
				double esicActualAmt=calculateEsicAmt(esic,this,true,false,false,false);
				double esicBaseAmt=calculateEsicAmt(esic,this,true,true,false,true);
				
				
				CtcComponent comp=new CtcComponent();
				comp.setDeduction(true);
				comp.setName(esic.getEsicName());
				comp.setShortName(esic.getEsicShortName());
				comp.setActualAmount(esicActualAmt);
				comp.setAmount(esicRateAmt);
				comp.setBaseAmount(esicBaseAmt);
				comp.setMaxPerOfCTC(esic.getEmployeeContribution());
				
				comp.setCondition("<");
				comp.setConditionValue((int) esic.getEmployeeToAmount());
				
				actualSumDeadAmount=actualSumDeadAmount+esicActualAmt;
				if(esicActualAmt!=0){
					updatedDeductionList.add(comp);
				}
			
			}
			
			
			/**
			 * Date : 22-08-2018 By ANIL
			 * deduction arrear list
			 */
			for(CtcComponent comp:arrearDeductionList){
				actualSumDeadAmount=actualSumDeadAmount+comp.getActualAmount();
				updatedDeductionList.add(comp);
			}
			
			totalDeduction=actualSumDeadAmount;
			netEarning=Math.round(grossEarning-actualSumDeadAmount);
			netEarningWithoutOT=Math.round(grossEarningWithoutOT-actualSumDeadAmount);
			System.out.println("");
			System.out.println("Total Deduction :-- "+totalDeduction);
			System.out.println("Net Earning     :-- "+netEarning);
			
		}
		System.out.println("");
		System.out.println("GROSS EARNING   :: ---- "+grossEarning);
		System.out.println("TOTAL DEDUCTION :: ---- "+totalDeduction);
		System.out.println("NET EARNING     :: ---- "+netEarning);
		System.out.println("DEDUCTION LIST SIZE NM  :: "+updatedDeductionList.size());
		System.out.println("");
		System.out.println("");
		this.deductionList.addAll(updatedDeductionList);

	}
	
	
	public double calculateEsicAmt(Esic esic,PaySlip paySlip,boolean isEmployeeContribution, boolean isEmployerContribution, boolean isRate,boolean isBaseAmt) {
		// TODO Auto-generated method stub
		if(esic!=null){
			System.out.println("MASTER ESIC NOT NULL");
			double esicAmt=0;
			double baseAmt=0;
			double arrearAmt=0;
			double rateBaseAmount=0;
			/**
			 * @author Anil @since 02-04-2021
			 * commenting old logic of calculating rate base amount
			 */
//			for(CtcComponent comp:paySlip.getEarningList()){
//				rateBaseAmount=rateBaseAmount+comp.getAmount()/12;
//			}
			for(OtEarningComponent esicComp:esic.getCompList()){
				for(CtcComponent comp:paySlip.getEarningList()){
					if(isEmployeeContribution||isEmployerContribution){
						if(esicComp.getComponentName().equals(comp.getName())){
							baseAmt=baseAmt+comp.getActualAmount();
						}
						if(comp.getArrearOf()!=null&&esicComp.getComponentName().equals(comp.getArrearOf())){
							arrearAmt=arrearAmt+comp.getActualAmount();
						}
					}
					/**
					 * @author Anil @since 02-03-2021
					 * rate base amount should be calculated as per the component added on it
					 */
					if(esicComp.getComponentName().equals(comp.getName())){
						rateBaseAmount=rateBaseAmount+comp.getAmount()/12;
					}
				}
			}
			System.out.println("BASE AMT : "+baseAmt);
			System.out.println("Arrear AMT : "+arrearAmt);
			System.out.println("RATE BASE AMT : "+rateBaseAmount);
			
			if(isRate){
				esicAmt=(rateBaseAmount*esic.getEmployeeContribution())/100;
				return Math.round(esicAmt);
			}
			
			if(paySlip.isOtIncludedInGross){
				baseAmt=baseAmt+paySlip.overtimeSalary;
			}
			
			if(isBaseAmt){
				baseAmt=baseAmt+arrearAmt;
				return Math.round(baseAmt);
			}
			
			if(isEmployeeContribution){
				if(esic.isCalculateOnActual()){
					baseAmt=baseAmt+arrearAmt;
					esicAmt=(baseAmt*esic.getEmployeeContribution())/100;
					return getEsicRoundOff(esicAmt);
				}else{
					if(rateBaseAmount>=esic.getEmployeeFromAmount()&&rateBaseAmount<=esic.getEmployeeToAmount()){
						baseAmt=baseAmt+arrearAmt;
						esicAmt=(baseAmt*esic.getEmployeeContribution())/100;
						return getEsicRoundOff(esicAmt);
					}
				}
				
			}
			if(isEmployerContribution){
				if(esic.isCalculateOnActual()){
					baseAmt=baseAmt+arrearAmt;
					esicAmt=(baseAmt*esic.getEmployerContribution())/100;
					return getEsicRoundOff(esicAmt);
				}else{
					if(rateBaseAmount>=esic.getEmployerFromAmount()&&rateBaseAmount<=esic.getEmployerToAmount()){
						baseAmt=baseAmt+arrearAmt;
						esicAmt=(baseAmt*esic.getEmployerContribution())/100;
						return getEsicRoundOff(esicAmt);
					}
				}
			}
		}
		return 0;
	}



	public double calculatePfAmt(ProvidentFund pf,
			ArrayList<CtcComponent> earningCompList, Double pfMaxFixedValue,
			int monthlyDays, double paidDays, boolean isRate, boolean isEmployee, boolean isEmployer,PaySlip paySlip,boolean isBaseAmt) {
		// TODO Auto-generated method stub
		Logger logger = Logger.getLogger("Payslip");
		logger.log(Level.SEVERE, "in payEmi");
		
		if(pf!=null){
			System.out.println("MASTER PF NOT NULL");
			double pfAmt=0;
			double baseAmt=0;
			double arrearAmt=0;
			for(OtEarningComponent pfComp:pf.getCompList()){
				for(CtcComponent comp:earningCompList){
					
					if(isEmployee||isEmployer){
						if(pfComp.getComponentName().equals(comp.getName())){
						//	logger.log(Level.SEVERE, "calculatePfAmt compname="+comp.getName()+" actual comp amount="+comp.getActualAmount());
							baseAmt=baseAmt+comp.getActualAmount();
							
						}
						if(comp.getArrearOf()!=null&&pfComp.getComponentName().equals(comp.getArrearOf())){
							arrearAmt=arrearAmt+comp.getActualAmount();
						}
					}
					
					if(isRate){
						if(pfComp.getComponentName().equals(comp.getName())){
							baseAmt=baseAmt+comp.getAmount()/12;
						}
					}
				}
			}
			logger.log(Level.SEVERE,"BASE AMT : "+baseAmt);
			logger.log(Level.SEVERE,"Arrear AMT : "+arrearAmt);
			
			if(isRate){
				if(pfMaxFixedValue!=null&&pfMaxFixedValue!=0&&baseAmt>pfMaxFixedValue){
					baseAmt=pfMaxFixedValue;
				}
				pfAmt=(baseAmt*pf.getEmployeeContribution())/100;
				return Math.round(pfAmt);
			}
			
//			if(paySlip.isOtIncludedInGross){
////				double amt=(paySlip.overtimeSalary/paySlip.paidDays)*paySlip.monthlyDays;
//				baseAmt=baseAmt+paySlip.overtimeSalary;
//			}
			
			if(isBaseAmt){
				if(pfMaxFixedValue!=null&&pfMaxFixedValue!=0&&baseAmt>pfMaxFixedValue){
					baseAmt=pfMaxFixedValue;
				}
				baseAmt=baseAmt+arrearAmt;
				return Math.round(baseAmt);
			}
			
			if(isEmployee){
				if(pfMaxFixedValue!=null&&pfMaxFixedValue!=0&&baseAmt>pfMaxFixedValue){
					baseAmt=pfMaxFixedValue;
				}
				baseAmt=baseAmt+arrearAmt;
				pfAmt=(baseAmt*pf.getEmployeeContribution())/100;
				logger.log(Level.SEVERE, "pfAmt"+pfAmt);
				
				return Math.round(pfAmt);
			}
			
			if(isEmployer){
				if(pfMaxFixedValue!=null&&pfMaxFixedValue!=0&&baseAmt>pfMaxFixedValue){
					baseAmt=pfMaxFixedValue;
				}
				baseAmt=baseAmt+arrearAmt;
				pfAmt=(baseAmt*pf.getEmployerContribution())/100;
				return Math.round(pfAmt);
			}
			
		}
		
		return 0;
	}



	@GwtIncompatible
	private CtcComponent getVoluntaryPf(EmployeeInfo info, ArrayList<CtcComponent> pfList, Date date, boolean isProvisional) {
		
		CtcComponent volPfCtcComp=null;
		if(pfList==null || pfList.size()==0){
			return null;
		}
//		VoluntaryPf volPf=null;
		VoluntaryPf volPf=ofy().load().type(VoluntaryPf.class).filter("companyId", info.getCompanyId()).filter("empId", info.getEmpCount()).filter("status", true).first().now();
		if(volPf!=null){
			if((date.after(volPf.getApplicableFromDate())||date.equals(volPf.getApplicableFromDate()))
					&&volPf.getApplicableToDate()!=null&&(date.before(volPf.getApplicableToDate())||date.equals(volPf.getApplicableToDate()))){
				double dedutionAmountMonthly=0;
				for(CtcComponent com:pfList){
					dedutionAmountMonthly+=getCtcComponentAmount(com.getCtcComponentName());
				}
				
				volPfCtcComp=new CtcComponent();
				volPfCtcComp.setSrNo(pfList.get(0).getSrNo());
				volPfCtcComp.setName(volPf.getName());
				volPfCtcComp.setShortName(volPf.getName());
				volPfCtcComp.setCtcComponentName(pfList.get(0).getCtcComponentName());
				volPfCtcComp.setMaxPerOfCTC(volPf.getVoluntaryPfPercentage());
				volPfCtcComp.setGender(pfList.get(0).getGender());
				
				double dedAmount=dedutionAmountMonthly*volPfCtcComp.getMaxPerOfCTC()/100;
				volPfCtcComp.setAmount(dedAmount);
				
				double oneDayDeduction = dedAmount / getMonthlyDays();
				double actualDeduction = oneDayDeduction* getPaidDays();
				actualDeduction = (oneDayDeduction* getPaidDays());
				volPfCtcComp.setActualAmount(actualDeduction);
				
			}
		}
		
		if(volPfCtcComp!=null){
			VoluntaryPfHistory vpfHis=new VoluntaryPfHistory();
			vpfHis.setCompanyId(volPf.getCompanyId());
			vpfHis.setEmpId(volPf.getEmpId());
			vpfHis.setEmpName(volPf.getEmpName());
			vpfHis.setEmpCellNo(volPf.getEmpCellNo());
			vpfHis.setName(volPf.getName());
			vpfHis.setPayrollMonth(this.getSalaryPeriod());
			vpfHis.setVoluntaryPfPercentage(volPf.getVoluntaryPfPercentage());
			vpfHis.setVoluntaryPfwageAmount(getPFAmount());
			vpfHis.setVoluntaryPfAmount(volPfCtcComp.getActualAmount());
			
			/**7-12-2018 branchname added by amol**/
			if(volPf.getBranchName()!=null){
			vpfHis.setBranchName(volPf.getBranchName());
			}
			/***15-1-2019 project name added by amol*****/
			if(volPf.getProjectName()!=null){
				vpfHis.setProjectName(volPf.getProjectName());
			}
			if(!isProvisional){
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(vpfHis);
			}
//			ofy().save().entity(vpfHis).now();
		}
		
		return volPfCtcComp;
	}




	public double getOtherArrearsAmount() {
		double amount=0;
		System.out.println("INSIDE OTHER EARNING ");
		/**
		 * @author Anil ,Date : 21-06-2019
		 * Added bonus and paid leave arrears on otherArearsAmount
		 */
		for(CtcComponent comp:earningList){
			if(comp.getArrearOf().equalsIgnoreCase("HRA")||comp.getArrearOf().trim().equalsIgnoreCase("Other Allowance")){
				amount+=comp.getActualAmount();
			}
			
			if(isBonusIncludedInEarning&&comp.getArrearOf().equalsIgnoreCase("Bonus")){
				amount+=comp.getActualAmount();
			}
			if(isPlIncludedInEarning&&comp.getArrearOf().equalsIgnoreCase("Paid Leave")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
	}




	public double getPFArrearsAmount() {
		double amount=0;
		for(CtcComponent comp:earningList){
			if((comp.getArrearOf()!=null&&comp.getArrearOf().equalsIgnoreCase("BASIC"))||(comp.getArrearOf()!=null&&comp.getArrearOf().equalsIgnoreCase("DA"))){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
	}
	
	public double getPFAmount() {
		double amount=0;
		for(CtcComponent comp:earningList){
			if(comp.getShortName().trim().equalsIgnoreCase("BASIC")||comp.getShortName().trim().equalsIgnoreCase("DA")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
	}
/***16-1-2019 added by amol for "total other earning"***/
public double getOtherEarningAmount() {
	double amount=0;
	for(CtcComponent comp:earningList){
		if(!comp.getShortName().trim().equalsIgnoreCase("BASIC")&&!comp.getShortName().trim().equalsIgnoreCase("DA")&&!comp.getShortName().trim().equalsIgnoreCase("HRA")){
			amount+=comp.getActualAmount();
		}
	}
	
	return amount;
	
	
}

/***Date 4-1-2019 added by Amol for BASIC in Earninglist***/
public double getBASICAmount(){
	double amount=0;
	for(CtcComponent comp:earningList){
		if(comp.getShortName().trim().equalsIgnoreCase("BASIC")){
			amount+=comp.getActualAmount();
		}
	}
	return amount;

}

/**Date 4-1-2019 added by Amol for DA in Earninglist**/
public double getDAAmount(){
	double amount=0;
	for(CtcComponent comp:earningList){
		if(comp.getShortName().trim().equalsIgnoreCase("DA")){
			amount+=comp.getActualAmount();
		}
	}
	return amount;

}




	
public double getDeductedPFAmount() {
		double amount=0;
		for(CtcComponent comp:deductionList){
			if(comp.getShortName().trim().equalsIgnoreCase("PF")||comp.getShortName().trim().equalsIgnoreCase("Provident Fund")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
	}

	/**26-12-2018 added by amol for hraamount***/
	public double getHRAAmount() {
		double amount=0;
		for(CtcComponent comp:earningList){
			if(comp.getShortName().trim().equalsIgnoreCase("HRA")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
	}
	
	/***16-1-2019 added by amol for "total other deduction, other than 5"***/

public double gettotalOtherDeductedAmount() {
	double amount=0;
	for(CtcComponent comp:deductionList){
		if(!comp.getShortName().trim().equalsIgnoreCase("PF")&&!comp.getShortName().trim().equalsIgnoreCase("ESIC")&&!comp.getShortName().trim().equalsIgnoreCase("PT")&&!comp.getShortName().trim().equalsIgnoreCase("MLWF")&&!comp.getShortName().trim().equalsIgnoreCase("IT")&&!comp.getShortName().trim().equalsIgnoreCase("ADVANCE")){
			amount+=comp.getActualAmount();
		}
	}
	
	return amount;
	
	
}
	
	
	/**26-12-2018 added by amol for deductionlist PF,ESIC,PT,MLWF,IT**/
	
	public double getDeductedESICAmount() {
		double amount=0;
		for(CtcComponent comp:deductionList){
			if(comp.getShortName().trim().equalsIgnoreCase("ESIC")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
		
		}
	
	
	public double getDeductedPTAmount() {
		double amount=0;
		for(CtcComponent comp:deductionList){
			if(comp.getShortName().trim().equalsIgnoreCase("PT")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
		
		}
	
	public double getDeductedLWFAmount() {
		double amount=0;
		for(CtcComponent comp:deductionList){
			if(comp.getShortName().trim().equalsIgnoreCase("LWF")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
		
		}
	
	public double getDeductedITAmount() {
		double amount=0;
		for(CtcComponent comp:deductionList){
			if(comp.getShortName().trim().equalsIgnoreCase("IT")){
				amount+=comp.getActualAmount();
			}
		}
		return amount;
		
		}
	
	
	
	
	
	



	public CtcComponent getPtDetailsForEmployee(List<ProfessionalTax> ptList, double grossEarning,String month,String gender) {
		if(ptList!=null&&ptList.size()!=0){
			for(ProfessionalTax pt:ptList){
				
				if(pt.getGender().equalsIgnoreCase("All")){
					if(pt.getRangeToAmt()!=0&&grossEarning>=pt.getRangeFromAmt()&&grossEarning<=pt.getRangeToAmt()){
						CtcComponent comp=new CtcComponent();
						comp.setDeduction(true);
						comp.setName(pt.getComponentName());
						comp.setShortName(pt.getShortName());
						if(pt.isExceptionForSpecificMonth==true&&pt.getSpecificMonth().equalsIgnoreCase(month)){
							comp.setActualAmount(Math.round(pt.getSpecificMonthAmount()));
							comp.setAmount(pt.getSpecificMonthAmount());
						}else{
							comp.setActualAmount(Math.round(pt.getPtAmount()));
							comp.setAmount(pt.getPtAmount());
						}
						return comp;
						
					}else if(pt.getRangeToAmt()==0&&grossEarning>=pt.getRangeFromAmt()){
						CtcComponent comp=new CtcComponent();
						comp.setDeduction(true);
						comp.setName(pt.getComponentName());
						comp.setShortName(pt.getShortName());
						if(pt.isExceptionForSpecificMonth==true&&pt.getSpecificMonth().equalsIgnoreCase(month)){
							comp.setActualAmount(Math.round(pt.getSpecificMonthAmount()));
							comp.setAmount(pt.getSpecificMonthAmount());
						}else{
							comp.setActualAmount(Math.round(pt.getPtAmount()));
							comp.setAmount(pt.getPtAmount());
						}
						return comp;
					}
				}else{
					
					if(pt.getGender().equals(gender)&&pt.getRangeToAmt()!=0&&grossEarning>=pt.getRangeFromAmt()&&grossEarning<=pt.getRangeToAmt()){
						CtcComponent comp=new CtcComponent();
						comp.setDeduction(true);
						comp.setName(pt.getComponentName());
						comp.setShortName(pt.getShortName());
						if(pt.isExceptionForSpecificMonth==true&&pt.getSpecificMonth().equalsIgnoreCase(month)){
							comp.setActualAmount(Math.round(pt.getSpecificMonthAmount()));
							comp.setAmount(pt.getSpecificMonthAmount());
						}else{
							comp.setActualAmount(Math.round(pt.getPtAmount()));
							comp.setAmount(pt.getPtAmount());
						}
						return comp;
						
					}else if(pt.getGender().equals(gender)&&pt.getRangeToAmt()==0&&grossEarning>=pt.getRangeFromAmt()){
						CtcComponent comp=new CtcComponent();
						comp.setDeduction(true);
						comp.setName(pt.getComponentName());
						comp.setShortName(pt.getShortName());
						if(pt.isExceptionForSpecificMonth==true&&pt.getSpecificMonth().equalsIgnoreCase(month)){
							comp.setActualAmount(Math.round(pt.getSpecificMonthAmount()));
							comp.setAmount(pt.getSpecificMonthAmount());
						}else{
							comp.setActualAmount(Math.round(pt.getPtAmount()));
							comp.setAmount(pt.getPtAmount());
						}
						return comp;
					}
					
					
					
				}
			}
		}
		return null;
	}
	




	private boolean validateDeduction(List<CtcComponent> ptDeductionList,CtcComponent currDed, double grossEarning) {
		for(CtcComponent appDed:ptDeductionList){
			if(currDed.getShortName().equals(appDed.getShortName())
					&&currDed.getGender().equals(appDed.getGender())){
				if(appDed.getAmount()!=0){
					System.out.println("CTC AMT "+appDed.getAmount()+" AMOUNT "+grossEarning+" MAX AMT "+appDed.getConditionValue()+" CURR MAX AMT "+currDed.getConditionValue());
//					if(ptDed.getMaxAmount()>=grossEarning&&ptDed.getMaxAmount()<deduction.getMaxAmount()){
//						return false;
//					}else{
//						ptDed.setAmount(0d);
//						return true;
//					}
					
					if(grossEarning>=appDed.getConditionValue()){
//					if(appDed.getConditionValue()>=grossEarning){
						if(appDed.getConditionValue()>currDed.getConditionValue()){
							return false;
						}else{
							appDed.setAmount(0d);;
							return true;
						}
					}else{
						appDed.setAmount(0d);;
						return true;
					}
				}
			}
		}
		return true;
	}
	/**
	 * Gets the eligible days.
	 *
	 * @return the eligible days
	 */
	public double getEligibleDays() {
		return eligibleDays;
	}

	/**
	 * Sets the eligible days.
	 *
	 * @param eligibleDays
	 *            the new eligible days
	 */
	public void setEligibleDays(double eligibleDays) {
//		eligibleDays = Math.round(eligibleDays * 100) / 100.0d;
		this.eligibleDays = eligibleDays;
	}

	/**
	 * Gets the paid days.
	 *
	 * @return the paid days
	 */
	public double getPaidDays() {
		return paidDays;
	}

	/**
	 * Sets the paid days.
	 *
	 * @param paidDays
	 *            the new paid days
	 */
	public void setPaidDays(double paidDays) {
		paidDays = Math.round(paidDays * 100) / 100.0d;
		this.paidDays = paidDays;

	}

	/**
	 * Gets the salary period.
	 *
	 * @return the salary period
	 */
	public String getSalaryPeriod() {
		return salaryPeriod;
	}

	/**
	 * Sets the salary period.
	 *
	 * @param salaryPeriod
	 *            the new salary period
	 */
	public void setSalaryPeriod(String salaryPeriod) {
		this.salaryPeriod = salaryPeriod;
	}

	public List<PaidEmis> getLoneEmis() {
		return loneEmis;
	}

	public void setLoneEmis(List<PaidEmis> loneEmis) {
		if (loneEmis != null) {
			this.loneEmis = new ArrayList<PaidEmis>();
			this.loneEmis.addAll(loneEmis);

		}
	}

	/**
	 * Sets the deduction list.
	 *
	 * @param deductionList the new deduction list
	 *            
	 */
	public void setDeductionList(ArrayList<CtcComponent> deductionList) {
		this.deductionList = deductionList;
	}

	/**
	 * Changes the Status of {@link Emi} from paid to unpaid if any unpaid
	 * {@link Loan} exists for this {@link Employee}
	 * 
	 * To Do : Loads all {@link LoneEmi} objects find ways to Load only Unpaid
	 * Emi objects.
	 */
	
//	@OnSave
	@GwtIncompatible
	private void payEmi() {
		Logger logger = Logger.getLogger("Payslip");
		logger.log(Level.SEVERE, "in payEmi");
		
		if (getId() == null) {
			
			List<LoneEmi> loneEmisLis = ofy().load().type(LoneEmi.class).filter("employeeKey", this.employeeInfoKey).list();
			logger.log(Level.SEVERE, "this.employeeInfoKey="+this.employeeInfoKey);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM");
			format.setTimeZone(TimeZone.getTimeZone("IST"));
			if(loneEmisLis!=null)
				logger.log(Level.SEVERE, "loneEmisList size="+loneEmisLis.size());
			else
				logger.log(Level.SEVERE, "loneEmisList null");
			for (LoneEmi loneEmis : loneEmisLis) {
				List<Emi> emiList = loneEmis.getEmi();

				for (Emi temp : emiList) {
					PaidEmis emi = new PaidEmis();
					String emiDate = format.format(temp.getDate());
					logger.log(Level.SEVERE, "emiDate="+emiDate+"salaryPeriod"+salaryPeriod);
					if (emiDate.equals(salaryPeriod)) {
						logger.log(Level.SEVERE, "dates matched");
						temp.setStatus(temp.PAID);
						emi.setEmi(temp);
						emi.setAdvanceKey(loneEmis.getLoanKey());
						this.loneEmis.add(emi);
						logger.log(Level.SEVERE, "loneEmis size ="+this.loneEmis.size());
						break;
					}

				}
			}
			if (loneEmisLis != null && loneEmisLis.size() != 0) {
				ofy().save().entities(loneEmisLis);

			}

		}

	}

	/**
	 * Fills Pay Slip Attributes from {@link Employee} The internal logic of
	 * Method will run only once when Pay Slip is created as we do not want to
	 * change these details when corresponding details changes in
	 * {@link Employee}
	 */
	@GwtIncompatible
	@OnSave
	private void fillPaySlipAttributes() {
		System.out.println("INSIDE PAY SLIP SAVE");
		if (getId() == null) {
			Employee emp = ofy().load().type(Employee.class)
					.filter("count", this.empid)
					.filter("companyId", getCompanyId()).first().now();
			EmployeeAdditionalDetails empAddDet = ofy().load().type(EmployeeAdditionalDetails.class)
					.filter("empInfo.empCount", this.empid).filter("companyId", getCompanyId()).first().now();
//			System.out.println("EMPLOYEE " + emp.toString());
			if (emp != null) {
				esicNumber = emp.getEmployeeESICcode();
				panNumber = emp.getEmployeePanNo();
				pfNumber = emp.getPPFNaumber();
				accountNumber = emp.getEmployeeBankAccountNo();
				ifscNumber = emp.getIfscCode();
				dob = emp.getDob();
				
//				bankBranch = emp.getEmployeeBankName();
//				bankName = emp.getBranchName();
				
				/**
				 * Date : 11-07-2018 by ANIL
				 */
				bankBranch =emp.getBankBranch();
				bankName =emp.getEmployeeBankName(); 
				branch=emp.getBranchName();
				
				String uin = "";
				String aadhar="";
				for (int i = 0; i < emp.getArticleTypeDetails().size(); i++) {
					if (emp.getArticleTypeDetails().get(i).getArticleTypeName().contains("UIN")) {
						uin = emp.getArticleTypeDetails().get(i).getArticleTypeValue();
					}
					if (emp.getArticleTypeDetails().get(i).getArticleTypeName().trim().toUpperCase().contains("AADHAR")) {
						aadhar = emp.getArticleTypeDetails().get(i).getArticleTypeValue();
					}
				}
				
				if(emp.getAadharNumber()!=0){
					aadhar=emp.getAadharNumber()+"";
				}
				this.uin=uin;
				this.aadharNo=aadhar;
				doj=emp.getJoinedAt();
				
				if(empAddDet!=null){
					for(EmployeeFamilyDeatails empFamDet:empAddDet.getEmpFamDetList()){
						if(empFamDet.getFamilyRelation().trim().toUpperCase().contains("FATHER")){
							fatherName=empFamDet.getFname()+" "+empFamDet.getLname();
//							break;
						}
						/**
						 * Date : 11-09-2018 By ANIL
						 */
						if(empFamDet.getFamilyRelation().trim().toUpperCase().contains("HUSBAND")){
							spouseName=empFamDet.getFname()+" "+empFamDet.getLname();
//							break;
						}
						if(empFamDet.getFamilyRelation().trim().toUpperCase().contains("WIFE")){
							spouseName=empFamDet.getFname()+" "+empFamDet.getLname();
//							break;
						}
						/**
						 * End
						 */
					}
				}
				
				empEmail=emp.getEmail();
				/**
				 * End
				 */
				
				/**
				 * Date : 04-08-2018 By ANIL
				 */
//				if(emp.getProjectName()!=null){
//					projectName=emp.getProjectName();
//				}
				
				if(emp.getGender()!=null){
					gender=emp.getGender();
				}
				/**
				 * Date : 22-08-2018 By ANIL
				 */
				if(emp.getUANno()!=null){
					uanNo=emp.getUANno();
				}
				/**
				 * Date : 11-09-2018 By ANIL
				 */
				if(emp.getHappayCardWalletNumber()!=null&&!emp.getHappayCardWalletNumber().equals("")){
					happayCardNumber=emp.getHappayCardWalletNumber();
				}
				
				getClientDetailsFromProject();
			}
		}

	}

	public Date getDateOfBirth() {
		return dob;
	}

	public String getEmpBankBranch() {
		return bankBranch;
	}

	public void setEmpBankBranch(String empBankBranch) {
		if (bankBranch != null)
			this.bankBranch = empBankBranch.trim();
	}

	public String getEmpBankName() {
		return bankName;
	}

	public void setEmpBankName(String empBankName) {
		if (bankName != null)
			this.bankName = empBankName.trim();
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	
	

	public boolean isPaidLeaveAndBonusAsAdditionalAllowance() {
		return paidLeaveAndBonusAsAdditionalAllowance;
	}




	public void setPaidLeaveAndBonusAsAdditionalAllowance(
			boolean paidLeaveAndBonusAsAdditionalAllowance) {
		this.paidLeaveAndBonusAsAdditionalAllowance = paidLeaveAndBonusAsAdditionalAllowance;
	}




	/**
	 * Method creates a {@link PaySlip} object.
	 * 
	 * @param ctc
	 *            {@link CTC} object corresponding to {@link PaySlip}
	 * @param workingDays
	 *            number of working days in month.
	 * @param eligleDays
	 *            number of eligible days in month.
	 * @param salaryPeriod
	 *            Salary Period.
	 * @param deduction
	 * @param ptDeduction 
	 * @param empGender 
	 * @param deductionTypeName 
	 * @param pfMaxFixedValue 
	 * @param numberRangeAsEmpTyp 
	 * @param info 
	 * @param extraDay 
	 * @param bonusAndPaidLeaveArrearsList 
	 * @param paidLeaveAndBonusAsAdditionalAllowance 
	 * @param otIncludedInGrossEarning 
	 * @param pf 
	 * @param projectName2 
	 * @param esic 
	 * @return
	 */

	@GwtIncompatible
	public static PaySlip createPaySlip(CTC ctc, int monthlyDayss, int weekDayss,
			int holidayss, double unpaidDayss,ArrayList<AvailedLeaves> availedLeavesList, 
			double workingDays,double eligleDays, String salaryPeriod, 
			List<CtcComponent> deduction,double overtimeHrs,double overtimeInDays,
			double overTymAmt,double earnedLeave,List<CtcComponent> ptDeduction, 
			String empGender, double pfMaxFixedValue, String deductionTypeName, 
			String numberRangeAsEmpTyp, EmployeeInfo info,ArrayList<CtcComponent>arrearEarnList,
			ArrayList<CtcComponent>arrearDeductionList,List<ProfessionalTax>ptList,String month, 
			int extraDay,double applicableDisabiltyPercentage,Date date,
			boolean isProvisional, ArrayList<CtcComponent> bonusAndPaidLeaveArrearsList, boolean paidLeaveAndBonusAsAdditionalAllowance,
			boolean otIncludedInGrossEarning, List<EmployeeOvertime> otList, ArrayList<EmployeeOvertimeHistory> empOtHisList, ProvidentFund pf, String projectName2, Esic esic,double woDayAsExtraDay,String extraDayCompName,int actualDaysInMonth,boolean fixedDayWisePayroll) {
		Logger logger=Logger.getLogger("Pay Pdf Logger");
		
		System.out.println(" ::: Availed LIST SIZE :::  "+availedLeavesList.size());
		
		PaySlip paySlip = new PaySlip();
		double emi=0;//Ashwini Patil
		
		paySlip.setProjectName(projectName2);
		HrProject project=ofy().load().type(HrProject.class).filter("companyId", info.getCompanyId()).filter("projectName", projectName2).first().now();//Ashwini Patil Date:18-07-2023
    	
		
		/**
		 * @author Anil, Date : 12-06-2019
		 */
		paySlip.setEmpOtHistoryList(empOtHisList);
		
		
		/**
		 * @author Anil , Date : 15-04-2019
		 * Setting ot flag
		 */
		paySlip.setOtIncludedInGross(otIncludedInGrossEarning);
		
		/**
		 * Rahul added on 14 July 2018 Desc: Earning components list on which
		 * are not excluded in ctc component
		 */
		ArrayList<CtcComponent> ctcEarningList = new ArrayList<CtcComponent>();
		ArrayList<CtcComponent> nonPayrollComponentList = new ArrayList<CtcComponent>();
		try{	
			for (CtcComponent earning : ctc.getEarning()) {
				if (!earning.isExcludeInPayRoll()) {
					/**
					 * @author Anil
					 * @since 09-09-2020
					 * Adding clone object in earning list to avoid data overriding
					 */
					CtcComponent comp=earning.Myclone();
//					ctcEarningList.add(earning);
					ctcEarningList.add(comp);
				} else {
					nonPayrollComponentList.add(earning);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		/**
		 * Ends
		 */
		
		/**
		 * Date : 22-08-2018 BY ANIL
		 */
		
		if(arrearEarnList.size()!=0){
			ctcEarningList.addAll(arrearEarnList);
		}
		/**
		 * End
		 */
		paySlip.setExtraDay(extraDay);
		paySlip.setEarningList(ctcEarningList);
		
		/**
		 * 
		 */
		
		paySlip.setWoDayAsExtraDay(woDayAsExtraDay);
		paySlip.setFixedDayWisePayroll(fixedDayWisePayroll);
		paySlip.setActualDaysInMonth(actualDaysInMonth);
		if(woDayAsExtraDay!=0){
			double extraDayRate=0;
			for(CtcComponent comp:paySlip.getEarningList()){
//				extraDayRate=extraDayRate+(comp.getAmount()/monthlyDayss);
				extraDayRate=extraDayRate+(comp.getAmount()/12);
			}
			double oneDayEarning=extraDayRate/monthlyDayss;
			double actualearning=oneDayEarning*woDayAsExtraDay;
			
			CtcComponent comp=new CtcComponent();
//			comp.setActualAmount(paySlip.getPaidLeaves()-paidLeaveArrear);
//			comp.setAmount(extraDayRate*woDayAsExtraDay);
			comp.setActualAmount(Math.round(actualearning));
			
			comp.setCompanyId(ctc.getCompanyId());
			if(extraDayCompName!=null&&!extraDayCompName.equals("")){
				comp.setName(extraDayCompName);
				comp.setShortName(extraDayCompName);
			}else{
				comp.setName("Extra Days");
				comp.setShortName("Extra Days");
			}
			paySlip.getEarningList().add(comp);
		}
		
		/**
		 * 
		 */
		
		
		/**
		 * @author Anil , Date : 02-10-2019
		 * Removing paid leave from working days
		 * 
		 */
		
		double paidLeaveInDays=0;
		if(availedLeavesList!=null&&availedLeavesList.size()!=0){
			double paidLeaveHours=0;
			for(AvailedLeaves empLeave:availedLeavesList){
				if(empLeave.isUnPaid()==false){
					paidLeaveHours=paidLeaveHours+empLeave.getLeavehour();
				}
			}
			if(paidLeaveHours!=0){
				paidLeaveInDays=paidLeaveHours/info.getLeaveCalendar().getWorkingHours();
//				workingDays=workingDays-paidLeaveInDays;
			}
		}
		/***
		 * 
		 * 
		 */
		if(fixedDayWisePayroll){
			workingDays=eligleDays-(paidLeaveInDays+holidayss);
		}else{
			workingDays=eligleDays-(paidLeaveInDays+weekDayss+holidayss);
		}
		
		paySlip.setFromdate(null);
		paySlip.setTodate(null);
		paySlip.setCountry(ctc.getCountry());
		paySlip.setBranch(ctc.getBranch());
		paySlip.setCreationDate(DateUtility.getDateWithTimeZone("IST",new Date()));
		paySlip.setStatus(null);
		
		
		paySlip.setMonthlyDays(monthlyDayss);
		paySlip.setWeekDays(weekDayss);
		paySlip.setHoliday(holidayss);
		paySlip.setUnpaidDay(unpaidDayss);
		paySlip.setAviledLeaves(availedLeavesList);
		
		paySlip.setOverTimehrs(overtimeHrs);
		paySlip.setOvertimeDay(overtimeInDays);
		
		paySlip.setEarnedLeave(earnedLeave);
		
		paySlip.setEmpid(ctc.getEmpid());
		paySlip.setEmployeeName(ctc.getEmployeeName());
		paySlip.setEmpCellNo(ctc.getEmpCellNo());
		paySlip.setBranch(ctc.getBranch());
		paySlip.setDepartment(ctc.getDepartment());
		paySlip.setEmployeeType(ctc.getEmployeeType());
		
		/**Date 4-10-2020 by Amol pick the designation from EmployeeInfo**/
		paySlip.setEmployeedDesignation(info.getDesignation());
//		paySlip.setEmployeedDesignation(ctc.getEmployeedDesignation());
		paySlip.setEmployeeRole(ctc.getEmployeeRole());
		paySlip.ctcAmount = ctc.ctcAmount;
		paySlip.setEligibleDays(workingDays);
		paySlip.setPaidDays(eligleDays);
		paySlip.setCompanyId(ctc.getCompanyId());
		paySlip.setSalaryPeriod(salaryPeriod);
		
		paySlip.calculateNetPayBeforeDeduction();
		emi=paySlip.calCulateNetDeduction(); 
		logger.log(Level.SEVERE, "received emi="+emi);
		/**
		 * @author Anil , Date : 21-06-2019
		 * Called this code below paid leave and bonus calculation
		 */
//		paySlip.calculateActualEarningComponentList(overTymAmt);
		
		/**
		 * Date : 18-05-2018 By ANIL
		 */
		System.out.println("DEDUDTION LIST SIZE BEFORE "+deduction.size());
		for(int i=0;i<deduction.size();i++){
			if(deduction.get(i).getShortName().equalsIgnoreCase("PT")){
				deduction.remove(i);
				i--;
			}
		}
		System.out.println("DEDUDTION LIST SIZE AFTER "+deduction.size());
		
		/**
		 * Date : 27-08-2018 BY ANIL
		 * new logic for calculating paid leave and bonus
		 * Date : 31-12-2018 BY ANIL
		 * Calculated bonus and paid leave arrear 
		 * @author Anil , Date : 21-06-2019
		 * capturing paid leave and bonus arrears for the puepose of showing it in on salary slip
		 */
		double paidLeaveArrear=0;
		double bonusArrear=0;
		double paidLeave=getPaidLeave(ctc,month,monthlyDayss,eligleDays);
		if(bonusAndPaidLeaveArrearsList.size()!=0){
//			paidLeave+=getPaidLeaveArrear(bonusAndPaidLeaveArrearsList);
			paidLeaveArrear=getPaidLeaveArrear(bonusAndPaidLeaveArrearsList);
			paidLeave+=paidLeaveArrear;
		}
		double bonus=getBonus(ctc,month,monthlyDayss,eligleDays);
		if(bonusAndPaidLeaveArrearsList.size()!=0){
//			bonus+=getBonusArrear(bonusAndPaidLeaveArrearsList);
			bonusArrear=getBonusArrear(bonusAndPaidLeaveArrearsList);
			bonus+=bonusArrear;
		}
		
		paySlip.setPaidLeaves(paidLeave);
		paySlip.setBonus(bonus);
		
		/**
		 * End
		 */
		
		/**
		 * @author Anil , Date : 19-06-2019
		 * Storing paid leave and bonus additional details
		 */
		
		paySlip.setPlName(ctc.getPlName());
		paySlip.setPlCorresponadenceName(ctc.getPlCorresponadenceName());
		paySlip.setPlIncludedInEarning(ctc.isPlIncludedInEarning());
		paySlip.setBonusName(ctc.getBonusName());
		paySlip.setBonusCorresponadenceName(ctc.getBonusCorresponadenceName());
		paySlip.setBonusIncludedInEarning(ctc.isBonusIncludedInEarning());
		
		if(paySlip.isPlIncludedInEarning()){
//			paySlip.getEarningList().add(addingPaidLeaveInEarning(paySlip,ctc,paidLeaveArrear));
			addingPaidLeaveInEarning(paySlip,ctc,paidLeaveArrear);
		}
		if(paySlip.isBonusIncludedInEarning()){
//			paySlip.getEarningList().add(addingBonusInEarning(paySlip,ctc,bonusArrear));
			addingBonusInEarning(paySlip,ctc,bonusArrear);
		}
		
		/**
		 * @author Anil , Date : 11-06-2019
		 * @since 02-03-2021 moved code below
		 * after calculating paid leave and bonus
		 */
		double formulaOtAmt=0;
		if(otList!=null&&otList.size()!=0){
			formulaOtAmt=getFormulaOtValue(otList,paySlip.getEarningList(),monthlyDayss,eligleDays,workingDays,paySlip);
		}
		overTymAmt=overTymAmt+formulaOtAmt;
		
		paySlip.setOvertimeSalary(Math.round(overTymAmt));
		
		/**
		 * @author Anil , Date : 21-08-2019
		 * adding ot in earning list of process configuration is true
		 */
		if(paySlip.isOtIncludedInGross&&paySlip.getEmpOtHistoryList()!=null&&paySlip.getEmpOtHistoryList().size()!=0){
			if(paySlip.getEmpOtHistoryList()!=null&&paySlip.getEmpOtHistoryList().size()!=0){
				ArrayList<CtcComponent> otList1=new ArrayList<CtcComponent>();
				
				for(EmployeeOvertimeHistory obj:paySlip.getEmpOtHistoryList()){
					if(otList1.size()!=0){
						boolean otValFlag=false;
						for(CtcComponent ctcComp:otList1){
							if(obj.isPayOtAsOther()==true){
								if(ctcComp.getName().equals(obj.getCorrespondanceName())){
									otValFlag=true;
									ctcComp.setActualAmount(Math.round(ctcComp.getActualAmount()+obj.getOtAmount()));
								}
							}else{
								if(ctcComp.getName().equals("Overtime")){
									otValFlag=true;
									ctcComp.setActualAmount(Math.round(ctcComp.getActualAmount()+obj.getOtAmount()));
								}
							}
						}
						if(otValFlag==false){
							CtcComponent comp=new CtcComponent();
							comp.setShortName("OT");
							if(obj.isPayOtAsOther()==true){
								comp.setName(obj.getCorrespondanceName());
							}else{
								comp.setName("Overtime");
							}
							comp.setActualAmount(Math.round(obj.getOtAmount()));
							otList1.add(comp);
						}
					}else{
						CtcComponent comp=new CtcComponent();
						comp.setShortName("OT");
						if(obj.isPayOtAsOther()==true){
							comp.setName(obj.getCorrespondanceName());
						}else{
							comp.setName("Overtime");
						}
						comp.setActualAmount(Math.round(obj.getOtAmount()));
						otList1.add(comp);
					}
				}
				if(otList1.size()!=0){
					paySlip.getEarningList().addAll(otList1);
				}
			}else{
				CtcComponent comp=new CtcComponent();
				comp.setName("Overtime");
				comp.setShortName("OT");
				comp.setActualAmount(Math.round(paySlip.getOvertimeSalary()));
				paySlip.getEarningList().add(comp);
			}
		}
		
		/**
		 * @author Anil, Date : 21-06-2019
		 */
		if(project!=null&&info.getLeaveCalendar()!=null)
			paySlip.calculateActualEarningComponentList(overTymAmt,project.isRotationalWeeklyOff(),salaryPeriod,info.getLeaveCalendar().getWeeklyoff(),project.getFixedPayrollDays());
		else if(project!=null)
			paySlip.calculateActualEarningComponentList(overTymAmt,project.isRotationalWeeklyOff(),salaryPeriod,null,project.getFixedPayrollDays());
		else
			paySlip.calculateActualEarningComponentList(overTymAmt,false,salaryPeriod,null,null);
		
		/**
		 * Adding salary period for specific type deductions
		 * Added by Rahul Verma on 26 June 2018
		 */
		paySlip.setDeductionList(deduction,ptDeduction,empGender,pfMaxFixedValue,deductionTypeName,numberRangeAsEmpTyp,salaryPeriod,arrearDeductionList,ptList,month,info,applicableDisabiltyPercentage,date,isProvisional,pf,esic);
		
		
		
		Employee employee = ofy().load().type(Employee.class).filter("companyId", info.getCompanyId())
				.filter("count", info.getEmpCount()).first().now();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM");
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		if (employee != null) {
			ArrayList<EmployeeCTCTemplate> validEmpTemplate = new ArrayList<EmployeeCTCTemplate>();

			for (EmployeeCTCTemplate empCtcTemplate : employee.getEmployeeCTCList()) {
				Date date1 = null;
				try {
					date1 = format.parse(salaryPeriod);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Date date2 = null;
				try {
					date2 = format.parse(format.format(empCtcTemplate.getEndDate()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				System.out.println("date1.compareTo(date2)"+ date1.compareTo(date2));
				if (date1.compareTo(date2) == -1||date1.compareTo(date2) == 0) {
					/**
					 * Date : 31 July 2018
					 * Dev: Rahul Verma
					 * Description: As suggested by Sanket this value should be on number of present days.
					 * (monthlyDayss-unpaidDayss)=Number of Paid Days. Since we dont have exact paid days
					 */
					double flatValue=empCtcTemplate.getAmount();
					double perDayValue=flatValue/monthlyDayss;
					
//					double presentDaysValue=perDayValue*(monthlyDayss-unpaidDayss);
					double presentDaysValue=perDayValue*eligleDays;
					
					empCtcTemplate.setAmount(Math.round(presentDaysValue));
					/**
					 * Ends
					 */
					validEmpTemplate.add(empCtcTemplate);
				}
				
			}
			if (validEmpTemplate.size() != 0)
				paySlip.setEmployeeCTCTemplate(validEmpTemplate);
		}
		/**
		 * Ends
		 */
		
		/**
		 * @author Anil, Date : 23-02-2019
		 */
		
		if(paySlip.getBonus()!=0||paySlip.getPaidLeaves()!=0){
			paySlip.setPaidLeaveAndBonusAsAdditionalAllowance(paidLeaveAndBonusAsAdditionalAllowance);
			if(paidLeaveAndBonusAsAdditionalAllowance){
				CtcComponent esicComp=getEsicComponentFromDeductionList(paySlip.getDeductionList());
				if(esicComp!=null){
					double washingAllowanceAmt=0;
					for(CtcComponent earn:paySlip.getEarningList()){
						if(earn.getName().equalsIgnoreCase(deductionTypeName)){
							washingAllowanceAmt=earn.getActualAmount();
							break;
						}
					}
					
					double esicGrossEarning=paySlip.getGrossEarningActualWithoutOT()-washingAllowanceAmt;
					if(esicGrossEarning<=esicComp.getConditionValue()){
						double additionalAllowance=paySlip.getBonus()+paySlip.getPaidLeaves();
						double upddatedApplicableGrossForEsicDed=esicGrossEarning+additionalAllowance;
						if(upddatedApplicableGrossForEsicDed<=esicComp.getConditionValue()){
							
							if(paySlip.getBonus()!=0){
								double deductionAmt=getEsicRoundOff1(paySlip.getBonus()*esicComp.getMaxPerOfCTC()/100);
								paySlip.getDeductionList().add(calculateEsicOnAdditionalComp(esicComp,"Bonus",deductionAmt));
								paySlip.setTotalDeduction(paySlip.getTotalDeduction()+deductionAmt);
							}
							if(paySlip.getPaidLeaves()!=0){
								double deductionAmt1=getEsicRoundOff1(paySlip.getPaidLeaves()*esicComp.getMaxPerOfCTC()/100);
								paySlip.getDeductionList().add(calculateEsicOnAdditionalComp(esicComp,"PL",deductionAmt1));
								paySlip.setTotalDeduction(paySlip.getTotalDeduction()+deductionAmt1);
							}
							
							
							paySlip.setNetEarning(paySlip.getGrossEarning()-paySlip.getTotalDeduction());
							paySlip.setNetEarningWithoutOT(paySlip.getGrossEarningActualWithoutOT()-paySlip.getTotalDeduction());
						}
					}
				}
			}
		}
		if(emi!=0) {
			logger.log(Level.SEVERE, "in total deductions emi="+emi);
			CtcComponent comp=new CtcComponent();
			comp.setActualAmount(emi);
			comp.setAmount(emi);								
			comp.setDeduction(true);
			comp.setName("Loan EMI");
			comp.setShortName("EMI");
			paySlip.getDeductionList().add(comp);
			paySlip.setTotalDeduction(paySlip.getTotalDeduction()+emi);
			paySlip.setNetEarning(paySlip.getGrossEarning()-paySlip.getTotalDeduction());
			paySlip.setNetEarningWithoutOT(paySlip.getGrossEarningActualWithoutOT()-paySlip.getTotalDeduction());
		
		}
		
		
		
		
		/**
		 * 
		 */

		return paySlip;

	}

	private static CtcComponent addingBonusInEarning(PaySlip paySlip, CTC ctc, double bonusArrear) {
		CtcComponent comp=new CtcComponent();
		comp.setActualAmount(paySlip.getBonus()-bonusArrear);
//		comp.setArrears(true);
//		comp.set
		comp.setAmount(ctc.getBonusAmt()*12);
		comp.setCompanyId(ctc.getCompanyId());
		if(paySlip.getBonusCorresponadenceName()!=null&&!paySlip.getBonusCorresponadenceName().equals("")){
			comp.setName(paySlip.getBonusCorresponadenceName());
			comp.setShortName(paySlip.getBonusCorresponadenceName());
		}else{
			comp.setName("Bonus");
			comp.setShortName("Bonus");
		}
		paySlip.getEarningList().add(comp);
		
		if(bonusArrear!=0){
			CtcComponent earnArrear=new CtcComponent();
			earnArrear.setName(comp.getName()+" Arrear");
			earnArrear.setShortName(comp.getShortName());
			earnArrear.setActualAmount(Math.round(bonusArrear));
			earnArrear.setArrears(true);
			earnArrear.setArrearOf(comp.getName());
			paySlip.getEarningList().add(earnArrear);
		}
		return comp;
	}



	private static CtcComponent addingPaidLeaveInEarning(PaySlip paySlip, CTC ctc, double paidLeaveArrear) {
		CtcComponent comp=new CtcComponent();
		comp.setActualAmount(paySlip.getPaidLeaves()-paidLeaveArrear);
		comp.setAmount(ctc.getPaidLeaveAmt()*12);
		comp.setCompanyId(ctc.getCompanyId());
		if(paySlip.getPlCorresponadenceName()!=null&&!paySlip.getPlCorresponadenceName().equals("")){
			comp.setName(paySlip.getPlCorresponadenceName());
			comp.setShortName(paySlip.getPlCorresponadenceName());
		}else{
			comp.setName("Paid Leave");
			comp.setShortName("Paid Leave");
		}
		paySlip.getEarningList().add(comp);
		
		if(paidLeaveArrear!=0){
			CtcComponent earnArrear=new CtcComponent();
			earnArrear.setName(comp.getName()+" Arrear");
			earnArrear.setShortName(comp.getShortName());
			earnArrear.setActualAmount(Math.round(paidLeaveArrear));
			earnArrear.setArrears(true);
			earnArrear.setArrearOf(comp.getName());
			paySlip.getEarningList().add(earnArrear);
		}
		
		
		return comp;
	}



	private static double getFormulaOtValue(List<EmployeeOvertime> otList, ArrayList<CtcComponent> earningList, int monthlyDays, double paidDays, double workingDays, PaySlip paySlip) {
		// TODO Auto-generated method stub
		double otAmt=0;
		for(EmployeeOvertime obj:otList){
			double baseAmt=0;
			double amount=0;
			if(obj.getOvertime().isFormulaOtFlat()==true
					||obj.getOvertime().isFormulaOtHourly()==true){
				if(obj.getOvertime().getOtEarningCompList()!=null
						&&obj.getOvertime().getOtEarningCompList().size()!=0){
					
					for(OtEarningComponent comp:obj.getOvertime().getOtEarningCompList()){
						baseAmt=baseAmt+getEarningComponentAmt(comp.getComponentName(),earningList);
					}
					
					if(obj.getOvertime().isFormulaOtHourly()){
						if(obj.getOvertime().getApplicableDays()!=0){
							double applicableDays=obj.getOvertime().getApplicableDays();
							amount=((baseAmt/applicableDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
						}else{
							if(obj.getOvertime().getApplicableDaysStr().equalsIgnoreCase("Monthly Days")){
								amount=((baseAmt/monthlyDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
							}else if(obj.getOvertime().getApplicableDaysStr().equalsIgnoreCase("Paid Days")){
								amount=((baseAmt/paidDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
							}else if(obj.getOvertime().getApplicableDaysStr().equalsIgnoreCase("Working Days")){
								amount=((baseAmt/workingDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
							}
						}
						amount=amount*obj.getOtHrs();
						
					}else{
						if(obj.getOvertime().getApplicableDays()!=0){
							amount=((baseAmt/obj.getOvertime().getApplicableDays())/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
						}else{
							if(obj.getOvertime().getApplicableDaysStr().equalsIgnoreCase("Monthly Days")){
								amount=((baseAmt/monthlyDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
							}else if(obj.getOvertime().getApplicableDaysStr().equalsIgnoreCase("Paid Days")){
								amount=((baseAmt/paidDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
							}else if(obj.getOvertime().getApplicableDaysStr().equalsIgnoreCase("Working Days")){
								amount=((baseAmt/workingDays)/obj.getOvertime().getWorkingHours())*obj.getOvertime().getRate();
							}
						}
					}
					
					/**
					 * @author Anil , Date : 12-06-2019
					 */
//					captureOtBifurcation(obj,Math.round(amount),paySlip);
					captureOtBifurcation(obj,amount,paySlip);
				}
			}
//			otAmt=otAmt+Math.round(amount);
			otAmt=otAmt+amount;
		}
		return Math.round(otAmt);
	}



	private static void captureOtBifurcation(EmployeeOvertime empOt, double otAmt, PaySlip paySlip) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		if(paySlip.getEmpOtHistoryList()!=null&&paySlip.getEmpOtHistoryList().size()!=0){
			boolean otAddedFlag=false;
			for(EmployeeOvertimeHistory otHis:paySlip.getEmpOtHistoryList()){
				if(otHis.getOtId()==empOt.getOvertime().getCount()){
					otAddedFlag=true;
					otHis.setOtHours(otHis.getOtHours()+empOt.getOtHrs());
					otHis.setOtAmount(otHis.getOtAmount()+otAmt);
				}
			}
			if(otAddedFlag==false){
				EmployeeOvertimeHistory obj= new EmployeeOvertimeHistory();
				obj.setOtId(empOt.getOvertime().getCount());
				obj.setOtName(empOt.getOvertime().getName());
				obj.setOtHours(empOt.getOtHrs());
				obj.setOtAmount(otAmt);
				obj.setPayOtAsOther(empOt.getOvertime().isPayOtAsOther());
				obj.setCorrespondanceName(empOt.getOvertime().getOtCorrespondanceName());
				paySlip.getEmpOtHistoryList().add(obj);
			}
			
			
		}else{
			EmployeeOvertimeHistory obj= new EmployeeOvertimeHistory();
			obj.setOtId(empOt.getOvertime().getCount());
			obj.setOtName(empOt.getOvertime().getName());
			obj.setOtHours(empOt.getOtHrs());
			obj.setOtAmount(otAmt);
			obj.setPayOtAsOther(empOt.getOvertime().isPayOtAsOther());
			obj.setCorrespondanceName(empOt.getOvertime().getOtCorrespondanceName());
			paySlip.getEmpOtHistoryList().add(obj);
		}
	
	}

	private static double getEarningComponentAmt(String componentName, ArrayList<CtcComponent> earningList) {
		// TODO Auto-generated method stub
		double amount=0;
		for(CtcComponent comp:earningList){
			if(componentName.equalsIgnoreCase("Gross Earning")){
				amount=amount+comp.getAmount()/12.0;
			}
			if(comp.getName().equals(componentName)){
				return comp.getAmount()/12.0;
			}
		}
		
		return amount;
	}



	public static CtcComponent calculateEsicOnAdditionalComp(CtcComponent esicComp, String compName, double deductionAmt) {
		// TODO Auto-generated method stub
		CtcComponent comp=new CtcComponent();
		comp.setActualAmount(deductionAmt);
		comp.setAmount(deductionAmt);
		comp.setAplicablity(esicComp.isAplicablity());
		comp.setArrearOf(esicComp.getArrearOf());
		comp.setArrears(esicComp.isArrears());
		comp.setCompanyId(esicComp.getCompanyId());
		comp.setCondition(esicComp.getCondition());
		comp.setConditionValue(esicComp.getConditionValue());
		comp.setCount(esicComp.getCount());
		comp.setCreatedBy(esicComp.getCreatedBy());
		comp.setCtcComponentName(esicComp.getCtcComponentName());
		comp.setDeduction(esicComp.isDeduction());
		comp.setGender(esicComp.getGender());
		comp.setIsNonMonthly(esicComp.getIsNonMonthly());
		comp.setIsRecord(esicComp.getIsRecord());
//		comp.setMaxAmount(esicComp.getMaxAmount());
//		comp.setMaxAmount(esicComp.getMaxAmount());
		comp.setMaxPerOfCTC(esicComp.getMaxPerOfCTC());
		comp.setName(compName+" ESIC");
		comp.setShortName(esicComp.getShortName());
		
		return comp;
	}

	public static double getEsicRoundOff1(double amount){
		int esicAmount=0;
		esicAmount=(int) amount;
		
		if(esicAmount<amount){
			esicAmount++;
		}
		return esicAmount;
	}


	public static CtcComponent getEsicComponentFromDeductionList(ArrayList<CtcComponent> deductionList) {
		// TODO Auto-generated method stub
		for(CtcComponent ctc:deductionList){
			if(ctc.getShortName().equalsIgnoreCase("ESIC")){
				return ctc;
			}
		}
		return null;
	}




	private static double getBonus(CTC ctc, String month, int monthlyDayss, double eligleDays) {
		if(ctc.isMonthly()==true){
			if(ctc.isBonus()==true&&ctc.getBonusAmt()!=0){
				double paidBonus=(ctc.getBonusAmt()/monthlyDayss)*eligleDays;
//				return round(paidBonus,2);
				return Math.round(paidBonus);
			}
		}else if(ctc.isAnnually()==true){
			if(ctc.isBonus()==true&&ctc.getBonusAmt()!=0){
//				double paidLeave=(ctc.getPaidLeaveAmt()/monthlyDayss)*eligleDays;
				if(ctc.getMonth().equalsIgnoreCase(month)){
					return Math.round(ctc.getBonusAmt());
				}
			}
		}
		
		return 0;
	}




	private static double getPaidLeave(CTC ctc, String month, int monthlyDayss, double eligleDays) {
//		DecimalFormat df = new DecimalFormat("0.00");
		if(ctc.isMonthly()==true){
			if(ctc.isPaidLeaveApplicable()==true&&ctc.getPaidLeaveAmt()!=0){
				double paidLeave=(ctc.getPaidLeaveAmt()/monthlyDayss)*eligleDays;
//				return Double.parseDouble(df.format(paidLeave));
				return Math.round(paidLeave);
			}
		}else if(ctc.isAnnually()==true){
			if(ctc.isPaidLeaveApplicable()==true&&ctc.getPaidLeaveAmt()!=0){
//				double paidLeave=(ctc.getPaidLeaveAmt()/monthlyDayss)*eligleDays;
				if(ctc.getMonth().equalsIgnoreCase(month)){
//					return Double.parseDouble(df.format(ctc.getPaidLeaveAmt()));
					return Math.round(ctc.getPaidLeaveAmt());
				}
			}
		}
		
		return 0;
	}
	
	private static double getBonusArrear(ArrayList<CtcComponent> bonusAndPaidLeaveArrearsList){
		double bonusArrear=0;
		for(CtcComponent ctc:bonusAndPaidLeaveArrearsList){
			if(ctc.getName().equals("Bonus")){
				bonusArrear+=ctc.getActualAmount();
			}
		}
		return bonusArrear;
	}
	
	private static double getPaidLeaveArrear(ArrayList<CtcComponent> bonusAndPaidLeaveArrearsList){
		double paidLeaveArrear=0;
		for(CtcComponent ctc:bonusAndPaidLeaveArrearsList){
			if(ctc.getName().equals("Paid Leave")){
				paidLeaveArrear+=ctc.getActualAmount();
			}
		}
		return paidLeaveArrear;
	}

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

	@GwtIncompatible
	private void calculateActualEarningComponentList(double overTymAmt,boolean isRotationalWeeklyoffFlagActiveInProject, String salaryPeriod,WeekleyOff weeklyOff,Integer fixedPayrollDays){
		Logger logger=Logger.getLogger("Pay SLIP Logger");
		double actualSum=0;
		int woCount=0;
		if(weeklyOff!=null){
			String[] res=salaryPeriod.split("-",0);
			String salmonth = res[1].trim();
			int m=getMonthNumber(salmonth);			
			int salyear =Integer.parseInt(res[0].trim());
			woCount=calculateEligibleWo(weeklyOff, salyear, m);
		//	logger.log(Level.SEVERE,"wocount="+woCount);
		}
		int woWiseEligibleDays=getMonthlyDays()-woCount;
		logger.log(Level.SEVERE,"MonthlyDays="+getMonthlyDays()+ " woWiseEligibleDays="+woWiseEligibleDays);
		logger.log(Level.SEVERE,"PaidDays()="+getPaidDays());
		for (CtcComponent earnings : getEarningList()) {
			double earning=0;
			if(earnings.getAmount()!=0){
				earning = earnings.getAmount() / 12;
				
				double oneDayEarning;
				
				if(fixedPayrollDays!=null&&fixedPayrollDays>0) {
					oneDayEarning=earning/fixedPayrollDays;	
				}
				else if(isRotationalWeeklyoffFlagActiveInProject){ //Ashwini Patil Date:18-07-2023					
				//	logger.log(Level.SEVERE,"isRotationalWeeklyoffFlagActiveInProject");
					oneDayEarning=earning/woWiseEligibleDays;	
					System.out.println("woWiseEligibleDays="+woWiseEligibleDays);
				}else{
					oneDayEarning=earning/getMonthlyDays();					
				}
			//	logger.log(Level.SEVERE,"oneDayEarning of "+earnings.getCtcComponentName()+"="+oneDayEarning);
								
				
				double actualearning=oneDayEarning*getPaidDays();
				
				System.out.println("getPaidDays="+getPaidDays());
				actualSum=actualSum+Math.round(actualearning);
				/**
				 * Date : 11-07-2018 BY ANIL
				 */
				earnings.setActualAmount(Math.round(actualearning));
				/**
				 * End
				 */
		
			}else if(earnings.getAmount()==0&&earnings.getActualAmount()!=0){
				actualSum=actualSum+earnings.getActualAmount();
			}
			
			
		}
		if(isOtIncludedInGross){
			grossEarning=actualSum;
		}else{
			grossEarning=actualSum+overTymAmt;
		}
		grossEarningWithoutOT=actualSum;
	}
	
	
	public double getCtcComponentAmount(String ctcComponentName){
		double amount=0;
		System.out.println("PASSED COMPONENT "+ctcComponentName);
		if(ctcComponentName.equals("Gross Earning")){
			System.out.println("INSIDE GROSS EARNING ");
			for (CtcComponent earnings : getEarningList()) {
				double earning = earnings.getAmount() / 12;
				amount=amount+earning;
			}
			/**
			 * @author Anil ,Date :15-04-2019
			 */
			if(isOtIncludedInGross){
				/**
				 * @author Anil , Date : 25-06-2019
				 * converting actual overtime amount to monthly amount as for deduction calculation we are considering rate amount
				 */
//				amount=amount+overtimeSalary;
				double amt=(overtimeSalary/this.paidDays)*this.monthlyDays;
				amount=amount+amt;
			}
		}else{
			System.out.println("INSIDE OTHER EARNING ");
			for(int i=0;i<earningList.size();i++){
				if(earningList.get(i).getName().equals(ctcComponentName)){
					amount=earningList.get(i).getAmount()/ 12;
				}
			}
		}
		return amount;
	}
	

	private void calculateNetPayBeforeDeduction() {
		double daySallary = ctcAmount / 365;
		netPayBeforeDeduction = daySallary * eligibleDays;
		setNetPayBeforeDeduction(netPayBeforeDeduction);
	}

//	@OnSave
	@GwtIncompatible
	private double calCulateNetDeduction() {
		double emi=0;
		double sum = 0d;
		for (CtcComponent comp : deductionList) {
			if (comp.getMaxAmount() != null) {
				sum = sum + (comp.getMaxAmount() / 12);
			} else {
				double ctcAmount = getCtcAmount();
				int dedAmount = (int) (ctcAmount * (comp.getMaxPerOfCTC()) / 100);
				dedAmount = dedAmount / 12;

				sum = sum + dedAmount;
			}
		}
		// Emi Deductions
		//Ashwini Patil
		if(getId()==null)
		{
		this.employeeInfoKey=ofy().load().type(EmployeeInfo.class).filter("companyId",getCompanyId()).
				filter("empCount",getEmpid()).keys().first().now();
		}
		payEmi();
		Logger logger = Logger.getLogger("Payslip");
		if(loneEmis!=null) {
			logger.log(Level.SEVERE, "loneEmis size="+loneEmis.size()+" sum="+sum);
			
		}
		for (PaidEmis emis : loneEmis) {
			sum = sum + emis.getEmiAmount();
			emi=emi+emis.getEmiAmount();
			logger.log(Level.SEVERE, "emi amount="+emis.getEmiAmount());
		}

		setNetDeduction(sum);
		logger.log(Level.SEVERE, "returning emi="+emi);
		return emi;
	}

	
	/*********************************** Getter and Setter ******************************************/
	
	
	public double getNetPayBeforeDeduction() {
		return netPayBeforeDeduction;
	}

	public void setNetPayBeforeDeduction(double netPayBeforeDeduction) {

		netPayBeforeDeduction = Math.round(netPayBeforeDeduction);
		this.netPayBeforeDeduction = netPayBeforeDeduction;
	}
	
	public double getNetDeduction() {
		return netDeduction;
	}

	public void setNetDeduction(double netDeduction) {
		netDeduction = Math.round(netDeduction);
		this.netDeduction = netDeduction;
	}
	
	public double getCtcAmount() {
		return this.ctcAmount;
	}

	public String getEsicNumber() {
		return esicNumber;
	}

	public void setEsicNumber(String esicNumber) {
		this.esicNumber = esicNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPfNumber() {
		return pfNumber;
	}

	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscNumber() {
		return ifscNumber;
	}

	public void setIfscNumber(String ifscNumber) {
		this.ifscNumber = ifscNumber;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setEarningList(ArrayList<CtcComponent> earningList1) {
//		this.earningList = earningList1;
		
		for(CtcComponent ctcComp:earningList1){
			if(ctcComp.getAmount()==0&&ctcComp.getActualAmount()==0){
				
			}else{
				this.earningList.add(ctcComp);
			}
		}
		
	}

	public double getMonthlyEarnings() {
		return monthlyEarnings;
	}

	public void setMonthlyEarnings(double monthlyEarnings) {
		this.monthlyEarnings = Math.round(monthlyEarnings);

	}

	public void setCtcAmount(double ctcAmount) {
		this.ctcAmount = ctcAmount;
	}

	public int getMonthlyDays() {
		return monthlyDays;
	}

	public void setMonthlyDays(int monthlyDays) {
		this.monthlyDays = monthlyDays;
	}

	public int getWeekDays() {
		return weekDays;
	}

	public void setWeekDays(int weekDays) {
		this.weekDays = weekDays;
	}

	public int getHoliday() {
		return holiday;
	}

	public void setHoliday(int holiday) {
		this.holiday = holiday;
	}

	public double getUnpaidDay() {
		return unpaidDay;
	}

	public void setUnpaidDay(double unpaidDay) {
		this.unpaidDay = unpaidDay;
	}

	public ArrayList<AvailedLeaves> getAviledLeaves() {
		return aviledLeaves;
	}

	public void setAviledLeaves(ArrayList<AvailedLeaves> aviledLeaves) {
		this.aviledLeaves = aviledLeaves;
	}
	
	public double getGrossEarning() {
		return grossEarning;
	}

	public void setGrossEarning(double grossEarning) {
		this.grossEarning = grossEarning;
	}

	public double getNetEarning() {
		return netEarning;
	}

	public void setNetEarning(double netEarning) {
		this.netEarning = netEarning;
	}

	public double getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(double totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public double getOvertimeDay() {
		return overtimeDay;
	}

	public void setOvertimeDay(double overtimeDay) {
		this.overtimeDay = overtimeDay;
	}

	public double getOvertimeSalary() {
		return overtimeSalary;
	}

	public void setOvertimeSalary(double overtimeSalary) {
		this.overtimeSalary = overtimeSalary;
	}

	public double getEarnedLeave() {
		return earnedLeave;
	}

	public void setEarnedLeave(double earnedLeave) {
		this.earnedLeave = earnedLeave;
	}
	
	public double getOverTimehrs() {
		return overTimehrs;
	}

	public void setOverTimehrs(double overTimehrs) {
		this.overTimehrs = overTimehrs;
	}
	
	
	
	public void updateDeductionList(List<CtcComponent> list){
		ArrayList<CtcComponent> deductionList=new ArrayList<CtcComponent>();
		deductionList.addAll(list);
		this.deductionList=deductionList;
	}

	/***********************************************************************************************/
	
	@GwtIncompatible
	public PaySlipPrintHelper getPaySlipPrintHelper() {
		PaySlipPrintHelper printHelper = new PaySlipPrintHelper();
		printHelper.setPaySlip(this);
		return printHelper;
	}
	
	public double getEsicRoundOff(double amount){
		int esicAmount=0;
		esicAmount=(int) amount;
		
		if(esicAmount<amount){
			esicAmount++;
		}
		return esicAmount;
	}
	public double getTotalCompanyContribution() {
		return totalCompanyContribution;
	}

	public void setTotalCompanyContribution(double totalCompanyContribution) {
		this.totalCompanyContribution = totalCompanyContribution;
	}
	
	/**
	 * @author Anil,Date : 28-03-2019
	 * This method fetch client detail from HrProject and store it in PaySlip
	 * raised by sonu and nitin sir for v allicance
	 */
	@GwtIncompatible
	private void getClientDetailsFromProject(){
		if(projectName!=null&&!projectName.equals("")){
			/**
			 * @author Anil,Date : 10-07-2019
			 * Earlier we were loading client info from HrProject but for now we also wanted to store cncId which is not in HrProject
			 * thats why we are loading now project allocation
			 * This requirement raised by Nitin Sir,Abhinav
			 */
//			HrProject project=ofy().load().type(HrProject.class).filter("companyId", this.companyId).filter("projectName", this.projectName).first().now();
			ProjectAllocation project=ofy().load().type(ProjectAllocation.class).filter("companyId", this.companyId).filter("projectName", this.projectName).first().now();
			if(project!=null){
				if(project.getPersonInfo()!=null){
					setClientInfo(project.getPersonInfo());
				}
				setCncId(project.getContractId()+"");
				/**Date 31-7-2019 
				 * @author Amol 
				 * set Employee Designation on payslip form Project Allocation
				 */
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "EmployeeDesignationFromProject", this.getCompanyId())){
					designationFlag=true;
				}
				if(designationFlag){
					/**
					 * @author Vijay @Since 21-09-2022
					 * Loading employee project allocation from different entity
					 */
					   CommonServiceImpl commonservice = new CommonServiceImpl();
					   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(project);
				    /**
				     * ends here
				     */
//					if(project.getEmployeeProjectAllocationList()!=null&&project.getEmployeeProjectAllocationList().size()!=0){
//						for(EmployeeProjectAllocation obj:project.getEmployeeProjectAllocationList()){

					if(empprojectallocationlist!=null&&empprojectallocationlist.size()!=0){
						for(EmployeeProjectAllocation obj:empprojectallocationlist){
						if(obj.getEmployeeName().equals(this.getEmployeeName())){
							this.setEmployeedDesignation(obj.getEmployeeRole());
						}
						}
					}
					
				}
				
			}
			
		}
	}



	public double getWoDayAsExtraDay() {
		return woDayAsExtraDay;
	}



	public void setWoDayAsExtraDay(double woDayAsExtraDay) {
		this.woDayAsExtraDay = woDayAsExtraDay;
	}



	public int getActualDaysInMonth() {
		return actualDaysInMonth;
	}



	public void setActualDaysInMonth(int actualDaysInMonth) {
		this.actualDaysInMonth = actualDaysInMonth;
	}



	public boolean isFixedDayWisePayroll() {
		return fixedDayWisePayroll;
	}



	public void setFixedDayWisePayroll(boolean fixedDayWisePayroll) {
		this.fixedDayWisePayroll = fixedDayWisePayroll;
	}
	
	
	
	
	public Date getStartDate() {
		return startDate;
	}



	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}



	public Date getEndDate() {
		return endDate;
	}



	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	

	public String getSiteLocation() {
		return siteLocation;
	}



	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	/**
	 * @author Vijay Chougule Date 22-06-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */
	public int getMonthNumber(String month){
		int m=0;
		switch(month){
		case "Jan":
			m=0;
			break;
		case "Feb":
			m=1;
			break;
		case "Mar":
			m=2;
			break;
		case "Apr":
			m=3;
			break;
		case "May":
			m=4;
			break;
		case "Jun":
			m=5;
			break;
		case "Jul":
			m=6;
			break;
		case "Aug":
			m=7;
			break;
		case "Sep":
			m=8;
			break;
		case "Oct":
			m=9;
			break;
		case "Nov":
			m=10;
			break;
		case "Dec":
			m=11;
			break;
		
			
		
		}
		return m;
	}
	@GwtIncompatible
	public int countDayOccurence(int year, int month,int dayToFindCount) {//Calendar.MONDAY
		java.util.Calendar calendar = java.util.Calendar.getInstance();
	    // Note that month is 0-based in calendar, bizarrely.
	    calendar.set(year, month, 1);
	    int daysInMonth = calendar.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);

	    int count = 0;
	    for (int day = 1; day <= daysInMonth; day++) {
	        calendar.set(year, month, day);
	        int dayOfWeek = calendar.get(java.util.Calendar.DAY_OF_WEEK);
	        if (dayOfWeek == dayToFindCount) {
	            count++;
	            // Or do whatever you need to with the result.
	        }
	    }
	    return count;
	}
	
	@GwtIncompatible
	public int calculateEligibleWo(WeekleyOff wo,int year, int month){
		int count=0;
		if(wo.isSUNDAY())
			count+=countDayOccurence(year, month, Calendar.SUNDAY);
		if(wo.isMONDAY())
			count+=countDayOccurence(year, month, Calendar.MONDAY);
		if(wo.isTUESDAY())
			count+=countDayOccurence(year, month, Calendar.TUESDAY);
		if(wo.isWEDNESDAY())
			count+=countDayOccurence(year, month, Calendar.WEDNESDAY);
		if(wo.isTHRUSDAY())
			count+=countDayOccurence(year, month, Calendar.THURSDAY);
		if(wo.isFRIDAY())	
			count+=countDayOccurence(year, month, Calendar.FRIDAY);
		if(wo.isSATAURDAY())
			count+=countDayOccurence(year, month, Calendar.SATURDAY);
		return count;
	}
}