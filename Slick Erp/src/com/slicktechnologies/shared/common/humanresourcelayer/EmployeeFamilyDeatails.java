package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.Date;

import com.google.web.bindery.requestfactory.shared.FanoutReceiver;
import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
@Embed
public class EmployeeFamilyDeatails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8530797679214675055L;


	String familyRelation;
	String fname;
	String mname;
	String lname;
	Long cellNo;
	Date dob;
	
	/**
	 * Date : 19-09-2018 BY ANIL
	 * Added aadhar and pan number for SASHA
	 */
	String aadharNumber;
	String panNumber;
	
	/**
	 * @author Anil,Date : 28-01-2019
	 * added address which will be used for nomination purpose in form 2
	 */
	String address;
	boolean isNonFamilyMember;
	String guardianName;
	String guardianAddress;
	double nomineeShare;
	String nominatedFor;
	
	
	public EmployeeFamilyDeatails() {
		familyRelation="";
		fname="";
		mname="";
		lname="";
		
		
//		aadharNumber="";
//		panNumber="";
//		address="";
//		isNonFamilyMember=false;
//		guardianName="";
//		guardianAddress="";
//		nominatedFor="";
		
	}
	
	

	public String getNominatedFor() {
		return nominatedFor;
	}



	public void setNominatedFor(String nominatedFor) {
		this.nominatedFor = nominatedFor;
	}



	public double getNomineeShare() {
		return nomineeShare;
	}

	public void setNomineeShare(double nomineeShare) {
		this.nomineeShare = nomineeShare;
	}
	public boolean isNonFamilyMember() {
		return isNonFamilyMember;
	}




	public void setNonFamilyMember(boolean isNonFamilyMember) {
		this.isNonFamilyMember = isNonFamilyMember;
	}




	public String getGuardianName() {
		return guardianName;
	}




	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}




	public String getGuardianAddress() {
		return guardianAddress;
	}




	public void setGuardianAddress(String guardianAddress) {
		this.guardianAddress = guardianAddress;
	}




	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getAadharNumber() {
		return aadharNumber;
	}



	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}



	public String getPanNumber() {
		return panNumber;
	}



	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public String getFamilyRelation() {
		return familyRelation;
	}

	public void setFamilyRelation(String familyRelation) {
		this.familyRelation = familyRelation;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Long getCellNo() {
		return cellNo;
	}

	public void setCellNo(Long cellNo) {
		this.cellNo = cellNo;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}



	@Override
	public String toString() {
		String fullName="";
		if(!fname.equals("")){
			fullName=fullName+fname+" ";
		}
		if(!mname.equals("")){
			fullName=fullName+mname+" ";
		}
		if(!lname.equals("")){
			fullName=fullName+lname;
		}
		return fullName;
	}
	
	public String getFullName(){
		String fullName="";
		if(!fname.equals("")){
			fullName=fullName+fname+" ";
		}
		if(!mname.equals("")){
			fullName=fullName+mname+" ";
		}
		if(!lname.equals("")){
			fullName=fullName+lname;
		}
		return fullName;
	}
	
	public void setFullName(String fName){
		fname=fName;
		mname="";
		lname="";
	}
	
	public EmployeeFamilyDeatails getFamilyDetails(EmployeeFamilyDeatails obj){
		EmployeeFamilyDeatails fam=new EmployeeFamilyDeatails();
		fam.setCount(obj.getCount());
		fam.setCompanyId(obj.getCompanyId());
		fam.setAadharNumber(obj.getAadharNumber());
		fam.setAddress(obj.getAddress());
		fam.setCellNo(obj.getCellNo());
		fam.setCreatedBy(obj.getCreatedBy());
		fam.setDob(obj.getDob());
		fam.setFamilyRelation(obj.getFamilyRelation());
		fam.setFname(obj.getFname());
		fam.setMname(obj.getMname());
		fam.setLname(obj.getLname());
		fam.setGuardianAddress(obj.getGuardianAddress());
		fam.setGuardianName(obj.getGuardianName());
		fam.setId(obj.getId());
		fam.setNominatedFor(obj.getNominatedFor());
		fam.setNomineeShare(obj.getNomineeShare());
		fam.setNonFamilyMember(obj.isNonFamilyMember());
		fam.setPanNumber(obj.getPanNumber());
		return fam;
	}
	
	
}
