package com.slicktechnologies.shared.common.humanresourcelayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HasCalendar;

// TODO: Auto-generated Javadoc
/**
 * Repersent a Country in organization Hirarchy.
 */
@Entity
@Cache
public class Country extends SuperModel implements HasCalendar,Serializable{

	/** The Constant serialVersionUID. */
	
	
	private static final long serialVersionUID = -9095413948956499512L;
	
	
	/** The country name. */
	@Index
	protected String countryName;
	
	/** The currency of country */
	protected String currency;
	
	/** The language of country */
	protected String language;
	
	/**  */
	protected String calendarName;
	

	protected Calendar calendar;

	protected String countryCode;
	
	public Country() {
		super();
		countryName="";
		currency="";
		language="";
		calendarName="";
		countryCode ="";
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel o) {
		if(o instanceof Country)
		{
			Country country=(Country) o;
			if(country.getCountryName()!=null)
			{
				return this.countryName.compareTo(country.countryName);
			}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		
		Country entity = (Country) m;
		String name = entity.getCountryName().trim();
		String curname=this.countryName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	
	}
	
	@Override
	public String toString()
	{
		return countryName.toString();
	}


	
	
	
	
	
	public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		
		this.calendar = calendar;
	}

	/***************************************Getters and Setters****************************************************/
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Country Name")
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		if(countryName!=null){
		this.countryName = countryName.trim();
	}
	}
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Currency")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		if(currency!=null){
		this.currency = currency.trim();
	}
	}
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Language")
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		if(language!=null){
		this.language = language.trim();
	}
	}
	

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Calendar Name")
	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		if(calendarName!=null)
		this.calendarName = calendarName;
	}

	public static void makeOjbectListBoxLive(ObjectListBox<Country> olbCountry) {
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Country());
		olbCountry.MakeLive(querry);
		
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	
	
	
}
