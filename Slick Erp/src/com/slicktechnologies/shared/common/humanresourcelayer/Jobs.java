package com.slicktechnologies.shared.common.humanresourcelayer;

// TODO: Auto-generated Javadoc
/**
 * The Class Jobs.
 */
public class Jobs extends HRProcess{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4641317915444877123L;
	
	/** The job title. */
	protected String jobTitle;
	
	/** same job title may have different salary */
	/*  minimum salary of related to job title  */
	protected double minSalary;
	
	/*  maximum salary of related to job title  */
	protected double maxSalary;
	
	/** The grade of employee */
	protected String gradeName;
	
	
	public Jobs() {
		super();
		jobTitle="";
		gradeName="";
	}

	/***************************************Getters and Setters****************************************************/
	
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		if(jobTitle!=null)
		this.jobTitle = jobTitle.trim();
	}

	public Double getMinSalary() {
		return minSalary;
	}

	public void setMinSalary(Double minSalary) {
		if(minSalary!=null)
		this.minSalary = minSalary;
	}

	public Double getMaxSalary() {
		return maxSalary;
	}

	public void setMaxSalary(Double maxSalary) {
		if(maxSalary!=null)
		this.maxSalary = maxSalary;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		if(gradeName!=null)
		this.gradeName = gradeName.trim();
	}
	
	
	
	

}
