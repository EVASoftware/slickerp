package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;
@Entity
public class ProvidentFund extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6022696100024642050L;
	@Index
	String pfName;
	@Index
	String pfShortName;
	@Index
	boolean status;
	double employeeContribution;
	double employerContribution;
	ArrayList<OtEarningComponent> compList=new ArrayList<OtEarningComponent>();
	double edli;
	double empoyerPF;
	
	@Index
	Date effectiveFromDate;
	
	
	public ProvidentFund() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	
	
	

	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}





	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}





	public String getPfName() {
		return pfName;
	}



	public void setPfName(String pfName) {
		this.pfName = pfName;
	}



	public String getPfShortName() {
		return pfShortName;
	}



	public void setPfShortName(String pfShortName) {
		this.pfShortName = pfShortName;
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	public double getEmployeeContribution() {
		return employeeContribution;
	}



	public void setEmployeeContribution(double employeeContribution) {
		this.employeeContribution = employeeContribution;
	}



	public double getEmployerContribution() {
		return employerContribution;
	}



	public void setEmployerContribution(double employerContribution) {
		this.employerContribution = employerContribution;
	}



	public ArrayList<OtEarningComponent> getCompList() {
		return compList;
	}



	public void setCompList(List<OtEarningComponent> compList) {
		ArrayList<OtEarningComponent> list=new ArrayList<OtEarningComponent>();
		list.addAll(compList);
		this.compList = list;
	}



	public double getEdli() {
		return edli;
	}



	public void setEdli(double edli) {
		this.edli = edli;
	}



	public double getEmpoyerPF() {
		return empoyerPF;
	}



	public void setEmpoyerPF(double empoyerPF) {
		this.empoyerPF = empoyerPF;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
