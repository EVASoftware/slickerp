package com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

import static com.googlecode.objectify.ObjectifyService.ofy;
// TODO: Auto-generated Javadoc

/**
 * Process Class which is Used to Assign Shifts to 
 * Employees.
 */
@Entity
public class AssignedShift extends SuperModel {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7570454421905106554L;
	
	/** Name of Shift Assignment Process */
	@Index
	public String name;
	
	/** The creation date of Shift Assignment Process */
	public Date creationDate;
	
	/** Date from which Shift is Assigned */
	@Index
	public Date toDate;
	
	/** Date up to where Shift is Assigned */
	@Index
	public Date fromDate;
	
	/** Employee Information Key List Used to store keys of Employees
	 * to Which Shift is Assigned*/
//	@Index
	public ArrayList<Key<EmployeeInfo>>employees;
	
	/** Employees corresponding to Key List value will not get
	 * saved in database */
	@Ignore
	public ArrayList<EmployeeInfo>employeeInfo;
	
	/** The day of pattern , the date from which Shift Assignment can be started */
	Integer    dayOfPattern;
	
	/** The Pattern which needs to be Assigned */
	
	public ShiftPattern pattern;
	
	/** Tells weather to Override Employees on Leave Or Not */
	public boolean overrideEmployeeOnLeave;
	
	/** Tells weather to Override Company Holidays or not */
	public boolean overrideCompanyHoliday;
	
	
	/**
	 * Instantiates a new assigned shift.
	 */
	public AssignedShift() {
		super();
		name="";
		creationDate=new Date();
		employees = new ArrayList<Key<EmployeeInfo>>();
		pattern=new ShiftPattern();
		
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.DATECOMPARATOR, flexFormNumber = "00", title = "Creation Date", variableName = "dateComparator",extra="AssignedShift")
	@TableAnnotation(ColType = FormTypes.DATETEXTCELL, colNo = 1, isFieldUpdater = false, isSortable = false, title = "Creation Date")
  public Date getCreationDate() {
		return creationDate;
	}

/**
 * Sets the creation date.
 *
 * @param creationDate the new creation date
 */
public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

  /**
   * Gets the pattern.
   *
   * @return the pattern
   */
  public ShiftPattern getPattern() {
	return pattern;
}

/**
 * Sets the pattern.
 *
 * @param pattern the new pattern
 */
public void setPattern(ShiftPattern pattern) {
	this.pattern = pattern;
}

/**
 * Gets the to date.
 *
 * @return the to date
 */
public Date getToDate() {
		return toDate;
	}

/**
 * Sets the to date.
 *
 * @param toDate the new to date
 */
public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


/**
 * Gets the from date.
 *
 * @return the from date
 */
public Date getFromDate() {
		return fromDate;
	}

/**
 * Sets the from date.
 *
 * @param fromDate the new from date
 */
public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

/**
 * Gets the employees.
 *
 * @return the employees
 */
public ArrayList<Key<EmployeeInfo>> getEmployees() {
		return employees;
	}

/**
 * Sets the employees.
 *
 * @param employees the new employees
 */
public void setEmployees(ArrayList<Key<EmployeeInfo>> employees) {
		this.employees = employees;
	}

/**
 * Gets the name.
 *
 * @return the name
 */
public String getName() {
		return name;
	}

/**
 * Sets the name.
 *
 * @param name the new name
 */
public void setName(String name) {
		if(name!=null)
		   this.name = name.trim();
	}


  /**
   * Gets the day of pattern.
   *
   * @return the day of pattern
   */
  public Integer getDayOfPattern() {
	return dayOfPattern;
}

/**
 * Sets the day of pattern.
 *
 * @param dayOfPattern the new day of pattern
 */
public void setDayOfPattern(Integer dayOfPattern) {
	this.dayOfPattern = dayOfPattern;
}

/**
 * Gets the pattern name.
 *
 * @return the pattern name
 */
public String getPatternName() {
	return pattern.getPatternName();
}

/**
 * Gets the patt short name.
 *
 * @return the patt short name
 */
public String getPattShortName() {
	return pattern.getPattShortName();
}

/**
 * Gets the pattern category.
 *
 * @return the pattern category
 */
public String getPatternCategory() {
	return pattern.getPatternCategory();
}

/**
 * Gets the shift.
 *
 * @return the shift
 */
public ArrayList<Shift> getShift() {
	return pattern.getShift();
}

/**
 * Sets the pattern name.
 *
 * @param patternName the new pattern name
 */
public void setPatternName(String patternName) {
	pattern.setPatternName(patternName);
}

/**
 * Sets the patt short name.
 *
 * @param pattShortName the new patt short name
 */
public void setPattShortName(String pattShortName) {
	pattern.setPattShortName(pattShortName);
}

/**
 * Sets the pattern category.
 *
 * @param patternCategory the new pattern category
 */
public void setPatternCategory(String patternCategory) {
	pattern.setPatternCategory(patternCategory);
}

/**
 * Sets the shift.
 *
 * @param shift the new shift
 */
public void setShift(List<Shift> shift) {
	pattern.setShift(shift);
}



/**
 * Checks if is override employee on leave.
 *
 * @return true, if is override employee on leave
 */
public boolean isOverrideEmployeeOnLeave() {
	return overrideEmployeeOnLeave;
}

/**
 * Sets the override employee on leave.
 *
 * @param overrideEmployeeOnLeave the new override employee on leave
 */
public void setOverrideEmployeeOnLeave(boolean overrideEmployeeOnLeave) {
	this.overrideEmployeeOnLeave = overrideEmployeeOnLeave;
}

/**
 * Checks if is override company holiday.
 *
 * @return true, if is override company holiday
 */
public boolean isOverrideCompanyHoliday() {
	return overrideCompanyHoliday;
}

/**
 * Sets the override company holiday.
 *
 * @param overrideCompanyHoliday the new override company holiday
 */
public void setOverrideCompanyHoliday(boolean overrideCompanyHoliday) {
	this.overrideCompanyHoliday = overrideCompanyHoliday;
}

/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
 */
@Override
	public boolean isDuplicate(SuperModel m) {
		
		AssignedShift entity = (AssignedShift) m;
		String name = entity.getName().trim();
		String curname=this.name.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	
	}
  
 
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
public String toString() {
	// TODO Auto-generated method stub
	return name.toString();
}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	/**
	 * Gets the employee info.
	 *
	 * @return the employee info
	 */
	public ArrayList<EmployeeInfo> getEmployeeInfo() {
		return employeeInfo;
	}

	/**
	 * Sets the employee info.
	 *
	 * @param employeeInfo the new employee info
	 */
	public void setEmployeeInfo(ArrayList<EmployeeInfo> employeeInfo) {
		this.employeeInfo = employeeInfo;
	}
	
	/**
	 * On save.
	 */
	@GwtIncompatible
	@OnSave
	public void onSave()
	{
		if(employeeInfo!=null)
		{
			employees=new ArrayList<Key<EmployeeInfo>>();
			for(EmployeeInfo info:employeeInfo)
			{
				if(info!=null)
				{
					Key<EmployeeInfo>empInfo=Key.create(info);
					employees.add(empInfo);
				}
			}
		}
	}
	
	/**
	 * On load.
	 */
	@GwtIncompatible
	@OnLoad
	public void onLoad()
	{
		this.employeeInfo=new ArrayList<EmployeeInfo>();
		for(Key<EmployeeInfo>key:this.employees)
		{
			if(key!=null)
			{
				EmployeeInfo info=ofy().load().key(key).now();
				employeeInfo.add(info);
				
			}
			
		}
	}
	
	
	
	

}
