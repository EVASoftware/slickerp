package com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.editor.client.Editor.Ignore;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.project.concproject.EmpolyeeTable;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;

// TODO: Auto-generated Javadoc
/**
 * Actual Shift Employee Shift Entity.
 * It Stores the Employee Shift Information.
 */
@Entity
public class EmployeeShift extends SuperModel implements Serializable
{
	
	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 7522974686402660251L;
	
	/** The employee. */
	@Ignore
	protected EmployeeInfo employee;
	
	/** The date shift. */
	@Ignore
	protected DateShift dateShift;
	
	/** The shift. */
	protected Shift shift;
	

	@Index
	/** The emp info key. */
	protected  Key<EmployeeInfo>empInfoKey;
	
	@Index
	protected Key<DateShift> dateShiftKey;
	
	/**
	 * Instantiates a new employee shift.
	 */
	public EmployeeShift()
	{
		
	}
	
	/**
	 * Methods creates Employee Shift List from Pattern.
	 *
	 * @param pattern the pattern Assigned Shift Object Corresponding to Which Shift is being created.
	 * @return the all employee shift from that pattern
	 */
	@GwtIncompatible
	public static ArrayList<EmployeeShift> getAllEmployeeShiftFromPattern(AssignedShift pattern)
	{	
		ArrayList<EmployeeShift> empShiftsList=new ArrayList<EmployeeShift>();
		//List of Employee Info Corresponding to Which Shift is being 
		//Created.
		ArrayList<EmployeeInfo> employeeList=pattern.getEmployeeInfo();
		//Array List of Employee Shifts
		
		//Array List of Shifts  Going to be assigned
		ArrayList<Shift> shiftList=pattern.getPattern().shift;
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(pattern.getFromDate());
		
		Date startDate=cal.getTime();
		Date endDate=pattern.getToDate();
		
		//Gets The Date Shift Object Corresponding to 
		//Employee Shift.
		HashMap<Date,Key<DateShift>> dateShiftHashmap=DateShift.getDateshiftKey(startDate, endDate,pattern.getCompanyId());
		boolean isHolidayDate = false;
		int shiftListSize=shiftList.size();
		
		for(EmployeeInfo employee: employeeList)
		{
			int i=0;
			com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar empCal =employee.getLeaveCalendar();
			//System.out.println("Employee "+employee.getFullName()+" id "+employee.getEmpCount());
			
			cal.setTime(pattern.getFromDate());
			startDate=cal.getTime();
			
			
			while(startDate.before(endDate))
			{
				EmployeeShift empShift=new EmployeeShift();
				empShift.employee=employee;
				//System.out.println("Date "+startDate);
				
				//Check weather Company Holiday
				 empShift.dateShiftKey=dateShiftHashmap.get(startDate);
				 boolean isEmployeeOnLeave=LeaveApplication.
							isLeaveDate(employee.getEmpCount(), employee.getCompanyId(),startDate);
				 //System.out.println("IS EMPLOYEE ON LEAVE      ---- "+isEmployeeOnLeave);
				if(empCal!=null)
					isHolidayDate=empCal.isHolidayDate(startDate);
				
					
					
					if(isHolidayDate==true&&pattern.overrideCompanyHoliday==false)
					{
						empShift.shift=Shift.getCOMPANYHOLIDAY();
						//System.out.println("Date in Company Holiday "+startDate);
						 empShiftsList.add(empShift);
						
					}
					
				
					else if(isEmployeeOnLeave==true&&pattern.overrideEmployeeOnLeave==false)
					{
						//Insert Employee On Leave Default Shift
						empShift.shift=Shift.getEMPLOYEEONLEAVE();
						//System.out.println("Date in Employee On Leave "+startDate);
						 empShiftsList.add(empShift);
					}
					
				else
				{
					
					
					int allShift=i%shiftListSize;
					
					//Before Assigining Validate for Overlapping if Overlapping do not Assign
					boolean flag=EmployeeShift.checkForOverlappingShiftPeriod(employee, shiftList.get(allShift));
					System.out.println("Value of Flag "+flag);
					if(flag==true)
					{
						empShift.shift=shiftList.get(allShift);
						 empShiftsList.add(empShift);
					}
					
				}
				empShift.setCompanyId(pattern.getCompanyId());
		       
		       
		        cal.setTime(startDate);
		        cal.add(Calendar.DATE, 1);
		        startDate=cal.getTime();
		        i++;
			}
			
			
		}
		for(EmployeeShift sh: empShiftsList)
		{
			System.out.println("Employee Name     "+sh.getEmployee().getFullName());
			System.out.println("Shift Name   --- "+sh.getShift().getShortName());
			DateShift dateShift=ofy().load().key(sh.dateShiftKey).now();
			
			System.out.println("Date   "+dateShift.getDate());
			
			System.out.println("From Time   "+sh.getShift().getFromTime());
			System.out.println("To Time     "+sh.getShift().getToTime());
			
			
			
			
		}
		
		return   empShiftsList;
	}
	
	
	@GwtIncompatible
	private static boolean checkForOverlappingShiftPeriod(EmployeeInfo temp,Shift shift)
	{
		Key<EmployeeInfo>empInfoKey=Key.create(temp);
		List<EmployeeShift>list=ofy().load().type(EmployeeShift.class).filter("empInfoKey",empInfoKey).list();
		for(EmployeeShift te:list)
		{
			if(te.shift.isOverlappingPeriod(shift))
				 return false;
		}
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#getCount()
	 */
	public int getEmployeeId() {
		return employee.getCount();
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public EmployeeInfo getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(EmployeeInfo employee) {
		this.employee = employee;
	}

	/**
	 * Gets the date shift.
	 *
	 * @return the date shift
	 */
	public DateShift getDateShift() {
		return dateShift;
	}

	/**
	 * Sets the date shift.
	 *
	 * @param dateShift the new date shift
	 */
	public void setDateShift(DateShift dateShift) {
		this.dateShift = dateShift;
	}

	/**
	 * Gets the shift.
	 *
	 * @return the shift
	 */
	public Shift getShift() {
		return shift;
	}

	/**
	 * Sets the shift.
	 *
	 * @param shift the new shift
	 */
	public void setShift(Shift shift) {
		this.shift = shift;
	}

	/**
	 * Gets the emp info key.
	 *
	 * @return the emp info key
	 */
	public Key<EmployeeInfo> getEmpInfoKey() {
		return empInfoKey;
	}

	/**
	 * Sets the emp info key.
	 *
	 * @param empInfoKey the new emp info key
	 */
	public void setEmpInfoKey(Key<EmployeeInfo> empInfoKey) {
		this.empInfoKey = empInfoKey;
	}

	/**
	 * Gets the date shift key.
	 *
	 * @return the date shift key
	 */
	public Key<DateShift> getDateShiftKey() {
		return dateShiftKey;
	}

	/**
	 * Sets the date shift key.
	 *
	 * @param dateShiftKey the new date shift key
	 */
	public void setDateShiftKey(Key<DateShift> dateShiftKey) {
		this.dateShiftKey = dateShiftKey;
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	public String getBranch() {
		return employee.getBranch();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#getCompanyId()
	 */
	public Long getCompanyId() {
		return employee.getCompanyId();
	}

	/**
	 * Gets the cell number.
	 *
	 * @return the cell number
	 */
	public Long getCellNumber() {
		return employee.getCellNumber();
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return employee.getFullName();
	}

	/**
	 * Gets the emp count.
	 *
	 * @return the emp count
	 */
	public int getEmpCount() {
		return employee.getEmpCount();
	}

	/**
	 * Gets the designation.
	 *
	 * @return the designation
	 */
	public String getDesignation() {
		return employee.getDesignation();
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return employee.getDepartment();
	}

	/**
	 * Gets the employeerole.
	 *
	 * @return the employeerole
	 */
	public String getEmployeerole() {
		return employee.getEmployeerole();
	}

	/**
	 * Gets the employee type.
	 *
	 * @return the employee type
	 */
	public String getEmployeeType() {
		return employee.getEmployeeType();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * On save.
	 */
	@GwtIncompatible
	@OnSave
	public void onSave()
	{
		if(employee!=null)
		{
				empInfoKey=Key.create(employee);
		}
		if(dateShift!=null)
			dateShiftKey=Key.create(dateShift);
	}
	
	/**
	 * On load.
	 */
	@GwtIncompatible
	@OnLoad
	public void onLoad()
	{
			if(empInfoKey!=null)
				employee=ofy().load().key(empInfoKey).now();
			if(dateShiftKey!=null)
				dateShift=ofy().load().key(dateShiftKey).now();	
	}
  
}
