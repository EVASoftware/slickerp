package com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;

// TODO: Auto-generated Javadoc
/**
 * Entity for Storing the dates corresponding to which a Shift is assigned.
 * For each DateShift there exists multiple {@link EmployeeShift}
 */
@Entity
@Embed
@Index
public class DateShift extends SuperModel implements  Serializable
	{
		
		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = -9192220422724257683L;
		
		/** The date. */
		Date date;
		
		/** The day of week. */
		Integer dayOfWeek;
		
		/** The day of month. */
		Integer dayOfMonth;
		
		/** The year. */
		Integer year;
		
		/** The day of year. */
		Integer dayOfYear;
		
		/** The month of year. */
		Integer monthOfYear;
		
		
		/**
		 * Instantiates a new date shift.
		 */
		public DateShift() {
			super();
		}
		
		
		/**
		 * Instantiates a new date shift.
		 *
		 * @param d the d
		 */
		@GwtIncompatible
		public DateShift(Date d) {
			super();
			Calendar cal=Calendar.getInstance();
			  cal.setTime(d);
			  dayOfMonth=cal.get(Calendar.DAY_OF_MONTH);
			  monthOfYear=cal.get(Calendar.MONTH);
			  dayOfWeek=cal.get(Calendar.DAY_OF_WEEK);
			  dayOfYear=cal.get(Calendar.DAY_OF_YEAR);
			  year=cal.get(Calendar.YEAR);
			  date=DateUtility.getDateWithTimeZone("IST",d);
		}

		
		/**
		 * Compare to.
		 *
		 * @param o the o
		 * @return the int
		 */
		public int compareTo(DateShift o) {
			if(o.date!=null&&this.date!=null)
				return date.compareTo(o.date);
			return 0;
		}

		/**
		 * Gets the date.
		 *
		 * @return the date
		 */
		public Date getDate() {
			return date;
		}

		/**
		 * Sets the date.
		 *
		 * @param date the new date
		 */
		public void setDate(Date date) {
			this.date = date;
		}

		
		/**
		 * Gets the day of week.
		 *
		 * @return the day of week
		 */
		public Integer getDayOfWeek() {
			return dayOfWeek;
		}

		/**
		 * Sets the day of week.
		 *
		 * @param dayOfWeek the new day of week
		 */
		public void setDayOfWeek(Integer dayOfWeek) {
			this.dayOfWeek = dayOfWeek;
		}

		/**
		 * Gets the day of month.
		 *
		 * @return the day of month
		 */
		public Integer getDayOfMonth() {
			return dayOfMonth;
		}

		/**
		 * Sets the day of month.
		 *
		 * @param dayOfMonth the new day of month
		 */
		public void setDayOfMonth(Integer dayOfMonth) {
			this.dayOfMonth = dayOfMonth;
		}

		/**
		 * Gets the day of year.
		 *
		 * @return the day of year
		 */
		public Integer getDayOfYear() {
			return dayOfYear;
		}

		/**
		 * Sets the day of year.
		 *
		 * @param dayOfYear the new day of year
		 */
		public void setDayOfYear(Integer dayOfYear) {
			this.dayOfYear = dayOfYear;
		}

		/**
		 * Gets the year.
		 *
		 * @return the year
		 */
		public Integer getYear() {
			return year;
		}

		/**
		 * Sets the year.
		 *
		 * @param year the new year
		 */
		public void setYear(Integer year) {
			this.year = year;
		}

		/**
		 * Gets the month of year.
		 *
		 * @return the month of year
		 */
		public Integer getMonthOfYear() {
			return monthOfYear;
		}

		/**
		 * Sets the month of year.
		 *
		 * @param monthOfYear the new month of year
		 */
		public void setMonthOfYear(Integer monthOfYear) {
			this.monthOfYear = monthOfYear;
		}
		
		/**
		 * Creates date shift objects if date shift does not exists in passed date range.
		 * else it simply returns the key of existing date shift.
		 *
		 * @param fromDate the from date the date from which Shift Generation Starts
		 * @param toDate the to date the date from which Shift Generation Ends
		 * @param companyId the company id Id of Company to Which Shift is Being Genarted.
		 * @return the dateshift dateShiftHashMap Hash map containg the Date and key of Date Shift
		 * corresponding to that Date.
		 */
		@GwtIncompatible
		public static HashMap<Date,Key<DateShift>> getDateshiftKey(Date fromDate,Date toDate,Long companyId)
		{
			HashMap<Date, Key<DateShift>> dateShiftHashMap=new HashMap<Date, Key<DateShift>>();
			Calendar cal=Calendar.getInstance();
			while(fromDate.before(toDate))
			{
				Key<DateShift> dateShiftKey=ofy().load().type(DateShift.class).filter("date",fromDate).keys().first().now();
				if(dateShiftKey==null)
				{
					//Save the Date Shift and fill hashMap
					DateShift ds=new DateShift(fromDate);
					
					dateShiftKey=ofy().save().entity(ds).now();
					
				}
				
				dateShiftHashMap.put(fromDate, dateShiftKey);
				
				cal.setTime(fromDate);
				
				cal.add(Calendar.DATE,1);
				fromDate=cal.getTime();
				

			}
			
			
			return dateShiftHashMap ;
			
		}

		/*@GwtIncompatible
		public static ArrayList<Key<DateShift>> getDateshiftKey(Date fromDate,Date toDate,Long companyId)
		{
			//HashMap<Date, Key<DateShift>> dateShiftHashMap=new HashMap<Date, Key<DateShift>>();
			ArrayList<Key<DateShift>> keyShiftArray=new ArrayList<Key<DateShift>>();
			Calendar cal=Calendar.getInstance();
			while(fromDate.before(toDate))
			{
				Key<DateShift> dateShiftKey=ofy().load().type(DateShift.class).filter("date",fromDate).keys().first().now();
				if(dateShiftKey==null)
				{
					//Save the Date Shift and fill hashMap
					DateShift ds=new DateShift(fromDate);
					
					dateShiftKey=ofy().save().entity(ds).now();
					
				}
				
			//	dateShiftHashMap.put(fromDate, dateShiftKey);
				keyShiftArray.add(dateShiftKey);
				cal.setTime(fromDate);
				
				cal.add(Calendar.DATE,1);
				fromDate=cal.getTime();
				

			}
			
			
			return keyShiftArray ;
			
		}*/


		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
	

		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
		 */
		@Override
		public boolean isDuplicate(SuperModel m) {
			// TODO Auto-generated method stub
			return false;
		}
		
		
	}