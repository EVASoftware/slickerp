package com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
// TODO: Auto-generated Javadoc
/**
 * The Class represents employee shifts.
 */

@Entity
@Embed
public class Shift extends SuperModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2279930808468819772L;
	
	/**  The category of shift. */
	protected String category;
	
	/**  The shift description. */
	protected String shiftDesc;
	
	/**  The shift start time. */
	protected double fromTime;
	
	/** shift end time. */
	protected double toTime;
	
	/** The shift name. */
	@Index
	protected String shiftName;
	
	/** shift short name. */
	protected String shortName;

	/**Weekly off **/
	protected boolean weeklyoff;
	
	/** date 11.8.2018 added by komal  for intime and outtime grace period (it should be in minutes)**/
	@Index
	protected double intimeGracePeriod;
	@Index
	protected double outtimeGracePeriod;
    /**
     * Instantiates a new shift.
     * 
     * 
     */
	
	private static Shift EMPLOYEEONLEAVE,COMPANYHOLIDAY;
	/** date 27.8.2018 added by komal for shift status**/
	@Index
	boolean status ;

	
    public Shift() {
    	super();
    	shiftName="";
    	shortName="";
    	shiftDesc="";
    	category="";
    	/** date 27.8.2018 added by komal for shift status**/
    	status = false;

	}

	
	
	/**
	 * *************************************Getters and Setters***************************************************.
	 *
	 * @return the category
	 */

	
	
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 * 
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Shift Category")
	public void setCategory(String category) {
		if(category!=null)
		this.category = category.trim();
	}

	/**
	 * Gets the shift desc.
	 *
	 * @return the shift desc
	 */
	public String getShiftDesc() {
		return shiftDesc;
	}

	/**
	 * Sets the shift desc.
	 *
	 * @param shiftDesc the new shift desc
	 */
	public void setShiftDesc(String shiftDesc) {
		if(shiftDesc!=null)
		this.shiftDesc = shiftDesc.trim();
	}

	
	/**
	 * Gets the from time.
	 *
	 * @return the from time
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "In Time(24hr)")
	public Double getFromTime() {
		return fromTime;
	}

	/**
	 * Sets the from time.
	 *
	 * @param fromTime the new from time
	 */
	public void setFromTime(Double fromTime) {
		if(fromTime!=null)
		this.fromTime = fromTime;
	}

	/**
	 * Gets the to time.
	 *
	 * @return the to time
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Out Time(24hr)")
	public Double getToTime() {
		return toTime;
	}

	/**
	 * Sets the to time.
	 *
	 * @param toTime the new to time
	 */
	public void setToTime(Double toTime) {
		if(toTime!=null)
		this.toTime = toTime;
	}
	
	/**
	 * Gets the shift name.
	 *
	 * @return the shift name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Shift")
	public String getShiftName() {
		return shiftName;
	}

	/**
	 * Sets the shift name.
	 *
	 * @param shiftName the new shift name
	 */
	public void setShiftName(String shiftName) {
		
		if(shiftName!=null)
     		this.shiftName = shiftName.trim();
	}
	
	

	/**
	 * Gets the short name.
	 *
	 * @return the short name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Short Name")
	public String getShortName() {
		return shortName;
	}

	/**
	 * Sets the short name.
	 *
	 * @param shortName the new short name
	 */
	public void setShortName(String shortName) {
		if(shortName!=null)
		this.shortName = shortName.trim();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel o) {
		if(o instanceof Shift)
		{
			Shift shift = (Shift) o;
			if(shift.getShiftName()!=null)
			{
				return this.shiftName.compareTo(shift.shiftName);
			}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {

		return isDuplicateName(m) || isDuplicateShortName(m)||isDuplicateTimePeriod(m);
	
	}
	
	
	/**
	 * Checks if is duplicate name.
	 *
	 * @param m the m
	 * @return true, if is duplicate name
	 */
	public boolean isDuplicateName(SuperModel m) {

		Shift entity = (Shift) m;
		String name = entity.getShiftName().trim();
		String curname=this.shiftName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	/**
	 * Checks if is duplicate short name.
	 *
	 * @param m the m
	 * @return true, if is duplicate short name
	 */
	public boolean isDuplicateShortName(SuperModel m) {

		Shift entity = (Shift) m;
		String name = entity.getShortName().trim();
		String curname=this.shortName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	
	public boolean isDuplicateTimePeriod(SuperModel m) {

		Shift entity = (Shift) m;
		double startTime = entity.getFromTime();
		double endTime=entity.getToTime();
		
		double curstartTime = getFromTime();
		double curendTime=getToTime();
		
		
		//New Object is being added
		if(id==null)
		{
			if((startTime==curstartTime)&&(endTime==curendTime))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((startTime==curstartTime)&&(endTime==curendTime))
			  return true;
			else
				return false;
				
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return shiftName.toString();
	}



	public void setWeeklyoff(boolean b) {
		weeklyoff=b;
		
	}
	
	public Boolean getWeeklyoff()
	{
		return weeklyoff;
	}



	public void setFromTime(double fromTime) {
		this.fromTime = fromTime;
	}



	public void setToTime(double toTime) {
		this.toTime = toTime;
	}
	
 public boolean isOverlappingPeriod(Shift checkForOverlappingShift)
 {
	if(this.toTime<checkForOverlappingShift.fromTime)
		return false;
	if(this.fromTime>checkForOverlappingShift.toTime)
		return false;
	
	return true;
 }



public static Shift getEMPLOYEEONLEAVE() {
	if(EMPLOYEEONLEAVE==null)
	{
       EMPLOYEEONLEAVE=new Shift();
       EMPLOYEEONLEAVE.shiftName="LEAVE";
       EMPLOYEEONLEAVE.shortName="EL";
	}
	return EMPLOYEEONLEAVE;
}



public static void setEMPLOYEEONLEAVE(Shift eMPLOYEEONLEAVE) {
	EMPLOYEEONLEAVE = eMPLOYEEONLEAVE;
}



public static Shift getCOMPANYHOLIDAY() 
{
	if(COMPANYHOLIDAY==null)
	{
		COMPANYHOLIDAY=new Shift();
		COMPANYHOLIDAY.shiftName="HOLIDAY";
		COMPANYHOLIDAY.shortName="HOL";
	}
	return COMPANYHOLIDAY;
}



public static void setCOMPANYHOLIDAY(Shift cOMPANYHOLIDAY) {
	COMPANYHOLIDAY = cOMPANYHOLIDAY;
}



public static long getSerialversionuid() {
	return serialVersionUID;
}



public double getIntimeGracePeriod() {
	return intimeGracePeriod;
}



public void setIntimeGracePeriod(double intimeGracePeriod) {
	this.intimeGracePeriod = intimeGracePeriod;
}



public double getOuttimeGracePeriod() {
	return outtimeGracePeriod;
}



public void setOuttimeGracePeriod(double outtimeGracePeriod) {
	this.outtimeGracePeriod = outtimeGracePeriod;
}



public boolean isStatus() {
	return status;
}



public void setStatus(boolean status) {
	this.status = status;
}
	
	
	
}
