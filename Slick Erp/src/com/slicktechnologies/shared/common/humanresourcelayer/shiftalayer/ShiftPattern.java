package com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.googlecode.objectify.annotation.Unindex;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Define Shift  Patterns, A Shift Pattern will be 
 * Used for Batch Assignments of Shifts.
 */

@Entity
@Embed
public class ShiftPattern extends SuperModel{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3397722348247047859L;
	
	/** The pattern name. */
	@Index
	protected String patternName;
	
	/** The pattern short name. */
	@Index
	protected String pattShortName;
	
	/** The pattern category. */
	@Unindex
	protected String patternCategory;
  
	/** The shift. */
	@Unindex
	@Serialize
	protected ArrayList<Shift> shift;

	
	/**
	 * Instantiates a new shift pattern.
	 */
	public ShiftPattern() {
		
		super();
		patternName="";
		pattShortName="";
		patternCategory="";
		shift=new ArrayList<Shift>();
		
	}
	
	
	
	/**
	 * *************************************Getters and Setters***************************************************.
	 *
	 * @return the pattern name
	 */
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Pattern")
	public String getPatternName() {
		return patternName;
	}

	/**
	 * Sets the pattern name.
	 *
	 * @param patternName the new pattern name
	 */
	public void setPatternName(String patternName) {
		if(patternName!=null)
		this.patternName = patternName.trim();
	}
	
	/**
	 * Gets the patt short name.
	 *
	 * @return the patt short name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Pattern Short Name")
	public String getPattShortName() {
		return pattShortName;
	}

	/**
	 * Sets the patt short name.
	 *
	 * @param pattShortName the new patt short name
	 */
	public void setPattShortName(String pattShortName) {
		if(pattShortName!=null)
		this.pattShortName = pattShortName.trim();
	}

	/**
	 * Gets the pattern category.
	 *
	 * @return the pattern category
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Pattern Category")
	public String getPatternCategory() {
		return patternCategory;
	}

	/**
	 * Sets the pattern category.
	 *
	 * @param patternCategory the new pattern category
	 */
	public void setPatternCategory(String patternCategory) {
		if(patternCategory!=null)
		this.patternCategory = patternCategory.trim();
	}
	
	/**
	 * Gets the shift.
	 *
	 * @return the shift
	 */
	public ArrayList<Shift> getShift() {
		return shift;
	}


	/**
	 * Sets the shift.
	 *
	 * @param shift the new shift
	 */
	public void setShift(List<Shift> shift) 
	{
		if(shift!=null)
		{
		    this.shift=new ArrayList<Shift>();
			this.shift.addAll(shift);
		
		}
	}

/**
 * ****************************************************************************************************************************************.
 *
 * @param o the o
 * @return the int
 */
	
	
	
	
	

	@Override
	public int compareTo(SuperModel o) {
		if(o instanceof ShiftPattern)
		{
			ShiftPattern shiftpattern = (ShiftPattern) o;
			if(shiftpattern.getPatternName()!=null)
			{
				return this.patternName.compareTo(shiftpattern.patternName);
			}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
	
		return isDuplicateName(m) || isDuplicateShortName(m);
}
	
	
	
	/**
	 * Checks if is duplicate name.
	 *
	 * @param m the m
	 * @return true, if is duplicate name
	 */
	public boolean isDuplicateName(SuperModel m) {
		ShiftPattern entity = (ShiftPattern) m;
		String name = entity.getPatternName().trim();
		String curname=this.patternName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
}

	
	
	/**
	 * Checks if is duplicate short name.
	 *
	 * @param m the m
	 * @return true, if is duplicate short name
	 */
	public boolean isDuplicateShortName(SuperModel m) {
		ShiftPattern entity = (ShiftPattern) m;
		String name = entity.getPattShortName().trim();
		String curname=this.pattShortName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}

}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return patternName.toString();
	}

		

}
