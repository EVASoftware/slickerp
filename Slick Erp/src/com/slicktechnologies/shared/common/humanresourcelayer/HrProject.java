package com.slicktechnologies.shared.common.humanresourcelayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.inventory.CountSheetDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeProjectHistory;


// TODO: Auto-generated Javadoc
/**
 * Represents continuing project of the Company.
 */
@Entity
public class HrProject extends SuperModel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7154721589045791067L;
	
	
	/** The project name. */
	@Index
	protected String projectName;
	protected String projectCode;
	protected ArrayList<EmployeeInfo> team;
	protected ArrayList<String> tasks;
	
	@Index
	PersonInfo personinfo;
	
	/** The status. */
	@Index
	protected boolean status;
	
	/** The description. */
	protected String description;
	
	
	/**
	 * Date : 31-05-2018
	 */
	double budgetedHours;
	Date fromDate;
	Date toDate;
	
	
	/**
	 * Date : 16-07-2018 By ANIL
	 */
	ArrayList<Overtime> otList;
	
	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/08/2018 @16:51
	 * Note: Added boolean variable to check is finger print allowed or not for specific project
	 */
	@Index
	boolean isFingerprintAllowed;

	/**
	 * Instantiates a new hr project.
	 */
	/** date 18.8.2018 added by komal to make hr project and cnc project similar**/
	@Index
	String supervisor;
	@Index
	String manager;
	@Index
	String branchManager;
	@Index
	int cncProjectCount;
	
	@Index
	String branch;
	
	/**
	 * Date :30-10-2018 @author Anil
	 */
	@Index
	String natureOfWork;
	
	/**
	 * date 10.1.2019 added by komal
	 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
	 *               which one is allowed to mark attendance
	 **/
	double syncFrequency;
	double submitFrequency;
	double allowedRadius;
	double locationLatitude;
	double locationLongitude;
	/** date 16.08.2019 added by komal to store minimum ot hours**/
	double miOtMinutes;
	
	
	/**
	 * @author Anil , Date : 01-10-2019
	 * Updating employee details from employee to HrProject to Project Allocation
	 */
	boolean reversEmpProjectAllocation=false;
	

	/**
	 * @author Anil , Date : 29-11-2019
	 * Capturing paid days for payroll, currently per day salary calculated on the basis of month days
	 */
	
	double paidDaysInMonth;
	boolean exceptionForMonth;
	double paidDaysForExceptionalMonth;
	String specificMonth;
	String extraDayCompName;
		protected String customerBranch;
		
	/**
	 * @author Anil
	 * @since 24-07-2020
	 * Adding field payroll start day which will define payroll cycle
	 */
	Integer payrollStartDay;
	
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * Adding state field for Sunrise Facility
	 * Raised by Rahul Tiwari
	 */
	@Index
	String state;
	
	/**
	 * @author Anil
	 * @since 27-08-2020
	 */
	ArrayList<Overtime> relieversOtList;

	String synchedBy;
	
	/**
	 * @author Anil @since 04-02-2021
	 * adding pf capping amount 
	 * for SUN RISE raised by Rahul
	 */
	double pfCappingAmt;
	
	@Index
	boolean seperateEntityFlag;
	ArrayList<Integer> deletedOtIdlist;
	
	/*
	 * Ashwini Patil 
	 * Date:11-07-2023 
	 * Spick and spine want to do payroll of one project in which every employee has a weekly off defined ie a specific day like monday or tuesday etc
	 * but employee can work on wo day and can take weekly off on another day
	 * at the end of month we have to calculate no of wo for that employee and need to pay salary as per no of days worked. 
	 * so defining this rotationalWeeklyOff flag to manage this
	 */
	boolean isRotationalWeeklyOff;
	
	

	// Ashwini Patil Date:02-11-2023 for spick and span
	Integer fixedPayrollDays;
	
	// Ashwini Patil Date:22-01-2024 for advance FM since their client given unplanned holidays
	boolean isAllowUnplannedHoliday;

	

	public HrProject() {
		super();
		projectName="";
		description="";
		projectCode="";
		personinfo=new PersonInfo();
		
		otList=new ArrayList<Overtime>();
		/*Added by Apeksha on 24/08/2018 @16:51*/
		isFingerprintAllowed = true;
		/** date 18.8.2018 added by komal to make hr project and cnc project similar**/
		supervisor = "";
		manager = "";
		branchManager = "";
		branch="";
		natureOfWork="";
		synchedBy = "";
		seperateEntityFlag = false;
		deletedOtIdlist = new ArrayList<Integer>();
		isRotationalWeeklyOff=false;
		isAllowUnplannedHoliday=false;
	}
	
	

	public String getNatureOfWork() {
		return natureOfWork;
	}



	public void setNatureOfWork(String natureOfWork) {
		this.natureOfWork = natureOfWork;
	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}



	public boolean isFingerprintAllowed() {
		return isFingerprintAllowed;
	}

	public void setFingerprintAllowed(boolean isFingerprintAllowed) {
		this.isFingerprintAllowed = isFingerprintAllowed;
	}
	
	

	public ArrayList<Overtime> getOtList() {
		return otList;
	}





	public void setOtList(List<Overtime> otList) {
		ArrayList<Overtime> otList1=new ArrayList<Overtime>();
		otList1.addAll(otList);
		this.otList = otList1;
	}





	public double getBudgetedHours() {
		return budgetedHours;
	}



	public void setBudgetedHours(double budgetedHours) {
		this.budgetedHours = budgetedHours;
	}



	public Date getFromDate() {
		return fromDate;
	}



	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}



	public Date getToDate() {
		return toDate;
	}



	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}



	/**
	 * **************************************Getters and Setters***********************************************.
	 *
	 * @return the project name
	 */
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Project Name")
	public String getProjectName() {
		return projectName;
	}

	public ArrayList<EmployeeInfo> getTeamtable() {
		return team;
	}

	public void setTeamtable(List<EmployeeInfo> list)
	{
		if(list!=null)
		{
			this.team=new ArrayList<EmployeeInfo>();
			this.team.addAll(list);
		
		}
	
	}

	public ArrayList<String> getTasktable() {
		return tasks;
	}

	public void setTasktable(List<String> list) {
		if(list!=null)
		{
			this.tasks=new ArrayList<String>();
			this.tasks.addAll(list);
		
		}
	}

	/**
	 * Sets the project name.
	 *
	 * @param projectName the new project name
	 */
	public void setProjectName(String projectName) {
		if(projectName!=null)
		this.projectName = projectName.trim();
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Boolean status) {
		if(status!=null)
		this.status = status;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null)
		this.description = description.trim();
	}

	
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof HrProject)
		{
			HrProject dep = (HrProject) arg0;
			if(dep.getProjectName()!=null)
			{
				return this.projectName.compareTo(dep.projectName);
			}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		HrProject entity = (HrProject) m;
		String name = entity.getProjectName().trim();
		String curname=this.projectName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return projectName.toString();
	}
	
	/**Date 19-3-2019 added by Amol for the update the employee project name as per the Hr Project Name**/
	@OnSave
	@GwtIncompatible
	public  void UpdateEmployeesProject(){
		/**
		 * This value gets true only when we are updating it from employee master
		 */
		if(reversEmpProjectAllocation){
			reversEmpProjectAllocation=false;
			return;
		}
		
		boolean siteLocation=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HrProject", "EnableSiteLocation", this.getCompanyId());
		if(this.getCncProjectCount()!=0&&siteLocation){
			
			ProjectAllocation projectAllocation=ofy().load().type(ProjectAllocation.class).filter("companyId", this.getCompanyId())
					.filter("projectName", projectName).filter("count", this.getCncProjectCount()).first().now();
			if(projectAllocation!=null){
				HashSet<String> empIdHs=new HashSet<String>();
				
				/**
				 * @author Vijay @Since 21-09-2022
				 * Loading employee project allocation from different entity
				 */
				   CommonServiceImpl commonservice = new CommonServiceImpl();
				   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
			    /**
			     * ends here
			     */
//				if(projectAllocation.getEmployeeProjectAllocationList()!=null){
				if(empprojectallocationlist!=null && empprojectallocationlist.size()>0){
					for(Overtime ot:otList){
						boolean updateAllFlag=false;
						for(EmployeeProjectAllocation empProjAll:empprojectallocationlist){
							if(empProjAll.getEmployeeId()!=null && empProjAll.getEmployeeId().equals(ot.getEmpId()+"")){
								updateAllFlag=true;
								if(getSiteLocationName(ot.getEmpId())!=null){
									if(ot.getSiteLocation()!=null){
										if(empProjAll.getSiteLocation()==null){
											empProjAll.setSiteLocation(ot.getSiteLocation());
										}else{
											if(!empProjAll.getSiteLocation().equals(ot.getSiteLocation())){
												empProjAll.setSiteLocation(ot.getSiteLocation());
											}
										}
									}
								}
								
							}
						}
						if(!updateAllFlag){
							if(ot.getEmpId()!=0){
								empIdHs.add(ot.getEmpId()+"");
							}
						}
					}
					if(empIdHs.size()>0){
						for(String empIdObj:empIdHs){
							Overtime ot=null;
							for(Overtime obj:otList){
								if(obj.getEmpId()==Integer.parseInt(empIdObj)){
									ot=obj;
									break;
								}
							}
							if(ot!=null){
								EmployeeProjectAllocation empProjAll=new EmployeeProjectAllocation();
								empProjAll.setEmployeeId(ot.getEmpId()+"");
								empProjAll.setEmployeeName(ot.getEmpName());
								empProjAll.setEmployeeRole(ot.getEmpDesignation());
								empProjAll.setShift(ot.getShift());
								empProjAll.setStartDate(ot.getStartDate());
								empProjAll.setEndDate(ot.getEndDate());
								empProjAll.setSiteLocation(ot.getSiteLocation());
								projectAllocation.getEmployeeProjectAllocationList().add(empProjAll);
								
								/**
								 * @author Vijay @Since 19-09-2022
								 * if Employee Project Allocation list greater than 600 then storing it into seperate intity 
								 */
								if(projectAllocation.getEmployeeProjectAllocationList().size()>AppConstants.employeeProjectAllocationListNumber || projectAllocation.isSeperateEntityFlag()){
										projectAllocation.getEmployeeProjectAllocationList().clear();
										commonservice.updateEmployeeProjectAllocationList(projectAllocation.getEmployeeProjectAllocationList(), "Save");
								}
								/**
								 * ends here
								 */
								
							}
						}
					}
					
					projectAllocation.setReversEmpProjectAllocation(true);
					ofy().save().entity(projectAllocation).now();
				}
			}
		}
		
		HashSet <Integer>hsempId=new  HashSet<Integer>();
		for(Overtime ot:otList){
			hsempId.add(ot.getEmpId());
		}
		ArrayList<Integer>empIdList=new ArrayList<Integer>();
		empIdList.addAll(hsempId);
		System.out.println("EMPLOYEEIDLISTTT"+empIdList.size());
		
		List<Employee> empList = null;
		if(empIdList!=null&&empIdList.size()!=0){
			empList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("count IN",empIdList).list();
			System.out.println("EMPLOYEE LIST SIZE"+empList.size());
		}
		
		
		if(empList!=null&&empList.size()!=0){
			for(Employee emp:empList){
				boolean siteFlag=false;
				/*****Date 10-8-2019 by Amol update employee branch in employee Master****/
				if(!emp.getProjectName().equals(projectName)&&emp.getBranchName().equals(branch)){
					emp.setProjectName(projectName);
					emp.setBranchName(branch);
					
					EmployeeProjectHistory emprojectHistrory=new EmployeeProjectHistory();
					emprojectHistrory.setProjectCode(projectCode);
					emprojectHistrory.setProjectName(projectName);
					emprojectHistrory.setProjectStartDate(new Date());
					emprojectHistrory.setProjectEndDate(new Date());
					
					if(siteLocation){
						emprojectHistrory.setSiteLocation(getSiteLocationName(emp.getCount()));
					}
					siteFlag=true;
					emp.getEmployeeProjectHistoryList().add(emprojectHistrory);
				}
				/**
				 * @author Anil
				 * @since 02-09-2020
				 * @since 03-03-2021 
				 * Removed siteFlag condition
				 */
				if(siteLocation){
//				if(!siteFlag&&siteLocation){
//					if(emp.getSiteLocation()!=null&&!emp.getSiteLocation().equals("")){
						String siteName=getSiteLocationName(emp.getCount());
						if(siteName!=null){
							String empSiteName="";
							if(emp.getSiteLocation()!=null){
								empSiteName=emp.getSiteLocation();
							}
							/**
							 * @author Anil @since 03-03-2021
							 * If site location on employee screen is null but it
							 */
							else{
								emp.setSiteLocation(siteName);
							}
							if(!siteName.equals(empSiteName)){
								Overtime ot=getSiteLocationName(emp.getCount(), siteName);
								EmployeeProjectHistory emprojectHistrory=new EmployeeProjectHistory();
								emprojectHistrory.setProjectCode(projectCode);
								emprojectHistrory.setProjectName(projectName);
								
								if(ot!=null){
									if(ot.getStartDate()!=null){
										emprojectHistrory.setProjectStartDate(ot.getStartDate());
									}else{
										emprojectHistrory.setProjectStartDate(new Date());
									}
									if(ot.getEndDate()!=null){
										emprojectHistrory.setProjectEndDate(ot.getEndDate());
									}else{
										emprojectHistrory.setProjectEndDate(new Date());
									}
								}else{
									emprojectHistrory.setProjectStartDate(new Date());
									emprojectHistrory.setProjectEndDate(new Date());
								}
								emprojectHistrory.setSiteLocation(siteName);
								emp.getEmployeeProjectHistoryList().add(emprojectHistrory);
								emp.setSiteLocation(siteName);
							}
						}
//					}
				}
			}
			ofy().save().entities(empList).now();
		}
	}
	
	public String getSiteLocationName(int empid){
		String siteName=null;
		HashSet<String> siteHs=new HashSet<String>();
		for(Overtime ot:otList){
			if(empid==ot.getEmpId()){
				if(ot.getSiteLocation()!=null&&!ot.getSiteLocation().equals("")){
					siteHs.add(ot.getSiteLocation());
					siteName=ot.getSiteLocation();
				}
			}
		}
		if(siteHs.size()!=0){
			if(siteHs.size()>1){
				siteName=null;
			}
		}
		return siteName;
	}
	
	public Overtime getSiteLocationName(int empid,String sitelocation){
		for(Overtime ot:otList){
			if(empid==ot.getEmpId()){
				if(ot.getSiteLocation()!=null&&!ot.getSiteLocation().equals("")&&sitelocation.equals(ot.getSiteLocation())){
					return ot;
				}
			}
		}
		return null;
	}
	
	/**
	 * Make project list box live.
	 *
	 * @param olb the olb
	 */
	public static void MakeProjectListBoxLive(ObjectListBox<HrProject>olb)
	{
		MyQuerry querry=new MyQuerry();
		Filter temp=new Filter();
		temp.setQuerryString("status");
		temp.setBooleanvalue(true);
		
		
		
		querry.getFilters().add(temp);
		querry.setQuerryObject(new HrProject());
		olb.MakeLive(querry);
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public PersonInfo getPersoninfo() {
		return personinfo;
	}

	public void setPersoninfo(PersonInfo personinfo) {
		this.personinfo = personinfo;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}



	public ArrayList<EmployeeInfo> getTeam() {
		return team;
	}



	public void setTeam(ArrayList<EmployeeInfo> team) {
		this.team = team;
	}



	public ArrayList<String> getTasks() {
		return tasks;
	}



	public void setTasks(ArrayList<String> tasks) {
		this.tasks = tasks;
	}



	public String getSupervisor() {
		return supervisor;
	}



	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}



	public String getManager() {
		return manager;
	}



	public void setManager(String manager) {
		this.manager = manager;
	}



	public String getBranchManager() {
		return branchManager;
	}



	public void setBranchManager(String branchManager) {
		this.branchManager = branchManager;
	}



	public int getCncProjectCount() {
		return cncProjectCount;
	}



	public void setCncProjectCount(int cncProjectCount) {
		this.cncProjectCount = cncProjectCount;
	}



	public void setOtList(ArrayList<Overtime> otList) {
		this.otList = otList;
	}



	public double getSyncFrequency() {
		return syncFrequency;
	}



	public void setSyncFrequency(double syncFrequency) {
		this.syncFrequency = syncFrequency;
	}



	public double getSubmitFrequency() {
		return submitFrequency;
	}



	public void setSubmitFrequency(double submitFrequency) {
		this.submitFrequency = submitFrequency;
	}



	public double getAllowedRadius() {
		return allowedRadius;
	}



	public void setAllowedRadius(double allowedRadius) {
		this.allowedRadius = allowedRadius;
	}



	public double getLocationLatitude() {
		return locationLatitude;
	}



	public void setLocationLatitude(double locationLatitude) {
		this.locationLatitude = locationLatitude;
	}



	public double getLocationLongitude() {
		return locationLongitude;
	}



	public void setLocationLongitude(double locationLongitude) {
		this.locationLongitude = locationLongitude;
	}



	public double getMiOtMinutes() {
		return miOtMinutes;
	}



	public void setMiOtMinutes(double miOtMinutes) {
		this.miOtMinutes = miOtMinutes;
	}



	public boolean isReversEmpProjectAllocation() {
		return reversEmpProjectAllocation;
	}



	public void setReversEmpProjectAllocation(boolean reversEmpProjectAllocation) {
		this.reversEmpProjectAllocation = reversEmpProjectAllocation;
	}



	public double getPaidDaysInMonth() {
		return paidDaysInMonth;
	}



	public void setPaidDaysInMonth(double paidDaysInMonth) {
		this.paidDaysInMonth = paidDaysInMonth;
	}



	public boolean isExceptionForMonth() {
		return exceptionForMonth;
	}



	public void setExceptionForMonth(boolean exceptionForMonth) {
		this.exceptionForMonth = exceptionForMonth;
	}



	public double getPaidDaysForExceptionalMonth() {
		return paidDaysForExceptionalMonth;
	}



	public void setPaidDaysForExceptionalMonth(double paidDaysForExceptionalMonth) {
		this.paidDaysForExceptionalMonth = paidDaysForExceptionalMonth;
	}



	public String getExtraDayCompName() {
		return extraDayCompName;
	}



	public void setExtraDayCompName(String extraDayCompName) {
		this.extraDayCompName = extraDayCompName;
	}



	public String getSpecificMonth() {
		return specificMonth;
	}



	public void setSpecificMonth(String specificMonth) {
		this.specificMonth = specificMonth;
	}
	
		public String getCustomerBranch() {
		return customerBranch;
	}



	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}
	
	public Integer getPayrollStartDay() {
		return payrollStartDay;
	}

	public void setPayrollStartDay(Integer payrollStartDay) {
		this.payrollStartDay = payrollStartDay;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}



	public ArrayList<Overtime> getRelieversOtList() {
		return relieversOtList;
	}



	public void setRelieversOtList(List<Overtime> relieversOtList) {
		ArrayList<Overtime> otList=new ArrayList<Overtime>();
		otList.addAll(relieversOtList);
		this.relieversOtList = otList;
	}



	public String getSynchedBy() {
		return synchedBy;
	}



	public void setSynchedBy(String synchedBy) {
		this.synchedBy = synchedBy;
	}



	public double getPfCappingAmt() {
		return pfCappingAmt;
	}



	public void setPfCappingAmt(double pfCappingAmt) {
		this.pfCappingAmt = pfCappingAmt;
	}



	public boolean isSeperateEntityFlag() {
		return seperateEntityFlag;
	}



	public void setSeperateEntityFlag(boolean seperateEntityFlag) {
		this.seperateEntityFlag = seperateEntityFlag;
	}



	public ArrayList<Integer> getDeletedOtIdlist() {
		return deletedOtIdlist;
	}



	public void setDeletedOtIdlist(ArrayList<Integer> deletedOtIdlist) {
		this.deletedOtIdlist = deletedOtIdlist;
	}

	public boolean isRotationalWeeklyOff() {
		return isRotationalWeeklyOff;
	}



	public void setRotationalWeeklyOff(boolean isRotationalWeeklyOff) {
		this.isRotationalWeeklyOff = isRotationalWeeklyOff;
	}



	public Integer getFixedPayrollDays() {
		return fixedPayrollDays;
	}



	public void setFixedPayrollDays(Integer fixedPayrollDays) {
		this.fixedPayrollDays = fixedPayrollDays;
	}

	public boolean isAllowUnplannedHoliday() {
		return isAllowUnplannedHoliday;
	}



	public void setAllowUnplannedHoliday(boolean isAllowUnplannedHoliday) {
		this.isAllowUnplannedHoliday = isAllowUnplannedHoliday;
	}




}
