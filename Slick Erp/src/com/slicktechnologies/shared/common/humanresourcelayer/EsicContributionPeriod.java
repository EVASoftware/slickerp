package com.slicktechnologies.shared.common.humanresourcelayer;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
@Index
public class EsicContributionPeriod extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7650176510907135674L;
	
	String fromPeriod;
	String toPeriod;
	
	public EsicContributionPeriod() {
		super();
	}

	
	
	public String getFromPeriod() {
		return fromPeriod;
	}



	public void setFromPeriod(String fromPeriod) {
		this.fromPeriod = fromPeriod;
	}



	public String getToPeriod() {
		return toPeriod;
	}



	public void setToPeriod(String toPeriod) {
		this.toPeriod = toPeriod;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
