package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class VoluntaryPf extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8164479632392265305L;
	
	@Index
	int empId;
	@Index
	String empName;
	@Index
	long empCellNo;
	
	double voluntaryPfPercentage;
	@Index
	Date applicableFromDate;
	@Index
	Date applicableToDate;
	
	@Index
	boolean status;
	
	@Index 
	String name;
	
	
	/**added by amol 6-12-2018**/
	@Index
    protected String branchName;
	
	/***12-1-2019 added by amol.**/
	protected  String projectName;
	
	


	public VoluntaryPf() {
		
	}
	

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getEmpId() {
		return empId;
	}


	public void setEmpId(int empId) {
		this.empId = empId;
	}


	public String getEmpName() {
		return empName;
	}


	public void setEmpName(String empName) {
		this.empName = empName;
	}


	public long getEmpCellNo() {
		return empCellNo;
	}


	public void setEmpCellNo(long empCellNo) {
		this.empCellNo = empCellNo;
	}


	public double getVoluntaryPfPercentage() {
		return voluntaryPfPercentage;
	}


	public void setVoluntaryPfPercentage(double voluntaryPfPercentage) {
		this.voluntaryPfPercentage = voluntaryPfPercentage;
	}


	public Date getApplicableFromDate() {
		return applicableFromDate;
	}


	public void setApplicableFromDate(Date applicableFromDate) {
		this.applicableFromDate = applicableFromDate;
	}


	public Date getApplicableToDate() {
		return applicableToDate;
	}


	public void setApplicableToDate(Date applicableToDate) {
		this.applicableToDate = applicableToDate;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public void setBranchName(String branchName) {
		// TODO Auto-generated method stub
		if(branchName!=null)
			this.branchName = branchName.trim();
	}


	public String getBranchName() {
		// TODO Auto-generated method stub
		return branchName;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	
	
	

}
