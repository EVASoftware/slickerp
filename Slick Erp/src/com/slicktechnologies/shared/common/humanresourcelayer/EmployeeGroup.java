package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

@Entity
public class EmployeeGroup extends SuperModel
{
	public String groupName;
	public ArrayList<EmployeeInfo>employees;
	public boolean status;
	public String description;
	public String groupcode;
	
	
	public EmployeeGroup() {
		super();
		groupName="";
		description="";
		status=true;
		groupcode="";
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Group Name")
	
	public String getGroupName() {
		return groupName;
	}



	public void setGroupName(String groupName) {
		if(groupName!=null)
		  this.groupName = groupName.trim();
	}



	public ArrayList<EmployeeInfo> getemployees() {
		return employees;
	}



	public void setemployees(List<EmployeeInfo> employees) 
	{
		if(employees!=null)
		{
		   this.employees = new ArrayList<EmployeeInfo>();
		   this.employees.addAll(employees);
		   
		}
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Group Code")
	public String getGroupcode() {
		return groupcode;
	}


	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		return isDuplicateGroupName(m) || isDuplicateGroupCode(m);
		
	}

	public boolean isDuplicateGroupName(SuperModel m)
	{
		EmployeeGroup entity = (EmployeeGroup) m;
		String name = entity.getGroupName().trim();
		String curname=this.groupName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
public boolean isDuplicateGroupCode(SuperModel m)
{
	EmployeeGroup entity = (EmployeeGroup) m;
	String name = entity.getGroupcode().trim();
	String curname=this.groupcode.trim();
	curname=curname.replaceAll("\\s","");
	curname=curname.toLowerCase();
	name=name.replaceAll("\\s","");
	name=name.toLowerCase();
	
	//New Object is being added
	if(id==null)
	{
		if(name.equals(curname))
			return true;
		else
			return false;
	}
	
	// Old object is being updated
	else
	{
		
		if(id.equals(entity.id))
		{
		  return false;	
		}
		if((name.equals(curname)==true))
		  return true;
		else
			return false;
			
	}
}

	public void setStatus(Boolean value) {
		this.status=value;
		
	}
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Status")
	
	public Boolean getStatus() {
		return status;
		
	}

   public String getDescription()
   {
	   return description;
   }
   
   public void setDescription(String description)
   {
	   if(description!=null)
	      this.description=description.trim();
   }



public static void makeObjectListBoxLive(
		ObjectListBox<EmployeeGroup> olbEmployeeGroup) {
	MyQuerry querry=new MyQuerry();
	querry.setQuerryObject(new EmployeeGroup());
	Filter filter=new Filter();
	filter.setQuerryString("status");
	filter.setBooleanvalue(true);
	
}


@Override
public int compareTo(SuperModel o) {
	// TODO Auto-generated method stub
	return 0;
}
   
   

	
	

}
