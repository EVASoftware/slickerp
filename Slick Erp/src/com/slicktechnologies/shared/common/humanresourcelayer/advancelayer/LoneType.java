package com.slicktechnologies.shared.common.humanresourcelayer.advancelayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Represents Type of Loan.
 */
@Entity
public class LoneType extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7490435798850733219L;
	

	
	/**  Name of Loan Type. */
	@Index
	protected String advanceTypeName;
	
	
	/**  Duration of the loan. */
	protected int advanceDuration;
	
	/**  Intrest on loan. */
	protected Double advanceInterest;
	
	/** The status. */
	@Index
	protected boolean status;
	
	/**  maximum advance amount for Loan type*. */
	protected int maxAdvance;
	
	
	
	/**
	 * Instantiates a new lone type.
	 */
	public LoneType() {
		super();
		advanceTypeName="";
		
	}

	/**
	 * ****************************************Getters and Setters**************************************************.
	 *
	 * @return the status
	 */
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Gets the max advance.
	 *
	 * @return the max advance
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Maximum Advance")
	public Integer getMaxAdvance() {
		return maxAdvance;
	}

	/**
	 * Sets the max advance.
	 *
	 * @param maxAdvance the new max advance
	 */
	public void setMaxAdvance(Integer maxAdvance) {
		this.maxAdvance = maxAdvance;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	/**
	 * Gets the advance type name.
	 *
	 * @return the advance type name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getAdvanceTypeName() {
	
		return advanceTypeName;
	}

	/**
	 * Sets the advance type name.
	 *
	 * @param advanceTypeName the new advance type name
	 */
	public void setAdvanceTypeName(String advanceTypeName) {
		if(advanceTypeName!=null)
		  this.advanceTypeName = advanceTypeName.trim();
	}

	/**
	 * Gets the advance duration.
	 *
	 * @return the advance duration
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Duration In Months")
	public Integer getAdvanceDuration() {
		return advanceDuration;
	}

	/**
	 * Sets the advance duration.
	 *
	 * @param advanceDuration the new advance duration
	 */
	public void setAdvanceDuration(Integer advanceDuration) {
		this.advanceDuration = advanceDuration;
	}

	/**
	 * Gets the advance interest.
	 *
	 * @return the advance interest
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Interest Rate(%)")
	public Double getAdvanceInterest() {
		return advanceInterest;
	}

	/**
	 * Sets the advance interest.
	 *
	 * @param advanceInterest the new advance interest
	 */
	public void setAdvanceInterest(Double advanceInterest) {
		this.advanceInterest = advanceInterest;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		LoneType entity = (LoneType) m;
		String name = entity.getAdvanceTypeName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.advanceTypeName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	

/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
	return advanceTypeName;
}

}
