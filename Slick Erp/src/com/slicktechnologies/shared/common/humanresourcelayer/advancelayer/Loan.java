package com.slicktechnologies.shared.common.humanresourcelayer.advancelayer;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Query;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.personlayer.Employee;

import static com.googlecode.objectify.ObjectifyService.ofy;
// TODO: Auto-generated Javadoc

/**
 * Application for taking the Loan, each approved Loan application will create an object of
 * {@link LoneEmi} in datastore.
 */
@Entity
public class Loan extends EmployeeHRProcess implements ApprovableProcess{
/********************************************Attributes**********************************************/

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5401336175976893101L;
	
	/** Represents weather advance is closed or not */
	public static final String ADVANCECLOSED="Closed";
	
	/** total amount of Advance */
	@Index
	protected int advanceAmount;
	
	/** Type of Advance*/
	@Index
	 protected String advanceType;
	 
	/** Reason for Advance*/
	protected String advReason;
	
	/** Duration of loan */
	protected int durationInMonth;
	
	/** Culmative previous balance of all Application this value is filled from sum of all
	 * unpaid EMIS for all {@link Loan}. for an Employee. It is a derived attribute so it is not saved*/
	@Ignore
	protected Double prevBalance;
	
	/** percentage interest on which advance is being taken, This value will come from Config *. */
	protected Double advInterest;
	
	/** Monthly Deduction from salary. */
	protected double deductionMonthly;
	
	/**
	 * Emis Corresponding to this Loan Application.
	 */
	@Ignore
	protected ArrayList<Emi>loanEmi;
	
	protected double balance;

 /******************************************Relational Part*******************************************/
	protected Key<LoneType>loanTypeKey;
	
/******************************************Loan Application*******************************************/	
	
	/**
	 * Instantiates a new advance.
	 */
	public Loan() {
		
		super();
		advReason="";
		loanEmi=new ArrayList<Emi>();
		advanceType="";
		
		
		}

/******************************************Getter Setter*******************************************/	

	
	/**
	 * Gets the prev balance.
	 *
	 * @return the prev balance
	 */
	public Double getPrevBalance() {
		return prevBalance;
	}
	/**
	 * Sets the prev balance.
	 *
	 * @param prevBalance the new prev balance
	 */
	public void setPrevBalance(Double prevBalance) {
		if(prevBalance!=null)
		  this.prevBalance = prevBalance;
	}

  public String getAdvReason() {
		return advReason;
	}

	
	/**
	 * Gets the advance type.
	 *
	 * @return the advance type
	 */
	public String getAdvanceType() {
		return advanceType;
	}


	/**
	 * Sets the advance type.
	 *
	 * @param advanceType the new advance type
	 */
	public void setAdvanceType(String advanceType) {
		if(advanceType!=null)
		this.advanceType = advanceType.trim();
	}


	/**
	 * Sets the adv reason.
	 *
	 * @param advReason the new adv reason
	 */
	public void setAdvReason(String advReason) {
		if(advReason!=null){
		this.advReason = advReason.trim();}
	}

	/**
	 * Gets the advance amount.
	 *
	 * @return the advance amount
	 */
	public Integer getAdvanceAmount() {
		return advanceAmount;
	}


	/**
	 * Sets the advance amount.
	 *
	 * @param advanceAmount the new advance amount
	 */
	public void setAdvanceAmount(Integer advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	
	/**
	 * Gets the adv interest.
	 *
	 * @return the adv interest
	 */
	public Double getAdvInterest() {
		return advInterest;
	}


	/**
	 * Sets the adv interest.
	 *
	 * @param advInterest the new adv interest
	 */
	public void setAdvInterest(Double advInterest) {
		this.advInterest = advInterest;
	}


	
	
	
	/**
	 * Gets the deduction monthly.
	 *
	 * @return the deduction monthly
	 */
	
	public Double getDeductionMonthly() {
		return deductionMonthly;
	}


	/**
	 * Sets the deduction monthly.
	 *
	 * @param deductionMonthly the new deduction monthly
	 */
	public void setDeductionMonthly(Double deductionMonthly) {
		this.deductionMonthly = deductionMonthly;
	}
	
	/**
	 * Gets the duration in month.
	 *
	 * @return the duration in month
	 */
	public Integer getDurationInMonth() {
		return durationInMonth;
	}

	/**
	 * Sets the duration in month.
	 *
	 * @param durationInMonth the new duration in month
	 */
	public void setDurationInMonth(Integer durationInMonth) {
		this.durationInMonth = durationInMonth;
	}

	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList()
	{
		ArrayList<String> custstatuslist=new ArrayList<String>();
		custstatuslist.add(CREATED);
		custstatuslist.add(REQUESTED);
		custstatuslist.add(APPROVED);
		custstatuslist.add(REJECTED);
		custstatuslist.add(ADVANCECLOSED);
		return custstatuslist;
	}
	
	
	
@Override
public String toString() {
	return super.toString();
}

/**
 * Sets the advance amount.
 *
 * @param advanceAmount the new advance amount
 */
public void setAdvanceAmount(int advanceAmount) {
	this.advanceAmount = advanceAmount;
}

/**
 * Sets the duration in month.
 *
 * @param durationInMonth the new duration in month
 */
public void setDurationInMonth(int durationInMonth) {
	this.durationInMonth = durationInMonth;
}


@Override
public String getEmployee() {
	return this.employeeName;
}



public ArrayList<Emi> getLoanEmi() {
	return loanEmi;
}

public void setLoanEmi(List<Emi> loanEmi) {
	if(loanEmi!=null)
	{
		loanEmi=new ArrayList<Emi>();
		this.loanEmi.addAll(loanEmi);
	   
	}
}

public void setDeductionMonthly(double deductionMonthly) {
	this.deductionMonthly = deductionMonthly;
}


@Override
public void setRemark(String remark) {
	
}

@Override
public String getRemark() {
	return null;
}

/******************************************Save********************************************************/	

@GwtIncompatible
@OnSave
private void onSave()
{
	//Load the Key From Object
	if(getAdvanceType()!=null)
	   loanTypeKey=ofy().load().type(LoneType.class).filter("advanceTypeName",getAdvanceType()).filter("companyId",getCompanyId()).keys().first().now();
}

@GwtIncompatible
@OnLoad
private void onLoad()
{
	//Load the Key From Object
	if(loanTypeKey!=null)
	{
		LoneType type=ofy().load().key(loanTypeKey).now();
		if(type!=null)
		{
			advanceType=type.getAdvanceTypeName();
		}
	}
	
}

/**
 * From Database Method fills the Culmative Unpaid Amount.
 */
@GwtIncompatible
@OnLoad
public void updatePreviousBalance()
{
	this.prevBalance=calculatePreviousBalance(getEmpid(),getCompanyId());
	System.out.println("Calling Previous Balance with ID"+this.prevBalance+" "+count);
}

@GwtIncompatible
@OnLoad
public void updateEmiDetails()
{
	LoneEmi lemi=ofy().load().type(LoneEmi.class).filter("advanceId",getCount()).filter("companyId",companyId).first().now();
    if(lemi!=null)
    {
    	this.loanEmi=lemi.getEmi();
    }
    

}


/*******************Calculates the Previous balance*********************************/
/**
 * Calculate Previous Balance of All Loan Application
 * Corresponding to this Employee.
 * @param empId id of Employee.
 * @param companyId companyId.
 * @return Culmative remaining Balance.
 * To Do : Find a way to get only those Loan Emi which are un paid.
 * To Do : No need to create Employee Key.
 */
@GwtIncompatible
public static double calculatePreviousBalance(int empId,Long companyId)
{
	List<LoneEmi> emiInfo;
	Key<EmployeeInfo>employeeKey;
	if(companyId!=null)
	{
		employeeKey=ofy().load().type(EmployeeInfo.class).filter("empCount",empId).filter("companyId",companyId).keys().first().now();
	}
	else
	{
		employeeKey=ofy().load().type(EmployeeInfo.class).filter("empCount",empId).keys().first().now();
	}
	
	if(employeeKey==null)
		return 0;
	
	if(companyId!=null)
	{
		emiInfo=ofy().load().type(LoneEmi.class).filter("employeeKey",employeeKey).filter("companyId",companyId).list();
	}
	else{
		emiInfo=ofy().load().type(LoneEmi.class).filter("employeeKey",employeeKey).list();
	}
	
	double sum=0;
	
	if(emiInfo!=null)
	{
		for(LoneEmi emi:emiInfo){
		  sum=sum+emi.unpaidAmt();
		}
	}
	return sum;
}


/**
 * On Approval of {@link Loan} A {@link LoneEmi} is Created and get Saved in Database.
 * To Do : Key issue
 */

@Override
@GwtIncompatible
public void reactOnApproval() 
{
	System.out.println("Inside Approval Method....");
	
	if(status.equals(Loan.APPROVED))
	{
		System.out.println("Inside Approved....");
		
		List<Emi>list=Emi.calculateEMI(deductionMonthly,advanceAmount,durationInMonth,fromdate,advInterest);
		
		System.out.println("Approval key EmpId : "+getEmpid());
		Key<EmployeeInfo>employeeKey=ofy().load().type(EmployeeInfo.class).filter("empCount",getEmpid()).filter("companyId",getCompanyId()).keys().first().now();
		
		System.out.println("Approval List Size : "+list.size());
		System.out.println("Approval advanceId : "+this.getCount());
		System.out.println("Approval emp keyIfo : "+employeeKey);
		
		LoneEmi emi=new LoneEmi();
		emi.setEmi(list);
		emi.setAdvanceId(this.getCount());
		emi.setLoanKey(Key.create(Loan.class, id));
		emi.setCompanyId(getCompanyId());
		emi.setEmployeeKey(employeeKey);
		
		GenricServiceImpl genimpl=new GenricServiceImpl();
		genimpl.save(emi);
		
		
		setBalance(this.advanceAmount);
			
   }
	
}

	@Override
	public void reactOnRejected() {
		//Do Nothing
		
	}
	
	public Double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		balance = Math.round(balance*100)/100.0d;
		this.balance = balance;
	}
}
