package com.slicktechnologies.shared.common.humanresourcelayer.advancelayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
// TODO: Auto-generated Javadoc
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Kind representing All Emi of advance taken by Employee for a particular {@link Loan}
 */
@Entity
public class LoneEmi extends SuperModel
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8115695028681309109L;
	
	/** Key of Loan Application corresponding to this LoneEmi*/
	@Index
	protected Key<Loan> loanKey;
	
	@Index
	protected Key<EmployeeInfo>employeeKey;
	
	/**
	 * it holds the advance id(count) of Loan Entity.
	 */
	@Index
	protected int advanceId;
	
	/** List of Emi Info corresponding to this Advance Emi */
//	@Serialize
	protected  ArrayList<Emi>emi;
	
	
	
	public LoneEmi() {
		super();
		emi=new ArrayList<Emi>();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Gets the advance id.
	 *
	 * @return the advance id
	 */
	public Key<Loan> getLoanKey() {
		return loanKey;
	}

	/**
	 * Sets the advance id.
	 *
	 * @param advanceId the new advance id
	 */
	public void setLoanKey(Key<Loan> advanceId) {
		this.loanKey = advanceId;
	}

	/**
	 * Gets the emi.
	 *
	 * @return the emi
	 */
	public ArrayList<Emi> getEmi() {
		return emi;
	}

	/**
	 * Sets the emi.
	 *
	 * @param list the new emi
	 */
	public void setEmi(List<Emi> list) {
		if(list!=null)
		{
		   emi =new ArrayList<Emi>();
		   emi.addAll(list);
		
		}
	}
	
	
	
	public Key<EmployeeInfo> getEmployeeKey() {
		return employeeKey;
	}

	public void setEmployeeKey(Key<EmployeeInfo> employeeKey) {
		this.employeeKey = employeeKey;
	}

	/**
	 * Calculates the Unpaid EMI Amount corresponding to a Loan Emi.
	 *
	 * @return the double
	 */
	public double unpaidAmt()
	{
		double sum=0;
		
		for(Emi emi:this.emi)
		{
			
			if(emi.getMonthlyStatus().equals(Emi.UNPAID))
				sum=sum+emi.emiAmount;
			
			
			
		}
		return sum;
	}

	public int getAdvanceId() {
		return advanceId;
	}

	public void setAdvanceId(int advanceId) {
		this.advanceId = advanceId;
	}
	

}
