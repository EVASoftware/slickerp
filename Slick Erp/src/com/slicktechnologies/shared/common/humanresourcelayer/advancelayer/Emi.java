package com.slicktechnologies.shared.common.humanresourcelayer.advancelayer;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;









import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

// TODO: Auto-generated Javadoc
/**
 * Contains attributes for an Emi corresponding to a particular Approved Advance Application.
 * To Do :Payment Period is assumed to be monthly should be configurable.
 */
@Embed
public class Emi implements Serializable,Comparable<Emi>
  {

	/** ***************************Attributes*********************************************************. */
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4379797795816280738L;


	/**  Represents The Emi State UNPAID ,i'e Emi is Unpaid. */
	public static final String UNPAID = "Unpaid";
	
	/**  Represents The Emi State PAID ,i'e Emi is PAID. */
	public static final String PAID = "Paid";

	
	/**  Represents principal amount part  of Emi Payment. */
	protected int principalAmount;
	
	/**  Represents interest amount part  of Emi Payment. */
	protected int intrest;
	
	/**  Represents net Emi Amount. */
	protected int emiAmount;
	
	/**  Balance Loan Amount to be paid after this Emi payment. */
	protected int loanBalance;
	
	/**  Due date of EMI Payment */
	protected double monthlyLoanPaidtoDate;
	
	/**  Status of this  EMI,weather this is paid or not. */
		protected String status;
	
	/**  Represents installment number of EMI. */
	
	protected int installmentNumber;
	
	/**  Emi Payment Date payment due date. */
	protected Date date;
	

	
	public int getInstallmentNumber() {
		return installmentNumber;
	}

	/**
	 * Sets the no of months.
	 *
	 * @param noOfMonths the new no of months
	 */
	public void setInstallemtNumber(int noOfMonths) {
		this.installmentNumber = noOfMonths;
	}

	
	/**
	 * Gets the monthly status.
	 *
	 * @return the monthly status
	 */
	public String getMonthlyStatus() {
		return status;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}





	/**
	 * Sets the monthly status.
	 *
	 * @param monthlyStatus the new monthly status
	 */
	public void setMonthlyStatus(String monthlyStatus) {
		this.status = monthlyStatus;
	}






	/**
	 * Gets the monthly loan paidto date.
	 *
	 * @return the monthly loan paidto date
	 */
	public double getMonthlyLoanPaidtoDate() {
		return monthlyLoanPaidtoDate;
	}






	/**
	 * Sets the monthly loan paidto date.
	 *
	 * @param monthlyLoanPaidtoDate the new monthly loan paidto date
	 */
	public void setMonthlyLoanPaidtoDate(double monthlyLoanPaidtoDate) {
		this.monthlyLoanPaidtoDate = monthlyLoanPaidtoDate;
	}






	/**
	 * Gets the monthlyprincipal amt.
	 *
	 * @return the monthlyprincipal amt
	 */
	public int getMonthlyprincipalAmt() {
		return principalAmount;
	}






	/**
	 * Sets the monthlyprincipal amt.
	 *
	 * @param monthlyprincipalAmt the new monthlyprincipal amt
	 */
	public void setMonthlyprincipalAmt(int monthlyprincipalAmt) {
		this.principalAmount = monthlyprincipalAmt;
	}






	/**
	 * Gets the monthly interest.
	 *
	 * @return the monthly interest
	 */
	public int getMonthlyInterest() {
		return intrest;
	}






	/**
	 * Sets the monthly interest.
	 *
	 * @param monthlyInterest the new monthly interest
	 */
	public void setMonthlyInterest(int monthlyInterest) {
		this.intrest = monthlyInterest;
	}






	/**
	 * Gets the monthly payment.
	 *
	 * @return the monthly payment
	 */
	public int getMonthlyPayment() {
		return emiAmount;
	}






	/**
	 * Sets the monthly payment.
	 *
	 * @param monthlyPayment the new monthly payment
	 */
	public void setMonthlyPayment(int monthlyPayment) {
		this.emiAmount = monthlyPayment;
	}






	/**
	 * Gets the monthly balance.
	 *
	 * @return the monthly balance
	 */
	public int getMonthlyBalance() {
		return loanBalance;
	}






	/**
	 * Sets the monthly balance.
	 *
	 * @param monthlyBalance the new monthly balance
	 */
	public void setMonthlyBalance(int monthlyBalance) {
		this.loanBalance = monthlyBalance;
	}

	/**
	 * Calculates the Emi Corresponding to a {@link Loan}.
	 * To Do : Method assumes EMI Calculation is monthly, Emi Calculation
	 * should be configurable.
	 *
	 * @param deductionamt Monthly deduction amount
	 * @param advAmount the Total loan amount
	 * @param duration the Duration of the Loan
	 * @param startDate the Start Date of the Amount
	 * @param intrerstRate the interest Rate on this Loan
	 * @return the list
	 */
	@GwtIncompatible
	 public static List<Emi> calculateEMI(double deductionamt,double advAmount,int duration,Date startDate,
			 Double intrerstRate)
		{
			List<Emi>listEmi=new ArrayList<Emi>();
			Double loanamt=advAmount;
			Calendar cal=Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String date=sdf.format(startDate);
			try {
				startDate=sdf.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cal.setTime(startDate);
			
			for(int i=1;i<=duration;i++)
			   {
				Emi ad=new Emi();
			
				double monthinter= (double) (advAmount*intrerstRate*1/12*1/100);
				double principal=(double) (deductionamt-monthinter);
				advAmount=advAmount-principal;
				double amttill=(advAmount/loanamt)*100;
				double amttilldate=Math.round(100-amttill);
				ad.setMonthlyBalance((int) advAmount);
				ad.setMonthlyPayment((int) deductionamt);
				ad.setInstallemtNumber(i);
				ad.setMonthlyInterest((int) monthinter);
				ad.setMonthlyprincipalAmt((int) principal);
				ad.setMonthlyLoanPaidtoDate((int) amttilldate);
				ad.setMonthlyStatus(Emi.UNPAID);
			    cal.add(Calendar.MONTH,1);
				
				ad.setDate(cal.getTime());
				listEmi.add(ad);
				
				
		
			}
				
				
			return listEmi;
		

		}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Emi o) {
		if(o!=null)
		{
			if(o.installmentNumber<installmentNumber)
				return 1;
			else if(o.installmentNumber>installmentNumber)
				return -1;
			else
				return 0;
		}
		return 0;
	}

	/**
	 * Gets the principal amount.
	 *
	 * @return the principal amount
	 */
	public int getPrincipalAmount() {
		return principalAmount;
	}

	/**
	 * Sets the principal amount.
	 *
	 * @param principalAmount the new principal amount
	 */
	public void setPrincipalAmount(int principalAmount) {
		this.principalAmount = principalAmount;
	}

	/**
	 * Gets the intrest.
	 *
	 * @return the intrest
	 */
	public int getIntrest() {
		return intrest;
	}

	/**
	 * Sets the intrest.
	 *
	 * @param intrest the new intrest
	 */
	public void setIntrest(int intrest) {
		this.intrest = intrest;
	}

	/**
	 * Gets the emi amount.
	 *
	 * @return the emi amount
	 */
	public int getEmiAmount() {
		return emiAmount;
	}

	/**
	 * Sets the emi amount.
	 *
	 * @param emiAmount the new emi amount
	 */
	public void setEmiAmount(int emiAmount) {
		this.emiAmount = emiAmount;
	}

	/**
	 * Gets the loan balance.
	 *
	 * @return the loan balance
	 */
	public int getLoanBalance() {
		return loanBalance;
	}

	/**
	 * Sets the loan balance.
	 *
	 * @param loanBalance the new loan balance
	 */
	public void setLoanBalance(int loanBalance) {
		this.loanBalance = loanBalance;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the emi number.
	 *
	 * @return the emi number
	 */
	public int getEmiNumber() {
		return installmentNumber;
	}

	/**
	 * Sets the emi number.
	 * @param emiNumber the new emi number
	 */
	public void setEmiNumber(int emiNumber) {
		this.installmentNumber = emiNumber;
	}

	

	

	
	
	

}
