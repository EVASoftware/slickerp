package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;

@Entity
public class PaidLeave extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3048859205253793337L;

	@Index
	String paidLeaveName;
	double rate;
	
	@Index
	boolean status;
	
	@Serialize
	ArrayList<OtEarningComponent> otEarningCompList=new ArrayList<OtEarningComponent>();
	
	
	
	
//	boolean isMonthly;
//	boolean isAnnully;
//	String deductionMonth;
//	boolean isIncludedInEarningComp;
	
	
	public PaidLeave() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	public String getPaidLeaveName() {
		return paidLeaveName;
	}





	public void setPaidLeaveName(String paidLeaveName) {
		this.paidLeaveName = paidLeaveName;
	}





	public double getRate() {
		return rate;
	}





	public void setRate(double rate) {
		this.rate = rate;
	}





	public boolean isStatus() {
		return status;
	}





	public void setStatus(boolean status) {
		this.status = status;
	}





	public ArrayList<OtEarningComponent> getOtEarningCompList() {
		return otEarningCompList;
	}


	public void setOtEarningCompList(List<OtEarningComponent> otEarningCompList) {
		ArrayList<OtEarningComponent> compList=new ArrayList<OtEarningComponent>();
		compList.addAll(otEarningCompList);
		this.otEarningCompList = compList;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub

		PaidLeave entity = (PaidLeave) m;
		String name = entity.getPaidLeaveName().trim();
		String curname = this.paidLeaveName.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();

		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}

		// Old object is being updated
		else {

			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;

		}

	}





	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return paidLeaveName;
	}
	
	

}
