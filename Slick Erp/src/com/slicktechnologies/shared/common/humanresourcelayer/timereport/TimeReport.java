package com.slicktechnologies.shared.common.humanresourcelayer.timereport;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;



import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveApplication;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveValidityInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;


// TODO: Auto-generated Javadoc
/**
 * Represents a Time Report.Right Now There is no independent existence of TimeReport 
 * Entrey it means that there will be no project based logic. As {@link TimeReportEntrey} do not
 * exists independently in Data Store and is not Searchable.
 * To Do :
 *   Design in such a way that {@link TimeReportEntrey} exists independently in database.
 */
@Entity
public class TimeReport extends EmployeeHRProcess implements ApprovableProcess{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7851625908827323113L;
	
	
	/**
	 * {@link TimeReportEntrey} list corresponding to this {@link TimeReport}
	 */
	@Serialize
	protected ArrayList<TimeReportEntrey> timeReportEntry;

	
	/**
	 * By Anil 27-07-15
	 */
	@Index
	protected String month;
	
	
	
	
	/**
	 * Instantiates a new time report.
	 */
	public TimeReport() {

		super();
		timeReportEntry = new ArrayList<TimeReportEntrey>();
		month="";
	}

	/**
	 * *************************************Getters and
	 * Setters***************************************************.
	 *
	 * @return the days
	 */
	
	
	

	public ArrayList<TimeReportEntrey> getTimeReportEntry() {
		return timeReportEntry;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Sets the days.
	 *
	 * @param days
	 *            the new days
	 */
	public void setTimeReportEntry(List<TimeReportEntrey> days) {
		if (days != null) {
			timeReportEntry = new ArrayList<TimeReportEntrey>();
			this.timeReportEntry.addAll(days);

		}
	}

	@Override
	public String getRemark() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRemark(String remark) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getEmployee() {
		return this.approverName;
	}

	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(TimeReport.CREATED);
		list.add(TimeReport.APPROVED);

		list.add(TimeReport.REQUESTED);
		list.add(TimeReport.REJECTED);
		return list;
	}

	@Override
	public void reactOnRejected() {
		// TODO Auto-generated method stub

	}

	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		System.out.println("Inside Time Report Approval.....");
		// Get time report entry
		List<TimeReportEntrey> lisEntrey = getTimeReportEntry();
		List<TimeReportEntrey> trLis = new ArrayList<TimeReportEntrey>();
		List<TimeReportEntrey> otLis = new ArrayList<TimeReportEntrey>();
		System.out.println("Time Report List Size : " + lisEntrey.size());
		/**
		 * Date : 01-06-2018 BY ANIL
		 */
		ArrayList<String> otList=new ArrayList<String>();
		for (TimeReportEntrey temp : lisEntrey) {
			// System.out.println("Inside Time Entery Report Loop..");
			if (temp.isEmployeeOnLeave()) {
				// System.out.println("Employee On Leave ...");
				String prjName = temp.getProjectName().replace("Leave :", "");
				if (temp.getHoursWorked() != 0) {
					trLis.add(temp);
					System.out.println("LEAVE List Size :: " + trLis.size());
				}
			}
			
			
			if (temp.isOvertime()) {
				// System.out.println("Employee On Leave ...");
				String prjName = temp.getProjectName().replace("OT :", "");
				otList.add(prjName);
				if (temp.getHoursWorked() != 0) {
					otLis.add(temp);
				}
			}
			System.out.println("OT List Size :: " + otLis.size());
		}
		grantLeave(trLis);
		getOvertime(otLis,otList);
	}

	
	@GwtIncompatible
	public void getOvertime(List<TimeReportEntrey> ovrTymList, ArrayList<String> otList) {
		System.out.println("Inside Overtime method......");
		if(ovrTymList.size()==0){
			return;
		}
		
		EmployeeInfo info=null;
		if (getCompanyId() != null) {
			info = ofy().load().type(EmployeeInfo.class).filter("companyId", getCompanyId()).filter("empCount", getEmpid()).first().now();
		} else {
			info = ofy().load().type(EmployeeInfo.class).filter("empCount", getEmpid()).first().now();
		}
		
		if(info==null){
			return;
		}
		
		/**
		 * Date : 01-06-2018 By ANIL
		 */
		List<Overtime> overtimeList=ofy().load().type(Overtime.class).filter("companyId", getCompanyId()).filter("name IN", otList).filter("status", true).list();
		
		
		
		List<EmployeeOvertime> employeeOvertime = new ArrayList<EmployeeOvertime>();
		
		for (TimeReportEntrey rep : ovrTymList) {

			String otName = rep.projectName;
			otName = otName.replace("OT :", "");
			Double hrs = rep.hoursWorked;
			
			System.out.println("OT ::: "+otName);
			System.out.println("HRS ::: "+hrs);

			EmployeeOvertime empOvrTym = new EmployeeOvertime();
			
			empOvrTym.setCompanyId(getCompanyId());
			empOvrTym.setEmployee(info);
			empOvrTym.setEmployeeKey(employeeInfoKey);
			
			empOvrTym.setOvertime(getOvertimeInfo(otName,overtimeList));
//			empOvrTym.setOvertime(info.getOvertime());
			empOvrTym.setOtDate(rep.date);
			empOvrTym.setOtHrs(rep.hoursWorked);
			/**
			 * Date : 14-05-2018 By ANIL
			 */
			empOvrTym.setEmplId(this.getEmpid());
			
			employeeOvertime.add(empOvrTym);
		}
		if (employeeOvertime.size() != 0) {
			ofy().save().entities(employeeOvertime).now();
		}
		grantCompensatoryLeave(employeeOvertime);
		
	}
	
	@GwtIncompatible
	private void grantCompensatoryLeave(List<EmployeeOvertime> employeeOvertime) {
		LeaveBalance levBal=ofy().load().type(LeaveBalance.class).filter("companyId",this.getCompanyId()).filter("empInfo.empCount", getEmpid()).first().now();
		
		for(EmployeeOvertime eot:employeeOvertime){
			if(eot.getOvertime().isLeave()==true){
				double earnedleave=0;
				double overtimeHrs=eot.getOtHrs();
				overtimeHrs=overtimeHrs*eot.getOvertime().getLeaveMultiplier();
				earnedleave=overtimeHrs/eot.getEmployee().getLeaveCalendar().getWorkingHours();
				System.out.println("earned Leave ::: "+earnedleave);
				
				
				if(levBal!=null){
					System.out.println("IS Leave BAL  ::: "+levBal.getEmpCount());
					for(int i=0;i<levBal.getLeaveGroup().getAllocatedLeaves().size();i++){
						if(eot.getOvertime().getLeaveName().trim().equals(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim())){
							
							double balEarned=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getEarned();
							balEarned=balEarned+earnedleave;
							levBal.getLeaveGroup().getAllocatedLeaves().get(i).setEarned(balEarned);
							
							double balLeave=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getBalance();
							System.out.println("Earned Leave ::: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().trim()+" BAL "+balLeave);
							
							balLeave=balLeave+earnedleave;
							System.out.println("BAL--- "+balLeave);
							levBal.getLeaveGroup().getAllocatedLeaves().get(i).setBalance(balLeave);
							
							LeaveValidityInfo leaveInfo=new LeaveValidityInfo();
							leaveInfo.setLeaveName(eot.getOvertime().getLeaveName());
							
							leaveInfo.setLeaveCount(earnedleave);
							
							Date validDate=new Date(eot.getOtDate().getTime());
							System.out.println("OT DATE : "+eot.getOtDate());
							System.out.println("NO OF DAYS : "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getNo_of_days_to_avail());
							Date validDate1=DateUtility.addDaysToDate(validDate, levBal.getLeaveGroup().getAllocatedLeaves().get(i).getNo_of_days_to_avail());
							System.out.println("Valid Until : "+validDate+" "+validDate1 );
							leaveInfo.setValidUntil(validDate1);
							
							if(levBal.getLeaveValidityList()!=null){
								levBal.getLeaveValidityList().add(leaveInfo);
							}else{
								List<LeaveValidityInfo> leaveLis=new ArrayList<LeaveValidityInfo>();
								leaveLis.add(leaveInfo);
								levBal.setLeaveValidityList(leaveLis);
							}
							
						}
					}
					ofy().save().entity(levBal).now();
				}
				
				
				
				
				
			}
		}
	}
	@GwtIncompatible
	private Overtime getOvertimeInfo(String otName, List<Overtime> overtimeList) {
		
		for(Overtime ot:overtimeList){
			if(ot.getName().equals(otName)){
				return ot;
			}
		}
		
		return null;
	}

	/**
	 * The requested Leaves are granted to Employees by this method. Technically
	 * it changes the Leave Balance properties for that Employee.
	 */
	@GwtIncompatible
	public void grantLeave(List<TimeReportEntrey> entrey) {
		Logger logger = Logger.getLogger("TimeReport");
		System.out.println("Inside grant leave method ...");
		LeaveBalance temp = null;
		List<LeaveBalance> bal = null;
		
		// Get the List of Leave Balances in Kind , there will be a List as
		// previous year Leave Balance
		// can exist.Ideally the Querry should be from Leave Balance
		
		if (getCompanyId() != null) {
			bal = ofy().load().type(LeaveBalance.class).filter("companyId", getCompanyId()).filter("empInfo.empCount", getEmpid()).list();
		} else {
			bal = ofy().load().type(LeaveBalance.class).filter("empInfo.empCount", getEmpid()).list();
		}

		// If for some reason balance is null return , accurate place to through
		// an Exception
		if (bal == null) {
			System.out.println("Leave Balance Is Not Found....");
			return;
		}

		// Initalize temp with Leave balance valid for current period only.
		// To This to happen, There should beich only one Entity in Kind Leave
		// Balance whichs valid Till
		// is greater then Current Date, Leave Allocation should ensure this.
		List<EmployeeLeave> employeeLeave = new ArrayList<EmployeeLeave>();

		for (LeaveBalance balance : bal) {
			if (balance.getValidtill().after(new Date())) {
				temp = balance;
				break;
			}

		}

		// If for some reason temp is null return , accurate place to through an
		// Exception
		if (temp == null) {
			System.out.println("Leave balance null while assigning...");
			return;
		}

		Key<EmployeeInfo> employeeInfoKey = temp.getEmpInfokey();
		ArrayList<AllocatedLeaves> allLeave = temp.getAllocatedLeaves();

		for (TimeReportEntrey rep : entrey) {

			String levaeType = rep.projectName;
			levaeType = levaeType.replace("Leave :", "");
			Double hrs = rep.hoursWorked;
			System.out.println("Name Of Leave Type IN TIME REPORT " + levaeType);

			for (AllocatedLeaves altemp : allLeave) {

				System.out.println("Name Of Leave Type IN Leave Balance "+ altemp.getName());
				
				if (altemp.getName().equals(levaeType)) {
					System.out.println("Name Of Leave Type" + levaeType);
					// Only Reduce The Balance when it is zero
					double balHrs = altemp.getBalance() * temp.getWorkingHrs();
					System.out.println("Balance Hrs" + balHrs);
					System.out.println("Asked Hrs" + hrs);
					double approvedDay = hrs / temp.getWorkingHrs();
					System.out.println("Approved Days" + approvedDay);
					if (balHrs > 0) {
						
						altemp.setAvailedDays(altemp.getAvailedDays()+ approvedDay);
						altemp.setBalance(altemp.getBalance() - approvedDay);

						EmployeeLeave empLeave = new EmployeeLeave();
						empLeave.setCompanyId(getCompanyId());
						empLeave.setEmployeeKey(employeeInfoKey);
						empLeave.setLeaveType(altemp);
						empLeave.setLeaveDate(rep.date);
						empLeave.setLeaveHrs(rep.hoursWorked);
						/**
						 * Date : 14-05-2018 By ANIL
						 */
						empLeave.setEmplId(this.getEmpid());
						employeeLeave.add(empLeave);
						logger.log(Level.SEVERE ,"employeeLeave created for employee: "+empLeave.getEmplId()+" leave date:"+empLeave.getLeaveDate());
					}

				}

			}

		}
		// Save the Leave Balance entity

		ofy().save().entity(temp).now();

		if (employeeLeave.size() != 0) {
			ofy().save().entities(employeeLeave).now();
		}
	}

	
}
