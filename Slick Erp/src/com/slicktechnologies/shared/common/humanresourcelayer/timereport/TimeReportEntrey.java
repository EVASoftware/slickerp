package com.slicktechnologies.shared.common.humanresourcelayer.timereport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.slicktechnologies.shared.common.humanresourcelayer.Days;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.DateShift;


// TODO: Auto-generated Javadoc
/**
 * Reperesents an Entry in TimeReport.Contains information about work done on a Project
 * To Do : Date will get removed when Project Level Tracking will start.
 */
@Embed

public class TimeReportEntrey implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8627614452380930290L;
	

	/** Project Corresponding to this Entry */
	@Index
	protected String projectName;
	/** Task  Corresponding to this Entry */
	@Index
	protected String taskName;
	/** Date of this Entry**/
	@Index
	protected Date date;
	/** Number of hours worked **/
    double hoursWorked;
    
    boolean employeeOnLeave,companyHoliday,overtime;
  
    /** Key of {@link DateShift}. This attribute will be used when we will provide
     * give project level tracking.
     */
    Key<DateShift>dateShiftKey;
    
	/**
     * Date : 23-05-2018 By ANIL
	 */
	boolean isLateMark,isEarlyMark;
	String inTime,outTime;
	
	
	/**
	 * Date : 01-06-2018 BY ANIL
	 * if it is OT then we have to store the project name against the Selected OT
	 * for Sasha ERP
	 */
	
	@Index
	String projNameForOt;
	
	
	
	/**
	 * Instantiates a new work project.
	 */
	public TimeReportEntrey() {
		super();
		taskName="";
		employeeOnLeave=false;
		companyHoliday=false;
		
		
		isLateMark=isEarlyMark=false;
		inTime="";
		outTime="";
		
		projNameForOt="";
	}


	
	/**
	 * ********************************************* Getters and Setters***************************************************.
	 *
	 * @return the proj name
	 */
	




	public String getProjNameForOt() {
		return projNameForOt;
	}



	public void setProjNameForOt(String projNameForOt) {
		this.projNameForOt = projNameForOt;
	}



	public boolean isLateMark() {
		return isLateMark;
	}



	public void setLateMark(boolean isLateMark) {
		this.isLateMark = isLateMark;
	}





	public boolean isEarlyMark() {
		return isEarlyMark;
	}





	public void setEarlyMark(boolean isEarlyMark) {
		this.isEarlyMark = isEarlyMark;
	}





	public String getInTime() {
		return inTime;
	}





	public void setInTime(String inTime) {
		this.inTime = inTime;
	}





	public String getOutTime() {
		return outTime;
	}





	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}
	
	
	public String getProjName() {
		return taskName;
	}

	/**
	 * Sets the proj name.
	 *
	 * @param projName the new proj name
	 */
	public void setProjName(String projName) {
		if(projName!=null)
		this.taskName = projName.trim();
	}

	/**
	 * Gets the projhours.
	 *
	 * @return the projhours
	 */
	public double getProjhours() {
		return this.hoursWorked;
	}

	/**
	 * Sets the projhours.
	 *
	 * @param projhours the new projhours
	 */
	public void setProjhours(Double projhours) {
		this.hoursWorked=projhours;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public double getHoursWorked() {
		return hoursWorked;
	}



	public void setHoursWorked(double hoursWorked) {
		this.hoursWorked = hoursWorked;
	}



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public String getTaskName() {
		return taskName;
	}



	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public boolean isEmployeeOnLeave() {
		return employeeOnLeave;
	}



	public void setEmployeeOnLeave(boolean employeeOnLeave) {
		this.employeeOnLeave = employeeOnLeave;
	}



	public boolean isCompanyHoliday() {
		return companyHoliday;
	}



	public void setCompanyHoliday(boolean companyHoliday) {
		this.companyHoliday = companyHoliday;
	}
	
	

	public boolean isOvertime() {
		return overtime;
	}
	public void setOvertime(boolean overtime) {
		this.overtime = overtime;
	}



	/***************************************** To String method *********************************************************/

	@Override
	public String toString() {
		System.out.println(":: TIME REPORT ENTERY ::");
		return "TimeReportEntrey [projectName=" + projectName + ", taskName="
				+ taskName + ", date=" + date + ", hoursWorked=" + hoursWorked
				+ ", employeeOnLeave=" + employeeOnLeave + ", companyHoliday="
				+ companyHoliday + ", dateShiftKey=" + dateShiftKey + "]";
	}

	
}
