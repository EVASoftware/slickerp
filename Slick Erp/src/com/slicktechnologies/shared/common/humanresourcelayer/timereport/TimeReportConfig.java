package com.slicktechnologies.shared.common.humanresourcelayer.timereport;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;

@Entity
@Embed
public class TimeReportConfig extends SuperModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9043787816550627920L;
	public String name;
	public int periodEndDay;
	public String description;
    public Calendar calendar;
    public Calendar calendarName;
    public String period;
    public int noDays;
    @Index
    public String status;
    
    public static final String CREATED="Created";
    public static final String ACTIVE="Active";
    
    
    
    
    public Key<Calendar>keyCalendar;
	
	

	public TimeReportConfig() {
		super();
		status=ACTIVE;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		TimeReportConfig entity=(TimeReportConfig) m;
		String name = entity.getName();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=this.name;
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(entity.id==null)
		{
		
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else
		{
			if(entity.id.equals(id))
				return false;	
			if(name.equals(curname)==true)
				return true;
		}
	
	return false;
	}


@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}


@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Period End Day")
	public int getPeriodStartDay() {
		return periodEndDay;
	}



	public void setPeriodStartDay(int periodStartDay) {
		this.periodEndDay = periodStartDay;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getNoDays() {
		return noDays;
	}



	public void setNoDays(int calendar) {
		this.noDays=noDays;
	}
	
	public Calendar getCalendar() {
		return calendar;
	}



	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Calender Name")
	public String getCalendarName()
	{
		if(this.calendar!=null)
			return this.calendar.getCalName();
		else
			return "";
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Period")
	public String getPeriod()
	{
		return this.period;
	}
	
	public void setCalendarName(Calendar calendarName) {
		this.calendarName = calendarName;
	}



	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public void setPeriod(String period)
	{
	   if(period!=null)
	   {
		   this.period=period.trim();
	   }
	}
	
	@GwtIncompatible
	@OnSave
	public void onSave()
	{
		if(this.calendar!=null)
		{
			keyCalendar=Key.create(Calendar.class,calendar.getId());
		}
	}

	@GwtIncompatible
	@OnLoad
	public void onLoad()
	{
		if(keyCalendar!=null)
		{

		}
	}


	public static void makePeriodListBoxLive(
			ObjectListBox<TimeReportConfig> timeReportPeriod) {
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(TimeReportConfig.ACTIVE);
		querry.setQuerryObject(new TimeReportConfig());
		timeReportPeriod.MakeLive(querry);
		
	}
	
	@Override
	public String toString()
	{
		return this.name;
	}
}
