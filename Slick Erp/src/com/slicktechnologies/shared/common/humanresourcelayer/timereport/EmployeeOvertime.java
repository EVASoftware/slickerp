package com.slicktechnologies.shared.common.humanresourcelayer.timereport;

import com.simplesoftwares.client.library.appstructure.SuperModel;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

@Entity
public class EmployeeOvertime extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3746905966994321526L;


	@Index
	protected Date otDate;
	
	
	protected Overtime overtime;

	@Index
	protected Key<EmployeeInfo>employeeKey;
	
	
	protected EmployeeInfo employee;
	
	protected double otHrs;
	
	
	/**
	 * Date : 14-05-2018 BY ANIL
	 * we were updating leave status against employee key,now storing against empId
	 */
	@Index
	protected int emplId;
	
	/**
	 * @author Anil, Date : 17-07-2019
	 * setting project name for calculating project wise leave and ot
	 */
	@Index
	String projectName;
	
	String holidayLabel;
	
	/**
	 * @author Anil
	 * @since 01-12-2020
	 * Adding sitelocation for showing annexure details site location wise on Invoice pdf
	 * raised by Rahul Tiwari for Sunrise
	 */
	
	@Index
	String siteLocation;
	
	/**
	 * @author Vijay Date :- 18-12-2020
	 * Des :- added Employee attendence Designation to calculate OT as per the attendence designation 
	 */
	String empDesignation;
	
	public EmployeeOvertime() {
		super();
		empDesignation ="";
	}
	
	
	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public int getEmplId() {
		return emplId;
	}



	public void setEmplId(int emplId) {
		this.emplId = emplId;
	}
	
	
	@GwtIncompatible
	@OnSave
	public void onSave() {
		if (employee != null && id == null) {
			employeeKey = Key.create(employee);

		}

	}

	@GwtIncompatible
	@OnLoad
	public void onLoad() {
		if (employeeKey != null) {
			EmployeeInfo info = ofy().load().key(employeeKey).now();
		}
	}
	
	
	
	
	
	public Date getOtDate() {
		return otDate;
	}

	public void setOtDate(Date otDate) {
		this.otDate = otDate;
	}

	public Overtime getOvertime() {
		return overtime;
	}

	public void setOvertime(Overtime overtime) {
		this.overtime = overtime;
	}

	public Key<EmployeeInfo> getEmployeeKey() {
		return employeeKey;
	}

	public void setEmployeeKey(Key<EmployeeInfo> employeeKey) {
		this.employeeKey = employeeKey;
	}

	public EmployeeInfo getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeInfo employee) {
		this.employee = employee;
	}

	public double getOtHrs() {
		return otHrs;
	}

	public void setOtHrs(double otHrs) {
		this.otHrs = otHrs;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


	public String getHolidayLabel() {
		return holidayLabel;
	}


	public void setHolidayLabel(String holidayLabel) {
		this.holidayLabel = holidayLabel;
	}
	
	public String getSiteLocation() {
		return siteLocation;
	}



	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}


	public String getEmpDesignation() {
		return empDesignation;
	}


	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	
	
}
