package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

// TODO: Auto-generated Javadoc
/**
 * The Class Days represents timereport of employee on each day
 */
@Embed
public class Days{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2658658383390599676L;

	/**  week day name. */
	protected String dayName;
	
	/** represents date of week day */
	protected Date date;

	public Days() {
		super();
		dayName="";
		date=new Date();
	}

	/***************************************Getters and Setters****************************************************/
	
	public String getDayName() {
		return dayName;
	}

	public void setDayName(String dayName) {
		if(dayName!=null){
		this.dayName = dayName.trim();
	}
	}
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
