package com.slicktechnologies.shared.common.humanresourcelayer;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;

@Entity
@Embed
public class HrProjectOvertime extends Overtime{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2921701235917476982L;
	
	
	@Index
	int projectId;
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}



	public int getProjectId() {
		return projectId;
	}



	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	
	
	
	
	

}
