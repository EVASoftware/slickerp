package com.slicktechnologies.shared.common.humanresourcelayer;

import com.googlecode.objectify.annotation.Entity;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;


// TODO: Auto-generated Javadoc
/**
 *  Class Grade represents employee grades
 */

@Entity
public class Grade extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6273614928119414029L;
	
	/**  grade name. */
	protected String gradeName;
	
	/** shortname of grade. */
	protected String gradeCode;
	
	/**  description of grade */
	protected String description;
	
	/**  status of grade */
	protected boolean status;
	
	
	public Grade() {
		super();
		gradeName="";
		gradeCode="";
		description="";
		
		
	}

	/***************************************Getters and Setters****************************************************/
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Grade Name")
	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		if(gradeName!=null){
		this.gradeName = gradeName.trim();}
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Grade Code")
	public String getGradeCode() {
		return gradeCode;
	}

	public void setGradeCode(String gradeCode) {
		if(gradeCode!=null){
		this.gradeCode = gradeCode.trim();}
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
		this.description = description.trim();}
	}

	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}


	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof Grade)
		{
			Grade grade=(Grade) arg0;
			if(grade.getGradeName()!=null)
			{
				return this.gradeName.compareTo(grade.gradeName);
			}
		}
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		
		return isDuplicateName(m)||isDuplicateCodeName(m);
		
		
	}
	
	public boolean isDuplicateName(SuperModel m) {
		Grade entity = (Grade) m;
		String name = entity.getGradeName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.gradeName.trim();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
}
	
	public boolean isDuplicateCodeName(SuperModel m) {
		Grade entity = (Grade) m;
		String name = entity.getGradeCode().trim();
		String curname=this.gradeCode.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	

		
		
	
	}

	@Override
	public String toString()
	{
		return gradeName.toString();
	}
		


}
