package com.slicktechnologies.shared.common.humanresourcelayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Super Class  of processes concerning  Human Resources.
 */
@Embed
public class HRProcess extends SuperModel implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7278714669446453674L;
	
	/**  Start Date Of Human Resourse Process. */
	@Index
	protected Date fromdate;
	
	/**  End date of Human Resource Process. */
	@Index
	protected Date todate;
	
	/**  Country where Human Resource processes is being originated */
	protected String country;
	
	
	
	/** ******************Applicability Attributes**************************************. */ 
	
	

	/** Branch where This process is applicable ,if value is All then this H.R process is 
	 * Applicable for all Branches */
	@Index
	protected String branch;
	
	/**  Department for which this Human Resource Process is applicable.  
	 * if value is All then this H.R process is 
	 * Applicable for all department*/
	@Index
	protected String department;
	
	
	
	
	/**  The creation date of human resource process. */
	@Index
	protected Date creationDate;
	
	/**  The status of human resource process. */
	@Index
	protected String status;
	
	
	

	/**
	 * Instantiates a new HR process.
	 */
	public HRProcess() {
		super();
		country="";
		branch="";
		department="";
		fromdate=new Date();
		todate=new Date();
		creationDate=new Date();
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	/**
	 * *************************************Getters and Setters***************************************************.
	 *
	 * @return the fromdate
	 */
	
	
	public Date getFromdate() {
		return fromdate;
	}

	/**
	 * Sets the fromdate.
	 *
	 * @param fromdate the new fromdate
	 */
	public void setFromdate(Date fromdate) {
		this.fromdate = fromdate;
	}

	/**
	 * Gets the todate.
	 *
	 * @return the todate
	 */
	public Date getTodate() {
		return todate;
	}

	/**
	 * Sets the todate.
	 *
	 * @param todate the new todate
	 */
	public void setTodate(Date todate) {
		this.todate = todate;
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Branch")
	public String getBranch() {
		return branch;
	}

	/**
	 * Sets the branch.
	 *
	 * @param branch the new branch
	 */
	public void setBranch(String branch) {
		if(branch!=null){
		this.branch = branch.trim();}
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Department")
	public String getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(String department) {
		if(department!=null){
		this.department = department.trim();}
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = true, title = "Status")
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		if(status!=null)
		this.status = status.trim();
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		if(country!=null){
		this.country = country.trim();}
	}
	
	

}
