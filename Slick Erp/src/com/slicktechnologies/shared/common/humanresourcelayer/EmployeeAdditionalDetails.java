package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;


@Entity
public class EmployeeAdditionalDetails extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4861612027110037312L;


	@Index
	protected EmployeeInfo empInfo;
	
	@Index
	private ArrayList<EmployeeAddressDetails> empAddDetList;
	@Index
	private ArrayList<EmployeeFamilyDeatails> empFamDetList;
	
	DocumentUpload documentUpload;
	
	public EmployeeAdditionalDetails() {
		// TODO Auto-generated constructor stub
		documentUpload=new DocumentUpload();
	}
	
	

	
	public DocumentUpload getDocumentUpload() {
		return documentUpload;
	}




	public void setDocumentUpload(DocumentUpload documentUpload) {
		this.documentUpload = documentUpload;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public List<EmployeeAddressDetails> getEmpAddDetList() {
		return empAddDetList;
	}


	public void setEmpAddDetList(List<EmployeeAddressDetails> empAddDetList) {
		ArrayList<EmployeeAddressDetails> arrBillItems=new ArrayList<EmployeeAddressDetails>();
		arrBillItems.addAll(empAddDetList);
		this.empAddDetList = arrBillItems;
	}


	public List<EmployeeFamilyDeatails> getEmpFamDetList() {
		return empFamDetList;
	}


	public void setEmpFamDetList(List<EmployeeFamilyDeatails> empFamDetList) {
		ArrayList<EmployeeFamilyDeatails> arrBillItems=new ArrayList<EmployeeFamilyDeatails>();
		arrBillItems.addAll(empFamDetList);
		this.empFamDetList = arrBillItems;
	}


	public EmployeeInfo getEmpInfo() {
		return empInfo;
	}


	public void setEmpInfo(EmployeeInfo empInfo) {
		this.empInfo = empInfo;
	}
	
	
	

}
