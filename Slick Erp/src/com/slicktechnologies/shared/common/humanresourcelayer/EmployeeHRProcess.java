package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.LoneEmi;

import static com.googlecode.objectify.ObjectifyService.ofy;



/**
 * Repersents HR Processes concerning Employees , Some Concrete examples are
 * TimeReport,Advance etc. 
 */

public class EmployeeHRProcess extends HRProcess {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6171323790326370151L;
	public static final String CREATED="Created",CANCELLED="Cancelled",REJECTED="Rejected",APPROVED="Approved",REQUESTED="Requested";
	
	/**  The Name Of the Approver who will approve the Human Resource Process. */
	@Index
	protected String approverName;
	
	/**   The Date Of Approval when the Human Resource Process will be approved. */
	@Index
	protected Date approvalDate;
	
	
	/** The employee id. */
	@Index
	protected int empid;
	
	/** The employee name. */
	@Index
	protected String employeeName;
	
	/** The employee type. */
	@Index
	protected String employeeType;
	
	
	
	@Index
	protected String employeedDesignation;
	
	@Index
	protected String employeeRole;
	
	
	
	@Index
	protected long empCellNo;
	
	@Index
	protected Key<EmployeeInfo>employeeInfoKey;
	
	
	
	/**
	 * Instantiates a new employee hr process.
	 */
	public EmployeeHRProcess() {
		super();
		approverName="";
		employeeName="";
		employeeType="";
		
		employeedDesignation="";
		employeeRole="";
	
		approvalDate=new Date();
		employeeRole="";
	}
	
	
	public String getEmployeedDesignation() {
		return employeedDesignation;
	}


	public void setEmployeedDesignation(String employeedDesignation) {
		this.employeedDesignation = employeedDesignation;
	}


	/**
	 * *************************************Getters and Setters***************************************************.
	 *
	 * @return the approver name
	 */
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Cell No")
	public Long getEmpCellNo() {
		return empCellNo;
	}

	public void setEmpCellNo(Long empCellNo) {
		this.empCellNo = empCellNo;
	}

	
	@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXEMPLOYEE,  flexFormNumber = "11", title = "Approver Name", variableName = "approverName",colspan=0)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Approver Name")
	public String getApproverName() {
		return approverName;
	}
	
	/**
	 * Sets the approver name.
	 *
	 * @param approverName the new approver name
	 */
	public void setApproverName(String approverName) {
		if(approverName!=null){
		this.approverName = approverName.trim();}
	}
	
	/**
	 * Gets the approval date.
	 *
	 * @return the approval date
	 */
	public Date getApprovalDate() {
		return approvalDate;
	}
	
	/**
	 * Sets the approval date.
	 *
	 * @param approvalDate the new approval date
	 */
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	

	/**
	 * Gets the employee id.
	 *
	 * @return the employee id
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "00", title = "Employee ID", variableName = "empid",colspan=0)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Employee ID")
	public int getEmpid() {
		return empid;
	}


	public void setEmpid(int empid) {
		this.empid = empid;
	}

	/**
	 * Sets the employee id.
	 *
	 * @param employeeId the new employee id
	 */
	
	
	/**
	 * Gets the employee name.
	 *
	 * @return the employee name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "01", title = "Employee Name", variableName = "employeeName",colspan=0)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Employee Name")
	public String getEmployeeName() {
		
		return employeeName;
	}
	
	

	/**
	 * Sets the employee name.
	 *
	 * @param employeeName the new employee name
	 */
	public void setEmployeeName(String employeeName) {
		if(employeeName!=null){
		this.employeeName = employeeName.trim();}
	}
	
	/**
	 * Gets the employee type.
	 *
	 * @return the employee type
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Employee Type")
	public String getEmployeeType() {
		return employeeType;
	}
	
	/**
	 * Sets the employee type.
	 *
	 * @param employeeType the new employee type
	 */
	public void setEmployeeType(String employeeType) {
		if(employeeType!=null){
		this.employeeType = employeeType.trim();}
	}
	
	
	
	
	


	
	

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 7, isFieldUpdater = false, isSortable = true, title = "Employee Role")
	public String getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole) {
		if(employeeRole!=null)
		this.employeeRole = employeeRole.trim();
	}
	
	/**
	 * Why we have to do this ?
	 * patch
	 */
	
	public void reactOnApproval() {
		
		
	}
	public void reactOnRejected()
	{
		
	}
	
	@OnSave
	@GwtIncompatible
	protected void createEmployeeInfoKey()
	{
		/**
		 * @author Anil
		 * @since 08-12-2020
		 * commented below code to avoid concurrent exception at the time payroll processing
		 */
//		if(getId()==null)
//		{
//		this.employeeInfoKey=ofy().load().type(EmployeeInfo.class).filter("companyId",getCompanyId()).
//				filter("empCount",getEmpid()).keys().first().now();
//		}
		
		
	}
	
	
}
