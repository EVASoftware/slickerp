package com.slicktechnologies.shared.common.humanresourcelayer.attendance;

import java.util.Date;

public class DateAttendence
{

	protected Date date;
	protected String status;
	
	public DateAttendence()
	{
		
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
