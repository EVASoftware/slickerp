package com.slicktechnologies.shared.common.humanresourcelayer.attendance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
// TODO: Auto-generated Javadoc

/**
 * Repersents an Attendance of Employee, the data will usually come from punch card
 */
@Entity

public class EmployeeAttendance extends SuperModel
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9171837678067357605L;
	
	/** The status. */
	@Index
	protected String status;
	
	/** The emp id. */
	@Index
	protected int empId;
	
	/** The punch card no. */
	@Index
	protected String punchCardNo;
	
	/** The emp name. */
	@Index
	protected String empName;
	
	/** The date. */
	@Index 
	protected Date date;
	
	/** The emp cell no. */
	@Index 
	protected Long empCellNo;
	
//	/** The in time. */
//	protected double inTime;
//	
//	/** The out time. */
//	protected double outTime;
	
	
	/** The employee info. */
	protected Key<EmployeeInfo>employeeInfo;
	
	/** The present. */
	public static String PRESENT ="P";
	
	/** The absent. */
	public static String ABSENT ="A";
	/**
	 *  nidhi
	 *  12-08-2017
	 */
	/** The in time. */
	@Index 
	protected Date inTime;
	
	/** The out time. */
	@Index 
	protected Date outTime;
	
	/** The out time. */
	@Index 
	protected double totalHours;
	/** for store leave type */
	@Index 
	protected String leaveType;
	
	/** for store report month */
	@Index
	protected String Month;
	
	/** for store work project name */
	@Index 
	protected String projectName;
	
	/** for store over time type */
	@Index
	protected String otType;
	
	/** for maintain report genrated or not */
	@Index
	protected boolean reportStatus;
	
	/** The designation. */
	@Index
	protected String designation;
	
	/** The department. */
	@Index
	protected String department;
	

	/** The employee type. */
	@Index
	protected String employeeType;

	/** The employeerole. */
	@Index
	protected String employeerole;
	
	
	/** The branch. */
	@Index
	protected String branch;
	
	@Index 
	protected String remark;
	
	/**
	 * end
	 */
	
	/**
	 * Instantiates a new employee attendance.
	 */
	public EmployeeAttendance()
	{
		
	}

	
	public Long getEmpCellNo() {
		return empCellNo;
	}


	public void setEmpCellNo(Long empCellNo) {
		this.empCellNo = empCellNo;
	}


	public double getTotalHours() {
		return totalHours;
	}


	public void setTotalHours(double totalHours) {
		this.totalHours = totalHours;
	}


	public String getLeaveType() {
		return leaveType;
	}


	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}


	public String getMonth() {
		return Month;
	}


	public void setMonth(String month) {
		Month = month;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getOtType() {
		return otType;
	}


	public void setOtType(String otType) {
		this.otType = otType;
	}


	public boolean isReportStatus() {
		return reportStatus;
	}


	public void setReportStatus(boolean reportStatus) {
		this.reportStatus = reportStatus;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public String getEmployeeType() {
		return employeeType;
	}


	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}


	public String getEmployeerole() {
		return employeerole;
	}


	public void setEmployeerole(String employeerole) {
		this.employeerole = employeerole;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	


	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * Gets the emp id.
	 *
	 * @return the emp id
	 */
	public int getEmpId() {
		return empId;
	}


	/**
	 * Sets the emp id.
	 *
	 * @param empId the new emp id
	 */
	public void setEmpId(int empId) {
		this.empId = empId;
	}


	/**
	 * Gets the punch card no.
	 *
	 * @return the punch card no
	 */
	public String getPunchCardNo() {
		return punchCardNo;
	}


	/**
	 * Sets the punch card no.
	 *
	 * @param punchCardNo the new punch card no
	 */
	public void setPunchCardNo(String punchCardNo) {
		this.punchCardNo = punchCardNo;
	}


	/**
	 * Gets the emp name.
	 *
	 * @return the emp name
	 */
	public String getEmpName() {
		return empName;
	}


	/**
	 * Sets the emp name.
	 *
	 * @param empName the new emp name
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}


	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}


	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}


	


	/**
	 * Gets the employee info.
	 *
	 * @return the employee info
	 */
	public Key<EmployeeInfo> getEmployeeInfo() {
		return employeeInfo;
	}


	/**
	 * Sets the employee info.
	 *
	 * @param employeeInfo the new employee info
	 */
	public void setEmployeeInfo(Key<EmployeeInfo> employeeInfo) {
		this.employeeInfo = employeeInfo;
	}
	
	

  /**
   * Gets the cell number.
   *
   * @return the cell number
   */
  public Long getCellNumber() {
		return empCellNo;
	}


	/**
	 * Sets the cell number.
	 *
	 * @param cellNumber the new cell number
	 */
	public void setCellNumber(long cellNumber) {
		empCellNo = cellNumber;
	}


/**
 * On load.
 */
@GwtIncompatible
	@OnLoad
	public void onLoad()
	{
		if(employeeInfo!=null)
		{
			EmployeeInfo empName=ofy().load().key(employeeInfo).now();
			if(empName!=null)
			{
				if(empName.equals("")==false)
				{
					setEmpName(empName.getFullName());
				}
			}
		}
	}
	
	
  /**
   * On save.
   */
  @GwtIncompatible
	@OnSave
	public void onSave()
	{
		if(empName!=null)
		{
			employeeInfo=ofy().load().type(EmployeeInfo.class).filter("fullName",empName).keys().first().now();
		}
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) 
	{
		return false;
	}
	
	

	/**
	 * Gets the in time.
	 *
	 * @return the in time
	 */
	public Date getInTime() {
		return inTime;
	}

	/**
	 * Sets the in time.
	 *
	 * @param inTime the new in time
	 */
	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	/**
	 * Gets the out time.
	 *
	 * @return the out time
	 */
	public Date getOutTime() {
		return outTime;
	}

	/**
	 * Sets the out time.
	 *
	 * @param outTime the new out time
	 */
	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	/**
	 * Check weather the attendance is corresponding to a valid employee or Not.
	 *
	 * @return true, if is valid employee
	 */
	@GwtIncompatible
	public boolean isValidEmployee()
	{
		int count=0;
		if(companyId!=null)
		{
			count=ofy().load().type(EmployeeInfo.class).filter("empCount",count).filter("companyId",companyId).count();
		}
		else
		{
			count=ofy().load().type(EmployeeInfo.class).filter("empCount",count).count();
		}
		
		if(count==0)
			return false;
		else
			return true;
	}
	
	
	/**
	 * Checks if is valid attendance.
	 *
	 * @return true, if is valid attendance
	 */
	@GwtIncompatible
	public boolean isValidAttendance()
	{
		int count=0;
		boolean attendance=true;
		if(companyId!=null)
		{
			count=ofy().load().type(EmployeeAttendance.class).filter("empId",empId)
					.filter("date",date).filter("companyId",companyId).count();
		}
		else
		{
			count=ofy().load().type(EmployeeAttendance.class).filter("empId",empId)
					.filter("date",date).count();
		}
		if(count!=0)
			return false;
		
		if(companyId!=null)
		{
			count=ofy().load().type(EmployeeInfo.class).filter("empCount",this.empId)
					.filter("companyId",companyId).count();
		}
		else
		{
			count=ofy().load().type(EmployeeInfo.class).filter("empCount",this.empId).count();
		}
		if(count==0)
			return false;
		EmployeeInfo info=null;
		if(companyId!=null)
		{
			info=ofy().load().type(EmployeeInfo.class).filter("empCount",this.empId)
					.filter("companyId",companyId).first().now();
		}
		else
		{
			info=ofy().load().type(EmployeeInfo.class).filter("empCount",this.empId).first().now();
		}
		
		if(info!=null&&info.getFullName().equals(this.empName.trim()))
			return true;
		else
			return false;
		
	}
	
	/**
	 * Save employee.
	 */
	@GwtIncompatible
	@OnSave
	public void saveEmployee()
	{
		EmployeeInfo info=null;
		if(companyId!=null)
		{
			info=ofy().load().type(EmployeeInfo.class).filter("empCount",this.empId)
					.filter("companyId",companyId).first().now();
		}
		else
		{
			info=ofy().load().type(EmployeeInfo.class).filter("empCount",this.empId).first().now();
		}
		
		if(info!=null)
			this.empCellNo=info.getCellNumber();
	}
	
}
