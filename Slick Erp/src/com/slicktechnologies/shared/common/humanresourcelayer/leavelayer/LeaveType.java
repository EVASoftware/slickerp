package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Represents a Leave Type contains all Rules applicable to a Leave Type.
 */

@Entity
public class LeaveType extends SuperModel
{

	/****************************************Attributes************************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1719383047766829038L;
	

	/** Short Name of Leave */
	protected String shortName;
	
	/** Name of Leave */
	protected String name;
	
	/**  Rule weather week end will be counted or not */
	protected boolean countWeekend;
	
	/**  Rule weather Holidays will be counted or not */
	protected boolean countHoliday;
	
	/** Rule weather Cancellation is allowed for now or not*/
	protected boolean cancellationAllow;
	
	/**  Minimum days for which leave can be applied or not */
	protected int minDays;
	
	/** Maximum days for which leave can be applied or not */
	protected int maxDays;
	
	/**  Is leave is a paid Leave*/
	@Index
	protected boolean paidLeave;
	
	/**  document required For Leave Submission */
	protected boolean documentRequired;
	
	/** The doument required in minimum days. */
	protected int doumentRequiredMinDays;
	
	
	/**  Allow Leave to be carry Forward when Calendar Ends */
	protected boolean relapseAllowCarryOver;
	
	/** Maximum Carry Forward applicable when Leave Relapses*/
	protected int maxCarryForward;
	
	/**  Whether Leave is incashable or Not */
	protected boolean leaveEncashable;
	
	/** Status of Leave */
	@Index
	protected boolean leaveStatus;
	
	/////////////////////////////// 27-08-2015
	
	
	@Index
	protected boolean monthlyAllocation;
	
	protected double monthlyAllocationLeave;
	
	
	/**
	 * Date : 29-05-2018 By ANIL
	 * For -Sasha ERP
	 */
	@Index
	boolean isCompensatoryLeave;
	int no_of_days_to_avail;
	
	/**
	 * End
	 */
	
	/****************************************Attributes Ends******************************************************************/
	
	/****************************************Constructor******************************************************************/
	
	/**
	 * Instantiates a new leave type.
	 */
	public LeaveType() {
		super();
		name = "";
		shortName = "";
	}
	
	/****************************************Constructor Ends******************************************************************/
	
	
	
	
	public boolean isCompensatoryLeave() {
		return isCompensatoryLeave;
	}

	public void setCompensatoryLeave(boolean isCompensatoryLeave) {
		this.isCompensatoryLeave = isCompensatoryLeave;
	}

	public int getNo_of_days_to_avail() {
		return no_of_days_to_avail;
	}

	public void setNo_of_days_to_avail(int no_of_days_to_avail) {
		this.no_of_days_to_avail = no_of_days_to_avail;
	}

/**
 * ****************************************Getters and Setters**************************************************************.
 *
 * @return the short name
 */
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Short Name")
	public String getShortName() {
		return shortName;
	}
	
	/**
	 * Sets the short name.
	 *
	 * @param shortName the new short name
	 */
	public void setShortName(String shortName) {
		if(shortName!=null)
		this.shortName = shortName.trim();
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		if(name!=null)
		this.name = name.trim();
	}
	
	/**
	 * Gets the count weekend.
	 *
	 * @return the count weekend
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Count Weekend")
	public Boolean getCountWeekend() {
		return countWeekend;
	}
	
	/**
	 * Sets the count weekend.
	 *
	 * @param countWeekend the new count weekend
	 */
	public void setCountWeekend(Boolean countWeekend) {
		if(countWeekend!=null)
		this.countWeekend = countWeekend;
	}
	
	/**
	 * Gets the count holiday.
	 *
	 * @return the count holiday
	 */
	public Boolean getCountHoliday() {
		return countHoliday;
	}
	
	/**
	 * Sets the count holiday.
	 *
	 * @param countHoliday the new count holiday
	 */
	public void setCountHoliday(Boolean countHoliday) {
		if(countHoliday!=null)
		this.countHoliday = countHoliday;
	}
	
	/**
	 * Gets the cancellation allow.
	 *
	 * @return the cancellation allow
	 */
	public Boolean getCancellationAllow() {
		return cancellationAllow;
	}
	
	/**
	 * Sets the cancellation allow.
	 *
	 * @param cancellationAllow the new cancellation allow
	 */
	public void setCancellationAllow(Boolean cancellationAllow) {
		if(cancellationAllow!=null)
		this.cancellationAllow = cancellationAllow;
	}
	
	/**
	 * Gets the min days.
	 *
	 * @return the min days
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 7, isFieldUpdater = false, isSortable = true, title = "Min Days")
	public Integer getMinDays() {
		return minDays;
	}
	
	/**
	 * Sets the min days.
	 *
	 * @param minDays the new min days
	 */
	public void setMinDays(Integer minDays) {
		this.minDays = minDays;
	}
	
	/**
	 * Gets the max days.
	 *
	 * @return the max days
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Max Days")
	public Integer getMaxDays() {
		return maxDays;
	}
	
	/**
	 * Sets the max days.
	 *
	 * @param maxDays the new max days
	 */
	public void setMaxDays(Integer maxDays) {
		this.maxDays = maxDays;
	}
	
	
	/**
	 * Gets the unpaid leave.
	 *
	 * @return the unpaid leave
	 */
	public Boolean getPaidLeave() {
		return paidLeave;
	}

	/**
	 * Sets the unpaid leave.
	 *
	 * @param unpaidLeave the new unpaid leave
	 */
	public void setUnpaidLeave(Boolean unpaidLeave) {
		this.paidLeave = unpaidLeave;
	}

	/**
	 * Gets the document required.
	 *
	 * @return the document required
	 */
	
	public Boolean getDocumentRequired() {
		return documentRequired;
	}
	
	/**
	 * Sets the document required.
	 *
	 * @param documentRequired the new document required
	 */
	public void setDocumentRequired(Boolean documentRequired) {
		if(documentRequired!=null)
		this.documentRequired = documentRequired;
	}
	
	/**
	 * Gets the doument required min days.
	 *
	 * @return the doument required min days
	 */
	public Integer getDoumentRequiredMinDays() {
		return doumentRequiredMinDays;
	}
	
	/**
	 * Sets the doument required min days.
	 *
	 * @param doumentRequiredMinDays the new doument required min days
	 */
	public void setDoumentRequiredMinDays(Integer doumentRequiredMinDays) {
		this.doumentRequiredMinDays = doumentRequiredMinDays;
	}
	
	
	
	/**
	 * Gets the relapse allow carry over.
	 *
	 * @return the relapse allow carry over
	 */
	public Boolean getRelapseAllowCarryOver() {
		return relapseAllowCarryOver;
	}
	
	/**
	 * Sets the relapse allow carry over.
	 *
	 * @param relapseAllowCarryOver the new relapse allow carry over
	 */
	public void setRelapseAllowCarryOver(Boolean relapseAllowCarryOver) {
		if(relapseAllowCarryOver!=null)
		this.relapseAllowCarryOver = relapseAllowCarryOver;
	}
	
	
	
	/**
	 * Gets the max carry forward.
	 *
	 * @return the max carry forward
	 */
	public Integer getMaxCarryForward() {
		return maxCarryForward;
	}

	/**
	 * Sets the max carry forward.
	 *
	 * @param maxCarryForward the new max carry forward
	 */
	public void setMaxCarryForward(Integer maxCarryForward) {
		this.maxCarryForward = maxCarryForward;
	}

		/**
		 * Gets the leave encashable.
		 *
		 * @return the leave encashable
		 */
		public Boolean getLeaveEncashable() {
			return leaveEncashable;
		}

		/**
		 * Sets the leave encashable.
		 *
		 * @param leaveEncashable the new leave encashable
		 */
		public void setLeaveEncashable(Boolean leaveEncashable) {
			if(leaveEncashable!=null)
			this.leaveEncashable = leaveEncashable;
		}

		
		
		
		
		
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof LeaveType){
		LeaveType leavetype=(LeaveType) arg0;
		if(leavetype.getName()!=null)
		{
			return this.name.compareTo(leavetype.name);
		}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		
		return isDuplicateShortName(m)||isDuplicateName(m);
		
		}
	
	
	
	/**
	 * Checks if is duplicate short name.
	 *
	 * @param m the m
	 * @return true, if is duplicate short name
	 */
	public boolean isDuplicateShortName(SuperModel m) {
		
		LeaveType entity = (LeaveType) m;
		String name = entity.getShortName().trim();
		String curname=this.shortName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		

		
		
	}
	
	
	/**
	 * Checks if is duplicate name.
	 *
	 * @param m the m
	 * @return true, if is duplicate name
	 */
	public boolean isDuplicateName(SuperModel m) {
		
		LeaveType entity = (LeaveType) m;
		String name = entity.getName().trim();
		String curname=this.name.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		

		
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return name.toString();
	}

	/**
	 * Sets the status.
	 *
	 * @param value the new status
	 */
	public void setStatus(Boolean value) {
		if(value!=null)
		leaveStatus=value;
		
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Boolean getStatus()
	{
		return leaveStatus;
	}

	
	
	
	public Boolean getMonthlyAllocation() {
		return monthlyAllocation;
	}

	public void setMonthlyAllocation(Boolean monthlyAllocation) {
		this.monthlyAllocation = monthlyAllocation;
	}

	public Double getMonthlyAllocationLeave() {
		return monthlyAllocationLeave;
	}

	public void setMonthlyAllocationLeave(Double monthlyAllocationLeave) {
		this.monthlyAllocationLeave = monthlyAllocationLeave;
	}

	
	
	
	
}
