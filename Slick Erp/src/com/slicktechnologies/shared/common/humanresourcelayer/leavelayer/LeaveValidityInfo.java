package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class LeaveValidityInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4149836085032864188L;
	@Index
	String leaveName;
	@Index
	Date validUntil;
	
	double leaveCount;
	
	public LeaveValidityInfo() {
		leaveName="";
	}

	public String getLeaveName() {
		return leaveName;
	}

	public void setLeaveName(String leaveName) {
		this.leaveName = leaveName;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public double getLeaveCount() {
		return leaveCount;
	}

	public void setLeaveCount(double leaveCount) {
		this.leaveCount = leaveCount;
	}

	
	

}
