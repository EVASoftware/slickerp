package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;


// TODO: Auto-generated Javadoc
/**
 * Leave Group class represents a group of Leave which will be allocated to Employee.
 */
@Entity

public class LeaveGroup extends SuperModel {

    /***************************************************Attributes************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5399902384350464934L;
	
	/** The group name. */
	String groupName;
	
	/** The allocated leaves. */
	@Serialize
	ArrayList<AllocatedLeaves> allocatedLeaves;
	
	/** The status. */
	boolean status;
	/***************************************************Attributes Ends************************************************/
	
	/***************************************************Constructor ************************************************/
	/**
	 * Instantiates a new leave group.
	 */
	public LeaveGroup() {
		super();
		groupName="";
	   allocatedLeaves=new ArrayList<AllocatedLeaves>();
	}
	
	/***************************************************Constructor Ends************************************************/

	/**
	 * ***********************************Getters and Setters**************************************************.
	 *
	 * @return the group name
	 */
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Group")
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Sets the group name.
	 *
	 * @param groupName the new group name
	 */
	public void setGroupName(String groupName) {
		if(groupName!=null)
		this.groupName = groupName;
	}

	/**
	 * Gets the allocated leaves.
	 *
	 * @return the allocated leaves
	 */
	public ArrayList<AllocatedLeaves> getAllocatedLeaves() {
		return allocatedLeaves;
	}

	/**
	 * Sets the allocated leaves.
	 *
	 * @param allocatedLeaves the new allocated leaves
	 */
	public void setAllocatedLeaves(List<AllocatedLeaves> allocatedLeaves) {
		if(allocatedLeaves!=null)
		{
		
			this.allocatedLeaves=new ArrayList<AllocatedLeaves>();
			this.allocatedLeaves.addAll(allocatedLeaves);
		
		
		}
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Boolean status) {
		if(status!=null)
		this.status = status;
	}

	
	
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		LeaveGroup entity = (LeaveGroup) m;
		String name = entity.getGroupName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.groupName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return groupName.toString();
	}


}


