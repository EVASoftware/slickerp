package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

// TODO: Auto-generated Javadoc
/**
 * Kind which keeps the record of Leave Balance of Employees.
 * To Do : Create Logic such that a Leave Balance is active 
 * when when current date is less than validtill otherwise it 
 * will become inactive.
 * 
 */
@Entity
@Embed

public class LeaveBalance extends SuperModel 
{

	/************************************Attributes***************************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1369893653303790656L;
	

	/** Start validity of Leave */
	@Index
	protected Date validfrom;

	/** End validity of Leave */
	@Index
	protected Date validtill;

	/** Employee Info for which leave is applied */
	@Index
	protected EmployeeInfo empInfo;
	
	/** The leave group. */
	@Serialize
	protected LeaveGroup leaveGroup;

	/**
	 * Represents working hrs of Employee as Some How we are doing Leave
	 * Calculation which is manually.
	 */
	
	double workingHrs;
	
	
	/**
	 * Date : 02-06-2018 By ANIL
	 * As Compensatory leaves are valid for 30days only.
	 * we will store  the leave taken and leave valid date here.
	 * For Sasha ERP
	 */
	ArrayList<LeaveValidityInfo> leaveValidityList;

	/************************************ Relational Attribute **********************************************/
	/** Key of EmployeeInfo for which the leave is allocated */
	protected Key<EmployeeInfo> empInfokey;

	/**
	 * Date : 24-12-2018 By ANIL
	 * storing upload remark
	 */
	String remark;
	
	/************************************ Attributes Ends ****************************************************/

	/************************************** Constructor *******************************************************/
	
	/**
	 * Instantiates a new leave balance.
	 */
	
	public LeaveBalance() {
		super();
		
		leaveValidityList=new ArrayList<LeaveValidityInfo>();
		remark="";

	}

	/****************************************** Constructor Ends ***********************************************************/

	public ArrayList<LeaveValidityInfo> getLeaveValidityList() {
		return leaveValidityList;
	}

	public void setLeaveValidityList(List<LeaveValidityInfo> leaveValidityList) {
		ArrayList<LeaveValidityInfo> list=new ArrayList<LeaveValidityInfo>();
		list.addAll(leaveValidityList);
		this.leaveValidityList = list;
	}

	/******************************************************************************************************/

	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	
	@Override
	public int compareTo(SuperModel o) {
		return 0;
	}

	/**
	 * Gets the emp infokey.
	 *
	 * @return the emp infokey
	 */
	public Key<EmployeeInfo> getEmpInfokey() {
		return empInfokey;
	}

	/**
	 * Sets the emp info key.
	 *
	 * @param empInfokey the new emp info key
	 *            
	 */
	public void setEmpInfokey(Key<EmployeeInfo> empInfokey) {
		this.empInfokey = empInfokey;
	}

	/**
	 * Gets the validfrom.
	 *
	 * @return the validfrom
	 */
	public Date getValidfrom() {
		return validfrom;
	}

	/**
	 * Sets the validfrom.
	 *
	 * @param validfrom
	 *            the new validfrom
	 */
	public void setValidfrom(Date validfrom) {
		this.validfrom = validfrom;
	}

	/**
	 * Gets the validtill.
	 *
	 * @return the validtill
	 */
	public Date getValidtill() {
		return validtill;
	}

	/**
	 * Sets the validtill.
	 *
	 * @param validtill
	 *            the new validtill
	 */
	public void setValidtill(Date validtill) {
		this.validtill = validtill;
	}

	/**
	 * Gets the emp info.
	 *
	 * @return the emp info
	 */
	public EmployeeInfo getEmpInfo() {
		return empInfo;
	}

	/**
	 * Sets the emp info.
	 *
	 * @param empInfo
	 *            the new emp info
	 */
	public void setEmpInfo(EmployeeInfo empInfo) {
		this.empInfo = empInfo;
	}

	/**
	 * Gets the leave group.
	 *
	 * @return the leave group
	 */
	public LeaveGroup getLeaveGroup() {
		return leaveGroup;
	}

	/**
	 * Sets the leave group.
	 *
	 * @param leaveGroup
	 *            the new leave group
	 */
	public void setLeaveGroup(LeaveGroup leaveGroup) {
		this.leaveGroup = leaveGroup;
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	public String getBranch() {
		return empInfo.getBranch();
	}

	/**
	 * Sets the branch.
	 *
	 * @param branch
	 *            the new branch
	 */
	public void setBranch(String branch) {
		empInfo.setBranch(branch);
	}

	/**
	 * Gets the cell number.
	 *
	 * @return the cell number
	 */
	public Long getCellNumber() {
		return empInfo.getCellNumber();
	}

	/**
	 * Sets the cell number.
	 *
	 * @param cellNumber
	 *            the new cell number
	 */
	public void setCellNumber(Long cellNumber) {
		empInfo.setCellNumber(cellNumber);
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return empInfo.getFullName();
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName
	 *            the new full name
	 */
	public void setFullName(String fullName) {
		empInfo.setFullName(fullName);
	}

	/**
	 * Gets the emp count.
	 *
	 * @return the emp count
	 */
	public int getEmpCount() {
		return empInfo.getEmpCount();
	}

	/**
	 * Sets the emp count.
	 *
	 * @param count
	 *            the new emp count
	 */
	public void setEmpCount(int count) {
		empInfo.setEmpCount(count);
	}

	/**
	 * Gets the designation.
	 *
	 * @return the designation
	 */
	public String getDesignation() {
		return empInfo.getDesignation();
	}

	/**
	 * Sets the designation.
	 *
	 * @param designation
	 *            the new designation
	 */
	public void setDesignation(String designation) {
		empInfo.setDesignation(designation);
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return empInfo.getDepartment();
	}

	/**
	 * Sets the department.
	 *
	 * @param department
	 *            the new department
	 */
	public void setDepartment(String department) {
		empInfo.setDepartment(department);
	}

	/**
	 * Gets the employeerole.
	 *
	 * @return the employeerole
	 */
	public String getEmployeerole() {
		return empInfo.getEmployeerole();
	}

	/**
	 * Sets the employeerole.
	 *
	 * @param employeerole
	 *            the new employeerole
	 */
	public void setEmployeerole(String employeerole) {
		empInfo.setEmployeerole(employeerole);
	}

	/**
	 * Gets the employee type.
	 *
	 * @return the employee type
	 */
	public String getEmployeeType() {
		return empInfo.getEmployeeType();
	}

	/**
	 * Sets the employee type.
	 *
	 * @param employeeType
	 *            the new employee type
	 */
	public void setEmployeeType(String employeeType) {
		empInfo.setEmployeeType(employeeType);
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return empInfo.getCountry();
	}

	/**
	 * Sets the country.
	 *
	 * @param country
	 *            the new country
	 */
	public void setCountry(String country) {
		empInfo.setCountry(country);
	}

	/**
	 * Gets the calendar name.
	 *
	 * @return the calendar name
	 */
	public String getCalendarName() {
		return empInfo.getCalendarName();
	}

	/**
	 * Sets the calendar name.
	 *
	 * @param calendarName
	 *            the new calendar name
	 */
	public void setCalendarName(String calendarName) {
		empInfo.setCalendarName(calendarName);
	}

	/**
	 * Gets the group name.
	 *
	 * @return the group name
	 */
	public String getGroupName() {
		return leaveGroup.getGroupName();
	}

	/**
	 * Sets the group name.
	 *
	 * @param groupName
	 *            the new group name
	 */
	public void setGroupName(String groupName) {
		leaveGroup.setGroupName(groupName);
	}

	/**
	 * Gets the allocated leaves.
	 *
	 * @return the allocated leaves
	 */
	public ArrayList<AllocatedLeaves> getAllocatedLeaves() {
		return leaveGroup.getAllocatedLeaves();
	}

	/**
	 * Sets the allocated leaves.
	 *
	 * @param list
	 *            the new allocated leaves
	 */
	public void setAllocatedLeaves(List<AllocatedLeaves> list) {
		if (list != null) {
			leaveGroup.setAllocatedLeaves(list);
		}
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Boolean getStatus() {
		return leaveGroup.getStatus();
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(Boolean status) {
		leaveGroup.setStatus(status);
	}

	public double getWorkingHrs() {
		if (this.empInfo != null) {
			return empInfo.getLeaveCalendar().getWorkingHours();

		}
		return workingHrs;
	}

	public void setWorkingHrs(double workingHrs) {

	}

	/****************************************** Relation Managment Part ***********************************************************/
	@GwtIncompatible
	@OnSave
	public void onSave() {
		if (empInfo != null && id == null) {
			empInfokey = Key.create(empInfo);

		}

	}

	/**
	 * On load.
	 */
	@GwtIncompatible
	@OnLoad
	public void onLoad() {
		/**
	    * @author Anil @since 25-01-2021
	    * commented below code which loads leave calendar every time of loading employee info
	    * @author Anil @since 29-01-2021
	    * loading calendar at the time of loading employee info
	    * this causes issues in attendance upload and payroll
	    */
		if (empInfokey != null) {
			EmployeeInfo info = ofy().load().key(empInfokey).now();
			setEmpInfo(info);
		}
	}

	/****************************************** Relation Managment Part Ends ***********************************************************/

	
}
