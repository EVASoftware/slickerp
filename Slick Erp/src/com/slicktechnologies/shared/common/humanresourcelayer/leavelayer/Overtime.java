package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;

@Entity
@Embed
public class Overtime extends SuperModel {

	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -4099973507421247097L;
	@Index
	protected String shortName;
	@Index
	protected String name;
	
	protected boolean hourly;
	
	protected double hourlyRate;
	
	protected boolean flat;
	
	protected double flatRate;
	
	protected double percentOfCTC;
	
	@Index
	protected boolean leave;
	
	protected double leaveMultiplier;
	
	@Index
	protected String leaveName;
	
	@Index
	protected boolean status;
	
	/**
	 * Date : 30-05-2018 By ANIL
	 * Compensatory OT applicable for following days
	 * Sasha ERP
	 */
	@Index
	protected String holidayType;
	@Index
	protected boolean weeklyOff;
	@Index
	protected boolean normalDays;
	
	/**
	 * End
	 */
	
	
	/**
	 * Date : 23-07-2018 BY ANIL
	 * In HrProject Assigning project Employee designation and employee wise
	 */
	
	@Index
	protected int empId;
	@Index
	protected String empName;
	@Index
	protected String empDesignation;
	/** date 18.8.2018 added by komal **/
	@Index
	protected String shift;
	@Index
	protected Date startDate;
	@Index
	protected Date endDate;
/**Date 5-4-2019 added by Amol**/
	@Index
	protected String overtime;
	
	/**
	 * @author Anil , Date : 08-06-2019
	 */
	@Index
	protected boolean formulaOtHourly;
	@Index
	protected boolean formulaOtFlat;
	
	@Serialize
	protected ArrayList<OtEarningComponent> otEarningCompList=new ArrayList<OtEarningComponent>();
	protected String applicableDaysStr;
	protected int applicableDays;
	protected double workingHours;
	protected double rate;
	
	@Index
	protected boolean payOtAsOther;
	protected String otCorrespondanceName;
	
	
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * Adding site location(customer branch) field for Sunrise Facility
	 * Raised by Rahul Tiwari
	 */
	@Index
	protected String siteLocation;
	
	public Overtime() {
		shortName="";
		name="";
		leaveName="";
		holidayType="";
		
		empName="";
		empDesignation="";
		/** date 18.8.2018 added by komal **/
		shift = "";
	}

	
	
	
	
	

	public boolean isPayOtAsOther() {
		return payOtAsOther;
	}







	public void setPayOtAsOther(boolean payOtAsOther) {
		this.payOtAsOther = payOtAsOther;
	}







	public String getOtCorrespondanceName() {
		return otCorrespondanceName;
	}







	public void setOtCorrespondanceName(String otCorrespondanceName) {
		this.otCorrespondanceName = otCorrespondanceName;
	}







	public boolean isFormulaOtHourly() {
		return formulaOtHourly;
	}







	public void setFormulaOtHourly(boolean formulaOtHourly) {
		this.formulaOtHourly = formulaOtHourly;
	}







	public boolean isFormulaOtFlat() {
		return formulaOtFlat;
	}







	public void setFormulaOtFlat(boolean formulaOtFlat) {
		this.formulaOtFlat = formulaOtFlat;
	}







	public ArrayList<OtEarningComponent> getOtEarningCompList() {
		return otEarningCompList;
	}







	public void setOtEarningCompList(List<OtEarningComponent> otEarningCompList) {
		ArrayList<OtEarningComponent> compList=new ArrayList<OtEarningComponent>();
		compList.addAll(otEarningCompList);
		this.otEarningCompList = compList;
	}







	public String getApplicableDaysStr() {
		return applicableDaysStr;
	}







	public void setApplicableDaysStr(String applicableDaysStr) {
		this.applicableDaysStr = applicableDaysStr;
	}







	public int getApplicableDays() {
		return applicableDays;
	}







	public void setApplicableDays(int applicableDays) {
		this.applicableDays = applicableDays;
	}







	public double getWorkingHours() {
		return workingHours;
	}







	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}







	public double getRate() {
		return rate;
	}







	public void setRate(double rate) {
		this.rate = rate;
	}







	public int getEmpId() {
		return empId;
	}





	public void setEmpId(int empId) {
		this.empId = empId;
	}





	public String getEmpName() {
		return empName;
	}





	public void setEmpName(String empName) {
		this.empName = empName;
	}





	public String getEmpDesignation() {
		return empDesignation;
	}





	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}





	public String getHolidayType() {
		return holidayType;
	}



	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}



	public boolean isWeeklyOff() {
		return weeklyOff;
	}



	public void setWeeklyOff(boolean weeklyOff) {
		this.weeklyOff = weeklyOff;
	}



	public boolean isNormalDays() {
		return normalDays;
	}



	public void setNormalDays(boolean normalDays) {
		this.normalDays = normalDays;
	}



	/**************************************** Getter and Setter *****************************************/
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isHourly() {
		return hourly;
	}

	public void setHourly(Boolean hourly) {
		this.hourly = hourly;
	}

	public Double getHourlyRate() {
		return hourlyRate;
	}
	
	public void setHourlyRate(Double hourlyRate) {
		if(hourlyRate==null){
			hourlyRate=0d;
		}
		this.hourlyRate = hourlyRate;
	}
	
	public Boolean isStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	
	public Boolean isFlat() {
		return flat;
	}



	public void setFlat(Boolean flat) {
		this.flat = flat;
	}



	public Double getFlatRate() {
		return flatRate;
	}



	public void setFlatRate(Double flatRate) {
		if(flatRate==null){
			flatRate=0d;
		}
		this.flatRate = flatRate;
	}



	public Double getPercentOfCTC() {
		return percentOfCTC;
	}



	public void setPercentOfCTC(Double percentOfCTC) {
		if(percentOfCTC==null){
			percentOfCTC=0d;
		}
		this.percentOfCTC = percentOfCTC;
	}



	public Boolean isLeave() {
		return leave;
	}



	public void setLeave(Boolean leave) {
		this.leave = leave;
	}



	public Double getLeaveMultiplier() {
		return leaveMultiplier;
	}



	public void setLeaveMultiplier(Double leaveMultiplier) {
		if(leaveMultiplier==null){
			leaveMultiplier=0d;
		}
		this.leaveMultiplier = leaveMultiplier;
	}


	
	
	

	public String getLeaveName() {
		return leaveName;
	}



	public void setLeaveName(String leaveName) {
		if(leaveName==null){
			leaveName=null;
		}
		this.leaveName = leaveName;
	}



	@Override
	public String toString()
	{
		return name.toString();
	}

	
	
	
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof Overtime){
			Overtime leavetype=(Overtime) arg0;
		if(leavetype.getName()!=null)
		{
			return this.name.compareTo(leavetype.name);
		}
		}
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {

		/**
		 * @author Anil , Date : 20-06-2019
		 * removing validation from overtime shortname
		 * for creating multiple single and  double ot 
		 * raised by Rahul tiwari
		 */
//		return isDuplicateShortName(m) || isDuplicateName(m);
		return isDuplicateName(m);

	}
	
	
	
	public boolean isDuplicateShortName(SuperModel m) {
		
		Overtime entity = (Overtime) m;
		String name = entity.getShortName().trim();
		String curname=this.shortName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		

		
		
	}
	
	
	public boolean isDuplicateName(SuperModel m) {
		
		Overtime entity = (Overtime) m;
		String name = entity.getName().trim();
		String curname=this.name.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		

		
		
	}





	public String getShift() {
		return shift;
	}





	public void setShift(String shift) {
		this.shift = shift;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	public String getOvertime() {
		return overtime;
	}

	public void setOvertime(String overtime) {
		this.overtime = overtime;
	}


	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}



	/** date 14.1.2019 added by komal to clone object 
	 * whenever you add new variable in entity , do add variable in this method
	 * **/
	public Overtime Myclone() {
		// TODO Auto-generated method stub
		Overtime temp=new Overtime();
		temp = this;
		temp.id = id;
		temp.count = count;
		temp.createdBy = createdBy;
		temp.shortName = shortName;
		temp.name = name;
		temp.hourly = hourly;	
		temp.hourlyRate = hourlyRate;		
		temp.flatRate = flatRate;
		temp.percentOfCTC = percentOfCTC;
		temp.leave = leave;
		temp.leaveMultiplier = leaveMultiplier;
		temp.status = status;
		temp.holidayType = holidayType;
		temp.weeklyOff = weeklyOff;
		temp.normalDays = normalDays;
		temp.empId = empId;
		temp.empName = empName;
		temp.empDesignation = empDesignation;
		temp.shift = shift;
		temp.startDate = startDate;
		temp.endDate = endDate;
		
		temp.overtime=overtime;
		temp.formulaOtHourly=formulaOtHourly;
		temp.formulaOtFlat=formulaOtFlat;
		temp.otEarningCompList=otEarningCompList;
		temp.applicableDaysStr=applicableDaysStr;
		temp.applicableDays=applicableDays;
		temp.workingHours=workingHours;
		temp.rate=rate;
		temp.payOtAsOther=payOtAsOther;
		temp.otCorrespondanceName=otCorrespondanceName;
		
		temp.siteLocation=siteLocation;
		
		return temp;
	}
	
	
	public HrProjectOvertime MycloneHrProjectOT(Overtime overtimeObj) {
		// TODO Auto-generated method stub
		
		HrProjectOvertime temp= new HrProjectOvertime();
		temp.companyId = overtimeObj.companyId;
		temp.id = overtimeObj.id;
		temp.count = overtimeObj.count;
		temp.createdBy = overtimeObj.createdBy;
		temp.shortName = overtimeObj.shortName;
		temp.name = overtimeObj.name;
		temp.hourly = overtimeObj.hourly;	
		temp.hourlyRate = overtimeObj.hourlyRate;		
		temp.flatRate = overtimeObj.flatRate;
		temp.percentOfCTC = overtimeObj.percentOfCTC;
		temp.leave = overtimeObj.leave;
		temp.leaveMultiplier = overtimeObj.leaveMultiplier;
		temp.status = overtimeObj.status;
		temp.holidayType = overtimeObj.holidayType;
		temp.weeklyOff = overtimeObj.weeklyOff;
		temp.normalDays = overtimeObj.normalDays;
		temp.empId = overtimeObj.empId;
		temp.empName = overtimeObj.empName;
		temp.empDesignation = overtimeObj.empDesignation;
		temp.shift = overtimeObj.shift;
		temp.startDate = overtimeObj.startDate;
		temp.endDate = overtimeObj.endDate;
		
		temp.overtime=overtimeObj.overtime;
		temp.formulaOtHourly=overtimeObj.formulaOtHourly;
		temp.formulaOtFlat=overtimeObj.formulaOtFlat;
		temp.otEarningCompList=overtimeObj.otEarningCompList;
		temp.applicableDaysStr=overtimeObj.applicableDaysStr;
		temp.applicableDays=overtimeObj.applicableDays;
		temp.workingHours=overtimeObj.workingHours;
		temp.rate=overtimeObj.rate;
		temp.payOtAsOther=overtimeObj.payOtAsOther;
		temp.otCorrespondanceName=overtimeObj.otCorrespondanceName;
		
		temp.siteLocation=overtimeObj.siteLocation;
		
		return temp;
	}
}
