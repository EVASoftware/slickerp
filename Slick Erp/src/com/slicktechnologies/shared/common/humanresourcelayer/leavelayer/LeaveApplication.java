package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeHRProcess;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReport;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.TimeReportEntrey;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Represents the Leaves application by an Employee.
 * On Approval of Leave Application Corresponding  Balance of Application Leave Type
 * will change in {@link LeaveBalance}
 */
@Entity
public class LeaveApplication extends EmployeeHRProcess implements ApprovableProcess{

	/************************************Attributes***************************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8641370042707559325L;
	
	
	/**  Type of Leave for which Employee has Applied. */
	@Index
	protected String leaveType;
	
	/**  Number of Days for which leave was requested. */
	protected int NoOfDays;	
	
	/**  Contact of Employee on Leave. */
	protected  Contact contact;
	
	/** Reason for taking leave. */
	protected String leaveReason;
	
	/**  Document uploaded corresponding to this Leave application. */
    
	protected Vector<DocumentUpload> doc;
	
	/**
	 * Leave Balance Object Corresponding to this Employee
	 * Attribute will not be saved in Database , it is used to render
	 * the view
	 */
	@Ignore
	protected LeaveBalance leaveBalance;

	/*******************************************************************************************************/
	
	/************************************Constructor***************************************************************/
	/**
	 * Instantiates a new leave application.
	 */
	public LeaveApplication() {
		super();
		leaveType="";
		leaveReason="";
		contact=new Contact();
		doc=new Vector<DocumentUpload>();
	}

	/*************************************************************************************************************/
	
	
	
	/**
	 * *************************************Getters and Setters***************************************************.
	 *
	 * @return the leave type
	 */
	
	
	public String getLeaveType() {
		return leaveType;
	}

	/**
	 * Sets the leave type.
	 *
	 * @param leaveType the new leave type
	 */
	public void setLeaveType(String leaveType) {
		if(leaveType!=null)
		  this.leaveType = leaveType.trim();
	}


	/**
	 * Gets the no of days.
	 *
	 * @return the no of days
	 */
	public Integer getNoOfDays() {
		return NoOfDays;
	}

	/**
	 * Sets the no of days.
	 *
	 * @param noOfDays the new no of days
	 */
	public void setNoOfDays(Integer noOfDays) {
		NoOfDays = noOfDays;
	}

	/**
	 * Gets the contact.
	 *
	 * @return the contact
	 */
	public Contact getContact() {
		return contact;
	}

	/**
	 * Sets the contact.
	 *
	 * @param contact the new contact
	 */
	public void setContact(Contact contact) {
		if(contact!=null)
		this.contact = contact;
	}
	
	

	/**
	 * Gets the leave reason.
	 *
	 * @return the leave reason
	 */
	public String getLeaveReason() {
		return leaveReason;
	}


	/**
	 * Sets the leave reason.
	 *
	 * @param leaveReason the new leave reason
	 */
	public void setLeaveReason(String leaveReason) {
		if(leaveReason!=null)
		this.leaveReason = leaveReason.trim();
	}
	
	/**
	 * Gets the doc.
	 * @return the doc
	 */
	public DocumentUpload getDocument() 
	{
		if(doc.size()!=0)
		  return doc.get(0);
		return null;
	}
	/**
	 * Sets the doc.
	 * @param doc the new doc
	 */
	public void setDocument(DocumentUpload doc)
	{
		this.doc.add(doc);
	}
	
	/**
	 * Gets the cell no1.
	 *
	 * @return the cell no1
	 */
	public Long getCellNo1() {
		return contact.getCellNo1();
	}
	/**
	 * Sets the cell no1.
	 *
	 * @param cellNo1 the new cell no1
	 */
	public void setCellNo1(Long cellNo1) {
		contact.setCellNo1(cellNo1);
	}
	/**
	 * Gets the cell no2.
	 *
	 * @return the cell no2
	 */
	public Long getCellNo2() {
		return contact.getCellNo2();
	}
	/**
	 * Sets the cell no2.
	 *
	 * @param cellNo2 the new cell no2
	 */
	public void setCellNo2(Long cellNo2) {
		contact.setCellNo2(cellNo2);
	}
	/**
	 * Gets the landline.
	 *
	 * @return the landline
	 */
	public Long getLandline() {
		return contact.getLandline();
	}
	/**
	 * Sets the landline.
	 *
	 * @param landline the new landline
	 */
	public void setLandline(Long landline) {
		contact.setLandline(landline);
	}
	/**
	 * Gets the fax no.
	 *
	 * @return the fax no
	 */
	public Long getFaxNo() {
		return contact.getFaxNo();
	}
	/**
	 * Sets the fax no.
	 *
	 * @param faxNo the new fax no
	 */
	public void setFaxNo(Long faxNo) {
		contact.setFaxNo(faxNo);
	}
	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return contact.getWebsite();
	}
	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		contact.setWebsite(website);
	}
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return contact.getEmail();
	}
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		contact.setEmail(email);
	}
	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return contact.getAddress();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return contact.hashCode();
	}
	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(Address address) {
		contact.setAddress(address);
	}
	@Override
	public String getRemark() {
		return null;
	}

	@Override
	public void setRemark(String remark) {
		
	}
	
	
	
/**
 * **************************************************************************************************************************************.
 *
 * @param o the o
 * @return the int
 */


	@Override
	public int compareTo(SuperModel o) {
		return super.compareTo(o);
	}
	
	
	/* (non-Javadoc)
	 * @see com.slicktechnologies.shared.common.humanresourcelayer.HRProcess#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		return super.isDuplicate(m);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString();
	}
	
	
	
	@Override
	public String getEmployee() {
		return this.employeeName;
	}


	public Vector<DocumentUpload> getDoc() {
		return doc;
	}

	public void setDoc(Vector<DocumentUpload> doc) {
		this.doc = doc;
	}

	public LeaveBalance getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(LeaveBalance leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setNoOfDays(int noOfDays) {
		NoOfDays = noOfDays;
	}
	

	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList() {
		ArrayList<String>list=new ArrayList<String>();
		list.add(LeaveApplication.CREATED);
		list.add(LeaveApplication.APPROVED);
		
		list.add(LeaveApplication.REQUESTED);
		list.add(LeaveApplication.REJECTED);
		return list;
	}

	
	
	  
	/*********************************** Buisness Logic Part **************************************/	
	
	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		
		setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
		setApprovalDate(new Date());
		
//		System.out.println("Employee Count :: "+this.getEmpid()+" :: "+this.getEmployeeName());
		LeaveBalance levBal=ofy().load().type(LeaveBalance.class).filter("companyId", this.getCompanyId()).filter("empInfo.empCount", this.getEmpid()).first().now();
		if(levBal!=null){
//			System.out.println("Leave list Size :: "+levBal.getLeaveGroup().getAllocatedLeaves().size());
//			System.out.println("Leave Type :: "+this.getLeaveType());
			for(int i=0;i<levBal.getLeaveGroup().getAllocatedLeaves().size();i++){
				if(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().equals(this.getLeaveType())){
					
					double prevAppLev=0;
					if(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getApproved()!=0){
						prevAppLev=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getApproved();
					}
//					System.out.println("Previous Approved Leave :: "+prevAppLev);
					
					double preReqLev=0;
					double balReqLev=0;
					if(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested()!=0){
						preReqLev=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested();
						balReqLev=preReqLev-this.getNoOfDays();
						if(balReqLev<0){
							balReqLev=balReqLev*(-1);
						}
					}
//					System.out.println("Previous Requested Leave :: "+preReqLev);
//					System.out.println("Leave Type :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName());
//					System.out.println("Requested Leave  :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested());
//					System.out.println("No. Of Days :: "+this.getNoOfDays());
					
					levBal.getLeaveGroup().getAllocatedLeaves().get(i).setApproved(prevAppLev+this.getNoOfDays());
					levBal.getLeaveGroup().getAllocatedLeaves().get(i).setRequested(balReqLev);
					
//					System.out.println("Approved Leave  :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getApproved());
//					System.out.println("After Requested Leave  :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested());
				}
			}
			
			ofy().save().entity(levBal).now();
		}else{
			System.out.println("inside leave bal null !!!");
		}
		
		
		getTimeReportStatus();
		
	
		
		
		
	}
	
	@GwtIncompatible
	public void getTimeReportStatus(){
		System.out.println("REACT ON APPROVAL INSIDE TIME REPORT STATUS METHOD ");
		SimpleDateFormat isoFormat = new SimpleDateFormat("MMMM");
		
		Calendar cal1 = Calendar.getInstance(); 
		Calendar cal2 = Calendar.getInstance(); 
		
		Date fromDate=this.getFromdate();
		Date toDate=this.getTodate();
		
		cal1.setTime(fromDate);
		cal2.setTime(toDate);
		System.out.println("LEAVE FROM DATE :: "+fromDate);
		System.out.println("LEAVE TO DATE :: "+toDate);
		
		HashSet<String> hset =new HashSet<String>();
		
		while(toDate.equals(cal1.getTime())||toDate.after(cal1.getTime())){
//			System.out.println("FROM DATE :: "+cal1.getTime());
//			System.out.println("TO DATE :: "+toDate);
			String monthName=isoFormat.format(cal1.getTime());
			System.out.println("MONTH :: "+monthName);
			hset.add(monthName);
			cal1.add(Calendar.DATE, 1);
		}
		
		System.out.println("MONTH LIST SIZE "+hset.size());
		
		ArrayList<String> monthList=new ArrayList<String>();
		monthList.addAll(hset);
		
		Date wkSrtDtFrm=calculatePeriodStartDate(this.getFromdate());
		Date wkEndDtFrm=calculatePeriodEndDate(this.getFromdate());
		
		System.out.println("WEEK START DATE FROM: "+wkSrtDtFrm);
		System.out.println("WEEK END DATE FROM: "+wkEndDtFrm);
		
		Date wkSrtDtTo=calculatePeriodStartDate(this.getTodate());
		Date wkEndDtTo=calculatePeriodEndDate(this.getTodate());
		
		System.out.println("WEEK START DATE TO: "+wkSrtDtTo);
		System.out.println("WEEK END DATE TO: "+wkEndDtTo);
		
		boolean isSameDate=false;
		
		if(wkSrtDtFrm.equals(wkSrtDtTo)&&wkEndDtFrm.equals(wkEndDtTo)){
			isSameDate=true;
		}
		
		String month1="";
		String month2="";
		
		if(monthList.size()==1){
			month1=monthList.get(0);
		}else{
			month1=monthList.get(0);
			month2=monthList.get(1);
		}
		
		if(monthList.size()==1){
			if(isSameDate==true){
				updateTimeReport(wkSrtDtFrm,wkEndDtFrm,month1,wkSrtDtFrm,wkEndDtFrm,month1,false);
			}else{
				updateTimeReport(wkSrtDtFrm,wkEndDtFrm,month1,wkSrtDtTo,wkEndDtTo,month1,true);
			}
			
		}else{
			if(isSameDate==true){
				updateTimeReport(wkSrtDtFrm,wkEndDtFrm,month1,wkSrtDtFrm,wkEndDtFrm,month2,true);
			}else{
				updateTimeReport(wkSrtDtFrm,wkEndDtFrm,month1,wkSrtDtTo,wkEndDtTo,month2,true);
			}
		}
		
		
	}
	
	@GwtIncompatible
	public void updateTimeReport(Date startDateFrm,Date endDateFrm,String month1,Date startDateTo,Date endDateTo,String month2,boolean checkSecond){
		
		Date leaveStartDate=this.getFromdate();
		Date lEnd=this.getTodate();
		Date leaveStartDate1=this.getFromdate();
		
		
//		System.out.println("LEAVE FROM DATE :: "+leaveStartDate);
//		System.out.println("LEAVE TO DATE :: "+lEnd);
		
		Calendar leavDate=Calendar.getInstance();
		leavDate.setTime(leaveStartDate);
		
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(leaveStartDate1);
		
		
		Calendar leavDate1=Calendar.getInstance();
		leavDate1.setTime(leaveStartDate);
		
		
		Date fromDate=startDateFrm;
		Date toDate=endDateFrm;
		
//		System.out.println("WEEK FROM DATE :: "+fromDate);
//		System.out.println("WEEK TO DATE :: "+toDate);
		
		Calendar cal1=Calendar.getInstance();
		cal1.setTime(fromDate);
		
		Calendar wkstDtCal=Calendar.getInstance();
		wkstDtCal.setTime(fromDate);
		
		
				
		EmployeeInfo info=ofy().load().type(EmployeeInfo.class).filter("companyId",this.getCompanyId()).filter("empCount", this.getEmpid()).first().now();
		TimeReport tymRep=ofy().load().type(TimeReport.class).filter("companyId", this.getCompanyId()).filter("empid", this.getEmpid()).filter("fromdate", startDateFrm).filter("month", month1).first().now();
		
		if(tymRep!=null){
			System.out.println("INSIDE TIME REPORT EXIST 1");
			boolean leaveExist=false;
			ArrayList<TimeReportEntrey> timeRepEnt=tymRep.getTimeReportEntry();
			int noOfDays=(int)( (toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
			noOfDays++;
			
			System.out.println("NO OF DAYS IN BETWEEN LEAVE DAYS "+noOfDays);
			for(int k=0;k<noOfDays;k++){
//				System.out.println("OUTER LOOP : "+k);
//				System.out.println("List Size : "+timeRepEnt.size());
				for(int i=0;i<timeRepEnt.size();i++){
					if(timeRepEnt.get(i).isEmployeeOnLeave()){
//						System.out.println("Employee On leave  ");
//						System.out.println("TR LEAVE NAME : "+timeRepEnt.get(i).getProjectName());
//						System.out.println("LEAVE NAME : "+this.getLeaveType());
						String leaveName=timeRepEnt.get(i).getProjectName().replace("Leave :", "");
//						System.out.println("EDITED LEAVE NAME : "+leaveName);
						if(leaveName.trim().equals(this.getLeaveType())){
//							System.out.println("TIME REPORT DATE :: "+timeRepEnt.get(i).getDate());
//							System.out.println("LEAVE DATE :: "+leavDate.getTime());
							if(timeRepEnt.get(i).getDate().equals(leavDate.getTime())&&(leavDate.getTime().equals(lEnd)||leavDate.getTime().before(lEnd)))	
							{
								System.out.println("LEAVE ALREADY EXIST IN TIMEREPORT AND UPDATING TIMEREPORT");
								leaveExist=true;
								timeRepEnt.get(i).setProjhours(info.getLeaveCalendar().getWorkingHours());
								leavDate.add(Calendar.DATE, 1);
							}
						}
					}
				}
				cal1.add(Calendar.DATE, 1);
			}
			
			if(leaveExist==true){
				System.out.println("INSIDE TRUE METHOD ");
				for(int k=0;k<noOfDays;k++){
					for(int i=0;i<timeRepEnt.size();i++){
						if(timeRepEnt.get(i).isEmployeeOnLeave()==false){
							if(timeRepEnt.get(i).getDate().equals(leavDate1.getTime())&&(leavDate1.getTime().equals(lEnd)||leavDate1.getTime().before(lEnd)))	
							{
								timeRepEnt.get(i).setProjhours(0d);
								leavDate1.add(Calendar.DATE, 1);
							}
						}
					}
				}
			}
			
			
			
			if(leaveExist==false){
				System.out.println("LEAVE DOES NOT EXIST IN TIMEREPORT NEW LEAVE IS CREATED");
				for(int i=0;i<noOfDays;i++){
					
//					System.out.println("Week Date : "+wkstDtCal.getTime());
//					System.out.println("LEAVE Date : "+cal2.getTime());
					
					
					
					if(wkstDtCal.getTime().equals(cal2.getTime())&&( cal2.getTime().equals(lEnd)||cal2.getTime().before(lEnd))){
						System.out.println("INSIDE DATE MATCH");
						
//						System.out.println("DATE TO BE SET : "+cal2.getTime());
						TimeReportEntrey tym=new TimeReportEntrey();
						
						tym.setEmployeeOnLeave(true);
						tym.setProjectName(this.leaveType);
						tym.setDate(cal2.getTime());
						tym.setProjhours(info.getLeaveCalendar().getWorkingHours());
						
						tymRep.getTimeReportEntry().add(tym);
						cal2.add(Calendar.DATE, 1);
					}else{
						TimeReportEntrey tym1=new TimeReportEntrey();
						
						tym1.setEmployeeOnLeave(true);
						tym1.setProjectName(this.leaveType);
						tym1.setDate(wkstDtCal.getTime());
						tym1.setProjhours(0d);
						
						tymRep.getTimeReportEntry().add(tym1);
					}
					
					
					
					wkstDtCal.add(Calendar.DATE, 1);
				}
			}
			
			ofy().save().entity(tymRep).now();
			
			if(checkSecond==true){
				updateTimeReport(startDateTo,endDateTo,month2);
			}
		}
		else{
			if(checkSecond==true){
				updateTimeReport(startDateTo,endDateTo,month2);
			}
			 System.out.println("TIME REPORT NOT EXIST FOR FIRST TR ");
		}
	}
	
	@GwtIncompatible
	public void updateTimeReport(Date startDate,Date endDate,String month){
		System.out.println("INSIDE SECOND TIME REPORT METHOD ::");
		Date leaveStartDate=this.getFromdate();
		Date lEnd=this.getTodate();
		Date leaveStartDate1=this.getFromdate();
		
		
//		System.out.println("LEAVE FROM DATE :: "+leaveStartDate);
//		System.out.println("LEAVE TO DATE :: "+lEnd);
		
		Calendar leavDate=Calendar.getInstance();
		leavDate.setTime(leaveStartDate);
		
		Calendar cal2=Calendar.getInstance();
		cal2.setTime(leaveStartDate1);
		
		Calendar leavDate1=Calendar.getInstance();
		leavDate1.setTime(leaveStartDate);
		
		
		
		Date fromDate=startDate;
		Date toDate=endDate;
		
//		System.out.println("WEEK FROM DATE :: "+fromDate);
//		System.out.println("WEEK TO DATE :: "+toDate);
		
		Calendar cal1=Calendar.getInstance();
		cal1.setTime(fromDate);
		
		Calendar wkstDtCal=Calendar.getInstance();
		wkstDtCal.setTime(fromDate);
		
		
				
		EmployeeInfo info=ofy().load().type(EmployeeInfo.class).filter("companyId",this.getCompanyId()).filter("empCount", this.getEmpid()).first().now();
		TimeReport tymRep=ofy().load().type(TimeReport.class).filter("companyId", this.getCompanyId()).filter("empid", this.getEmpid()).filter("fromdate", startDate).filter("month", month).first().now();
		
		if(tymRep!=null){
			System.out.println("INSIDE TIME REPORT EXIST 1");
			boolean leaveExist=false;
			ArrayList<TimeReportEntrey> timeRepEnt=tymRep.getTimeReportEntry();
			int noOfDays=(int)( (toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
			noOfDays++;
			
			System.out.println("NO OF DAYS IN BETWEEN LEAVE DAYS "+noOfDays);
			for(int k=0;k<noOfDays;k++){
//				System.out.println("OUTER LOOP : "+k);
//				System.out.println("List Size : "+timeRepEnt.size());
				for(int i=0;i<timeRepEnt.size();i++){
					if(timeRepEnt.get(i).isEmployeeOnLeave()){
//						System.out.println("Employee On leave  ");
//						System.out.println("TR LEAVE NAME : "+timeRepEnt.get(i).getProjectName());
//						System.out.println("LEAVE NAME : "+this.getLeaveType());
						String leaveName=timeRepEnt.get(i).getProjectName().replace("Leave :", "");
//						System.out.println("EDITED LEAVE NAME : "+leaveName);
						if(leaveName.trim().equals(this.getLeaveType())){
//							System.out.println("TIME REPORT DATE :: "+timeRepEnt.get(i).getDate());
//							System.out.println("LEAVE DATE :: "+leavDate.getTime());
							if(timeRepEnt.get(i).getDate().equals(leavDate.getTime())&&(leavDate.getTime().equals(lEnd)||leavDate.getTime().before(lEnd)))	
							{
								System.out.println("LEAVE ALREADY EXIST IN TIMEREPORT AND UPDATING TIMEREPORT");
								leaveExist=true;
								timeRepEnt.get(i).setProjhours(info.getLeaveCalendar().getWorkingHours());
								leavDate.add(Calendar.DATE, 1);
							}
						}
					}
				}
				cal1.add(Calendar.DATE, 1);
			}
			
			
			
			if(leaveExist==true){
				for(int k=0;k<noOfDays;k++){
					for(int i=0;i<timeRepEnt.size();i++){
						if(timeRepEnt.get(i).isEmployeeOnLeave()==false){
							if(timeRepEnt.get(i).getDate().equals(leavDate1.getTime())&&(leavDate1.getTime().equals(lEnd)||leavDate1.getTime().before(lEnd)))	
							{
								leaveExist=true;
								timeRepEnt.get(i).setProjhours(0d);
								leavDate1.add(Calendar.DATE, 1);
							}
						}
					}
				}
			}
			
			
			if(leaveExist==false){
				System.out.println("LEAVE DOES NOT EXIST IN TIMEREPORT NEW LEAVE IS CREATED");
				for(int i=0;i<noOfDays;i++){
					
//					System.out.println("Week Date : "+wkstDtCal.getTime());
//					System.out.println("LEAVE Date : "+cal2.getTime());
					
					
					
					if(wkstDtCal.getTime().equals(cal2.getTime())&&( cal2.getTime().equals(lEnd)||cal2.getTime().before(lEnd))){
						System.out.println("INSIDE DATE MATCH");
						
//						System.out.println("DATE TO BE SET : "+cal2.getTime());
						TimeReportEntrey tym=new TimeReportEntrey();
						
						tym.setEmployeeOnLeave(true);
						tym.setProjectName(this.leaveType);
						tym.setDate(cal2.getTime());
						tym.setProjhours(info.getLeaveCalendar().getWorkingHours());
						
						tymRep.getTimeReportEntry().add(tym);
						cal2.add(Calendar.DATE, 1);
					}else{
						TimeReportEntrey tym1=new TimeReportEntrey();
						
						tym1.setEmployeeOnLeave(true);
						tym1.setProjectName(this.leaveType);
						tym1.setDate(wkstDtCal.getTime());
						tym1.setProjhours(0d);
						
						tymRep.getTimeReportEntry().add(tym1);
					}
					
					
					
					wkstDtCal.add(Calendar.DATE, 1);
				}
			}
			
			ofy().save().entity(tymRep).now();
		}else{
			System.out.println("TIME REPORT DOES NOT EXIST");
		}
	}
	
	@GwtIncompatible
	public Date calculatePeriodStartDate(Date date) {
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		Date currDate = date;
		Calendar c = Calendar.getInstance();
		c.setTime(currDate);
//		System.out.println(c.get(Calendar.DAY_OF_WEEK));
//		System.out.println(f.format(c.getTime()));
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
//		System.out.println(c.get(Calendar.DAY_OF_WEEK));
		System.out.println(f.format(c.getTime()));
		
		currDate=c.getTime();
		return currDate;
	}
	
	@GwtIncompatible
	public Date calculatePeriodEndDate(Date date) {

		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		Date currDate = date;
		Calendar c = Calendar.getInstance();
		c.setTime(currDate);
//		System.out.println(c.get(Calendar.DAY_OF_WEEK));
//		System.out.println(f.format(c.getTime()));
		c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
//		System.out.println(c.get(Calendar.DAY_OF_WEEK));
		System.out.println(f.format(c.getTime()));
		
		currDate=c.getTime();
		return currDate;
		
	}
	
	
	
	
	
	
	@Override
	@GwtIncompatible
	public void reactOnRejected() {
		//reactOnLeaveRejetcion();
		System.out.println("Called Leave Rejection");
		setApprovalDate(DateUtility.getDateWithTimeZone("IST",new Date()));
		
		System.out.println("Employee Count :: "+this.getEmpid()+" :: "+this.getEmployeeName());
		LeaveBalance levBal=ofy().load().type(LeaveBalance.class).filter("companyId", this.getCompanyId()).filter("empInfo.empCount", this.getEmpid()).first().now();
		if(levBal!=null){
			System.out.println("Leave list Size :: "+levBal.getLeaveGroup().getAllocatedLeaves().size());
			System.out.println("Leave Type :: "+this.getLeaveType());
			
			for(int i=0;i<levBal.getLeaveGroup().getAllocatedLeaves().size();i++){
				if(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName().equals(this.getLeaveType())){
					
					double prevRejLev=0;
					if(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRejected()!=0){
						prevRejLev=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRejected();
					}
					System.out.println("Previous Rejected Leave :: "+prevRejLev);
					
					double preReqLev=0;
					double balReqLev=0;
					if(levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested()!=0){
						preReqLev=levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested();
						balReqLev=preReqLev-this.getNoOfDays();
						if(balReqLev<0){
							balReqLev=balReqLev*(-1);
						}
					}
					System.out.println("Previous Requested Leave :: "+preReqLev);
					
					System.out.println("Leave Type :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getName());
					System.out.println("Requested Leave  :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested());
					System.out.println("No. Of Days :: "+this.getNoOfDays());
					
					levBal.getLeaveGroup().getAllocatedLeaves().get(i).setRejected(prevRejLev+this.getNoOfDays());
					levBal.getLeaveGroup().getAllocatedLeaves().get(i).setRequested(balReqLev);
					
					System.out.println("Rejected Leave  :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRejected());
					System.out.println("After Requested Leave  :: "+levBal.getLeaveGroup().getAllocatedLeaves().get(i).getRequested());
				}
			}
			
			ofy().save().entity(levBal).now();
		}else{
			System.out.println("inside leave bal null !!!");
		}
		
	}
	
	
	
	
	/**
	 * This method querry Leave Balance for the Applicant Employee.
	 * It Loads the Leave Balance List and then increases the requested attribute of Leave Type for which Application
	 * is Applied.
	 * 
	 */
	@GwtIncompatible
	public void reactOnLeaveRequest()
	{
		//If Status of Leave Application is not Requested return
		if(!(this.status.equals(LeaveApplication.REQUESTED)))
				return;
		LeaveBalance temp = null;
		List<LeaveBalance> bal=null;
		//Load all the Leave Balances for this particular Employee
		if(getCompanyId()!=null)
			 bal=ofy().load().type(LeaveBalance.class).
		     filter("companyId",getCompanyId()).filter("empInfo.empCount",getEmpid()).list();
		else
			bal=ofy().load().type(LeaveBalance.class).filter("empInfo.empCount",getEmpid()).list();
		//Load only active Leave Balance for that Employee.This is critical point
		//Pls Understand that there should be only one Active Leave Balance for a 
		//Particular Employee, Active Leave Balance means such Leave Balance which
		//valid till date is greater than current Date.
		for(int i=0;i<bal.size();i++)
	    {
	    	LeaveBalance balance=bal.get(i);
			
			if(balance.getValidtill().after(new Date()))
	    	{
	    		temp=balance;
	    		break;
	    	}
	    	

	    }
		//If Status of Leave Application is not Requested return
		ArrayList<AllocatedLeaves> allLeave=temp.getAllocatedLeaves();
		for(AllocatedLeaves altemp:allLeave )
		{
			if(altemp.name.equals(this.leaveType.trim()))
			{
				altemp.requested=altemp.requested+this.NoOfDays;
				ofy().save().entity(temp).now();
			}
		}
		
	}
	
	
	
	/**
	 * This method querry Leave Balance for the Applicant Employee.
	 * It Loads the Leave Balance List and then increases the requested attribute of Leave Type for which Application
	 * is Applied.
	 * 
	 */
	@GwtIncompatible
	public void reactOnLeaveRejetcion()
	{
		//If Status of Leave Application is not Requested return
		if(!(this.status.equals(LeaveApplication.REJECTED)))
				return;
		LeaveBalance temp = null;
		List<LeaveBalance> bal=null;
		//Load all the Leave Balances for this particular Employee
		if(getCompanyId()!=null)
			 bal=ofy().load().type(LeaveBalance.class).
		     filter("companyId",getCompanyId()).filter("empInfo.empCount",getEmpid()).list();
		else
			bal=ofy().load().type(LeaveBalance.class).filter("empInfo.empCount",getEmpid()).list();
		//Load only active Leave Balance for that Employee.This is critical point
		//Pls Understand that there should be only one Active Leave Balance for a 
		//Particular Employee, Active Leave Balance means such Leave Balance which
		//valid till date is greater than current Date.
		for(int i=0;i<bal.size();i++)
	    {
	    	LeaveBalance balance=bal.get(i);
			
			if(balance.getValidtill().after(new Date()))
	    	{
	    		temp=balance;
	    		break;
	    	}
	    	

	    }
		//If Status of Leave Application is not Requested return
		ArrayList<AllocatedLeaves> allLeave=temp.getAllocatedLeaves();
		for(AllocatedLeaves altemp:allLeave )
		{
			if(altemp.name.equals(this.leaveType.trim()))
				altemp.requested=altemp.requested-this.NoOfDays;
		}
		ofy().save().entity(temp).now();
	}
	
	/**
	 * The requested Leaves are granted to Employees by this method. Technically it changes the Leave Balance
	 * properties for that Employee.
	 */
	@GwtIncompatible
	public void grantLeave()
	{
		 LeaveBalance temp = null;
		 List<LeaveBalance> bal=null;
		 //Get the List of Leave Balances in Kind , there will be a List as previous year Leave Balance
		 //can exist.Ideally the Querry should be from Leave Balance
		if(getCompanyId()!=null)
		{
		   bal=ofy().load().type(LeaveBalance.class).filter("companyId",getCompanyId()).filter("empInfo.empCount",getEmpid()).list();
		}
		else
		{
			bal=ofy().load().type(LeaveBalance.class).filter("empInfo.empCount",getEmpid()).list();
		}
		//If for some reason balance is null return , accurate place to through an Exception
		if(bal==null)
			return;
		//Initalize temp with Leave balance valid for current period only.
		//To This to happen, There should beich only one Entity in Kind Leave Balance whichs valid Till
		//is greater then Current Date, Leave Allocation should ensure this.
		for(LeaveBalance balance:bal)
	    {
	    	if(balance.getValidtill().after(new Date()))
	    	{
	    		temp=balance;
	    		break;
	    	}

	    }
		//If for some reason temp is null return , accurate place to through an Exception
		if(temp==null)
			return;
		ArrayList<AllocatedLeaves> allLeave=temp.getAllocatedLeaves();
		//Find the Leave for which Leave Appliaction is applied for,make the changes
		for(AllocatedLeaves altemp:allLeave )
		{
			if(altemp.name.equals(this.leaveType.trim()))
			{
				System.out.println("Name Of Leave Type"+leaveType.trim());
				//Only Reduce The Balance when it is zero
				if(altemp.balance>0)
				{
				altemp.requested=altemp.requested-this.NoOfDays;
				altemp.availedDays=altemp.availedDays+this.NoOfDays;
				altemp.balance=altemp.balance-this.NoOfDays;
				}
				break;
			}
			
		}
		//Save the Leave Balance entity
		
		ofy().save().entity(temp).now();
	}

	
	
	/**
	 * 
	 * @param employeeId id of Employee for which Leave Date is being validated.
	 * @param companyId  company id  for company separation purpose.
	 * @param leaveDate  date to be validated that it is Leave Date or not.
	 * @return
	 */
	@GwtIncompatible
	 public static boolean isLeaveDate(int employeeId,long companyId,Date leaveDate)
	 {
		 List<LeaveApplication>list=ofy().load().type(LeaveApplication.class).filter("status",LeaveApplication.APPROVED).
		 filter("companyId", companyId).filter("empid", employeeId).list();
		 for(LeaveApplication application:list)
		 {
			 boolean isRange=isWithinRange(leaveDate,application.getFromdate(),application.getTodate());
			 if(isRange){
				 return true;
			 }
		 }
		 return false;
		 
	 }
	
	public static boolean  isWithinRange(Date testDate,Date startDate,Date endDate) {
		   return !(testDate.before(startDate) || testDate.after(endDate));
	}
	
	/**
	 * Methods load Leave Balance object for the purpose of UI Rendering
	 * Purpose.
	 */
	@OnLoad
	@GwtIncompatible
	private void loadLeaveBalance()
	{
		List<LeaveBalance>temp=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount",empid).filter("validtill > ",new Date()).list();
		//IF Leave Balance Exists.
	//	 System.out.println("Size Of Leave Balance-- "+empid);
		if(temp!=null&&temp.size()!=0)
		{
			this.leaveBalance=temp.get(0);
	//		 System.out.println("Size Of Leave Balance"+leaveBalance.getFullName());
		}
		else{
			leaveBalance=null;
		}
	}

	

	
	/*******************************************************Buisness Logic Partd Ends**************************************/
}


	
	

