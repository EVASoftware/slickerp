package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

// TODO: Auto-generated Javadoc
/**
 * Represents process of Allocation of Leave to an Employee.This allocation process
 * can only be performed once in a calendar period.Saving of the Allocation
 * process will create {@link LeaveBalance} entities.When a {@link LeaveBalance} entity is created
 * then it should also contain previously carry forwarded leaves described in LeaveBalance Entity.
 * 
 */
@Entity
public class LeaveAllocationProcess extends SuperModel 
{
   
	/******************************Attributes*********************************************************************/
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -85419010705327355L;
	
	/** User who do the allocation process */
	protected String user;
	
	/** Filter criteria on which leaves are allocated, For example Leave Can be Allocated to
	 * Employee */
	protected String allocatedTo;
	
	/** Validity start date of Leave */
	@Index
	protected Date validFrom;
	
	/** Validity end date of Leave */
	@Index
	protected Date validTo;
	
	/** The system time where Leaves are allocated */
	protected Date systemTime;
	
	protected String calendarName;

/******************************************************************************************************************/	
	
/**************************************Constructor***************************************************************************/	

	public LeaveAllocationProcess() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/***********************************************************************************************************************/	

	
	/**************************************Getter Setters***************************************************************************/	

	
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}






	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}



	/**
	 * Gets the allocated to.
	 *
	 * @return the allocated to
	 */
	public String getAllocatedTo() {
		return allocatedTo;
	}



	/**
	 * Sets the allocated to.
	 *
	 * @param allocatedTo the new allocated to
	 */
	public void setAllocatedTo(String allocatedTo) {
		this.allocatedTo = allocatedTo;
	}



	/**
	 * Gets the valid from.
	 *
	 * @return the valid from
	 */
	public Date getValidFrom() {
		return validFrom;
	}



	/**
	 * Sets the valid from.
	 *
	 * @param validFrom the new valid from
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}



	/**
	 * Gets the valid to.
	 *
	 * @return the valid to
	 */
	public Date getValidTo() {
		return validTo;
	}



	/**
	 * Sets the valid to.
	 *
	 * @param validTo the new valid to
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}



	/**
	 * Gets the system time.
	 *
	 * @return the system time
	 */
	public Date getSystemTime() {
		return systemTime;
	}



	/**
	 * Sets the system time.
	 *
	 * @param systemTime the new system time
	 */
	public void setSystemTime(Date systemTime) {
		this.systemTime = systemTime;
	}



	public String getCalendarName() {
		return calendarName;
	}



	public void setCalendarName(String calendarName) {
		if(calendarName!=null)
		   this.calendarName = calendarName.trim();
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
