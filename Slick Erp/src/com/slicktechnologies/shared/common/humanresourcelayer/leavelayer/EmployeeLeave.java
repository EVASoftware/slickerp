package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

@Entity
public class EmployeeLeave extends SuperModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9116504102628605307L;
	@Index
	protected Date leaveDate;
	@Serialize
	protected LeaveType leaveType;
	@Index
	protected Key<EmployeeInfo>employeeKey;
	
	/**
	 * Date : 14-05-2018 BY ANIL
	 * we were updating leave status against employee key,now storing against empId
	 */
	@Index
	protected int emplId;
	
	@Ignore
	protected EmployeeInfo employee;
	protected double LeaveHrs;
	
	
	/**
	 * @author Anil, Date : 17-07-2019
	 * setting project name for calculating project wise leave and ot
	 */
	@Index
	String projectName;
	
	/**
	 * @author Anil
	 * @since 01-12-2020
	 * Adding sitelocation for showing annexure details site location wise on Invoice pdf
	 * raised by Rahul Tiwari for Sunrise
	 */
	
	@Index
	String siteLocation;
	
	public EmployeeLeave() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public int getEmplId() {
		return emplId;
	}



	public void setEmplId(int emplId) {
		this.emplId = emplId;
	}



	public Date getLeaveDate() {
		return leaveDate;
	}





	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}





	public String getLeaveTypeName() {
		if(leaveType!=null)
		   return leaveType.getName();
		return "";
	}





	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}





	public Key<EmployeeInfo> getEmployeeKey() {
		return employeeKey;
	}





	public void setEmployeeKey(Key<EmployeeInfo> employeeKey) {
		this.employeeKey = employeeKey;
	}





	




	public EmployeeInfo getEmployee() {
		return employee;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	@OnLoad
	@GwtIncompatible
	public void loadEmployeeInfo(){
		if(employeeKey!=null){
			employee=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount", emplId).first().now();
		}
	}
	
	
	@OnSave
	@GwtIncompatible
	public void loadKeys()
	{
		if(getId()==null)
		{
		
		if(employee!=null)
		  employeeKey= ofy().load().type(EmployeeInfo.class).
		   filter("empCount",employee.getCount()).
		   filter("companyId",employee.getCompanyId()).keys().first().now();
	   }
		
		
	}
	public LeaveType getLeaveType() {
		return leaveType;
	}
	public double getLeaveHrs() {
		return LeaveHrs;
	}
	public void setLeaveHrs(double leaveHrs) {
		LeaveHrs = leaveHrs;
	}
	public String getShortName() {
		return leaveType.getShortName();
	}
	public String getName() {
		return leaveType.getName();
	}
	public Boolean getCountWeekend() {
		return leaveType.getCountWeekend();
	}
	public Boolean getCountHoliday() {
		return leaveType.getCountHoliday();
	}
	public Boolean getCancellationAllow() {
		return leaveType.getCancellationAllow();
	}
	public Integer getMinDays() {
		return leaveType.getMinDays();
	}
	public Integer getMaxDays() {
		return leaveType.getMaxDays();
	}
	public Boolean getUnpaidLeave() {
		return leaveType.getPaidLeave();
	}
	public Boolean getDocumentRequired() {
		return leaveType.getDocumentRequired();
	}
	public Integer getDoumentRequiredMinDays() {
		return leaveType.getDoumentRequiredMinDays();
	}
	public Boolean getRelapseAllowCarryOver() {
		return leaveType.getRelapseAllowCarryOver();
	}
	public Integer getMaxCarryForward() {
		return leaveType.getMaxCarryForward();
	}
	public Boolean getLeaveEncashable() {
		return leaveType.getLeaveEncashable();
	}
	public Boolean getStatus() {
		return leaveType.getStatus();
	}



	public String getSiteLocation() {
		return siteLocation;
	}



	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}
	
	

}
