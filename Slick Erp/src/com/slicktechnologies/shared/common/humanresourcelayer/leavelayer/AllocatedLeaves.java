package com.slicktechnologies.shared.common.humanresourcelayer.leavelayer;

import java.io.Serializable;
import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;

// TODO: Auto-generated Javadoc
/**
 * Represents Leaves which are going to be allocated.Contains Attributes for Saving 
 * daysAllocated,balance , requested etc.
 * 
 */
@Embed

public class AllocatedLeaves extends LeaveType implements Serializable,Cloneable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6005384139831430755L;


	/**  No of  initial allocated days. */
	Double daysAllocated;
	
	/**  No of Availed Leaves by employee. */
	protected Double availedDays=0d;
	
	/**  No of Balance Leaves. */
	protected Double balance;
	
	/** No of  Requested Leave. */
	protected Double requested=0d;
	
	/** No of  Approved Leave. */
	protected Double approved=0d;
	
	/** No of  Rejected Leave. */
	protected Double rejected=0d;
	
	/** No of  Earned Leave. */
	protected Double earned=0d;
	
	/**
	 * @author Anil ,Date : 08-03-2019
	 * it stores previous opening balance i.e balance leave
	 */
	protected double openingBalance;
	
  
	/**
	 * Instantiates a new allocated leaves.
	 */
	public AllocatedLeaves() {
		super();
		requested=0d;
		approved=0d;
		rejected=0d;
		earned=0d;
		
	}
	
	

	public double getOpeningBalance() {
		return openingBalance;
	}



	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}



	/**
	 * Gets the days allocated.
	 *
	 * @return the days allocated
	 */
	public Double getDaysAllocated() {
		return daysAllocated;
	}

	/**
	 * Sets the days allocated.
	 *
	 * @param daysAllocated the new days allocated
	 */
	public void setDaysAllocated(Double daysAllocated) {
		this.daysAllocated = daysAllocated;
	}

	/**
	 * Gets the availed days.
	 *
	 * @return the availed days
	 */
	public Double getAvailedDays() {
		return availedDays;
	}

	/**
	 * Sets the availed days.
	 *
	 * @param availedDays the new availed days
	 */
	public void setAvailedDays(Double availedDays) {
		availedDays = Math.round(availedDays*100)/100.0d;
		this.availedDays = availedDays;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * Sets the balance.
	 *
	 * @param balance the new balance
	 */
	public void setBalance(Double balance) {
		balance = Math.round(balance*100)/100.0d;
		this.balance = balance;
	}

	/**
	 * Gets the requested.
	 *
	 * @return the requested
	 */
	public Double getRequested() {
		if(requested==null){
			requested=0d;
		}
		requested = Math.round(requested*100)/100.0d;
		return requested;
	}

	/**
	 * Sets the requested.
	 *
	 * @param requested the new requested
	 */
	public void setRequested(Double requested) {
		
		this.requested = requested;
	}
	
	public Double getApproved() {
		if(approved==null){
			approved=0d;
		}
		approved = Math.round(approved*100)/100.0d;
		return approved;
	}

	public void setApproved(Double approved) {
		this.approved = approved;
	}

	public Double getRejected() {
		if(rejected==null){
			rejected=0d;
		}
		rejected = Math.round(rejected*100)/100.0d;
		return rejected;
	}

	public void setRejected(Double rejected) {
		this.rejected = rejected;
	}

	
	public Double getEarned() {
		if(earned==null){
			earned=0d;
		}
		earned = Math.round(earned*100)/100.0d;
		return earned;
	}

	public void setEarned(Double earned) {
		this.earned = earned;
	}

	@GwtIncompatible
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
		} 
	
	
}
