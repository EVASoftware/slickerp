package com.slicktechnologies.shared.common.humanresourcelayer.calendar;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Config Representing Holiday Type.
 */
@Entity
public class HolidayType extends SuperModel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6711875721946592358L;
	
	/** The holiday type name. */
	protected String holidayTypeName;
	
	/** The description. */
	protected String description;
	
	/** The status. */
	@Index
	protected boolean status;
	
	
	

	public HolidayType() {
		super();
		holidayTypeName="";
		description="";
	}

	
	
	
	
	
/**
 * *********************************Getters and Setters***********************************************.
 *
 * @return the holiday type name
 */
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Holiday Type")
	public String getHolidayTypeName() {
		return holidayTypeName;
	}
	
	/**
	 * Sets the holiday type name.
	 *
	 * @param holidayTypeName the new holiday type name
	 */
	public void setHolidayTypeName(String holidayTypeName) {
		if(holidayTypeName!=null)
		this.holidayTypeName = holidayTypeName;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null)
		this.description = description;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		HolidayType entity = (HolidayType) m;
		String name = entity.getHolidayTypeName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.holidayTypeName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	@Override
	public String toString() {
		return holidayTypeName.toString();
	}
	
	

}
