package com.slicktechnologies.shared.common.humanresourcelayer.calendar;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;
import com.googlecode.objectify.annotation.Embed;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.appstructure.SuperModel;

// TODO: Auto-generated Javadoc
/**
 * Represents a Holiday.
 */
@Embed
public class Holiday extends SuperModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4589362086185531485L;
	
	/**  The Name of the holiday in that particular {@link Calendar}. */
	protected String holidayname;
	
	/**  The Holiday Date related to the Holiday Name. */
	protected Date holidaydate;
	
	/** The holiday type. */
	protected String holidayType;
	
	
	/**
	 * Instantiates a new holiday.
	 */
	public Holiday() {
		super();
		holidayname="";
		holidaydate=new Date();
		holidayType="";
	}
	
	
	/**
	 * ******************************************Getters and Setters*************************************************.
	 *
	 * @return the holidayname
	 */
	
	
	/**
	 * Gets the holidayname.
	 *
	 * @return the holidayname
	 */
	public String getHolidayname() {
		return holidayname;
	}
	
	/**
	 * Sets the holidayname.
	 *
	 * @param holidayname the new holidayname
	 */
	public void setHolidayname(String holidayname) {
		if(holidayname!=null){
		this.holidayname = holidayname.trim();}
	}

	/**
	 * Gets the holidaytype.
	 *
	 * @return the holidaytype
	 */

	public String getHolidayType() {
		return holidayType;
	}

	/**
	 * Sets the holidaytype.
	 *
	 * @param holidayType the new holiday type
	 */
	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}


	/**
	 * Gets the holidaydate.
	 *
	 * @return the holidaydate
	 */
	public Date getHolidaydate() {
		return holidaydate;
	}
	
	/**
	 * Sets the holidaydate.
	 *
	 * @param holidaydate the new holidaydate
	 */
	public void setHolidaydate(Date holidaydate) {
		this.holidaydate = holidaydate;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object arg0) {
		
		if(arg0==null)
			return false;
		else if(arg0 instanceof Holiday)
		{
			Holiday holiday=(Holiday) arg0;
			if(((Holiday) arg0).getHolidaydate()==null)
				return false;
			if(this.holidaydate==null)
				return false;
			if(this.holidaydate.equals(((Holiday) arg0).holidaydate))
			
				return false;
		  String holidayName=holiday.getHolidayname();
		  holidayName=holidayName.replaceAll("\\s","");
		  holidayName=holidayName.trim();
		  String temp=this.holidayname;
		  temp=temp.trim();
		  temp=temp.replaceAll("\\s","");
		  temp=temp.toLowerCase();
		  holidayName=holidayName.toLowerCase();
		  if(temp.equals(holidayName))
			  return false;
		  return true;
		}
		return false;
	}


	/********************************************************************************************************************************/
	

	
	
}
