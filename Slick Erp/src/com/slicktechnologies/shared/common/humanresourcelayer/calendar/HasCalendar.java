package com.slicktechnologies.shared.common.humanresourcelayer.calendar;

public interface HasCalendar 
{
	public Calendar getCalendar();
	public void setCalendar(Calendar cal);
  
}
