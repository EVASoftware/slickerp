package com.slicktechnologies.shared.common.humanresourcelayer.calendar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;

// TODO: Auto-generated Javadoc
/**
 * Calendar of Employee,each employee can have different Calendar.
 * Status of Calendar will become expired if Current Date is greater Than fromDate
 * To Do : No relation Level Code from Employee.
 */
@Entity
@Embed
public class Calendar extends SuperModel {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 800574032928144992L;


	/** The Constant CREATED. */
	public static final String CREATED = "Created";
	
	/** The Constant ACTIVE. */
	public static final String ACTIVE = "Active";
	
	/** The Constant Expired. */
	public static final String EXPIRED = "Expired";

	/**  Name of  Calendar. */
	@Index
	protected String calName;
	
	/**  Owner of Calendar. */
	@Index
	protected String calOwnerName;
	
	/**  Start Date of Calendar. */
	
	protected Date calStartDate;
	
	/**  End Date of Calendar. */
	protected Date calEndDate;
	
	
	/** The employee name. */
	protected Key<EmployeeRelation> employeeName;
	
	/** The holiday. */
	@Serialize
	protected ArrayList<Holiday> holiday;
	
	/** The status. */
	@Index
	protected String status;
	
	/** The weeklyoff. */
	@Serialize
	protected WeekleyOff weeklyoff;
	
	/** The working hours. */
	protected double workingHours;

	/**
	 * Instantiates a new leave calendar.
	 */
	protected boolean flexible;
	public Calendar()
	{
		super();
		calName="";
		calOwnerName="";
		calStartDate=new Date();
		calEndDate=new Date();
		holiday=new ArrayList<Holiday>();
		weeklyoff=new WeekleyOff();
	}
	
	
	/**
	 * *************************************Getters and Setters***************************************************.
	 *
	 * @return the cal name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "03", title = "Status", variableName = "status",colspan=0)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Status")
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the cal name.
	 *
	 * @return the cal name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "01", title = "Calendar Name", variableName = "calName",colspan=0)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Calendar Name")
	public String getCalName() {
		return calName;
	}
	
	/**
	 * Sets the cal name.
	 *
	 * @param calName the new cal name
	 */
	public void setCalName(String calName) {
		if(calName!=null)
		this.calName = calName.trim();
	}
	
	/**
	 * Gets the cal owner name.
	 *
	 * @return the cal owner name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXEMPLOYEE, flexFormNumber = "02", title = "Responsible Person", variableName = "calOwnerName",colspan=0)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Responsible Person")
	public String getCalOwnerName() {
		return calOwnerName;
	}
	
	/**
	 * Sets the cal owner name.
	 *
	 * @param calOwnerName the new cal owner name
	 */
	public void setCalOwnerName(String calOwnerName) {
		if(calOwnerName!=null)
		this.calOwnerName = calOwnerName.trim();
	}
	
	/**
	 * Gets the cal start date.
	 *
	 * @return the cal start date
	 */
	public Date getCalStartDate() {
		return calStartDate;
	}
	
	/**
	 * Sets the cal start date.
	 *
	 * @param calStartDate the new cal start date
	 */
	public void setCalStartDate(Date calStartDate) {
		if(calStartDate!=null)
		this.calStartDate = calStartDate;
	}
	
	/**
	 * Gets the cal end date.
	 *
	 * @return the cal end date
	 */
	public Date getCalEndDate() {
		return calEndDate;
	}
	
	/**
	 * Sets the cal end date.
	 *
	 * @param calEndDate the new cal end date
	 */
	public void setCalEndDate(Date calEndDate) {
		if(calEndDate!=null)
		this.calEndDate = calEndDate;
	}
	
	/**
	 * Checks if is caldefault.
	 *
	 * @return the boolean
	 */
	

		/**
		 * Gets the holiday.
		 *
		 * @return the holiday
		 */
	
	public ArrayList<Holiday> getHoliday() {
		return holiday;
	}

	/**
	 * Sets the holiday.
	 *
	 * @param holiday the new holiday
	 */
	public void setHoliday(ArrayList<Holiday> holiday) {
		if(holiday!=null)
		{
		    this.holiday = new ArrayList<Holiday>();
		    this.holiday.addAll(holiday);
		}
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof Calendar)
		{
			Calendar leaveCalendar=(Calendar) arg0;
			if(leaveCalendar.getCalName()!=null)
			{
				return this.calName.compareTo(leaveCalendar.calName);
			}
		}
		return 0;

	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		
		Calendar entity = (Calendar) m;
		String name = entity.getCalName().trim();
		String curname=this.calName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return calName.toString();
	}


	/**
	 * Make object list box live.
	 *
	 * @param leaveCalendar the leave calendar
	 */
	public static void makeObjectListBoxLive(
		ObjectListBox<Calendar> leaveCalendar) 
	{
		MyQuerry myQuerry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(ACTIVE);
		myQuerry.getFilters().add(filter);
		myQuerry.setQuerryObject(new Calendar());
		leaveCalendar.MakeLive(myQuerry);
		
	}


	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList() 
	{
		ArrayList<String>statusListBox=new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(ACTIVE);
		statusListBox.add(EXPIRED);
		return statusListBox;
	}


	  /**
	   * Checks if is holiday date.
	   *
	   * @param date the date
	   * @return true, if is holiday date
	   */
	
	  public boolean isHolidayDate(Date date)
	  {
		  if(holiday!=null)
		  {
			  for(int i=0;i<holiday.size();i++)
			  {
				 Holiday temp= holiday.get(i);
				 if(temp.getHolidaydate().equals(date)){
					 
					 return true;
				 }
			  }
		  }
		  return false;
	  }
	
	 @GwtIncompatible
	  public boolean isHolidayDate1(Date date)
	  {Logger logger=Logger.getLogger("Pay Logger");
		  if(holiday!=null)
		  {
			  for(int i=0;i<holiday.size();i++)
			  {
				 Holiday temp= holiday.get(i);
				 
				 Date holidayDate=getModifiedDate(temp.getHolidaydate());
//				 logger.log(Level.SEVERE," HOLIDAY DATE"+temp.getHolidaydate());
//				 logger.log(Level.SEVERE," REAL HOLIDAY DATE"+holidayDate);
				 if(holidayDate.equals(date)){
					 
					 return true;
				 }
			  }
		  }
		  return false;
	  }

	
	  @GwtIncompatible
	  private Date getModifiedDate(Date date){
		  java.util.Calendar c = java.util.Calendar.getInstance();
			 
			Date holidayDate=DateUtility.getDateWithTimeZone("IST", date);
			c.setTime(holidayDate);
			c.set(java.util.Calendar.HOUR_OF_DAY, 00);
			c.set(java.util.Calendar.MINUTE, 00);
			c.set(java.util.Calendar.SECOND, 00);
			c.set(java.util.Calendar.MILLISECOND, 00);
//			c.add( java.util.Calendar.DATE, 1);
			holidayDate=c.getTime();
		  return holidayDate;
	  }
	  
	  /**
	   * Repersents wo corresponding to a Calendar.
	   */
	  public static class WeekleyOff implements Serializable
	  {
		  
	  	/** The Constant serialVersionUID. */
		private static final long serialVersionUID = -8633252950677090768L;
		
		/** The sataurday. */
		boolean SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THRUSDAY,FRIDAY,SATAURDAY;
	
		/**
		 * Checks if is sunday.
		 *
		 * @return true, if is sunday
		 */
		public boolean isSUNDAY() {
			return SUNDAY;
		}
	
		/**
		 * Sets the sunday.
		 *
		 * @param sUNDAY the new sunday
		 */
		public void setSUNDAY(boolean sUNDAY) {
			SUNDAY = sUNDAY;
		}
	
		/**
		 * Checks if is monday.
		 *
		 * @return true, if is monday
		 */
		public boolean isMONDAY() {
			return MONDAY;
		}
	
		/**
		 * Sets the monday.
		 *
		 * @param mONDAY the new monday
		 */
		public void setMONDAY(boolean mONDAY) {
			MONDAY = mONDAY;
		}
	
		/**
		 * Checks if is tuesday.
		 *
		 * @return true, if is tuesday
		 */
		public boolean isTUESDAY() {
			return TUESDAY;
		}
	
		/**
		 * Sets the tuesday.
		 *
		 * @param tUESDAY the new tuesday
		 */
		public void setTUESDAY(boolean tUESDAY) {
			TUESDAY = tUESDAY;
		}
	
		/**
		 * Checks if is wednesday.
		 *
		 * @return true, if is wednesday
		 */
		public boolean isWEDNESDAY() {
			return WEDNESDAY;
		}
	
		/**
		 * Sets the wednesday.
		 *
		 * @param wEDNESDAY the new wednesday
		 */
		public void setWEDNESDAY(boolean wEDNESDAY) {
			WEDNESDAY = wEDNESDAY;
		}
	
		/**
		 * Checks if is thrusday.
		 *
		 * @return true, if is thrusday
		 */
		public boolean isTHRUSDAY() {
			return THRUSDAY;
		}
	
		/**
		 * Sets the thrusday.
		 *
		 * @param tHRUSDAY the new thrusday
		 */
		public void setTHRUSDAY(boolean tHRUSDAY) {
			THRUSDAY = tHRUSDAY;
		}
	
		/**
		 * Checks if is friday.
		 *
		 * @return true, if is friday
		 */
		public boolean isFRIDAY() {
			return FRIDAY;
		}
	
		/**
		 * Sets the friday.
		 *
		 * @param fRIDAY the new friday
		 */
		public void setFRIDAY(boolean fRIDAY) {
			FRIDAY = fRIDAY;
		}
	
		/**
		 * Checks if is sataurday.
		 *
		 * @return true, if is sataurday
		 */
		public boolean isSATAURDAY() {
			return SATAURDAY;
		}
	
		/**
		 * Sets the sataurday.
		 *
		 * @param sATAURDAY the new sataurday
		 */
		public void setSATAURDAY(boolean sATAURDAY) {
			SATAURDAY = sATAURDAY;
		}

	
		  
		  
	  }

	/**
	 * Checks if is sunday.
	 *
	 * @return true, if is sunday
	 */
	public boolean isSUNDAY() {
		return weeklyoff.isSUNDAY();
	}
	
	
	/**
	 * Sets the sunday.
	 *
	 * @param sUNDAY the new sunday
	 */
	public void setSUNDAY(boolean sUNDAY) {
		weeklyoff.setSUNDAY(sUNDAY);
	}
	
	
	/**
	 * Checks if is monday.
	 *
	 * @return true, if is monday
	 */
	public boolean isMONDAY() {
		return weeklyoff.isMONDAY();
	}
	
	
	/**
	 * Sets the monday.
	 *
	 * @param mONDAY the new monday
	 */
	public void setMONDAY(boolean mONDAY) {
		weeklyoff.setMONDAY(mONDAY);
	}
	
	
	/**
	 * Checks if is tuesday.
	 *
	 * @return true, if is tuesday
	 */
	public boolean isTUESDAY() {
		return weeklyoff.isTUESDAY();
	}
	
	
	/**
	 * Sets the tuesday.
	 *
	 * @param tUESDAY the new tuesday
	 */
	public void setTUESDAY(boolean tUESDAY) {
		weeklyoff.setTUESDAY(tUESDAY);
	}
	
	
	/**
	 * Checks if is wednesday.
	 *
	 * @return true, if is wednesday
	 */
	public boolean isWEDNESDAY() {
		return weeklyoff.isWEDNESDAY();
	}
	
	
	/**
	 * Sets the wednesday.
	 *
	 * @param wEDNESDAY the new wednesday
	 */
	public void setWEDNESDAY(boolean wEDNESDAY) {
		weeklyoff.setWEDNESDAY(wEDNESDAY);
	}
	
	
	/**
	 * Checks if is thrusday.
	 *
	 * @return true, if is thrusday
	 */
	public boolean isTHRUSDAY() {
		return weeklyoff.isTHRUSDAY();
	}
	
	
	/**
	 * Sets the thrusday.
	 *
	 * @param tHRUSDAY the new thrusday
	 */
	public void setTHRUSDAY(boolean tHRUSDAY) {
		weeklyoff.setTHRUSDAY(tHRUSDAY);
	}
	
	
	/**
	 * Checks if is friday.
	 *
	 * @return true, if is friday
	 */
	public boolean isFRIDAY() {
		return weeklyoff.isFRIDAY();
	}
	
	
	/**
	 * Sets the friday.
	 *
	 * @param fRIDAY the new friday
	 */
	public void setFRIDAY(boolean fRIDAY) {
		weeklyoff.setFRIDAY(fRIDAY);
	}
	
	
	/**
	 * Checks if is sataurday.
	 *
	 * @return true, if is sataurday
	 */
	public boolean isSATAURDAY() {
		return weeklyoff.isSATAURDAY();
	}
	
	
	/**
	 * Sets the sataurday.
	 *
	 * @param sATAURDAY the new sataurday
	 */
	public void setSATAURDAY(boolean sATAURDAY) {
		weeklyoff.setSATAURDAY(sATAURDAY);
	}


	/**
	 * Gets the weeklyoff.
	 *
	 * @return the weeklyoff
	 */
	public WeekleyOff getWeeklyoff() {
		return weeklyoff;
	}
	
	
	
	/**
	 * Sets the weeklyoff.
	 *
	 * @param weeklyoff the new weeklyoff
	 */
	public void setWeeklyoff(WeekleyOff weeklyoff) {
		this.weeklyoff = weeklyoff;
	}
	
	
	/**
	 * Gets the employee name.
	 *
	 * @return the employee name
	 */
	public Key<EmployeeRelation> getEmployeeName() {
		return employeeName;
	}
	
	
	/**
	 * Sets the employee name.
	 *
	 * @param employeeName the new employee name
	 */
	public void setEmployeeName(Key<EmployeeRelation> employeeName) {
		this.employeeName = employeeName;
	}
	
	
	/**
	 * Gets the working hours.
	 *
	 * @return the working hours
	 */
	public double getWorkingHours() {
		return workingHours;
	}
	
	
	/**
	 * Sets the working hours.
	 *
	 * @param workingHours the new working hours
	 */
	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}


	public boolean isFlexible() {
		return flexible;
	}


	public void setFlexible(boolean flexible) {
		this.flexible = flexible;
	}


	/**
	 * Method checks weather a Calendar is expired or Not , if yes it sets the status
	 * to Expired
	 */
	 @OnLoad
	public void makeCalendarExpired()
	{
		 /**
		  * Ashwini Patil 
		  * Date:23-05-2022 
		  * Descirption: commented code below as after applying active filter expired calendars were getting displayed in result
		  * Ajinkya reported the issue and Nitin sir told to change the code 
		  */
		  
		 //		if(calEndDate.before(new Date())&&(status.equals(Calendar.EXPIRED)==false))
//		{
//			status=Calendar.EXPIRED;
//		}
	}
  
	 @GwtIncompatible
	 public  boolean isWeekleyOff(Date d)
	 {
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.setTime(d);
		String dayOfWeek = c.get(c.DAY_OF_WEEK)+"";
		
		if(dayOfWeek.equals("1"))
		   return isSUNDAY();
		if(dayOfWeek.equals("2"))
			return isMONDAY();
		if(dayOfWeek.equals("3"))
			return isTUESDAY();
		if(dayOfWeek.equals("4"))
			return isWEDNESDAY();
		if(dayOfWeek.equals("5"))
			return isTHRUSDAY();
		if(dayOfWeek.equals("6"))
			return isFRIDAY();
		if(dayOfWeek.equals("7"))
			return isSATAURDAY();
		return false;
	}
	 
	 
	 
	 	@OnSave
		@GwtIncompatible
		public void reactingOnSave()
		{
	 		System.out.println("INSIDE CALENDAR ENTITY ON SAVE ");
			if(calStartDate!=null){
				System.out.println("INSIDE CALENDAR ENTITY SETTING START DATE ");
				setCalStartDate(DateUtility.getDateWithTimeZone("IST", calStartDate));
			}
			if(calEndDate!=null){
				System.out.println("INSIDE CALENDAR ENTITY SETTING END DATE ");
				setCalEndDate(DateUtility.getDateWithTimeZone("IST", calEndDate));
			}
		}
	 	
	 	
	/**
	 * @author Anil
	 * @since 13-07-2020
	 * @param date
	 * @return
	 */
	public String getHolidayType(Date date) {
		if (holiday != null) {
			for (int i = 0; i < holiday.size(); i++) {
				Holiday temp = holiday.get(i);
				if (temp.getHolidaydate().equals(date)) {
					return temp.getHolidayType();
				}
			}
		}
		return null;
	}
	 
	/**
	 * @author Vijay Date :- 08-02-2023
	 * Des:- This method is calling from contract screen while schedule the services by using caledar
	 */
	 public  boolean checkWeekleyOff(Date d)
	 {
		DateTimeFormat format = DateTimeFormat.getFormat("c");

		String dayOfWeek = format.format(d);
		Console.log("dayOfWeek"+dayOfWeek);
		if(dayOfWeek.equals("0"))
		   return isSUNDAY();
		if(dayOfWeek.equals("1"))
			return isMONDAY();
		if(dayOfWeek.equals("2"))
			return isTUESDAY();
		if(dayOfWeek.equals("3"))
			return isWEDNESDAY();
		if(dayOfWeek.equals("4"))
			return isTHRUSDAY();
		if(dayOfWeek.equals("5"))
			return isFRIDAY();
		if(dayOfWeek.equals("6"))
			return isSATAURDAY();
		
		return false;
	}
	/**
	 * ends here
	 */
  
}
