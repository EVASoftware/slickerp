package com.slicktechnologies.shared.common.humanresourcelayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

@Entity
public class EmployeeCheckListType extends SuperModel {



	/**
	 * 
	 */
	private static final long serialVersionUID = 1534622946163013687L;
	/**
	 * 
	 */
	@Index
	String employeeType;
	@Index 
	CheckListType checkListType;
	@Index
	Boolean status;
	
	public EmployeeCheckListType(){
		employeeType = "";
		checkListType = new CheckListType();
		status  = false;
	}
	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	
	public CheckListType getCheckListType() {
		return checkListType;
	}
	public void setCheckListType(CheckListType checkListType) {
		this.checkListType = checkListType;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
