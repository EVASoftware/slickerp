package com.slicktechnologies.shared.common.humanresourcelayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class VoluntaryPfHistory extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2346205425233621516L;
	
	@Index
	int empId;
	@Index
	String empName;
	@Index
	long empCellNo;
	
	double voluntaryPfPercentage;
	double voluntaryPfAmount;
	double voluntaryPfwageAmount;
	@Index
	String payrollMonth;
	
	String name;
	@Index
	protected String branchName;
	
	/**12-1-2019 added by amol for project name column***/
	protected String projectName;
	
	



	public String getBranchName() {
		return branchName;
	}



	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}



	public VoluntaryPfHistory() {
		// TODO Auto-generated constructor stub
	}
	
	

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public double getVoluntaryPfwageAmount() {
		return voluntaryPfwageAmount;
	}



	public void setVoluntaryPfwageAmount(double voluntaryPfwageAmount) {
		this.voluntaryPfwageAmount = voluntaryPfwageAmount;
	}



	public int getEmpId() {
		return empId;
	}



	public void setEmpId(int empId) {
		this.empId = empId;
	}



	public String getEmpName() {
		return empName;
	}



	public void setEmpName(String empName) {
		this.empName = empName;
	}



	public long getEmpCellNo() {
		return empCellNo;
	}



	public void setEmpCellNo(long empCellNo) {
		this.empCellNo = empCellNo;
	}



	public double getVoluntaryPfPercentage() {
		return voluntaryPfPercentage;
	}



	public void setVoluntaryPfPercentage(double voluntaryPfPercentage) {
		this.voluntaryPfPercentage = voluntaryPfPercentage;
	}



	public double getVoluntaryPfAmount() {
		return voluntaryPfAmount;
	}



	public void setVoluntaryPfAmount(double voluntaryPfAmount) {
		this.voluntaryPfAmount = voluntaryPfAmount;
	}



	public String getPayrollMonth() {
		return payrollMonth;
	}



	public void setPayrollMonth(String payrollMonth) {
		this.payrollMonth = payrollMonth;
	}
	



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
