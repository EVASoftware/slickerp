package com.slicktechnologies.shared.common.humanresourcelayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.OtEarningComponent;

@Entity
public class Esic extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3415951395498143163L;

	@Index
	String esicName;
	@Index
	String esicShortName;
	@Index
	boolean status;
	double employeeContribution;
	double employerContribution;
	ArrayList<OtEarningComponent> compList=new ArrayList<OtEarningComponent>();
	
	double empoyerEsic;
	
	@Index
	Date effectiveFromDate;
	
	double employeeFromAmount;
	double employeeToAmount;
	
	double employerFromAmount;
	double employerToAmount;
	
	ArrayList<EsicContributionPeriod> contributionPeriodList;
	
	boolean calculateOnActual;
	
	
	public Esic() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	
	
	

	public ArrayList<EsicContributionPeriod> getContributionPeriodList() {
		return contributionPeriodList;
	}





	public void setContributionPeriodList(List<EsicContributionPeriod> contributionPeriodList) {
		ArrayList<EsicContributionPeriod> list=new ArrayList<EsicContributionPeriod>();
		list.addAll(contributionPeriodList);
		this.contributionPeriodList = list;
	}





	public double getEmployerFromAmount() {
		return employerFromAmount;
	}





	public void setEmployerFromAmount(double employerFromAmount) {
		this.employerFromAmount = employerFromAmount;
	}





	public double getEmployerToAmount() {
		return employerToAmount;
	}





	public void setEmployerToAmount(double employerToAmount) {
		this.employerToAmount = employerToAmount;
	}





	public double getEmployeeFromAmount() {
		return employeeFromAmount;
	}





	public void setEmployeeFromAmount(double fromAmount) {
		this.employeeFromAmount = fromAmount;
	}





	public double getEmployeeToAmount() {
		return employeeToAmount;
	}





	public void setEmployeeToAmount(double toAmount) {
		this.employeeToAmount = toAmount;
	}





	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}





	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}





	public String getEsicName() {
		return esicName;
	}



	public void setEsicName(String esicName) {
		this.esicName = esicName;
	}



	public String getEsicShortName() {
		return esicShortName;
	}



	public void setEsicShortName(String esicShortName) {
		this.esicShortName = esicShortName;
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	public double getEmployeeContribution() {
		return employeeContribution;
	}



	public void setEmployeeContribution(double employeeContribution) {
		this.employeeContribution = employeeContribution;
	}



	public double getEmployerContribution() {
		return employerContribution;
	}



	public void setEmployerContribution(double employerContribution) {
		this.employerContribution = employerContribution;
	}



	public ArrayList<OtEarningComponent> getCompList() {
		return compList;
	}



	public void setCompList(List<OtEarningComponent> compList) {
		ArrayList<OtEarningComponent> list=new ArrayList<OtEarningComponent>();
		list.addAll(compList);
		this.compList = list;
	}

	public double getEmpoyerEsic() {
		return empoyerEsic;
	}



	public void setEmpoyerEsic(double empoyerEsic) {
		this.empoyerEsic = empoyerEsic;
	}

	

	public boolean isCalculateOnActual() {
		return calculateOnActual;
	}





	public void setCalculateOnActual(boolean calculateOnActual) {
		this.calculateOnActual = calculateOnActual;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
