package com.slicktechnologies.shared.common.humanresourcelayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HasCalendar;

// TODO: Auto-generated Javadoc
/**
 * The Class Department.
 */
@Entity
public class Department extends SuperModel implements HasCalendar{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4571443121844946354L;
	
	/** department name. */
	protected String parentDeptName;
	
	/** child department name. */
	@Index
	protected String deptName;
	
	/**  status of department */
	@Index 
	protected boolean status;
	
	/**  description of department. */
	protected String description;
	
	/** manager's name of department */
	protected String hodName;
	/**
	 * Departments contact information
	 */
	protected  Contact contact;
	/**
	 * 
	 */
	protected String calendarName;
	
	
	protected Calendar calendar;
	
	public Department() {
	
		super();
		parentDeptName="";
		deptName="";
		description="";
		hodName="";
		contact=new Contact();
		calendarName="";
		
	}


	/***************************************Getters and Setters****************************************************/
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Parent Department")
	public String getParentDeptName() {
		return parentDeptName;
	}

	public void setParentDeptName(String parentDeptName) {
		if(parentDeptName!=null)
		  this.parentDeptName = parentDeptName.trim();
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Department")
	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		if(deptName!=null){
		this.deptName = deptName.trim();
	}
	}
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
		this.description = description.trim();
	}}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "H.O.D")
	public String getHodName() {
		return hodName;
	}

	public void setHodName(String hodName) {
		if(hodName!=null){
		this.hodName = hodName.trim();
	}
	}
	
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		if(contact!=null)
		this.contact = contact;
	}

	public Long getCellNo1() {
		return contact.getCellNo1();
	}


	public void setCellNo1(Long cellNo1) {
		contact.setCellNo1(cellNo1);
	}


	public Long getCellNo2() {
		return contact.getCellNo2();
	}


	public void setCellNo2(Long cellNo2) {
		contact.setCellNo2(cellNo2);
	}


	public Long getLandline() {
		return contact.getLandline();
	}


	public void setLandline(Long landline) {
		contact.setLandline(landline);
	}


	public Long getFaxNo() {
		return contact.getFaxNo();
	}


	public void setFaxNo(Long faxNo) {
		contact.setFaxNo(faxNo);
	}


	public String getWebsite() {
		return contact.getWebsite();
	}


	public void setWebsite(String website) {
		contact.setWebsite(website);
	}


	public String getEmail() {
		return contact.getEmail();
	}


	public void setEmail(String email) {
		contact.setEmail(email);
	}


	public Address getAddress() {
		return contact.getAddress();
	}


	public int hashCode() {
		return contact.hashCode();
	}


	public void setAddress(Address address) {
		contact.setAddress(address);
	}
	

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Calendar")
	public String getCalendarName() {
		return calendarName;
	}


	public void setCalendarName(String calendarName) {
		if(calendarName!=null)
		this.calendarName = calendarName;
	}



	
	
public Calendar getCalendar() {
		return calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


/**************************************************************************************************************************/	
	
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof Department)
		{
			Department dep = (Department) arg0;
			if(dep.getDeptName()!=null)
			{
				return this.deptName.compareTo(dep.deptName);
			}
		}
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		Department entity = (Department) m;
		String name = entity.getDeptName().trim();
		String curname=this.deptName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}

		
	
	
	}


	@Override
	public String toString() {
		return deptName.toString();
	}
	
	
	public static void makeDepartMentListBoxLive(ObjectListBox<Department>dep)
	{
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Department());
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		dep.MakeLive(querry);

	}
	
	/*@OnLoad
	@GwtIncompatible
	public void onLoad()
	{
		
		if(calendarName!=null)
		{
		   calendar=ofy().load().type(LeaveCalendar.class).filter("calName",calendarName).first().now();
		   
	    }
	}
	
	@OnSave
	@GwtIncompatible
	public void onSave()
	{
		if(calendar!=null)
		{
			calendarName=calendar.getCalName();
		}
	}*/
}
