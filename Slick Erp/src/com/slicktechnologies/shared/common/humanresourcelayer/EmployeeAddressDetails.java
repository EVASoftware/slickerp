package com.slicktechnologies.shared.common.humanresourcelayer;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class EmployeeAddressDetails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3099091616147986401L;


	
	String addressType;
	String fullAddress;

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
	
	
	

}
