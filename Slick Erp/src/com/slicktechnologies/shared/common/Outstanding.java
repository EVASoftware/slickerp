package com.slicktechnologies.shared.common;

import java.io.Serializable;

public class Outstanding implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2598660536123828599L;
	
	/******************************************Applicability Attributes****************************************/
	int contractId;
	String docType , date  , branch;
	int docID;
	int custId;
	String custName;
	String custCellNo;
	String poc;
	double total , netAmount;
	String status ;
	
	/*******************************************Getters And Setters******************************************/
	
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
	public int getDocID() {
		return docID;
	}
	public void setDocID(int docID) {
		this.docID = docID;
	}
	
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
	public String getCustCellNo() {
		return custCellNo;
	}
	public void setCustCellNo(String custCellNo) {
		this.custCellNo = custCellNo;
	}
	
	public String getPoc() {
		return poc;
	}
	public void setPoc(String poc) {
		this.poc = poc;
	}
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getBranch() {
		return branch ;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public double getNetAmount() {  
		return netAmount;
	}
	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}
	
	
	
	
}
