package com.slicktechnologies.shared.common.attendance;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
@Embed
public class EmpAttendanceDetails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8971479649336426304L;
	
	int empId;
	String empName;
	String empDesignation;
	
	String month;
	String projectName;
	String shiftName;
	
	int totalNoOfDays;
	int totalNoOfPresentDays;
	int totalNoOfHalfDays;
	
	int totalNoOfWeeklyOff;
	int totalNoOfHolidays;
	
	int totalNoOfPaidLeave;
	int totalNoOfUnpdaidLeave;
	
	int totalNoOfSingleOt;
	int totalNoOfDoubleOt;
	
	int totalNoOfWeeklyoffOt;
	int totalNoOfNhOt;
	int totalNoOfPhOt;
	
	String leaveShortName;
	
	EmployeeInfo empInfo;
	LeaveBalance empLeaveBalance;
	HrProject project;
	
	int noOfPl;
	

	public int getEmpId() {
		return empId;
	}



	public void setEmpId(int empId) {
		this.empId = empId;
	}



	public String getEmpName() {
		return empName;
	}



	public void setEmpName(String empName) {
		this.empName = empName;
	}



	public String getEmpDesignation() {
		return empDesignation;
	}



	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}



	public String getMonth() {
		return month;
	}



	public void setMonth(String month) {
		this.month = month;
	}



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public String getShiftName() {
		return shiftName;
	}



	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}



	public int getTotalNoOfDays() {
		return totalNoOfDays;
	}



	public void setTotalNoOfDays(int totalNoOfDays) {
		this.totalNoOfDays = totalNoOfDays;
	}



	public int getTotalNoOfPresentDays() {
		return totalNoOfPresentDays;
	}



	public void setTotalNoOfPresentDays(int totalNoOfPresentDays) {
		this.totalNoOfPresentDays = totalNoOfPresentDays;
	}



	public int getTotalNoOfHalfDays() {
		return totalNoOfHalfDays;
	}



	public void setTotalNoOfHalfDays(int totalNoOfHalfDays) {
		this.totalNoOfHalfDays = totalNoOfHalfDays;
	}



	public int getTotalNoOfWeeklyOff() {
		return totalNoOfWeeklyOff;
	}



	public void setTotalNoOfWeeklyOff(int totalNoOfWeeklyOff) {
		this.totalNoOfWeeklyOff = totalNoOfWeeklyOff;
	}



	public int getTotalNoOfHolidays() {
		return totalNoOfHolidays;
	}



	public void setTotalNoOfHolidays(int totalNoOfHolidays) {
		this.totalNoOfHolidays = totalNoOfHolidays;
	}



	public int getTotalNoOfPaidLeave() {
		return totalNoOfPaidLeave;
	}



	public void setTotalNoOfPaidLeave(int totalNoOfPaidLeave) {
		this.totalNoOfPaidLeave = totalNoOfPaidLeave;
	}



	public int getTotalNoOfUnpdaidLeave() {
		return totalNoOfUnpdaidLeave;
	}



	public void setTotalNoOfUnpdaidLeave(int totalNoOfUnpdaidLeave) {
		this.totalNoOfUnpdaidLeave = totalNoOfUnpdaidLeave;
	}



	public int getTotalNoOfSingleOt() {
		return totalNoOfSingleOt;
	}



	public void setTotalNoOfSingleOt(int totalNoOfSingleOt) {
		this.totalNoOfSingleOt = totalNoOfSingleOt;
	}



	public int getTotalNoOfDoubleOt() {
		return totalNoOfDoubleOt;
	}



	public void setTotalNoOfDoubleOt(int totalNoOfDoubleOt) {
		this.totalNoOfDoubleOt = totalNoOfDoubleOt;
	}



	public int getTotalNoOfWeeklyoffOt() {
		return totalNoOfWeeklyoffOt;
	}



	public void setTotalNoOfWeeklyoffOt(int totalNoOfWeeklyoffOt) {
		this.totalNoOfWeeklyoffOt = totalNoOfWeeklyoffOt;
	}



	public int getTotalNoOfNhOt() {
		return totalNoOfNhOt;
	}



	public void setTotalNoOfNhOt(int totalNoOfNhOt) {
		this.totalNoOfNhOt = totalNoOfNhOt;
	}



	public int getTotalNoOfPhOt() {
		return totalNoOfPhOt;
	}



	public void setTotalNoOfPhOt(int totalNoOfPhOt) {
		this.totalNoOfPhOt = totalNoOfPhOt;
	}

	

	public EmployeeInfo getEmpInfo() {
		return empInfo;
	}



	public void setEmpInfo(EmployeeInfo empInfo) {
		this.empInfo = empInfo;
	}



	public LeaveBalance getEmpLeaveBalance() {
		return empLeaveBalance;
	}



	public void setEmpLeaveBalance(LeaveBalance empLeaveBalance) {
		this.empLeaveBalance = empLeaveBalance;
	}

	


	public String getLeaveShortName() {
		return leaveShortName;
	}



	public void setLeaveShortName(String leaveShortName) {
		this.leaveShortName = leaveShortName;
	}


	

	public HrProject getProject() {
		return project;
	}



	public void setProject(HrProject project) {
		this.project = project;
	}

	


	public int getNoOfPl() {
		return noOfPl;
	}



	public void setNoOfPl(int noOfPl) {
		this.noOfPl = noOfPl;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
