package com.slicktechnologies.shared.common.attendance;

import java.io.Serializable;

public class AttandanceError implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8814763467737940916L;
	/**
	 * 
	 */
	
	int empId;
	String empName;
	String projectName;
	double totalNoOfDays;
	double totalNoOfPresentDays;
	double totalNoOfWO;
	double totalNoOfHoliday;
	double totalNoOfAbsentDays;
	double totalNoOfLeave;
	String remark;
	
	double totalNoOfExtraDays;
	
	public AttandanceError() {
		empName="";
		remark="";
		projectName="";
	}
	
	

	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public double getTotalNoOfExtraDays() {
		return totalNoOfExtraDays;
	}



	public void setTotalNoOfExtraDays(double totalNoOfExtraDays) {
		this.totalNoOfExtraDays = totalNoOfExtraDays;
	}



	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getTotalNoOfDays() {
		return totalNoOfDays;
	}

	public void setTotalNoOfDays(double totalNoOfDays) {
		this.totalNoOfDays = totalNoOfDays;
	}

	public double getTotalNoOfPresentDays() {
		return totalNoOfPresentDays;
	}

	public void setTotalNoOfPresentDays(double totalNoOfPresentDays) {
		this.totalNoOfPresentDays = totalNoOfPresentDays;
	}

	public double getTotalNoOfLeave() {
		return totalNoOfLeave;
	}

	public void setTotalNoOfLeave(double totalNoOfLeave) {
		this.totalNoOfLeave = totalNoOfLeave;
	}

	public double getTotalNoOfWO() {
		return totalNoOfWO;
	}

	public void setTotalNoOfWO(double totalNoOfWO) {
		this.totalNoOfWO = totalNoOfWO;
	}

	public double getTotalNoOfHoliday() {
		return totalNoOfHoliday;
	}

	public void setTotalNoOfHoliday(double totalNoOfHoliday) {
		this.totalNoOfHoliday = totalNoOfHoliday;
	}

	public double getTotalNoOfAbsentDays() {
		return totalNoOfAbsentDays;
	}

	public void setTotalNoOfAbsentDays(double totalNoOfAbsentDays) {
		this.totalNoOfAbsentDays = totalNoOfAbsentDays;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
}
