package com.slicktechnologies.shared.common.attendance;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ProjectNameDetails {
	String projectName;
	Date inTimeForProject;
	Date outTimeForProject;
	double workingHours;
	
	ProjectNameDetails(){
		projectName="";
		inTimeForProject=null;
		outTimeForProject=null;
		workingHours=0;
	}

	/************************************Getters and Setters*********************************************************/
	
	
	
	
	public String getProjectName() {
		return projectName;
	}

	public Date getInTimeForProject() {
		return inTimeForProject;
	}

	public void setInTimeForProject(Date inTimeForProject) {
		this.inTimeForProject = inTimeForProject;
	}

	public Date getOutTimeForProject() {
		return outTimeForProject;
	}

	public void setOutTimeForProject(Date outTimeForProject) {
		this.outTimeForProject = outTimeForProject;
	}

	public double getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	
}
