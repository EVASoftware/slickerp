package com.slicktechnologies.shared.common.attendance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;

@Entity
@Embed
public class AttandanceInfo implements  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3784971919915141340L;



	/**
	 * 
	 */

	int errorCode;
	
	
	
	long companyId;
	String month;
	int empId;
	String empName;
	String empDesignation;
	String projName;
	String shiftName;
	
	String year;
	
	ArrayList<AttandanceDetails> attandList;
	
	String monthYear;
	
	/**
	 * @author Anil , Date :09-05-2019
	 */
	String singleOt;
	String doubleOt;
	boolean otDotFlag;
	
	String comment;
	
	/**
	 * @author Anil
	 * @since 27-07-2020
	 */
	Date startDate;
	Date endDate;
	
	int startLimit;
	int endLimit;
	
	int noOfDays;
	int lastIndexUpdated;
	
	/**
	 * @author Anil
	 * @since 26-08-2020
	 */
	String siteLocation;
	
	public AttandanceInfo() {
		month="";
		empName="";
		empDesignation="";
		projName="";
		shiftName="";
		attandList=new ArrayList<AttandanceDetails>();
		year="";
		monthYear="";
	}
	
	
	
	

	public String getComment() {
		return comment;
	}





	public void setComment(String comment) {
		this.comment = comment;
	}





	public boolean isOtDotFlag() {
		return otDotFlag;
	}




	public void setOtDotFlag(boolean otDotFlag) {
		this.otDotFlag = otDotFlag;
	}




	public String getSingleOt() {
		return singleOt;
	}




	public void setSingleOt(String singleOt) {
		this.singleOt = singleOt;
	}




	public String getDoubleOt() {
		return doubleOt;
	}




	public void setDoubleOt(String doubleOt) {
		this.doubleOt = doubleOt;
	}




	public String getMonthYear() {
		return monthYear;
	}




	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}




	public String getYear() {
		return year;
	}




	public void setYear(String year) {
		this.year = year;
	}




	public long getCompanyId() {
		return companyId;
	}




	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}




	public int getErrorCode() {
		return errorCode;
	}



	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}



	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpDesignation() {
		return empDesignation;
	}

	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	public String getProjName() {
		return projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public ArrayList<AttandanceDetails> getAttandList() {
		return attandList;
	}

	public void setAttandList(ArrayList<AttandanceDetails> attandList) {
		this.attandList = attandList;
	}





	public Date getStartDate() {
		return startDate;
	}





	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}





	public Date getEndDate() {
		return endDate;
	}





	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}





	public int getStartLimit() {
		return startLimit;
	}





	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}





	public int getEndLimit() {
		return endLimit;
	}





	public void setEndLimit(int endLimit) {
		this.endLimit = endLimit;
	}





	public int getNoOfDays() {
		return noOfDays;
	}





	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}





	public int getLastIndexUpdated() {
		return lastIndexUpdated;
	}





	public void setLastIndexUpdated(int lastIndexUpdated) {
		this.lastIndexUpdated = lastIndexUpdated;
	}





	public String getSiteLocation() {
		return siteLocation;
	}





	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}
	
	
	
}
