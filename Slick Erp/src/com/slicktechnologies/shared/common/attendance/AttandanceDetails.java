package com.slicktechnologies.shared.common.attendance;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class AttandanceDetails implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1846426363695840853L;
	Date date;
	String value;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
