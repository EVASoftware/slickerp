package com.slicktechnologies.shared.common.attendance;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class Attendance extends ConcreteBusinessProcess implements ApprovableProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1925245617040100240L;
	
	public static final String ATTENDANCECANCEL = "Cancelled";
	public static final String ATTENDANCECREATED = "Created";
	public static final String ATTENDANCEAPPROVED = "Approved";
	
	@Index
	Date attendanceDate;
	@Index
	String projectNameForOT;
	@Index
	String leaveType;
	@Index
	String overtimeType;
	@Index
	Date inTime;
	@Index
	Date outTime;
	@Index
	String taskName;
	@Index
	boolean isOvertime;
	@Index
	boolean isLeave;
	@Index
	boolean isPresent;
	@Index
	boolean isHalfDay;
	/**
	 * Hours employee have worked including all(OT and all)
	 */
	@Index
	double totalWorkedHours;
	@Index
	double overtimeHours;
	@Index
	int empId;
	@Index
	String employeeName;
	@Index
	String approverName;
	@Index
	String approverRemark;
	@Index
	String status;
	@Index
	String actionLabel;
	@Index
	String shift;
	/**
	 * Hours to be worked
	 */
	@Index
	double workingHours;
	/**
	 * Hours without OT
	 */
	@Index
	double workedHours;
	@Index
	String projectName;
//	double otHours;
	/**
	 * Just for better querry purpose.
	 * MMM-YYYY format
	 */
	@Index
	String month;
	@Index
	Date targetInTime;
	@Index
	Date targetOutTime;
	@Index
	String empDesignation;
	@Index
	double leaveHours;
	@Index
	boolean isEarlyMarkInTime;
	@Index
	boolean isEarlyMarkOutTime;
	@Index
	boolean isLateMarkInTime;
	@Index
	boolean isLateMarkOutTime;
	@Index
	boolean isWeeklyOff;
	
	@Index
	boolean isHoliday;
	
	/** date 10.8.2018 added by komal for requested and approvved OT hours **/
	@Index
	double requestedOTHours;
	@Index
	double approvedOTHOURS;
	@Index
	String otStatus;
	
	/*
	 * Name: Apeksha Gunjal
	 * Date: 15/08/2018 @ 10:30
	 * Note: Added 2 new fields to check when actually in time and out time 
	 * 		save in attendance.
	 */
	@Index
	String serverInTime;
	@Index
	String serverOutTime;
	@Index
	String updatedEmployeeRole;
	@Index
	String updatedEmployeeName;
	/**
	 * Rahul Verma added this on 05 Oct 2018
	 * Description this will store attendance location.
	 */
	@Index
	String markInAddress,markOutAddress;
	
	/**
	 * @author Anil ,Date : 28-03-2019
	 * storing client info,raised by sonu and nitin sir for v allaince
	 */
	@Index
	PersonInfo clientInfo;
	
	/**
	 * @author Anil
	 * @since 28-07-2020
	 * Storing payroll cycle start and end date
	 */
	
	@Index
	Date startDate;
	@Index
	Date endDate;
	
	@Index
	String siteLocation;
	
	String holidayLabel;
	
	public Attendance() {
		super();
		attendanceDate=null;
		projectNameForOT="";
		leaveType="";
		inTime=null;
		outTime=null;
		taskName="";
		isOvertime=false;
		isLeave=false;
		overtimeHours=0;
		empId=0;
		employeeName="";
		approverName="";
		approverRemark="";
		status="";
		actionLabel="";
		overtimeType="";
		shift="";
		workingHours=0;
		projectName="";
		isPresent=false;
//		otHours=0;
		isHalfDay=false;
		workedHours=0;
		targetInTime=null;
		targetOutTime=null;
		empDesignation="";
		leaveHours=0;
		isEarlyMarkInTime=false;
		isEarlyMarkOutTime=false;
		isLateMarkInTime=false;
		isLateMarkOutTime=false;
		isWeeklyOff=false;
		isHoliday=false;
		otStatus="";
		
		/*
		 * Name: Apeksha Gunjal
		 * Date: 15/08/2018 @ 10:30
		 * Note: Added 2 new fields to check when actually in time and out time 
		 * 		save in attendance.
		 */
		serverInTime = null;
		serverOutTime = null;
		updatedEmployeeRole = "";
		updatedEmployeeName = "";
		markInAddress="";
		markOutAddress="";
	}
	

	public PersonInfo getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(PersonInfo clientInfo) {
		this.clientInfo = clientInfo;
	}

	
	
	/*
	 * Name: Apeksha Gunjal
	 * Date: 15/08/2018 @ 10:30
	 * Note: Added getter setter for serverInTime and serverOutTime.
	 */


	public String getServerInTime() {
		return serverInTime;
	}




	public void setServerInTime(String serverInTime) {
		this.serverInTime = serverInTime;
	}
	
	


	public String getServerOutTime() {
		return serverOutTime;
	}


	public void setServerOutTime(String serverOutTime) {
		this.serverOutTime = serverOutTime;
	}


	public boolean isHoliday() {
		return isHoliday;
	}




	public void setHoliday(boolean isHoliday) {
		this.isHoliday = isHoliday;
	}




	public Date getAttendanceDate() {
		return attendanceDate;
	}






	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}






	public String getProjectNameForOT() {
		return projectNameForOT;
	}






	public void setProjectNameForOT(String projectNameForOT) {
		this.projectNameForOT = projectNameForOT;
	}






	public String getLeaveType() {
		return leaveType;
	}






	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}






	public String getOvertimeType() {
		return overtimeType;
	}






	public void setOvertimeType(String overtimeType) {
		this.overtimeType = overtimeType;
	}






	public Date getInTime() {
		return inTime;
	}






	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}






	public Date getOutTime() {
		return outTime;
	}






	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}






	public String getTaskName() {
		return taskName;
	}






	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}






	public boolean isOvertime() {
		return isOvertime;
	}






	public void setOvertime(boolean isOvertime) {
		this.isOvertime = isOvertime;
	}






	public boolean isLeave() {
		return isLeave;
	}






	public void setLeave(boolean isLeave) {
		this.isLeave = isLeave;
	}






	public boolean isPresent() {
		return isPresent;
	}






	public void setPresent(boolean isPresent) {
		this.isPresent = isPresent;
	}






	public boolean isHalfDay() {
		return isHalfDay;
	}






	public void setHalfDay(boolean isHalfDay) {
		this.isHalfDay = isHalfDay;
	}






	public double getTotalWorkedHours() {
		return totalWorkedHours;
	}






	public void setTotalWorkedHours(double totalWorkedHours) {
		this.totalWorkedHours = totalWorkedHours;
	}






	public double getOvertimeHours() {
		return overtimeHours;
	}






	public void setOvertimeHours(double overtimeHours) {
		this.overtimeHours = overtimeHours;
	}






	public int getEmpId() {
		return empId;
	}






	public void setEmpId(int empId) {
		this.empId = empId;
	}






	public String getEmployeeName() {
		return employeeName;
	}






	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}






	public String getApproverName() {
		return approverName;
	}






	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}






	public String getApproverRemark() {
		return approverRemark;
	}






	public void setApproverRemark(String approverRemark) {
		this.approverRemark = approverRemark;
	}






	public String getStatus() {
		return status;
	}






	public void setStatus(String status) {
		this.status = status;
	}






	public String getActionLabel() {
		return actionLabel;
	}






	public void setActionLabel(String actionLabel) {
		this.actionLabel = actionLabel;
	}






	public String getShift() {
		return shift;
	}






	public void setShift(String shift) {
		this.shift = shift;
	}






	public double getWorkingHours() {
		return workingHours;
	}






	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}






	public double getWorkedHours() {
		return workedHours;
	}






	public void setWorkedHours(double workedHours) {
		this.workedHours = workedHours;
	}






	public String getProjectName() {
		return projectName;
	}






	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}






	public String getMonth() {
		return month;
	}






	public void setMonth(String month) {
		this.month = month;
	}






	public Date getTargetInTime() {
		return targetInTime;
	}






	public void setTargetInTime(Date targetInTime) {
		this.targetInTime = targetInTime;
	}






	public Date getTargetOutTime() {
		return targetOutTime;
	}






	public void setTargetOutTime(Date targetOutTime) {
		this.targetOutTime = targetOutTime;
	}






	public String getEmpDesignation() {
		return empDesignation;
	}






	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}






	public double getLeaveHours() {
		return leaveHours;
	}






	public void setLeaveHours(double leaveHours) {
		this.leaveHours = leaveHours;
	}






	public boolean isEarlyMarkInTime() {
		return isEarlyMarkInTime;
	}






	public void setEarlyMarkInTime(boolean isEarlyMarkInTime) {
		this.isEarlyMarkInTime = isEarlyMarkInTime;
	}






	public boolean isEarlyMarkOutTime() {
		return isEarlyMarkOutTime;
	}






	public void setEarlyMarkOutTime(boolean isEarlyMarkOutTime) {
		this.isEarlyMarkOutTime = isEarlyMarkOutTime;
	}






	public boolean isLateMarkInTime() {
		return isLateMarkInTime;
	}






	public void setLateMarkInTime(boolean isLateMarkInTime) {
		this.isLateMarkInTime = isLateMarkInTime;
	}






	public boolean isLateMarkOutTime() {
		return isLateMarkOutTime;
	}






	public void setLateMarkOutTime(boolean isLateMarkOutTime) {
		this.isLateMarkOutTime = isLateMarkOutTime;
	}






	public boolean isWeeklyOff() {
		return isWeeklyOff;
	}






	public void setWeeklyOff(boolean isWeeklyOff) {
		this.isWeeklyOff = isWeeklyOff;
	}




	public double getRequestedOTHours() {
		return requestedOTHours;
	}




	public void setRequestedOTHours(double requestedOTHours) {
		this.requestedOTHours = requestedOTHours;
	}




	public double getApprovedOTHOURS() {
		return approvedOTHOURS;
	}




	public void setApprovedOTHOURS(double approvedOTHOURS) {
		this.approvedOTHOURS = approvedOTHOURS;
	}




	public String getOtStatus() {
		return otStatus;
	}




	public void setOtStatus(String otStatus) {
		this.otStatus = otStatus;
	}





	/*
	 * Name: Apeksha Gunjal
	 * Date: 16/08/2018 @ 15:30
	 * Note: Returned values all fields of attendance entity in string.
	 */
	@Override
	public String toString() {
		return "Attendance [attendanceDate=" + attendanceDate
				+ ", projectNameForOT=" + projectNameForOT + ", leaveType="
				+ leaveType + ", overtimeType=" + overtimeType + ", inTime="
				+ inTime + ", outTime=" + outTime + ", taskName=" + taskName
				+ ", isOvertime=" + isOvertime + ", isLeave=" + isLeave
				+ ", isPresent=" + isPresent + ", isHalfDay=" + isHalfDay
				+ ", totalWorkedHours=" + totalWorkedHours + ", overtimeHours="
				+ overtimeHours + ", empId=" + empId + ", employeeName="
				+ employeeName + ", approverName=" + approverName
				+ ", approverRemark=" + approverRemark + ", status=" + status
				+ ", actionLabel=" + actionLabel + ", shift=" + shift
				+ ", workingHours=" + workingHours + ", workedHours="
				+ workedHours + ", projectName=" + projectName + ", month="
				+ month + ", targetInTime=" + targetInTime + ", targetOutTime="
				+ targetOutTime + ", empDesignation=" + empDesignation
				+ ", leaveHours=" + leaveHours + ", isEarlyMarkInTime="
				+ isEarlyMarkInTime + ", isEarlyMarkOutTime="
				+ isEarlyMarkOutTime + ", isLateMarkInTime=" + isLateMarkInTime
				+ ", isLateMarkOutTime=" + isLateMarkOutTime + ", isWeeklyOff="
				+ isWeeklyOff + ", isHoliday=" + isHoliday
				+ ", requestedOTHours=" + requestedOTHours
				+ ", approvedOTHOURS=" + approvedOTHOURS + ", otStatus="
				+ otStatus + ", serverInTime=" + serverInTime
				+ ", serverOutTime=" + serverOutTime + "]";
	}


	public String getUpdatedEmployeeRole() {
		return updatedEmployeeRole;
	}


	public void setUpdatedEmployeeRole(String updatedEmployeeRole) {
		this.updatedEmployeeRole = updatedEmployeeRole;
	}


	public String getUpdatedEmployeeName() {
		return updatedEmployeeName;
	}


	public void setUpdatedEmployeeName(String updatedEmployeeName) {
		this.updatedEmployeeName = updatedEmployeeName;
	}





	public String getMarkInAddress() {
		return markInAddress;
	}




	public void setMarkInAddress(String markInAddress) {
		this.markInAddress = markInAddress;
	}




	public String getMarkOutAddress() {
		return markOutAddress;
	}




	public void setMarkOutAddress(String markOutAddress) {
		this.markOutAddress = markOutAddress;
	}

	
	
	
	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	

	public String getSiteLocation() {
		return siteLocation;
	}


	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	

	public String getHolidayLabel() {
		return holidayLabel;
	}


	public void setHolidayLabel(String holidayLabel) {
		this.holidayLabel = holidayLabel;
	}


	/**
	 * @author Anil,Date : 28-03-2019
	 * This method fetch client detail from HrProject and store it in PaySlip
	 * raised by sonu and nitin sir for v allicance
	 */
	@com.googlecode.objectify.annotation.OnSave
	@GwtIncompatible
	private void getClientDetailsFromProject(){
		if(id==null){
			if(projectName!=null&&!projectName.equals("")){
				HrProject project=ofy().load().type(HrProject.class).filter("companyId", this.companyId).filter("projectName", this.projectName).first().now();
				if(project!=null&&project.getPersoninfo()!=null){
					setClientInfo(project.getPersoninfo());
				}
			}
		}
	}
	



}
