package com.slicktechnologies.shared.common.attendance;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
@Entity
@Embed
public class EmployeeLeaveBalance implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4898356422997666109L;
	int empId;
	String empName;
	String leaveName;
	String leaveShortName;
	double openingBalance;
	double availedDays;
	double earnedDays;
	double balanceDays;
	long companyId;
	
	public EmployeeLeaveBalance(){
		
	}
	
	public EmployeeLeaveBalance(int empId,String empName,String leaveName,String leaveShortName,double openingBalance,double availedDays,double earnedDays,double balanceDays,long companyId) {
		// TODO Auto-generated constructor stub
		this.empId=empId;
		this.empName=empName;
		this.leaveName=leaveName;
		this.leaveShortName=leaveShortName;
		this.openingBalance=openingBalance;
		this.availedDays=availedDays;
		this.earnedDays=earnedDays;
		this.balanceDays=balanceDays;
		this.companyId=companyId;
	}


	public int getEmpId() {
		return empId;
	}


	public void setEmpId(int empId) {
		this.empId = empId;
	}


	public String getEmpName() {
		return empName;
	}


	public void setEmpName(String empName) {
		this.empName = empName;
	}


	public String getLeaveName() {
		return leaveName;
	}


	public void setLeaveName(String leaveName) {
		this.leaveName = leaveName;
	}


	public String getLeaveShortName() {
		return leaveShortName;
	}


	public void setLeaveShortName(String leaveShortName) {
		this.leaveShortName = leaveShortName;
	}


	public double getOpeningBalance() {
		return openingBalance;
	}


	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}


	public double getAvailedDays() {
		return availedDays;
	}


	public void setAvailedDays(double availedDays) {
		this.availedDays = availedDays;
	}


	public double getEarnedDays() {
		return earnedDays;
	}


	public void setEarnedDays(double earnedDays) {
		this.earnedDays = earnedDays;
	}


	public double getBalanceDays() {
		return balanceDays;
	}


	public void setBalanceDays(double balanceDays) {
		this.balanceDays = balanceDays;
	}


	public long getCompanyId() {
		return companyId;
	}


	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	
	
	
	

}
