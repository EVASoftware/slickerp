package com.slicktechnologies.shared.common.attendance;

import java.io.Serializable;
import java.util.Date;
import java.util.TreeMap;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;

@Embed
public class AttendanceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2707433745938597117L;
	/**
	 * 
	 */
	String overtimeType;
	int empId;
	String employeeName;	
	String projectName;
	/**
	 * Just for better querry purpose.
	 * MMM-YYYY format
	 */
	String month;
	String empDesignation;
	double totalWorkedHours;
	boolean isPresent;
	String leaveType;
	
	@Serialize
	@EmbedMap
	TreeMap<Date, String> attendanceMap;
	
	@Serialize
	@EmbedMap
	TreeMap<Date, Attendance> attendanceObjectMap;
	
	/**
	 * @author Anil,Date :24-01-2019
	 * Here we stores the total otHours in month
	 */
//	@Ignore
//	double totalOtHours;
	String shift ;
	
	int daysArray[];
	
	Date fromDate;
	Date toDate;
	String branchName;
	
	/**
	 * @author Anil
	 * @since 27-08-2020
	 */
	String siteLocation;
	long companyId;
	
	/**
	 * @author Ashwini Patil
	 * @since 07-05-2022
	 */
	String workedDays;
	String leaveDays;

	public AttendanceBean() {
		super();
		
		empId=0;
		employeeName="";		
		overtimeType="";		
		projectName="";
		empDesignation="";
		attendanceMap = new TreeMap<Date, String>();
		isPresent=false;
		leaveType="";
//		totalOtHours=0;
		shift = "";
		attendanceObjectMap = new TreeMap<Date, Attendance>();
		
		fromDate=null;
		toDate=null;
		branchName="";
		workedDays="";
		leaveDays="";
	}
	
	
	

//	public double getTotalOtHours() {
//		return totalOtHours;
//	}
//
//
//
//
//	public void setTotalOtHours(double totalOtHours) {
//		this.totalOtHours = totalOtHours;
//	}




	public String getOvertimeType() {
		return overtimeType;
	}

	public void setOvertimeType(String overtimeType) {
		this.overtimeType = overtimeType;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getEmpDesignation() {
		return empDesignation;
	}

	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	public TreeMap<Date, String> getAttendanceMap() {
		return attendanceMap;
	}

	public void setAttendanceMap(TreeMap<Date, String> attendanceMap) {
		this.attendanceMap = attendanceMap;
	}

	public double getTotalWorkedHours() {
		return totalWorkedHours;
	}

	public void setTotalWorkedHours(double totalWorkedHours) {
		this.totalWorkedHours = totalWorkedHours;
	}

	public boolean isPresent() {
		return isPresent;
	}

	public void setPresent(boolean isPresent) {
		this.isPresent = isPresent;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}




	public String getShift() {
		return shift;
	}




	public void setShift(String shift) {
		this.shift = shift;
	}




	public TreeMap<Date, Attendance> getAttendanceObjectMap() {
		return attendanceObjectMap;
	}




	public void setAttendanceObjectMap(TreeMap<Date, Attendance> attendanceObjectMap) {
		this.attendanceObjectMap = attendanceObjectMap;
	}




	public int[] getDaysArray() {
		return daysArray;
	}




	public void setDaysArray(int[] daysArray) {
		this.daysArray = daysArray;
	}




	public Date getFromDate() {
		return fromDate;
	}




	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}




	public Date getToDate() {
		return toDate;
	}




	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}




	public String getBranchName() {
		return branchName;
	}




	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}




	public String getSiteLocation() {
		return siteLocation;
	}




	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}




	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	public String getWorkedDays() {
		return workedDays;
	}

	public void setWorkedDays(String workedDays) {
		this.workedDays = workedDays;
	}

	public String getLeaveDays() {
		return leaveDays;
	}

	public void setLeaveDays(String leaveDays) {
		this.leaveDays = leaveDays;
	}
	
}
