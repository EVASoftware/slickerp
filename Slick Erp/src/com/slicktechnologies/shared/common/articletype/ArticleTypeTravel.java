package com.slicktechnologies.shared.common.articletype;

import com.simplesoftwares.client.library.appstructure.SuperModel;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
@Embed


public class ArticleTypeTravel extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5081173937284054566L;

	
	@Index
	protected String articleTypeName;
	@Index
	protected String articleTypeValue;
	@Index
	protected int validity;
	@Index
	protected String issuedAt;
	@Index
	protected String issuedBy;
	@Index
	protected Date issueDate;
	
	@Index
	protected Date validityuntil;
	
	@Index
	protected String status;
	
	public ArticleTypeTravel() {
		super();
		articleTypeName="";
		articleTypeValue="";
		issuedAt="";
		issuedBy="";
		issueDate=new Date();
		validityuntil=new Date();
		status="";
	}
	
	
	
	

	public String getStatus() {
		return status;
	}





	public void setStatus(String status) {
		this.status = status;
	}





	public Date getValidityuntil() {
		return validityuntil;
	}





	public void setValidityuntil(Date validityuntil) {
		this.validityuntil = validityuntil;
	}





	public String getArticleTypeName() {
		return articleTypeName;
	}





	public void setArticleTypeName(String articleTypeName) {
		this.articleTypeName = articleTypeName;
	}





	public String getArticleTypeValue() {
		return articleTypeValue;
	}





	public void setArticleTypeValue(String articleTypeValue) {
		this.articleTypeValue = articleTypeValue;
	}





	public int getValidity() {
		return validity;
	}





	public void setValidity(int validity) {
		this.validity = validity;
	}





	public String getIssuedAt() {
		return issuedAt;
	}





	public void setIssuedAt(String issuedAt) {
		this.issuedAt = issuedAt;
	}





	public String getIssuedBy() {
		return issuedBy;
	}





	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}





	public Date getIssueDate() {
		return issueDate;
	}





	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


}
