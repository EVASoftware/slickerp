package com.slicktechnologies.shared.common.articletype;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


@Embed
public class ArticleType extends SuperModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1760503682281257912L;
	
	/*****************************************Applicability Attributes************************************/
	
	@Index
	protected String articleTypeName;
	@Index
	protected String articleTypeValue;
	protected String articleDescription;
	@Index
	protected String articlePrint;
	protected String documentName;
	
	/********************************************Constructor********************************************/
	
	public ArticleType() {
		super();
		articleTypeName="";
		articleTypeValue="";
		articleDescription="";
		articlePrint="";
		documentName="";
	}
	
	/**************************************Getters And Setters*****************************************/
	
	
	public String getArticleTypeName() {
		return articleTypeName;
	}

	public void setArticleTypeName(String articleTypeName) {
		if(articleTypeName!=null){
			this.articleTypeName = articleTypeName;
		}
	}

	public String getArticleTypeValue() {
		return articleTypeValue;
	}

	public void setArticleTypeValue(String articleTypeValue) {
		if(articleTypeValue!=null){
			this.articleTypeValue = articleTypeValue;
		}
	}

	public String getArticleDescription() {
		return articleDescription;
	}

	public void setArticleDescription(String articleDescription) {
		if(articleDescription!=null){
			this.articleDescription = articleDescription.trim();
		}
	}

	public String getArticlePrint() {
		return articlePrint;
	}

	public void setArticlePrint(String articlePrint) {
		if(articlePrint!=null){
			this.articlePrint = articlePrint.trim();
		}
	}
	

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		if(documentName!=null){
			this.documentName = documentName.trim();
		}
	}

/****************************************Overridden Method******************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}



}
