package com.slicktechnologies.shared.common.cronjobcongiration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


@Entity
public class CronJobConfigration extends SuperModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6172411298056596321L;
	/**
	 * nidhi
	 * 23-12-2017.
	 * email reminder configration details
	 */

	

	@Index
	protected ArrayList<CronJobConfigrationDetails> cronJobsProcessList;
	@Index
	protected String cronJobsName;
	@Index
	protected boolean configStatus;
	
	Date lastUpdatedDate; //Ashwini Patil Date:11-12-2023 to check when to deactivate evausagereportcronjob
	String updatedBy; //Ashwini Patil Date:11-12-2023

	/***********************************Constructor********************************************/

	public CronJobConfigration(){
		super();
		cronJobsProcessList=new ArrayList<CronJobConfigrationDetails>();
		cronJobsName="";
	}
	
	/**************************************Getters And Setters************************************/
	
	public List<CronJobConfigrationDetails> getCronJobsProcessList() {
		return cronJobsProcessList;
	}

	public void setCronJobsNameList(List<CronJobConfigrationDetails> cronJobsNameList) {
		ArrayList<CronJobConfigrationDetails> arrProcess=new ArrayList<CronJobConfigrationDetails>();
		arrProcess.addAll(cronJobsNameList);
		this.cronJobsProcessList = arrProcess;
	}

	

	public String getCronJobsName() {
		return cronJobsName;
	}

	public void setCronJobsName(String cronJobsName) {
		this.cronJobsName = cronJobsName;
	}

	public Boolean isConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(boolean configStatus) {
		this.configStatus = configStatus;
	}
	
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		CronJobConfigration entity = (CronJobConfigration) m;
		String name = entity.getCronJobsName().trim();
		String curname = this.cronJobsName.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();

		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}

		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}
	

}
