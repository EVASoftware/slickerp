package com.slicktechnologies.shared.common.cronjobcongiration;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class CronJobConfigrationDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6991978062778279782L;

	/**
	 * 
	 */

	/**
	 * nidhi
	 * 23-12-2017.
	 * email reminder configration details
	 */
	@Index
	public String cronJobName;
	
	@Index
	public String employeeRole;
	@Index
	public String frequencyType;
	@Index
	public int frequencyDay;
	@Index
	public int interval;
	@Index
	public boolean overdueStatus;
	@Index
	public int overdueDays;
	@Index
	public String subject;
	public String mailBody;
	public String footer;
	@Index
	protected boolean status;
	@Index
	protected boolean companyEmail;
	@Index
	protected String customerGroup;
	@Index
	protected Date createdDate;
	
	public Date StartingDate;  //added by Ashwini
	
	public int startDay;
	public int endDay;
	public String communicationChannel;
	public String templateName;
	
	public CronJobConfigrationDetails() {
		// TODO Auto-generated constructor stub
		footer ="";
		mailBody = "";
		subject = "";
		customerGroup = "";
		frequencyType = "";
		employeeRole = "";
		overdueDays = 0;
		interval = 0;
		companyEmail = true;
		StartingDate = createdDate;
		communicationChannel ="";
		templateName ="";
	}


	public String getEmployeeRole() {
		return employeeRole;
	}


	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}


	public String getFrequencyType() {
		return frequencyType;
	}


	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}

	 /**
		 * Date:07/07/2018
		 * Developer:Ashwini
		 * @return
		 */
		public Date getStartingDate(){
			if(this.StartingDate == null){
				return this.createdDate;
			}else{
				return this.StartingDate;
			}
		}

		public void setStartingDate(Date StartingDate){
			this.StartingDate=StartingDate;
			
		}
		
		 /*
		 * End by Ashwini
		 */

	public int getInterval() {
		return interval;
	}


	public void setInterval(int interval) {
		this.interval = interval;
	}


	public boolean isOverdueStatus() {
		return overdueStatus;
	}


	public void setOverdueStatus(boolean overdueStatus) {
		this.overdueStatus = overdueStatus;
	}


	public int getOverdueDays() {
		return overdueDays;
	}


	public void setOverdueDays(int overdueDays) {
		this.overdueDays = overdueDays;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getMailBody() {
		return mailBody;
	}


	public void setMainBody(String mailBody) {
		this.mailBody = mailBody;
	}


	public String getFooter() {
		return footer;
	}


	public void setFooter(String footer) {
		this.footer = footer;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public String getCronJobName() {
		return cronJobName;
	}


	public void setCronJobName(String cronJobName) {
		this.cronJobName = cronJobName;
	}


	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}


	public boolean isCompanyEmail() {
		return companyEmail;
	}


	public void setCompanyEmail(boolean companyEmail) {
		this.companyEmail = companyEmail;
	}


	public String getCustomerGroup() {
		return customerGroup;
	}


	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}


	public int getFrequencyDay() {
		return frequencyDay;
	}


	public void setFrequencyDay(int frequencyDay) {
		this.frequencyDay = frequencyDay;
	}


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public int getStartDay() {
		return startDay;
	}


	public void setStartDay(int startDay) {
		this.startDay = startDay;
	}


	public int getEndDay() {
		return endDay;
	}


	public void setEndDay(int endDay) {
		this.endDay = endDay;
	}


	public String getCommunicationChannel() {
		return communicationChannel;
	}


	public void setCommunicationChannel(String communicationChannel) {
		this.communicationChannel = communicationChannel;
	}


	public String getTemplateName() {
		return templateName;
	}


	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	
	
	
	
}
