package com.slicktechnologies.shared.common.registration;

import java.util.Date;

import com.googlecode.objectify.annotation.Cache;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Contact;
@Cache
public class RegisteredCustomer extends SuperModel
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4562286932454351433L;
	private static final String ACTIVE="Active";
	private static final String DEMO="Demo";
	private static final String EXPIRED="Expired";
	
	protected Date creationDate;
	protected int noOfUser;
	protected int noOfWeeks;
	protected String status;
	protected String userId;
	protected String passWord;
	protected Company company;
	
	
	
	
	public RegisteredCustomer() 
	{
		noOfUser=1;
		status="";
		
	}

	
	
	
	
	
	
	







	public Date getCreationDate() {
		return creationDate;
	}















	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}















	public int getNoOfUser() {
		return noOfUser;
	}















	public void setNoOfUser(int noOfUser) {
		this.noOfUser = noOfUser;
	}















	public int getNoOfWeeks() {
		return noOfWeeks;
	}















	public void setNoOfWeeks(int noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}















	public String getStatus() {
		return status;
	}















	public void setStatus(String status) {
		this.status = status;
	}















	public static String getActive() {
		return ACTIVE;
	}















	public static String getDemo() {
		return DEMO;
	}















	public static String getExpired() {
		return EXPIRED;
	}















	@Override
	public int compareTo(SuperModel arg0) {
		RegisteredCustomer cust=(RegisteredCustomer) arg0;
		if(cust!=null)
		{
		  if(cust.creationDate!=null)
			  return this.creationDate.compareTo(cust.creationDate );
	
		}
		return -1;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		if(m.getCount()==this.count)
			return true;
		return false;
	}
   
}
