package com.slicktechnologies.shared.common.salesorder;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class OtherChargesMargin extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3676280294549792469L;
	
	int productId;
	String productName;
	double otherCharges;
	String remark;
	int productSrNo;
	boolean isReadOnly;
	String costType;
	
	
	public int getProductId() {
		return productId;
	}


	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	
	
	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public double getOtherCharges() {
		return otherCharges;
	}



	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getProductSrNo() {
		return productSrNo;
	}


	public void setProductSrNo(int productSrNo) {
		this.productSrNo = productSrNo;
	}
	
	

	public boolean isReadOnly() {
		return isReadOnly;
	}


	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}
	
	


	public String getCostType() {
		return costType;
	}


	public void setCostType(String costType) {
		this.costType = costType;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
