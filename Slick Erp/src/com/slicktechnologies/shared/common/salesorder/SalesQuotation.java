package com.slicktechnologies.shared.common.salesorder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.QuotationServiceImplentor;
import com.slicktechnologies.server.SalesQuotationServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class SalesQuotation extends Sales implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6141057687971026946L;
	
	public final static String  SALESQUOTATIONSUCESSFUL="Successful";
	public final static String  SALESQUOTATIONUNSUCESSFUL="UnSuccessful";
	public final static String  QUOTATIONREVISED="Revised";
	
	
	/*************************************Entity Attributes********************************************/
	
	/** The total Amount to be paid */
	protected double netpayable;

	/** The valid until. Expiry date of the quotation*/
	protected Date validUntill;

	/**Priority Of Quotation*/
	@Index
	protected String priority;
	
	protected Date deliveryDate;
//	@Index
//	protected boolean checkAddress;
	@Index
	protected Address shippingAddress;
	
	protected boolean checkAddress;
	
	
	/*********************************Attributes for Security Deposit PopUp*************************************/
	
	/** ID of current document */
	@Index
	protected int sdDocId;
	
	/** Status of current document */
	@Index
	protected String sdDocStatus;
	
	/** Date of current Document */
	protected Date sdDocDate;
	
	@Index
	protected Date quotationDate;
	
	/** Field to identify if quotation includes the pay of deposit amount  */
	@Index
	protected String isDepositPaid;
	
	/** Represents the amount paid as deposit */
	@Index
	protected double sdDepositAmount;
	
	/** Represents the payment method of deposit amount */
	@Index
	protected String sdPaymentMethod;
	
	/** Represents the return of deposit amount in case of quotation not accepted */
	@Index
	protected Date sdReturnDate;
	
	/** Field to identify whether deposit has been returned or not */
	@Index
	protected boolean isDepositReceived;
	
	/** Deposit Bank Information */
	
	protected String sdBankAccNo;
	protected String sdBankName;
	protected String sdBankBranch;
	protected String sdPayableAt;
	protected String sdFavouring;
	protected int sdChequeNo;
	protected Date sdChequeDate;
	protected String sdReferenceNo;
	protected String sdIfscCode;
	protected String sdMicrCode;
	protected String sdSwiftCode;
	protected Address sdAddress;
	protected String sdInstruction;
	
	/**
	 *  Represents the field to add remark/comment if deposit amount is received.
	 *  Saved while marking as payment received (On PopUp of List)
	 */
	
	protected String sdRemark;

	
	
	/**************************************************Relational Part************************************************************/
    protected Key<Config>keySalesQuotationPriority;
    protected boolean statusChanged=false;
	private Date startDate;
	private Date endDate;
	
	

	/** Date 18-09-2017 added by vijay for total amount after tax before other charges  *****/
	  protected double inclutaxtotalAmount;

	  /** Date 09/03/2018 added by komal for followup date **/
	  @Index
	  private Date followUpDate;

	  
    /**
	 * Date 08/06/2018 By vijay
	 * Des :- for before round off total final Amt and Round off Amount
	 */
	protected double totalFinalAmount;
	protected double roundOffAmount;
	/**
	 * ends here
	 */
	
	/**
	 * Date 11-08-2018 By Vijay
	 * Des :- revised quotation old quotation id in new quotation 
	 */
	protected int oldQuotationId;
	/**
	 * ends here
	 */
	
	/**
	 * @author Anil
	 * @since 16-07-2020
	 * For PTSPL raised by Rahul Tiwari
	 * calculating vendor and other charges margin
	 */
	
	ArrayList<VendorMargin> vendorMargins;
	ArrayList<OtherChargesMargin> otherChargesMargins;
	
	double totalVendorMargin;
	double totalOtherChargesMargin;
	@Index
	String customerBranch;
	Address custBillingAddress;
	
	protected ArrayList<TermsAndConditions> termsAndConditionlist;
	protected boolean printAllTermsConditions;
	
	protected String paymentModeName;
	
	/**************************************************Default Ctor**********************************************/
	/**
	 * Instantiates a new quotation.
	 */
	public SalesQuotation()
	{
		super();
		priority="";
		this.status=CREATED;
		statusChanged=false;
		deliveryDate=new Date();
		shippingAddress=new Address();
		sdDocStatus="";
		isDepositPaid="";
		sdPaymentMethod="";
		sdBankAccNo="";
		sdBankName="";
		sdBankBranch="";
		sdPayableAt="";
		sdFavouring="";
		sdIfscCode="";
		sdMicrCode="";
		sdSwiftCode="";
		sdAddress=new Address();
		sdInstruction="";
		sdRemark="";
		sdReferenceNo="";
		customerBranch = "";
		custBillingAddress = new Address();
		
		termsAndConditionlist = new ArrayList<TermsAndConditions>();
		printAllTermsConditions = false;
		
		paymentModeName ="";
	}
	
	
	


	/********************************************Getters And Setters***********************************/
	
	public double getNetpayable() {
		return netpayable;
	}


	public void setNetpayable(Double netpayable) {
		if(netpayable!=null){
		this.netpayable = netpayable;
		}
	}


	public Date getValidUntill() {
		return validUntill;
	}


	public void setValidUntill(Date validUntill) {
		this.validUntill = validUntill;
	}


	public String getPriority() {
		return priority;
	}


	public void setPriority(String priority) {
		if(priority!=null){
		this.priority = priority.trim();
		}
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(Date deliveryDate) {
		if(deliveryDate!=null){
		this.deliveryDate = deliveryDate;
		}
	}
	
	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
//	public Boolean getCheckAddress() {
//		return checkAddress;
//	}
//
//
//	public void setCheckAddress(boolean checkAddress) {
//		this.checkAddress = checkAddress;
//	}
	
	

	public Boolean getCheckAddress() {
		return checkAddress;
	}


	public void setCheckAddress(boolean checkAddress) {
		this.checkAddress = checkAddress;
	}

	public Date getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(Date quotationDate) {
		if(quotationDate!=null){
			this.quotationDate = quotationDate;
		}
	}


	/*************************************Getters And Setters For Security Deposit*****************************/

	public int getSdDocId() {
		return sdDocId;
	}

	public void setSdDocId(int sdDocId) {
		this.sdDocId = sdDocId;
	}


	public Date getSdDocDate() {
		return sdDocDate;
	}


	public void setSdDocDate(Date sdDocDate) {
		this.sdDocDate = sdDocDate;
	}


	public String getSdDocStatus() {
		return sdDocStatus;
	}


	public void setSdDocStatus(String sdDocStatus) {
		this.sdDocStatus = sdDocStatus;
	}


	public String getIsDepositPaid() {
		return isDepositPaid;
	}


	public void setIsDepositPaid(String isDepositPaid) {
		this.isDepositPaid = isDepositPaid;
	}


	public double getSdDepositAmount() {
		return sdDepositAmount;
	}


	public void setSdDepositAmount(double sdDepositAmount) {
		this.sdDepositAmount = sdDepositAmount;
	}


	public String getSdPaymentMethod() {
		return sdPaymentMethod;
	}


	public void setSdPaymentMethod(String sdPaymentMethod) {
		this.sdPaymentMethod = sdPaymentMethod;
	}


	public Date getSdReturnDate() {
		return sdReturnDate;
	}


	public void setSdReturnDate(Date sdReturnDate) {
		this.sdReturnDate = sdReturnDate;
	}


	public Boolean isDepositReceived() {
		return isDepositReceived;
	}


	public void setDepositReceived(boolean isDepositReceived) {
		this.isDepositReceived = isDepositReceived;
	}


	public String getSdBankAccNo() {
		return sdBankAccNo;
	}


	public void setSdBankAccNo(String sdBankAccNo) {
		this.sdBankAccNo = sdBankAccNo;
	}


	public String getSdBankName() {
		return sdBankName;
	}


	public void setSdBankName(String sdBankName) {
		this.sdBankName = sdBankName;
	}


	public String getSdBankBranch() {
		return sdBankBranch;
	}


	public void setSdBankBranch(String sdBankBranch) {
		this.sdBankBranch = sdBankBranch;
	}


	public String getSdPayableAt() {
		return sdPayableAt;
	}


	public void setSdPayableAt(String sdPayableAt) {
		this.sdPayableAt = sdPayableAt;
	}


	public String getSdFavouring() {
		return sdFavouring;
	}


	public void setSdFavouring(String sdFavouring) {
		this.sdFavouring = sdFavouring;
	}


	public int getSdChequeNo() {
		return sdChequeNo;
	}


	public void setSdChequeNo(int sdChequeNo) {
		this.sdChequeNo = sdChequeNo;
	}


	public Date getSdChequeDate() {
		return sdChequeDate;
	}


	public void setSdChequeDate(Date sdChequeDate) {
		this.sdChequeDate = sdChequeDate;
	}


	public String getSdReferenceNo() {
		return sdReferenceNo;
	}


	public void setSdReferenceNo(String sdReferenceNo) {
		if(sdReferenceNo!=null){
			this.sdReferenceNo = sdReferenceNo.trim();
		}
	}


	public String getSdIfscCode() {
		return sdIfscCode;
	}


	public void setSdIfscCode(String sdIfscCode) {
		this.sdIfscCode = sdIfscCode;
	}


	public String getSdMicrCode() {
		return sdMicrCode;
	}


	public void setSdMicrCode(String sdMicrCode) {
		this.sdMicrCode = sdMicrCode;
	}


	public String getSdSwiftCode() {
		return sdSwiftCode;
	}


	public void setSdSwiftCode(String sdSwiftCode) {
		this.sdSwiftCode = sdSwiftCode;
	}


	public Address getSdAddress() {
		return sdAddress;
	}


	public void setSdAddress(Address sdAddress) {
		this.sdAddress = sdAddress;
	}


	public String getSdInstruction() {
		return sdInstruction;
	}


	public void setSdInstruction(String sdInstruction) {
		this.sdInstruction = sdInstruction;
	}
	
	public String getSdRemark() {
		return sdRemark;
	}

	public void setSdRemark(String sdRemark) {
		this.sdRemark = sdRemark;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessprocesslayer.ConcreteBuisnessProcess#compareTo(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel o) {
		SalesQuotation quot=(SalesQuotation) o;
		return quot.creationDate.compareTo(creationDate);
	}


	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessprocesslayer.ConcreteBuisnessProcess#isDuplicate(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getStatus() {
		return super.getStatus();
	}
	
	
	
	public double getInclutaxtotalAmount() {
		return inclutaxtotalAmount;
	}

	public void setInclutaxtotalAmount(double inclutaxtotalAmount) {
		this.inclutaxtotalAmount = inclutaxtotalAmount;
	}
	


	public Date getFollowUpDate() {
		return followUpDate;
	}


	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}

	
	
	public double getTotalFinalAmount() {
		return totalFinalAmount;
	}


	public void setTotalFinalAmount(double totalFinalAmount) {
		this.totalFinalAmount = totalFinalAmount;
	}


	public double getRoundOffAmount() {
		return roundOffAmount;
	}


	public void setRoundOffAmount(double roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}
	

	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void manageSalesQuotationRelation()
	{
			keySalesQuotationPriority=MyUtility.getConfigKeyFromCondition(priority,ConfigTypes.QUOTATIONPRIORITY.getValue());	
	}
	
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{	

	}
	/*********************************************************************************************************************/

	
	/**
	 * Date 10-Aug-2018 below code updated 
	 * Des :- Lead status will changed as "Quotation Created", when we create quotation against it
	 */
	@OnSave
	@GwtIncompatible
	private void changeLeadStatus() {
		
		if(this.id==null && this.getLeadCount()!=0){

			Lead lead = ofy().load().type(Lead.class).filter("companyId", this.getCompanyId()).filter("count", this.getLeadCount()).first().now();
			if(lead!=null && !lead.getStatus().equals("Quotation Sent")){
					List<Config> leadStatusList = ofy().load().type(Config.class).filter("type",6).filter("companyId", this.getCompanyId()).list();
					boolean flag = false;
					for(Config config :leadStatusList){
						if(config.getName().equals("Quotation Created")){
							flag =true;
							break;
						}
					}
					if(flag==false){
						Config config = new Config();
						config.setName("Quotation Created");
						config.setStatus(true);
						config.setType(6);
						config.setCompanyId(lead.getCompanyId());
						GenricServiceImpl genImpl = new GenricServiceImpl();
						genImpl.save(config);
					}
					lead.setStatus("Quotation Created");
					ofy().save().entity(lead);
				}
			}
		
		if(this.id==null){
			
			/**
			 * Date 11-08-2018 By Vijay
			 * Des :- revised old quotation changing status to Revised as per nitin sir
			 * because when we create revised quotation then on dashboard old and new quotation display which is wrong
			 * so old quotation changed status to revised
			 */
			if(this.getOldQuotationId()!=0){
				SalesQuotation quotation = ofy().load().type(SalesQuotation.class).filter("companyId", this.getCompanyId()).filter("count", this.getOldQuotationId()).first().now();
				if(quotation!=null){
					quotation.setStatus(QUOTATIONREVISED);
					ofy().save().entity(quotation);
				}
			}
		}
	}
	/**
	 * ends here
	 */

	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CREATED);
		statuslist.add(SALESQUOTATIONSUCESSFUL);
		statuslist.add(SALESQUOTATIONUNSUCESSFUL);
		statuslist.add(APPROVED);
		statuslist.add(Contract.REJECTED);
		statuslist.add(Contract.REQUESTED);
		
		return statuslist;
	}
	

	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		SalesQuotationServiceImpl implementor=new SalesQuotationServiceImpl();
		implementor.changeStatus(this);
	}
	
	
	public int getOldQuotationId() {
		return oldQuotationId;
	}


	public void setOldQuotationId(int oldQuotationId) {
		this.oldQuotationId = oldQuotationId;
	}

	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
		if(this.getFollowUpDate()!=null){
			this.setFollowUpDate(dateUtility.setTimeMidOftheDayToDate(this.getFollowUpDate()));
		}
	}
	/**
	 * ends here	
	 */

	public ArrayList<VendorMargin> getVendorMargins() {
		return vendorMargins;
	}


	public void setVendorMargins(List<VendorMargin> vendorMargins) {
		ArrayList<VendorMargin> list=new ArrayList<VendorMargin>();
		if(vendorMargins!=null&&vendorMargins.size()!=0){
			list.addAll(vendorMargins);
		}
		this.vendorMargins = list;
	}


	public ArrayList<OtherChargesMargin> getOtherChargesMargins() {
		return otherChargesMargins;
	}


	public void setOtherChargesMargins(List<OtherChargesMargin> otherChargesMargins) {
		ArrayList<OtherChargesMargin> list=new ArrayList<OtherChargesMargin>();
		if(otherChargesMargins!=null&&otherChargesMargins.size()!=0){
			list.addAll(otherChargesMargins);
		}
		this.otherChargesMargins = list;
	}


	public double getTotalVendorMargin() {
		return totalVendorMargin;
	}


	public void setTotalVendorMargin(double totalVendorMargin) {
		this.totalVendorMargin = totalVendorMargin;
	}


	public double getTotalOtherChargesMargin() {
		return totalOtherChargesMargin;
	}


	public void setTotalOtherChargesMargin(double totalOtherChargesMargin) {
		this.totalOtherChargesMargin = totalOtherChargesMargin;
	}


	public String getCustomerBranch() {
		return customerBranch;
	}


	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}


	public Address getCustBillingAddress() {
		return custBillingAddress;
	}


	public void setCustBillingAddress(Address custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	
	public ArrayList<TermsAndConditions> getTermsAndConditionlist() {
		return termsAndConditionlist;
	}


	public void setTermsAndConditionlist(ArrayList<TermsAndConditions> termsAndConditionlist) {
		this.termsAndConditionlist = termsAndConditionlist;
	}





	public boolean isPrintAllTermsConditions() {
		return printAllTermsConditions;
	}



	public void setPrintAllTermsConditions(boolean printAllTermsConditions) {
		this.printAllTermsConditions = printAllTermsConditions;
	}
	
	public String getPaymentModeName() {
		return paymentModeName;
	}

	public void setPaymentModeName(String paymentModeName) {
		this.paymentModeName = paymentModeName;
	}
	
	
}
