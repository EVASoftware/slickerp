package com.slicktechnologies.shared.common.salesorder;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CncBillServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.SalesOrderServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

@Entity
public class SalesOrder extends SalesQuotation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 367738172272175506L;
	
	/*******************************************Constants***********************************************/
	
	public static final String SALESORDERCANCEL = "Cancelled";
	
	/****************************************Applicability Attributes************************************/
	
	
	/** ID of old contract, if this contract is renewed from old contract.Otherwise zero */
	@Index
	protected String refNo;
	
	protected Date sodeliveryDate;
	protected Address soshippingAddress;
	protected Date referenceDate;
	@Index
	protected Date salesOrderDate;
	
	
	protected boolean salesOrderStatusChanged=false;
	
	
	
	/**
	 * Date 14 july 2017 added by vijay for Quick Sales Order
	 */
	@Index
	protected double paymentRecieved;
	@Index
	protected double balancePayment;
	@Index
	protected String newcompanyname;
	@Index
	protected String newcustomerfullName;
	protected Long newcustomercellNumber;
	protected String newcustomerEmail;
	@Index
	protected boolean customersaveflag;
	@Index
	protected String newCustomerBranch;
	@Index
	protected Address newcustomerAddress;
	@Index
	Date quickSalesInvoiceDate;
	@Index
	protected boolean isQuickSalesOrder;
	/** The bank name. */
	protected String bankName;
	
	/** The bank branch. */
	protected String bankBranch;
	
	/** The bank acc no. */
	protected String bankAccNo;
	
	/** The cheque no. */
	protected String chequeNo;
	
	/** The cheque date. */
	protected Date chequeDate;
	
	/** The amount transfer date. */
	protected Date amountTransferDate;
	
	/** The reference no. */
	protected String referenceNo;
	
	/** The chequeIssuedBy. */
	protected String chequeIssuedBy;
	
	/** Date 02-09-2017 added by vijay for new customer GST number **/
	protected String customerGSTNumber;
	
	
	/**
	 * ends here
	 */
	
	/*
	 * Date:19/07/2018
	 * Developer:Ashwini
	 */
	protected String numberRange;
	/*
	 * end by Ashwini
	 */

	/****************************************Default Constructor****************************************/

	/**
	 * Instantiates a new contract.
	 */
	/** date 3.10.2018 added by komal */
	@Index
	protected int poId;
	@Index
	protected int grnId;
	/**end komal**/
	/** date 24.11.2018 added by komal for subbilltype save **/
	private String subBillType;
	/****2-2-2019 added by Amol****/
	protected String refOrderNO;
	
	/**
	 * @author Anil ,Date : 16-04-2019
	 * storing customer branch and customer POC name
	 */
	@Index
	String custBranch;
	@Index
	String custPocName;
	String cncVersion;
	
	/**
	 * @author Anil @since 10-11-2021
	 * Sunrise SCM
	 */
	double budget;
	String billType;
	String costCode;
	String carpetArea;
	
	@Index
	protected String paymentMode; //Ashwini Patil Date:5-09-2023
	
	
	public SalesOrder() 
	{
		super();
		salesOrderStatusChanged=false;
		sodeliveryDate=new Date();
		soshippingAddress=new Address();
		refNo="";
		
		newcompanyname="";
		newcustomerfullName="";
		customersaveflag = false;
		newcustomerEmail="";
		newCustomerBranch="";
		quickSalesInvoiceDate=null;
		isQuickSalesOrder=false;
		
		bankName="";
		bankBranch="";
		bankAccNo="";
		chequeNo="";
		referenceNo="";
		chequeIssuedBy="";
		
		customerGSTNumber="";
		subBillType = "";
		cncVersion="";
	}

	/**************************************************Getters And Setters***********************************************************/


	public String getCustBranch() {
		return custBranch;
	}

	public void setCustBranch(String custBranch) {
		this.custBranch = custBranch;
	}

	public String getCustPocName() {
		return custPocName;
	}

	public void setCustPocName(String custPocName) {
		this.custPocName = custPocName;
	}
	
	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		if(refNo!=null){
			this.refNo = refNo;
		}
	}
	
	public Date getSodeliveryDate() {
		return sodeliveryDate;
	}

	public void setSodeliveryDate(Date sodeliveryDate) {
		if(sodeliveryDate!=null){
			this.sodeliveryDate = sodeliveryDate;
		}
	}
	
	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		if(referenceDate!=null){
			this.referenceDate = referenceDate;
		}
	}

	public Address getSoshippingAddress() {
		return soshippingAddress;
	}

	public void setSoshippingAddress(Address soshippingAddress) {
		this.soshippingAddress = soshippingAddress;
	}
	
	
	public Date getSalesOrderDate() {
		return salesOrderDate;
	}

	public void setSalesOrderDate(Date salesOrderDate) {
		if(salesOrderDate!=null){
			this.salesOrderDate = salesOrderDate;
		}
	}

	
	
	

	public double getPaymentRecieved() {
		return paymentRecieved;
	}

	public void setPaymentRecieved(double paymentRecieved) {
		this.paymentRecieved = paymentRecieved;
	}

	public double getBalancePayment() {
		return balancePayment;
	}

	public void setBalancePayment(double balancePayment) {
		this.balancePayment = balancePayment;
	}

	public String getNewcompanyname() {
		return newcompanyname;
	}

	public void setNewcompanyname(String newcompanyname) {
		this.newcompanyname = newcompanyname;
	}

	public String getNewcustomerfullName() {
		return newcustomerfullName;
	}

	public void setNewcustomerfullName(String newcustomerfullName) {
		this.newcustomerfullName = newcustomerfullName;
	}

	public Long getNewcustomercellNumber() {
		return newcustomercellNumber;
	}

	public void setNewcustomercellNumber(Long newcustomercellNumber) {
		this.newcustomercellNumber = newcustomercellNumber;
	}

	public String getNewcustomerEmail() {
		return newcustomerEmail;
	}

	public void setNewcustomerEmail(String newcustomerEmail) {
		this.newcustomerEmail = newcustomerEmail;
	}

	public boolean isCustomersaveflag() {
		return customersaveflag;
	}

	public void setCustomersaveflag(boolean customersaveflag) {
		this.customersaveflag = customersaveflag;
	}

	public String getNewCustomerBranch() {
		return newCustomerBranch;
	}

	public void setNewCustomerBranch(String newCustomerBranch) {
		this.newCustomerBranch = newCustomerBranch;
	}

	public Address getNewcustomerAddress() {
		return newcustomerAddress;
	}

	public void setNewcustomerAddress(Address newcustomerAddress) {
		this.newcustomerAddress = newcustomerAddress;
	}
	
	
	public Date getQuickSalesInvoiceDate() {
		return quickSalesInvoiceDate;
	}

	public void setQuickSalesInvoiceDate(Date quickSalesInvoiceDate) {
		this.quickSalesInvoiceDate = quickSalesInvoiceDate;
	}

	

	public boolean isQuickSalesOrder() {
		return isQuickSalesOrder;
	}

	public void setQuickSalesOrder(boolean isQuickSalesOrder) {
		this.isQuickSalesOrder = isQuickSalesOrder;
	}

	
	
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBankAccNo() {
		return bankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeIssuedBy() {
		return chequeIssuedBy;
	}

	public void setChequeIssuedBy(String chequeIssuedBy) {
		this.chequeIssuedBy = chequeIssuedBy;
	}

	
	public Date getAmountTransferDate() {
		return amountTransferDate;
	}

	public void setAmountTransferDate(Date amountTransferDate) {
		this.amountTransferDate = amountTransferDate;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	

	public String getCustomerGSTNumber() {
		return customerGSTNumber;
	}

	public void setCustomerGSTNumber(String customerGSTNumber) {
		this.customerGSTNumber = customerGSTNumber;
	}

	/*
	 * Date:19/07/2018
	 * Developer:Ashwini
	 */
	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}
	/*
	 * End by Ashwini
	 */
	/***************************************Relation Management Part**************************************/
	

	/***************************************Status List for Search***************************************/
	
	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CREATED);
		statuslist.add(APPROVED);
		statuslist.add(SalesOrder.REJECTED);
		statuslist.add(SalesOrder.REQUESTED);
		statuslist.add(SALESORDERCANCEL);
		
		return statuslist;
	}

	
	/*****************************************On Approval Logic*******************************************/

	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		/** date 24.11.2018 added by komal for subbilltype save **/
		CNC cnc = null;
			if (this.getRefNo() != null
					&& !this.getRefNo().equals("")) {
				int refNo = 0;
				try {
					refNo = Integer.parseInt(this.getRefNo());
				} catch (Exception e) {
					refNo = 0;
				}
				if (refNo != 0) {
					cnc = ofy().load().type(CNC.class)
							.filter("count", refNo)
							.filter("companyId", this.getCompanyId())
							.first().now();
					if (cnc != null) {
						boolean uploadConsumableFlag=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget", companyId);
						for(Consumables consumables : cnc.getConsumablesList()){
							/**
							 * @author Anil @since 12-11-2021
							 * If Consumable with site location is active then
							 * check bill type for particular location
							 * Sunrise SCM
							 */
							if(uploadConsumableFlag){
								try{
									if(custBranch!=null&&!custBranch.equals("")&&consumables.getSiteLocation()!=null&&!consumables.getSiteLocation().equals("")){
										if(custBranch.equals(consumables.getSiteLocation())){
											if(consumables.getBillingType().equalsIgnoreCase("FIXED")){
												this.setSubBillType(AppConstants.FIXEDCONSUMABLES);
												break;
											}
										}
									}
								}catch(Exception e){
									
								}
							}else{
								if(consumables.getBillingType().equalsIgnoreCase("FIXED")){
									this.setSubBillType(AppConstants.FIXEDCONSUMABLES);
									break;
								}
							}
						}
						/** date 28.11.2018 added by komal for actual consumables **/
						if(this.getSubBillType() == null || this.getSubBillType().equalsIgnoreCase("")){
							this.setSubBillType(AppConstants.CONSUMABLES);
						}
					}
				}
			}
			/** 
			 * end komal
			 */
		SalesOrderServiceImpl implementor=new SalesOrderServiceImpl();
		implementor.changeStatus(this);
		
		if(this.getPaymentTermsList().size()>0){
			/** date 24.11.2018 added by komal for fixed cnc billing**/
			if(this.subBillType != null && !this.subBillType.equals("")){
				CncBillServiceImpl impl = new CncBillServiceImpl();
				impl.generateBill(this.getSalesOrderDate(), this.getSalesOrderDate(), cnc , this.getCount());
			}else{
				createBillings();
			}
		}
		
		if(this.getCount()!=0){
			saveTaxesAndCharges();
		}
		
		if(this.getCount()!=0){
			createDeliveryNote();
		}
		
		
		
		System.out.println("Querying Processname");
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGSO).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag =0;
		
		if(processConfig!=null){
			System.out.println("Process Config Not Null");
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				System.out.println("----------------------------------------------------------");
				System.out.println("One===== "+processConfig.getProcessList().get(i).getProcessType());
				System.out.println("One===== "+processConfig.getProcessList().get(i).isStatus());
				System.out.println("----------------------------------------------------------");
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			accountingInterface();
		}
	}
	
	@GwtIncompatible
	@OnSave
	private void setSalesQuotationId()
	{
		SalesQuotation quot;
		System.out.println("Called Quotation Count"+(id==null &&quotationCount!=-1));
		if(id==null &&quotationCount!=-1)
		{
		  if(getCompanyId()==null)
			 quot= ofy().load().type(SalesQuotation.class).filter("count",quotationCount).first().now();
		  else
			  quot= ofy().load().type(SalesQuotation.class).filter("count",quotationCount).filter("companyId", getCompanyId()).first().now();
		
		   if(quot!=null)
		   {
			   quot.setContractCount(count);
			   ofy().save().entity(quot).now();
			   
			   /**
			    * @author Vijay Date 22-01-2020 for copying address from Quotation to Contract
			    */
			   if(quot.getShippingAddress()!=null)
			   this.setShippingAddress(quot.getShippingAddress());
			   if(quot.getCustBillingAddress()!=null)
			   this.setNewcustomerAddress(quot.getCustBillingAddress());
			   System.out.println("quotation address storing in salesorder");
		   }
		   System.out.println("Called Quotation Count----");
		}
	}

	
	private double getTotalBillAmt(List<SalesOrderProductLineItem> lisForTotalBill)
	{
		double saveTotalBillAmt=0;
		for(int i=0;i<lisForTotalBill.size();i++)
		{
			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getBaseBillingAmount();
		}
		
		return saveTotalBillAmt;
	}
	
	
	/**
	 * Methods returns the arraylist of sales order products.
	 * The Base Billing Amount is calculated as per percent corresponding to payment terms.
	 * @param payTermPercent
	 * @return
	 */
	@GwtIncompatible
	private List<SalesOrderProductLineItem> retrieveSalesProducts(double payTermPercent)
	{
		
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<this.getItems().size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", this.getItems().get(i).getPrduct().getCount()).first().now();
			
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
//			SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProdId(this.getItems().get(i).getPrduct().getCount());
			salesOrder.setProdCategory(this.getItems().get(i).getProductCategory());
			salesOrder.setProdCode(this.getItems().get(i).getProductCode());
			salesOrder.setProdName(this.getItems().get(i).getProductName());
			salesOrder.setQuantity(this.getItems().get(i).getQty());
			salesOrder.setOrderDuration(0);
			salesOrder.setOrderServices(0);
			totalTax=this.removeTaxAmt(this.getItems().get(i).getPrduct());
			System.out.println("Price in entity"+this.getItems().get(i).getPrice()+"   TaxAMt  "+totalTax);
			prodPrice=this.getItems().get(i).getPrice()-totalTax;
			System.out.println("Prod"+prodPrice);
			salesOrder.setPrice(prodPrice);
			salesOrder.setVatTax(this.getItems().get(i).getVatTax());
			salesOrder.setServiceTax(this.getItems().get(i).getServiceTax());
			salesOrder.setProdPercDiscount(this.getItems().get(i).getPercentageDiscount());
			salesOrder.setDiscountAmt(this.getItems().get(i).getDiscountAmt());//Ashwini Patil Date:8-06-2023
			salesOrder.setUnitOfMeasurement(this.getItems().get(i).getUnitOfMeasurement());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			if(this.getItems().get(i).getPercentageDiscount()!=0){
				percAmt=prodPrice-(prodPrice*this.getItems().get(i).getPercentageDiscount()/100);
				percAmt=percAmt*this.getItems().get(i).getQty();
			}
			if(this.getItems().get(i).getPercentageDiscount()==0){
				percAmt=prodPrice*this.getItems().get(i).getQty();
			}
			if(this.getItems().get(i).getDiscountAmt()!=0){//Ashwini Patil Date:8-06-2023
				percAmt=percAmt-this.getItems().get(i).getDiscountAmt();
			}

			
			salesOrder.setTotalAmount(percAmt);
			
			if(payTermPercent!=0){
				baseBillingAmount=(percAmt*payTermPercent)/100;
			}
			System.out.println("Base Billing Amount Through Payment Terms"+baseBillingAmount);
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			
		//  Vijay added this code for setting HSN Code Date : 20-07-2017
			salesOrder.setHsnCode(this.getItems().get(i).getPrduct().getHsnNumber());
			System.out.println("HSN --- "+this.getItems().get(i).getPrduct().getHsnNumber());
	
			salesOrder.setPaymentPercent(100.0);
			
			/**
			 * Date : 05-08-2017 By ANIL
			 */
			salesOrder.setBasePaymentAmount(baseBillingAmount);
			/**
			 * End
			 */
			
		//  date 24-08-2017 vijay added this code for area considered in calculation
			if(this.getItems().get(i).getArea()!=null)
			salesOrder.setArea(this.getItems().get(i).getArea());
					
			salesOrder.setIndexVal(i+1);
			/**
			 * nidhi
			 *20-06-2018 
			 */
			salesOrder.setProductSrNumber(this.getItems().get(i).getProductSrNo());
			/**
			 * end
			 */
			/***
			 * nidhi
			 * 29-08-2018
			 * for serial map to billing details
			 */
			salesOrder.setProSerialNoDetails(this.getItems().get(i).getProSerialNoDetails());
			/**
			 * end
			 */
			/////////////////////////////////////////////////////////////////////////////////
			
			/** Date 21-01-2019 By Vijay For sales and service bill merging invoice print required this **/
			salesOrder.setPrduct(superProdEntity);
			
			salesOrder.setTypeOfOrder(AppConstants.ORDERTYPESALES);
			
			
			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
	}
	
	private List<ContractCharges> listForBillingTaxes(double percPay){
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
		double assessValue=0;
		for(int i=0;i<this.productTaxes.size();i++){
			ContractCharges taxDetails=new ContractCharges();
			taxDetails.setTaxChargeName(getProductTaxes().get(i).getChargeName());
			taxDetails.setTaxChargePercent(getProductTaxes().get(i).getChargePercent());
			assessValue=getProductTaxes().get(i).getAssessableAmount()*percPay/100;
			taxDetails.setTaxChargeAssesVal(assessValue);
			taxDetails.setIdentifyTaxCharge(getProductTaxes().get(i).getIndexCheck());
			arrBillTax.add(taxDetails);
		}
		return arrBillTax;
	}
	
	private List<ContractCharges> saveCharges()
	{
		ArrayList<ContractCharges> arrCharge=new ArrayList<ContractCharges>();
		double calcBalAmt=0;
		for(int i=0;i<this.getProductCharges().size();i++)
		{
			ContractCharges chargeDetails=new ContractCharges();
			chargeDetails.setTaxChargeName(getProductCharges().get(i).getChargeName());
			chargeDetails.setTaxChargePercent(getProductCharges().get(i).getChargePercent());
			chargeDetails.setTaxChargeAbsVal(getProductCharges().get(i).getChargeAbsValue());
			System.out.println("Find Assess"+getProductCharges().get(i).getAssessableAmount());
			chargeDetails.setTaxChargeAssesVal(getProductCharges().get(i).getAssessableAmount());
			if(getProductCharges().get(i).getChargeAbsValue()!=0){
				calcBalAmt=getProductCharges().get(i).getChargeAbsValue();
			}
			if(getProductCharges().get(i).getChargePercent()!=0){
				calcBalAmt=getProductCharges().get(i).getChargePercent()*getProductCharges().get(i).getAssessableAmount()/100;
			}
			chargeDetails.setChargesBalanceAmt(calcBalAmt);
			arrCharge.add(chargeDetails);
		}
		return arrCharge;
		
	}
	
	
	public double removeTaxAmt(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			/**
			 * Date : 31-07-2017 By ANIL
			 * Checking GST printable name 
			 */
			if(entity.getServiceTax().getTaxPrintName()!=null && ! entity.getServiceTax().getTaxPrintName().equals("")
					   && entity.getVatTax().getTaxPrintName()!=null && ! entity.getVatTax().getTaxPrintName().equals(""))
			{
				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
			}
			else
			{
			double removeServiceTax=(entity.getPrice()/(1+service/100));
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
//			double taxPerc=service+vat;
//			retrServ=entity.getPrice()/(1+taxPerc/100);
//			retrServ=entity.getPrice()-retrServ;
			}
		}
		tax=retrVat+retrServ;
		System.out.println("Shared Tax Return"+tax);
		return tax;
	}

	@GwtIncompatible
	protected void createBillings()
	{
		
		Logger billingLogger=Logger.getLogger("Billing Logger");
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
		billingLogger.log(Level.SEVERE,"Current Date "+currentDate);
		
		for(int i=0;i<getPaymentTermsList().size();i++)
		{
			ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			PaymentTerms paymentTerms=new PaymentTerms();
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			Date conStartDate=null;
			
			
			/***********************Invoice Date******************************/
			billingLogger.log(Level.SEVERE,"SO Date As per IST"+DateUtility.getDateWithTimeZone("IST",this.getSalesOrderDate()));
			billingLogger.log(Level.SEVERE,"SO Date As Per Saved"+this.getSalesOrderDate());
			
			
			Calendar calInvoiceDate = Calendar.getInstance();
			if(this.getSalesOrderDate()!=null){
				Date salesDate=DateUtility.getDateWithTimeZone("IST", this.getSalesOrderDate());
				calInvoiceDate.setTime(salesDate);
			}
			else{
				conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
				calInvoiceDate.setTime(conStartDate);
			}
			
			
			calInvoiceDate.add(Calendar.DATE, this.getPaymentTermsList().get(i).getPayTermDays());
			Date calculatedInvoiceDate=null;
			try {
				calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
				calInvoiceDate.set(Calendar.MINUTE,59);
				calInvoiceDate.set(Calendar.SECOND,59);
				calInvoiceDate.set(Calendar.MILLISECOND,999);
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
				
				
				if(currentDate.after(calculatedInvoiceDate))
				{
					calculatedInvoiceDate=currentDate;
				}
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***********************************************************************************/
			
			/*********************************Billing Date************************************/
			
			Date calBillingDate=null;
			Calendar calBilling=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
			calBilling.add(Calendar.DATE, -2);
			try {
				calBilling.set(Calendar.HOUR_OF_DAY,23);
				calBilling.set(Calendar.MINUTE,59);
				calBilling.set(Calendar.SECOND,59);
				calBilling.set(Calendar.MILLISECOND,999);
				calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
				
				if(currentDate.after(calBillingDate)){
					calBillingDate=currentDate;
				}
				
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***************************************************************************************/
			
			/********************************Payment Date******************************************/
			
			Date calPaymentDate=null;
			int creditVal=0;
			if(this.getCreditPeriod()!=null){
				creditVal=this.getCreditPeriod();
			}
			
			Calendar calPayment=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calPayment.setTime(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
			calPayment.add(Calendar.DATE, creditVal);
			try {
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				calPayment.set(Calendar.HOUR_OF_DAY,23);
				calPayment.set(Calendar.MINUTE,59);
				calPayment.set(Calendar.SECOND,59);
				calPayment.set(Calendar.MILLISECOND,999);
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***********************************************************************************/
			/******************************************************************************/
			if(this.getCinfo()!=null)
				billingDocEntity.setPersonInfo(this.getCinfo());
			if(this.getCount()!=0)
				billingDocEntity.setContractCount(this.getCount());
			if(this.getNetpayable()!=0)
				billingDocEntity.setTotalSalesAmount(this.getNetpayable());
			
			salesProdLis=this.retrieveSalesProducts(this.getPaymentTermsList().get(i).getPayTermPercent());
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			System.out.println("Size here here"+salesOrdArr.size());
			if(salesOrdArr.size()!=0)
				billingDocEntity.setSalesOrderProducts(salesOrdArr);
			if(this.getCompanyId()!=null)
				billingDocEntity.setCompanyId(this.getCompanyId());
			
			if(calBillingDate!=null){
				
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST",calBillingDate));
				billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
			}
			
			if(calculatedInvoiceDate!=null){
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
			}
			if(calPaymentDate!=null){
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
				billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
			}
			if(this.getPaymentMethod()!=null)
				billingDocEntity.setPaymentMethod(this.getPaymentMethod());
			if(this.getApproverName()!=null)
				billingDocEntity.setApproverName(this.getApproverName());
			if(this.getEmployee()!=null)
				billingDocEntity.setEmployee(this.getEmployee());
			if(this.getBranch()!=null)
				billingDocEntity.setBranch(this.getBranch());
			
			/**
			 * Date : 21-09-2017 BY ANIL
			 * If other charges are selected at sales order level then we have to split those charges as per payment terms.
			 */
			double sumOfOc=0;
			if(this.getOtherCharges()!=null){
				ArrayList<OtherCharges> ocList=retrieveOtherCharges(this.getPaymentTermsList().get(i).getPayTermPercent());
				for(OtherCharges oc:ocList){
					sumOfOc=sumOfOc+oc.getAmount();
				}
				billingDocEntity.setOtherCharges(ocList);
				billingDocEntity.setTotalOtherCharges(sumOfOc);
			}
			
			/**
			 * End
			 */
			
			
			double taxamt =0;
			
			if(this.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				//billTaxLis=this.listForBillingTaxes(salesProdLis);
				billTaxLis=this.listForBillingTaxes(this.getPaymentTermsList().get(i).getPayTermPercent());
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
				
				//Date 20-sep-2017 added by vijay for tax amt calculation
				taxamt = getTaxAmt(billTaxArr);
			}
			
			billingDocEntity.setOrderCreationDate(this.getCreationDate());

			double totBillAmt=this.getTotalBillAmt(salesProdLis);
			billingDocEntity.setTotalBillingAmount(totBillAmt);
			
			int payTrmsDays=this.getPaymentTermsList().get(i).getPayTermDays();
			double payTrmsPercent=this.getPaymentTermsList().get(i).getPayTermPercent();
			String payTrmsComment=this.getPaymentTermsList().get(i).getPayTermComment().trim();
			
			paymentTerms.setPayTermDays(payTrmsDays);
			paymentTerms.setPayTermPercent(payTrmsPercent);
			paymentTerms.setPayTermComment(payTrmsComment);
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			
			if(this.getCformstatus()!=null){
				billingDocEntity.setOrderCformStatus(this.getCformstatus());
			}
			else{
				billingDocEntity.setOrderCformStatus("");
			}
			
			if(this.getCstpercent()!=null){
				billingDocEntity.setOrderCformPercent(this.getCstpercent());
			}
			else{
				billingDocEntity.setOrderCformPercent(-1);
			}
			
			/*
			 *  nidhi
			 *  	24-07-2017
			 *   contract persons ref no . this fields details come from customer ref 1 
			 */
		
			if(this.getRefNo()!=null){
				billingDocEntity.setRefNumber(this.getRefNo());
			}
			/*
			 * End
			 */
			
			
			/*
			 * Date=24/07/2018
			 * Developer=Ashwini
			 * Des:To add number range for Sales Order document in billing details.
			 */
			if(this.getNumberRange()!=null){
				billingDocEntity.setNumberRange(this.getNumberRange());
			}
			
			//Ashwini Patil Date:8-05-2023
			if(this.getCustBranch()!=null&&!this.getCustBranch().equals("")){
				billingDocEntity.setCustomerBranch(this.getCustBranch());
			}
			
			/*
			 * End
			 */
			
	//***********************changes made by rohan for saving gross value *************	
			double grossValue=this.getTotalAmount();
			
			
			billingDocEntity.setGrossValue(grossValue);
			
	//********************************changes ends here *******************************		
			
			
			
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESALES);
			billingDocEntity.setStatus(BillingDocument.CREATED);
			billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
			
			
			/** Date 03-09-2017 added by vijay for total amount before taxes**/
			billingDocEntity.setTotalAmount(totBillAmt);
			/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
			billingDocEntity.setFinalTotalAmt(totBillAmt);
			 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
			double totalamtincludingtax=totBillAmt+taxamt+sumOfOc;
			
			billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
			/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
			billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
			
			
			/**Date 29-7-2019 by Amol to mapping the other charges to billing**/
			if(getPaymentTermsList().size()==1){
				billingDocEntity.setOtherCharges(otherCharges);
				
			}
			
			
			 /** Date 09-06-2018 added by vijay for roundoff amount **/
			double roundoff=0;
			if(this.getRoundOffAmount()!=0){
				roundoff=this.getRoundOffAmount()/this.getPaymentTermsList().size();
				billingDocEntity.setRoundOffAmt(roundoff);
			}
			
			//Ashwini Patil Date:5-09-2023
			if(this.getPaymentMode()!=null)
				billingDocEntity.setPaymentMode(this.getPaymentMode());
			
			/**
			 * ends here
			 */
			
			arrbilling.add(billingDocEntity);
			/******************************************************************************/
			GenricServiceImpl impl=new GenricServiceImpl();
			if(arrbilling.size()!=0){
				impl.save(arrbilling);
			}
		}
	}
	
	/** Date 20-sep-2017 added by vijay for getting tax amt **********/
	public double getTaxAmt(ArrayList<ContractCharges> billTaxArr) {

		double totalCalcAmt=0;
		for(int i=0;i<billTaxArr.size();i++)
		{
			totalCalcAmt+=billTaxArr.get(i).getTaxChargeAssesVal()*billTaxArr.get(i).getTaxChargePercent()/100;
		}
		System.out.println("TAX AMT "+totalCalcAmt);
		return totalCalcAmt;
	}
	
	@GwtIncompatible
	private void saveTaxesAndCharges()
	{
		final GenricServiceImpl genimpl=new GenricServiceImpl();
		TaxesAndCharges taxChargeEntity=new TaxesAndCharges();
		List<ContractCharges> listaxcharge=this.saveCharges();
		ArrayList<ContractCharges> arrtaxchrg=new ArrayList<ContractCharges>();
		arrtaxchrg.addAll(listaxcharge);
		taxChargeEntity.setContractId(this.getCount());
		taxChargeEntity.setIdentifyOrder(AppConstants.BILLINGSALESFLOW.trim());
		taxChargeEntity.setTaxesChargesList(arrtaxchrg);
		taxChargeEntity.setCompanyId(this.getCompanyId());
		
		genimpl.save(taxChargeEntity);
	}
	
	
	
	@GwtIncompatible
	protected void createDeliveryNote()
	{
		final GenricServiceImpl genimpl=new GenricServiceImpl();
		
		DeliveryNote deliveryEntity=new DeliveryNote();
		
		deliveryEntity.setCinfo(this.getCinfo());
		deliveryEntity.setSalesOrderCount(this.getCount());
		if(this.getLeadCount()!=0){
			deliveryEntity.setLeadCount(this.getLeadCount());
		}
		if(this.getQuotationCount()!=0){
			deliveryEntity.setQuotationCount(this.getQuotationCount());
		}
		if(!this.getRefNo().equals("")){
			deliveryEntity.setReferenceNumber(this.getRefNo());
		}
		if(this.getReferenceDate()!=null){
			deliveryEntity.setReferenceDate(this.getReferenceDate());
		}
		if(this.getEmployee()!=null){
			deliveryEntity.setEmployee(this.getEmployee());
		}
		if(this.getCformstatus()!=null){
			deliveryEntity.setCformStatus(this.getCformstatus());
		}
		if(this.getCstpercent()!=0){
			deliveryEntity.setCstPercent(this.getCstpercent());
		}
		if(this.getCstName()!=null){
			deliveryEntity.setCstName(this.getCstName());
		}
		deliveryEntity.setStatus(DeliveryNote.CREATED);
		deliveryEntity.setBranch(this.getBranch());
		deliveryEntity.setDeliveryDate(this.getDeliveryDate());
		if(this.getItems().size()!=0){
//			List<DeliveryLineItems> deliveryLis=retrieveDeliveryItems();
			
			
			/*
			 * nidhi
			 * 26-06-2017
			 */
			List<Integer> productId = new ArrayList<Integer>();
			
			for(SalesLineItem items : this.getItems()){
				productId.add(items.getPrduct().getCount());
			}
			Collections.sort(productId);
			List<ProductInventoryView> productInvList=ofy().load().type(ProductInventoryView.class).filter("companyId",getCompanyId()).filter("productinfo.prodID IN", productId).list();
			
			List<DeliveryLineItems> deliveryLis=retrieveDeliveryItems();
			int prodcount = 0;
			int prodid1 = 0;
			
			
			for(ProductInventoryView pro : productInvList){
				for(DeliveryLineItems obj:deliveryLis){
					if(obj.getProdId()==pro.getProdID()){
						if(pro.getDetails().size()==1){
							obj.setWareHouseName(pro.getDetails().get(0).getWarehousename());
							obj.setStorageLocName(pro.getDetails().get(0).getStoragelocation());
							obj.setStorageBinName(pro.getDetails().get(0).getStoragebin());
							obj.setProdAvailableQuantity(pro.getDetails().get(0).getAvailableqty());
						}
					}
				}
			}
			
			/**
			 * End
			 */
			
			
			ArrayList<DeliveryLineItems> arrLineItems=new ArrayList<DeliveryLineItems>();
			arrLineItems.addAll(deliveryLis);
			deliveryEntity.setDeliveryItems(arrLineItems);
		}
		if(this.getTotalAmount()!=0){
			deliveryEntity.setTotalAmount(this.getTotalAmount());
		}
		if(this.getProductTaxes().size()!=0){
			deliveryEntity.setProdTaxesLis(this.getProductTaxes());
		}
		if(this.getProductCharges().size()!=0){
			deliveryEntity.setProdChargeLis(this.getProductCharges());
		}
		if(this.getNetpayable()!=0){
			deliveryEntity.setNetpayable(this.getNetpayable());
		}
		if(this.getShippingAddress()!=null){
			deliveryEntity.setShippingAddress(this.getShippingAddress());
		}
		deliveryEntity.setCompanyId(this.getCompanyId());
		
		deliveryEntity.setCompanyId(this.getCompanyId());
		/*
		 * Date:24/07/2018
		 * Developer:Ashwini
		 * Des:To set number range value in delivery note
		 */
		if(this.getNumberRange()!=null){
			deliveryEntity.setNumberRange(this.getNumberRange());	
			
		}
		
		/**
		 * @author Anil, Date : 16-04-2019
		 */
		if(this.getCustBranch()!=null){
			deliveryEntity.setCustBranch(this.getCustBranch());
		}
		if(this.getCustPocName()!=null){
			deliveryEntity.setCustPocName(this.getCustPocName());
		}
		
		/**
		 * @author Anil
		 * @since 30-11-2020
		 * setting billing address of sales order to delivery note
		 */
		deliveryEntity.setBillingAddress(this.getNewcustomerAddress());
		
		/*
		 * End by Ashwini
		 */
		
		genimpl.save(deliveryEntity);
	}
	
	@GwtIncompatible
	private List<DeliveryLineItems> retrieveDeliveryItems()
	{
		ArrayList<DeliveryLineItems> arrDeliveryItems=new ArrayList<DeliveryLineItems>();
		double priceVal=0,taxValueAmt=0,totalAmt=0;
		
		for(int i=0;i<this.getItems().size();i++)
		{
			DeliveryLineItems delLine=new DeliveryLineItems();
			/*26-06-2017
			 * nidhi comment this line
			 * and new code for get product id
			 */
//			ItemProduct ipEntity=ofy().load().type(ItemProduct.class).filter("companyId",getCompanyId()).filter("productCode", this.getItems().get(i).getProductCode().trim()).first().now();
			System.out.println("ASD"+getItems().get(i).getProductCode());
//			delLine.setProdId(ipEntity.getCount());
			delLine.setProdId(this.getItems().get(i).getPrduct().getCount());
			delLine.setProdCode(this.getItems().get(i).getProductCode());
			delLine.setProdCategory(this.getItems().get(i).getProductCategory());
			delLine.setProdName(this.getItems().get(i).getProductName());
			delLine.setQuantity(this.getItems().get(i).getQty());
			taxValueAmt=removeTaxAmt(this.getItems().get(i).getPrduct());
			priceVal=this.getItems().get(i).getPrice()-taxValueAmt;
			delLine.setPrice(priceVal);
			delLine.setVatTax(this.getItems().get(i).getVatTax());
			delLine.setServiceTax(this.getItems().get(i).getServiceTax());
			delLine.setProdPercDiscount(this.getItems().get(i).getPercentageDiscount());
			
			/*Rohan added below line for copy Disc Amount from sales order to delivery note 
			 * Dated : 12/09/2016 
			 */
			delLine.setDiscountAmt(this.getItems().get(i).getDiscountAmt());
			if(this.getItems().get(i).getPercentageDiscount()!=0){
				totalAmt=priceVal-(priceVal*this.getItems().get(i).getPercentageDiscount()/100);
				totalAmt=totalAmt*this.getItems().get(i).getQty();
			}
			if(this.getItems().get(i).getPercentageDiscount()==0){
				totalAmt=priceVal*this.getItems().get(i).getQty();
			}
			delLine.setTotalAmount(totalAmt);
			
			/**
			 * @author Ashwini Patil
			 * @since 23-04-2022
			 * warehouse , storage bin , storage location and available quantity was not getting mapped from sales order to delivery note
			 */
			if(this.getItems().get(i).getItemProductWarehouseName()!=null){
				delLine.setWareHouseName(this.getItems().get(i).getItemProductWarehouseName());
				System.out.println("WarehouseName set to="+this.getItems().get(i).getItemProductWarehouseName());
			}
			if(this.getItems().get(i).getItemProductStrorageLocation()!=null){
				delLine.setStorageLocName(this.getItems().get(i).getItemProductStrorageLocation());
			}
			if(this.getItems().get(i).getItemProductStorageBin()!=null){
				delLine.setStorageBinName(this.getItems().get(i).getItemProductStorageBin());
			}
			delLine.setProdAvailableQuantity(this.getItems().get(i).getItemProductAvailableQty());
			
			
			arrDeliveryItems.add(delLine);
			
			/**
			 * nidhi 
			 * 1-11-2018
			 * for map squence for multiple productes
			 */
			delLine.setProductSrNumber(this.getItems().get(i).getProductSrNo());
			delLine.setPrduct(this.getItems().get(i).getPrduct());
			if(this.getItems().get(i).getPrduct().getHsnNumber()!=null&& !this.getItems().get(i).getPrduct().getHsnNumber().equals("")){
				delLine.getPrduct().setHsnNumber(this.getItems().get(i).getPrduct().getHsnNumber());

			}
			/**
			 * nidhi 
			 */
			/**
			 * nidhi
			 * 24-08-2018
			 */
			if(this.getItems().get(i).getProSerialNoDetails()!= null &&
					this.getItems().get(i).getProSerialNoDetails().containsKey(0) 
					&& this.getItems().get(i).getProSerialNoDetails().get(0).size()>0){
				delLine.getProSerialNoDetails().put(0, this.getItems().get(i).getProSerialNoDetails().get(0));
			}
			/**
			 * end
			 */
		}
		return arrDeliveryItems;
		
	}
	
	
	@GwtIncompatible
	 protected void accountingInterface()
	 {
		 
		 if(this.getCount()!=0)
		 {
			for(int  i = 0;i<this.getItems().size();i++){
				
					String unitofmeasurement = this.getItems().get(i).getUnitOfMeasurement();
					int prodId=this.getItems().get(i).getPrduct().getCount();
					String productCode = this.getItems().get(i).getProductCode();
					String productName = this.getItems().get(i).getProductName();
					double productQuantity = this.getItems().get(i).getQty();
					double taxamount = removeTaxAmt(this.getItems().get(i).getPrduct());
					double productprice = (this.getItems().get(i).getPrice()-taxamount);
					double totalAmount =(this.getItems().get(i).getPrice()-taxamount)*this.getItems().get(i).getQty();
					
					double vatPercent=0;
					double calculatedamt = 0;
					double serviceTax=0 ;
					double calculetedservice = 0;
					double cformamtService=0;
					 
					String cform = "";
					double cformPercent=0;
					double cformAmount=0;
					
//					double vatAmount=0; 
					 
					int vatglaccno=0;
					int serglaccno=0;
					int cformglaccno=0;
					 
				/********************************************for cform*******************************************/
					 
					
					 if(this.getCformstatus()!= null&&this.getCformstatus().trim().equals(AppConstants.YES)){
						 cform=AppConstants.YES;
						 cformPercent=this.getCstpercent();
						 cformAmount=this.getCstpercent()*totalAmount/100;
						 cformamtService=this.getCstpercent()*totalAmount/100;
						 if(this.getItems().get(i).getServiceTax().getPercentage()!=0){
							 serviceTax=this.getItems().get(i).getServiceTax().getPercentage();
							 calculetedservice=((cformamtService+totalAmount)*this.getItems().get(i).getServiceTax().getPercentage())/100;
						 }
						 
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getCstpercent()).filter("isCentralTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 cformglaccno=serglAcc.getGlAccountNo();
					 }
					 
					 else if(this.getCformstatus()!= null&&this.getCformstatus().trim().equals(AppConstants.NO)){
						 cform=AppConstants.NO;
						 cformPercent=this.getItems().get(i).getVatTax().getPercentage();
						 cformAmount=this.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
						 cformamtService=this.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
						 
						 if(this.getItems().get(i).getServiceTax().getPercentage()!=0){
							 serviceTax=this.getItems().get(i).getServiceTax().getPercentage();
							 calculetedservice=((cformamtService+totalAmount)*this.getItems().get(i).getServiceTax().getPercentage())/100;
						 }
						 
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargeName",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 cformglaccno=serglAcc.getGlAccountNo();
					 }
					 else{//if the customer is of same state
						 
						 if(this.getItems().get(i).getVatTax().getPercentage()!=0)
						 {
							 vatPercent=this.getItems().get(i).getVatTax().getPercentage();
							 calculatedamt=this.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getItems().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
							 vatglaccno=vatglAcc.getGlAccountNo();
						 }
						 
						 if(this.getItems().get(i).getServiceTax().getPercentage()!=0)
						 {
							 serviceTax=this.getItems().get(i).getServiceTax().getPercentage();
							 calculetedservice=(calculatedamt+totalAmount)*this.getItems().get(i).getServiceTax().getPercentage()/100;
						 }
					 }
					
					 
					 if(this.getItems().get(i).getServiceTax().getPercentage()!=0)
					 {
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getItems().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 serglaccno=serglAcc.getGlAccountNo();
					 }
					 
					 
					 /***************************************************************/
					 
					 
					 
					 

					 
//					 double totalAmount = getTotalAmount();
					 double netPayable = getNetpayable();
					 
					 
					 
					 String[] oc = new String[12];
					 double[] oca = new double[12];
					 
					 
					 
					 System.out.println("qwedsa"+this.getProductCharges().size());
					 for(int k = 0; k < this.getProductCharges().size();k++){
						 oc[k] = getProductCharges().get(k).getChargeName();
						 oca[k] = getProductCharges().get(k).getChargePayable();
					 }
					 String numberRange = "";
					 if(this.getNumberRange()!=null){
						 numberRange = this.getNumberRange();
					 }
					 
					 /**********************************End************************************************/
					 UpdateAccountingInterface.updateTally
					 (
							 new Date(),   								//accountingInterfaceCreationDate
							 this.getEmployee(),						//	accountingInterfaceCreatedBy
							 this.getStatus(),							//  document status
							 AppConstants.STATUS ,						//	Status
							 "",						//	Remark
							 "Sales",//	Module
							 "Sales Order",			//	documentType				
								this.getCount(),		//	documentID				
								" ", 					//	documentTitle			
								this.getCreationDate(),		//	documentDate			
								AppConstants.REFGLACCOUNTSO,			//	doucmentGL				
								this.getRefNo()+"",			//	referenceDocumentNumber1				
								this.getReferenceDate(),			//	referenceDocumentDate1
								AppConstants.REFDOCSO,					//	referenceDocumentType
								"",										//	referenceDocumentNumber2
								null,										//	referencedocumentDate2
								"",										//	referencedocumentType2
								AppConstants.ACCOUNTTYPE,				//	accountType
								this.getCinfo().getCount(),				//	customerID
								this.getCinfo().getFullName(),			//	customerName
								this.getCinfo().getCellNumber(),		//	customerCell
								0,										//vendorID
								"",										//	vendorName
								0,										//	vendorCell	
								0,											//employeeID
								"",									//employeeName
								this.getBranch(),						//	Branch
								this.getEmployee(),						//personResponsible
								"",											//requestedBy
								this.getApproverName(),					//approvedBy
								this.getPaymentMethod(),			//paymentMethod
								null,									//paymentDate		
								"",										//	chequeNumber
								null,									//	chequeDate
								null,									//bankName
								null,										//bankAccount
								0,										//transferReferenceNumber
								null,									//transactionDate
								null,									// contract start date
								null,									// contract end date
								prodId,										//	productID	
								productCode,							//productCode
								productName,						//productName
								productQuantity,						//Quantity
								null,						//	productDate
								0,										//	duration
								0,										//services
								unitofmeasurement,									//	unitOfMeasurement		
								productprice, 									//productPrice
								vatPercent,								//VATpercent
								calculatedamt,								//VATamount
								vatglaccno,								//	VATglAccount
								serviceTax,									//	serviceTaxPercent
								calculetedservice,					//	serviceTaxAmount
								serglaccno,								//serviceTaxGLaccount
								cform,										//cForm
								cformPercent,						//	cFormPercent
								cformAmount,						//cFormAmount
								cformglaccno,							//cFormGlACcount
								totalAmount,							// totalAmount		
								netPayable,								//		netPayable	
								"",//Contract Category
								0,									//amountRecieved
								0.0,                               //  base Amt taken in payment for tds by rohan  
								0,                                 //  tds percentage by rohan 
								0,                                 //  tds amount by rohan 
								oc[0],										
								oca[0],									
								oc[1],									
								oca[1],										
								oc[2],										
								oca[2],										
								oc[3],										
								oca[3],									
								oc[4],
								oca[4],
								oc[5],
								oca[5],
								oc[6],
								oca[6],
								oc[7],
								oca[7],
								oc[8],
								oca[8],
								oc[9],
								oca[9],
								oc[10],
								oca[10],
								oc[11],
								oca[11],
								this.getShippingAddress().getAddrLine1(),
								this.getShippingAddress().getLocality(),
								this.getShippingAddress().getLandmark(),
								this.getShippingAddress().getCountry(),
								this.getShippingAddress().getState(),
								this.getShippingAddress().getCity(),
								this.getShippingAddress().getPin(),
								this.getCompanyId(),
								null,				//  billing from date (rohan)
								null,		//  billing to date (rohan)
								"", //Warehouse
								"",				//warehouseCode
								"",				//ProductRefId
								"",				//Direction
								"",				//sourceSystem
								"",				//Destination System
								null,
								null,
								numberRange
							);
					 
				}
		 	}
		 }

	
	/**
	 * Date 18 july 2017 added by vijay for Quick Sales Order
	 * new Customer
	 */
	@OnSave
	@GwtIncompatible
	private void savecustomer(){
		
		System.out.println("full Name === "+this.getNewcustomerfullName());
		System.out.println("compnay name ==="+this.getNewcompanyname());

		
		if(this.isCustomersaveflag()==false){
		if(!this.newcompanyname.equals("") || !this.newcustomerfullName.equals("")){
			
			System.out.println("customer iddddd === "+this.getCinfo().getCount());

			if(this.getCinfo().getCount()==0){
			GenricServiceImpl genimpl = new GenricServiceImpl();
			Customer cust = new Customer();
				if(!this.getNewcompanyname().equals("")){
					cust.setCompany(true);
					cust.setCompanyName(this.getNewcompanyname().toUpperCase());
				}else{
					cust.setCompany(false);
				}
				if(this.getNewcustomerfullName()!=null){
					cust.setFullname(this.getNewcustomerfullName().toUpperCase());
				}
				if(this.getNewcustomerEmail()!=null){
					cust.setEmail(this.getNewcustomerEmail());
				}
				if(this.getNewcustomercellNumber()!=null){
					cust.setCellNumber1(this.getNewcustomercellNumber());
				}
				
				//  rohan added branch in customer
				if(this.getNewCustomerBranch()!=null){
					cust.setBranch(this.getNewCustomerBranch());
				}
				
				cust.setNewCustomerFlag(true);

				if (this.getNewcustomerAddress()!=null) {
					cust.setAdress(this.getNewcustomerAddress());
					cust.setSecondaryAdress(this.getNewcustomerAddress());
				}
				cust.setCompanyId(this.getCompanyId());
			   
				// Date 02-09-2017 added by vijay for new customer GST
				if(this.getCustomerGSTNumber()!=null && this.getCustomerGSTNumber().equals("")){
				ArticleType articalType=new ArticleType();
				articalType.setDocumentName("Invoice");
				articalType.setArticleTypeName("GSTIN");
				articalType.setArticleTypeValue(this.getCustomerGSTNumber());
				articalType.setArticlePrint("Yes");
				cust.getArticleTypeDetails().add(articalType);
				}
				genimpl.save(cust);	   

				System.out.println("after saving customer count ==="+cust.getCount());
				
				PersonInfo pinfo = new PersonInfo();
				
				pinfo.setCount(cust.getCount());
				
				if(cust.isCompany()){
					pinfo.setFullName(cust.getCompanyName());
					pinfo.setPocName(cust.getFullname());
				}else{
					pinfo.setFullName(cust.getFullname());
					pinfo.setPocName(cust.getFullname());
				}
				
				pinfo.setCellNumber(cust.getCellNumber1());
				pinfo.setEmail(cust.getEmail());
				
				this.setCinfo(pinfo);
			
			}else{
				
				
			 Customer customer = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getCinfo().getCount()).first().now();
				
				System.out.println("compnay name ==="+this.getNewcompanyname());
				
				if(!this.getNewcompanyname().equals("")){
					customer.setCompany(true);
					customer.setCompanyName(this.getNewcompanyname().toUpperCase());
				}else{
					customer.setCompany(false);
				}
				if(this.getNewcustomerfullName()!=null){
					customer.setFullname(this.getNewcustomerfullName().toUpperCase());
				}
				if(this.getNewcustomerEmail()!=null){
					customer.setEmail(this.getNewcustomerEmail());
				}
				if(this.getNewcustomercellNumber()!=null){
					customer.setCellNumber1(this.getNewcustomercellNumber());
				}

				if (this.getNewcustomerAddress()!=null) {
					customer.setAdress(this.getNewcustomerAddress());
					customer.setSecondaryAdress(this.getNewcustomerAddress());
				}
				customer.setCompanyId(this.getCompanyId());
			   
			//  rohan added branch in customer
				if(this.getNewCustomerBranch()!=null){
					customer.setBranch(this.getNewCustomerBranch());
				}
				
				// Date 02-09-2017 added by vijay for new customer GST
				if(this.getCustomerGSTNumber()!=null && this.getCustomerGSTNumber().equals("")){
				ArticleType articalType=new ArticleType();
				articalType.setDocumentName("Invoice");
				articalType.setArticleTypeName("GSTIN");
				articalType.setArticleTypeValue(this.getCustomerGSTNumber());
				articalType.setArticlePrint("Yes");
				customer.getArticleTypeDetails().add(articalType);
				}

				
				ofy().save().entity(customer);
				System.out.println("customer updated");
				PersonInfo pinfo = new PersonInfo();
				pinfo.setCount(customer.getCount());
			
				if(customer.isCompany()){
					pinfo.setFullName(customer.getCompanyName());
					pinfo.setPocName(customer.getFullname());
				}else{
					pinfo.setFullName(customer.getFullname());
					pinfo.setPocName(customer.getFullname());
				}
				
				pinfo.setCellNumber(customer.getCellNumber1());
				pinfo.setEmail(customer.getEmail());
				
				this.setCinfo(pinfo);
			}
		}
	}
	}
	@OnSave
	@GwtIncompatible
	private void reactOnSave(){
    if(id == null){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "MonthwiseNumberGeneration", this.getCompanyId())){
			DecimalFormat df=new DecimalFormat("000");
			ServerAppUtility utility = new ServerAppUtility();
			SimpleDateFormat mmm = new SimpleDateFormat("MMM");
			mmm.setTimeZone(TimeZone.getTimeZone("IST"));
			String month = mmm.format(this.getCreationDate());
			String year = "";
			try {
				year = utility.getFinancialYearFormat(this.getCreationDate(), true);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String number = "SO/"+month+"/"+year;
			ProcessConfiguration config = new ProcessConfiguration();
			config= ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", companyId)
					.filter("processName", number).first().now();
			int cnt = 1;
				if (config !=null) {

					for (int i = 0; i < config.getProcessList().size(); i++) {
						try{
							cnt = Integer.parseInt(config.getProcessList().get(i).getProcessType());
							cnt = cnt +1;
							config.getProcessList().get(i).setProcessType(cnt+"");
						}catch(Exception e){
							cnt = 0;
						}
					}

				}
				else{
					config = new ProcessConfiguration();
					config.setCompanyId(this.companyId);
					config.setProcessName(number);
					config.setConfigStatus(true);
					ArrayList<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
					ProcessTypeDetails type = new ProcessTypeDetails();
					type.setProcessName(number);
					type.setProcessType(cnt+"");
					type.setStatus(true);
					processList.add(type);		
					config.setProcessList(processList);
				}
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(config);
				if(cnt <= 999){
									this.setRefOrderNO("SO/"+month.toUpperCase()+"/"+df.format(cnt)+"/"+year);
				}else{
					this.setRefOrderNO("SO/"+month.toUpperCase()+"/"+cnt+"/"+year);
				}
			}
		}

	
		
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Date : 21-09-2017 BY ANIL
	 */
	@GwtIncompatible
	public ArrayList<OtherCharges> retrieveOtherCharges(double paymentTermsPercent)
	{
		ArrayList<OtherCharges> otherChargesList=new ArrayList<OtherCharges>();
		
		for(OtherCharges object:this.getOtherCharges()){
			OtherCharges oc=new OtherCharges();
			oc.setOtherChargeName(object.getOtherChargeName());
			oc.setAmount((object.getAmount()*paymentTermsPercent)/100);
			oc.setTax1(object.getTax1());
			oc.setTax2(object.getTax2());
			otherChargesList.add(oc);
		}
		
		return otherChargesList;
	}

	/**
	 * @author Vijay Chougule Date 22-06-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */
	
	public int getPoId() {
		return poId;
	}

	public void setPoId(int poId) {
		this.poId = poId;
	}

	public int getGrnId() {
		return grnId;
	}

	public void setGrnId(int grnId) {
		this.grnId = grnId;
	}

	public String getSubBillType() {
		return subBillType;
	}

	public void setSubBillType(String subBillType) {
		this.subBillType = subBillType;
	}

	public String getRefOrderNO() {
		return refOrderNO;
	}

	public void setRefOrderNO(String refOrderNO) {
		this.refOrderNO = refOrderNO;
	}

	public String getCncVersion() {
		return cncVersion;
	}

	public void setCncVersion(String cncVersion) {
		this.cncVersion = cncVersion;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getCostCode() {
		return costCode;
	}

	public void setCostCode(String costCode) {
		this.costCode = costCode;
	}

	public String getCarpetArea() {
		return carpetArea;
	}

	public void setCarpetArea(String carpetArea) {
		this.carpetArea = carpetArea;
	}
	
	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}


}
