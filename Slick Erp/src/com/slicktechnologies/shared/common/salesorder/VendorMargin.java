package com.slicktechnologies.shared.common.salesorder;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class VendorMargin extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3473542787972790957L;
	
	int productId;
	String productName;
	double salesPrice;
	double vendorPrice;
	double vendorMargin;
	String remark;
	int productSrNo;
	boolean isReadOnly;
	
	double purchasePrice;
	double purchaseMargin;
	String purchaseRemark;
	
	
	

	public int getProductId() {
		return productId;
	}




	public void setProductId(int productId) {
		this.productId = productId;
	}




	public String getProductName() {
		return productName;
	}




	public void setProductName(String productName) {
		this.productName = productName;
	}




	public double getSalesPrice() {
		return salesPrice;
	}




	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}




	public double getVendorPrice() {
		return vendorPrice;
	}




	public void setVendorPrice(double vendorPrice) {
		this.vendorPrice = vendorPrice;
	}




	public double getVendorMargin() {
		return vendorMargin;
	}




	public void setVendorMargin(double vendorMargin) {
		this.vendorMargin = vendorMargin;
	}




	public String getRemark() {
		return remark;
	}




	public void setRemark(String remark) {
		this.remark = remark;
	}




	public int getProductSrNo() {
		return productSrNo;
	}




	public void setProductSrNo(int productSrNo) {
		this.productSrNo = productSrNo;
	}

	public boolean isReadOnly() {
		return isReadOnly;
	}


	public void setReadOnly(boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
	}
	
	


	public double getPurchasePrice() {
		return purchasePrice;
	}




	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}




	public double getPurchaseMargin() {
		return purchaseMargin;
	}




	public void setPurchaseMargin(double purchaseMargin) {
		this.purchaseMargin = purchaseMargin;
	}




	public String getPurchaseRemark() {
		return purchaseRemark;
	}




	public void setPurchaseRemark(String purchaseRemark) {
		this.purchaseRemark = purchaseRemark;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
