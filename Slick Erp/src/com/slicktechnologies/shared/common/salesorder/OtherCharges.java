package com.slicktechnologies.shared.common.salesorder;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.productlayer.Tax;
@Embed
public class OtherCharges  extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3139755163624846421L;

	String otherChargeName;
	Tax tax1;
	Tax tax2;
	double amount;
	String hsnSacCode;
	
	/**
	 * Date : 18-10-2017 By ANIL
	 * capturing order id and billing id captured for the making single invoice from multiple bill. 
	 */
	int orderId;
	int billingId;
	/**
	 * End
	 */
	
	public OtherCharges() {
		super();
		otherChargeName="";
		tax1=new Tax();
		tax2=new Tax();
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	public String getOtherChargeName() {
		return otherChargeName;
	}


	public void setOtherChargeName(String otherChargeName) {
		this.otherChargeName = otherChargeName;
	}


	public Tax getTax1() {
		return tax1;
	}


	public void setTax1(Tax tax1) {
		this.tax1 = tax1;
	}


	public Tax getTax2() {
		return tax2;
	}


	public void setTax2(Tax tax2) {
		this.tax2 = tax2;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getBillingId() {
		return billingId;
	}

	public void setBillingId(int billingId) {
		this.billingId = billingId;
	}

	public String getHsnSacCode() {
		return hsnSacCode;
	}

	public void setHsnSacCode(String hsnSacCode) {
		this.hsnSacCode = hsnSacCode;
	}
	

}
