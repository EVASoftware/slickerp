package com.slicktechnologies.shared.common.unitconversion;

import java.util.ArrayList;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
@Entity
public class UnitConversion extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 199826188790525109L;

	protected int unitCount;
	@Index
	protected String unit = "";

	
	protected String description = "";
	
	/** The status. */
	@Index 
	protected boolean unitStatus=true;
	protected String unitPrAbleName = "";
	ArrayList<UnitConversionDetailList> unitConversionList = new ArrayList<UnitConversionDetailList>();
	@Index
	protected int unitCode;
	public UnitConversion(){
		super();
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getUnitCount() {
		return unitCount;
	}

	public void setUnitCount(int unitCount) {
		this.unitCount = unitCount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public ArrayList<UnitConversionDetailList> getUnitConversionList() {
		return unitConversionList;
	}

	public void setUnitConversionList(
			ArrayList<UnitConversionDetailList> unitConversionList) {
		this.unitConversionList = unitConversionList;
	}

	public boolean isUnitStatus() {
		return unitStatus;
	}

	public void setUnitStatus(boolean unitStatus) {
		this.unitStatus = unitStatus;
	}

	public String getUnitPrAbleName() {
		return unitPrAbleName;
	}

	public void setUnitPrAbleName(String unitPrAbleName) {
		this.unitPrAbleName = unitPrAbleName;
	}

	public int getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(int unitCode) {
		this.unitCode = unitCode;
	}

	
}
