package com.slicktechnologies.shared.common.unitconversion;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class UnitConversionDetailList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2872562881678381721L;
	
	protected double maxValue = 0;
	protected String maxUnit = "";
	protected double minValue = 0;
	protected String minUnit = "";
	
	protected String unitPrName = "";
	protected String maxUnitPrName = "";
	protected String minUnitPrName = "";
	
	protected boolean status = true;
	public double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}
	public String getMaxUnit() {
		return maxUnit;
	}
	public void setMaxUnit(String maxUnit) {
		this.maxUnit = maxUnit;
	}
	public double getMinValue() {
		return minValue;
	}
	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}
	public String getMinUnit() {
		return minUnit;
	}
	public void setMinUnit(String minUnit) {
		this.minUnit = minUnit;
	}
	public String getUnitPrName() {
		return unitPrName;
	}
	public void setUnitPrName(String unitPrName) {
		this.unitPrName = unitPrName;
	}
	public String getMaxUnitPrName() {
		return maxUnitPrName;
	}
	public void setMaxUnitPrName(String maxUnitPrName) {
		this.maxUnitPrName = maxUnitPrName;
	}
	public String getMinUnitPrName() {
		return minUnitPrName;
	}
	public void setMinUnitPrName(String minUnitPrName) {
		this.minUnitPrName = minUnitPrName;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
