package com.slicktechnologies.shared.common.shippingpackingprocess;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

@Entity
public class CheckListStep extends SuperModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7617728203158305978L;
	/*************************************Applicability Attributes****************************************/
	
	protected int stepNumber;
	@Index
	protected String stepCode;
	protected String stepName;
	@Index
	protected boolean stepMandatory;
	@Index
	protected boolean stepStatus;
	protected String stepDescription;
	
	
	/************************************Constructor*****************************************************/
	
	public CheckListStep() {
		super();
		stepCode="";
		stepName="";
		stepDescription="";
	}
	
	/**************************************Getters And Setters******************************************/
	

	public int getStepNumber() {
		return stepNumber;
	}

	public void setStepNumber(int stepNumber) {
		this.stepNumber = stepNumber;
	}

	public String getStepCode() {
		return stepCode;
	}

	public void setStepCode(String stepCode) {
		if(stepCode!=null){
			this.stepCode = stepCode.trim();
		}
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		if(stepName!=null){
			this.stepName = stepName.trim();
		}
	}

	public Boolean isStepMandatory() {
		return stepMandatory;
	}

	public void setStepMandatory(boolean stepMandatory) {
		this.stepMandatory = stepMandatory;
	}

	public Boolean isStepStatus() {
		return stepStatus;
	}

	public void setStepStatus(boolean stepStatus) {
		this.stepStatus = stepStatus;
	}

	public String getStepDescription() {
		return stepDescription;
	}

	public void setStepDescription(String stepDescription) {
		if(stepDescription!=null){
			this.stepDescription = stepDescription.trim();
		}
	}
	


	/**************************************Overridden Method*************************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return duplicateStepCode(m)||duplicateStepNumber(m);
	}

	/**
	 * Method for Step Number Duplicate
	 * @param m
	 * @return
	 */
	public boolean duplicateStepNumber(SuperModel m)
	{
		CheckListStep entity = (CheckListStep) m;
		String number = entity.getStepNumber()+"";
		String curname=this.stepNumber+"";
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		number=number.replaceAll("\\s","");
		number=number.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(number.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((number.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	/**
	 * Method for Step Code Duplicate
	 * @param m
	 * @return
	 */
	public boolean duplicateStepCode(SuperModel m)
	{
		CheckListStep entity = (CheckListStep) m;
		String name = entity.getStepCode().trim();
		String curname=this.stepCode.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	

	}
	
	public static void initializeStepName(ObjectListBox<CheckListStep> listStep)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("stepStatus");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new CheckListStep());
		listStep.MakeLive(querry);
	}

	@Override
	public String toString() {
		return stepName.toString();
		
	}

	

}
