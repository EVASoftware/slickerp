package com.slicktechnologies.shared.common.shippingpackingprocess;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

@Entity
@Cache
public class ShippingMethod extends SuperModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5845247920832125881L;
	
	/**************************************Applicability Attributes******************************************/
	@Index
	protected String shippingMethodName;
	@Index
	protected String checkListName;
	@Index
	protected boolean status;
	protected String description;
	
	/**************************************Relational Attributes**********************************************/
	
	@Index
	protected Key<CheckListType>checklistKey;	
	
	
	/**************************************Constructor***************************************************/
	
	public ShippingMethod() {
		super();
		shippingMethodName="";
		checkListName="";
		description="";
	}
	
	/****************************************Getters And Setters********************************************/
	
	
	public String getShippingMethodName() {
		return shippingMethodName;
	}

	public void setShippingMethodName(String shippingMethodName) {
		if(shippingMethodName!=null){
			this.shippingMethodName = shippingMethodName.trim();
		}
	}

	public String getCheckListName() {
		return checkListName;
	}

	public void setCheckListName(String checkListName) {
		if(checkListName!=null){
			this.checkListName = checkListName.trim();
		}
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}

	
	/***************************************Overridden method*****************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return duplicateMethodName(m);
	}

	/**
	 * Method for Step Number Duplicate
	 * @param m
	 * @return
	 */
	public boolean duplicateMethodName(SuperModel m)
	{
		ShippingMethod entity = (ShippingMethod) m;
		String number = entity.getShippingMethodName()+"";
		String curname=this.shippingMethodName+"";
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		number=number.replaceAll("\\s","");
		number=number.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(number.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((number.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	/**
	 * Method for Step Code Duplicate
	 * @param m
	 * @return
	 */
	public boolean duplicateCheckListName(SuperModel m)
	{
		ShippingMethod entity = (ShippingMethod) m;
		String name = entity.getCheckListName().trim();
		String curname=this.checkListName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	
	public static void initializeShippingName(ObjectListBox<ShippingMethod> listStep)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new ShippingMethod());
		listStep.MakeLive(querry);
	}

	@Override
	public String toString() {
		return shippingMethodName.toString();
	}
	
	@OnSave
	@GwtIncompatible
	public void OnSave()
	{
		if(checkListName!=null){
		   checklistKey=ofy().load().type(CheckListType.class).filter("checkListName",this.checkListName).filter("companyId", getCompanyId()).keys().first().now();
		   
		}
	}
	
	@OnLoad
	@GwtIncompatible
	public void onLoad()
	{
		System.out.println("On Load");
	  if(checklistKey!=null)
	  {
		 CheckListType checkList=ofy().load().key(checklistKey).now();
		 if(checkList!=null)
			 this.checkListName=(checkList.getCheckListName());
			 
	}
	}

	
	
	
	
	

}
