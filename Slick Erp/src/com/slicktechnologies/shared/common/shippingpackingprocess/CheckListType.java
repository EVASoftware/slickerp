package com.slicktechnologies.shared.common.shippingpackingprocess;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

@Entity
@Embed
public class CheckListType extends SuperModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1079743787086316903L;
	@Index
	protected String checkListName;
	protected String checkListDesc;
	@Index
	protected boolean status;
	
	protected ArrayList<StepsDetails> stepsInfo;
	
	/************************************Getters And Setters***************************************/

	public String getCheckListName() {
		return checkListName;
	}

	public void setCheckListName(String checkListName) {
		if(checkListName!=null){
			this.checkListName = checkListName.trim();
		}
	}

	public String getCheckListDesc() {
		return checkListDesc;
	}

	public void setCheckListDesc(String checkListDesc) {
		if(checkListDesc!=null){
			this.checkListDesc = checkListDesc.trim();
		}
	}

	
	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<StepsDetails> getStepsInfo() {
		return stepsInfo;
	}

	public void setStepsInfo(ArrayList<StepsDetails> stepsInfo) {
		ArrayList<StepsDetails> arrSrteps=new ArrayList<StepsDetails>();
		arrSrteps.addAll(stepsInfo);
		this.stepsInfo = arrSrteps;
	}

/***************************************Overridden Method***********************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return duplicateCheckListName(m);
	}
	
	
	public boolean duplicateCheckListName(SuperModel m)
	{
		CheckListType entity = (CheckListType) m;
		String name = entity.getCheckListName().trim();
		String curname=this.checkListName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	

	}
	

	public static void initializeTypeName(ObjectListBox<CheckListType> listType)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new CheckListType());
		listType.MakeLive(querry);
	}

	@Override
	public String toString() {
		return checkListName.toString();
		
	}

}
