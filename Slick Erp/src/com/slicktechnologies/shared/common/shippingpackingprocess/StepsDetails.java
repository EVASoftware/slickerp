package com.slicktechnologies.shared.common.shippingpackingprocess;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class StepsDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2575617105028193803L;
	
	/***********************************Applicability Attributes*****************************************/
	
	protected String stepDetailsCode;
	protected String stepDetailsName;
	protected String stepDetailsMandatory;
	protected int stepDetailsNumber;

	/*************************************Getters And Setters******************************************/
	
	public String getStepDetailsCode() {
		return stepDetailsCode;
	}
	public void setStepDetailsCode(String stepDetailsCode) {
		if(stepDetailsCode!=null){
			this.stepDetailsCode = stepDetailsCode.trim();
		}
	}
	
	
	public String getStepDetailsName() {
		return stepDetailsName;
	}
	public void setStepDetailsName(String stepDetailsName) {
		if(stepDetailsName!=null){
			this.stepDetailsName = stepDetailsName.trim();
		}
	}
	
	
	public String getStepDetailsMandatory() {
		return stepDetailsMandatory;
	}
	public void setStepDetailsMandatory(String stepDetailsMandatory) {
		if(stepDetailsMandatory!=null){
			this.stepDetailsMandatory = stepDetailsMandatory.trim();
		}
	}
	
	
	public int getStepDetailsNumber() {
		return stepDetailsNumber;
	}
	public void setStepDetailsNumber(int stepDetailsNumber) {
		this.stepDetailsNumber = stepDetailsNumber;
	}
	
	

}
