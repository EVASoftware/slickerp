package com.slicktechnologies.shared.common.shippingpackingprocess;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

@Entity
@Cache
public class PackingMethod extends SuperModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2803931154564067534L;
	/**************************************Applicability Attributes******************************************/
	@Index
	protected String packingMethodName;
	protected String checkListName;
	@Index
	protected boolean status;
	protected String description;
	
	/**************************************Constructor***************************************************/
	
	public PackingMethod() {
		super();
		packingMethodName="";
		checkListName="";
		description="";
	}
	
	/****************************************Getters And Setters********************************************/
	
	
	public String getPackingMethodName() {
		return packingMethodName;
	}

	public void setPackingMethodName(String packingMethodName) {
		if(packingMethodName!=null){
			this.packingMethodName = packingMethodName.trim();
		}
	}

	public String getCheckListName() {
		return checkListName;
	}

	public void setCheckListName(String checkListName) {
		if(checkListName!=null){
			this.checkListName = checkListName.trim();
		}
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}

	
	
	

	@Override
	public boolean isDuplicate(SuperModel m) {
		return duplicateMethodName(m);
	}
	
	
	/**
	 * Method for Step Number Duplicate
	 * @param m
	 * @return
	 */
	public boolean duplicateMethodName(SuperModel m)
	{
		PackingMethod entity = (PackingMethod) m;
		String number = entity.getPackingMethodName()+"";
		String curname=this.packingMethodName+"";
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		number=number.replaceAll("\\s","");
		number=number.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(number.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((number.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	/**
	 * Method for Check List Code Duplicate
	 * @param m
	 * @return
	 */
	public boolean duplicateCheckListName(SuperModel m)
	{
		PackingMethod entity = (PackingMethod) m;
		String name = entity.getCheckListName().trim();
		String curname=this.checkListName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	

	}
	

	public static void initializePackingName(ObjectListBox<PackingMethod> listStep)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new PackingMethod());
		listStep.MakeLive(querry);
	}

	@Override
	public String toString() {
		return packingMethodName.toString();
		
	}

}
