package com.slicktechnologies.shared.common.service;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class StackDetails implements Serializable {

	/**
	 * This is class is created on 08-Sept-2016 by Rahul, because of NBHC
	 * requirement in ServiceForm.
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = -3045140116426808234L;

	@Index
	String wareHouse; // This field is added to locate service is for which
						// warehouse.
	String warehouseCode;
	String shade;
	
	String storageLocation; // Added to define storageLocation of a particular
							// Warehouse.
	String stackNo; // Added to define stackNo/chambers present within
					// particular storageLocation.
	double qauntity;//This will add quantity of stacks

	String description; // Added whether they want to describe something about
						// the particular warehouse/storageLocation/stackNo.
	
//	boolean isRecordSelect;
	
	
	public StackDetails(){
		wareHouse="";
		warehouseCode="";
		shade="";
		storageLocation="";
		stackNo="";
		description="";
	}
	
	// Getter Setters

	/*
	 * Gets the WareHouse Name in Service
	 */
	public String getWareHouse() {
		return wareHouse;
	}

	/*
	 * Set wareHouse name in Service
	 */
	public void setWareHouse(String wareHouse) {
		this.wareHouse = wareHouse;
	}

	/*
	 * getStorageLocation in service
	 */
	public String getStorageLocation() {
		return storageLocation;
	}

	/*
	 * set storageLocation in service
	 */
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	/*
	 * Stack No getter
	 */
	public String getStackNo() {
		return stackNo;
	}

	/*
	 * Stack no setter
	 */
	public void setStackNo(String stackNo) {
		this.stackNo = stackNo;
	}

	/*
	 * gets Description
	 */
	public String getDescription() {
		return description;
	}

	/*
	 * setDescription
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getQauntity() {
		return qauntity;
	}

	public void setQauntity(double qauntity) {
		this.qauntity = qauntity;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getShade() {
		return shade;
	}

	public void setShade(String shade) {
		this.shade = shade;
	}

//	public boolean isRecordSelect() {
//		return isRecordSelect;
//	}
//
//	public void setRecordSelect(boolean isRecordSelect) {
//		this.isRecordSelect = isRecordSelect;
//	}
	
	

}
