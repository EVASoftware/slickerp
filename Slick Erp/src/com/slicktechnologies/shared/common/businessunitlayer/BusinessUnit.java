package com.slicktechnologies.shared.common.businessunitlayer;

import java.util.Vector;

import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.personlayer.Person;

// TODO: Auto-generated Javadoc
/**
 * Represents a Business Unit.
 */

public class BusinessUnit extends SuperModel 
{
	
	/** *********************************************Entity Attributes***************************************************. */
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8501491375116064162L;

	/** The point of contact relative to this Buisness Process. */
	protected Person pointOfContact;

	/** The business unit name. */
	@Index 
	protected String buisnessUnitName;

	/** The contact. */
	@Index 
	protected  Vector<Contact> contact;

	/** The social info. */
	protected  SocialInformation socialinfo;
	
	
	/**
	 * Instantiates a new business unit.
	 */
	public BusinessUnit()
	{
		pointOfContact=new Person();

		//Form right now contains only one Contact object so we are manually inserting it here all subclass will
		//get One Contact free
		pointOfContact=new Person();
		Contact temp=new Contact();
		contact=new Vector<Contact>();
		contact.add(temp);
		//inserting contact in person
		temp=new Contact();
		this.pointOfContact.getContact().add(temp);
		socialinfo = new SocialInformation();
		buisnessUnitName="";
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.helperlayer.SuperModel#getCompanyId()
	 */
	public Long getCompanyId() 
	{
		return companyId;
	}

	/**
	 * Gets the socialinfo.
	 * @return the socialinfo
	 */
	public SocialInformation getSocialInfo() 
	{
		return socialinfo;
	}

	/**
	 * Sets the socialinfo.
	 * @param value the new socialinfo
	 */
	public void setSocialInfo(SocialInformation value) 
	{
		this.socialinfo=value;
	}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.helperlayer.SuperModel#getCount()
	 */
	public int getCount()
	{
		return count;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.helperlayer.SuperModel#setCount(int)
	 */
	public void setCount(int count) 
	{
		this.count = count;
	}

	/**
	 * Gets the poc.
	 * @return the poc
	 */
	public Person getPoc() 
	{
		return pointOfContact;
	}

	/**
	 * Sets the poc.
	 * @param poc the new poc
	 */
	public void setPoc(Person poc) 
	{
		this.pointOfContact = poc;
	}
	
	

	/**
	 * Gets the contact.

	 * @return the contact
	 */
	public Vector<Contact> getContact()
	{
		return contact;
	}

	/**
	 * Gets the point of contact.
	 *
	 * @return the point of contact
	 */
	public Person getPointOfContact()
	{
		return pointOfContact;
	}

	/**
	 * Sets the point of contact.
	 *
	 * @param pointOfContact the new point of contact
	 */
	public void setPointOfContact(Person pointOfContact)
	{
		this.pointOfContact = pointOfContact;
	}

	
	/**
	 * Sets the contact.
	 *
	 * @param contact the new contact
	 */
	public void setContact(Vector<Contact> contact) 
	{
		this.contact = contact;
	}

	/**
	 * Gets the buisnessunit name.
	 * @return the buisnessunit name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getBusinessUnitName()
	{
		return buisnessUnitName;
	}

	/**
	 * Sets the buisnessunit name.
	 * @param buisnessunitName the new buisnessunit name
	 */
	public void setBusinessUnitName(String buisnessunitName) 
	{
		if(buisnessunitName!=null)
			this.buisnessUnitName = buisnessunitName.trim();
	}

	/**
	 * Sets the adress.
	 *
	 * @param o the o
	 * @return the int
	 */

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(SuperModel o) 
	{
		String type=buisnessUnitName.trim();
		BusinessUnit entity=(BusinessUnit) o;
		String type1= entity.buisnessUnitName;
		return type.compareTo(type1);
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.helperlayer.SuperModel#isDuplicate(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	public boolean isDuplicate( SuperModel model)
	{
		{
			BusinessUnit entity=(BusinessUnit) model;
			String name = entity.getBusinessUnitName().trim();
			name=name.replaceAll("\\s","");
			name=name.toLowerCase();
			
			String curname=buisnessUnitName.trim();
			curname=curname.replaceAll("\\s","");
			curname=curname.toLowerCase();
			//New Object is being added
			if(entity.id==null)
			{
				if(name.equals(curname))
					return true;
				else
					return false;
			}
			// Old object is being updated
			else
			{
				if(entity.id.equals(id))
					return false;	
				if(name.equals(curname)==true)
					return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return buisnessUnitName;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email)
	{
		this.contact.get(0).setEmail(email);
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail()
	{
		return this.contact.get(0).getEmail();
	}

	/**
	 * Gets the landline.
	 *
	 * @return the landline
	 */
	public Long getLandline()
	{
		return this.contact.get(0).getLandline();
	}

	/**
	 * Sets the landline.
	 *
	 * @param landline the new landline
	 */
	public void setLandline(Long landline)
	{
		this.contact.get(0).setLandline(landline);
	}

	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website)
	{
		this.contact.get(0).setWebsite(website);
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite()
	{
		return this.contact.get(0).getWebsite();
	}

	/**
	 * Gets the cell number1.
	 *
	 * @return the cell number1
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo =2 , isFieldUpdater = false, isSortable = true, title = "Phone")
	public  Long getCellNumber1()
	{
		return contact.get(0).getCellNo1();
	}

	/**
	 * Sets the cell number1.
	 *
	 * @param cellNuber1 the new cell number1
	 */
	public  void setCellNumber1(long cellNuber1)
	{
		contact.get(0).setCellNo1(cellNuber1);
	}

	/**
	 * Gets the cell number2.
	 *
	 * @return the cell number2
	 */
	public  Long getCellNumber2()
	{
		return contact.get(0).getCellNo2();
	}

	/**
	 * Sets the cell number2.
	 *
	 * @param cellNuber2 the new cell number2
	 */
	public  void setCellNumber2(long cellNuber2)
	{
		contact.get(0).setCellNo2(cellNuber2);
	}

	/**
	 * Gets the fax number.
	 *
	 * @return the fax number
	 */
	public  Long getFaxNumber()
	{
		return contact.get(0).getFaxNo();
	}

	/**
	 * Sets the fax number.
	 *
	 * @param faxno the new fax number
	 */
	public  void setFaxNumber(long faxno)
	{
		contact.get(0).setFaxNo(faxno);
	}


	/**
	 * Sets the poc name.
	 *
	 * @param name the new poc name
	 */
	public void setPocName(String name)
	{
		this.pointOfContact.setFullname(name);
	}

	/**
	 * Gets the poc name.
	 *
	 * @return the poc name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Contact Name")
	public String getPocName()
	{
		return  this.pointOfContact.getFullname();
	}

	/**
	 * Sets the poc landline.
	 *
	 * @param landline the new poc landline
	 */
	public void setPocLandline(long landline)
	{
		this.pointOfContact.getContact().get(0).setLandline(landline);
	}

	/**
	 * Gets the poc landline.
	 *
	 * @return the poc landline
	 */
	public Long getPocLandline()
	{
		return this.pointOfContact.getContact().get(0).getLandline();
	}

	/**
	 * Gets the poc cell.
	 *
	 * @return the poc cell
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Contact Phone")
	public Long getPocCell()
	{
		return this.pointOfContact.getContact().get(0).getCellNo1();
	}

	/**
	 * Sets the poc cell.
	 *
	 * @param cell the new poc cell
	 */
	public void setPocCell(long cell)
	{
		this.pointOfContact.getContact().get(0).setCellNo1(cell);
	}

	/**
	 * Gets the poc email.
	 *
	 * @return the poc email
	 */
	public String getPocEmail()
	{
		return this.pointOfContact.getContact().get(0).getEmail();
	}

	/**
	 * Sets the poc email.
	 *
	 * @param email the new poc email
	 */
	public void setPocEmail(String email)
	{
		this.pointOfContact.getContact().get(0).setEmail(email);
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(Address address)
	{
		this.contact.get(0).setAddress(address);
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress()
	{
		return contact.get(0).getAddress();
	}

}

