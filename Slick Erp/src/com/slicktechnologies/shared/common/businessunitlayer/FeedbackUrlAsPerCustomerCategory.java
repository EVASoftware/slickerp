package com.slicktechnologies.shared.common.businessunitlayer;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class FeedbackUrlAsPerCustomerCategory extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5973522377776232043L;
	
	String customerCategory;
	String feedbackUrl;
	
	

	public String getCustomerCategory() {
		return customerCategory;
	}



	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}



	public String getFeedbackUrl() {
		return feedbackUrl;
	}



	public void setFeedbackUrl(String feedbackUrl) {
		this.feedbackUrl = feedbackUrl;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
