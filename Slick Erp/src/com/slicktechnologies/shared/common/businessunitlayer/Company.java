package com.slicktechnologies.shared.common.businessunitlayer;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.Serializable;















import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.SearchCompositeAnnatonation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

/**
 * Represents Company
 */
@Entity

public class Company extends BusinessUnit implements Serializable
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7164313201588780483L;

	public static final String ACTIVE = "Active";

	public static final String DEMO = "Demo";

	public static final String EXPIRED = "Expired";

	public static final String INACTIVE = "Inacttive";

	private static final String ApiUrl = null;

	/** The service tax no. */
	protected String serviceTaxNo;

	/** The vat tax no. */
	protected String vatTaxNo;

	/** The pan card. */
	protected String panCard;

	/** The logo. */
	protected DocumentUpload logo;
	
	
	//***************rohan added these document uploads for sending in email *******************
	
	protected DocumentUpload uploadHeader;
	
	protected DocumentUpload uploadFooter;
	
	/**
	 *   Date : 08-02-2021
	 *   Des : Priyanka Added this Document upload for ID Card PDF.
	 */
	protected DocumentUpload idCardLogo;
	protected DocumentUpload idCarduploadHeader;
	protected DocumentUpload idCarduploadFooter;
	
	/**
	 *  End 
	 */

	/**
	 * Added by Rahul for Digital Signature
	 */
	protected DocumentUpload uploadDigitalSign,uploadPedioJson,uploadKretoJson,uploadPrioraJson;;
	
	String companyURL;
	
	
	String  startdate,enddate,startMonth,endMonth; 
	
	//*******************************changes ends here *****************************************

	/** The license. */
	
	protected String license;


	/** The company type. */
	@Index 
	protected String companyType;
	
	@Index
	protected Integer noOfUser;
	
	protected Integer noOfWeeks;
	
	@Index
	protected String accessUrl;
	@Index
	protected Date licenseStartDate;
	@Index
	protected String status;
	
	@Index
	protected Date licenseEndingDate;
	
	/**
	 * Rahul Verma added on 12 Sept 2017
	 * 
	 */
	@Index
	protected String signatoryText;
	@Index
	protected String companyGSTType;
	@Index
	protected String companyGSTTypeText;
	
	protected ArrayList<ArticleType> articleTypeDetails;

	/**
	 * Rahul Verma added these fields for license of android application
	 */
	@Index
	protected int pedioLicense;
	@Index
	protected int prioraLicense;
	@Index
	protected int kretoLicense;

	/**
	   * Date 31-3-2018
	   * By jayshree
	   * Des.to add the statecode and country code
	   */
	@Index
	protected String statecode;
	
	@Index
	protected String countrycode;
	
	//end By jayshree
	/***********************************************Relational Attributes****************************************************/
	Key<Config> companyTypeKey;

	/*************************************Default Ctor******************************************************/
	/**
	 * Instantiates a new company.
	 */
	/**
	 * Rahul Verma Added this
	 */
	String fcmServerKeyForPedio,fcmServerKeyForKreto,fcmServerKeyForPriora;
	String bucketName;
	
	/**
	 * Date : 04-04-2018 BY ANIL
	 * This list is used to stores the license details of different different application 
	 * with multiple start date, end date and no. of user.
	 */
	ArrayList<LicenseDetails> licenseDetailsList;
	
	
	
	
	
	
	/** Added by Viraj Date 31-10-2018 for Ordient Ventures to Add Company Profile and Terms And Conditions **/
	protected DocumentUpload companyProfile;
	protected DocumentUpload termsAndCondition;
	/** Ends **/
	
	/**
	 * Date : 02-12-2018 By ANIL
	 * 
	 */
	String pfGroupNum;
	String pfOfficeAt;
	
	/**15-12-2018 added by amol***/
	protected Date dateofcoverage;
	/**
	 *Updated By: Viraj
	 *Date: 14-01-2019
	 *Description: To save description added in company
	 */
	protected String description;
	/** Ends **/
	/**
	 * Updated By: Viraj 
	 * Date: 24-03-2019
	 * Description: To add franchise Information   
	 */
	protected String franchiseType;
	protected String brandUrl;
	protected String masterUrl;
	/** Ends **/
	
	/**
	 * @author Anil , Date : 10-04-2020
	 * This field stores display name for from email id 
	 * it is used during email communication
	 */
	
	String displayNameForCompanyEmailId;
	
	/**
	 * @author Amol , Date : 1-08-2020
	 *  to store invoiceid prefix raised by Ashwini Bhagwat 
	 * 
	 */
	String invoicePrefix;
	
	String merchantId;
	
	/**
	 * @author Anil @since 09-03-2021
	 * As per the nitin sir instruction we have to store feedback url on company master
	 */
	ArrayList<FeedbackUrlAsPerCustomerCategory> feedbackUrlList;
	
	/**
	 * @author Anil @since 18-02-2021
	 * adding upload composite to capture the back side of id card
	 * for Alkosh raised by Rahul & Nitin
	 */
	protected DocumentUpload idCardBackSideLogo;
	
	/**
	 * @author Anil @since 30-04-2021
	 * Capturing the scheduling link and the minimum duration for scheduling
	 */
	String schedulingUrl;
	int minimumDuration;
	
	protected String websiteServiceTermsAndConditions;
	protected String currency;
	protected String paymentGatewayId;
	protected String paymentGatewayKey;
	
	
	/**
	 * @author Anil @since 03-01-20222
	 * Telephony fields for pedio and priora app
	 * Requirement raised by Nitin
	 */
	
	String accountSid;
	String apiKey;
	String apiToken;
	String callerId;
	String apiUrl;
	
	/**
	 * @author Vijay Date : 26-04-2022
	 * Des :- To send message on Whats app storing whats app API URL and instance ID
	 */
	String whatsAppApiUrl;
	String whatsAppInstaceId;
	boolean whatsAppApiStatus;
	int mobileNoPrefix;
	String accessToken;
	
	/*
	 * @author Ashwini Patil Date:7-10-2022
	 * Des: to generate einvoice we need to store following credentials
	 */
	String customerId, userName, password, apiId, apiSecret,einvoicevendor,eInvoiceEnvironment;
	

	ArrayList<BranchUsernamePassword> branchUsernamePasswordList;
	
	String companyIntroduction;
	String zohoOrganisationID;
	String zohoPermissionsCode;
	String zohoClientID;
	String zohoClientSecret;
	String zohoSurveyUrl;
	Integer serviceCompletionDeadline;
	Integer invoiceEditDeadline;
	
	public Company()
	{
		super();
		logo=new DocumentUpload();
		
		uploadFooter= new DocumentUpload();
		uploadHeader= new DocumentUpload();
		
		idCardLogo=new DocumentUpload();
		idCarduploadHeader=new DocumentUpload();
		idCarduploadFooter=new DocumentUpload();
		
		/** Added by Viraj Date 31-10-2018 for Ordient Ventures to Add Company Profile and Terms And Conditions **/
		companyProfile = new DocumentUpload();
		termsAndCondition = new  DocumentUpload();
		/** Ends **/
		
		serviceTaxNo="";
		vatTaxNo="";
		panCard="";
		license="";
		companyType="";
		accessUrl="";
		companyURL="";

		signatoryText="";
		companyGSTType="";
		companyGSTTypeText="";
		articleTypeDetails=new ArrayList<ArticleType>();
		pedioLicense=0;
		prioraLicense=0;
		pedioLicense=0;
		fcmServerKeyForPedio="";
		fcmServerKeyForKreto="";
		fcmServerKeyForPriora="";
		bucketName="";
		
		
		/**
		   * Date 31-3-2018
		   * By jayshree
		   * Des.to add the statecode and country code
		   */
		 statecode="";
		 countrycode="";
		 
		 licenseDetailsList=new ArrayList<>();
		 pfGroupNum="";
		 pfOfficeAt="";
		 /**
		 *Updated By: Viraj
		 *Date: 14-01-2019
		 *Description: To save description added in company
		 */
		 description = new String();
		 /** Ends **/
		 /**
		 * Updated By: Viraj 
		 * Date: 24-03-2019
		 * Description: To add franchise Information   
		 */
		 franchiseType = new String();
		 brandUrl = new String();
		 masterUrl = new String();
		 /** Ends **/
		 displayNameForCompanyEmailId="";
		 
		 merchantId = "";
		 
		 feedbackUrlList=new ArrayList<FeedbackUrlAsPerCustomerCategory>();
		 
		 idCardBackSideLogo=new DocumentUpload();
		 
		 websiteServiceTermsAndConditions = "";
		 currency = "";
		 paymentGatewayId="";
		 paymentGatewayKey="";
		 
		 whatsAppApiUrl = "";
		 whatsAppInstaceId ="";
		 whatsAppApiStatus = false;
		 accessToken="";
		 
		 customerId="";
		 userName="";
		 password="";
		 apiId=""; 
		 apiSecret="";
		 branchUsernamePasswordList=new ArrayList<BranchUsernamePassword>();
		 companyIntroduction ="";
		 einvoicevendor="";
		 
		 zohoOrganisationID="";
		 zohoPermissionsCode="";
		 
	}
	
	
	
	

	public String getDisplayNameForCompanyEmailId() {
		return displayNameForCompanyEmailId;
	}




	public void setDisplayNameForCompanyEmailId(String displayNameForCompanyEmailId) {
		this.displayNameForCompanyEmailId = displayNameForCompanyEmailId;
	}




	/**
	 * Updated By: Viraj 
	 * Date: 24-03-2019
	 * Description: To add franchise Information   
	 */
	public String getFranchiseType() {
		return franchiseType;
	}

	public void setFranchiseType(String franchiseType) {
		this.franchiseType = franchiseType;
	}

	public String getBrandUrl() {
		return brandUrl;
	}

	public void setBrandUrl(String brandUrl) {
		this.brandUrl = brandUrl;
	}

	public String getMasterUrl() {
		return masterUrl;
	}
	
	public void setMasterUrl(String masterUrl) {
		this.masterUrl = masterUrl;
	}
	/** Ends **/


	public String getPfGroupNum() {
		return pfGroupNum;
	}



	public void setPfGroupNum(String pfGroupNum) {
		this.pfGroupNum = pfGroupNum;
	}



	public String getPfOfficeAt() {
		return pfOfficeAt;
	}



	public void setPfOfficeAt(String pfOfficeAt) {
		this.pfOfficeAt = pfOfficeAt;
	}


	
	
	
	
	
	
	
	
	

	public Date getDateofcoverage() {
		return dateofcoverage;
	}



	public void setDateofcoverage(Date dateofcoverage) {
		this.dateofcoverage = dateofcoverage;
	}



	public ArrayList<LicenseDetails> getLicenseDetailsList() {
		return licenseDetailsList;
	}



	public void setLicenseDetailsList(List<LicenseDetails> licenseDetailsList) {
		ArrayList<LicenseDetails> list=new ArrayList<LicenseDetails>();
		if(licenseDetailsList!=null){
			list.addAll(licenseDetailsList);
			this.licenseDetailsList = list;
		}else{
			this.licenseDetailsList=null;
		}
		
	}

	
	
	public String getBucketName() {
		return bucketName;
	}



	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}



	/**
	 * Gets the service tax no.
	 * @return the service tax no
	 */
	public String getServiceTaxNo() 
	{
		return serviceTaxNo;
	}

	/**
	 * Sets the service tax no.
	 * @param serviceTaxNo the new service tax no
	 */
	public void setServiceTaxNo(String serviceTaxNo)
	{
		if(serviceTaxNo!=null)
			this.serviceTaxNo = serviceTaxNo.trim();
	}

	/**
	 * Gets the vat tax no.
	 * @return the vat tax no
	 */
	public String getVatTaxNo()
	{
		return vatTaxNo;
	}

	/**
	 * Sets the vat tax no.
	 * @param vatTaxNo the new vat tax no
	 */
	public void setVatTaxNo(String vatTaxNo)
	{
		if(vatTaxNo!=null)
			this.vatTaxNo = vatTaxNo.trim();
	}
	/**
	 * Gets the pan card.
	 * @return the pan card
	 */
	public String getPanCard()
	{
		return panCard;
	}


	/**
	 * Sets the pan card.
	 * @param panCard the new pan card
	 */
	public void setPanCard(String panCard) 
	{
		if(panCard!=null)
			this.panCard = panCard.trim();
	}
	

	public Date getLicenseEndingDate() {
		return licenseEndingDate;
	}


	public void setLicenseEndingDate(Date licenseEndingDate) {
		
		System.out.println("getter n setter  end date = =  "+licenseEndingDate);
		if(licenseEndingDate!=null){
			this.licenseEndingDate = licenseEndingDate;
		}
		
		System.out.println("getter n setter  end date1 = =  "+this.licenseEndingDate);
	}

	

	public String getCompanyURL() {
		return companyURL;
	}

	public void setCompanyURL(String companyURL) {
		this.companyURL = companyURL;
	}

	public DocumentUpload getUploadHeader() {
		return uploadHeader;
	}

	public void setUploadHeader(DocumentUpload uploadHeader) {
		this.uploadHeader = uploadHeader;
	}

	public DocumentUpload getUploadFooter() {
		return uploadFooter;
	}

	public void setUploadFooter(DocumentUpload uploadFooter) {
		this.uploadFooter = uploadFooter;
	}
	
	/** Added by Viraj Date 31-10-2018 for Ordient Ventures **/
	public DocumentUpload getCompanyProfile() {
		return companyProfile;
	}
	public void setCompanyProfile(DocumentUpload companyProfile) {
		this.companyProfile = companyProfile;
	}

	public DocumentUpload getTermsAndCondition() {
		return termsAndCondition;
	}
	public void setTermsAndCondition(DocumentUpload termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}
	/** Ends **/
	/**
	 *Updated By: Viraj
	 *Date: 14-01-2019
	 *Description: To save description added in company
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	/** Ends **/

	/**
	 * Gets the logo.
	 * @return the logo
	 */
	public DocumentUpload getLogo() 
	{
		return logo;
	}

	/**
	 * Sets the logo.
	 * @param logo the new logo
	 */
	public void setLogo(DocumentUpload logo) 
	{
		this.logo = logo;
	}
	
	/**
	 * Gets the license.
	 * @return the license
	 */
	public String getLicense() 
	{
		return license;
	}

	/**
	 * Sets the license.
	 * @param license the new license
	 */
	public void setLicense(String license) 
	{
		if(license!=null)
			this.license = license.trim();
	}

	/**
	 * Sets the company type.
	 * @param companyType the new company type
	 */
	public void setCompanyType(String companyType)
	{
		if(companyType!=null)
			this.companyType=companyType.trim();
	}

	/**
	 * Gets the company type.
	 * @return the company type
	 */
	public String getCompanyType()
	{
		return companyType;
	}

	//corresponding to  toString field. To String field can be only one 

	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessunitlayer.BusinessUnit#toString()
	 */
	@Override
	public String toString()
	{
		return this.buisnessUnitName;
	}
	
	public static List<String> getStatusList()
	{
		ArrayList<String> custstatuslist=new ArrayList<String>();
		custstatuslist.add(ACTIVE);
		custstatuslist.add(DEMO);
		custstatuslist.add(EXPIRED);
		custstatuslist.add(INACTIVE);
		return custstatuslist;
	}

	
	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void onSave()
	{
		if(id==null)
			setLicenseStartDate(DateUtility.getDateWithTimeZone("IST",new Date()));
		companyTypeKey=MyUtility.getConfigKeyFromCondition(companyType,ConfigTypes.COMPANYTYPE.getValue());
		
	}
	
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{
		
		/**
		 * Date : 09-04-2018 By ANIL
		 * Loading old type of login details in updated login detail list at the time of loading company.
		 */
		
		Date todaysDate=DateUtility.getDateWithTimeZone("IST", new Date());
//		if(getLicenseEndingDate()!=null&&todaysDate.before(getLicenseEndingDate())){
		if(getLicenseDetailsList()==null||getLicenseDetailsList().size()==0){
			
			System.out.println("ADDING LOGIN DETAILS IN UPDATED LOGIN LIST");
			LicenseDetails licenseDet=new LicenseDetails();
			licenseDet.setLicenseType(AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE);
			licenseDet.setStartDate(getLicenseStartDate());
			licenseDet.setEndDate(getLicenseEndingDate());
			licenseDet.setStatus(true);
			licenseDet.setNoOfLicense(this.getNoOfUser());
			getLicenseDetailsList().add(licenseDet);
			
		}else{
			ArrayList<LicenseDetails> licList=new ArrayList<LicenseDetails>();
			for(LicenseDetails obj:getLicenseDetailsList()){
				if(obj.getLicenseType().equals(AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE)){
					licList.add(obj);
				}
			}
			
			if(licList.size()==0){
				LicenseDetails licenseDet=new LicenseDetails();
				licenseDet.setLicenseType(AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE);
				licenseDet.setStartDate(getLicenseStartDate());
				licenseDet.setEndDate(getLicenseEndingDate());
				licenseDet.setStatus(true);
				licenseDet.setNoOfLicense(this.getNoOfUser());
				getLicenseDetailsList().add(licenseDet);
			}
			
		}
		
		/**
		 * End
		 */
		
		
		
		/**
		 * Date : 06-04-20018 BY ANIL
		 * Marking license as Inactive ,if it passes the end date
		 */
		
//		if(getLicenseDetailsList()!=null&&getLicenseDetailsList().size()!=0){
//			System.out.println("ON LOAD CHECKING COMPANIES LICENSE END DATE "+todaysDate);
//			for(LicenseDetails licObj:getLicenseDetailsList()){
//				if(licObj.getLicenseType().equals(AppConstants.LICENSETYPELIST.EVA_ERP_LICENSE)){
//					System.out.println(" "+DateUtility.getDateWithTimeZone("IST", licObj.getEndDate()));
//					if(todaysDate.after(DateUtility.getDateWithTimeZone("IST", licObj.getEndDate()))){
//						System.out.println("MARKED LICENSE STATUS INACTIVE");
//						licObj.setStatus(false);
//					}
//				}
//			}
//		}
		
		/**
		 * EnD
		 */
		
		
		String companyName=MyUtility.getConfigNameFromKey(companyTypeKey);
		if(companyName!=null&&(companyName.equals("")==false))
		{
			if(companyName.equals("")==false)
				setCompanyType(companyName);
		}
	}

	@SearchAnnotation(Datatype = ColumnDatatype.INTEGERBOX, flexFormNumber = "10", title = "No. of Users", variableName = "noOfUsers")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = false, title = "License Start Date")
	public Integer getNoOfUser() {
		return noOfUser;
	}

	public void setNoOfUser(Integer noOfUser) {
		this.noOfUser = noOfUser;
	}

	public Integer getNoOfWeeks() {
		return noOfWeeks;
	}

	public void setNoOfWeeks(Integer noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.DATECOMPARATOR, flexFormNumber = "00", title = "License Start Date", variableName = "dateComparator",extra="Lead")
	@TableAnnotation(ColType = FormTypes.DATETEXTCELL, colNo = 3, isFieldUpdater = false, isSortable = false, title = "License Start Date")
	public Date getLicenseStartDate() {
		return licenseStartDate;
	}

	public void setLicenseStartDate(Date licenseStartDate) {
		this.licenseStartDate = licenseStartDate;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.LISTBOX, flexFormNumber = "01", title = "Status", variableName = "lbStatus",extra="getStatusArraylist()",colspan=2)
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getAccessUrl() {
		return accessUrl;
	}


	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}
	
	public ArrayList<ArticleType> getArticleTypeDetails() {
		return articleTypeDetails;
	}

	public void setArticleTypeDetails(ArrayList<ArticleType> articleTypeDetails) {
		ArrayList<ArticleType> arrAricle=new ArrayList<ArticleType>();
		arrAricle.addAll(articleTypeDetails);
		this.articleTypeDetails = arrAricle;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	
	
	public DocumentUpload getUploadDigitalSign() {
		return uploadDigitalSign;
	}

	public void setUploadDigitalSign(DocumentUpload uploadDigitalSign) {
		this.uploadDigitalSign = uploadDigitalSign;
	}

	
	public String getSignatoryText() {
		return signatoryText;
	}

	public void setSignatoryText(String signatoryText) {
		this.signatoryText = signatoryText;
	}

	public String getCompanyGSTType() {
		return companyGSTType;
	}

	public void setCompanyGSTType(String companyGSTType) {
		this.companyGSTType = companyGSTType;
	}

	public String getCompanyGSTTypeText() {
		return companyGSTTypeText;
	}

	public void setCompanyGSTTypeText(String companyGSTTypeText) {
		this.companyGSTTypeText = companyGSTTypeText;
	}
	
	/**
	   * Date 31-3-2018
	   * By jayshree
	   * Des.to add the statecode and country code
	   */
	
	public String getStateCode() {
		return statecode;
	}

	public void setStateCode(String statecode) {
		this.statecode = statecode;
	}
	
	public String getCountryCode() {
		return countrycode;
	}

	public void setCountryCode(String countrycode) {
		this.countrycode = countrycode;
	}
	
	//End By jayshree
	

	public int getPedioLicense() {
		return pedioLicense;
	}

	public void setPedioLicense(int pedioLicense) {
		this.pedioLicense = pedioLicense;
	}

	public int getPrioraLicense() {
		return prioraLicense;
	}

	public void setPrioraLicense(int prioraLicense) {
		this.prioraLicense = prioraLicense;
	}

	public int getKretoLicense() {
		return kretoLicense;
	}

	public void setKretoLicense(int kretoLicense) {
		this.kretoLicense = kretoLicense;
	}
	

	public String getFcmServerKeyForPedio() {
		return fcmServerKeyForPedio;
	}

	public void setFcmServerKeyForPedio(String fcmServerKeyForPedio) {
		this.fcmServerKeyForPedio = fcmServerKeyForPedio;
	}

	public String getFcmServerKeyForKreto() {
		return fcmServerKeyForKreto;
	}

	public void setFcmServerKeyForKreto(String fcmServerKeyForKreto) {
		this.fcmServerKeyForKreto = fcmServerKeyForKreto;
	}

	public String getFcmServerKeyForPriora() {
		return fcmServerKeyForPriora;
	}

	public void setFcmServerKeyForPriora(String fcmServerKeyForPriora) {
		this.fcmServerKeyForPriora = fcmServerKeyForPriora;
	}
	

	
	public DocumentUpload getUploadPedioJson() {
		return uploadPedioJson;
	}

	public void setUploadPedioJson(DocumentUpload uploadPedioJson) {
		this.uploadPedioJson = uploadPedioJson;
	}

	public DocumentUpload getUploadKretoJson() {
		return uploadKretoJson;
	}

	public void setUploadKretoJson(DocumentUpload uploadKretoJson) {
		this.uploadKretoJson = uploadKretoJson;
	}

	public DocumentUpload getUploadPrioraJson() {
		return uploadPrioraJson;
	}

	public void setUploadPrioraJson(DocumentUpload uploadPrioraJson) {
		this.uploadPrioraJson = uploadPrioraJson;
	}

	public String getInvoicePrefix() {
		return invoicePrefix;
	}

	public void setInvoicePrefix(String invoicePrefix) {
		this.invoicePrefix = invoicePrefix;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}




	/****************************************************************************************************************/

   
	@OnSave
   @GwtIncompatible
	public void getLicenseEndDate() {
		if(id==null){
		int planDurationinDay=getNoOfWeeks()*7;
	       Date startDate=getLicenseStartDate();
	       SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	       Calendar calendar = Calendar.getInstance();
	       calendar.setTime(startDate); // Now use today date.
	       calendar.add(Calendar.DATE, planDurationinDay);
	       
	       setLicenseEndingDate(calendar.getTime());
		}
	}



	/**
	 *   Date : 08-02-2021
	 *   Des : Priyanka Added this Document upload for ID Card PDF.
	 */
	public DocumentUpload getIdCardLogo() {
		return idCardLogo;
	}


	public void setIdCardLogo(DocumentUpload idCardLogo) {
		this.idCardLogo = idCardLogo;
	}


	public DocumentUpload getIdCarduploadHeader() {
		return idCarduploadHeader;
	}


	public void setIdCarduploadHeader(DocumentUpload idCarduploadHeader) {
		this.idCarduploadHeader = idCarduploadHeader;
	}


	public DocumentUpload getIdCarduploadFooter() {
		return idCarduploadFooter;
	}




	public void setIdCarduploadFooter(DocumentUpload idCarduploadFooter) {
		this.idCarduploadFooter = idCarduploadFooter;
	}




	public ArrayList<FeedbackUrlAsPerCustomerCategory> getFeedbackUrlList() {
		return feedbackUrlList;
	}




	public void setFeedbackUrlList(List<FeedbackUrlAsPerCustomerCategory> feedbackUrlList) {
		ArrayList<FeedbackUrlAsPerCustomerCategory> list=new ArrayList<FeedbackUrlAsPerCustomerCategory>();
		if(feedbackUrlList!=null){
			list.addAll(feedbackUrlList);
			this.feedbackUrlList = list;
		}else{
			this.feedbackUrlList=null;
		}
	}




	public DocumentUpload getIdCardBackSideLogo() {
		return idCardBackSideLogo;
	}




	public void setIdCardBackSideLogo(DocumentUpload idCardBackSideLogo) {
		this.idCardBackSideLogo = idCardBackSideLogo;
	}




	public String getSchedulingUrl() {
		return schedulingUrl;
	}




	public void setSchedulingUrl(String schedulingUrl) {
		this.schedulingUrl = schedulingUrl;
	}




	public int getMinimumDuration() {
		return minimumDuration;
	}




	public void setMinimumDuration(int minimumDuration) {
		this.minimumDuration = minimumDuration;
	}




	public String getWebsiteServiceTermsAndConditions() {
		return websiteServiceTermsAndConditions;
	}




	public void setWebsiteServiceTermsAndConditions(
			String websiteServiceTermsAndConditions) {
		this.websiteServiceTermsAndConditions = websiteServiceTermsAndConditions;
	}




	public String getCurrency() {
		return currency;
	}




	public void setCurrency(String currency) {
		this.currency = currency;
	}




	public String getPaymentGatewayId() {
		return paymentGatewayId;
	}




	public void setPaymentGatewayId(String paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}




	public String getPaymentGatewayKey() {
		return paymentGatewayKey;
	}




	public void setPaymentGatewayKey(String paymentGatewayKey) {
		this.paymentGatewayKey = paymentGatewayKey;
	}




	public String getAccountSid() {
		return accountSid;
	}




	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}




	public String getApiKey() {
		return apiKey;
	}




	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}




	public String getApiToken() {
		return apiToken;
	}




	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}




	public String getCallerId() {
		return callerId;
	}




	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}
	
	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}




	public String getWhatsAppApiUrl() {
		return whatsAppApiUrl;
	}




	public void setWhatsAppApiUrl(String whatsAppApiUrl) {
		this.whatsAppApiUrl = whatsAppApiUrl;
	}




	public String getWhatsAppInstaceId() {
		return whatsAppInstaceId;
	}




	public void setWhatsAppInstaceId(String whatsAppInstaceId) {
		this.whatsAppInstaceId = whatsAppInstaceId;
	}




	public boolean isWhatsAppApiStatus() {
		return whatsAppApiStatus;
	}




	public void setWhatsAppApiStatus(boolean whatsAppApiStatus) {
		this.whatsAppApiStatus = whatsAppApiStatus;
	}




	public int getMobileNoPrefix() {
		return mobileNoPrefix;
	}




	public void setMobileNoPrefix(int mobileNoPrefix) {
		this.mobileNoPrefix = mobileNoPrefix;
	}
	
	public String getAccessToken() {
		return accessToken;
	}


	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getApiSecret() {
		return apiSecret;
	}


	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}


	public String getApiId() {
		return apiId;
	}


	public void setApiId(String apiId) {
		this.apiId = apiId;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getCustomerId() {
		return customerId;
	}


	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public ArrayList<BranchUsernamePassword> getBranchUsernamePasswordList() {
		return branchUsernamePasswordList;
	}


	public void setBranchUsernamePasswordList(List<BranchUsernamePassword> list) {
		
		ArrayList<BranchUsernamePassword> arrlist=new ArrayList<BranchUsernamePassword>();
		if(list!=null){
			arrlist.addAll(list);
			this.branchUsernamePasswordList = arrlist;
		}else{
			this.branchUsernamePasswordList = arrlist;
		}
	}



	public String getCompanyIntroduction() {
		return companyIntroduction;
	}
	
	public void setCompanyIntroduction(String companyIntroduction) {
		this.companyIntroduction = companyIntroduction;
	}
	
	public String getEinvoicevendor() {
		return einvoicevendor;
	}

	public void setEinvoicevendor(String einvoicevendor) {
		this.einvoicevendor = einvoicevendor;
	}
	
	public String geteInvoiceEnvironment() {
		return eInvoiceEnvironment;
	}


	public void seteInvoiceEnvironment(String eInvoiceEnvironment) {
		this.eInvoiceEnvironment = eInvoiceEnvironment;
	}





	public String getZohoOrganisationID() {
		return zohoOrganisationID;
	}


	public void setZohoOrganisationID(String zohoOrganisationID) {
		this.zohoOrganisationID = zohoOrganisationID;
	}


	public String getZohoPermissionsCode() {
		return zohoPermissionsCode;
	}


	public void setZohoPermissionsCode(String zohoPermissionsCode) {
		this.zohoPermissionsCode = zohoPermissionsCode;
	}





	public String getZohoClientID() {
		return zohoClientID;
	}





	public void setZohoClientID(String zohoClientID) {
		this.zohoClientID = zohoClientID;
	}





	public String getZohoClientSecret() {
		return zohoClientSecret;
	}





	public void setZohoClientSecret(String zohoClientSecret) {
		this.zohoClientSecret = zohoClientSecret;
	}





	public String getZohoSurveyUrl() {
		return zohoSurveyUrl;
	}





	public void setZohoSurveyUrl(String zohoSurveyUrl) {
		this.zohoSurveyUrl = zohoSurveyUrl;
	}





	public Integer getServiceCompletionDeadline() {
		return serviceCompletionDeadline;
	}





	public void setServiceCompletionDeadline(Integer serviceCompletionDeadline) {
		this.serviceCompletionDeadline = serviceCompletionDeadline;
	}





	public Integer getInvoiceEditDeadline() {
		return invoiceEditDeadline;
	}





	public void setInvoiceEditDeadline(Integer invoiceEditDeadline) {
		this.invoiceEditDeadline = invoiceEditDeadline;
	}

	
	
}
