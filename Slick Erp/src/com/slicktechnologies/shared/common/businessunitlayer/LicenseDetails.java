package com.slicktechnologies.shared.common.businessunitlayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class LicenseDetails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2860234605121865400L;


	@Index
	long contractCount;
	@Index
	String licenseType;
	@Index
	Date startDate;
	@Index
	Date endDate;
	int duration;
	int noOfLicense;
	
	@Index
	boolean status;
	
	public LicenseDetails() {
		super();
	}
	
	
	
	

	public boolean getStatus() {
		return status;
	}





	public void setStatus(boolean status) {
		this.status = status;
	}





	public long getContractCount() {
		return contractCount;
	}





	public void setContractCount(long contractCount) {
		this.contractCount = contractCount;
	}





	public String getLicenseType() {
		return licenseType;
	}





	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}





	public Date getStartDate() {
		return startDate;
	}





	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getNoOfLicense() {
		return noOfLicense;
	}

	public void setNoOfLicense(int noOfLicense) {
		this.noOfLicense = noOfLicense;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
