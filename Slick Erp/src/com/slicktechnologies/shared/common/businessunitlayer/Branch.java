package com.slicktechnologies.shared.common.businessunitlayer;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.HasCalendar;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Repersents A Branch.
 */
@Entity
@Cache
public class Branch extends BusinessUnit implements HasCalendar,Serializable  
{	
	/***********************************************Entity Attributes****************************************************/
	/**The Constant serialVersionUID. */
	private static final long serialVersionUID = 982013130256104959L;
	
	/**The status of this branch can be active or inactive */
	@Index
	protected boolean status;
	
	protected Calendar calendar;
	/***********************************************Join Kind Attribute*****************************************************/
	@Index
	protected Key<BranchRelation> keyName;
	@Index
	protected String calendarName;
	protected boolean nameChanged=false;
	/*Added By Rahul*/
	@Index 
	protected String GSTINNumber;
	
	/**
	 * Date 16/12/2017
	 * By Jayshree
	 * Des.add the branchcode button
	 */
	@Index
	protected String branchCode;
	/*******************************************Default Ctor********************************************************/
	/**
	 * Instantiates a new branch.
	 */
	
	/**
	 *  nidhi
	 *  5-04-2018
	 *  for brach as a company
	 *  
	 */
	protected DocumentUpload uploadHeader;
	
	protected DocumentUpload uploadFooter;

	/**
	 * Added by Rahul for Digital Signature
	 */
	protected DocumentUpload uploadDigitalSign;/** The logo. */
	protected DocumentUpload logo;
	@Index
	protected String signatoryText;
	protected String paymentMode;
	
	/**
	 * end
	 */
	/** date 13.04.2018 added by komal for pestmoeterm **/
	@Index
	protected String referenceNumber;
	/**
	 * end
	 */
	
	
	/**
	 * Date: 02-10-2018 By ANIL
	 * Storing ESIC code ,PT code and LWF code of company
	 */
	
	protected String esicCode;
	protected String ptCode;
	protected String lwfCode;
	
	/**
	 * UPdate By: Viraj
	 * Date: 12-01-2019
	 * Description: To save fumigation info
	 */
	protected String shortName;
	protected String regNoMBR;
	protected String regNoALP;
	/** Ends **/
	/** date 4.5.2019 added by komal for reference number 2(and profit center for nbhc)**/
	@Index
	protected String referenceNumber2;
	
	
	
	/**
	 * @author Anil , Date : 29-11-2019
	 * Capturing paid days for payroll, currently per day salary calculated on the basis of month days
	 */
	
	double paidDaysInMonth;
	boolean exceptionForMonth;
	double paidDaysForExceptionalMonth;
	String specificMonth;
	String extraDayCompName;
	
	/**
	 * @author Abhinav
	 * @since 06/12/2019
	 * For Bitco by Rahul Tiwari, We Added a "Correspondence Name" field in branch Master
	 */
	String correspondenceName;


	/**
	 * @author Anil
	 * @since 02-05-2020
	 * Adding shift 
	 */
	Shift shift;
	@Index String shiftName;
	/**
	 * @author Amol , Date : 1-08-2020
	 *  to store invoiceid prefix raised by Ashwini Bhagwat 
	 * 
	 */
	String invoicePrefix;
	
	String PANNumber;
	
	/**
	 * @author Anil @since 01-10-2021
	 * Capturing invoice title 
	 * Raised by Rahul Tiwari and Nitin Sir
	 */
	String invoiceTitle;
	boolean printBankDetails;
	
	
	protected ArrayList<ArticleType> articleTypeDetails;


	protected boolean einvoiceApplicable;

	//Ashwini Patil Date:13-05-2024 Added for ultima search. If number range is selected on branch then it will automatically get selected on contract screen.
	String numberRange;

	public Branch() 
	{
		super();
		nameChanged=false;	
		calendarName="";
		GSTINNumber="";
		branchCode="";
		
		/**
		 *  nidhi
		 *  05-04-2018
		 *  for branch as a company
		 */
		logo=new DocumentUpload();
		
		uploadFooter= new DocumentUpload();
		uploadHeader= new DocumentUpload();
		signatoryText = "";
		uploadDigitalSign = new DocumentUpload();
		paymentMode = "";
		
		esicCode="";
		ptCode="";
		lwfCode="";
		referenceNumber2="";
		correspondenceName="";
		PANNumber = "";
		articleTypeDetails=new ArrayList<ArticleType>();

		einvoiceApplicable = false;
		numberRange="";
	}




	public String getEsicCode() {
		return esicCode;
	}




	public void setEsicCode(String esicCode) {
		this.esicCode = esicCode;
	}




	public String getPtCode() {
		return ptCode;
	}




	public void setPtCode(String ptCode) {
		this.ptCode = ptCode;
	}




	public String getLwfCode() {
		return lwfCode;
	}




	public void setLwfCode(String lwfCode) {
		this.lwfCode = lwfCode;
	}

	/**
	 * Gets the status.
	 * @return the status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getstatus() 
	{
		return status;
	}

	/**
	 * Sets the status.
	 * @param status the new status
	 */
	public void setstatus(boolean status) 
	{
		this.status=status;	
	}

	@Override
	public void setBusinessUnitName(String buisnessunitName) 
	{
		
		if(buisnessunitName!=null)
		{
		System.out.println("Boolean Condition is "+buisnessUnitName.equals(buisnessunitName.trim()));
		if(!(this.buisnessUnitName.equals(buisnessunitName.trim())))
			nameChanged=true;
		this.buisnessUnitName=buisnessunitName;
		}
	}

	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void onSave()
	{
		if(id==null)
		{
			BranchRelation branchRelation=new BranchRelation();
			branchRelation.setName(buisnessUnitName);
			
			keyName=(Key<BranchRelation>) MyUtility.createRelationalKey(branchRelation);
			System.out.println("kEY NAME "+keyName);
		}
		else
		{
			
			if(nameChanged==true)
			{
				if(keyName!=null)
				{
					BranchRelation branchRelation=new BranchRelation();
					branchRelation.setName(buisnessUnitName);
					MyUtility.updateRelationalEntity(branchRelation,keyName);
					System.out.println("iii");
				}
			}
		}
		
		//To handle Batch Assignments
		if(calendar!=null)
		{
			calendarName=calendar.getCalName();
		}
		
	}
	
	
	@OnLoad
	@GwtIncompatible
	public void onLoad()
	{
		
		if(calendarName!=null)
		{
		   calendar=ofy().load().type(Calendar.class).filter("calName",calendarName).first().now();
		   
	    }
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getGSTINNumber() {
		return GSTINNumber;
	}

	public void setGSTINNumber(String gSTINNumber) {
		GSTINNumber = gSTINNumber;
	}
	
	
	/**
	 * Date 16/12/2017
	 * By Jayshree
	 * To add the branchCode Changes are done
	 */
	
	
	public void setBranchCode(String branchcode) {
		branchCode=branchcode;
		
	}

	public String getBranchCode() {
		// TODO Auto-generated method stub
		return  branchCode;
	}
	//end by jayshree
	
	/**
	 * nidhi
	 */
	public DocumentUpload getUploadHeader() {
		return uploadHeader;
	}

	public void setUploadHeader(DocumentUpload uploadHeader) {
		this.uploadHeader = uploadHeader;
	}

	public DocumentUpload getUploadFooter() {
		return uploadFooter;
	}

	public void setUploadFooter(DocumentUpload uploadFooter) {
		this.uploadFooter = uploadFooter;
	}

	public DocumentUpload getUploadDigitalSign() {
		return uploadDigitalSign;
	}

	public void setUploadDigitalSign(DocumentUpload uploadDigitalSign) {
		this.uploadDigitalSign = uploadDigitalSign;
	}

	public DocumentUpload getLogo() {
		return logo;
	}

	public void setLogo(DocumentUpload logo) {
		this.logo = logo;
	}

	public String getSignatoryText() {
		return signatoryText;
	}

	public void setSignatoryText(String signatoryText) {
		this.signatoryText = signatoryText;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * end
	 */
	
	/**
	 * Update By: Viraj
	 * Date: 12-01-2019
	 * Description: To save fumigation info
	 */
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getRegNoMBR() {
		return regNoMBR;
	}

	public void setRegNoMBR(String regNoMBR) {
		this.regNoMBR = regNoMBR;
	}

	public String getRegNoALP() {
		return regNoALP;
	}

	public void setRegNoALP(String regNoALP) {
		this.regNoALP = regNoALP;
	}
	/** Ends **/




	public String getReferenceNumber2() {
		return referenceNumber2;
	}




	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}




	public double getPaidDaysInMonth() {
		return paidDaysInMonth;
	}




	public void setPaidDaysInMonth(double paidDaysInMonth) {
		this.paidDaysInMonth = paidDaysInMonth;
	}




	public boolean isExceptionForMonth() {
		return exceptionForMonth;
	}




	public void setExceptionForMonth(boolean exceptionForMonth) {
		this.exceptionForMonth = exceptionForMonth;
	}




	public double getPaidDaysForExceptionalMonth() {
		return paidDaysForExceptionalMonth;
	}




	public void setPaidDaysForExceptionalMonth(double paidDaysForExceptionalMonth) {
		this.paidDaysForExceptionalMonth = paidDaysForExceptionalMonth;
	}




	public String getExtraDayCompName() {
		return extraDayCompName;
	}




	public void setExtraDayCompName(String extraDayCompName) {
		this.extraDayCompName = extraDayCompName;
	}




	public String getSpecificMonth() {
		return specificMonth;
	}




	public void setSpecificMonth(String specificMonth) {
		this.specificMonth = specificMonth;
	}

	public String getCorrespondenceName() {
		return correspondenceName;
	}
	
	public void setCorrespondenceName(String correspondenceName) {
		this.correspondenceName = correspondenceName;
	}




	public Shift getShift() {
		return shift;
	}




	public void setShift(Shift shift) {
		this.shift = shift;
	}




	public String getShiftName() {
		return shiftName;
	}




	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

		public String getInvoicePrefix() {
		return invoicePrefix;
	}

	public void setInvoicePrefix(String invoicePrefix) {
		this.invoicePrefix = invoicePrefix;
	}


	public String getPANNumber() {
		return PANNumber;
	}

	public void setPANNumber(String pANNumber) {
		PANNumber = pANNumber;
	}


	public boolean isPrintBankDetails() {
		return printBankDetails;
	}




	public void setPrintBankDetails(boolean printBankDetails) {
		this.printBankDetails = printBankDetails;
	}




	public String getInvoiceTitle() {
		return invoiceTitle;
	}




	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}




	public ArrayList<ArticleType> getArticleTypeDetails() {
		return articleTypeDetails;
	}




	public void setArticleTypeDetails(ArrayList<ArticleType> articleTypeDetails) {
		this.articleTypeDetails = articleTypeDetails;
	}




	public boolean isEinvoiceApplicable() {
		return einvoiceApplicable;
	}




	public void setEinvoiceApplicable(boolean einvoiceApplicable) {
		this.einvoiceApplicable = einvoiceApplicable;
	}




	public String getNumberRange() {
		return numberRange;
	}




	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}
	
	
	
	
	/***********************************************************************************************************************/
}
