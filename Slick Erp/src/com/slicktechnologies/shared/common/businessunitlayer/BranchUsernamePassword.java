package com.slicktechnologies.shared.common.businessunitlayer;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class BranchUsernamePassword extends SuperModel{
	
//	private static final long serialVersionUID = 5973522377776232043L;
	String branchName;
	int branchId;
	String username;
	String password;

	public BranchUsernamePassword() {
		 branchName="";
		 branchId=0;
		 username="";
		 password="";
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
