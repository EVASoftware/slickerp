package com.slicktechnologies.shared.common.documenthistorydetails;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.slicktechnologies.server.utility.DateUtility;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SaveHistory {
	
	@GwtIncompatible
	public static void saveHistoryData(Long companyId,String moduleName,String documentName,int docId,String transactionType,String employeeName,String userName)
	{
		/**
		 * @author Anil , Date : 23-10-2019
		 * Commenting approval history code
		 * it takes lot of time to save approval request
		 * Raised BY NBHC
		 * 
		 */
//		DocumentHistory historyObj=new DocumentHistory();
//
//		if(companyId!=null){
//			historyObj.setCompanyId(companyId);
//		}
//		if(moduleName!=null){
//			historyObj.setModuleName(moduleName);
//		}
//		if(documentName!=null){
//			historyObj.setDocumentName(documentName);
//		}
//		if(docId!=0){
//			historyObj.setDocumentId(docId);
//		}
//		if(transactionType!=null){
//			historyObj.setTransactionType(transactionType);
//		}
//		if(employeeName!=null){
//			historyObj.setCreatedBy(employeeName);
//		}
//		
//		if(userName!=null){
//			historyObj.setUserName(userName);
//		}
//		
//		int count=ofy().load().type(DocumentHistory.class).count();
//		
////		DateFormat dateFormat=new SimpleDateFormat("HH:mm:ss");
////		Date date=new Date();
////		String time=dateFormat.format(new Date().getTime());
//		
//		historyObj.setTransactionDate(DateUtility.getDateWithTimeZone("IST", new Date()));
//		historyObj.setTransactionTime(DateUtility.getTimeWithTimeZone("IST", new Date()));
//		
//		historyObj.setCount(count+1);
//		
//		if(historyObj!=null){
//			ofy().save().entity(historyObj).now();
//		}
		
	}
}
