package com.slicktechnologies.shared.common.documenthistorydetails;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.GLAccount;

@Entity
public class DocumentHistory extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8973563751084695331L;
	
	/*************************************Constants**************************************************/
	public static final String CREATED = "Created";
	public static final String UPDATED = "Updated";
	public static final String APPROVALREQUESTED = "Approval Requested";
	public static final String CANCELLED= "Cancelled";
	public static final String APPROVALCANCELLED="Approval Request Cancelled";
	public static final String APPROVALREJECTED="Approval Rejected";
	public static final String APPROVED="Approved";
	
	/**************************************Applicability Attributes************************************/
	
	@Index
	protected String moduleName;
	@Index
	protected String documentName;
	@Index
	protected int documentId;
	@Index
	protected String transactionType;
	@Index
	protected Date transactionDate;
	@Index
	protected String transactionTime;
	@Index
	protected String createdBy;
	@Index
	protected String userName;
	
	
	/*************************************Constructor****************************************************/
	
	
	public DocumentHistory() {
		super();
		moduleName="";
		documentName="";
		transactionType="";
		transactionDate=new Date();
		transactionTime="";
		createdBy="";
		userName="";
	}
	
	/****************************************Getters And Setters***************************************/

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		if(moduleName!=null){
			this.moduleName = moduleName.trim();
		}
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		if(documentName!=null){
			this.documentName = documentName;
		}
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		if(transactionType!=null){
			this.transactionType = transactionType;
		}
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		if(transactionDate!=null){
			this.transactionDate = transactionDate;
		}
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		if(transactionTime!=null){
			this.transactionTime = transactionTime;
		}
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		if(createdBy!=null){
			this.createdBy = createdBy;
		}
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		if(userName!=null){
			this.userName = userName.trim();
		}
	}

	/********************************************Overridden Method***************************************/

	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	/*****************************************Business Logic*******************************************/
	
	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CREATED);
		statuslist.add(UPDATED);
		statuslist.add(APPROVALREQUESTED);
		statuslist.add(APPROVALCANCELLED);
		statuslist.add(APPROVED);
		statuslist.add(APPROVALREJECTED);
		statuslist.add(CANCELLED);
		
		return statuslist;
	}
	

}
