package com.slicktechnologies.shared.common.sms;

import java.io.Serializable;

import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;

public class SMSDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 523702469243214448L;

	protected SmsTemplate smsTemplate;
	
	protected SuperModel model;
	
	protected String mobileNumber;
	
	protected String appId;
	
	protected String appURL;
	
	protected String pdfURL;
	
	protected String landingPageText;
	
	protected String entityName;
	
	protected String communicationChannel;
	
	protected String message;
	
	/**
	 * @author Vijay Date :- 03-06-2022
	 * Des :- below are used only when automatic message sending using
	 * validateAndSendSMS() common method
	 */
	protected String moduleName;
	protected String documentName;
	/**
	 * ends here
	 */
	
	public SMSDetails(){
		appId ="";
		appURL ="";
		pdfURL ="";
		landingPageText="";
		entityName ="";
		communicationChannel ="";
		message = "";
		moduleName="";
		documentName="";
	}


	public SmsTemplate getSmsTemplate() {
		return smsTemplate;
	}


	public void setSmsTemplate(SmsTemplate smsTemplate) {
		this.smsTemplate = smsTemplate;
	}


	public SuperModel getModel() {
		return model;
	}


	public void setModel(SuperModel model) {
		this.model = model;
	}


	public String getAppId() {
		return appId;
	}


	public void setAppId(String appId) {
		this.appId = appId;
	}


	public String getAppURL() {
		return appURL;
	}


	public void setAppURL(String appURL) {
		this.appURL = appURL;
	}


	public String getPdfURL() {
		return pdfURL;
	}


	public void setPdfURL(String pdfURL) {
		this.pdfURL = pdfURL;
	}


	public String getLandingPageText() {
		return landingPageText;
	}


	public void setLandingPageText(String landingPageText) {
		this.landingPageText = landingPageText;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getEntityName() {
		return entityName;
	}


	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}


	public String getCommunicationChannel() {
		return communicationChannel;
	}


	public void setCommunicationChannel(String communicationChannel) {
		this.communicationChannel = communicationChannel;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}




	public String getDocumentName() {
		return documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public String getModuleName() {
		return moduleName;
	}


	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	
	
	
}
