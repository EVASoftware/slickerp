package com.slicktechnologies.shared.common.workorder;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class WorkOrderProductDetails extends SuperModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1037087446629779675L;
	
	private int compProdId;
	private int productId;
	private String productCode;
	private String productName;
	private String productCategory;
	private double productQty;
	private double productPrice;
	private String productUOM;
	
	private String prodDescription;

	public WorkOrderProductDetails() {
		productCode="";
		productCategory="";
		productName="";
		productUOM="";
		
		prodDescription="";
	}
	
	
	
	
	public int getCompProdId() {
		return compProdId;
	}




	public void setCompProdId(int compProdId) {
		this.compProdId = compProdId;
	}




	public int getProductId() {
		return productId;
	}




	public void setProductId(int productId) {
		this.productId = productId;
	}




	public String getProductCode() {
		return productCode;
	}




	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}




	public String getProductName() {
		return productName;
	}




	public void setProductName(String productName) {
		this.productName = productName;
	}




	public String getProductCategory() {
		return productCategory;
	}




	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}




	public double getProductQty() {
		return productQty;
	}




	public void setProductQty(double productQty) {
		this.productQty = productQty;
	}




	public double getProductPrice() {
		return productPrice;
	}




	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}




	public String getProductUOM() {
		return productUOM;
	}




	public void setProductUOM(String productUOM) {
		this.productUOM = productUOM;
	}

	public String getProdDescription() {
		return prodDescription;
	}


	public void setProdDescription(String prodDescription) {
		this.prodDescription = prodDescription;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
