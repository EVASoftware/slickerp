package com.slicktechnologies.shared.common.workorder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import static com.googlecode.objectify.ObjectifyService.ofy;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.Screen;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.democonfiguration.DemoConfigurationImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.MaterialRequestNote;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
@Entity
public class WorkOrder extends ConcreteBusinessProcess {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1797388047932273120L;
	
	public static final String CREATED = "Created";
	public static final String APPROVED = "Approved";
	public static final String REJECTED = "Rejected";
	public static final String CANCELLED = "Cancelled";

	public static final Object COMPLETED = "Completed";
	
	@Index
	private String woTitle;
	@Index
	private int orderId;
	@Index 
	private Date orderDate;
	
	private Date completionDate;
	private String refNum;
	private Date refDate;
	private Date orderDeliveryDate;
	@Index
	private String woProject;
	@Index
	private String salesPerson;
	
	private String woDescription;
	private DocumentUpload upload;
	@Index
	private ArrayList<WorkOrderProductDetails> woTable;
	/**
	 * Date : 20-12-2018 BY ANIL
	 * Removed Index annotation ,because work order was not getting saved due serialized map used
	 * in SalesLineItem 
	 */
//	@Index
	private ArrayList<SalesLineItem> bomTable;
	
	private String userTrainingOne;
	private String userTrainingTwo;
	private boolean cablingByGenesis;
	private Date completionDateone;
	private String remark;
	
	public WorkOrder() {
		woTitle="";
		refNum="";
		woProject="";
		salesPerson="";
		woDescription="";
		woTable=new ArrayList<WorkOrderProductDetails>();
		bomTable=new ArrayList<SalesLineItem>();
		upload=new DocumentUpload();
	}



	public String getWoTitle() {
		return woTitle;
	}



	public void setWoTitle(String woTitle) {
		this.woTitle = woTitle;
	}



	public int getOrderId() {
		return orderId;
	}



	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}



	public Date getOrderDate() {
		return orderDate;
	}



	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}



	public String getRefNum() {
		return refNum;
	}



	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}



	public Date getRefDate() {
		return refDate;
	}



	public void setRefDate(Date refDate) {
		this.refDate = refDate;
	}

	public DocumentUpload getUpload() {
		return upload;
	}



	public void setUpload(DocumentUpload upload) {
		this.upload = upload;
	}


	public String getWoProject() {
		return woProject;
	}



	public void setWoProject(String woProject) {
		this.woProject = woProject;
	}


	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public String getWoDescription() {
		return woDescription;
	}

	public void setWoDescription(String woDescription) {
		this.woDescription = woDescription;
	}
	

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public List<WorkOrderProductDetails> getWoTable() {
		return woTable;
	}

	public void setWoTable(List<WorkOrderProductDetails> woTable) {
		ArrayList<WorkOrderProductDetails> arrBillItems=new ArrayList<WorkOrderProductDetails>();
		arrBillItems.addAll(woTable);
		this.woTable = arrBillItems;
	}
	
	
	public List<SalesLineItem> getBomTable() {
		return bomTable;
	}

	public void setBomTable(List<SalesLineItem> bomTable) {
		ArrayList<SalesLineItem> arrBillItems=new ArrayList<SalesLineItem>();
		arrBillItems.addAll(bomTable);
		this.bomTable = arrBillItems;
	}



	public Date getOrderDeliveryDate() {
		return orderDeliveryDate;
	}

	public void setOrderDeliveryDate(Date orderDeliveryDate) {
		this.orderDeliveryDate = orderDeliveryDate;
	}

	


public String getUserTrainingOne() {
		return userTrainingOne;
	}



	public void setUserTrainingOne(String userTrainingOne) {
		this.userTrainingOne = userTrainingOne;
	}



	public String getUserTrainingTwo() {
		return userTrainingTwo;
	}



	public void setUserTrainingTwo(String userTrainingTwo) {
		this.userTrainingTwo = userTrainingTwo;
	}



	public boolean isCablingByGenesis() {
		return cablingByGenesis;
	}



	public void setCablingByGenesis(boolean cablingByGenesis) {
		this.cablingByGenesis = cablingByGenesis;
	}



	public Date getCompletionDateone() {
		return completionDateone;
	}



	public void setCompletionDateone(Date completionDateone) {
		this.completionDateone = completionDateone;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



/*************************************Status List for Search***************************************/
	
	public static List<String> getStatusList() {
		ArrayList<String> statusListBox = new ArrayList<String>();
		statusListBox.add(CREATED);
		statusListBox.add(APPROVED);
		statusListBox.add(REJECTED);
		statusListBox.add(REQUESTED);
		statusListBox.add(CANCELLED);
		return statusListBox;
	}



	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		
		if(this.getCount()!=0){
			createMRN();
		}
		
		if(this.getCount()!=0){
			createClientAsset();
			this.setCompletionDateone(new Date());
		}
	}

	@GwtIncompatible
	private void createClientAsset() {
		// TODO Auto-generated method stub
		
		
		
		SalesOrder salesOrder = ofy().load().type(SalesOrder.class).filter("companyId",this.getCompanyId()).filter("count",this.getOrderId()).first().now();
		System.out.println("11111111");
		
		if(salesOrder!=null){
			
			if(this.woTable.size()!=0){

				for(int i=0;i<woTable.size();i++){
				
				final GenricServiceImpl genricimpl=new GenricServiceImpl();

				ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId",this.getCompanyId()).filter("count",woTable.get(i).getProductId()).first().now();

				
			ClientSideAsset clientasset = new ClientSideAsset();
			
			System.out.println("salesOrder="+salesOrder);
			
			System.out.println("In If Condition");
			PersonInfo pinfo = new PersonInfo();
			
			pinfo.setCount(salesOrder.getCinfo().getCount());
			pinfo.setFullName(salesOrder.getCinfo().getFullName());
			pinfo.setCellNumber(salesOrder.getCinfo().getCellNumber());
			pinfo.setPocName(salesOrder.getCinfo().getPocName());

			clientasset.setPersonInfo(pinfo);
			
			clientasset.setSalesOrderId(this.getOrderId());
			clientasset.setWorkOrderId(this.getCount());
			
			clientasset.setProductId(woTable.get(i).getCompProdId());
			clientasset.setMaterialId(woTable.get(i).getProductId());
			clientasset.setName(woTable.get(i).getProductName());
			

			clientasset.setBrand(itemproduct.getBrandName());
			clientasset.setModelNo(itemproduct.getModel());
			clientasset.setSrNo(itemproduct.getSerialNumber());
			
			clientasset.setDateOfInstallation(this.getCompletionDate());
			clientasset.setWarrenty(salesOrder.getItems().get(0).getWarrantyUntil());
			clientasset.setCompanyId(this.getCompanyId());
			
			System.out.println("Cate========="+ woTable.get(i).getProductCategory());

			Config config = ofy().load().type(Config.class).filter("companyId",this.getCompanyId()).filter("type",ConfigTypes.getConfigType(Screen.CLIENTSIDEASSETCATEGORY)).filter("name", this.woTable.get(i).getProductCategory().trim()).first().now();
			
			System.out.println("config=="+config);
			
			if(config!=null){
				clientasset.setCategory(woTable.get(i).getProductCategory());
				System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
			}else{
				DemoConfigurationImpl democonfig = new DemoConfigurationImpl();
				
				Config configuration = democonfig.getConfigData(ConfigTypes.getConfigType(Screen.CLIENTSIDEASSETCATEGORY), woTable.get(i).getProductCategory(), this.getCompanyId());
				System.out.println("Configuration=="+configuration);
				
				democonfig.saveConfig(configuration);
				clientasset.setCategory(woTable.get(i).getProductCategory());
				
			}
			
			System.out.println("Cate========="+ woTable.get(i).getProductCategory());
			
			genricimpl.save(clientasset);
			}
			}
		}
	}
	
	
	@GwtIncompatible
	private void createMRN() {
		final GenricServiceImpl genimpl=new GenricServiceImpl();
		
		MaterialRequestNote mrnEntity=new MaterialRequestNote();
		mrnEntity.setMrnWoId(this.getCount());
		mrnEntity.setMrnWoDate(this.getCreationDate());
		
		if(this.getOrderId()!=0){
			mrnEntity.setMrnSoId(this.getOrderId());
		}
		if(this.getOrderDate()!=null){
			mrnEntity.setMrnSoDate(this.getOrderDate());
		}
		if(this.getRefNum()!=null){
			mrnEntity.setMrnRefId(this.getRefNum());
		}
		if(this.getRefDate()!=null){
			mrnEntity.setMrnRefDate(this.getRefDate());
		}
		if(this.getOrderDeliveryDate()!=null){
			mrnEntity.setMrnDeliveryDate(this.getOrderDeliveryDate());
		}
		
		if(this.getWoProject()!=null){
			mrnEntity.setMrnProject(this.getWoProject());
		}
		if(this.getSalesPerson()!=null){
			mrnEntity.setMrnSalesPerson(this.getSalesPerson());
		}
		
		mrnEntity.setMrnTitle("MRN-"+this.getWoTitle());
		mrnEntity.setMrnDate(new Date());
		
		if(this.getEmployee()!=null){
			mrnEntity.setEmployee(this.getEmployee());
		}
		if(this.getApproverName()!=null){
			mrnEntity.setApproverName(this.getApproverName());
		}
		mrnEntity.setStatus(MaterialRequestNote.CREATED);
		mrnEntity.setBranch(this.getBranch());
		
		if(this.getWoTable().size()!=0){
			ArrayList<MaterialProduct> newprod = new ArrayList<MaterialProduct>();
			List<MaterialProduct> newprodlist=new ArrayList<MaterialProduct>();
			
			List<WorkOrderProductDetails> prodlist = this.getWoTable();
			
			
			List<Integer>prodIdlist=new ArrayList<Integer>();
			for(int i=0;i<prodlist.size();i++){
				prodIdlist.add(prodlist.get(i).getProductId());
			}
			
			
			System.out.println("Before Product Id List size ::: "+prodIdlist.size());
			
			List<Integer> mergelist=new ArrayList<Integer>(new HashSet<Integer>(prodIdlist));
			
			System.out.println("After Product Id List size ::: "+mergelist.size());
			
			for( int i=0;i<mergelist.size();i++){
				int counter=0;
				for(int j=0;j<prodlist.size();j++){
					if(mergelist.get(i)==prodlist.get(j).getProductId()){
						if(counter==0){
						MaterialProduct mp = new MaterialProduct();
						
						mp.setMaterialProductId(prodlist.get(j).getProductId());
						mp.setMaterialProductCode(prodlist.get(j).getProductCode());
						mp.setMaterialProductCategory(prodlist.get(j).getProductCategory());
						mp.setMaterialProductName(prodlist.get(j).getProductName());
						mp.setMaterialProductUOM(prodlist.get(j).getProductUOM());
						mp.setMaterialProductRequiredQuantity(prodlist.get(j).getProductQty());
						mp.setMaterialProductWarehouse("");
						mp.setMaterialProductStorageLocation("");
						mp.setMaterialProductStorageBin("");
						
						newprodlist.add(mp);
						}
						counter++;
					}
				}
			}
			System.out.println("Before MRN Product Id List size ::: "+newprodlist.size());
			
			for( WorkOrderProductDetails temp :prodlist){
				MaterialProduct mp = new MaterialProduct();
				
				mp.setMaterialProductId(temp.getProductId());
				mp.setMaterialProductCode(temp.getProductCode());
				mp.setMaterialProductCategory(temp.getProductCategory());
				mp.setMaterialProductName(temp.getProductName());
				mp.setMaterialProductUOM(temp.getProductUOM());
				mp.setMaterialProductRequiredQuantity(temp.getProductQty());
				mp.setMaterialProductWarehouse("");
				mp.setMaterialProductStorageLocation("");
				mp.setMaterialProductStorageBin("");
				
				newprod.add(mp);
			}
			
			
			
			
			
			for(int i=0;i<newprodlist.size();i++){
				double qty=0;
				for(int j=0;j<newprod.size();j++){
					if(newprodlist.get(i).getMaterialProductId()==newprod.get(j).getMaterialProductId()){
						
						qty+=newprod.get(j).getMaterialProductRequiredQuantity();
						newprodlist.get(i).setMaterialProductRequiredQuantity(qty);;
						
					}
				}
			}
			
			
			
			
			mrnEntity.setSubProductTableMrn(newprodlist);
		}
		if(this.getBomTable()!=null){
			mrnEntity.setProductTableMrn(this.getBomTable());
		}
		mrnEntity.setCompanyId(this.getCompanyId());
		genimpl.save(mrnEntity);
	}

	
	
	@GwtIncompatible
	  @OnSave
	  public void OnSave()
	  {
			if(creationDate!=null){
				setCreationDate(DateUtility.getDateWithTimeZone("IST",new Date()));
			}
	  }
				
				
	
	
	

}
