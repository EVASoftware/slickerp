package com.slicktechnologies.shared.common.servicerelated;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class AssetMovementInfo extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2365694202767150894L;

	@Index
	String warehouseName;
	@Index
	String location;
	@Index
	Date fromDate;
	@Index
	Date toDate;
	
	public AssetMovementInfo() {
		warehouseName="";
		location="";
	}
	
	

	public String getWarehouseName() {
		return warehouseName;
	}



	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public Date getFromDate() {
		return fromDate;
	}



	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}



	public Date getToDate() {
		return toDate;
	}



	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
