package com.slicktechnologies.shared.common.servicerelated;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.articletype.ArticleTypeTravel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

// TODO: Auto-generated Javadoc
/**
 * Entity for Saving Company Asset.
 */
@Entity
@Embed
public class CompanyAsset extends SuperModel {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8368053713177774644L;

	/** The brand. */
	@Index
	protected String brand;

	/** The category. */
	@Index
	protected String category;

	/** The name. */
	@Index
	protected String name;
	@Index
	/** The model no. */
	protected String modelNo;
	@Index
	/** The sr no. */
	protected String srNo;

	/** The date of manufacture. */
	protected Date dateOfManufacture;

	/** The date of installation. */
	protected Date dateOfInstallation;

	/** The document. */

	protected DocumentUpload document;

	/** The purchased from. */
	protected String purchasedFrom;

	/** The price. */
	protected double price;

	/** The warrenty. */
	protected Date warrenty;

	/** The PO date. */
	protected Date PODate;

	/** The Po no. */
	protected String PoNo;

	@Index
	protected ArrayList<ArticleTypeTravel> articleTypeDetails;
	@Index
	protected Boolean status;

	/**
	 * its stores the ref num of asset. Date : 06-10-2016 By Anil Release : 30
	 * Sept 2016 Project : PURCHASE MODIFICATION(NBHC)
	 */

	@Index
	protected String referenceNumber;

	/**
	 * End
	 */

	/**
	 * Following New fields are added for maintaining asset location ,retire
	 * date and movement list Date : 20-10-2016 By Anil Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	@Index
	protected String warehouseName;
	@Index
	protected String storageLocation;
	@Index
	protected String storageBin;
	@Index
	protected Date retireDate;
	protected String description;
	@Index
	protected ArrayList<AssetMovementInfo> assetMovementList;

	@Index
	protected int productId;
	@Index
	protected int grnId;

	/**
	 * End
	 */

	/**
	 * rohan added this brnach for NBHC for restiction of branches
	 */
	@Index
	protected String branch;

	/**
	 * ends here
	 */
	/** date 3/1/2018 added bby komal for hvac asset status **/
	@Index
	protected String amcStatus;
	protected boolean recordSelect;
	/**
	 * ends here
	 */

	/**
	 * Instantiates a new company asset.
	 */
	public CompanyAsset() {
		brand = "";
		category = "";
		name = "";
		modelNo = "";
		srNo = "";
		document = new DocumentUpload();
		purchasedFrom = "";
		PoNo = "";

//		articleTypeDetails = new ArrayList<ArticleTypeTravel>();
		status = true;
		referenceNumber = "";

		warehouseName = "";
		storageLocation = "";
		storageBin = "";
		description = "";
//		assetMovementList = new ArrayList<AssetMovementInfo>();
	}

	public int getGrnId() {
		return grnId;
	}

	public void setGrnId(int grnId) {
		this.grnId = grnId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}

	public Date getRetireDate() {
		return retireDate;
	}

	public void setRetireDate(Date retireDate) {
		this.retireDate = retireDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<AssetMovementInfo> getAssetMovementList() {
		return assetMovementList;
	}

	public void setAssetMovementList(List<AssetMovementInfo> assetMovementList) {
		if(assetMovementList!=null) {
			ArrayList<AssetMovementInfo> list = new ArrayList<AssetMovementInfo>();
			list.addAll(assetMovementList);
			this.assetMovementList = list;
		}
		
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * Instantiates a new company asset.
	 *
	 * @param brand
	 *            the brand
	 * @param category
	 *            the category
	 * @param name
	 *            the name
	 * @param modelNo
	 *            the model no
	 * @param srNo
	 *            the sr no
	 * @param dateOfManufacture
	 *            the date of manufacture
	 * @param dateOfInstallation
	 *            the date of installation
	 * @param document
	 *            the document
	 * @param purchasedFrom
	 *            the purchased from
	 * @param price
	 *            the price
	 * @param warrenty
	 *            the warrenty
	 * @param PODate
	 *            the PO date
	 * @param PONo
	 *            the PO no
	 */
	public CompanyAsset(String brand, String category, String name,
			String modelNo, String srNo, Date dateOfManufacture,
			Date dateOfInstallation, DocumentUpload document,
			String purchasedFrom, double price, Date warrenty, Date PODate,
			String PONo) {
		super();

		this.brand = brand;
		this.category = category;
		this.name = name;
		this.modelNo = modelNo;
		this.srNo = srNo;
		this.dateOfManufacture = dateOfManufacture;
		this.dateOfInstallation = dateOfInstallation;
		this.document = document;
		this.purchasedFrom = purchasedFrom;
		this.price = price;
		this.warrenty = warrenty;
		this.PODate = PODate;
		this.PoNo = PONo;
	}

	// @OnSave
	// @GwtIncompatible
	// private void createProduct()
	// {
	// String ipModelNo="";
	// if(this.getModelNo()!=null){
	// ipModelNo=this.getModelNo();
	// }
	//
	// String ipBrand="";
	// if(this.getBrand()!=null){
	// brand=this.getBrand();
	// }
	//
	// String ipSrNo="";
	// if(this.getSrNo()!=null){
	// ipSrNo=this.getSrNo();
	// }
	// String refNum="";
	// if(this.getReferenceNumber()!=null){
	// refNum=this.getReferenceNumber();
	// }
	// if(this.getId()==null&&this.getName()!=null)
	// {
	// createProductfromCompanyasset(this.getName(), this.getCategory(),ipBrand,
	// ipModelNo, ipSrNo,getCompanyId(),this.getCount(),refNum);
	// }
	// }

	@GwtIncompatible
	private void createProductfromCompanyasset(String name,
			String assetcategory, String brand, String modelno, String srno,
			Long companyid, int prodcode, String refNum) {
		ItemProduct ip = new ItemProduct();

		ip.setProductName(name);
		// ip.setProductCategory(assetcategory);
		ip.setBrandName(brand);
		ip.setModel(modelno);
		ip.setSerialNumber(srno);
		ip.setCompanyId(companyId);
		ip.setProductCode(String.valueOf("A" + prodcode));
		// ip.setSpecifications("sr no"+srno1);
		ip.setProductCategory(AppConstants.ASSETPRODUCTCATEGORY);
		ip.setUnitOfMeasurement("Nos");
		ip.setRefNumber1(refNum);

		GenricServiceImpl gsi = new GenricServiceImpl();
		if (ip != null) {
			gsi.save(ip);
		}
	}

	/**
	 * Gets the warrenty.
	 *
	 * @return the warrenty
	 */
	public Date getWarrenty() {
		return warrenty;
	}

	/**
	 * Gets the PO date.
	 *
	 * @return the PO date
	 */
	public Date getPODate() {
		return PODate;
	}

	/**
	 * Sets the PO date.
	 *
	 * @param pODate
	 *            the new PO date
	 */
	public void setPODate(Date pODate) {
		PODate = pODate;
	}

	/**
	 * Gets the po no.
	 *
	 * @return the po no
	 */
	public String getPoNo() {
		return PoNo;
	}

	/**
	 * Sets the po no.
	 *
	 * @param poNo
	 *            the new po no
	 */
	public void setPoNo(String poNo) {
		if (poNo != null)
			PoNo = poNo.trim();
	}

	/**
	 * Gets the brand.
	 *
	 * @return the brand
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "11", title = "Brand", variableName = "tbBrand")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Brand")
	public String getBrand() {
		return brand;
	}

	/**
	 * Sets the brand.
	 *
	 * @param brand
	 *            the new brand
	 */
	public void setBrand(String brand) {
		if (brand != null)
			this.brand = brand.trim();
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "10", title = "Category", variableName = "olbAssetCategory")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Category")
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category
	 *            the new category
	 */
	public void setCategory(String category) {
		if (this.category != null)
			this.category = category.trim();
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "01", title = "Asset Name", variableName = "assetName")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Asset Name")
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		if (name != null)
			this.name = name.trim();
	}

	/**
	 * Gets the model no.
	 *
	 * @return the model no
	 */
	public String getModelNo() {
		return modelNo;
	}

	/**
	 * Sets the model no.
	 *
	 * @param modelNo
	 *            the new model no
	 */
	public void setModelNo(String modelNo) {
		if (modelNo != null)
			this.modelNo = modelNo.trim();
	}

	/**
	 * Gets the sr no.
	 *
	 * @return the sr no
	 */
	public String getSrNo() {
		return srNo;
	}

	/**
	 * Sets the sr no.
	 *
	 * @param srNo
	 *            the new sr no
	 */
	public void setSrNo(String srNo) {
		if (srNo != null) {
			this.srNo = srNo.trim();
			if (srNo.equals(""))
				srNo = "N.A";
		}
	}

	/**
	 * Gets the date of manufacture.
	 *
	 * @return the date of manufacture
	 */
	public Date getDateOfManufacture() {
		return dateOfManufacture;
	}

	/**
	 * Sets the date of manufacture.
	 *
	 * @param dateOfManufacture
	 *            the new date of manufacture
	 */
	public void setDateOfManufacture(Date dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}

	/**
	 * Gets the date of installation.
	 *
	 * @return the date of installation
	 */
	public Date getDateOfInstallation() {
		return dateOfInstallation;
	}

	/**
	 * Sets the date of installation.
	 *
	 * @param dateOfInstallation
	 *            the new date of installation
	 */
	public void setDateOfInstallation(Date dateOfInstallation) {
		this.dateOfInstallation = dateOfInstallation;
	}
	
	
	

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * Gets the document.
	 *
	 * @return the document
	 */
	public DocumentUpload getDocument() {
		return document;
	}

	/**
	 * Sets the document.
	 *
	 * @param document
	 *            the new document
	 */
	public void setDocument(DocumentUpload document) {
		this.document = document;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price
	 *            the new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Checks if is warrenty.
	 *
	 * @return the date
	 */
	public Date isWarrenty() {
		return warrenty;
	}

	/**
	 * Sets the warrenty.
	 *
	 * @param warrenty
	 *            the new warrenty
	 */
	public void setWarrenty(Date warrenty) {
		this.warrenty = warrenty;
	}

	/**
	 * Gets the purchased from.
	 *
	 * @return the purchased from
	 */
	public String getPurchasedFrom() {
		return purchasedFrom;
	}

	/**
	 * Sets the purchased from.
	 *
	 * @param purchasedFrom
	 *            the new purchased from
	 */
	public void setPurchasedFrom(String purchasedFrom) {
		if (purchasedFrom != null)
			this.purchasedFrom = purchasedFrom.trim();
	}

	public ArrayList<ArticleTypeTravel> getArticleTypeDetails() {
		return articleTypeDetails;
	}

	public void setArticleTypeDetails(List<ArticleTypeTravel> articleTypeDetails) {
		if(articleTypeDetails!=null) {
			ArrayList<ArticleTypeTravel> arrAricle = new ArrayList<ArticleTypeTravel>();
			arrAricle.addAll(articleTypeDetails);
			this.articleTypeDetails = arrAricle;
		}
		
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {

		boolean res1 = isDuplicateName(m);
		boolean res2 = isDuplicateSrNo(m);
		return res1 && res2;

	}

	/**
	 * Checks if is duplicate name.
	 *
	 * @param model
	 *            the model
	 * @return true, if is duplicate name
	 */
	public boolean isDuplicateName(SuperModel model) {
		CompanyAsset per = (CompanyAsset) model;
		String name = per.getName();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		String curname = this.name.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		// New Object is being added
		if (per.id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (per.id.equals(id))
				return false;
			else if (name.equals(curname))
				return true;
			else
				return false;
		}
	}

	/**
	 * Checks if is duplicate sr no.
	 *
	 * @param model
	 *            the model
	 * @return true, if is duplicate sr no
	 */
	public boolean isDuplicateSrNo(SuperModel model) {

		CompanyAsset per = (CompanyAsset) model;
		String name = per.getSrNo();
		// System.out.println("VALUE OF PRODUCT "+per);
		// System.out.println("VALUE OF PRODUCT NAME "+per.prodName);
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		String curname = this.srNo.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		// New Object is being added
		if (per.id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (per.id.equals(id))
				return false;
			else if (name.equals(curname))
				return true;
			else
				return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CompanyAsset) {
			CompanyAsset newObject = (CompanyAsset) obj;
			if (newObject.id != null && this.id != null) {
				if (newObject.id.equals(this.id))
					return true;
				else
					return false;
			}
		}
		return false;
	}

	public String getAmcStatus() {
		return amcStatus;
	}

	public void setAmcStatus(String amcStatus) {
		this.amcStatus = amcStatus;
	}

	public boolean isRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	
}
