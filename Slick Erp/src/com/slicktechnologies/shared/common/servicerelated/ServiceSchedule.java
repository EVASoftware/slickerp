package com.slicktechnologies.shared.common.servicerelated;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

@Entity
@Embed
public class ServiceSchedule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7186007906415971355L;

	/****************************************
	 * Applicability Attributes
	 ***************************************/

	protected Date scheduleStartDate;
	protected int scheduleProdId;
	protected String scheduleProdName;
	protected int scheduleDuration;
	protected int scheduleNoOfServices;
	protected int scheduleServiceNo;
	protected Date scheduleServiceDate;
	protected Date scheduleProdStartDate;
	protected Date scheduleProdEndDate;
	protected String scheduleServiceTime;
	protected int scheduleProQty;
	protected String scheduleProBranch;
	protected String scheduleServiceDay;

	protected String servicingBranch;
	protected Double totalServicingTime;

	protected String premisesDetails;

	protected int serSrNo;
	protected boolean checkService;

	/*
	 * Date :- 07 feb 2017 added by vijay for contract level remark will display
	 * in service
	 */
	protected String serviceRemark;

	/**
	 * Date 29-03-2017 added by vijay for week
	 */
	protected int weekNo;
	/**
	 * Ends here
	 */

	/**
	 * Date : 26-08-2017 BY ANIL Added field document type and document id for
	 * storing document count when storing scheduling list to separate to
	 * contract entity.
	 */
	@Index
	protected String documentType;
	@Index
	protected int documentId;
	@Index
	protected long companyId;

	@Id
	protected Long id;
	/**
	 * End
	 */
	/**
	 * nidhi 5-10-2018 *:*:*
	 */

	@EmbedMap
	@Serialize
	HashMap<Integer, ArrayList<ProductGroupList>> serviceProductList;
	

	/***
	 * @author Vijay Date 31-07-2019 Des :- NBHC CCPM Stack Details Maintaining
	 *         for Fumigation services
	 */
	@Index
	String warehouseName;
	@Index
	String warehouseCode;
	@Index
	String shade;
	@Index
	String compartment;
	@Index
	String stackNo;
	double stackQty;
	String commodityName;
	String clusterName;
	
	/**
	 * @author Anil
	 * @since 06-06-2020
	 * adding fields for asset, tier and component details
	 */
	int assetId;
	String tierName;
	String tat;
	String componentName;
	String mfgNum;
	Date mfgDate;
	Date replacementDate;	
	
	/**
	 * @author Anil
	 * @since 08-06-2020
	 * for generating asset id
	 */
	int srNo;
	
	/**
	 * @author Anil
	 * @since 16-06-2020
	 * asset unit
	 */
	String assetUnit;
	
	/**
	 * @author Vijay Chougule Date :- 09-09-2020
	 * Des :- Added Service Duration for Life Line
	 */
	double serviceDuration;

	/**
	 * please update copyofobject method when ever new variable declared in this
	 * class must do this nidhi 19-09-2018
	 */
	public ServiceSchedule() {
		documentType = "";
		warehouseCode="";
		shade="";
		compartment="";
		stackNo="";
		commodityName="";
		clusterName="";
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	/**************************************
	 * Getters And Setters
	 **************************************/

	public String getPremisesDetails() {
		return premisesDetails;
	}

	public void setPremisesDetails(String premisesDetails) {
		this.premisesDetails = premisesDetails;
	}

	public Date getScheduleStartDate() {
		return scheduleStartDate;
	}

	public void setScheduleStartDate(Date scheduleStartDate) {
		if (scheduleStartDate != null) {
			this.scheduleStartDate = scheduleStartDate;
		}
	}

	public int getScheduleProdId() {
		return scheduleProdId;
	}

	public void setScheduleProdId(int scheduleProdId) {
		this.scheduleProdId = scheduleProdId;
	}

	public String getScheduleProdName() {
		return scheduleProdName;
	}

	public void setScheduleProdName(String scheduleProdName) {
		if (scheduleProdName != null) {
			this.scheduleProdName = scheduleProdName.trim();
		}
	}

	public int getScheduleDuration() {
		return scheduleDuration;
	}

	public void setScheduleDuration(int scheduleDuration) {
		this.scheduleDuration = scheduleDuration;
	}

	public int getScheduleNoOfServices() {
		return scheduleNoOfServices;
	}

	public void setScheduleNoOfServices(int scheduleNoOfServices) {
		this.scheduleNoOfServices = scheduleNoOfServices;
	}

	public int getScheduleServiceNo() {
		return scheduleServiceNo;
	}

	public void setScheduleServiceNo(int scheduleSerViceNo) {
		this.scheduleServiceNo = scheduleSerViceNo;
	}

	public Date getScheduleServiceDate() {
		return scheduleServiceDate;
	}

	public void setScheduleServiceDate(Date scheduleServiceDate) {
		if (scheduleServiceDate != null) {
			this.scheduleServiceDate = scheduleServiceDate;
		}
	}

	public Date getScheduleProdStartDate() {
		return scheduleProdStartDate;
	}

	public void setScheduleProdStartDate(Date scheduleProdStartDate) {
		if (scheduleProdStartDate != null) {
			this.scheduleProdStartDate = scheduleProdStartDate;
		}
	}

	public Date getScheduleProdEndDate() {
		return scheduleProdEndDate;
	}

	public void setScheduleProdEndDate(Date scheduleProdEndDate) {
		if (scheduleProdEndDate != null) {
			this.scheduleProdEndDate = scheduleProdEndDate;
		}
	}

	public String getScheduleServiceTime() {
		return scheduleServiceTime;
	}

	public void setScheduleServiceTime(String scheduleServiceTime) {
		if (scheduleServiceTime != null) {
			this.scheduleServiceTime = scheduleServiceTime.trim();
		}
	}

	public int getScheduleProQty() {
		return scheduleProQty;
	}

	public void setScheduleProQty(int scheduleProQty) {
		this.scheduleProQty = scheduleProQty;
	}

	public String getScheduleProBranch() {
		return scheduleProBranch;
	}

	public void setScheduleProBranch(String scheduleProBranch) {
		if (scheduleProBranch != null) {
			this.scheduleProBranch = scheduleProBranch.trim();
		}
	}

	public String getScheduleServiceDay() {
		return scheduleServiceDay;
	}

	public void setScheduleServiceDay(String scheduleServiceDay) {
		if (scheduleServiceDay != null) {
			this.scheduleServiceDay = scheduleServiceDay;
		}
	}

	public String getServicingBranch() {
		return servicingBranch;
	}

	public void setServicingBranch(String servicingBranch) {
		this.servicingBranch = servicingBranch;
	}

	public Double getTotalServicingTime() {
		return totalServicingTime;
	}

	public void setTotalServicingTime(Double totalServicingTime) {
		this.totalServicingTime = totalServicingTime;
	}

	public int getSerSrNo() {
		return serSrNo;
	}

	public void setSerSrNo(int serSrNo) {
		this.serSrNo = serSrNo;
	}

	public boolean isCheckService() {
		return checkService;
	}

	public void setCheckService(boolean checkService) {
		this.checkService = checkService;
	}

	public String getServiceRemark() {
		return serviceRemark;
	}

	public void setServiceRemark(String serviceRemark) {
		this.serviceRemark = serviceRemark;
	}

	public int getWeekNo() {
		return weekNo;
	}

	public void setWeekNo(int weekNo) {
		this.weekNo = weekNo;
	}

	public HashMap<Integer, ArrayList<ProductGroupList>> getServiceProductList() {
		return serviceProductList;
	}

	public void setServiceProductList(HashMap<Integer, ArrayList<ProductGroupList>> serviceProductList) {
		this.serviceProductList = serviceProductList;
	}

	public ServiceSchedule copyOfObject() {
		ServiceSchedule serDt = new ServiceSchedule();

		serDt.setScheduleStartDate(this.getScheduleStartDate());

		serDt.setScheduleProdId(this.getScheduleProdId());
		serDt.setScheduleProdName(this.getScheduleProdName());

		serDt.setScheduleDuration(this.getScheduleDuration());
		serDt.setScheduleNoOfServices(this.getScheduleNoOfServices());
		serDt.setScheduleServiceNo(this.getScheduleServiceNo());
		serDt.setScheduleServiceDate(this.getScheduleServiceDate());
		serDt.setScheduleProdStartDate(this.getScheduleProdStartDate());
		serDt.setScheduleProdEndDate(this.getScheduleProdEndDate());
		serDt.setScheduleServiceTime(this.getScheduleServiceTime());
		serDt.setScheduleProQty(this.getScheduleProQty());
		serDt.setScheduleProBranch(this.getScheduleProBranch());
		serDt.setScheduleServiceDay(this.getScheduleServiceDay());

		serDt.setServicingBranch(this.getServicingBranch());
		serDt.setTotalServicingTime(this.getTotalServicingTime());

		serDt.setPremisesDetails(this.getPremisesDetails());

		serDt.setSerSrNo(this.getSerSrNo());
		serDt.setCheckService(this.isCheckService());

		serDt.setServiceRemark(this.getServiceRemark());

		serDt.setWeekNo(this.getWeekNo());
		serDt.setDocumentType(this.getDocumentType());
		serDt.setCompanyId(this.getCompanyId());
		serDt.setServiceProductList(this.getServiceProductList());
		/** Date 05-08-2019 by Vijay *****/
		if(this.getWarehouseCode()!=null){
		serDt.setWarehouseCode(this.getWarehouseCode());
		}
		if(serDt.getWarehouseName()!=null){
			serDt.setWarehouseName(serDt.getWarehouseName());
		}
		if(this.getShade()!=null){
			serDt.setShade(this.getShade());
		}
		if(this.getCompartment()!=null){
			serDt.setCompartment(this.getCompartment());
		}
		if(this.getStackNo()!=null){
			serDt.setStackNo(this.getStackNo());
		}
		/*** Ends here *******/
		
		/**
		 * @author Anil
		 * @since 06-06-2020
		 */
		serDt.setAssetId(this.getAssetId());
		serDt.setTierName(this.getTierName());
		serDt.setTat(this.getTat());
		serDt.setComponentName(this.getComponentName());
		serDt.setMfgNum(this.getMfgNum());
		serDt.setMfgDate(this.getMfgDate());
		serDt.setReplacementDate(this.getReplacementDate());
		serDt.setSrNo(this.getSrNo());
		serDt.setAssetUnit(this.getAssetUnit());
		
		return serDt;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getShade() {
		return shade;
	}

	public void setShade(String shade) {
		this.shade = shade;
	}

	public String getCompartment() {
		return compartment;
	}

	public void setCompartment(String compartment) {
		this.compartment = compartment;
	}

	public String getStackNo() {
		return stackNo;
	}

	public void setStackNo(String stackNo) {
		this.stackNo = stackNo;
	}

	public double getStackQty() {
		return stackQty;
	}

	public void setStackQty(double stackQty) {
		this.stackQty = stackQty;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public int getAssetId() {
		return assetId;
	}

	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}

	public String getTierName() {
		return tierName;
	}

	public void setTierName(String tierName) {
		this.tierName = tierName;
	}

	public String getTat() {
		return tat;
	}

	public void setTat(String tat) {
		this.tat = tat;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getMfgNum() {
		return mfgNum;
	}

	public void setMfgNum(String mfgNum) {
		this.mfgNum = mfgNum;
	}

	public Date getMfgDate() {
		return mfgDate;
	}

	public void setMfgDate(Date mfgDate) {
		this.mfgDate = mfgDate;
	}

	public Date getReplacementDate() {
		return replacementDate;
	}

	public void setReplacementDate(Date replacementDate) {
		this.replacementDate = replacementDate;
	}

	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}

	public String getAssetUnit() {
		return assetUnit;
	}

	public void setAssetUnit(String assetUnit) {
		this.assetUnit = assetUnit;
	}

	public double getServiceDuration() {
		return serviceDuration;
	}

	public void setServiceDuration(double serviceDuration) {
		this.serviceDuration = serviceDuration;
	}
	
	
	
}
