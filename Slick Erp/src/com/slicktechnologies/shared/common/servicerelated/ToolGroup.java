
package com.slicktechnologies.shared.common.servicerelated;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.googlecode.objectify.annotation.Unindex;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.paymentlayer.Payment;

// TODO: Auto-generated Javadoc
/**
 * Repersents a Set of Tool which are going to be used for Service Purpose.
 */
@Entity
@Index
public class ToolGroup extends SuperModel
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7741099205260771385L;
	
	/** Name of Group/Set*/
	@Index
	protected String name;
	
	/** Set Code */
	@Index
	protected String code;
	
	/** Category Name of Set/Group */
	protected String categoryName;
	
	/** The status. */
	@Index
	protected boolean status;
	
	/** Tools in this Set. */
	@Unindex
	@Serialize
	protected ArrayList<CompanyAsset> arraylistTool;
	
	
	/**
	 * Instantiates a new tool group.
	 */
	public ToolGroup()
	{
		super();
		arraylistTool=new ArrayList<CompanyAsset>();
	}
	
	
	/**
	 * Instantiates a new tool group.
	 *
	 * @param name the name
	 * @param categoryName the category name
	 * @param status the status
	 * @param code the code
	 */
	public ToolGroup(String name, String categoryName,boolean status,String code) {
		super();
		this.name = name;
		this.categoryName = categoryName;
		this.status=status;
		this.code=code;
	}

	
	
	/**
	 * Gets the arraylist tool.
	 *
	 * @return the arraylist tool
	 */
	public ArrayList<CompanyAsset> getArraylistTool() {
		return arraylistTool;
	}


	/**
	 * Sets the arraylist tool.
	 *
	 * @param arraylistTool the new arraylist tool
	 */
	public void setArraylistTool(List<CompanyAsset> arraylistTool)
	{
		if(arraylistTool!=null)
		{
			this.arraylistTool=new ArrayList<CompanyAsset>();
			this.arraylistTool.addAll(arraylistTool);
			
		}
		
	}


	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "11", title = "Code", variableName = "tbcode")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Code")
	public String getCode() {
		return code;
	}


	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "01", title = "ToolSet Name", variableName = "groupName")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "ToolSet Name")
	public String getName() {
		return name;
	}


	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	

	/**
	 * Gets the category name.
	 *
	 * @return the category name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "10", title = "Category Name", variableName = "olbTool")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Category Name")
	public String getCategoryName() {
		return categoryName;
	}


	/**
	 * Sets the category name.
	 *
	 * @param category the new category name
	 */
	public void setCategoryName(String category) {
		this.categoryName = category;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel model) {
		ToolGroup per=(ToolGroup) model;
		String name = per.name;
		//System.out.println("VALUE OF PRODUCT "+per);
		//System.out.println("VALUE OF PRODUCT NAME "+per.prodName);
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();

		String curname=this.name;
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(per.id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else
		{
			if(per.id.equals(id))
				return false;
			else if (name.equals(curname))
				return true;
			else
				return false;
		}
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}


	
	
	

}
