package com.slicktechnologies.shared.common.servicerelated;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.server.customerSaveTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.GeneralKeyValueBean;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.inventory.BillProductDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Repersents a Location with assets which are going to be served.
 */
@Entity
public class ServiceProject extends SuperModel implements Serializable
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1054448051514926019L;
	
	/** The Constant SCHEDULED. */
	public static final String SCHEDULED = "Scheduled";
	
	public static final String CANCELLED = "Cancelled";
	
	public static final String CREATED = "Created";
	
	/** The Constant COMPLETED. */
	public static final String COMPLETED = "Completed";
	//protected int id;
	/** The contract id. */
	@Index
	protected int contractId;
	
	/** The service id. */
	@Index
	protected int serviceId;
	
	@Index
	protected int projectIndex;
	
	/** The point of contact. */
	@Index
	protected Person pointOfContact;
	
	/** The lattitude. */
	protected double lattitude;
	
	/** The longitude. */
	protected double longitude;
	
	/** The comment. */
	protected String comment;
	
	/** The service engineer. */
	@Index
	public String serviceEngineer;
	
	/** The service status. */
	@Index
	public String serviceStatus;
	
	/** The contract end date. */
	protected Date contractStartDate,contractEndDate;
	
	/** The service date. */
	@Index
	protected Date serviceDate;
	
	/** The tool group. */
	@Serialize
	protected String toolGroup;

	/** The tool. */
	@Index
	protected String tool;
	
	/** The project name. */
	@Index
	protected String projectName;
	
	/** The addr. */
	protected Address addr;
	
	/** The emp. */
	protected String emp;
	
	/** The person info. */
	@Index
	protected PersonInfo personInfo;
	
	/** The status. */
	protected boolean status;
	
	/** The technicians. */
	protected ArrayList<EmployeeInfo> technicians;
	
	/** The employee info key. */
	protected ArrayList<Key<EmployeeInfo>>employeeInfoKey;
	
	/** The tooltable. */
	protected ArrayList<CompanyAsset> tooltable;
	
	/** The clientside assett. */
	protected ArrayList<ServicedClientSideAsset> clientsideAssett;
	
	/** The project status. */
	@Index
	protected String projectStatus;
	
	protected ArrayList<BillProductDetails> billMaterialLis;
	
	@Index
	protected int serviceSrNo;
	
	/** The list for adding item products in project */
	protected ArrayList<ProductGroupList> prodDetailsList;
	
	/**
	 * Instantiates a new service project.
	 */
	protected ArrayList<GeneralKeyValueBean> technicianValueList;
	
	/**
	 * @author Anil , Date : 22-11-2019
	 * customeNameChange flag
	 * flag value set to to true only when customer change name service called
	 * we are doing this to avoid concurrent exception at the time of updating customer name
	 */
	boolean custNameChangeFlag=false;
	
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@Index
	Date lastUpdatedDate;
	
	
	
	public ServiceProject()
	{
		technicians=new ArrayList<EmployeeInfo>();
		employeeInfoKey=new ArrayList<Key<EmployeeInfo>>();
		tooltable=new ArrayList<CompanyAsset>();
		pointOfContact=new Person();
		Contact cont=new Contact();
		pointOfContact.getContacts().add(cont);
		serviceStatus="";
		projectStatus="";
		clientsideAssett=new ArrayList<ServicedClientSideAsset>();
		comment="";
		serviceEngineer="";
		toolGroup="";
		tool="";
		projectName="";
		addr=new Address();
		emp="";
		personInfo=new PersonInfo();
		billMaterialLis=new ArrayList<BillProductDetails>();
		prodDetailsList=new ArrayList<ProductGroupList>();
		technicianValueList = new ArrayList<GeneralKeyValueBean> ();
		
		custNameChangeFlag=false;
	}


	/**
	 * Instantiates a new service project.
	 *
	 * @param contractId the contract id
	 * @param person the person
	 * @param lattitude the lattitude
	 * @param longitude the longitude
	 * @param comment the comment
	 * @param toolGroup the tool group
	 * @param clientSideGroup the client side group
	 * @param projectName the project name
	 * @param addr the addr
	 * @param tool the tool
	 * @param emp the emp
	 * @param status the status
	 */
	public ServiceProject(int contractId, Person person, double lattitude,
			double longitude, String comment, String toolGroup,
			String clientSideGroup,String projectName,Address addr,String tool,String emp,boolean status) {
		super();
		this.contractId = contractId;
		this.pointOfContact = person;
		this.lattitude = lattitude;
		this.longitude = longitude;
		this.comment = comment;
		this.toolGroup = toolGroup;
		
		this.projectName=projectName;
		this.addr=addr;
		this.tool=tool;
		this.emp=emp;
		this.status=status;
		custNameChangeFlag=false;
	}
	
	
	
	public List<EmployeeInfo> getTechnicians() {
		return technicians;
	}


	public void setTechnicians(List<EmployeeInfo> technicians) {
		ArrayList<EmployeeInfo> arrEmployee=new ArrayList<EmployeeInfo>();
		arrEmployee.addAll(technicians);
		this.technicians = arrEmployee;
	}


	/**
	 * Gets the tooltable.
	 *
	 * @return the tooltable
	 */
	public ArrayList<CompanyAsset> getTooltable() {
		return tooltable;
	}


	/**
	 * Sets the tooltable.
	 *
	 * @param tooltable the new tooltable
	 */
	public void setTooltable(List<CompanyAsset> tooltable) {
		if(tooltable!=null)
		{
			this.tooltable=new ArrayList<CompanyAsset>();
		this.tooltable.addAll(tooltable);
		}
	}


	/**
	 * Gets the clientside assettable.
	 *
	 * @return the clientside assettable
	 */
	public ArrayList<ServicedClientSideAsset> getClientsideAssettable() {
		return clientsideAssett;
	}


	/**
	 * Sets the clientside assettable.
	 *
	 * @param clientsideAssettable the new clientside assettable
	 */
	public void setClientsideAssettable(
			List<ServicedClientSideAsset> clientsideAssettable) {
		if(clientsideAssettable!=null)
		{
			this.clientsideAssett=new ArrayList<ServicedClientSideAsset>();
			this.clientsideAssett.addAll(clientsideAssettable);
			
		}
		
	}


	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}


	/**
	 * Gets the contract start date.
	 *
	 * @return the contract start date
	 */
	public Date getContractStartDate() {
		return contractStartDate;
	}


	/**
	 * Sets the contract start date.
	 *
	 * @param contractStartDate the new contract start date
	 */
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}


	/**
	 * Gets the contract end date.
	 *
	 * @return the contract end date
	 */
	public Date getContractEndDate() {
		return contractEndDate;
	}


	/**
	 * Sets the contract end date.
	 *
	 * @param contractEndDate the new contract end date
	 */
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}


	/**
	 * Gets the person info.
	 *
	 * @return the person info
	 */
	public PersonInfo getPersonInfo() {
		return personInfo;
	}


	/**
	 * Sets the person info.
	 *
	 * @param personInfo the new person info
	 */
	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}

	
	/**
	 * Gets the emp.
	 *
	 * @return the emp
	 */
	public String getEmp() {
		return emp;
	}


	/**
	 * Sets the emp.
	 *
	 * @param emp the new emp
	 */
	public void setEmp(String emp) {
		if(emp!=null)
			this.emp = emp.trim();
	}


	/**
	 * Sets the poc name.
	 *
	 * @param name the new poc name
	 */
	public void setPocName(String name)
	{
		this.pointOfContact.setFullname(name);
	}

	/**
	 * Gets the poc name.
	 *
	 * @return the poc name
	 */
	
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "00", title = "POC Name", variableName = "tbPointOfContactName")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "POC Name")
	public String getPocName()
	{
		
	     return  this.pointOfContact.getFullname();
	}

	/**
	 * Sets the poc landline.
	 *
	 * @param landline the new poc landline
	 */
	public void setPocLandline(Long landline)
	{
		if(landline!=null)
		   this.pointOfContact.getContact().get(0).setLandline(landline);
	}

	/**
	 * Gets the poc landline.
	 *
	 * @return the poc landline
	 */
	public Long getPocLandline()
	{
		
		return this.pointOfContact.getContact().get(0).getLandline();
	}

	/**
	 * Gets the poc cell.
	 *
	 * @return the poc cell
	 */
	
	public Long getPocCell()
	{
		
		return this.pointOfContact.getContact().get(0).getCellNo1();
	}

	/**
	 * Sets the poc cell.
	 *
	 * @param cell the new poc cell
	 */
	public void setPocCell(Long cell)
	{
		if(cell!=null)
		  this.pointOfContact.getContact().get(0).setCellNo1(cell);
	}

	/**
	 * Gets the poc email.
	 *
	 * @return the poc email
	 */
	public String getPocEmail()
	{
		
		return this.pointOfContact.getContact().get(0).getEmail();
	}

	/**
	 * Sets the poc email.
	 *
	 * @param email the new poc email
	 */
	public void setPocEmail(String email)
	{
		if(email!=null)
		  this.pointOfContact.getContact().get(0).setEmail(email.trim());
	}

	/**
	 * Sets the address.
	 *
	 * @return the addr
	 */
	
	

	public Address getAddr() {
		return addr;
	}


	/**
	 * Sets the addr.
	 *
	 * @param address the new addr
	 */
	public void setAddr(Address address) {
		this.addr = address;
	}

	/**
	 * Gets the project name.
	 *
	 * @return the project name
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "01", title = "Project Name", variableName = "tbProjectName")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "POC Name")
	public String getProjectName() {
		return projectName;
	}


	/**
	 * Sets the project name.
	 *
	 * @param projectName the new project name
	 */
	public void setProjectName(String projectName) {
		if(projectName!=null)
			this.projectName = projectName.trim();
	}


	/**
	 * Gets the contract id.
	 *
	 * @return the contract id
	 */
	public int getContractId() {
		return contractId;
	}


	/**
	 * Sets the contract id.
	 *
	 * @param contractId the new contract id
	 */
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}


	/**
	 * Gets the person.
	 *
	 * @return the person
	 */
	public Person getPerson() {
		return pointOfContact;
	}


	/**
	 * Sets the person.
	 *
	 * @param person the new person
	 */
	public void setPerson(Person person) {
		this.pointOfContact = person;
	}


	/**
	 * Gets the lattitude.
	 *
	 * @return the lattitude
	 */
	public double getLattitude() {
		return lattitude;
	}


	/**
	 * Sets the lattitude.
	 *
	 * @param lattitude the new lattitude
	 */
	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}


	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}


	/**
	 * Sets the longitude.
	 *
	 * @param longitude the new longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}


	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		if(comment!=null)
			this.comment = comment.trim();
	}


	

	/**
	 * Gets the point of contact.
	 *
	 * @return the point of contact
	 */
	public Person getPointOfContact() {
		return pointOfContact;
	}


	/**
	 * Sets the point of contact.
	 *
	 * @param pointOfContact the new point of contact
	 */
	public void setPointOfContact(Person pointOfContact) {
		this.pointOfContact = pointOfContact;
	}


	/**
	 * Gets the tool group.
	 *
	 * @return the tool group
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "10", title = "Tool Group", variableName = "olbToolGroup")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Tool Group")
	public String getToolGroup() {
		return toolGroup;
	}


	/**
	 * Sets the tool group.
	 *
	 * @param toolGroup the new tool group
	 */
	public void setToolGroup(String toolGroup) {
		if(toolGroup!=null)
			this.toolGroup = toolGroup.trim();
	}


	

	/**
	 * Gets the tool.
	 *
	 * @return the tool
	 */
	public String getTool() {
		return tool;
	}


	/**
	 * Sets the tool.
	 *
	 * @param tool the new tool
	 */
	public void setTool(String tool) {
		if(tool!=null)
			this.tool = tool.trim();
	}
	
	public int getProjectIndex() {
		return projectIndex;
	}


	public void setProjectIndex(int projectIndex) {
		this.projectIndex = projectIndex;
	}


	public List<BillProductDetails> getBillMaterialLis() {
		return billMaterialLis;
	}


	public void setBillMaterialLis(List<BillProductDetails> billMaterialLis) {
		ArrayList<BillProductDetails> arrBill=new ArrayList<BillProductDetails>();
		arrBill.addAll(billMaterialLis);
		this.billMaterialLis = arrBill;
	}
	
	public List<ProductGroupList> getProdDetailsList() {
		return prodDetailsList;
	}

	public void setProdDetailsList(List<ProductGroupList> prodDetailsList) {
		ArrayList<ProductGroupList> productsList=new ArrayList<ProductGroupList>();
		productsList.addAll(prodDetailsList);
		this.prodDetailsList = productsList;
	}


	public Integer getServiceSrNo() {
		return serviceSrNo;
	}

	public void setServiceSrNo(int serviceSrNo) {
		this.serviceSrNo = serviceSrNo;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel arg0) 
	{
	
		return 0;
	}


	



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m)
	{
		String name = getProjectName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=this.projectName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else
		{
			if(id.equals(id))
				return false;
			else if (name.equals(curname))
				return true;
			
			else
				return false;		
		}
    }


	/**
	 * Gets the service date.
	 *
	 * @return the service date
	 */
	public Date getserviceDate() {
		// TODO Auto-generated method stub
		return serviceDate;
	}


	/**
	 * Gets the service engineer.
	 *
	 * @return the service engineer
	 */
	public String getserviceEngineer() {
		// TODO Auto-generated method stub
		return serviceEngineer;
	}


	/**
	 * Gets the service status.
	 *
	 * @return the service status
	 */
	public String getServiceStatus() {
		// TODO Auto-generated method stub
		return serviceStatus;
	}
	
	
	/**
	 * Sets the service date.
	 *
	 * @param servicedate the new service date
	 */
	public void setserviceDate(Date servicedate) {
		if(servicedate!=null)
			this.serviceDate=servicedate;
	}


	/**
	 * Sets the service engineer.
	 *
	 * @param serviceEngineer the new service engineer
	 */
	public void setserviceEngineer(String serviceEngineer) {
		if(serviceEngineer!=null)
		{
			this.serviceEngineer=serviceEngineer.trim();
		}
	}


	/**
	 * Sets the service status.
	 *
	 * @param status the new service status
	 */
	public void setServiceStatus(String status) {
		if(status!=null)
		{
			this.serviceStatus=status.trim();
			
		}
	}


	/**
	 * Sets the service id.
	 *
	 * @param value the new service id
	 */
	public void setserviceId(Integer value) {
		if(value!=null)
		{
			serviceId=value;
		}
	}
	
	/**
	 * Gets the service id.
	 *
	 * @return the service id
	 */
	public Integer getserviceId() {
		return serviceId;
	}


	/**
	 * Gets the customer cell number.
	 *
	 * @return the customer cell number
	 */
	public Long getCustomerCellNumber() {
		return personInfo.getCellNumber();
	}
	


	/**
	 * Gets the project status.
	 *
	 * @return the project status
	 */
	public String getProjectStatus() {
		return projectStatus;
	}


	/**
	 * Sets the project status.
	 *
	 * @param projectStatus the new project status
	 */
	public void setProjectStatus(String projectStatus) {
		if(projectStatus!=null)
			this.projectStatus = projectStatus.trim();
	}


	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return personInfo.getFullName();
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#getCustomerId()
	 */
	public int getCustomerCount() {
		return personInfo.getCount();
	}
	
	
	
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@OnSave
	@GwtIncompatible
	public void setLastUpdatedDate(){
		setLastUpdatedDate(DateUtility.setTimeToMidOfDay( new Date()));
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	
	/**
	 * On save.
	 */
	@GwtIncompatible
	@OnSave
	private void onSave()
	{
//		ArrayList<Key<EmployeeInfo>>tempkeys=new ArrayList<Key<EmployeeInfo>>();
//		if(technicians!=null)
//		{
//			for(EmployeeInfo emp:technicians)
//			{
//				Key tempKey=Key.create(emp);
//				if(tempKey!=null)
//				{
//					tempkeys.add(tempKey);
//				}
//			}
//		}
//		employeeInfoKey=tempkeys;
		
		/**
		 * @author Anil ,Date : 22-11-2019
		 */
		if(custNameChangeFlag){
			custNameChangeFlag=false;
			return;
		}
		
		Service service = ofy().load().type(Service.class).filter("companyId", this.getCompanyId()).filter("count", this.getserviceId()).first().now();
		ArrayList<GeneralKeyValueBean> list = new ArrayList<GeneralKeyValueBean>();
		if(service != null){
			double serviceValue = service.getServiceValue();
			int size = 0;
			double perPersonServiceValue = 0;
			if(this.technicians != null && this.technicians.size() > 0){
				size = this.technicians.size();
			}
			if(size > 0){
				perPersonServiceValue = serviceValue / size;
			}
			for(EmployeeInfo info : this.technicians){
				GeneralKeyValueBean bean = new GeneralKeyValueBean();
				bean.setName(info.getFullName());
				bean.setValue(perPersonServiceValue);
				list.add(bean);
			}
			this.setTechnicianValueList(list);
		}
			
	}
	
	/**
	 * On load.
	 */
	@GwtIncompatible
	@OnLoad
    private void onLoad()
	{
//		this.technicians=new ArrayList<EmployeeInfo>();
//		if(employeeInfoKey!=null)
//		{
//			for(Key<EmployeeInfo>empKey:employeeInfoKey)
//			{
//				if(empKey!=null)
//				{
//					EmployeeInfo info=ofy().load().key(empKey).now();
//					technicians.add(info);
//				}
//			}
//		}
	}


	public ArrayList<GeneralKeyValueBean> getTechnicianValueList() {
		return technicianValueList;
	}


	public void setTechnicianValueList(
			ArrayList<GeneralKeyValueBean> technicianValueList) {
		this.technicianValueList = technicianValueList;
	}


	public boolean isCustNameChangeFlag() {
		return custNameChangeFlag;
	}


	public void setCustNameChangeFlag(boolean custNameChangeFlag) {
		this.custNameChangeFlag = custNameChangeFlag;
	}
	
	
	
}

