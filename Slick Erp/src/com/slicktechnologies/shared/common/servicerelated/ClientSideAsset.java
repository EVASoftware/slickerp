package com.slicktechnologies.shared.common.servicerelated;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

// TODO: Auto-generated Javadoc
/**
 * Entity for Saving Customer Assets.
 */
@Entity

@Embed
public class ClientSideAsset extends SuperModel
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3541421559978297723L;
	
	/**  The lattitude of the Asset. */
	@Index
	protected String lattitude;
	
	/** The longitude of the Asset. */
	@Index
	protected String longitude;
	
	/** ID of Contract on Which Asset is Currently Served.*/
	@Index
	protected Integer contractId;
	
	/** Repersents Customer who own the Asset.*/
	@Index
	protected PersonInfo personInfo;
	
	/** Boolean to determining weather Service is Manadatory or Not. */
	private Boolean mandatoryService;
	
	/** The name. */
	@Index
	protected String name;
	@Index
	/** The model no. */
	protected String modelNo;
	@Index
	/** The sr no. */
	protected String srNo;
	/** The brand. */
	@Index
	protected String brand;
	
	/** The date of manufacture. */
	protected Date dateOfManufacture;
	
	/** The date of installation. */
	protected Date dateOfInstallation;
	/** The PO date. */
	protected Date PODate;
	
	/** The Po no. */
	protected String PoNo;
	
	/** The warrenty. */
	protected Date warrenty;
	@Index
	protected String category;
	
	protected DocumentUpload document;
	
	/** The purchased from. */
	protected String purchasedFrom;
	
	/** The price. */
	protected double price;
	
	@Index
	protected Integer salesOrderId;
	@Index
	protected Integer workOrderId;
	@Index
	protected Integer productId;
	
	@Index
	protected Integer materialId;
	
	//**********vaishnavi****************
		protected boolean recordSelect;
	
	/**
	 * Instantiates a new client side asset.
	 */
	public ClientSideAsset()
	{
		super();
		lattitude="";
		longitude="";
		personInfo=new PersonInfo();
		name="";
		modelNo="";
		brand="";
		srNo="";
		PoNo="";
		category="";
		document=new DocumentUpload();
		purchasedFrom="";
		salesOrderId=0;
		workOrderId=0;
		productId=0;
	}

	/**
	 * Instantiates a new client side asset.
	 *
	 * @param lattitude the lattitude
	 * @param longitude the longitude
	 * @param ContractId the contract id
	 */
	public ClientSideAsset(String lattitude, String longitude,Long ContractId) {
		super();
		this.lattitude = lattitude;
		this.longitude = longitude;
	}

	
	
	/**
	 * Gets the person info.
	 *
	 * @return the person info
	 */
	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	/**
	 * Sets the person info.
	 *
	 * @param personInfo the new person info
	 */
	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}

	/**
	 * Gets the lattitude.
	 *
	 * @return the lattitude
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "20", title = "Lattitude", variableName = "tblattitude")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Lattitude")
	public String getLattitude() {
		return lattitude;
	}

	/**
	 * Sets the lattitude.
	 *
	 * @param lattitude the new lattitude
	 */
	public void setLattitude(String lattitude) {
		if(lattitude!=null)
			this.lattitude = lattitude.trim();
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "21", title = "Longitude", variableName = "tblongitude")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = true, title = "Longitude")
	public String getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude the new longitude
	 */
	public void setLongitude(String longitude) {
		if(longitude!=null)
			this.longitude = longitude.trim();
	}

	/**
	 * Gets the contract id.
	 *
	 * @return the contract id
	 */
	public Integer getContractId() {
		return contractId;
	}

	/**
	 * Sets the contract id.
	 *
	 * @param integer the new contract id
	 */
	public void setContractId(Integer integer) {
		contractId = integer;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.shared.common.servicerelated.CompanyAsst#toString()
	 */
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString()
	{
		return this.name;
	}

	/**
	 * Gets the mandatory service.
	 *
	 * @return the mandatory service
	 */
	public Boolean getMandatoryService() {
		// TODO Auto-generated method stub
		return mandatoryService;
	}

	/**
	 * Sets the mandatory service.
	 *
	 * @param value the new mandatory service
	 */
	public void setMandatoryService(Boolean value) {
		if(value!=null)
			mandatoryService=value;
		
	}

	/**
	 * Gets the customer cell.
	 *
	 * @return the customer cell
	 */
	public Long getCustomerCell() {
		return personInfo.getCellNumber();
	}

	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return personInfo.getFullName();
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public int getCustomerCount() {
		return personInfo.getCount();
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return personInfo.getEmail();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name!=null){
			this.name = name.trim();
		}
	}

	public String getModelNo() {
		return modelNo;
	}

	public void setModelNo(String modelNo) {
		if(modelNo!=null){
			this.modelNo = modelNo.trim();
		}
	}

	public String getSrNo() {
		return srNo;
	}

	public void setSrNo(String srNo) {
		if(srNo!=null){
			this.srNo = srNo.trim();
		}
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		if(brand!=null){
			this.brand = brand.trim();
		}
	}
	
	public Date getDateOfManufacture() {
		return dateOfManufacture;
	}

	public void setDateOfManufacture(Date dateOfManufacture) {
		if(dateOfManufacture!=null){
			this.dateOfManufacture = dateOfManufacture;
		}
	}

	public Date getDateOfInstallation() {
		return dateOfInstallation;
	}

	public void setDateOfInstallation(Date dateOfInstallation) {
		if(dateOfInstallation!=null){
			this.dateOfInstallation = dateOfInstallation;
		}
	}
	
	public Date getPODate() {
		return PODate;
	}

	public void setPODate(Date pODate) {
		if(pODate!=null){
			PODate = pODate;
		}
	}

	public String getPoNo() {
		return PoNo;
	}

	public void setPoNo(String poNo) {
		if(PoNo!=null){
			PoNo = poNo.trim();
		}
	}
	
	public Date isWarrenty() {
		return warrenty;
	}

	public void setWarrenty(Date warrenty) {
		if(warrenty!=null){
			this.warrenty = warrenty;
		}
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		if(category!=null){
			this.category = category.trim();
		}
	}
	
	public DocumentUpload getDocument() {
		return document;
	}

	public void setDocument(DocumentUpload document) {
		this.document = document;
	}

	public String getPurchasedFrom() {
		return purchasedFrom;
	}

	public void setPurchasedFrom(String purchasedFrom) {
		if(purchasedFrom!=null){
			this.purchasedFrom = purchasedFrom.trim();
		}
	}
	
	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public Integer getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(Integer salesOrderId) {
		this.salesOrderId = salesOrderId;
	}

	public Integer getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(Integer workOrderId) {
		this.workOrderId = workOrderId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}

	public boolean isRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	
	
	
	

}
