package com.slicktechnologies.shared.common.servicerelated;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.service.ServiceSearchPopUp;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Embed

public class ServicedClientSideAsset implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2053014561039715871L;
	protected ClientSideAsset asset;
	protected Date serviceDate;
	protected boolean servicedStatus;
	protected boolean testreportattach;
	
	
	
	public ServicedClientSideAsset()
	{
		super();
		asset=new ClientSideAsset();
		serviceDate=new Date();
		servicedStatus=false;
		testreportattach=false;
	}


	public boolean isTestreportattach() {
		return testreportattach;
	}


	public void setTestreportattach(boolean testreportattach) {
		this.testreportattach = testreportattach;
	}


	public boolean isServicedStatus() {
		return servicedStatus;
	}


	public void setServicedStatus(boolean servicedStatus) {
		this.servicedStatus = servicedStatus;
	}


	public ClientSideAsset getAsset() {
		return asset;
	}


	public void setAsset(ClientSideAsset asset) {
		this.asset = asset;
	}


	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}


	public String getLattitude() {
		return asset.getLattitude();
	}


	public int getCount() {
		return asset.getCount();
	}


	public void setLattitude(String lattitude) {
		asset.setLattitude(lattitude);
	}


	public String getLongitude() {
		return asset.getLongitude();
	}


	public Long getCompanyId() {
		return asset.getCompanyId();
	}


	public void setLongitude(String longitude) {
		asset.setLongitude(longitude);
	}


	public Integer getContractId() {
		return asset.getContractId();
	}


	public int getId() {
		return asset.getCount();
	}


	public void setContractId(int contractId) {
		asset.setContractId(contractId);
	}


	public void setId(Long id) {
		asset.setId(id);
	}


	public Date getPODate() {
		return asset.getPODate();
	}


	public void setPODate(Date pODate) {
		asset.setPODate(pODate);
	}


	public String getPoNo() {
		return asset.getPoNo();
	}


	public void setPoNo(String poNo) {
		asset.setPoNo(poNo);
	}


	public boolean isDeleted() {
		return asset.isDeleted();
	}


	public String getBrand() {
		return asset.getBrand();
	}


	public void setDeleted(boolean deleted) {
		asset.setDeleted(deleted);
	}


	public void setCompanyId(Long companyId) {
		asset.setCompanyId(companyId);
	}


	public void setBrand(String brand) {
		asset.setBrand(brand);
	}


	public String getCategory() {
		return asset.getCategory();
	}


	public void setCategory(String category) {
		asset.setCategory(category);
	}


	public String getName() {
		return asset.getName();
	}


	public void setName(String name) {
		asset.setName(name);
	}


	public String getModelNo() {
		return asset.getModelNo();
	}


	public void setModelNo(String modelNo) {
		asset.setModelNo(modelNo);
	}


	public String getSrNo() {
		return asset.getSrNo();
	}


	public void setSrNo(String srNo) {
		asset.setSrNo(srNo);
	}


	public Date getDateOfManufacture() {
		return asset.getDateOfManufacture();
	}


	public void setDateOfManufacture(Date dateOfManufacture) {
		asset.setDateOfManufacture(dateOfManufacture);
	}


	public Date getDateOfInstallation() {
		return asset.getDateOfInstallation();
	}


	public void setDateOfInstallation(Date dateOfInstallation) {
		asset.setDateOfInstallation(dateOfInstallation);
	}


	public DocumentUpload getDocument() {
		return asset.getDocument();
	}


	public void setDocument(DocumentUpload document) {
		asset.setDocument(document);
	}


	public double getPrice() {
		return asset.getPrice();
	}


	public void setPrice(double price) {
		asset.setPrice(price);
	}


	public Date isWarrenty() {
		return asset.isWarrenty();
	}


	public void setWarrenty(Date warrenty) {
		asset.setWarrenty(warrenty);
	}


	public String getPurchasedFrom() {
		return asset.getPurchasedFrom();
	}


	public int hashCode() {
		return asset.hashCode();
	}


	public void setPurchasedFrom(String purchasedFrom) {
		asset.setPurchasedFrom(purchasedFrom);
	}


	public boolean isDuplicate(SuperModel m) {
		return asset.isDuplicate(m);
	}
	
	


	public Boolean getMandatoryService() {
		return asset.getMandatoryService();
	}


	public void setMandatoryService(Boolean value) {
		asset.setMandatoryService(value);
	}


	public ServicedClientSideAsset(ClientSideAsset asset, Date serviceDate) {
		super();
		this.asset = asset;
		this.serviceDate = serviceDate;
	}


	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if((obj instanceof ServicedClientSideAsset))
		{
			ServicedClientSideAsset clAsset=(ServicedClientSideAsset) obj;
			if(this.asset!=null&&clAsset.asset!=null)
			{
				if((this.asset.getId()==null)||(clAsset.asset.getId()==null))
					return false;
				
				if(this.asset.getId().equals(clAsset.asset.getId()))
					return true;
				else
					return false;
			}
		}
		
		return false;
	}
	
	
	

}
