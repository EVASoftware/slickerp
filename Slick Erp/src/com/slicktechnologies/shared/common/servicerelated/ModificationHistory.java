package com.slicktechnologies.shared.common.servicerelated;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

// TODO: Auto-generated Javadoc
/**
 * Repersents Service Modification History.
 */
@Embed
public class ModificationHistory implements Serializable
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1862570803067892260L;
	
	/** user name who modified the Service*/
	public String userName;
	
	/** The old service date. */
	public Date oldServiceDate;
	
	/** The resheduled  date. */
	public Date resheduleDate;
	
	/** The reason for Service changes */
	public String reason;
	
	/** The system date. */
	public Date systemDate;
	
	
	/**
	 * Instantiates a new modification history.
	 */
	public ModificationHistory() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Gets the old service date.
	 *
	 * @return the old service date
	 */
	public Date getOldServiceDate() {
		return oldServiceDate;
	}
	
	/**
	 * Sets the old service date.
	 *
	 * @param oldServiceDate the new old service date
	 */
	public void setOldServiceDate(Date oldServiceDate) {
		this.oldServiceDate = oldServiceDate;
	}
	
	/**
	 * Gets the reshedule date.
	 *
	 * @return the reshedule date
	 */
	public Date getResheduleDate() {
		return resheduleDate;
	}
	
	/**
	 * Sets the reshedule date.
	 *
	 * @param resheduleDate the new reshedule date
	 */
	public void setResheduleDate(Date resheduleDate) {
		this.resheduleDate = resheduleDate;
	}
	
	/**
	 * Gets the reason.
	 *
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	
	/**
	 * Sets the reason.
	 *
	 * @param reason the new reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
	

}
