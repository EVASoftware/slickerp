package com.slicktechnologies.shared.common.coststructure;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class MaterialCostStructure extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4977605468506623228L;


	int serviceId;
	String serviceName;
	String serviceCode;
	String productName;
	double productQty;
	double productCost;
	double totalCost;
	/**
	 * nidhi |*|
	 * 7-01-2019
	 * make noneditable for bom service product list
	 */
	boolean bomActive = false;
	public MaterialCostStructure() {
		serviceName="";
		serviceCode="";
		productName="";
	}
	
	
	
	
	
	public int getServiceId() {
		return serviceId;
	}





	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}





	public String getServiceName() {
		return serviceName;
	}





	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}





	public String getServiceCode() {
		return serviceCode;
	}





	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}





	public String getProductName() {
		return productName;
	}





	public void setProductName(String productName) {
		this.productName = productName;
	}





	public double getProductQty() {
		return productQty;
	}





	public void setProductQty(double productQty) {
		this.productQty = productQty;
	}





	public double getProductCost() {
		return productCost;
	}





	public void setProductCost(double productCost) {
		this.productCost = productCost;
	}





	public double getTotalCost() {
		return totalCost;
	}





	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}





	public boolean isBomActive() {
		return bomActive;
	}





	public void setBomActive(boolean bomActive) {
		this.bomActive = bomActive;
	}

}
