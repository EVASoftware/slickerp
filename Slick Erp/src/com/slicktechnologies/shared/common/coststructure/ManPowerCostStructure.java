package com.slicktechnologies.shared.common.coststructure;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed

public class ManPowerCostStructure extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6591036360898602371L;

	int serviceId;
	String serviceName;
	String serviceCode;
	String employeeLevel;
	int noOfOprator;
	double manCostPerHour;
	double totalCost;
	/** Date:- 17-01-2018 By vijay for no of hour **/
	double noOfHour;

	public ManPowerCostStructure() {
		serviceName="";
		serviceCode="";
		employeeLevel="";
		noOfHour=1d;
	}
	
	
	
	
	public int getServiceId() {
		return serviceId;
	}




	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}




	public String getServiceName() {
		return serviceName;
	}




	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}




	public String getServiceCode() {
		return serviceCode;
	}




	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}




	public String getEmployeeLevel() {
		return employeeLevel;
	}




	public void setEmployeeLevel(String employeeLevel) {
		this.employeeLevel = employeeLevel;
	}




	public int getNoOfOprator() {
		return noOfOprator;
	}




	public void setNoOfOprator(int noOfOprator) {
		this.noOfOprator = noOfOprator;
	}




	public double getManCostPerHour() {
		return manCostPerHour;
	}




	public void setManCostPerHour(double manCostPerHour) {
		this.manCostPerHour = manCostPerHour;
	}




	public double getTotalCost() {
		return totalCost;
	}




	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}


	public double getNoOfHour() {
		return noOfHour;
	}




	public void setNoOfHour(double noOfHour) {
		this.noOfHour = noOfHour;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
}
