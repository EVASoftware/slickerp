package com.slicktechnologies.shared.common.coststructure;
import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class OtherCost extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6509611213622534144L;

	String otherCostName;
	double totalCost;
	
	public OtherCost() {
		otherCostName="";
	}
	
	
	public String getOtherCostName() {
		return otherCostName;
	}


	public void setOtherCostName(String otherCostName) {
		this.otherCostName = otherCostName;
	}


	public double getTotalCost() {
		return totalCost;
	}


	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
