package com.slicktechnologies.shared.common.coststructure;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class EmployeeLevel extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8892685174034734692L;

	@Index
	String empLvlName;
	double avgHourlyRate;
	String description;
	@Index
	boolean status;
	
	public EmployeeLevel() {
		empLvlName="";
		description="";
		status=true;
	}
	
	

	public String getEmpLvlName() {
		return empLvlName;
	}



	public void setEmpLvlName(String empLvlName) {
		this.empLvlName = empLvlName;
	}



	public double getAvgHourlyRate() {
		return avgHourlyRate;
	}



	public void setAvgHourlyRate(double avgHourlyRate) {
		this.avgHourlyRate = avgHourlyRate;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public Boolean getStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		
		return isDuplicateOnEmpLvlName(m);
	}
	
	public boolean isDuplicateOnEmpLvlName(SuperModel model)
	{
		EmployeeLevel entity = (EmployeeLevel) model;
		String name = entity.getEmpLvlName().trim();
		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		String curname = getEmpLvlName().trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();
		// New Object is being added
		if (entity.id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (entity.id.equals(id))
				return false;
			if (name.equals(curname) == true)
				return true;
		}
		return false;
	}



	@Override
	public String toString() {
		return empLvlName;
	}
	

}
