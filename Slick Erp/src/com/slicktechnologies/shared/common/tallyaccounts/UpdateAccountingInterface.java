package com.slicktechnologies.shared.common.tallyaccounts;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;

import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class UpdateAccountingInterface  {

 public static void updateTally(
							 	Date accountingInterfaceCreationDate,
								String accountingInterfaceCreatedBy,
								String docStatus,
								String Status,
								String Remark,
								String Module,
								String documentType,
								int documentID,
								String documentTitle,
								Date documentDate,
								String doucmentGL,
								String referenceDocumentNumber1,
								Date referenceDocumentDate1,
								String referenceDocumentType,
								String referenceDocumentNumber2,
								Date referencedocumentDate2,
								String referencedocumentType2,
								String accountType,
								int customerID,
								String customerName,
								long customerCell,
								int vendorID,
								String vendorName,
								long vendorCell,
								int employeeID,
								String employeeName,
								String Branch,
								String personResponsible,
								String requestedBy,
								String approvedBy,
								String paymentMethod,
								Date paymentDate,
//								rohan made changes here for printing cheque no ************* 
//								int chequeNumber,
								String chequeNumber,
								Date chequeDate,
								String bankName,
								String bankAccount,
								int transferReferenceNumber,
								Date transactionDate,
								Date contractStartDate,
								Date contractEndDate,
								int productID,
								String productCode,
								String productName,
								double Quantity,
								Date productDate,
								int duration,
								int services,
								String unitOfMeasurement,
								double productPrice,
								double VATpercent,
								double VATamount,
								int VATglAccount,
								double serviceTaxPercent,
								double serviceTaxAmount,
								int serviceTaxGLaccount,
								String cForm,
								double cFormPercent,
								double cFormAmount,
								int cFormGlAccount,
								double totalAmount,
								double netPayable,
								String contractCategory,//This is added by RV for NBHC to distinguish between Tax Applicable and not Applicale in Contract
								double amountRecieved,
								
								 double baseAmount,
								 double tdsPercentage,
								 double tdsAmount,
								
								String otherCharges1,
								double otherChargesAmount1,
								String otherCharges2,
								double otherChargesAmount2,
								String otherCharges3,
								double otherChargesAmount3,
								String otherCharges4,
								double otherChargesAmount4,
								String otherCharges5,
								double otherChargesAmount5,
								String otherCharges6,
								double otherChargesAmount6,
								String otherCharges7,
								double otherChargesAmount7,
								String otherCharges8,
								double otherChargesAmount8,
								String otherCharges9,
								double otherChargesAmount9,
								String otherCharges10,
								double otherChargesAmount10,
								String otherCharges11,
								double otherChargesAmount11,
								String otherCharges12,
								double otherChargesAmount12,
								String Address,
								String Locality,
								String Landmark,
								String Country,
								String State,
								String City,
								long Pincode,
								long companyId,
								Date billingPeroidFromDate,
								Date billingPeroidToDate,
								
								/*******Added by Rahul verma for Interface logics****/
								String warehouse,
								String warehouseCode,
								String productRefid,
								String direction,
								String sourceSystem,
								String destinationSystem,
								String hsnnumber,
								String gstin,
								String numberRange
		 					)
 	
 	   {
	 
	 
	 System.out.println("ROHAN   in side update Accnt interface ");
	 
	 
		GenricServiceImpl impl = new GenricServiceImpl();
	 	AccountingInterface tallyentity = new AccountingInterface();
	 	
	 	 
	 	 if(accountingInterfaceCreationDate!=null){
	 		 tallyentity.setAccountingInterfaceCreationDate(accountingInterfaceCreationDate);
	 	 }
	 	 
	 	 if(accountingInterfaceCreatedBy!=null){
	 		 tallyentity.setAccountingInterfaceCreatedBy(accountingInterfaceCreatedBy);
	 	 }
	 	 
	 	 if(docStatus!=null){
	 		 tallyentity.setDocumentStatus(docStatus);
	 	 }
	 	 
	 	 if(Status!=null){
	 		 tallyentity.setStatus(Status);
	 	 }
	 	 
	 	 /**
	 	  * Date : 14-11-2017 BY ANIL
	 	  * this fields stores the remark after it synch
	 	  */
//	 	 if(Remark!=null){
//	 		 tallyentity.setRemark(Remark);
//	 	 }
	 	 
		 if(Module!=null){
			 tallyentity.setModule(Module);
		 }
		 
		 if(documentType!=null){
			 tallyentity.setDocumentType(documentType);
		 }
		 
		 if(documentID!=0){
			 tallyentity.setDocumentID(documentID);
		 }
		 
		 if(documentTitle!=null){
			 tallyentity.setDocumentTitle(documentTitle);
		 }
		 
		 if(documentDate!=null){
			 tallyentity.setDocumentDate(documentDate);
		 }
		 
		 if(doucmentGL!=null){
			 tallyentity.setDocumentGL(doucmentGL);
		 }
		 
		 if(referenceDocumentNumber1!=null){
			 tallyentity.setReferenceDocumentNo1(referenceDocumentNumber1);
		 }
		 
		 if(referenceDocumentDate1!=null){
			 tallyentity.setReferenceDocumentDate1(referenceDocumentDate1);
		 }
		 
		 if(referenceDocumentType!=null){
			 tallyentity.setReferenceDocumentType1(referenceDocumentType);
		 }
		 
		 if(referenceDocumentNumber2!=null){
			 tallyentity.setReferenceDocumentNo2(referenceDocumentNumber2);
		 }
		 
		 if(referencedocumentDate2!=null){
			 tallyentity.setReferenceDocumentDate2(referencedocumentDate2);
		 }
		 
		 if(referencedocumentType2!=null){
			 tallyentity.setReferenceDocumentType2(referencedocumentType2);
		 }
		 
		 if(accountType!=null){
			 tallyentity.setAccountType(accountType);
		 }
		 
		 if(customerID!=0){
			 tallyentity.setCustomerID(customerID);
		 }
		 
		 if(customerName!=null){
			 tallyentity.setCustomerName(customerName);
		 }
		 
		 if(customerCell!=0){
			 tallyentity.setCustomerCell(customerCell);
		 }
		 
		 if(vendorID!=0){
			 tallyentity.setVendorID(vendorID);
		 }
		 
		 if(vendorName!=null){
			 tallyentity.setVendorName(vendorName);
		 }
		 
		 if(vendorCell!=0){
			tallyentity.setVendorCell(vendorCell);
		 }
		 
		 if(employeeID!=0){
			 tallyentity.setEmployeeID(employeeID);
		 }
		 
		 if(employeeName!=null){
			 tallyentity.setEmployeeName(employeeName);
		 }
		 
		 if(Branch!=null){
			 tallyentity.setBranch(Branch);
		 }
		 
		 if(personResponsible!=null){
			 tallyentity.setPersonResponsible(personResponsible);
		 }
		 
		 if(requestedBy!=null){
			 tallyentity.setRequestedBy(requestedBy);
		 }
		 
		 if(approvedBy!=null){
			 tallyentity.setApprovedBy(approvedBy);
		 }
		 
		 if(paymentMethod!=null){
			 tallyentity.setPaymentMethod(paymentMethod);
		 }
		 
		 if(paymentDate!=null){
			 tallyentity.setPaymentDate(paymentDate);
		 }
		 
		 if(chequeNumber!=""){
			 tallyentity.setChequeNumber(chequeNumber);
		 }
		 
		 if(chequeDate!=null){
			 tallyentity.setChequeDate(chequeDate);
		 }
		 
		 if(bankName!=null){
			 tallyentity.setBankName(bankName);
		 }
		 
		 if(bankAccount!=null){
			 tallyentity.setBankAccount(bankAccount);
		 }
		 
		 if(transferReferenceNumber!=0){
			 tallyentity.setTransferReferenceNumber(transferReferenceNumber);
		 }
		 
		 if(transactionDate!=null){
			 tallyentity.setTransactionDate(transactionDate);
		 }
		 if(contractStartDate!=null){
			 tallyentity.setContractStartDate(contractStartDate);
		 }
		 if(contractEndDate!=null){
			 tallyentity.setContractEndDate(contractEndDate);
		 }
		 
		 if(productID!=0){
			 tallyentity.setProductID(productID);
		 }
		 
		 if(productCode!=null){
			 tallyentity.setProductCode(productCode);
		 }
		 
		 if(productName!=null){
			 tallyentity.setProductName(productName);
		 }
		 
		 if(Quantity!=0){
			 tallyentity.setQuantity(Quantity);
		 }
		 
		 if(productDate!=null){
			 tallyentity.setProductDate(productDate);
		 }
		 
		 if(duration!=0){
			 tallyentity.setProdDuration(duration);
		 }
		 
		 if(services!=0){
			 tallyentity.setProdServices(services);
		 }
		 
		 
		 if(unitOfMeasurement!=null){
			 tallyentity.setUnitOfMeasurement(unitOfMeasurement);
		 }
		 
		 if(productPrice!=0){
			 tallyentity.setProductPrice(productPrice);
		 }
		 
		 if(VATpercent!=0){
			 tallyentity.setVatPercent(VATpercent);
		 }
		 
		 if(VATamount!=0){
			 tallyentity.setVatAmount(VATamount);
		 }
		 
		 if(VATglAccount!=0){
			 tallyentity.setVatGlaccount(VATglAccount);
		 }
		 
		 if(serviceTaxPercent!=0){
			 tallyentity.setServiceTaxPercent(serviceTaxPercent);
		 }
		 
		 if(serviceTaxAmount!=0){
			 tallyentity.setServiceTaxAmount(serviceTaxAmount);
		 }
		 
		 if(serviceTaxGLaccount!=0){
			 tallyentity.setServiceTaxGlAccount(serviceTaxGLaccount);
		 }
		 
		 if(cForm!=null){
			 tallyentity.setcForm(cForm);
		 }
		 
		 if(cFormPercent!=0){
			 tallyentity.setcFormPercent(cFormPercent);
		 }
		 
		 if(cFormAmount!=0){
			 tallyentity.setcFormAmount(cFormAmount);
		 }
		 
		 if(cFormGlAccount!=0){
			 tallyentity.setcFormGlaccount(cFormGlAccount);
		 }
		 
		 if(totalAmount!=0){
			 tallyentity.setTotalAmount(totalAmount);
		 }
		 
		 if(netPayable!=0){
			 tallyentity.setNetPayable(netPayable);
		 }
		 
		 
		 if(contractCategory!=null){
			 tallyentity.setConractCategory(contractCategory);
		 }
		 
		 if(amountRecieved!=0){
			 tallyentity.setAmountRecieved(amountRecieved);
		 }
		 
		 System.out.println("ROHAN saving base Amount "+baseAmount);
		 if(baseAmount!=0){
			 tallyentity.setBaseAmount(baseAmount);
		 }
		 
		 System.out.println("ROHAN saving  tds percntage "+tdsPercentage);
		 if(tdsPercentage!=0){
			 tallyentity.setTdsPercentage(tdsPercentage);
		 }
		 
		 System.out.println("ROHAN saving  tds percntage "+tdsAmount);
		 if(tdsAmount!=0){
			 tallyentity.setTdsAmount(tdsAmount);
		 }
		 
		 if(otherCharges1!=null){
			 tallyentity.setOtherCharges1(otherCharges1);
		 }
		 
		 if(otherChargesAmount1!=0){
			 tallyentity.setOtherChargesAmount1(otherChargesAmount1);
		 }
		 
		 if(otherCharges2!=null){
			 tallyentity.setOtherCharges2(otherCharges2);
		 }
		 
		 if(otherChargesAmount2!=0){
			 tallyentity.setOtherChargesAmount2(otherChargesAmount2);
		 }

		 if(otherCharges3!=null){
			 tallyentity.setOtherCharges3(otherCharges3);
		 }
		 
		 if(otherChargesAmount3!=0){
			 tallyentity.setOtherChargesAmount3(otherChargesAmount3);
		 }

		 if(otherCharges4!=null){
			 tallyentity.setOtherCharges4(otherCharges4);
		 }
		 
		 if(otherChargesAmount4!=0){
			 tallyentity.setOtherChargesAmount4(otherChargesAmount4);
		 }

		 if(otherCharges5!=null){
			 tallyentity.setOtherCharges5(otherCharges5);
		 }
		 
		 if(otherChargesAmount5!=0){
		 tallyentity.setOtherChargesAmount5(otherChargesAmount5);
		 }

		 if(otherCharges6!=null){
			 tallyentity.setOtherCharges6(otherCharges6);
		 }
		 
		 if(otherChargesAmount6!=0){
			 tallyentity.setOtherChargesAmount6(otherChargesAmount6);
		 }

		 if(otherCharges7!=null){
			 tallyentity.setOtherCharges7(otherCharges7);
		 }
		 
		 if(otherChargesAmount7!=0){
			 tallyentity.setOtherChargesAmount7(otherChargesAmount7);
		 }

		 if(otherCharges8!=null){
			 tallyentity.setOtherCharges8(otherCharges8);
		 }
		 
		 if(otherChargesAmount8!=0){
			 tallyentity.setOtherChargesAmount8(otherChargesAmount8);
		 }

		 if(otherCharges9!=null){
			 tallyentity.setOtherCharges9(otherCharges9);
		 }
		 
		 if(otherChargesAmount9!=0){
			 tallyentity.setOtherChargesAmount9(otherChargesAmount9);
		 }

		 if(otherCharges10!=null){
			 tallyentity.setOtherCharges10(otherCharges10);
		 }
		 
		 if(otherChargesAmount10!=0){
			 tallyentity.setOtherChargesAmount10(otherChargesAmount10);
		 }
		 
		 if(otherCharges11!=null){
			 tallyentity.setOtherCharges11(otherCharges11);
		 }
		 
		 if(otherChargesAmount11!=0){
			 tallyentity.setOtherChargesAmount11(otherChargesAmount11);
		 }

		 if(otherCharges12!=null){
			 tallyentity.setOtherCharges12(otherCharges12);
		 }
		 
		 if(otherChargesAmount12!=0){
			 tallyentity.setOtherChargesAmount12(otherChargesAmount12);
		 }
		 
		 if(Address!=null){
			 tallyentity.setAddress(Address);
		 }
		 
		 if(Locality!=null){
			 tallyentity.setLocality(Locality);
		 }
		 
		 if(Landmark!=null){
			 tallyentity.setLandmark(Landmark);
		 }
		 
		 if(Country!=null){
			 tallyentity.setCountry(Country);
		 }
		 
		 if(State!=null){
			 tallyentity.setState(State);
		 }
		 
		 if(City!=null){
			 tallyentity.setCity(City);
		 }
		 
		 if(Pincode!=0){
			 tallyentity.setPincode(Pincode);
		 }
		 
		 if(billingPeroidFromDate!=null){
	 		 tallyentity.setBillingPeroidFromDate(billingPeroidFromDate);
	 	 }
		 
		 if(billingPeroidToDate!=null){
	 		 tallyentity.setBillingPeroidToDate(billingPeroidToDate);
	 	 }
		 
		 if(warehouse!=null&&warehouse.trim().length()>0){
			 tallyentity.setWareHouse(warehouse.trim());
		 }
		 if(warehouseCode!=null&&warehouseCode.trim().length()>0){
			 tallyentity.setWareHouseCode(warehouseCode);
		 }
		 if(productRefid!=null&&productRefid.trim().length()>0){
			 tallyentity.setProductRefId(productRefid.trim());
		 }
		 if(direction!=null&&direction.trim().length()>0){
			 tallyentity.setDirection(direction.trim());
		 }
		 if(sourceSystem!=null&&sourceSystem.trim().length()>0){
			tallyentity.setSourceSystem(sourceSystem.trim());	
		 }
		 if(destinationSystem!=null&&destinationSystem.trim().length()>0){
			 tallyentity.setDestinationSystem(destinationSystem.trim());	
		 } 
		 /**
		  * Date : 14-11-2017 BY ANIL
		  * setting customers reference number
		  */
		 if(referenceDocumentNumber2!=null){
			 tallyentity.setCustomerRefId(referenceDocumentNumber2);
		 }
		 
		 if(Remark!=null){
			 tallyentity.setDocumentRemark(Remark);
		 }
		 /**
		  * End
		  */
		 
		 tallyentity.setCompanyId(companyId);
		 if(hsnnumber!=null){
			 tallyentity.setHsnSac(hsnnumber);
		 }
		 if(gstin!=null){
			 tallyentity.setGstn(gstin);
		 }
		 
		 if(numberRange!=null && !numberRange.equals("")){
			tallyentity.setNumberRange(numberRange);
		 }
		 
		 impl.save(tallyentity);
 }
 public static void updateTally(
		 	Date accountingInterfaceCreationDate,
			String accountingInterfaceCreatedBy,
			String docStatus,
			String Status,
			String Remark,
			String Module,
			String documentType,
			int documentID,
			String documentTitle,
			Date documentDate,
			String doucmentGL,
			String referenceDocumentNumber1,
			Date referenceDocumentDate1,
			String referenceDocumentType,
			String referenceDocumentNumber2,
			Date referencedocumentDate2,
			String referencedocumentType2,
			String accountType,
			int customerID,
			String customerName,
			long customerCell,
			int vendorID,
			String vendorName,
			long vendorCell,
			int employeeID,
			String employeeName,
			String Branch,
			String personResponsible,
			String requestedBy,
			String approvedBy,
			String paymentMethod,
			Date paymentDate,
//			rohan made changes here for printing cheque no ************* 
//			int chequeNumber,
			String chequeNumber,
			Date chequeDate,
			String bankName,
			String bankAccount,
			int transferReferenceNumber,
			Date transactionDate,
			Date contractStartDate,
			Date contractEndDate,
			int productID,
			String productCode,
			String productName,
			double Quantity,
			Date productDate,
			int duration,
			int services,
			String unitOfMeasurement,
			double productPrice,
			double VATpercent,
			double VATamount,
			int VATglAccount,
			double serviceTaxPercent,
			double serviceTaxAmount,
			int serviceTaxGLaccount,
			String cForm,
			double cFormPercent,
			double cFormAmount,
			int cFormGlAccount,
			double totalAmount,
			double netPayable,
			String contractCategory,//This is added by RV for NBHC to distinguish between Tax Applicable and not Applicale in Contract
			double amountRecieved,
			
			 double baseAmount,
			 double tdsPercentage,
			 double tdsAmount,
			
			String otherCharges1,
			double otherChargesAmount1,
			String otherCharges2,
			double otherChargesAmount2,
			String otherCharges3,
			double otherChargesAmount3,
			String otherCharges4,
			double otherChargesAmount4,
			String otherCharges5,
			double otherChargesAmount5,
			String otherCharges6,
			double otherChargesAmount6,
			String otherCharges7,
			double otherChargesAmount7,
			String otherCharges8,
			double otherChargesAmount8,
			String otherCharges9,
			double otherChargesAmount9,
			String otherCharges10,
			double otherChargesAmount10,
			String otherCharges11,
			double otherChargesAmount11,
			String otherCharges12,
			double otherChargesAmount12,
			String Address,
			/** date 26/3/2018 added by komal for new tally interface format **/
			String address2,
			String Locality,
			String Landmark,
			String Country,
			String State,
			String City,
			long Pincode,
			long companyId,
			Date billingPeroidFromDate,
			Date billingPeroidToDate,
			
			/*******Added by Rahul verma for Interface logics****/
			String warehouse,
			String warehouseCode,
			String productRefid,
			String direction,
			String sourceSystem,
			String destinationSystem,
			/** date 26/3/2018 added by komal for new tally interface format **/
			String hsnSac,
			String gstn,
			String groupName1,
			String groupName2,
			String tax1,
			String tax2,
			double discount,
			double indirectExpenses,
			String saddrLine1,
			String saddrLine2,
			String saddrLocality,
			String saddrLandmark,
			String saddrCountry,
			String saddrState,
			String saddrCity,
			long saddrPin,
			String productCategory, String invoiceNum, String invoiceRefNum, String godownName,
			String einvoiceStatus, String einvoiceNo, String envoiceAcknowledgementNo, String envoiceAcknowledgementDate,
			String einvoiceCancellationDate, String einvoiceQRCodeString,String numberRange
			)

{


System.out.println("ROHAN   in side update Accnt interface ");


GenricServiceImpl impl = new GenricServiceImpl();
AccountingInterface tallyentity = new AccountingInterface();
/** date 5.1.2018 added by komal to add prod sr number filter **/
if(productRefid !=  null && !productRefid.equals("")){
	tallyentity = ofy().load().type(AccountingInterface.class).filter("companyId", companyId)
			.filter("documentID", documentID).filter("documentType", documentType)
			.filter("productID", productID).filter("productRefId", productRefid).first().now();
}else{
	tallyentity = ofy().load().type(AccountingInterface.class).filter("companyId", companyId)
					.filter("documentID", documentID).filter("documentType", documentType)
					.filter("productID", productID).first().now();
}

if(docStatus.equals("Cancelled")) {
	tallyentity = new AccountingInterface();
}
if(tallyentity == null){
	tallyentity = new AccountingInterface();
}else if(tallyentity.getStatus().equalsIgnoreCase(AccountingInterface.TALLYSYNCED)){
	tallyentity = new AccountingInterface();
}
if(accountingInterfaceCreationDate!=null){
tallyentity.setAccountingInterfaceCreationDate(accountingInterfaceCreationDate);
}

if(accountingInterfaceCreatedBy!=null){
tallyentity.setAccountingInterfaceCreatedBy(accountingInterfaceCreatedBy);
}

if(docStatus!=null){
tallyentity.setDocumentStatus(docStatus);
}

if(Status!=null){
tallyentity.setStatus(Status);
}

/**
* Date : 14-11-2017 BY ANIL
* this fields stores the remark after it synch
*/
//if(Remark!=null){
//tallyentity.setRemark(Remark);
//}

if(Module!=null){
tallyentity.setModule(Module);
}

if(documentType!=null){
tallyentity.setDocumentType(documentType);
}

if(documentID!=0){
tallyentity.setDocumentID(documentID);
}

if(documentTitle!=null){
tallyentity.setDocumentTitle(documentTitle);
}

if(documentDate!=null){
tallyentity.setDocumentDate(documentDate);
}

if(doucmentGL!=null){
tallyentity.setDocumentGL(doucmentGL);
}

if(referenceDocumentNumber1!=null){
tallyentity.setReferenceDocumentNo1(referenceDocumentNumber1);
}

if(referenceDocumentDate1!=null){
tallyentity.setReferenceDocumentDate1(referenceDocumentDate1);
}

if(referenceDocumentType!=null){
tallyentity.setReferenceDocumentType1(referenceDocumentType);
}

if(referenceDocumentNumber2!=null){
tallyentity.setReferenceDocumentNo2(referenceDocumentNumber2);
}

if(referencedocumentDate2!=null){
tallyentity.setReferenceDocumentDate2(referencedocumentDate2);
}

if(referencedocumentType2!=null){
tallyentity.setReferenceDocumentType2(referencedocumentType2);
}

if(accountType!=null){
tallyentity.setAccountType(accountType);
}

if(customerID!=0){
tallyentity.setCustomerID(customerID);
}

if(customerName!=null){
tallyentity.setCustomerName(customerName);
}

if(customerCell!=0){
tallyentity.setCustomerCell(customerCell);
}

if(vendorID!=0){
tallyentity.setVendorID(vendorID);
}

if(vendorName!=null){
tallyentity.setVendorName(vendorName);
}

if(vendorCell!=0){
tallyentity.setVendorCell(vendorCell);
}

if(employeeID!=0){
tallyentity.setEmployeeID(employeeID);
}

if(employeeName!=null){
tallyentity.setEmployeeName(employeeName);
}

if(Branch!=null){
tallyentity.setBranch(Branch);
}

if(personResponsible!=null){
tallyentity.setPersonResponsible(personResponsible);
}

if(requestedBy!=null){
tallyentity.setRequestedBy(requestedBy);
}

if(approvedBy!=null){
tallyentity.setApprovedBy(approvedBy);
}

if(paymentMethod!=null){
tallyentity.setPaymentMethod(paymentMethod);
}

if(paymentDate!=null){
tallyentity.setPaymentDate(paymentDate);
}

if(chequeNumber!=""){
tallyentity.setChequeNumber(chequeNumber);
}

if(chequeDate!=null){
tallyentity.setChequeDate(chequeDate);
}

if(bankName!=null){
tallyentity.setBankName(bankName);
}

if(bankAccount!=null){
tallyentity.setBankAccount(bankAccount);
}

if(transferReferenceNumber!=0){
tallyentity.setTransferReferenceNumber(transferReferenceNumber);
}

if(transactionDate!=null){
tallyentity.setTransactionDate(transactionDate);
}
if(contractStartDate!=null){
tallyentity.setContractStartDate(contractStartDate);
}
if(contractEndDate!=null){
tallyentity.setContractEndDate(contractEndDate);
}

if(productID!=0){
tallyentity.setProductID(productID);
}

if(productCode!=null){
tallyentity.setProductCode(productCode);
}

if(productName!=null){
tallyentity.setProductName(productName);
}

if(Quantity!=0){
tallyentity.setQuantity(Quantity);
}

if(productDate!=null){
tallyentity.setProductDate(productDate);
}

if(duration!=0){
tallyentity.setProdDuration(duration);
}

if(services!=0){
tallyentity.setProdServices(services);
}


if(unitOfMeasurement!=null){
tallyentity.setUnitOfMeasurement(unitOfMeasurement);
}

if(productPrice!=0){
tallyentity.setProductPrice(productPrice);
}

if(VATpercent!=0){
tallyentity.setVatPercent(VATpercent);
}

if(VATamount!=0){
tallyentity.setVatAmount(VATamount);
}

if(VATglAccount!=0){
tallyentity.setVatGlaccount(VATglAccount);
}

if(serviceTaxPercent!=0){
tallyentity.setServiceTaxPercent(serviceTaxPercent);
}

if(serviceTaxAmount!=0){
tallyentity.setServiceTaxAmount(serviceTaxAmount);
}

if(serviceTaxGLaccount!=0){
tallyentity.setServiceTaxGlAccount(serviceTaxGLaccount);
}

if(cForm!=null){
tallyentity.setcForm(cForm);
}

if(cFormPercent!=0){
tallyentity.setcFormPercent(cFormPercent);
}

if(cFormAmount!=0){
tallyentity.setcFormAmount(cFormAmount);
}

if(cFormGlAccount!=0){
tallyentity.setcFormGlaccount(cFormGlAccount);
}

if(totalAmount!=0){
tallyentity.setTotalAmount(totalAmount);
}

if(netPayable!=0){
tallyentity.setNetPayable(netPayable);
}


if(contractCategory!=null){
tallyentity.setConractCategory(contractCategory);
}

if(amountRecieved!=0){
tallyentity.setAmountRecieved(amountRecieved);
}

System.out.println("ROHAN saving base Amount "+baseAmount);
if(baseAmount!=0){
tallyentity.setBaseAmount(baseAmount);
}

System.out.println("ROHAN saving  tds percntage "+tdsPercentage);
if(tdsPercentage!=0){
tallyentity.setTdsPercentage(tdsPercentage);
}

System.out.println("ROHAN saving  tds percntage "+tdsAmount);
if(tdsAmount!=0){
tallyentity.setTdsAmount(tdsAmount);
}

if(otherCharges1!=null){
tallyentity.setOtherCharges1(otherCharges1);
}

if(otherChargesAmount1!=0){
tallyentity.setOtherChargesAmount1(otherChargesAmount1);
}

if(otherCharges2!=null){
tallyentity.setOtherCharges2(otherCharges2);
}

if(otherChargesAmount2!=0){
tallyentity.setOtherChargesAmount2(otherChargesAmount2);
}

if(otherCharges3!=null){
tallyentity.setOtherCharges3(otherCharges3);
}

if(otherChargesAmount3!=0){
tallyentity.setOtherChargesAmount3(otherChargesAmount3);
}

if(otherCharges4!=null){
tallyentity.setOtherCharges4(otherCharges4);
}

if(otherChargesAmount4!=0){
tallyentity.setOtherChargesAmount4(otherChargesAmount4);
}

if(otherCharges5!=null){
tallyentity.setOtherCharges5(otherCharges5);
}

if(otherChargesAmount5!=0){
tallyentity.setOtherChargesAmount5(otherChargesAmount5);
}

if(otherCharges6!=null){
tallyentity.setOtherCharges6(otherCharges6);
}

if(otherChargesAmount6!=0){
tallyentity.setOtherChargesAmount6(otherChargesAmount6);
}

if(otherCharges7!=null){
tallyentity.setOtherCharges7(otherCharges7);
}

if(otherChargesAmount7!=0){
tallyentity.setOtherChargesAmount7(otherChargesAmount7);
}

if(otherCharges8!=null){
tallyentity.setOtherCharges8(otherCharges8);
}

if(otherChargesAmount8!=0){
tallyentity.setOtherChargesAmount8(otherChargesAmount8);
}

if(otherCharges9!=null){
tallyentity.setOtherCharges9(otherCharges9);
}

if(otherChargesAmount9!=0){
tallyentity.setOtherChargesAmount9(otherChargesAmount9);
}

if(otherCharges10!=null){
tallyentity.setOtherCharges10(otherCharges10);
}

if(otherChargesAmount10!=0){
tallyentity.setOtherChargesAmount10(otherChargesAmount10);
}

if(otherCharges11!=null){
tallyentity.setOtherCharges11(otherCharges11);
}

if(otherChargesAmount11!=0){
tallyentity.setOtherChargesAmount11(otherChargesAmount11);
}

if(otherCharges12!=null){
tallyentity.setOtherCharges12(otherCharges12);
}

if(otherChargesAmount12!=0){
tallyentity.setOtherChargesAmount12(otherChargesAmount12);
}

if(Address!=null){
tallyentity.setAddress(Address);
}

if(Locality!=null){
tallyentity.setLocality(Locality);
}

if(Landmark!=null){
tallyentity.setLandmark(Landmark);
}

if(Country!=null){
tallyentity.setCountry(Country);
}

if(State!=null){
tallyentity.setState(State);
}

if(City!=null){
tallyentity.setCity(City);
}

if(Pincode!=0){
tallyentity.setPincode(Pincode);
}

if(billingPeroidFromDate!=null){
tallyentity.setBillingPeroidFromDate(billingPeroidFromDate);
}

if(billingPeroidToDate!=null){
tallyentity.setBillingPeroidToDate(billingPeroidToDate);
}

if(warehouse!=null&&warehouse.trim().length()>0){
tallyentity.setWareHouse(warehouse.trim());
}
if(warehouseCode!=null&&warehouseCode.trim().length()>0){
tallyentity.setWareHouseCode(warehouseCode);
}
if(productRefid!=null&&productRefid.trim().length()>0){
tallyentity.setProductRefId(productRefid.trim());
}
if(direction!=null&&direction.trim().length()>0){
tallyentity.setDirection(direction.trim());
}
if(sourceSystem!=null&&sourceSystem.trim().length()>0){
tallyentity.setSourceSystem(sourceSystem.trim());	
}
if(destinationSystem!=null&&destinationSystem.trim().length()>0){
tallyentity.setDestinationSystem(destinationSystem.trim());	
} 
/**
* Date : 14-11-2017 BY ANIL
* setting customers reference number
*/
if(referenceDocumentNumber2!=null){
tallyentity.setCustomerRefId(referenceDocumentNumber2);
}

if(Remark!=null){
tallyentity.setDocumentRemark(Remark);
}
/**
* End
*/

/** date 26/3/2018 added by komal for new tally interface format **/
if(hsnSac !=null){
	tallyentity.setHsnSac(hsnSac);
}
if(gstn !=null){
	tallyentity.setGstn(gstn);
}
if(groupName1 !=null){
	tallyentity.setGroupName1(groupName1);
}
if(groupName2 !=null){
	tallyentity.setGroupName2(groupName2);
}
if(tax1 !=null){
	tallyentity.setTax1(tax1);
}
if(tax2!=null){
	tallyentity.setTax2(tax2);
}
tallyentity.setDiscount(discount);
tallyentity.setIndirectExpenses(indirectExpenses);
tallyentity.setCompanyId(companyId);
/** date 29.5.2018 added by komal for billing address**/
tallyentity.setAddress1(address2);
tallyentity.setSaddress(saddrLine1);
tallyentity.setSaddress2(saddrLine2);
tallyentity.setSlocality(saddrLocality);
tallyentity.setSlandmark(saddrLandmark);
tallyentity.setScountry(saddrCountry); 
tallyentity.setSstate(saddrState);
tallyentity.setScity(saddrCity);
tallyentity.setSpincode(saddrPin);
/** 
* end komal
*/
if(productCategory!=null){
tallyentity.setProductCategory(productCategory);
}

if(invoiceNum!=null){
	tallyentity.setInvoiceNum(invoiceNum);
}
if(invoiceRefNum!=null){
	tallyentity.setInvoiceRefNum(invoiceRefNum);
}
if(godownName!=null){
	tallyentity.setGodownName(godownName);
}



if(einvoiceStatus!=null){
	tallyentity.setEinvoiceStatus(einvoiceStatus);
}
if(einvoiceNo!=null) {
	tallyentity.setEinvoiceNo(einvoiceNo);
}
if(envoiceAcknowledgementNo!=null) {
	tallyentity.setEinvoiceAckNo(envoiceAcknowledgementNo);
}
if(envoiceAcknowledgementDate!=null) {
	tallyentity.setEinvoiceAckDate(envoiceAcknowledgementDate);
}
if(einvoiceCancellationDate!=null) {
	tallyentity.setEinvoiceCancellationDate(einvoiceCancellationDate);
}
if(einvoiceQRCodeString!=null) {
	tallyentity.setEinvoiceQRCodeString(einvoiceQRCodeString);
}

if(numberRange!=null && !numberRange.equals("")){
	tallyentity.setNumberRange(numberRange);
}


impl.save(tallyentity);
}
 
}
