package com.slicktechnologies.shared.common.tallyaccounts;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.GeneralKeyValueBean;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
@Entity
public class AccountingInterface extends SuperModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2828547610121273721L;
	/*******************************************Contants*******************************************/
	
	public static final String TALLYCREATED = "Created" ;
	public static final String TALLYSYNCED = "Synched" ;
	public static final String TALLYCANCELLED = "Cancelled" ;
	public static final String DELETEFROMTALLY ="DeleteFromTally";
	public static final String CANCELLED ="Cancelled";
	public static final String INTEGRATESYNCED = "Integrate Synched" ;
	public static final String FAILED = "FAILED" ;
	public static final String PENDING = "Pending" ;
	public static final String INBOUNDDIRECTION = "Inbound" ;
	public static final String OUTBOUNDDIRECTION = "Outbound" ;
	/*************************************Applicability Attributes*****************************************/
	
	/**
	 * Added by Rahul Verma for nbhc
	 */
	boolean selectRecord;
	@Index
	protected Date accountingInterfaceCreationDate;
	@Index
	protected String accountingInterfaceCreatedBy;
	@Index
	protected String status;
	@Index
	protected String remark;
	@Index
	protected Date dateofSynch;
	@Index
	protected String synchedBy;
	@Index
	protected String module;
	@Index
	protected String documentType;
	@Index
	protected int documentID;
	@Index
	protected String documentTitle;
	@Index
	protected Date documentDate;
	@Index
	protected String documentGL;
	@Index
	protected String referenceDocumentNo1;
	@Index
	protected Date referenceDocumentDate1;
	@Index
	protected String referenceDocumentType1;
	protected String referenceDocumentNo2;
	protected Date referenceDocumentDate2;
	protected String referenceDocumentType2;
	@Index
	protected String accountType;
	@Index
	protected int customerID;
	@Index
	protected String customerName;
	@Index
	protected long customerCell;
	@Index
	protected int vendorID;
	@Index
	protected String vendorName;
	@Index
	protected long vendorCell;
	@Index
	protected int employeeID;
	@Index
	protected String employeeName;
	@Index
	protected String branch;
	@Index
	protected String personResponsible;
	@Index
	protected String requestedBy;
	@Index
	protected String approvedBy;
	@Index
	protected String paymentMethod;
	@Index
	protected Date paymentDate;
	@Index
	
// ***************rohan made chages here for printing cheque  no ******************	
	protected double baseAmount;
	protected double tdsPercentage;
	protected double tdsAmount;
	
	
	
	protected String chequeNumber;
	@Index
	protected Date chequeDate;
	@Index
	protected String bankName;
	@Index
	protected String bankAccount;
	@Index
	protected int transferReferenceNumber;
	@Index
	protected Date transactionDate;
	@Index
	protected Date contractStartDate;
	@Index
	protected Date contractEndDate;
	@Index
	protected int productID;
	@Index
	protected String productCode;
	@Index
	protected String productName;
	@Index
	protected double quantity;
	@Index
	protected Date productDate;
	@Index
	protected int prodDuration;
	@Index
	protected int prodServices;
	@Index
	protected String unitOfMeasurement;
	@Index
	protected double productPrice;
	@Index
	protected double vatPercent;
	@Index
	protected double vatAmount;
	@Index
	protected int vatGlaccount;
	@Index
	protected double serviceTaxPercent;
	@Index
	protected double serviceTaxAmount;
	@Index
	protected int serviceTaxGlAccount;
	@Index
	protected String cForm;
	@Index
	protected double cFormPercent;
	@Index
	protected double cFormAmount;
	@Index
	protected double cFormGlaccount;
	@Index
	protected double amountRecieved;
	@Index
	protected double totalAmount;
	@Index
	protected double netPayable;
	protected String otherCharges1;
	protected double otherChargesAmount1;
	protected String otherCharges2;
	protected double otherChargesAmount2;
	protected String otherCharges3;
	protected double otherChargesAmount3;
	protected String otherCharges4;
	protected double otherChargesAmount4;
	protected String otherCharges5;
	protected double otherChargesAmount5;
	protected String otherCharges6;
	protected double otherChargesAmount6;
	protected String otherCharges7;
	protected double otherChargesAmount7;
	protected String otherCharges8;
	protected double otherChargesAmount8;
	protected String otherCharges9;
	protected double otherChargesAmount9;
	protected String otherCharges10;
	protected double otherChargesAmount10;
	protected String otherCharges11;
	protected double otherChargesAmount11;
	protected String otherCharges12;
	protected double otherChargesAmount12;
	@Index
	protected String address;
	protected String locality;
	protected String landmark;
	@Index
	protected String country;
	@Index
	protected String state;
	@Index
	protected String city;
	@Index
	protected long pincode;
	@Index
	protected String documentStatus;
	
	
	@Index
	protected Date billingPeroidFromDate;
	@Index
	protected Date billingPeroidToDate;
	
	//Added By Rahul Verma
	@Index
	protected String customerRefId;
	@Index
	protected String conractCategory;
	protected String sapStatus;
	protected String sapSynchedBy;
	protected String sapRemark;
	protected String sapSynchedDate;
	
	/*
	 * Added by Rahul Verma
	 * Date : 24 March 2017
	 */
	@Index
	protected String wareHouse;
	@Index
	protected String wareHouseCode;
	@Index 
	protected String productRefId;
	@Index 
	protected String direction;
	@Index 
	protected String sourceSystem;
	@Index 
	protected String destinationSystem;
	
	/**
	 * Date : 14-11-2017 BY ANIL
	 * this fields captures the remark/comment at document level
	 * also this remark is used in api for NBHC
	 */
	protected String documentRemark;
	
	/** date 27.03.2018 added by komal for new tally interface requirement **/
	protected String hsnSac;
	protected String gstn;
	protected String groupName1;
	protected String groupName2;
	protected String tax1;
	protected String tax2;
	protected double discount;
	protected double indirectExpenses;
	@Index
	protected String documentUniqueId;
	
	protected String address1;
	protected String saddress;
	protected String saddress2;
	protected String slocality;
	protected String slandmark;	
	protected String scountry;	
	protected String sstate;	
	protected String scity;
	protected long spincode;
	/**
	 * end komal
	 */
	/** date 5.11.2018 added by komal to store payroll related data **/
	private ArrayList<GeneralKeyValueBean> payRollList;
	
	/**
	 * @author Anil , Date : 30-05-2019
	 * Adding Product category
	 */
	String productCategory;
	
	/**
	 * @author Anil , Date : 05-07-2019
	 */
	String invoiceNum,invoiceRefNum,godownName;
	
	
	protected String einvoiceNo;
	protected String einvoiceStatus;
	protected String einvoiceAckNo;
	protected String einvoiceAckDate;
	protected String einvoiceCancellationDate;
	protected String einvoiceQRCodeString;
	
	protected String numberRange;

/*******************************************Constructor*************************************************/
	 
	public AccountingInterface() {
		super();
		accountingInterfaceCreationDate=new Date();
		accountingInterfaceCreatedBy="";
		status="";
		remark="";
		synchedBy="";
		module="";
		documentType="";
		documentTitle="";
		documentGL="";
		referenceDocumentType1="";
		referenceDocumentType2="";
		accountType="";
		customerName="";
		vendorName="";
		employeeName="";
		branch="";
		personResponsible="";
		requestedBy="";
		approvedBy="";
		paymentMethod="";
		bankName="";
		bankAccount="";
		productCode="";
		productName="";
		unitOfMeasurement="";
		cForm="";
		otherCharges1="";
		otherCharges2="";
		otherCharges3="";
		otherCharges4="";
		otherCharges5="";
		otherCharges6="";
		otherCharges7="";
		otherCharges8="";
		otherCharges9="";
		otherCharges10="";
		otherCharges11="";
		otherCharges12="";
		address="";
		locality="";
		landmark="";
		country="";
		state="";
		city="";
		documentStatus="";
		referenceDocumentNo1="";
		referenceDocumentNo2="";
		baseAmount=0;
		
		//  rohan added because of vijay added this in billing,invoice, payment Date : 3/3/2017
		billingPeroidFromDate = null;
		billingPeroidToDate = null;
		
		selectRecord=false;
		
		/*
		 * Added by Rahul
		 */
		selectRecord=false;
		wareHouse="";
		wareHouseCode="";
		productRefId="";
		direction="";
		sourceSystem="";
		destinationSystem="";
		
		documentRemark="";
		/** date 29.5.2018 added ny komal **/
		hsnSac = "";
		gstn = "";
		groupName1 = "";
		groupName2 = "";
		tax1 = "";
		tax2 = "";	
		address1 = "";
		saddress = "";
		saddress2 = "";
		slocality = "";
		slandmark = "";	
		scountry = "";
		sstate ="";	
		scity="";
		payRollList = new ArrayList<GeneralKeyValueBean>();
		einvoiceNo="";
		einvoiceStatus="";	
		einvoiceAckNo="";
		einvoiceAckDate="";
		einvoiceCancellationDate="";
		einvoiceQRCodeString="";
		numberRange="";
	}
	
	
	
	
	
	public String getInvoiceNum() {
		return invoiceNum;
	}





	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}
	
	
	
	
	
	public String getInvoiceRefNum() {
		return invoiceRefNum;
	}
	
	
	
	
	
	public void setInvoiceRefNum(String invoiceRefNum) {
		this.invoiceRefNum = invoiceRefNum;
	}
	
	
	
	
	
	public String getGodownName() {
		return godownName;
	}
	
	
	
	
	
	public void setGodownName(String godownName) {
		this.godownName = godownName;
	}





	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}



	public boolean isSelectRecord() {
		return selectRecord;
	}

	public void setSelectRecord(boolean selectRecord) {
		this.selectRecord = selectRecord;
	}


	/*
	 * Added by Rahul Verma
	 */
	public String getCustomerRefId() {
		return customerRefId;
	}

		public void setCustomerRefId(String customerRefId) {
			this.customerRefId = customerRefId;
		}
		
		
		
/*
 * Done adding CustomerRefId
 */

	
	public String getDocumentRemark() {
			return documentRemark;
		}

		public void setDocumentRemark(String documentRemark) {
			this.documentRemark = documentRemark;
		}

	/*********************************************Overridden Method**********************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


		public static List<String> getStatusList()
		{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(TALLYCREATED);
		statuslist.add(TALLYSYNCED);
		statuslist.add(TALLYCANCELLED);
		statuslist.add(DELETEFROMTALLY);
		statuslist.add(CANCELLED);
		statuslist.add(FAILED);
		statuslist.add(PENDING);
			
		return statuslist;
	}


	
	/****************************************Getters And Setters**********************************************/
	
	

	public Date getAccountingInterfaceCreationDate() {
		return accountingInterfaceCreationDate;
	}


	public void setAccountingInterfaceCreationDate(Date accountingInterfaceCreationDate) {
		if(accountingInterfaceCreationDate!=null){
			this.accountingInterfaceCreationDate = accountingInterfaceCreationDate;
		}
	}


	public String getAccountingInterfaceCreatedBy() {
		return accountingInterfaceCreatedBy;
	}


	public void setAccountingInterfaceCreatedBy(String accountingInterfaceCreatedBy) {
		if(accountingInterfaceCreatedBy!=null){
			this.accountingInterfaceCreatedBy = accountingInterfaceCreatedBy.trim();
		}
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		if(status!=null){
			this.status = status.trim();
		}
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		if(remark!=null){
			this.remark = remark.trim();
		}
	}


	public Date getDateofSynch() {
		return dateofSynch;
	}


	public void setDateofSynch(Date dateofSynch) {
		if(dateofSynch!=null){
			this.dateofSynch = dateofSynch;
		}
	}


	public String getSynchedBy() {
		return synchedBy;
	}


	public void setSynchedBy(String synchedBy) {
		if(synchedBy!=null){
			this.synchedBy = synchedBy.trim();
		}
	}


	public String getModule() {
		return module;
	}


	public void setModule(String module) {
		if(module!=null){
			this.module = module.trim();
		}
	}


	public String getDocumentType() {
		return documentType;
	}


	public void setDocumentType(String documentType) {
		if(documentType!=null){
			this.documentType = documentType.trim();
		}
	}


	public int getDocumentID() {
		return documentID;
	}


	public void setDocumentID(int documentID) {
		this.documentID = documentID;
	}


	public String getDocumentTitle() {
		return documentTitle;
	}


	public void setDocumentTitle(String documentTitle) {
		if(documentTitle!=null){
			this.documentTitle = documentTitle.trim();
		}
	}


	public Date getDocumentDate() {
		return documentDate;
	}


	public void setDocumentDate(Date documentDate) {
		if(documentDate!=null){
			this.documentDate = documentDate;
		}
	}


	public String getDocumentGL() {
		return documentGL;
	}


	public void setDocumentGL(String documentGL) {
		if(documentGL!=null){
			this.documentGL = documentGL.trim();
		}
	}


	


	public Date getReferenceDocumentDate1() {
		return referenceDocumentDate1;
	}


	public void setReferenceDocumentDate1(Date referenceDocumentDate1) {
		if(referenceDocumentDate1!=null){
			this.referenceDocumentDate1 = referenceDocumentDate1;
		}
	}


	public String getReferenceDocumentType1() {
		return referenceDocumentType1;
	}


	public void setReferenceDocumentType1(String referenceDocumentType1) {
		if(referenceDocumentType1!=null){
			this.referenceDocumentType1 = referenceDocumentType1.trim();
		}
	}

	public String getReferenceDocumentNo1() {
		return referenceDocumentNo1;
	}

	public void setReferenceDocumentNo1(String referenceDocumentNo1) {
		if(referenceDocumentNo1!=null){
			this.referenceDocumentNo1 = referenceDocumentNo1.trim();
		}
	}

	public String getReferenceDocumentNo2() {
		return referenceDocumentNo2;
	}

	public void setReferenceDocumentNo2(String referenceDocumentNo2) {
		if(referenceDocumentNo2!=null){
			this.referenceDocumentNo2 = referenceDocumentNo2.trim();
		}
	}

	public Date getReferenceDocumentDate2() {
		return referenceDocumentDate2;
	}


	public void setReferenceDocumentDate2(Date referenceDocumentDate2) {
		if(referenceDocumentDate2!=null){
			this.referenceDocumentDate2 = referenceDocumentDate2;
		}
	}


	public String getReferenceDocumentType2() {
		return referenceDocumentType2;
	}


	public void setReferenceDocumentType2(String referenceDocumentType2) {
		if(referenceDocumentType2!=null){
			this.referenceDocumentType2 = referenceDocumentType2.trim();
		}
	}


	public String getAccountType() {
		return accountType;
	}


	public void setAccountType(String accountType) {
		if(accountType!=null){
			this.accountType = accountType.trim();
		}
	}


	public int getCustomerID() {
		return customerID;
	}


	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		if(customerName!=null){
			this.customerName = customerName.trim();
		}
	}


	public long getCustomerCell() {
		return customerCell;
	}


	public void setCustomerCell(long customerCell) {
		this.customerCell = customerCell;
	}


	public int getVendorID() {
		return vendorID;
	}


	public void setVendorID(int vendorID) {
		this.vendorID = vendorID;
	}


	public String getVendorName() {
		return vendorName;
	}


	public void setVendorName(String vendorName) {
		if(vendorName!=null){
			this.vendorName = vendorName.trim();
		}
	}


	public long getVendorCell() {
		return vendorCell;
	}


	public void setVendorCell(long vendorCell) {
		this.vendorCell = vendorCell;
	}


	public int getEmployeeID() {
		return employeeID;
	}


	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}


	public String getEmployeeName() {
		return employeeName;
	}


	public void setEmployeeName(String employeeName) {
		if(employeeName!=null){
			this.employeeName = employeeName.trim();
		}
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		if(branch!=null){
			this.branch = branch.trim();
		}
	}

	public String getPersonResponsible() {
		return personResponsible;
	}


	public void setPersonResponsible(String personResponsible) {
		if(personResponsible!=null){
			this.personResponsible = personResponsible.trim();
		}
	}


	public String getRequestedBy() {
		return requestedBy;
	}


	public void setRequestedBy(String requestedBy) {
		if(requestedBy!=null){
			this.requestedBy = requestedBy.trim();
		}
	}


	public String getApprovedBy() {
		return approvedBy;
	}


	public void setApprovedBy(String approvedBy) {
		if(approvedBy!=null){
			this.approvedBy = approvedBy.trim();
		}
	}


	public String getPaymentMethod() {
		return paymentMethod;
	}


	public void setPaymentMethod(String paymentMethod) {
		if(paymentMethod!=null){
			this.paymentMethod = paymentMethod.trim();
		}
	}


	public Date getPaymentDate() {
		return paymentDate;
	}


	public void setPaymentDate(Date paymentDate) {
		if(paymentDate!=null){
			this.paymentDate = paymentDate;
		}
	}


//	public int getChequeNumber() {
//		return chequeNumber;
//	}
//
//
//	public void setChequeNumber(int chequeNumber) {
//		this.chequeNumber = chequeNumber;
//	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	
	
	

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		if(chequeDate!=null){
			this.chequeDate = chequeDate;
		}
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		if(bankName!=null){
			this.bankName = bankName.trim();
		}
	}

	public String getBankAccount() {
		return bankAccount;
	}


	public void setBankAccount(String bankAccount) {
		if(bankAccount!=null){
			this.bankAccount = bankAccount.trim();
		}
	}


	public int getTransferReferenceNumber() {
		return transferReferenceNumber;
	}


	public void setTransferReferenceNumber(int transferReferenceNumber) {
		this.transferReferenceNumber = transferReferenceNumber;
	}


	public Date getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		if(transactionDate!=null){
			this.transactionDate = transactionDate;
		}
	}


	public int getProductID() {
		return productID;
	}


	public void setProductID(int productID) {
		this.productID = productID;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		if(productCode!=null){
			this.productCode = productCode.trim();
		}
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		if(productName!=null){
			this.productName = productName.trim();
		}
	}


	public double getQuantity() {
		return quantity;
	}


	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	public Date getProductDate() {
		return productDate;
	}


	public void setProductDate(Date productDate) {
		if(productDate!=null){
			this.productDate = productDate;
		}
	}


	public int getProdDuration() {
		return prodDuration;
	}


	public void setProdDuration(int prodDuration) {
		this.prodDuration = prodDuration;
	}


	public int getProdServices() {
		return prodServices;
	}


	public void setProdServices(int prodServices) {
		this.prodServices = prodServices;
	}


	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}


	public void setUnitOfMeasurement(String unitOfMeasurement) {
		if(unitOfMeasurement!=null){
			this.unitOfMeasurement = unitOfMeasurement.trim();
		}
	}


	public double getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}


	public double getVatPercent() {
		return vatPercent;
	}


	public void setVatPercent(double vatPercent) {
		this.vatPercent = vatPercent;
	}


	public double getVatAmount() {
		return vatAmount;
	}


	public void setVatAmount(double vatAmount) {
		this.vatAmount = vatAmount;
	}


	public int getVatGlaccount() {
		return vatGlaccount;
	}


	public void setVatGlaccount(int vatGlaccount) {
		this.vatGlaccount = vatGlaccount;
	}


	public double getServiceTaxPercent() {
		return serviceTaxPercent;
	}


	public void setServiceTaxPercent(double serviceTaxPercent) {
		this.serviceTaxPercent = serviceTaxPercent;
	}


	public double getServiceTaxAmount() {
		return serviceTaxAmount;
	}


	public void setServiceTaxAmount(double serviceTaxAmount) {
		this.serviceTaxAmount = serviceTaxAmount;
	}


	public int getServiceTaxGlAccount() {
		return serviceTaxGlAccount;
	}


	public void setServiceTaxGlAccount(int serviceTaxGlAccount) {
		this.serviceTaxGlAccount = serviceTaxGlAccount;
	}


	public String getcForm() {
		return cForm;
	}


	public void setcForm(String cForm) {
		if(cForm!=null){
			this.cForm = cForm.trim();
		}
	}


	public double getcFormPercent() {
		return cFormPercent;
	}


	public void setcFormPercent(double cFormPercent) {
		this.cFormPercent = cFormPercent;
	}


	public double getcFormAmount() {
		return cFormAmount;
	}


	public void setcFormAmount(double cFormAmount) {
		this.cFormAmount = cFormAmount;
	}


	public double getcFormGlaccount() {
		return cFormGlaccount;
	}


	public void setcFormGlaccount(double cFormGlaccount) {
		this.cFormGlaccount = cFormGlaccount;
	}


	public double getAmountRecieved() {
		return amountRecieved;
	}


	public void setAmountRecieved(double amountRecieved) {
		this.amountRecieved = amountRecieved;
	}


	public double getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}


	public double getNetPayable() {
		return netPayable;
	}


	public void setNetPayable(double netPayable) {
		this.netPayable = netPayable;
	}


	public String getOtherCharges1() {
		return otherCharges1;
	}


	public void setOtherCharges1(String otherCharges1) {
		if(otherCharges1!=null){
			this.otherCharges1 = otherCharges1.trim();
		}
	}


	public double getOtherChargesAmount1() {
		return otherChargesAmount1;
	}


	public void setOtherChargesAmount1(double otherChargesAmount1) {
		this.otherChargesAmount1 = otherChargesAmount1;
	}

	public String getOtherCharges2() {
		return otherCharges2;
	}

	public void setOtherCharges2(String otherCharges2) {
		if(otherCharges2!=null){
			this.otherCharges2 = otherCharges2.trim();
		}
	}


	public double getOtherChargesAmount2() {
		return otherChargesAmount2;
	}


	public void setOtherChargesAmount2(double otherChargesAmount2) {
		this.otherChargesAmount2 = otherChargesAmount2;
	}


	public String getOtherCharges3() {
		return otherCharges3;
	}


	public void setOtherCharges3(String otherCharges3) {
		if(otherCharges3!=null){
			this.otherCharges3 = otherCharges3.trim();
		}
	}


	public double getOtherChargesAmount3() {
		return otherChargesAmount3;
	}


	public void setOtherChargesAmount3(double otherChargesAmount3) {
		this.otherChargesAmount3 = otherChargesAmount3;
	}

	public String getOtherCharges4() {
		return otherCharges4;
	}

	public void setOtherCharges4(String otherCharges4) {
		if(otherCharges4!=null){
			this.otherCharges4 = otherCharges4.trim();
		}
	}

	public double getOtherChargesAmount4() {
		return otherChargesAmount4;
	}

	public void setOtherChargesAmount4(double otherChargesAmount4) {
		this.otherChargesAmount4 = otherChargesAmount4;
	}

	public String getOtherCharges5() {
		return otherCharges5;
	}

	public void setOtherCharges5(String otherCharges5) {
		if(otherCharges5!=null){
			this.otherCharges5 = otherCharges5.trim();
		}
	}

	public double getOtherChargesAmount5() {
		return otherChargesAmount5;
	}


	public void setOtherChargesAmount5(double otherChargesAmount5) {
		this.otherChargesAmount5 = otherChargesAmount5;
	}


	public String getOtherCharges6() {
		return otherCharges6;
	}


	public void setOtherCharges6(String otherCharges6) {
		if(otherCharges6!=null){
			this.otherCharges6 = otherCharges6.trim();
		}
	}


	public double getOtherChargesAmount6() {
		return otherChargesAmount6;
	}


	public void setOtherChargesAmount6(double otherChargesAmount6) {
		this.otherChargesAmount6 = otherChargesAmount6;
	}


	public String getOtherCharges7() {
		return otherCharges7;
	}


	public void setOtherCharges7(String otherCharges7) {
		if(otherCharges7!=null){
			this.otherCharges7 = otherCharges7.trim();
		}
	}


	public double getOtherChargesAmount7() {
		return otherChargesAmount7;
	}


	public void setOtherChargesAmount7(double otherChargesAmount7) {
		this.otherChargesAmount7 = otherChargesAmount7;
	}


	public String getOtherCharges8() {
		return otherCharges8;
	}


	public void setOtherCharges8(String otherCharges8) {
		if(otherCharges8!=null){
			this.otherCharges8 = otherCharges8.trim();
		}
	}


	public double getOtherChargesAmount8() {
		return otherChargesAmount8;
	}

	public void setOtherChargesAmount8(double otherChargesAmount8) {
		this.otherChargesAmount8 = otherChargesAmount8;
	}

	public String getOtherCharges9() {
		return otherCharges9;
	}

	public void setOtherCharges9(String otherCharges9) {
		if(otherCharges9!=null){
			this.otherCharges9 = otherCharges9.trim();
		}
	}

	public double getOtherChargesAmount9() {
		return otherChargesAmount9;
	}

	public void setOtherChargesAmount9(double otherChargesAmount9) {
		this.otherChargesAmount9 = otherChargesAmount9;
	}

	public String getOtherCharges10() {
		return otherCharges10;
	}

	public void setOtherCharges10(String otherCharges10) {
		if(otherCharges10!=null){
			this.otherCharges10 = otherCharges10.trim();
		}
	}

	public double getOtherChargesAmount10() {
		return otherChargesAmount10;
	}

	public void setOtherChargesAmount10(double otherChargesAmount10) {
		this.otherChargesAmount10 = otherChargesAmount10;
	}

	public String getOtherCharges11() {
		return otherCharges11;
	}

	public void setOtherCharges11(String otherCharges11) {
		if(otherCharges11!=null){
			this.otherCharges11 = otherCharges11.trim();
		}
	}

	public double getOtherChargesAmount11() {
		return otherChargesAmount11;
	}

	public void setOtherChargesAmount11(double otherChargesAmount11) {
		this.otherChargesAmount11 = otherChargesAmount11;
	}

	public String getOtherCharges12() {
		return otherCharges12;
	}

	public void setOtherCharges12(String otherCharges12) {
		if(otherCharges12!=null){
			this.otherCharges12 = otherCharges12.trim();
		}
	}

	public double getOtherChargesAmount12() {
		return otherChargesAmount12;
	}

	public void setOtherChargesAmount12(double otherChargesAmount12) {
		this.otherChargesAmount12 = otherChargesAmount12;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if(address!=null){
			this.address = address.trim();
		}
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		if(locality!=null){
			this.locality = locality.trim();
		}
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		if(landmark!=null){
			this.landmark = landmark.trim();
		}
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		if(country!=null){
			this.country = country.trim();
		}
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		if(state!=null){
			this.state = state.trim();
		}
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		if(city!=null){
			this.city = city.trim();
		}
	}

	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public String getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(String documentStatus) {
		if(documentStatus!=null){
			this.documentStatus = documentStatus.trim();
		}
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		if(contractStartDate!=null){
			this.contractStartDate = contractStartDate;
		}
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		if(contractEndDate!=null){
			this.contractEndDate = contractEndDate;
		}
	}

	public double getTdsPercentage() {
		return tdsPercentage;
	}

	public void setTdsPercentage(double tdsPercentage) {
		this.tdsPercentage = tdsPercentage;
	}

	public double getTdsAmount() {
		return tdsAmount;
	}

	public void setTdsAmount(double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Date getBillingPeroidFromDate() {
		return billingPeroidFromDate;
	}

	public void setBillingPeroidFromDate(Date billingPeroidFromDate) {
		this.billingPeroidFromDate = billingPeroidFromDate;
	}

	public Date getBillingPeroidToDate() {
		return billingPeroidToDate;
	}

	public void setBillingPeroidToDate(Date billingPeroidToDate) {
		this.billingPeroidToDate = billingPeroidToDate;
	}
	
	public String getConractCategory() {
		return conractCategory;
	}

	public void setConractCategory(String conractCategory) {
		this.conractCategory = conractCategory;
	}
	
	
	public String getSapStatus() {
		return sapStatus;
	}

	public void setSapStatus(String sapStatus) {
		this.sapStatus = sapStatus;
	}

	public String getSapSynchedBy() {
		return sapSynchedBy;
	}

	public void setSapSynchedBy(String sapSynchedBy) {
		this.sapSynchedBy = sapSynchedBy;
	}

	public String getSapRemark() {
		return sapRemark;
	}

	public void setSapRemark(String sapRemark) {
		this.sapRemark = sapRemark;
	}

	public String getSapSynchedDate() {
		return sapSynchedDate;
	}

	public void setSapSynchedDate(String sapSynchedDate) {
		this.sapSynchedDate = sapSynchedDate;
	}
	
	public String getWareHouse() {
		return wareHouse;
	}

	public void setWareHouse(String wareHouse) {
		this.wareHouse = wareHouse;
	}

	public String getWareHouseCode() {
		return wareHouseCode;
	}

	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}

	public String getProductRefId() {
		return productRefId;
	}

	public void setProductRefId(String productRefId) {
		this.productRefId = productRefId;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getDestinationSystem() {
		return destinationSystem;
	}

	public void setDestinationSystem(String destinationSystem) {
		this.destinationSystem = destinationSystem;
	}
	
	public String getHsnSac() {
		return hsnSac;
	}

	public void setHsnSac(String hsnSac) {
		this.hsnSac = hsnSac;
	}

	public String getGstn() {
		return gstn;
	}

	public void setGstn(String gstn) {
		this.gstn = gstn;
	}

	public String getGroupName1() {
		return groupName1;
	}

	public void setGroupName1(String groupName1) {
		this.groupName1 = groupName1;
	}

	public String getGroupName2() {
		return groupName2;
	}

	public void setGroupName2(String groupName2) {
		this.groupName2 = groupName2;
	}

	public String getTax1() {
		return tax1;
	}

	public void setTax1(String tax1) {
		this.tax1 = tax1;
	}

	public String getTax2() {
		return tax2;
	}

	public void setTax2(String tax2) {
		this.tax2 = tax2;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getIndirectExpenses() {
		return indirectExpenses;
	}

	public void setIndirectExpenses(double indirectExpenses) {
		this.indirectExpenses = indirectExpenses;
	}

	public String getDocumentUniqueId() {
		return documentUniqueId;
	}

	public void setDocumentUniqueId(String documentUniqueId) {
		this.documentUniqueId = documentUniqueId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getSaddress() {
		return saddress;
	}

	public void setSaddress(String saddress) {
		this.saddress = saddress;
	}

	public String getSaddress2() {
		return saddress2;
	}

	public void setSaddress2(String saddress2) {
		this.saddress2 = saddress2;
	}

	public String getSlocality() {
		return slocality;
	}

	public void setSlocality(String slocality) {
		this.slocality = slocality;
	}

	public String getSlandmark() {
		return slandmark;
	}

	public void setSlandmark(String slandmark) {
		this.slandmark = slandmark;
	}

	public String getScountry() {
		return scountry;
	}

	public void setScountry(String scountry) {
		this.scountry = scountry;
	}

	public String getSstate() {
		return sstate;
	}

	public void setSstate(String sstate) {
		this.sstate = sstate;
	}

	public String getScity() {
		return scity;
	}

	public void setScity(String scity) {
		this.scity = scity;
	}

	public long getSpincode() {
		return spincode;
	}

	public void setSpincode(long spincode) {
		this.spincode = spincode;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return documentUniqueId;
	}
	@OnSave
	@GwtIncompatible
	private void updateCustomerRefId(){
		/**
		 * Date : 09-11-2017 BY ANIL
		 * for NBHC :this code commented because one customer can have multiple reference number 
		 * in sap so we will not pick reference number from customer 
		 */
//		if (id == null) {
//			Customer customer = ofy().load().type(Customer.class).filter("count", this.getCustomerID())
//					.filter("companyId", getCompanyId()).first().now();
//			if(customer!=null){
//				if(customer.getRefrNumber1()!=null){
//					setCustomerRefId(customer.getRefrNumber1());
//				}
//			}
//		}
		
	}

	public ArrayList<GeneralKeyValueBean> getPayRollList() {
		return payRollList;
	}

	public void setPayRollList(ArrayList<GeneralKeyValueBean> payRollList) {
		this.payRollList = payRollList;
	}





	public String getEinvoiceNo() {
		return einvoiceNo;
	}





	public void setEinvoiceNo(String einvoiceNo) {
		this.einvoiceNo = einvoiceNo;
	}





	public String getEinvoiceStatus() {
		return einvoiceStatus;
	}





	public void setEinvoiceStatus(String einvoiceStatus) {
		this.einvoiceStatus = einvoiceStatus;
	}





	public String getEinvoiceAckNo() {
		return einvoiceAckNo;
	}





	public void setEinvoiceAckNo(String einvoiceAckNo) {
		this.einvoiceAckNo = einvoiceAckNo;
	}





	public String getEinvoiceAckDate() {
		return einvoiceAckDate;
	}





	public void setEinvoiceAckDate(String einvoiceAckDate) {
		this.einvoiceAckDate = einvoiceAckDate;
	}





	public String getEinvoiceCancellationDate() {
		return einvoiceCancellationDate;
	}





	public void setEinvoiceCancellationDate(String einvoiceCancellationDate) {
		this.einvoiceCancellationDate = einvoiceCancellationDate;
	}





	public String getEinvoiceQRCodeString() {
		return einvoiceQRCodeString;
	}





	public void setEinvoiceQRCodeString(String einvoiceQRCodeString) {
		this.einvoiceQRCodeString = einvoiceQRCodeString;
	}





	public String getNumberRange() {
		return numberRange;
	}





	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}

	
	
	
	
	
}
