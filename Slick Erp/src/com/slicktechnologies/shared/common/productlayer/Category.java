package com.slicktechnologies.shared.common.productlayer;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ProductCategoryChecklist;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

// TODO: Auto-generated Javadoc
/**
 * The Represents Category of a {@link SuperProduct}
 */
@Entity
public class Category extends SuperModel {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4516701443157581866L;

	/** The id. */
	
	/** The cat code. */
	protected String catCode;
	
	/** The cat name. */
	@Index protected String catName;
	
	/** The status. */
	@Index
	protected Boolean status;
	
	/** Description of category */
	protected String description;
	
	/** type of a category  right now only two types of categories(SELL/PURCHASE) also not using this attribute */
	protected String type;
	
	@Index
	protected boolean isSellCategory;
	
	@Index
	protected boolean isServiceCategory;
    /**@Sheetal : 08-02-2022
     * Des : Adding Checklist in product category**/
	protected ArrayList<ProductCategoryChecklist> checkList;
	
	protected double serviceValuePercentage;
	/**
	 * Instantiates a new category.
	 */
	public Category()
	{
		catCode="";
		catName="";
		status=true;
		description="";
		checkList=new ArrayList<ProductCategoryChecklist>();
	}

	/**
	 * Gets the cat code.
	 *
	 * @return the cat code
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Category Code")
	public String getCatCode() {
		return catCode;
	}

	/**
	 * Sets the cat code.
	 *
	 * @param catCode the new cat code
	 */
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	/**
	 * Gets the cat name.
	 *
	 * @return the cat name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Category Name")
	public String getCatName() {
		return catName;
	}

	/**
	 * Sets the cat name.
	 *
	 * @param catName the new cat name
	 */
	public void setCatName(String catName) {
		this.catName = catName;
	}

	
	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Category Status")
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Sell Category")
	public Boolean getIsSellCategory()
	 
	{
		return isSellCategory;
	}

	public void setSellCategory(Boolean sellCategory)
	{
		this.isSellCategory=sellCategory;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = true, title = "Service Category")
	public Boolean isServiceCategory() {
		return isServiceCategory;
	}

	public void setServiceCategory(boolean isServiceCategory) {
		this.isServiceCategory = isServiceCategory;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.catName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
	/**Added by sheetal : 08-02-2022**/
	public List<ProductCategoryChecklist> getcheckList() {
		return checkList;
	}

	public void setcheckList(List<ProductCategoryChecklist> checkList) {
		ArrayList<ProductCategoryChecklist> arrProcess=new ArrayList<ProductCategoryChecklist>();
		arrProcess.addAll(checkList);
		this.checkList = arrProcess;
	}

	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0!=null)
		{
			Category cat=(Category) arg0;
			if(cat.getCatName()!=null)
				return this.catName.compareTo(cat.catName);
		}
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		
		return isDuplicateonCatCode(m)||isDuplicateOnCatName(m);
	}
	/**
	 * Calculates duplicate on Category Name
	 * @param model {@link SuperModel} object
	 * @return
	 */
	public boolean isDuplicateOnCatName(SuperModel model)
	{
		{
			Category entity=(Category) model;
			String name = entity.getCatName().trim();
			name=name.replaceAll("\\s","");
			name=name.toLowerCase();
			
			String curname=getCatName().trim();
			curname=curname.replaceAll("\\s","");
			curname=curname.toLowerCase();
			//New Object is being added
			if(entity.id==null)
			{
				if(name.equals(curname))
					return true;
				else
					return false;
			}
			// Old object is being updated
			else
			{
				if(entity.id.equals(id))
					return false;	
				if(name.equals(curname)==true)
					return true;
			}
		}
		return false;
	}
	/**
	 * Compares is Duplicate on cat Code
	 * @param model {@link SuperModel} object to check for duplicate
	 * @return
	 */
	public boolean isDuplicateonCatCode(SuperModel model)
	{
		Category entity=(Category) model;
		String name = entity.getCatCode().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=getCatCode().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(entity.id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else
		{
			if(entity.id.equals(id))
				return false;	
			if(name.equals(curname)==true)
				return true;
		}
	
	return false;
	}
	
	public static void makeItemCategoryListBoxLive(ObjectListBox<Category> olbProductCategory)
	{
		MyQuerry myQuerry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);

		
		Filter itemCatFilter=new Filter();
		itemCatFilter.setQuerryString("isSellCategory");
		itemCatFilter.setBooleanvalue(true);
		
		myQuerry.getFilters().add(filter);
		myQuerry.getFilters().add(itemCatFilter);
		myQuerry.setQuerryObject(new Category());
		olbProductCategory.MakeLive(myQuerry);
	
	}

	public double getServiceValuePercentage() {
		return serviceValuePercentage;
	}

	public void setServiceValuePercentage(double serviceValuePercentage) {
		this.serviceValuePercentage = serviceValuePercentage;
	}

	
	

}
