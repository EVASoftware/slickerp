package com.slicktechnologies.shared.common.productlayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * The Class ProductDetails. SuperClass of product related details of inventory.
 */
@Embed
@Entity
public class ProductDetail1 extends SuperModel
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6895583181107678650L;

	/** The product id. */
	protected int productId;
	
	/** The product category. */
	@Index
	protected String productCategory;
	
	/** The product code. */
	
	protected String productCode;
	
	/** The product name. */
	@Index
	protected String productName;
	
	/** The unit of measurment. */
	protected String unitOfMeasurment;
	
	protected Tax lbtTax;
	protected Tax vatTax;
	
	Double price;
	@Index
	String warehouseLocation;
	
	public ProductDetail1() {
		super();
		productCategory="";
		productCode="";
		productName="";
		unitOfMeasurment="";
		warehouseLocation="";
		lbtTax=new Tax();
		vatTax=new Tax();
	}

	
	
    
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		if(productCategory!=null)
		this.productCategory = productCategory.trim();
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		if(productCode!=null)
		this.productCode = productCode.trim();
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) 
	{
		if(productName!=null)
		this.productName = productName.trim();
	}

	public String getUnitOfMeasurment() {
		return unitOfMeasurment;
	}

	public void setUnitOfMeasurment(String unitOfMeasurment) {
		if(unitOfMeasurment!=null)
		this.unitOfMeasurment = unitOfMeasurment.trim();
	}
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public static void makeProductDetailListBoxLive(
			ObjectListBox<ProductDetail1> olbProductName) 
	{
		MyQuerry myQuerry=new MyQuerry();
		Filter filter=new Filter();
	
		myQuerry.getFilters().add(filter);
		myQuerry.setQuerryObject(new ProductDetail1());
		olbProductName.MakeLive(myQuerry);
		
	}
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public String toString() {
		return productName;
	}


	public Tax getLbtTax() {
		return lbtTax;
	}


	public void setLbtTax(Tax lbtTax) {
		this.lbtTax = lbtTax;
	}


	public Tax getVatTax() {
		return vatTax;
	}


	public void setVatTax(Tax vatTax) {
		this.vatTax = vatTax;
	}


	public String getWarehouseLocation() {
		return warehouseLocation;
	}


	public void setWarehouseLocation(String warehouseLocation) {
		this.warehouseLocation = warehouseLocation;
	}
	
	
	
	

}
