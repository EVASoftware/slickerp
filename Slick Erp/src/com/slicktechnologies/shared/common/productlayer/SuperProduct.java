package com.slicktechnologies.shared.common.productlayer;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

import static  com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * The Class SuperProduct. Abstracts the common attributes of any product.
 */
@Entity
public class SuperProduct extends SuperModel
{
	
	/** *********************************************Entity Attributes***************************************************. */
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1890L;

	/** The prod code. */
	@Index
	protected String productCode;

	/** The prod name. */
	@Index
	protected String productName;

	/** The spec. */
	protected String specifications;

	/** The comment. */
	protected String comment;
	
	protected String commentdesc;
	protected String commentdesc1;
	protected String commentdesc2;

	/** Unit of measurement of the product. */
	@Index
	protected String unitOfMeasurement;

	/** The price can be (sell/purchase/other) further will be decided as per the business logic */
	protected double price;

	/** The terms and conditions. */
	protected DocumentUpload termsAndConditions;

	/** The lbt tax entity. */
	//protected Tax lbtTax;

	/** The status. */
	@Index
	protected boolean status=true;

	/** The product category. */
	@Index
	protected String productCategory;
	
	@Index
	protected String productGroup;
	@Index
	protected String productType;
	@Index
	protected String productClassification;
	
	/** The product image. */
	protected DocumentUpload productImage;
	
	//***************rohan added this for leather techno requirement ****************
	
	/** The product image1. */
	protected DocumentUpload productImage1;
	/** The product image2. */
	protected DocumentUpload productImage2;
	/** The product image3. */
	protected DocumentUpload productImage3;
	
	
	/** The service tax entity. */
	protected Tax serviceTax;
	
	/** The vat tax entity. */
	protected Tax vatTax;
	
	protected String refNumber1;
	protected String refNumber2;

	/**
	 * Rohan added this field for HSN code  
	 */
	
	protected String hsnNumber;
	/**
	 * ends here 
	 */
	/** *********************************************Relational Attributes***************************************************. */
	@Index
	protected Key<Category> categoryKey;

	
	
	/**
	 * Date : 04-01-2018 BY ANIL
	 */
	
	protected Tax purchaseTax1;
	protected Tax purchaseTax2;
	protected double purchasePrice;
	/**
	 * End
	 */
	/** date 14.7.2018 added by komal for margin of purchase **/
	protected double margin;
	
	protected String termsAndConditionLink;
	
	protected long zohoItemID;
	
	/**
	 * *********************************************Default Ctor***************************************************.
	 */
	/** Instantiates a new super product.*/
	public SuperProduct()
	{
		productImage=new DocumentUpload();
		termsAndConditions=new DocumentUpload();
		//lbtTax=new Tax();
		serviceTax=new Tax();
		vatTax=new Tax();
		productCode="";
		productName="";
		specifications="";
		comment="";
		unitOfMeasurement="";
		productCategory="";
		productGroup="";
		productType="";
		productClassification="";
		refNumber1="";
		refNumber2="";
		commentdesc="";
		
		purchaseTax1=new Tax();
		purchaseTax2=new Tax();
		purchasePrice=0;
		
		/** Date 02-11-2019 By Vijay Contract Data Corrupt ***/
		commentdesc1="";
		commentdesc2="";
		productImage1=new DocumentUpload();
		productImage2=new DocumentUpload();
		productImage3=new DocumentUpload();
		termsAndConditionLink ="";

	}

	/**
	 * *********************************************Getter/Setter***************************************************/
	
	
	public Tax getPurchaseTax1() {
		return purchaseTax1;
	}

	public void setPurchaseTax1(Tax purchaseTax1) {
		this.purchaseTax1 = purchaseTax1;
	}

	public Tax getPurchaseTax2() {
		return purchaseTax2;
	}

	public void setPurchaseTax2(Tax purchaseTax2) {
		this.purchaseTax2 = purchaseTax2;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	
	
	
	/**
	 * Gets the prod code.
	 *
	 * @return the prod code
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "PCode")
	public String getProductCode() {
		return productCode;
	}


	/**
	 * Sets the prod code.
	 *
	 * @param prodCode the new prod code
	 */
	public void setProductCode(String prodCode)
	{
		if(prodCode!=null)
			this.productCode = prodCode.trim();
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProductName(String prodName) 
	{
		if(prodName!=null)
			this.productName = prodName.trim();
	}

	
	/**
	 * Gets the unit of measurement.
	 *
	 * @return the unit of measurement
	 */
	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	/**
	 * Sets the unit of measurement.
	 *
	 * @param unitOfMeasurement the new unit of measurement
	 */
	public void setUnitOfMeasurement(String unitOfMeasurement)
	{
		if(unitOfMeasurement!=null)
			this.unitOfMeasurement = unitOfMeasurement.trim();
	}

	/**
	 * Gets the product category.
	 *
	 * @return the product category
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Category")
	public String getProductCategory() {
		return productCategory;
	}

	/**
	 * Sets the product category.
	 *
	 * @param productCategory the new product category
	 */
	public void setProductCategory(String productCategory) {
		if(productCategory!=null)
			this.productCategory = productCategory.trim();
	}
	
	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		if(productGroup!=null){
			this.productGroup = productGroup.trim();
		}
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		if(productType!=null){
			this.productType = productType.trim();
		}
	}

	public String getProductClassification() {
		return productClassification;
	}

	public void setProductClassification(String productClassification) {
		if(productClassification!=null){
			this.productClassification = productClassification;
		}
	}

	/**
	 * Gets the category key.
	 *
	 * @return the category key
	 */
	public Key<Category> getCategoryKey() {
		return categoryKey;
	}

	/**
	 * Sets the category key.
	 *
	 * @param categoryKey the new category key
	 */
	public void setCategoryKey(Key<Category> categoryKey) {
		this.categoryKey = categoryKey;
	}

	/**
	 * Gets the spec.
	 *
	 * @return the spec
	 */
	public String getSpecification() {
		return specifications;
	}

	/**
	 * Sets the spec.
	 *
	 * @param spec the new spec
	 */
	public void setSpec(String spec)
	{
		if(specifications!=null)
			this.specifications = spec.trim();
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment)
	{
		if(comment!=null)
			this.comment = comment.trim();
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Price")
	public Double getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Gets the term ncondition.
	 *
	 * @return the term ncondition
	 */
	public DocumentUpload getTermNcondition() {
		return termsAndConditions;
	}

	/**
	 * Sets the term ncondition.
	 *
	 * @param termNcondition the new term ncondition
	 */
	public void setTermNcondition(DocumentUpload termNcondition) {
		this.termsAndConditions = termNcondition;
	}


	/**
	 * Gets the lbt tax entity.
	 *
	 * @return the lbt tax entity
	 */
	/*@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = false, title = "Lbt")
	public Tax getLbtTax() {
		return lbtTax;
	}
*/
	/**
	 * Sets the lbt tax entity.
	 *
	 * @param lbtTaxEntity the new lbt tax entity
	 */
	/*public void setLbtTax(Tax lbtTaxEntity) {
		this.lbtTax = lbtTaxEntity;
	}*/



	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public Boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the specifications.
	 *
	 * @return the specifications
	 */
	public String getSpecifications() {
		return specifications;
	}

	/**
	 * Sets the specifications.
	 *
	 * @param specifications the new specifications
	 */
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	/**
	 * Gets the terms and conditions.
	 *
	 * @return the terms and conditions
	 */
	public DocumentUpload getTermsAndConditions() {
		return termsAndConditions;
	}

	/**
	 * Sets the terms and conditions.
	 *
	 * @param termsAndConditions the new terms and conditions
	 */
	public void setTermsAndConditions(DocumentUpload termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	/**
	 * Gets the product image.
	 *
	 * @return the product image
	 */
	public DocumentUpload getProductImage() {
		return productImage;
	}

	/**
	 * Sets the product image.
	 *
	 * @param productImage the new product image
	 */
	public void setProductImage(DocumentUpload productImage) {
		this.productImage = productImage;
	}
	
	/**
	 * Gets the service tax.
	 *
	 * @return the service tax
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = false, title = "Service Tax")
	public Tax getServiceTax() {
		return serviceTax;
	}

	/**
	 * Sets the service tax.
	 *
	 * @param serviceTax the new service tax
	 */
	public void setServiceTax(Tax serviceTax) {
		this.serviceTax = serviceTax;
	}
	/**
	 * Gets the vat tax entity.
	 *
	 * @return the vat tax entity
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = false, title = "VAT")
	public Tax getVatTax() {
		return vatTax;
	}
	
	/**
	 * Sets the vat tax entity.
	 *
	 * @param vatTaxEntity the new vat tax entity
	 */
	public void setVatTax(Tax vatTaxEntity) {
		this.vatTax = vatTaxEntity;
	}
	
	public String getRefNumber1() {
		return refNumber1;
	}

	public void setRefNumber1(String refNumber1) {
		if(refNumber1!=null){
			this.refNumber1 = refNumber1.trim();
		}
	}

	public String getRefNumber2() {
		return refNumber2;
	}

	public void setRefNumber2(String refNumber2) {
		if(refNumber2!=null){
			this.refNumber2 = refNumber2.trim();
		}
	}
	
	public String getCommentdesc() {
		return commentdesc;
	}

	public void setCommentdesc(String commentdesc) {
		if(commentdesc!=null){
			this.commentdesc = commentdesc.trim();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		SuperProduct per=(SuperProduct)arg0;
		return per.productName.compareTo(productName);
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.common.helperlayer.SuperModel#isDuplicate(com.simplesoftwares.shared.common.helperlayer.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel model) {
		//boolean prodNamebool=isDuplicateProductName(model);
		boolean prodCodebool=isDuplicateProductCode(model);
		//System.out.println(prodNamebool+"NAME");
		System.out.println(prodCodebool+"CODE");
		//System.out.println("Returning"+(prodNamebool&&prodCodebool));
		System.out.println("Returning"+(prodCodebool));
		return prodCodebool;
		
		
	}
	
	
	public boolean isDuplicateProductCode(SuperModel model) {
		SuperProduct per=(SuperProduct) model;
		String name = per.getProductCode().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=this.productCode.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(per.id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}

		// Old object is being updated
		else
		{
			if(per.id.equals(id))
				return false;
			else if (name.equals(curname))
				return true;
			else
				return false;

		}
	}
	

	public boolean isDuplicateProductName(SuperModel model) {
		SuperProduct per=(SuperProduct) model;
		String name = per.getProductName();
		//System.out.println("VALUE OF PRODUCT "+per);
		//System.out.println("VALUE OF PRODUCT NAME "+per.prodName);
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();

		String curname=this.productName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(per.id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}

		// Old object is being updated
		else
		{
			if(per.id.equals(id))
				return false;
			else if (name.equals(curname))
				return true;
			else
				return false;

		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return productName;
	}

	@GwtIncompatible
	public void onSave()
	{
		if(productCategory!=null)
		   categoryKey=ofy().load().type(Category.class).filter("catName",this.productCategory).keys().first().now();
		
	}
	
	@GwtIncompatible
	public void onLoad()
	{
	  if(categoryKey!=null)
	  {
		 Category cat=ofy().load().key(categoryKey).now();
		 if(cat!=null)
		 {
			if(cat.getCatName().equals("")==false)
			 this.productCategory=(cat.getCatName());
		 }
		 
	  }
	}

	public DocumentUpload getProductImage1() {
		return productImage1;
	}

	public void setProductImage1(DocumentUpload productImage1) {
		this.productImage1 = productImage1;
	}

	public DocumentUpload getProductImage2() {
		return productImage2;
	}

	public void setProductImage2(DocumentUpload productImage2) {
		this.productImage2 = productImage2;
	}

	public DocumentUpload getProductImage3() {
		return productImage3;
	}

	public void setProductImage3(DocumentUpload productImage3) {
		this.productImage3 = productImage3;
	}

	public String getCommentdesc1() {
		return commentdesc1;
	}

	public void setCommentdesc1(String commentdesc1) {
		this.commentdesc1 = commentdesc1;
	}

	public String getCommentdesc2() {
		return commentdesc2;
	}

	public void setCommentdesc2(String commentdesc2) {
		this.commentdesc2 = commentdesc2;
	}

	public String getHsnNumber() {
		return hsnNumber;
	}

	public void setHsnNumber(String hsnNumber) {
		this.hsnNumber = hsnNumber;
	}

	public double getMargin() {
		return margin;
	}

	public void setMargin(double margin) {
		this.margin = margin;
	}

	
	public String getTermsAndConditionLink() {
		return termsAndConditionLink;
	}

	public void setTermsAndConditionLink(String termsAndConditionLink) {
		this.termsAndConditionLink = termsAndConditionLink;
	}

	public long getZohoItemID() {
		return zohoItemID;
	}

	public void setZohoItemID(long zohoItemID) {
		this.zohoItemID = zohoItemID;
	}
	
	
}
