package com.slicktechnologies.shared.common.productlayer;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.editor.client.Editor.Ignore;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.EntitySubclass;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;


/**
 * Represents product only for sell or purchase.
 */
@EntitySubclass(index=true)
@Embed
public class ItemProduct extends SuperProduct 
{	
	/***********************************************Entity Attributes****************************************************/
	/**  If true product is for sell. */
	@Index 
	protected boolean isSellProduct;
	
	/** if true product is for purchase. */
	protected boolean isPurchaseProduct;

	/** The vat tax entity. */
	//protected Tax vatTax;
	
	/** it represents the remainder about when to purchase product in the inventory */
	protected int reOrderQuantity;
	
	/**  The barcode for this product. */
	@Index
	protected String barcode;

	/**  The brand of this product. */
	@Index
	protected String brandName;
	
	protected String model;
	protected String serialNumber;
	
	
	@Index
	protected Integer warrantyPeriodInMonths;
	
	
	/**
	 * Date 21 july 2017 added by vijay for QuickSales
	 */
	@Index
	protected double availableQuantity;
	@Index
	protected String warehouse;
	@Index
	protected String storageLocation;
	@Index
	protected String storageBin;
	
	/**
	 * Ends here
	 */
	/**
	 * Date :04-07-2018 BY RAHUL
	 */
	double mrp;
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7252002290923932933L;

	/***********************************************Relational Attributes****************************************************/
	protected Key<Brand> brandKey;
	
	/***********************************************Default Ctor****************************************************/
	/** Instantiates a new item product */
	
	/**
	 * Date : 16-04-2018 By ANIL
	 * here we are storing the reference service product against item product
	 * Requirement By : Rohan and Nitin sir.
	 * Description : for creating amc contract against sales order invoice
	 */
//	ServiceProduct serviceProduct;
	
	/**
	 * Date : 18-04-2018 BY ANIL
	 */
	String serProdName;
	@Index
	String serProdCode;
	@Index
	int serProdId;
	int warrantyDuration;
	int noOfServices;
	protected boolean isFreez=false;
	
	
	
	public ItemProduct()
	{
		super();
		//vatTax=new Tax();
		barcode="";
		brandName="";
		//vatTax=new Tax();
		brandKey=null;
		//brandName="";
		warrantyPeriodInMonths=0;
		
		model="";
		serialNumber="";
		
		warehouse="";
		storageLocation="";
		storageBin="";
		
//		serviceProduct=new ServiceProduct();
		serProdName="";
		serProdCode="";
		
		
	}
		
	
	/***********************************************Getter/Setter****************************************************/
	
	
	
	
//	public ServiceProduct getServiceProduct() {
//		return serviceProduct;
//	}
//	public void setServiceProduct(ServiceProduct serviceProduct) {
//		this.serviceProduct = serviceProduct;
//	}
	
	
	public String getSerProdName() {
		return serProdName;
	}
	public double getMrp() {
		return mrp;
	}


	public void setMrp(double mrp) {
		this.mrp = mrp;
	}


	public void setSerProdName(String serProdName) {
		this.serProdName = serProdName;
	}
	public String getSerProdCode() {
		return serProdCode;
	}
	public void setSerProdCode(String serProdCode) {
		this.serProdCode = serProdCode;
	}
	public int getSerProdId() {
		return serProdId;
	}
	public void setSerProdId(int serProdId) {
		this.serProdId = serProdId;
	}
	public int getWarrantyDuration() {
		return warrantyDuration;
	}
	public void setWarrantyDuration(int warrantyDuration) {
		this.warrantyDuration = warrantyDuration;
	}
	public int getNoOfServices() {
		return noOfServices;
	}
	public void setNoOfServices(int noOfServices) {
		this.noOfServices = noOfServices;
	}


	
	/**
	 * Checks if is sell product.
	 *
	 * @return true, if is sell product
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 7, isFieldUpdater = false, isSortable = true, title = "Sell Product")
	public boolean isSellProduct() {
		return isSellProduct;
	}
	
	/**
	 * Sets the sell product.
	 *
	 * @param isSellProduct the new sell product
	 */
	public void setSellProduct(boolean isSellProduct) {
		this.isSellProduct = isSellProduct;
	}
	
	/**
	 * Checks if is purchase product.
	 *
	 * @return true, if is purchase product
	 */
	public boolean isPurchaseProduct() {
		return isPurchaseProduct;
	}
	
	/**
	 * Sets the purchase product.
	 *
	 * @param isPurchaseProduct the new purchase product
	 */
	public void setPurchaseProduct(boolean isPurchaseProduct) {
		this.isPurchaseProduct = isPurchaseProduct;
	}
	
	/**
	 * Gets the vat tax entity.
	 *
	 * @return the vat tax entity
	 */
	//@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = false, title = "Vat")
	/*public Tax getVatTax() {
		return vatTax;
	}*/
	
	/**
	 * Sets the vat tax entity.
	 *
	 * @param vatTaxEntity the new vat tax entity
	 */
/*	public void setVatTax(Tax vatTaxEntity) {
		this.vatTax = vatTaxEntity;
	}*/
	
	/**
	 * Gets the barcode.
	 *
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}
	
	/**
	 * Sets the barcode.
	 *
	 * @param barcode the new barcode
	 */
	public void setBarcode(String barcode)
	{
		if(barcode!=null)
			this.barcode = barcode.trim();
	}
	
	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	
	
	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void OnSave()
	{
		super.onSave();
		
	}
	
	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public boolean isFreez() {
		return isFreez;
	}

	public void setFreez(boolean isFreez) {
		this.isFreez = isFreez;
	}
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{
		super.onLoad();
	}

	@Override
	public boolean isDuplicate(SuperModel model) {
		return isDuplicateProductCode(model);
	}

	public Integer getWarrantyPeriodInMonths() {
		return warrantyPeriodInMonths;
	}

	public void setWarrantyPeriodInMonths(Integer warrantyPeriodInMonths) {
		this.warrantyPeriodInMonths = warrantyPeriodInMonths;
	}
	
	
	public double getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(double availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}
	
	
	/***********************************************************************************************************************/
}


