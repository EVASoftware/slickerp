 package com.slicktechnologies.shared.common.productlayer;

import com.googlecode.objectify.annotation.Embed;
/**
 * Represents brand name of {@link SuperProduct}
 */
 @Embed
public class Brand 
{
	 /**************************************************Entity Attributes**********************************************/
	/** The name. */
	protected String name;
	
	/** The description. */
	protected String description;
	
	/** The status. */
	protected boolean status;
	
	
	/**************************************************Default Ctor**********************************************/
	/**
	 * Instantiates a new brand.
	 */
	public Brand() 
	{
		name="";
		description="";
	}
	
	/**************************************************Getter/Setter**********************************************/
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) 
	{
		if(name!=null)
			this.name = name.trim();
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/***************************************************************************************************************/
}
