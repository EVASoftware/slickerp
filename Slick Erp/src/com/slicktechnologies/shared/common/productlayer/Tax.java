package com.slicktechnologies.shared.common.productlayer;
import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

/**
 * Repersents any type of Tax
 */
@Embed
public class Tax implements Serializable 
{
	/**************************************************Entity Attributes**********************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 10000L;
	
	/** Name of Tax */
	protected String taxName;
	
	/** Tax Config Name */
	protected String taxConfigName;
	
	/** Percentage for tax */
	protected double percentage;
	
	/** Inlusive or exclusive */
	protected boolean inclusiveOrNot;
	
	
	
	/**
	 * rohan added this code for GST implementation On date : 23-06-2017
	 */
	
	protected String taxPrintName;  
	/**
	 * ends here 
	 */
	/**************************************************Default Ctor**********************************************/
	public Tax()
	{
		super();
		taxName="";
		taxConfigName="";
		taxPrintName="";
	}
	
	/**************************************************Getter/Setter**********************************************/
	
	/**
	 * Gets the tax name.
	 *
	 * @return the tax name
	 */
	public String getTaxName() {
		return taxName;
	}
	
	
	
	public String getTaxPrintName() {
		return taxPrintName;
	}

	public void setTaxPrintName(String taxPrintName) {
		this.taxPrintName = taxPrintName;
	}

	/**
	 * Sets the tax name.
	 *
	 * @param taxName the new tax name
	 */
	public void setTaxName(String taxName) 
	{
		if(taxName!=null)
			this.taxName = taxName.trim();
	}
	
	/**
	 * Gets the percentage.
	 *
	 * @return the percentage
	 */
	public double getPercentage() {
		return percentage;
	}
	
	/**
	 * Sets the percentage.
	 *
	 * @param percentage the new percentage
	 */
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	
	/**
	 * Checks if is inclusive.
	 *
	 * @return true, if is inclusive
	 */
	public boolean isInclusive() {
		return inclusiveOrNot;
	}
	
	/**
	 * Sets the inclusive.
	 *
	 * @param applicable the new inclusive
	 */
	public void setInclusive(boolean applicable) {
		this.inclusiveOrNot = applicable;
	}
	
	public String getTaxConfigName() {
		return taxConfigName;
	}

	public void setTaxConfigName(String taxConfigName) {
		if(taxConfigName!=null){
			this.taxConfigName = taxConfigName.trim();
		}
	}

	@Override
	public String toString() {
		return percentage+"";
	}

	public int compareTo(Tax lbtTax) {
		if(lbtTax==null)
			return 1;
		if(lbtTax.getPercentage()>this.percentage)
			return -1;
		else if(lbtTax.getPercentage()<this.percentage)
			return 1;
		
		return 0;
	}
	
	
	
	
	/*************************************************************************************************************************/

}
