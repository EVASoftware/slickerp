package com.slicktechnologies.shared.common.productlayer;
import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.EntitySubclass;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.shared.MyUtility;

// TODO: Auto-generated Javadoc
/**
 * Represents service product each product has duration and number of services.
 * class is designed such that duration's unit is undefined(it can be yearly, monthly,hourly)
 */
@EntitySubclass(index=true)
@Embed
public class ServiceProduct extends SuperProduct implements Serializable
{
	
	/** *********************************************Entity Attributes***************************************************. */
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1003665740505929790L;
	
	/** The no. of services  */
	protected int numberOfServices;
	
	/** The duration of services*/
	protected int duration;
	
	/** The start date. */
	//protected Date startDate;
	
	/** The service tax entity. */
	//protected Tax serviceTax;
	
	protected double serviceTime;
	
	/**The terms of treatment*/
	protected String termsoftreatment;
	
	/**
	 * Date : 24-04-2018 BY ANIL
	 * this variable is used when we are creating services from sales order
	 * For : HVAC/Rohan
	 */
	@Index
	boolean isStartDate;
	
	/**
	 * Date 06-06-2018 by Vijay
	 * Des :- for Service product warranty period 
	 * Requirement :- Neatedge Services
	 */
	protected int warrantyPeriod;
	/**
	 * ends here
	 */
	
	/**
	 * *********************************************Default Ctor***************************************************.
	 */
	/**
	 * Instantiates a new service product.
	 */
	public ServiceProduct()
	{
		super();
		//serviceTax=new Tax();
	}
	
	
	public boolean isStartDate() {
		return isStartDate;
	}


	public void setStartDate(boolean isStartDate) {
		this.isStartDate = isStartDate;
	}
	
	/**
	 * *********************************************Getter/Setter***************************************************.
	 *
	 * @return the duration
	 */
	/**
	 * Sets the no of service.
	 *
	 * @param noOfService the new no of service
	 */
	
	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 6, isFieldUpdater = false, isSortable = true, title = "Duration")
	public Integer getDuration() {
		return duration;
	}
	
	/**
	 * Sets the duration.
	 *
	 * @param duration the new duration
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * Gets the number of service.
	 *
	 * @return the number of service
	 */
	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 7, isFieldUpdater = false, isSortable = true, title = "No of Services")
	public Integer getNumberOfServices() {
		return numberOfServices;
	}

	/**
	 * Sets the number of services.
	 *
	 * @param numberOfService the new number of services
	 */
	public void setNumberOfServices(int numberOfService) {
		this.numberOfServices = numberOfService;
	}

	/**
	 * Gets the service tax.
	 *
	 * @return the service tax
	 */
/*	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = false, title = "Service Tax")
	public Tax getServiceTax() {
		return serviceTax;
	}*/

	/**
	 * Sets the service tax.
	 *
	 * @param serviceTax the new service tax
	 */
/*	public void setServiceTax(Tax serviceTax) {
		this.serviceTax = serviceTax;
	}*/
	
	/**
	 * ************************************************Relation Managment Part*********************************************.
	 */
	
	@OnSave
	@GwtIncompatible
	public void OnSave()
	{
		
			super.onSave();
			
		
	}
	
	/**
	 * On load.
	 */
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{
		super.onLoad();
	}


	public void setNumberOfService(Integer val1) {
		this.numberOfServices=val1;
		
	}


	public Double getServiceTime() {
		return serviceTime;
	}


	public void setServiceTime(Double serviceTime) {
		this.serviceTime = serviceTime;
	}


	public String getTermsoftreatment() {
		return termsoftreatment;
	}


	public void setTermsoftreatment(String termsoftreatment) {
		this.termsoftreatment = termsoftreatment;
	}
	
	
	/**********************************************************************************************************************/
	/**
	 *  nidhi
	 *   for create copy of seriveproduct entity
	 * @param serPro
	 */
	public void copyOfObject(ServiceProduct serPro){
		this.numberOfServices = serPro.numberOfServices;
		
		this.duration = serPro.duration;
		
		this.serviceTime = serPro.serviceTime;

		this.termsoftreatment = serPro.termsoftreatment;
		
		this.companyId = serPro.companyId;
		
		this.count = serPro.count;
		
		this.hsnNumber = serPro.hsnNumber;
		
		this.id = serPro.id;
		
		this.price = serPro.price;
		
		this.productCategory = serPro.productCategory;
		this.productCode = serPro.productCode;
		this.productGroup = serPro.productGroup;
		this.productClassification = serPro.productClassification;
		this.productName = serPro.productName;
		this.productType = serPro.productType;
		this.refNumber1 = serPro.refNumber1;
		this.refNumber2 = serPro.refNumber2;
		this.serviceTax = serPro.serviceTax;
		this.serviceTime = serPro.serviceTime;
		this.status = serPro.isStatus();
				
	}
	/**
	 *  end
	 */
	
	/**
	 * Date : 16-04-2018 By ANIL
	 * for showing product name in objectlistbox of service product
	 */
	@Override
	public String toString() {
		return getProductName();
	}
	
	
	public int getWarrantyPeriod() {
		return warrantyPeriod;
	}


	public void setWarrantyPeriod(int warrantyPeriod) {
		this.warrantyPeriod = warrantyPeriod;
	}
}





