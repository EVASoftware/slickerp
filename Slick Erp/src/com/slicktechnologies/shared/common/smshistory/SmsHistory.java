package com.slicktechnologies.shared.common.smshistory;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;

@Entity
public class SmsHistory extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6258628491604333021L;
	
	/**************************************Applicability Attributes************************************/
	
	@Index
	protected String documentType;
	@Index
	protected int documentId;
	@Index
	protected String name;
	@Index
	protected long cellNo;
	@Index
	protected String smsText;
	@Index
	protected Date transactionDate;
	@Index
	protected String transactionTime;
	@Index
	protected String userId;
	
	/*******************************************Constructor******************************************/
	
	public SmsHistory() {
		super();
		documentType="";
		name="";
		smsText="";
		transactionTime="";
		userId="";
	}
	
	/***************************************Getters And Setters****************************************/
	
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		if(documentType!=null){
			this.documentType = documentType.trim();
		}
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name!=null){
			this.name = name.trim();
		}
	}

	public long getCellNo() {
		return cellNo;
	}

	public void setCellNo(long cellNo) {
		this.cellNo = cellNo;
	}

	public String getSmsText() {
		return smsText;
	}

	public void setSmsText(String smsText) {
		if(smsText!=null){
			this.smsText = smsText.trim();
		}
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		if(transactionDate!=null){
			this.transactionDate = transactionDate;
		}
	}

	public String getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(String transactionTime) {
		if(transactionTime!=null){
			this.transactionTime = transactionTime;
		}
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		if(userId!=null){
			this.userId = userId.trim();
		}
	}
	
	/**************************************Business Logic******************************************/
	
	@OnSave
	@GwtIncompatible
	public void reactonSave(){
		if(id==null){
			setTransactionDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			setTransactionTime(DateUtility.getTimeWithTimeZone("IST", new Date()));
		}
	}


	/*******************************************Overridden Method**************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	
	

}
