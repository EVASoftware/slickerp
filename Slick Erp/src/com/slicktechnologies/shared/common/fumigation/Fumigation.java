package com.slicktechnologies.shared.common.fumigation;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.AddressComposite;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

@Entity
public class Fumigation extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2458188422716274978L;

	@Index
	protected String treatmentcardno;

	protected String quantitydeclare;
	
	protected String quantitydeclareUnit;

	protected String distiinguishingmarks;

	protected String invoiceNo;

	protected String nameofvessel;

	protected String declarepointentry;

	protected String descriptionofgoods;
	
	
	//  rohan added this for siddhivinay 
	
	protected String packingValue;
	
	protected String volumeofcontainer;
	
	protected String totalNetWeight;
	
	protected String grossWeight;

	// *******************************88888*****************
	@Index
	protected String shippingBillNo;

	
	@Index
	protected PersonInfo cInfo;
	
	protected Boolean Commoditybool;
	protected Boolean Packing;
	protected Boolean BothCommodityandPacking;
	protected Boolean StackUnderSheet;
	protected Boolean ContainerUnderSheet;
	protected Boolean PermanentChamber;
	protected Boolean PressureTestedContainer;

	@Index
	protected String importcountry; // for country of origin
	protected String consignmentlink;// for same in aus
	@Index
	protected String portncountryloading;// for same aus
	protected String countryofdestination; // country of destination

	protected String moredecl;

	protected String commodity; // for commodity
	@Index
	protected Date dateofissue;// date of issue in aus
	@Index
	protected String registrationno; // registration in aus
	@Index
	protected String certificateNo; // samein aus
    @Index
	protected String nameoffumigation; // same in aus
	protected Date dateoffumigation; // date of fumigation completed in aus
	protected String placeoffumigation; // palce of fumigation in aus

	protected String doseratefumigation; // aqis dose rate in aus
	protected String durartionfumigation; // exposure period in aus
	protected String minairtemp; // forecast min temp in aus
	protected String declaration; // same in aus
	protected String additionaldeclaration; // same in aus
	protected Address fromaddress; // address of exporter in aus
	protected Address tomaddress; // address of importer in aus
	@Index
	protected String fromCompanyname; // name of exporter in aus
	@Index
	protected String toCompanyname; // name of importer in aus
	protected Date pdate; // same in aus
	protected String place; // same in aus

	@Index
	protected Integer contractID;// same in aus
	@Index
	protected Integer serviceID;// same in aus
	protected Date contractEnddate;// same in aus
	protected Date contractStartdate;// same in aus
	@Index
	protected Date serviceDate; // same in aus
	@Index
	protected Date creationDate; // same in aus
	@Index
	protected boolean australia; // for identification purpose
	
	
	protected Date invoiceDate;
	protected Date shippingBillDate;
	
	

	protected String note1; // for taking value as yes or no
	// *******************************************

	protected String note2;
	protected String note3;
	protected String note4;
	protected String note11;
	protected String note12;
	protected String note13;
	protected String note14;
	protected String note15;
	protected String note123;
	
	protected String fumigationPerformed; //Add By jayshree
	protected String ventilated;

	// ************************for australia fields *******************

	

	
	String afasRegNo; // for aus only
	String aeiRegNO; // for aus only
	String appliedDoseRate; // for aus only
	String containerNo; // container no
	String ventilation;// for ventilation
	
	
	//************************************* ANIL 11/10/2015
	public boolean consignereditFlag=false;
	public boolean consigneeEditFlag=false;
	
	String consignerName;
	String consignerPocName;
	String consignerEmail;
	Long consignerCellNo;
	
	
	
	String consigneeName;
	String consigneePocName;
	String consigneeEmail;
	Long consigneeCellNo;
	
	/** date  7.4.2018 added by komal for new fumigation details**/
	protected Boolean unSheetedContainer;
	@Index
	protected int serialNumber;
	protected String importOrExport;
	protected String dtePPQSRegistrationNumber;
	protected Address branchAddress;
	protected String nameOfCommodity;
	/** Updated By: Viraj Date: 17-04-2019 Description: as per requirement of pest mortom **/
	protected String Quantity;
	@Index
	protected String branch;
	protected String humidity;
	protected String typeOfCargo;
	protected String descriptionOfCargo;
	protected Address notifiedPartyAddress;
	@Index
	protected String notifiedPartyCompanyname;
	protected String fumigationPerformedIn;
	protected String shippingBrandName;
	
	@Index
	protected String accreditedFumigator;
	protected String notifiedPartyName;
	protected String notifiedPartyPocName;
	protected String notifiedPartyEmail;
	protected Long notifiedPartyCellNo;
   boolean notifiedPartyEditFlag;
   @Index
   protected Date completionDate;
   @Index
   protected String numberRange;
	/**
	 * end komal
	**/
   
   protected String fromAddess;
   protected String notifiedAdd;
 

protected String duration;
/**
 * UPdated By: Viraj
 * Date: 13-04-2019
 * Description: To store additional declaration
 */
	String addDeclaration;

	
	
	/**
	 * UPdated By: Viraj
	 * Date: 13-04-2019
	 * Description: To store additional declaration
	 */
   public String getAddDeclaration() {
	   return addDeclaration;
	}
	
	public void setAddDeclaration(String addDeclaration) {
		this.addDeclaration = addDeclaration;
	}
/** Ends **/


public String getDuration() {
	return duration;
}



public void setDuration(String duration) {
	this.duration = duration;
}



public String getFromAddess() {
		return fromAddess;
	}



	public void setFromAddess(String fromAddess) {
		this.fromAddess = fromAddess;
	}



	public String getToAddress() {
		return toAddress;
	}



	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

protected String toAddress;
  /****/
	
	///////// *************************************************/

	
	
	public Fumigation() {
		creationDate = new Date();

		shippingBillNo="";
		invoiceNo="";
	}
	
	
	
	/****************************************** Getter and Setter ***************************************************/

	public boolean isAustralia() {
		return australia;
	}

	public void setAustralia(boolean australia) {
		this.australia = australia;
	}

	

	public String getTreatmentcardno() {
		return treatmentcardno;
	}

	public void setTreatmentcardno(String treatmentcardno) {
		this.treatmentcardno = treatmentcardno;
	}

	public Date getDateofissue() {
		return dateofissue;
	}

	public void setDateofissue(Date dateofissue) {
		this.dateofissue = dateofissue;
	}

	public String getRegistrationno() {
		return registrationno;
	}

	public void setRegistrationno(String registrationno) {
		this.registrationno = registrationno;
	}

	public String getImportcountry() {
		return importcountry;
	}

	public void setImportcountry(String importcountry) {
		this.importcountry = importcountry;
	}

	public String getDescriptionofgoods() {
		return descriptionofgoods;
	}

	public void setDescriptionofgoods(String descriptionofgoods) {
		this.descriptionofgoods = descriptionofgoods;
	}

	public String getQuantitydeclare() {
		return quantitydeclare;
	}

	public void setQuantitydeclare(String quantitydeclare) {
		this.quantitydeclare = quantitydeclare;
	}

	public String getDistiinguishingmarks() {
		return distiinguishingmarks;
	}

	public void setDistiinguishingmarks(String distiinguishingmarks) {
		this.distiinguishingmarks = distiinguishingmarks;
	}

	public String getConsignmentlink() {
		return consignmentlink;
	}

	public void setConsignmentlink(String consignmentlink) {
		this.consignmentlink = consignmentlink;
	}

	public String getPortncountryloading() {
		return portncountryloading;
	}

	public void setPortncountryloading(String portncountryloading) {
		this.portncountryloading = portncountryloading;
	}

	public String getNameofvessel() {
		return nameofvessel;
	}

	public void setNameofvessel(String nameofvessel) {
		this.nameofvessel = nameofvessel;
	}

	public String getCountryofdestination() {
		return countryofdestination;
	}

	public void setCountryofdestination(String countryofdestination) {
		this.countryofdestination = countryofdestination;
	}

	public String getDeclarepointentry() {
		return declarepointentry;
	}

	public void setDeclarepointentry(String declarepointentry) {
		this.declarepointentry = declarepointentry;
	}

	public Address getFromaddress() {
		return fromaddress;
	}

	public void setFromaddress(Address fromaddress) {
		this.fromaddress = fromaddress;
	}

	public Address getTomaddress() {
		return tomaddress;
	}

	public void setTomaddress(Address tomaddress) {
		this.tomaddress = tomaddress;
	}

	public String getNameoffumigation() {
		return nameoffumigation;
	}

	public void setNameoffumigation(String nameoffumigation) {
		this.nameoffumigation = nameoffumigation;
	}

	public Date getDateoffumigation() {
		return dateoffumigation;
	}

	public void setDateoffumigation(Date dateoffumigation) {
		this.dateoffumigation = dateoffumigation;
	}

	public String getPlaceoffumigation() {
		return placeoffumigation;
	}

	public void setPlaceoffumigation(String placeoffumigation) {
		this.placeoffumigation = placeoffumigation;
	}

	public String getDoseratefumigation() {
		return doseratefumigation;
	}

	public void setDoseratefumigation(String doseratefumigation) {
		this.doseratefumigation = doseratefumigation;
	}

	public String getDurartionfumigation() {
		return durartionfumigation;
	}

	public void setDurartionfumigation(String durartionfumigation) {
		this.durartionfumigation = durartionfumigation;
	}

	public String getMinairtemp() {
		return minairtemp;
	}

	public void setMinairtemp(String minairtemp) {
		this.minairtemp = minairtemp;
	}

	public String getAdditionaldeclaration() {
		return additionaldeclaration;
	}

	public void setAdditionaldeclaration(String additionaldeclaration) {
		this.additionaldeclaration = additionaldeclaration;
	}

	public Date getPdate() {
		return pdate;
	}

	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getFromCompanyname() {
		return fromCompanyname;
	}

	public void setFromCompanyname(String fromCompanyname) {
		this.fromCompanyname = fromCompanyname;
	}

	public String getToCompanyname() {
		return toCompanyname;
	}

	public void setToCompanyname(String toCompanyname) {
		this.toCompanyname = toCompanyname;
	}

	public Integer getContractID() {
		return contractID;
	}

	public void setContractID(Integer contractID) {
		this.contractID = contractID;
	}

	public Integer getServiceID() {
		return serviceID;
	}

	public void setServiceID(Integer serviceID) {
		this.serviceID = serviceID;
	}

	public Date getContractEnddate() {
		return contractEnddate;
	}

	public void setContractEnddate(Date contractEnddate) {
		this.contractEnddate = contractEnddate;
	}

	public Date getContractStartdate() {
		return contractStartdate;
	}

	public void setContractStartdate(Date contractStartdate) {
		this.contractStartdate = contractStartdate;
	}

	public String getNote1() {
		return note1;
	}

	public void setNote1(String note1) {
		this.note1 = note1;
	}

	public String getNote2() {
		return note2;
	}

	public void setNote2(String note2) {
		this.note2 = note2;
	}

	public String getNote3() {
		return note3;
	}

	public void setNote3(String note3) {
		this.note3 = note3;
	}

	public String getNote4() {
		return note4;
	}

	public void setNote4(String note4) {
		this.note4 = note4;
	}

	public String getNote11() {
		return note11;
	}

	public void setNote11(String note11) {
		this.note11 = note11;
	}

	public String getNote12() {
		return note12;
	}

	public void setNote12(String note12) {
		this.note12 = note12;
	}

	public String getNote13() {
		return note13;
	}

	public void setNote13(String note13) {
		this.note13 = note13;
	}

	public String getNote14() {
		return note14;
	}

	public void setNote14(String note14) {
		this.note14 = note14;
	}

	public String getNote15() {
		return note15;
	}

	public void setNote15(String note15) {
		this.note15 = note15;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public String getCertificateNo() {
		return certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getAfasRegNo() {
		return afasRegNo;
	}

	public void setAfasRegNo(String afasRegNo) {
		this.afasRegNo = afasRegNo;
	}

	public String getAeiRegNO() {
		return aeiRegNO;
	}

	public void setAeiRegNO(String aeiRegNO) {
		this.aeiRegNO = aeiRegNO;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getVentilation() {
		return ventilation;
	}

	public void setVentilation(String ventilation) {
		this.ventilation = ventilation;
	}

	public void setCreationDate(Date creationDate) {

		if (creationDate != null) {

			this.creationDate = creationDate;
		}

	}

	public String getAppliedDoseRate() {
		return appliedDoseRate;
	}

	public void setAppliedDoseRate(String appliedDoseRate) {
		this.appliedDoseRate = appliedDoseRate;
	}

	public String getNote123() {
		return note123;
	}

	public void setNote123(String note123) {
		this.note123 = note123;
	}

	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	public String getMoredecl() {
		return moredecl;
	}

	public void setMoredecl(String moredecl) {
		this.moredecl = moredecl;
	}

	public Boolean getPacking() {
		return Packing;
	}

	public void setPacking(Boolean packing) {
		Packing = packing;
	}

	public Boolean getBothCommodityandPacking() {
		return BothCommodityandPacking;
	}

	public void setBothCommodityandPacking(Boolean bothCommodityandPacking) {
		BothCommodityandPacking = bothCommodityandPacking;
	}

	public Boolean getStackUnderSheet() {
		return StackUnderSheet;
	}

	public void setStackUnderSheet(Boolean stackUnderSheet) {
		StackUnderSheet = stackUnderSheet;
	}

	public Boolean getContainerUnderSheet() {
		return ContainerUnderSheet;
	}

	public void setContainerUnderSheet(Boolean containerUnderSheet) {
		ContainerUnderSheet = containerUnderSheet;
	}

	public Boolean getPermanentChamber() {
		return PermanentChamber;
	}

	public void setPermanentChamber(Boolean permanentChamber) {
		PermanentChamber = permanentChamber;
	}

	public Boolean getPressureTestedContainer() {
		return PressureTestedContainer;
	}

	public void setPressureTestedContainer(Boolean pressureTestedContainer) {
		PressureTestedContainer = pressureTestedContainer;
	}

	public Boolean getCommoditybool() {
		return Commoditybool;
	}

	public void setCommoditybool(Boolean commoditybool) {
		Commoditybool = commoditybool;
	}

	public String getDeclaration() {
		return declaration;
	}

	public void setDeclaration(String declaration) {
		this.declaration = declaration;
	}

	@OnSave
	@GwtIncompatible
	public void savecreationdate() {
		if (id == null) {
			setCreationDate(DateUtility.getDateWithTimeZone("IST", new Date()));
		}
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getShippingBillNo() {
		return shippingBillNo;
	}

	public void setShippingBillNo(String shippingBillNo) {
		this.shippingBillNo = shippingBillNo;
	}

	
	/********************************************************************************/
	
	
	
	@OnSave
	@GwtIncompatible
	private void saveFromCompanyname() {
		
		if(consignereditFlag==false)
		{
			
		System.out.println("in side  consignerName false condition ");	
		
		if (consignerName!=null) {
//			if (this.getFromCompanyname() != null) {
				Customer cust = new Customer();
				GenricServiceImpl gsimpl = new GenricServiceImpl();

				// cust.setFullname(this.getFromCompanyname());
				cust.setCompany(true);
				cust.setCompanyName(consignerName);
				if(consignerPocName!=null){
					cust.setFirstName(consignerPocName);
					cust.setLastName("");
				}
				if(consignerEmail!=null){
					cust.setEmail(consignerEmail);
				}
				if(consignerCellNo!=null){
					cust.setCellNumber1(consignerCellNo);
				}
				cust.setNewCustomerFlag(true);

				if (this.getFromaddress() != null) {
					cust.setAdress(this.getFromaddress());
				}
				cust.setCompanyId(this.getCompanyId());
				gsimpl.save(cust);
//			}
		}
		}
		else
		{
			
			System.out.println("in side flage true condition ");	
			
			Customer cust;
			{
			  if(getCompanyId()==null)
				  cust= ofy().load().type(Customer.class).filter("companyName",this.getToCompanyname().trim()).first().now();
			  else
				  cust= ofy().load().type(Customer.class).filter("companyName",this.getToCompanyname().trim()).filter("companyId", getCompanyId()).first().now();
			
			   if(cust!=null)
			   {
				  
				   cust.setCompany(true);
					cust.setCompanyName(consignerName);
					if(consignerPocName!=null){
						cust.setFirstName(consignerPocName);
						cust.setLastName("");
					}
					if(consignerEmail!=null){
						cust.setEmail(consignerEmail);
					}
					if(consignerCellNo!=null){
						cust.setCellNumber1(consignerCellNo);
					}
					cust.setNewCustomerFlag(true);

					if (this.getFromaddress() != null) {
						cust.setAdress(this.getFromaddress());
					}
					cust.setCompanyId(this.getCompanyId());
				   
				   ofy().save().entity(cust).now();
			   }
			}
		}

	}

	@OnSave
	@GwtIncompatible
	private void saveToCompanyname() {

//		if (id == null) {

		if(consigneeEditFlag==false)
		{
		
			System.out.println("consigneeName in side false condition ");
			if (consigneeName != null) {
				Customer cust = new Customer();
				GenricServiceImpl gsimpl = new GenricServiceImpl();

				// cust.setFullname(this.getToCompanyname());
				cust.setCompany(true);
				cust.setCompanyName(consigneeName);
				if(consigneePocName!=null){
					cust.setFirstName(consigneePocName);
					cust.setLastName("");
				}
				if(consigneeEmail!=null){
					cust.setEmail(consigneeEmail);
				}
				if(consigneeCellNo!=null){
					cust.setCellNumber1(consigneeCellNo);
				}
				cust.setNewCustomerFlag(true);
				if (this.getTomaddress() != null) {
					cust.setAdress(this.getTomaddress());
				}
				cust.setCompanyId(this.getCompanyId());
				gsimpl.save(cust);
			}
		}
		else
		{
			Customer cust;
			{
			  if(getCompanyId()==null)
				  cust= ofy().load().type(Customer.class).filter("companyName",this.getToCompanyname().trim()).first().now();
			  else
				  cust= ofy().load().type(Customer.class).filter("companyName",this.getToCompanyname().trim()).filter("companyId", getCompanyId()).first().now();
			
			   if(cust!=null)
			   {
				   cust.setCompany(true);
					cust.setCompanyName(consignerName);
					if(consignerPocName!=null){
						cust.setFirstName(consignerPocName);
						cust.setLastName("");
					}
					if(consignerEmail!=null){
						cust.setEmail(consignerEmail);
					}
					if(consignerCellNo!=null){
						cust.setCellNumber1(consignerCellNo);
					}
					cust.setNewCustomerFlag(true);

					if (this.getFromaddress() != null) {
						cust.setAdress(this.getFromaddress());
					}
					cust.setCompanyId(this.getCompanyId());
				   
				   ofy().save().entity(cust).now();
			   }
		}
		}
		
//		}
	}

	public String getConsignerName() {
		return consignerName;
	}

	public void setConsignerName(String consignerName) {
		this.consignerName = consignerName;
	}

	public String getConsignerPocName() {
		return consignerPocName;
	}

	public void setConsignerPocName(String consignerPocName) {
		this.consignerPocName = consignerPocName;
	}

	public String getConsignerEmail() {
		return consignerEmail;
	}

	public void setConsignerEmail(String consignerEmail) {
		this.consignerEmail = consignerEmail;
	}

	public Long getConsignerCellNo() {
		return consignerCellNo;
	}

	public void setConsignerCellNo(Long consignerCellNo) {
		this.consignerCellNo = consignerCellNo;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneePocName() {
		return consigneePocName;
	}

	public void setConsigneePocName(String consigneePocName) {
		this.consigneePocName = consigneePocName;
	}

	public String getConsigneeEmail() {
		return consigneeEmail;
	}

	public void setConsigneeEmail(String consigneeEmail) {
		this.consigneeEmail = consigneeEmail;
	}

	public Long getConsigneeCellNo() {
		return consigneeCellNo;
	}

	public void setConsigneeCellNo(Long consigneeCellNo) {
		this.consigneeCellNo = consigneeCellNo;
	}



	public Date getInvoiceDate() {
		return invoiceDate;
	}



	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}



	public Date getShippingBillDate() {
		return shippingBillDate;
	}



	public void setShippingBillDate(Date shippingBillDate) {
		this.shippingBillDate = shippingBillDate;
	}



	public String getQuantitydeclareUnit() {
		return quantitydeclareUnit;
	}



	public void setQuantitydeclareUnit(String quantitydeclareUnit) {
		this.quantitydeclareUnit = quantitydeclareUnit;
	}



	public boolean isConsignereditFlag() {
		return consignereditFlag;
	}



	public void setConsignereditFlag(boolean consignereditFlag) {
		this.consignereditFlag = consignereditFlag;
	}



	public boolean isConsigneeEditFlag() {
		return consigneeEditFlag;
	}



	public void setConsigneeEditFlag(boolean consigneeEditFlag) {
		this.consigneeEditFlag = consigneeEditFlag;
	}



	public PersonInfo getcInfo() {
		return cInfo;
	}



	public void setcInfo(PersonInfo cInfo) {
		this.cInfo = cInfo;
	}



	public String getVolumeofcontainer() {
		return volumeofcontainer;
	}



	public void setVolumeofcontainer(String volumeofcontainer) {
		this.volumeofcontainer = volumeofcontainer;
	}



	public String getTotalNetWeight() {
		return totalNetWeight;
	}



	public void setTotalNetWeight(String totalNetWeight) {
		this.totalNetWeight = totalNetWeight;
	}



	public String getPackingValue() {
		return packingValue;
	}



	public void setPackingValue(String packingValue) {
		this.packingValue = packingValue;
	}



	public String getGrossWeight() {
		return grossWeight;
	}



	public void setGrossWeight(String grossWeight) {
		this.grossWeight = grossWeight;
	}



	public Boolean getUnSheetedContainer() {
		return unSheetedContainer;
	}



	public void setUnSheetedContainer(Boolean unSheetedContainer) {
		this.unSheetedContainer = unSheetedContainer;
	}



	public int getSerialNumber() {
		return serialNumber;
	}



	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}



	public String getImportOrExport() {
		return importOrExport;
	}



	public void setImportOrExport(String importOrExport) {
		this.importOrExport = importOrExport;
	}



	public String getDtePPQSRegistrationNumber() {
		return dtePPQSRegistrationNumber;
	}



	public void setDtePPQSRegistrationNumber(String dtePPQSRegistrationNumber) {
		this.dtePPQSRegistrationNumber = dtePPQSRegistrationNumber;
	}



	public Address getBranchAddress() {
		return branchAddress;
	}



	public void setBranchAddress(Address branchAddress) {
		this.branchAddress = branchAddress;
	}



	public String getNameOfCommodity() {
		return nameOfCommodity;
	}



	public void setNameOfCommodity(String nameOfCommodity) {
		this.nameOfCommodity = nameOfCommodity;
	}



	public String getQuantity() {
		return Quantity;
	}



	public void setQuantity(String quantity) {
		Quantity = quantity;
	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}



	public String getHumidity() {
		return humidity;
	}



	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}



	public String getTypeOfCargo() {
		return typeOfCargo;
	}



	public void setTypeOfCargo(String typeOfCargo) {
		this.typeOfCargo = typeOfCargo;
	}



	public String getDescriptionOfCargo() {
		return descriptionOfCargo;
	}



	public void setDescriptionOfCargo(String descriptionOfCargo) {
		this.descriptionOfCargo = descriptionOfCargo;
	}



	public Address getNotifiedPartyAddress() {
		return notifiedPartyAddress;
	}



	public void setNotifiedPartyAddress(Address notifiedPartyAddress) {
		this.notifiedPartyAddress = notifiedPartyAddress;
	}



	public String getNotifiedPartyCompanyname() {
		return notifiedPartyCompanyname;
	}



	public void setNotifiedPartyCompanyname(String notifiedPartyCompanyname) {
		this.notifiedPartyCompanyname = notifiedPartyCompanyname;
	}



	public String getFumigationPerformedIn() {
		return fumigationPerformedIn;
	}



	public void setFumigationPerformedIn(String fumigationPerformedIn) {
		this.fumigationPerformedIn = fumigationPerformedIn;
	}



	public String getShippingBrandName() {
		return shippingBrandName;
	}



	public void setShippingBrandName(String shippingBrandName) {
		this.shippingBrandName = shippingBrandName;
	}



	public String getAccreditedFumigator() {
		return accreditedFumigator;
	}



	public void setAccreditedFumigator(String accreditedFumigator) {
		this.accreditedFumigator = accreditedFumigator;
	}



	public String getNotifiedPartyName() {
		return notifiedPartyName;
	}



	public void setNotifiedPartyName(String notifiedPartyName) {
		this.notifiedPartyName = notifiedPartyName;
	}



	public String getNotifiedPartyPocName() {
		return notifiedPartyPocName;
	}



	public void setNotifiedPartyPocName(String notifiedPartyPocName) {
		this.notifiedPartyPocName = notifiedPartyPocName;
	}



	public String getNotifiedPartyEmail() {
		return notifiedPartyEmail;
	}



	public void setNotifiedPartyEmail(String notifiedPartyEmail) {
		this.notifiedPartyEmail = notifiedPartyEmail;
	}



	public Long getNotifiedPartyCellNo() {
		return notifiedPartyCellNo;
	}



	public void setNotifiedPartyCellNo(Long notifiedPartyCellNo) {
		this.notifiedPartyCellNo = notifiedPartyCellNo;
	}



	public boolean isNotifiedPartyEditFlag() {
		return notifiedPartyEditFlag;
	}



	public void setNotifiedPartyEditFlag(boolean notifiedPartyEditFlag) {
		this.notifiedPartyEditFlag = notifiedPartyEditFlag;
	}



	public Date getCompletionDate() {
		return completionDate;
	}



	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}
	
	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}
	
	/**
	 * Add By Jayshree
	 * @return
	 */
	
	public String getFumigationPerformed() {
		return fumigationPerformed;
	}



	public void setFumigationPerformed(String fumigationPerformed) {
		this.fumigationPerformed = fumigationPerformed;
	}
	
	public String getVentilated() {
		return ventilated;
	}



	public void setVentilated(String ventilated) {
		this.ventilated = ventilated;
	}

	
	  public String getNotifiedAdd() {
			return notifiedAdd;
		}



		public void setNotifiedAdd(String notifiedAdd) {
			this.notifiedAdd = notifiedAdd;
		}

	//komal
	@OnSave
	@GwtIncompatible
	private void saveNotifiedPartyname() {
		
		if(notifiedPartyEditFlag==false)
		{
			
		System.out.println("in side  notified party Name false condition ");	
		
		if (notifiedPartyName!=null) {
//			if (this.getFromCompanyname() != null) {
				Customer cust = new Customer();
				GenricServiceImpl gsimpl = new GenricServiceImpl();

				// cust.setFullname(this.getFromCompanyname());
				cust.setCompany(true);
				cust.setCompanyName(notifiedPartyName);
				if(notifiedPartyPocName!=null){
					cust.setFirstName(notifiedPartyPocName);
					cust.setLastName("");
				}
				if(notifiedPartyEmail!=null){
					cust.setEmail(notifiedPartyEmail);
				}
				if(notifiedPartyCellNo!=null){
					cust.setCellNumber1(notifiedPartyCellNo);
				}
				cust.setNewCustomerFlag(true);
				
				//Comment by jayshree
				
//				if (this.getNotifiedPartyAddress() != null) {
//					cust.setAdress(this.getNotifiedPartyAddress());
//				}
				
				if (this.notifiedPartyAddress!= null) {
					cust.setAdress(this.notifiedPartyAddress);
				}
				cust.setCompanyId(this.getCompanyId());
				gsimpl.save(cust);
//			}
		}
		}
		else
		{
			
			System.out.println("in side flage true condition ");	
			
			Customer cust;
			{
			  if(getCompanyId()==null)
				  cust= ofy().load().type(Customer.class).filter("companyName",this.getNotifiedPartyCompanyname().trim()).first().now();
			  else
				  cust= ofy().load().type(Customer.class).filter("companyName",this.getNotifiedPartyCompanyname().trim()).filter("companyId", getCompanyId()).first().now();
			
			   if(cust!=null)
			   {
				  
				   cust.setCompany(true);
					cust.setCompanyName(notifiedPartyName);
					if(notifiedPartyPocName!=null){
						cust.setFirstName(notifiedPartyPocName);
						cust.setLastName("");
					}
					if(notifiedPartyEmail!=null){
						cust.setEmail(notifiedPartyEmail);
					}
					if(notifiedPartyCellNo!=null){
						cust.setCellNumber1(notifiedPartyCellNo);
					}
					cust.setNewCustomerFlag(true);

					if (this.getNotifiedPartyAddress() != null) {
						cust.setAdress(this.getNotifiedPartyAddress());
					}
					cust.setCompanyId(this.getCompanyId());
				   
				   ofy().save().entity(cust).now();
			   }
			}
		}

	}

	@GwtIncompatible
	@OnSave
	private void setCertificateNumber(){
		System.out.println("certificate number: "+this.getCertificateNo());
		/**
		 * Updated By: Viraj
		 * Date: 17-03-2019
		 * Description: Since certificate No was not getting created had to change if condition
		 */
		if(this.getCertificateNo()==null || this.getCertificateNo().equals("")){
		NumberGeneration ng = ofy().load().type(NumberGeneration.class).filter("companyId",companyId)
				.filter("processName", this.getNumberRange()).filter("status", true).first().now();
		long number = ng.getNumber() + 1;
		ng.setNumber(number);
		System.out.println("number: "+number);
    	ofy().save().entity(ng).now();
    	
    	long lastNumber = number % 10000; 
    	//String certificateNumber = "";
    	String name = "";
    	if(this.getNameoffumigation().equalsIgnoreCase("Methyle Bromide") && !this.isAustralia()){
    		name = "MB";
    	}else if(this.getNameoffumigation().equalsIgnoreCase("Aluminium Phosphide") && !this.isAustralia()){
    		name = "ALP";
    	}else{
    		name = "AFAS";
    	}
    	System.out.println("name: "+name);
    	/**
    	 * Updated By: Viraj
    	 * Date: 12-01-2019
    	 * Description: to add branch short name if any
    	 */
    	String b = "";
    	Branch branch = ofy().load().type(Branch.class).filter("companyId",companyId)
				.filter("buisnessUnitName", this.getBranch()).first().now();
    	if(branch != null) {
    		if(branch.getShortName() != null && branch.getShortName() != "") {
        		b = branch.getShortName();
        	} else {
        		b = ""+this.getBranch().substring(0, 3);
        	}
    	}else {
    		b = ""+this.getBranch().substring(0, 3);
    	}
    	System.out.println("branch: "+b);
    	/** Ends **/
    	
		String certificateNumber = "PMI/"+b+"/"+name+"/";
		certificateNumber += lastNumber+"/";
		System.out.println("certificateNumber wid lastno: "+certificateNumber);
		ServerAppUtility serverApp = new ServerAppUtility();
		String year;
		try {
			year = serverApp.getFinancialYearOfDate(new Date());
			certificateNumber += year;
			System.out.println("certificateNumber wid year: "+certificateNumber);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setCertificateNo(certificateNumber);
		//tbCertificateNo.setValue(certificateNumber);
	}
}

	
}
