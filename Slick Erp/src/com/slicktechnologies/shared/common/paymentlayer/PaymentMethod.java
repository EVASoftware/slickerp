package com.slicktechnologies.shared.common.paymentlayer;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * The Interface PaymentMethod.
 */
public interface PaymentMethod extends Serializable{
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	double getAmount();

}
