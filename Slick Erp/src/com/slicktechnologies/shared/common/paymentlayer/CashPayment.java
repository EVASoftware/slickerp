package com.slicktechnologies.shared.common.paymentlayer;

import com.googlecode.objectify.annotation.Embed;

// TODO: Auto-generated Javadoc
/**
 * The Class CashPayment. Represents the payment method done with cash.
 */
@Embed
public class CashPayment implements PaymentMethod
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4907068920941760971L;
	/***********************************************Entity Attributes****************************************************/
	/** The amount. */
	protected double amount;

	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new cash payment.
	 */
	public CashPayment()
	{
		
	}
	
	
	/***********************************************Getter/Setter****************************************************/
	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.paymentlayer.PaymentMethod#getAmount()
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(Double amount) {
		if(amount!=null)
		   this.amount = amount;
	}

	/**
	 * Instantiates a new cash payment.
	 *
	 * @param amount the amount
	 */
	public CashPayment(double amount) {
		super();
		this.amount = amount;
	}

}
