package com.slicktechnologies.shared.common.paymentlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class PaymentMethodProxy implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2971088414510608782L;
	private CardPayment cardPayment;
	private CashPayment cashPayment;
	private ChequePayment chequePayment;
	
	
	public PaymentMethodProxy() {
		super();
		cardPayment=new CardPayment();
		cashPayment=new CashPayment();
		chequePayment=new ChequePayment();
	}


	public String getCardType() {
		return cardPayment.getcardType();
	}


	public void setCardType(String cardtype) {
		cardPayment.setcardType(cardtype);
	}


	public String getCardNo() {
		return cardPayment.getcardNo();
	}


	public void setCardNo(String cardno) {
		cardPayment.setcardNo(cardno);
	}
	
	public Double getCardAmount() {
		return cardPayment.getAmount();
	}


	public void setCardAmount(Double amount) {
		cardPayment.setamount(amount);
	}


	

	public String getbankName() {
		return chequePayment.getbankName();
	}


	public void setbankName(String bankname) {
		chequePayment.setbankName(bankname);
	}


	public String getbranch() {
		return chequePayment.getbranch();
	}


	public void setbranch(String branch) {
		chequePayment.setbranch(branch);
	}


	public void setchequeNo(String chno) {
		chequePayment.setchequeNo(chno);
	}


	public String getChequeNo() {
		return chequePayment.getChequeNo();
	}


	public void setchqDate(Date chdate) {
		chequePayment.setchqDate(chdate);
	}


	public Date getChqDate() {
		return chequePayment.getChqDate();
	}

	
	public void setChqAmount(Double amount)
	{
		this.chequePayment.setchqAmount(amount);
	}
	
	public Double getChqAmount()
	{
		return this.chequePayment.getAmount();
	}
	
	public void setCashAmount(Double amount)
	{
		this.cashPayment.setAmount(amount);
	}
	
	public Double getCashAmount()
	{
		return this.cashPayment.getAmount();
	}

	public Double getAmount()
	{
		return this.cardPayment.getAmount()+this.chequePayment.getAmount()+this.cashPayment.getAmount();
	}
	
	public void setAmount(Double Amount)
	{
		this.setAmount(Amount);
	}
	
}
