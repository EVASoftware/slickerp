package com.slicktechnologies.shared.common.paymentlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
// TODO: Auto-generated Javadoc

/**
 * The Class PaymentTerms.
 */
@Embed
public class PaymentTerms implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 599107785434412925L;

	/** The pay termdays. */
	protected int payTermDays;
	
	/** The pay term percent. */
	protected double payTermPercent;
	
	/** The pay term comment. */
	protected String payTermComment;
	
	protected String branch;
	
	/**
	 * Date : 08-08-2017 By ANIL
	 */
	protected Date paymentDate;
	
	/**
	 * End
	 */
	
	
	
	
	
	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	
	

	/**
	 * Gets the pay term days.
	 *
	 * @return the pay term days
	 */
	public Integer getPayTermDays() {
		return payTermDays;
	}

	/**
	 * Sets the pay term days.
	 *
	 * @param payTermDays the new pay term days
	 */
	public void setPayTermDays(Integer payTermDays) {
		this.payTermDays = payTermDays;
	}

	/**
	 * Gets the pay term percent.
	 *
	 * @return the pay term percent
	 */
	public Double getPayTermPercent() {
		return payTermPercent;
	}
	
	/**
	 * Sets the pay term percent.
	 *
	 * @param payTermPercent the new pay term percent
	 */
	public void setPayTermPercent(Double payTermPercent) {
		this.payTermPercent = payTermPercent;
	}
	
	/**
	 * Gets the pay term comment.
	 *
	 * @return the pay term comment
	 */
	public String getPayTermComment() {
		return payTermComment;
	}
	
	/**
	 * Sets the pay term comment.
	 *
	 * @param payTermComment the new pay term comment
	 */
	public void setPayTermComment(String payTermComment) {
		if(payTermComment!=null)
			this.payTermComment = payTermComment.trim();
		
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	
	
	
	

}
