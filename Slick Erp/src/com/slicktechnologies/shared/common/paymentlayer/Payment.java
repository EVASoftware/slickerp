package com.slicktechnologies.shared.common.paymentlayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;


// TODO: Auto-generated Javadoc
/**
 * The Class Payment. Represents Payment. Date of payment & the list through which the payment is made.
 */
@Embed
public class Payment implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7385443729254227388L;

	/***********************************************Entity Attributes****************************************************/
	
	
	/** The date. */
	protected Date date;
	
	/** The payment methods. */
	protected PaymentMethodProxy paymentMethods;
	
	/** Represents an unique payment*/
	Long id;
	
	/***********************************************Default Ctor****************************************************/
	
	
	
	/**
	 * Instantiates a new payment.
	 */
	public Payment() 
	 {
		paymentMethods=new PaymentMethodProxy();
		date=new Date();
	 }

	/***********************************************Getter/Setter****************************************************/
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() 
	{
		return date;
	}
	
/**
 * Instantiates a new payment.
 *
 * @param date the date
 * @param paymentMethods the payment methods
 */
public Payment(Date date, PaymentMethodProxy paymentMethods) {
		super();
		this.date = date;
		this.paymentMethods = paymentMethods;
		this.id=new Date().getTime();
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

 /**
  * Gets the payment methods.
  *
  * @return the payment methods
  */
 public PaymentMethodProxy getPaymentMethods() {
		return paymentMethods;
	}

/**
 * Sets the payment methods.
 *
 * @param paymentMethods the new payment methods
 */
public void setPaymentMethods(PaymentMethodProxy paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

/**
 * Gets the total amount.
 *
 * @return the total amount
 */
public double getTotalAmount()
	{
		return this.paymentMethods.getAmount();
	}

public String getCardType() {
	return paymentMethods.getCardType();
}

public void setCardType(String cardtype) {
	paymentMethods.setCardType(cardtype);
}

public String getCardNo() {
	return paymentMethods.getCardNo();
}

public void setCardNo(String cardno) {
	paymentMethods.setCardNo(cardno);
}

public Double getCardAmount() {
	return paymentMethods.getCardAmount();
}

public void setCardAmount(Double amount) {
	paymentMethods.setCardAmount(amount);
}

public String getbankName() {
	return paymentMethods.getbankName();
}

public void setbankName(String bankname) {
	paymentMethods.setbankName(bankname);
}

public String getbranch() {
	return paymentMethods.getbranch();
}

public void setbranch(String branch) {
	paymentMethods.setbranch(branch);
}

public void setchequeNo(String chno) {
	paymentMethods.setchequeNo(chno);
}

public String getChequeNo() {
	return paymentMethods.getChequeNo();
}

public void setchqDate(Date chdate) {
	paymentMethods.setchqDate(chdate);
}

public Date getChqDate() {
	return paymentMethods.getChqDate();
}

public void setChqAmount(Double amount) {
	paymentMethods.setChqAmount(amount);
}

public Double getChqAmount() {
	return paymentMethods.getChqAmount();
}

public void setCashAmount(Double amount) {
	paymentMethods.setCashAmount(amount);
}

public Double getCashAmount() {
	return paymentMethods.getCashAmount();
}

public Object getId() {
	// TODO Auto-generated method stub
	return id;
}






/******************************************************************************************************************/
}
