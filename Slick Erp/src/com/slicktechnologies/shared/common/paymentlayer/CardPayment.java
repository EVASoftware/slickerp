package com.slicktechnologies.shared.common.paymentlayer;

import com.googlecode.objectify.annotation.Embed;
// TODO: Auto-generated Javadoc

/**
 * The Class CardPayment. Represents the payment done with card.
 */
@Embed
public class CardPayment implements PaymentMethod
{
	
	/** *********************************************Entity Attributes***************************************************. */
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7697806109420829529L;

	/** The card type. */
	protected String cardType;

	/** The card no. */
	protected String cardNo;

	/** The amount. */
	protected double amount;

	
	/**
	 * *********************************************Default Ctor***************************************************.
	 */
	/**
	 * Instantiates a new card entity.
	 */
	public CardPayment()
	{
		cardType="";
		cardNo="";
	}
	
	/**
	 * ********************************************Getter/Setter****************************************************.
	 *
	 * @return the card type
	 */

	/**
	 * Gets the card type.
	 *
	 * @return the card type
	 */
	public String getcardType()
	{
		return cardType;
	}

	/**
	 * Sets the card type.
	 *
	 * @param cardtype the new card type
	 */
	public void setcardType(String cardtype)
	{
		if(cardtype!=null)
			this.cardType=cardtype.trim();
	}

	/**
	 * Gets the card no.
	 *
	 * @return the card no
	 */
	public String getcardNo()
	{
		return cardNo;
	}

	/**
	 * Sets the card no.
	 *
	 * @param cardno the new card no
	 */
	public void setcardNo(String cardno)
	{
		if(cardno!=null)	
			this.cardNo=cardno.trim();
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setamount(Double amount)
	{
		if(amount!=null)  
			this.amount=amount;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.paymentlayer.PaymentMethod#getAmount()
	 */
	@Override
	public double getAmount() {
		
		return amount;
	}
	
	/**********************************************************************************************************/
}
