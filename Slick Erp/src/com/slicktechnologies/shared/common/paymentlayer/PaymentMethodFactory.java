package com.slicktechnologies.shared.common.paymentlayer;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating PaymentMethod objects.
 */
public class PaymentMethodFactory
{
    
    /** The Constant CASH REPERSENTS CASH PAYMENT */
    public static final int CASH=1;
    
    /** The Constant CHEQUE REPERSENTS CHEQUE PAYMENT */
    public static final int CHEQUE=2;
    
    /** The Constant CARD PAYMENT*/
    public static final int CARD=3;
	
	/**
	 * Gets the payment method.
	 *
	 * @param type the type
	 * @return the payment method
	 */
	public static PaymentMethod  getPaymentMethod(int type)
        {
        	if(type==CASH)
        	{
        		return new CashPayment();
        	}
        	
        	if(type==CHEQUE)
        	{
        		return new ChequePayment();
        	}
        	
        	if(type==CARD)
        	{
        		return new CardPayment();
        	}
        	return null;
        }
}
