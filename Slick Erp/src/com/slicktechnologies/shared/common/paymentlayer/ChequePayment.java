package com.slicktechnologies.shared.common.paymentlayer;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

// TODO: Auto-generated Javadoc
/**
 * The Class Cheque payment represents the payment done with cheque.
 */
@Embed
public class ChequePayment implements PaymentMethod
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1129737516054897123L;

	/** The bank name. */
	protected String bankName;

	/** The branch. */
	protected String bankBranch;

	/** The chqno. */
	protected String chequeNumber;

	/** The chqamount. */
	protected double chequeAmount;

	/** The chqdate. */
	protected Date chequeDate;

	
	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new bank entity.
	 */
	public ChequePayment()
	{
		bankName="";
		bankBranch="";
		chequeNumber="";


	}

	/***********************************************Getter/Setter****************************************************/
	/**
	 * Gets the bank name.
	 *
	 * @return the bank name
	 */
	public String getbankName()
	{
		return bankName;
	}

	/**
	 * Sets the bank name.
	 *
	 * @param bankname the new bank name
	 */
	public void setbankName(String bankname)
	{
		if(bankname!=null)
			this.bankName=bankname.trim();
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	public String getbranch()
	{
		return bankBranch;
	}

	/**
	 * Sets the branch.
	 *
	 * @param branch the new branch
	 */
	public void setbranch(String branch)
	{
		if(branch!=null)
			this.bankBranch=branch.trim();
	}


	/**
	 * Sets the cheque no.
	 *
	 * @param chno the new cheque no
	 */
	public void setchequeNo(String chno) {
		if(chno!=null)
			this.chequeNumber=chno.trim();
	}


	/**
	 * Gets the cheque no.
	 *
	 * @return the cheque no
	 */
	public String getChequeNo() {

		return chequeNumber;
	}


	/**
	 * Sets the chq date.
	 *
	 * @param chdate the new chq date
	 */
	public void setchqDate(Date chdate) {

		if(chdate!=null)
			this.chequeDate=chdate;
	}


	/**
	 * Gets the chq date.
	 *
	 * @return the chq date
	 */
	public Date getChqDate() {

		return chequeDate;
	}


	/**
	 * Sets the chq amount.
	 *
	 * @param amount the new chq amount
	 */
	public void setchqAmount(Double amount) {
		if(amount!=null)
			this.chequeAmount=amount;

	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.paymentlayer.PaymentMethod#getAmount()
	 */
	@Override
	public double getAmount() {
		// TODO Auto-generated method stub
		return chequeAmount;
	}

	/**************************************************************************************************************************/
}
