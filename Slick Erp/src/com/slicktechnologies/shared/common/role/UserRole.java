package com.slicktechnologies.shared.common.role;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.User;

import static com.googlecode.objectify.ObjectifyService.ofy;



@Entity
@Embed
public class UserRole extends SuperModel
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8867841169533384977L;
	
	
	@Index
	protected String roleName;
	@Index
	protected boolean roleStatus;
	protected String roleDescription;
	protected String roleCategory;
	@Index
	protected String moduleName;
	@Index
	ArrayList<ScreenAuthorization> authorization;
	
	public UserRole() {
		super();
		roleName="";
		roleStatus=true;
		roleDescription="";
		roleCategory="";
		moduleName= "";
//		UserRole urole=ofy().load().type(UserRole.class).filter("companyId", getCompanyId()).first().now();
//		authorization=ScreenAuthorization.getAllAuthorizations();
		authorization=ScreenAuthorization.getAllAuthorizationsForEnvironment();
	}
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = false, title = "Role Name")
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		if(roleName!=null)
		this.roleName = roleName.trim();
	}
	
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		if(moduleName!=null){
			this.moduleName = moduleName.trim();
		}
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = false, title = "Role Status")
	public Boolean getRoleStatus() {
		return roleStatus;
	}

	public void setRoleStatus(boolean roleStatus) {
		this.roleStatus = roleStatus;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}


	//@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = false, title = "Role Category")
	public String getRoleCategory() {
		return roleCategory;
	}

	public void setRoleCategory(String roleCategory) {
		if(roleCategory!=null)
		this.roleCategory = roleCategory.trim();
	}

	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof SuperModel)
		{
			UserRole role=(UserRole) arg0;
			if(role.roleName!=null)
				return role.roleName.compareTo(roleName);
		}
		
		return -1;
	}

	@Override
	public boolean isDuplicate(SuperModel model) {
		UserRole entity = (UserRole) model;
		String name = entity.roleName.trim();
		String curname=this.roleName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;				
		}
	}

	public ArrayList<ScreenAuthorization> getAuthorization() {
		return authorization;
	}



	public void setAuthorization(List<ScreenAuthorization> list) {
		ArrayList<ScreenAuthorization> scr=new ArrayList<ScreenAuthorization>();
		scr.addAll(list);
		this.authorization=scr;
	}


	@Override
	public String toString() {
		return this.roleName;
	}
	
	
	@GwtIncompatible
	@OnSave
	public void updateUser() {
		
		if (this.getRoleName() != null) {
		List<User> userObj = ofy().load().type(User.class).filter("role.companyId", this.companyId).filter("companyId", this.getCompanyId()).filter("role.roleName", this.getRoleName().trim()).list();
		if (userObj.size()>0) {
			
			
			for(int i=0;i<userObj.size();i++)
			{
				userObj.get(i).getRole().setAuthorization(this.getAuthorization());
				userObj.get(i).getRole().setRoleStatus(this.getRoleStatus());
				userObj.get(i).getRole().setRoleName(this.getRoleName());
				
			}
			ofy().save().entities(userObj).now();
		}
	}
	
	}
	
	
	/*******************************************Old Code**********************************************/
	
//	public boolean getAuthorization(Screen screen)
//	{		
//		for(ScreenAuthorization auth:authorization)
//		{
//			//*****HR Config******//
//			boolean HRtemp= isHRConfig(screen);
//	    	System.out.println("Inside HR::::::;"+screen+"    "+HRtemp);
//	    	if(HRtemp==true)
//	    	{
//	    		for(ScreenAuthorization hr:authorization)
//	    		{
//	    			if(hr.getScreens().equals(Screen.HRCONFIGURATION))
//	    				return hr.isAuthorized();
//	    		}
//	    	}
//	    	
//	    	//***Sales Config***//
//	    	boolean temp= isSalesConfig(screen);
//	    	System.out.println("Inside Sales Config::"+screen +"  "+temp);
//	    	if(temp==true)
//	    	{
//	    	   
//	    		for(ScreenAuthorization sales:authorization)
//	    		{
//	    			if(sales.getScreens().equals(Screen.SALESCONFIGURATION))
//	    				return sales.isAuthorized();
//	    		}
//	    	}
//			
//	    	//****Setting Config**********//
//	    	boolean settingtemp=isSettingConfig(screen);
//	    	if(settingtemp==true)
//	    	{
//	    		for(ScreenAuthorization setting:authorization)
//	    		{
//	    			if(setting.getScreens().equals(Screen.SETTINGCONFIGURATION))
//	    				return setting.isAuthorized();
//	    		}
//	    	}
//	    	
//	    	//*****Account Config******//
//	    	boolean accounttemp=isAccountConfig(screen);
//	    	if(accounttemp==true)
//	    	{
//	    		for(ScreenAuthorization account:authorization)
//	    		{
//	    			if(account.getScreens().equals(Screen.ACCOUNTCONFIGURATION))
//	    				return account.isAuthorized();
//	    		}
//	    	}
//	    	
//	    	//****Purchase Config*********//
//	    	boolean purchasetemp=isAccountConfig(screen);
//	    	if(purchasetemp==true)
//	    	{
//	    		for(ScreenAuthorization purchase:authorization)
//	    		{
//	    			if(purchase.getScreens().equals(Screen.PURCHASECONFIGURATION))
//	    				return purchase.isAuthorized();
//	    		}
//	    	}
//	    	
//	    	
//	    	
//			if(auth.getScreens().equals(screen))
//			{
//				System.out.println("***************"+screen);
//				return auth.isAuthorized();
//			}
//		///////////////////////////////////////////////////////////////////////////	
//			/*else {
//				    switch(auth.screens)
//				    {
//				    case HRCONFIGURATION:
//				    	
//				        break;	
//				    	
//				    case SALESCONFIGURATION:
//				    	
//				    	
//				        break;	
//				    
//				    	
//				    case ACCOUNTCONFIGURATION:
//				    	boolean Accounttemp= isAccountConfig(screen);
//				    	System.out.println("Inside Account::::::;"+screen+"    "+Accounttemp);
//				    	if(Accounttemp==true)
//				    	   return auth.isAuthorized();
//				        break;
//				    	
//				    case PURCHASECONFIGURATION:
//				    	boolean Purchasetemp= isPurchaseConfig(screen);
//				    	System.out.println("Inside Purchase::::::;"+screen+"    "+Purchasetemp);
//				    	if(Purchasetemp==true)
//				    	   return auth.isAuthorized();
//				        break;
//				    case SETTINGCONFIGURATION:
//				    	boolean Settingtemp= isSettingConfig(screen);
//				    	System.out.println("Inside Setting::::::;"+screen+"    "+Settingtemp);
//				    	if(Settingtemp==true)
//				    	   return auth.isAuthorized();
//				        break;
//				    }
//			     }*/
//		}
//		return false;
//	}
//	
//	private boolean isSalesConfig(Screen screen)
//	{
//		
//		return screen.equals(Screen.CUSTOMERGROUP) || screen.equals(Screen.CUSTOMERCATEGORY) || screen.equals(Screen.CUSTOMERTYPE) || screen.equals(Screen.CUSTOMERLEVEL) ||screen.equals(Screen.CUSTOMERPRIORITY) || 
//				screen.equals(Screen.LEADCATEGORY) ||screen.equals(Screen.LEADGROUP)|| screen.equals(Screen.LEADTYPE) || screen.equals(Screen.LEADSTATUS) || screen.equals(Screen.LEADPRIORITY)||
//				screen.equals(Screen.QUOTATIONCATEGORY) || screen.equals(Screen.QUOTATIONGROUP) || screen.equals(Screen.QUOTATIONTYPE)||screen.equals(Screen.QUOTATIONPRIORITY)||screen.equals(Screen.QUOTATIONSTATUS)||
//				screen.equals(Screen.CONTRACTGROUP)||screen.equals(Screen.CONTRACTCATEGORY)||screen.equals(Screen.CONTRACTTYPE);
//	}
//	
//	
//	private boolean isSettingConfig(Screen screen)
//	{
//		boolean val=false;
//		Screen[]screens={Screen.COMPANYTYPE,Screen.ACESSLEVEL};
//		for(int i=0;i<screens.length;i++)
//		{
//			val=val||screen.equals(screens[i]);
//		}
//		return val;
//	}
//	
//	private boolean isAccountConfig(Screen screen)
//	{
//		boolean val=false;
//		Screen[]screens={Screen.EXPENSEGROUP,Screen.EXPENSECATEGORY,Screen.EXPENSETYPE,Screen.PAYMENTMETHODS,Screen.PAYMENTTERMS,Screen.CURRENCY};
//		for(int i=0;i<screens.length;i++)
//		{
//			val=val||screen.equals(screens[i]);
//		}
//		return val;
//	}
//	
//	private boolean isPurchaseConfig(Screen screen)
//	{
//		boolean val=false;
//		Screen[]screens={Screen.VENDORGROUP,Screen.VENDORCATEGORY,Screen.VENDORTYPE};
//		for(int i=0;i<screens.length;i++)
//		{
//			val=val||screen.equals(screens[i]);
//		}
//		return val;
//	}
//
//	@SuppressWarnings("unused")
//	private boolean isProductConfig(Screen screen)
//	{
//		boolean val=false;
//		Screen[]screens={Screen.CATEGORY};
//		for(int i=0;i<screens.length;i++)
//		{
//			val=val||screen.equals(screens[i]);
//		}
//		return val;
//	}
//	
//	private boolean isHRConfig(Screen screen)
//	{
//		boolean val=false;
//		Screen[]screens={Screen.EMPLOYEEROLE,Screen.EMPLOYEEDESIGNATION,Screen.EMPLOYEEGROUP,Screen.EMPLOYEETYPE,Screen.SHIFTCATEGORY,Screen.PATTERNCATEGORY
//				,Screen.HOLIDAYTYPE,Screen.ADVANCETYPE,Screen.HRPROJECT};
//		for(int i=0;i<screens.length;i++)
//		{
//			val=val||screen.equals(screens[i]);
//		}
//		return val;
//	}

	/********************************************New Code*******************************************/
	public boolean getAuthorization(String screen,String module) {
		for (ScreenAuthorization auth : authorization) {

			boolean authorization = checkAuthorization(screen,module);
			if (authorization == true) {
				ScreenAuthorization saless = new ScreenAuthorization();
				System.out.println(" under AUthorization is ------------------ "+saless.isAuthorized());
				return saless.isAuthorized();
			}
		}
		return false;
	}

	private boolean checkAuthorization(String screen,String moduleName) {
		boolean val = false;
		for(int i=0;i<this.getAuthorization().size();i++)
		{
			val=val||(screen.equalsIgnoreCase(this.getAuthorization().get(i).getScreens())&&moduleName.equalsIgnoreCase(this.getAuthorization().get(i).getModule().trim()));
		}
		return val;
	}
	
	public boolean getViewAuthorization(String screen){
		boolean val=false;
		for(int i=0;i<this.getAuthorization().size();i++){
			if(screen.equalsIgnoreCase(this.getAuthorization().get(i).getScreens())){
				val=this.getAuthorization().get(i).isView();
			}
		}
		return val;
	}
	
	public boolean getCreateEditAuthorization(String screen){
		boolean val=false;
		for(int i=0;i<this.getAuthorization().size();i++){
			if(screen.equalsIgnoreCase(this.getAuthorization().get(i).getScreens())){
				val=this.getAuthorization().get(i).isCreate();
			}
		}
		return val;
	}
	
	public boolean getDownloadAuthorization(String screen){
		boolean val=false;
		for(int i=0;i<this.getAuthorization().size();i++){
			if(screen.equalsIgnoreCase(this.getAuthorization().get(i).getScreens())){
				val=this.getAuthorization().get(i).isDownload();
			}
		}
		return val;
	}
	
	public boolean getDownloadAuthorization(Screen screen){
		boolean val=false;
		for(int i=0;i<this.getAuthorization().size();i++){
			if(screen.toString().equalsIgnoreCase(this.getAuthorization().get(i).getScreens())){
				val=this.getAuthorization().get(i).isDownload();
			}
		}
		return val;
	}


}
