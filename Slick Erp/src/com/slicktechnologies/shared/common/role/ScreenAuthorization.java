package com.slicktechnologies.shared.common.role;

import java.io.Serializable;
import java.util.ArrayList;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.client.utility.Screen;
@Embed
public class ScreenAuthorization implements Serializable
{
   /**
	 * 
	 */
	private static final long serialVersionUID = 7655537195276991859L;
	
	@Index
	private String screens;
	@Index
	private boolean view;
	@Index
	private boolean create;
	@Index
	private boolean download;
	@Index
	private String module;
	private boolean approve;
	/**
	 * Dev : Rahul Verma
	 * Dated : 27 Nov 2017
	 * Description : This field is for android screen for particular user
	 */
	@Index
	private boolean android;
	/**
	 * Ends for Rahul Verma
	 */
   
   public ScreenAuthorization()
   {
	   screens="";
	   view=true;
	   create=true;
	   approve=true;
	   download=true;
	   android=false;
	   module="";
   }


//	public static ArrayList<ScreenAuthorization> getAllAuthorizations() {
//		ArrayList<ScreenAuthorization>screens=new ArrayList<ScreenAuthorization>();
//		Screen[]values=Screen.values();
//		for(int i=0;i<values.length;i++)
//		{
//			ScreenAuthorization auth=new ScreenAuthorization();
//			auth.regScreens=values[i];
//			screens.add(auth);
//		}
//		return screens;
//	}
	
	public static ArrayList<ScreenAuthorization> getAllAuthorizationsForEnvironment() {
		ArrayList<ScreenAuthorization>screens=new ArrayList<ScreenAuthorization>();
		Screen[]values=Screen.values();
		for(int i=0;i<values.length;i++)
		{
			ScreenAuthorization auth=new ScreenAuthorization();
			auth.screens=values[i].toString();
			screens.add(auth);
		}
		return screens;
	}


		//public Screen getScreens() {
		//	return screens;
		//}
		//
		//
		//public void setScreens(Screen screens) {
		//	this.screens = screens;
		//}
		//

		
		public String getScreens() {
			return screens;
		}
		
		public void setScreens(String screenss) {
			this.screens = screenss;
		}
		
		public boolean isView() {
			return view;
		}
		
		
		public void setView(boolean view) {
			this.view = view;
		}
		
		
		public boolean isCreate() {
			return create;
		}
		
		
		public void setCreate(boolean create) {
			this.create = create;
		}
		
		
		public boolean isApprove() {
			return approve;
		}
		
		
		public void setApprove(boolean approve) {
			this.approve = approve;
		}
		
		
		public boolean isDownload() {
			return download;
		}
		
		
		public void setDownload(boolean download) {
			this.download = download;
		}
		
		
		public boolean isAuthorized() {
			return(view&&create);
			
		}


		public String getModule() {
			return module;
		}


		public void setModule(String module) {
			this.module = module;
		}


		public boolean isAndroid() {
			return android;
		}


		public void setAndroid(boolean android) {
			this.android = android;
		}


		
//		public Screen getRegScreens() {
//			return regScreens;
//		}
//
//
//		public void setRegScreens(Screen regScreens) {
//			this.regScreens = regScreens;
//		}
		
		
		   
   
}
