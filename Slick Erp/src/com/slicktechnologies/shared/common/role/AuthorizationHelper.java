
package com.slicktechnologies.shared.common.role;

import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;

public class AuthorizationHelper 
{
	public static void setAsPerAuthorization(Screen screen,String moduleName) {
		
		String screenName = checkScreenName(screen);
		UserRole role = UserConfiguration.getRole();
		if (role == null)
			return;
		for (ScreenAuthorization hr : role.authorization) {
			
			if(AppConstants.ADMINUSER.equals(role.getRoleName().trim()))
			{
				if(hr.getScreens().trim().equalsIgnoreCase(screenName.trim())) {
					manageAppLevelMenu(hr);
					break;
				}
			}
			else
			{
				if(hr.getScreens().trim().equalsIgnoreCase(screenName.trim())&&hr.getModule().equalsIgnoreCase(moduleName.trim())) {
					manageAppLevelMenu(hr);
					break;
				}
			}
		}
	}
  
	private static void manageAppLevelMenu(ScreenAuthorization auth) {
		System.out.println("======111111111111111111111");
		
		if (auth.isView() == false) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int i = 0; i < menus.length; i++) {
				String text = menus[i].getText();
				if (text.contains("Search")) {
					menus[i].setVisible(false);
				}
			}
		}

		if (auth.isCreate() == false) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int j = 0; j < menus.length; j++) {
				String text = menus[j].getText();
				if (text.contains("Save") || text.contains("Edit")) {
					menus[j].setVisible(false);
				}
			}
		}

		if (auth.isDownload() == false) {
			InlineLabel[] menus = AppMemory.getAppMemory().skeleton.getMenuLabels();
			for (int j = 0; j < menus.length; j++) {
				String text = menus[j].getText();
				if (text.contains("Download")) {
					menus[j].setVisible(false);
				}
			}
		}
	}
  
	public static boolean getDownloadAuthorization(Screen screen,String moduleName) {
		String screenName = checkScreenName(screen);
		UserRole role = UserConfiguration.getRole();
		boolean dwnl = true;
		int i = 0;
		for (ScreenAuthorization hr : role.authorization) {
			
			if(AppConstants.ADMINUSER.equals(role.getRoleName().trim()))
			{
				if(hr.getScreens().equalsIgnoreCase(screenName)) {
					dwnl = role.getAuthorization().get(i).isDownload();
				}
			}
			else
			{
				if(hr.getScreens().equalsIgnoreCase(screenName)&&hr.getModule().equalsIgnoreCase(moduleName.trim())) {
					dwnl = role.getAuthorization().get(i).isDownload();
				}
			}
			
			i++;
		}
		return dwnl;
	}
	
	public static String checkScreenName(Screen screen) {
//		List<ScreenAuthorization> moduleValidation = UserConfiguration.getRole().getAuthorization();
//		List<String> moduleName = new ArrayList<String>();

		String screenName = null;
		switch (screen) {
		case SALESHOME:
			screenName = "Dashboard";
			break;
		case SALESCUSTOMER:
			screenName = "Customer";
			break;
		case SALESLEAD:
			screenName = "Lead";
			break;
		case SALESQUOTATION:
			screenName = "Quotation";
			break;
		case SALESORDER:
			screenName = "Sales Order";
			break;
		case QUICKSALESORDER:
			screenName = "Quick Sales Order";
			break;
		case DELIVERYNOTE:
			screenName = "Delivery Note";
			break;
		case INTERACTIONLIST:
			screenName = "Interaction To Do List";
			break;
		case INTERACTION:
			screenName = "Interaction To Do";
			break;
		case CONTACTPERSONLIST:
			screenName = "Contact Person List";
			break;
		case CONTACTPERSONDETAILS:
			screenName = "Contact Person Details";
			break;
		case SALESSECURITYDEPOSITLIST:
			screenName = "Security Deposit List";
			break;
		case CUSTOMERGROUP:
			screenName = "Customer Group";
			break;
		case CUSTOMERCATEGORY:
			screenName = "Customer Category";
			break;
		case CUSTOMERTYPE:
			screenName = "Customer Type";
			break;
		case CUSTOMERLEVEL:
			screenName = "Customer Level";
			break;
		case CUSTOMERPRIORITY:
			screenName = "Customer Priority";
			break;
			
		case RELIGION:
			screenName = "Religion";
			break;	
			
		case LEADSTATUS:
			screenName = "Lead Status";
			break;
		case LEADGROUP:
			screenName = "Lead Group";
			break;
		case LEADCATEGORY:
			screenName = "Lead Category";
			break;
		case LEADTYPE:
			screenName = "Lead Type";
			break;
		case LEADPRIORITY:
			screenName = "Lead Priority";
			break;
		case SALESQUOTATIONGROUP:
			screenName = "Sales Quotation Group";
			break;
		case SALESQUOTATIONCATEGORY:
			screenName = "Sales Quotation Category";
			break;
		case SALESQUOTATIONTYPE:
			screenName = "Sales Quotation Type";
			break;
		case SALESQUOTATIONPRIORITY:
			screenName = "Sales Quotation Priority";
			break;
		case SALESORDERGROUP:
			screenName = "Sales Order Group";
			break;
		case SALESORDERCATEGORY:
			screenName = "Sales Order Category";
			break;
		case DELIVERYNOTETYPE:
			screenName = "Delivery Note Type";
			break;
		case CHECKLISTSTEP:
			screenName = "Checklist Step";
			break;
		case CHECKLISTTYPE:
			screenName = "Checklist Type";
			break;
		case SHIPPINGMETHOD:
			screenName = "Shipping Method";
			break;
		case PACKINGMETHOD:
			screenName = "Packing Method";
			break;
		case PERSONTYPE:
			screenName = "Entity Type";
			break;
		case GROUPTYPE:
			screenName = "Interaction Group";
			break;
		case COMMUNICATIONCATEGORY:
			screenName = "Communication Category";
			break;
		case COMMUNICATIONTYPE:
			screenName = "Communication Type";
			break;
		case INTERACTIONPRIORITY:
			screenName = "Interaction Priority";
			break;
		case CONTACTROLE:
			screenName = "Contact Role";
			break;
			
			
		case HOME:
			screenName = "Dashboard";
			break;
		case CUSTOMER:
			screenName = "Customer";
			break;
		case LEAD:
			screenName = "Lead";
			break;
		case QUOTATION:
			screenName = "Quotation";
			break;
		case CONTRACT:
			screenName = "Contract";
			break;
		case SERVICE:
			screenName = "Customer Service List";
			break;
		case DEVICE:
			screenName = "Customer Service";
			break;
		case PROJECT:
			screenName = "Customer Project";
			break;
			
		case TECHNICIANDASHBOARD:
			screenName = "Technician Dashboard";
			break;
			
		case SALESPERSONDASHBOARD:
			screenName = "Sales Person Dashboard";
			break;
		case SALESPERSONTARGET:
			screenName = "Sales Person Targets";
			break;
		case SALESPERSONPERFORMANCE:
			screenName = "Sales Person Targets Performance";
			break;
			
		case LIAISONSTEPS:
			screenName = "Liasion Steps";
			break;
		case LIAISONGROUP:
			screenName = "Liasion Group";
			break;
		case LIASION:
			screenName = "Liasion";
			break;
		case SERVICESECURITYDEPOSITLIST:
			screenName = "Security Deposit List";
			break;
	
		case CONTRACTGROUP:
			screenName = "Contract Group";
			break;
		case CONTRACTCATEGORY:
			screenName = "Contract Category";
			break;
		case CONTRACTTYPE:
			screenName = "Contract Type";
			break;
		case BILLOFMATERIAL:
			screenName = "Bill Of Material";
			break;	
		
		case CUSTOMERLIST:
			screenName = "Customer List";
			break;	
			
		//purchase
		case PURCHASEDASHBOARD:
			screenName = "Dashboard";
			break;
		case VENDOR:
			screenName = "Vendor";
			break;
		case PURCHASEREQUISITE:
			screenName = "Purchase Requisition";
			break;
		case REQUESTFORQUOTATION:
			screenName = "RFQ";
			break;
		case LETTEROFINTENT:
			screenName = "Letter Of Intent(LOI)";
			break;	
		case PURCHASEORDER:
			screenName = "Purchase Order";
			break;
		case VENDORPRODUCTLIST:
			screenName = "Vendor Product Pricelist";
			break;	
		case VENDORPRICELIST:
			screenName = "Vendor Product Price";
			break;	
		
		case SERVICEPO:
			screenName = "Service PO";
			break;	
			
		case VENDORGROUP:
			screenName = "Vendor Group";
			break;
		case VENDORCATEGORY:
			screenName = "Vendor Category";
			break;
		case VENDORTYPE:
			screenName = "Vendor Type";
			break;
		case PRGROUP:
			screenName = "PR Group";
			break;
		case PRCATEGORY:
			screenName = "PR Category";
			break;
		case PRTYPE:
			screenName = "PR Type";
			break;
		case RFQCATEGORY:
			screenName = "RFQ Category";
			break;	
		case RFQTYPE:
			screenName = "RFQ Type";
			break;	
		case LOICATEGORY:
			screenName = "LOI Category";
			break;	
		case LOITYPE:
			screenName = "LOI Type";
			break;	
		case PURCHASEORDERCATEGORY:
			screenName = "PO Category";
			break;
		case PURCHASEORDERTYPE:
			screenName = "PO Type";
			break;		
			
		
		case PURCHASEREQUISITIONLIST:
			screenName = "Purchase Requisition List";
			break;
		case SERVICEPOCATEGORY:
			screenName = "Service PO Category";
			break;
		case SERVICEPOTYPE:
			screenName = "Service PO Type";
			break;	
			
			// inventory
		case INVENTORYHOME:
			screenName = "Dashboard";
			break;
		case PRODUCTINVENTORYTRANSACTION:
			screenName = "Inventory Transaction List";
			break;
		case PRODUCTINVENTORYVIEW:
			screenName = "Product Inventory View";
			break;
		case PRODUCTINVENTORYVIEWLIST:
			screenName = "Product Inventory List";
			break;
		case GRN:
			screenName = "GRN";
			break;	
		case MATERIALREQUESTNOTE:
			screenName = "Material Request Note";
			break;		
		case MATERIALISSUENOTE:
			screenName = "Material Issue Note";
			break;	
		case MATERIALMOVEMENTNOTE:
			screenName = "Material Movement Note";
			break;
		case WAREHOUSEDETAILS:
			screenName = "Physical Inventory";
			break;	
		
		case WAREHOUSE:
			screenName = "Warehouse";
			break;
		case STORAGELOCATION:
			screenName = "Storage Location";
			break;
		case STORAGEBIN:
			screenName = "Storage Bin";
			break;
		case GRNGROUP:
			screenName = "GRN Group";
			break;
		case GRNCATEGORY:
			screenName = "GRN Category";
			break;
		case GRNTYPE:
			screenName = "GRN Type";
			break;
		case MRNCATEGORY:
			screenName = "MRN Category";
			break;	
		case MRNTYPE:
			screenName = "MRN Type";
			break;	
		case MINCATEGORY:
			screenName = "MIN Category";
			break;	
		case MINTYPE:
			screenName = "MIN Type";
			break;	
		case MMNCATEGORY:
			screenName = "MMN Category";
			break;
		case MMNTYPE:
			screenName = "MMN Type";
			break;	
		case MMNDIRECTION:
			screenName = "MMN Direction";
			break;
		case MMNTRANSACTIONTYPE:
			screenName = "MMN Transaction Type";
			break;
			
		case WAREHOUSETYPE:
			screenName = "Warehouse Type";
			break;	
			
		//HR Admin
		case EMPLOYEE:
			screenName = "Employee";
			break;
		case LEAVEAPPLICATION:
			screenName = "Leave Application";
			break;	
		case ADVANCE:
			screenName = "Advance Request";
			break;
		case UPLOADATTENDENCE:
			screenName = "Upload Attendance";
			break;
		case EMPLOYEEATTENDENCE:
			screenName = "Employee Attendance";
			break;
		case TIMEREPORT:
			screenName = "Time Report";
			break;
		case CTC:
			screenName = "CTC";
			break;
		case LEAVEBALANCE:
			screenName = "Leave Balance";
			break;
		case ASSIGNEDSHIFT:
			screenName = "Assign Shift";
			break;
		case PROCESSPAYROLL:
			screenName = "Process Payroll";
			break;
			
			
		case EMPLOYEEROLE:
			screenName = "Employee Role";
			break;
		case EMPLOYEEDESIGNATION:
			screenName = "Employee Designation";
			break;
		case EMPLOYEETYPE:
			screenName = "Employee Type";
			break;
		case DEPARTMENT:
			screenName = "Department";
			break;
		case SHIFT:
			screenName = "Shift";
			break;
		case SHIFTPATTERN:
			screenName = "Shift Pattern";
			break;
		
		case SHIFTCATEGORY:
			screenName = "Shift Category";
			break;
		case PATTERNCATEGORY:
			screenName = "Pattern Category";
			break;
		case HRPROJECT:
			screenName = "Project";
			break;
		case CALENDAR:
			screenName = "Calendar";
			break;
		case HOLIDAYTYPE:
			screenName = "Holiday Type";
			break;
		case ADVANCETYPE:
			screenName = "Advance Type";
			break;
		case SALARYHEAD:
			screenName = "CTC Component";
			break;
		case ATTENDANCECONFIG:
			screenName = "Attendance Type";
			break;
		case LEAVEALLOCATION:
			screenName = "Calendar & Leave Allocation";
			break;
		case DEDUCTION:
			screenName = "Deductions";
			break;
		case LEAVETYPE:
			screenName = "Leave Type";
			break;
		case LEAVEGROUP:
			screenName = "Leave Group";
			break;
		case TIMEREPORTCONFIG:
			screenName = "Time Reporting Period";
			break;
			/**
			 * @author Anil , Date : 19-06-2019
			 * 
			 */
		case PAIDLEAVE:
			screenName = "Paid Leave";
			break;
		case BONUS:
			screenName = "Bonus";
			break;
			
		case PROVIDENTFUND:
			screenName = "Provident Fund";
			break;
			//Employee Self Service
			//Product
		case SERVICEPRODUCT:
			screenName = "Service Product";
			break;
		case ITEMPRODUCT:
			screenName = "Item Product";
			break;
		case PRODUCTGROUPLIST:
			screenName = "Product Group List";
			break;
		case PRODUCTGROUPDETAILS:
			screenName = "Product Group Details";
			break;	
			
			
		case CATEGORY:
			screenName = "Product Category";
			break;
		case UNITOFMEASUREMENT:
			screenName = "Unit Of Measurement";
			break;	
		case PRODUCTGROUP:
			screenName = "Product Group";
			break;		
		case PRODUCTTYPE:
			screenName = "Product Type";
			break;
		case PRODUCTCLASSIFICATION:
			screenName = "Product Classification";
			break;	
		
		// Asset Management
			
		case COMPANYASSET:
			screenName = "Company Asset";
			break;
		case CLIENTSIDEASSET:
			screenName = "Client Asset";
			break;
		case TOOLGROUP:
			screenName = "Tool Set";
			break;
			
		case ASSETCATEGORY:
			screenName = "Tool Category";
			break;
		case CLIENTSIDEASSETCATEGORY:
			screenName = "Client Asset Category";
			break;	
		//Accounts
		case ACCOUNTDASHBOARD:
			screenName = "Dashboard (AR)";
			break;
		case ACCOUNTSDASHBOARDAP:
			screenName = "Dashboard (AP)";
			break;
		case EXPENSEMANAGMENT:
			screenName = "Expense Management";
			break;
		case PETTYCASHTRANSACTION:
			screenName = "Petty Cash Transaction";
			break;	
		case BILLINGLIST:
			screenName = "Billing List";
			break;
		case BILLINGDETAILS:
			screenName = "Billing Details";
			break;	
		case INVOICELIST:
			screenName = "Invoice List";
			break;		
		case INVOICEDETAILS:
			screenName = "Invoice Details";
			break;	
		case PAYMENTLIST:
			screenName = "Payment List";
			break;	
		case PAYMENTDETAILS:
			screenName = "Payment Details";
			break;	
		case INTERFACETALLYLIST:
			screenName = "Accounting Interface";
			break;		
			
			
		case EXPENSEGROUP:
			screenName = "Expense Group";
			break;

		case EXPENSECATEGORY:
			screenName = "Expense Category";
			break;
		case EXPENSETYPE:
			screenName = "Expense Type";
			break;
		case GLACCOUNT:
			screenName = "GLAccount";
			break;	
		case GLACCOUNTGROUP:
			screenName = "GLAccount Group";
			break;
		case PAYMENTMETHODS:
			screenName = "Payment Method";
			break;
		case OTHERCHARGES:
			screenName = "Other Charges";
			break;
		case PETTYCASH:
			screenName = "Petty Cash";
			break;	
		case COMPANY:
			screenName = "Company";
			break;	
		case COMPANYTYPE:
			screenName = "Company Type";
			break;	
		case BILLINGCATEGORY:
			screenName = "Billing Category";
			break;	
		case BILLINGTYPE:
			screenName = "Billing Type";
			break;		
		case BILLINGGROUP:
			screenName = "Billing Group";
			break;	
		case TAXDETAILS:
			screenName = "Tax Details";
			break;
		case COMPANYPAYMENT:
			screenName = "Payment Mode";
			break;	
		case VENDORPAYMENT:
			screenName = "Vendor Payment Mode";
			break;				
				// Settings
		case BRANCH:
			screenName = "Branch";
			break;
		case USER:
			screenName = "User";
			break;	
		case CURRENCY:
			screenName = "Currency";
			break;	
		case COUNTRY:
			screenName = "Country";
			break;	
		case STATE:
			screenName = "State";
			break;		
		case CITY:
			screenName = "City";
			break;	
		case LOCALITY:
			screenName = "Locality";
			break;
		case MODULENAME:
			screenName = "Module Name";
			break;
		case DOCUMENTNAME:
			screenName = "Document Name";
			break;
		case NUMBERGENERATOR:
			screenName = "Number Generation";
			break;	
		case NUMBERGENERATORPROCESSNAME:
			screenName = "Process Name";
			break;	
		case PROCESSCONFIGURATION:
			screenName = "Process Configuration";
			break;		
		case PROCESSTYPE:
			screenName = "Process Type";
			break;	
		case ARTICLETYPE:
			screenName = "Article Type";
			break;	
		case DOCUMENTHISTORY:
			screenName = "Document History";
			break;	
		case LOGGEDINHISTORY:
			screenName = "Logged In History";
			break;
		case USERAUTHORIZE:
			screenName = "User Authorize";
			break;	
			// Approval
		case APPROVAL:
			screenName = "Approval";
			break;	
			//Registration
		case REGISTRATION:
			screenName = "Registration";
			break;	
			//Support
		case RAISETICKET:
			screenName = "Raise Ticket";
			break;	
		case REFRESHSCREEN:
			screenName = "Refresh Screen";
			break;		
			
			////////////////////////				/////////////
		
		
		
		case QUOTATIONGROUP:
			screenName = "Quotation Group";
			break;
		case QUOTATIONCATEGORY:
			screenName = "Quotation Category";
			break;
		case QUOTATIONTYPE:
			screenName = "Quotation Type";
			break;
		case QUOTATIONPRIORITY:
			screenName = "Quotation Priority";
			break;
		case PAYMENTTERMS:
			screenName = "Payment Terms";
			break;
		case PURCHASE:
			screenName = "Purchase Register";
			break;
		case TAX:
			screenName = "Tax";
			break;
//		case COMPLAIN:
//			screenName = "Complain";
//			break;
		case COMPLAIN:
			screenName = "Customer Support";
			break;	
		case COMPLAINCATEGORY:
			screenName = "Complaint Category";
			break;
		case COMPLAINSTATUS:
			screenName = "Complaint Status";
			break;
		case COMPLAINPRIORITY:
			screenName = "Compalint Priority";
			break;
		case USERAUTHORIZATION:
			screenName = "User Authorize";
			break;
		case ROLEDEFINITION:
			screenName = "Role Definition";
			break;
		case EMPLOYEELEVEL:
			screenName = "Employee Level";
			break;	
		case QUICKCONTRACT:
			screenName = "Quick Contract";
			break;
		case QUICKPURCHASEORDER:
			screenName = "Purchase Register";
			break;
		case EXPENSEPOLICIES:
			screenName = "Expense Policies";
			break;
			
			/**@author Amol
			 * Date 6-6-2019
			 * added authorization for app registration history
			 * 
			 */
			
		case APPREGISTRATIONHISTORY:
			screenName ="App Registration History";
			break;
			
			
			/**@author Amol
			 * Date 24-8-2019
			 * added authorization for PF,ESIC,LWF,PT.
			 * 
			 */
		case ESIC:
			screenName ="Esic";
			break;
			
		case PROFESSIONALTAX:
			screenName ="Professional Tax";
			break;
			
			
		case LWF:
			screenName ="LWF";
			break;
			
		case REPORTS:
			screenName ="Reports";
			break;
			
		case REPORTAUTHORISATION:
			screenName ="Report Authorization";
			break;
			
		case REPORTSCONFIGURATIONS:
			screenName ="Report Configuration";
			break;
			
		case VENDORINVOICEDETAILS:
			screenName = "Vendor Invoice Details";
			break;
			
		case VENDORPAYMENTDETAILS:
			screenName = "Vendor Payment Details";	
			break;	
			
		case SALARYSLIP:
			screenName = "Salary Slip";
			break;
			
		case CONTRACTRENEWAL:
			screenName = "Contract Renewal";
			break;
			
		default:
			screenName = screen.toString();
		}
		return screenName;
	}
  
  
  
  
  
}
