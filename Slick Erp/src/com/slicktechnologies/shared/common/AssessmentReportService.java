package com.slicktechnologies.shared.common;

import java.io.Serializable;
import java.util.Date;


import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

       /****Date :20/11/2017 
            BY :This Class is Created by Manisha
            Description :Incase of inspection quotation there should be some schedules for visiting person,
             create single service for assessment single service.**/
@Embed
public class AssessmentReportService extends SuperModel
{
	
	private static final long serialVersionUID = 1L;

	protected int productId;
	
	protected String productCode;
	
	protected String productName;
	
	protected String branch;
	
    protected String employeeName;
	
	protected Date serviceDate;
	
	
	public AssessmentReportService() {
		super();
		productCode= "";
		productName= "";
		branch= "";
		employeeName= "";
	}
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
