package com.slicktechnologies.shared.common.cancelsummary;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.documentcancellation.CancellationSummary;


@Entity
public class CancelSummary extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6029124192513407183L;

	@Index
	String docType;
	@Index
	int docID;
	protected ArrayList<CancellationSummary> cancelLis;
	
	/****************************************Constructor***********************************************/
	
	public CancelSummary() {
		super();
		docType="";
		cancelLis = new ArrayList<CancellationSummary>();
	}
	
	
	/*********************************************Getters And Setters***************************************/
	

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		if(docType!=null){
			this.docType = docType.trim();
		}
	}

	public int getDocID() {
		return docID;
	}

	public void setDocID(int docID) {
		this.docID = docID;
	}
	
	public List<CancellationSummary> getCancelLis() {
		
		return cancelLis;
	}

	public void setCancelLis(List<CancellationSummary> cancellistsetting) {
		if(cancellistsetting !=null){
			ArrayList<CancellationSummary> cancesumllist=new ArrayList<CancellationSummary>();
			cancesumllist.addAll(cancellistsetting);
			this.cancelLis = cancesumllist;
		}
	}
	
	/****************************************Overridden Method********************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
