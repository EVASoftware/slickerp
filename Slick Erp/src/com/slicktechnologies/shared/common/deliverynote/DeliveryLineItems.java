package com.slicktechnologies.shared.common.deliverynote;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.salesprocess.ProductLineItem;

@Embed
public class DeliveryLineItems extends ProductLineItem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -803773832497163912L;
	
	protected String wareHouseName;
	protected String storageLocName;
	protected String storageBinName;
	
	
	public DeliveryLineItems() {
		super();
		wareHouseName="";
		storageLocName="";
		storageBinName="";
	}


	public String getWareHouseName() {
		return wareHouseName;
	}


	public void setWareHouseName(String wareHouseName) {
		if(wareHouseName!=null){
			this.wareHouseName = wareHouseName.trim();
		}
	}


	public String getStorageLocName() {
		return storageLocName;
	}


	public void setStorageLocName(String storageLocName) {
		if(storageLocName!=null){
			this.storageLocName = storageLocName.trim();
		}
	}


	public String getStorageBinName() {
		return storageBinName;
	}


	public void setStorageBinName(String storageBinName) {
		if(storageBinName!=null){
			this.storageBinName = storageBinName.trim();
		}
	}
	
	
	
	
	

}
