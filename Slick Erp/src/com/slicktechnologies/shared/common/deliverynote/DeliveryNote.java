package com.slicktechnologies.shared.common.deliverynote;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.dev.shell.log.SwingTreeLogger.LogEvent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.UpdateStock;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

@Entity
public class DeliveryNote extends ConcreteBusinessProcess {

	/**
	 * 
	 */
	private static final long serialVersionUID = 63467821414233154L;
	public static final String DELIVERYNOTECANCEL = "Cancelled";
	public static final String DELIVERYCOMPLETED = "Completed";
	
	/****************************************Applicability Attributes**************************************/
	@Index 
	protected PersonInfo cinfo;
	@Index
	protected Integer salesOrderCount;
	@Index
	protected Integer quotationCount;
	@Index
	protected Integer leadCount;
	/**
	 * Updated By:Viraj
	 * Date:27-06-2019
	 * Description: made ref no index to search through query
	 */
	@Index
	protected String referenceNumber;
	protected Date referenceDate;
	@Index
	protected String cformStatus;
	protected double cstPercent;
	protected String cstName;
	protected DocumentUpload deliveryDocument;
	@Index
	protected Date deliveryDate;
	protected String shippingTypeName;
	protected String shipCheckList;
	protected String packingTypeName;
	protected String packCheckList;
	ArrayList<ShippingSteps> shippingStepsLis;
	ArrayList<ShippingSteps> packingStepsLis;
	ArrayList<DeliveryLineItems> deliveryItems;
	ArrayList<ProductOtherCharges> prodTaxesLis;
	ArrayList<ProductOtherCharges> prodChargeLis;
	protected String description;
	protected double totalAmount;
	protected Address shippingAddress;
	protected double netpayable;
	protected double amountInclTax;
	Date completionDate;
	protected String vehicleNo;
	@Index
	protected int invoiceId;
	protected String otherInfo1;
	protected String otherInfo2;
	
	protected String driverName;
	protected long driverPhone;
	protected String vehicleName;
	protected String lrNumber;
	protected String trackingNumber;
	protected String numberRange;//Added by Ashwini
	
	/****2-2-2019 added by Amol****/
	protected String refOrderNO;
	
	/**
	 * @author Anil ,Date : 16-04-2019
	 * storing customer branch and customer POC name
	 */
	@Index
	String custBranch;
	@Index
	String custPocName;
	
	/**
	 * @author Anil , Date : 31-07-2019
	 * adding eway bill number
	 * raised by Sasha,Nitin sir, sonu
	 */
	@Index
	String ewayBillNumber;
	
	/**
	 * @author Anil
	 * @since 21-07-2020
	 * Adding delivery status field
	 * for PTSPL raised by Rahul Tiwari
	 */
	
	@Index
	String deliveryStatus;
	
	/*****************************************************************************************************/
	
	/****************************************Relational Attributes******************************************/
	
	@Index
	protected Key<CheckListType>checklistKey;	
	
	@Index
	protected Address billingAddress;
  	protected String desriptionTwo;
	
	/***************************************************************************************************/
	
	/*******************************************Constructor************************************************/
	
	public DeliveryNote() {
		cinfo=new PersonInfo();
		cformStatus="";
		deliveryDocument=new DocumentUpload();
		shippingTypeName="";
		packingTypeName="";
		shipCheckList="";
		packCheckList="";
		shippingStepsLis=new ArrayList<ShippingSteps>();
		packingStepsLis=new ArrayList<ShippingSteps>();
		deliveryItems=new ArrayList<DeliveryLineItems>();
		prodTaxesLis=new ArrayList<ProductOtherCharges>();
		prodChargeLis=new ArrayList<ProductOtherCharges>();
		shippingAddress=new Address();
		description="";
		vehicleNo="";
		cstName="";
		driverName="";
		vehicleName="";
		lrNumber="";
		trackingNumber="";
		otherInfo1="";
		otherInfo2="";
	}
	
/***********************************************************************************************************/	
	
	/************************************Getters And Setters***********************************************/
	

	public String getEwayBillNumber() {
		return ewayBillNumber;
	}

	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}
	
	public String getCustBranch() {
		return custBranch;
	}

	public void setCustBranch(String custBranch) {
		this.custBranch = custBranch;
	}

	public String getCustPocName() {
		return custPocName;
	}

	public void setCustPocName(String custPocName) {
		this.custPocName = custPocName;
	}
	
	
	public PersonInfo getCinfo() {
		return cinfo;
	}

	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}

	public Integer getSalesOrderCount() {
		return salesOrderCount;
	}

	public void setSalesOrderCount(Integer salesOrderCount) {
		if(salesOrderCount!=null){
			this.salesOrderCount = salesOrderCount;
		}
	}

	public Integer getQuotationCount() {
		return quotationCount;
	}

	public void setQuotationCount(Integer quotationCount) {
		this.quotationCount = quotationCount;
	}

	public Integer getLeadCount() {
		return leadCount;
	}

	public void setLeadCount(Integer leadCount) {
		this.leadCount = leadCount;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		if(referenceNumber!=null){
			this.referenceNumber = referenceNumber;
		}
	}

	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		if(referenceDate!=null){
			this.referenceDate = referenceDate;
		}
	}

	public String getCformStatus() {
		return cformStatus;
	}

	public void setCformStatus(String cformStatus) {
		if(cformStatus!=null){
			this.cformStatus = cformStatus.trim();
		}
	}

	public Double getCstPercent() {
		return cstPercent;
	}

	public void setCstPercent(double cstPercent) {
		this.cstPercent = cstPercent;
	}
	
	public String getCstName() {
		return cstName;
	}

	public void setCstName(String cstName) {
		if(cstName!=null){
			this.cstName = cstName.trim();
		}
	}

	public DocumentUpload getDeliveryDocument() {
		return deliveryDocument;
	}

	public void setDeliveryDocument(DocumentUpload deliveryDocument) {
		if(deliveryDocument!=null){
			this.deliveryDocument = deliveryDocument;
		}
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		if(deliveryDate!=null){
			this.deliveryDate = deliveryDate;
		}
	}

	public String getShippingTypeName() {
		return shippingTypeName;
	}

	public void setShippingTypeName(String shippingTypeName) {
		if(shippingTypeName!=null){
			this.shippingTypeName = shippingTypeName.trim();
		}
	}

	public String getShipCheckList() {
		return shipCheckList;
	}

	public void setShipCheckList(String shipCheckList) {
		if(shipCheckList!=null){
			this.shipCheckList = shipCheckList.trim();
		}
	}

	public String getPackingTypeName() {
		return packingTypeName;
	}

	public void setPackingTypeName(String packingTypeName) {
		if(packingTypeName!=null){
			this.packingTypeName = packingTypeName.trim();
		}
	}

	public String getPackCheckList() {
		return packCheckList;
	}

	public void setPackCheckList(String packCheckList) {
		if(packCheckList!=null){
			this.packCheckList = packCheckList.trim();
		}
	}

	public List<ShippingSteps> getShippingStepsLis() {
		return shippingStepsLis;
	}

	public void setShippingStepsLis(List<ShippingSteps> shippingStepsLis) {
		ArrayList<ShippingSteps> arrSSteps=new ArrayList<ShippingSteps>();
		arrSSteps.addAll(shippingStepsLis);
		this.shippingStepsLis = arrSSteps;
	}

	public List<ShippingSteps> getPackingStepsLis() {
		return packingStepsLis;
	}

	public void setPackingStepsLis(List<ShippingSteps> packingStepsLis) {
		ArrayList<ShippingSteps> arrPSteps=new ArrayList<ShippingSteps>();
		arrPSteps.addAll(packingStepsLis);
		this.packingStepsLis = arrPSteps;
	}

	public List<DeliveryLineItems> getDeliveryItems() {
		return deliveryItems;
	}

	public void setDeliveryItems(List<DeliveryLineItems> deliveryItems) {
		ArrayList<DeliveryLineItems> arrDelivery=new ArrayList<DeliveryLineItems>();
		arrDelivery.addAll(deliveryItems);
		this.deliveryItems = arrDelivery;
	}

	public List<ProductOtherCharges> getProdTaxesLis() {
		return prodTaxesLis;
	}

	public void setProdTaxesLis(List<ProductOtherCharges> prodTaxesLis) {
		ArrayList<ProductOtherCharges> arrTax=new ArrayList<ProductOtherCharges>();
		arrTax.addAll(prodTaxesLis);
		this.prodTaxesLis = arrTax;
	}

	public List<ProductOtherCharges> getProdChargeLis() {
		return prodChargeLis;
	}

	public void setProdChargeLis(List<ProductOtherCharges> prodChargeLis) {
		ArrayList<ProductOtherCharges> arrCharge=new ArrayList<ProductOtherCharges>();
		arrCharge.addAll(prodChargeLis);
		this.prodChargeLis = arrCharge;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		if(totalAmount!=null){
			this.totalAmount = totalAmount;
		}
	}

	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		if(shippingAddress!=null){
			this.shippingAddress = shippingAddress;
		}
	}

	public double getNetpayable() {
		return netpayable;
	}

	public void setNetpayable(Double netpayable) {
		if(netpayable!=null){
			this.netpayable = netpayable;
		}
	}
	
	public double getAmountInclTax() {
		return amountInclTax;
	}

	public void setAmountInclTax(Double amountInclTax) {
		if(amountInclTax!=null){
			this.amountInclTax = amountInclTax;
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}
	
	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		if(vehicleNo!=null){
			this.vehicleNo = vehicleNo.trim();
		}
	}
	
	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		if(completionDate!=null){
			this.completionDate = completionDate;
		}
	}
	
	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		if(driverName!=null){
			this.driverName = driverName.trim();
		}
	}

	public long getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(long driverPhone) {
		this.driverPhone = driverPhone;
	}

	public String getVehicleName() {
		return vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		if(vehicleName!=null){
			this.vehicleName = vehicleName.trim();
		}
	}

	public String getLrNumber() {
		return lrNumber;
	}

	public void setLrNumber(String lrNumber) {
		if(lrNumber!=null){
			this.lrNumber = lrNumber.trim();
		}
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		if(trackingNumber!=null){
			this.trackingNumber = trackingNumber.trim();
		}
	}
	
	public String getOtherInfo1() {
		return otherInfo1;
	}

	public void setOtherInfo1(String otherInfo1) {
		if(otherInfo1!=null){
			this.otherInfo1 = otherInfo1.trim();
		}
	}

	public String getOtherInfo2() {
		return otherInfo2;
	}

	public void setOtherInfo2(String otherInfo2) {
		if(otherInfo2!=null){
			this.otherInfo2 = otherInfo2.trim();
		}
	}
	
	
	
	
	
	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	/*Date:2-2-2019
	 * @author AMOL
	 * for refrence order no
	 */
	public String getRefOrderNO() {
		return refOrderNO;
	}

	public void setRefOrderNO(String refOrderNO) {
		this.refOrderNO = refOrderNO;
	}
	/*
	 * Date:23/07/2018
	 * Developer:Ashwini
	 * Des:To get and Set number range.
	 */
	
	

	public String getNumberRange(){
		return numberRange;
	}

	public void setNumberRange(String numberRange){
		this.numberRange = numberRange;
	}
	
	public String getDesriptionTwo() {
		return desriptionTwo;
	}

	public void setDesriptionTwo(String desriptionTwo) {
		this.desriptionTwo = desriptionTwo;
	}
	/*
	 * End by Ashwini
	 */
	/******************************************React On Approval****************************************************/
	
	@GwtIncompatible
	public void reactOnApproval() {
		Logger logger=Logger.getLogger("Sms Logger");

		if(getStatus().equals(ConcreteBusinessProcess.APPROVED))
		{
			logger.log(Level.SEVERE, "status"+getStatus());
			
			System.out.println("Delivery note testing inside shared");
//			for(int i=0;i<getDeliveryItems().size();i++){
//			
//				int productId=getDeliveryItems().get(i).getProdId();
//				String storageLocation=getDeliveryItems().get(i).getStorageLocName();
//				String storageBin=getDeliveryItems().get(i).getStorageBinName();
//				String warehouse=getDeliveryItems().get(i).getWareHouseName();
//				double productQty=getDeliveryItems().get(i).getQuantity();
//				Date date=getCreationDate();
//				long compId=getCompanyId();
//				
//				System.out.println("Hi I Am In");
//				System.out.println("Prod Id"+getDeliveryItems().get(i).getProdId());
//				System.out.println("Storage Loc"+getDeliveryItems().get(i).getStorageLocName());
//				System.out.println("Storage Bin"+getDeliveryItems().get(i).getStorageBinName());
//				System.out.println("WareHouse Name"+getDeliveryItems().get(i).getWareHouseName());
//				System.out.println("Prod Qty"+getDeliveryItems().get(i).getQuantity());
//				System.out.println("Company Id InApproval");
	//
//			
//				UpdateStock.setProductInventory(productId,productQty, count,date,"Delivery Note","",
//						warehouse, storageLocation, storageBin, AppConstants.SUBTRACT,compId);
//			}
			
//			updateInventoryViewList();
			
//				HashMap<Integer, Double> proQty= getValidateStockForOrder(false);
				
				boolean flag = false;
				Integer sorder = this.getSalesOrderCount();
				List<String> status= new ArrayList<String>();
				status.add(DeliveryNote.CREATED);
				status.add(DeliveryNote.APPROVED);
				status.add(DeliveryNote.REJECTED);
				status.add(DeliveryNote.CLOSED);
				status.add(DeliveryNote.REQUESTED);
				status.add(DeliveryNote.DELIVERYCOMPLETED);
				List<DeliveryNote> deliveryList = ofy().load().type(DeliveryNote.class)
							.filter("salesOrderCount",this.getSalesOrderCount()).filter("status IN", status)
							.filter("companyId", getCompanyId()).list();
				
				SalesOrder srOrder  = ofy().load().type(SalesOrder.class).filter("count",this.getSalesOrderCount()).filter("companyId", getCompanyId()).first().now();
				HashMap<Integer, ArrayList<ProductSerialNoMapping>> prListSr = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				HashMap<Integer, ArrayList<ProductSerialNoMapping>> drprListSr = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				int counter=0;
				HashMap<Integer, Double> proqty = new HashMap<Integer, Double>();
				for(int i = 0 ; i < srOrder.getItems().size() ; i++){
					for(DeliveryNote dtNote : deliveryList){
						for(int j = 0 ;j<dtNote.getDeliveryItems().size();j++){
							if(dtNote.getCount() != this.count){
								
							}
							if(dtNote.getDeliveryItems().get(j).getProdId() == srOrder.getItems().get(i).getPrduct().getCount()
									&& dtNote.getDeliveryItems().get(j).getProductSrNumber() == srOrder.getItems().get(i).getProductSrNo()){
								if(srOrder.getItems().get(i).getProSerialNoDetails()!=null &&
										srOrder.getItems().get(i).getProSerialNoDetails().size()>0 &&
										srOrder.getItems().get(i).getProSerialNoDetails().containsKey(0)
										&& srOrder.getItems().get(i).getProSerialNoDetails().get(0) != null){
									if(prListSr.containsKey(srOrder.getItems().get(i).getPrduct().getCount())){
										prListSr.get(srOrder.getItems().get(i).getPrduct().getCount()).addAll(srOrder.getItems().get(i).getProSerialNoDetails().get(0));
									}else{
										prListSr.put(srOrder.getItems().get(i).getPrduct().getCount(),srOrder.getItems().get(i).getProSerialNoDetails().get(0));
									}
									
								}

								if(proqty.containsKey(dtNote.getDeliveryItems().get(j).getProdId())){
									
									double qty = proqty.get(dtNote.getDeliveryItems().get(j).getProdId());
									qty = qty + dtNote.getDeliveryItems().get(j).getQuantity();
									proqty.put(dtNote.getDeliveryItems().get(j).getProdId(), qty);
									
									if(drprListSr.containsKey(dtNote.getDeliveryItems().get(j).getProdId()) 
											&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails()!=null
											&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails().containsKey(0) 
											&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0)!=null){
										
										drprListSr.get(dtNote.getDeliveryItems().get(j).getProdId()).addAll(dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0));
									}else if( dtNote.getDeliveryItems().get(j).getProSerialNoDetails()!=null
											&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails().containsKey(0) 
											&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0)!=null){
										drprListSr.put(dtNote.getDeliveryItems().get(j).getProdId(),dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0));
									}
								
								}else{
								
									if(dtNote.getDeliveryItems().get(j).getProSerialNoDetails()!=null
											&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails().containsKey(0) 
												&& dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0)!=null){
										if(drprListSr.containsKey(dtNote.getDeliveryItems().get(j).getProdId())){
											drprListSr.get(dtNote.getDeliveryItems().get(j).getProdId()).addAll(dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0));
										}else{
											drprListSr.put(dtNote.getDeliveryItems().get(j).getProdId(),dtNote.getDeliveryItems().get(j).getProSerialNoDetails().get(0));
											
										}
										
										}
									double qty =  dtNote.getDeliveryItems().get(j).getQuantity();
									proqty.put(dtNote.getDeliveryItems().get(j).getProdId(), qty);
								
								}
							}
						}
					}
				}
				
				counter = 0;
				
				HashMap<Integer, Double> reProQty = new HashMap<Integer, Double>();
				HashMap<Integer, Double> lssProQty = new HashMap<Integer, Double>();
				HashMap<Integer, ArrayList<ProductSerialNoMapping>> rmDlSrList = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
				for(Integer prID : proqty.keySet()){
					for(int i = 0 ; i < srOrder.getItems().size() ; i++){
						if(srOrder.getItems().get(i).getPrduct().getCount() == prID && srOrder.getItems().get(i).getQty() == proqty.get(prID)){
							counter ++;
							
						}else if(srOrder.getItems().get(i).getPrduct().getCount() == prID && srOrder.getItems().get(i).getQty() < proqty.get(prID)){
							reProQty.put(srOrder.getItems().get(i).getPrduct().getCount(), proqty.get(prID) - srOrder.getItems().get(i).getQty());
						}else if(srOrder.getItems().get(i).getPrduct().getCount() == prID && srOrder.getItems().get(i).getQty() > proqty.get(prID)){
							ArrayList<ProductSerialNoMapping> prMapingRm = new ArrayList<ProductSerialNoMapping>();
							if(prListSr.containsKey(srOrder.getItems().get(i).getPrduct().getCount())){
								prMapingRm = createDeliveryNoteOfRemaining(prListSr,drprListSr,srOrder.getItems().get(i).getPrduct().getCount());
								rmDlSrList.put(srOrder.getItems().get(i).getPrduct().getCount(), prMapingRm);
							}
							
							lssProQty.put(srOrder.getItems().get(i).getPrduct().getCount(),  srOrder.getItems().get(i).getQty() - proqty.get(prID));
						}
					}
				}
				
				if(reProQty.size()>0){
					return;
				}else if(lssProQty.size()>0){
					List<DeliveryLineItems> updList = getUpdatedList1(lssProQty,srOrder,rmDlSrList);
					
					

					System.out.println("Entity Enter Here");
					DeliveryNote deliveryEntity=new DeliveryNote();
					deliveryEntity.setCompanyId(this.getCompanyId());
					if(this.getLeadCount()!=0){
						deliveryEntity.setLeadCount(this.getLeadCount());
					}
					if(this.getQuotationCount()!=0){
						deliveryEntity.setQuotationCount(this.getQuotationCount());
					}
					if(this.getSalesOrderCount()!=0){
						deliveryEntity.setSalesOrderCount(this.getSalesOrderCount());
					}
					if(this.getReferenceNumber()!=null){
						deliveryEntity.setReferenceNumber(this.getReferenceNumber());
					}
					if(this.getReferenceDate()!=null){
						deliveryEntity.setReferenceDate(this.getReferenceDate());
					}
					if(this.getEmployee()!=null){
						deliveryEntity.setEmployee(this.getEmployee());
					}
					if(this.getCformStatus()!=null){
						deliveryEntity.setCformStatus(this.getCformStatus());
					}
					if(this.getCstPercent()!=0){
						deliveryEntity.setCstPercent(this.getCstPercent());
					}
					if(this.getDeliveryDocument()!=null){
						deliveryEntity.setDeliveryDocument(this.getDeliveryDocument());
					}
					deliveryEntity.setStatus(ConcreteBusinessProcess.CREATED);
					if(this.getBranch()!=null){
						deliveryEntity.setBranch(this.getBranch());
					}
					if(this.getCategory()!=null){
						deliveryEntity.setCategory(this.getCategory());
					}
					if(this.getType()!=null){
						deliveryEntity.setType(this.getType());
					}
					if(this.getGroup()!=null){
						deliveryEntity.setGroup(this.getGroup());
					}
					if(this.getDeliveryDate()!=null){
						deliveryEntity.setDeliveryDate(this.getDeliveryDate());
					}
					if(this.getShippingAddress()!=null){
						deliveryEntity.setShippingAddress(this.getShippingAddress());
					}
					if(this.getDeliveryItems().size()!=0){
						deliveryEntity.setDeliveryItems(updList);
					}
					if(this.getShippingTypeName()!=null){
						deliveryEntity.setShippingTypeName(this.getShippingTypeName());
					}
					if(this.getShipCheckList()!=null){
						deliveryEntity.setShipCheckList(this.getShipCheckList());
					}
					if(this.getPackingTypeName()!=null){
						deliveryEntity.setPackingTypeName(this.getPackingTypeName());
					}
					if(this.getPackCheckList()!=null){
						deliveryEntity.setPackCheckList(this.getPackCheckList());
					}
					
					if(this.getCinfo().getCount()!=0){
						deliveryEntity.getCinfo().setCount(this.getCinfo().getCount());
					}
					if(this.getCinfo().getFullName()!=null){
						deliveryEntity.getCinfo().setFullName(this.getCinfo().getFullName());
					}
					if(this.getCinfo().getCellNumber()!=0){
						deliveryEntity.getCinfo().setCellNumber(this.getCinfo().getCellNumber());
					}
					
					if(this.getApproverName()!=null){
						deliveryEntity.setApproverName(this.getApproverName());
					}
					if(this.getPackingStepsLis().size()!=0){
						deliveryEntity.setPackingStepsLis(this.getPackingStepsLis());
					}
					
					if(this.getProdChargeLis().size()!=0){
						deliveryEntity.setProdChargeLis(this.getProdChargeLis());
					}
					if(this.getProdTaxesLis().size()!=0){
						deliveryEntity.setProdTaxesLis(this.getProdTaxesLis());
					}
					if(this.getTotalAmount()!=0){
						deliveryEntity.setTotalAmount(this.getTotalAmount());
					}
					
					if(this.getShippingStepsLis().size()!=0){
						deliveryEntity.setShippingStepsLis(this.getShippingStepsLis());
					}
					
					
					GenricServiceImpl grnImpl  = new GenricServiceImpl();
					grnImpl.save(deliveryEntity);
//					}
				}else {
//					return null; 
				}
//				return null;
				
			
				
//				if(proQty != null && proQty.size()>0){
//					return;
//				}else{
//					
//				}
				
				/**
				 * @author Vijay Date :- 02-11-2020
				 * Des :- below stock deduction logic moved to mark completion
				 * when delivery note mark completed then its stock will deducted
				 * requirement raised by Rahul Tiwari
				 * 
				 */
				
//				/**
//				 * Date : 18-01-2017 By Anil
//				 * New Stock Updation and Transaction creation Code
//				 */
//				
//				ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
//				for(DeliveryLineItems product:getDeliveryItems()){
//					InventoryTransactionLineItem item=new InventoryTransactionLineItem();
//					item.setCompanyId(this.getCompanyId());
//					item.setBranch(this.getBranch());
//					item.setDocumentType(AppConstants.DELIVERYNOTE);
//					item.setDocumentId(this.getCount());
//					item.setDocumentDate(this.getCreationDate());
//					item.setDocumnetTitle("");
//					item.setProdId(product.getProdId());
//					item.setUpdateQty(product.getQuantity());
//					item.setOperation(AppConstants.SUBTRACT);
//					item.setWarehouseName(product.getWareHouseName());
//					item.setStorageLocation(product.getStorageLocName());
//					item.setStorageBin(product.getStorageBinName());
//					/**
//					 * nidhi
//					 * 27-08-2018
//					 */
//					item.setProSerialNoDetails(product.getProSerialNoDetails());
//					/**
//					 * end
//					 */
//					itemList.add(item);
//				}
//				UpdateStock.setProductInventory(itemList);
//				
//				/**
//				 * End
//				 */
//				
				
				/**
				 * @author Vijay Chougule Date 21-09-2020
				 * Des :- Deliver Note SMS
				 */
				logger.log(Level.SEVERE, " Sending SMS");
				sendSMStoClient();
				
				/**
				 * ends here
				 */
			
			}
	}
	
	
	@GwtIncompatible
	protected void updateInventoryViewList()
	{
		List<DeliveryLineItems> deliveryLis=this.getDeliveryItems();
		for(int i=0;i<deliveryLis.size();i++)
		{
			int prodId=deliveryLis.get(i).getProdId();
			String wareHouse=deliveryLis.get(i).getWareHouseName();
			String storageLoc=deliveryLis.get(i).getStorageLocName();
			String storageBin=deliveryLis.get(i).getStorageBinName();
			
			ProductInventoryViewDetails prodInvDtls=ofy().load().type(ProductInventoryViewDetails.class).filter("count",prodId).filter("warehousename", wareHouse.trim()).filter("storagelocation", storageLoc.trim()).filter("storagebin", storageBin.trim()).filter("companyId", getCompanyId()).first().now();
			
			if(prodInvDtls!=null){
				prodInvDtls.setAvailableqty(prodInvDtls.getAvailableqty()-deliveryLis.get(i).getQuantity());
				GenricServiceImpl impl=new GenricServiceImpl();
				impl.save(prodInvDtls);
			}
		}
	}
	
	/*********************************Relational Key Management**********************************************/
	
	
	@OnSave
	@GwtIncompatible
	public void OnSave()
	{
		if(shipCheckList!=null){
		   checklistKey=ofy().load().type(CheckListType.class).filter("checkListName",this.shipCheckList).filter("companyId", getCompanyId()).keys().first().now();
		}
		
		if(this.getDeliveryDate()!=null){
			setDeliveryDate(DateUtility.getDateWithTimeZone("IST", this.getDeliveryDate()));
		}
		if(this.getCompletionDate()!=null){
			setCompletionDate(DateUtility.getDateWithTimeZone("IST",this.getCompletionDate()));
		}
		
	}
	
	@OnSave
	@GwtIncompatible
	private void reactOnSave(){

	    if(id == null){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "MonthwiseNumberGeneration", this.getCompanyId())){
				DecimalFormat df=new DecimalFormat("000");
				ServerAppUtility utility = new ServerAppUtility();
				SimpleDateFormat mmm = new SimpleDateFormat("MMM");
				mmm.setTimeZone(TimeZone.getTimeZone("IST"));
				String month = mmm.format(this.getDeliveryDate());
				String year = "";
				try {
					year = utility.getFinancialYearFormat(this.getDeliveryDate(), true);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String number = "DN/"+month+"/"+year;
				ProcessConfiguration config = new ProcessConfiguration();
				config= ofy().load()
						.type(ProcessConfiguration.class)
						.filter("companyId", companyId)
						.filter("processName", number).first().now();
				int cnt = 1;
					if (config !=null) {

						for (int i = 0; i < config.getProcessList().size(); i++) {
							try{
								cnt = Integer.parseInt(config.getProcessList().get(i).getProcessType());
								cnt = cnt +1;
								config.getProcessList().get(i).setProcessType(cnt+"");
							}catch(Exception e){
								cnt = 0;
							}
						}

					}
					else{
						config = new ProcessConfiguration();
						config.setCompanyId(this.companyId);
						config.setProcessName(number);
						config.setConfigStatus(true);
						ArrayList<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
						ProcessTypeDetails type = new ProcessTypeDetails();
						type.setProcessName(number);
						type.setProcessType(cnt+"");
						type.setStatus(true);
						processList.add(type);		
						config.setProcessList(processList);
					}
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(config);
					if(cnt <= 999){
										this.setRefOrderNO("DN/"+month.toUpperCase()+"/"+df.format(cnt)+"/"+year);
					}else{
						this.setRefOrderNO("DN/"+month.toUpperCase()+"/"+cnt+"/"+year);
					}
				}
			}

		
			
		
		
	}
	
	@OnLoad
	@GwtIncompatible
	public void onLoad()
	{
	  if(checklistKey!=null)
	  {
		 CheckListType checkList=ofy().load().key(checklistKey).now();
		 if(checkList!=null)
			 this.shipCheckList=(checkList.getCheckListName());
			 
	}
	}

	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getDeliveryDate()!=null){
			this.setDeliveryDate(dateUtility.setTimeMidOftheDayToDate(this.getDeliveryDate()));
		}
		if(this.getCompletionDate()!=null){
			this.setCompletionDate(dateUtility.setTimeMidOftheDayToDate(this.getCompletionDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */
	/****************************************Status List Box**********************************************/
	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CREATED);
		statuslist.add(APPROVED);
		statuslist.add(REJECTED);
		statuslist.add(REQUESTED);
		statuslist.add(DELIVERYNOTECANCEL);
		statuslist.add(DELIVERYCOMPLETED);
		
		return statuslist;
	}
	/*************************************Overridden Method******************************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	private ArrayList<ProductSerialNoMapping> createDeliveryNoteOfRemaining(HashMap<Integer, ArrayList<ProductSerialNoMapping>> srPrDtl,HashMap<Integer, ArrayList<ProductSerialNoMapping>> dlPrDtl, Integer prCode){
		ArrayList<ProductSerialNoMapping> prDt  = new ArrayList<ProductSerialNoMapping>();
		
		for(Iterator<ProductSerialNoMapping> itr = srPrDtl.get(prCode).iterator();itr.hasNext();)
	      {
			boolean get = false;
			ProductSerialNoMapping dPrDl = (ProductSerialNoMapping) itr.next();
			for(Iterator<ProductSerialNoMapping> itr1 = dlPrDtl.get(prCode).iterator();itr1.hasNext();)
		      {
				ProductSerialNoMapping srPrDl = (ProductSerialNoMapping) itr1.next();
				
				if(dPrDl.getProSerialNo().equals(srPrDl.getProSerialNo())){
					itr.remove();
					get = true;
					break;
				}
				
		      }
			if(!get){
				ProductSerialNoMapping prDtl = new ProductSerialNoMapping();
				prDtl.setProSerialNo(dPrDl.getProSerialNo());
				prDtl.setNewAddNo(dPrDl.isNewAddNo());
				prDtl.setAvailableStatus(dPrDl.isNewAddNo());
				prDtl.setStatus(dPrDl.isStatus());
				prDt.add(prDtl);
			}
	      }
		
		return prDt;
	}
	
	private List<DeliveryLineItems> getUpdatedList1(HashMap<Integer, Double> reProQty,SalesOrder srOdr,HashMap<Integer, ArrayList<ProductSerialNoMapping>> prSerialLst)
	{
		ArrayList<DeliveryLineItems> arrGrn=new ArrayList<DeliveryLineItems>();
		
		for(int i=0;i<this.getDeliveryItems().size();i++)
		{
			for(Integer prId : reProQty.keySet()){
				if(this.getDeliveryItems().get(i).getProdId()==prId){
				
					
					DeliveryLineItems dli=new DeliveryLineItems();
					dli.setProdId(this.getDeliveryItems().get(i).getProdId());
					dli.setProdCategory(this.getDeliveryItems().get(i).getProdCategory());
					dli.setProdCode(this.getDeliveryItems().get(i).getProdCode());
					dli.setProdName(this.getDeliveryItems().get(i).getProdName());
					dli.setQuantity(reProQty.get(prId));
					dli.setPrice(this.getDeliveryItems().get(i).getPrice());
					dli.setVatTax(this.getDeliveryItems().get(i).getVatTax());
					dli.setServiceTax(this.getDeliveryItems().get(i).getServiceTax());
					dli.setProdPercDiscount(this.getDeliveryItems().get(i).getProdPercDiscount());
					dli.setTotalAmount(this.getDeliveryItems().get(i).getTotalAmount());
					dli.setWareHouseName(this.getDeliveryItems().get(i).getWareHouseName());
					dli.setStorageLocName(this.getDeliveryItems().get(i).getStorageLocName());
					dli.setStorageBinName(this.getDeliveryItems().get(i).getStorageBinName());
					
					/**Date 4-5-2019 by Amol
					 * set product serial number to solve the issue of product quantity not update in delivery note
					 * issue raised by Sonu
					 */
					dli.setProductSrNumber(this.getDeliveryItems().get(i).getProductSrNumber());
					/**
					 * 
					 */
					HashMap<Integer, ArrayList<ProductSerialNoMapping>> prlist = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
					if(prSerialLst.containsKey(this.getDeliveryItems().get(i).getProdId())){
						prlist.put(0,prSerialLst.get(this.getDeliveryItems().get(i).getProdId()) );
					}
					
					dli.setProSerialNoDetails(prlist);
					arrGrn.add(dli);
				}
			}
		}
		return arrGrn;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	
	@GwtIncompatible
	public void sendSMStoClient() {
		Logger logger=Logger.getLogger("Sms Logger");
		logger.log(Level.SEVERE,"In side SMS to client");

//		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",this.getCompanyId()).filter("status", true).first().now();
		DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
//		logger.log(Level.SEVERE,"smsconfig "+smsconfig);
//		if(smsconfig!=null){
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",this.getCompanyId()).filter("event","Delivery Note Approved").filter("status",true).first().now();
			logger.log(Level.SEVERE,"smsEntity "+smsEntity);

			/**
			 * @author Vijay Date :- 10-08-2021
			 * Des :- added below method to check customer DND status if customer DND status is Active 
			 * then SMS will not send to customer
			 */
			ServerAppUtility serverapputility = new ServerAppUtility();
			boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(this.getCinfo().getCount(), companyId);
			if(!dndstatusFlag){
				
			if(smsEntity!=null){
				String templateMsgWithBraces = smsEntity.getMessage();
				logger.log(Level.SEVERE,"templateMsgWithBraces "+templateMsgWithBraces);

				String deliveryDate = fmt1.format(this.getDeliveryDate());
				String customerName = templateMsgWithBraces.replace("{CustomerName}", this.getCinfo().getFullName()+"");
				String orderId = customerName.replace("{OrderID}",this.getSalesOrderCount()+"");
				String deliveryDatemsg = orderId.replace("{DeliveryDate}",deliveryDate);
				String lrnNumber = deliveryDatemsg.replace("{LRNo}", this.getLrNumber());
				String deliveryPersonName = lrnNumber.replace("{DeliveryPerson}", this.getDriverName());
				String actualMsg = deliveryPersonName.replace("{CellNo}", this.getDriverPhone()+"");
				if(Slick_Erp.businessUnitName!=null)
				actualMsg = actualMsg.replace("{CompanyName}", Slick_Erp.businessUnitName);

				logger.log(Level.SEVERE,"actualMsg "+actualMsg);

				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				int value =  smsimpl.sendSmsToClient(actualMsg, this.getCinfo().getCellNumber(), smsconfig.getAccountSID(),  smsconfig.getAuthToken(),  smsconfig.getPassword(), this.getCompanyId());
				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.SALESMODULE,AppConstants.DELIVERYNOTE,smsEntity.getEvent(),this.getCompanyId(),actualMsg,this.getCinfo().getCellNumber(),false);
				logger.log(Level.SEVERE,"after sendMessage method");
				
			}
		}
//		}
	}
	
}
