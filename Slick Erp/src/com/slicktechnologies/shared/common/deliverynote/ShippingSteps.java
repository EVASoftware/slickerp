package com.slicktechnologies.shared.common.deliverynote;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ShippingSteps implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8983335941797856039L;
	
	/******************************************Applicability Attributes*************************************/
	
	protected String shippingStepCode;
	protected String shippingStepName;
	protected boolean completionStatus;
	protected String shippingStepMandatory;
	protected int shippingStepNumber;
	
	/*****************************************Getters And Setters********************************************/
	
	public String getShippingStepCode() {
		return shippingStepCode;
	}
	public void setShippingStepCode(String shippingStepCode) {
		if(shippingStepCode!=null){
			this.shippingStepCode = shippingStepCode.trim();
		}
	}
	
	public String getShippingStepName() {
		return shippingStepName;
	}
	public void setShippingStepName(String shippingStepName) {
		if(shippingStepName!=null){
			this.shippingStepName = shippingStepName.trim();
		}
	}
	
	public Boolean isCompletionStatus() {
		return completionStatus;
	}
	public void setCompletionStatus(boolean completionStatus) {
		this.completionStatus = completionStatus;
	}
	
	public String getShippingStepMandatory() {
		return shippingStepMandatory;
	}
	public void setShippingStepMandatory(String shippingStepMandatory) {
		if(shippingStepMandatory!=null){
			this.shippingStepMandatory = shippingStepMandatory.trim();
		}
	}
	
	public int getShippingStepNumber() {
		return shippingStepNumber;
	}
	public void setShippingStepNumber(int shippingStepNumber) {
		this.shippingStepNumber = shippingStepNumber;
	}
	
	
	
	

}
