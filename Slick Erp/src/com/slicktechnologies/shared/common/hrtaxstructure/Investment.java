package com.slicktechnologies.shared.common.hrtaxstructure;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class Investment extends SuperModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -682949418960482037L;
	/**
	 * 
	 */
	@Index
	String year;
	@Index
	String sectionName;
	@Index
	double upperLimit;
	@Index
	Boolean status;
	 
	public Investment() {
		// TODO Auto-generated constructor stub
		sectionName = "";
		status = false;
	}
	
	
	public String getSectionName() {
		return sectionName;
	}


	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}


	public double getUpperLimit() {
		return upperLimit;
	}


	public void setUpperLimit(double upperLimit) {
		this.upperLimit = upperLimit;
	}


	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}


	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return sectionName;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
