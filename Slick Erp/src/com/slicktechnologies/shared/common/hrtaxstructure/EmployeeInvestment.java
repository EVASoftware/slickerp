package com.slicktechnologies.shared.common.hrtaxstructure;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class EmployeeInvestment extends SuperModel{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3993481292770796643L;
	/**
	 * 
	 */
	@Index
	int empId;
	@Index
	String empName;
	@Index
	long empCellNo;
	ArrayList<EmployeeInvestmentBean> list;
	Double previousCompanyCTC;
	Double previousCompanyTax;

	public EmployeeInvestment(){
		 list = new ArrayList<EmployeeInvestmentBean>();
		 empName = "";
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public long getEmpCellNo() {
		return empCellNo;
	}

	public void setEmpCellNo(long empCellNo) {
		this.empCellNo = empCellNo;
	}

	public ArrayList<EmployeeInvestmentBean> getList() {
		return list;
	}

	public void setList(List<EmployeeInvestmentBean> list) {
		ArrayList<EmployeeInvestmentBean> empList = new ArrayList<EmployeeInvestmentBean>();
		empList.addAll(list);
		this.list = empList;
	}

	public Double getPreviousCompanyCTC() {
		return previousCompanyCTC;
	}

	public void setPreviousCompanyCTC(Double previousCompanyCTC) {
		this.previousCompanyCTC = previousCompanyCTC;
	}

	public Double getPreviousCompanyTax() {
		return previousCompanyTax;
	}

	public void setPreviousCompanyTax(Double previousCompanyTax) {
		this.previousCompanyTax = previousCompanyTax;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
