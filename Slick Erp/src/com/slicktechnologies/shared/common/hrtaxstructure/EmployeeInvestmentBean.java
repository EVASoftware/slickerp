package com.slicktechnologies.shared.common.hrtaxstructure;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class EmployeeInvestmentBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9068026469532213528L;
	/**
	 * 
	 */
	String year;
	String sectionName;
	double upperLimit;
	double declaredInvestment;
	double actualInvestment;
	
	public EmployeeInvestmentBean() {
		// TODO Auto-generated constructor stub
		year = "";
		sectionName = "";
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public double getUpperLimit() {
		return upperLimit;
	}

	public void setUpperLimit(double upperLimit) {
		this.upperLimit = upperLimit;
	}

	public double getDeclaredInvestment() {
		return declaredInvestment;
	}

	public void setDeclaredInvestment(double declaredInvestment) {
		this.declaredInvestment = declaredInvestment;
	}

	public double getActualInvestment() {
		return actualInvestment;
	}

	public void setActualInvestment(double actualInvestment) {
		this.actualInvestment = actualInvestment;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
}
