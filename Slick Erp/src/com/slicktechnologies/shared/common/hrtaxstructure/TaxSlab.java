package com.slicktechnologies.shared.common.hrtaxstructure;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class TaxSlab extends SuperModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7256810548861940995L;
	/**
	 * 
	 */
	@Index
	String year;
	@Index
	String category;
	@Index
	double upperLimit;
	@Index
	double lowerLimit;
	@Index
	double tax;
	@Index
	double educationTax;
	@Index
	double hsEducationTax;
	@Index
	Boolean  status;
	@Index
	double slabMaxTax;
	
	public TaxSlab(){
		year = "";
		category = "";
		status = false;
	}
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public double getUpperLimit() {
		return upperLimit;
	}


	public void setUpperLimit(double upperLimit) {
		this.upperLimit = upperLimit;
	}


	public double getLowerLimit() {
		return lowerLimit;
	}


	public void setLowerLimit(double lowerLimit) {
		this.lowerLimit = lowerLimit;
	}


	public double getTax() {
		return tax;
	}


	public void setTax(double tax) {
		this.tax = tax;
	}


	public double getEducationTax() {
		return educationTax;
	}


	public void setEducationTax(double educationTax) {
		this.educationTax = educationTax;
	}


	public double getHsEducationTax() {
		return hsEducationTax;
	}


	public void setHsEducationTax(double hsEducationTax) {
		this.hsEducationTax = hsEducationTax;
	}


	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	public double getSlabMaxTax() {
		return slabMaxTax;
	}


	public void setSlabMaxTax(double slabMaxTax) {
		this.slabMaxTax = slabMaxTax;
	}


}
