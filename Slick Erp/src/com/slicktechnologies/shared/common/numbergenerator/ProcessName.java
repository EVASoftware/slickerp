package com.slicktechnologies.shared.common.numbergenerator;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.common.helperlayer.Config;

@Entity
public class ProcessName extends SuperModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8535429285025038193L;

	/*****************************************Applicability Attributes*************************************/
	
	@Index
	protected String processName;
	@Index
	protected boolean status;
	
	/*****************************************Constructor*************************************/
	
	public ProcessName() {
		super();
		processName="";
	}
	
	/******************************************Getters And Setters*****************************************/
	
	public String getProcessName(){
		return processName;
	}

	public void setProcessName(String processName) {
		if(processName!=null){
			this.processName = processName.trim();
		}
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	/******************************************Overridden Method*****************************************/

	@Override
	public boolean isDuplicate(SuperModel m) 
	{
		ProcessName obj=(ProcessName) m;
		String name = obj.getProcessName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.processName.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
		if(obj.id==null)
		{
			if(name.equals(currentName))
				return true;
			
			else
				return false;
		}
		//while updating a Old object
		else
		{
			if(obj.id.equals(id))
				return false;
			
			else if (name.equals(currentName))
				return true;
			
			else
				return false;	
		}			
	}

	@Override
	public String toString() {
		return processName.toString();
	}

	public static void initializeProcessName(ObjectListBox<ProcessName> processName)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		
		querry.setQuerryObject(new ProcessName());
		processName.MakeLive(querry);
	}
	
	

	
}
