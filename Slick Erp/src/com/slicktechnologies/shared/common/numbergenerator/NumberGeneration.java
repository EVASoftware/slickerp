package com.slicktechnologies.shared.common.numbergenerator;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;

@Entity
public class NumberGeneration extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4985662200978446980L;
	
	/***************************************Applicability Attributes******************************************/
	@Index 
	protected String processName;
	@Index 
	protected long number;
	protected String remark;
	@Index 
	protected boolean status;
	Date lastUpdatedDate; //Ashwini Patil Date:7-12-2023
	String updatedBy; //Ashwini Patil Date:7-12-2023
	
	

	/*******************************************Constructor*************************************************/
	
	public NumberGeneration() {
		super();
		
		processName = "";
		remark = "";
		updatedBy="";
		
	}

/*********************************************Getters And Setters**********************************************/
	
	public String getProcessName() {
		return processName;
	}
	
	public void setProcessName(String processName) {
		if(processName!=null)
			{
			this.processName = processName.trim();
			}
	}
	
	public Long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		if(remark!=null)
		
		{
			this.remark = remark.trim();
		}
	}
	
	public Boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
/*********************************************Overridden Method**********************************************/

	/**
	 * @author Vijay Date :- 11-08-2021
	 * Des :- Updated code to duplicate number genration not allow
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		
		NumberGeneration entity = (NumberGeneration) m;
		String name = entity.getProcessName().trim();
		String curname=this.processName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
		
	}

	

}
