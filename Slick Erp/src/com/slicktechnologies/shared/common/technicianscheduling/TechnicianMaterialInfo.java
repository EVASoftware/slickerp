package com.slicktechnologies.shared.common.technicianscheduling;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class TechnicianMaterialInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1753369771037056177L;
	
	protected String productName;
	protected String warehouseName;
	protected String storageLocation;
	protected String storageBin;
	protected double quantity;
	
	protected int productId;

	public TechnicianMaterialInfo(){
		
		productName="";
		warehouseName="";
		storageLocation="";
		storageBin="";
	}




	public String getWarehouseName() {
		return warehouseName;
	}


	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}


	public String getStorageLocation() {
		return storageLocation;
	}


	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}


	public String getStorageBin() {
		return storageBin;
	}


	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}


	public double getQuantity() {
		return quantity;
	}


	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}




	public int getProductId() {
		return productId;
	}




	public void setProductId(int productId) {
		this.productId = productId;
	}
	


}
