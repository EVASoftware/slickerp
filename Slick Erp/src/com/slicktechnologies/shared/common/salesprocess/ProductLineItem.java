package com.slicktechnologies.shared.common.salesprocess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

// TODO: Auto-generated Javadoc
/**
 * The Attributes of Product comes in many location, So we need to have a Super Class 
 * to Capture common attributes.
 */

@Embed
public class ProductLineItem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6611992338609900985L;
	
	/************************************Attributes******************************************************/

	/** The service product */
	private ServiceProduct serviceProduct;
	
	/** The item product */
	private ItemProduct itemProduct;
	
	
	protected int prodId;
	
	/** The prod category. */
	protected String prodCategory;
	
	/** The prod code. */
	protected String prodCode;
	
	/** The prod name. */
	protected String prodName;
	
	/** The quantity. */
	protected double quantity;
	
	/** The price. */
	protected double price;
	
	/** The vat tax. */
	protected Tax vatTax;
	
	/** The service tax. */
	protected Tax serviceTax;
	
	/** The Percentage discount */
	protected double prodPercDiscount;
	
	/** The total amout. */
	protected double totalAmount;
	
	protected String unitOfMeasurement;
	
	protected int orderDuration;
	
	protected int orderServices;
	
	protected String prodDesc1;
	protected String prodDesc2;
	
	
	String serviceTaxEdit;
	String vatTaxEdit;
	
	 double discountAmt; 
	 double flatDiscount;
	
	 /**
	  * This is added by rohan 
	  * Reason : Used for maintaining Uniqueness of product in case of multiple product added in contract
	  * Date: 30/09/2016   
	  */
	 	int productSrNumber;
	 
	 /**
	  * ends here 
	  */ 
	 	
	 	
	 	/**
		  * This is added by rohan Bhagde
		  * Reason : Used for maintaining area in the billing table  
		  * Date: 30/09/2016   
		  */
	 	  String area = "NA";
		 
		 /**
		  * ends here 
		  */ 	
	 	  
 	 /*
 	  *  		 * nidhi
 		 * 24-06-2017
 		 * for save Product Available Quantity
 		 */
 		protected double prodAvailableQuantity;
 		
 		/*
 		 * end
 		 */
 		
	/**
	 * nidhi 8-08-2018 for map model no , serial no
	 */
	protected String proModelNo;
	protected String proSerialNo;
	/**
	 * nidhi 21-08-2018 for map serial number details
	 */
	@EmbedMap
	@Serialize
	protected HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails = new HashMap<Integer, ArrayList<ProductSerialNoMapping>>();
	/** date 15.04.2019 added by komal for nbhc service branches save **/
	@EmbedMap
	@Serialize
	protected HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchesInfo;
	
	String manpower;
	/** Date 18-08-2020 by Vijay for sasha To calculate No of man power and Man Days **/
	String billType;
	
	/**
	 * @author Anil @since 12-11-2021
	 * billProductName
	 */
	String billProductName;
	
	/**
	 * @author Anil @since 13-11-2021
	 * Capturing sales order summary
	 * Sunrise SCM
	 */
	@EmbedMap
	@Serialize
	protected HashMap<String,ArrayList<SalesAnnexureType>> salesAnnexureMap;
	
	public HashMap<String, ArrayList<SalesAnnexureType>> getSalesAnnexureMap() {
		return salesAnnexureMap;
	}

	public void setSalesAnnexureMap(
			HashMap<String, ArrayList<SalesAnnexureType>> salesAnnexureMap) {
		this.salesAnnexureMap = salesAnnexureMap;
	}
	
	public ProductLineItem() {
		proModelNo = "";
		proSerialNo = "";
		/** date 15.04.2019 added by komal for nbhc service branches save **/
		serviceBranchesInfo = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
		manpower ="";
		billType ="";
		billProductName="";
		salesAnnexureMap=new HashMap<String,ArrayList<SalesAnnexureType>>();
	}
 		  
 		  
 		
 		/*************************************Getters And Setters***********************************************/
	
 		/*
 		 * Nidhi
 		 * 24-06-2017
 		 */
 		public double getProdAvailableQuantity() {
 			return prodAvailableQuantity;
 		}

 		public void setProdAvailableQuantity(double prodAvailableQuantity) {
 			this.prodAvailableQuantity = prodAvailableQuantity;
 		}
 		
 		/*
 		 * End
 		 */
 		
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public int getProdId() {
		return prodId;
	}

	/**
	 * Sets the prod id.
	 *
	 * @param prodId the new prod id
	 */
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}
	
	/**
	 * Gets the prod category.
	 *
	 * @return the prod category
	 */
	public String getProdCategory() {
		return prodCategory;
	}
	
	/**
	 * Sets the prod category.
	 *
	 * @param prodCategory the new prod category
	 */
	public void setProdCategory(String prodCategory) {
		this.prodCategory = prodCategory;
	}
	
	/**
	 * Gets the prod code.
	 *
	 * @return the prod code
	 */
	public String getProdCode() {
		return prodCode;
	}
	
	/**
	 * Sets the prod code.
	 *
	 * @param prodCode the new prod code
	 */
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}
	
	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	public String getProdName() {
		return prodName;
	}
	
	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public double getQuantity() {
		return quantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Tax getVatTax() {
		return vatTax;
	}
	
	/**
	 * Sets the vat tax.
	 *
	 * @param vatTax the new vat tax
	 */
	public void setVatTax(Tax vatTax) {
		this.vatTax = vatTax;
	}
	
	/**
	 * Gets the service tax.
	 *
	 * @return the service tax
	 */
	public Tax getServiceTax() {
		return serviceTax;
	}
	
	/**
	 * Sets the service tax.
	 *
	 * @param serviceTax the new service tax
	 */
	public void setServiceTax(Tax serviceTax) {
		this.serviceTax = serviceTax;
	}
	
	/**
	 * Gets the total amout.
	 *
	 * @return the total amout
	 */
	public double getTotalAmount() {
		return totalAmount;
	}
	
	/**
	 * Sets the total amout.
	 *
	 * @param totalAmout the new total amout
	 */
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * Gets the prod percentage discount.
	 *
	 * @return the total amout
	 */
	public Double getProdPercDiscount() {
		return prodPercDiscount;
	}
	
	
	/**
	 * Sets the product percentage discount.
	 *
	 * @param prodPercentageDiscount the new product percentage discount
	 */


	public void setProdPercDiscount(Double prodPercDiscount) {
		if(prodPercDiscount!=null){
			this.prodPercDiscount = prodPercDiscount;
		}
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		if(unitOfMeasurement!=null){
			this.unitOfMeasurement = unitOfMeasurement.trim();
		}
	}

	public int getOrderDuration() {
		return orderDuration;
	}

	public void setOrderDuration(int orderDuration) {
		this.orderDuration = orderDuration;
	}

	public int getOrderServices() {
		return orderServices;
	}

	public void setOrderServices(int orderServices) {
		this.orderServices = orderServices;
	}

	public String getProdDesc1() {
		return prodDesc1;
	}

	public void setProdDesc1(String prodDesc1) {
		if(prodDesc1!=null){
			this.prodDesc1 = prodDesc1.trim();
		}
	}

	public String getProdDesc2() {
		return prodDesc2;
	}

	public void setProdDesc2(String prodDesc2) {
		if(prodDesc2!=null){
			this.prodDesc2 = prodDesc2.trim();
		}
	}
	
	
	/**
	 * Gets the prduct.
	 * @return the prduct
	 */
	public SuperProduct getPrduct()
	{

		if(serviceProduct==null)
			return itemProduct;
		
		else if(itemProduct==null)
			return serviceProduct;
		else
		{
			return null;
		}	
	}
	
	
	/**
	 * Sets the prduct.
	 *
	 * @param product the new prduct
	 */
	public void setPrduct(SuperProduct product) 
	{
		if(product instanceof ServiceProduct)
			serviceProduct=(ServiceProduct) product;
		else if(product instanceof ItemProduct)
			itemProduct=(ItemProduct) product;
		System.out.println("Service Product IS "+serviceProduct);
		System.out.println("Item Product IS "+serviceProduct);
	}

	public String getServiceTaxEdit() {
		return serviceTaxEdit;
	}

	public void setServiceTaxEdit(String serviceTaxEdit) {
		this.serviceTaxEdit = serviceTaxEdit;
	}

	public String getVatTaxEdit() {
		return vatTaxEdit;
	}

	public void setVatTaxEdit(String vatTaxEdit) {
		this.vatTaxEdit = vatTaxEdit;
	}

	public double getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public double getFlatDiscount() {
		return flatDiscount;
	}

	public void setFlatDiscount(double flatDiscount) {
		this.flatDiscount = flatDiscount;
	}

	public int getProductSrNumber() {
		return productSrNumber;
	}

	public void setProductSrNumber(int productSrNumber) {
		this.productSrNumber = productSrNumber;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * nidhi
	 * 8-08-2018
	 * 
	 * @return
	 */
	public String getProModelNo() {
		if(this.proModelNo != null){
			return this.proModelNo;
		}else{
			return "";
		}
	}


	public void setProModelNo(String proModelNo) {
		this.proModelNo = proModelNo;
	}


	public String getProSerialNo() {
		if(this.proSerialNo != null){
			return this.proSerialNo;
		}else{
			return "";
		}
	}


	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}

	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}

	public HashMap<Integer, ArrayList<BranchWiseScheduling>> getServiceBranchesInfo() {
		return serviceBranchesInfo;
	}

	public void setServiceBranchesInfo(
			HashMap<Integer, ArrayList<BranchWiseScheduling>> serviceBranchesInfo) {
		this.serviceBranchesInfo = serviceBranchesInfo;
	}



	public String getManpower() {
		return manpower;
	}


	public void setManpower(String manpower) {
		this.manpower = manpower;
	}



	public String getBillType() {
		return billType;
	}



	public void setBillType(String billType) {
		this.billType = billType;
	}



	public String getBillProductName() {
		return billProductName;
	}



	public void setBillProductName(String billProductName) {
		this.billProductName = billProductName;
	}

	
	
	/**
	 * ends here
	 */
	
	
	
	
}
