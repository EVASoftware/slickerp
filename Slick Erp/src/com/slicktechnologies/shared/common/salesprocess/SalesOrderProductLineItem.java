package com.slicktechnologies.shared.common.salesprocess;

import java.util.Date;
import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.productlayer.Tax;

// TODO: Auto-generated Javadoc
/**
 * Product detail for Sales Order Products.
 */
@Embed
public class SalesOrderProductLineItem extends ProductLineItem {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5651272720951554625L;

/** **********************************************Attributes*********************************************. */
	
	    /** The payment percent of product total */
    	protected double paymentPercent;
	    
    	/** The base billing amount. */
	    protected double baseBillingAmount;
	    
	    protected int indexVal;
    	
 
	    protected int orderId;
		protected int biilingId;
	    
		/** The base billing amount Taken by rohan. */
	    protected double basePaymentAmount;
	    
	    /** rohan added  this code for HSN code **/
	    protected String hsnCode;
	    
	    /**
		 * Date 06/06/2018 By vijay
		 * Des :- for service product warranty days
		 * Requirement :- Neatedge Services
		 */
			protected int WarrantyPeriod; 
		/**
		 * ends here	
		 */
			
		/**
		 * Date 09-04-2019 by Vijay 
		 * Des :- NBHC CCPM must need billing base amount in invoice
		 * so i am storing billing base amount in invoice for the revenue loss	
		 */
		protected double billingDocBaseAmount; 	
		
		/** date 04.09.2019 added by komal to store management fees and total seperately for cnc bill **/
		protected double managementFees;
		
		protected double totalExcludingManagementFees;
		/** Date 16-09-2020 to set ctc of each staff amount from CNC ***/
		protected double ctcAmount;
		
		protected Tax refproductmasterTax1;
		protected Tax refproductmasterTax2;
		
		protected boolean payableamtupdateFlag;
		
		protected String typeOfOrder;
			
/**
 * ***************************************Constructor**********************************************.
 */	    
		/**
		 * Instantiates a new sales order product info.
		 */
		public SalesOrderProductLineItem() 
		{
			super();
			hsnCode="";
			
			refproductmasterTax1 = new Tax();
			refproductmasterTax2 = new Tax();
			payableamtupdateFlag = false;
			typeOfOrder ="";
		}

/**
 * **************************************************************************************************.
 *
 * @return the payment percent
 */
		
/*****************************************Getters And Setters**********************************************/		
		
		
		

		/**
		 * Gets the payment percent.
		 *
		 * @return the payment percent
		 */
		public double getPaymentPercent() {
			return paymentPercent;
		}

		public String getHsnCode() {
			return hsnCode;
		}

		public void setHsnCode(String hsnCode) {
			this.hsnCode = hsnCode;
		}

		public double getBasePaymentAmount() {
			return basePaymentAmount;
		}

		public void setBasePaymentAmount(double basePaymentAmount) {
			this.basePaymentAmount = basePaymentAmount;
		}

		/**
		 * Sets the payment percent.
		 *
		 * @param paymentPercent the new payment percent
		 */
		public void setPaymentPercent(double paymentPercent) {
			this.paymentPercent = paymentPercent;
		}

		/**
		 * Gets the base billing amount.
		 *getBaseBillingAmount
		 * @return the base billing amount
		 */
		public double getBaseBillingAmount() {
			return baseBillingAmount;
		}

		/**
		 * Sets the base billing amount.
		 *
		 * @param baseBillingAmount the new base billing amount
		 */
		public void setBaseBillingAmount(double baseBillingAmount) {
			this.baseBillingAmount = baseBillingAmount;
		}

		public int getIndexVal() {
			return indexVal;
		}

		public void setIndexVal(int indexVal) {
			this.indexVal = indexVal;
		}

		public int getOrderId() {
			return orderId;
		}

		public void setOrderId(int orderId) {
			this.orderId = orderId;
		}

		public int getBiilingId() {
			return biilingId;
		}

		public void setBiilingId(int biilingId) {
			this.biilingId = biilingId;
		}
		
		public int getWarrantyPeriod() {
			return WarrantyPeriod;
		}

		public void setWarrantyPeriod(int warrantyPeriod) {
			WarrantyPeriod = warrantyPeriod;
		}

		public double getBillingDocBaseAmount() {
			return billingDocBaseAmount;
		}

		public void setBillingDocBaseAmount(double billingDocBaseAmount) {
			this.billingDocBaseAmount = billingDocBaseAmount;
		}

		public double getManagementFees() {
			return managementFees;
		}

		public void setManagementFees(double managementFees) {
			this.managementFees = managementFees;
		}

		public double getTotalExcludingManagementFees() {
			return totalExcludingManagementFees;
		}

		public void setTotalExcludingManagementFees(double totalExcludingManagementFees) {
			this.totalExcludingManagementFees = totalExcludingManagementFees;
		}

		public double getCtcAmount() {
			return ctcAmount;
		}

		public void setCtcAmount(double ctcAmount) {
			this.ctcAmount = ctcAmount;
		}

		public Tax getRefproductmasterTax1() {
			return refproductmasterTax1;
		}

		public void setRefproductmasterTax1(Tax refproductmasterTax1) {
			this.refproductmasterTax1 = refproductmasterTax1;
		}

		public Tax getRefproductmasterTax2() {
			return refproductmasterTax2;
		}

		public void setRefproductmasterTax2(Tax refproductmasterTax2) {
			this.refproductmasterTax2 = refproductmasterTax2;
		}
		
		
		public boolean isPayableamtupdateFlag() {
			return payableamtupdateFlag;
		}

		public void setPayableamtupdateFlag(boolean payableamtupdateFlag) {
			this.payableamtupdateFlag = payableamtupdateFlag;
		}

		public String getTypeOfOrder() {
			return typeOfOrder;
		}

		public void setTypeOfOrder(String typeOfOrder) {
			this.typeOfOrder = typeOfOrder;
		}
		
		
		

		
/********************************************************************************************************/		



}
