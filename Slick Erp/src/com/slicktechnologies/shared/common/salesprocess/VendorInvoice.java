package com.slicktechnologies.shared.common.salesprocess;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.OnSave;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

@Entity
public class VendorInvoice extends Invoice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2842249809428139932L;
	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		if(getStatus().equals(Invoice.APPROVED))
		{
			this.createPaymentDetails();
			
			/**
			 * Date : 18-04-2018 BY ANIL
			 * Creating AMC contract with service for HVAC on approval of Sales Invoice
			 * Rohan/Nitin Sir
			 */
			if(this.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
				SalesOrder salesOrder=ofy().load().type(SalesOrder.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
				if(salesOrder!=null){
					ArrayList<SalesLineItem> amcLineItem=new ArrayList<SalesLineItem>();
					for(SalesLineItem item:salesOrder.getItems()){
						if(item.getServiceProductId()!=0&&item.getServiceProductName()!=null&&!item.getServiceProductName().equals("")){
							if(item.getWarrantyDuration()!=0){
								amcLineItem.add(item);
							}
						}
					}
					if(amcLineItem.size()!=0){
						this.createAmcContract(salesOrder,amcLineItem);
					}
				}
			}
			
			/**
			 * End
			 */
		}
		
		System.out.println("Querying Processname");
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGINV).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag =0;
		
		if(processConfig!=null){
			System.out.println("Process Config Not Null");
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				System.out.println("----------------------------------------------------------");
				System.out.println("One===== "+processConfig.getProcessList().get(i).getProcessType());
				System.out.println("One===== "+processConfig.getProcessList().get(i).isStatus());
				System.out.println("----------------------------------------------------------");
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			accountingInterface(this,"Vendor Invoice");
		}
		
		ArrayList<SalesOrderProductLineItem> salesOrderProductLineItemList=new ArrayList<SalesOrderProductLineItem>();
		for (int i = 0; i < this.getSalesOrderProducts().size(); i++) {
			if(this.getSalesOrderProducts().get(i).getPaymentPercent()<100){
				salesOrderProductLineItemList.add(this.getSalesOrderProducts().get(i));
			}
		}
		/** date 14.7.2018 added by komal for AR invoices **/
		if(salesOrderProductLineItemList.size()!=0 && isPO && this.accountType.equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
			this.createInvoiceDocument(salesOrderProductLineItemList);
			System.out.println("Sales product inside Komal");
		}
		/**Date 31-1-2020 this method is Commented by Amol 
		 * for the Multiple Bill generate Issue at the time of Invoice Submition
		 *  Raised by Ashwini for OOLITE Pest Control
		 * 
		 */
//		else{
//			System.out.println("Create Billing Document Condition"+salesOrderProductLineItemList.size());
//			createBillingDocument(salesOrderProductLineItemList);
//		}
		
	}
	@GwtIncompatible
	private void createInvoiceDocument(ArrayList<SalesOrderProductLineItem> salesOrderProductLineItemList) {

		List<SalesOrderProductLineItem> salesProductsLis = updateProductTable(salesOrderProductLineItemList);
		ArrayList<SalesOrderProductLineItem> salesProductsArr = new ArrayList<SalesOrderProductLineItem>();
		salesProductsArr.addAll(salesProductsLis);
		VendorInvoice billdetails = new VendorInvoice();
		billdetails.setInvoiceType(AppConstants.CREATETAXINVOICE);
		billdetails.setPersonInfo(this.getPersonInfo());
		billdetails.setContractCount(this.getContractCount());
		billdetails.setTotalSalesAmount(this.getTotalSalesAmount());
		billdetails.setSalesOrderProducts(salesProductsArr);
		billdetails.setInvoiceDate(new Date());
		billdetails.setPaymentDate(new Date());
		billdetails.setArrPayTerms(this.getArrPayTerms());
		billdetails.setContractStartDate(this.getContractStartDate());
		billdetails.setContractEndDate(this.getContractEndDate());
		List<ContractCharges> taxesLis = updateTaxesTable(salesProductsLis);
		ArrayList<ContractCharges> arrcc = new ArrayList<ContractCharges>();
		arrcc.addAll(taxesLis);
		billdetails.setBillingTaxes(arrcc);
		if (this.getCompanyId() != null)
			billdetails.setCompanyId(this.getCompanyId());
//		if (this.getBillingCategory() != null)
//			billdetails.setBillingCategory(model.getBillingCategory());
//		if (model.getBillingType() != null)
//			billdetails.setBillingType(model.getBillingType());
//		if (model.getBillingGroup() != null)
//			billdetails.setBillingGroup(model.getBillingGroup());
		billdetails.setApproverName(this.getApproverName());
		billdetails.setEmployee(this.getEmployee());
		billdetails.setBranch(this.getBranch());
		if (this.getInvoiceDate() != null)
			billdetails.setInvoiceDate(this.getInvoiceDate());
		if (this.getPaymentDate() != null)
			billdetails.setPaymentDate(this.getPaymentDate());

		billdetails.setStatus(BillingDocument.CREATED);
		billdetails.setOrderCformStatus(this.getOrderCformStatus());
		billdetails.setOrderCformPercent(this.getOrderCformPercent());
		billdetails.setOrderCreationDate(this.getOrderCreationDate());
		billdetails.setTypeOfOrder(this.getTypeOfOrder());
		billdetails.setAccountType(this.getAccountType());
		/**
		 * Date : 22-11-2017 BY ANIL
		 */
		System.out.println("OLD NUM RANGE : "+this.getNumberRange());
		if(this.getNumberRange()!=null){
			billdetails.setNumberRange(this.getNumberRange());
		}
		System.out.println("NEW NUM RANGE : "+billdetails.getNumberRange());
		if(this.getSegment()!=null){
			billdetails.setSegment(this.getSegment());
		}
		if(this.getRefNumber()!=null){
			billdetails.setRefNumber(this.getRefNumber());
		}
		double totBillAmt=0;
		double taxAmt=0;
		for(SalesOrderProductLineItem item:salesProductsLis){
			totBillAmt=totBillAmt+item.getBasePaymentAmount();
		}
		for(ContractCharges chrg:billdetails.getBillingTaxes()){
			taxAmt=taxAmt+chrg.getPayableAmt();
		}
		//billdetails.setTotalAmount(totBillAmt);
		billdetails.setTotalSalesAmount(totBillAmt);
		billdetails.setFinalTotalAmt(totBillAmt);
		double totalamtincludingtax=totBillAmt+taxAmt;
		billdetails.setTotalAmtIncludingTax(totalamtincludingtax);
//		billdetails.setGrandTotalAmount(totalamtincludingtax);
		billdetails.setTotalBillingAmount(totalamtincludingtax);
		billdetails.setNetPayable(totalamtincludingtax);
		billdetails.setComment("This is created from Partial Invoice. Reference Invoice No : "+this.getCount());
		billdetails.setBillingPeroidFromDate(this.getBillingPeroidFromDate());
		billdetails.setBillingPeroidToDate(this.getBillingPeroidToDate());
		billdetails.setInvoiceAmount(totalamtincludingtax);
		billdetails.setInvoiceCategory(this.getInvoiceCategory());
		billdetails.setInvoiceGroup(this.getInvoiceGroup());
		billdetails.setInvoiceType(this.getInvoiceConfigType());
		/**
		 * End
		 */
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(billdetails);
		
	}
	
	@OnSave
	@GwtIncompatible
	public void setReferenceNoAndReferenceDate(){
//		Console.log("inside setReferenceNoAndReferenceDate Method");
		if(id == null){
			PurchaseOrder po = ofy().load().type(PurchaseOrder.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
		if(po != null){
//			Console.log("inside set ref no and ref date");
			this.setReferenceDate(po.getReferenceDate());
//			this.setInvRefNumber(po.getRefOrderNO());//Ashwini Patil Date: 4-01-2022 As per Nitin sir's instruction removing this mapping as it is getting printed as vendor invoice number in po print
			this.setInvRefNumber("");
			ofy().save().entity(po).now();
		}
		}
	}
	
	
	
	
	
	
	
	/** date 15.10.2018 added by komal for unique invoice number **/
	@OnSave
	@GwtIncompatible
	private void getInvoiceNumber(){
		/** date 14.10.2018 added by komal for sasha (auto generate po number)**/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("VendorInvoice", "MonthwiseNumberGeneration", this.getCompanyId())){
		if(id == null){
			DecimalFormat df=new DecimalFormat("000");
			ServerAppUtility utility = new ServerAppUtility();
			PurchaseOrder po = ofy().load().type(PurchaseOrder.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
			String categoryShortName = "";
			if(po != null){
				if(po.getLOIGroup() != null && !(po.getLOIGroup().equals(""))){
				List<ConfigCategory> catList = ofy().load().type(ConfigCategory.class).filter("companyId", this.getCompanyId()).filter("internalType", CategoryTypes.getCategoryInternalType(Screen.PURCHASEORDERCATEGORY)).list();
				for(ConfigCategory cat : catList){
				if(cat != null){
					if(po.getLOIGroup().equals(cat.getCategoryName())){
						if(cat.getDescription()!=null && !(cat.getDescription().equals(""))){
							categoryShortName = cat.getDescription();
							break;
						}
					}
				 }
				}
				}
			}
			if(!categoryShortName.equals("")){
			String year = "";
			try {
				year = utility.getFinancialYearFormat(this.getCreationDate(), false);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**
			 * Date : 26-12-2018 BY ANIL
			 * adding month in custom generated invoice reference number
			 */
			SimpleDateFormat mmm = new SimpleDateFormat("MMM");
			mmm.setTimeZone(TimeZone.getTimeZone("IST"));
			String month = mmm.format(this.getInvoiceDate()).toUpperCase();
//			String number = "PV/"+categoryShortName+"/"+year;
			String number = "PV/"+categoryShortName+"/"+month+"/"+year;
			/**
			 * End
			 */
			ProcessConfiguration config = new ProcessConfiguration();
			config= ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", companyId)
					.filter("processName", number).first().now();
			int cnt = 1;
				if (config !=null) {

					for (int i = 0; i < config.getProcessList().size(); i++) {
						try{
							cnt = Integer.parseInt(config.getProcessList().get(i).getProcessType());
							cnt = cnt +1;
							config.getProcessList().get(i).setProcessType(cnt+"");
						}catch(Exception e){
							cnt = 0;
						}
					}

				}
				else{
					config = new ProcessConfiguration();
					config.setCompanyId(this.companyId);
					config.setProcessName(number);
					config.setConfigStatus(true);
					ArrayList<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
					ProcessTypeDetails type = new ProcessTypeDetails();
					type.setProcessName(number);
					type.setProcessType(cnt+"");
					type.setStatus(true);
					processList.add(type);		
					config.setProcessList(processList);
				}
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(config);
				if(cnt <= 999){
					this.setSegment("PV/"+categoryShortName.toUpperCase()+"/"+month.toUpperCase()+"/"+df.format(cnt)+"/"+year);
				}else{
					this.setSegment("PV/"+categoryShortName.toUpperCase()+"/"+month.toUpperCase()+"/"+cnt+"/"+year);
				}
					
			}
		  }
		}


	}

	
	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getBillingPeroidFromDate()!=null){
			this.setBillingPeroidFromDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidFromDate()));
		}
		if(this.getBillingPeroidToDate()!=null){
			this.setBillingPeroidToDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidToDate()));
		}
		if(this.getInvoiceDate()!=null){
			this.setInvoiceDate(dateUtility.setTimeMidOftheDayToDate(this.getInvoiceDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
		if(this.getPaymentDate()!=null){
			this.setPaymentDate(dateUtility.setTimeMidOftheDayToDate(this.getPaymentDate()));
		}
		if(this.id==null){
			if(this.getContractStartDate()!=null){
				this.setContractStartDate(dateUtility.setTimeMidOftheDayToDate(this.getContractStartDate()));
			}
			if(this.getContractEndDate()!=null){
				this.setContractEndDate(dateUtility.setTimeMidOftheDayToDate(this.getContractEndDate()));
			}
			if(this.getCreationDate()!=null){
				this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
			}
		}
		

	}
	/**
	 * ends here	
	 */

}
