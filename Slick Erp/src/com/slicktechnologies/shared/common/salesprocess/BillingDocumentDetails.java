package com.slicktechnologies.shared.common.salesprocess;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

// TODO: Auto-generated Javadoc
/**
 *  Represents billing document information corresponding to Billing Document
 */
@Embed
public class BillingDocumentDetails extends SuperModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7052255683931031643L;
	
	
	/*******************************************Attributes*********************************************/
	
	/** The bill id. */
	protected int billId;
	
	/** The billing date. */
	@Index
	protected Date billingDate;
	
	/** The billing category. */
	@Index
	protected String billingCategory;
	
	/** The billing type. */
	@Index
	protected String billingType;
	
	/** The billing group. */
	@Index
	protected String billingGroup;
	
	/** The bill amount. */
	protected double billAmount;
	
	/** The billstatus. */
	protected String billstatus;
	
	//**********anil channges **********
	/** The order id. */
	protected int orderId;
	
/**************************************************************************************************/

/**********************************Getters And Setters****************************************/
	
/**
 *
 *
 * @return the bill id
 */
	public Integer getBillId() {
		return billId;
	}


	public int getOrderId() {
	return orderId;
}


public void setOrderId(int orderId) {
	this.orderId = orderId;
}


	/**
	 * Sets the bill id.
	 *
	 * @param billId the new bill id
	 */
	public void setBillId(int billId) {
		this.billId = billId;
	}

	/**
	 * Gets the billing date.
	 *
	 * @return the billing date
	 */
	public Date getBillingDate() {
		return billingDate;
	}

	/**
	 * Sets the billing date.
	 *
	 * @param billingDate the new billing date
	 */
	public void setBillingDate(Date billingDate) {
		if(billingDate!=null)
			this.billingDate = billingDate;
	}
	
	
	/**
	 * Gets the billing category.
	 *
	 * @return the billing category
	 */
	public String getBillingCategory() {
		return billingCategory;
	}
	
	
	/**
	 * Sets the billing category.
	 *
	 * @param billingCategory the new billing category
	 */
	public void setBillingCategory(String billingCategory) {
		if(billingCategory!=null)
			this.billingCategory = billingCategory;
	}
	
	
	/**
	 * Gets the billing type.
	 *
	 * @return the billing type
	 */
	public String getBillingType() {
		return billingType;
	}
	
	
	/**
	 * Sets the billing type.
	 *
	 * @param billingType the new billing type
	 */
	public void setBillingType(String billingType) {
		if(billingType!=null)
			this.billingType = billingType;
	}
	
	
	/**
	 * Gets the billing group.
	 *
	 * @return the billing group
	 */
	public String getBillingGroup() {
		return billingGroup;
	}
	
	
	/**
	 * Sets the billing group.
	 *
	 * @param billingGroup the new billing group
	 */
	public void setBillingGroup(String billingGroup) {
		if(billingGroup!=null)
			this.billingGroup = billingGroup;
	}
	
	/**
	 * Gets the bill amount.
	 *
	 * @return the bill amount
	 */
	public Double getBillAmount() {
		return billAmount;
	}

	/**
	 * Sets the bill amount.
	 *
	 * @param billAmount the new bill amount
	 */
	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	/**
	 * Gets the billstatus.
	 *
	 * @return the billstatus
	 */
	public String getBillstatus() {
		return billstatus;
	}
	
	/**
	 * Sets the billstatus.
	 *
	 * @param billstatus the new billstatus
	 */
	public void setBillstatus(String billstatus) {
		if(billstatus!=null)
			this.billstatus = billstatus;
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
