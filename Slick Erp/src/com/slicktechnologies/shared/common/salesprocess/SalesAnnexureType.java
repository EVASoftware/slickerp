package com.slicktechnologies.shared.common.salesprocess;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Serialize;

@Serialize
public class SalesAnnexureType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3439857703790410052L;

	int soId;
	Date soDate;
	double soAmount;
	double budget;
	double payableAmount;
	
	String siteLocation;
	String carpetArea;
	String costCode;
	
	double igstAmt;
	double utgstAmt;
	double sgstAmt;
	double cgstAmt;
	
	double igstPer;
	double utgstPer;
	double sgstPer;
	double cgstPer;
	
	double grandTotal;
	
	String billProductName;
	
	

	public SalesAnnexureType() {
		super();
		// TODO Auto-generated constructor stub
	}




	public SalesAnnexureType(int soId, Date soDate, double soAmount,
			double budget, double payableAmount, String siteLocation,
			String carpetArea, String costCode, double igstAmt,
			double utgstAmt, double sgstAmt, double cgstAmt, double igstPer,
			double utgstPer, double sgstPer, double cgstPer, double grandTotal, String billProductName) {
		super();
		this.soId = soId;
		this.soDate = soDate;
		this.soAmount = soAmount;
		this.budget = budget;
		this.payableAmount = payableAmount;
		this.siteLocation = siteLocation;
		this.carpetArea = carpetArea;
		this.costCode = costCode;
		this.igstAmt = igstAmt;
		this.utgstAmt = utgstAmt;
		this.sgstAmt = sgstAmt;
		this.cgstAmt = cgstAmt;
		this.igstPer = igstPer;
		this.utgstPer = utgstPer;
		this.sgstPer = sgstPer;
		this.cgstPer = cgstPer;
		this.grandTotal = grandTotal;
		this.billProductName = billProductName;
	}




	public int getSoId() {
		return soId;
	}

	public void setSoId(int soId) {
		this.soId = soId;
	}

	public Date getSoDate() {
		return soDate;
	}

	public void setSoDate(Date soDate) {
		this.soDate = soDate;
	}

	public double getSoAmount() {
		return soAmount;
	}

	public void setSoAmount(double soAmount) {
		this.soAmount = soAmount;
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public double getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public String getSiteLocation() {
		return siteLocation;
	}

	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}

	public String getCarpetArea() {
		return carpetArea;
	}

	public void setCarpetArea(String carpetArea) {
		this.carpetArea = carpetArea;
	}

	public String getCostCode() {
		return costCode;
	}

	public void setCostCode(String costCode) {
		this.costCode = costCode;
	}

	public double getIgstAmt() {
		return igstAmt;
	}

	public void setIgstAmt(double igstAmt) {
		this.igstAmt = igstAmt;
	}

	public double getUtgstAmt() {
		return utgstAmt;
	}

	public void setUtgstAmt(double utgstAmt) {
		this.utgstAmt = utgstAmt;
	}

	public double getSgstAmt() {
		return sgstAmt;
	}

	public void setSgstAmt(double sgstAmt) {
		this.sgstAmt = sgstAmt;
	}

	public double getCgstAmt() {
		return cgstAmt;
	}

	public void setCgstAmt(double cgstAmt) {
		this.cgstAmt = cgstAmt;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public double getIgstPer() {
		return igstPer;
	}

	public void setIgstPer(double igstPer) {
		this.igstPer = igstPer;
	}

	public double getUtgstPer() {
		return utgstPer;
	}

	public void setUtgstPer(double utgstPer) {
		this.utgstPer = utgstPer;
	}

	public double getSgstPer() {
		return sgstPer;
	}

	public void setSgstPer(double sgstPer) {
		this.sgstPer = sgstPer;
	}

	public double getCgstPer() {
		return cgstPer;
	}

	public void setCgstPer(double cgstPer) {
		this.cgstPer = cgstPer;
	}

	public String getBillProductName() {
		return billProductName;
	}

	public void setBillProductName(String billProductName) {
		this.billProductName = billProductName;
	}
	
	
	
	
}
