package com.slicktechnologies.shared.common.salesprocess;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

// TODO: Auto-generated Javadoc
/**
 * The Class BillingDocument.Created when a Contract is Approved.
 * Creation should happen through transection.
 */
@Entity
public class BillingDocument extends SalesPaymentProcess implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2218718809787295514L;
	
	/** The Constant BILLINGINVOICED. */
	public static final String BILLINGINVOICED = "Invoiced";
	
	
	//   rohan added new status for proforma invoice 
	
	public static final String PROFORMAINVOICE = "PInvoice";
	
	
	/** ****************************************Attributes********************************************. */
	
	protected boolean recordSelect;
	
	/** The billing document info. */
	protected BillingDocumentDetails billingDocumentInfo;
	
	
	
	// ***********rohan added gross value field here for printing that in invoice download ***
	
		double grossValue;
		
		/**
		 * Rahul Added this for submit button only
		 * 
		 */
			@Index
			boolean selfApprovalFlag;
			@Index
			Date approvedDate;
		/**
		 * Ends Rahul changes here
		 */
	
	//************************************changes ends here **********************************
	
	 /** Date 03-09-2017 added by vijay for discount for total amt **/
	  protected double discountAmt;
	  
	  /** Date 03-09-2017 added by vijay for after discount final total amt  before taxes**/
	  protected double finalTotalAmt;
	  
	  /** Date 03-09-2017 added by vijay for Round Off amt **/
	  protected double roundOffAmt;		
	  
	  /** Date 03-09-2017 added by vijay for total amount before discount and taxes **/
	  protected double totalAmount;
	
	  /** Date 03-09-2017 added by vijay for total amount after taxes and before other charges **/
	  protected double totalAmtIncludingTax;

	  /** Date 04-09-2017 added by vijay for total amount after taxes and before round off charges **/
	  protected double grandTotalAmount;
	
	  /** Date 06/2/2018 added by komal for consolidate price checkbox in contract**/
	  protected boolean consolidatePrice;
	  
	  /*** Date 11-08-2020 by Vijay for CNC project Name for Sasha ***/
	  @Index
	  protected String projectName;
	  /** Date 12-08-2020 by Vijay TO map PO's Supplier's Ref./Order No for sasha***/
	  protected String referenceNumer;
	  /** Date 12-08-2020 by Vijay TO map GRN Receipt Note No for sasha***/
	  protected String referenceNumber2;
	  /*** Date 13-08-2020 by Vijay To map Credit Period from Sales order and CNC **/
	  protected int creditPeriod;
	  /*** Date 14-08-2020 by Vijay To Map CNC Contract number ***/
	  protected String cncContractNumber;
	  /*** Date 18-08-2020 by Vijay for Sasha to calculate Total Number of Man Days and Man Power ***/
	  protected double totalManPower;
	  protected double totalManDays;
	  
	  /** Date 16-09-2020 by Vijay to store CNC from Date and To Date when they raise the bill and cnc Version **/
	  @Index
	  protected Date cncbillfromDate;
	  @Index
	  protected Date cncbillToDate;
	  protected String cncVersion;
	  
	  protected double totalQty;

	  
	/******************************************Constructor********************************************. */
	/**
	 * Instantiates a new billing document.
	 */
	public BillingDocument() {
		super();
		recordSelect=false;
		billingDocumentInfo=new BillingDocumentDetails();
		selfApprovalFlag=false;
		approvedDate=null;
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		consolidatePrice =false;
		projectName="";
		referenceNumer="";
		referenceNumber2="";
		cncContractNumber="";
		cncVersion = "";
		
	}
	
	
	
	/*************************************************************************************************. */
	 /************************************Getters And Setters**************************************/
	
	
	/**
	 *
	 *
	 * @return the billing document info
	 */
	
	
	public BillingDocumentDetails getBillingDocumentInfo() {
		return billingDocumentInfo;
	}

	/**
	 * Sets the billing document info.
	 *
	 * @param billingDocumentInfo the new billing document info
	 */
	public void setBillingDocumentInfo(BillingDocumentDetails billingDocumentInfo) {
		if(billingDocumentInfo!=null)
			this.billingDocumentInfo = billingDocumentInfo;
	}

	/**
	 * Gets the billing date.
	 *
	 * @return the billing date
	 */
	public Date getBillingDate() {
		return billingDocumentInfo.getBillingDate();
	}

	/**
	 * Sets the billing date.
	 *
	 * @param billingDate the new billing date
	 */
	public void setBillingDate(Date billingDate) {
		if(billingDate!=null)
			billingDocumentInfo.setBillingDate(billingDate);
	}

	/**
	 * Gets the billing category.
	 *
	 * @return the billing category
	 */
	public String getBillingCategory() {
		return billingDocumentInfo.getBillingCategory();
	}

	/**
	 * Sets the billing category.
	 *
	 * @param billingCategory the new billing category
	 */
	public void setBillingCategory(String billingCategory) {
		if(billingCategory!=null)
			billingDocumentInfo.setBillingCategory(billingCategory.trim());
	}

	/**
	 * Gets the billing type.
	 *
	 * @return the billing type
	 */
	public String getBillingType() {
		return billingDocumentInfo.getBillingType();
	}

	/**
	 * Sets the billing type.
	 *
	 * @param billingType the new billing type
	 */
	public void setBillingType(String billingType) {
		if(billingType!=null)
			billingDocumentInfo.setBillingType(billingType.trim());
	}

	/**
	 * Gets the billing group.
	 *
	 * @return the billing group
	 */
	public String getBillingGroup() {
		return billingDocumentInfo.getBillingGroup();
	}

	/**
	 * Sets the billing group.
	 *
	 * @param billingGroup the new billing group
	 */
	public void setBillingGroup(String billingGroup) {
		if(billingGroup!=null)
			billingDocumentInfo.setBillingGroup(billingGroup.trim());
	}

	/**
	 * Gets the billstatus.
	 *
	 * @return the billstatus
	 */
	public String getBillstatus() {
		return billingDocumentInfo.getBillstatus();
	}

	/**
	 * Sets the billstatus.
	 *
	 * @param billstatus the new billstatus
	 */
	public void setBillstatus(String billstatus) {
		if(billstatus!=null)
			billingDocumentInfo.setBillstatus(billstatus);
	}
	
	/**
	 * Gets the bill amount.
	 *
	 * @return the bill amount
	 */
	public Double getBillAmount() {
		return billingDocumentInfo.getBillAmount();
	}

	/**
	 * Sets the bill amount.
	 *
	 * @param billAmount the new bill amount
	 */
	public void setBillAmount(double billAmount) {
		billingDocumentInfo.setBillAmount(billAmount);
	}

	/**
	 * Gets the record select.
	 *
	 * @return the record select
	 */
	public Boolean getRecordSelect() {
		return recordSelect;
	}

	/**
	 * Sets the record select.
	 *
	 * @param recordSelect the new record select
	 */
	public void setRecordSelect(Boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	
//*********************************************changes here **********************8
	public double getGrossValue() {
		return grossValue;
	}


	public void setGrossValue(double grossValue) {
		this.grossValue = grossValue;
	}

	
	
	
//***************************changes ends here ********************************	
	
	/*******************************************************************************************************/
	
	


	public boolean isSelfApprovalFlag() {
		return selfApprovalFlag;
	}



	public void setSelfApprovalFlag(boolean selfApprovalFlag) {
		this.selfApprovalFlag = selfApprovalFlag;
	}



	public Date getApprovedDate() {
		return approvedDate;
	}



	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}



	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList() {
		ArrayList<String>list=new ArrayList<String>();
		list.add(BillingDocument.CREATED);
		list.add(BillingDocument.REQUESTED);
		list.add(BillingDocument.REJECTED);
		list.add(BillingDocument.APPROVED);
		list.add(BillingDocument.CANCELLED);
		list.add(BillingDocument.BILLINGINVOICED);
		return list;
	}

/*********************************************************************************************************/		
	
	@Override	
	@GwtIncompatible
	public void reactOnApproval() {
		boolean validatePayment=this.checkPayableAmount();
		/**
		 * nidhi 14-12-2018
		 */
		boolean parcialBillFlag =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "DisableParticalBillDocument",this.getCompanyId());
		
		if(parcialBillFlag){
			validatePayment = false;
		}
		/**
		 * end
		 */
		if(validatePayment==true&&getStatus().equals(BillingDocument.APPROVED))
		{
			this.createNewBillingDocument();
		}
		
		if(getStatus().equals(BillingDocument.APPROVED))
		{
			updateBillingChargeBalance();
		}
	}
	
	/**
	 * Method for validating payable amount of billing document.
	 * If payable amount is less than billing document amount then it returns true so that
	 * new billing document can be created.
	 * It the amount is same then it will return false and no new billing document will be created.
	 * @return
	 */
	protected boolean checkPayableAmount()
	{
		double baseBilling=0,payableAmt=0;
		for(int i=0;i<this.getSalesOrderProducts().size();i++)
		{
			baseBilling=baseBilling+this.getSalesOrderProducts().get(i).getBaseBillingAmount() - this.getSalesOrderProducts().get(i).getFlatDiscount();
			payableAmt=payableAmt+((this.getSalesOrderProducts().get(i).getBaseBillingAmount()- this.getSalesOrderProducts().get(i).getFlatDiscount())*this.getSalesOrderProducts().get(i).getPaymentPercent())/100;
		}
		if(payableAmt<baseBilling)
		{
			return true;
		}
		return false;
	}
	
	
	/**
	 * Creates the new billing document.
	 * This method is called when the payable billing amount is less than the original billing amount
	 * The condition is checked in {@link checkPayableAmount method}
	 */
	@GwtIncompatible
    protected void createNewBillingDocument()
    {
    	GenricServiceImpl impl=new GenricServiceImpl();
		
		List<SalesOrderProductLineItem> salesProductsLis=this.updateProductTable();
		ArrayList<SalesOrderProductLineItem> salesProductsArr=new ArrayList<SalesOrderProductLineItem>();
		salesProductsArr.addAll(salesProductsLis);
		BillingDocument billdetails=new BillingDocument();
		billdetails.setPersonInfo(this.getPersonInfo());
		billdetails.setContractCount(this.getContractCount());
		billdetails.setTotalSalesAmount(this.getTotalSalesAmount());
		billdetails.setSalesOrderProducts(salesProductsArr);
		billdetails.setBillingDate(this.getBillingDate());
		billdetails.setArrPayTerms(this.getArrPayTerms());
		billdetails.setContractStartDate(this.getContractStartDate());
		billdetails.setContractEndDate(this.getContractEndDate());
		List<ContractCharges> taxesLis=this.updateTaxesTable(salesProductsLis);
		ArrayList<ContractCharges> arrcc=new ArrayList<ContractCharges>();
		arrcc.addAll(taxesLis);
		billdetails.setBillingTaxes(arrcc);
		if(this.getCompanyId()!=null)
			billdetails.setCompanyId(this.getCompanyId());
		if(this.getBillingCategory()!=null)
			billdetails.setBillingCategory(this.getBillingCategory());
		if(this.getBillingType()!=null)
			billdetails.setBillingType(this.getBillingType());
		if(this.getBillingGroup()!=null)
			billdetails.setBillingGroup(this.getBillingGroup());
		billdetails.setApproverName(this.getApproverName());
		billdetails.setEmployee(this.getEmployee());
		billdetails.setBranch(this.getBranch());
		if(this.getInvoiceDate()!=null)
			billdetails.setInvoiceDate(this.getInvoiceDate());
		if(this.getPaymentDate()!=null)
			billdetails.setPaymentDate(this.getPaymentDate());
		
		billdetails.setStatus(BillingDocument.CREATED);
		billdetails.setOrderCformStatus(this.getOrderCformStatus());
		billdetails.setOrderCformPercent(this.getOrderCformPercent());
		billdetails.setOrderCreationDate(this.getOrderCreationDate());
		billdetails.setTypeOfOrder(this.getTypeOfOrder());
		billdetails.setAccountType(this.getAccountType());
		/**
		 * Date : 20-11-2017 BY ANIL
		 */
		if(this.getNumberRange()!=null){
			billdetails.setNumberRange(this.getNumberRange());
		}
		if(this.getSegment()!=null){
			billdetails.setSegment(this.getSegment());
		}
		if(this.getRefNumber()!=null){
			billdetails.setRefNumber(this.getRefNumber());
		}
		double totBillAmt=0;
		double taxAmt=0;
		for(SalesOrderProductLineItem item:billdetails.getSalesOrderProducts()){
			totBillAmt=totBillAmt+item.getBaseBillingAmount();
		}
		for(ContractCharges chrg:billdetails.getBillingTaxes()){
			taxAmt=taxAmt+chrg.getPayableAmt();
		}
		billdetails.setTotalAmount(totBillAmt);
		billdetails.setFinalTotalAmt(totBillAmt);
		double totalamtincludingtax=totBillAmt+taxAmt;
		billdetails.setTotalAmtIncludingTax(totalamtincludingtax);
		billdetails.setGrandTotalAmount(totalamtincludingtax);
		billdetails.setTotalBillingAmount(totalamtincludingtax);
		
		billdetails.setComment(this.getComment());
		billdetails.setBillingPeroidFromDate(this.getBillingPeroidFromDate());
		billdetails.setBillingPeroidToDate(this.getBillingPeroidToDate());
		/** Date 11-08-2020 by Vijay for Project Name mapping ***/
		if(this.getProjectName()!=null){
			billdetails.setProjectName(this.getProjectName());
		}
		
		/**
		 * End
		 */
		
		/**
		 * @author Vijay 28-03-2022 for do not print service address flag updating to invoice
		 */
		billdetails.setDonotprintServiceAddress(this.isDonotprintServiceAddress());
		
		/**Sheetal:01-04-2022,storing payment mode in invoice**/
		if(this.getPaymentMode()!=null) {
		   billdetails.setPaymentMode(this.getPaymentMode());
		}
		
		impl.save(billdetails);
    }
	
	/**
	 * Method returns arraylist of Sales Order Products.
	 * This method is called while creating new Billing Document if the billing document amount is less than
	 * base billing amount.
	 * @return
	 */
	protected List<SalesOrderProductLineItem> updateProductTable()
	{
		ArrayList<SalesOrderProductLineItem> arrSalesProducts=new ArrayList<SalesOrderProductLineItem>();
		double calPayableAmt=0,calBaseBillAmt;
		for(int i=0;i<this.getSalesOrderProducts().size();i++)
		{
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
			salesOrder.setProdCategory(this.getSalesOrderProducts().get(i).getProdCategory());
			salesOrder.setProdCode(this.getSalesOrderProducts().get(i).getProdCode());
			salesOrder.setProdName(this.getSalesOrderProducts().get(i).getProdName());
			salesOrder.setQuantity(this.getSalesOrderProducts().get(i).getQuantity());
			salesOrder.setPrice(this.getSalesOrderProducts().get(i).getPrice());
			
			salesOrder.setVatTaxEdit(this.getSalesOrderProducts().get(i).getVatTaxEdit());
			salesOrder.setServiceTaxEdit(this.getSalesOrderProducts().get(i).getServiceTaxEdit());
			salesOrder.setVatTax(this.getSalesOrderProducts().get(i).getVatTax());
			salesOrder.setServiceTax(this.getSalesOrderProducts().get(i).getServiceTax());
			
			
			salesOrder.setTotalAmount(this.getSalesOrderProducts().get(i).getTotalAmount());
			salesOrder.setProdPercDiscount(this.getSalesOrderProducts().get(i).getProdPercDiscount());
		
			
			
			//  rohan added this code
			salesOrder.setPrduct(this.getSalesOrderProducts().get(i).getPrduct());
			
			calPayableAmt=((this.getSalesOrderProducts().get(i).getBaseBillingAmount()-this.getSalesOrderProducts().get(i).getFlatDiscount())*this.getSalesOrderProducts().get(i).getPaymentPercent())/100;
			calBaseBillAmt=this.getSalesOrderProducts().get(i).getBaseBillingAmount()-calPayableAmt;
			salesOrder.setBaseBillingAmount(calBaseBillAmt);
			if(this.getSalesOrderProducts().get(i).getPaymentPercent()==100){
				salesOrder.setPaymentPercent(0.0);
			}
			else{
				salesOrder.setPaymentPercent(100.0);
			}
			
			salesOrder.setIndexVal(this.getSalesOrderProducts().get(i).getIndexVal());
			salesOrder.setProductSrNumber(this.getSalesOrderProducts().get(i).getProductSrNumber());
			/** Date 09-02-2018 By vijay for partial billing document setting base billing amt **/
			salesOrder.setBasePaymentAmount(calBaseBillAmt);
			/** Date 15-02-2018 By vijay partial biling setting HSN Code **/
			if(this.getSalesOrderProducts().get(i).getHsnCode()!=null && !this.getSalesOrderProducts().get(i).getHsnCode().equals(""))
			salesOrder.setHsnCode(this.getSalesOrderProducts().get(i).getHsnCode());
			
			arrSalesProducts.add(salesOrder);
		}
		return arrSalesProducts;
	}
	
	
	protected List<ContractCharges> updateTaxesTable(List<SalesOrderProductLineItem> salesList)
	{
		
		//  old code 
		
//		ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
//		
//		for(int i=0;i<this.getBillingTaxes().size();i++)
//		{
//			double assesCalc=getBaseBillValue(salesList,this.getBillingTaxes().get(i).getIdentifyTaxCharge());
//			ContractCharges ccent=new ContractCharges();
//			ccent.setTaxChargeName(this.getBillingTaxes().get(i).getTaxChargeName());
//			ccent.setTaxChargePercent(this.getBillingTaxes().get(i).getTaxChargePercent());
//			ccent.setTaxChargeAssesVal(assesCalc);
//			arrConCharges.add(ccent);
//		}
//		return arrConCharges;
		
		
		
		/** New Code 
		 * Date : 11-02-2017 By Anil
		 * This code is used to calculate the tax amount for partial billing
		 * Note:this is a temporary solution
		 * RELEASE : 11-02-2017 NBHC CCPM
		 */
		ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
		double totalAmouunt=0;
		double totalAmount1=0;
		
		boolean gstFlag=false;
		
		for(SalesOrderProductLineItem item:salesList){
			totalAmouunt=totalAmouunt+item.getBaseBillingAmount();
			if(item.getVatTax().getTaxPrintName()!=null && !item.getServiceTax().getTaxPrintName().equals("")){
				if(item.getVatTax().getTaxPrintName().equals("SGST") || item.getVatTax().getTaxPrintName().equals("CSGT")||item.getVatTax().getTaxPrintName().equals("IGST")){
					gstFlag = true;
				}
				
				if(item.getServiceTax().getTaxPrintName().equals("SGST") || item.getServiceTax().getTaxPrintName().equals("CSGT")||item.getServiceTax().getTaxPrintName().equals("IGST")){
					gstFlag = true;
				}
			}
		}
		System.out.println("Rohan gstFlag"+gstFlag);
		if(gstFlag)
		{
			for(ContractCharges tax:this.getBillingTaxes()){
				
				ContractCharges ccent=new ContractCharges();
				ccent.setTaxChargeName(tax.getTaxChargeName());
				ccent.setTaxChargePercent(tax.getTaxChargePercent());
				ccent.setTaxChargeAssesVal(totalAmouunt);
				
				double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
				ccent.setPayableAmt(totalCalcAmt);
				arrConCharges.add(ccent);
				
//				totalAmount1=totalAmount1+totalAmouunt+totalCalcAmt;
			}
		}
		else
		{
			for(ContractCharges tax:this.getBillingTaxes()){
				if(tax.getTaxChargeName().trim().equalsIgnoreCase("VAT") ||tax.getTaxChargeName().trim().equalsIgnoreCase("CST")){
					ContractCharges ccent=new ContractCharges();
					ccent.setTaxChargeName(tax.getTaxChargeName());
					ccent.setTaxChargePercent(tax.getTaxChargePercent());
					ccent.setTaxChargeAssesVal(totalAmouunt);
					
					double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
					ccent.setPayableAmt(totalCalcAmt);
					arrConCharges.add(ccent);
					
					totalAmount1=totalAmount1+totalAmouunt+totalCalcAmt;
				}
			}
			
			for(ContractCharges tax:this.getBillingTaxes()){
				if(tax.getTaxChargeName().trim().equalsIgnoreCase("Service Tax")){
					ContractCharges ccent=new ContractCharges();
					ccent.setTaxChargeName(tax.getTaxChargeName());
					ccent.setTaxChargePercent(tax.getTaxChargePercent());
					ccent.setTaxChargeAssesVal(totalAmount1);
					double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
					ccent.setPayableAmt(totalCalcAmt);
					arrConCharges.add(ccent);
				}
			}
		}
		
		System.out.println("arrConCharges size"+arrConCharges.size());
		
		return arrConCharges;
		
		/**
		 * End
		 */
	}
	
	protected double getBaseBillValue(List<SalesOrderProductLineItem> salesProLis,int indexCheck)
	{
		double baseBillVal=0;
		for(int i=0;i<salesProLis.size();i++)
		{
			if(salesProLis.get(i).getIndexVal()==indexCheck){
				baseBillVal=salesProLis.get(i).getBaseBillingAmount();
			}
		}
		return baseBillVal;
	}
	
	/**
	 * This method is used for updating the other charges balance when billing document is approved.
	 * 
	 */
	
	@GwtIncompatible
	protected void updateBillingChargeBalance()
	{
		if(this.getBillingOtherCharges().size()!=0){
			String typeFlow="";
			if(getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				typeFlow=AppConstants.BILLINGSALESFLOW;
			}
			if(getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				typeFlow=AppConstants.BILLINGPURCHASEFLOW;
			}
			if(getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
				typeFlow=AppConstants.BILLINGSERVICEFLOW;
			}
			
			
			TaxesAndCharges billBalance=ofy().load().type(TaxesAndCharges.class).filter("contractId", this.getContractCount()).filter("companyId", this.getCompanyId()).filter("identifyOrder", typeFlow.trim()).first().now();
			if(billBalance!=null){
				List<ContractCharges> saveBalance=updateChargesBalance();
				ArrayList<ContractCharges> saveBalArr=new ArrayList<ContractCharges>();
				saveBalArr.addAll(saveBalance);
				billBalance.setTaxesChargesList(saveBalArr);
				ofy().save().entities(billBalance).now();
			}
		}
		else{
			String typeFlow="";
			if(getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
				typeFlow=AppConstants.BILLINGSALESFLOW;
			}
			if(getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				typeFlow=AppConstants.BILLINGPURCHASEFLOW;
			}
			if(getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
				typeFlow=AppConstants.BILLINGSERVICEFLOW;
			}
			
			TaxesAndCharges billBalance=ofy().load().type(TaxesAndCharges.class).filter("contractId", this.getContractCount()).filter("companyId", this.getCompanyId()).filter("identifyOrder", typeFlow.trim()).first().now();
			if(billBalance!=null){
				List<ContractCharges> saveBalance=billBalance.getTaxesChargesList();
				for(int i=0;i<saveBalance.size();i++){
					saveBalance.get(i).setBillingId(this.getCount());
				}
				ArrayList<ContractCharges> saveBalArr=new ArrayList<ContractCharges>();
				saveBalArr.addAll(saveBalance);
				
				
				setBillingOtherCharges(saveBalArr);
			}
		}
	}
	
	/**
	 * This method is used for updating balance when billing document is approved.
	 * The balance amount is retrieved from billing screen charges table and saved here in 
	 * taxes and charges.
	 * If pay % is 100 then it sets the balance to zero.
	 */
	protected List<ContractCharges> updateChargesBalance()
	{
		System.out.println("Updating Balance Code");
		List<ContractCharges> updateBalanceLis=this.getBillingOtherCharges();
		final ArrayList<ContractCharges> updateBalanceArr=new ArrayList<ContractCharges>();
		double balVal=0;
		for(int i=0;i<updateBalanceLis.size();i++)
		{
			ContractCharges ccObj=new ContractCharges();
			ccObj.setTaxChargeName(updateBalanceLis.get(i).getTaxChargeName());
			ccObj.setTaxChargePercent(updateBalanceLis.get(i).getTaxChargePercent());
			ccObj.setTaxChargeAbsVal(updateBalanceLis.get(i).getTaxChargeAbsVal());
			ccObj.setTaxChargeAssesVal(updateBalanceLis.get(i).getTaxChargeAssesVal());
			if(updateBalanceLis.get(i).getPaypercent()!=100&&updateBalanceLis.get(i).getPaypercent()!=0){
				balVal=updateBalanceLis.get(i).getPaypercent()*updateBalanceLis.get(i).getChargesBalanceAmt()/100;
				balVal=updateBalanceLis.get(i).getChargesBalanceAmt()-balVal;
			}
			if(updateBalanceLis.get(i).getPaypercent()==0&&updateBalanceLis.get(i).getChargesBalanceAmt()!=0){
				balVal=updateBalanceLis.get(i).getChargesBalanceAmt();
			}
			if(updateBalanceLis.get(i).getPaypercent()==100){
				balVal=0;
			}
			ccObj.setChargesBalanceAmt(balVal);
			updateBalanceArr.add(ccObj);
		}
		
		return updateBalanceArr;
	}
	
	
	
	/*************************************Relation Management Part*******************************************/
	
	
	@GwtIncompatible
	@OnSave
	private void updateBillingProductList()
	{
		if(id==null){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "EnableServiceBranch", this.getCompanyId())){
				Contract contract =ofy().load().type(Contract.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
				HashMap<Integer, ArrayList<BranchWiseScheduling>> map= new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
				ArrayList<BranchWiseScheduling> branchList = new ArrayList<BranchWiseScheduling>();
				Set<BranchWiseScheduling> branchSet = new HashSet<BranchWiseScheduling>();
				if(contract != null && contract.getItems() != null){
					for(SalesLineItem item :contract.getItems()){
						
						branchSet = new HashSet<BranchWiseScheduling>();
						if(item.getCustomerBranchSchedulingInfo() != null && item.getCustomerBranchSchedulingInfo().size() > 0){
							for(BranchWiseScheduling branch : item.getCustomerBranchSchedulingInfo().get(item.getProductSrNo())){
								if(branch.isCheck()){
									if(!branch.getBranchName().equals("Service Address")){
										BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
										branchWiseScheduling.setBranchName(branch.getServicingBranch());
										branchWiseScheduling.setArea(0);
										branchWiseScheduling.setCheck(false);
										branchSet.add(branchWiseScheduling);
									
									}else{						
											BranchWiseScheduling branchWiseScheduling = new BranchWiseScheduling();
											branchWiseScheduling.setBranchName(contract.getBranch());
											branchWiseScheduling.setArea(0);
											branchWiseScheduling.setCheck(false);
											branchSet.add(branchWiseScheduling);
											
									//	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "ServiceBranchFromSchedulePopup", this.getCompanyId())){
											BranchWiseScheduling branchWiseScheduling1 = new BranchWiseScheduling();
											branchWiseScheduling1.setBranchName("Service Address");
											branchWiseScheduling1.setArea(0);
											branchWiseScheduling1.setCheck(false);
											branchWiseScheduling1.setServicingBranch(branch.getServicingBranch());
											branchSet.add(branchWiseScheduling1);
									//	}
									}
								}
								
							}
						}
						branchList = new ArrayList<BranchWiseScheduling>(branchSet);
						map.put(item.getProductSrNo(), branchList);
						
					}
					if(this.getSalesOrderProducts().size()!=0){
						
						for(int i=0;i<this.getSalesOrderProducts().size();i++){
							if(map != null && map.size() > 0 && map.containsKey(this.getSalesOrderProducts().get(i).getProductSrNumber())){
								HashMap<Integer, ArrayList<BranchWiseScheduling>> map1= new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
								map1.put(this.getSalesOrderProducts().get(i).getProductSrNumber(), map.get(this.getSalesOrderProducts().get(i).getProductSrNumber()));
								this.getSalesOrderProducts().get(i).setServiceBranchesInfo(map1);
							}
							
						}
						
					}
				}
				
				
			}
			if(this.getSalesOrderProducts().size()!=0){
				for(int i=0;i<this.getSalesOrderProducts().size();i++){
					this.getSalesOrderProducts().get(i).setBiilingId(this.getCount());
				}
			}
			/** date 20.10.2018 added by komal for subbilltype save **/
			if (this.getSubBillType() == null
					|| this.getSubBillType().equals("")) {
				if (this.getRefNumber() != null
						&& !this.getRefNumber().equals("")) {
					int refNo = 0;
					try {
						refNo = Integer.parseInt(this.getRefNumber());
					} catch (Exception e) {
						refNo = 0;
					}
					if (refNo != 0) {
						CNC cnc = ofy().load().type(CNC.class)
								.filter("count", refNo)
								.filter("companyId", this.getCompanyId())
								.first().now();
						if (cnc != null) {
							this.setSubBillType(AppConstants.CONSUMABLES);
						}
					}
				}
			}

		}
		/**
		 * @author Vijay Chougule
		 * @Since Date 18-08-2020 
		 * Des :- when below process config is Active then it will calculate the Total Man Power and Total Man Days
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn", this.getCompanyId())){
			double totalManPower = 0;
			double totalManDays=0;
			double labourManPower = 0;
			double overtimeManPower=0;
			double holidayManPower = 0;
			
			for(SalesOrderProductLineItem product : this.getSalesOrderProducts()){
				System.out.println("product.getBillType()"+product.getBillType());
				if(product.getBillType()!=null && !product.getBillType().equals("") && ( product.getBillType().equals("Labour") || product.getBillType().equals("Overtime") || product.getBillType().equals("Holiday")) ){
					if(product.getManpower()!=null && !product.getManpower().equals("")){
						if(product.getBillType().equals("Labour")){
							labourManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Overtime")){
							overtimeManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Holiday")){
							holidayManPower += Double.parseDouble(product.getManpower());
						}
					}
					if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
						totalManDays +=Double.parseDouble(product.getArea());
						System.out.println("totalManDays "+totalManDays);
					}
				}
				
			}
			totalManPower = getmaxManPower(labourManPower,overtimeManPower,holidayManPower);
			this.setTotalManPower(totalManPower);
			this.setTotalManDays(totalManDays);
			System.out.println("totalManPower"+totalManPower);
			System.out.println("totalManDays"+totalManDays);
		}
		/**
		 * @author Vijay Date :- 28-03-2022
		 * Des :- as per nitin sir need sum of qty 
		 */
		else {
			double totalqty=0;
			for(SalesOrderProductLineItem product : this.getSalesOrderProducts()){
					if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
						try {
							totalqty +=Double.parseDouble(product.getArea());
						} catch (Exception e) {
						}
					}
				
			}
			this.setTotalQty(totalqty);
		}
	}
	
	

	@GwtIncompatible
	private double getmaxManPower(double labourManPower,double overtimeManPower, double holidayManPower) {
		double maxManPower = 0;
		if(labourManPower>=overtimeManPower && labourManPower>=holidayManPower){
				maxManPower = labourManPower;
		}
		else if(overtimeManPower>=holidayManPower && overtimeManPower>=labourManPower){
				maxManPower = overtimeManPower;
		}
		else{
			maxManPower = holidayManPower;
		}
		
		return maxManPower;
	}



	/** Date 03-09-2017 added by vijay for getter and setter  **/

	public double getDiscountAmt() {
		return discountAmt;
	}


	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}


	public double getFinalTotalAmt() {
		return finalTotalAmt;
	}


	public void setFinalTotalAmt(double finalTotalAmt) {
		this.finalTotalAmt = finalTotalAmt;
	}


	public double getRoundOffAmt() {
		return roundOffAmt;
	}


	public void setRoundOffAmt(double roundOffAmt) {
		this.roundOffAmt = roundOffAmt;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}



	public double getTotalAmtIncludingTax() {
		return totalAmtIncludingTax;
	}


	public void setTotalAmtIncludingTax(double totalAmtIncludingTax) {
		this.totalAmtIncludingTax = totalAmtIncludingTax;
	}
	
	public double getGrandTotalAmount() {
		return grandTotalAmount;
	}

	public void setGrandTotalAmount(double grandTotalAmount) {
		this.grandTotalAmount = grandTotalAmount;
	}

	public boolean isConsolidatePrice() {
		return consolidatePrice;
	}

	public void setConsolidatePrice(boolean consolidatePrice) {
		this.consolidatePrice = consolidatePrice;
	}
	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.billingDocumentInfo.getBillingDate()!=null){
			this.billingDocumentInfo.setBillingDate(dateUtility.setTimeMidOftheDayToDate(this.billingDocumentInfo.getBillingDate()));
		}
		
		if(this.getBillingPeroidFromDate()!=null){
			this.setBillingPeroidFromDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidFromDate()));
		}
		if(this.getBillingPeroidToDate()!=null){
			this.setBillingPeroidToDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidToDate()));
		}
		if(this.getInvoiceDate()!=null){
			this.setInvoiceDate(dateUtility.setTimeMidOftheDayToDate(this.getInvoiceDate()));
		}
		if(this.getOrderCreationDate()!=null){
			this.setOrderCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getOrderCreationDate()));
		}
		if(this.getPaymentDate()!=null){
			this.setPaymentDate(dateUtility.setTimeMidOftheDayToDate(this.getPaymentDate()));
		}
		if(this.id==null){
			if(this.getContractStartDate()!=null){
				this.setContractStartDate(dateUtility.setTimeMidOftheDayToDate(this.getContractStartDate()));
			}
			if(this.getContractEndDate()!=null){
				this.setContractEndDate(dateUtility.setTimeMidOftheDayToDate(this.getContractEndDate()));
			}
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
		
		if(this.getCncbillfromDate()!=null){
			this.setCncbillfromDate(dateUtility.setTimeMidOftheDayToDate(this.getCncbillfromDate()));
		}
		if(this.getCncbillToDate()!=null){
			this.setCncbillToDate(dateUtility.setTimeMidOftheDayToDate(this.getCncbillToDate()));
		}
		
	}
	/**
	 * ends here
	 */



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public String getReferenceNumer() {
		return referenceNumer;
	}



	public void setReferenceNumer(String referenceNumer) {
		this.referenceNumer = referenceNumer;
	}



	public String getReferenceNumber2() {
		return referenceNumber2;
	}



	public void setReferenceNumber2(String referenceNumber2) {
		this.referenceNumber2 = referenceNumber2;
	}



	public int getCreditPeriod() {
		return creditPeriod;
	}



	public void setCreditPeriod(int creditPeriod) {
		this.creditPeriod = creditPeriod;
	}



	public String getCncContractNumber() {
		return cncContractNumber;
	}



	public void setCncContractNumber(String cncContractNumber) {
		this.cncContractNumber = cncContractNumber;
	}



	public double getTotalManPower() {
		return totalManPower;
	}



	public void setTotalManPower(double totalManPower) {
		this.totalManPower = totalManPower;
	}



	public double getTotalManDays() {
		return totalManDays;
	}



	public void setTotalManDays(double totalManDays) {
		this.totalManDays = totalManDays;
	}



	public Date getCncbillfromDate() {
		return cncbillfromDate;
	}



	public void setCncbillfromDate(Date cncbillfromDate) {
		this.cncbillfromDate = cncbillfromDate;
	}



	public Date getCncbillToDate() {
		return cncbillToDate;
	}



	public void setCncbillToDate(Date cncbillToDate) {
		this.cncbillToDate = cncbillToDate;
	}



	public String getCncVersion() {
		return cncVersion;
	}



	public void setCncVersion(String cncVersion) {
		this.cncVersion = cncVersion;
	}
	
	
	/**
	 * @author Anil @since 09-08-2021
	 */
	public BillingDocument MyClone(){
		BillingDocument doc=new BillingDocument();
		doc.setId(this.getId());
		doc.setCompanyId(this.getCompanyId());
		doc.setCount(this.getCount());
		
		doc.setAccountType(this.getAccountType());
		doc.setApprovalDate(this.getApprovalDate());
		doc.setApprovalTime(this.getApprovalTime());
		doc.setApprovedDate(this.getApprovedDate());
		doc.setApproverName(this.getApproverName());
		doc.setArrayBillingDocument(this.getArrayBillingDocument());
		doc.setArrPayTerms(this.getArrPayTerms());
		
		doc.setBillAmount(this.getBillAmount());
		doc.setBillingCategory(this.getBillingCategory());
		doc.setBillingDate(this.getBillingDate());
		doc.setBillingDocumentInfo(this.getBillingDocumentInfo());
		doc.setBillingGroup(this.getBillingGroup());
		doc.setBillingOtherCharges(this.getBillingOtherCharges());
		doc.setBillingPeroidFromDate(this.getBillingPeroidFromDate());
		doc.setBillingPeroidToDate(this.getBillingPeroidToDate());
		doc.setBillingTaxes(this.getBillingTaxes());
		doc.setBillingType(this.getBillingType());
		doc.setBillstatus(this.getBillstatus());
		
		doc.setCancleAmt(this.getCancleAmt());
		doc.setCancleAmtRemark(this.getCancleAmtRemark());
		doc.setCategory(this.getCategory());
		doc.setCategoryKey(this.getCategoryKey());
		doc.setCncBillAnnexureId(this.getCncBillAnnexureId());
		doc.setCncbillfromDate(this.getCncbillfromDate());
		doc.setCncbillToDate(this.getCncbillToDate());
		doc.setCncVersion(this.getCncVersion());
		doc.setCncContractNumber(this.getCncContractNumber());
		doc.setComment(this.getComment());
		doc.setConEndDur(this.getConEndDur());
		doc.setConsolidatePrice(this.isConsolidatePrice());
		doc.setConStartDur(this.getConStartDur());
		doc.setContractCount(this.getContractCount());
		doc.setContractEndDate(this.getContractEndDate());
		doc.setContractStartDate(this.getContractStartDate());
		doc.setCreatedBy(this.getCreatedBy());
		doc.setCreationDate(this.getCreationDate());
		doc.setCreationTime(this.getCreationTime());
		doc.setCreditPeriod(this.getCreditPeriod());
		doc.setCustomerBranch(this.getCustomerBranch());
		doc.setCustomerId(this.getCustomerId());
		
		doc.setDeleted(this.isDeleted());
		doc.setDiscountAmt(this.getDiscountAmt());
		doc.setDocStatusReason(this.getDocStatusReason());
		
		doc.setEmployee(this.getEmployee());
		doc.setEwayBillNumber(this.getEwayBillNumber());
		
		doc.setFinalTotalAmt(this.getFinalTotalAmt());

		doc.setGrandTotalAmount(this.getGrandTotalAmount());
		doc.setGrossValue(this.getGrossValue());
		doc.setGroup(this.getGroup());
		doc.setGroupKey(this.getGroupKey());
		doc.setGstinNumber(this.getGstinNumber());
		
		doc.setInvoiceAmount(this.getInvoiceAmount());
		doc.setInvoiceCategory(this.getInvoiceCategory());
		doc.setInvoiceConfigType(this.getInvoiceConfigType());
		doc.setInvoiceCount(this.getInvoiceCount());
		doc.setInvoiceDate(this.getInvoiceDate());
		doc.setInvoiceGroup(this.getInvoiceGroup());
		doc.setInvoiceType(this.getInvoiceType());
		
		doc.setKeyNameBranch(this.getKeyNameBranch());
		doc.setKeyNameEmployee(this.getKeyNameEmployee());
		
		doc.setManagementFeesPercent(this.getManagementFeesPercent());
		doc.setMultipleOrderBilling(this.isMultipleOrderBilling());
		
		doc.setNumberRange(this.getNumberRange());
		
		doc.setOrderCformPercent(this.getOrderCformPercent());
		doc.setOrderCformStatus(this.getOrderCformStatus());
		doc.setOrderCreationDate(this.getOrderCreationDate());
		doc.setOtherCharges(this.getOtherCharges());
		
		doc.setPaymentAmt(this.getPaymentAmt());
		doc.setPaymentDate(this.getPaymentDate());
		doc.setPaymentMethod(this.getPaymentMethod());
		doc.setPaymentMode(this.getPaymentMode());
		doc.setPersonInfo(this.getPersonInfo());
		doc.setProjectName(this.getProjectName());
		
		doc.setQuantity(this.getQuantity());
		
		doc.setRateContractServiceId(this.getRateContractServiceId());
		doc.setRecordSelect(this.getRecordSelect());
		doc.setReferenceNumber2(this.getReferenceNumber2());
		doc.setReferenceNumer(this.getReferenceNumer());
		doc.setRefNumber(this.getRefNumber());
		doc.setRemark(this.getRemark());
		doc.setRenewContractFlag(this.isRenewContractFlag());
		doc.setRenewFlag(this.isRenewFlag());
		doc.setRoundOffAmt(this.getRoundOffAmt());
		
		doc.setSalesOrderProducts(this.getSalesOrderProducts());
		doc.setSegment(this.getSegment());
		doc.setSelfApprovalFlag(this.isSelfApprovalFlag());
		doc.setServiceId(this.getServiceId());
		doc.setStatus(this.getStatus());
		doc.setStatusKey(this.getStatusKey());
		doc.setSubBillType(this.getSubBillType());
		doc.setSystemcreationDate(this.getSystemcreationDate());
		
		doc.setTotalAmount(this.getTotalAmount());
		doc.setTotalAmtIncludingTax(this.getTotalAmtIncludingTax());
		doc.setTotalBillingAmount(this.getTotalBillingAmount());
		doc.setTotalManDays(this.getTotalManDays());
		doc.setTotalManPower(this.getTotalManPower());
		doc.setTotalOtherCharges(this.getTotalOtherCharges());
		doc.setTotalSalesAmount(this.getTotalSalesAmount());
		doc.setType(this.getType());
		doc.setTypeKey(this.getTypeKey());
		doc.setTypeOfOrder(this.getTypeOfOrder());
		
		doc.setUom(this.getUom());
		doc.setUserId(this.getUserId());
		
		return doc;
	}



	public double getTotalQty() {
		return totalQty;
	}



	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}
	
	
	
	
	}
