package com.slicktechnologies.shared.common.salesprocess;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ApprovableProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

// TODO: Auto-generated Javadoc
/**
 * Super Class of All Sales Payment Related Process.
 */
@Embed
public class SalesPaymentProcess extends ConcreteBusinessProcess {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3755324259461319800L;
	
	/** The Constant PAYMENTCLOSED. */
	public static final String PAYMENTCLOSED = "Closed";
	
/** ****************************************Attributes***************************************************. */
	
	@Index
	protected PersonInfo personInfo;
	
	/** The contract count. */
	@Index
	protected Integer contractCount;
	
	/* Th ref count */
	@Index
	protected String refNumber;
	
	/** The contract start date. */
	/**
	 * @author Anil , Date : 01-04-2020
	 * Removed unused index
	 * Throwing error while saving invoice - raised by nbhc
	 */
	protected Date contractStartDate;
	
	/** The contract end date. */
	protected Date contractEndDate;
	
	/** The total sales amount which is to be paid by customer. */
	/**
	 * @author Anil , Date : 01-04-2020
	 * Removed unused index
	 * Throwing error while saving invoice - raised by nbhc
	 */
	protected double totalSalesAmount;
	
	/**  The total billing amount in billing document. */
	protected double totalBillingAmount;
	
	/** The comment. */
	protected String comment;
	
	/** The invoice date. */
	@Index
	protected Date invoiceDate;
	
	/** The invoice type. */
	@Index
	protected String invoiceType;
	
	/** The array billing document. */
	@Serialize
	protected ArrayList<BillingDocumentDetails> arrayBillingDocument;
	
	/** The payment date. */
	@Index
	protected Date paymentDate;
	
	/** The invoice count. */
	@Index
	protected int invoiceCount;
	
	/** The invoice amount. */
	protected double invoiceAmount;
	
	@Index
	protected  boolean renewFlag;
	@Index
	protected String invoiceCategory;
	@Index
	protected String invoiceConfigType;
	@Index
	protected String invoiceGroup;
	
	protected int paymentAmt;//Added by Ashwini

	/**  Product corresponding to this Billing Document. */
	@Serialize
	protected ArrayList<SalesOrderProductLineItem> salesOrderProducts;
	
	/** The payment method. */
	@Index
	protected String paymentMethod;
	
	/**  Represents the Billing Document from which payment term it is created. */
	protected ArrayList<PaymentTerms> arrPayTerms;
	
	/**  Person Info Key *. */
	protected Key<CustomerRelation> personInfoKey;
	
	/** The salesp product key. */
	protected ArrayList<Key<SuperProduct>> salespProductKey;
	
	/** To identify which type of order is being processed. for eg: contract or sales order **/
	@Index
	protected String typeOfOrder;
	
	protected ArrayList<ContractCharges> billingTaxes;
	protected ArrayList<ContractCharges> billingOtherCharges;
	
	protected Date orderCreationDate;
	
	@Index
	protected String accountType;
	
	@Index
	protected String orderCformStatus;
	
	@Index
	protected double orderCformPercent;
	
	boolean multipleOrderBilling;
	
	/** The contract  conStartDur date. */
	protected Date conStartDur;
	
	/** The contract  conEndDur date. */
	protected Date conEndDur;
	
	protected String customerBranch;
	@Index
	protected String numberRange;
	
	/**
	 * Date 13 Feb 2017
	 * by vijay
	 * for billing period (from date to date) based on contract payment terms
	 */
	
	protected Date billingPeroidFromDate;
	protected Date billingPeroidToDate;
	
	/**
	 * End Here
	 */
	
	

	/**
	 * Date 15 Feb 2017
	 * by Vijay
	 * when rate contract created then its service id copied in billing document 	
	 */
	 @Index	
	 int rateContractServiceId; 	
	
	 /**
	  * End Here
	  */
	 
	 /*
	  * 	nidhi
	  *  30-06-2017
	  */
	 @Index
	 protected String segment;
	 
	 /*
	  * end
	  */
	 
	 
	 /**
	  * Date:19-07-2017 By Anil
	  * this field is used to store the clients GSTIN number which is stores in clients article info.
	  * first requested from Ultra pEst control 
	  */
	 @Index
	 protected String gstinNumber;
	
	/** ***************************************************Constructor***********************************. */
	
	 /*
		 *  nidhi
		 *  20-06-2017
		 *  this fields are declared for save service area quantity and unit
		 */
				protected double quantity;
				protected String uom;
		/*
		 *  end
		 */
				
		/**
		 * Date : 21-09-2017 By ANIL
		 */
		protected ArrayList<OtherCharges> otherCharges;
		protected double totalOtherCharges;
		/**
		 * End
		 */
		/*
		 * nidhi
		 * 27-04-2018
		 * for contract created from old contract
		 */
		@Index
		protected boolean renewContractFlag;
		
	/**
	 * Instantiates a new sales payment process.
	 */
		@Index
		protected String paymentMode;
		
		/** date 1.9.2018 added by komal**/
		@Index
		protected String subBillType;
		/**
		 * nidhi
		 *  11-10-2018 ||*
		 *  for map service Id for billing and invoice
		 */
		@Index
		ArrayList<Integer> serviceId = new ArrayList<Integer>();
		
		/**
		 * nidhi
		 * 13-12-2018 for remark of cancled amount
		 */
		String cancleAmtRemark = "";
		double cancleAmt= 0;
		
		/**
		 * @author Anil , Date : 31-07-2019
		 * adding eway bill number
		 * raised by Sasha,Nitin sir, sonu
		 */
		@Index
		String ewayBillNumber;
		
		@Index
		ArrayList<Integer> cncBillAnnexureId = new ArrayList<Integer>();
		
	/**
	 * @author Anil @since 05-04-2021
	 * Adding management percent filed for Alkosh Invoice 
	 * Raised by Rahul Tiwari 
	 */
	double managementFeesPercent;
	
	protected boolean donotprintServiceAddress;
	
	protected String invoicePdfNameToPrint;


	public SalesPaymentProcess() {
		arrayBillingDocument=new ArrayList<BillingDocumentDetails>();
		salesOrderProducts=new ArrayList<SalesOrderProductLineItem>();
		personInfo=new PersonInfo();
		typeOfOrder="";
		billingTaxes=new ArrayList<ContractCharges>();
		billingOtherCharges=new ArrayList<ContractCharges>();
		accountType="";
		orderCformStatus="";
		comment="";
		renewFlag=false;
		invoiceCategory="";
		invoiceConfigType="";
		invoiceGroup="";
		
		customerBranch="";
		
		multipleOrderBilling=false;
		segment="";
		gstinNumber="";
		/*
		 *  nidhi
		 */
		quantity=0;
		uom="";
		/**
		 *  end
		 */
		refNumber="";  
		otherCharges=new ArrayList<OtherCharges>();
		/**
		 * nidhi
		 */
		renewContractFlag = false;
		/**
		 * nidhi
		 * 9-06-2018
		 */
		paymentMode = "";
		subBillType = "";
		donotprintServiceAddress = false;
		
		invoicePdfNameToPrint = "";
	}
	
	
	
	
	public String getEwayBillNumber() {
		return ewayBillNumber;
	}




	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}




	public String getSubBillType() {
		return subBillType;
	}



	public void setSubBillType(String subBillType) {
		this.subBillType = subBillType;
	}
	
	
	/**
	 * ************************************************************************************************.
	 *
	 * @return the person info
	 */
	
	public double getTotalOtherCharges() {
		return totalOtherCharges;
	}

	public void setTotalOtherCharges(double totalOtherCharges) {
		this.totalOtherCharges = totalOtherCharges;
	}

	public ArrayList<OtherCharges> getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(List<OtherCharges> otherCharges) {
		ArrayList<OtherCharges> list=new ArrayList<OtherCharges>();
		list.addAll(otherCharges);
		this.otherCharges = list;
	}
	
	public String getGstinNumber() {
		return gstinNumber;
	}



	public void setGstinNumber(String gstinNumber) {
		this.gstinNumber = gstinNumber;
	}



	/****************************************Getters And Setters*****************************************. */

	


	public String getSegment() {
		return segment;
	}



	public void setSegment(String segment) {
		this.segment = segment;
	}


	/*
	 *  nidhi
	 */
	public double getQuantity() {
		return quantity;
	}



	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}



	public String getUom() {
		return uom;
	}



	public void setUom(String uom) {
		this.uom = uom;
	}
	/*
	 *  end
	 */

	/**
	 * Gets the person info.
	 *
	 * @return the person info
	 */
	public PersonInfo getPersonInfo() {
		return personInfo;
	}
	
	public boolean isMultipleOrderBilling() {
		return multipleOrderBilling;
	}

	public void setMultipleOrderBilling(boolean multipleOrderBilling) {
		this.multipleOrderBilling = multipleOrderBilling;
	}

	public int getCustomerId() {
		return personInfo.getCount();
	}
	
	public void setCustomerId(int CustomerId) {
		personInfo.setCount(CustomerId);
	}

	/**
	 * Sets the person info.
	 *
	 * @param personInfo the new person info
	 */
	public void setPersonInfo(PersonInfo personInfo) {
			this.personInfo = personInfo;
	}
	
	/**
	 * Gets the customer count.
	 *
	 * @return the customer count
	 */
	public Integer getCustomerCount(){
		if(personInfo!=null)
			return personInfo.getCount();
		else
			return null;
	}
	
	/**
	 * Gets the cell number.
	 *
	 * @return the cell number
	 */
	public Long getCellNumber() {
		if(personInfo!=null)
			return personInfo.getCellNumber();
		else
			return null;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		if(personInfo!=null)
			return personInfo.getFullName();
		else
			return null;
	}

	/**
	 * Gets the contract count.
	 *
	 * @return the contract count
	 */
	public Integer getContractCount() {
		return contractCount;
	}

	/**
	 * Sets the contract count.
	 *
	 * @param contractCount the new contract count
	 */
	public void setContractCount(Integer contractCount) {
		this.contractCount = contractCount;
	}

	/**
	 * Gets the contract start date.
	 *
	 * @return the contract start date
	 */
	public Date getContractStartDate() {
		return contractStartDate;
	}

	/**
	 * Sets the contract start date.
	 *
	 * @param contractStartDate the new contract start date
	 */
	public void setContractStartDate(Date contractStartDate) {
		if(contractStartDate!=null)
			this.contractStartDate = contractStartDate;
	}

	/**
	 * Gets the contract end date.
	 *
	 * @return the contract end date
	 */
	public Date getContractEndDate() {
		return contractEndDate;
	}

	/**
	 * Sets the contract end date.
	 *
	 * @param contractEndDate the new contract end date
	 */
	public void setContractEndDate(Date contractEndDate) {
		if(contractEndDate!=null)
			this.contractEndDate = contractEndDate;
	}

	/**
	 * Gets the total sales amount.
	 *
	 * @return the total sales amount
	 */
	public Double getTotalSalesAmount() {
		return totalSalesAmount;
	}

	/**
	 * Sets the total sales amount.
	 *
	 * @param totalSalesAmount the new total sales amount
	 */
	public void setTotalSalesAmount(double totalSalesAmount) {
		this.totalSalesAmount = totalSalesAmount;
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		if(comment!=null)
			this.comment = comment;
	}

	/**
	 * Gets the total billing amount.
	 *
	 * @return the total billing amount
	 */
	public Double getTotalBillingAmount() {
		return totalBillingAmount;
	}

	/**
	 * Sets the total billing amount.
	 *
	 * @param totalBillingAmount the new total billing amount
	 */
	public void setTotalBillingAmount(double totalBillingAmount) {
		this.totalBillingAmount = totalBillingAmount;
	}

	/**
	 * Gets the array billing document.
	 *
	 * @return the array billing document
	 */
	public List<BillingDocumentDetails> getArrayBillingDocument() {
		return arrayBillingDocument;
	}

	/**
	 * Sets the array billing document.
	 *
	 * @param arrayBillingDocument the new array billing document
	 */
	public void setArrayBillingDocument(List<BillingDocumentDetails> arrayBillingDocument) {
		ArrayList<BillingDocumentDetails> arrBilling=new ArrayList<BillingDocumentDetails>();
		arrBilling.addAll(arrayBillingDocument);
		this.arrayBillingDocument = arrBilling;
	}

	/**
	 * Gets the invoice date.
	 *
	 * @return the invoice date
	 */
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	/**
	 * Sets the invoice date.
	 *
	 * @param invoiceDate the new invoice date
	 */
	public void setInvoiceDate(Date invoiceDate) {
		if(invoiceDate!=null)
			this.invoiceDate = invoiceDate;
	}

	/**
	 * Gets the invoice type.
	 *
	 * @return the invoice type
	 */
	public String getInvoiceType() {
		return invoiceType;
	}

	/**
	 * Sets the invoice type.
	 *
	 * @param invoiceType the new invoice type
	 */
	public void setInvoiceType(String invoiceType) {
		if(invoiceType!=null)
			this.invoiceType = invoiceType;
	}
	
	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}
	
	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(Date paymentDate) {
		if(paymentDate!=null)
			this.paymentDate = paymentDate;
	}

	/**
	 * Gets the invoice count.
	 *
	 * @return the invoice count
	 */
	public int getInvoiceCount() {
		return invoiceCount;
	}

	/**
	 * Sets the invoice count.
	 *
	 * @param invoiceCount the new invoice count
	 */
	public void setInvoiceCount(int invoiceCount) {
		this.invoiceCount = invoiceCount;
	}

	/**
	 * Gets the invoice amount.
	 *
	 * @return the invoice amount
	 */
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	/**
	 * Sets the invoice amount.
	 *
	 * @param invoiceAmount the new invoice amount
	 */
	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	

	/**
	 * Gets the sales order products.
	 *
	 * @return the sales order products
	 */
	public ArrayList<SalesOrderProductLineItem> getSalesOrderProducts() {
		return salesOrderProducts;
	}

	/**
	 * Sets the sales order products.
	 *
	 * @param salesOrderProducts the new sales order products
	 */
	/**
	 * Sets the sales order products.
	 *
	 * @param salesOrderProducts the new sales order products
	 */
	public void setSalesOrderProducts(ArrayList<SalesOrderProductLineItem> salesOrderProducts) {
		if(salesOrderProducts!=null){
			for(int i=0;i<salesOrderProducts.size();i++){
				salesOrderProducts.get(i).setOrderId(this.getContractCount());
			}
			this.salesOrderProducts = salesOrderProducts;
		}
	}
	
	/**
	 * Gets the payment method.
	 *
	 * @return the payment method
	 */
	public String getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * Sets the payment method.
	 *
	 * @param paymentMethod the new payment method
	 */
	public void setPaymentMethod(String paymentMethod) {
		if(paymentMethod!=null)
			this.paymentMethod = paymentMethod;
	}
	
	/**
	 * Gets the arr pay terms.
	 *
	 * @return the arr pay terms
	 */
	public ArrayList<PaymentTerms> getArrPayTerms() {
		return arrPayTerms;
	}

	/**
	 * Sets the arr pay terms.
	 *
	 * @param arrPayTerms the new arr pay terms
	 */
	public void setArrPayTerms(ArrayList<PaymentTerms> arrPayTerms) {
		if(arrPayTerms!=null)
		{
			this.arrPayTerms = new ArrayList<PaymentTerms>();
			this.arrPayTerms.addAll(arrPayTerms);
		}
		
	}
	
	public String getTypeOfOrder() {
		return typeOfOrder;
	}

	public void setTypeOfOrder(String typeOfOrder) {
		if(typeOfOrder!=null){
			this.typeOfOrder = typeOfOrder.trim();
		}
	}
	
	public List<ContractCharges> getBillingOtherCharges() {
		return billingOtherCharges;
	}

	public void setBillingOtherCharges(List<ContractCharges> billingOtherCharges) {
		ArrayList<ContractCharges> chargebill=new ArrayList<ContractCharges>();
		chargebill.addAll(billingOtherCharges);
		this.billingOtherCharges = chargebill;
	}

	public List<ContractCharges> getBillingTaxes() {
		return billingTaxes;
	}

	public void setBillingTaxes(List<ContractCharges> billingTaxes) {
		ArrayList<ContractCharges> taxbill=new ArrayList<ContractCharges>();
		taxbill.addAll(billingTaxes);
		this.billingTaxes = taxbill;
	}

	public Date getOrderCreationDate() {
		return orderCreationDate;
	}

	public void setOrderCreationDate(Date orderCreationDate) {
		if(orderCreationDate!=null){
			this.orderCreationDate = orderCreationDate;
		}
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		if(accountType!=null){
			this.accountType = accountType;
		}
	}
	
	public String getOrderCformStatus() {
		return orderCformStatus;
	}

	public void setOrderCformStatus(String orderCformStatus) {
		if(orderCformStatus!=null){
			this.orderCformStatus = orderCformStatus.trim();
		}
	}

	public double getOrderCformPercent() {
		return orderCformPercent;
	}

	public void setOrderCformPercent(double orderCformPercent) {
		this.orderCformPercent = orderCformPercent;
	}
	
	public boolean isRenewFlag() {
		return renewFlag;
	}

	public void setRenewFlag(boolean renewFlag) {
		this.renewFlag = renewFlag;
	}
	
	public String getInvoiceCategory() {
		return invoiceCategory;
	}

	public void setInvoiceCategory(String invoiceCategory) {
		if(invoiceCategory!=null){
			this.invoiceCategory = invoiceCategory.trim();
		}
	}

	public String getInvoiceConfigType() {
		return invoiceConfigType;
	}

	public void setInvoiceConfigType(String invoiceConfigType) {
		if(invoiceConfigType!=null){
			this.invoiceConfigType = invoiceConfigType.trim();
		}
	}

	public String getInvoiceGroup() {
		return invoiceGroup;
	}

	public void setInvoiceGroup(String invoiceGroup) {
		if(invoiceGroup!=null){
			this.invoiceGroup = invoiceGroup.trim();
		}
	}
	
	
	/*
	 * Date:04/08/2018
	 * Developer:Ashwini
	 */
	public int getPaymentAmt() {
		return paymentAmt;
	}

	public void setPaymentAmt(int paymentAmt) {
		this.paymentAmt = paymentAmt;
	}

	/*
	 * End by Ashwini
	 */

	/**************************************RELATION MANAGEMENT PART*****************************************/
	
	
	@OnSave	
	@GwtIncompatible
    protected void createPersonKey()
    {
		/*
		 * @author Ashwini Patil
		 * Date:11-08-2023
		 * Pecopp reported issue that sometimes while creating tax invoice from proforma invoice, wrong customer name appears in tax invoice.
		 * After checking reported entries from backend, found personinfokey field value set in such faulty records.
		 * commenting this code to avoid same issue in future.
		 */
		
		
//		System.out.println("ppppppppppppppppppppppppdipdip");
//		MyQuerry querry=new MyQuerry();
//		Filter filter=new Filter();
//		filter.setIntValue(personInfo.getCount());
//		filter.setQuerryString("customerCount");
//		querry.getFilters().add(filter);
//		querry.setQuerryObject(new CustomerRelation());
//		if(id==null)
//		{
//			personInfoKey=(Key<CustomerRelation>) MyUtility.getRelationalKeyFromCondition(querry);
//		}
    }

	
	  @OnLoad
	   @GwtIncompatible
	   public void onLoad()
	   {
		if(salespProductKey!=null)
		{
			salespProductKey=new ArrayList<Key<SuperProduct>>();
			int i=0;
			for(Key<SuperProduct>  prodKey:salespProductKey)
			{
				SuperProduct prod= ofy().load().key(prodKey).now();
				if(prodKey!=null)
				{
					salesOrderProducts.get(i).setProdCode(prod.getProductCode());
					salesOrderProducts.get(i).setProdName(prod.getProductName());
					salesOrderProducts.get(i).setProdCategory(prod.getProductCategory());
					i++;
				}
			}
		}
		
		  if(personInfoKey!=null)
		   {
			   CustomerRelation custrelation=MyUtility.getCustomerRelationFromKey(personInfoKey);
			   if(custrelation!=null&&personInfo!=null)
			   {
		    	this.personInfo.setCellNumber(custrelation.getCellNumber());
		    	this.personInfo.setCount(custrelation.getCustomerCount());
		    	this.personInfo.setFullName(custrelation.getFullName());
			   }
		   }
	   }

	public Date getConStartDur() {
		return conStartDur;
	}

	public void setConStartDur(Date conStartDur) {
		this.conStartDur = conStartDur;
	}

	public Date getConEndDur() {
		return conEndDur;
	}

	public void setConEndDur(Date conEndDur) {
		this.conEndDur = conEndDur;
	}

	public String getCustomerBranch() {
		return customerBranch;
	}

	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}

	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public Date getBillingPeroidFromDate() {
		return billingPeroidFromDate;
	}

	public void setBillingPeroidFromDate(Date billingPeroidFromDate) {
		this.billingPeroidFromDate = billingPeroidFromDate;
	}

	public Date getBillingPeroidToDate() {
		return billingPeroidToDate;
	}

	public void setBillingPeroidToDate(Date billingPeroidToDate) {
		this.billingPeroidToDate = billingPeroidToDate;
	}

	public int getRateContractServiceId() {
		return rateContractServiceId;
	}

	public void setRateContractServiceId(int rateContractServiceId) {
		this.rateContractServiceId = rateContractServiceId;
	}



	public boolean isRenewContractFlag() {
		return renewContractFlag;
	}



	public void setRenewContractFlag(boolean renewContractFlag) {
		this.renewContractFlag = renewContractFlag;
	}



	public String getPaymentMode() {
		return paymentMode;
	}



	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}



	public ArrayList<Integer> getServiceId() {
		return serviceId;
	}



	public void setServiceId(ArrayList<Integer> serviceId) {
		this.serviceId = serviceId;
	}



	public String getCancleAmtRemark() {
		return cancleAmtRemark;
	}



	public void setCancleAmtRemark(String cancleAmtRemark) {
		this.cancleAmtRemark = cancleAmtRemark;
	}

	public double getCancleAmt() {
		return cancleAmt;
	}

	public void setCancleAmt(double cancleAmt) {
		this.cancleAmt = cancleAmt;
	}




	public ArrayList<Integer> getCncBillAnnexureId() {
		return cncBillAnnexureId;
	}




	public void setCncBillAnnexureId(ArrayList<Integer> cncBillAnnexureId) {
		this.cncBillAnnexureId = cncBillAnnexureId;
	}




	public double getManagementFeesPercent() {
		return managementFeesPercent;
	}




	public void setManagementFeesPercent(double managementFeesPercent) {
		this.managementFeesPercent = managementFeesPercent;
	}




	public boolean isDonotprintServiceAddress() {
		return donotprintServiceAddress;
	}




	public void setDonotprintServiceAddress(boolean donotprintServiceAddress) {
		this.donotprintServiceAddress = donotprintServiceAddress;
	}




	public String getInvoicePdfNameToPrint() {
		return invoicePdfNameToPrint;
	}




	public void setInvoicePdfNameToPrint(String invoicePdfNameToPrint) {
		this.invoicePdfNameToPrint = invoicePdfNameToPrint;
	}




	

	

}
