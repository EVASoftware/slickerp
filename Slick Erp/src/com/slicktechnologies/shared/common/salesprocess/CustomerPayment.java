package com.slicktechnologies.shared.common.salesprocess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;


// TODO: Auto-generated Javadoc
/**
 *Repersent Payment Corresponding to Invoice.This class will get changed in near future 
 *because we need a Super Class Payment.
 */
@Entity
public class CustomerPayment extends SalesPaymentProcess {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1941586031397647295L;
	
/********************************************Attributes***************************************************/
	
	/** The bank name. */
	protected String bankName;
	
	/** The bank branch. */
	protected String bankBranch;
	
	/** The bank acc no. */
	protected String bankAccNo;
	
	/** The cheque no. */
	protected String chequeNo;
	
	/** The cheque date. */
	protected Date chequeDate;
	
	/** The cheque amount. */
	protected double chequeAmount;
	
	
	/** The amount transfer date. */
	protected Date amountTransferDate;

	/** The reference no. */
	protected String referenceNo;
	
	/** The payment received. */
	protected double paymentReceived;
	
	/** The payment amount */
	protected int paymentAmt;
	
	/** The payment amount in double */
	protected double paymentAmtInDecimal;

	protected String chequeIssuedBy;
	
	protected double netPay;
	
	protected double tdsTaxValue;
	
	protected String tdsPercentage;
	
	@Index
	protected boolean tdsApplicable;
	
	/** The cheque amount. */
	protected double baseAmmount;
	
	
	@Index 
	protected int ticketId;
	
	/**
	 * Date 1 march 2017 added by vijay 
	 * for add cash payment into petty cash entry
	 */
	protected boolean addintoPettyCash;
	protected String pettyCashName;
	
	/**
	 * ends here
	 */
	
	// rohan added this code for selection of customer payment record
	protected boolean recordSelect;
	
	/**
	 * Date 21-12-2017 added by vijay 
	 * for TDS Certificate information
	 */
	@Index
	protected boolean tdsCertificateReceived;
	@Index
	protected int tdsId;
	@Index
	protected Date tdsDate;
	protected Vector<DocumentUpload> tdsCertificate;
	
	/** date 21.02.2018 added by komal for actual revenue and tax calculation**/
	@Index 
	protected double balanceTax;
	
	@Index 
	protected double balanceRevenue;
	
	@Index
	protected double receivedTax;
	
	@Index
	protected double receivedRevenue;
	
	/**
	 * ends here
	 */
	
	/** Date 09-05-2018 added by Vijay for  Follow-up date **/ 
	@Index
	protected Date followUpDate;
	/**
	 * ends here
	 */
	/**Date 26-11-2019 by Amol for reference Number 2.**/
	protected String refNumberTwo;
	
	@Index
	protected int creditNoteId;
	/** Added by Priyanka **/
	protected Vector<DocumentUpload> docUpload1;
	
	//Ashwini Patil Date:23-09-2022
	protected String invRefNumber;
	

	

	/**************************************Constructor**************************************************/	
	/**
	 * Instantiates a new customer payment.
	 */
	public CustomerPayment() {
		super();
		recordSelect=false;
		tdsCertificateReceived=false;
		tdsCertificate = new Vector<DocumentUpload>();
		docUpload1=new Vector<DocumentUpload>();
	}
	
/*********************************************************************************************************/
	
	
/******************************************Getters And Setters*********************************************/
	
	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}
	
	public boolean getRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	
	/**
	 * Gets the bank name.
	 *
	 * @return the bank name
	 */
	
	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	
//	public double getTdsPercentage() {
//		return tdsPercentage;
//	}
//
//	public void setTdsPercentage(double tdsPercentage) {
//		this.tdsPercentage = tdsPercentage;
//	}
	
	
	
	

	public double getBaseAmmount() {
		return baseAmmount;
	}

	public void setBaseAmmount(double baseAmmount) {
		this.baseAmmount = baseAmmount;
	}

	public String getBankName() {
		return bankName;
	}
	

	public String getChequeIssuedBy() {
		return chequeIssuedBy;
	}

	public void setChequeIssuedBy(String chequeIssuedBy) {
		this.chequeIssuedBy = chequeIssuedBy;
	}

	/**
	 * Sets the bank name.
	 *
	 * @param bankName the new bank name
	 */
	public void setBankName(String bankName) {
		if(bankName!=null)
			this.bankName = bankName;
	}
	
	/**
	 * Gets the bank branch.
	 *
	 * @return the bank branch
	 */
	public String getBankBranch() {
		return bankBranch;
	}
	
	/**
	 * Sets the bank branch.
	 *
	 * @param bankBranch the new bank branch
	 */
	public void setBankBranch(String bankBranch) {
		if(bankBranch!=null)
			this.bankBranch = bankBranch;
	}
	
	/**
	 * Gets the bank acc no.
	 *
	 * @return the bank acc no
	 */
	public String getBankAccNo() {
		return bankAccNo;
	}
	
	/**
	 * Sets the bank acc no.
	 *
	 * @param bankAccNo the new bank acc no
	 */
	public void setBankAccNo(String bankAccNo) {
		if(bankAccNo!=null)
			this.bankAccNo = bankAccNo;
	}
	
	
	
//	/**
//	 * Gets the cheque no.
//	 *
//	 * @return the cheque no
//	 */
//	public Integer getChequeNo() {
//		return chequeNo;
//	}
//	
//	/**
//	 * Sets the cheque no.
//	 *
//	 * @param chequeNo the new cheque no
//	 */
//	public void setChequeNo(int chequeNo) {
//		this.chequeNo = chequeNo;
//	}
	
	public double getNetPay() {
		return netPay;
	}

	public void setNetPay(double netPay) {
		this.netPay = netPay;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	
	
	
	
	
	
	
	public double getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(double paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	/**
	 * Gets the cheque date.
	 *
	 * @return the cheque date
	 */
	public Date getChequeDate() {
		return chequeDate;
	}
	
	

	/**
	 * Sets the cheque date.
	 *
	 * @param chequeDate the new cheque date
	 */
	public void setChequeDate(Date chequeDate) {
		if(chequeDate!=null)
			this.chequeDate = chequeDate;
	}
	
	/**
	 * Gets the cheque amount.
	 *
	 * @return the cheque amount
	 */
	public Double getChequeAmount() {
		return chequeAmount;
	}
	
	/**
	 * Sets the cheque amount.
	 *
	 * @param chequeAmount the new cheque amount
	 */
	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	
	/**
	 * Gets the amount transfer date.
	 *
	 * @return the amount transfer date
	 */
	public Date getAmountTransferDate() {
		return amountTransferDate;
	}
	
	/**
	 * Sets the amount transfer date.
	 *
	 * @param amountTransferDate the new amount transfer date
	 */
	public void setAmountTransferDate(Date amountTransferDate) {
//		if(amountTransferDate!=null)
			this.amountTransferDate = amountTransferDate;
	}
	
	/**
	 * Gets the reference no.
	 *
	 * @return the reference no
	 */
	public String getReferenceNo() {
		return referenceNo;
	}
	
	/**
	 * Sets the reference no.
	 *
	 * @param referenceNo the new reference no
	 */
	public void setReferenceNo(String referenceNo) {
		if(referenceNo!=null)
			this.referenceNo = referenceNo;
	}
	
//	/**
//	 * Gets the payment received.
//	 *
//	 * @return the payment received
//	 */
//	public Integer getPaymentReceived() {
//		return paymentReceived;
//	}
//	
//	/**
//	 * Sets the payment received.
//	 *
//	 * @param paymentReceived the new payment received
//	 */
//	public void setPaymentReceived(int paymentReceived) {
//		this.paymentReceived = paymentReceived;
//	}
	
	public int getPaymentAmt() {
		return paymentAmt;
	}

	public void setPaymentAmt(int paymentAmt) {
		this.paymentAmt = paymentAmt;
	}
	
	
//	public int getNetPay() {
//		return netPay;
//	}
//
//	public void setNetPay(int netPay) {
//		this.netPay = netPay;
//	}

//	public int getTdsTaxValue() {
//		return tdsTaxValue;
//	}
//
//	public void setTdsTaxValue(int tdsTaxValue) {
//		this.tdsTaxValue = tdsTaxValue;
//	}

	
	
	
	

	public double getTdsTaxValue() {
		return tdsTaxValue;
	}

	public String getTdsPercentage() {
		return tdsPercentage;
	}

	public void setTdsPercentage(String tdsPercentage) {
		this.tdsPercentage = tdsPercentage;
	}

	public void setTdsTaxValue(double tdsTaxValue) {
		this.tdsTaxValue = tdsTaxValue;
	}


	public boolean isTdsApplicable() {
		return tdsApplicable;
	}

	public void setTdsApplicable(boolean tdsApplicable) {
		this.tdsApplicable = tdsApplicable;
	}
	
	public boolean isAddintoPettyCash() {
		return addintoPettyCash;
	}

	public void setAddintoPettyCash(boolean addintoPettyCash) {
		this.addintoPettyCash = addintoPettyCash;
	}
	
	public String getPettyCashName() {
		return pettyCashName;
	}

	public void setPettyCashName(String pettyCashName) {
		this.pettyCashName = pettyCashName;
	}
	
	
	
	public boolean isTdsCertificateReceived() {
		return tdsCertificateReceived;
	}

	public void setTdsCertificateReceived(boolean tdsCertificateReceived) {
		this.tdsCertificateReceived = tdsCertificateReceived;
	}

	public int getTdsId() {
		return tdsId;
	}

	public void setTdsId(int tdsId) {
		this.tdsId = tdsId;
	}

	public Date getTdsDate() {
		return tdsDate;
	}

	public void setTdsDate(Date tdsDate) {
		this.tdsDate = tdsDate;
	}
	
	
	public double getBalanceTax() {
		return balanceTax;
	}

	public void setBalanceTax(double balanceTax) {
		this.balanceTax = balanceTax;
	}

	public double getBalanceRevenue() {
		return balanceRevenue;
	}

	public void setBalanceRevenue(double balanceRevenue) {
		this.balanceRevenue = balanceRevenue;
	}

	public double getReceivedTax() {
		return receivedTax;
	}

	public void setReceivedTax(double receivedTax) {
		this.receivedTax = receivedTax;
	}

	public double getReceivedRevenue() {
		return receivedRevenue;
	}

	public void setReceivedRevenue(double receivedRevenue) {
		this.receivedRevenue = receivedRevenue;
	}

	public DocumentUpload getTdsCertificate() {
		if(tdsCertificate.size()!=0)
			  return tdsCertificate.get(0);
			return null;
	}

	public void setTdsCertificate(DocumentUpload documentUpload) {
		this.tdsCertificate=new Vector<DocumentUpload>();
		if(documentUpload!=null)
		   this.tdsCertificate.add(documentUpload);
	}
	
	
	/*************************************************************************************************/
	




	

	public String getRefNumberTwo() {
		return refNumberTwo;
	}

	public void setRefNumberTwo(String refNumberTwo) {
		this.refNumberTwo = refNumberTwo;
	}

	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList() {
		ArrayList<String>list=new ArrayList<String>();
		list.add(CustomerPayment.CREATED);
		list.add(CustomerPayment.PAYMENTCLOSED);
		list.add(CustomerPayment.CANCELLED);
		return list;
	}
	
	/***18-10-2017 sagar sore [Balance amount calculation]**/

	public double getBalanceAmount()
	{
		double balanceAmount;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("CONTRACT","PC_NO_ROUNDOFF_INVOICE_CONTRACT")) {
			balanceAmount = this.getPaymentAmtInDecimal()-this.getPaymentReceived();
		}else {
			balanceAmount = this.getPaymentAmt()-this.getPaymentReceived();
		}
		
		return balanceAmount;
	}
	
	@com.googlecode.objectify.annotation.OnSave
	@GwtIncompatible
	public void reactOnSubmitPayment()
	{
		Logger logger=Logger.getLogger("Customerpayment.class");

		if(getStatus().equals(CustomerPayment.PAYMENTCLOSED)){
		
		
			System.out.println("Querying Processname");
			ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
			System.out.println("Process Name Query Executed");
			ProcessConfiguration processConfig=null;
			if(processName!=null){
				System.out.println("NotNull Process Name");
				processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGCP).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
			}
			System.out.println("Process Config Executed");
			int flag =0;
			
			if(processConfig!=null){
				System.out.println("Process Config Not Null");
				for(int i = 0;i<processConfig.getProcessList().size();i++){
					System.out.println("----------------------------------------------------------");
					System.out.println("One===== "+processConfig.getProcessList().get(i).getProcessType());
					System.out.println("One===== "+processConfig.getProcessList().get(i).isStatus());
					System.out.println("----------------------------------------------------------");
					if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
						System.out.println("Flag One=====*****=====");
						flag = 1;
					}
				}
			}
			
			if(flag == 1&&processConfig!=null){
				System.out.println("ExecutingAccInterface");
				ServerAppUtility serverutility = new ServerAppUtility();
				boolean einvoiceFlag = serverutility.checkEInvoiceConfigAtivation(this.getCompanyId(), this.getBranch());
				logger.log(Level.SEVERE, "einvoiceFlag"+einvoiceFlag);
				Invoice invoiceentity = ofy().load().type(Invoice.class).filter("companyId", this.getCompanyId()).filter("count", this.getInvoiceCount()).first().now();
					
				
				/*
				 * Ashwini Patil 
				 * Date:4-01-2024 
				 * Duplicate payment entries were getting created in accounting interface. 
				 * reported by ultra. 
				 * It was happening when they were updating customer name.
				 * When we customer name change program runs, it updated payments as well and since payment status is closed this reactonsubmit method gets executed again and duplicate accounting interface entry was getting created
				 * Now modifying program to delete old entries before creating new one.
				 */
				List<AccountingInterface> accountinginterfacelist = ofy().load().type(AccountingInterface.class).filter("companyId", this.getCompanyId())
						.filter("documentStatus", CustomerPayment.CLOSED).filter("documentID", this.getCount()).list();
				if(accountinginterfacelist.size()>0){
					logger.log(Level.SEVERE, "accountingInterface size"+accountinginterfacelist.size());
					for(AccountingInterface ac:accountinginterfacelist) {
						if(ac.getDocumentType().equals("Customer Payment")) {
							logger.log(Level.SEVERE, "deleting accounting interface old entry with id "+ac.getCount());
							ofy().delete().entity(ac);
						}
					}
					
				}
				
				
				if(!einvoiceFlag){
					accountingInterface("Payment.java check 1");
				}
				else if(einvoiceFlag && invoiceentity!=null && invoiceentity.getIRN()!=null && !invoiceentity.getIRN().equals("")){
					accountingInterface("Payment.java check 2");
				}
			}
	}
	}
	
	
	
	
	@GwtIncompatible
	public void accountingInterface(String callFrom){
		Logger logger = Logger.getLogger("Name of logger");
		if(callFrom!=null)
			logger.log(Level.SEVERE,"in accountingInterface callFrom: "+callFrom);
	System.out.println("in side accnt interface");
		
		if(this.getCount()!=0&&getStatus().equals(CustomerPayment.PAYMENTCLOSED) || this.getCount()!=0&&getStatus().equals(CustomerPayment.CANCELLED)){
			
			System.out.println("Product List Size :: "+this.getSalesOrderProducts().size());
			
			/*************************************************************************************************************/
			//Invoice so=ofy().load().type(Invoice.class).filter("companyId",getCompanyId()).filter("count", this.getInvoiceCount()).first().now();
			Invoice so = null;
			if(this.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
				so=ofy().load().type(VendorInvoice.class).filter("companyId",getCompanyId()).filter("count", this.getInvoiceCount()).first().now();
				System.out.println("APPPP");
			}else{
				so=ofy().load().type(Invoice.class).filter("companyId",getCompanyId()).filter("count", this.getInvoiceCount()).first().now();
			}
			for(int  i = 0;i<1;i++){//so.getSalesOrderProducts().size()
				
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					cust=ofy().load().type(Customer.class).filter("companyId",getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
				}
				if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
				}				
				if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					vendor=ofy().load().type(Vendor.class).filter("companyId",getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
				}
				
		/*************************************************Start************************************************/
				
				 int prodId=0;
				 String productCode ="";
				 String productName = "";
				 
//				 int prodId=so.getSalesOrderProducts().get(i).getProdId();
//				 String productCode = so.getSalesOrderProducts().get(i).getProdCode();
//				 String productName = so.getSalesOrderProducts().get(i).getProdName();
				 /**
				  * @author Vijay Purchase payment QTy details if condition and else for other old code added 
				  */
				 double productQuantity = 0;
				 double prodQty = 0;
				 if(this.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
					 /**
					  * @author Anil @since 03-06-2021
					  * added NA condition to avoid task queue failure and multiple bill and invoice creation
					  * also payment was not getting created
					  */
					 if(so.getSalesOrderProducts().get(i).getArea()!=null&& !so.getSalesOrderProducts().get(i).getArea().equalsIgnoreCase("NA") && !so.getSalesOrderProducts().get(i).getArea().equals("")){
						 prodQty  = Double.parseDouble(so.getSalesOrderProducts().get(i).getArea());
					 }
				 }
				 else{
					 prodQty = so.getSalesOrderProducts().get(i).getQuantity();
				 }
//				 double productQuantity = so.getSalesOrderProducts().get(i).getQuantity();
//				 double totalAmount =  so.getSalesOrderProducts().get(i).getPrice()*productQuantity;
				 double totalAmount =  so.getSalesOrderProducts().get(i).getPrice()*prodQty;
				 double calculatedamt = 0;
//				 double calculatedamt = totalAmount*so.getSalesOrderProducts().get(i).getVatTax().getPercentage()/100;
				 double productprice =0;
//				 double productprice = (so.getSalesOrderProducts().get(i).getPrice());
				 double calculetedservice = 0;
				 
				 double cformamtService = 0;
				 
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;
				 double vatPercent=0;
				 double servicePercent=0;
				 String cform = "";
				 double cformP=0;
				 Date contractStartDate=null;
				 Date contractEndDate=null;
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
				 int personInfoId = 0;
				 String personInfoName="";
				 long personInfoCell = 0;
				 int vendorInfoId = 0;
				 String vendorInfoName="";
				 long vendorInfoCell=0;
				 int empInfoId;
				 String empInfoName="";
				 long empinfoCell;
				 
				 String addrLine1="";
				 String addrLocality="";
				 String addrLandmark="";
				 String addrCountry="";
				 String addrState="";
				 String addrCity="";
				 long addrPin=0;
				 
				 
				 
				 
			/******************************************for cform*********************************************/
				 if(so.getOrderCformStatus()!= null&&so.getOrderCformStatus().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=so.getOrderCformPercent();
					 cformAmount=so.getOrderCformPercent()*totalAmount/100;
					 cformamtService=so.getOrderCformPercent()*totalAmount/100;
					 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//						 serviceTax=so.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//						 calculetedservice=((cformamtService+totalAmount)*so.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//						 System.out.println("calculetedservice 1111 = "+calculetedservice);
					 }
					 /**
					  * Date:01-08-2017 By ANIL
					  * GLAccount is not required while creating accounting interface
					  */
					// TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",so.getOrderCformPercent()).filter("isCentralTax",true).first().now();
					// GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					// cformglaccno=serglAcc.getGlAccountNo();
					
				 }
				 
				 else if(so.getOrderCformStatus()!= null&&so.getOrderCformStatus().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=so.getSalesOrderProducts().get(i).getVatTax().getPercentage();
					 cformAmount=so.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 cformamtService=so.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 
					 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
//						 serviceTax=so.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
//						 calculetedservice=((cformamtService+totalAmount)*so.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;
//						 System.out.println("calculetedservice =$ "+calculetedservice);
					 }
					 /**
					  * Date:01-08-2017 By ANIL
					  * GLAccount is not required while creating accounting interface
					  */
					 //TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargeName",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
					 //GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 //cformglaccno=serglAcc.getGlAccountNo();
				 }
				 
				 else{//if the customer is of same state
					 
					 if(so.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
					 {
//						 vatPercent=so.getSalesOrderProducts().get(i).getVatTax().getPercentage();
//						 calculatedamt=so.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
//						 System.out.println("calculatedamt == "+calculatedamt);
						/**
					  * Date:01-08-2017 By ANIL
					  * GLAccount is not required while creating accounting interface
					  */

						//TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",so.getSalesOrderProducts().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
						// GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
						// vatglaccno=vatglAcc.getGlAccountNo();
					 }
					 
					 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
					 {
//						 serviceTax=so.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						//changed by komal
//						 calculetedservice=(totalAmount)*so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100;
//						 System.out.println("calculetedservice "+calculetedservice);
					 }
					 
					 
				 }
				 
				 
				
				 
				 if(so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
				 {
					 /**
					  * Date:01-08-2017 By ANIL
					  * GLAccount is not required while creating accounting interface
					  */
					// TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",so.getSalesOrderProducts().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
					 //GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
					 //serglaccno=serglAcc.getGlAccountNo();
				 }
				 
				 
				 
				 
				 
				 
	/****************************************************************************************/
				 
				 
				 
//				 double totalAmount = getTotalAmount();
				 double netPayable = so.getTotalSalesAmount();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
				 
				 
				 for(int k = 0; k < so.getBillingOtherCharges().size();k++){
					 oc[k] = so.getBillingOtherCharges().get(k).getTaxChargeName();
					 oca[k] = so.getBillingOtherCharges().get(k).getPayableAmt();
				 }
				 
				 
				 if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					 
					 personInfoId=this.getPersonInfo().getCount();
//					 personInfoName=this.getPersonInfo().getFullName();   // commented by Ashwini
					   
					    /*
						 * @Author Ashwini
						 * Date:17-11-2018
						 * Des:to take correspondance name if present
						 */
					 /** Date 05-12-2018 By Vijay
						 * Des :- updated with != blank in if condition
						 */
						if(cust.getCustPrintableName()!=null &&!cust.getCustPrintableName().equals("")){
							personInfoName=cust.getCustPrintableName();
						}else{
							personInfoName=this.getPersonInfo().getFullName();
						}
						
						/*
						 * end by Ashwini
						 */

					 personInfoCell=this.getPersonInfo().getCellNumber();
					 
					 addrLine1=cust.getAdress().getAddrLine1();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
				 }
				 

				 if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					 
					 personInfoId=this.getPersonInfo().getCount();
//					 personInfoName=this.getPersonInfo().getFullName();   //commented by Ashwini
					 /*
						 * @Author Ashwini
						 * Date:17-11-2018
						 * Des:to take correspondance name if present
						 */
					 /** Date 05-12-2018 By Vijay
						 * Des :- updated with != blank in if condition
						 */
						if(cust.getCustPrintableName()!=null &&!cust.getCustPrintableName().equals("") ){
							personInfoName=cust.getCustPrintableName();
						}else{
							personInfoName=this.getPersonInfo().getFullName();
						}
						
						/*
						 * end by Ashwini
						 */

					 personInfoCell=this.getPersonInfo().getCellNumber();
					 
					 addrLine1=cust.getAdress().getAddrLine1();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
					 if(this.getContractStartDate()!=null){
						 contractStartDate=this.getContractStartDate();
					 }
					 if(this.getContractEndDate()!=null){
						 contractEndDate=this.getContractEndDate();
					 }
				 }
				 
				 

				 if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					 
					 vendorInfoId=this.getPersonInfo().getCount();
					 vendorInfoName=this.getPersonInfo().getFullName();
					 vendorInfoCell=this.getCellNumber();
					 
					 addrLine1=vendor.getPrimaryAddress().getAddrLine1();
					 addrLocality=vendor.getPrimaryAddress().getLocality();
					 addrLandmark=vendor.getPrimaryAddress().getLandmark();
					 addrCountry=vendor.getPrimaryAddress().getCountry();
					 addrState=vendor.getPrimaryAddress().getState();
					 addrCity=vendor.getPrimaryAddress().getCity();
					 addrPin=vendor.getPrimaryAddress().getPin();
				 }
				 
			 		
				 
				 /**********************************End************************************************/
				 System.out.println("in side accnt interface:::: before updateTally");
				 Double tdsper=0d;
				 double tdsValue = 0;
				 		if(this.getTdsPercentage()!=null && !this.getTdsPercentage().equals("")){
				 			tdsper = Double.parseDouble(this.getTdsPercentage());
						}
						else
						{
							tdsper= 0d;
						}
				 		
				 		
				 		
				 		if(this.getTdsTaxValue()!=0.0)
				 		{
				 			tdsValue =this.getTdsTaxValue();
				 		}
				 		else
				 		{
//				 			tdsValue=0;
				 			/**
				 			 * @author Vijay Date :-19-05-2023
				 			 * Des :- when we added tds with percentage its not reflecting on accounting interface tally export
				 			 */
				 			if(tdsper>0){
				 				if(this.getBalanceRevenue()!=0){
					 				double tdsAmt = (this.getBalanceRevenue()*tdsper)/100;
					 				tdsValue=tdsAmt;
					 				logger.log(Level.SEVERE, "tdsValue "+tdsValue);
					 			}
				 			}
				 			/**
				 			 * ends here
				 			 */
				 			
				 			
				 		}
				 		
				 		double baseAmt=0;
				 		if(this.getBaseAmmount()!=0)
				 		{
				 			baseAmt = this.getBaseAmmount();
				 		}
				 		
				 		
				 		/**
				 		 * @author Anil ,Date : 31-01-2019
				 		 * if payment mode is selected then we will print selected bank name else default bank would come
				 		 * for Pepcopp raised by Sonu
				 		 */
				 		int compPayId=0;
				 		if(this.getPaymentMode()!=null&&!this.getPaymentMode().equals("")){
				 			List<String> compPayment = Arrays.asList(this.getPaymentMode().trim().split("/"));
				 			if(compPayment.get(0).trim().matches("[0-9]+")){
								compPayId = Integer.parseInt(compPayment.get(0).trim());
				 			}
				 		}
				 		
				 		CompanyPayment companyPayment=null;
				 		if(compPayId!=0){
				 			companyPayment = ofy().load().type(CompanyPayment.class).filter("companyId", getCompanyId()).filter("count", compPayId).first().now();
				 		}else{
				 			/** date 29/03/2018 added by  komal for new tally interface requirement**/
				 			companyPayment = ofy().load().type(CompanyPayment.class).filter("companyId", getCompanyId()).filter("paymentStatus", true).filter("paymentDefault", true).first().now();
				 		}
				 		 logger.log(Level.SEVERE , "company payment : " + companyPayment+" comp pay id "+compPayId);
				 		 String companyBankName = "";
				    	 if(companyPayment!=null){
				    		
				    		 companyBankName = companyPayment.getPaymentBankName();
				    		 logger.log(Level.SEVERE , "company payment : " + companyBankName);
				    	 }
				    	// logger.log(Level.SEVERE , "company payment : " + companyPayment.getPaymentBankName());
				    	 Date chequeDate = null;
				    	 String chequeNumber = "";
//				    	 if(this.getPaymentMethod().equalsIgnoreCase("NEFT")){
//				    		 if(this.getAmountTransferDate()!=null){
//					    		 chequeDate = this.getAmountTransferDate();
//				    		 }
//				    		 if(this.getReferenceNo()!=null)
//					    		 chequeNumber = this.getReferenceNo();
//				    		 }
				    	 if(this.getPaymentMethod().equalsIgnoreCase("Cheque")){
				    		 if(this.getChequeDate()!=null){
				    			 chequeDate = this.getChequeDate();
				    		 }
				    		 if(this.getChequeNo()!=null)
				    		 chequeNumber = this.getChequeNo();
				    	 }else {	//Ashwini Patil Date:27-09-2022			    		 
					    		 if(this.getAmountTransferDate()!=null){
						    		 chequeDate = this.getAmountTransferDate();
					    		 }
					    		 if(this.getReferenceNo()!=null)
						    		 chequeNumber = this.getReferenceNo();					    		 
				    	 }
				    	 /**
				    	  * end komal
				    	  */
				    		/***date 28.7.2018 added by komal for new date format **/
				    	 Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
				    	 
				    	 /**
				    	  * Date : 19-12-2018 BY ANIL
				    	  * adding customer payment comment value in accounting interface
				    	  * mapped comment value under contractCategory
				    	  * For Ankita Pest Control raised by Sonu
				    	  * this comment will map under narration
				    	  */
				    	 String comment="";
				    	 if(this.getComment()!=null){
				    		 comment=this.getComment();
				    	 }
				    	 /**
				    	  * End
				    	  */
				    	 
				    	 String gstn = "";
							if(this.getGstinNumber()!=null && !this.getGstinNumber().equals("")){
								gstn = this.getGstinNumber();
							}else{
								try{
									Customer customer=ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
									if(customer!=null){
										if(customer.getArticleTypeDetails()!=null){  						
											for(ArticleType object:customer.getArticleTypeDetails()){
												if(customer.getArticleTypeDetails().get(i).getDocumentName().equals("ServiceInvoice") ||
														customer.getArticleTypeDetails().get(i).getDocumentName().equals("Invoice Details")){
													if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
														gstn = object.getArticleTypeValue();
														break;
													}
												}
											}
										}
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							
				    	 
				    	 UpdateAccountingInterface.updateTally(currentDate,//accountingInterfaceCreationDate
								 this.getEmployee(),//accountingInterfaceCreatedBy
								 this.getStatus(),
								 AppConstants.STATUS ,//status
								 companyBankName,//remark
								 "Accounts",//module
								 "Customer Payment",//doc type
									this.getCount(),//doc ID
									" ",//doc title
									this.getAmountTransferDate(),//doc date
									this.getAccountType()+"-Payment Doc",//doc gl
									this.getInvoiceCount()+"",//ref doc no.1
									so.getCreationDate(),//ref. doc date 1
									"Invoice",//ref.doc.type.1
//									"",//ref doc no.2
									this.getRefNumber(),//ref doc no.2
									null,//ref doc date 2
									"",//ref doc type 2
									this.getAccountType(),//accounttype
									personInfoId,//custID	
									personInfoName,//custName	
									personInfoCell,//custCell
									vendorInfoId,//venID	
									vendorInfoName,//venName	
									vendorInfoCell,//venCell
									0,//emp Id
									" ",//emp Name
									this.getBranch(),//Branch
									this.getEmployee(),//personResponsible
									"",//requested By
									this.getApproverName(),//approverName
									this.getPaymentMethod(),//paymentmethod
									this.getPaymentDate(),//paymentDate
									chequeNumber,//cheque no./**changed by komal for new tally interface**/
									chequeDate,//cheque date/** changed by komal for new tally interface**/
									this.getBankName(),//bank name
									this.getBankAccNo(),//bank acc.
									0,//transferrefernceno.
									null,//transactiondate
									contractStartDate,//contract start date
									contractEndDate, // contract end date
									prodId,//prodId
									productCode,//prodCode
									productName,//prodName
									productQuantity,//prodQuantity
									null,//productDate
									0,//duration
									0,//services
									null,//UOM
									productprice,//product price
									vatPercent,//vat %
									calculatedamt,//vat amount
									vatglaccno,//vatglaccno.
									serviceTax,//service tax percent
									calculetedservice,//service tax amount
									serglaccno,//service tax gl accno.
									cform,//cformstatus
									cformPercent,//cformpercent
									cformAmount,//cformAmount
									cformglaccno,//cformglaccno
									totalAmount,//totalamount
									netPayable,//netpayable
									comment,//Contract Category
									this.getPaymentReceived(),//amountRecieved
									
									//   rohan added this 2 fields 
									baseAmt,
									tdsper,
									tdsValue,
									
									
									oc[0],
									oca[0],
									oc[1],
									oca[1],
									oc[2],
									oca[2],
									oc[3],
									oca[3],
									oc[4],
									oca[4],
									oc[5],
									oca[5],
									oc[6],
									oca[6],
									oc[7],
									oca[7],
									oc[8],
									oca[8],
									oc[9],
									oca[9],
									oc[10],
									oca[10],
									oc[11],
									oca[11], 
									addrLine1,
									addrLocality,
									addrLandmark,
									addrCountry,
									addrState,
									addrCity,
									addrPin,
									this.getCompanyId(),
									this.getBillingPeroidFromDate(),//  billing from date (rohan)
									this.getBillingPeroidToDate(), //  billing to date (rohan)
									"", //Warehouse
									"",				//warehouseCode
									"",				//ProductRefId
									"",				//Direction
									"",				//sourceSystem
									"",				//Destination Syste
									null,
									gstn,numberRange
									);

			
						}
					}
					logger.log(Level.SEVERE,"end of accountingInterface");
					
					}
	
	
	
	
	
	
	
	
	
	

	/**
	 * @author Vijay Chougule Date 19-06-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getChequeDate()!=null){
			this.setChequeDate(dateUtility.setTimeMidOftheDayToDate(this.getChequeDate()));
		}
		if(this.getFollowUpDate()!=null){
			this.setFollowUpDate(dateUtility.setTimeMidOftheDayToDate(this.getFollowUpDate()));
		}
		if(this.getInvoiceDate()!=null){
			this.setInvoiceDate(dateUtility.setTimeMidOftheDayToDate(this.getInvoiceDate()));
		}
		if(this.getPaymentDate()!=null){
			this.setPaymentDate(dateUtility.setTimeMidOftheDayToDate(this.getPaymentDate()));

		}
		if(this.getAmountTransferDate()!=null){
			this.setAmountTransferDate(dateUtility.setTimeMidOftheDayToDate(this.getAmountTransferDate()));

		}
		if(this.getBillingPeroidFromDate()!=null){
			this.setBillingPeroidFromDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidFromDate()));
		}
		if(this.getBillingPeroidToDate()!=null){
			this.setBillingPeroidToDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidToDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */

	public int getCreditNoteId() {
		return creditNoteId;
	}

	public void setCreditNoteId(int creditNoteId) {
		this.creditNoteId = creditNoteId;
	}

	public DocumentUpload getDocUpload1() {
		if(docUpload1.size()!=0)
			  return docUpload1.get(0);
			return null;
	}

	public void setDocUpload1(DocumentUpload docupload1) {
		this.docUpload1=new Vector<DocumentUpload>();
		if(docupload1!=null)
		   this.docUpload1.add(docupload1);
	}
	public double getPaymentAmtInDecimal() {
		return paymentAmtInDecimal;
	}

	public void setPaymentAmtInDecimal(double paymentAmtInDecimal) {
		this.paymentAmtInDecimal = paymentAmtInDecimal;
	}
	public String getInvRefNumber() {
		return invRefNumber;
	}

	public void setInvRefNumber(String invRefNumber) {
		this.invRefNumber = invRefNumber;
	}
}
