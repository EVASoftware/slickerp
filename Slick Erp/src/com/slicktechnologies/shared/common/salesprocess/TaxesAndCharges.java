package com.slicktechnologies.shared.common.salesprocess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class TaxesAndCharges extends SuperModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2336203935423677647L;
	
  /**********************************Applicability Attributes*******************************************/
	@Index
	protected Integer contractId;
	@Index
	protected String identifyOrder;
	protected ArrayList<ContractCharges> taxesChargesList;
	
	/***********************************Constructor*********************************************/
	
	public TaxesAndCharges() {
		super();
		taxesChargesList=new ArrayList<ContractCharges>();
		identifyOrder="";
	}
   
	/*****************************************************************************************************/
	
	/*****************************************Getters And Setters*****************************************/
	
	public Integer getContractId() {
		return contractId;
	}
	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}


	public List<ContractCharges> getTaxesChargesList() {
		return taxesChargesList;
	}
	public void setTaxesChargesList(List<ContractCharges> taxesChargesList) {
		ArrayList<ContractCharges> chargesArr=new ArrayList<ContractCharges>();
		chargesArr.addAll(taxesChargesList);
		System.out.println("INSIDE SETTING ORDER ID TO CHARGES LIST.....!!! :"+chargesArr.size()+" ID "+this.contractId);
		for(int i=0;i<chargesArr.size();i++){
			chargesArr.get(i).setOrderId(this.getContractId());
		}
		
		this.taxesChargesList = chargesArr;
	}
	
	public String getIdentifyOrder() {
		return identifyOrder;
	}

	public void setIdentifyOrder(String identifyOrder) {
		if(identifyOrder!=null){
			this.identifyOrder = identifyOrder.trim();
		}
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	
/***************************************Contract Charges Class********************************************/

	@Embed
	public static class ContractCharges implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -6460194026743324031L;
		
		protected int orderId;
		protected int billingId;
		
		protected String taxChargeName;
		protected double taxChargePercent;
		protected double taxChargeAbsVal;
		protected double taxChargeAssesVal;
		protected double chargesBalanceAmt;
		protected double payableAmt;
		protected double paypercent;
		protected int identifyTaxCharge;
		
		
		
		
		public int getBillingId() {
			return billingId;
		}
		public void setBillingId(int billingId) {
			this.billingId = billingId;
		}
		public int getOrderId() {
			return orderId;
		}
		public void setOrderId(int orderId) {
			this.orderId = orderId;
		}
		public String getTaxChargeName() {
			return taxChargeName;
		}
		public void setTaxChargeName(String taxChargeName) {
			if(taxChargeName!=null){
				this.taxChargeName = taxChargeName.trim();
			}
		}
		
		public double getTaxChargePercent() {
			return taxChargePercent;
		}
		public void setTaxChargePercent(double taxChargePercent) {
			this.taxChargePercent = taxChargePercent;
		}
		
		public double getTaxChargeAbsVal() {
			return taxChargeAbsVal;
		}
		public void setTaxChargeAbsVal(double taxChargeAbsVal) {
			this.taxChargeAbsVal = taxChargeAbsVal;
		}
		
		public double getTaxChargeAssesVal() {
			return taxChargeAssesVal;
		}
		public void setTaxChargeAssesVal(double taxChargeAssesVal) {
			this.taxChargeAssesVal = taxChargeAssesVal;
		}
		
		public double getChargesBalanceAmt() {
			return chargesBalanceAmt;
		}
		public void setChargesBalanceAmt(double chargesBalanceAmt) {
			this.chargesBalanceAmt = chargesBalanceAmt;
		}
		
		public double getPayableAmt() {
			return payableAmt;
		}
		public void setPayableAmt(double payableAmt) {
			this.payableAmt = payableAmt;
		}
		
		public double getPaypercent() {
			return paypercent;
		}
		public void setPaypercent(double paypercent) {
			this.paypercent = paypercent;
		}
		public int getIdentifyTaxCharge() {
			return identifyTaxCharge;
		}
		public void setIdentifyTaxCharge(int identifyTaxCharge) {
			this.identifyTaxCharge = identifyTaxCharge;
		}
		
		
		
	
		
	}

}
