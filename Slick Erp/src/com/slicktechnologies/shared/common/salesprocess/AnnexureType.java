package com.slicktechnologies.shared.common.salesprocess;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Serialize
public class AnnexureType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -759603369486765778L;
	
	String locationName;
	String designation;
	
	String noOfStaff;
	String ratePerBoy;
	double noOfDaysPresent;
	
	double normalDayOt;
	double phOt;
	double nhOt;
	
	double otAmount;
	double totalAmount;
	
	double sgstAmt;
	double cgstAmt;
	double igstAmt;
	
	double netTotalAmt;
	
	double ctcAmount;
	double basePrice;
	double cgstPer;
	double sgstPer;
	double igstPer;
	
	double basePriceNormalOt;
	double basePriceNhOt;
	double basePricePhOt;
	

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getNoOfStaff() {
		return noOfStaff;
	}

	public void setNoOfStaff(String noOfStaff) {
		this.noOfStaff = noOfStaff;
	}

	public String getRatePerBoy() {
		return ratePerBoy;
	}

	public void setRatePerBoy(String ratePerBoy) {
		this.ratePerBoy = ratePerBoy;
	}

	public double getNoOfDaysPresent() {
		return noOfDaysPresent;
	}

	public void setNoOfDaysPresent(double noOfDaysPresent) {
		this.noOfDaysPresent = noOfDaysPresent;
	}

	public double getNormalDayOt() {
		return normalDayOt;
	}

	public void setNormalDayOt(double normalDayOt) {
		this.normalDayOt = normalDayOt;
	}

	public double getPhOt() {
		return phOt;
	}

	public void setPhOt(double phOt) {
		this.phOt = phOt;
	}

	public double getNhOt() {
		return nhOt;
	}

	public void setNhOt(double nhOt) {
		this.nhOt = nhOt;
	}

	public double getOtAmount() {
		return otAmount;
	}

	public void setOtAmount(double otAmount) {
		this.otAmount = otAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getSgstAmt() {
		return sgstAmt;
	}

	public void setSgstAmt(double sgstAmt) {
		this.sgstAmt = sgstAmt;
	}

	public double getCgstAmt() {
		return cgstAmt;
	}

	public void setCgstAmt(double cgstAmt) {
		this.cgstAmt = cgstAmt;
	}

	public double getIgstAmt() {
		return igstAmt;
	}

	public void setIgstAmt(double igstAmt) {
		this.igstAmt = igstAmt;
	}

	public double getNetTotalAmt() {
		return netTotalAmt;
	}

	public void setNetTotalAmt(double netTotalAmt) {
		this.netTotalAmt = netTotalAmt;
	}

	public double getCtcAmount() {
		return ctcAmount;
	}

	public void setCtcAmount(double ctcAmount) {
		this.ctcAmount = ctcAmount;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public double getCgstPer() {
		return cgstPer;
	}

	public void setCgstPer(double cgstPer) {
		this.cgstPer = cgstPer;
	}

	public double getSgstPer() {
		return sgstPer;
	}

	public void setSgstPer(double sgstPer) {
		this.sgstPer = sgstPer;
	}

	public double getIgstPer() {
		return igstPer;
	}

	public void setIgstPer(double igstPer) {
		this.igstPer = igstPer;
	}

	public double getBasePriceNormalOt() {
		return basePriceNormalOt;
	}

	public void setBasePriceNormalOt(double basePriceNormalOt) {
		this.basePriceNormalOt = basePriceNormalOt;
	}

	public double getBasePriceNhOt() {
		return basePriceNhOt;
	}

	public void setBasePriceNhOt(double basePriceNhOt) {
		this.basePriceNhOt = basePriceNhOt;
	}

	public double getBasePricePhOt() {
		return basePricePhOt;
	}

	public void setBasePricePhOt(double basePricePhOt) {
		this.basePricePhOt = basePricePhOt;
	}

	

}
