package com.slicktechnologies.shared.common.salesprocess;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.CustomerNameChangeServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.LoginServiceImpl;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.sms.SMSDetails;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

// TODO: Auto-generated Javadoc
/**
 * Repersents an Invoice ,it Shares one To many relation ship with {@link BillingDocument}
 */
@Entity
public class Invoice extends SalesPaymentProcess implements Serializable {

	//*********************rohan add 3 fields for invoice download(ie. gross value,tax value ,tax amt ) 
		double grossValue;
		double taxPercent;
		double taxAmt;
		double netPayable;
		double discount;
		
		
		@Index
		protected Integer proformaCount;
		
		/*
		 * Added by rahul
		 */
		@Index
		String sapInvoiceId;
		/*
		 * Added by Rahul VermaT
		 * Client : Only for NBHC
		 * totalAmount=netValue+taxAmount
		 */
		
		/**
		 * @author Anil , Date : 01-04-2020
		 * Removed unused index
		 * Throwing error while saving invoice - raised by nbhc
		 */
		protected double netValue;
		protected double taxAmount;
		protected double totalAmount;
		
		
	 /** Date 03-09-2017 added by vijay for discount for total amt before taxes **/
	  protected double discountAmt;
	  
	  /** Date 03-09-2017 added by vijay for after discount final total amt  before taxes**/
	  protected double finalTotalAmt;
	  
	  /** Date 03-09-2017 added by vijay for total amount before discount and taxes **/
	  protected double totalAmtExcludingTax;
	
	  /** Date 03-09-2017 added by vijay for total amount after taxes and before other charges  **/
	  protected double totalAmtIncludingTax;
	
	 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
	  protected boolean consolidatePrice;
//		@Index
//		protected String customerBranch;
	//****************************changes ends here ****************************************************
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4871918764350629478L;
	
	/******************************************Attributes**********************************************/
	
	
	
	/******************************************Constructor***********************************************/
	
	/**
	 * Instantiates a new invoice.
	 */
	 /** date 14.7.2018 added by komal for direct invoice **/
	@Index
	boolean isPO;
	/** date 28.09.2018 added by komal for invoice reference number for sasha**/
	@Index
	protected String invRefNumber;
	/** date 24.10.2018 added by komal for tds **/
	protected double tdsTaxValue;
	
	protected String tdsPercentage;
	
	@Index
	protected boolean tdsApplicable;
	/** date 27.10.2018 added by komal to store client's PO number , WO number and cnc number **/
	@Index
	protected String poNumber;
	@Index
	protected String woNumber;
	@Index
	protected String contractNumber;
	/**
	 * Date 03-4-2019 by Vijay for NBHC CCPM cancellation Date
	 */
	@Index
	protected Date cancellationDate;
	@Index
	protected String invoiceRefNo;

	/*** Date 11-08-2020 by Vijay for CNC project Name for Sasha ***/
	protected String projectName;
	/*** Date 13-08-2020 by Vijay To map Credit Period from Sales order and CNC **/
	protected int creditPeriod;
	/*** Date 13-08-2020 by Vijay To map Dispatch Date to Calculate Payment Date **/
	protected Date dispatchDate;
	
	protected double totalManPower;
	protected double totalManDays;
    protected String InvPrefix;
	protected Date referenceDate;
	protected String cancellationTime;
	protected String comment1;
	
	protected Vector<DocumentUpload> docUpload1;
	protected Vector<DocumentUpload> docUpload2;

	/**
	 * @author Vijay Date 13-03-2021
	 * added to manage sales register credit note invoices 
	 * for logic purpose 
	 */
	protected boolean creditnoteflag;
	/**
	 * @author Anil @since 25-03-2021
	 * Capturing annexure details for CNC bills i.e. attendace summary
	 */
	@EmbedMap
	@Serialize
	protected HashMap<String,ArrayList<AnnexureType>> annexureMap;
	
	/**
	 * @author 27-06-2021 by Vijay
	 * Des :- payment Gateway unique ID	
	 */
	@Index
	protected String paymentGatewayUniqueId;
	
	/**
	 * @author Anil
	 * @since 19-01-2022
	 * adding declaration field which will be printed on invoice. raised by Nithila and Nitin Sir for Innovative
	 */
	String declaration;
	
	double totalQty;
	
	/**@author Sheetal : 28-05-2022 
	 * Des : Adding Payment Terms, Transaction Line type, Tax classification code, 
	 * Tax exemption code,vendor name fields for envocare oracle format**/
	 
	String paymentTerms, transactionLineType, taxClassificationCode, taxExemptionCode, vendorName;
	
	
	protected Date updationDate;

	protected String updatedBy,updationTime,cancelledBy,log;
	
	protected String IRN;
	protected String irnAckNo;
	protected String irnAckDate;
	protected String irnQrCode;	
	protected String irnStatus;	
	protected String irnCancellationDate;
	protected Boolean recordSelect;
	protected String eInvoiceResult;
	protected String bulkEinvoiceTaskID;
	@Index
	protected long zohoInvoiceUniqueID;
	@Index
	protected String zohoInvoiceID;
	protected String zohoSyncDetails;
	protected Date zohoSyncDate;
	@Index
	protected String referenceInvoiceNumber; //Ashwini Patil Date:27-11-2024 added for ultima to store their old 10 digit invoice id
	
	public Invoice() {
		super();
		comment="";
		sapInvoiceId="";
		netValue=0;
		taxAmount=0;
		totalAmount=0;
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		consolidatePrice =false;
		paymentMode = "";
		 /** date 14.7.2018 added by komal for direct invoice **/
		isPO = false;
		invRefNumber  = "";
		/** date 24.10.2018 added by komal for tds **/
		tdsApplicable = false;
		tdsPercentage = 0+"";
		poNumber = "";
		woNumber = "";
		contractNumber = "";
		projectName ="";
		cancellationTime = "";
		comment1 = "";
			
		docUpload1=new Vector<DocumentUpload>();
		docUpload2 = new Vector<DocumentUpload>();
		creditnoteflag = false;
		paymentGatewayUniqueId ="";
		
		paymentTerms="";
		transactionLineType="";
		taxClassificationCode="";
		taxExemptionCode="";
		vendorName="";
		
		updatedBy="";
		updationTime="";
		cancelledBy="";
		log="";
		IRN="";		
		irnAckNo="";	
		irnAckDate="";	
		irnQrCode="";	
		irnStatus="";
		irnCancellationDate="";
		eInvoiceResult="";
		bulkEinvoiceTaskID="";
		
		zohoInvoiceID="";
		zohoSyncDetails="";
		zohoInvoiceUniqueID=0;
	}
	
/********************************************************************************************************/
	
	
	
 /*******************************************Getters And Setters*****************************************/

/********************************************************************************************************/
	
	/**
	 * Gets the status list.
	 *
	 * @return the status list
	 */
	public static List<String> getStatusList()
	{
		ArrayList<String> invoicestatuslist=new ArrayList<String>();
		invoicestatuslist.add(CREATED);
		invoicestatuslist.add(REQUESTED);
		invoicestatuslist.add(REJECTED);
		invoicestatuslist.add(APPROVED);
		invoicestatuslist.add(CANCELLED);
		//  rohan added this on 24/2/2017 for Proforma invoice
		invoicestatuslist.add(PROFORMAINVOICE);
		return invoicestatuslist;
	}
	
	
	
	/*****************************************************************************************************/
	
	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		
		Logger logger=Logger.getLogger("Invoice.class");
		//Ashwini Patil Date:15-01-2025 If client is going to maintain invoices in zoho then no need to create payment in eva
		boolean isInvoiceIntegrationWithZohoBooksActive=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks", this.getCompanyId())) {
			isInvoiceIntegrationWithZohoBooksActive=true;			
		}

		if(getStatus().equals(Invoice.APPROVED))
		{
			if(!isInvoiceIntegrationWithZohoBooksActive) {
			if(this.getInvoiceAmount()>0) //Ashwini Patil Date:12-03-2022 Description: Nitin sir given requirement- Do not create payment document if invoice amount zero.
				this.createPaymentDetails();
			else
				logger.log(Level.SEVERE, "not creating payment as Invoice amount is zero");
			}
			
			/*
			 * Ashwini Patil
			 * Date:12-02-2025
			 * no one is using this flow so commenting this
			 */
			/**
			 * Date : 18-04-2018 BY ANIL
			 * Creating AMC contract with service for HVAC on approval of Sales Invoice
			 * Rohan/Nitin Sir
			 */
//			if(this.getTypeOfOrder().equals(AppConstants.ORDERTYPESALES)){
//				SalesOrder salesOrder=ofy().load().type(SalesOrder.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
//				if(salesOrder!=null){
//					ArrayList<SalesLineItem> amcLineItem=new ArrayList<SalesLineItem>();
//					for(SalesLineItem item:salesOrder.getItems()){
//						if(item.getServiceProductId()!=0&&item.getServiceProductName()!=null&&!item.getServiceProductName().equals("")){
//							if(item.getWarrantyDuration()!=0){
//								amcLineItem.add(item);
//							}
//						}
//					}
//					if(amcLineItem.size()!=0){
//						createAmcContract(salesOrder,amcLineItem);
//					}
//				}
//			}
			
			/**
			 * End
			 */
			
			
			/**
			 * @author Vijay Chougule
			 * @Since Date 18-08-2020 
			 * Des :- when below process config is Active
			 * then it will calculate the Total Man Power and Total Man Days
			 *        
			 */
			try {
				if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddManpowercolumn",this.getCompanyId())) {
					double totalManPower = 0;
					double totalManDays=0;
					double labourManPower = 0;
					double overtimeManPower=0;
					double holidayManPower = 0;
					
					for(SalesOrderProductLineItem product : this.getSalesOrderProducts()){
						System.out.println("product.getBillType()"+product.getBillType());
						if(product.getBillType()!=null && !product.getBillType().equals("") && ( product.getBillType().equals("Labour") || product.getBillType().equals("Overtime") || product.getBillType().equals("Holiday")) ){
							if(product.getManpower()!=null && !product.getManpower().equals("")){
								if(product.getBillType().equals("Labour")){
									labourManPower += Double.parseDouble(product.getManpower());
								}
								else if(product.getBillType().equals("Overtime")){
									overtimeManPower += Double.parseDouble(product.getManpower());
								}
								else if(product.getBillType().equals("Holiday")){
									holidayManPower += Double.parseDouble(product.getManpower());
								}
							}
							if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
								totalManDays +=Double.parseDouble(product.getArea());
								System.out.println("totalManDays "+totalManDays);
							}
						}
						
					}
					totalManPower = getmaxManPower(labourManPower,overtimeManPower,holidayManPower);
					this.setTotalManPower(totalManPower);
					this.setTotalManDays(totalManDays);
				}
				/**
				 * @author Vijay Date :- 28-03-2022
				 * Des :- as per nitin sir need sum of qty 
				 */
				else{
					double totalqty=0;

					for(SalesOrderProductLineItem product : this.getSalesOrderProducts()){
							if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
								try {
									totalqty +=Double.parseDouble(product.getArea());
								} catch (Exception e) {
								}
							}
						
					}
					this.setTotalQty(totalqty);
				}
				/**
				 * ends here
				 */
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			/**
			 * @author Vijay Chougule Date - 29-12-2022 
			 * Des :- To send automate message with pdf 
			 */
			 sendMessage();
			
			
		}
		
		System.out.println("Querying Processname");
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGINV).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag =0;
		
		if(processConfig!=null){
			System.out.println("Process Config Not Null");
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				System.out.println("----------------------------------------------------------");
				System.out.println("One===== "+processConfig.getProcessList().get(i).getProcessType());
				System.out.println("One===== "+processConfig.getProcessList().get(i).isStatus());
				System.out.println("----------------------------------------------------------");
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}
		logger.log(Level.SEVERE, "flag"+flag);
		logger.log(Level.SEVERE, "processConfig"+processConfig);

		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			logger.log(Level.SEVERE, "ExecutingAccInterface");
			ServerAppUtility serverutility = new ServerAppUtility();
			boolean einvoiceFlag = serverutility.checkEInvoiceConfigAtivation(this.getCompanyId(), this.getBranch());
			logger.log(Level.SEVERE, "einvoiceFlag"+einvoiceFlag);
			
			/*
			 * Ashwini Patil
			 * Date:15-09-2023
			 * As per old logic if einvoice is active then accounting interface entry gets created after generating einvoice. 
			 * But for B2C customer einvoice is not appliable and in this case accounting interface entry was not getting created which is a problem.
			 * So we are applying logic that if GSTIN not present in customer then we will create accounting interface entry at the time of invoice approval only.
			 * For purchase invoices no need to check for einvoice flag. We will create accounting interface entry at the time of invoice approval only.
			 */
			
			if(!einvoiceFlag){
				accountingInterface(this,"Invoice 1");
			}else {
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(this.getTypeOfOrder()!=null&&this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)||this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",this.getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
				}			
				if(this.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					accountingInterface(this,"Invoice 2");
				}
				
				if(cust!=null) {
					ArrayList <ArticleType> articleList=cust.getArticleTypeDetails();
					String custGstNo="";
					for(ArticleType ar:articleList)
					{
						if(ar.getArticleTypeName().equals("GSTIN"))
							custGstNo=ar.getArticleTypeValue();
					}
					if(custGstNo==null||custGstNo.equals("")) 
						accountingInterface(this,"Invoice 3");
				}
				
			}
			
		}
		
		ArrayList<SalesOrderProductLineItem> salesOrderProductLineItemList=new ArrayList<SalesOrderProductLineItem>();
		for (int i = 0; i < this.getSalesOrderProducts().size(); i++) {
			if(this.getSalesOrderProducts().get(i).getPaymentPercent()<100){
				salesOrderProductLineItemList.add(this.getSalesOrderProducts().get(i));
			}
		}
		
		/**
		 * Date 18-11-2019
		 * @author Vijay Chougule
		 * Des :- NBHC CCPM Partial Invoice billing document should not create
		 */
		boolean parcialBillFlag =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "DisableParticalBillDocument",this.getCompanyId());
		if(!parcialBillFlag){
			if(salesOrderProductLineItemList.size()!=0){
				createBillingDocument(salesOrderProductLineItemList);
			}
		}
		/**
		 * end
		 */
//		if(salesOrderProductLineItemList.size()!=0){
//			createBillingDocument(salesOrderProductLineItemList);
//		}
	}
	
	
	@GwtIncompatible
	private void sendMessage() {

		 Logger logger = Logger.getLogger("NameOfYourLogger");
		 SMSDetails smsdetails = new SMSDetails();
		 smsdetails.setModel(this);
		 SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", AppConstants.INVOICEAPPROVAL).filter("status",true).first().now();
		 
		 if(smsEntity!=null) {
			 smsdetails.setSmsTemplate(smsEntity);
			 smsdetails.setEntityName("Invoice");
			 
			 String mobileNo=Long.toString(this.getPersonInfo().getCellNumber());
			 smsdetails.setMobileNumber(mobileNo);
			 smsdetails.setCommunicationChannel(AppConstants.SMS);
			 smsdetails.setModuleName(AppConstants.ACCOUNTMODULE);
			 smsdetails.setDocumentName(AppConstants.INVOICEDETAILS);

			 SmsServiceImpl sendImpl = new SmsServiceImpl();
			 String smsmsg = sendImpl.validateAndSendSMS(smsdetails);
			 logger.log(Level.SEVERE," inside validateAndSendSMS"+smsmsg);
		 }
		 
	}

	@GwtIncompatible
	protected void createAmcContract(SalesOrder salesOrder,ArrayList<SalesLineItem> amcLineItem) {
		HashSet<Integer> idHs=new HashSet<Integer>();
		for(SalesLineItem item:amcLineItem){
			idHs.add(item.getServiceProductId());
		}
		ArrayList<Integer> idList=new ArrayList<Integer>(idHs);
		List<ServiceProduct> serProdList=ofy().load().type(ServiceProduct.class).filter("companyId", salesOrder.getCompanyId()).filter("count IN", idList).list();
		
		Contract contract=new Contract();
		
		contract.setCompanyId(salesOrder.getCompanyId());
		contract.setRefNo(salesOrder.getCount()+"");
		contract.setRefDate(salesOrder.getSalesOrderDate());
		contract.setStatus(Contract.APPROVED);
		
		contract.setCinfo(salesOrder.getCinfo());
		contract.setBranch(salesOrder.getBranch());
		contract.setEmployee(salesOrder.getEmployee());
		contract.setApproverName(salesOrder.getApproverName());
		contract.setContractDate(this.getInvoiceDate());
		
		List<SalesLineItem> contractLineItemList=new ArrayList<SalesLineItem>();
		ArrayList<Date>startDateList=new ArrayList<Date>();
		ArrayList<Date>endDateList=new ArrayList<Date>();
		
		if(serProdList.size()!=0){
			int srNo=1;
			for(ServiceProduct product:serProdList){
				for(SalesLineItem item :amcLineItem){
					
					if(item.getServiceProductId()==product.getCount()){
						SalesLineItem conLineItem=new SalesLineItem();
						conLineItem.setPrduct(product);
						conLineItem.setPrice(0);
						conLineItem.setDuration(item.getWarrantyDuration());
						conLineItem.setNumberOfService(item.getNo_Of_Services());
						conLineItem.setQuantity(1d);
						conLineItem.setProductSrNo(srNo);
						
						conLineItem.setStartDate(this.getInvoiceDate());
						
						Date startDate=new Date(conLineItem.getStartDate().getTime());
						conLineItem.setEndDate(DateUtility.addDaysToDate(startDate, conLineItem.getDuration()));
						
						startDateList.add(conLineItem.getStartDate());
						endDateList.add(conLineItem.getEndDate());
						
						
						conLineItem.setTermsAndConditions(new DocumentUpload());
						conLineItem.setProductImage(new DocumentUpload());
						conLineItem.getPrduct().setProductImage1(new DocumentUpload());
						conLineItem.getPrduct().setProductImage2(new DocumentUpload());
						conLineItem.getPrduct().setProductImage3(new DocumentUpload());
						conLineItem.getPrduct().setComment("");
						conLineItem.getPrduct().setCommentdesc("");
						conLineItem.getPrduct().setCommentdesc1("");
						conLineItem.getPrduct().setCommentdesc2("");
						
						/**
						 * Date :12-05-2018 By ANIL
						 * copying premise details at sales order line to contract product line item  
						 */
						conLineItem.setPremisesDetails(item.getPremisesDetails());
						/**
						 * End
						 */
						srNo++;
						contractLineItemList.add(conLineItem);
					}
				}
			}
		}
		
		contract.setItems(contractLineItemList);
		contract.setServiceScheduleList(getServiceSchedulingList(contract.getItems()));
		Collections.sort(startDateList);
		Collections.sort(endDateList);
		contract.setStartDate(startDateList.get(0));
		contract.setEndDate(endDateList.get(endDateList.size()-1));
		
		contract.setTotalAmount(0d);
		contract.setNetpayable(0d);
		contract.setFinalTotalAmt(0d);
		contract.setFlatDiscount(0d);
		contract.setGrandTotal(0d);
		contract.setGrandTotalAmount(0d);
		contract.setInclutaxtotalAmount(0d);
		
		GenricServiceImpl impl =new GenricServiceImpl();
		impl.save(contract);
		
		ContractServiceImplementor conSerImpl=new ContractServiceImplementor();
		conSerImpl.makeServices(contract);
		}
	@GwtIncompatible
	private List<ServiceSchedule> getServiceSchedulingList(List<SalesLineItem> items) {
		List<ServiceSchedule> serschelist = new ArrayList<ServiceSchedule>();

		for (int i = 0; i < items.size(); i++) {
			int qty=(int) items.get(i).getQty();
			
			/**
			 * Date : 24-04-2018 By ANIL
			 * setting service date as contract start date or it should be after first interval 
			 * for HVAC/Rohan
			 */
			ServiceProduct serProd=(ServiceProduct) items.get(i).getPrduct();
			boolean isStartDate=serProd.isStartDate();
			
			
			for (int k = 0; k < qty; k++) {
				long noServices = (long) (items.get(i).getNumberOfServices());
				int noOfdays = items.get(i).getDuration();
				int interval = (int) (noOfdays / items.get(i).getNumberOfServices());
				
				Date servicedate = new Date(items.get(i).getStartDate().getTime());
				Date d = new Date(servicedate.getTime());
				Date productEndDate = new Date(servicedate.getTime());
				
				productEndDate=DateUtility.addDaysToDate(productEndDate, noOfdays);
				Date prodenddate = new Date(productEndDate.getTime());
				productEndDate = prodenddate;
				Date tempdate = new Date();
				for (int j = 0; j < noServices; j++) {
					// if start date flag is true then we have to create first service on contract start date
					//else we have to create first service on calculated interval
					if (isStartDate==false || j > 0) {
						d=DateUtility.addDaysToDate(d, interval);
						tempdate = new Date(d.getTime());
					}
					ServiceSchedule ssch = new ServiceSchedule();
					Date Stardaate = new Date();
					ssch.setSerSrNo(items.get(i).getProductSrNo());
					ssch.setScheduleStartDate(Stardaate);
					ssch.setScheduleProdId(items.get(i).getPrduct().getCount());
					ssch.setScheduleProdName(items.get(i).getProductName());
					ssch.setScheduleDuration(items.get(i).getDuration());
					ssch.setScheduleNoOfServices(items.get(i).getNumberOfServices());
					ssch.setScheduleServiceNo(j + 1);
					ssch.setScheduleProdStartDate(Stardaate);
					ssch.setScheduleProdEndDate(productEndDate);
					ssch.setTotalServicingTime(items.get(i).getServicingTIme());
					ssch.setScheduleServiceTime("Flexible");
					if (j == 0) {
						ssch.setScheduleServiceDate(servicedate);
					}
					/**
					 * Date : 24-04-2018 By ANIL
					 */
					if(isStartDate==false){
						ssch.setScheduleServiceDate(tempdate);
					}
					if (j != 0) {
						if (productEndDate.before(d) == false) {
							ssch.setScheduleServiceDate(tempdate);
						} else {
							ssch.setScheduleServiceDate(productEndDate);
						}
					}
					
					ssch.setScheduleProBranch("Service Address");
					ssch.setServicingBranch(this.getBranch());
					
					
					serschelist.add(ssch);
				}
			}
		}
		return serschelist;
	}
	
	

	@GwtIncompatible
	protected void createBillingDocument(
			ArrayList<SalesOrderProductLineItem> salesOrderProductLineItemList) {

		List<SalesOrderProductLineItem> salesProductsLis = updateProductTable(salesOrderProductLineItemList);
		ArrayList<SalesOrderProductLineItem> salesProductsArr = new ArrayList<SalesOrderProductLineItem>();
		salesProductsArr.addAll(salesProductsLis);
		BillingDocument billdetails = new BillingDocument();
		billdetails.setPersonInfo(this.getPersonInfo());
		billdetails.setContractCount(this.getContractCount());
		billdetails.setTotalSalesAmount(this.getTotalSalesAmount());
		billdetails.setSalesOrderProducts(salesProductsArr);
		billdetails.setBillingDate(new Date());
		billdetails.setArrPayTerms(this.getArrPayTerms());
		billdetails.setContractStartDate(this.getContractStartDate());
		billdetails.setContractEndDate(this.getContractEndDate());
		List<ContractCharges> taxesLis = updateTaxesTable(salesProductsLis);
		ArrayList<ContractCharges> arrcc = new ArrayList<ContractCharges>();
		arrcc.addAll(taxesLis);
		billdetails.setBillingTaxes(arrcc);
		if (this.getCompanyId() != null)
			billdetails.setCompanyId(this.getCompanyId());
//		if (this.getBillingCategory() != null)
//			billdetails.setBillingCategory(model.getBillingCategory());
//		if (model.getBillingType() != null)
//			billdetails.setBillingType(model.getBillingType());
//		if (model.getBillingGroup() != null)
//			billdetails.setBillingGroup(model.getBillingGroup());
		billdetails.setApproverName(this.getApproverName());
		billdetails.setEmployee(this.getEmployee());
		billdetails.setBranch(this.getBranch());
		if (this.getInvoiceDate() != null)
			billdetails.setInvoiceDate(this.getInvoiceDate());
		if (this.getPaymentDate() != null)
			billdetails.setPaymentDate(this.getPaymentDate());

		billdetails.setStatus(BillingDocument.CREATED);
		billdetails.setOrderCformStatus(this.getOrderCformStatus());
		billdetails.setOrderCformPercent(this.getOrderCformPercent());
		billdetails.setOrderCreationDate(this.getOrderCreationDate());
		billdetails.setTypeOfOrder(this.getTypeOfOrder());
		billdetails.setAccountType(this.getAccountType());
		/**
		 * Date : 22-11-2017 BY ANIL
		 */
		System.out.println("OLD NUM RANGE : "+this.getNumberRange());
		if(this.getNumberRange()!=null){
			billdetails.setNumberRange(this.getNumberRange());
		}
		System.out.println("NEW NUM RANGE : "+billdetails.getNumberRange());
		if(this.getSegment()!=null){
			billdetails.setSegment(this.getSegment());
		}
		if(this.getRefNumber()!=null){
			billdetails.setRefNumber(this.getRefNumber());
		}
		double totBillAmt=0;
		double taxAmt=0;
		for(SalesOrderProductLineItem item:salesProductsLis){
			totBillAmt=totBillAmt+item.getBasePaymentAmount();
		}
		for(ContractCharges chrg:billdetails.getBillingTaxes()){
			taxAmt=taxAmt+chrg.getPayableAmt();
		}
		billdetails.setTotalAmount(totBillAmt);
		billdetails.setFinalTotalAmt(totBillAmt);
		double totalamtincludingtax=totBillAmt+taxAmt;
		System.out.println("totalamtincludingtax "+totalamtincludingtax);
		System.out.println("totBillAmt "+totBillAmt);
		System.out.println("taxAmt"+taxAmt);
		billdetails.setTotalAmtIncludingTax(totalamtincludingtax);
//		billdetails.setGrandTotalAmount(totalamtincludingtax);
		billdetails.setTotalBillingAmount(totalamtincludingtax);
		
		billdetails.setComment("This is created from Partial Invoice. Reference Invoice No : "+this.getCount());
		billdetails.setBillingPeroidFromDate(this.getBillingPeroidFromDate());
		billdetails.setBillingPeroidToDate(this.getBillingPeroidToDate());
		/**
		 * End
		 */
		/** Date 11-08-2020 by Vijay for Project Name mapping ***/
		if(this.getProjectName()!=null){
			billdetails.setProjectName(this.getProjectName());
		}
		if(this.getSubBillType()!=null){
			billdetails.setSubBillType(this.getSubBillType());
		}
		billdetails.setCreditPeriod(this.getCreditPeriod());
		/** Date 14-08-2020 by Vijay  CNC contract number mapping in biling document ***/
		if(this.getContractNumber()!=null){
			billdetails.setCncContractNumber(this.getContractNumber());
		}
		
		/**
		 * @author Vijay 28-03-2022 for do not print service address flag updating to invoice
		 */
		billdetails.setDonotprintServiceAddress(this.isDonotprintServiceAddress());
		
		/**Sheetal:01-04-2022,storing payment mode in invoice**/
		if(this.getPaymentMode()!=null && !this.getPaymentMode().equals("")) {
		  billdetails.setPaymentMode(this.getPaymentMode());
		}
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(billdetails);
		
	}

	@GwtIncompatible
	protected List<ContractCharges> updateTaxesTable(
			List<SalesOrderProductLineItem> salesProductsLis) {
		
		ArrayList<ContractCharges> arrConCharges=new ArrayList<ContractCharges>();
		double totalAmouunt=0;
		double totalAmount1=0;   
		
		for(SalesOrderProductLineItem item:salesProductsLis){
			if(item.getBasePaymentAmount() == 0 || item.getBasePaymentAmount() == 0.0){
				totalAmouunt=totalAmouunt+item.getBaseBillingAmount();
			}
			else{
				totalAmouunt=totalAmouunt+item.getBasePaymentAmount();
			}
		}
		
		for(ContractCharges tax:this.getBillingTaxes()){
			if(tax.getTaxChargeName().trim().equalsIgnoreCase("VAT") ||tax.getTaxChargeName().trim().equalsIgnoreCase("CST")){
				ContractCharges ccent=new ContractCharges();
				ccent.setTaxChargeName(tax.getTaxChargeName());
				ccent.setTaxChargePercent(tax.getTaxChargePercent());
				ccent.setTaxChargeAssesVal(totalAmouunt);
				
				double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
				ccent.setPayableAmt(totalCalcAmt);
				arrConCharges.add(ccent);
				
				totalAmount1=totalAmount1+totalAmouunt+totalCalcAmt;
			}
			/**
			 * Date : 22-11-2017 BY ANIL
			 * updating tax table according to gst
			 */
			else if(tax.getTaxChargeName().trim().equalsIgnoreCase("CGST") ||tax.getTaxChargeName().trim().equalsIgnoreCase("SGST")||tax.getTaxChargeName().trim().equalsIgnoreCase("IGST")){
				ContractCharges ccent=new ContractCharges();
				ccent.setTaxChargeName(tax.getTaxChargeName());
				ccent.setTaxChargePercent(tax.getTaxChargePercent());
				ccent.setTaxChargeAssesVal(totalAmouunt);
				
				double totalCalcAmt=(ccent.getTaxChargeAssesVal()*ccent.getTaxChargePercent())/100;
				ccent.setPayableAmt(totalCalcAmt);
				arrConCharges.add(ccent);
			}
			
		}
	
	
	for(ContractCharges tax:this.getBillingTaxes()){
		if(tax.getTaxChargeName().trim().equalsIgnoreCase("Service Tax")){
			ContractCharges ccent=new ContractCharges();
			ccent.setTaxChargeName(tax.getTaxChargeName());
			ccent.setTaxChargePercent(tax.getTaxChargePercent());
			ccent.setTaxChargeAssesVal(totalAmount1);
			double totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
			ccent.setPayableAmt(totalCalcAmt);
			arrConCharges.add(ccent);
		}
	}
	
	return arrConCharges;
		
		
	
	}

	@GwtIncompatible
	protected List<SalesOrderProductLineItem> updateProductTable(
			ArrayList<SalesOrderProductLineItem> salesOrderProductLineItemList) {
		ArrayList<SalesOrderProductLineItem> arrSalesProducts=new ArrayList<SalesOrderProductLineItem>();
		double calPayableAmt=0,calBaseBillAmt;
		for(int i=0;i<salesOrderProductLineItemList.size();i++)
		{
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
			salesOrder.setProdCategory(salesOrderProductLineItemList.get(i).getProdCategory());
			salesOrder.setProdCode(salesOrderProductLineItemList.get(i).getProdCode());
			salesOrder.setProdName(salesOrderProductLineItemList.get(i).getProdName());
			salesOrder.setQuantity(salesOrderProductLineItemList.get(i).getQuantity());
			salesOrder.setPrice(salesOrderProductLineItemList.get(i).getPrice());
			
			salesOrder.setVatTaxEdit(salesOrderProductLineItemList.get(i).getVatTaxEdit());
			salesOrder.setServiceTaxEdit(salesOrderProductLineItemList.get(i).getServiceTaxEdit());
			salesOrder.setVatTax(salesOrderProductLineItemList.get(i).getVatTax());
			salesOrder.setServiceTax(salesOrderProductLineItemList.get(i).getServiceTax());
			
			
			salesOrder.setTotalAmount(salesOrderProductLineItemList.get(i).getTotalAmount());
			salesOrder.setProdPercDiscount(salesOrderProductLineItemList.get(i).getProdPercDiscount());
		
			
			
			//  rohan added this code
			salesOrder.setPrduct(salesOrderProductLineItemList.get(i).getPrduct());
			
			calPayableAmt=((salesOrderProductLineItemList.get(i).getBaseBillingAmount()-salesOrderProductLineItemList.get(i).getFlatDiscount())*salesOrderProductLineItemList.get(i).getPaymentPercent())/100;
			calBaseBillAmt=salesOrderProductLineItemList.get(i).getBaseBillingAmount()-calPayableAmt;
			salesOrder.setBaseBillingAmount(calBaseBillAmt);
			if(salesOrderProductLineItemList.get(i).getPaymentPercent()==100){
				salesOrder.setPaymentPercent(0.0);
			}
			else{
				salesOrder.setPaymentPercent(100.0);
			}
			salesOrder.setBasePaymentAmount(calBaseBillAmt);
			salesOrder.setIndexVal(salesOrderProductLineItemList.get(i).getIndexVal());
			salesOrder.setIndexVal(salesOrderProductLineItemList.get(i).getIndexVal());
			/** Date 09-02-2018 By vijay for partial billing document setting base billing amt **/
			salesOrder.setBasePaymentAmount(calBaseBillAmt);
			/** Date 15-02-2018 By vijay partial biling setting HSN Code **/
			if(this.getSalesOrderProducts().get(i).getHsnCode()!=null && !this.getSalesOrderProducts().get(i).getHsnCode().equals(""))
				salesOrder.setHsnCode(this.getSalesOrderProducts().get(i).getHsnCode());
			
			/**
			 * @author Anil,Date : 04-03-2018
			 * if invoice created from bill which was created after submission of partial invoice ,
			 * below details are not mapped.
			 * for pepcopp raised by rahul tiwari
			 */
			
			salesOrder.setOrderDuration(this.getSalesOrderProducts().get(i).getOrderDuration());
		
			salesOrder.setProductSrNumber(this.getSalesOrderProducts().get(i).getProductSrNumber());
			salesOrder.setProdId(this.getSalesOrderProducts().get(i).getPrduct().getCount());
			

			
			/** date 30.3.2019 added by komal to add order services in partial bill **/
			salesOrder.setOrderServices(this.getSalesOrderProducts().get(i).getOrderServices());
			
			/*** Date 13-08-2020 by Vijay for sasha man power for partila billing ***/
			if(this.getSalesOrderProducts().get(i).getManpower()!=null && !this.getSalesOrderProducts().get(i).getManpower().equals("")){
				salesOrder.setManpower(this.getSalesOrderProducts().get(i).getManpower());
			}
			/*** Date 14-08-2020 by Vijay Bug :- for partil bills area is not mapped so updated the code ****/
			if(this.getSalesOrderProducts().get(i).getArea()!=null && !this.getSalesOrderProducts().get(i).getArea().equals("")){
				salesOrder.setArea(this.getSalesOrderProducts().get(i).getArea());
			}
			
			if(this.getSalesOrderProducts().get(i).getBillType()!=null && !this.getSalesOrderProducts().get(i).getBillType().equals("") ){
				salesOrder.setBillType(this.getSalesOrderProducts().get(i).getBillType());
			}
			/*** Date 16-06-2020 added by Vijay when CNC partial bill raise **/
			if(this.getSalesOrderProducts().get(i).getCtcAmount()!=0){
				salesOrder.setCtcAmount(this.getSalesOrderProducts().get(i).getCtcAmount());
			}
			if(this.getSalesOrderProducts().get(i).getManagementFees()!=0){
				salesOrder.setManagementFees(this.getSalesOrderProducts().get(i).getManagementFees());
			}
			
			arrSalesProducts.add(salesOrder);
		}
		return arrSalesProducts;
	}

	public void setSalesOrderProductFromBilling(ArrayList<SalesOrderProductLineItem> salesOrderProducts){
		if(salesOrderProducts!=null){
			this.salesOrderProducts = salesOrderProducts;
		}
	}
	
	
	/**
	 * Creates the payment details.
	 */
	@GwtIncompatible
    protected void createPaymentDetails()
    {
		
		Logger logger = Logger.getLogger("Name of logger");	
		logger.log(Level.SEVERE, "in createPaymentDetails");
		GenricServiceImpl impl=new GenricServiceImpl();
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(this.getPersonInfo());
		paydetails.setContractCount(this.getContractCount());
		if(this.getContractStartDate()!=null){
			paydetails.setContractStartDate(this.getContractStartDate());
		}
		if(this.getContractEndDate()!=null){
			paydetails.setContractEndDate(this.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(this.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(this.getArrayBillingDocument());
		
		/**
		 * old Code commented by aNil on 23-10-2017
		 */
//		paydetails.setTotalBillingAmount(this.getTotalBillingAmount());
		/**
		 * New code by ANIL
		 */
		paydetails.setTotalBillingAmount(this.getInvoiceAmount());
		/**
		 * End
		 */
		paydetails.setInvoiceAmount(this.getInvoiceAmount());
		
		
		paydetails.setInvoiceCount(this.getCount());
		paydetails.setInvoiceDate(this.getInvoiceDate());
		paydetails.setInvoiceType(this.getInvoiceType());
		paydetails.setPaymentDate(this.getPaymentDate());
		double invoiceamt=this.getInvoiceAmount();
		int payAmt=(int)(invoiceamt);
		paydetails.setPaymentAmt(payAmt);
		paydetails.setPaymentAmtInDecimal(invoiceamt);//Ashwini Patil
		paydetails.setPaymentMethod(this.getPaymentMethod());
		paydetails.setTypeOfOrder(this.getTypeOfOrder());
		paydetails.setStatus(CustomerPayment.CREATED);
		paydetails.setOrderCreationDate(this.getOrderCreationDate());
		paydetails.setBranch(this.getBranch());
		paydetails.setAccountType(this.getAccountType());
		paydetails.setEmployee(this.getEmployee());
		paydetails.setCompanyId(this.getCompanyId());
		paydetails.setOrderCformStatus(this.getOrderCformStatus());
		paydetails.setOrderCformPercent(this.getOrderCformPercent());
		
		//  rohan added this code for sessing c
		
		if(this.getPaymentMethod()!= null && !this.getPaymentMethod().equals(""))
		{
			if(this.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
				paydetails.setChequeIssuedBy(this.getPersonInfo().getFullName());
			}
			else
			{
				paydetails.setChequeIssuedBy("");
			}
		}
		else
		{
			paydetails.setChequeIssuedBy("");
		}
		/***************** vijay*********************/
		if(this.getNumberRange()!=null)
		paydetails.setNumberRange(this.getNumberRange());
		/*******************************************/
		
		/**
		 * date 14 Feb 2017
		 * added by vijay for Setting billing period from date and To date in Payment
		 */
		if(this.getBillingPeroidFromDate()!=null)
			paydetails.setBillingPeroidFromDate(this.getBillingPeroidFromDate());
		if(this.getBillingPeroidToDate()!=null)
			paydetails.setBillingPeroidToDate(this.getBillingPeroidToDate());
		/**
		 * end here
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(this.getSegment()!=null){
			paydetails.setSegment(this.getSegment());
		}
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(this.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(this.getRateContractServiceId());
		}
		/*
		 *  end
		 */
		
		/*
		 *  nidhi
		 *   1-07-2017
		 */
		if(this.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(this.getRateContractServiceId());
		}
		/*
		 *  end
		 */
		
		/**
		 *  nidhi
		 *  20-07-2017
		 *   quantity and mesurement transfer to the invoice entity 
		 */
		if( getQuantity() > 0){
			paydetails.setQuantity(getQuantity());
		}
		
		if( getUom() !=null){
			paydetails.setUom(getUom());
		}
		/*
		 * end   
		 */
		
		/**
		 *  nidhi
		 *  24-07-2017
		 *  
		 */
		if( getRefNumber() !=null){
			paydetails.setRefNumber(getRefNumber());
		}
		
		//Ashwini Patil Date:23-09-2022
		if( getInvRefNumber() !=null){
			paydetails.setInvRefNumber(getInvRefNumber());
		}
		/**
		 *  end
		 */
		/** date 21.02.2018 added by komal for balance tax and revenue **/
		double taxAmount = 0;
		taxAmount = Math.round(this.getTotalAmtIncludingTax() - (this.getFinalTotalAmt() + this.totalOtherCharges));
		paydetails.setBalanceTax(taxAmount);
		paydetails.setBalanceRevenue(this.getNetPayable() - taxAmount);
		/**
		 * end komal
		 */
		/**
		 * nidhi
		 * 28-04-2018
		 */
		paydetails.setRenewContractFlag(this.isRenewContractFlag());
		/** date 20.10.2018 added by komal**/
		if(this.getSubBillType() != null && !this.getSubBillType().equals("")){
			paydetails.setSubBillType(this.getSubBillType());
		}
		/** date 24.10.2018 added by komal for tds from purchaseInvoice**/
		paydetails.setTdsTaxValue(this.getTdsTaxValue());
		
		if(this.getTdsPercentage()!=null){
			paydetails.setTdsPercentage(this.getTdsPercentage()+"");
		}
		paydetails.setTdsApplicable(this.isTdsApplicable());
		
		/**Sheetal:01-04-2022,storing payment mode in payment document**/
		if(this.getPaymentMode()!=null && !this.getPaymentMode().equals("")) {
		   paydetails.setPaymentMode(this.getPaymentMode());
		}
		/** end komal **/
		
		if(this.getInvoiceConfigType()!=null){
			paydetails.setInvoiceConfigType(this.getInvoiceConfigType());
		}
		
		impl.save(paydetails);
		logger.log(Level.SEVERE, "Payment created for Invoice "+this.getCount());
    }
	
	
	@GwtIncompatible
	public void accountingInterface(Invoice invoice,String callFrom){
		Logger logger = Logger.getLogger("Name of logger");
		if(invoice.getCount()!=0&&invoice.getStatus().equals(Invoice.APPROVED) || invoice.getCount()!=0&&invoice.getStatus().equals(Invoice.CANCELLED)){
			 String refDoc2Type="";
			 Date refDoc2Date=null;
			/**
			 * @author Anil , Date : 04-07-2019
			 */
			GRN grn=null;
			int grnId=0;
			if(invoice instanceof VendorInvoice){
				refDoc2Type="GRN";
				
	    		 try{
	    			grnId=Integer.parseInt(this.getRefNumber().trim()); 
	    		 }catch(Exception e){
	    			 
	    		 }
	    		 System.out.println("INSIDE VENDOR INVOICE APPROVAL :: "+grnId);
	    		 if(grnId!=0){
	    			 grn=ofy().load().type(GRN.class).filter("companyId", this.getCompanyId()).filter("count", grnId).first().now();
	    			 if(grn!=null && grn.getCreationDate()!=null){
	    				 refDoc2Date = grn.getCreationDate();
	    			 }
	    		 }
			}
			
			for(int  i = 0;i<this.getSalesOrderProducts().size();i++){
				
				Customer cust=null;
				Vendor vendor=null;
				Employee emp=null;
				
				if(invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					cust=ofy().load().type(Customer.class).filter("companyId",invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
				}
				if(invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					cust=ofy().load().type(Customer.class).filter("companyId",invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
				}				
				if(invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					vendor=ofy().load().type(Vendor.class).filter("companyId",invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
				}
				/** date 05.1.2019 added by komal for serial number **/
				int prodSrNumber = invoice.getSalesOrderProducts().get(i).getProductSrNumber();
				
				Date docCreationDate=null;
				 int prodId=invoice.getSalesOrderProducts().get(i).getProdId();
				 String productCode =invoice.getSalesOrderProducts().get(i).getProdCode();
				 String productName = invoice.getSalesOrderProducts().get(i).getProdName();
				 double productQuantity = invoice.getSalesOrderProducts().get(i).getQuantity();
 				 double productprice = (invoice.getSalesOrderProducts().get(i).getPrice());
				 String unitofmeasurement = invoice.getSalesOrderProducts().get(i).getUnitOfMeasurement();
				 //  rohan commented this code 
//				 double totalAmount = this.getSalesOrderProducts().get(i).getPrice()*this.getSalesOrderProducts().get(i).getQuantity();
				
				 //   rohan added new code for calculations 
//				 double totalAmount = invoice.getSalesOrderProducts().get(i).getBaseBillingAmount(); //Ashwini Patil Date:9-10-2024 commented this as it should map payable amount instaed of total amount from invice screen - product table
				 
				 double totalAmount = invoice.getSalesOrderProducts().get(i).getBasePaymentAmount();
				 
				 
				 double calculatedamt = 0;
				 double calculetedservice =0;
				 	
				 int orderDuration = invoice.getSalesOrderProducts().get(i).getOrderDuration();
				 int orderServices = invoice.getSalesOrderProducts().get(i).getOrderServices();
				 
				 double cformamtService = 0;
				 double serviceTax=0 ;
				 double cformPercent=0;
				 double cformAmount=0;					
				 double vatPercent=0;
				 String cform = "";
				 String refDocType="";
				 double cformP=0;
				 String accountType="";
				 Date contractStartDate=null;
				 Date contractEndDate=null;
				 
				 int vatglaccno=0;
				 int serglaccno=0;
				 int cformglaccno=0;
				 
				 int personInfoId = 0;
				 String personInfoName="";
				 long personInfoCell = 0;
				 int vendorInfoId = 0;
				 String vendorInfoName="";
				 long vendorInfoCell=0;
				 int empInfoId;
				 String empInfoName="";
				 long empinfoCell;
				 
				 String addrLine1="";
				 String addrLocality="";
				 String addrLandmark="";
				 String addrCountry="";
				 String addrState="";
				 String addrCity="";
				 long addrPin=0;
				 
				 /** date 29/3/2018 added by komal for tax name **/
				 String taxvatName="";
				 String serTaxName = "";
				 String glAccountName1 = "";
				 String glAccountName2 = ""; 
				 /** date 29.5.2018 added by komal for billing address **/
				 String saddrLine1 ="";
				 String addrLine2 ="";
				 String saddrLine2 ="";
				 String saddrLocality = "";
				 String saddrLandmark = "";
				 String saddrCountry = "";
				 String saddrState="";
				 String saddrCity="";
				 long saddrPin = 0;
	/************************************for document type********************************************/			 
				 
				 
				 
				 
				if(invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESALES)){
					docCreationDate=invoice.getOrderCreationDate();
					refDocType=AppConstants.REFDOCSO;
					personInfoId=invoice.getPersonInfo().getCount();
//					personInfoName=this.getPersonInfo().getFullName();   //commented by Ashwini
					/*
					 * @Author Ashwini
					 * Date:17-11-2018
					 * Des:to take correspondance name if present
					 */
					/** Date 05-12-2018 By Vijay
					 * Des :- updated with != blank in if condition
					 */
					/**
					 * Date 16-10-2019 by Vijay
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableCustomerNameAsInCorespondanceNameInAccountingInterface", this.getCompanyId())){
						if(cust.getCustPrintableName()!=null &&!cust.getCustPrintableName().equals("")){
							personInfoName=cust.getCustPrintableName();
						}else{
							personInfoName=invoice.getPersonInfo().getFullName();
						}
					}
					else{
						personInfoName=invoice.getPersonInfo().getFullName();
					}
					
					
					/*
					 * end by Ashwini
					 */

					personInfoCell=invoice.getPersonInfo().getCellNumber();
					
					 addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					 addrLocality=cust.getAdress().getLocality();
					 addrLandmark=cust.getAdress().getLandmark();
					 addrCountry=cust.getAdress().getCountry();
					 addrState=cust.getAdress().getState();
					 addrCity=cust.getAdress().getCity();
					 addrPin=cust.getAdress().getPin();
					 
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
					
					if(invoice.getOrderCformStatus()!=null){
						cform=invoice.getOrderCformStatus();
						cformP=invoice.getOrderCformPercent();
					}
				}
				
				if(invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPESERVICE)){
					docCreationDate=invoice.getOrderCreationDate();
					refDocType=AppConstants.REFDOCCONTRACT;
					personInfoId=invoice.getPersonInfo().getCount();
//					personInfoName=this.getPersonInfo().getFullName();  //commented by Ashwini
					/*
					 * @Author Ashwini
					 * Date:17-11-2018
					 * Des:to take correspondance name if present
					 */
					/** Date 05-12-2018 By Vijay
					 * Des :- updated with != blank in if condition
					 */
//					if(cust.getCustPrintableName()!=null &&!cust.getCustPrintableName().equals("")){
//						personInfoName=cust.getCustPrintableName();
//					}else{
//						personInfoName=invoice.getPersonInfo().getFullName();
//					}
					/**
					 * Date 16-10-2019 by Vijay
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableCustomerNameAsInCorespondanceNameInAccountingInterface", this.getCompanyId())){
						if(cust.getCustPrintableName()!=null &&!cust.getCustPrintableName().equals("")){
							personInfoName=cust.getCustPrintableName();
						}else{
							personInfoName=invoice.getPersonInfo().getFullName();
						}
					}
					else{
						personInfoName=invoice.getPersonInfo().getFullName();
					}
					
					/*
					 * end by Ashwini
					 */

					personInfoCell=invoice.getPersonInfo().getCellNumber();
				
					addrLine1=cust.getAdress().getAddrLine1();
					 /** date 29.5.2018 added by komal for billing address **/
					 addrLine2=cust.getAdress().getAddrLine2();
					addrLocality=cust.getAdress().getLocality();
					addrLandmark=cust.getAdress().getLandmark();
					addrCountry=cust.getAdress().getCountry();
					addrState=cust.getAdress().getState();
					addrCity=cust.getAdress().getCity();
					addrPin=cust.getAdress().getPin();
					contractStartDate=invoice.getContractStartDate();
					contractEndDate=invoice.getContractEndDate();
					
					 addrPin=cust.getAdress().getPin();
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=cust.getSecondaryAdress().getAddrLine1();
					saddrLine2=cust.getSecondaryAdress().getAddrLine2();
					saddrLocality=cust.getSecondaryAdress().getLocality();
					saddrLandmark=cust.getSecondaryAdress().getLandmark();
					saddrCountry=cust.getSecondaryAdress().getCountry();
					saddrState=cust.getSecondaryAdress().getState();
					saddrCity=cust.getSecondaryAdress().getCity();
					saddrPin=cust.getSecondaryAdress().getPin();
				}
				
				if(invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					docCreationDate=this.getOrderCreationDate();
					refDocType=AppConstants.REFDOCPO;
					
					vendorInfoId=invoice.getPersonInfo().getCount();
					vendorInfoName=invoice.getPersonInfo().getFullName();
					vendorInfoCell=invoice.getCellNumber();
					
					addrLine1=vendor.getPrimaryAddress().getAddrLine1();
					/** date 29.5.2018 added by komal for billing address **/
					addrLine2=vendor.getPrimaryAddress().getAddrLine2();
					addrLocality=vendor.getPrimaryAddress().getLocality();
					addrLandmark=vendor.getPrimaryAddress().getLandmark();
					addrCountry=vendor.getPrimaryAddress().getCountry();
					addrState=vendor.getPrimaryAddress().getState();
					addrCity=vendor.getPrimaryAddress().getCity();
					addrPin=vendor.getPrimaryAddress().getPin();
					
					 /** date 29.5.2018 added by komal for billing address **/
					saddrLine1=vendor.getSecondaryAddress().getAddrLine1();
					saddrLine2=vendor.getSecondaryAddress().getAddrLine2();
					saddrLocality=vendor.getSecondaryAddress().getLocality();
					saddrLandmark=vendor.getSecondaryAddress().getLandmark();
					saddrCountry=vendor.getSecondaryAddress().getCountry();
					saddrState=vendor.getSecondaryAddress().getState();
					saddrCity=vendor.getSecondaryAddress().getCity();
					saddrPin=vendor.getSecondaryAddress().getPin();
					
					if(invoice.getOrderCformStatus()!=null){
						cform=invoice.getOrderCformStatus();
						cformP=invoice.getOrderCformPercent();
					}
					
					/**
					 * @author Anil , Date : 30-05-2019
					 * if docCreation date is null then will show there invoice date
					 * if product quantity is 0 then will print value from area
					 */
					if(docCreationDate==null){
						docCreationDate=invoice.getInvoiceDate();
					}
					if(productQuantity==0){
						try{
							productQuantity=Double.parseDouble(invoice.getSalesOrderProducts().get(i).getArea().trim());
						}catch(Exception e){
							
						}
					}
					
				}
				
				
				
			/*********************************for cform*************************************************/	

				double discountAmount = invoice.getDiscountAmt();
				 if(invoice.getOrderCformStatus()!= null&&invoice.getOrderCformStatus().trim().equals(AppConstants.YES)){
					 cform=AppConstants.YES;
					 cformPercent=invoice.getOrderCformPercent();
					 cformAmount=invoice.getOrderCformPercent()*totalAmount/100;
					 cformamtService=invoice.getOrderCformPercent()*totalAmount/100;
					 if(invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount-discountAmount)*invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100; //Ashwini Patil Date:12-02-2024 subtracting discount
						 logger.log(Level.SEVERE,"calculetedservice 1");
					 }
					 
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getOrderCformPercent()).filter("isCentralTax",true).first().now();
//					 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//					 cformglaccno=serglAcc.getGlAccountNo();
					 cformglaccno=0;
				 }
				 
				 else if(invoice.getOrderCformStatus()!= null&&invoice.getOrderCformStatus().trim().equals(AppConstants.NO)){
					 cform=AppConstants.NO;
					 cformPercent=invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage();
					 cformAmount=invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 cformamtService=invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage()*totalAmount/100;
					 
					 if(invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0){
						 serviceTax=invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
						 calculetedservice=((cformamtService+totalAmount-discountAmount)*invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage())/100;//Ashwini Patil Date:12-02-2024 subtracting discount
						 logger.log(Level.SEVERE,"calculetedservice 2");
					 }
					 
					 
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",AppConstants.FILTERFORCST).filter("isCentralTax",true).first().now();
//					 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 vatglaccno=vatglAcc.getGlAccountNo();
					 vatglaccno=0;
				 }
				 
				 
				 
				 	else{//if the customer is of same state
					 
						 if(invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage()!=0)
						 {
							 vatPercent=invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage();
							 calculatedamt=invoice.getSalesOrderProducts().get(i).getVatTax().getPercentage()*(totalAmount-discountAmount)/100; //Ashwini Patil Date:12-02-2024 subtracting discountAmount
							 //date 29.3.2018 added by komal
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",invoice.getCompanyId()).filter("taxChargeName",invoice.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName()).first().now();
							 if(vattaxDtls != null)	
							 glAccountName2 = vattaxDtls.getGlAccountName();
//							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//							 vatglaccno=vatglAcc.getGlAccountNo();
							 vatglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							// String taxvatName="";
									if(invoice.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName()!=null && !invoice.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName().equals("")){
										taxvatName =invoice.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName();
									}
									else{
										taxvatName =invoice.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName();
									}
						 }
						 
						 
						
						 				 
						 if(invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()!=0)
						 {
							 serviceTax=invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage();
							 calculetedservice=(totalAmount - discountAmount)*invoice.getSalesOrderProducts().get(i).getServiceTax().getPercentage()/100; //Ashwini Patil Date:12-02-2024
							 logger.log(Level.SEVERE,"calculetedservice 3");
							 //date 29.3.2018 added by komal 
							 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",invoice.getCompanyId()).filter("taxChargeName",invoice.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName()).first().now();
							 if(sertaxDtls != null)
							 glAccountName1 = sertaxDtls.getGlAccountName();
//							 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
//							 serglaccno=serglAcc.getGlAccountNo();
							 serglaccno=0;
							 
							 /**
							  * Rohan bhagde added this code on date : 28-06-2017
							  */
							 /** date 03/04/2018 changed by komal  vat tax to service tax**/
							 //String serTaxName="";
								if(invoice.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName()!=null && !invoice.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName().equals("")){
									serTaxName =invoice.getSalesOrderProducts().get(i).getServiceTax().getTaxPrintName();
								}
								else{
									serTaxName =invoice.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName();
								}
						 }
				 	}
				 
				 
				 
				

				 /*****************************************************************************************/
				
				 //  old code 
//				 double netPayable = getTotalSalesAmount();
				 //  new code by rohan as there is calculations error
				 double netPayable = invoice.getNetPayable();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
//				 if(this.getBillingOtherCharges().size()!=0){
//					 for(int k = 0; k < invoice.getBillingOtherCharges().size();k++){
//						 oc[k] = invoice.getBillingOtherCharges().get(k).getTaxChargeName();
//						 oca[k] = invoice.getBillingOtherCharges().get(k).getPayableAmt();
//
//					 }
//				 }
					 for(int k = 0; k < invoice.getOtherCharges().size();k++){
						 oc[k] = invoice.getOtherCharges().get(k).getOtherChargeName();
						 oca[k] = invoice.getOtherCharges().get(k).getAmount();

					 }

					 
				/**
				 * Date : 01-11-2017 BY ANIL
				 * Setting invoice comment to accounting interface remark	 
				 */
				String invoiceComment="";
				if(invoice.getComment()!=null){
					invoiceComment=invoice.getComment();
				}
				
				logger.log(Level.SEVERE,"Invoice Group"+invoice.getInvoiceGroup());
				/**
				 * End
				 */
				/** date 29/03/2018 added by  komal for new tally interface requirement**/
				String gstn = "";
				double indirectExpenses = invoice.getTotalOtherCharges();
				if(invoice.getGstinNumber()!=null && !invoice.getGstinNumber().equals("")){
					gstn = invoice.getGstinNumber();
				}else{
					try{
						Customer customer=ofy().load().type(Customer.class).filter("companyId", invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
						if(customer!=null){
							if(customer.getArticleTypeDetails()!=null){  						
								for(ArticleType object:customer.getArticleTypeDetails()){
									if(customer.getArticleTypeDetails().get(i).getDocumentName().equals("ServiceInvoice") ||
											customer.getArticleTypeDetails().get(i).getDocumentName().equals("Invoice Details")){
										if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
											invoice.setGstinNumber(object.getArticleTypeValue());
											break;
										}
									}
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					gstn = invoice.getGstinNumber();
				}
		         String hsnNumber = "";
		      if(invoice.getSalesOrderProducts().get(i).getHsnCode() != null && !invoice.getSalesOrderProducts().get(i).getHsnCode().equals("")){
		    	  hsnNumber = invoice.getSalesOrderProducts().get(i).getHsnCode();
		      }else{
		       SuperProduct superProduct = ofy().load().type(SuperProduct.class).filter("companyId",invoice.getCompanyId()).filter("productCode", invoice.getSalesOrderProducts().get(i).getProdCode()).first().now();
		       /**
		        * @author Anil @since 01-06-2021
		        * Envocare invoice submit issue raised by Vaishnavi
		        * Product code was changed due this product master was not loading throwing null pointer exception
		        */
		       if(superProduct!=null&&superProduct.getHsnNumber()!=null){
		    	   hsnNumber = superProduct.getHsnNumber();
		       }
		      }		     
//		    	 personInfoName=this.getPersonInfo().getFullName();  //commented by Ashwini
		    	 /*
			        * @Author Ashwini
			        * Date:19-11-2018
			        * Des:To print correspondance name 
			        */
		    	    /** Date 05-12-2018 By Vijay
					 * Des :- updated with != blank in if condition
					 */
//		      	if(cust != null){
//			       if(cust.getCustPrintableName()!=null&&!cust.getCustPrintableName().equals("")){
//						personInfoName=cust.getCustPrintableName();
//					}else{
//						personInfoName=invoice.getPersonInfo().getFullName();
//					}
//		      	}else{
//		      		personInfoName=invoice.getPersonInfo().getFullName();
//		      	}
		      
		      if(!invoice.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
		    	  /**
					 * Date 16-10-2019 by Vijay
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableCustomerNameAsInCorespondanceNameInAccountingInterface", this.getCompanyId())){
						if(cust != null){
							if(cust.getCustPrintableName()!=null &&!cust.getCustPrintableName().equals("")){
								personInfoName=cust.getCustPrintableName();
							}else{
								personInfoName=invoice.getPersonInfo().getFullName();
							}
						}
						else{
							personInfoName=invoice.getPersonInfo().getFullName();
						}
					}
					else{
						personInfoName=invoice.getPersonInfo().getFullName();
					}
					
		      }
		     
			       /*
			        * end by Ashwini
			        */

		    	 /**
		    	  * end komal
		    	  */
					 
				 /**********************************End************************************************/
		    	 
			    	/***date 28.7.2018 added by komal for new date format **/
			    	 Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
			    	 
			    	 /**
			    	  * @author Anil , Date : 30-05-2019
			    	  */
			    	 String productCategory=invoice.getSalesOrderProducts().get(i).getProdCategory();
				 
			    	 /**
			    	  * @author Anil , Date : 30-05-2019
			    	  * updating invoice number and refnumber
			    	  */
			    	 String invoiceNum="";
			    	 String invoiceRefNum="";
			    	 String godownName="";
			    	 if(invoice instanceof VendorInvoice){
			    		 if(invoice.getSegment()!=null){
			    			 invoiceNum=invoice.getSegment();
			    		 }
			    		 if(invoice.getInvRefNumber()!=null){
			    			 invoiceRefNum=invoice.getInvRefNumber();
			    		 }
			    		 if(grnId!=0){
			    			 if(grn!=null){
			    				 System.out.println("GRN NOT NULL");
			    				 for(GRNDetails obj:grn.getInventoryProductItem()){
			    					 System.out.println("PROD ID : "+obj.getProductID()+" == "+invoice.getSalesOrderProducts().get(i).getProdId());
			    					 if(obj.getProductID()==invoice.getSalesOrderProducts().get(i).getProdId()){
			    						 godownName=obj.getWarehouseLocation();
			    						 break;
			    					 }
			    				 }
			    			 }
			    		 }
			    		 
			    		 System.out.println("INV NUM : "+invoiceNum+" INV REF NUM : "+invRefNumber+" GODOWN : "+godownName);
			    	 }
			    	 
			    	 String einvoiceNo = "",einvoiceAckNo="",einvoiceAckDate="",einvoiceQrCode="",einvoicestatus="",
			    			 einvoiceCancellationDate="";
			    	 if(invoice.getIRN()!=null){
			    		 einvoiceNo =  invoice.getIRN();
			    	 }
			    	 if(invoice.getIrnAckNo()!=null){
			    		 einvoiceAckNo =  invoice.getIrnAckNo();
			    	 }
			    	 if(invoice.getIrnAckDate()!=null){
			    		 einvoiceAckDate =  invoice.getIrnAckDate();
			    	 }
			    	 if(invoice.getIrnQrCode()!=null){
			    		 einvoiceQrCode =  invoice.getIrnQrCode();
			    	 }
			    	 if(invoice.getIrnQrCode()!=null){
			    		 einvoicestatus =  invoice.getIrnStatus();
			    	 }
			    	 if(invoice.getIrnCancellationDate()!=null){
			    		 einvoiceCancellationDate =  invoice.getIrnCancellationDate();
			    	 }
			    	 
			    	 String numberRange="";
			    	 if(invoice.getNumberRange()!=null){
			    		 numberRange = invoice.getNumberRange();
			    	 }
			    	 
				 UpdateAccountingInterface.updateTally(currentDate,//accountingInterfaceCreationDate
														 this.getEmployee(),//accountingInterfaceCreatedBy
														 this.getStatus(),
														AppConstants.STATUS ,//Status
														invoiceComment,//Remark
														"Accounts",//Module
														"Invoice",//documentType
															this.getCount(),//documentID
															" ", //documentTitle
															this.getInvoiceDate(),//documentDate
															this.getAccountType().trim()+"-Invoice",//documentGl
															this.getContractCount()+"",//referenceDocumentNumber1
															docCreationDate,//referenceDocumentDate1
															refDocType,//referenceDocumentType
															this.getRefNumber(),//referenceDoc.no.2 Date 09-11-2017 this is sap customer ref num for NBHC
															refDoc2Date,//reference doc.date.2
															refDoc2Type,//reference doc type no2.
															this.getAccountType(),//accountType	
															personInfoId,//custID	
															personInfoName,//custName	
															personInfoCell,//custCell
															vendorInfoId,//venID	
															vendorInfoName,//venName	
															vendorInfoCell,//venCell
															0,//emplID
															" ",//emplName
															this.getBranch(),//branch
															getEmployee(),//personResponsible
															"",//requestdBy
															this.getApproverName(),//AproverName
															this.getPaymentMethod(),//PaymentMethod
															this.getPaymentDate(),//PaymentDate
															"",//Cheq no.
															null,//cheq date.
															null,//Bank Name
															null,//bankAccount
															0,//transferReferenceNumber
															null,//transactionDate
															contractStartDate,  //contract start date
															contractEndDate,  // contract end date
															prodId,//prodID
															productCode,//prodCode
															productName, //prodNAme
															productQuantity,//prodQuant
															this.getContractStartDate(),//Prod DAte
															orderDuration,//duration	
															orderServices,//services
															unitofmeasurement,//UOM
															productprice, //prod Price
															vatPercent,//vat%
															calculatedamt,//vatamt
															vatglaccno,//VATglAccount
															serviceTax,//serviceTaxPercent
															calculetedservice,//serviceTaxAmount
															serglaccno,//serviceTaxGLaccount
															cform,//cform
															cformPercent,//cform%
															cformAmount,//cformamt
															cformglaccno, //cformglacc
															totalAmount,//totalamt
															netPayable,//netpay
															this.getInvoiceGroup(),//Contract Category//27 fEB PReviously it was contract category now it is from invoice group
															0,			//amountRecieved
															0.0,     // Base Amount taken in payment for tds calculation by rohan 
															0,   //  tds percentage by rohan 
															0,   //  tds amount by rohan 
															oc[0],
															oca[0],
															oc[1],
															oca[1],
															oc[2],
															oca[2],
															oc[3],
															oca[3],
															oc[4],
															oca[4],
															oc[5],
															oca[5],
															oc[6],
															oca[6],
															oc[7],
															oca[7],
															oc[8],
															oca[8],
															oc[9],
															oca[9],
															oc[10],
															oca[10],
															oc[11],
															oca[11],
															addrLine1,
															addrLine2,
															addrLocality,
															addrLandmark,
															addrCountry,
															addrState,
															addrCity,
															addrPin,
															this.getCompanyId(),
															this.getBillingPeroidFromDate(),//  billing from date (rohan)
															this.getBillingPeroidToDate(),//  billing to date (rohan)
															"", //Warehouse
															"",				//warehouseCode
															prodSrNumber+"",				//ProductRefId/** date 05.1.2019 added bby komal to save prod serial no **/
															"",				//Direction
															"",				//sourceSystem
															""	,			//Destination Syste
															hsnNumber, // hsc/Sac
															gstn, // gstn
															glAccountName1, // goupname1 for tax1
															glAccountName2, // groupname2 for tax 2
															serTaxName, // tax1 name
															taxvatName, // tax2 name
															discountAmount, // discount amount
															indirectExpenses, // indirect expenses
															saddrLine1,
															saddrLine2, 
															saddrLocality, 
															saddrLandmark,
															saddrCountry,
															saddrState,
															saddrCity, 
															saddrPin,
															productCategory,
															invoiceNum,
															invoiceRefNum,
															godownName,
															einvoicestatus,
															einvoiceNo,
															einvoiceAckNo,
															einvoiceAckDate,
															einvoiceCancellationDate,
															einvoiceQrCode,
															numberRange
															);
				 
			
			}
			
		}
		logger.log(Level.SEVERE,"end of accountingInterface");
	}
	
	
	/**
	 * Date:19-07-2017 By ANIL
	 * This method checks customer GSTIN number at the time of invoice creation and stores it.
	 * Ultra Pest Control
	 */
	@OnSave
	@GwtIncompatible
	private void getGstinNumberFromCustomer(){
		System.out.println("Inside getGstinNumberFromCustomer ");
		if(id==null){                                                // ajinkya uncommented this code as per Anil sir suggestions
			if(this.accountType.equals("AR")){
				try{
					Customer customer=ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
					if(customer!=null){
						if(customer.getArticleTypeDetails()!=null){  
							for(ArticleType object:customer.getArticleTypeDetails()){
								if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
									this.setGstinNumber(object.getArticleTypeValue());
									break;
								}
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}else if(this.accountType.equals("AP")){
				/**
				 * @author Anil
				 * @since 19-10-2020
				 * Setting up vendors GSTIN number invoice in case of payable invoice
				 */
				try{
					Vendor vedor=ofy().load().type(Vendor.class).filter("companyId", this.getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
					if(vedor!=null){
						if(vedor.getArticleTypeDetails()!=null){  
							for(ArticleType object:vedor.getArticleTypeDetails()){
								if(object.getArticleTypeName().trim().equalsIgnoreCase("GSTIN")){
									this.setGstinNumber(object.getArticleTypeValue());
									break;
								}
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			} 
			
//		/** date 20.10.2018 added by komal for subbilltype save **/
//			if(this.getRefNumber() != null &&  !this.getRefNumber().equals("")){
//				int refNo = 0;
//			try{
//				refNo = Integer.parseInt(this.getRefNumber());
//			}catch(Exception e){
//				refNo = 0;
//			}
//			if(refNo != 0){//50408
//			CNC cnc= ofy().load().type(CNC.class)
//					.filter("count", refNo)
//					.filter("companyId", this.getCompanyId()).first().now();
//			if(cnc != null){
//				this.setSubBillType(AppConstants.CONSUMABLES);				
//			  }
//			}
//		  }
		}
		
		
//		/**
//		 * Date 30-01-2018 By vijay
//		 * for ultra proforma invoice issue tracing logger
//		 */
//		if(this.getInvoiceType().equals("Proforma Invoice")){
//			
//			if(this.getStatus().equalsIgnoreCase("Cancelled")){
//				Logger logger = Logger.getLogger("Name of logger");
//				logger.log(Level.SEVERE,"Hi vijay this is for Proforma invoice getting cancelled");
//			
//					ArrayList<String> toEmailList=new ArrayList<String>();
//					ArrayList<String> ccEmailList=new ArrayList<String>();
//					String fromEmail=null;
//					
//					
//					System.out.println("To Email List Size : "+toEmailList.size());
//						Company comp=ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
//						if(comp!=null){
//							if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
//								fromEmail=comp.getEmail();
//							}
//						}
//						
//						toEmailList.add("vijay.chougule@evasoftwaresolutions.com");
//						ccEmailList.add("vijaychougule55@gmail.com");
//					
//						
//						String mailSub="Invoice Id -"+this.getCount()+" Branch -"+this.getBranch();
//						String mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
//										+"Invoice Id "+this.getCount()+" Branch "+this.getBranch()+" is getting cancelled."+"\n"+"\n"+"\n";
//					
//						String mailmsg2 ="";
//						if(UserConfiguration.getCompanyId()!=null){
//							mailmsg2 = "Login User : "+UserConfiguration.getUserconfig().getUser().getUserName()+"\n";
//						}
//						mailMsg.concat(mailmsg2);
//						
//						Email obj=new Email();
//						obj.sendEmail(mailSub, mailMsg, toEmailList, ccEmailList, fromEmail);
//						
//						logger.log(Level.SEVERE,"Email Sent sucessfully");
//			}
//			
//		}
		/**
		 * ends here
		 */
		
	}
	
	
	/**
	 * Date 12-09-2017 added by vijay for Ultra Pinvoice tax invoice button not showing issue
	 * this is patch as per nitin sir disccusion added code
	 * The issue is some time some performa invoice tax invoice button not showing so added code
	 */
	
	@com.googlecode.objectify.annotation.OnLoad
	@GwtIncompatible
	private void getInvoiceStatus(){
		System.out.println("Hi vijay");
		if(this.getInvoiceType().equals("Proforma Invoice")){
			if(!this.getStatus().equals("Cancelled") && (!this.getStatus().equals("PInvoice") && !this.getStatus().equals("Invoiced"))){
				if(this.getProformaCount()!=null && this.getProformaCount()!=0){
					this.setStatus("Invoiced");
				}else{
					this.setStatus("PInvoice");
				}
			}
		}
		
	}
	
	@OnSave
	@GwtIncompatible
	public void getInvoicePrefix(){
		Logger logger = Logger.getLogger("logger");
		logger.log(Level.SEVERE, "in getInvoicePrefix");

		/**Date 10-9-2020
		 * @author Amol to store the Invoice Prefix in Reference order Number Field
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PC_PRINTINVOICENUMBERPREFIX", this.getCompanyId())){
			logger.log(Level.SEVERE, "in getInvoicePrefix step1 ");
			if(id==null){
				logger.log(Level.SEVERE, "in getInvoicePrefix step2 ");
				
		CustomerNameChangeServiceImpl obj=new CustomerNameChangeServiceImpl();
		String prefix=obj.getInvoicePrefixDetails(this.getCompanyId(), this.getBranch());
		
		String str = this.getCount()+"";
		String invoiceNumber = "";
		
		int count=0;
		for (int j = 4; j < str.length(); j++) {
		
			  if(str.charAt(j)=='0'){
			    
			  }else{
			    for(int k = j; k < str.length(); k++){
			      invoiceNumber+=str.charAt(k);
			      count=k;
			    }
			    j=count;
			  }
			}
		String prefixValue = prefix + invoiceNumber;
		this.setInvRefNumber(prefixValue);
		}
		}
	
	
	
	}
	
	//*************************getters and setters *****************************
	
	
	
	public double getGrossValue() {
		return grossValue;
	}

	public void setGrossValue(double grossValue) {
		this.grossValue = grossValue;
	}

	public double getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(double taxPercent) {
		this.taxPercent = taxPercent;
	}

	public double getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public Integer getProformaCount() {
		return proformaCount;
	}

	public void setProformaCount(Integer proformaCount) {
		this.proformaCount = proformaCount;
	}

	public double getNetPayable() {
		return netPayable;
	}

	public void setNetPayable(double netPayable) {
		this.netPayable = netPayable;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getSapInvoiceId() {
		return sapInvoiceId;
	}

	public void setSapInvoiceId(String sapInvoiceId) {
		this.sapInvoiceId = sapInvoiceId;
	}

	public double getSAPNetValue() {
		return netValue;
	}

	public void setSAPNetValue(double netValue) {
		this.netValue = netValue;
	}

	public double getSAPTaxAmount() {
		return taxAmount;
	}

	public void setSAPTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getSAPTotalAmount() {
		return totalAmount;
	}

	public void setSAPTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

//	public String getCustomerBranch() {
//		return customerBranch;
//	}
//
//	public void setCustomerBranch(String customerBranch) {
//		this.customerBranch = customerBranch;
//	}


	/** Date 03-09-2017 added by vijay for getter and setter  **/

	public double getDiscountAmt() {
		return discountAmt;
	}


	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}


	public double getFinalTotalAmt() {
		return finalTotalAmt;
	}


	public void setFinalTotalAmt(double finalTotalAmt) {
		this.finalTotalAmt = finalTotalAmt;
	}

	public double getTotalAmtExcludingTax() {
		return totalAmtExcludingTax;
	}

	public void setTotalAmtExcludingTax(double totalAmtExcludingTax) {
		this.totalAmtExcludingTax = totalAmtExcludingTax;
	}
	
	public double getTotalAmtIncludingTax() {
		return totalAmtIncludingTax;
	}

	public void setTotalAmtIncludingTax(double totalAmtIncludingTax) {
		this.totalAmtIncludingTax = totalAmtIncludingTax;
	}
	
	public boolean isConsolidatePrice() {
		return consolidatePrice;
	}

	public void setConsolidatePrice(boolean consolidatePrice) {
		this.consolidatePrice = consolidatePrice;
	}

	public boolean isPO() {
		return isPO;
	}

	public void setPO(boolean isPO) {
		this.isPO = isPO;
	}

	public String getInvRefNumber() {
		return invRefNumber;
	}

	public void setInvRefNumber(String invRefNumber) {
		this.invRefNumber = invRefNumber;
	}

	public double getTdsTaxValue() {
		return tdsTaxValue;
	}

	public void setTdsTaxValue(double tdsTaxValue) {
		this.tdsTaxValue = tdsTaxValue;
	}

	public String getTdsPercentage() {
		return tdsPercentage;
	}

	public void setTdsPercentage(String tdsPercentage) {
		this.tdsPercentage = tdsPercentage;
	}

	public boolean isTdsApplicable() {
		return tdsApplicable;
	}

	public void setTdsApplicable(boolean tdsApplicable) {
		this.tdsApplicable = tdsApplicable;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getWoNumber() {
		return woNumber;
	}

	public void setWoNumber(String woNumber) {
		this.woNumber = woNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		if(referenceDate!=null){
			this.referenceDate = referenceDate;
		}
	}
	
	/***Date 7-2-2019
	 * @author Amol
	 * added getter setter for invoice ref number
	 * @return
	 */
public String getInvoiceRefNo() {
		return invoiceRefNo;
	}

	public void setInvoiceRefNo(String invoiceRefNo) {
		this.invoiceRefNo = invoiceRefNo;
	}

/** date 15.10.2018 added by komal for sasha invoices **/
	public static boolean checkBillSubType(String billType){
		
		switch(billType){
			case AppConstants.NATIONALHOLIDAY:
			case AppConstants.OVERTIME:
			case AppConstants.STAFFINGANDRENTAL:
			case AppConstants.OTHERSERVICES:
			case AppConstants.CONSUMABLES:
			case AppConstants.FIXEDCONSUMABLES :
			case AppConstants.EQUIPMENTRENTAL:
			case AppConstants.STAFFING:
			case AppConstants.CONSOLIDATEBILL:	
				return true;
		}
	  return false;		
		}

	public static ArrayList<String> getSubBillTypeList() 
	    {
		ArrayList<String> subbilltype=new ArrayList<String>();
		subbilltype.add(AppConstants.NATIONALHOLIDAY);//created from cnc screen(Raise bill) when user select duration and click ok automatically
		subbilltype.add(AppConstants.OVERTIME);//created from cnc screen(Raise bill) when user select duration and click ok automatically
		subbilltype.add(AppConstants.STAFFINGANDRENTAL);//created from cnc screen(Raise bill) when user select duration and click ok automatically
		subbilltype.add(AppConstants.OTHERSERVICES);//created from cnc screen(Raise bill) when user select duration and click ok automatically
		subbilltype.add(AppConstants.CONSUMABLES);// created from sales order when we put cnc id in reference number and in cnc consumables are added as "Actual"
		subbilltype.add(AppConstants.FIXEDCONSUMABLES);// created from sales order when we put cnc id in reference number and in cnc consumables are added as "Fixed"
		subbilltype.add(AppConstants.EQUIPMENTRENTAL);//created from cnc screen(Raise bill) when user select duration , equipment and staffing different = True and click ok automatically
		subbilltype.add(AppConstants.STAFFING);//created from cnc screen(Raise bill) when user select duration , equipment and staffing different = True and click ok automatically
		subbilltype.add(AppConstants.CONSOLIDATEBILL);//created from billing list screen when user select all bills of same cnc and click on process billing document
		subbilltype.add(AppConstants.ARREARSBILL);//created from cnc screen(Arrears bill button) when user select duration , equipment and staffing different = True and click ok automatically
		
		return subbilltype;
	}
	
	public Date getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	
	/**
	 * @author Vijay Chougule Date 22-06-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getCancellationDate()!=null){
			this.setCancellationDate(dateUtility.setTimeMidOftheDayToDate(this.getCancellationDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
		if(this.getBillingPeroidFromDate()!=null){
			this.setBillingPeroidFromDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidFromDate()));
		}
		if(this.getBillingPeroidToDate()!=null){
			this.setBillingPeroidToDate(dateUtility.setTimeMidOftheDayToDate(this.getBillingPeroidToDate()));
		}
		if(this.getInvoiceDate()!=null){
			this.setInvoiceDate(dateUtility.setTimeMidOftheDayToDate(this.getInvoiceDate()));
		}
		if(this.getOrderCreationDate()!=null){
			this.setOrderCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getOrderCreationDate()));
		}
		if(this.getPaymentDate()!=null){
			this.setPaymentDate(dateUtility.setTimeMidOftheDayToDate(this.getPaymentDate()));
		}
		if(this.id==null){
			if(this.getContractStartDate()!=null){
				this.setContractStartDate(dateUtility.setTimeMidOftheDayToDate(this.getContractStartDate()));
			}
			if(this.getContractEndDate()!=null){
				this.setContractEndDate(dateUtility.setTimeMidOftheDayToDate(this.getContractEndDate()));
			}
		}
	}
	/**
	 * ends here	
	 */
	
	/**
	 * @author Vijay Chougule
	 * @Since Date 18-08-2020 
	 * Des :- when below process config is Active then it will calculate the Total Man Power and Total Man Days
	 */
	@OnSave
	@GwtIncompatible
	public void saveTotalNoOfManPowerAndManDays()
	{
		if(id!=null && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddManpowercolumn", this.getCompanyId())){
			double totalManPower = 0;
			double totalManDays=0;
			double labourManPower = 0;
			double overtimeManPower=0;
			double holidayManPower = 0;
			
			for(SalesOrderProductLineItem product : this.getSalesOrderProducts()){
				System.out.println("product.getBillType()"+product.getBillType());
				if(product.getBillType()!=null && !product.getBillType().equals("") && ( product.getBillType().equals("Labour") || product.getBillType().equals("Overtime") || product.getBillType().equals("Holiday")) ){
					if(product.getManpower()!=null && !product.getManpower().equals("")){
						if(product.getBillType().equals("Labour")){
							labourManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Overtime")){
							overtimeManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Holiday")){
							holidayManPower += Double.parseDouble(product.getManpower());
						}
					}
					if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
						totalManDays +=Double.parseDouble(product.getArea());
					}
				}
				
			}
			totalManPower = getmaxManPower(labourManPower,overtimeManPower,holidayManPower);
			this.setTotalManPower(totalManPower);
			this.setTotalManDays(totalManDays);

		}
		/**
		 * @author Vijay Date :- 29-11-2020
		 * Des :- as per rahuls requirement when we cancel the invoice storing cancellation date and cancelaaion Time
		 * 
		 */
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		if(this.getStatus().equals(Invoice.CANCELLED)) {
			this.setCancellationDate(new Date());
			this.setCancellationTime(timeFormat.format(new Date()));
		}
	}
	/**
	 * ends here
	 */
	
	@GwtIncompatible
	private double getmaxManPower(double labourManPower,double overtimeManPower, double holidayManPower) {
		double maxManPower = 0;
		if(labourManPower>=overtimeManPower && labourManPower>=holidayManPower){
				maxManPower = labourManPower;
		}
		else if(overtimeManPower>=holidayManPower && overtimeManPower>=labourManPower){
				maxManPower = overtimeManPower;
		}
		else{
			maxManPower = holidayManPower;
		}
		
		return maxManPower;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(int creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public Date getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	public double getTotalManPower() {
		return totalManPower;
	}

	public void setTotalManPower(double totalManPower) {
		this.totalManPower = totalManPower;
	}

	public double getTotalManDays() {
		return totalManDays;
	}

	public void setTotalManDays(double totalManDays) {
		this.totalManDays = totalManDays;
	}
	
		public String getInvPrefix() {
		return InvPrefix;
	}

	public void setInvPrefix(String invPrefix) {
		InvPrefix = invPrefix;
	}

	public String getCancellationTime() {
		return cancellationTime;
	}

	public void setCancellationTime(String cancellationTime) {
		this.cancellationTime = cancellationTime;
	}
	
		public String getComment1() {
		return comment1;
	}

	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}

	public DocumentUpload getDocUpload1() {
		if(docUpload1.size()!=0)
			  return docUpload1.get(0);
			return null;
	}

	public void setDocUpload1(DocumentUpload docupload1) {
		this.docUpload1=new Vector<DocumentUpload>();
		if(docupload1!=null)
		   this.docUpload1.add(docupload1);
	}

	public DocumentUpload getDocUpload2() {
		if(docUpload2.size()!=0)
			  return docUpload2.get(0);
			return null;
	}

	public void setDocUpload2(DocumentUpload docupload2) {
		this.docUpload2=new Vector<DocumentUpload>();
		if(docupload2!=null)
		   this.docUpload2.add(docupload2);
	}

	public boolean isCreditnoteflag() {
		return creditnoteflag;
	}

	public void setCreditnoteflag(boolean creditnoteflag) {
		this.creditnoteflag = creditnoteflag;
	}
	
	
	public HashMap<String, ArrayList<AnnexureType>> getAnnexureMap() {
		return annexureMap;
	}




	public void setAnnexureMap(HashMap<String, ArrayList<AnnexureType>> annexureMap) {
		this.annexureMap = annexureMap;
	}

	public String getPaymentGatewayUniqueId() {
		return paymentGatewayUniqueId;
	}

	public void setPaymentGatewayUniqueId(String paymentGatewayUniqueId) {
		this.paymentGatewayUniqueId = paymentGatewayUniqueId;
	}

	public String getDeclaration() {
		return declaration;
	}

	public void setDeclaration(String declaration) {
		this.declaration = declaration;
	}

	public double getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(double totalQty) {
		this.totalQty = totalQty;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getTransactionLineType() {
		return transactionLineType;
	}

	public void setTransactionLineType(String transactionLineType) {
		this.transactionLineType = transactionLineType;
	}

	public String getTaxClassificationCode() {
		return taxClassificationCode;
	}

	public void setTaxClassificationCode(String taxClassificationCode) {
		this.taxClassificationCode = taxClassificationCode;
	}

	public String getTaxExemptionCode() {
		return taxExemptionCode;
	}

	public void setTaxExemptionCode(String taxExemptionCode) {
		this.taxExemptionCode = taxExemptionCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log =log;
	}

	public String getCancelledBy() {
		return cancelledBy;
	}

	public void setCancelledBy(String cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public String getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(String updationTime) {
		this.updationTime = updationTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Date getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}
	
	public String getIRN() {
		return IRN;
	}

	public void setIRN(String iRN) {
		IRN = iRN;
	}
	public String getIrnAckNo() {
		return irnAckNo;
	}

	public void setIrnAckNo(String irnAckNo) {
		this.irnAckNo = irnAckNo;
	}
	public String getIrnAckDate() {
		return irnAckDate;
	}

	public void setIrnAckDate(String irnAckDate) {
		this.irnAckDate = irnAckDate;
	}
	public String getIrnQrCode() {
		return irnQrCode;
	}

	public void setIrnQrCode(String irnQrCode) {
		this.irnQrCode = irnQrCode;
	}
	
	public String getIrnStatus() {
		return irnStatus;
	}

	public void setIrnStatus(String irnStatus) {
		this.irnStatus = irnStatus;
	}

	
	public String getIrnCancellationDate() {
		return irnCancellationDate;
	}

	public void setIrnCancellationDate(String irnCancellationDate) {
		this.irnCancellationDate = irnCancellationDate;
	}

	public Boolean getRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(Boolean recordSelect) {
		this.recordSelect = recordSelect;
	}

	public String geteInvoiceResult() {
		return eInvoiceResult;
	}

	public void seteInvoiceResult(String eInvoiceResult) {
		this.eInvoiceResult = eInvoiceResult;
	}

	public String getBulkEinvoiceTaskID() {
		return bulkEinvoiceTaskID;
	}

	public void setBulkEinvoiceTaskID(String bulkEinvoiceTaskID) {
		this.bulkEinvoiceTaskID = bulkEinvoiceTaskID;
	}

	public String getZohoSyncDetails() {
		return zohoSyncDetails;
	}

	public void setZohoSyncDetails(String zohoSyncDetails) {
		this.zohoSyncDetails = zohoSyncDetails;
	}

	public long getZohoInvoiceUniqueID() {
		return zohoInvoiceUniqueID;
	}

	public void setZohoInvoiceUniqueID(long zohoInvoiceUniqueID) {
		this.zohoInvoiceUniqueID = zohoInvoiceUniqueID;
	}

	public String getZohoInvoiceID() {
		return zohoInvoiceID;
	}

	public void setZohoInvoiceID(String zohoInvoiceID) {
		this.zohoInvoiceID = zohoInvoiceID;
	}
	
	public Date getZohoSyncDate() {
		return zohoSyncDate;
	}

	public void setZohoSyncDate(Date zohoSyncDate) {
		this.zohoSyncDate = zohoSyncDate;
	}

	public String getReferenceInvoiceNumber() {
		return referenceInvoiceNumber;
	}

	public void setReferenceInvoiceNumber(String referenceInvoiceNumber) {
		this.referenceInvoiceNumber = referenceInvoiceNumber;
	}
	
	
}
