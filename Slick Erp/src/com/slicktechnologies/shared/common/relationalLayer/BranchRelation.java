package com.slicktechnologies.shared.common.relationalLayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class BranchRelation extends SuperModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9116481960758354617L;
	@Index public String branchName;

	
	
	public BranchRelation() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getName() {
		return branchName;
	}

	public void setName(String name) {
		if(name!=null)
		this.branchName = name.trim();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return branchName;
	}
	
	

}
