package com.slicktechnologies.shared.common.relationalLayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class CustomerRelation extends SuperModel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7735257762601951361L;
	
	@Index protected int customerCount ;
	@Index protected Long cellNumber;
	@Index protected String fullName;
	
	
	
	
	
	public CustomerRelation() {
		super();
		// TODO Auto-generated constructor stub
	}





	public CustomerRelation(int count, Long cellNumber, String fullName) {
		super();
		this.customerCount = count;
		this.cellNumber = cellNumber;
		this.fullName = fullName;
	}





	public Long getId() {
		return id;
	}





	public void setId(Long id) {
		this.id = id;
	}





	public int getCustomerCount() {
		return customerCount;
	}





	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}





	public Long getCellNumber() {
		return cellNumber;
	}





	public void setCellNumber(Long cellNumber) {
		this.cellNumber = cellNumber;
	}





	public String getFullName() {
		return fullName;
	}





	public void setFullName(String fullName) {
		this.fullName = fullName;
	}





	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
	
	

}
