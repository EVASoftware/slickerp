package com.slicktechnologies.shared.common.relationalLayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


@Entity
public class EmployeeRelation extends SuperModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -57700338466461039L;
	@Index protected String employeeName;
	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String branchName) {
		this.employeeName = branchName;
	}
	
	
	

}
