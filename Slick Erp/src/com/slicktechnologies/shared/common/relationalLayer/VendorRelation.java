package com.slicktechnologies.shared.common.relationalLayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class VendorRelation extends SuperModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6317788647630545019L;
	@Index
	protected String vendorName;
	
	
	
	public VendorRelation() {
		super();
		
	}

	@Override
	public int compareTo(SuperModel o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	

}
