package com.slicktechnologies.shared.common;

public class DropDown {
	
	protected String name;
	protected int type;
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	

}
