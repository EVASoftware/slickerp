package com.slicktechnologies.shared.common;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class CompanyAssetListArticleInformation extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8520242641233231639L;

	int assetId;
	String vehicleCategory;
	String vehicleType;
	String registrationNo;
	String articleType;
	String articleNumber;
	int validityInDays;
	Date issueDate;
	String issuedAt;
	String issuedBy;
	Date validityUntil;
	String status;
	
	/************************** getters & setters ******************************/
	public CompanyAssetListArticleInformation() {
		super();
	}
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	public int getAssetId() {
		return assetId;
	}


	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}


	public String getVehicleCategory() {
		return vehicleCategory;
	}


	public void setVehicleCategory(String vehicleCategory) {
		this.vehicleCategory = vehicleCategory;
	}


	public String getVehicleType() {
		return vehicleType;
	}


	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}


	public String getRegistrationNo() {
		return registrationNo;
	}


	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}


	public String getArticleType() {
		return articleType;
	}


	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}


	public String getArticleNumber() {
		return articleNumber;
	}


	public void setArticleNumber(String articleNumber) {
		this.articleNumber = articleNumber;
	}


	public int getValidityInDays() {
		return validityInDays;
	}


	public void setValidityInDays(int validityInDays) {
		this.validityInDays = validityInDays;
	}


	public Date getIssueDate() {
		return issueDate;
	}


	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}


	public String getIssuedAt() {
		return issuedAt;
	}


	public void setIssuedAt(String issuedAt) {
		this.issuedAt = issuedAt;
	}


	public String getIssuedBy() {
		return issuedBy;
	}


	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}


	public Date getValidityUntil() {
		return validityUntil;
	}


	public void setValidityUntil(Date validityUntil) {
		this.validityUntil = validityUntil;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

}
