package com.slicktechnologies.shared.common.cnc;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
public class CNCVersion extends CNC {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8252746592456863202L;
	/**
	 * 
	 */
	/**
	 * 
	*/
	@Index
	String version;
	@Index
	String versionStatus;
	@Index
	Date versionCreationDate;
	CNC cnc;
	
	public CNCVersion(){
		versionStatus = "";
		versionCreationDate = new Date();
		cnc = new CNC();
	}

		public String getVersionStatus() {
		return versionStatus;
	}

	public void setVersionStatus(String versionStatus) {
		this.versionStatus = versionStatus;
	}

	public Date getVersionCreationDate() {
		return versionCreationDate;
	}

	public void setVersionCreationDate(Date versionCreationDate) {
		this.versionCreationDate = versionCreationDate;
	}

	public CNC getCnc() {
		return cnc;
	}

	public void setCnc(CNC cnc) {
		this.cnc = cnc;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return version+"";
	}
	
}
