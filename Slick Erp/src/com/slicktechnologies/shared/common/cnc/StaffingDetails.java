package com.slicktechnologies.shared.common.cnc;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class StaffingDetails implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5007292566975509088L;
	/**
	 * 
	 */
	String ctcTemplates;
	String designation;
	int noOfStaff;
	double grossSalary;
	double basic;
	String da;
	String hra;
	String others;
	double netSalary;
	double payableOTRates;
	double chargableOTRates;
	double uniformCost;
	double washingAllowance;
	String bandP;
	String medical;
	String conveyance;
	String cLeave;
	String cBonus;
	String gratuity;
	String takeHome;
	String sdPF;
	String sdESIC;
	String sdPT;
	String sdLWF;
	String spPF;
	String spESIC;
	String spEDLI;
	String spMWF;
	String spBonus;
	String satutoryDeductions;
	String satutoryPayments;
	String total;//A+B
	String paidLeaves;
	String employementOverHeadCost;
	String totalOfPaidAndOverhead;
	String managementFees;
	String totalIncludingManagementFees;
	String grandTotalOfCTC;
	String overHeadCost;
	Double cBonusPercentage=0d;
	boolean isBonusRelever=false;
	Double manageMentfeesPercent;
	double totalPaidIntoEmployee;
	double travellingAllowance;
	double travellingAllowanceOverhead;
	boolean cLeaveApplicable;
	boolean cBonusApplicable;
	boolean gratuityApplicable;
	
	boolean isPaidLeaveRelever=false;
	/**
	 * Rahul Verma added on 08 Aug 2018 
	 */
	String paidLeaveDays;
	/** date 4.10.2018 added by komal for additional allowance**/
	double additionalAllowance;
	/**date 20-2-2019 added by amol for CCA(City Compensatory Allowance (CCA)**/
	double cityCompensatoryAllowance;
	double administrativecost;
	double supervision;
	double trainingCharges;
	double esicCost;
	double leaveSalary;
	/**Date 12-3-2019 by Amol**/
	double plEsic;
	double bonusEsic;
	double cPlEsic;
	double cBonusEsic;
	/**
	 * @author Anil
	 * @since 07-07-2020
	 * adding option to capture ESIC percent
	 */
	
	double esicPercent;
	
	/**
	 * @author Anil
	 * @since 03-09-2020
	 * Adding management fees rate
	 * for sun facility raised by Rahul Tiwari
	 */
	double managementFeesRate;
	
	/**
	 * @author Anil @since 05-04-2021
	 * Storing calendar working hours for an employee
	 * Used for calculating OT bill
	 * Raised by Rahul Tiwari For Alkosh
	 */
	double workingHours;
	
	public StaffingDetails() {
		super();
		// TODO Auto-generated constructor stub
		ctcTemplates="";
		designation="";
		noOfStaff=0;
		grossSalary=0;
		basic=0;
		da=0+"";
		hra=0+"";
		others=0+"";
		netSalary=0;
		payableOTRates=0;
		chargableOTRates=0;
		uniformCost=0;
		washingAllowance=0;
		bandP=0+"";
		medical=0+"";
		conveyance=0+"";
		cLeave=0+"";
		cBonus=0+"";
		takeHome=0+"";
		sdPF=0+"";
		sdESIC=0+"";
		sdPT=0+"";
		sdLWF=0+"";
		spPF=0+"";
		spESIC=0+"";
		spEDLI=0+"";
		spMWF=0+"";
		spBonus=0+"";
		satutoryDeductions=0+"";
		satutoryPayments=0+"";
		total=0+"";//A+B
		paidLeaves=0+"";
		employementOverHeadCost=0+"";
		totalOfPaidAndOverhead=0+"";
		managementFees=0+"";
		totalIncludingManagementFees=0+"";
		grandTotalOfCTC=0+"";
		overHeadCost=0+"";
		cBonusPercentage=0d;
		isBonusRelever=false;
		isPaidLeaveRelever=false;
		manageMentfeesPercent=0d;
		totalPaidIntoEmployee=0d;
		travellingAllowance=0d;
		travellingAllowanceOverhead=0d;
		cBonusApplicable=false;
		cLeaveApplicable=false;
		paidLeaveDays=0+"";
		administrativecost=0d;
		gratuityApplicable=false;
		
	}
	
	
	/*******************************Getters and Setters********************************************/
	
	
	
	
	
	public double getTravellingAllowance() {
		return travellingAllowance;
	}

	public String getPaidLeaveDays() {
		return paidLeaveDays;
	}


	public void setPaidLeaveDays(String paidLeaveDays) {
		this.paidLeaveDays = paidLeaveDays;
	}


	public boolean isBonusRelever() {
		return isBonusRelever;
	}


	public void setBonusRelever(boolean isBonusRelever) {
		this.isBonusRelever = isBonusRelever;
	}


	public boolean isPaidLeaveRelever() {
		return isPaidLeaveRelever;
	}


	public void setPaidLeaveRelever(boolean isPaidLeaveRelever) {
		this.isPaidLeaveRelever = isPaidLeaveRelever;
	}


	public boolean iscLeaveApplicable() {
		return cLeaveApplicable;
	}


	public void setcLeaveApplicable(boolean cLeaveApplicable) {
		this.cLeaveApplicable = cLeaveApplicable;
	}


	public boolean iscBonusApplicable() {
		return cBonusApplicable;
	}


	public void setcBonusApplicable(boolean cBonusApplicable) {
		this.cBonusApplicable = cBonusApplicable;
	}


	public double getTravellingAllowanceOverhead() {
		return travellingAllowanceOverhead;
	}


	public void setTravellingAllowanceOverhead(double travellingAllowanceOverhead) {
		this.travellingAllowanceOverhead = travellingAllowanceOverhead;
	}


	public void setTravellingAllowance(double travellingAllowance) {
		this.travellingAllowance = travellingAllowance;
	}
	
	public String getCtcTemplates() {
		return ctcTemplates;
	}

	public double getTotalPaidIntoEmployee() {
		return totalPaidIntoEmployee;
	}


	public void setTotalPaidIntoEmployee(double totalPaidIntoEmployee) {
		this.totalPaidIntoEmployee = totalPaidIntoEmployee;
	}


	public Double getManageMentfeesPercent() {
		return manageMentfeesPercent;
	}


	public void setManageMentfeesPercent(Double manageMentfeesPercent) {
		this.manageMentfeesPercent = manageMentfeesPercent;
	}


	public Double getcBonusPercentage() {
		return cBonusPercentage;
	}


	public void setcBonusPercentage(Double cBonusPercentage) {
		this.cBonusPercentage = cBonusPercentage;
	}

	public String getOverHeadCost() {
		return overHeadCost;
	}


	public void setOverHeadCost(String overHeadCost) {
		this.overHeadCost = overHeadCost;
	}


	public String getTotal() {
		return total;
	}


	public void setTotal(String total) {
		this.total = total;
	}


	public String getPaidLeaves() {
		return paidLeaves;
	}


	public void setPaidLeaves(String paidLeaves) {
		this.paidLeaves = paidLeaves;
	}


	public String getEmployementOverHeadCost() {
		return employementOverHeadCost;
	}


	public void setEmployementOverHeadCost(String employementOverHeadCost) {
		this.employementOverHeadCost = employementOverHeadCost;
	}


	public String getTotalOfPaidAndOverhead() {
		return totalOfPaidAndOverhead;
	}


	public void setTotalOfPaidAndOverhead(String totalOfPaidAndOverhead) {
		this.totalOfPaidAndOverhead = totalOfPaidAndOverhead;
	}


	public String getManagementFees() {
		return managementFees;
	}


	public void setManagementFees(String managementFees) {
		this.managementFees = managementFees;
	}


	public String getTotalIncludingManagementFees() {
		return totalIncludingManagementFees;
	}


	public void setTotalIncludingManagementFees(String totalIncludingManagementFees) {
		this.totalIncludingManagementFees = totalIncludingManagementFees;
	}


	public String getGrandTotalOfCTC() {
		return grandTotalOfCTC;
	}


	public void setGrandTotalOfCTC(String grandTotalOfCTC) {
		this.grandTotalOfCTC = grandTotalOfCTC;
	}


	public String getSpEDLI() {
		return spEDLI;
	}


	public void setSpEDLI(String spEDLI) {
		this.spEDLI = spEDLI;
	}


	public String getSpMWF() {
		return spMWF;
	}


	public void setSpMWF(String spMWF) {
		this.spMWF = spMWF;
	}


	public String getSpBonus() {
		return spBonus;
	}


	public void setSpBonus(String spBonus) {
		this.spBonus = spBonus;
	}


	public String getSatutoryDeductions() {
		return satutoryDeductions;
	}


	public void setSatutoryDeductions(String satutoryDeductions) {
		this.satutoryDeductions = satutoryDeductions;
	}


	public String getSatutoryPayments() {
		return satutoryPayments;
	}


	public void setSatutoryPayments(String satutoryPayments) {
		this.satutoryPayments = satutoryPayments;
	}


	public String getSpPF() {
		return spPF;
	}


	public void setSpPF(String spPF) {
		this.spPF = spPF;
	}


	public String getSpESIC() {
		return spESIC;
	}


	public void setSpESIC(String spESIC) {
		this.spESIC = spESIC;
	}

	public String getSdPF() {
		return sdPF;
	}


	public void setSdPF(String sdPF) {
		this.sdPF = sdPF;
	}


	public String getSdESIC() {
		return sdESIC;
	}


	public void setSdESIC(String sdESIC) {
		this.sdESIC = sdESIC;
	}


	public String getSdPT() {
		return sdPT;
	}


	public void setSdPT(String sdPT) {
		this.sdPT = sdPT;
	}


	public String getSdLWF() {
		return sdLWF;
	}


	public void setSdLWF(String sdLWF) {
		this.sdLWF = sdLWF;
	}


	public double getUniformCost() {
		return uniformCost;
	}


	public void setUniformCost(double uniformCost) {
		this.uniformCost = uniformCost;
	}


	public void setCtcTemplates(String ctcTemplates) {
		this.ctcTemplates = ctcTemplates;
	}
	
	public String getDesignation() {
		return designation;
	}
	
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public int getNoOfStaff() {
		return noOfStaff;
	}
	public void setNoOfStaff(int noOfStaff) {
		this.noOfStaff = noOfStaff;
	}
	public double getGrossSalary() {
		return grossSalary;
	}
	public void setGrossSalary(double grossSalary) {
		this.grossSalary = grossSalary;
	}
	public double getBasic() {
		return basic;
	}
	public void setBasic(double basic) {
		this.basic = basic;
	}
	public String getDa() {
		return da;
	}
	public void setDa(String da) {
		this.da = da;
	}
	public String getHra() {
		return hra;
	}
	public void setHra(String hra) {
		this.hra = hra;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public double getNetSalary() {
		return netSalary;
	}
	public void setNetSalary(double netSalary) {
		this.netSalary = netSalary;
	}
	public double getPayableOTRates() {
		return payableOTRates;
	}
	public void setPayableOTRates(double payableOTRates) {
		this.payableOTRates = payableOTRates;
	}
	public double getChargableOTRates() {
		return chargableOTRates;
	}
	public void setChargableOTRates(double chargableOTRates) {
		this.chargableOTRates = chargableOTRates;
	}


	public double getWashingAllowance() {
		return washingAllowance;
	}


	public void setWashingAllowance(double washingAllowance) {
		this.washingAllowance = washingAllowance;
	}


	public String getBandP() {
		return bandP;
	}


	public void setBandP(String bandP) {
		this.bandP = bandP;
	}


	public String getMedical() {
		return medical;
	}


	public void setMedical(String medical) {
		this.medical = medical;
	}


	public String getConveyance() {
		return conveyance;
	}


	public void setConveyance(String conveyance) {
		this.conveyance = conveyance;
	}


	public String getcLeave() {
		return cLeave;
	}


	public void setcLeave(String cLeave) {
		this.cLeave = cLeave;
	}


	public String getcBonus() {
		return cBonus;
	}


	public void setcBonus(String cBonus) {
		this.cBonus = cBonus;
	}


	public String getTakeHome() {
		return takeHome;
	}


	public void setTakeHome(String takeHome) {
		this.takeHome = takeHome;
	}


	public double getAdditionalAllowance() {
		return additionalAllowance;
	}


	public void setAdditionalAllowance(double additionalAllowance) {
		this.additionalAllowance = additionalAllowance;
	}


	public double getCityCompensatoryAllowance() {
		return cityCompensatoryAllowance;
	}


	public void setCityCompensatoryAllowance(double cityCompensatoryAllowance) {
		this.cityCompensatoryAllowance = cityCompensatoryAllowance;
	}


	


	public double getAdministrativecost() {
		return administrativecost;
	}


	public void setAdministrativecost(double administrativecost) {
		this.administrativecost = administrativecost;
	}


	public boolean isGratuityApplicable() {
		return gratuityApplicable;
	}


	public void setGratuityApplicable(boolean gratuityApplicable) {
		this.gratuityApplicable = gratuityApplicable;
	}


	public String getGratuity() {
		return gratuity;
	}


	public void setGratuity(String gratuity) {
		this.gratuity = gratuity;
	}


	public double getSupervision() {
		return supervision;
	}


	public void setSupervision(double supervision) {
		this.supervision = supervision;
	}


	public double getTrainingCharges() {
		return trainingCharges;
	}


	public void setTrainingCharges(double trainingCharges) {
		this.trainingCharges = trainingCharges;
	}


	public double getEsicCost() {
		return esicCost;
	}


	public void setEsicCost(double esicCost) {
		this.esicCost = esicCost;
	}


	public double getLeaveSalary() {
		return leaveSalary;
	}


	public void setLeaveSalary(double leaveSalary) {
		this.leaveSalary = leaveSalary;
	}





	public double getBonusEsic() {
		return bonusEsic;
	}


	public void setBonusEsic(double bonusEsic) {
		this.bonusEsic = bonusEsic;
	}


	


	public double getPlEsic() {
		return plEsic;
	}


	public void setPlEsic(double plEsic) {
		this.plEsic = plEsic;
	}


	public double getcPlEsic() {
		return cPlEsic;
	}


	public void setcPlEsic(double cPlEsic) {
		this.cPlEsic = cPlEsic;
	}


	public double getcBonusEsic() {
		return cBonusEsic;
	}


	public void setcBonusEsic(double cBonusEsic) {
		this.cBonusEsic = cBonusEsic;
	}
	public double getEsicPercent() {
		return esicPercent;
	}


	public void setEsicPercent(double esicPercent) {
		this.esicPercent = esicPercent;
	}
	
	


	public double getManagementFeesRate() {
		return managementFeesRate;
	}


	public void setManagementFeesRate(double managementFeesRate) {
		this.managementFeesRate = managementFeesRate;
	}

	

	public double getWorkingHours() {
		return workingHours;
	}


	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}


	/**
	 * @author Anil , Date :12-10-2019
	 * 
	 */

	public double getBonusWithoutReliever(){
		double bonusValue = 0;
		try{
			if(getSpBonus()!=null&&!getSpBonus().equals("")){
				bonusValue = Math.round(((this.getBasic() + Double.parseDouble(this.getDa())) * (this.getcBonusPercentage() / 100)));
			}
		}catch(Exception e){
			System.out.println("IN SIDE BONUS EXCEPTION..");
		}
		return bonusValue;
	}
	
	public double getPaidLeaveWithoutReliever(){
		double paidLeavesValue=0;
		try{
			if(getPaidLeaves()!=null&&!getPaidLeaves().equals("")){
				double paiddays=Double.parseDouble(this.getPaidLeaveDays().trim());
				double totalValue = Double.parseDouble(this.getSpEDLI())
						+ Double.parseDouble(this.getSpESIC())
						+ Double.parseDouble(this.getSpMWF())
						+ Double.parseDouble(this.getSpPF());
				totalValue = Math.round(totalValue*100.0)/100.0;
				paidLeavesValue = (this.getGrossSalary() + (totalValue)) * ((paiddays/12)/30);
			}
		}catch(Exception e){
			
		}
		return paidLeavesValue;
	}
	

	public double getActualPayementExcludingOverhead(){
		double actualPayment=0;
		try{
			double totalValue = Double.parseDouble(this.getSpEDLI())
					+ Double.parseDouble(this.getSpESIC())
					+ Double.parseDouble(this.getSpMWF())
					+ Double.parseDouble(this.getSpPF());
			totalValue = Math.round(totalValue*100.0)/100.0;
			totalValue+=getGrossSalary()+getPaidLeaveWithoutReliever()+getBonusWithoutReliever();
			actualPayment=totalValue*getNoOfStaff();
		}catch(Exception e){
			
		}
		return actualPayment;
	}
	





	


	


	
	
	
	
	
	
}
