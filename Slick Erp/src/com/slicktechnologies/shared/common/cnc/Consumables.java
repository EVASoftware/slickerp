package com.slicktechnologies.shared.common.cnc;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class Consumables implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5043709149126476845L;
	/**
	 * 
	 */
	String consumbales;
	String billingType;
	String chargable;
	String opsBudget;
	
	/**
	 * @author Anil @since 09-11-2021
	 * Adding branch carpet area,cost code and site location(customer branch) for Sunrise SCM development
	 * Raised by Rahul Tiwari
	 */
	String branchCarpetArea;
	String costCode;
	String siteLocation;
	
	
	
	
	public Consumables() {
		super();
		// TODO Auto-generated constructor stub
		consumbales="";
		billingType="";
		chargable="";  
		opsBudget="";  
		
		branchCarpetArea="";
		costCode="";
		siteLocation="";
	}
	/**********************************Getters and Setters*******************/
	
	public String getConsumbales() {
		return consumbales;
	}
	public void setConsumbales(String consumbales) {
		this.consumbales = consumbales;
	}
	public String getBillingType() {
		return billingType;
	}
	public void setBillingType(String billingType) {
		this.billingType = billingType;
	}
	public String getChargable() {
		return chargable;
	}
	public void setChargable(String chargable) {
		this.chargable = chargable;
	}
	public String getOpsBudget() {
		return opsBudget;
	}
	public void setOpsBudget(String opsBudget) {
		this.opsBudget = opsBudget;
	}
	public String getBranchCarpetArea() {
		return branchCarpetArea;
	}
	public void setBranchCarpetArea(String branchCarpetArea) {
		this.branchCarpetArea = branchCarpetArea;
	}
	public String getCostCode() {
		return costCode;
	}
	public void setCostCode(String costCode) {
		this.costCode = costCode;
	}
	public String getSiteLocation() {
		return siteLocation;
	}
	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}
	
	
	
}
