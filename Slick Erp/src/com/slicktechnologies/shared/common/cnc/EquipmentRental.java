package com.slicktechnologies.shared.common.cnc;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class EquipmentRental implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8326197558144026610L;
	/**
	 * 
	 */
	String machine;
	String brand;
	double marketPrice;
	double qty;
	double individual_total;
	String rentalEquipmentPerMonth;
	String interestOnMachineryPerMonth;
	String maintananceAndInsurancePerMonth;
	String total;
	String noOFMonths;
	double cBonusPercentage;
	double machineryInterest;
	double maintanancePercentage;
	/** date 31.8.2018 added by komal for discount**/
	double discount;
	double totalWithoutDiscount;
	int productId;
	String productCode;
	
	/**
	 added by Amol for Model no in housekeeping equipment charges
	 Date 4-3-2019
	 **/
	String  modelNumber;
	
	/**
	 * @author Anil , Date : 11-10-2019
	 * adding purchase price to calculate the profit
	 */
	double purchasePrice;
	
	public EquipmentRental() {
		super();
		// TODO Auto-generated constructor stub
		machine="";
		brand="";
		marketPrice=0;
		qty=0;
		individual_total=0;
		rentalEquipmentPerMonth=0+"";
		interestOnMachineryPerMonth=0+"";
		maintananceAndInsurancePerMonth=0+"";
		total=0+"";
		noOFMonths=0+"";
		machineryInterest=0;
		maintanancePercentage=0;
		productCode = "";
	}
	/********************************Getters and Setters*****************/
	
	
	public double getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	
	public String getMachine() {
		return machine;
	}
	
	public double getMachineryInterest() {
		return machineryInterest;
	}
	public void setMachineryInterest(double machineryInterest) {
		this.machineryInterest = machineryInterest;
	}
	public double getMaintanancePercentage() {
		return maintanancePercentage;
	}
	public void setMaintanancePercentage(double maintanancePercentage) {
		this.maintanancePercentage = maintanancePercentage;
	}
	public Double getcBonusPercentage() {
		return cBonusPercentage;
	}
	public void setcBonusPercentage(Double cBonusPercentage) {
		this.cBonusPercentage = cBonusPercentage;
	}
	
	public void setMachine(String machine) {
		this.machine = machine;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(double marketPrice) {
		this.marketPrice = marketPrice;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public double getIndividual_total() {
		return individual_total;
	}
	public void setIndividual_total(double individual_total) {
		this.individual_total = individual_total;
	}
	public String getRentalEquipmentPerMonth() {
		return rentalEquipmentPerMonth;
	}
	public void setRentalEquipmentPerMonth(String rentalEquipmentPerMonth) {
		this.rentalEquipmentPerMonth = rentalEquipmentPerMonth;
	}
	public String getInterestOnMachineryPerMonth() {
		return interestOnMachineryPerMonth;
	}
	public void setInterestOnMachineryPerMonth(String interestOnMachineryPerMonth) {
		this.interestOnMachineryPerMonth = interestOnMachineryPerMonth;
	}
	public String getMaintananceAndInsurancePerMonth() {
		return maintananceAndInsurancePerMonth;
	}
	public void setMaintananceAndInsurancePerMonth(
			String maintananceAndInsurancePerMonth) {
		this.maintananceAndInsurancePerMonth = maintananceAndInsurancePerMonth;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getNoOFMonths() {
		return noOFMonths;
	}
	public void setNoOFMonths(String noOFMonths) {
		this.noOFMonths = noOFMonths;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getTotalWithoutDiscount() {
		return totalWithoutDiscount;
	}
	public void setTotalWithoutDiscount(double totalWithoutDiscount) {
		this.totalWithoutDiscount = totalWithoutDiscount;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	
	
	
	
	
	
}
