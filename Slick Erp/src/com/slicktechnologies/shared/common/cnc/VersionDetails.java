package com.slicktechnologies.shared.common.cnc;

import java.io.Serializable;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
@Embed
public class VersionDetails extends SuperModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7167237426778580289L;
	/**
	 * 
	 */
	@Index
	int contractCount;
	@Index
	List<StaffingDetails> saffingDetailsList;
	@Index
	List<EquipmentRental> equipmentRentalList;
	@Index
	List<Consumables> consumablesList;
	@Index
	List<OtherServices> otherServicesList;
	
	/*********************************Getters and Setters*************************************************/	
	
	public List<StaffingDetails> getSaffingDetailsList() {
		return saffingDetailsList;
	}
	public void setSaffingDetailsList(List<StaffingDetails> saffingDetailsList) {
		this.saffingDetailsList = saffingDetailsList;
	}
	public List<EquipmentRental> getEquipmentRentalList() {
		return equipmentRentalList;
	}
	public void setEquipmentRentalList(List<EquipmentRental> equipmentRentalList) {
		this.equipmentRentalList = equipmentRentalList;
	}
	public List<Consumables> getConsumablesList() {
		return consumablesList;
	}
	public void setConsumablesList(List<Consumables> consumablesList) {
		this.consumablesList = consumablesList;
	}
	public List<OtherServices> getOtherServicesList() {
		return otherServicesList;
	}
	public void setOtherServicesList(List<OtherServices> otherServicesList) {
		this.otherServicesList = otherServicesList;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
