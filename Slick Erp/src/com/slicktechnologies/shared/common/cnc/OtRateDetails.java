package com.slicktechnologies.shared.common.cnc;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
@Embed
public class OtRateDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7392881200273002448L;

	String designation;
	String otName;
	double flatRate;
	double factorRate;
	String type;
	
	public OtRateDetails() {
		super();
		designation="";
		otName="";
		flatRate=0;
		factorRate=0;
		type="";
	}
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getOtName() {
		return otName;
	}
	public void setOtName(String otName) {
		this.otName = otName;
	}
	public double getFlatRate() {
		return flatRate;
	}
	public void setFlatRate(double flatRate) {
		this.flatRate = flatRate;
	}
	public double getFactorRate() {
		return factorRate;
	}
	public void setFactorRate(double factorRate) {
		this.factorRate = factorRate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
