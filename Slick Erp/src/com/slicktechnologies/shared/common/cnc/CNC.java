package com.slicktechnologies.shared.common.cnc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.CNCArrearsDetails;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

@Embed
@Entity
public class CNC extends ConcreteBusinessProcess {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5305567455769080324L;

	public static final String CONTRACTCANCEL = "Cancelled";
	public static final String CONTRACTCREATED = "Created";
	public static final String CONTRACTAPPROVED = "Approved";
	public static final String CONTRACTCONFIRMED = "Confirmed";

	@Index
	protected PersonInfo personInfo;
	@Index
	Date contractStartDate;
	@Index
	Date contractEndDate;
	@Index
	String status;
	@Index
	String contractGroup;
	@Index
	String contractCategory;
	@Index
	String contractType;
	@Index
	String branch;
	@Index
	String salesPerson;
	@Index
	String refNum;
	@Index
	String paymentMethod;
	@Index
	String paymentTerms;
	@Index
	int creditPeriod;
	@Index
	String approverName;
	@Index
	String approverRemark;
	
	@Index
	String  cancellationRemark;
	
	// @Index
	// List<VersionDetails> versionDetailsList;
	@Index
	ArrayList<StaffingDetails> saffingDetailsList;
	@Index
	ArrayList<EquipmentRental> equipmentRentalList;
	@Index
	ArrayList<Consumables> consumablesList;
	@Index
	ArrayList<OtherServices> otherServicesList;
	@Serialize
	@EmbedMap
	HashMap<Integer, CNC> versionMap;
	@Index
	double noOfMonthsForRental, interestOnMachinary, maintainanceCharges;
	double overheadCostPerEmployee;
	double managementFees;
	@Index
	String projectName;
	String cBonusPercentage;
	boolean bonus_relever;
	boolean paidLeave_Relever;
	double netPayableOFStaffing;
	double netPayableOFEquipment;
	double netPayableOFConsumables;
	double netPayableOFOther;
	double netPayableOFCNC;
	double grossTotalOfStaffing, managementFeesValue;
	boolean isCLeaveApplicable;
	boolean isCBonusApplicable;
	boolean isGratuityApplicable;
	/** 10.8.2018 added by komal **/
	String termsAndConditions;
	String paidLeavesDays/* Rahul Verma added this on 08 Aug 2018 */;
	/** date 13.8.2018 added by komal to store %bonus-value , %travelling-value , %overhead cost-value**/
	double flatManagementFees;
	double bonusPercent;
	double flatBonus;
//	double travellingPercent;
	double flatTravelling;
//	double overheadCostPercent;
	double flatOverheadCost;
	/** date 20.8.2018 added by komal for version number**/
	@Index
	double versionNumber;
	@Index
	Date versionStartDate;
	@Index
	Date versionEndDate;
	@Index
	boolean islatestVersion;
	/** date 31.8.2018 added by komal**/
	double equipmentDiscountAmount;
	double TotalEquipmentAmount;//withount discount
	/** date 27.10.2018 added by komal for invoice under contract number field in sasha cnc billing **/
	String contractNumber;
	/** date 30.11.2018 added by komal to make 2 seperate bills for staffing and equipment rental **/
	boolean isStaffingRentalDifferent;
	/** date 23.1.2019 added by komal to add description in CNC**/
	String description;
    /**Date 25-6-2019 by Amol**/
	double supervision;
	double trainingCharges;
	
	
	/**
	 * @author Anil , Date : 17-07-2019
	 * Adding cncid in string for connecting it to payroll and invoice
	 * raised by Nitin Sir, Abhinav,Rohan and komal for google studio report
	 */
	@Index
	String cncId;
	
	double monthlyFinancePlan,interestOnFinAmt,estimatedRepair,adminOverheadCost,approxUniformCost;
	
	ArrayList<CNCArrearsDetails> cncArrearsList;
	/**Date by 3-7-2020 by Amol***/
	protected String customerBranch;
	Address billingAddress;
	Address serviceAddress;
	
	/**
	 * @author Anil
	 * @since 24-07-2020
	 * Adding field payroll start day which will define payroll cycle
	 */
	Integer payrollStartDay;
	
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * Adding state field for Sunrise Facility
	 * Raised by Rahul Tiwari
	 */
	@Index
	String state;
	@Index
	String paidLeaveName;
	
	/**
	 * @author Anil
	 * @since 20-10-2020
	 * Adding ot rate details
	 */
	@Index
	ArrayList<OtRateDetails> otRateDetailsList;
	double workingHours;
	
	@Index
	String customerGstNumber;
	
	public CNC() {
		super();
		// TODO Auto-generated constructor stub
		status = "";

		contractGroup = "";

		contractCategory = "";

		contractType = "";

		branch = "";

		salesPerson = "";

		refNum = "";

		paymentMethod = "";

		paymentTerms = "";

		creditPeriod = 0;

		approverName = "";

		approverRemark = "";

		saffingDetailsList = new ArrayList<StaffingDetails>();
		equipmentRentalList = new ArrayList<EquipmentRental>();
		consumablesList = new ArrayList<Consumables>();
		otherServicesList = new ArrayList<OtherServices>();

		noOfMonthsForRental = 0;
		interestOnMachinary = 0;
		maintainanceCharges = 0;
		// versionMap=new HashMap<Integer, CNC>();
		overheadCostPerEmployee = 0;
		managementFees = 0;
		projectName = "";
		cBonusPercentage = 0 + "";
		bonus_relever = false;
		paidLeave_Relever = false;
		netPayableOFStaffing = 0;
		netPayableOFEquipment = 0;
		netPayableOFConsumables = 0;
		netPayableOFOther = 0;
		netPayableOFCNC = 0;
		grossTotalOfStaffing = 0;
		managementFeesValue = 0;
		supervision=0;
		trainingCharges=0;
		
		isCLeaveApplicable = false;
		isCBonusApplicable = false;
		isGratuityApplicable=false;
		/** 10.8.2018 added by komal **/
		termsAndConditions = "";
		/*
		 * Rahul Verma added on 08 Aug 2018
		 */
		paidLeavesDays = 0 + "";
		islatestVersion = false;
		contractNumber = "";
		isStaffingRentalDifferent = false;
		description = "";
		cncArrearsList  = new ArrayList<CNCArrearsDetails>();
		
		otRateDetailsList=new ArrayList<OtRateDetails>();
	}

	/***************************** Getters and Setters *****************************/

	public double getAdminOverheadCost() {
		return adminOverheadCost;
	}

	public void setAdminOverheadCost(double adminOverheadCost) {
		this.adminOverheadCost = adminOverheadCost;
	}

	public double getApproxUniformCost() {
		return approxUniformCost;
	}

	public void setApproxUniformCost(double approxUniformCost) {
		this.approxUniformCost = approxUniformCost;
	}

	public double getMonthlyFinancePlan() {
		return monthlyFinancePlan;
	}
	
	public void setMonthlyFinancePlan(double monthlyFinancePlan) {
		this.monthlyFinancePlan = monthlyFinancePlan;
	}

	public double getInterestOnFinAmt() {
		return interestOnFinAmt;
	}

	public void setInterestOnFinAmt(double interestOnFinAmt) {
		this.interestOnFinAmt = interestOnFinAmt;
	}

	public double getEstimatedRepair() {
		return estimatedRepair;
	}

	public void setEstimatedRepair(double estimatedRepair) {
		this.estimatedRepair = estimatedRepair;
	}
	
	public String getPaidLeavesDays() {
		return paidLeavesDays;
	}

	public String getCncId() {
		return cncId;
	}

	public void setCncId(String cncId) {
		this.cncId = cncId;
	}

	public void setPaidLeavesDays(String paidLeavesDays) {
		this.paidLeavesDays = paidLeavesDays;
	}

	public String getProjectName() {
		return projectName;
	}

	public boolean isBonus_relever() {
		return bonus_relever;
	}

	public void setBonus_relever(boolean bonus_relever) {
		this.bonus_relever = bonus_relever;
	}

	public boolean isPaidLeave_Relever() {
		return paidLeave_Relever;
	}

	public void setPaidLeave_Relever(boolean paidLeave_Relever) {
		this.paidLeave_Relever = paidLeave_Relever;
	}

	public boolean isCLeaveApplicable() {
		return isCLeaveApplicable;
	}

	public void setCLeaveApplicable(boolean isCLeaveApplicable) {
		this.isCLeaveApplicable = isCLeaveApplicable;
	}

	public boolean isCBonusApplicable() {
		return isCBonusApplicable;
	}

	public void setCBonusApplicable(boolean isCBonusApplicable) {
		this.isCBonusApplicable = isCBonusApplicable;
	}

	public double getGrossTotalOfStaffing() {
		return grossTotalOfStaffing;
	}

	public void setGrossTotalOfStaffing(double grossTotalOfStaffing) {
		this.grossTotalOfStaffing = grossTotalOfStaffing;
	}

	public double getManagementFeesValue() {
		return managementFeesValue;
	}

	public void setManagementFeesValue(double managementFeesValue) {
		this.managementFeesValue = managementFeesValue;
	}

	public double getNetPayableOFEquipment() {
		return netPayableOFEquipment;
	}

	public void setNetPayableOFEquipment(double netPayableOFEquipment) {
		this.netPayableOFEquipment = netPayableOFEquipment;
	}

	public double getNetPayableOFConsumables() {
		return netPayableOFConsumables;
	}

	public void setNetPayableOFConsumables(double netPayableOFConsumables) {
		this.netPayableOFConsumables = netPayableOFConsumables;
	}

	public double getNetPayableOFOther() {
		return netPayableOFOther;
	}

	public void setNetPayableOFOther(double netPayableOFOther) {
		this.netPayableOFOther = netPayableOFOther;
	}

	public double getNetPayableOFCNC() {
		return netPayableOFCNC;
	}

	public void setNetPayableOFCNC(double netPayableOFCNC) {
		this.netPayableOFCNC = netPayableOFCNC;
	}

	public double getNetPayableOFStaffing() {
		return netPayableOFStaffing;
	}

	public void setNetPayableOFStaffing(double netPayableOFStaffing) {
		this.netPayableOFStaffing = netPayableOFStaffing;
	}

	public String getcBonusPercentage() {
		return cBonusPercentage;
	}

	public void setcBonusPercentage(String cBonusPercentage) {
		this.cBonusPercentage = cBonusPercentage;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public double getOverheadCostPerEmployee() {
		return overheadCostPerEmployee;
	}

	public void setOverheadCostPerEmployee(double overheadCostPerEmployee) {
		this.overheadCostPerEmployee = overheadCostPerEmployee;
	}

	public double getManagementFees() {
		return managementFees;
	}

	public void setManagementFees(double managementFees) {
		this.managementFees = managementFees;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContractGroup() {
		return contractGroup;
	}

	public void setContractGroup(String contractGroup) {
		this.contractGroup = contractGroup;
	}

	public String getContractCategory() {
		return contractCategory;
	}

	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public int getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(int creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getApproverRemark() {
		return approverRemark;
	}

	public void setApproverRemark(String approverRemark) {
		this.approverRemark = approverRemark;
	}
	
	

	public String getCancellationRemark() {
		return cancellationRemark;
	}

	public void setCancellationRemark(String cancellationRemark) {
		this.cancellationRemark = cancellationRemark;
	}

	
	
	
	
	public String getCustomerBranch() {
		return customerBranch;
	}

	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<StaffingDetails> getSaffingDetailsList() {
		return saffingDetailsList;
	}

	public void setSaffingDetailsList(List<StaffingDetails> saffingDetailList) {
		ArrayList<StaffingDetails> saffingDetailsList = new ArrayList<StaffingDetails>();
		saffingDetailsList.addAll(saffingDetailList);
		this.saffingDetailsList = saffingDetailsList;
	}

	public List<EquipmentRental> getEquipmentRentalList() {
		return equipmentRentalList;
	}

	public void setEquipmentRentalList(List<EquipmentRental> equipmentrentalList) {
		ArrayList<EquipmentRental> equipmentRentalList = new ArrayList<EquipmentRental>();
		equipmentRentalList.addAll(equipmentrentalList);
		this.equipmentRentalList = equipmentRentalList;
	}

	public List<Consumables> getConsumablesList() {
		return consumablesList;
	}

	public void setConsumablesList(List<Consumables> consumableList) {
		ArrayList<Consumables> consumablesList = new ArrayList<Consumables>();
		consumablesList.addAll(consumableList);
		this.consumablesList = consumablesList;
	}

	public List<OtherServices> getOtherServicesList() {
		return otherServicesList;
	}

	public void setOtherServicesList(List<OtherServices> otherServiceList) {
		ArrayList<OtherServices> otherServicesList = new ArrayList<OtherServices>();
		otherServicesList.addAll(otherServiceList);
		this.otherServicesList = otherServicesList;
	}

	public HashMap<Integer, CNC> getVersionMap() {
		return versionMap;
	}

	public void setVersionMap(HashMap<Integer, CNC> versionMap) {
		this.versionMap = versionMap;
	}

	
	public boolean isGratuityApplicable() {
		return isGratuityApplicable;
	}

	public void setGratuityApplicable(boolean isGratuityApplicable) {
		this.isGratuityApplicable = isGratuityApplicable;
	}
	
	public double getSupervision() {
		return supervision;
	}

	public void setSupervision(double supervision) {
		this.supervision = supervision;
	}

	public double getTrainingCharges() {
		return trainingCharges;
	}

	public void setTrainingCharges(double trainingCharges) {
		this.trainingCharges = trainingCharges;
	}

	@Override
	public String getEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		// // TODO Auto-generated method stub
		// ProjectAllocation projectAllocation=new ProjectAllocation();
		// projectAllocation.setBranch(this.getBranch());
		// projectAllocation.setContractId(this.getCount());
		// projectAllocation.setEndDate(this.getContractEndDate());
		// projectAllocation.setStartDate(this.getContractStartDate());
		// projectAllocation.setInterestOnMachinary(this.getInterestOnMachinary());
		// projectAllocation.setMaintainanceCharges(this.getMaintainanceCharges());
		// projectAllocation.setManagementFees(this.getManagementFees());
		// projectAllocation.setNoOfMonthsForRental(this.getNoOfMonthsForRental());
		// projectAllocation.setOverheadCostPerEmployee(this.getOverheadCostPerEmployee());
		// projectAllocation.setPersonInfo(this.getPersonInfo());
		// projectAllocation.setProjectName(this.getCount()+this.getPersonInfo().getFullName());
		// projectAllocation.setStatus(CREATED);
		// ArrayList<EmployeeProjectAllocation>
		// employeeProjectAllocationList=new
		// ArrayList<EmployeeProjectAllocation>();
		//
		// for (int i = 0; i < this.getSaffingDetailsList().size(); i++) {
		// for (int j = 0; j <
		// this.getSaffingDetailsList().get(i).getNoOfStaff(); j++) {
		// StaffingDetails staffingDetails=new StaffingDetails();
		// staffingDetails=this.getSaffingDetailsList().get(i);
		// EmployeeProjectAllocation employeeProject=new
		// EmployeeProjectAllocation(staffingDetails.getDesignation(),"","","","");
		// employeeProjectAllocationList.add(employeeProject);
		// }
		// }
		// projectAllocation.setEmployeeProjectAllocationList(employeeProjectAllocationList);
		//
		// ArrayList<Consumables> listConsumables=new ArrayList<Consumables>();
		// listConsumables.addAll(this.getConsumablesList());
		// projectAllocation.setConsumablesList(listConsumables);
		//
		// ArrayList<EquipmentRental> listEquipment=new
		// ArrayList<EquipmentRental>();
		// listEquipment.addAll(this.getEquipmentRentalList());
		// projectAllocation.setEquipmentRentalList(listEquipment);
		//
		// ArrayList<OtherServices> listOtherServices=new
		// ArrayList<OtherServices>();
		// listOtherServices.addAll(this.getOtherServicesList());
		// projectAllocation.setOtherServicesList(listOtherServices);
		//
		// projectAllocation.setCompanyId(this.getCompanyId());
		// NumberGeneration ng = new NumberGeneration();
		// ng = ofy().load().type(NumberGeneration.class)
		// .filter("companyId",this.getCompanyId())
		// .filter("processName", "ProjectAllocation").filter("status", true)
		// .first().now();
		// long number=ng.getNumber();
		// int count=(int)number;
		// ng.setNumber(count+1);
		// ofy().save().entity(ng);
		// projectAllocation.setCount(count+1);
		// ofy().save().entity(projectAllocation);
	}

	@Override
	public void reactOnRejected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setApprovalDate(Date date) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getRemark() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRemark(String remark) {
		// TODO Auto-generated method stub

	}

	public double getNoOfMonthsForRental() {
		return noOfMonthsForRental;
	}

	public void setNoOfMonthsForRental(double noOfMonthsForRental) {
		this.noOfMonthsForRental = noOfMonthsForRental;
	}

	public double getInterestOnMachinary() {
		return interestOnMachinary;
	}

	public void setInterestOnMachinary(double interestOnMachinary) {
		this.interestOnMachinary = interestOnMachinary;
	}

	public double getMaintainanceCharges() {
		return maintainanceCharges;
	}

	public void setMaintainanceCharges(double maintainanceCharges) {
		this.maintainanceCharges = maintainanceCharges;
	}

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public double getFlatManagementFees() {
		return flatManagementFees;
	}

	public void setFlatManagementFees(double flatManagementFees) {
		this.flatManagementFees = flatManagementFees;
	}

	public double getBonusPercent() {
		return bonusPercent;
	}

	public void setBonusPercent(double bonusPercent) {
		this.bonusPercent = bonusPercent;
	}

	public double getFlatBonus() {
		return flatBonus;
	}

	public void setFlatBonus(double flatBonus) {
		this.flatBonus = flatBonus;
	}

	public double getFlatTravelling() {
		return flatTravelling;
	}

	public void setFlatTravelling(double flatTravelling) {
		this.flatTravelling = flatTravelling;
	}

	public double getFlatOverheadCost() {
		return flatOverheadCost;
	}

	public void setFlatOverheadCost(double flatOverheadCost) {
		this.flatOverheadCost = flatOverheadCost;
	}
	
	public double getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(double versionNumber) {
		this.versionNumber = versionNumber;
	}

	public boolean isIslatestVersion() {
		return islatestVersion;
	}

	public void setIslatestVersion(boolean islatestVersion) {
		this.islatestVersion = islatestVersion;
	}

	public double getEquipmentDiscountAmount() {
		return equipmentDiscountAmount;
	}

	public void setEquipmentDiscountAmount(double equipmentDiscountAmount) {
		this.equipmentDiscountAmount = equipmentDiscountAmount;
	}

	public double getTotalEquipmentAmount() {
		return TotalEquipmentAmount;
	}

	public void setTotalEquipmentAmount(double totalEquipmentAmount) {
		TotalEquipmentAmount = totalEquipmentAmount;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public boolean isStaffingRentalDifferent() {
		return isStaffingRentalDifferent;
	}

	public void setStaffingRentalDifferent(boolean isStaffingRentalDifferent) {
		this.isStaffingRentalDifferent = isStaffingRentalDifferent;
	}

//	/**  date 27.8.2018 added by komal to add data in cncversion entity**/
//	@OnSave
//	@GwtIncompatible
//	public void createVersion(){
//		GenricServiceImpl impl = new GenricServiceImpl();
//		if(this.getStatus().equals(AppConstants.CREATED) || this.getStatus().equals(AppConstants.APPROVED) || this.getStatus().equals(AppConstants.REQUESTED)){
//			CNCVersion cncVer = new CNCVersion();
//			this.setIslatestVersion(false);
//			cncVer.setCnc(this);
//			cncVer.setVersionCreationDate(DateUtility.getDateWithTimeZone("IST", new Date()));
//			cncVer.setVersionStatus(AppConstants.CREATED);
//			cncVer.setVersion(this.getVersionNumber()+"");
//			cncVer.setCompanyId(this.getCompanyId());
//			impl.save(cncVer);
//		}
//		
//	}
	
	
	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Address getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(Address serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	@OnSave
	@GwtIncompatible
	public void updateCNCId(){
			cncId=count+"";
	}
	/**1-12-2018 added status list box by amol **/
	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CONTRACTCREATED);
		statuslist.add(CONTRACTAPPROVED);
		statuslist.add(CONTRACTCONFIRMED);
		statuslist.add(CONTRACTCANCEL);
			
		return statuslist;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public ArrayList<CNCArrearsDetails> getCncArrearsList() {
		return cncArrearsList;
	}

	public void setCncArrearsList(ArrayList<CNCArrearsDetails> cncArrearsList) {
		this.cncArrearsList = cncArrearsList;
	}
	
	

	public Integer getPayrollStartDay() {
		return payrollStartDay;
	}

	public void setPayrollStartDay(Integer payrollStartDay) {
		this.payrollStartDay = payrollStartDay;
	}
	
	

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	

	public String getPaidLeaveName() {
		return paidLeaveName;
	}

	public void setPaidLeaveName(String paidLeaveName) {
		this.paidLeaveName = paidLeaveName;
	}

	/**
	 * @author Vijay Chougule Date 22-06-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getContractStartDate()!=null){
			this.setContractStartDate(dateUtility.setTimeMidOftheDayToDate(this.getContractStartDate()));
		}
		if(this.getContractEndDate()!=null){
			this.setContractEndDate(dateUtility.setTimeMidOftheDayToDate(this.getContractEndDate()));
		}
		
	}
	/**
	 * ends here	
	 */

	public ArrayList<OtRateDetails> getOtRateDetailsList() {
		return otRateDetailsList;
	}

	public void setOtRateDetailsList(List<OtRateDetails> list2) {
		ArrayList<OtRateDetails> list=new ArrayList<OtRateDetails>();
		list.addAll(list2);
		this.otRateDetailsList = list;
	}

	public double getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}

	public String getCustomerGstNumber() {
		return customerGstNumber;
	}

	public void setCustomerGstNumber(String customerGstNumber) {
		this.customerGstNumber = customerGstNumber;
	}
	
	
	

}
