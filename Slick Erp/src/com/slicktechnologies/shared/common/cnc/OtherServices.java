package com.slicktechnologies.shared.common.cnc;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

@Embed
public class OtherServices implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4942907267042423338L;
	/**
	 * 
	 */
	String otherService;
	double chargableRates;
	String details;
	String frequencyOfServices;
	double vendorRates;
	String specialInformation;
	/** date 1.9.2018 added by komal to save product info**/
	ServiceProduct serviceProduct;

	
	public OtherServices() {
		super();
		// TODO Auto-generated constructor stub
		otherService="";       
		chargableRates=0d;     
		details="";            
		frequencyOfServices="";
		vendorRates=0d;        
		specialInformation="";
		/** date 1.9.2018 added by komal to save product info**/
		serviceProduct = new ServiceProduct();
	}
	public ServiceProduct getServiceProduct() {
		return serviceProduct;
	}
	public void setServiceProduct(ServiceProduct serviceProduct) {
		this.serviceProduct = serviceProduct;
	}
	/********************Getters and Setters*************************/
	
	public String getOtherService() {
		return otherService;
	}
	public void setOtherService(String otherService) {
		this.otherService = otherService;
	}
	public double getChargableRates() {
		return chargableRates;
	}
	public void setChargableRates(double chargableRates) {
		this.chargableRates = chargableRates;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getFrequencyOfServices() {
		return frequencyOfServices;
	}
	public void setFrequencyOfServices(String frequencyOfServices) {
		this.frequencyOfServices = frequencyOfServices;
	}
	public double getVendorRates() {
		return vendorRates;
	}
	public void setVendorRates(double vendorRates) {
		this.vendorRates = vendorRates;
	}
	public String getSpecialInformation() {
		return specialInformation;
	}
	public void setSpecialInformation(String specialInformation) {
		this.specialInformation = specialInformation;
	}
	
	
	
	
	
}
