package com.slicktechnologies.shared.common.scheduling;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class TeamWorkingHours extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2849149587259473094L;


	String teamName;
	double fromTime;
	double toTime;
	double teamWorkingHrs;
	double totalWorkingHrs;
	double reservedTime;
	double overTime;
	double perDayWorkingHrs;
	
	
	
	
	
	
	public String getTeamName() {
		return teamName;
	}






	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}






	public double getFromTime() {
		return fromTime;
	}






	public void setFromTime(double fromTime) {
		this.fromTime = fromTime;
	}






	public double getToTime() {
		return toTime;
	}






	public void setToTime(double toTime) {
		this.toTime = toTime;
	}






	public double getTotalWorkingHrs() {
		return totalWorkingHrs;
	}






	public void setTotalWorkingHrs(double totalWorkingHrs) {
		this.totalWorkingHrs = totalWorkingHrs;
	}






	public double getTeamWorkingHrs() {
		return teamWorkingHrs;
	}






	public void setTeamWorkingHrs(double teamWorkingHrs) {
		this.teamWorkingHrs = teamWorkingHrs;
	}






	public double getReservedTime() {
		return reservedTime;
	}






	public void setReservedTime(double reservedTime) {
		this.reservedTime = reservedTime;
	}






	public double getOverTime() {
		return overTime;
	}






	public void setOverTime(double overTime) {
		this.overTime = overTime;
	}






	public double getPerDayWorkingHrs() {
		return perDayWorkingHrs;
	}






	public void setPerDayWorkingHrs(double perDayWorkingHrs) {
		this.perDayWorkingHrs = perDayWorkingHrs;
	}






	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
}
