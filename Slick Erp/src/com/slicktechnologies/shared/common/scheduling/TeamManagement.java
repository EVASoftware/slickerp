package com.slicktechnologies.shared.common.scheduling;

import com.simplesoftwares.client.library.appstructure.SuperModel;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
@Entity
public class TeamManagement extends SuperModel{

	private static final long serialVersionUID = 8976634169587632503L;

	@Index
	String teamName;
	@Index
	String shiftName;
	
	@Index
	Double totalWorkingHrs;
	
	@Index
	protected double fromTime;
	@Index
	protected double toTime;
	
	boolean status;
	ArrayList<TeamGroup> teamList;
	
//	@Ignore
	protected Calendar leaveCalendar;
	
	public TeamManagement() {
		super();
		teamName="";
		teamList=new ArrayList<TeamGroup>();
	}
	
	
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public ArrayList<TeamGroup> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<TeamGroup> teamList) {
		if(teamList!=null)
		{
			this.teamList=new ArrayList<TeamGroup>();
			this.teamList.addAll(teamList);
		}
	}
	
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		if(status!=null)
		this.status = status;
	}

	public double getFromTime() {
		return fromTime;
	}

	public void setFromTime(double fromTime) {
		this.fromTime = fromTime;
	}
	public double getToTime() {
		return toTime;
	}

	public void setToTime(double toTime) {
		this.toTime = toTime;
	}

	public Double getTotalWorkingHrs() {
		return totalWorkingHrs;
	}

	public void setTotalWorkingHrs(Double totalWorkingHrs) {
		this.totalWorkingHrs = totalWorkingHrs;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		TeamManagement entity = (TeamManagement) m;
		String name = entity.getTeamName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.teamName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
		}
	}
	
	
	
	
	public Calendar getLeaveCalendar() {
		return leaveCalendar;
	}

	public void setLeaveCalendar(Calendar leaveCalendar) {
		this.leaveCalendar = leaveCalendar;
	}

	@Override
	public String toString() {
		return teamName.toString();
	}
	
	
	@GwtIncompatible
	@OnSave
	private void saveCalender(){
		System.out.println("0");
		if(id==null){
			System.out.println("1");
			if(teamList.size()!=0){
				System.out.println("2");
				EmployeeInfo info=ofy().load().type(EmployeeInfo.class).filter("fullName",teamList.get(0).getEmployeeName()).filter("companyId", getCompanyId()).first().now();
				if(info!=null){
					System.out.println("3");
					System.out.println("Cal 1 ::: "+info.getLeaveCalendar());
					setLeaveCalendar(info.getLeaveCalendar());
					System.out.println("Cal ::: "+getLeaveCalendar());
				}
			}
		}
	}
	

}
