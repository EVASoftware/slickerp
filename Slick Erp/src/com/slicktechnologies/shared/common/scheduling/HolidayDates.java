package com.slicktechnologies.shared.common.scheduling;

import com.simplesoftwares.client.library.appstructure.SuperModel;
import java.util.Date;
public class HolidayDates extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -306223866619841521L;

	String holidayName;
	Date holidayDate;
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


	public String getHolidayName() {
		return holidayName;
	}


	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}


	public Date getHolidayDate() {
		return holidayDate;
	}


	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	
}
