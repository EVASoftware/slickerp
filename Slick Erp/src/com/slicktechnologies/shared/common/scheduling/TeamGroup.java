package com.slicktechnologies.shared.common.scheduling;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class TeamGroup extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7865729107138853773L;

	String employeeName;
	/** date 8.3.2018 added by komal for customer service list new changes (to add team) **/
	int customerCount;
	long phoneNumber;
	String empDesignation;
	
	
	public TeamGroup() {
		super();
	}
	
	
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getCustomerCount() {
		return customerCount;
	}


	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}


	public long getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getEmpDesignation() {
		return empDesignation;
	}


	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
