package com.slicktechnologies.shared.common.contractrenewal;

import com.simplesoftwares.client.library.appstructure.SuperModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.shared.SalesLineItem;
@Entity
public class ContractRenewal extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7568375655375101494L;
	/**
	 * 
	 */
	@Index
	int contractId;
	@Index
	Date contractDate;
	@Index
	Date contractEndDate;
	@Index
	String branch;
	@Index
	String salesPerson;
	@Index
	int customerId;
	@Index
	String customerName;
	@Index
	long customerCell;
	String city;
	String locality;
	int productId;
	String productName;
	String productCode;
	double productPrice;
	double productNewPrice;
	int duration;
	String year;
	@Index
	String status;
	Date date;
	String user;
	protected boolean recordSelect;
	protected ArrayList<SalesLineItem>items;
	
	String description;
	
	/**
	 * Date 11 April 2017 added by vijay for processed contracts net amount and contract type
	 */
	String contractType;
	double netPayable;
	
	/**
	 * nidhi
	 * 9-06-2018
	 */
	String nonRenewalRemark;
	
	/**
	 * @author 27-06-2021 by Vijay
	 * Des :- payment Gateway unique ID	
	 */
	@Index
	protected String paymentGatewayUniqueId;
	String contractCategory; //16-01-2024 Ultra pest requirement
	
	

	public ContractRenewal() {
		items=new ArrayList<SalesLineItem>();
		contractType="";
		nonRenewalRemark="";
		paymentGatewayUniqueId="";
		contractCategory="";
	}
	
	
	public int getContractId() {
		return contractId;
	}



	public void setContractId(int contractId) {
		this.contractId = contractId;
	}



	public Date getContractDate() {
		return contractDate;
	}






	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}






	public Date getContractEndDate() {
		return contractEndDate;
	}






	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}






	public String getBranch() {
		return branch;
	}






	public void setBranch(String branch) {
		this.branch = branch;
	}






	public String getSalesPerson() {
		return salesPerson;
	}






	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}






	public int getCustomerId() {
		return customerId;
	}






	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}






	public String getCustomerName() {
		return customerName;
	}






	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}






	public long getCustomerCell() {
		return customerCell;
	}






	public void setCustomerCell(long customerCell) {
		this.customerCell = customerCell;
	}






	public String getCity() {
		return city;
	}






	public void setCity(String city) {
		this.city = city;
	}






	public String getLocality() {
		return locality;
	}






	public void setLocality(String locality) {
		this.locality = locality;
	}






	public int getProductId() {
		return productId;
	}






	public void setProductId(int productId) {
		this.productId = productId;
	}






	public String getProductName() {
		return productName;
	}






	public void setProductName(String productName) {
		this.productName = productName;
	}






	public String getProductCode() {
		return productCode;
	}






	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}






	public double getProductPrice() {
		return productPrice;
	}






	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}






	public double getProductNewPrice() {
		return productNewPrice;
	}






	public void setProductNewPrice(double productNewPrice) {
		this.productNewPrice = productNewPrice;
	}






	public int getDuration() {
		return duration;
	}






	public void setDuration(int duration) {
		this.duration = duration;
	}






	public String getYear() {
		return year;
	}






	public void setYear(String year) {
		this.year = year;
	}






	public String getStatus() {
		return status;
	}






	public void setStatus(String status) {
		this.status = status;
	}






	public Date getDate() {
		return date;
	}






	public void setDate(Date date) {
		this.date = date;
	}






	public String getUser() {
		return user;
	}






	public void setUser(String user) {
		this.user = user;
	}


	public boolean getRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	/**
	 * Gets the items.
	 * @return the items
	 */
	public List<SalesLineItem> getItems() 
	{
		return items;
	}

	/**
	 * Sets the items.
	 * @param items the new items
	 */
	public void setItems(List<SalesLineItem> items) 
	{
		ArrayList<SalesLineItem> arrayitems=new ArrayList<SalesLineItem>();
		arrayitems.addAll(items);
		this.items = arrayitems;
	}


//	@GwtIncompatible
//	@OnSave
//	private void updateContractStatus()
//	{
//		
//		Contract con=ofy().load().type(Contract.class).filter("companyId",this.getCompanyId()).filter("count",this.getContractId()).first().now();
//		
//		if(con!=null){
//			con.setRenewProcessed(AppConstants.YES);
//			ofy().save().entity(con).now();
//		}
//	}

	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getContractType() {
		return contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public double getNetPayable() {
		return netPayable;
	}


	public void setNetPayable(double netPayable) {
		this.netPayable = netPayable;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


	public String getNonRenewalRemark() {
		return nonRenewalRemark;
	}


	public void setNonRenewalRemark(String nonRenewalRemark) {
		this.nonRenewalRemark = nonRenewalRemark;
	}


	public String getPaymentGatewayUniqueId() {
		return paymentGatewayUniqueId;
	}


	public void setPaymentGatewayUniqueId(String paymentGatewayUniqueId) {
		this.paymentGatewayUniqueId = paymentGatewayUniqueId;
	}
	
	public String getContractCategory() {
		return contractCategory;
	}


	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}

}
