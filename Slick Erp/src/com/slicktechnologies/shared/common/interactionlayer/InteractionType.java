package com.slicktechnologies.shared.common.interactionlayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

@Entity
public class InteractionType extends SuperModel {

	private static final long serialVersionUID = -2027537110783182133L;

	public static final String INTERACTIONCREATED = "Created";
	public static final String INTERACTIONCLOSED = "Closed";

	/************************************************************************************************************/

	/********************************************* Applicability Attributes ******************************************/

	@Index
	protected String interactionTitle;
	@Index
	protected Date interactionCreationDate;
	@Index
	protected Date interactionDueDate;
	@Index
	protected String interactionProject;
	@Index
	protected String interactionModuleName;
	@Index
	protected String interactionDocumentName;
	@Index
	protected String interactionCommunicationType;
	@Index
	protected String interactionCommunicationCategory;
	@Index
	protected String interactionPersonResponsible;
	@Index
	protected String interactionBranch;
	@Index
	protected String interactionGroup;
	@Index
	protected String interactionStatus;
	@Index
	protected String interactionPersonType;
	@Index
	protected String interactionPurpose;
	@Index
	protected String interactionOutcome;
	@Index
	protected String interactionPriority;
	@Index
	protected int interactionDocumentId;

	protected PersonInfo personInfo;
	protected EmployeeInfo employeeInfo;
	protected PersonInfo personVendorInfo;

	/**
	 * Date 14 April 2017 
	 * added by vijay for maintion Communication log
	 */
	protected String interactionTime;
	protected boolean communicationlogFlag;
	
	/**
	 * Dev : Rahul verma
	 * 
	 */
	@Index
	protected String interactionDueTime;
	
	/************************************************ Constructor *****************************************************/

	public InteractionType() {
		super();
		interactionTitle = "";
		interactionProject = "";
		interactionCreationDate = new Date();
		interactionDueDate = new Date();
		interactionStatus = "";
		interactionPurpose = "";
		interactionOutcome = "";
		interactionModuleName = "";
		interactionDocumentName = "";
		interactionCommunicationType = "";
		interactionCommunicationCategory = "";
		interactionPersonResponsible = "";
		interactionBranch = "";
		interactionGroup = "";
		interactionPersonType = "";
		interactionPriority="";
		personInfo = new PersonInfo();
		personVendorInfo = new PersonInfo();
		employeeInfo = new EmployeeInfo();
		interactionTime="";
		interactionDueTime="";
	}
	
	/*******************************************************************************************************************/

	/************************************* Getters And Setters ****************************************************/

	
	
	public String getInteractionPurpose() {
		return interactionPurpose;
	}

	public String getInteractionDueTime() {
		return interactionDueTime;
	}

	public void setInteractionDueTime(String interactionDueTime) {
		this.interactionDueTime = interactionDueTime;
	}

	public void setInteractionPurpose(String interactionPurpose) {
		if (interactionPurpose != null)
			this.interactionPurpose = interactionPurpose.trim();
	}

	public String getInteractionOutcome() {
		return interactionOutcome;
	}

	public void setInteractionOutcome(String interactionOutcome) {
		if (interactionOutcome != null)
			this.interactionOutcome = interactionOutcome;
	}

	public Date getInteractionDueDate() {
		return interactionDueDate;
	}

	public void setInteractionDueDate(Date interactionDueDate) {
		if(interactionDueDate!=null){
		this.interactionDueDate = interactionDueDate;
		}
	}

	public String getInteractionBranch() {
		return interactionBranch;
	}

	public void setInteractionBranch(String interactionBranch) {
		if (interactionBranch != null)
			this.interactionBranch = interactionBranch;
	}

	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		if (personInfo != null)
			this.personInfo = personInfo;
	}

	public String getInteractionTitle() {
		return interactionTitle;
	}

	public void setInteractionTitle(String interactionTitle) {
		if (interactionTitle != null)
			this.interactionTitle = interactionTitle;
	}

	public Date getinteractionCreationDate() {
		return interactionCreationDate;
	}

	public void setinteractionCreationDate(Date interactionCreationDate) {
		this.interactionCreationDate = interactionCreationDate;
	}

	public String getInteractionModuleName() {
		return interactionModuleName;
	}

	public void setInteractionModuleName(String interactionModuleName) {
		if (interactionModuleName != null)
			this.interactionModuleName = interactionModuleName;
	}

	public String getInteractionDocumentName() {
		return interactionDocumentName;
	}

	public void setInteractionDocumentName(String interactionDocumentName) {
		if (interactionDocumentName != null)
			this.interactionDocumentName = interactionDocumentName;
	}

	public String getInteractionCommunicationType() {
		return interactionCommunicationType;
	}

	public void setInteractionCommunicationType(
			String interactionCommunicationType) {
		if (interactionCommunicationType != null)
			this.interactionCommunicationType = interactionCommunicationType;
	}

	public String getInteractionCommunicationCategory() {
		return interactionCommunicationCategory;
	}

	public void setInteractionCommunicationCategory(
			String interactionCommunicationCategory) {
		if (interactionCommunicationCategory != null)
			this.interactionCommunicationCategory = interactionCommunicationCategory;
	}

	public String getInteractionPersonResponsible() {
		return interactionPersonResponsible;
	}

	public void setInteractionPersonResponsible(
			String interactionPersonResponsible) {
		if (interactionPersonResponsible != null)
			this.interactionPersonResponsible = interactionPersonResponsible;
	}

	public String getInteractionProject() {
		return interactionProject;
	}

	public void setInteractionProject(String interactionProject) {
		if (interactionProject != null)
			this.interactionProject = interactionProject;
	}

	public String getInteractionGroup() {
		return interactionGroup;
	}

	public void setInteractionGroup(String interactionGroup) {
		if (interactionGroup != null)
			this.interactionGroup = interactionGroup;
	}

	public String getInteractionStatus() {
		return interactionStatus;
	}

	public void setInteractionStatus(String interactionStatus) {
		if (interactionStatus != null)
			this.interactionStatus = interactionStatus;
	}

	public String getInteractionPersonType() {
		return interactionPersonType;
	}

	public void setInteractionPersonType(String interactionPersonType) {
		if (interactionPersonType != null)
			this.interactionPersonType = interactionPersonType;
	}

	public EmployeeInfo getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(EmployeeInfo employeeInfo) {
		if(employeeInfo != null)
			this.employeeInfo = employeeInfo;
	}

	public PersonInfo getPersonVendorInfo() {
		return personVendorInfo;
	}

	public void setPersonVendorInfo(PersonInfo personVendorInfo) {
		if (personVendorInfo != null)
			this.personVendorInfo = personVendorInfo;
	}
	

	public String getInteractionPriority() {
		return interactionPriority;
	}

	public void setInteractionPriority(String interactionPriority) {
		this.interactionPriority = interactionPriority;
	}

	public int getInteractionDocumentId() {
		return interactionDocumentId;
	}

	public void setInteractionDocumentId(int interactionDocumentId) {
		this.interactionDocumentId = interactionDocumentId;
	}

	public boolean isCommunicationlogFlag() {
		return communicationlogFlag;
	}

	public void setCommunicationlogFlag(boolean communicationlogFlag) {
		this.communicationlogFlag = communicationlogFlag;
	}
	
	public String getInteractionTime() {
		return interactionTime;
	}

	public void setInteractionTime(String interactionTime) {
		this.interactionTime = interactionTime;
	}
	
	/************************************************************************************************************/

	
	/*************************************************Overridden Method**************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/****************************************Status List Box for Search*************************************/

	public static List<String> getStatusList() {
		ArrayList<String> intstatuslist = new ArrayList<String>();
		intstatuslist.add(INTERACTIONCREATED);
		intstatuslist.add(INTERACTIONCLOSED);
		return intstatuslist;
	}
	
	


	@OnSave
	@GwtIncompatible
	public void reactOnSaveData()
	{
		if(this.getInteractionDueDate()!=null){
			setInteractionDueDate(DateUtility.getDateWithTimeZone("IST",this.getInteractionDueDate()));
		}
	}
	
	
}
