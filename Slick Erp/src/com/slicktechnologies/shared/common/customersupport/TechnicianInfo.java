package com.slicktechnologies.shared.common.customersupport;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class TechnicianInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1440373719597050281L;
	
	int employeeId;
	String employeeName;
	long empCellNumber;
	String EmployeeDesignation;
	

	public TechnicianInfo(){
		employeeName ="";
		EmployeeDesignation = "";
	}


	public int getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}


	public String getEmployeeName() {
		return employeeName;
	}


	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}


	public long getEmpCellNumber() {
		return empCellNumber;
	}


	public void setEmpCellNumber(long empCellNumber) {
		this.empCellNumber = empCellNumber;
	}


	public String getEmployeeDesignation() {
		return EmployeeDesignation;
	}


	public void setEmployeeDesignation(String employeeDesignation) {
		EmployeeDesignation = employeeDesignation;
	}
	
	
	
	
	
	
}
