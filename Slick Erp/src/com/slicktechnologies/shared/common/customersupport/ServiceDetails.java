package com.slicktechnologies.shared.common.customersupport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Serialize;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

@Embed
public class ServiceDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4089234809082733628L;
	
	int serviceId;
	String branch;
	String serviceTime;
	Date serviceDate;
	int contractId;
	@EmbedMap
	@Serialize
	protected HashMap<Integer, ArrayList<EmployeeInfo>> technicianlist;
	@EmbedMap
	@Serialize
	protected HashMap<Integer, ArrayList<TechnicianInfo>> technicians;
	@EmbedMap
	@Serialize
	protected HashMap<Integer, ArrayList<ProductGroupList>> serviceProdGroupList;
	  
	public ServiceDetails(){
		branch = "";
		serviceTime ="";
//		technicianlist = new HashMap<Integer, ArrayList<EmployeeInfo>>();
//		serviceProdGroupList = new HashMap<Integer, ArrayList<ProductGroupList>>();
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public HashMap<Integer, ArrayList<EmployeeInfo>> getTechnicianlist() {
		return technicianlist;
	}

	public void setTechnicianlist(HashMap<Integer, ArrayList<EmployeeInfo>> technicianlist) {
		this.technicianlist = technicianlist;
	}

	public HashMap<Integer, ArrayList<ProductGroupList>> getServiceProdGroupList() {
		return serviceProdGroupList;
	}

	public void setServiceProdGroupList(HashMap<Integer, ArrayList<ProductGroupList>> serviceProdGroupList) {
		this.serviceProdGroupList = serviceProdGroupList;
	}

	public HashMap<Integer, ArrayList<TechnicianInfo>> getTechnicians() {
		return technicians;
	}

	public void setTechnicians(HashMap<Integer, ArrayList<TechnicianInfo>> technicians) {
		this.technicians = technicians;
	}
	
	
	
}
