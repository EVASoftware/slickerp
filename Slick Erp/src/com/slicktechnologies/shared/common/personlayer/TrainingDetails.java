package com.slicktechnologies.shared.common.personlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
@Embed
public class TrainingDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6400775819715715465L;
	/**
	 * 
	 */
	String trainingName;
	String trainingDescription;
	Date trainingDate;
	
	public TrainingDetails() {
		// TODO Auto-generated constructor stub
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}
	
	public String getTrainingDescription() {
		return trainingDescription;
	}

	public void setTrainingDescription(String trainingDescription) {
		this.trainingDescription = trainingDescription;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	

}
