package com.slicktechnologies.shared.common.personlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
/**
 * 
 * @author Admin
 *
 */
@Embed
public class EmployeeCTCTemplate  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3232663093348270583L;
	/**
	 * 
	 */
	
	String ctcTemplateName;
	Date startDate;
	@Index
	Date endDate;
	double amount;
	
	
	
	public EmployeeCTCTemplate() {
		super();
		// TODO Auto-generated constructor stub
	}


	public EmployeeCTCTemplate(String ctcTemplateName,Date startDate,Date endDate,double amount) {
		super();
		this.ctcTemplateName = ctcTemplateName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.amount=amount;
	}

	
	/***************************Getters and Setters*****************************************/
	
	
	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String getCtcTemplateName() {
		return ctcTemplateName;
	}

	public void setCtcTemplateName(String ctcTemplate) {
		this.ctcTemplateName = ctcTemplate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
}
