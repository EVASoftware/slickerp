package com.slicktechnologies.shared.common.personlayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
@Entity
public class UploadHistory extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -57204493162009608L;
	
	
	@Index
	String documentType;
	@Index
	int documentId;
	@Index
	String documentName;
	@Index
	Date creationDate;
	DocumentUpload upload;
	@Index
	String uploadCompositeName;
	
	public UploadHistory() {
		// TODO Auto-generated constructor stub
	}
	

	public UploadHistory(String documentType,int documentId,DocumentUpload upload,String uploadCompositeName,String documentName,long companyId,String createdBy) {
		// TODO Auto-generated constructor stub
		creationDate=new Date();
		this.documentType=documentType;
		this.documentId=documentId;
		this.upload=upload;
		this.uploadCompositeName=uploadCompositeName;
		this.documentName=documentName;
		this.companyId=companyId;
		this.createdBy=createdBy;
		
	}
	
	
	public String getDocumentType() {
		return documentType;
	}





	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}





	public int getDocumentId() {
		return documentId;
	}





	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}





	public Date getCreationDate() {
		return creationDate;
	}





	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}





	public DocumentUpload getUpload() {
		return upload;
	}





	public void setUpload(DocumentUpload upload) {
		this.upload = upload;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
