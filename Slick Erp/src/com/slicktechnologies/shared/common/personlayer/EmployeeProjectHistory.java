package com.slicktechnologies.shared.common.personlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class EmployeeProjectHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5092774787456138229L;
	/**
	 * 
	 */
	
	String projectName;
	Date projectStartDate;
	Date projectEndDate;
	int projectId;
	
	/**
	 * Date : 13-08-2018 BY ANIL
	 * Adding Project Code 
	 */
	String projectCode;
	
	/**
	 * @author Anil
	 * @since 02-09-2020
	 * EnableSiteLocation
	 */
	String siteLocation;
	
	

	/******************************Getters and Setters************************/

	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Date getProjectStartDate() {
		return projectStartDate;
	}
	public void setProjectStartDate(Date projectStartDate) {
		this.projectStartDate = projectStartDate;
	}
	public Date getProjectEndDate() {
		return projectEndDate;
	}
	public void setProjectEndDate(Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getSiteLocation() {
		return siteLocation;
	}
	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}
	


}
