package com.slicktechnologies.shared.common.personlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class PromotionDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8828488649047820910L;
	
	String previousDesignation;
	double previousSalary;
	String currentDesignation;
	double currentSalary;
	Date dateOfPromotion;
	String promotedBy;
	
	public PromotionDetails() {
		previousDesignation="";
		currentDesignation="";
		promotedBy="";
	}

	public String getPreviousDesignation() {
		return previousDesignation;
	}

	public void setPreviousDesignation(String previousDesignation) {
		this.previousDesignation = previousDesignation;
	}

	public double getPreviousSalary() {
		return previousSalary;
	}

	public void setPreviousSalary(double previousSalary) {
		this.previousSalary = previousSalary;
	}

	public String getCurrentDesignation() {
		return currentDesignation;
	}

	public void setCurrentDesignation(String currentDesignation) {
		this.currentDesignation = currentDesignation;
	}

	public double getCurrentSalary() {
		return currentSalary;
	}

	public void setCurrentSalary(double currentSalary) {
		this.currentSalary = currentSalary;
	}

	public Date getDateOfPromotion() {
		return dateOfPromotion;
	}

	public void setDateOfPromotion(Date dateOfPromotion) {
		this.dateOfPromotion = dateOfPromotion;
	}

	public String getPromotedBy() {
		return promotedBy;
	}

	public void setPromotedBy(String promotedBy) {
		this.promotedBy = promotedBy;
	}
	
	
	
	

}
