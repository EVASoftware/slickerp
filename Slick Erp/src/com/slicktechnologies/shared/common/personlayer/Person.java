package com.slicktechnologies.shared.common.personlayer;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;


/**
 * Represents Person.
 */
@SuppressWarnings("serial")
@Embed
public class Person extends SuperModel
{
	/***********************************************Entity Attributes****************************************************/
	/** The first name of Person. */
	@Ignore 
	protected String firstName;
	
	/** The middle name of Person. */
	@Ignore 
	protected String middleName;
	
	/** The last name. of Person*/
	@Ignore 
	protected String lastName;
	
	/** The full name of person ,this will get actually saved in Database */
	@Index 
	protected String fullname;
	
	/** The gender. of Person */
	@Index
	protected String gender;
	
	/** The marital status of Person */
	protected String maritalStatus;
	
	/** 	Vector of Contact as One Person can have many Contact */
	@Index
	protected List<Contact> contacts;
	
	/** The date of birth of Person */
	protected Date birthDate;
	
	/** The social info. */
	protected SocialInformation socialInfo;
	
	
	protected Company personWorksForCompany;

	
	
	/***********************************************Default Ctor***************************************/
	/**
	 * Instantiates a new person.
	 */
	public Person()
	{
		contacts=new Vector<Contact>();
		socialInfo=new SocialInformation();
		firstName="";
		middleName="";
		lastName="";
		fullname="";
		gender="";
		maritalStatus="";


	

	}

	/***********************************************Getter/Setter******************************/
	/**
	 * Gets the first name.
	 * @return the first name
	 */
	public String getFirstName() 
	{
		return firstName;
	}

	/**
	 * Sets the first name.
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) 
	{
		if(firstName!=null)
		{
		firstName=firstName.trim();
		this.firstName = firstName;
		}
	}
	/**
	 * Gets the middle name.
	 * @return the middle name
	 */
	public String getMiddleName() 
	{	
		return middleName;
	}
	/**
	 * Sets the middle name.
	 * @param middleName the new middle name
	 */
	public void setMiddleName(String middleName) 
	{
		if(middleName!=null)
		{
		    this.middleName=middleName.trim();
			this.middleName = middleName;	   
		}
	}
	/**
	 * Gets the last name.
	 * @return the last name
	 */
	public String getLastName() 
	{
		return lastName;
	}
	/**
	 * Sets the last name
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) 
	{
		if(this.lastName!=null)
		{
		    this.lastName=lastName.trim();
			this.lastName = lastName;
		}
	}
	
	/**
	 * Gets the fullname
	 * @return the fullname
	 */
	public String getFullname()
	{
		return fullname.trim();
	}

	/**
    * Sets the fullname.
    */
   public void setFullname() 
   {
		fillFullName();
	}
	/**
	 * Gets the dob.
	 * @return the dob
	 */
	public Date getDob()
	{
		return birthDate;
	}
	/**
	 * Sets the dob.
	 * @param dob the new dob
	 */
	public void setDob(Date dob)
	{
		this.birthDate = dob;
	}
	/**
	 * Gets the social info.
	 * @return the social info
	 */
	public SocialInformation getSocialInfo()
	{
		return socialInfo;
	}
	/**
	 * Sets the social info.
	 * @param socialInfo the new social info
	 */
	public void setSocialInfo(SocialInformation socialInfo) 
	{
		this.socialInfo = socialInfo;
	}
	/**
	 * Gets the gender.
	 * @return the gender
	 */
	public String getGender()
	{
		return gender;
	}
	/**
	 * Sets the gender.
	 * @param gender the new gender
	 */
	public void setGender(String gender)
	{
		if(gender!=null)
			this.gender = gender.trim();
	}
	/**
	 * Gets the marital status.
	 * @return the marital status
	 */
	public String getMaritalStatus() 
	{
		return maritalStatus;
	}
	/**
	 * Sets the marital status.
	 *
	 * @param maritalStatus the new marital status
	 */
	public void setMaritalStatus(String maritalStatus) 
	{
		if(maritalStatus!=null)
			this.maritalStatus = maritalStatus.trim();
	}
	/**
	 * Gets the contact.
	 * @return the contact
	 */
	public List<Contact> getContact() 
	{
		return contacts;
	}
	
	

	/**
	 * Sets the contact.
	 * @param contact the new contact
	 */
	public void setContact(List<Contact> contact)
	{
		this.contacts = contact;
	}
	
	public List<Contact> getContacts() 
	{
		return contacts;
	}
	public void setContacts(List<Contact> contacts)
	{
		this.contacts = contacts;
	}
	
	
	/**
	 * Fill full name.Fill person's entire name
	 */
	public void fillFullName()
	{
		if(middleName.trim().equals(""))
			fullname=firstName+" "+lastName;
		else
		   fullname=firstName+" "+middleName+" "+lastName;
	}
	
	 /**
 	 * Fill all names. Fill person's first ,middle & last name.
 	 */
 	public void fillAllNames()
	{
		String[]names=fullname.split(" ");
		if(names.length==3)
		{
			firstName=names[0];
			middleName=names[1];
			lastName=names[2];
		}
		if(names.length==2)
		{
			firstName=names[0];
			lastName=names[1];
		}
	}
	/**
	 * Sets the fullname.
	 *
	 * @param fullname the new fullname
	 */
	public void setFullname(String fullname)
	{
		this.fullname=fullname.trim();
		
	}
	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.common.helperlayer.SuperModel#isDuplicate(com.simplesoftwares.shared.common.helperlayer.SuperModel)
	 */
	public boolean isDuplicate(SuperModel model)
	{
		{
			Person per=(Person) model;
			String name = per.getFullname().trim();
			name=name.replaceAll("\\s","");
			name=name.toLowerCase();
			
			String curname=this.fullname.trim();
			curname=curname.replaceAll("\\s","");
			curname=curname.toLowerCase();
			//New Object is being added
			if(per.id==null)
			{
				if(name.equals(curname))
					return true;
				else
					return false;
			}
			// Old object is being updated
			else
			{
				if(per.id.equals(id))
					return false;
				else if (name.equals(curname))
					return true;
				
				else
					return false;		
			}
	    }
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
	    if(obj instanceof Person)
	    {
	    	Person pe=(Person) obj;
	    	if(id==null||pe.id==null)
	    		return false;
	    	if(pe.id.equals(id))
	    		return true;
	    	else
	    		return false;
	    }
	    return false;
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(SuperModel o) {
		Person p=(Person) o;
		String name=p.getFullname();
		
		return fullname.compareTo(name);
	}

	/**************************************************Relation Managment Part**********************************************/
	
	public void onSave()
	{
		fillFullName();
	}
	

	public void onLoad()
	{
		fillAllNames();
	}
	
	
	/**************************************************************************************************************/

	
}
