package com.slicktechnologies.shared.common.personlayer;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.humanresourcelayer.LeaveAllocationServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.arrears.ArrearsDetails;
import com.slicktechnologies.shared.common.arrears.ArrearsInfoBean;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EducationalInfo;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PassportInformation;
import com.slicktechnologies.shared.common.helperlayer.PreviousCompanyHistory;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

/**
 * Represents Employee of the Software's user company.
 */
@Entity
@Embed
@Cache
public class Employee extends Person implements Serializable
{
	/***********************************************Entity Attributes*********************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7126833574273951975L;

	/** monthly Working Hrs of the employee */
	protected double monthlyWorkingHrs;
	/** salary of the employee */
	protected double salary;
	/** joining date*/
	@Index
	protected Date joinedAt;
	/** Photo of Employee **/
	protected DocumentUpload photo;
	/** Employee Status */
	@Index
	protected boolean status;
	/** The branch name of employee*/
	@Index
	protected String branchName;
		/** The designation of employee  */
	@Index
     protected String designation;
    /** The Name of Role*/
	@Index
	protected String roleName;

	/** The ID Card Number*/
	@Index
	protected String idCardNo;
	/** The Acess Card Number*/
	@Index
	protected String accessCardNo;
	/** Passport Number*/
	@Index
	protected String departMent;
	/** name of employee to whom this employee reports **/
	@Index
	protected String reportsTo;
	
	/** Array List of Leave Approvers **/
	protected  ArrayList<String>approvers;
	
	/** Array List of Previous Work History **/
	protected ArrayList<PreviousCompanyHistory>previousCompanyHistory;
	/** Array List of Educational Information **/
	protected ArrayList<EducationalInfo>educationalInfo;
	
	/** Array List of Branch Information **/
	protected List<EmployeeBranch> empBranchList;
	
	/** Passport Information **/
	protected PassportInformation information;
	
	/**Type of Employee **/
	@Index
	protected String employeeType;
	
	protected String employeeBankAccountNo;
	
	protected String employeeBankName;
	
	protected String employeeESICcode;
	
	protected String employeePanNo;
	
	protected String ifscCode;
	
	protected String bankBranch;
	
	protected String accountNo;
	
	protected String ppfNo;
	protected String bloodGroup;
	protected ArrayList<ArticleType> articleTypeDetails;
	
	/**Country of Employee **/
	@Index
	protected String country;
	

	/** Address Details of Employee **/
	protected DocumentUpload uploadAddDet;
	
	/** Experience Details of Employee **/
	protected DocumentUpload uploadExpDet;
	
	/** Educational Details of Employee **/
	protected DocumentUpload uploadEduDet;
	
	
	/**
	 * rohan added this for NBHC employee approvable process 
	 */
	protected String approverName;
	protected String approveStatus;
	protected double salaryAmt;
	
	/**
	 * ends here 
	 */
	
	/**
	 * Date : 15-12-2018 By ANIL
	 * added number range process for employee id
	 */
	@Index
	protected String numberRange;
/***********************************************Relational Attributes****************************************/
	
	protected Key<Config>keyRole;
	protected Key<Config>keyDesignation;
	protected Key<BranchRelation>keyBranchName;
/***********************************************Join Kind Attribute******************************************/
	@Ignore
	protected boolean nameChanged=false;
	@Index
	protected Key<EmployeeRelation> keyName;

	protected UserRole role;
	
	
	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}
	
	public String getUserRoleName() {
		return role.getRoleName();
	}

	public void setUserRoleName(String roleName) {
		role.setRoleName(roleName);
	}



	protected String dailyRoute;
	
	/**
	 * Date : 22-09-2018 Merged by Anil
	 */
	
	/** Instantiates a new employee */
	/**
	 * Secondary address added by Rahul Verma for Employee on 22 June 2018
	 */
	protected Address secondaryAddress;
	/**
	 * Rahul added for employee project history
	 */
	ArrayList<EmployeeProjectHistory> employeeProjectHistoryList;
	
	/**
	 * Rahul added for employee Name
	 */
	@Index
	String refEmployee;
	@Index
	String maritalStatus;
	@Index
	String UANno;
	
	/**
	 * Rahul added on 16th July 2018
	 */
	@Index
	ArrayList<EmployeeCTCTemplate> employeeCTCList;
	
	
	/**
	 * Date : 20-07-2018 BY ANIL
	 */
	@Index
	String projectName;
	
	@Index
	boolean isResigned;
	
	@Index
	Date resignationDate;
	
	@Index
	Date lastWorkingDate;
	/** date 24.7.2018 added by komal **/
	@Index
	double noticePeriod;
	@Index
	boolean isTerminated;
	@Index
	Date terminationDate;
	@Index
	boolean isAbsconded;
	@Index
	Date abscondDate;
	@Index
	ArrayList<EmployeeAssetBean> checkList;
	@Index
	Double shortfall;
	@Index
	Double waivedOff;
	@Index
	CheckListType checkListType;
	@Index
	boolean isRealeaseForPayment;
	@Index
	boolean isOneTimeDeduction;
	@Index
	double oneTimeDeductionAmount;
	/**
	 * end komal
	 */
	
	/**
	 * Date : 13-08-2018 By ANIL
	 */
	ArrayList<TrainingDetails> trainingInfoList;
	
	/**
	 * Date : 14-08-2018 By ANIL
	 * Following fields are added as directed by Nitin Sir
	 * for printing Sasha Bio data and storing happay card details
	 * also for registering finger
	 */
	@Index
	String interviewedBy;
	@Index 
	String salaryAuthorizedBy;
	
	String nomineeName;
	int nomineeAge;
	String nomineeRelation;
	String nomineeAddress;
	
	@Index 
	String shiftName;
	
	@Index
	String happayCardUserId;
	@Index 
	String happayCardUserName;
	@Index
	String happayCardWalletNumber;
	
	@Index
	boolean isFingerRegistered;
	String fingerRegisteredBy;
	Date fingerRegisterationDate;
	
	/**
	 * End
	 */
	
	
	
	/**
	 * End
	 */
	
	/**
	 * Rahul added this on 15th Aug 2018 (Independance Day hai AAj)
	 * 
	 */
	@Index
	boolean fingerDataPresent;
	
	
	/**
	 * Date : 20-08-2018 By ANIL
	 */
	@Index
	long aadharNumber;
	String shiftFromTime;
	String shiftToTime;
	
	
	/**
	 * Date : 11-09-2018 By ANIL
	 * Added the flag that capture whether it is old PF/ESIC number
	 */
	@Index
	boolean isPreviousPfNumber;
	@Index
	boolean isPreviousEsicNumber;
	
	/** Date : 14-09-2018 By KOmal**/
	@Index
	Date fnfMonth;
	
	/**
	 * Date : 18-09-2018 BY ANIL
	 */
	@Index
	boolean isSkilled;
	@Index
	boolean isSemiSkilled;
	@Index
	boolean isUnskilled;
	@Index
	String lanuageSpoken;
	/** Added by viraj Date 11-10-2018 for orion **/
	String husbandName;
	/** End **/
	/**
	 * End
	 */
	
	protected DocumentUpload uploadSign;

	/** Instantiates a new employee */
	/**
	 * Rahul Verma added on 13 Oct 2018
	 * Description : For outstation Employee
	 */
	boolean outstationEmployee;
	/** date 25.10.2018 added by komal for reason for leaving **/
	private String reasonForLeaving;

		/**
	 * Date : 19-10-2018 By ANIL
	 */
	@Index
	ArrayList<PromotionDetails> promotionHistoryList;
	
	@Index
	Date dateOfRetirement;
	
	/**
	 * Date : 10-11-2018 BY ANIL
	 * added upload option for compliance form and other
	 */
	
	DocumentUpload documentUpload1;
	DocumentUpload documentUpload2;
	
	/**
	 * Date 14-11-2018 By ANIL:
	 * Capturing person disability with percentage 
	 */
	@Index
	boolean isDisabled;
	double disablityPercentage;
	DocumentUpload uploadDisabilityDoc;
	String disabiltyRemark;
	
	/**
	 * Date : 13-12-2018 BY ANIL
	 */
	@Index 
	String updatedBy;
	@Index
	Date updateDate;
	
	
	/**
	 * @author Anil , Date : 07-01-2018 
	 * Adding scheme certificate number and pension payment order number for printing 
	 * on Form 11 PF declaration Form 
	 * By Nitin Sir
	 */
	String schemeCertificateNum;
	String pensionPaymentNum;
	
	/**
	 * @author Anil,Date : 28-01-2019
	 * Storing nomination details
	 */
	
	ArrayList<EmployeeFamilyDeatails> nominationList;
	
	/**
	 * @author Anil , Date : 10-04-2019
	 */
	@Index
	String empGrp;
	
	/**
	 * @author Anil , Date : 23-04-2019
	 * Added FnF date
	 */
	
	@Index
	Date fnfDate;
	
	/**
	 * @author Anil , Date : 24-05-2019
	 * fnf status
	 */
	@Index
	String fnfStatus;
	
	
	/**
	 * @author Anil , Date : 18-07-2019
	 */
	@Index
	Date creationDate;
	
	/**
	 * @author Anil , Date : 13-08-2019
	 */
	boolean updatePfNum;
	
		/**Amol ,Date 1-10-2019
	 * added cast feild**/
	@Index
	 String caste;
	 
	 /**
	 * @author Anil , Date : 05-10-2019
	 * for reliever employee we will process only single payroll despite of  multiple site attendance
	 * For Sasha
	 */
	@Index
	boolean isReliever;
	
	/**
	 * @author Anil
	 * @since 30-07-2020
	 * For Sun rise raised by rahul tiwari
	 */
	protected DocumentUpload policeVerfDoc;
	@Index
	Date expiryDate;
	
	/**
	 * @author Anil
	 * @since 02-09-2020
	 * site location for sun facility
	 */
	@Index
	String siteLocation;
	
	// Added By Priyanka
	@Index
	protected String employeeName;
		
	/** The user Id. */
	@Index protected String userName;
	
	
	/** The password. */
	@Index protected String password;
    
    
	/** The employeerole. */
	protected String employeerole;
	
	 @Index
    String email;
	 
	 /**
	  * @author Ashwini Patil
	  * @since 23-03-2023
	  * As per orion requirement
	  */
	protected double incentivePercent;
	
	protected String overtimeName; //Ashwini Patil Date:25-08-2023
	protected String calendar; //Ashwini Patil Date:25-08-2023
	protected String leaveGroup; //Ashwini Patil Date:25-08-2023
	protected String ctcTemplate; //Ashwini Patil Date:25-08-2023
	

	// End
	public Employee()
	{
	    super();
		photo=new DocumentUpload();
		Contact temp=new Contact();
		contacts.add(temp);
		branchName="";
		designation="";
		roleName="";
		nameChanged=false;
		departMent="";
		reportsTo="";
		previousCompanyHistory=new ArrayList<PreviousCompanyHistory>();
		educationalInfo=new ArrayList<EducationalInfo>();
		empBranchList = new ArrayList<EmployeeBranch>();
		information=new PassportInformation();
		employeeType="";
		country="";
		idCardNo="";
		accessCardNo="";
		articleTypeDetails=new ArrayList<ArticleType>();
		employeeBankAccountNo="";
		
		employeeBankName="";
		
		 employeeESICcode="";
		
		employeePanNo="";
		
		
		
		ifscCode="";
		
		 bankBranch="";
		
		accountNo="";
		
		ppfNo="";
		bloodGroup="";
		approverName = "";
		approveStatus = "";
		dailyRoute="";
		
		numberRange="";
		
		secondaryAddress=new Address();
		employeeProjectHistoryList=new ArrayList<EmployeeProjectHistory>();
		employeeCTCList=new ArrayList<EmployeeCTCTemplate>();
		refEmployee="";
		maritalStatus="";
		UANno="";
		
		projectName="";
		/** date 18.7.2018 added by komal to store resignation deatils**/
		isResigned = false;
		checkListType = new CheckListType();
		checkList = new ArrayList<EmployeeAssetBean>();
		isTerminated = false;
		isAbsconded = false;
		isRealeaseForPayment = false;
		isOneTimeDeduction = false;
		
		trainingInfoList=new ArrayList<TrainingDetails>();
		
		
		interviewedBy="";
		salaryAuthorizedBy="";
		
		nomineeName="";           
		nomineeRelation="";       
		nomineeAddress="";        
		                       
		shiftName="";             
		                       
		happayCardUserId="";      
		happayCardUserName="";    
		happayCardWalletNumber="";
		
		isFingerRegistered=false;   
		fingerRegisteredBy="";   
		
		fingerDataPresent=false;
		
		shiftFromTime="";
		shiftToTime="";
		
		isSkilled=false;
		isSemiSkilled=false;
		isUnskilled=false;
		lanuageSpoken="";
		
		uploadSign=new DocumentUpload();

		outstationEmployee=false;
		reasonForLeaving = "";
		promotionHistoryList=new ArrayList<PromotionDetails>();
		
		documentUpload1=new DocumentUpload();
		documentUpload2=new DocumentUpload();
		
		isDisabled=false;
		uploadDisabilityDoc=new DocumentUpload();
		disabiltyRemark="";
		
		schemeCertificateNum="";
		pensionPaymentNum="";
			caste="";
//		nominationList=new ArrayList<EmployeeFamilyDeatails>();
		updatePfNum=false;
		incentivePercent=0;
		overtimeName="";
		calendar="";
		leaveGroup="";
		ctcTemplate="";
	}
	
	
	
	
	
	
	public boolean isUpdatePfNum() {
		return updatePfNum;
	}





	public void setUpdatePfNum(boolean updatePfNum) {
		this.updatePfNum = updatePfNum;
	}





	public Date getCreationDate() {
		return creationDate;
	}





	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}





	public String getFnfStatus() {
		return fnfStatus;
	}





	public void setFnfStatus(String fnfStatus) {
		this.fnfStatus = fnfStatus;
	}





	public Date getFnfDate() {
		return fnfDate;
	}





	public void setFnfDate(Date fnfDate) {
		this.fnfDate = fnfDate;
	}





	public String getEmpGrp() {
		return empGrp;
	}



	public void setEmpGrp(String empGrp) {
		this.empGrp = empGrp;
	}



	public ArrayList<EmployeeFamilyDeatails> getNominationList() {
		return nominationList;
	}



	public void setNominationList(List<EmployeeFamilyDeatails> nominationList) {
		ArrayList<EmployeeFamilyDeatails> list=new ArrayList<EmployeeFamilyDeatails>();
		list.addAll(nominationList);
		this.nominationList = list;
	}



	public String getSchemeCertificateNum() {
		return schemeCertificateNum;
	}



	public void setSchemeCertificateNum(String schemeCertificateNum) {
		this.schemeCertificateNum = schemeCertificateNum;
	}



	public String getPensionPaymentNum() {
		return pensionPaymentNum;
	}



	public void setPensionPaymentNum(String pensionPaymentNum) {
		this.pensionPaymentNum = pensionPaymentNum;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		if(updatedBy!=null&&!updatedBy.equals("")){
			updateDate=new Date();
//			setUpdateDate(DateUtility.getDateWithTimeZone("IST", new Date()));
		}
		this.updatedBy = updatedBy;
	}



	public Date getUpdateDate() {
		return updateDate;
	}



	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}



	public boolean isDisabled() {
		return isDisabled;
	}



	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}



	public double getDisablityPercentage() {
		return disablityPercentage;
	}



	public void setDisablityPercentage(double disablityPercentage) {
		this.disablityPercentage = disablityPercentage;
	}



	public DocumentUpload getUploadDisabilityDoc() {
		return uploadDisabilityDoc;
	}



	public void setUploadDisabilityDoc(DocumentUpload uploadDisabilityDoc) {
		this.uploadDisabilityDoc = uploadDisabilityDoc;
	}



	public String getDisabiltyRemark() {
		return disabiltyRemark;
	}



	public void setDisabiltyRemark(String disabiltyRemark) {
		this.disabiltyRemark = disabiltyRemark;
	}



	public DocumentUpload getDocumentUpload1() {
		return documentUpload1;
	}





	public void setDocumentUpload1(DocumentUpload documentUpload1) {
		this.documentUpload1 = documentUpload1;
	}





	public DocumentUpload getDocumentUpload2() {
		return documentUpload2;
	}





	public void setDocumentUpload2(DocumentUpload documentUpload2) {
		this.documentUpload2 = documentUpload2;
	}
	

	public boolean isOutstationEmployee() {
		return outstationEmployee;
	}




	public void setOutstationEmployee(boolean outstationEmployee) {
		this.outstationEmployee = outstationEmployee;
	}

		public Date getDateOfRetirement() {
		return dateOfRetirement;
	}





	public void setDateOfRetirement(Date dateOfRetirement) {
		this.dateOfRetirement = dateOfRetirement;
	}





	public ArrayList<PromotionDetails> getPromotionHistoryList() {
		return promotionHistoryList;
	}





	public void setPromotionHistoryList(List<PromotionDetails> promotionHistoryList) {
		ArrayList<PromotionDetails> list=new ArrayList<PromotionDetails>();
		list.addAll(promotionHistoryList);
		this.promotionHistoryList = list;
	}
	
	/** Added by viraj Date 11-10-2018 **/
	public String getHusbandName() {
		return husbandName;
	}
	
	public void setHusbandName(String husbandName) {
		this.husbandName = husbandName;
	}
	/** End **/	
	
	public DocumentUpload getUploadSign() {
		return uploadSign;
	}



	public void setUploadSign(DocumentUpload uploadSign) {
		this.uploadSign = uploadSign;
	}



	public String getLanuageSpoken() {
		return lanuageSpoken;
	}



	public void setLanuageSpoken(String lanuageSpoken) {
		this.lanuageSpoken = lanuageSpoken;
	}



	public boolean isSkilled() {
		return isSkilled;
	}



	public void setSkilled(boolean isSkilled) {
		this.isSkilled = isSkilled;
	}



	public boolean isSemiSkilled() {
		return isSemiSkilled;
	}



	public void setSemiSkilled(boolean isSemiSkilled) {
		this.isSemiSkilled = isSemiSkilled;
	}



	public boolean isUnskilled() {
		return isUnskilled;
	}



	public void setUnskilled(boolean isUnskilled) {
		this.isUnskilled = isUnskilled;
	}



	public Date getFnfMonth() {
		return fnfMonth;
	}



	public void setFnfMonth(Date fnfMonth) {
		this.fnfMonth = fnfMonth;
	}



	public boolean isPreviousPfNumber() {
		return isPreviousPfNumber;
	}



	public void setPreviousPfNumber(boolean isPreviousPfNumber) {
		this.isPreviousPfNumber = isPreviousPfNumber;
	}



	public boolean isPreviousEsicNumber() {
		return isPreviousEsicNumber;
	}



	public void setPreviousEsicNumber(boolean isPreviousEsicNumber) {
		this.isPreviousEsicNumber = isPreviousEsicNumber;
	}



	public String getShiftFromTime() {
		return shiftFromTime;
	}



	public void setShiftFromTime(String shiftFromTime) {
		this.shiftFromTime = shiftFromTime;
	}



	public String getShiftToTime() {
		return shiftToTime;
	}



	public void setShiftToTime(String shiftToTime) {
		this.shiftToTime = shiftToTime;
	}



	public long getAadharNumber() {
		return aadharNumber;
	}



	public void setAadharNumber(long aadharNumber) {
		this.aadharNumber = aadharNumber;
	}



	public boolean isFingerDataPresent() {
		return fingerDataPresent;
	}

	public void setFingerDataPresent(boolean fingerDataPresent) {
		this.fingerDataPresent = fingerDataPresent;
	}
	
	public String getInterviewedBy() {
		return interviewedBy;
	}

	public void setInterviewedBy(String interviewedBy) {
		this.interviewedBy = interviewedBy;
	}

	public String getSalaryAuthorizedBy() {
		return salaryAuthorizedBy;
	}

	public void setSalaryAuthorizedBy(String salaryAuthorizedBy) {
		this.salaryAuthorizedBy = salaryAuthorizedBy;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public int getNomineeAge() {
		return nomineeAge;
	}

	public void setNomineeAge(int nomineeAge) {
		this.nomineeAge = nomineeAge;
	}

	public String getNomineeRelation() {
		return nomineeRelation;
	}
	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}
	public String getNomineeAddress() {
		return nomineeAddress;
	}
	public void setNomineeAddress(String nomineeAddress) {
		this.nomineeAddress = nomineeAddress;
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public String getHappayCardUserId() {
		return happayCardUserId;
	}
	public void setHappayCardUserId(String happayCardUserId) {
		this.happayCardUserId = happayCardUserId;
	}
	public String getHappayCardUserName() {
		return happayCardUserName;
	}
	public void setHappayCardUserName(String happayCardUserName) {
		this.happayCardUserName = happayCardUserName;
	}
	public String getHappayCardWalletNumber() {
		return happayCardWalletNumber;
	}
	public void setHappayCardWalletNumber(String happayCardWalletNumber) {
		this.happayCardWalletNumber = happayCardWalletNumber;
	}
	public boolean isFingerRegistered() {
		return isFingerRegistered;
	}
	public void setFingerRegistered(boolean isFingerRegistered) {
		this.isFingerRegistered = isFingerRegistered;
	}
	public String getFingerRegisteredBy() {
		return fingerRegisteredBy;
	}
	public void setFingerRegisteredBy(String fingerRegisteredBy) {
		this.fingerRegisteredBy = fingerRegisteredBy;
	}
	public Date getFingerRegisterationDate() {
		return fingerRegisterationDate;
	}
	public void setFingerRegisterationDate(Date fingerRegisterationDate) {
		this.fingerRegisterationDate = fingerRegisterationDate;
	}
	public ArrayList<TrainingDetails> getTrainingInfoList() {
		return trainingInfoList;
	}
	public void setTrainingInfoList(List<TrainingDetails> trainingInfoList) {
		ArrayList<TrainingDetails> list=new ArrayList<TrainingDetails>();
		list.addAll(trainingInfoList);
		this.trainingInfoList = list;
	}

	public Date getLastWorkingDate() {
		return lastWorkingDate;
	}



	public void setLastWorkingDate(Date lastWorkingDate) {
		this.lastWorkingDate = lastWorkingDate;
	}



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public ArrayList<EmployeeCTCTemplate> getEmployeeCTCList() {
		return employeeCTCList;
	}




	public void setEmployeeCTCList(List<EmployeeCTCTemplate> employeeCTCList) {
		ArrayList<EmployeeCTCTemplate> employeeProjectArrayList=new ArrayList<EmployeeCTCTemplate>();
		employeeProjectArrayList.addAll(employeeCTCList);
		this.employeeCTCList = employeeProjectArrayList;
	}
	
	
	public String getPpfNo() {
		return ppfNo;
	}




	public void setPpfNo(String ppfNo) {
		this.ppfNo = ppfNo;
	}




	public String getUANno() {
		return UANno;
	}




	public void setUANno(String uANno) {
		UANno = uANno;
	}




	public String getRefEmployee() {
		return refEmployee;
	}




	public void setRefEmployee(String refEmployee) {
		this.refEmployee = refEmployee;
	}




	public String getMaritalStatus() {
		return maritalStatus;
	}




	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}




	public ArrayList<EmployeeProjectHistory> getEmployeeProjectHistoryList() {
		return employeeProjectHistoryList;
	}




	public void setEmployeeProjectHistoryList(List<EmployeeProjectHistory> employeeProjectHistoryList) {
		ArrayList<EmployeeProjectHistory> employeeProjectList=new ArrayList<EmployeeProjectHistory>();
		employeeProjectList.addAll(employeeProjectHistoryList);
		this.employeeProjectHistoryList = employeeProjectList;
	}




	public Address getSecondaryAddress() {
		return secondaryAddress;
	}




	public void setSecondaryAddress(Address secondaryAddress) {
		this.secondaryAddress = secondaryAddress;
	}
	

	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}


	/**
	 * Gets the salary.
	 *
	 * @return the salary
	 */
	public Double getSalary() {
		return salary;
	}
	
	

	/**
	 * Sets the salary.
	 *
	 * @param salary the new salary
	 */


	/**
	 * Gets the joined at.
	 *
	 * @return the joined at
	 */
	
	public Date getJoinedAt() {
		return joinedAt;
	}
	
	public String getFullName()
	{
		return this.fullname.trim();
	}

	/**
	 * Sets the joined at.
	 *
	 * @param joinedAt the new joined at
	 */
	public void setJoinedAt(Date joinedAt) {
		this.joinedAt = joinedAt;
	}
	/**
	 * Gets the photo.
	 *
	 * @return the photo
	 */
	public DocumentUpload getPhoto() {
		return photo;
	}
	
	public PassportInformation getPassPortInformation()
	{
		return this.information;
	}
	
	public void setPassPortInformation(PassportInformation information)
	{
		this.information=information;
	}

	/**
	 * Sets the photo.
	 *
	 * @param photo the new photo
	 */
	public void setPhoto(DocumentUpload photo) {
		this.photo = photo;
	}

	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = true, 
			title = "Employee Status")
	public Boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the branch name.
	 *
	 * @return the branch name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, 
			title = "Branch")
	public String getBranchName() {
		return branchName;
	}

	/**
	 * Sets the branch name.
	 *
	 * @param branchName the new branch name
	 */
	public void setBranchName(String branchName) {
		if(branchName!=null)
			this.branchName = branchName.trim();
	}

	/**
	 * Gets the designation.
	 *
	 * @return the designation
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true,
			title = "Employee Designation")
	public String getDesignation() {
		return designation;
	}

	/**
	 * Sets the designation.
	 *
	 * @param designation the new designation
	 */
	public void setDesignation(String designation) {
		if(designation!=null)
			this.designation = designation.trim();
	}

	/**
	 * Gets the role name.
	 *
	 * @return the role name
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Sets the role name.
	 *
	 * @param roleName the new role name
	 */
	public void setRoleName(String roleName) 
	{
		if(roleName!=null)
			this.roleName = roleName.trim();
	}

	public Long getLandline()
	{
		return this.contacts.get(0).getLandline();
	}

	public void setLandline(Long landline)
	{
		if(landline!=null)
			contacts.get(0).setLandline(landline);
		else
			contacts.get(0).setLandline(0l);
	}


	public void setEmail(String email)
	{
		if(email!=null)
			this.contacts.get(0).setEmail(email.trim());
	}

	public String getEmail()
	{
		return this.contacts.get(0).getEmail();
	}

	public Long getLandLine(Long landlineNo)
	{
		return this.contacts.get(0).getLandline();

	}

	public void setLandLine(Long landline)
	{
		if(landline!=null) 
			this.contacts.get(0).setLandline(landline);
	}

	public void setWebsite(String website)
	{
		this.contacts.get(0).setWebsite(website);
	}

	public String getWebsite()
	{
		return this.contacts.get(0).getWebsite();
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, 
			title = "Phone")
	public  Long getCellNumber1()
	{
		return contacts.get(0).getCellNo1();
	}

	public  void setCellNumber1(Long cellNuber1)
	{
		if(cellNuber1!=null)  
			contacts.get(0).setCellNo1(cellNuber1);
	}

	public  Long getCellNumber2()
	{
		return contacts.get(0).getCellNo2();
	}

	public  void setCellNumber2(Long cellNuber2)
	{
		if(cellNuber2!=null)  
			contacts.get(0).setCellNo2(cellNuber2);
	}

	public  long getFaxNumber()
	{
		return contacts.get(0).getFaxNo();
	}

	public  void setFaxNumber(Long faxno)
	{
		if(faxno!=null)    
			contacts.get(0).setFaxNo(faxno);
	}

	public void setAddress(Address address)
	{
		this.contacts.get(0).setAddress(address);
	}

	public Address getAddress()
	{
		return contacts.get(0).getAddress();
	}

	public void setSalary(Double salary) 
	{
		if(salary!=null)
			this.salary = salary;
		else
			this.salary=0;
	}
	

	
	

	@Override
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, 
	title = "Name")
	public String getFullname() {
		
	/* 
	 * bellow old code commented by vijay to change employee first middle and last to fullName 
	 */
//		fillFullName();
		
		return super.getFullname().trim();
	}

	@Override
	public void setFirstName(String firstName)
	{
		
		if(firstName==null)
			return;
		if(!(this.firstName.equals(firstName)))
			nameChanged=true;
		super.setFirstName(firstName);	
	}

	public ArrayList<ArticleType> getArticleTypeDetails() {
		return articleTypeDetails;
	}

	public void setArticleTypeDetails(ArrayList<ArticleType> articleTypeDetails) {
		ArrayList<ArticleType> arrOfArticle=new ArrayList<ArticleType>();
		arrOfArticle.addAll(articleTypeDetails);
		this.articleTypeDetails = arrOfArticle;
	}


	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String string) {
		this.idCardNo = string;
	}

	public String getAccessCardNo() {
		return accessCardNo;
	}

	public void setAccessCardNo(String string) {
		this.accessCardNo = string;
	}

	public String getDepartMent() {
		return departMent;
	}

	public void setDepartMent(String departMent) {
		this.departMent = departMent;
	}

	public String getReportsTo() {
		return reportsTo;
	}

	public void setReportsTo(String reportsTo) {
		this.reportsTo = reportsTo;
	}

	public ArrayList<String> getApprovers() {
		return approvers;
	}

	public void setApprovers(List<String> approvers) {
		if(approvers!=null)
		{
		this.approvers = new ArrayList<String>();
		this.approvers.addAll(approvers);
		
		}
	}

	public ArrayList<PreviousCompanyHistory> getPreviousCompanyHistory() {
		return previousCompanyHistory;
	}

	public void setPreviousCompanyHistory(
			List<PreviousCompanyHistory> list) {
		if(list!=null)
		{
		 previousCompanyHistory = new ArrayList<PreviousCompanyHistory>();
		 previousCompanyHistory.addAll(list);
		
		}
	}

	public ArrayList<EducationalInfo> getEducationalInfo() {
		return educationalInfo;
	}

	public void setEducationalInfo(List<EducationalInfo> list) {
		if(list!=null)
		{
		this.educationalInfo = new ArrayList<EducationalInfo>();
		educationalInfo.addAll(list);
		}
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public void setMiddleName(String middleName) 
	{
		if(middleName==null)
			return;
		if(!(this.middleName.equals(middleName)))
			nameChanged=true;
		super.setMiddleName(middleName);
		
	}

	@Override
	public void setLastName(String lastName) 
	{
		if(lastName==null)
		if(!(this.lastName.equals(lastName)))
			nameChanged=true;
		super.setLastName(lastName);
		
	}
	

	public void setPreviousCompanyHistory(
				ArrayList<PreviousCompanyHistory> previousCompanyHistory) {
			this.previousCompanyHistory = previousCompanyHistory;
		}
	
		public void setEducationalInfo(ArrayList<EducationalInfo> educationalInfo) {
			this.educationalInfo = educationalInfo;
		}
		
		
		public String getEmployeeType() {
			return employeeType;
		}
	
		public void setEmployeeType(String employeeType) {
			if(employeeType!=null)
			this.employeeType = employeeType.trim();
		}
		
		public String getEmployeeBankAccountNo() {
			return employeeBankAccountNo;
		}


		public void setEmployeeBankAccountNo(String employeeBankAccountNo) {
			this.employeeBankAccountNo = employeeBankAccountNo;
		}


		public String getEmployeeBankName() {
			return employeeBankName;
		}

	

		public void setEmployeeBankName(String employeeBankName) {
			this.employeeBankName = employeeBankName;
		}


		public String getEmployeeESICcode() {
			return employeeESICcode;
		}


		public void setEmployeeESICcode(String employeeESICcode) {
			this.employeeESICcode = employeeESICcode;
		}


		public String getEmployeePanNo() {
			return employeePanNo;
		}


		public void setEmployeePanNo(String employeePanNo) {
			this.employeePanNo = employeePanNo;
		}
		
	
	
	private EmployeeInfo saveEmployeeInfo()
	{
		EmployeeInfo empinfo=new EmployeeInfo();
		empinfo.setCount(getCount());
		empinfo.setBranch(getBranchName());
		empinfo.setCellNumber(getCellNumber1());
		empinfo.setDepartment(getDepartMent());
		empinfo.setDesignation(getDesignation());
		empinfo.setEmpCount(getCount());
		empinfo.setEmployeerole(getRoleName());
		empinfo.setEmployeeType(getEmployeeType());
		empinfo.setFullName(getFullname());
		empinfo.setId(getId());
		empinfo.setCountry(this.getCountry());
		empinfo.setCompanyId(this.companyId);
		empinfo.setCaste(this.getCaste());
		/**
		 * Date : 04-05-2018 BY ANIL
		 */
		if (this.getGender() != null) {
			empinfo.setGender(this.getGender());
		} else {
			empinfo.setGender("");
		}
		
		/**
		 * Date : 29-05-2018 BY ANIL
		 */
		if (this.getNumberRange() != null) {
			empinfo.setNumberRangeAsEmpTyp(this.getNumberRange());
		} else {
			empinfo.setNumberRangeAsEmpTyp("");
		}
		
		/**
		 * Date : 20-07-2018 BY ANIL
		 */
		if(this.getProjectName()!=null){
			empinfo.setProjectName(getProjectName());
		}
		
		empinfo.setStatus(this.isStatus());
		
		/**
		 * Date : 25-07-2018 By  ANIL
		 */
		if(getDob()!=null){
			empinfo.setDateOfBirth(getDob());
		}
		
		if(getJoinedAt()!=null){
			empinfo.setDateOfJoining(getJoinedAt());
		}
		
		if(getLastWorkingDate()!=null){
			empinfo.setLastWorkingDate(getLastWorkingDate());
		}else{
			empinfo.setLastWorkingDate(null);
		}
		/**
		 * Date : 14-11-2018 BY ANIL
		 */
		empinfo.setDisabled(isDisabled);
		empinfo.setDisablityPercentage(disablityPercentage);
		

		/**
		 * Date : 21-12-2018 By ANIL
		 */
		empinfo.setAbsconded(isAbsconded);
		empinfo.setTerminated(isTerminated);
		empinfo.setResigned(isResigned);
		empinfo.setRealeaseForPayment(isRealeaseForPayment);
		/**
		 * End
		 */
		
		/**
		 * @author Anil ,Date : 10-04-2019
		 */
		if(empGrp!=null){
			empinfo.setEmpGrp(empGrp);
		}
		
		/**
		 * @author Anil , Date : 23-09-2019
		 */
		if(shiftName!=null){
			empinfo.setShiftName(shiftName);
		}
		
		empinfo.setReliever(isReliever);
		return empinfo;
	}
	
	
	
		
	
	public PassportInformation getInformation() {
		return information;
	}
	
	
	public void setInformation(PassportInformation information) {
		this.information = information;
	}
	
	
	public String getIfscCode() {
		return ifscCode;
	}
	
	
	public void setIfscCode(String ifscCode) {
		if(ifscCode!=null)
		this.ifscCode = ifscCode;
	}
	
	
	public String getBankBranch() {
		return bankBranch;
	}
	
	
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	
	
	public String getAccountNo() {
		return accountNo;
	}
	
	
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	
	
	public Key<Config> getKeyRole() {
		return keyRole;
	}
	
	
	public void setKeyRole(Key<Config> keyRole) {
		this.keyRole = keyRole;
	}
	
	
	public Key<Config> getKeyDesignation() {
		return keyDesignation;
	}
	
	
	public void setKeyDesignation(Key<Config> keyDesignation) {
		this.keyDesignation = keyDesignation;
	}
	
	
	public Key<BranchRelation> getKeyBranchName() {
		return keyBranchName;
	}
	
	
	public void setKeyBranchName(Key<BranchRelation> keyBranchName) {
		this.keyBranchName = keyBranchName;
	}
	
	
	public boolean isNameChanged() {
		return nameChanged;
	}
	
	
	public void setNameChanged(boolean nameChanged) {
		this.nameChanged = nameChanged;
	}
	
	
	public Key<EmployeeRelation> getKeyName() {
		return keyName;
	}
	
	
	public void setKeyName(Key<EmployeeRelation> keyName) {
		this.keyName = keyName;
	}
	
	
	public void setApprovers(ArrayList<String> approvers) {
		this.approvers = approvers;
	}


    public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}





/**************************************************Relation Managment Part******************************/
	@GwtIncompatible
	@OnSave
	public void onSave()
	{
		Logger logger=Logger.getLogger("Employee Save");
		{
			logger.log(Level.SEVERE,"Inside employee save menthod");
			
//			Added by Priyanka
//			if(id==null){
				if (this.fullname!= null&&this.getUserName()!=null&&this.getPassword()!=null&&this.getRole()!=null) {
					System.out.println("user role11");
//					updateEmployee();
//					User user=ofy().load().type(User.class).filter("companyId", this.getCompanyId()).filter("employeeName", this.fullname).filter("userName", this.getUserName()).filter("status", true).first().now();
					User user=ofy().load().type(User.class).filter("companyId", this.getCompanyId()).filter("empCount", this.count).filter("userName", this.getUserName()).filter("status", true).first().now();
					
					UserRole userRole=null;
					System.out.println("user role"+getRole());
					if(getRole()!=null){
						System.out.println("company Id ="+this.getCompanyId());
						System.out.println("Inside filter userRole ="+this.getUserRoleName());
						userRole=ofy().load().type(UserRole.class).filter("companyId", this.getCompanyId()).filter("roleName", this.getUserRoleName()).filter("roleStatus", true).first().now();
					}
					
					if(user!=null){
						System.out.println("Existing user");
						//user.setRole(getRole());
						user.setRole(userRole);
						user.setUserName(getUserName());
//						user.setPassword(getPassword()); //Ashwini Patil Date:1-12-2023 commented this code as password was getting reset when project is getting saved
						user.setRole(userRole);
						user.setEmployeeName(getFullname());
						user.setEmpCount(getCount());//Ashwini Patil Date:22-08-2023	
						System.out.println("emp count set in user");
						user.setBranch(this.getBranchName());
						user.setEmail(this.getEmail());
						user.setStatus(true);
					}else{
						user = new User();
						user.setUserName(getUserName());
						System.out.println("user name"+getUserName());
						user.setPassword(getPassword());
						System.out.println("password"+getPassword());
						//user.setRole(getRole());
						user.setRole(userRole);
						user.setCompanyId(getCompanyId());
						user.setEmployeeName(getFullname());
						user.setEmpCount(getCount());//Ashwini Patil Date:22-08-2023
						System.out.println("emp count set in user");
						System.out.println("emp name"+getFullname());
						user.setBranch(this.getBranchName());
						user.setEmail(this.getEmail());
						user.setStatus(true);
					}
					System.out.println("user ="+user);
					System.out.println("userRole ="+userRole);
					if(user!=null&&userRole!=null){
						//System.out.println("userrole"+userRole);
//						ofy().save().entity(user).now();
						GenricServiceImpl genimpl = new GenricServiceImpl();
						genimpl.save(user);
					}
				}							
				
//			}
			
			
			
			
			//Name has changed , this Relational Kind needs to be updated
			if(nameChanged==true)
			{
				System.out.println("Name Has Been Changed Value of Key Name is "+keyName);
				if(keyName!=null)
				{
				EmployeeRelation employeeRelation=new EmployeeRelation();
				employeeRelation.setEmployeeName(this.fullname);
				MyUtility.updateRelationalEntity(employeeRelation,keyName);
				}
				
			}
			
			
			/** date 11.8.2018 added by komal for one time deduction***/
			if(isOneTimeDeduction){
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM");
				double amount = getOneTimeDeductionAmount();
				ArrearsDetails arrearsDetails = null;
				boolean flag = false;
				if(amount > 0){
					arrearsDetails = ofy().load().type(ArrearsDetails.class).filter("companyId", this.getCompanyId()).filter("empId", this.getCount()).first().now();
					ArrayList<ArrearsInfoBean> list = new ArrayList<ArrearsInfoBean>();
					if(arrearsDetails == null ){
						arrearsDetails = new ArrearsDetails();
					}else{
						list = arrearsDetails.getArrearsList();
						for(ArrearsInfoBean bean : list){
							if(bean.getHead().equals(AppConstants.ONETIMEDEDUCTION)){
								bean.setAmount(amount);
								flag = true;
							}
						}
					}
					
					if(!flag){
						ArrearsInfoBean infoBean = new ArrearsInfoBean();
						infoBean.setHead(AppConstants.ONETIMEDEDUCTION);
						infoBean.setOperation(AppConstants.DEDUCT);
						infoBean.setAmount(amount);
						infoBean.setFromDate(format.format(this.getJoinedAt()));
						infoBean.setToDate(format.format(this.getJoinedAt()));
						list.add(infoBean);
						arrearsDetails.setArrearsList(list);
					}
					arrearsDetails.setEmpId(this.getCount());
					arrearsDetails.setEmpName(this.getFullname().trim());
					arrearsDetails.setEmpCellNo(this.getCellNumber1());
					arrearsDetails.setCompanyId(this.getCompanyId());
					
								
					GenricServiceImpl impl = new GenricServiceImpl();
					impl.save(arrearsDetails);
				}
			}
			/** date 28.8.2018 added by komal for cnc project**/
			Date date = null;
			if(this.isAbsconded() && this.getAbscondDate()!=null){
				date = this.getAbscondDate();
			}else if(this.isTerminated() && this.getTerminationDate()!=null){
				date = this.getTerminationDate();
			}else if(this.isResigned() && this.getLastWorkingDate()!=null){
				date = this.getLastWorkingDate();
			}
//			if(date != null){
//				List<ProjectAllocation> allocationList = ofy().load().type(ProjectAllocation.class).filter("companyId", this.getCompanyId()).filter("employeeProjectAllocationList.employeeId",this.count+"").list(); 
//				if(allocationList.size()>0){
//
//					for(ProjectAllocation alloc : allocationList){
//						for(EmployeeProjectAllocation empAllocation : alloc.getEmployeeProjectAllocationList()){
//							if(empAllocation.getEmployeeId()!=null && !empAllocation.getEmployeeId().equals("")){
//								if(Integer.parseInt(empAllocation.getEmployeeId()) == this.count){
//									if(empAllocation.getStartDate()!=null && empAllocation.getEndDate()!=null){
//										Date currentDate = new Date();
//										if(currentDate.after(empAllocation.getStartDate()) && currentDate.before(empAllocation.getEndDate())){
//											if(date.compareTo(empAllocation.getEndDate()) <= 0)
//												empAllocation.setEndDate(date);
//										}
//										break;
//									}
//								}
//							}
//							
//						}
//					}
//					ofy().save().entities(allocationList);
//					
//				}
//				
//			}
			/** end komal**/
			
		}
		EmployeeInfo empinfo=null;
		if(id==null)
		{
			setCreationDate(DateUtility.getDateWithTimeZone("IST",new Date()));
			// One Part Handling
			EmployeeRelation employeeRelation = new EmployeeRelation();
			employeeRelation.setEmployeeName(this.fullname);
			keyName = (Key<EmployeeRelation>) MyUtility.createRelationalKey(employeeRelation);
			empinfo = saveEmployeeInfo();
			ofy().save().entity(empinfo);
			 
		}	 
		
		if(id!=null){
			updateEmployeeInfo();
		}
		
		if(updatePfNum){
			updatePfNumberOnPaySlip();
			setUpdatePfNum(false);
		}
		
		/**
		 * @author Anil , Date : 01-10-2019
		 * Update Employee Project name on HrProject and ProjectAllocation
		 * For sasha 
		 */
		if(projectName!=null&&!projectName.equals("")){
			HrProject project=ofy().load().type(HrProject.class).filter("companyId", this.getCompanyId())
					.filter("projectName", projectName).first().now();
			if(project!=null){
				boolean updateFlag=false;
				

				/**
				 * @author Vijay Date :- 08-09-2022
				 * Des :- used common method to read OT from project or new entity HrProjectOvertime`
				 */
				ArrayList<Overtime> overtimelist = new ArrayList<Overtime>();
				CommonServiceImpl commonservice = new CommonServiceImpl();
				overtimelist = commonservice.getHRProjectOvertimelist(project);
				/**
				 * ends here
				 */
//				if(project.getOtList()!=null&&project.getOtList().size()!=0){
//					for(Overtime ot:project.getOtList()){
				
				String otShortName="";
				if(overtimelist!=null&&overtimelist.size()!=0){
					for(Overtime ot:overtimelist){
						if(ot.getEmpId()==this.getCount()){
							updateFlag=true;
						}
						if(this.getOvertimeName()!=null&&!this.getOvertimeName().equals("")&&ot.getName().equals(this.getOvertimeName())){
							otShortName=ot.getShortName();
						}
					}
					if(!updateFlag){
						project.setReversEmpProjectAllocation(true);
						Overtime ot = new Overtime();
						
						//Ashwini Patil Date:25-08-2023
						if(this.getOvertimeName()!=null&&!this.getOvertimeName().equals("")){
							ot.setShortName(otShortName);
							ot.setName(this.getOvertimeName());
						}
						
						ot.setEmpId(this.getCount());
						ot.setEmpName(this.getFullname());
						ot.setShift(this.getShiftName());
						ot.setEmpDesignation(this.getDesignation());
						ot.setCompanyId(this.getCompanyId());
						ot.setStartDate(new Date());
						
						Date endDate=new Date(8099, 11, 31); //8099+1900=9999
						ot.setEndDate(endDate);//Ashwini Patil Date:25-08-2023
						
						ot.setSiteLocation(this.getSiteLocation());
						
						project.getOtList().add(ot);
						
//						EmployeeProjectHistory emprojectHistrory=new EmployeeProjectHistory();
//						emprojectHistrory.setProjectCode(project.getProjectCode());
//						emprojectHistrory.setProjectName(project.getProjectName());
//						emprojectHistrory.setProjectStartDate(new Date());
//						emprojectHistrory.setProjectEndDate(new Date());
//						this.getEmployeeProjectHistoryList().add(emprojectHistrory);
						
						
						/**
						 * @author Vijay Date :- 09-09-2022
						 * Des :- updating Project OT in seperate entity
						 */
						if(project.getOtList().size()>150 || project.isSeperateEntityFlag()){
							final ArrayList<HrProjectOvertime> otlist = new ArrayList<HrProjectOvertime>();
							HrProjectOvertime hrot = new HrProjectOvertime();
							hrot = hrot.MycloneHrProjectOT(ot);
							hrot.setProjectId(project.getCount());
							otlist.add(hrot);
							commonservice.updateHrProjectOvertime(otlist, "Save");
						}
						else {
							ofy().save().entity(project).now();
						}
						
						if(project.getCncProjectCount()!=0){
							ProjectAllocation projectAllocation=ofy().load().type(ProjectAllocation.class).filter("companyId", this.getCompanyId())
									.filter("projectName", projectName).filter("count", project.getCncProjectCount()).first().now();
							if(projectAllocation!=null){
								boolean updateAllFlag=false;
								
								/**
								 * @author Vijay @Since 21-09-2022
								 * Loading employee project allocation from different entity
								 */
								   List<EmployeeProjectAllocation> empprojectallocationlist = commonservice.getEmployeeProjectAllocationlist(projectAllocation);
							    /**
							     * ends here
							     */
//								if(projectAllocation.getEmployeeProjectAllocationList()!=null){
   
								if(empprojectallocationlist!=null && empprojectallocationlist.size()>0){
									for(EmployeeProjectAllocation empProjAll:empprojectallocationlist){
										if(empProjAll.getEmployeeId()!=null && !empProjAll.getEmployeeId().equals("") && empProjAll.getEmployeeId().equals(this.getCount()+"")){
											updateAllFlag=true;
										}
									}
									if(!updateAllFlag){
										EmployeeProjectAllocation empProjAll=new EmployeeProjectAllocation();
										empProjAll.setEmployeeId(this.getCount()+"");
										empProjAll.setEmployeeName(this.getFullname());
										empProjAll.setEmployeeRole(this.getDesignation());
										empProjAll.setShift(this.getShiftName());
										empProjAll.setStartDate(new Date());
										
										empProjAll.setSiteLocation(this.getSiteLocation());
										
										projectAllocation.getEmployeeProjectAllocationList().add(empProjAll);
										projectAllocation.setReversEmpProjectAllocation(true);
										
										/**
										 * @author Vijay @Since 19-09-2022
										 * if Employee Project Allocation list greater than 600 then storing it into seperate intity 
										 */
										if(projectAllocation.getEmployeeProjectAllocationList().size()>AppConstants.employeeProjectAllocationListNumber || projectAllocation.isSeperateEntityFlag()){
												projectAllocation.getEmployeeProjectAllocationList().clear();
												commonservice.updateEmployeeProjectAllocationList(projectAllocation.getEmployeeProjectAllocationList(), "Save");
										}
										/**
										 * ends here
										 */
										
										ofy().save().entity(projectAllocation).now();
										
									}
								}
							}
						}
						
						
					}
				}
				
			}
			
		}
		
		//calendar and leave allocation process

		if(this.getCalendar()!=null&&!this.getCalendar().equals("")&&this.getLeaveGroup()!=null&&!this.getLeaveGroup().equals("")){
			logger.log(Level.SEVERE,"in calendar and leave allocation process. cal="+this.getCalendar()+" leave group="+this.getLeaveGroup()+" count="+this.getCount());
			ArrayList<LeaveBalance> allocateLeavesArray = new ArrayList<LeaveBalance>();			
			LeaveBalance leavebal = new LeaveBalance();
			Calendar cal=ofy().load().type(Calendar.class).filter("calName",this.getCalendar() ).filter("companyId", this.getCompanyId()).first().now();
			List<LeaveGroup> leaveGroupList= ofy().load().type(LeaveGroup.class).filter("companyId", this.getCompanyId()).list();
			LeaveGroup leaveGroup=null;
			if(leaveGroupList!=null){
				for(LeaveGroup lg:leaveGroupList) {
					if(lg.getGroupName().equals(this.getLeaveGroup())) {
						leaveGroup=lg;
						break;
					}
				}
			}
			if(empinfo==null)
				empinfo=ofy().load().type(EmployeeInfo.class).filter("empCount", this.getCount()).filter("companyId", this.getCompanyId()).first().now();
			
			if(cal!=null)
				logger.log(Level.SEVERE,"cal not null");
			if(leaveGroup!=null)
				logger.log(Level.SEVERE,"leaveGroup not null");
			if(empinfo!=null)
				logger.log(Level.SEVERE,"empinfo not null");
			
			if(cal!=null&&leaveGroup!=null&&empinfo!=null){
				logger.log(Level.SEVERE,"calendar and leave allocation process started");
				empinfo.setLeaveCalendar(cal);
				empinfo.setLeaveGroupName(this.getLeaveGroup());
				leavebal.setEmpInfo(empinfo);				
				leavebal.setLeaveGroup(leaveGroup);
				leavebal.setValidfrom(cal.getCalStartDate());
				leavebal.setValidtill(cal.getCalEndDate());
				leavebal.setCompanyId(this.getCompanyId());
				allocateLeavesArray.add(leavebal);	
				logger.log(Level.SEVERE,"allocateLeavesArray size="+allocateLeavesArray.size());
				LeaveAllocationServiceImpl leaveAllocationservice=new LeaveAllocationServiceImpl();
				leaveAllocationservice.allocateLeave(allocateLeavesArray);	
				logger.log(Level.SEVERE,"calendar and leave allocation process completed");
			}
		
		}
		
		
		
		
		
		
//		//Many Part Handling
//		MyQuerry querry=new MyQuerry();
//	  	Vector<Filter>filter=new Vector<Filter>();
//	  	Filter nameFilter=new Filter();
//	  	nameFilter.setStringValue(this.branchName);
//	  	nameFilter.setQuerryString("branchName");
//	  	filter.add(nameFilter);
//	  	querry.setFilters(filter);
//		
//		querry.setQuerryObject(new BranchRelation());
//		keyBranchName=(Key<BranchRelation>) MyUtility.getRelationalKeyFromCondition(querry);
		keyDesignation=MyUtility.getConfigKeyFromCondition(designation,ConfigTypes.
				EMPLOYEEDESIGNATION.getValue());
	    keyRole=MyUtility.getConfigKeyFromCondition(roleName,ConfigTypes.EMPLOYEEROLE.getValue());
		   
	}
	
	@GwtIncompatible
	private void updatePfNumberOnPaySlip() {

		List<PaySlip> paySlipList=ofy().load().type(PaySlip.class).filter("empid", this.getCount()).filter("companyId", getCompanyId()).list();
		if(paySlipList!=null&&paySlipList.size()!=0){
			for(PaySlip payslip:paySlipList){
				//PF
				if(this.getPPFNaumber()!=null&&!this.getPPFNaumber().equals("")){
					if(payslip.getPfNumber()!=null&&!payslip.getPfNumber().equals("")){
						if(!payslip.getPfNumber().equals(this.getPPFNaumber())){
							payslip.setPfNumber(this.getPPFNaumber());
						}
					}else{
						payslip.setPfNumber(this.getPPFNaumber());
					}
				}
				//ESIC
				if(this.getEmployeeESICcode()!=null&&!this.getEmployeeESICcode().equals("")){
					if(payslip.getEsicNumber()!=null&&!payslip.getEsicNumber().equals("")){
						if(!payslip.getEsicNumber().equals(this.getEmployeeESICcode())){
							payslip.setEsicNumber(this.getEmployeeESICcode());
						}
					}else{
						payslip.setEsicNumber(this.getEmployeeESICcode());
					}
				}
				//UAN
				if(this.getUANno()!=null&&!this.getUANno().equals("")){
					if(payslip.getUanNo()!=null&&!payslip.getUanNo().equals("")){
						if(!payslip.getUanNo().equals(this.getUANno())){
							payslip.setUanNo(this.getUANno());
						}
					}else{
						payslip.setUanNo(this.getUANno());
					}
				}
				//AADHAR
				if(this.getAadharNumber()!=0){
					if(payslip.getAadharNo()!=null&&!payslip.getAadharNo().equals("")){
						if(!payslip.getAadharNo().equals(this.getAadharNumber())){
							payslip.setAadharNo(this.getAadharNumber()+"");
						}
					}else{
						payslip.setAadharNo(this.getAadharNumber()+"");
					}
				}
				//PAN
				if(this.getEmployeePanNo()!=null&&!this.getEmployeePanNo().equals("")){
					if(payslip.getPanNumber()!=null&&!payslip.getPanNumber().equals("")){
						if(!payslip.getPanNumber().equals(this.getEmployeePanNo())){
							payslip.setPanNumber(this.getEmployeePanNo());
						}
					}else{
						payslip.setPanNumber(this.getEmployeePanNo());
					}
				}
			}
			ofy().save().entities(paySlipList).now();
		}
	}





	@GwtIncompatible
	@OnLoad
	 public void onLoad()
	{
		this.fillAllNames();
		/**
		 * @author Anil @since 25-01-2021
		 * Commented below code of checking branch relation , config and designation to optimize the loading process
		 * did to optimize the attendance marking process
		 */
//		String branchName=MyUtility.getBranchNameFromKey(keyBranchName);
//		if(branchName!=null)
//		{
//			if(branchName.equals("")==false)
//				setBranchName(branchName.trim());
//		}
//		String roleName=MyUtility.getConfigNameFromKey(keyRole);
//		if(roleName!=null)
//		{
//			if(roleName.equals("")==false)
//				setRoleName(roleName.trim());
//		}
//		String designationName=MyUtility.getConfigNameFromKey(keyDesignation);
//		if(designationName!=null)
//		{
//			if(designationName.equals("")==false)
//				setDesignation(designationName.trim());
//		}
		
		 
	}
/*****************************************************************************************************/

		@Override
		public String toString() {
			return fullname.trim();
		}
	
	
	public String getCountry() {
		return country;
	}
	
	
	public void setCountry(String country) {
		if(country!=null)
		this.country = country.trim();
	}
	
	
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj==null)
			return false;
		else
		{
			if(obj instanceof Employee)
			{
				Employee emp=(Employee) obj;
				if(emp.getId()!=null&getId()!=null)
				{
					if(emp.getId()==getId())
						return true;
					else
						return false;
				}
				else
					return false;
				
				
			}
			return false;
		}
		
		
	}

	
	public void setPPFNumber(String value) {
		this.ppfNo=value.trim();
		
	}
	
	
	public String getPPFNaumber() {
		// TODO Auto-generated method stub
		return this.ppfNo;
	}
	
	
	public String getBloodGroup() {
		// TODO Auto-generated method stub
		return bloodGroup;
	}
	
	
	public void setBloodGroup(String value) {
		if(value!=null)
		this.bloodGroup=value.trim();
		
	}


	public DocumentUpload getUploadAddDet() {
		return uploadAddDet;
	}


	public void setUploadAddDet(DocumentUpload uploadAddDet) {
		this.uploadAddDet = uploadAddDet;
	}


	public DocumentUpload getUploadExpDet() {
		return uploadExpDet;
	}


	public void setUploadExpDet(DocumentUpload uploadExpDet) {
		this.uploadExpDet = uploadExpDet;
	}


	public DocumentUpload getUploadEduDet() {
		return uploadEduDet;
	}


	public void setUploadEduDet(DocumentUpload uploadEduDet) {
		this.uploadEduDet = uploadEduDet;
	}

	
	public List<EmployeeBranch> getEmpBranchList() {
		return empBranchList;
	}


	public void setEmpBranchList(List<EmployeeBranch> empBranchList) {
		
		List<EmployeeBranch> branchlis=  new ArrayList<EmployeeBranch>();
		branchlis.addAll(empBranchList);
		this.empBranchList = branchlis;
	}
//*****************anil *****************
	
	

	


	@GwtIncompatible
	private void updateEmployeeInfo()
	{
		EmployeeInfo empInfo=ofy().load().type(EmployeeInfo.class).filter("empCount", this.getCount()).filter("companyId", getCompanyId()).first().now();
		if(empInfo!=null){
			empInfo.setBranch(getBranchName());
			empInfo.setCellNumber(getCellNumber1());
			empInfo.setDepartment(getDepartMent());
			empInfo.setDesignation(getDesignation());
			empInfo.setEmployeerole(getRoleName());
			empInfo.setEmployeeType(getEmployeeType());
			empInfo.setFullName(getFullname());
			empInfo.setCountry(this.getCountry());
			 empInfo.setCaste(this.getCaste());
			/**
			 * Date : 04-05-2018 BY ANIL
			 */
			if (this.getGender() != null) {
				empInfo.setGender(this.getGender());
			} else {
				empInfo.setGender("");
			}
			
			/**
			 * Date : 29-05-2018 BY ANIL
			 */
			if (this.getNumberRange() != null) {
				empInfo.setNumberRangeAsEmpTyp(this.getNumberRange());
			} else {
				empInfo.setNumberRangeAsEmpTyp("");
			}
			if(this.getProjectName()!=null){
				empInfo.setProjectName(getProjectName());
			}
			empInfo.setStatus(this.isStatus());
			
			/**
			 * Date : 25-07-2018 By  ANIL
			 */
			if(getDob()!=null){
				empInfo.setDateOfBirth(getDob());
			}
			
			if(getJoinedAt()!=null){
				empInfo.setDateOfJoining(getJoinedAt());
			}
			
			if(getLastWorkingDate()!=null){
				empInfo.setLastWorkingDate(getLastWorkingDate());
			}else{
				empInfo.setLastWorkingDate(null);
			}
			
			/**
			 * Date : 14-11-2018 BY ANIL
			 */
			empInfo.setDisabled(isDisabled);
			empInfo.setDisablityPercentage(disablityPercentage);
			
			/**
			 * Date : 21-12-2018 By ANIL
			 */
			empInfo.setAbsconded(isAbsconded);
			empInfo.setTerminated(isTerminated);
			empInfo.setResigned(isResigned);
			empInfo.setRealeaseForPayment(isRealeaseForPayment);
			/**
			 * End
			 */
			
			/**
			 * @author Anil ,Date : 10-04-2019
			 */
			if(empGrp!=null){
				empInfo.setEmpGrp(empGrp);
			}
			/**
			 * @author Anil , Date : 23-09-2019
			 */
			if(shiftName!=null){
				empInfo.setShiftName(shiftName);
			}
			
			empInfo.setReliever(isReliever);
			
			ofy().save().entity(empInfo).now();
		}
	}


	public String getApproverName() {
		return approverName;
	}


	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}


	public String getApproveStatus() {
		return approveStatus;
	}


	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}


	public double getSalaryAmt() {
		return salaryAmt;
	}


	public void setSalaryAmt(double salaryAmt) {
		this.salaryAmt = salaryAmt;
	}

	public double getMonthlyWorkingHrs() {
		return monthlyWorkingHrs;
	}


	public void setMonthlyWorkingHrs(double monthlyWorkingHrs) {
		this.monthlyWorkingHrs = monthlyWorkingHrs;
	}


	public String getDailyRoute() {
		return dailyRoute;
	}


	public void setDailyRoute(String dailyRoute) {
		this.dailyRoute = dailyRoute;
	}
	
	public boolean isResigned() {
		return isResigned;
	}



	public void setResigned(boolean isResigned) {
		this.isResigned = isResigned;
	}



	public Date getResignationDate() {
		return resignationDate;
	}



	public void setResignationDate(Date resignationDate) {
		this.resignationDate = resignationDate;
	}



	public double getNoticePeriod() {
		return noticePeriod;
	}



	public void setNoticePeriod(double noticePeriod) {
		this.noticePeriod = noticePeriod;
	}



	public boolean isTerminated() {
		return isTerminated;
	}



	public void setTerminated(boolean isTerminated) {
		this.isTerminated = isTerminated;
	}



	public Date getTerminationDate() {
		return terminationDate;
	}



	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}



	public boolean isAbsconded() {
		return isAbsconded;
	}



	public void setAbsconded(boolean isAbsconded) {
		this.isAbsconded = isAbsconded;
	}



	public Date getAbscondDate() {
		return abscondDate;
	}



	public void setAbscondDate(Date abscondDate) {
		this.abscondDate = abscondDate;
	}



	public ArrayList<EmployeeAssetBean> getCheckList() {
		return checkList;
	}


	public void setCheckList(List<EmployeeAssetBean> checkList) {
		ArrayList<EmployeeAssetBean> list =new  ArrayList<EmployeeAssetBean>();
		if(checkList.size() > 0){
			list.addAll(checkList);
		}
		this.checkList = list;
	}

	public Double getShortfall() {
		return shortfall;
	}



	public void setShortfall(Double shortfall) {
		this.shortfall = shortfall;
	}



	public Double getWaivedOff() {
		return waivedOff;
	}



	public void setWaivedOff(Double waivedOff) {
		this.waivedOff = waivedOff;
	}



	public CheckListType getCheckListType() {
		return checkListType;
	}



	public void setCheckListType(CheckListType checkListType) {
		this.checkListType = checkListType;
	}



	public boolean isRealeaseForPayment() {
		return isRealeaseForPayment;
	}



	public void setRealeaseForPayment(boolean isRealeaseForPayment) {
		this.isRealeaseForPayment = isRealeaseForPayment;
	}



	public boolean isOneTimeDeduction() {
		return isOneTimeDeduction;
	}



	public void setOneTimeDeduction(boolean isOneTimeDeduction) {
		this.isOneTimeDeduction = isOneTimeDeduction;
	}



	public double getOneTimeDeductionAmount() {
		return oneTimeDeductionAmount;
	}



	public void setOneTimeDeductionAmount(double oneTimeDeductionAmount) {
		this.oneTimeDeductionAmount = oneTimeDeductionAmount;
	}



	public void setEmployeeProjectHistoryList(
			ArrayList<EmployeeProjectHistory> employeeProjectHistoryList) {
		this.employeeProjectHistoryList = employeeProjectHistoryList;
	}



	public void setEmployeeCTCList(ArrayList<EmployeeCTCTemplate> employeeCTCList) {
		this.employeeCTCList = employeeCTCList;
	}


	public String getReasonForLeaving() {
		return reasonForLeaving;
	}


	public void setReasonForLeaving(String reasonForLeaving) {
		this.reasonForLeaving = reasonForLeaving;
	}
	
	@OnSave
	@GwtIncompatible
	private void updateUploadHistory(){
		ArrayList<UploadHistory> uploadHisList=new ArrayList<UploadHistory>();
		if(photo!=null&&photo.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, photo, "Photo",fullname,companyId,photo.getUploadedBy());
			uploadHisList.add(uploadHis);
			photo.setStatus(false);
			setUploadCompToNull(photo);
		}
		if(documentUpload1!=null&&documentUpload1.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, documentUpload1, "documentUpload1",fullname,companyId,documentUpload1.getUploadedBy());
			uploadHisList.add(uploadHis);
			documentUpload1.setStatus(false);
			setUploadCompToNull(documentUpload1);
		}
		if(documentUpload2!=null&&documentUpload2.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, documentUpload2, "documentUpload2",fullname,companyId,documentUpload2.getUploadedBy());
			uploadHisList.add(uploadHis);
			documentUpload2.setStatus(false);
			setUploadCompToNull(documentUpload2);
		}
		if(uploadAddDet!=null&&uploadAddDet.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, uploadAddDet, "Address Document",fullname,companyId,uploadAddDet.getUploadedBy());
			uploadHisList.add(uploadHis);
			uploadAddDet.setStatus(false);
			setUploadCompToNull(uploadAddDet);
		}
		
		if(uploadDisabilityDoc!=null&&uploadDisabilityDoc.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, uploadDisabilityDoc, "Disability Document",fullname,companyId,uploadDisabilityDoc.getUploadedBy());
			uploadHisList.add(uploadHis);
			uploadDisabilityDoc.setStatus(false);
			setUploadCompToNull(uploadDisabilityDoc);
		}
		if(uploadEduDet!=null&&uploadEduDet.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, uploadEduDet, "Educational Document",fullname,companyId,uploadEduDet.getUploadedBy());
			uploadHisList.add(uploadHis);
			uploadEduDet.setStatus(false);
			setUploadCompToNull(uploadEduDet);
		}
		if(uploadExpDet!=null&&uploadExpDet.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, uploadExpDet, "Experience Document",fullname,companyId,uploadExpDet.getUploadedBy());
			uploadHisList.add(uploadHis);
			uploadExpDet.setStatus(false);
			setUploadCompToNull(uploadExpDet);
		}
		if(uploadSign!=null&&uploadSign.isStatus()==true){
			UploadHistory uploadHis=new UploadHistory("Employee", count, uploadSign, "Sign",fullname,companyId,uploadSign.getUploadedBy());
			uploadHisList.add(uploadHis);
			uploadSign.setStatus(false);
			setUploadCompToNull(uploadSign);
		}
		
		if(uploadHisList!=null&&uploadHisList.size()!=0){
			ofy().save().entities(uploadHisList).now();
		}
	}
	
	@GwtIncompatible
	private void setUploadCompToNull(DocumentUpload docUp){
		if(docUp.getName().equals("")&&docUp.getUrl().equals("")){
			docUp=null;
		}
	}





	public boolean isReliever() {
		return isReliever;
	}





	public void setReliever(boolean isReliever) {
		this.isReliever = isReliever;
	}
	
	
	public DocumentUpload getPoliceVerfDoc() {
		return policeVerfDoc;
	}





	public void setPoliceVerfDoc(DocumentUpload policeVerfDoc) {
		this.policeVerfDoc = policeVerfDoc;
	}





	public Date getExpiryDate() {
		return expiryDate;
	}





	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}





	public String getSiteLocation() {
		return siteLocation;
	}





	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}





	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getExpiryDate()!=null){
			this.setExpiryDate(dateUtility.setTimeMidOftheDayToDate(this.getExpiryDate()));
		}
	}





	public String getEmployeeName() {
		return employeeName;
	}





	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}





	public String getUserName() {
		return userName;
	}





	public void setUserName(String userName) {
		this.userName = userName;
	}





	public String getPassword() {
		return password;
	}





	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public boolean isDuplicateUser(SuperModel model)
	{
		Employee entity = (Employee) model;
		String name = entity.getUserName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		System.out.println("for duplication "+name);
		
		String curname=this.getUserName().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		System.out.println("for duplication 123"+curname);
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	public double getIncentivePercent() {
		return incentivePercent;
	}

	public void setIncentivePercent(double incentivePercent) {
		this.incentivePercent = incentivePercent;
	}

	public String getOvertimeName() {
		return overtimeName;
	}

	public void setOvertimeName(String overtimeName) {
		this.overtimeName = overtimeName;
	}

	public String getCalendar() {
		return calendar;
	}

	public void setCalendar(String calendar) {
		this.calendar = calendar;
	}

	public String getLeaveGroup() {
		return leaveGroup;
	}

	public void setLeaveGroup(String leaveGroup) {
		this.leaveGroup = leaveGroup;
	}

	public String getCtcTemplate() {
		return ctcTemplate;
	}

	public void setCtcTemplate(String ctcTemplate) {
		this.ctcTemplate = ctcTemplate;
	}

	

}
