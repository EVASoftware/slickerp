package com.slicktechnologies.shared.common.contactidentification;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;

@Entity
public class ContactPersonIdentification extends SuperModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6304443852730117025L;
	
	/************************************************Applicability Attributes*****************************************************/
	
	protected PersonInfo personInfo;
	protected EmployeeInfo employeeInfo;
	protected PersonInfo personVendorInfo;
	
	protected SocialInformation socialInfo;
	@Index
	protected Integer documentid;
	@Index
	protected String role;
	@Index
	protected String entype;
	@Index
	protected String name;
	@Index
	protected Address contact;
	@Index
	protected String email;
	protected String email1;
	@Index
	protected Long cell;
	@Index
	protected Long cell1;
	protected Long landline;
	protected Long fax;
	protected Address addressInfo;
	
	/** date 17/10/2017 added by komal to add poc name in contact detail form **/
	@Index
	protected String pocName;
	/** date 17/10/2017 added by komal to add bp name in  Contact person list **/
	@Index
	protected String bpName;

	
	/********************************************Constructor**************************************************/
	
	public ContactPersonIdentification() {
		super();
		personInfo=new PersonInfo();
		employeeInfo=new EmployeeInfo();
		personVendorInfo=new PersonInfo();
		socialInfo=new SocialInformation();
		role="";
		entype="";
		name="";
		contact=new Address();
		email="";
		email1="";
		addressInfo=new Address();
		
	}

	/************************************************Getters And Setters********************************************************/
		
	public Long getCell() {
		return cell;
	}

	public Integer getDocumentid() {
		return documentid;
	}

	public void setDocumentid(Integer documentid) {
		this.documentid = documentid;
	}

	public void setCell(Long cell) {
		this.cell = cell;
	}

	public Long getCell1() {
		return cell1;
	}

	public void setCell1(Long cell1) {
		this.cell1 = cell1;
	}

	public Long getLandline() {
		return landline;
	}

	public void setLandline(Long landline) {
		this.landline = landline;
	}

	public Long getFax() {
		return fax;
	}

	public void setFax(Long fax) {
		this.fax = fax;
	}
	
	public SocialInformation getSocialInfo() {
		return socialInfo;
	}

	public void setSocialInfo(SocialInformation socialInfo) {
		this.socialInfo = socialInfo;
	}
	
	public String getEntype() {
		return entype;
	}

	public void setEntype(String entype) {
		this.entype = entype;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name!=null){
			this.name = name;
		}
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		if(email1!=null){
			this.email1 = email1;
		}
	}
	
	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		if(personInfo!=null){
			this.personInfo = personInfo;
		}
	}

	public EmployeeInfo getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(EmployeeInfo employeeInfo) {
		if(employeeInfo!=null){
			this.employeeInfo = employeeInfo;
		}
	}

	public PersonInfo getPersonVendorInfo() {
		return personVendorInfo;
	}

	public void setPersonVendorInfo(PersonInfo personVendorInfo) {
		if(personVendorInfo!=null){
			this.personVendorInfo = personVendorInfo;
		}
	}

	public Address getContact() {
		return contact;
	}

	public void setContact(Address contact) {
		if(contact!=null){
			this.contact = contact;
		}
	}
	
	public Address getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(Address address) {
		if(addressInfo!=null){
			this.addressInfo = address;
		}
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		if(role!=null){
			this.role = role.trim();
		}
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(email!=null){
			this.email = email;
		}
	}
	
	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		if(pocName != null){
		this.pocName = pocName;
		}
	}

	
	public String getBpName() {
		
		if(getEntype().equals("Employee"))
			bpName = getEmployeeInfo().getFullName();
			else if(getEntype().equals("Customer"))
				bpName = getPersonInfo().getFullName();
			else if(getEntype().equals("Vendor"))
				bpName = getPersonVendorInfo().getFullName();
			else 
				bpName = "";
		return bpName;
	}


	/********************************************Overridden Method*******************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
//		return super.toString();
		return name;
	}
	

}
