package com.slicktechnologies.shared.common.arrears;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class ArrearsInfoBean implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1053490454425581135L;
	/**
	 * 
	 */
	String head;
	String fromDate;
	String toDate;
	String operation;
	double amount;
	
	/**
	 * Date :27-08-2018 BY ANIL
	 */
	@Index
	String earningCompName;
	
	/**
	 * Date : 31-12-2018 By ANIL
	 * added flag for bonus and paid leave revision
	 */
	boolean isBonusAndPaidLeaveRevised;
	
	/**
	 * @author Anil , Date : 13-07-2019
	 */
	@Index
	String projectName;
	
	
	public ArrearsInfoBean(){
		head = "";
		fromDate = "";
		toDate = "";
		operation = "";
		
		earningCompName="";
	}
	
	
	
	

	public String getProjectName() {
		return projectName;
	}





	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}





	public boolean isBonusAndPaidLeaveRevised() {
		return isBonusAndPaidLeaveRevised;
	}





	public void setBonusAndPaidLeaveRevised(boolean isBonusAndPaidLeaveRevised) {
		this.isBonusAndPaidLeaveRevised = isBonusAndPaidLeaveRevised;
	}





	public String getEarningCompName() {
		return earningCompName;
	}



	public void setEarningCompName(String earningCompName) {
		this.earningCompName = earningCompName;
	}



	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
}
