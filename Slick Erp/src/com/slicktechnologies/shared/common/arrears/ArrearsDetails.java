package com.slicktechnologies.shared.common.arrears;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.EmployeeAssetBean;

@Entity
public class ArrearsDetails extends SuperModel implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6078577595665220240L;
	/**
	 * 
	 */
	@Index
	int empId;
	@Index
	String empName;
	@Index
	long empCellNo;
	ArrayList<ArrearsInfoBean> arrearsList;

	public ArrearsDetails(){
		empName ="";
		arrearsList = new ArrayList<ArrearsInfoBean>();
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public long getEmpCellNo() {
		return empCellNo;
	}

	public void setEmpCellNo(long empCellNo) {
		this.empCellNo = empCellNo;
	}

	public ArrayList<ArrearsInfoBean> getArrearsList() {
		return arrearsList;
	}

	public void setArrearsList(List<ArrearsInfoBean> arrearsList) {
		ArrayList<ArrearsInfoBean> list = new ArrayList<ArrearsInfoBean>();
		if(arrearsList !=null){
			list.addAll(arrearsList);
			this.arrearsList = list;
		}
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


}
