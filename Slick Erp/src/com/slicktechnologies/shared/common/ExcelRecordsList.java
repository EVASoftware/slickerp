package com.slicktechnologies.shared.common;

import java.io.Serializable;

public class ExcelRecordsList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5075182586415482808L;


	/******************************************Applicability Attributes****************************************/
	
	String firstName;
	String lastName;
	String cellNo;
	String emailId;
	
	
	
	/*******************************************Getters And Setters******************************************/
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCellNo() {
		return cellNo;
	}
	public void setCellNo(String cellNo) {
		this.cellNo = cellNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	

	
	
	
	
}
