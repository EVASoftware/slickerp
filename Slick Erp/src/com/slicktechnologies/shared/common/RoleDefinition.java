package com.slicktechnologies.shared.common;
import java.util.ArrayList;
import java.util.List;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
@Entity
public class RoleDefinition extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -588154785170186953L;

	@Index
	String moduleName;
	@Index 
	String documentName;
	@Index
	String roleType;
	@Index 
	boolean status;
	@Index
	ArrayList<String> roleList;
	
	public RoleDefinition() {
		moduleName="";
		documentName="";
		roleType="";
		status=true;
		roleList=new ArrayList<String>();
	}
	
	
	

	public String getModuleName() {
		return moduleName;
	}




	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}




	public String getDocumentName() {
		return documentName;
	}




	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}




	public String getRoleType() {
		return roleType;
	}




	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}




	public boolean isStatus() {
		return status;
	}




	public void setStatus(boolean status) {
		this.status = status;
	}




	public ArrayList<String> getRoleList() {
		return roleList;
	}




	public void setRoleList(List<String> list) {
		ArrayList<String> roleList=new ArrayList<String>();
		roleList.addAll(list);
		this.roleList = roleList;
	}




	@Override
	public boolean isDuplicate(SuperModel model) {
		
		RoleDefinition entity=(RoleDefinition) model;
		String moduelName1 = entity.getModuleName().trim();
		moduelName1=moduelName1.replaceAll("\\s","");
		moduelName1=moduelName1.toLowerCase();
		
		String documentName1 = entity.getDocumentName().trim();
		documentName1=documentName1.replaceAll("\\s","");
		documentName1=documentName1.toLowerCase();
		
		String roleType1 = entity.getRoleType().trim();
		roleType1=roleType1.replaceAll("\\s","");
		roleType1=roleType1.toLowerCase();
		
		
		
		String currModuleName=moduleName.trim();
		currModuleName=currModuleName.replaceAll("\\s","");
		currModuleName=currModuleName.toLowerCase();
		
		String currDocumentName = documentName.trim();
		currDocumentName=currDocumentName.replaceAll("\\s","");
		currDocumentName=currDocumentName.toLowerCase();
		
		String currRoleType = roleType.trim();
		currRoleType=currRoleType.replaceAll("\\s","");
		currRoleType=currRoleType.toLowerCase();
		
		
		//New Object is being added
		if(entity.id==null){
			if(moduelName1.equals(currModuleName)&&documentName1.equals(currDocumentName)&&roleType1.equals(currRoleType)){
				System.out.println("1");
				return true;
			}else{
				System.out.println("0");
				return false;
			}
		}
		else{
			if(entity.id.equals(id)){
				System.out.println("0");
				return false;	
			}
			if(moduelName1.equals(currModuleName)==true&&documentName1.equals(currDocumentName)==true&&roleType1.equals(currRoleType)==true){
				System.out.println("1");
				return true;
			}
		}
		return false;
	}

}
