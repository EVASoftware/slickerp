package com.slicktechnologies.shared.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingAttendies;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingSpeaker;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;

	@Entity
	public class CustomerTrainingDetails extends ConcreteBusinessProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4634235336682129139L;
	
	
	
	String title;
	@Index
	String status;
	@Index
	String pocName;
	String pocContact;
	String pocEmail;
	String from_time;
	String address;
	Date from_dt;
	String description;
	String smsTemplate;
	
	protected boolean sendSMS;
	protected boolean sendEmail;
	protected boolean eventManagement;
	
	List<CustomerTrainingSpeaker> spreakersLis= new ArrayList<CustomerTrainingSpeaker>();
	List<CustomerTrainingAttendies> attendiesLis= new ArrayList<CustomerTrainingAttendies>();
	
	
	public static final String CREATED = "Created";
	public static final String REQUESTED = "Requested";
	public static final String APPROVED = "Approved";
	public static final String EXECUTED = "Executed";
	
	
	
	
	/********************************Drop down List For status****************************/
	
	
	public static List<String> getStatusList()
	{
  
		ArrayList<String> statusList=new ArrayList<String>();
		
		statusList.add("Created");
		statusList.add("Requested");
		statusList.add("Approved");
		statusList.add("Rejected");
		statusList.add("Executed");
	
		return statusList;
	}
	
	
	
	
	

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getPocName() {
		return pocName;
	}


	public void setPocName(String pocName) {
		this.pocName = pocName;
	}


	public String getPocContact() {
		return pocContact;
	}


	public void setPocContact(String pocContact) {
		this.pocContact = pocContact;
	}


	public String getPocEmail() {
		return pocEmail;
	}


	public void setPocEmail(String pocEmail) {
		this.pocEmail = pocEmail;
	}


	public String getFrom_time() {
		return from_time;
	}


	public void setFrom_time(String from_time) {
		this.from_time = from_time;
	}


//	public String getTo_time() {
//		return to_time;
//	}
//
//
//	public void setTo_time(String to_time) {
//		this.to_time = to_time;
//	}


	public Date getFrom_dt() {
		return from_dt;
	}


	public void setFrom_dt(Date from_dt) {
		this.from_dt = from_dt;
	}


//	public Date getTo_dt() {
//		return to_dt;
//	}
//
//
//	public void setTo_dt(Date to_dt) {
//		this.to_dt = to_dt;
//	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<CustomerTrainingSpeaker> getSpreakersLis() {
		return spreakersLis;
	}


	public void setSpreakersLis(List<CustomerTrainingSpeaker> spreakersLis) {
		this.spreakersLis = spreakersLis;
	}


	public List<CustomerTrainingAttendies> getAttendiesLis() {
		return attendiesLis;
	}


	public void setAttendiesLis(List<CustomerTrainingAttendies> attendiesLis) {
		this.attendiesLis = attendiesLis;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getSmsTemplate() {
		return smsTemplate;
	}


	public void setSmsTemplate(String smsTemplate) {
		this.smsTemplate = smsTemplate;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}
	
	
	public boolean isSendSMS() {
		return sendSMS;
	}

	public void setSendSMS(boolean sendSMS) {
		this.sendSMS = sendSMS;
	}

	public boolean isSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public boolean isEventManagement() {
		return eventManagement;
	}

	public void setEventManagement(boolean eventManagement) {
		this.eventManagement = eventManagement;
	}

	
	//****************************react on approval logic **********************


	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		super.reactOnApproval();
		System.out.println("Inside Approval..................");
		
			if(this.getStatus().equals(ConcreteBusinessProcess.APPROVED))
			{
			GenricServiceImpl gsimpl= new GenricServiceImpl();
			
			System.out.println("this.getAttendiesLis() sizzze=================="+this.getAttendiesLis().size());
				for(int i=0;i<this.getAttendiesLis().size();i++){
					
					if(this.getAttendiesLis().get(i).getCustId()==0){
				Customer cust = new Customer();
				cust.setFirstName(this.getAttendiesLis().get(i).getCustFirstName());
				cust.setLastName(this.getAttendiesLis().get(i).getCustLastName());
				
				
				if(!this.getAttendiesLis().get(i).getCustEmail().equals("")){
				cust.setEmail(this.getAttendiesLis().get(i).getCustEmail());
				}
				
				if(!this.getAttendiesLis().get(i).getCustCell().equals("")){
				cust.setCellNumber1(Long.parseLong(this.getAttendiesLis().get(i).getCustCell()));
				}
				
				cust.setNewCustomerFlag(true);
				cust.setCompanyId(this.getCompanyId());
				gsimpl.save(cust);
			}
			}
			}
	}
	
	
	

}
