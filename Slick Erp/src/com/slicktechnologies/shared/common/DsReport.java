package com.slicktechnologies.shared.common;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;

@Entity
public class DsReport extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6295839946318346864L;

	@Index
	public String reportName;
	@Index
	public String entityName;
	@Index
	public String reportLink;
	
	@Index
	public String role;
	
	@Index
	protected boolean status;;
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getReportLink() {
		return reportLink;
	}
	public void setReportLink(String reportLink) {
		this.reportLink = reportLink;
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		DsReport obj=(DsReport) m;
		String name = obj.getReportName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.reportName.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
		if(obj.id==null)
		{
			if(name.equals(currentName))
				return true;
			
			else
				return false;
		}
		//while updating a Old object
		else
		{
			if(obj.id.equals(id))
				return false;
			
			else if (name.equals(currentName))
				return true;
			
			else
				return false;	
		}			
	}
	
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString()
	{
		return reportName.toString();
	}
	
//	@OnSave
//	@GwtIncompatible
//	public void setReportNameStatus(){
//		System.out.println("inside setReportNameStatus");
//		List<DsReportDetails> DsReoprtList=ofy().load().type(DsReportDetails.class).filter("reportName", this.getReportName()).list();
//		for(DsReportDetails obj:DsReoprtList){
//			obj.setStatus(this.getStatus());
//			ofy().save().entity(obj).now();
//		}
//		
//		System.out.println(" setReportNameStatus value "+this.getStatus());
//		
//	}
	
}
