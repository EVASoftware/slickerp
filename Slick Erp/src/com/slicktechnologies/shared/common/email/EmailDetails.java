package com.slicktechnologies.shared.common.email;

import java.io.Serializable;
import java.util.ArrayList;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Embed
public class EmailDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5175508484076527936L;
	
	protected String fromEmailid;
	
	protected ArrayList<String> toEmailId;
	
	protected ArrayList<String> ccEmailId;
	
	protected ArrayList<String> bccEmailId;

	protected String subject;

	protected String emailBody;
	
	protected boolean pdfAttachment;
	
	protected DocumentUpload uploadedDocument;

	protected SuperModel model;
	
	protected String entityName;
	
	protected String appId;
	
	protected String appURL;
	
	protected String pdfURL;
	
	protected String landingPageText;
	
	protected int documentId;
	
	protected boolean renewalEmailReminder;
	
	
	public EmailDetails(){
		fromEmailid = "";
		toEmailId = new ArrayList<String>();
		ccEmailId = new ArrayList<String>();
		bccEmailId = new ArrayList<String>();
		subject = "";
		emailBody = "";
		entityName = "";
		appId = "";
		appURL = "";
		pdfURL = "";
		landingPageText ="";
		renewalEmailReminder = false;
	}

	public String getFromEmailid() {
		return fromEmailid;
	}

	public void setFromEmailid(String fromEmailid) {
		this.fromEmailid = fromEmailid;
	}

	public ArrayList<String> getToEmailId() {
		return toEmailId;
	}

	public void setToEmailId(ArrayList<String> toEmailId) {
		this.toEmailId = toEmailId;
	}

	public ArrayList<String> getCcEmailId() {
		return ccEmailId;
	}

	public void setCcEmailId(ArrayList<String> ccEmailId) {
		this.ccEmailId = ccEmailId;
	}

	public ArrayList<String> getBccEmailId() {
		return bccEmailId;
	}

	public void setBccEmailId(ArrayList<String> bccEmailId) {
		this.bccEmailId = bccEmailId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public boolean isPdfAttachment() {
		return pdfAttachment;
	}

	public void setPdfAttachment(boolean pdfAttachment) {
		this.pdfAttachment = pdfAttachment;
	}

	public DocumentUpload getUploadedDocument() {
		return uploadedDocument;
	}

	public void setUploadedDocument(DocumentUpload uploadedDocument) {
		this.uploadedDocument = uploadedDocument;
	}


	public String getEntityName() {
		return entityName;
	}

	public String getLandingPageText() {
		return landingPageText;
	}

	public void setLandingPageText(String landingPageText) {
		this.landingPageText = landingPageText;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public SuperModel getModel() {
		return model;
	}

	public void setModel(SuperModel model) {
		this.model = model;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppURL() {
		return appURL;
	}

	public void setAppURL(String appURL) {
		this.appURL = appURL;
	}

	public String getPdfURL() {
		return pdfURL;
	}

	public void setPdfURL(String pdfURL) {
		this.pdfURL = pdfURL;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public boolean isRenewalEmailReminder() {
		return renewalEmailReminder;
	}

	public void setRenewalEmailReminder(boolean renewalEmailReminder) {
		this.renewalEmailReminder = renewalEmailReminder;
	}



	
	
	
	
	
	

}
