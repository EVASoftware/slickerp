package com.slicktechnologies.shared.common.email;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.State;

@Entity 
public class EmailTemplateConfiguration  extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3867807700908296575L;

	
	
	
	@Index
	protected String moduleName;
	
	@Index
	protected String documentName;
	
	@Index
	protected String templateName;
	
	@Index
	protected boolean status;
	
	protected String description;

	@Index
	protected String communicationChannel;
	
	@Index 
	protected boolean automaticMsg;
	
	@Index
	protected boolean sendMsgAuto;
	
	public EmailTemplateConfiguration(){
		super();
		moduleName = "";
		documentName ="";
		templateName ="";
		description ="";
		communicationChannel ="";
		automaticMsg = false;
		sendMsgAuto = false;
	}
	
	
	
	public String getModuleName() {
		return moduleName;
	}


	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}


	public String getDocumentName() {
		return documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	
	
	
	public String getTemplateName() {
		return templateName;
	}



	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}




	public String getCommunicationChannel() {
		return communicationChannel;
	}



	public void setCommunicationChannel(String communicationChannel) {
		this.communicationChannel = communicationChannel;
	}






	public boolean isAutomaticMsg() {
		return automaticMsg;
	}



	public void setAutomaticMsg(boolean automaticMsg) {
		this.automaticMsg = automaticMsg;
	}



	public boolean isSendMsgAuto() {
		return sendMsgAuto;
	}



	public void setSendMsgAuto(boolean sendMsgAuto) {
		this.sendMsgAuto = sendMsgAuto;
	}



	@Override
	public boolean isDuplicate(SuperModel model) {
		
		EmailTemplateConfiguration obj=(EmailTemplateConfiguration) model;
		String moduleName = obj.getModuleName().trim();
		moduleName=moduleName.replaceAll("\\s","");
		moduleName=moduleName.toLowerCase();
		
		String currentModuleName=this.moduleName.trim();
		currentModuleName=currentModuleName.replaceAll("\\s","");
		currentModuleName=currentModuleName.toLowerCase();
		
		String documentName = obj.getDocumentName().trim();
		documentName=documentName.replaceAll("\\s","");
		documentName=documentName.toLowerCase();
		
		String currentDocumentName=this.documentName.trim();
		currentDocumentName=currentDocumentName.replaceAll("\\s","");
		currentDocumentName=currentDocumentName.toLowerCase();
		
		String communicationChannelName = obj.getCommunicationChannel().trim();
		communicationChannelName=communicationChannelName.replaceAll("\\s","");
		communicationChannelName=communicationChannelName.toLowerCase();
		
		String currentcommunicationChannelName=this.communicationChannel.trim();
		currentcommunicationChannelName=currentcommunicationChannelName.replaceAll("\\s","");
		currentcommunicationChannelName=currentcommunicationChannelName.toLowerCase();
		
		String templateName = obj.getTemplateName().trim();
		templateName=templateName.replaceAll("\\s","");
		templateName=templateName.toLowerCase();
		
		String currenttemplateName= "";
		if(this.templateName!=null)
			currenttemplateName =this.templateName.trim();
		currenttemplateName=currenttemplateName.replaceAll("\\s","");
		currenttemplateName=currenttemplateName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(moduleName.equals(currentModuleName) && documentName.equals(currentDocumentName) &&
							communicationChannelName.equals(currentcommunicationChannelName) && templateName.equals(currenttemplateName)) {
						return true;
					}
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if(moduleName.equals(currentModuleName) && documentName.equals(currentDocumentName) &&
							communicationChannelName.equals(currentcommunicationChannelName) && templateName.equals(currenttemplateName)) {
						return true;
					}
					else
						return false;	
				}	
		
	}

}
