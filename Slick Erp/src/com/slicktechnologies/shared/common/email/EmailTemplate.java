package com.slicktechnologies.shared.common.email;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;

@Entity
public class EmailTemplate extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1467989056248488410L;

	
	@Index
	protected String templateName;
	
	@Index
	protected boolean templateStatus;
	
	protected String subject;
	
	protected String emailBody;
	
	protected boolean pdfattachment;


	public EmailTemplate(){
		super();
		templateName ="";
		subject="";
		emailBody ="";
		pdfattachment = false;
	}
	
	
	public String getTemplateName() {
		return templateName;
	}



	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public boolean isTemplateStatus() {
		return templateStatus;
	}



	public void setTemplateStatus(boolean templateStatus) {
		this.templateStatus = templateStatus;
	}


	public String getSubject() {
		return subject;
	}



	public void setSubject(String subject) {
		this.subject = subject;
	}



	public String getEmailBody() {
		return emailBody;
	}



	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}



	public boolean isPdfattachment() {
		return pdfattachment;
	}


	public void setPdfattachment(boolean pdfattachment) {
		this.pdfattachment = pdfattachment;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {

		
		EmailTemplate entity = (EmailTemplate) m;
		String name = entity.getTemplateName().trim();
		String curname=this.templateName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	
	
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return templateName;
	}

	
	
}
