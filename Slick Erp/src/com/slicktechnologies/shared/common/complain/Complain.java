package com.slicktechnologies.shared.common.complain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.android.analytics.AnylaticsDataCreation;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customersupport.ServiceDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class Complain extends SuperModel{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3377813688207556454L;
//
//	@Index
//	protected String custFirstName;
//	
//	@Index
//	protected String custMiddleName;
//	
//	@Index
//	protected String custLastName;
//	protected Long custCell;
//	protected String custEmail;
//	
//	@Index
//	protected int custCount;
//	@Index
//	protected String custFullName;
	
	@Index 
	protected PersonInfo personinfo;
	
	@Index
	protected Date complainDate;
	
	@Index
	protected String complainTitle;
	
	protected String complaintime;
	@Index
	protected String SalesPerson;
	@Index
	protected String branch;
	protected String priority;
	
	
	protected ArrayList<SalesLineItem>items;
	@Index
	protected Date date;
	protected String time;
	
	@Index
	protected Date dueDate;
	@Index
	protected String assignto;
	protected String destription;
	
	@Index
	protected ProductInfo pic;
	protected String brandName;
	protected String modelNumber;
	protected String serialNumber;	
	
	public ArrayList<ComplainList> complainList;
	
	@Index
	protected Boolean bbillable;
	 
	protected String remark;
	@Index
	protected Date serviceDate;
	protected String servicetime;
	@Index
	protected String compStatus;
	
	@Index
	protected Integer existingContractId;
	
	@Index
	protected Integer existingServiceId;
	
	@Index
	protected Date existingServiceDate;
	
	@Index
	protected String serviceEngForExistingService;
	
	@Index
	protected Integer quatationId;
	
	@Index
	protected Integer contrtactId;
	
	@Index
	protected Integer serviceId;
	
	protected String callerName;
	protected long callerCellNo;
	
	
	private Double netPayable;
	private Double amountReceived;
	private Double balance;
	protected Integer ticketId;
	private Double extraCharges;
	 
	@Index
	protected String customerFeedback;
	
	protected String serviceDetails;
	
	/** date 13.03.2018 added by komal for nbhc **/
	@Index
	protected String email;
	/**end komal **/
	/** date 19.4.2018 added by komal (it can be used when user directly create new service directly)**/
	@Index
	protected Date newServiceDate;
	@Index
	protected String productName;
	List<Service> serviceList;
	protected String newServiceTime;

	/**
	 * nidhi
	 * 9-08-2018
	 */
	protected String proSerialNo;
	
	/*** Date 16-10-2018 By Vijay for Complain Ticket Category **/
	@Index
	protected String Category;
	/*** Date 17-10-2018 By Vijay for development Efforts(hours) and Actual Hours **/
	protected double plannedHours;
	protected double earnedHours;
	/** Date 19-10-2018 By Vijay For Status wise history maintain ***/
	protected ArrayList<ComplainList> statusHistory;
	protected Date planFromDate;
	protected Date planToDate;
	
	/**
	 * Date 22-05-2019 by vijay for NBHC CCPM
	 */
	protected String customerBranch;
	protected ArrayList<ServiceDetails> servicedetails;
	protected String city; 
	@Index
	protected Integer assesmentId;
	
		protected String serviceAddress;

	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@Index
	Date lastUpdatedDate;
	
	/**
	 * @author Anil
	 * @since 03-02-2020
	 * Adding following fields to calculate actual turn around time for premium tech
	 */
	@Index
	Date expectedCompletionDate;
	@Index
	String expectedCompletionTime;
	@Index
	Date assignedDate;
	@Index
	String assignedTime;
	@Index
	Date completionDate;
	@Index
	String completionTime;
	
	/**
	 * @author Anil
	 * @since 13-05-2020
	 * changed type from double to string
	 */
	String tat_sla;
	String tat_Tech;
	String tat_Actual;
	String tat_Sup;
	
	/**
	 * @author Anil
	 * @since 10-06-2020
	 */
	
	double tat_sla_num;
	double tat_Tech_num;
	double tat_Actual_num;
	double tat_Sup_num;
	
	/**
	 * @author Anil
	 * @since 22-06-2020
	 */
	
	@Index
	int assetId;
	String mfgNum;
	String assetUnit;
	Date mfgDate;
	Date replacementDate;
	String componentName;
	
	protected DocumentUpload document;
	
	protected String serviceType;
	protected String productGroup;
	
	public Complain() {
//		 personinfo=new PersonInfo();
//		 personinfo.setCount(-1);
		 
		 items = new ArrayList<SalesLineItem>();
		 complainList = new ArrayList<ComplainList>();
		 serviceDetails="";
		 /** date 19.4.2018 added by komal (it can be used when user directly create new service directly)**/
		 serviceList = new ArrayList<Service>();
		 serviceDetails="";
		 /** date 19.4.2018 added by komal (it can be used when user directly create new service directly)**/
		 newServiceTime = "";
		 proSerialNo = "" ; //nidhi
		 Category="";
		 statusHistory = new ArrayList<ComplainList>();
		 customerBranch = "";
		 servicedetails = new ArrayList<ServiceDetails>();
		 city = "";
		 
		 document = new DocumentUpload();
		 
		 serviceType="";
		 productGroup="";
	 }
	
	/***********************************Getter and Setter********************************************/
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@OnSave
	@GwtIncompatible
	public void setLastUpdatedDate(){
		setLastUpdatedDate(DateUtility.setTimeToMidOfDay( new Date()));
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	

	

	public PersonInfo getPersoninfo() {
		return personinfo;
	}





	public void setPersoninfo(PersonInfo personinfo) {
		this.personinfo = personinfo;
	}





	public Date getComplainDate() {
		return complainDate;
	}





	public void setComplainDate(Date complainDate) {
		this.complainDate = complainDate;
	}





	public String getComplaintime() {
		return complaintime;
	}





	public void setComplaintime(String complaintime) {
		this.complaintime = complaintime;
	}





	public String getSalesPerson() {
		return SalesPerson;
	}





	public void setSalesPerson(String salesPerson) {
		SalesPerson = salesPerson;
	}





	public String getBranch() {
		return branch;
	}





	public void setBranch(String branch) {
		this.branch = branch;
	}





	public String getPriority() {
		return priority;
	}





	public void setPriority(String priority) {
		this.priority = priority;
	}





	public ArrayList<SalesLineItem> getItems() {
		return items;
	}





	public void setItems(ArrayList<SalesLineItem> items) {
		this.items = items;
	}





	public Date getDueDate() {
		return dueDate;
	}





	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}





	public String getAssignto() {
		return assignto;
	}





	public void setAssignto(String assignto) {
		this.assignto = assignto;
	}





	public String getDestription() {
		return destription;
	}





	public void setDestription(String destription) {
		this.destription = destription;
	}





	



	public ProductInfo getPic() {
		return pic;
	}





	public void setPic(ProductInfo pic) {
		this.pic = pic;
	}





	public String getBrandName() {
		return brandName;
	}





	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}





	public String getModelNumber() {
		if(this.modelNumber!=null){
			return modelNumber;
		}else{
			return "";
		}
		
	}





	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}





	public String getSerialNumber() {
		return serialNumber;
	}





	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}





	public ArrayList<ComplainList> getComplainList() {
		return complainList;
	}





	public void setComplainList(ArrayList<ComplainList> complainList) {
		this.complainList = complainList;
	}
	
	
	
	
	public Boolean getBbillable() {
		return bbillable;
	}





	public void setBbillable(Boolean bbillable) {
		this.bbillable = bbillable;
	}





	public String getRemark() {
		return remark;
	}





	public void setRemark(String remark) {
		this.remark = remark;
	}





	public Date getDate() {
		return date;
	}





	public void setDate(Date date) {
		this.date = date;
	}





	public String getTime() {
		return time;
	}





	public void setTime(String time) {
		this.time = time;
	}


	
	



	public Date getServiceDate() {
		return serviceDate;
	}





	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}





	public String getServicetime() {
		return servicetime;
	}





	public void setServicetime(String servicetime) {
		this.servicetime = servicetime;
	}





	public String getCompStatus() {
		return compStatus;
	}





	public void setCompStatus(String compStatus) {
		this.compStatus = compStatus;
	}


	public Integer getExistingContractId() {
		return existingContractId;
	}

	public void setExistingContractId(Integer existingContractId) {
		this.existingContractId = existingContractId;
	}

	public Integer getQuatationId() {
		return quatationId;
	}

	public void setQuatationId(Integer quatationId) {
		this.quatationId = quatationId;
	}

	public Integer getContrtactId() {
		return contrtactId;
	}

	public void setContrtactId(Integer contrtactId) {
		this.contrtactId = contrtactId;
	}





	public Integer getExistingServiceId() {
		return existingServiceId;
	}

	public void setExistingServiceId(Integer existingServiceId) {
		this.existingServiceId = existingServiceId;
	}
	public String getServiceEngForExistingService() {
		return serviceEngForExistingService;
	}

	public void setServiceEngForExistingService(String serviceEngForExistingService) {
		this.serviceEngForExistingService = serviceEngForExistingService;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	public String getComplainTitle() {
		return complainTitle;
	}
	public void setComplainTitle(String complainTitle) {
		this.complainTitle = complainTitle;
	}
	public String getCallerName() {
		return callerName;
	}
	public void setCallerName(String callerName) {
		this.callerName = callerName;
	}
	public long getCallerCellNo() {
		return callerCellNo;
	}
	public void setCallerCellNo(long callerCellNo) {
		this.callerCellNo = callerCellNo;
	}
	public Double getNetPayable() {
		return netPayable;
	}
	public void setNetPayable(Double netPayable) {
		this.netPayable = netPayable;
	}
	public Double getAmountReceived() {
		return amountReceived;
	}
	public void setAmountReceived(Double amountReceived) {
		this.amountReceived = amountReceived;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public Double getExtraCharges() {
		return extraCharges;
	}
	public void setExtraCharges(Double extraCharges) {
		this.extraCharges = extraCharges;
	}
	public String getCustomerFeedback() {
		return customerFeedback;
	}
	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}
	public Date getExistingServiceDate() {
		return existingServiceDate;
	}
	public void setExistingServiceDate(Date existingServiceDate) {
		this.existingServiceDate = existingServiceDate;
	}
	public String getServiceDetails() {
		return serviceDetails;
	}
	public void setServiceDetails(String serviceDetails) {
		this.serviceDetails = serviceDetails;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getNewServiceDate() {
		return newServiceDate;
	}
	public void setNewServiceDate(Date newServiceDate) {
		this.newServiceDate = newServiceDate;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<Service> getServiceList() {
		return serviceList;
	}
	public void setServiceList(List<Service> serviceList) {
		this.serviceList = serviceList;
	}
	public String getNewServiceTime() {
		return newServiceTime;
	}
	public void setNewServiceTime(String newServiceTime) {
		this.newServiceTime = newServiceTime;
	}
	
	
	public String getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(String serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public String getProSerialNo() {
		if(this.proSerialNo!=null){
			return proSerialNo;
		}else{
			return "";
		}
	}
	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	
	public double getPlannedHours() {
		return plannedHours;
	}
	public void setPlannedHours(double plannedHours) {
		this.plannedHours = plannedHours;
	}
	
	/**
	 * Date 17-10-2018 By Vijay
	 * Des :- To Maintain Document history with process config.
	 * whenever user will change the status then its hostory will save
	 */
	@OnSave
	@GwtIncompatible
	private void saveDocumentHistory(){
		Logger logger = Logger.getLogger("Name of logger");
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		SimpleDateFormat timeDateFormat = new SimpleDateFormat("HH:mm:ss");
		timeDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(this.id==null){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnableComplainStatusHistory", this.getCompanyId())){
				setHistoryData();
			}	
			
			SmsServiceImpl smsimpl = new SmsServiceImpl();
			Company companyEntity = ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
//			smsimpl.checkSMSConfigAndSendComplaintResolvedSMS(this.getCompanyId(), this.getNewServiceDate(), this.getPersoninfo().getFullName(), this.getCount(), companyEntity.getBusinessUnitName(), this.getPersoninfo().getCellNumber(), this.getPersoninfo().getCount());
			
			
			String serviceId = "";
			try {
				String servicedetails = this.getServiceDetails();
				String [] servicedet = servicedetails.split(" ");
				serviceId = servicedet[1];
			} catch (Exception e) {
			}
			
			smsimpl.checkSMSConfigAndSendComplaintRegisterSMS(this.getCompanyId(), serviceId, this.getPersoninfo().getFullName(), this.getCount(), companyEntity.getBusinessUnitName(), this.getPersoninfo().getCellNumber(), this.getPersoninfo().getCount());
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId())
				 	.filter("count", this.getPersoninfo().getCount()).first().now();
			
			Service serviceentity=null;
			
			String serviceIdstr = "";
			try {
				String servicedetails = this.getServiceDetails();
				logger.log(Level.SEVERE,"servicedetails="+servicedetails);
				String [] servicedet = servicedetails.split("/");
				logger.log(Level.SEVERE,"servicedetails 0="+servicedet[0]);
				logger.log(Level.SEVERE,"servicedetails 1="+servicedet[1]);
				logger.log(Level.SEVERE,"servicedetails 2="+servicedet[2]);
				serviceIdstr = servicedet[2];
			} catch (Exception e) {
			}
			int serId=0;
			if(serviceIdstr!=null&&!serviceIdstr.equals(""))
				serId=Integer.parseInt(serviceIdstr);
			if(serId>0) {				
				if(this.getContrtactId()!=null&& this.getContrtactId()!=0){
					 serviceentity = ofy().load().type(Service.class).filter("companyId",  this.getCompanyId())
							.filter("count", serId).filter("contractCount",this.getExistingContractId()).first().now();
				}
				else{
					 serviceentity = ofy().load().type(Service.class).filter("companyId",  this.getCompanyId())
							.filter("count", serId).first().now();
				}
			}
			
			
			AnylaticsDataCreation api=new AnylaticsDataCreation();
			api.sendComplainCreationEmail(this, customer, serviceentity);
			
		}else{
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Complain", "EnableComplainStatusHistory", this.getCompanyId())){
				if(this.getStatusHistory()==null || this.getStatusHistory().size()==0){
					setHistoryData();
				}else{
					if(this.getStatusHistory().size()==1){
						if(!this.getStatusHistory().get(0).getStatus().equals(this.getCompStatus())){
							System.out.println("Lats status =="+this.getCompStatus());
						updateHistoryData();
						}
					}
					else if(!this.getStatusHistory().get(this.getStatusHistory().size()-1).getStatus().equals(this.getCompStatus())){
						updateHistoryData();
					}
				}
			}

			/**
			 * @author Anil
			 * @sinnce 22-06-2020
			 * updating component details on service
			 */
			if(this.getServiceId()!=null&&this.getAssetId()!=0){
				
				ObjectifyService.reset();
				ofy().consistency(Consistency.STRONG);
				ofy().clear();
				
				DateFormat fmt=new SimpleDateFormat("dd/MM/yyyy");
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				Service service=ofy().load().type(Service.class).filter("companyId", this.getCompanyId()).filter("ticketNumber", this.getCount()).first().now();
				if(service!=null){					
					logger.log(Level.SEVERE,service.getCount()+"/"+service.isServiceScheduled()+"/"+service.getServiceNumber());
					service.setAssetId(this.getAssetId());
					service.setAssetUnit(this.getAssetUnit());
					service.setMfgNo(this.getMfgNum());
					service.setMfgDate(this.getMfgDate());
					service.setReplacementDate(this.getReplacementDate());
					
					if(this.getCompStatus().equals("Cancelled")){
						service.setStatus("Cancelled");
						service.setCancellationDate(new Date());
						if(service.getComment()!=null){
							service.setReasonForChange("This service is cancelled from complain."+"\n"+service.getComment());
						}else{
							service.setReasonForChange("This service is cancelled from complain.");
						}
					}
					
					/**
					 * @author Anil
					 * @since 23-06-2020
					 * setting asset id,asset unit and mfg num in description
					 */
					String descHead="";
					String descVal="";
					if(service.getAssetId()!=0){
						descHead="Asset Id";
						descVal=service.getAssetId()+"";
					}
					if(service.getAssetUnit()!=null&&!service.getAssetUnit().equals("")){
						if(!descHead.equals("")){
							descHead=descHead+"/"+"Asset Unit";
							descVal=descVal+"/"+service.getAssetUnit();
						}else{
							descHead="Asset Unit";
							descVal=service.getAssetUnit();
						}
					}
					if(service.getMfgNo()!=null&&!service.getMfgNo().equals("")){
						if(!descHead.equals("")){
							descHead=descHead+"/"+"Mfg No.";
							descVal=descVal+"/"+service.getMfgNo();
						}else{
							descHead="Mfg No.";
							descVal=service.getMfgNo();
						}
					}
					String description="";
					if(!descHead.equals("")){
						description=descHead+" : "+descVal+" ";
					}
					
					if(this.getExpectedCompletionDate()!=null&&this.getExpectedCompletionTime()!=null&&!this.getExpectedCompletionTime().equals("")) {
						description=description+"Exp Date/Time : "+fmt.format(this.getExpectedCompletionDate())+"/"+this.getExpectedCompletionTime();
					}
					
					
					if(!description.equals("")){
						description=description+" "+this.getDestription();
					}else{
						description=this.getDestription();
					}
					service.setDescription(description);
					
					ofy().save().entity(service).now();
				}
			}
			
				}
		
		updateTatValueInNumber();
		
	}
	@GwtIncompatible
	private void setHistoryData() {
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		SimpleDateFormat timeDateFormat = new SimpleDateFormat("HH:mm:ss");
		timeDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		ArrayList<ComplainList> statustHistoryList = new ArrayList<ComplainList>();
		ComplainList docHistory = new ComplainList();
		Date todayDate = new Date();
		Date today = null;
		try {
			today = dateFormat.parse(dateFormat.format(todayDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		docHistory.setDate(today);
		docHistory.setStatus(this.getCompStatus());
		docHistory.setTime(timeDateFormat.format(today));
		if(UserConfiguration.getCompanyId()!=null){
		     createdBy=UserConfiguration.getUserconfig().getUser().getEmployeeName();
		}
		docHistory.setUserName(createdBy);
		statustHistoryList.add(docHistory);
		this.setStatusHistory(statustHistoryList);
	}
	@GwtIncompatible
	private void updateHistoryData() {
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		SimpleDateFormat timeDateFormat = new SimpleDateFormat("HH:mm:ss");
		timeDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		ArrayList<ComplainList>statusHistorylist = this.getStatusHistory();
		ComplainList statusHistory = new ComplainList();
		Date todayDate = new Date();
		Date today = null;
		try {
			today = dateFormat.parse(dateFormat.format(todayDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		statusHistory.setDate(today);
		statusHistory.setStatus(this.getCompStatus());
		statusHistory.setTime(timeDateFormat.format(today));
		if(UserConfiguration.getCompanyId()!=null){
		     createdBy=UserConfiguration.getUserconfig().getUser().getEmployeeName();
		}
		statusHistory.setUserName(createdBy);
		statusHistorylist.add(statusHistory);
		
		this.setStatusHistory(statusHistorylist);
		
	}
	
	/**
	 * ends here
	 */
	public ArrayList<ComplainList> getStatusHistory() {
		return statusHistory;
	}
	public void setStatusHistory(ArrayList<ComplainList> statusHistory) {
		this.statusHistory = statusHistory;
	}
	public double getEarnedHours() {
		return earnedHours;
	}
	public void setEarnedHours(double earnedHours) {
		this.earnedHours = earnedHours;
	}
	public Date getPlanFromDate() {
		return planFromDate;
	}
	public void setPlanFromDate(Date planFromDate) {
		this.planFromDate = planFromDate;
	}
	public Date getPlanToDate() {
		return planToDate;
	}
	public void setPlanToDate(Date planToDate) {
		this.planToDate = planToDate;
	}
	public String getCustomerBranch() {
		return customerBranch;
	}
	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}
	
	public ArrayList<ServiceDetails> getServicedetails() {
		return servicedetails;
	}
	public void setServicedetails(ArrayList<ServiceDetails> servicedetails) {
		this.servicedetails = servicedetails;
	}

	public static List<String> getStatusList() {
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add("Created");
		statuslist.add("Completed");
		return statuslist;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	 public Integer getAssesmentId() {
			return assesmentId;
		}

		public void setAssesmentId(Integer assesmentId) {
			this.assesmentId = assesmentId;
		}
	public Date getExpectedCompletionDate() {
			return expectedCompletionDate;
		}

		public void setExpectedCompletionDate(Date expectedCompletionDate) {
			this.expectedCompletionDate = expectedCompletionDate;
		}

		public String getExpectedCompletionTime() {
			return expectedCompletionTime;
		}

		public void setExpectedCompletionTime(String expectedCompeletionTime) {
			this.expectedCompletionTime = expectedCompeletionTime;
		}

		public Date getAssignedDate() {
			return assignedDate;
		}

		public void setAssignedDate(Date assignedDate) {
			this.assignedDate = assignedDate;
		}

		public String getAssignedTime() {
			return assignedTime;
		}

		public void setAssignedTime(String assignedTime) {
			this.assignedTime = assignedTime;
		}

		public Date getCompletionDate() {
			return completionDate;
		}

		public void setCompletionDate(Date completionDate) {
			this.completionDate = completionDate;
		}

		public String getCompletionTime() {
			return completionTime;
		}

		public void setCompletionTime(String completionTime) {
			this.completionTime = completionTime;
		}

		public String getTat_sla() {
			return tat_sla;
		}

		public void setTat_sla(String tat_sla) {
			this.tat_sla = tat_sla;
		}

		public String getTat_Tech() {
			return tat_Tech;
		}

		public void setTat_Tech(String tat_Tech) {
			this.tat_Tech = tat_Tech;
		}

		public String getTat_Actual() {
			return tat_Actual;
		}

		public void setTat_Actual(String tat_Actual) {
			this.tat_Actual = tat_Actual;
		}

		public String getTat_Sup() {
			return tat_Sup;
		}

		public void setTat_Sup(String tat_Sup) {
			this.tat_Sup = tat_Sup;
		}
		
		
		
		
	
		public double getTat_sla_num() {
			return tat_sla_num;
		}

		public void setTat_sla_num(double tat_sla_num) {
			this.tat_sla_num = tat_sla_num;
		}

		public double getTat_Tech_num() {
			return tat_Tech_num;
		}

		public void setTat_Tech_num(double tat_Tech_num) {
			this.tat_Tech_num = tat_Tech_num;
		}

		public double getTat_Actual_num() {
			return tat_Actual_num;
		}

		public void setTat_Actual_num(double tat_Actual_num) {
			this.tat_Actual_num = tat_Actual_num;
		}

		public double getTat_Sup_num() {
			return tat_Sup_num;
		}

		public void setTat_Sup_num(double tat_Sup_num) {
			this.tat_Sup_num = tat_Sup_num;
		}
		
		

		public int getAssetId() {
			return assetId;
		}

		public void setAssetId(int assetId) {
			this.assetId = assetId;
		}

		public String getMfgNum() {
			return mfgNum;
		}

		public void setMfgNum(String mfgNum) {
			this.mfgNum = mfgNum;
		}

		public String getAssetUnit() {
			return assetUnit;
		}

		public void setAssetUnit(String assetUnit) {
			this.assetUnit = assetUnit;
		}

		public Date getMfgDate() {
			return mfgDate;
		}

		public void setMfgDate(Date mfgDate) {
			this.mfgDate = mfgDate;
		}

		public Date getReplacementDate() {
			return replacementDate;
		}

		public void setReplacementDate(Date replacementDate) {
			this.replacementDate = replacementDate;
		}

		public String getComponentName() {
			return componentName;
		}

		public void setComponentName(String componentName) {
			this.componentName = componentName;
		}

		public Double convertTimeToNumbers(String time){
			Double hrsInNum=0d;
			try{
				String timeArr[] =time.split(":");
				String hh = timeArr[0];
				String mm = timeArr[1];
				double mins=Integer.parseInt(mm)/60.0;
				hrsInNum=Integer.parseInt(hh)+mins;
			}catch(Exception e){
				
			}
			return hrsInNum;
		}
		
		
		public void updateTatValueInNumber(){
			if(this.tat_sla!=null&&!this.tat_sla.equals("")){
				tat_sla_num=convertTimeToNumbers(this.tat_sla);
			}
			if(this.tat_Actual!=null&&!this.tat_Actual.equals("")){
				tat_Actual_num=convertTimeToNumbers(this.tat_Actual);
			}
			if(this.tat_Sup!=null&&!this.tat_Sup.equals("")){
				tat_Sup_num=convertTimeToNumbers(this.tat_Sup);
			}
			if(this.tat_Tech!=null&&!this.tat_Tech.equals("")){
				tat_Tech_num=convertTimeToNumbers(this.tat_Tech);
			}
		}
		
		/**
		 * @author Vijay Chougule Date 19-06-2020
		 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
		 */

		@OnSave
		@GwtIncompatible
		public void setTimeMidOfTheDayToDate()
		{
			DateUtility dateUtility = new DateUtility();
			if(this.getServiceDate()!=null){
				this.setServiceDate(dateUtility.setTimeMidOftheDayToDate(this.getServiceDate()));
			}
			if(this.getNewServiceDate()!=null){
				this.setNewServiceDate(dateUtility.setTimeMidOftheDayToDate(this.getNewServiceDate()));
			}
			if(this.getComplainDate()!=null){
				this.setComplainDate(dateUtility.setTimeMidOftheDayToDate(this.getComplainDate()));
			}
			if(this.getAssignedDate()!=null){
				this.setAssignedDate(dateUtility.setTimeMidOftheDayToDate(this.getAssignedDate()));
			}
			if(this.getCompletionDate()!=null){
				this.setCompletionDate(dateUtility.setTimeMidOftheDayToDate(this.getCompletionDate()));
			}
		}
		/**
		 * ends here	
		 */

		public DocumentUpload getDocument() {
			return document;
		}

		public void setDocument(DocumentUpload document) {
			this.document = document;
		}

		public String getServiceType() {
			return serviceType;
		}

		public void setServiceType(String serviceType) {
			this.serviceType = serviceType;
		}

		public String getProductGroup() {
			return productGroup;
		}

		public void setProductGroup(String productGroup) {
			this.productGroup = productGroup;
		}
		
		
}
