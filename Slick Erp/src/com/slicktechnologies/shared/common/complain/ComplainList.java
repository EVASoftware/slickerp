package com.slicktechnologies.shared.common.complain;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class ComplainList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7179589034750558848L;

	

	protected int srNo;
	@Index
	protected Date date;
	protected String time;
	protected String remark;
	protected String userName;
	
	/**
	 * Date 17-10-2018 By Vijay
	 * Des :- For EVA Status wise history
	 */
	@Index
	protected String status;
	
	/**
	 * ends here
	 */
	
	
	/********************************************getter and setter*************************************************/
	
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
