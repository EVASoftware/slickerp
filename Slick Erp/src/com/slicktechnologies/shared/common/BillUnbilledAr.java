package com.slicktechnologies.shared.common;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
/**
 * @author Viraj
 * Date: 25-01-2019
 * Description: Embed class to represent data in table
 *
 */
@Embed
public class BillUnbilledAr implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3566781362281748871L;
	
	private long id;
	private Date followUp;
	private String customerName;
	private long customerCell;
	private long contractId;
	private double salesAmount;
	private double invoiceAmt;
	private String status;
	private String branch;
	protected PersonInfo personInfo;
	protected String employee;
	/**
	 * Updated By: Viraj
	 * Date: 16-02-2016
	 * Description: To store doc type for follow up
	 */
	protected String docType;
	protected Date docDate;
	/**
	 * Updated By: Viraj
	 * Date: 13-03-2019
	 * Description: To add balance amt
	 */
	protected double balAmt;
	
	
	public double getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(double balAmt) {
		this.balAmt = balAmt;
	}
	/** Ends **/
	public Date getDocDate() {
		return docDate;
	}
	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public PersonInfo getPersonInfo() {
		return personInfo;
	}
	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getFollowUp() {
		return followUp;
	}
	public void setFollowUp(Date followUp) {
		this.followUp = followUp;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getCustomerCell() {
		return customerCell;
	}
	public void setCustomerCell(long customerCell) {
		this.customerCell = customerCell;
	}
	public long getContractId() {
		return contractId;
	}
	public void setContractId(long contractId) {
		this.contractId = contractId;
	}
	public double getSalesAmount() {
		return salesAmount;
	}
	public void setSalesAmount(double salesAmount) {
		this.salesAmount = salesAmount;
	}
	public double getInvoiceAmt() {
		return invoiceAmt;
	}
	public void setInvoiceAmt(double invoiceAmt) {
		this.invoiceAmt = invoiceAmt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	

}
/** Ends **/