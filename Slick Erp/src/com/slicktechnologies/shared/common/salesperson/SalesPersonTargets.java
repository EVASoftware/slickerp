package com.slicktechnologies.shared.common.salesperson;

import com.googlecode.objectify.annotation.Entity;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import java.util.ArrayList;
import java.util.List;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;


@Entity
public class SalesPersonTargets extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5792397526396954719L;

	

	/************************** Attributes*********************************************/
	
	int target;
	double percentage;
	double flatAmount;
	@Index
	String employeeRole;
	@Index
	String financialYear;
	@Index
	protected ArrayList<TargetInformation> targetInfoList;
	@Index
	protected ArrayList<SalesPersonInfo> employeeInfo;
	
	public SalesPersonTargets(){
		super();
		
	}
	
	
	//     getters and setters  
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	public int getTarget() {
		return target;
	}
	public void setTarget(int target) {
		this.target = target;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	public double getFlatAmount() {
		return flatAmount;
	}
	public void setFlatAmount(double flatAmount) {
		this.flatAmount = flatAmount;
	}
	public String getEmployeeRole() {
		return employeeRole;
	}
	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	public String getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}
	public ArrayList<TargetInformation> getTargetInfoList() {
		return targetInfoList;
	}
	public void setTargetInfoList(List<TargetInformation> targetInfoList) {
		ArrayList<TargetInformation> targetInfoarr = new ArrayList<TargetInformation>();
		targetInfoarr.addAll(targetInfoList);
		this.targetInfoList = targetInfoarr;
	}
	public ArrayList<SalesPersonInfo> getEmployeeInfo() {
		return employeeInfo;
	}
	public void setEmployeeInfo(List<SalesPersonInfo> employeeInfolist) {
		ArrayList<SalesPersonInfo> SalesPersonInfoarr = new ArrayList<SalesPersonInfo>();
		SalesPersonInfoarr.addAll(employeeInfolist);
		this.employeeInfo = SalesPersonInfoarr;
	}

	
	
	
}
