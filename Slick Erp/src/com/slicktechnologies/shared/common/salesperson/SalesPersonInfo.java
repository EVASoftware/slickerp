package com.slicktechnologies.shared.common.salesperson;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
@Embed
public class SalesPersonInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6624464241043902044L;
	
	
	int Id;
	String fullName;
	long CellNumber;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public long getCellNumber() {
		return CellNumber;
	}
	public void setCellNumber(long cellNumber) {
		CellNumber = cellNumber;
	}
	
	
	//   getters and setters 
	
	
}
