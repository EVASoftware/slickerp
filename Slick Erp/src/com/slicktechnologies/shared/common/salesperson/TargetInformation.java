package com.slicktechnologies.shared.common.salesperson;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
@Embed
public class TargetInformation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -416549445906793300L;

	
	/******************************** Application Attributes  ***************************/

	int target;
	double percatage;
	double flatAmount;
	String productCategory;
	String Duration;
	String financialYear;
	String Months;
	double achievedTargetAmt;
	String EmployeeName;
	
	double employeeIncentives;
	
	
	public TargetInformation(){
		productCategory="";
	}

	/****************************  getters & setters ********************************/ 
	public int getTarget() {
		return target;
	}


	public void setTarget(int target) {
		this.target = target;
	}


	public double getPercatage() {
		return percatage;
	}


	public void setPercatage(double percatage) {
		this.percatage = percatage;
	}


	public double getFlatAmount() {
		return flatAmount;
	}


	public void setFlatAmount(double flatAmount) {
		this.flatAmount = flatAmount;
	}


	public String getProductCategory() {
		return productCategory;
	}


	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}


	public String getDuration() {
		return Duration;
	}


	public void setDuration(String duration) {
		Duration = duration;
	}


	public String getFinancialYear() {
		return financialYear;
	}


	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}


	public String getMonths() {
		return Months;
	}


	public void setMonths(String months) {
		Months = months;
	}


	public double getAchievedTargetAmt() {
		return achievedTargetAmt;
	}


	public void setAchievedTargetAmt(double achievedTargetAmt) {
		this.achievedTargetAmt = achievedTargetAmt;
	}


	public String getEmployeeName() {
		return EmployeeName;
	}


	public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}


	public double getEmployeeIncentives() {
		return employeeIncentives;
	}


	public void setEmployeeIncentives(double employeeIncentives) {
		this.employeeIncentives = employeeIncentives;
	}
	
	
	
}
