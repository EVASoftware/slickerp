package com.slicktechnologies.shared.common.customerbranch;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
@Entity
public class CustomerBranchServiceLocation extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3884441995801617442L;

	@Index 
	protected PersonInfo cinfo;
	@Index
	protected int custBranchId;
	@Index
	protected String customerBranchName;
	@Index
	protected String serviceLocation;
	protected String description;
	
	@Index
	protected ArrayList<String> areaList;
	
	@Index
	protected boolean status;
	
	protected ArrayList<Area> areaList2;
	
	protected double plannedService;
	protected String frequency;
	
	public CustomerBranchServiceLocation() {
		super();
		
	}





	public PersonInfo getCinfo() {
		return cinfo;
	}





	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}





	public int getCustBranchId() {
		return custBranchId;
	}





	public void setCustBranchId(int custBranchId) {
		this.custBranchId = custBranchId;
	}





	public String getCustomerBranchName() {
		return customerBranchName;
	}





	public void setCustomerBranchName(String customerBranchName) {
		this.customerBranchName = customerBranchName;
	}





	public String getServiceLocation() {
		return serviceLocation;
	}





	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}





	public String getDescription() {
		return description;
	}





	public void setDescription(String description) {
		this.description = description;
	}



	


	public ArrayList<String> getAreaList() {
		return areaList;
	}





	public void setAreaList(List<String> areaList) {
		ArrayList<String> list=new ArrayList<String>();
		list.addAll(areaList);
		this.areaList = list;
	}





	public boolean isStatus() {
		return status;
	}





	public void setStatus(boolean status) {
		this.status = status;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		CustomerBranchServiceLocation entity=(CustomerBranchServiceLocation) m;
		
		String custName = entity.getCinfo().getFullName().trim();
		custName=custName.replaceAll("\\s","");
		custName=custName.toLowerCase();
		
		String custBranchName = entity.getCustomerBranchName().trim();
		custBranchName=custBranchName.replaceAll("\\s","");
		custBranchName=custBranchName.toLowerCase();
		
		String servLocName = entity.getServiceLocation().trim();
		servLocName=servLocName.replaceAll("\\s","");
		servLocName=servLocName.toLowerCase();
		
		
		
		String curname=this.getCinfo().getFullName().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		String curLocName=this.getCustomerBranchName().trim();
		curLocName=curLocName.replaceAll("\\s","");
		curLocName=curLocName.toLowerCase();
		
		String curWhName=this.getServiceLocation().trim();
		curWhName=curWhName.replaceAll("\\s","");
		curWhName=curWhName.toLowerCase();
		
		
		if(id==null){
			if(custName.equals(curname)&&custBranchName.equals(curLocName)&&servLocName.equals(curWhName)){
				return true;
			}else{
				return false;
			}
		}else{
			if(id.equals(entity.id)){
			  return false;	
			}
			if((custName.equals(curname)==true)&&custBranchName.equals(curLocName)==true&&servLocName.equals(curWhName)==true){
			  return true;
			}else{
			  return false;
			}
		}
	}





	@Override
	public String toString() {
		return serviceLocation;
	}





	public ArrayList<Area> getAreaList2() {
		return areaList2;
	}





	public void setAreaList2(List<Area> areaList2) {
		ArrayList<Area> arrayitems=new ArrayList<Area>();
		arrayitems.addAll(areaList2);
		this.areaList2 = arrayitems;
	}
	
	

}
