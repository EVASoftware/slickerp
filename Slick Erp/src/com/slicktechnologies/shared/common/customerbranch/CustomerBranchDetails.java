package com.slicktechnologies.shared.common.customerbranch;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.personlayer.Person;

@Entity
public class CustomerBranchDetails extends BusinessUnit implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7734042179319359145L;
	
	@Index
	protected boolean status;
	
	@Index 
	protected PersonInfo cinfo;
	
	protected Person pointOfContact;

	/** The business unit name. */
	@Index 
	protected String buisnessUnitName;

	/** The contact. */
	@Index 
	protected  Vector<Contact> contact;

	/** The social info. */
	protected  SocialInformation socialinfo;
	
	@Index 
	protected String branch;
	
/**
 * Added By Rahul on 08 Sept 2017
 */
	@Index
	protected String GSTINNumber;
	
	/** date 16.02.2018 added by komal for area wise billing**/
	@Index
	double area;
	
	@Index
	String unitOfMeasurement;
	
	@Index
	String costCenter;
	/**
	 * end komal
	 */
	
	/**
	 * @author Anil ,Date : 22-03-2019
	 * added billing address
	 */
	@Index
	Address billingAddress;
	
	
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@Index
	Date lastUpdatedDate;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 */
	String tat;
	@Index
	String tierName;

	protected String serviceAddressName;//Ashwini Patil Date:06-09-2022

	public CustomerBranchDetails() 
	{
		super();
		pointOfContact=new Person();

		//Form right now contains only one Contact object so we are manually inserting it here all subclass will
		//get One Contact free
		pointOfContact=new Person();
		Contact temp=new Contact();
		contact=new Vector<Contact>();
		contact.add(temp);
		//inserting contact in person
		temp=new Contact();
		this.pointOfContact.getContact().add(temp);
		socialinfo = new SocialInformation();
		buisnessUnitName="";
		GSTINNumber="";
		cinfo=new PersonInfo();
		serviceAddressName="";
	}
	
	
	
	public Address getBillingAddress() {
		return billingAddress;
	}



	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}



	public Boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public PersonInfo getCinfo() {
		return cinfo;
	}

	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}
	
	public Person getPoc() 
	{
		return pointOfContact;
	}

	/**
	 * Sets the poc.
	 * @param poc the new poc
	 */
	public void setPoc(Person poc) 
	{
		this.pointOfContact = poc;
	}

	public Vector<Contact> getContact()
	{
		return contact;
	}

	public Person getPointOfContact()
	{
		return pointOfContact;
	}

	public void setPointOfContact(Person pointOfContact)
	{
		this.pointOfContact = pointOfContact;
	}

	public void setContact(Vector<Contact> contact) 
	{
		this.contact = contact;
	}

	public String getBusinessUnitName()
	{
		return buisnessUnitName;
	}

	public void setBusinessUnitName(String buisnessunitName) 
	{
		if(buisnessunitName!=null)
			this.buisnessUnitName = buisnessunitName.trim();
	}

	public SocialInformation getSocialinfo() {
		return socialinfo;
	}

	public void setSocialinfo(SocialInformation socialinfo) {
		this.socialinfo = socialinfo;
	}
	
	
	public void setEmail(String email)
	{
		this.contact.get(0).setEmail(email);
	}

	public String getEmail()
	{
		return this.contact.get(0).getEmail();
	}

	public Long getLandline()
	{
		return this.contact.get(0).getLandline();
	}

	public void setLandline(Long landline)
	{
		this.contact.get(0).setLandline(landline);
	}

	public void setWebsite(String website)
	{
		this.contact.get(0).setWebsite(website);
	}

	public String getWebsite()
	{
		return this.contact.get(0).getWebsite();
	}

	public  Long getCellNumber1()
	{
		return contact.get(0).getCellNo1();
	}

	public  void setCellNumber1(long cellNuber1)
	{
		contact.get(0).setCellNo1(cellNuber1);
	}

	public  Long getCellNumber2()
	{
		return contact.get(0).getCellNo2();
	}

	public  void setCellNumber2(long cellNuber2)
	{
		contact.get(0).setCellNo2(cellNuber2);
	}

	public  Long getFaxNumber()
	{
		return contact.get(0).getFaxNo();
	}

	public  void setFaxNumber(long faxno)
	{
		contact.get(0).setFaxNo(faxno);
	}


	public void setPocName(String name)
	{
		this.pointOfContact.setFullname(name);
	}

	public String getPocName()
	{
		return  this.pointOfContact.getFullname();
	}

	public void setPocLandline(long landline)
	{
		this.pointOfContact.getContact().get(0).setLandline(landline);
	}

	public Long getPocLandline()
	{
		return this.pointOfContact.getContact().get(0).getLandline();
	}

	public Long getPocCell()
	{
		return this.pointOfContact.getContact().get(0).getCellNo1();
	}

	public void setPocCell(long cell)
	{
		this.pointOfContact.getContact().get(0).setCellNo1(cell);
	}

	public String getPocEmail()
	{
		return this.pointOfContact.getContact().get(0).getEmail();
	}

	public void setPocEmail(String email)
	{
		this.pointOfContact.getContact().get(0).setEmail(email);
	}

	public void setAddress(Address address)
	{
		this.contact.get(0).setAddress(address);
	}

	public Address getAddress()
	{
		return contact.get(0).getAddress();
	}
	

	@Override
	public String toString() {
		return buisnessUnitName;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getGSTINNumber() {
		return GSTINNumber;
	}

	public void setGSTINNumber(String gSTINNumber) {
		GSTINNumber = gSTINNumber;
	}
	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public String getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(String unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@OnSave
	@GwtIncompatible
	public void setLastUpdatedDate(){
		setLastUpdatedDate(DateUtility.setTimeToMidOfDay( new Date()));
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}



	public String getTat() {
		return tat;
	}



	public void setTat(String tat) {
		this.tat = tat;
	}



	public String getTierName() {
		return tierName;
	}



	public void setTierName(String tierName) {
		this.tierName = tierName;
	}
	
	public String getServiceAddressName() {
		return serviceAddressName;
	}



	public void setServiceAddressName(String serviceAddressName) {
		this.serviceAddressName = serviceAddressName;
	}
	

}
