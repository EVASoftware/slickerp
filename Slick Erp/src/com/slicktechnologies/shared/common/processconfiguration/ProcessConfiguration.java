package com.slicktechnologies.shared.common.processconfiguration;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;


@Entity
public class ProcessConfiguration extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8917940153637643651L;
	
	/************************************Applicability Attributes*************************************/
	
	@Index
	protected ArrayList<ProcessTypeDetails> processList;
	@Index
	protected String processName;
	@Index
	protected boolean configStatus;

	/***********************************Constructor********************************************/

	public ProcessConfiguration(){
		super();
		processList=new ArrayList<ProcessTypeDetails>();
		processName="";
	}
	
	/**************************************Getters And Setters************************************/
	
	public List<ProcessTypeDetails> getProcessList() {
		return processList;
	}

	public void setProcessList(List<ProcessTypeDetails> processList) {
		ArrayList<ProcessTypeDetails> arrProcess=new ArrayList<ProcessTypeDetails>();
		arrProcess.addAll(processList);
		this.processList = arrProcess;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
	public Boolean isConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(boolean configStatus) {
		this.configStatus = configStatus;
	}

	/*****************************************Overridden Method****************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		ProcessConfiguration obj=(ProcessConfiguration) m;
		String name = obj.getProcessName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.processName.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
		if(obj.id==null)
		{
			if(name.equals(currentName))
				return true;
			
			else
				return false;
		}
		//while updating a Old object
		else
		{
			if(obj.id.equals(id))
				return false;
			
			else if (name.equals(currentName))
				return true;
			
			else
				return false;	
		}			
	}



	
}
