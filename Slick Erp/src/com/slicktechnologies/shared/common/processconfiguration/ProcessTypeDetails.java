package com.slicktechnologies.shared.common.processconfiguration;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ProcessTypeDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6200198624283975719L;
	
	
	/****************************************Applicability Attributes***********************************/
	
	public String processName;
	public String processType;
	protected boolean status;
	
	
	/**********************************Getetrs And Setters*****************************************/
	
	
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
	
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	
	
	public Boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	

}
