package com.slicktechnologies.shared.common;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
@Entity
public class EmployeeProjectAllocation extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3216491094665866892L;
	/**
	 * 
	 */
	@Index
	int projectCount;
	@Index
	String employeeRole;
	@Index
	String employeeName;
	@Index
	String shift;
	@Index
	String employeeId;
	@Index
	String employeeProfileUrl;
	/** date 15.8.2018 added by komal**/
	@Index
	Date startDate;
	@Index
	Date endDate;
	@Index
	String overtime;
	@Index
	int otCount;
	/** date 7.9.2018 added by komal **/
	@Index
	boolean isExtra;
	
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * Adding site location(customer branch) field for Sunrise Facility
	 * Raised by Rahul Tiwari
	 */
	@Index
	String siteLocation;
	
	/**
	 * @author Anil
	 * @since 17-12-2020
	 * this field stores the reference name of employee
	 * refName is nothing but combination of employee name and employee id separated by forward slash
	 * raised by Rahul Tiwari for Sunrise
	 */
	String refName;
	
	@Index
	int projectAllocationId;
	
	public EmployeeProjectAllocation(String roleStr, String employeeStr,
			String shiftStr,String employeeIdStr,String employeeProfileUrlStr) {
		// TODO Auto-generated constructor stub
		this.employeeName=employeeStr;
		this.employeeRole=roleStr;
		this.shift=shiftStr;
		this.employeeId=employeeIdStr;
		this.employeeProfileUrl=employeeProfileUrlStr;
	}
	public EmployeeProjectAllocation() {
		// TODO Auto-generated constructor stub
		overtime = "";
		isExtra = false;
	}
	/****************Getter and Setter******************/
	
	public int getProjectCount() {
		return projectCount;
	}
	public void setProjectCount(int projectCount) {
		this.projectCount = projectCount;
	}
	public String getEmployeeRole() {
		return employeeRole;
	}
	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getShift() {
		return shift;
	}
	public void setShift(String shift) {
		this.shift = shift;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeProfileUrl() {
		return employeeProfileUrl;
	}
	public void setEmployeeProfileUrl(String employeeProfileUrl) {
		this.employeeProfileUrl = employeeProfileUrl;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getOvertime() {
		return overtime;
	}
	public void setOvertime(String overtime) {
		this.overtime = overtime;
	}
	public int getOtCount() {
		return otCount;
	}
	public void setOtCount(int otCount) {
		this.otCount = otCount;
	}
	public boolean isExtra() {
		return isExtra;
	}
	public void setExtra(boolean isExtra) {
		this.isExtra = isExtra;
	}
	public String getSiteLocation() {
		return siteLocation;
	}
	public void setSiteLocation(String siteLocation) {
		this.siteLocation = siteLocation;
	}
	public String getRefName() {
		return refName;
	}
	public void setRefName(String refName) {
		this.refName = refName;
	}
	
	public int getProjectAllocationId() {
		return projectAllocationId;
	}
	public void setProjectAllocationId(int projectAllocationId) {
		this.projectAllocationId = projectAllocationId;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
