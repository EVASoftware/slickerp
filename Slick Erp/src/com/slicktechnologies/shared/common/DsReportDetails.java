package com.slicktechnologies.shared.common;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class DsReportDetails extends SuperModel{

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2658500711191958968L;

	@Index
	public String reportName;
	
	@Index
	public String role;
	
	@Index
	protected boolean status;;
	
	@Index
	public String entityName;
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	
	
	
	
//	
//	@OnSave
//	@GwtIncompatible
//	public void setRole(){
//		DsReport dsReport=ofy().load().type(DsReport.class).filter("reportName", this.getReportName()).first().now();
//		dsReport.setRole(this.getRole());
//		ofy().save().entity(dsReport).now();
//	}
	
	
	
	
	
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
