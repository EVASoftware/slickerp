package com.slicktechnologies.shared.common;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.user.client.ui.TextBox;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

@Entity
public class AssesmentReport extends SuperModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5842363148318265662L;
	
	public static final String CREATED="Created",CLOSED="Closed";
	
	@Index 
	protected PersonInfo cinfo;
	protected String newcompanyname;
	protected String newcustomerfullName;
	protected Long newcustomercellNumber;
	protected String newcustomerEmail;
	protected boolean customersaveflag;
	
	@Index
	protected Address newcustomerAddress;
	@Index
	protected Date assessmentDate;
	@Index
	protected String assessedPeron1,assessedPeron2,assessedPeron3;
	protected String accompainedByPerson1,accompainedByPerson2,accompainedByPerson3;
	
	protected ArrayList<AssesmentReportEmbedForTable> assessmentDetailsLIst;
	
	//***********rohan changes here for NBHC  ************************
	protected DocumentUpload assessmentUpload;
	
	
	Date submissionDate;//Ashwini Patil Date: 20-09-2022
	
	protected DocumentUpload customerSignature;//Ashwini Patil Date: 21-12-2022

	

	/****Date :20/11/2017 
    BY : Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
                 create single service for assessment single service.**/
	
	protected ArrayList<AssessmentReportService> assessmentreportServiceList;
	/**End**/
	
	/**** Date 23-05-2019 by Vijay for Service no storing and complaint no stroring for NBHC CCPM ***/
	@Index
	protected int serviceId;
	@Index
	protected int complaintId;
	@Index
	protected int contractId;//Ashwini Patil Date:01-06-2022
	

	@Index
	protected String status;
	@Index
	protected String branch;
	

	@Index
	protected String customerBranch;
	
	/***
	 * @author Anil @since 16-12-2021
	 * As multiple services should be created against single assessment
	 */
	
	@Index
	ArrayList<Integer> serviceIdList;
	
	/**
	 * @author Anil @since 05-01-2020
	 * Adding service location for audit app
	 * raised by Nitin Sir
	 */
	@Index
	protected String serviceLocation;
	
	@Index
	protected int customerBranchId;

	@Index
	protected int serviceLocationId;
	
	protected DocumentUpload image1;
	protected DocumentUpload image2;
	protected DocumentUpload image3;
	protected DocumentUpload image4;
	protected DocumentUpload image5;
	protected DocumentUpload image6;
	protected DocumentUpload image7;
	protected DocumentUpload image8;
	protected DocumentUpload image9;
	protected DocumentUpload image10;

	protected boolean printPdfHeaderRepetative;
	protected boolean donotprintstatuscreationDate;
	protected String printAttn;
	
	protected String image1label;
	protected String image2label;
	protected String image3label;
	protected String image4label;
	protected String image5label;
	protected String image6label;
	protected String image7label;
	protected String image8label;
	protected String image9label;
	protected String image10label;
	
	public AssesmentReport() {
		super();
		
		assessmentDetailsLIst = new ArrayList<AssesmentReportEmbedForTable>();
		assessmentDate = null;
		
		newcompanyname = "";
		newcustomerfullName ="";
		  /****Date :20/11/2017 
        BY :Manisha
        Description :Incase of inspection quotation there should be some schedules for visiting person,
                     create single service for assessment single service.**/
	
		assessmentreportServiceList=new ArrayList<AssessmentReportService>();
	    /**end**/
		branch ="";
		
		serviceIdList=new ArrayList<Integer>();
		printPdfHeaderRepetative = false;
		
		printAttn ="";
	}
	
	
	@OnSave
	@GwtIncompatible
	private void savecustomer(){
		
		System.out.println("full Name === "+this.getNewcustomerfullName());
		System.out.println("compnay name ==="+this.getNewcompanyname());

		
//		if(this.isCustomersaveflag()==false){
		if(!this.newcompanyname.equals("") || !this.newcustomerfullName.equals("")){
			
			if(this.getCinfo()== null){
				
			System.out.println("inside new customer method");
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			
			Customer cust = new Customer();
			  
			System.out.println("compnay name ==="+this.getNewcompanyname());
			System.out.println("hi == "+ !this.getNewcompanyname().equals(""));
			
			
			
				if(!this.getNewcompanyname().equals("")){
					System.out.println("22222222222222");
					cust.setCompany(true);
					cust.setCompanyName(this.getNewcompanyname().toUpperCase());
				}else{
					System.out.println("111111111");
					cust.setCompany(false);
				}
				if(this.getNewcustomerfullName()!=null){
					cust.setFullname(this.getNewcustomerfullName().toUpperCase());
				}
				if(this.getNewcustomerEmail()!=null){
					cust.setEmail(this.getNewcustomerEmail());
				}
				if(this.getNewcustomercellNumber()!=null){
					cust.setCellNumber1(this.getNewcustomercellNumber());
				}
				cust.setNewCustomerFlag(true);

				if (this.getNewcustomerAddress()!=null) {
					cust.setAdress(this.getNewcustomerAddress());
					cust.setSecondaryAdress(this.getNewcustomerAddress());
				}
				cust.setCompanyId(this.getCompanyId());
			   
				genimpl.save(cust);	   

				System.out.println("after saving customer count ==="+cust.getCount());
				
				PersonInfo pinfo = new PersonInfo();
				
				pinfo.setCount(cust.getCount());
				
				if(cust.isCompany()){
					pinfo.setFullName(cust.getCompanyName());
					pinfo.setPocName(cust.getFullname());
				}else{
					pinfo.setFullName(cust.getFullname());
					pinfo.setPocName(cust.getFullname());
				}
				
				pinfo.setCellNumber(cust.getCellNumber1());
				pinfo.setEmail(cust.getEmail());
				
				this.setCinfo(pinfo);
			
			}else{
				
				
			 Customer customer = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getCinfo().getCount()).first().now();
				
				System.out.println("compnay name ==="+this.getNewcompanyname());
				System.out.println("hi == "+ !this.getNewcompanyname().equals(""));
				System.out.println("for editing customer ===");
				
				if(!this.getNewcompanyname().equals("")){
					System.out.println("22222222222222");
					customer.setCompany(true);
					customer.setCompanyName(this.getNewcompanyname().toUpperCase());
				}else{
					System.out.println("111111111");
					customer.setCompany(false);
				}
				if(this.getNewcustomerfullName()!=null){
					customer.setFullname(this.getNewcustomerfullName().toUpperCase());
				}
				if(this.getNewcustomerEmail()!=null){
					customer.setEmail(this.getNewcustomerEmail());
				}
				if(this.getNewcustomercellNumber()!=null){
					customer.setCellNumber1(this.getNewcustomercellNumber());
				}

				if (this.getNewcustomerAddress()!=null) {
					customer.setAdress(this.getNewcustomerAddress());
					customer.setSecondaryAdress(this.getNewcustomerAddress());
				}
				customer.setCompanyId(this.getCompanyId());
			   

				ofy().save().entity(customer);
				
				System.out.println("customer updated");
				
				PersonInfo pinfo = new PersonInfo();
				pinfo.setCount(customer.getCount());
			
				if(customer.isCompany()){
					pinfo.setFullName(customer.getCompanyName());
					pinfo.setPocName(customer.getFullname());
				}else{
					pinfo.setFullName(customer.getFullname());
					pinfo.setPocName(customer.getFullname());
				}
				
				pinfo.setCellNumber(customer.getCellNumber1());
				pinfo.setEmail(customer.getEmail());
				
				this.setCinfo(pinfo);
			}
		}
//	}
		
	}
	
	
	/*yY
	 * @author Abhinav
	 * @since 
	 * For Nbhc
	 */
	
	@OnSave
	@GwtIncompatible
	public void updateAssesmentIdInComaplain()
	{
		
		if(id==null){
			if(getComplaintId()!=0){
				
				Complain complain=ofy().load().type(Complain.class).filter("companyId", this.getCompanyId()).filter("count", this.getComplaintId()).first().now();
				if(complain!=null){
					complain.setAssesmentId(this.getCount());
				}
				ofy().save().entity(complain);
			}
			
		}
	}
	/*********************************************/
	
	
	

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public String getCustomerBranch() {
		return customerBranch;
	}


	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}


	public Date getAssessmentDate() {
		return assessmentDate;
	}

	public void setAssessmentDate(Date assessmentDate) {
		this.assessmentDate = assessmentDate;
	}

	public PersonInfo getCinfo() {
		return cinfo;
	}

	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}

	public Address getNewcustomerAddress() {
		return newcustomerAddress;
	}

	public void setNewcustomerAddress(Address newcustomerAddress) {
		this.newcustomerAddress = newcustomerAddress;
	}

	public String getAssessedPeron1() {
		return assessedPeron1;
	}

	public void setAssessedPeron1(String assessedPeron1) {
		this.assessedPeron1 = assessedPeron1;
	}

	public String getAssessedPeron2() {
		return assessedPeron2;
	}

	public void setAssessedPeron2(String assessedPeron2) {
		this.assessedPeron2 = assessedPeron2;
	}

	public String getAssessedPeron3() {
		return assessedPeron3;
	}

	public void setAssessedPeron3(String assessedPeron3) {
		this.assessedPeron3 = assessedPeron3;
	}

	public String getAccompainedByPerson1() {
		return accompainedByPerson1;
	}

	public void setAccompainedByPerson1(String accompainedByPerson1) {
		this.accompainedByPerson1 = accompainedByPerson1;
	}

	public String getAccompainedByPerson2() {
		return accompainedByPerson2;
	}

	public void setAccompainedByPerson2(String accompainedByPerson2) {
		this.accompainedByPerson2 = accompainedByPerson2;
	}

	public String getAccompainedByPerson3() {
		return accompainedByPerson3;
	}

	public void setAccompainedByPerson3(String accompainedByPerson3) {
		this.accompainedByPerson3 = accompainedByPerson3;
	}

	public String getNewcompanyname() {
		return newcompanyname;
	}

	public void setNewcompanyname(String newcompanyname) {
		this.newcompanyname = newcompanyname;
	}

	public String getNewcustomerfullName() {
		return newcustomerfullName;
	}

	public void setNewcustomerfullName(String newcustomerfullName) {
		this.newcustomerfullName = newcustomerfullName;
	}

	public Long getNewcustomercellNumber() {
		return newcustomercellNumber;
	}

	public void setNewcustomercellNumber(Long newcustomercellNumber) {
		this.newcustomercellNumber = newcustomercellNumber;
	}

	public String getNewcustomerEmail() {
		return newcustomerEmail;
	}

	public void setNewcustomerEmail(String newcustomerEmail) {
		this.newcustomerEmail = newcustomerEmail;
	}

	public boolean isCustomersaveflag() {
		return customersaveflag;
	}

	public void setCustomersaveflag(boolean customersaveflag) {
		this.customersaveflag = customersaveflag;
	}

	public ArrayList<AssesmentReportEmbedForTable> getAssessmentDetailsLIst() {
		return assessmentDetailsLIst;
	}

	public void setAssessmentDetailsLIst(ArrayList<AssesmentReportEmbedForTable> assessmentDetailsLIst) {
	
		ArrayList<AssesmentReportEmbedForTable> assessmentDetails = new ArrayList<AssesmentReportEmbedForTable>();
		assessmentDetails.addAll(assessmentDetailsLIst);
		this.assessmentDetailsLIst = assessmentDetails;
	}


	public DocumentUpload getAssessmentUpload() {
		return assessmentUpload;
	}


	public void setAssessmentUpload(DocumentUpload assessmentUpload) {
		this.assessmentUpload = assessmentUpload;
	}

	   /****Date :20/11/2017 
    BY : Manisha
    Description :Incase of inspection quotation there should be some schedules for visiting person,
                 create single service for assessment single service.**/
	
	public ArrayList<AssessmentReportService> getAssessmentreportServiceList() {
		return assessmentreportServiceList;
	}


	public void setAssessmentreportServiceList(ArrayList<AssessmentReportService> assessmentreportServiceList) {
		ArrayList<AssessmentReportService> assessmentreportService = new ArrayList<AssessmentReportService>();
		assessmentreportService.addAll(assessmentreportServiceList);
		this.assessmentreportServiceList = assessmentreportService;
	}
       /**end for Manisha**/


	


	public int getServiceId() {
		return serviceId;
	}


	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}


	public int getComplaintId() {
		return complaintId;
	}


	public void setComplaintId(int complaintId) {
		this.complaintId = complaintId;
	}
	public int getContractId() {
		return contractId;
	}


	public void setContractId(int contractId) {
		this.contractId = contractId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public ArrayList<Integer> getServiceIdList() {
		return serviceIdList;
	}


	public void setServiceIdList(ArrayList<Integer> serviceIdList) {
		this.serviceIdList = serviceIdList;
	}


	public String getServiceLocation() {
		return serviceLocation;
	}


	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}


	public int getCustomerBranchId() {
		return customerBranchId;
	}


	public void setCustomerBranchId(int customerBranchId) {
		this.customerBranchId = customerBranchId;
	}


	public int getServiceLocationId() {
		return serviceLocationId;
	}


	public void setServiceLocationId(int serviceLocationId) {
		this.serviceLocationId = serviceLocationId;
	}
	
	 public Date getSubmissionDate() {
			return submissionDate;
	 }


	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}


	public DocumentUpload getImage1() {
		return image1;
	}


	public void setImage1(DocumentUpload image1) {
		this.image1 = image1;
	}


	public DocumentUpload getImage2() {
		return image2;
	}


	public void setImage2(DocumentUpload image2) {
		this.image2 = image2;
	}


	public DocumentUpload getImage3() {
		return image3;
	}


	public void setImage3(DocumentUpload image3) {
		this.image3 = image3;
	}


	public DocumentUpload getImage4() {
		return image4;
	}


	public void setImage4(DocumentUpload image4) {
		this.image4 = image4;
	}


	public DocumentUpload getImage5() {
		return image5;
	}


	public void setImage5(DocumentUpload image5) {
		this.image5 = image5;
	}


	public DocumentUpload getImage6() {
		return image6;
	}


	public void setImage6(DocumentUpload image6) {
		this.image6 = image6;
	}


	public DocumentUpload getImage7() {
		return image7;
	}


	public void setImage7(DocumentUpload image7) {
		this.image7 = image7;
	}


	public DocumentUpload getImage8() {
		return image8;
	}


	public void setImage8(DocumentUpload image8) {
		this.image8 = image8;
	}


	public DocumentUpload getImage9() {
		return image9;
	}


	public void setImage9(DocumentUpload image9) {
		this.image9 = image9;
	}


	public DocumentUpload getImage10() {
		return image10;
	}


	public void setImage10(DocumentUpload image10) {
		this.image10 = image10;
	}




	public boolean isDonotprintstatuscreationDate() {
		return donotprintstatuscreationDate;
	}


	public void setDonotprintstatuscreationDate(boolean donotprintstatuscreationDate) {
		this.donotprintstatuscreationDate = donotprintstatuscreationDate;
	}


	public boolean isPrintPdfHeaderRepetative() {
		return printPdfHeaderRepetative;
	}


	public void setPrintPdfHeaderRepetative(boolean printPdfHeaderRepetative) {
		this.printPdfHeaderRepetative = printPdfHeaderRepetative;
	}


	public String getPrintAttn() {
		return printAttn;
	}


	public void setPrintAttn(String printAttn) {
		this.printAttn = printAttn;
	}


	public DocumentUpload getCustomerSignature() {
		return customerSignature;
	}


	public void setCustomerSignature(DocumentUpload customerSignature) {
		this.customerSignature = customerSignature;
	}


	public String getImage1label() {
		return image1label;
	}


	public void setImage1label(String image1label) {
		this.image1label = image1label;
	}


	public String getImage2label() {
		return image2label;
	}


	public void setImage2label(String image2label) {
		this.image2label = image2label;
	}


	public String getImage3label() {
		return image3label;
	}


	public void setImage3label(String image3label) {
		this.image3label = image3label;
	}


	public String getImage4label() {
		return image4label;
	}


	public void setImage4label(String image4label) {
		this.image4label = image4label;
	}


	public String getImage5label() {
		return image5label;
	}


	public void setImage5label(String image5label) {
		this.image5label = image5label;
	}


	public String getImage6label() {
		return image6label;
	}


	public void setImage6label(String image6label) {
		this.image6label = image6label;
	}


	public String getImage7label() {
		return image7label;
	}


	public void setImage7label(String image7label) {
		this.image7label = image7label;
	}


	public String getImage8label() {
		return image8label;
	}


	public void setImage8label(String image8label) {
		this.image8label = image8label;
	}


	public String getImage9label() {
		return image9label;
	}


	public void setImage9label(String image9label) {
		this.image9label = image9label;
	}


	public String getImage10label() {
		return image10label;
	}


	public void setImage10label(String image10label) {
		this.image10label = image10label;
	}
  
}
