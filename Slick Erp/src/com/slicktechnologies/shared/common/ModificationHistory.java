package com.slicktechnologies.shared.common;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ModificationHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1862570803067892260L;
	public String userName;
	public Date oldServiceDate;
	public Date resheduleDate;
	public String reason;
	public Date systemDate;
	
	public String resheduleTime;
	public String resheduleBranch;
	
	
	public ModificationHistory() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getOldServiceDate() {
		return oldServiceDate;
	}
	public void setOldServiceDate(Date oldServiceDate) {
		this.oldServiceDate = oldServiceDate;
	}
	public Date getResheduleDate() {
		return resheduleDate;
	}
	public void setResheduleDate(Date resheduleDate) {
		this.resheduleDate = resheduleDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getResheduleTime() {
		return resheduleTime;
	}
	public void setResheduleTime(String resheduleTime) {
		this.resheduleTime = resheduleTime;
	}
	public String getResheduleBranch() {
		return resheduleBranch;
	}
	public void setResheduleBranch(String resheduleBranch) {
		this.resheduleBranch = resheduleBranch;
	}
	public Date getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	
	
	
	
	
	

}
