package com.slicktechnologies.shared.common.serviceLiaisonLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

// TODO: Auto-generated Javadoc
/**
 * Transectial variant of {@link LiaisonStep} contains attributes which 
 * will be used in Liasion.
 */
@Embed
public class LiaisonStepLineItem extends SuperModel
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3078818644709167476L;






	

/** The reposslist. */
	@Serialize
	ArrayList<LiaisonSubstepLineItem> subStepLineItem;

	/** The status of this step*/
	protected boolean status;
	
	/** The documents of this step */
	protected DocumentUpload doc;
	
	/** The start date on which liaison will be initiated*/
	@Index
	protected Date startDate;
	
	/** The comment. */
	protected String comment;

	/** The employee. */
	protected String employee;
	
	
	/** The info. */
	LiaisonStep liasionStep;
	
	protected int index;
	
	protected String GroupName;
	  





	/***********************************Constructor**********************************************/	
	/**
	 * Instantiates a new liaison step line item.
	 */
	public LiaisonStepLineItem()
	{
	
		liasionStep=new LiaisonStep();
		subStepLineItem=new ArrayList<LiaisonSubstepLineItem>();
		
		
		
	}
	
	
	  


/******************************Getters and Setters**********************************************/

	public String getGroupName() {
		return GroupName;
	}




	
	public void setGroupName(String groupName) {
		if(groupName!=null)
		   GroupName = groupName.trim();
	}

	  
	  
	public ArrayList<LiaisonSubstepLineItem> getSubStepLineItem() {
		return subStepLineItem;
	}


	/**
	 * Sets the reposslist.
	 *
	 * @param reposslist the new reposslist
	 */
	public void setSubStepLineItem(List<LiaisonSubstepLineItem> reposslist) {
		if(reposslist!=null)
		{
			this.subStepLineItem =new ArrayList<LiaisonSubstepLineItem>();
			this.subStepLineItem.addAll(reposslist);
		
		}
	}

		/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
			return comment;
		}

		/**
		 * Sets the comment.
		 *
		 * @param comment the new comment
		 */
		public void setComment(String comment) {
			this.comment = comment;
		}

	  
		/**
		 * Gets the status 
		 *
		 * @return the statusss
		 */
		public Boolean getStatusss() {
			return status;
		}

		/**
		 * Sets the statusss.
		 *
		 * @param statusss the new statusss
		 */
		public void setStatusss(boolean statusss) {
			this.status = statusss;
		}

		/**
		 * Gets the doc.
		 *
		 * @return the doc
		 */
		public DocumentUpload getDoc() {
			return doc;
		}

		/**
		 * Sets the doc.
		 *
		 * @param doc the new doc
		 */
		public void setDoc(DocumentUpload doc) {
			this.doc = doc;
		}

		/**
		 * Gets the date.
		 *
		 * @return the date
		 */
		public Date getDate() {
			return startDate;
		}

		/**
		 * Sets the date.
		 *
		 * @param date the new date
		 */
		public void setDate(Date date) {
			this.startDate = date;
		}

		/**
		 * Gets the employee.
		 *
		 * @return the employee
		 */
		public String getEmployee() {
			return employee;
		}

		/**
		 * Sets the employee.
		 *
		 * @param employee the new employee
		 */
		public void setEmployee(String employee) {
			this.employee = employee;
		}

		/**
		 * Gets the info.
		 *
		 * @return the info
		 */
		public LiaisonStep getLisionStep() {
			return liasionStep;
		}

		/**
		 * Sets the info.
		 *
		 * @param info the new info
		 */
		public void setLisionStep(LiaisonStep info) {
			this.liasionStep = info;
			
		}

		/**
		 * Gets the step.
		 *
		 * @return the step
		 */
		public String getStep() {
			return liasionStep.getStepName();
			}

		/**
		 * Sets the step.
		 *
		 * @param step the new step
		 */
		public void setStep(String step) {
			liasionStep.setStepName(step);
		}

		/**
		 * Gets the sla.
		 *
		 * @return the sla
		 */
		public int getSLA() {
			return liasionStep.getSLA();
		}

		/**
		 * Sets the sla.
		 *
		 * @param sLA the new sla
		 */
		public void setSLA(int sLA) {
			liasionStep.setSLA(sLA);
		}

		/**
		 * Gets the description.
		 *
		 * @return the description
		 */
		public String getDescription() {
			return liasionStep.getDescription();
		}

		/**
		 * Sets the description.
		 *
		 * @param description the new description
		 */
		public void setDescription(String description) {
			liasionStep.setDescription(description);
		}

		/**
		 * Gets the mandatory.
		 *
		 * @return the mandatory
		 */
		public boolean getMandatory() {
			return liasionStep.getMandatory();
		}

		/**
		 * Sets the mandatory.
		 *
		 * @param mandatory the new mandatory
		 */
		public void setMandatory(boolean mandatory) {
			liasionStep.setMandatory(mandatory);
		}

		/**
		 * Gets the dependent step.
		 *
		 * @return the dependent step
		 */
		public String getParentStep() {
			return liasionStep.getParentStep();
		}

		/**
		 * Sets the dependent step.
		 *
		 * @param dependentStep the new dependent step
		 */
		public void setParentStep(String dependentStep) {
			if(dependentStep!=null)
			   liasionStep.setParentStep(dependentStep.trim());
		}

		
		
		public int getIndex() {
			return index;
		}





		public void setIndex(int index) {
			this.index = index;
		}





		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return liasionStep.toString();
		}

		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
		 */
		@Override
		public boolean isDuplicate(SuperModel m) {
			// TODO Auto-generated method stub
			return false;
		}
		
		

	
  
}
