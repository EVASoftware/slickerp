package com.slicktechnologies.shared.common.serviceLiaisonLayer;

import java.util.ArrayList;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

// TODO: Auto-generated Javadoc
/**
 * Represents Group of liaisonSteps and their respective substeps
 */
@Entity
public class LiaisonGroup extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7777252347995858915L;

	/** The group name. */
	protected String groupName;
	
	/** The stepsinfo. */
	@Serialize
	ArrayList<LiaisonStep> stepsinfo;
	
	@Index
	/** The status. */
	protected boolean status;
	
	
	
	/***********************************Constuctor****************************************/
	
	public LiaisonGroup()
	{
		super();
	}
	
	
	
	
	
	/********************************isDuplicate Method*************************************/
	
		/* (non-Javadoc)
		 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
		 */
		@Override
	public boolean isDuplicate(SuperModel m) {
			LiaisonGroup entity = (LiaisonGroup) m;
			String name = entity.getGroupName().trim();
			String curname=this.groupName.trim();
			curname=curname.replaceAll("\\s","");
			curname=curname.toLowerCase();
					name=name.replaceAll("\\s","");
			name=name.toLowerCase();
			
			//New Object is being added
			if(id==null)
			{
				if(name.equals(curname))
					return true;
				else
					return false;
			}
			
			// Old object is being updated
			else
			{
				
				if(id.equals(entity.id))
				{
				  return false;	
				}
				if((name.equals(curname)==true))
				  return true;
				else
					return false;
					
			}
			
		


	}
		
		
		
		
		
/**********************************Getters and Setters************************************/
		 /*
		 * @return the group name
		 */
		
		@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
		public String getGroupName() {
			return groupName;
		}


		/**
		 * Sets the group name.
		 *
		 * @param groupName the new group name
		 */
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}


		
		/**
		 * Gets the status.
		 *
		 * @return the status
		 */
		@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Status")
		public boolean getStatus() {
			return status;
		}


		/**
		 * Sets the status.
		 *
		 * @param status the new status
		 */
		public void setStatus(boolean status) {
			this.status = status;
		}
/*
		public ArrayList<LiaisonGroupInfo> getStepsinfo() {
			return stepsinfo;
		}




		public void setStepsinfo(ArrayList<LiaisonGroupInfo> stepsinfo) {
			this.stepsinfo = stepsinfo;
		}
*/
		/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
		public String toString() {
			return groupName.toString();
		}




		/**
		 * Gets the stepsinfo.
		 *
		 * @return the stepsinfo
		 */
		public ArrayList<LiaisonStep> getStepsinfo() {
			return stepsinfo;
		}




		/**
		 * Sets the stepsinfo.
		 *
		 * @param stepsinfo the new stepsinfo
		 */
		public void setStepsinfo(ArrayList<LiaisonStep> stepsinfo) {
			this.stepsinfo = stepsinfo;
		}

}
