package com.slicktechnologies.shared.common.serviceLiaisonLayer;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
// TODO: Auto-generated Javadoc

/**
 * Represents Sub Steps of a Liaison Step.
 */
@Embed
public class LiaisonSubstep extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6708914514406462560L;

	/** The substep name. */
	protected String subStepName;
	
	/** The substep description. */
	protected String subStepDescription;
	
	/** The minimum days to complete substep */
	protected int subStepSLA;
	
	/** It repesents whether the substep is mandatory or not. */
	protected boolean subStepMandatory;
	
	
/**********************************Constructor************************************************/
	
	
	public LiaisonSubstep()
	{
		super();
	}
	
	
	
	
/*************************************Getters and Setters*************************************/	
	
	/**
	 * Gets the reponse name.
	 *
	 * @return the reponse name
	 */
	public String getReponseName() {
		return subStepName;
	}
	/**
	 * Sets the reponse name.
	 *
	 * @param reponseName the new reponse name
	 */
	public void setReponseName(String reponseName) {
		this.subStepName = reponseName;
	}

	/**
	 * Gets the reponse description.
	 *
	 * @return the reponse description
	 */
	public String getReponseDescription() {
		return subStepDescription;
	}
	/**
	 * Sets the reponse description.
	 *
	 * @param reponseDescription the new reponse description
	 */
	public void setReponseDescription(String reponseDescription) {
		this.subStepDescription = reponseDescription;
	}
	/**
	 * Gets the reponse sla.
	 *
	 * @return the reponse sla
	 */
	public int getReponseSLA() {
		return subStepSLA;
	}

	/**
	 * Sets the reponse sla.
	 *
	 * @param reponseSLA the new reponse sla
	 */
	public void setReponseSLA(int reponseSLA) {
		this.subStepSLA = reponseSLA;
	}
	/**
	 * Gets the reponse mandatory.
	 *
	 * @return the reponse mandatory
	 */
	public Boolean getReponseMandatory() {
		return subStepMandatory;
	}

	/**
	 * Sets the reponse mandatory.
	 *
	 * @param reponseMandatory the new reponse mandatory
	 */
	public void setReponseMandatory(Boolean reponseMandatory) {
		this.subStepMandatory = reponseMandatory;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return subStepName.toString();
	}

}
