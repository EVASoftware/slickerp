package com.slicktechnologies.shared.common.serviceLiaisonLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

// TODO: Auto-generated Javadoc
/**
 * Repersents a Liasion. A Lision is normally connected with a Service
 */
@Entity
public class Liaison extends ConcreteBusinessProcess{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2630283984576762811L;

	public static final String LIASIONSCHEDULE="Schedule";
	public static final String LIASIONCOMPLETED="Completed";
	public static final String LIASIONCANCELLED="Cancelled";
	
	
	@Index
	protected String liaisonTitle;
	/** The service id. */
	@Index
	protected int serviceId;
	
	/** The person info. */
	@Index
	protected PersonInfo personInfo;
	
	/** The contract count. */
	@Index
	protected Integer contractCount;
	
	
	/** The contract start date. */
	@Index
	protected Date contractStartDate;
	
	@Index
	/** The contract end date. */
	protected Date contractEndDate;
	
	/** The uptest report. */
	protected DocumentUpload uptestReport;
	
	/** The up device document. */
	
	 /** The step line item. */
	ArrayList<LiaisonStepLineItem> stepLineItem;
	
	 
	 /** The service date. */
 	protected Date serviceDate;

 	@Index
 	protected Date LiaisonStartDate;
 	
 	@Index
 	protected Date LiaisonEndDate;
 	
 	/****************************************Constructor**************************************/
 	
 	public Liaison()
 	{
 		super();
 		liaisonTitle="";
 		personInfo=new PersonInfo();
 	}
	 
	 
	 
	 
	/*****************************Getters and Setters*****************************************/
	
 	
 	
 	/**
 	 * Gets the service date.
 	 *
 	 * @return the service date
 	 */
 	public Date getServiceDate() {
		return serviceDate;
	}

	public Date getLiaisonStartDate() {
		return LiaisonStartDate;
	}




	public void setLiaisonStartDate(Date liaisonStartDate) {
		LiaisonStartDate = liaisonStartDate;
	}




	public Date getLiaisonEndDate() {
		return LiaisonEndDate;
	}




	public void setLiaisonEndDate(Date liaisonEndDate) {
		LiaisonEndDate = liaisonEndDate;
	}




	public String getLiaisonTitle() {
		return liaisonTitle;
	}




	public void setLiaisonTitle(String liaisonTitle) {
		if(liaisonTitle!=null)
		this.liaisonTitle = liaisonTitle.trim();
	}




	/**
	 * Sets the service date.
	 *
	 * @param serviceDate the new service date
	 */
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	/**
	 * Gets the person info.
	 *
	 * @return the person info
	 */
	public PersonInfo getPersonInfo() {
			return personInfo;
		}

		/**
		 * Sets the person info.
		 *
		 * @param personInfo the new person info
		 */
		public void setPersonInfo(PersonInfo personInfo) {
			this.personInfo = personInfo;
		}

		/**
		 * Gets the contract count.
		 *
		 * @return the contract count
		 */
		public Integer getContractCount() {
			return contractCount;
		}

		/**
		 * Sets the contract count.
		 *
		 * @param contractCount the new contract count
		 */
		public void setContractCount(Integer contractCount) {
			this.contractCount = contractCount;
		}

		/**
		 * Gets the contract start date.
		 *
		 * @return the contract start date
		 */
		public Date getContractStartDate() {
			return contractStartDate;
		}

		/**
		 * Sets the contract start date.
		 *
		 * @param contractStartDate the new contract start date
		 */
		public void setContractStartDate(Date contractStartDate) {
			this.contractStartDate = contractStartDate;
		}

		/**
		 * Gets the contract end date.
		 *
		 * @return the contract end date
		 */
		public Date getContractEndDate() {
			return contractEndDate;
		}

		/**
		 * Sets the contract end date.
		 *
		 * @param contractEndDate the new contract end date
		 */
		public void setContractEndDate(Date contractEndDate) {
			this.contractEndDate = contractEndDate;
		}

		/**
		 * Gets the uptest report.
		 *
		 * @return the uptest report
		 */
		public DocumentUpload getUptestReport() {
			return uptestReport;
		}

		/**
		 * Sets the uptest report.
		 *
		 * @param uptestReport the new uptest report
		 */
		public void setUptestReport(DocumentUpload uptestReport) {
			this.uptestReport = uptestReport;
		}

		/**
		 * Gets the up device document.
		 *
		 * @return the up device document
		 */
		/*	
		public List<LiaisonResponseLineItem> getResponselineitem() {
			return responselineitem;
		}

		public void setResponselineitem(List<LiaisonResponseLineItem> responselineitem) {
			if(responselineitem!=null){
			this.responselineitem = new ArrayList<LiaisonResponseLineItem>();
			this.responselineitem.addAll(responselineitem);
			}
		}
*/
		/**
	 * Gets the requestlineitem.
	 *
	 * @return the requestlineitem
	 */
	public List<LiaisonStepLineItem> getRequestlineitem() {
			return stepLineItem;
		}

		/**
		 * Sets the requestlineitem.
		 *
		 * @param requestlineitem the new requestlineitem
		 */
		public void setRequestlineitem(List<LiaisonStepLineItem> requestlineitem) {
			if(requestlineitem!=null){
				this.stepLineItem=new ArrayList<LiaisonStepLineItem>();
				this.stepLineItem.addAll(requestlineitem);
			}
		}

	
	
	
	/**
	 * Gets the service id.
	 *
	 * @return the service id
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * Sets the service id.
	 *
	 * @param serviceId the new service id
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	
	
	
	/*****************************************************************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public String getEmail() {
		return personInfo.getEmail();
	}




	public void setEmail(String email) {
		personInfo.setEmail(email);
	}




	public Long getCellNumber() {
		return personInfo.getCellNumber();
	}




	public void setCellNumber(Long cellNumber) {
		personInfo.setCellNumber(cellNumber);
	}




	public String getFullName() {
		return personInfo.getFullName();
	}




	public void setFullName(String fullName) {
		personInfo.setFullName(fullName);
	}




	public int getCustomerId() {
		return personInfo.getCount();
	}




	public void setCustomerId(int id) {
		personInfo.setCount(id);
	}




	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(LIASIONSCHEDULE);
		statuslist.add(LIASIONCOMPLETED);
		statuslist.add(LIASIONCANCELLED);
		
		return statuslist;
	}

}
