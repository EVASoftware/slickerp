package com.slicktechnologies.shared.common.serviceLiaisonLayer;



import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;


// TODO: Auto-generated Javadoc
/**
 * Repersents steps for Liasoning. It Contains List of {@link LiaisonSubstep} which repersents 
 * Sub Steps of this Liasion Steps.As a Liasioning step must contain substeps. 
 */
@Entity
@Embed
public class LiaisonStep extends SuperModel{
	/**
	 * Constant repersenting type of relation between parent and child steps.
	 */
	private final int  PARENTCOMPLETEDTOSTART=1;

	/**
	 * Instantiates a new liaison request step.
	 */
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1593668081843712936L;

	/** Name of Liasion Step */
	@Index
	protected String stepName;
	
	/** Minimum number of days to complete this step */
	protected int SLA;
	
	@Index
	protected boolean status;



	/** The description of step */
	protected String description;
	
	/** Weather the step is mandatory or not */
	protected boolean mandatory;
	
	/** Name of Parent Step, Step cannot be initiated unless this step is completed */
	protected String parentStep;
	
	/**Type of Relation from Parent Step, at present we are hanling only one Type which is 
	 * Parent has been completed before the step can start.
	 */
	public int parentRelationType;
	
	
	Key<LiaisonStep>keyLiasionStep;
	ArrayList<LiaisonSubstep> responseinfo;

	
/******************************Constructor**************************************************/	
	
	public LiaisonStep()
	{
		//super();
		responseinfo=new ArrayList<LiaisonSubstep>();
		parentRelationType=PARENTCOMPLETEDTOSTART;
		parentStep="";
		
	}
	
	
	
	
	
	
	/*****************************Relational Management Part******************************/
	
	Key<LiaisonStep>StepKey;
	
	@GwtIncompatible
	@OnSave
	protected void makeLiaisonStepKey()
	{
		if(getId()==null)
		{
			StepKey=ofy().load().type(LiaisonStep.class).filter("stepName",getParentStep()).
					filter("companyId",getCompanyId()).
					keys().first().now();
		
		}
		
	}
	
	
	@GwtIncompatible
	@OnLoad
	protected void updateLiaisonStepAttribute()
	{
		if(StepKey!=null)
		{
			
			LiaisonStep step= ofy().load().key(StepKey).now();
		 if(step!=null)
		 {
			 this.parentStep=step.getStepName();
			
		 }
		}
	}
	
	
	
	
	
	/******************************Getters and Setters***********************************/
	
	
	
	public String getParentStep() {
		return parentStep;
	}




	public boolean getStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public void setParentStep(String parentStep) {
		if(parentStep!=null)
		this.parentStep = parentStep.trim();
	}


	
	
	
	
	/**
 * Gets the responseinfo.
 *
 * @return the responseinfo
 */
public ArrayList<LiaisonSubstep> getResponseinfo() {
		return responseinfo;
	}
	
	/**
	 * Sets the responseinfo.
	 *
	 * @param responseinfo the new responseinfo
	 */
	public void setResponseinfo(ArrayList<LiaisonSubstep> responseinfo) {
		this.responseinfo = responseinfo;
	}
	
	
	
	

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getStepName() {
		return stepName;
	}
	
	/**
	 * Sets the step.
	 *
	 * @param step the new step
	 */
	public void setStepName(String step) {
		this.stepName = step;
	}
	
	/**
	 * Gets the sla.
	 *
	 * @return the sla
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "SLA")
	public int getSLA() {
		return SLA;
	}
	
	/**
	 * Sets the sla.
	 *
	 * @param sLA the new sla
	 */
	public void setSLA(int sLA) {
		SLA = sLA;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Description")
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the mandatory.
	 *
	 * @return the mandatory
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Mandatory")
	public boolean getMandatory() {
		return mandatory;
	}
	
	/**
	 * Sets the mandatory.
	 *
	 * @param mandatory the new mandatory
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		LiaisonStep entity = (LiaisonStep) m;
		String name = entity.getStepName().trim();
		String curname=this.stepName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		

	}

	
/*********************************************************************************************/	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return stepName.toString();
	}
}
