package com.slicktechnologies.shared.common.serviceLiaisonLayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

// TODO: Auto-generated Javadoc
/**
 * Repersents Transactional part of A Liasion Sub Step.
 */
@Embed

public class LiaisonSubstepLineItem extends SuperModel
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2278377116632593097L;


	
	/** The statusss. */
	protected boolean statusss;
	
	/** The doc. */
	protected DocumentUpload doc;
	
	/** The date. */
	protected Date date;
	
	/** The comment. */
	protected String comment;

	/** The employee. */
	protected String employee;
	
	/** The refindex. */
	protected int refindex;
	
	/** The respo. */
	protected LiaisonSubstep respo;
	
	protected String requestStep;
	
	
	
	
	/****************************Constructor**************************************************/
	
	public LiaisonSubstepLineItem()
	{
		super();
		respo=new LiaisonSubstep();
	}
	
	
	
	
	
	
	/*****************************Getters and Setters******************************************/
	
	
	/**
	 * Gets the respo.
	 *
	 * @return the respo
	 */
	public LiaisonSubstep getRespo() {
		return respo;
	}

	public String getRequestStep() {
		return requestStep;
	}






	public void setRequestStep(String requestStep) {
		if(requestStep!=null)
		this.requestStep = requestStep.trim();
	}






	/**
	 * Sets the respo.
	 *
	 * @param respo the new respo
	 */
	public void setRespo(LiaisonSubstep respo) {
		this.respo = respo;
	}

	
	/**
	 * Gets the refindex.
	 *
	 * @return the refindex
	 */
	public int getRefindex() {
		return refindex;
	}

	/**
	 * Sets the refindex.
	 *
	 * @param refindex the new refindex
	 */
	public void setRefindex(int refindex) {
		this.refindex = refindex;
	}

	
	

/**
 * Gets the reponse name.
 *
 * @return the reponse name
 */
public String getReponseName() {
		return respo.getReponseName().toString();
	}

	/**
	 * Gets the reponse description.
	 *
	 * @return the reponse description
	 */
	public String getReponseDescription() {
		return respo.getReponseDescription();
	}

	/**
	 * Gets the reponse sla.
	 *
	 * @return the reponse sla
	 */
	public int getReponseSLA() {
		return respo.getReponseSLA();
	}

	/**
	 * Gets the reponse mandatory.
	 *
	 * @return the reponse mandatory
	 */
	public Boolean getReponseMandatory() {
		return respo.getReponseMandatory();
	}

	/**
	 * Gets the statusss.
	 *
	 * @return the statusss
	 */
	public Boolean getStatusss() {
		return statusss;
	}

	/**
	 * Sets the statusss.
	 *
	 * @param statusss the new statusss
	 */
	public void setStatusss(boolean statusss) {
		this.statusss = statusss;
	}

	/**
	 * Gets the doc.
	 *
	 * @return the doc
	 */
	public DocumentUpload getDoc() {
		return doc;
	}

	/**
	 * Sets the doc.
	 *
	 * @param doc the new doc
	 */
	public void setDoc(DocumentUpload doc) {
		this.doc = doc;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 *
	 * @param comment the new comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public String getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee the new employee
	 */
	public void setEmployee(String employee) {
		this.employee = employee;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getReponseName().toString();
	}
	

}
