package com.slicktechnologies.shared.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.CncBillServiceImpl;
import com.slicktechnologies.shared.common.cnc.Consumables;
import com.slicktechnologies.shared.common.cnc.EquipmentRental;
import com.slicktechnologies.shared.common.cnc.OtherServices;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

@Entity
public class ProjectAllocation extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9149694344824724841L;
	/**
	 * 
	 */
	@Index
	String status;
	@Index
	Date startDate;
	@Index
	Date endDate;
	@Index
	String projectName;
	@Index
	String remark;
	@Index
	String supervisorName;
	@Index
	String role;
	/**
	 * The contact information of the person corresponding to this project or
	 * contract
	 */
	@Index
	protected PersonInfo personInfo;
	@Index
	int contractId;
	// Manager Name will be sales person of contract
	@Index
	String managerName;
	ArrayList<EmployeeProjectAllocation> employeeProjectAllocationList;
	@Index
	String branch;
	ArrayList<EquipmentRental> equipmentRentalList;
	ArrayList<Consumables> consumablesList;
	ArrayList<OtherServices> otherServicesList;
//	@Serialize
//	@EmbedMap
//	HashMap<Integer, ProjectAllocation> versionMap;
	double noOfMonthsForRental,interestOnMachinary,maintainanceCharges;
	double overheadCostPerEmployee;
	double managementFees;
	/** date 14.8.2018 added by komal**/
	@Index
	String branchMangerName;

	/*
	 * Name: Apeksha Gunjal
	 * Date: 24/08/2018 @16:51
	 * Note: Added boolean variable to check is finger print allowed or not for specific project
	 */
	@Index
	boolean isFingerprintAllowed;
	/************************ Getters and Setter ***********************/
	/**
	 * date 10.1.2019 added by komal
	 * description : to set sync , submit frequency ,lat - long of project , allowed radius within
	 *               which one is allowed to mark attendance
	 **/
	double syncFrequency;
	double submitFrequency;
	double allowedRadius;
	double locationLatitude;
	double locationLongitude;
	/** date 23.1.2019 added by komal  to set active and inactive status in project**/
	@Index
	protected boolean projectStatus;
	
	/**
	 * @author Anil,Date : 22-02-2019
	 * Paid leave and bonus as additional allowance
	 */
	@Index
	boolean paidLeaveAndBonusAsAdditionalAllowance;
	/** date 16.08.2019 added by komal to store minimum ot hours**/
	double miOtMinutes;
	
	/**
	 * @author Anil , Date : 01-10-2019
	 * Updating employee details from employee to HrProject to Project Allocation
	 */
	boolean reversEmpProjectAllocation=false;
	/**Date by 3-7-2020 by Amol***/
	protected String customerBranch;
	
	/**
	 * @author Anil
	 * @since 24-07-2020
	 * Adding field payroll start day which will define payroll cycle
	 */
	Integer payrollStartDay;
	
	/**
	 * @author Anil
	 * @since 19-08-2020
	 * Adding state field for Sunrise Facility
	 * Raised by Rahul Tiwari
	 */
	@Index
	String state;
	
	/**
	 * @author Vijay Since 20-09-2022
	 * Sunrise getting issue when updating employee with project name so  employee project
	 * allocation list storing in seperate entity when user delete entry from table it should  delete
	 * from the entity also so added below list to manage delete operation
	 */
	@Index
	boolean seperateEntityFlag;
	ArrayList<EmployeeProjectAllocation> deletedEmpoyeeProjectAllocationlist;

	public ProjectAllocation() {
		// TODO Auto-generated constructor stub
		employeeProjectAllocationList = new ArrayList<EmployeeProjectAllocation>();
		equipmentRentalList=new ArrayList<EquipmentRental>();
		consumablesList=new ArrayList<Consumables>();
		otherServicesList=new ArrayList<OtherServices>();
//		versionMap=new HashMap<Integer, ProjectAllocation>();
		noOfMonthsForRental=0;
		interestOnMachinary=0;
		maintainanceCharges=0;
		overheadCostPerEmployee=0;
		managementFees=0;
		/** date 14.8.2018 added by komal**/
		branchMangerName ="";
		/*Added by Apeksha on 24/08/2018 @16:51*/
		isFingerprintAllowed = true;
		projectStatus = true;
		seperateEntityFlag = false;

	}
	
	public boolean isPaidLeaveAndBonusAsAdditionalAllowance() {
		return paidLeaveAndBonusAsAdditionalAllowance;
	}
	public void setPaidLeaveAndBonusAsAdditionalAllowance(
			boolean paidLeaveAndBonusAsAdditionalAllowance) {
		this.paidLeaveAndBonusAsAdditionalAllowance = paidLeaveAndBonusAsAdditionalAllowance;
	}




	public boolean isFingerprintAllowed() {
		return isFingerprintAllowed;
	}

	public void setFingerprintAllowed(boolean isFingerprintAllowed) {
		this.isFingerprintAllowed = isFingerprintAllowed;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSupervisorName() {
		return supervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}

	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public ArrayList<EmployeeProjectAllocation> getEmployeeProjectAllocationList() {
		return employeeProjectAllocationList;
	}

	public void setEmployeeProjectAllocationList(
			List<EmployeeProjectAllocation> employeeProjectAllocationList) {
		ArrayList<EmployeeProjectAllocation> employeeProjectList=new ArrayList<EmployeeProjectAllocation>();
		employeeProjectList.addAll(employeeProjectAllocationList);
		this.employeeProjectAllocationList = employeeProjectList;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@OnSave
	@GwtIncompatible
	public void updateProjectIdInList(){
		
	}

	
	public ArrayList<EquipmentRental> getEquipmentRentalList() {
		return equipmentRentalList;
	}

	public void setEquipmentRentalList(
			List<EquipmentRental> equipmentRentList) {
		ArrayList<EquipmentRental> equipmentRentalListDeatails=new ArrayList<EquipmentRental>();
		equipmentRentalListDeatails.addAll(equipmentRentList);
		this.equipmentRentalList = equipmentRentalListDeatails;
	}

	public ArrayList<Consumables> getConsumablesList() {
		return consumablesList;
	}

	public void setConsumablesList(List<Consumables> consumablesList) {
		ArrayList<Consumables> consumablesListDetails=new ArrayList<Consumables>();
		consumablesListDetails.addAll(consumablesList);
		this.consumablesList = consumablesListDetails;
	}

	public ArrayList<OtherServices> getOtherServicesList() {
		return otherServicesList;
	}

	public void setOtherServicesList(List<OtherServices> otherServicesList) {
		ArrayList<OtherServices> otherServicesListDetails=new ArrayList<OtherServices>();
		otherServicesListDetails.addAll(otherServicesList);
		this.otherServicesList = otherServicesListDetails;
	}

//	public HashMap<Integer, ProjectAllocation> getVersionMap() {
//		return versionMap;
//	}
//
//	public void setVersionMap(HashMap<Integer, ProjectAllocation> versionMap) {
//		this.versionMap = versionMap;
//	}

	public double getNoOfMonthsForRental() {
		return noOfMonthsForRental;
	}

	public void setNoOfMonthsForRental(double noOfMonthsForRental) {
		this.noOfMonthsForRental = noOfMonthsForRental;
	}

	public double getInterestOnMachinary() {
		return interestOnMachinary;
	}

	public void setInterestOnMachinary(double interestOnMachinary) {
		this.interestOnMachinary = interestOnMachinary;
	}

	public double getMaintainanceCharges() {
		return maintainanceCharges;
	}

	public void setMaintainanceCharges(double maintainanceCharges) {
		this.maintainanceCharges = maintainanceCharges;
	}

	public double getOverheadCostPerEmployee() {
		return overheadCostPerEmployee;
	}

	public void setOverheadCostPerEmployee(double overheadCostPerEmployee) {
		this.overheadCostPerEmployee = overheadCostPerEmployee;
	}

	public double getManagementFees() {
		return managementFees;
	}

	public void setManagementFees(double managementFees) {
		this.managementFees = managementFees;
	}

	public String getBranchMangerName() {
		return branchMangerName;
	}

	public void setBranchMangerName(String branchMangerName) {
		this.branchMangerName = branchMangerName;
	}
	/** date 27.8.2018 added by komal**/
	@OnSave
	@GwtIncompatible
	public void createHrProject(){	
		/**
		 * This value gets true only when we are updating it from employee master
		 */
		if(reversEmpProjectAllocation){
			reversEmpProjectAllocation=false;
			return;
		}
		/**
		 * @author Vijay Since :- 22-09-2022
		 * Des  :- when seperate entity flag is true then this method called from on save method from entitypresenter
		 */
		if(!this.seperateEntityFlag) {
			CncBillServiceImpl impl = new CncBillServiceImpl();
			impl.generateHRProject(this);
		}
		
		
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return projectName;
	}


	public double getSyncFrequency() {
		return syncFrequency;
	}


	public void setSyncFrequency(double syncFrequency) {
		this.syncFrequency = syncFrequency;
	}


	public double getSubmitFrequency() {
		return submitFrequency;
	}


	public void setSubmitFrequency(double submitFrequency) {
		this.submitFrequency = submitFrequency;
	}


	public double getAllowedRadius() {
		return allowedRadius;
	}


	public void setAllowedRadius(double allowedRadius) {
		this.allowedRadius = allowedRadius;
	}


	public double getLocationLatitude() {
		return locationLatitude;
	}


	public void setLocationLatitude(double locationLatitude) {
		this.locationLatitude = locationLatitude;
	}


	public double getLocationLongitude() {
		return locationLongitude;
	}


	public void setLocationLongitude(double locationLongitude) {
		this.locationLongitude = locationLongitude;
	}


	public boolean isProjectStatus() {
		return projectStatus;
	}


	public void setProjectStatus(boolean projectStatus) {
		this.projectStatus = projectStatus;
	}

	public double getMiOtMinutes() {
		return miOtMinutes;
	}

	public void setMiOtMinutes(double miOtMinutes) {
		this.miOtMinutes = miOtMinutes;
	}

	public boolean isReversEmpProjectAllocation() {
		return reversEmpProjectAllocation;
	}

	public void setReversEmpProjectAllocation(boolean reversEmpProjectAllocation) {
		this.reversEmpProjectAllocation = reversEmpProjectAllocation;
	}

	public String getCustomerBranch() {
		return customerBranch;
	}

	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}

	public Integer getPayrollStartDay() {
		return payrollStartDay;
	}

	public void setPayrollStartDay(Integer payrollStartDay) {
		this.payrollStartDay = payrollStartDay;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isSeperateEntityFlag() {
		return seperateEntityFlag;
	}

	public void setSeperateEntityFlag(boolean seperateEntityFlag) {
		this.seperateEntityFlag = seperateEntityFlag;
	}

	public ArrayList<EmployeeProjectAllocation> getDeletedEmpoyeeProjectAllocationlist() {
		return deletedEmpoyeeProjectAllocationlist;
	}

	public void setDeletedEmpoyeeProjectAllocationlist(
			ArrayList<EmployeeProjectAllocation> deletedEmpoyeeProjectAllocationlist) {
		this.deletedEmpoyeeProjectAllocationlist = deletedEmpoyeeProjectAllocationlist;
	}

	
	

}
