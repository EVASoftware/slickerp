package com.slicktechnologies.shared.common.helperlayer;
import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

// TODO: Auto-generated Javadoc
/**
 * The Contact.
 */
@Embed
public class Contact implements Serializable
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7772329901307719395L;

	/** The cell no1. */
	@Index 
	protected Long cellNo1;

	/** The cell no2. */
	protected Long cellNo2;

	/** The landline. */
	protected Long landline;

	/** The fax no. */
	protected Long faxNo;

	/** The website. */
	protected String website;

	/** The email. */
	protected String email;

	/** The address {@link Address}*/
	protected Address address;

	
	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new contact entity.
	 */
	public Contact()
	{
		address=new Address();
		website="";
		email="";
		cellNo1=0l;
		cellNo2=0l;
		landline=0l;
		
		
	}

	/***********************************************Getter/Setter****************************************************/
	/**
	 * Gets the cell no1.
	 *
	 * @return the cell no1
	 */
	public long getCellNo1() {
		return cellNo1;
	}

	/**
	 * Sets the cell no1.
	 *
	 * @param cellNo1 the new cell no1
	 */
	public void setCellNo1(long cellNo1) {
		this.cellNo1 = cellNo1;
	}

	/**
	 * Gets the cell no2.
	 *
	 * @return the cell no2
	 */
	public Long getCellNo2() {
		return cellNo2;
	}

	/**
	 * Sets the cell no2.
	 *
	 * @param cellNo2 the new cell no2
	 */
	public void setCellNo2(Long cellNo2) {
		this.cellNo2 = cellNo2;
	}

	/**
	 * Gets the landline.
	 *
	 * @return the landline
	 */
	public Long getLandline() {
		return landline;
	}

	/**
	 * Sets the landline.
	 *
	 * @param landline the new landline
	 */
	public void setLandline(Long landline) {
		if(landline!=null)
			this.landline = landline;
	}

	/**
	 * Gets the fax no.
	 *
	 * @return the fax no
	 */
	public Long getFaxNo() {
		return faxNo;
	}

	/**
	 * Sets the fax no.
	 *
	 * @param faxNo the new fax no
	 */
	public void setFaxNo(Long faxNo) {
		if(faxNo!=null)
			this.faxNo = faxNo;
	}

	/**
	 * Gets the website.
	 *
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Sets the website.
	 *
	 * @param website the new website
	 */
	public void setWebsite(String website) {
		if(website!=null)
			this.website = website.trim();
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		if(email!=null)
			this.email = email.trim();
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/*****************************************************************************************************************************/

}
