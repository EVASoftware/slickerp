package com.slicktechnologies.shared.common.helperlayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;



public class DemoConfigSummary extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2953953443724871466L;

	/************************************Applicability Attributes*************************************/
	
	@Index
	protected String documentName;
	@Index
	protected int documentId;
	@Index
	protected String status;

	
	/***********************************Constructor********************************************/
	
	public  DemoConfigSummary() {
		super();
		documentName="";
		status="";
	}
	
	/***************************************Getters And Setters*************************************/
	
	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		if(documentName!=null){
			this.documentName = documentName.trim();
		}
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if(status!=null){
			this.status = status.trim();
		}
	}

	/********************************************Overridden Method***********************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
}
