package com.slicktechnologies.shared.common.helperlayer;
import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

// TODO: Auto-generated Javadoc
/**
 * The Class SocialInformationEntity. Represents person's social information.
 */
@Embed
public class SocialInformation implements Serializable {
	
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5189824340004372759L;
	
	/** The face book id. */
	protected String faceBookId;
	
	/** The twitter id. */
	protected String twitterId;
	
	/** The google plus id. */
	protected String googlePlusId;
	
	
	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new social information entity.
	 */
	public SocialInformation() {
		faceBookId="";
		twitterId="";
		googlePlusId="";
	}
	
	/***********************************************Getter/Setter****************************************************/
	/**
	 * Gets the face book id.
	 *
	 * @return the face book id
	 */
	public String getFaceBookId() {
		return faceBookId;
	}
	
	
	
	



	/**
	 * Sets the face book id.
	 *
	 * @param faceBookId the new face book id
	 */
	public void setFaceBookId(String faceBookId)
	{
		if(faceBookId!=null)
			this.faceBookId = faceBookId.trim();
	}
	
	/**
	 * Gets the twitter id.
	 *
	 * @return the twitter id
	 */
	public String getTwitterId() {
		return twitterId;
	}
	
	/**
	 * Sets the twitter id.
	 *
	 * @param twitterId the new twitter id
	 */
	public void setTwitterId(String twitterId)
	{
		if(twitterId!=null)
			this.twitterId = twitterId.trim();
	}
	
	/**
	 * Gets the google plus id.
	 *
	 * @return the google plus id
	 */
	public String getGooglePlusId() {
		return googlePlusId;
	}
	
	/**
	 * Sets the google plus id.
	 *
	 * @param googlePlusId the new google plus id
	 */
	public void setGooglePlusId(String googlePlusId)
	{
		if(googlePlusId!=null)
			this.googlePlusId = googlePlusId.trim();
	}
	
	/*********************************************************************************************************************/
	

}
