package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Calendar;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.shippingpackingprocess.CheckListType;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Shraded Class of Employee.
 * The Calendar for Employee is Saved here.
 * It also save weather Leave has been Allocated to Employee 
 * or Not.
 * After initial Allocation Leave Can only be Allocated when toDate of Calendar is less
 * than from date of Allocation;
 */
@SuppressWarnings("serial")
@Embed
@Index
@Entity
public class EmployeeInfo extends SuperModel implements Serializable
{
	/** *********************************************Entity Attributes***************************************. */
	/** The cell number. */
	protected Long cellNumber;
	
	/** The full name. */
	protected String fullName;
	
	/** The id. */
	protected int empCount;
	
	/** The designation. */
	protected String designation;
	
	/** The department. */
	protected String department;
	

	/** The employee type. */
	protected String employeeType;

	/** The employeerole. */
	protected String employeerole;
	
	
	/** The branch. */
	protected String branch;
	
	/** The country. */
	protected String country;
	/**
	 * A hack to show allocated Leave Groups
	 */
	protected String leaveGroupName;
	
	/** *********************************************Relational Attributes***************************************. */
	
	/** The calender key. */
	protected Key<Calendar> calenderKey;
	
	/**Boolean repersenting that Leave and Calendar  has been Allocated or Not.Value is initialized 
	 * with false.**/
	boolean allocatable;
	
	/** The leave calendar. */
	@Ignore
	protected Calendar leaveCalendar;
	
	@Ignore
	protected Overtime overtime;
	
	
	/**
	 * Date : 02-04-2018 BY ANIL
	 */
	
	boolean isCtcAllocated;
	boolean isCtcCreated;
	
	/**
	 * Date : 04-05-2018 By ANIL 
	 * adding gender field used for payroll calculation ,PT
	 */
	
	String gender;
	
	
	/**
	 * Date : 12-05-2018 By ANIL
	 * Adding ctcAmount,and Arrears
	 */
	double ctcAmount;
	boolean isArrears;
	
	
	/**
	 * Date : 29-05-2018 BY ANIL
	 * Adding field which reads the employee type from numberRange of Employee Entity
	 * This is used at the time of payroll when we deduct ESCI for Diract employee
	 * Sasha ERP-Facility Management
	 */
	
	@Index
	String numberRangeAsEmpTyp;
	
	/**
	 * Date : 20-07-2018 By ANIL
	 */
	@Index
	boolean status;
	
	@Index
	String projectName;
	
	/**
	 * Date :25-07-2018 By  ANIL
	 */
	@Index
	Date dateOfJoining;
	
	@Index
	Date lastWorkingDate;
	
	@Index
	Date dateOfBirth;
	

	/** date 21.11.2018 added by komal to restrict payroll of 
	 *  resigned , terminated and absconded employee 
	 */
	@Index
	boolean isResigned;
	@Index
	boolean isTerminated;

	/**
	 * Date 14-11-2018 By ANIL:
	 * Capturing person disability with percentage 
	 */
	@Index
	boolean isDisabled;
	double disablityPercentage;
	

	
	@Index
	boolean isAbsconded;

	@Index
	boolean isRealeaseForPayment;
	
	/**
	 * @author Anil , Date : 10-04-2019
	 */
	@Index
	String empGrp;

	/**Date 4-4-2019 by Amol
	 * for Ctc Template Name
	 */
	String ctcTemplateName;
	
	
	/**
	 * @author Anil , Date : 23-09-2019
	 * Added shiftName
	 */
	
	@Index
	String shiftName;

	
	/**
	 * @author Anil , Date : 05-10-2019
	 * for reliever employee we will process only single payroll despite of  multiple site attendance
	 * For Sasha
	 */
	@Index
	boolean isReliever;


	/**date 1-10-2019 by Amol**/
	String caste;

	
	/**
	 * **************************************** Constructor ***************************************.
	 */
	
	
	 /**
 	 * Instantiates a new employee info.
 	 */
 	public EmployeeInfo() {
		super();
	    fullName="";
	    designation="";
	    department="";
	    employeerole="";
	    employeeType="";
	    branch="";
	    country="";
	    allocatable=false;
	    
	    gender="";
	    numberRangeAsEmpTyp="";
	    
	    projectName="";
	    status=false;
	    isResigned = false;
	    isAbsconded = false;
	    isResigned = false;
	    isRealeaseForPayment = false;

	    isDisabled=false;

	   caste="";
	}
 	
 	
 	
 	public String getShiftName() {
		return shiftName;
	}



	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}



	public String getEmpGrp() {
		return empGrp;
	}



	public void setEmpGrp(String empGrp) {
		this.empGrp = empGrp;
	}
 	
 	public boolean isDisabled() {
		return isDisabled;
	}



	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}



	public double getDisablityPercentage() {
		return disablityPercentage;
	}



	public void setDisablityPercentage(double disablityPercentage) {
		this.disablityPercentage = disablityPercentage;
	}
	
 	public Date getDateOfBirth() {
		return dateOfBirth;
	}




	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}




	public Date getDateOfJoining() {
		return dateOfJoining;
	}




	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}




	public Date getLastWorkingDate() {
		return lastWorkingDate;
	}




	public void setLastWorkingDate(Date lastWorkingDate) {
		this.lastWorkingDate = lastWorkingDate;
	}




	public boolean isStatus() {
		return status;
	}




	public void setStatus(boolean status) {
		this.status = status;
	}




	public String getProjectName() {
		return projectName;
	}




	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
 	
 	
	public String getNumberRangeAsEmpTyp() {
		return numberRangeAsEmpTyp;
	}




	public void setNumberRangeAsEmpTyp(String numberRangeAsEmpTyp) {
		this.numberRangeAsEmpTyp = numberRangeAsEmpTyp;
	}




	/**
	 * Instantiates a new employee info.
	 *
	 * @param cellNumber the cell number
	 * @param fullName the full name
	 * @param id the id
	 * @param designation the designation
	 * @param department the department
	 * @param employeeRole the employee role
	 * @param employeeType the employee type
	 * @param branch the branch
	 */
	public EmployeeInfo(Long cellNumber, String fullName, int id,String designation,String department,String employeeRole,String employeeType,String branch) {
		super();
		this.cellNumber = cellNumber;
		this.fullName = fullName;
		this.empCount = id;
		this.employeerole=employeeRole;
		this.employeeType=employeeType;
		this.branch=branch;
	}

	
	
	public double getCtcAmount() {
		return ctcAmount;
	}


	public void setCtcAmount(double ctcAmouunt) {
		this.ctcAmount = ctcAmouunt;
	}


	public boolean isArrears() {
		return isArrears;
	}


	public void setArrears(boolean isArrears) {
		this.isArrears = isArrears;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	/**
	 * ******************************Getters and Setters**************************************.
	 *
	 * @return the branch
	 */
	
	
	

	public String getBranch() {
		return branch;
	}


	/**
	 * Sets the branch.
	 *
	 * @param branch the new branch
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}


	/**
	 * Gets the cell number.
	 *
	 * @return the cell number
	 */
	public Long getCellNumber() {
		return cellNumber;
	}


	/**
	 * Sets the cell number.
	 *
	 * @param cellNumber the new cell number
	 */
	public void setCellNumber(Long cellNumber) {
		this.cellNumber = cellNumber;
	}


	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}


	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		if(fullName!=null)
		this.fullName = fullName.trim();
	}


	/**
	 * Gets the emp count.
	 *
	 * @return the emp count
	 */
	public int getEmpCount() {
		return empCount;
	}


	/**
	 * Sets the emp count.
	 *
	 * @param count the new emp count
	 */
	public void setEmpCount(int count) {
		this.empCount = count;
	}


	/**
	 * Gets the designation.
	 *
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}


	/**
	 * Sets the designation.
	 *
	 * @param designation the new designation
	 */
	public void setDesignation(String designation) {
		if(designation!=null)
		this.designation = designation.trim();
	}

	
	  
	/**
	 * Gets the calender key.
	 *
	 * @return the calender key
	 */
	public Key<Calendar> getCalenderKey() {
		return calenderKey;
	}


	/**
	 * Sets the calender key.
	 *
	 * @param calenderKey the new calender key
	 */
	public void setCalenderKey(Key<Calendar> calenderKey) {
		this.calenderKey = calenderKey;
	}


	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}


	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(String department) {
		if(department!=null)
		this.department = department.trim();
	}

	/**
	 * Gets the employeerole.
	 *
	 * @return the employeerole
	 */
	public String getEmployeerole() {
		return employeerole;
	}


	/**
	 * Sets the employeerole.
	 *
	 * @param employeerole the new employeerole
	 */
	public void setEmployeerole(String employeerole) {
		this.employeerole = employeerole;
	}


	/**
	 * Gets the employee type.
	 *
	 * @return the employee type
	 */
	public String getEmployeeType() {
		return employeeType;
	}


	/**
	 * Sets the employee type.
	 *
	 * @param employeeType the new employee type
	 */
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}


	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}


	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	

	
	/**
	 * Gets the leave calendar.
	 *
	 * @return the leave calendar
	 */
	public Calendar getLeaveCalendar() {
		return leaveCalendar;
	}
	/**
	 * Sets the leave calendar.
	 *
	 * @param leaveCalendar the new leave calendar
	 */
	public void setLeaveCalendar(Calendar leaveCalendar) {
		this.leaveCalendar = leaveCalendar;
	}

	
	public Overtime getOvertime() {
		return overtime;
	}


	public void setOvertime(Overtime overtime) {
		this.overtime = overtime;
	}
	
	/**
	 * Gets the calendar name.
	 *
	 * @return the calendar name
	 */
	
	public String getCalendarName() {
		
		if(leaveCalendar!=null){
			return leaveCalendar.getCalName();
		}
		else{
			return "";
		}
	}


	/**
	 * Sets the calendar name.
	 *
	 * @param calendarName the new calendar name
	 */
	public void setCalendarName(String calendarName) {
		if(calendarName!=null)
			calendarName=calendarName.trim();
	}


	public boolean isLeaveAllocated() {
		return allocatable;
	}
	
	public void setLeaveAllocated(boolean leaveAllocated) {
		this.allocatable = leaveAllocated;
	}
	
	public String getLeaveGroupName() {
		if(leaveGroupName==null){
			leaveGroupName="";
		}
		return leaveGroupName;
	}
	
	public void setLeaveGroupName(String leaveGroupName) {
		this.leaveGroupName = leaveGroupName;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


	@Override
	public int compareTo(SuperModel o) {
		return 0;
	}

/**
 * *********************************************Buisness Logic***************************************.
 *
 * @param empInfo the emp info
 */
   
   /**
    * Make object list box live.
    *
    * @param empInfo the emp info
    */
   public static void makeObjectListBoxLive(ObjectListBox<EmployeeInfo>empInfo)
   {
	   MyQuerry querry=new MyQuerry();
	   querry.setQuerryObject(new EmployeeInfo());
	   empInfo.MakeLive(querry);
   }
   
   /**
    * Method creates Calendar Key From Calendar Name.
    */
   @GwtIncompatible
	@OnSave
	public void onSave()
	{
	   String calendarName=getCalendarName();
	   if(calendarName!=null&&calendarName.equals("")==false)
		{
			calenderKey=ofy().load().type(Calendar.class).filter("calName",calendarName).filter("companyId",getCompanyId()).
					keys().first().now();
		}
	}
   
   /**
    * Method Loads Calendar from Calendar Key.
    */
   @GwtIncompatible
	@OnLoad
	public void onLoad()
	{
	   /**
	    * @author Anil @since 25-01-2021
	    * commented below code which loads leave calendar every time of loading employee info
	    * @author Anil @since 29-01-2021
	    * loading calendar at the time of loading employee info
	    * this causes issues in attendance upload and payroll
	    */
	  if(calenderKey!=null){
		  leaveCalendar= ofy().load().key(calenderKey).now();
	  }
	}
   
   /**
    * Checks if is allocation of Leave And Calendar is Possible.
    * Leave or Calendar cannot be allocated if a {@link Calendar} exist 
    * corresponding to this Employee and Calendar is Not Expired
    * To Do : Time Zone Issue.
    *
    * @param startDate the start date
    * @return true, if is allocatable
    */
   public boolean isAllocatable(Date startDate)
   {
	   System.out.println("Calendar "+leaveCalendar);
	   /**
	    * Date : 31-12-2018 BY ANIL
	    * Commented code ,calendar allocation was not done because of it
	    * @author Anil,Date : 24-01-2019
	    * Uncommented again
	    */
	   if(calenderKey==null){
		   return true;
	   }
	   else if(leaveCalendar.getCalEndDate().before(startDate)){
		   return true;
	   }
	   return false;
//	   return true;
   }
   
   
   /**
    * If Current Date is Greater then Calendar End Date it means that
    * Leave and Calendar can be Allocated again.
    * To Do : Time zone issues
    */
   	@GwtIncompatible
  	@OnLoad
  	private void changeAllocationStatus()
  	{
   		/**
 	    * Date : 02-01-2019 BY ANIL
 	    * Commented code ,calendar allocation was not done because of it
 	    */
   		
//  	 	if(leaveCalendar!=null&&allocatable==true)
//  	 	{
//  	 		if(new Date().after(leaveCalendar.getCalEndDate()))
//  	 		{
//  	 			allocatable=false;
//  	 			calenderKey=null;
//  	 		}
//  	 	}
  	}



	@Override
	public String toString() {
		return fullName.toString();
	}
	

	@Override
	public boolean equals(Object arg0) {
		if(arg0 instanceof EmployeeInfo)
		{
			EmployeeInfo info=(EmployeeInfo) arg0;
			if(info.getId()==null||id==null){
				return false;
			}
			if(info.getId()==id){
				return true;
			}
			
		}
		return false;
	}


	public boolean isCtcAllocated() {
		return isCtcAllocated;
	}


	public void setCtcAllocated(boolean isCtcAllocated) {
		this.isCtcAllocated = isCtcAllocated;
	}


	public boolean isCtcCreated() {
		return isCtcCreated;
	}


	public void setCtcCreated(boolean isCtcCreated) {
		this.isCtcCreated = isCtcCreated;
	}

	public boolean isResigned() {
		return isResigned;
	}

	public void setResigned(boolean isResigned) {
		this.isResigned = isResigned;
	}

	public boolean isTerminated() {
		return isTerminated;
	}

	public void setTerminated(boolean isTerminated) {
		this.isTerminated = isTerminated;
	}

	public boolean isAbsconded() {
		return isAbsconded;
	}

	public void setAbsconded(boolean isAbsconded) {
		this.isAbsconded = isAbsconded;
	}

	public boolean isRealeaseForPayment() {
		return isRealeaseForPayment;
	}

	public void setRealeaseForPayment(boolean isRealeaseForPayment) {
		this.isRealeaseForPayment = isRealeaseForPayment;
	}

	public String getCtcTemplateName() {
		return ctcTemplateName;
	}

	public void setCtcTemplateName(String ctcTemplateName) {
		this.ctcTemplateName = ctcTemplateName;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}
		public boolean isReliever() {
		return isReliever;
	}





	public void setReliever(boolean isReliever) {
		this.isReliever = isReliever;
	}
	
	
	
	
}
