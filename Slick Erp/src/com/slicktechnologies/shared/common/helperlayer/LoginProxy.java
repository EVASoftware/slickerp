package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

public class LoginProxy implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8659385604292631644L;
	protected String userId;
	protected String password;
	protected String companyName;
	
	public LoginProxy() {
		super();
		
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
	

}
