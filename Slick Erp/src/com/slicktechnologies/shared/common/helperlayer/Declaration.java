package com.slicktechnologies.shared.common.helperlayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class Declaration  extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6020717288112230763L;

	
	
/*****************************************Applicability Attributes****************************************/
	
	@Index
	protected String title;
	/**
	 * @author Anil
	 * @since 21-01-2022
	 * as declarationMsg was marked index it was not allowed to save more than 500 character
	 * raised by Nithila
	 */
	protected String declaratiomMsg;
	@Index
	protected Boolean status;
	
	
/*********************************************Constructor************************************/
	
	public Declaration() {
		super();

		title="";
		declaratiomMsg="";
	}

	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return title;
	}
	
	
	//*******************getters and setters ********************
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDeclaratiomMsg() {
		return declaratiomMsg;
	}


	public void setDeclaratiomMsg(String declaratiomMsg) {
		this.declaratiomMsg = declaratiomMsg;
	}


	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
