package com.slicktechnologies.shared.common.helperlayer;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
@Embed
public class TermsAndConditions  extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5038010325793566706L;

	
	
/*****************************************Applicability Attributes****************************************/
	
	@Index
	protected String title;
	@Index
	protected String document;	
	protected Integer sequenceNumber;
	protected String msg;
	@Index
	protected Boolean status;
	
	
/*********************************************Constructor************************************/
	
	public TermsAndConditions() {
		super();

		title="";
		msg="";
		document="";
		sequenceNumber=1;
	}

	
	


	/*********************************************Getters and Setters************************************/
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}


	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}





	@Override
	public String toString() {
		return this.title.toString();
	}

	
	
	
}
