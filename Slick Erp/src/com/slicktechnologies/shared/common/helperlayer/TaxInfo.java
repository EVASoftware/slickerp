package com.slicktechnologies.shared.common.helperlayer;
import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;


/**
 * The Class TaxInfoEntity. Represents the tax information.
 */
@Embed
public class TaxInfo  implements Serializable
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2158445313463508510L;

	/** The tax info1. */
	protected String tax1;

	/** The tax info2. */
	protected String tax2;

	/** The tax info3. */
	protected String tax3;

	/** The vat. */
	protected String vat;

	
	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new tax info entity.
	 */
	public TaxInfo()
	{
		tax1="";
		tax2="";
		tax3="";
		vat="";
	}
	
	/***********************************************Getter/Setter****************************************************/

	/**
	 * Gets the tax info1.
	 *
	 * @return the tax info1
	 */
	public String getTaxInfo1() {
		return tax1;
	}

	/**
	 * Sets the tax info1.
	 *
	 * @param taxInfo1 the new tax info1
	 */
	public void setTaxInfo1(String taxInfo1) {
		if(taxInfo1!=null)
			this.tax1 = taxInfo1.trim();
	}

	/**
	 * Gets the tax info2.
	 *
	 * @return the tax info2
	 */
	public String getTaxInfo2() {
		return tax2;
	}

	/**
	 * Sets the tax info2.
	 *
	 * @param taxInfo2 the new tax info2
	 */
	public void setTaxInfo2(String taxInfo2) {
		if(taxInfo2!=null)
			this.tax2 = taxInfo2.trim();
	}

	/**
	 * Gets the tax info3.
	 *
	 * @return the tax info3
	 */
	public String getTaxInfo3() {

		return tax3;
	}

	/**
	 * Sets the tax info3.
	 *
	 * @param taxInfo3 the new tax info3
	 */
	public void setTaxInfo3(String taxInfo3) 
	{
		if(taxInfo3!=null)
			this.tax3 = taxInfo3.trim();
	}

	/**
	 * Gets the vat.
	 *
	 * @return the vat
	 */
	public String getVat() {
		return vat;
	}

	/**
	 * Sets the vat.
	 *
	 * @param vat the new vat
	 */
	public void setVat(String vat) {
		if(vat!=null)
			this.vat = vat.trim();
	}
	/*************************************************************************************************************************/
}
