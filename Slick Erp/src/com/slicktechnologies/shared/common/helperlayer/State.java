package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
@Cache
public class State extends SuperModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7126697059112597123L;
	@Index
	String country;
	@Index
	String stateName;
	@Index
	boolean status;
	
	/**
	 * Date : 01-06-2017 BY ANIL
	 */
	@Index
	String stateCode;
	
	/**
	 * End
	 */
	
	
	protected Key<Country>countryKey;
	
	
	
	public State() {
		super();
		country="";
		stateName="";
		stateCode="";
	}
	
	
     

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Code")
	public String getStateCode() {
		return stateCode;
	}






	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 0, isFieldUpdater = false, isSortable = true, title = "Country")
	public String getCountry() {
		return country;
	}







	public void setCountry(String country) {
		if(country!=null)
		   this.country = country.trim();
	}






	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "State")
	public String getStateName() {
		return stateName;
	}







	public void setStateName(String stateName) {
		if(stateName!=null)
		  this.stateName = stateName.trim();
	}







	public Key<Country> getCountryKey() {
		return countryKey;
	}







	public void setCountryKey(Key<Country> countryKey) {
		this.countryKey = countryKey;
	}







	@Override
	public boolean isDuplicate(SuperModel model) {
		boolean statebool=isDuplicateState(model);
		boolean countbool=isDuplicateCountry(model);
		return statebool&&countbool;
	}
		
		
	private boolean  isDuplicateState(SuperModel model)
	{
		State obj=(State) model;
		String name = obj.getStateName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.stateName.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(name.equals(currentName))
						return true;
					
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if (name.equals(currentName))
						return true;
					
					else
						return false;	
				}		
		
	
	}
	
	private boolean isDuplicateCountry(SuperModel model)
	{
		State obj=(State) model;
		String name = obj.getCountry().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.getCountry().trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(name.equals(currentName))
						return true;
					
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if (name.equals(currentName))
						return true;
					
					else
						return false;	
				}		
	}
	
	@GwtIncompatible
	@OnSave
	private void createCountryKey()
	{
		this.countryKey=ofy().load().type(Country.class)
				.filter("countryName",getCountry()).filter("companyId",getCompanyId()).keys().first().now();
	}
	@GwtIncompatible
	@OnLoad
	private void loadCountry()
	{
		Country country = null;
		if(countryKey!=null)
		{
		   country=ofy().load().key(countryKey).now();
		}
		if(country!=null)
		{
			this.country=country.getCountryName();
		}
	}







	public void setStatus(Boolean value) {
		this.status=value;
		
	}






	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Status")
	public boolean getStatus() {
		return status;
	
	}
	
	public boolean isStatus() {
		return status;
	}







	public void setStatus(boolean status) {
		this.status = status;
	}






	public static void makeOjbectListBoxLive(ObjectListBox<State> olbState) {
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new State());
		olbState.MakeLive(querry);
		
	}
	
	@Override
	public String toString() {
		return stateName+"";
	}
	
	
	

}
