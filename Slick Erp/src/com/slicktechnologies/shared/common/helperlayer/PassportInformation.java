package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
@Embed
public class PassportInformation  implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6573130938185248542L;
	public String passportNumber;
	public String issuedAt;
	public Date issueDate;
	public Date expiryDate;
	public PassportInformation() {
		super();
		passportNumber="";
		issuedAt="";
		
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		if(passportNumber!=null)
		this.passportNumber = passportNumber.trim();
	}
	public String getIssuedAt() {
		return issuedAt;
	}
	public void setIssuedAt(String issuedAt) {
		if(issuedAt!=null)
		this.issuedAt = issuedAt.trim();
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	

}
