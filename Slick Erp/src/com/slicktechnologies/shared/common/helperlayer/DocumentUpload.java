package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;


// TODO: Auto-generated Javadoc
/**
 * Represents Metadata of uploaded documents.
 */
@Embed
public class DocumentUpload implements Serializable 
{	
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4642382596422695362L;
	
	/** The name of document */
	protected String name;
	
	/** The status. */
	protected boolean status;

	/**
	 * Url of the Uploaded document
	 */
	protected String url;
	
	/**
	 * Date : 19-11-2018 By ANIL
	 */
	protected String uploadedBy;
	protected Date uploadDate;
	
	/***********************************************Default Ctor****************************************************/
	public DocumentUpload()
	{
		name="";
		url="";
		uploadedBy="";
		/**
		 * Date : 22-12-2018 BY ANIL
		 */
		uploadDate=null;
	}
	
	/***********************************************Getter/Setter****************************************************/
	
	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		if(name!=null)
		  this.name = name.trim();
	}

	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) 
	{
		if(url!=null)
			this.url = url.trim();
	}
	
	/***************************************************************************************************************************/
}
