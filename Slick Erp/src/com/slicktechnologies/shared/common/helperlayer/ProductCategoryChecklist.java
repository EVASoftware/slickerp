package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ProductCategoryChecklist implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4219237592805845898L;
	
	String checklistName;
	
	public ProductCategoryChecklist(){
		checklistName ="";
	}

	public String getChecklistName() {
		return checklistName;
	}

	public void setChecklistName(String checklistName) {
		this.checklistName = checklistName;
	}
	
	
	

}
