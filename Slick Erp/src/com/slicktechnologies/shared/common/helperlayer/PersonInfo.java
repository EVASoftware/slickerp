package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


/**
 * The PersonInfo Represents the Person's identity and cell no. (will get replaced by contactInfo)
 */
@SuppressWarnings("serial")
@Embed
@Index
public class PersonInfo implements Serializable 
{	
	/***********************************************Entity Attributes****************************************************/
	
	/** The cell number. */
	protected Long cellNumber;
	
	/** The full name. */
	protected String fullName;
	
	/** The id. */
	protected int count;
	
	/** The email */
	protected String email;
	
	protected String pocName;
	
	/**
	 * Date 10 April 2017
	 * added by vijay
	 * for differentiate between customer and vendor
	 */
	protected boolean isVendor;
	/**
	 * ends here
	 */
	
	/**
	 * Date : 12-06-2017 BY ANIL
	 */
	
	protected String status;
	/**
	 * End
	 */
	
	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new person info.
	 */
	public PersonInfo() {
		super();
	    fullName="";
	    email="";
	    pocName="";
	    status="";
	}
	
	/***********************************************Getter/Setter****************************************************/
	/**
	 * Instantiates a new person info.
	 *
	 * @param cellNumber the cell number
	 * @param fullName the full name
	 * @param id the id
	 */
	public PersonInfo(Long cellNumber, String fullName, int id,String pocName) {
		super();
		this.cellNumber = cellNumber;
		this.fullName = fullName;
		this.count = id;
		this.pocName=pocName;
		status="";
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if(email!=null)
		  this.email = email.trim();
	}

	/**
	 * Gets the cell number.
	 *
	 * @return the cell number
	 */
	public Long getCellNumber() {
		return cellNumber;
	}
	
	/**
	 * Sets the cell number.
	 *
	 * @param cellNumber the new cell number
	 */
	public void setCellNumber(Long cellNumber) {
		this.cellNumber = cellNumber;
	}
	
	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		if(fullName!=null)
		  this.fullName = fullName.trim();
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getCount() {
		return count;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setCount(int id) {
		this.count = id;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		if(pocName!=null){
			this.pocName = pocName.trim();
		}
	}
	
	public boolean isVendor() {
		return isVendor;
	}

	public void setVendor(boolean isVendor) {
		this.isVendor = isVendor;
	}
	
	

	
	
	/********************************************************************************************************************************/

}
