package com.slicktechnologies.shared.common.helperlayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class IPAddressAuthorization extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7896929499907515451L;

	
	/***********************************Applicability Attributes************************************/
	
	@Index
	String ipAddress;
	String description;
	@Index
	boolean status;
	
	/***************************************Constructor***********************************************/
	
	public IPAddressAuthorization() {
		super();
		ipAddress="";
		description="";
	}
	
	/***************************************Getters And Setters*********************************************/
	
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		if(ipAddress!=null){
			this.ipAddress = ipAddress.trim();
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	/***************************************Overridden Method******************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	
	

}
