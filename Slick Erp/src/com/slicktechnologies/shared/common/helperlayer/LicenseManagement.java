package com.slicktechnologies.shared.common.helperlayer;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;

@Entity
public class LicenseManagement extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6671250227161518347L;

	
	/****************************************Applicability Attributes**********************************/
	
	@Index
	protected String companyName;
	@Index
	protected long companyCount;
	protected int previousUserCount;
	protected int currentUserCount;
	protected Date previousEndDate;
	protected Date currentEndDate;
	protected Date licenseStartDate;
	@Index
	protected Date updationDate;
	
	
	/***************************************Constructor*********************************************/
	
	public LicenseManagement() {
		super();
		companyName="";
	}
	
	/**************************************Getters And Setters**************************************/
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		if(companyName!=null){
			this.companyName = companyName.trim();
		}
	}

	public long getCompanyCount() {
		return companyCount;
	}

	public void setCompanyCount(long companyCount) {
		this.companyCount = companyCount;
	}

	public int getPreviousUserCount() {
		return previousUserCount;
	}

	public void setPreviousUserCount(int previousUserCount) {
		this.previousUserCount = previousUserCount;
	}

	public int getCurrentUserCount() {
		return currentUserCount;
	}

	public void setCurrentUserCount(int currentUserCount) {
		this.currentUserCount = currentUserCount;
	}

	public Date getPreviousEndDate() {
		return previousEndDate;
	}

	public void setPreviousEndDate(Date previousEndDate) {
		if(previousEndDate!=null){
			this.previousEndDate = previousEndDate;
		}
	}

	public Date getCurrentEndDate() {
		return currentEndDate;
	}

	public void setCurrentEndDate(Date currentEndDate) {
		if(currentEndDate!=null){
			this.currentEndDate = currentEndDate;
		}
	}

	public Date getLicenseStartDate() {
		return licenseStartDate;
	}

	public void setLicenseStartDate(Date licenseStartDate) {
		if(licenseStartDate!=null){
			this.licenseStartDate = licenseStartDate;
		}
	}

	public Date getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		if(updationDate!=null){
			this.updationDate = updationDate;
		}
	}

	
	/************************************Business Logic*****************************************/
	
	@OnSave
	@GwtIncompatible
	public void reactOnDataSave()
	{
		setUpdationDate(DateUtility.getDateWithTimeZone("IST", new Date()));
	}
	
	/**************************************Overridden Method***************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
