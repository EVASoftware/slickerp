package com.slicktechnologies.shared.common.helperlayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class SmsConfiguration extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3688776103196916037L;

	

	/****************************************Applicability Attributes*********************************/
	
	@Index
	protected String accountSID;
	@Index
	protected String authToken;
	@Index
	protected String password; 
//	protected long fromNumber;
	@Index
	protected boolean status;
	
	protected String smsAPIUrl;
	
	protected String mobileNoPrefix;
	/**********************************************Constructor******************************************/

	public SmsConfiguration() {
		super();
		accountSID="";
		authToken="";
		password="";
		smsAPIUrl = "";
		mobileNoPrefix ="";
		
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/****************************************Getters & Setters****************************************/
	
	public String getAccountSID() {
		return accountSID;
	}

	public void setAccountSID(String accountSID) {
		this.accountSID = accountSID;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

//	public long getFromNumber() {
//		return fromNumber;
//	}
//
//	public void setFromNumber(long fromNumber) {
//		this.fromNumber = fromNumber;
//	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	

	public String getSmsAPIUrl() {
		return smsAPIUrl;
	}

	public void setSmsAPIUrl(String smsAPIUrl) {
		this.smsAPIUrl = smsAPIUrl;
	}
	
	public String getMobileNoPrefix() {
		return mobileNoPrefix;
	}

	public void setMobileNoPrefix(String mobileNoPrefix) {
		this.mobileNoPrefix = mobileNoPrefix;
	}

	/*******************************************Overridden Method**************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}


}
