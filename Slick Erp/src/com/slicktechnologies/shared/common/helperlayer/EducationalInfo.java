package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.personlayer.Employee;

// TODO: Auto-generated Javadoc
/**
 * The Class EducationalInfo. Represents the educational details of the {@link Employee}
 */
@Embed
public class EducationalInfo implements Serializable
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9195391190279391560L;

	/** The schoolname. */
	protected String schoolName;
	
	/** The year of passing. */
	protected String yearOfPassing;

	/** The percentage obtained */
	protected String percentage;
	
	/** The degree obtained. */
	protected String degreeObtained;
	
	/** The university attended */
	protected String university;
	
	
	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new educational info.
	 */
	public EducationalInfo() {
		super();
		schoolName="";
		degreeObtained="";
		university="";
		yearOfPassing="";
	}

	/***********************************************Getter/Setter****************************************************/
	/**
	 * Gets the school name.
	 *
	 * @return the school name
	 */
	public String getSchoolName() {
		return schoolName;
	}

	/**
	 * Sets the school name.
	 *
	 * @param schoolName the new school name
	 */
	public void setSchoolName(String schoolName) {
		if(schoolName!=null)
		  this.schoolName = schoolName.trim();
	}

	

	public String getYearOfPassing() {
		return yearOfPassing;
	}

	public void setYearOfPassing(String yearOfPassing) {
		this.yearOfPassing = yearOfPassing;
	}

	/**
	 * Gets the percentage.
	 *
	 * @return the percentage
	 */
	public String getPercentage() {
		return percentage;
	}

	/**
	 * Sets the percentage.
	 *
	 * @param value the new percentage
	 */
	public void setPercentage(String value) {
		if(value!=null)
		  this.percentage = value;
	}

	/**
	 * Gets the degree obtained.
	 *
	 * @return the degree obtained
	 */
	public String getDegreeObtained() {
		return degreeObtained;
	}

	/**
	 * Sets the degree obtained.
	 *
	 * @param degreeObtained the new degree obtained
	 */
	public void setDegreeObtained(String degreeObtained) {
		if(degreeObtained!=null)
			this.degreeObtained = degreeObtained.trim();
	}

	/**
	 * Gets the university.
	 *
	 * @return the university
	 */
	public String getUniversity() {
		return university;
	}

	/**
	 * Sets the university.
	 *
	 * @param university the new university
	 */
	public void setUniversity(String university) {
		if(university!=null)
		  this.university = university.trim();
	}
	/************************************************************************************************************************/

}
