package com.slicktechnologies.shared.common.helperlayer;

public class SessionManagement {
	
	protected String userName;
	protected int sessionId;
	protected long lastAccessedTime;
	protected long companyId;
	protected String sessionIdByServer;
	
	protected String employeeName;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public long getLastAccessedTime() {
		return lastAccessedTime;
	}
	public void setLastAccessedTime(long lastAccessedTime) {
		this.lastAccessedTime = lastAccessedTime;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getSessionIdByServer() {
		return sessionIdByServer;
	}
	public void setSessionIdByServer(String sessionIdByServer) {
		this.sessionIdByServer = sessionIdByServer;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	
}
