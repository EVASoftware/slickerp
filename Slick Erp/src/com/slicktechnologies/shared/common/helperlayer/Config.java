package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigEntity. Represents the Configurable elements.
 */
@Entity
@Index
@Cache
public class Config extends SuperModel implements Serializable 
{	
	
	/** *****************************Entity Attributes***************************************************. */
	/** The Constant serialVersionUID. */
	protected static final long serialVersionUID = 1670970289762969531L;
	
	
	/** The name. */
	@Index 
	protected String name;
	
	/** The type. */
	@Index
	protected int type;
	

	protected String description;
	
	/** The status. */
	@Index 
	protected boolean status=true;
	
	@Index 
	protected boolean gstApplicable;
	
	/**
	 * @author Anil @since 01-10-2021
	 * Capturing invoice title 
	 * Raised by Rahul Tiwari and Nitin Sir
	 */
	protected String invoiceTitle;
	boolean printBankDetails;
	
	protected String wholeAmtLabel; //Ashwini Patil Date: 12-04-2022 
	

	protected String decimalLabel; //Ashwini Patil Date: 12-04-2022
	
	
	
	

	/**
	 * ******************************Default Ctor***************************************************.
	 */
	/**
	 * Instantiates a new config entity.
	 */
	public Config() 
	{
		name="";
		description="";
		wholeAmtLabel="";
		decimalLabel="";
		gstApplicable = false;
	}
	
	public boolean isPrintBankDetails() {
		return printBankDetails;
	}

	public void setPrintBankDetails(boolean printBankDetails) {
		this.printBankDetails = printBankDetails;
	}
	
	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}
	
	public String getWholeAmtLabel() {
		return wholeAmtLabel;
	}

	public void setWholeAmtLabel(String wholeAmtLabel) {
		this.wholeAmtLabel = wholeAmtLabel;
	}

	public String getDecimalLabel() {
		return decimalLabel;
	}

	public void setDecimalLabel(String decimalLabel) {
		this.decimalLabel = decimalLabel;
	}
	/**
	 * *****************************Abstract Method Implementation**********************************.
	 *
	 * @param model the model
	 * @return true, if is duplicate
	 */
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel model) 
	{
		Config obj=(Config) model;
		String name = obj.getName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.name.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
		if(obj.id==null)
		{
			if(name.equals(currentName))
				return true;
			
			else
				return false;
		}
		
		//while updating a Old object
		else
		{
			if(obj.id.equals(id))
				return false;
			
			else if (name.equals(currentName))
				return true;
			
			else
				return false;	
		}			
	}
	
	/**
	 * *****************************SuperModel's overridden method************************************.
	 *
	 * @param obj the obj
	 * @return the int
	 */
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel obj) 
	{
		Config entityToCompare=(Config) obj;
		String name=this.name.toLowerCase();
		String objName=entityToCompare.name.toLowerCase();
		return name.compareTo(objName);
	}
	
	/**
	 * *****************************Getter/Setter***************************************************.
	 *
	 * @return the id
	 */
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		if(name!=null)
		  this.name = name.trim();
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null)
		  this.description = description.trim();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.name;
	}

	/**
	 * Sets the type.
	 *
	 * @param configType the new type
	 */
	public void setType(int configType) {
		this.type=configType;
		
	}

	public boolean isGstApplicable() {
		return gstApplicable;
	}

	public void setGstApplicable(boolean gstApplicable) {
		this.gstApplicable = gstApplicable;
	}
	
	
	
	
	
	/************************************************************************************************/
	
}
