package com.slicktechnologies.shared.common.helperlayer;


import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
@Entity
public class CustomerUser extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6944112042917937981L;


	@Index
	protected String customerName;
	@Index 
	protected String userName;
	@Index 
	protected String password;
	
	protected boolean status;
	protected String branch;
	 
	@Index 
	protected PersonInfo cinfo;
	 
	/**
	 * This email stores user email address
	 * Date 17-09-2016 By Anil
	 * Release : 30 Sept 2016
	 */
    @Index
    String email;
    /*
     * End
     */
    /**
     * Updated By: Amol
     * Date: 19-02-2020
     * Description: To store company Type 
     */
    protected String companyType;
	 
	public CustomerUser() {
		super();
		userName="";
		customerName="";
		password="";
		branch="";
		email="";
	}
	
	
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Customer")
	public String getCustomerName() {
		return customerName;
	}




	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}



	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "User ID")
	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Branch")
	public String getBranch() {
		return branch;
	}




	public void setBranch(String branch) {
		this.branch = branch;
	}
	

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}




	public void setStatus(boolean status) {
		this.status = status;
	}



	




	public PersonInfo getCinfo() {
		return cinfo;
	}




	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
			
			boolean res1=isDuplicateUser(m);
			boolean res2=isDuplicateEmployee(m);
			boolean res3=isDuplicateEmail(m);
			System.out.println("Result of User Duplicate  "+(res1));
			System.out.println("Result of Employee Duplicate  "+(res2));
			
			return res1||res2||res3;
			
	    }

	public boolean isDuplicateUser(SuperModel model)
	{
		CustomerUser entity = (CustomerUser) model;
		String name = entity.getUserName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		String curname=this.getUserName().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}

	public boolean isDuplicateEmployee(SuperModel model)
	{
		CustomerUser entity = (CustomerUser) model;
		String name = entity.getCustomerName().trim();
		String curname=this.customerName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}

	
	/**
	 * This method checks you entered email id is already exist or not
	 * Date :17-09-2016 By Anil
	 * Release 30 Sept 2016
	 */
	
	public boolean isDuplicateEmail(SuperModel model)
	{
		CustomerUser entity = (CustomerUser) model;
		String name = entity.getEmail().trim();
		String curname=this.email.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}


	public String getCompanyType() {
		return companyType;
	}




	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}
	
}
