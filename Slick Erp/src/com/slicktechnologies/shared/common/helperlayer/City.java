package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
@Cache
public class City extends SuperModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7126690009112597123L;
	@Index
	String stateName;
	@Index
	String cityName;
	
	@Index
	boolean status;
	
	protected Key<State>stateKey;
	
	/**
	 * @author Anil Date : 22-08-2019
	 */
	@Index
	String className;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 * storing turn around time city and tier wise
	 */
	String tat;
	
	public City() {
		super();
		stateName="";
		cityName="";
	}
	
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setState(String country) {
		if(country!=null)
		   this.stateName = country.trim();
	}







	public String getStateName() {
		return stateName;
	}







	public void setStateName(String stateName) {
		if(stateName!=null)
		  this.stateName = stateName.trim();
	}







	






	public String getCityName() {
		return cityName;
	}














	public void setCityName(String cityName) {
		if(cityName!=null)
		   this.cityName = cityName.trim();
	}





    @Override
	public boolean isDuplicate(SuperModel model) {
		boolean statebool=isDuplicateState(model);
		boolean countbool=isDuplicateCity(model);
		return statebool&&countbool;
	}
		
		
	private boolean  isDuplicateState(SuperModel model)
	{
		City obj=(City) model;
		String name = obj.getStateName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.stateName.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(name.equals(currentName))
						return true;
					
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if (name.equals(currentName))
						return true;
					
					else
						return false;	
				}		
		
	
	}
	
	private boolean isDuplicateCity(SuperModel model)
	{
		City obj=(City) model;
		String name = obj.getCityName();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.getCityName().trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(name.equals(currentName))
						return true;
					
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if (name.equals(currentName))
						return true;
					
					else
						return false;	
				}		
	}
	
	@GwtIncompatible
	@OnSave
	private void createCountryKey()
	{
		this.stateKey=ofy().load().type(State.class).filter("companyId",getCompanyId())
				.filter("stateName",getStateName()).keys().first().now();
	}
	@GwtIncompatible
	@OnLoad
	private void loadCountry()
	{
		State country = null;
		if(stateKey!=null)
		{
		   country=ofy().load().key(stateKey).now();
		}
		if(country!=null)
		{
			this.stateName=country.getStateName();
		}
	}














	












	public boolean isStatus() {
		return status;
	}














	public void setStatus(Boolean status) {
		this.status = status;
		System.out.println("Status is "+status);
	}














	@Override
	public String toString() {
		return cityName+"";
	}














	public static void makeOjbectListBoxLive(ObjectListBox<City> olbState) {
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new City());
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		olbState.MakeLive(querry);
		
	}


	public String getTat() {
		return tat;
	}


	public void setTat(String tat) {
		this.tat = tat;
	}
	
	
	
	

}
