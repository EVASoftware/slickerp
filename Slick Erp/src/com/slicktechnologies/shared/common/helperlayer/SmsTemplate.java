package com.slicktechnologies.shared.common.helperlayer;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

@Entity
public class SmsTemplate extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7882695802347746511L;
	
	
	


	/*****************************************Applicability Attributes****************************************/
	
	@Index
	protected String event;
//	@Index //Ashwini Patil Date:30-06-2023 commented as it was not allowing more than 500 characters 
	protected String message;
	@Index
	protected int day;
	@Index
	protected boolean status;
	@Index
	protected String availableTags;
	
	protected String landingPageText;
	
	protected Boolean automaticMsg;
	
	/*********************************************Constructor************************************/
	
	public SmsTemplate() {
		super();
		event="";
		message="";
		availableTags="";
		landingPageText="";
		automaticMsg = false;
	}


/********************************************Getters And Setters*********************************************/


		
		public String getEvent() {
			return event;
		}
		
		
		public void setEvent(String event) {
			if(event!=null){
				this.event = event.trim();
			}
		}
		
		
		public String getMessage() {
			return message;
		}
		
		
		public void setMessage(String message) {
			if(message!=null){
				this.message = message.trim();
			}
		}
		
		
		public int getDay() {
			return day;
		}
		
		
		public void setDay(int day) {
			this.day = day;
		}
		
		public boolean getStatus() {
			return status;
		}
		
		
		public void setStatus(boolean status) {
			this.status = status;
		}
		
		public String getAvailableTags() {
			return availableTags;
		}

		public void setAvailableTags(String availableTags) {
			this.availableTags = availableTags;
		}

		

		public String getLandingPageText() {
			return landingPageText;
		}


		public void setLandingPageText(String landingPageText) {
			this.landingPageText = landingPageText;
		}
		

		public Boolean getAutomaticMsg() {
			return automaticMsg;
		}


		public void setAutomaticMsg(Boolean automaticMsg) {
			this.automaticMsg = automaticMsg;
		}


/******************************************Overridden Method************************************************/


	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return event;
	}
	
	
	@OnSave
	@GwtIncompatible
	private void updateCommunicationConfiguration(){
		
		List<EmailTemplateConfiguration> emailtemplateconfiglist = ofy().load().type(EmailTemplateConfiguration.class).filter("companyId", this.getCompanyId())
										.filter("templateName", this.getEvent()).list();
		
		if(emailtemplateconfiglist!=null && emailtemplateconfiglist.size()>0) {
			for(EmailTemplateConfiguration emailconfig : emailtemplateconfiglist) {
				if(this.getAutomaticMsg()!=null)
				emailconfig.setAutomaticMsg(this.getAutomaticMsg());
			}
			ofy().save().entities(emailtemplateconfiglist);
		}
	}
 
	


}
