package com.slicktechnologies.shared.common.helperlayer;

import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.DateUtility;

@Entity
public class LoggedIn extends SuperModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5003036575160595159L;
	
/**********************************************Applicability Attributes*********************************/
	
	@Index
	protected String employeeName;
	@Index
	protected String userName;
	protected String loginTime;
	protected String logoutTime;
	@Index
	protected Date loginDate;
	protected Date logoutDate;
	@Index
	protected String processName;
	@Index
	protected int sessionId;
	@Index
	protected String status;
	@Index
	protected String remark;
	
	
	/********************************************Constructor*******************************************/
	
	public LoggedIn() {
		super();
		employeeName="";
		userName="";
		loginTime="";
		logoutTime="";
		processName="";
		status="";
		remark="";
	}

	/******************************************Getters And Setters*******************************************/
	
	
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		if(employeeName!=null){
			this.employeeName = employeeName.trim();
		}
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		if(userName!=null){
			this.userName = userName.trim();
		}
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		if(loginTime!=null){
			this.loginTime = loginTime;
		}
	}

	public String getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(String logoutTime) {
		if(logoutTime!=null){
			this.logoutTime = logoutTime;
		}
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		if(loginDate!=null){
			this.loginDate = loginDate;
		}
	}

	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Date logoutDate) {
		if(logoutDate!=null){
			this.logoutDate = logoutDate;
		}
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		if(processName!=null){
			this.processName = processName.trim();
		}
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if(status!=null){
			this.status = status.trim();
		}
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		if(remark!=null){
			this.remark = remark.trim();
		}
	}

	
/*****************************************On Save Method*******************************************/
	
	@GwtIncompatible
	@OnSave
	public void reactOnSessionId()
	{
		if(this.getStatus().trim().equals(AppConstants.ACTIVE))
		{
			setLoginDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			setLoginTime(DateUtility.getTimeWithTimeZone("IST", new  Date()));
		}
		
		setSessionId(getCount());
	}
	
	/***************************************Compare To**************************************************/

	@Override
	public int compareTo(SuperModel o) {
		return 0;
	}

	/*******************************************Overridden Method*****************************************/
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	
}
