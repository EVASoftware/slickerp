package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
@Cache
public class Locality extends SuperModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7126690009112597123L;
	@Index
	String locality;
	@Index
	String cityName;
	
	@Index
	
	boolean status;
	
	protected Key<City>cityKey;
	
	/**	
	 * 25-07-2017
	 * sagar sore
	 */
	@Index
	long pinCode;
		
	
	public Locality() {
		super();
		locality="";
		cityName="";
		pinCode=0L;
	}
	
	
     











	public void setLocality(String country) {
		if(country!=null)
		   this.locality = country.trim();
	}







	public String getLocality() {
		return locality;
	}







	public void setStateName(String stateName) {
		if(stateName!=null)
		  this.locality = stateName.trim();
	}







	






	public String getCityName() {
		return cityName;
	}














	public void setCityName(String cityName) {
		if(cityName!=null)
		   this.cityName = cityName.trim();
	}


	public long getPinCode() {
		return pinCode;
	}

	public void setPinCode(long pinCode) {
		this.pinCode = pinCode;
	}



    @Override
	public boolean isDuplicate(SuperModel model) {
		boolean statebool=isDuplicateState(model);
		boolean countbool=isDuplicateCity(model);
		return statebool&&countbool;
	}
		
		
	private boolean  isDuplicateState(SuperModel model)
	{
		Locality obj=(Locality) model;
		String name = obj.getLocality().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.locality.trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(name.equals(currentName))
						return true;
					
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if (name.equals(currentName))
						return true;
					
					else
						return false;	
				}		
		
	
	}
	
	private boolean isDuplicateCity(SuperModel model)
	{
		Locality obj=(Locality) model;
		String name = obj.getCityName();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String currentName=this.getCityName().trim();
		currentName=currentName.replaceAll("\\s","");
		currentName=currentName.toLowerCase();
		
		//check duplicate value while new Object is being added
				if(obj.id==null)
				{
					if(name.equals(currentName))
						return true;
					
					else
						return false;
				}
				
				//while updating a Old object
				else
				{
					if(obj.id.equals(id))
						return false;
					
					else if (name.equals(currentName))
						return true;
					
					else
						return false;	
				}		
	}
	
	@GwtIncompatible
	@OnSave
	private void createCountryKey()
	{
		this.cityKey=ofy().load().type(City.class).filter("companyId",getCompanyId()).
				filter("cityName",getCityName()).keys().first().now();
	}
	@GwtIncompatible
	@OnLoad
	private void loadCountry()
	{
		City country = null;
		if(cityKey!=null)
		{
		   country=ofy().load().key(cityKey).now();
		}
		if(country!=null)
		{
			this.cityName=country.getCityName();
		}
	}














	












	public boolean isStatus() {
		return status;
	}














	public void setStatus(Boolean status) {
		this.status = status;
	}














	@Override
	public String toString() {
		return locality;
	}














	public static void MakeObjectListBoxLive(ObjectListBox<Locality> locality2) {
		MyQuerry querry=new MyQuerry();
		querry.setQuerryObject(new Locality());
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		querry.getFilters().add(filter);
		locality2.MakeLive(querry);
		
	}
	
	

}
