package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


@SuppressWarnings("serial")
@Embed
@Index
public class ProductInfo extends SuperModel  implements Serializable{

	/**
	 * 
	 */
//	private static final long serialVersionUID = -3679315148040394143L;
	
	protected int prodID;
	protected String productCode;
	protected String productName;
	protected String productCategory;
	protected double productPrice;
	protected String UnitofMeasure;
	protected double serviceTax;
	protected double vat;
	
	
	/**
	 * Date : 10-07-2018 BY ANIL
	 * Adding purchase price
	 */
	
	double purchasePrice;
	
	/**
	 * 
	 */
	boolean status;
	
	
	public ProductInfo()
	{
		super();
		productCode="";
		productName="";
		productCategory="";
	}
	
	
	
	
/****************************************Getters and Setters**************************************************/	
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public double getPurchasePrice() {
		return purchasePrice;
	}


	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	
	public double getServiceTax() {
		return serviceTax;
	}
	




	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	public ProductInfo(int prodID,String productCode,String productName,String productCategory,double productPrice)
	{
		super();
		this.prodID=prodID;
		this.productCode=productCode;
		this.productName=productName;
		this.productCategory=productCategory;
		this.productPrice=productPrice;
	}

	
	
	
	public int getProdID() {
		return prodID;
	}

	public void setProdID(int prodID) {
		this.prodID = prodID;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	
	
	public String getUnitofMeasure() {
		return UnitofMeasure;
	}

	public void setUnitofMeasure(String unitofMeasure) {
		UnitofMeasure = unitofMeasure;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
		
	

}
