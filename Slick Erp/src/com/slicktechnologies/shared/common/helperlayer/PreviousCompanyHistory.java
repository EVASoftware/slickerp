package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * The Class PreviousCompanyHistory. Represents the {@link Employee} history in previous company.
 */
@Embed
public class PreviousCompanyHistory implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3673625705014591277L;
	String company, designation,adress;
	double salary;
	String fromDate,toDate;
	/**@author Amol  
	 * Date 7-4-2019 
	 * added column Pfnumber ,pensionpaymentnumber,schemecirtificatenumber
	 */
	String pfNumber;
	String pensionPaymentOrderNumber;
	String schemeCirtificateNumber;
	String reasonOfLeaving;
	
	public PreviousCompanyHistory()
	{
		company="";
		designation="";
		adress="";
		fromDate="";
		toDate="";
		pfNumber="";
		pensionPaymentOrderNumber="";
		schemeCirtificateNumber="";
		reasonOfLeaving="";
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public double getSalary() {
		return salary;
	}


	public void setSalary(double salary) {
		this.salary = salary;
	}


	public String getAdress() {
		return adress;
	}


	public void setAdress(String adress) {
		this.adress = adress;
	}


	public String getFromDate() {
		return fromDate;
	}


	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}


	public String getToDate() {
		return toDate;
	}


	public void setToDate(String toDate) {
		this.toDate = toDate;
	}


	public String getPfNumber() {
		return pfNumber;
	}


	public void setPfNumber(String pfNumber) {
		this.pfNumber = pfNumber;
	}


	public String getPensionPaymentOrderNumber() {
		return pensionPaymentOrderNumber;
	}


	public void setPensionPaymentOrderNumber(String pensionPaymentOrderNumber) {
		this.pensionPaymentOrderNumber = pensionPaymentOrderNumber;
	}


	public String getSchemeCirtificateNumber() {
		return schemeCirtificateNumber;
	}


	public void setSchemeCirtificateNumber(String schemeCirtificateNumber) {
		this.schemeCirtificateNumber = schemeCirtificateNumber;
	}


	public String getReasonOfLeaving() {
		return reasonOfLeaving;
	}


	public void setReasonOfLeaving(String reasonOfLeaving) {
		this.reasonOfLeaving = reasonOfLeaving;
	}



}
