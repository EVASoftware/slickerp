

package com.slicktechnologies.shared.common.helperlayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;

// TODO: Auto-generated Javadoc
/**
 * Repersents any Generic Category for Configguration Purposes.
 */

@Entity
@Cache
public class ConfigCategory  extends SuperModel implements Serializable
{
   
   /**
	 * 
	 */
	private static final long serialVersionUID = 141398370449295364L;

/** The category code. */
	@Index
   protected	String categoryCode;
   
   /** The category name. */
   @Index
   protected	String categoryName;
   
   /** The description. */
   protected	String description;
   
   /** Repersents type of Category for example It may be One for Customer,two for Contract etc*/
   @Index
   protected	int internalType;
   /** Status  of this Category **/
   @Index
   protected boolean status;
	
	/**
	 * Instantiates a new category.
	 */
	public ConfigCategory() {
		super();
		categoryCode="";
		categoryName="";
		description="";
	}
	
	/**
	 * Gets the category code.
	 *
	 * @return the category code
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Category Code")
	public String getCategoryCode() {
		return categoryCode;
	}
	
	/**
	 * Sets the category code.
	 *
	 * @param categoryCode the new category code
	 */
	
	public void setCategoryCode(String categoryCode) {
		if(categoryCode!=null)
		  this.categoryCode = categoryCode.trim();
	}
	
	/**
	 * Gets the category name.
	 *
	 * @return the category name
	 */
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Category Name")
	public String getCategoryName() {
		return categoryName;
	}
	
	/**
	 * Sets the category name.
	 *
	 * @param categoryName the new category name
	 */
	
	public void setCategoryName(String categoryName) {
		if(categoryName!=null)
		  this.categoryName = categoryName.trim();
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null)
		   this.description = description.trim();
	}
	
	/**
	 * Gets the internal type.
	 *
	 * @return the internal type
	 */
	public int getInternalType() {
		return internalType;
	}
	
	/**
	 * Sets the internal type.
	 *
	 * @param internalType the new internal type
	 */
	public void setInternalType(int internalType) {
	
		this.internalType = internalType;
	}

	public void setStatus(Boolean value) {
		this.status=value;
		
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Category Status")
	public Boolean getStatus()
	{
		return status;
		
	}

	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0!=null)
		{
			ConfigCategory cat=(ConfigCategory) arg0;
			if(cat.getCategoryName()!=null)
			{
				categoryName.compareTo(cat.getCategoryName());
			}
		}
		return -1;
	}

	/**
	 * Checks for Duplicate on Two criterias cat Code and on Cat Name
	 * @param model {@link SuperModel} object
	 * @return true if either category code and category name are same.
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		
		return isDuplicateonCatCode(m)||isDuplicateOnCatName(m);
	}
	/**
	 * Calculates duplicate on Category Name
	 * @param model {@link SuperModel} object
	 * @return
	 */
	public boolean isDuplicateOnCatName(SuperModel model)
	{
		{
			ConfigCategory entity=(ConfigCategory) model;
			String name = entity.getCategoryName().trim();
			name=name.replaceAll("\\s","");
			name=name.toLowerCase();
			
			String curname=getCategoryName().trim();
			curname=curname.replaceAll("\\s","");
			curname=curname.toLowerCase();
			//New Object is being added
			if(entity.id==null)
			{
				if(name.equals(curname))
					return true;
				else
					return false;
			}
			// Old object is being updated
			else
			{
				if(entity.id.equals(id))
					return false;	
				if(name.equals(curname)==true)
					return true;
			}
		}
		return false;
	}
	/**
	 * Compares is Duplicate on cat Code
	 * @param model {@link SuperModel} object to check for duplicate
	 * @return
	 */
	public boolean isDuplicateonCatCode(SuperModel model)
	{
		ConfigCategory entity=(ConfigCategory) model;
		String name = entity.getCategoryCode().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=getCategoryCode().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(entity.id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else
		{
			if(entity.id.equals(id))
				return false;	
			if(name.equals(curname)==true)
				return true;
		}
	
	return false;
	}

	@Override
	public String toString() {
		return categoryName.toString();
	}

	@Override
	public boolean equals(Object arg0) {
		if(arg0 instanceof ConfigCategory)
		{
			ConfigCategory con=(ConfigCategory) arg0;
			if(this.id==con.id)
				return true;
			return false;
		}
		return false;
	}
	
	
	

}
