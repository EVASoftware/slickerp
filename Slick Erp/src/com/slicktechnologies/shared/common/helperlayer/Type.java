package com.slicktechnologies.shared.common.helperlayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.shared.common.businessprocesslayer.BusinessProcess;


/**
 * The Class Type.
 */


@Entity
@Index
@Cache
public class Type extends SuperModel implements Serializable
{
	
	/** Represents a Generic Type,Type can be against any {@link BusinessProcess} Category and Type will always have one to many relationship. */
	private static final long serialVersionUID = 4383662131952982215L;
	
	/** Type Code*/
	@Index
	protected String typeCode;
	
	/** Name of Type */
	@Index
	protected String typeName;
	
	/** The status. */
	@Index
	protected boolean status;
	
	/** Corresponding category Name */
	@Index
	protected String categoryName;
	@Index
	protected String catCode;
	
	/** Field Represents the Business Process Type*/
	@Index
	protected int internalType;
	
	/** The category key. */
	@Index
	protected Key<ConfigCategory>categoryKey;
	
	
	/** The description. */
	String description;
	
	
	
	
	
	public Type() {
		super();
		typeCode="";
		typeName="";
		categoryName="";
		catCode="";
		
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Type Code")
  public String getTypeCode() {
		return typeCode;
	}


public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}


@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Type Name")

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Type Status")
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Category")

	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public int getInternalType() {
		return internalType;
	}


	public void setInternalType(int internalType) {
		this.internalType = internalType;
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof Type)
		{
			Type type=(Type) arg0;
			if(type!=null)
			{
				return typeName.compareTo(type.typeName);
			}
		}
		return 0;
	}
	
	
	
	
	
	
	

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */

	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
			
			boolean res1=isDuplicateTypeCode(m);
			boolean res2=isDuplicateTypeName(m);
			return res1||res2;
			
	    }

	public boolean isDuplicateTypeName(SuperModel model)
	{
		Type entity = (Type) model;
		
		String catname=entity.getCategoryName().trim();
		catname=catname.replaceAll("\\s","");
		catname=catname.toLowerCase();
		
		String curcatname=this.categoryName.trim();
		curcatname=curcatname.replaceAll("\\s","");
		curcatname=curcatname.toLowerCase();
		
		String name = entity.typeName;
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		String curname=typeName;
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true)&&(catname.equals(curcatname)==true))
			  return true;
			else
				return false;
				
		}
	}

	public boolean isDuplicateTypeCode(SuperModel model)
	{
		Type entity = (Type) model;
		String name = entity.typeCode.trim();
		String curname=this.typeCode.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}

	@Override
	public String toString() 
	{
		return this.typeName.toString();
	}

	
	@OnSave
	@GwtIncompatible
	public void OnSave()
	{
		if(categoryName!=null){
		   categoryKey=ofy().load().type(ConfigCategory.class).filter("companyId", this.getCompanyId()).filter("categoryName",this.categoryName).filter("internalType",this.internalType).filter("categoryCode", this.catCode.trim()).keys().first().now();
		}
	}
		
	@OnLoad
	@GwtIncompatible
	public void onLoad()
	{
	  if(categoryKey!=null)
	  {
		 ConfigCategory cat=ofy().load().key(categoryKey).now();
		 if(cat!=null)
			 this.categoryName=(cat.getCategoryName());
			 
		}
	}
}
