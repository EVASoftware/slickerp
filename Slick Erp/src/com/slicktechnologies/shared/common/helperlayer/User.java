package com.slicktechnologies.shared.common.helperlayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.role.UserRole;

import static com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * Represents software User for login purpose. At present
 *  corresponding to 1 employee there is only 1 user.
 */
@Entity
public class User extends SuperModel {
	
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -888074958627156271L;
	
	/** The user name. */
	@Index protected String userName;
	
	/** The acess level granted to user */
	protected String acessLevel;
	
	/** The employee name for this user as all user are employees */
	@Index
	protected String employeeName;
	
	/** The password. */
	@Index protected String password;
    
    /** The status. */
	@Index
    protected boolean status;
    
    protected String branch;
    
    protected UserRole role;
    
    protected String callerId; //Ashwini Patil Date:11-10-2024 for hi tech
    
    @Index
    List<String> ipAddress;
    
    /**
	 * This email stores user email address
	 * Date 17-09-2016 By Anil
	 * Release : 30 Sept 2016
	 */
    @Index
    String email;
    /*
     * End
     */
    /**
     * Date : 04-12-2017 By ANIL
     * this field stores company's license end which will be set at the time of login 
     */
    protected Date licenseEndDate;
    
    /********************************************************Relational Attributes ********************************************/
    @Index
    protected Key<EmployeeRelation>keyEmployee;
    protected Key<Config>acessLevelKey;
    protected Key<BranchRelation> branchKey;
    /**
     * Updated By: Viraj
     * Date: 22-06-2019
     * Description: To store company Type 
     */
    protected String companyType;
    protected boolean hrStatus;				//to store status of process configuration is active or not
	/** Ends **/
    
    
    protected String loginIPAddress;
    
    
    @Index 
	protected Integer empCount; //Ashwini Patil Date:22-08-2023
    
    
   

	/***********************************************Default Ctor****************************************************/
	/**
	 * Instantiates a new user.
	 */
	public User() {
		super();
		userName="";
		acessLevel="";
		employeeName="";
		password="";
		branch="";
		ipAddress= new ArrayList<String>();
		email="";
		companyType="";
		loginIPAddress="";
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.shared.common.helperlayer.SuperModel#isDuplicate(com.slicktechnologies.shared.common.helperlayer.SuperModel)
	 */
	

	/***********************************************Getter/Setter****************************************************/
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "User ID")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		if(userName!=null)
		  this.userName = userName.trim();
	}

	/**
	 * Gets the acess level.
	 *
	 * @return the acess level
	 */
	public String getAccessLevel() {
		return acessLevel;
	}

	/**
	 * Sets the acess level.
	 *
	 * @param acessLevel the new acess level
	 */
	public void setAccessLevel(String acessLevel) {
	    if(acessLevel!=null)
		  this.acessLevel = acessLevel.trim();
	}

	/**
	 * Gets the employee name.
	 *
	 * @return the employee name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Employee")
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * Sets the employee name.
	 *
	 * @param employeeName the new employee name
	 */
	public void setEmployeeName(String employeeName) {
		if(employeeName!=null)
		  this.employeeName = employeeName.trim();
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "User Role")
	public String getRoleName() {
		return role.getRoleName();
	}

	public void setRoleName(String roleName) {
		role.setRoleName(roleName);
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		if(password!=null)
		  this.password = password.trim();
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {
			
			boolean res1=isDuplicateUser(m);
			boolean res2=isDuplicateEmployee(m);
			boolean res3=isDuplicateEmail(m);
			
			return res1||res2||res3;
			
	    }

	public boolean isDuplicateUser(SuperModel model)
	{
		User entity = (User) model;
		String name = entity.getUserName().trim();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		System.out.println("for duplication "+name);
		
		String curname=this.getUserName().trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		
		System.out.println("for duplication 123"+curname);
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}

	public boolean isDuplicateEmployee(SuperModel model)
	{
		User entity = (User) model;
		String name = entity.getEmployeeName().trim();
		String curname=this.employeeName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}


	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Status")
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
	public String getAcessLevel() {
		return acessLevel;
	}

	public void setAcessLevel(String acessLevel) {
		this.acessLevel = acessLevel;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Branch")
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch)
	{
		if(branch!=null)
			this.branch = branch.trim();
	}

	public Key<EmployeeRelation> getKeyEmployee() {
		return keyEmployee;
	}

	public void setKeyEmployee(Key<EmployeeRelation> keyEmployee) {
		this.keyEmployee = keyEmployee;
	}

	public Key<Config> getAcessLevelKey() {
		return acessLevelKey;
	}

	public void setAcessLevelKey(Key<Config> acessLevelKey) {
		this.acessLevelKey = acessLevelKey;
	}

	public Key<BranchRelation> getBranchKey() {
		return branchKey;
	}

	public void setBranchKey(Key<BranchRelation> branchKey) {
		this.branchKey = branchKey;
	}

	/******************************************************************************************************************************/
	@GwtIncompatible
	  @OnSave
	  public void OnSave()
	  {
		 
//		  
//			  MyQuerry querry=new MyQuerry();
//			  Filter filter=null;
//			  
//			  filter=new Filter();
//			  filter.setQuerryString("employeeName");
//			  filter.setStringValue(this.employeeName);
//			  
////			  filter.setQuerryString(this.branch);
//			  System.out.println("Querry Filter "+querry.getFilters());
//			  querry.getFilters().add(filter);
//			  querry.setQuerryObject(new EmployeeRelation() );
//			  
//			  keyEmployee= (Key<EmployeeRelation>) MyUtility.getRelationalKeyFromCondition(querry);	
//			  System.out.println("KEY EMPLOYEE ::::: :::: :::: :::: :: "+keyEmployee);
//			  
//			  acessLevelKey=MyUtility.getConfigKeyFromCondition(this.acessLevel,ConfigTypes.ACESSLEVEL.getValue());
//			  
//			  filter=new Filter();
//			  filter.setQuerryString("branchName");
////			  filter.setQuerryString(this.branch);
//			  filter.setStringValue(this.branch);
//			  
//			  System.out.println("Querry Filter "+querry.getFilters());
//			  querry.getFilters().add(filter);
//			  querry.setQuerryObject(new BranchRelation() );
//			  
//			  
//			  branchKey=(Key<BranchRelation>) MyUtility.getRelationalKeyFromCondition(querry);
//			 
//		  
//			 
			if(this.getStatus()) {
				List<LoggedIn> loggedInlist = ofy().load().type(LoggedIn.class).filter("companyId", this.getCompanyId()).filter("userName", this.getUserName())
									.filter("remark", "Invalid Login").filter("status", AppConstants.INACTIVE).list();
				if(loggedInlist.size()>0) {
					ofy().delete().entities(loggedInlist);
				}
			}
		
		
	  }
	  @GwtIncompatible
	  @OnLoad
	  public void OnLoad()
	  {
//		  //Get Entity From Key for Branch 
//		
//		  if(keyEmployee!=null)
//		  {
//			  String employeeName=MyUtility.getEmployeeNameFromKey(keyEmployee);
//			  if(employeeName!=null)
//			  {
//				  if(employeeName.equals("")==false)
//					  setEmployeeName(employeeName.trim());
//			  }
//		  }
//		  
//		  if(acessLevelKey!=null)
//		  {
//			  String acesslevel=MyUtility.getConfigNameFromKey(acessLevelKey);
//			  if(acesslevel!=null)
//			  {
//				  if(acesslevel.equals("")==false)
//					  setAccessLevel(acesslevel.trim());
//			  }
//		  }
//		  
//		 if(branchKey!=null)
//		  {
//			  
//			  String branchName=MyUtility.getBranchNameFromKey(branchKey);
//			  if(branchName!=null)
//			  {
//				  if(branchName.equals("")==false)
//					  setBranch(branchName);
//			  }
//		  }
	  }

	  
	public List<String> getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(List<String> ipAddress) {
		ArrayList<String> allIPAddress = new ArrayList<String>();
		allIPAddress.addAll(ipAddress);
		this.ipAddress = allIPAddress;
	}
	
	  

	/**
	 * This method checks you entered email id is already exist or not
	 * Date :17-09-2016 By Anil
	 * Release 30 Sept 2016
	 */
	
	public boolean isDuplicateEmail(SuperModel model)
	{
		User entity = (User) model;
		String name = entity.getEmail().trim();
		String curname=this.email.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getLicenseEndDate() {
		return licenseEndDate;
	}

	public void setLicenseEndDate(Date licenseEndDate) {
		this.licenseEndDate = licenseEndDate;
	}
	/**
	 * Updated By: Viraj
	 * Date: 22-06-2019
	 * Description: To store company Type
	 */
	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public boolean isHrStatus() {
		return hrStatus;
	}

	public void setHrStatus(boolean hrStatus) {
		this.hrStatus = hrStatus;
	}

	public String getLoginIPAddress() {
		return loginIPAddress;
	}

	public void setLoginIPAddress(String loginIPAddress) {
		this.loginIPAddress = loginIPAddress;
	}
	
	public Integer getEmpCount() {
		return empCount;
	}

	public void setEmpCount(Integer empCount) {
		this.empCount = empCount;
	}
	/** Ends **/

	public String getCallerId() {
		return callerId;
	}

	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}
	
	
}
