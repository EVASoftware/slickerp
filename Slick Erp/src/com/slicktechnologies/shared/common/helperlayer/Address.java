package com.slicktechnologies.shared.common.helperlayer;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.slicktechnologies.client.utils.Console;

// TODO: Auto-generated Javadoc
/**
 * Represents address.(in future it will ger replaced by GAE's address datatype)
 */
@Embed
public class Address  implements  Serializable
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3873203176522693343L;
	
	/** Line one of Adress. */
	protected String addressLine1;
	
	/** Line two of Adress */
	protected String addressLine2;
	
	/** The locality */
	@Index
	protected String locality;
	
	/** The landmark. */
	protected String landmark;
	
	/** The state. */
	protected String state;
	
	/** The city. */
	protected String city;
	
	/** The pin. */
	protected long pin;
	
	/** The country. */
	protected String country;
	protected String latitude;
	protected String longitude;

	/**********************************************Default ctor*******************************************************/
	/**
	 * Instantiates a new address entity.
	 */
	public Address() {
		super();
	    addressLine1="";
		addressLine2="";
		locality="";
		landmark="";
		state="";
		city="";
		country="";
		latitude = "";
		longitude = "";
	}
	
	/*************************************Getter/Setter****************************************************************/
	
	/**
	 * Gets the addr line1.
	 *
	 * @return the addr line1
	 */
	public String getAddrLine1() {
		return addressLine1;
	}


	/**
	 * Sets the addr line1.
	 *
	 * @param addrLine1 the new addr line1
	 */
	public void setAddrLine1(String addrLine1) {
		if(addrLine1!=null)
			this.addressLine1 = addrLine1.trim();
	}


	/**
	 * Gets the addr line2.
	 *
	 * @return the addr line2
	 */
	public String getAddrLine2() {
		
		return addressLine2;
	}


	/**
	 * Sets the addr line2.
	 *
	 * @param addrLine2 the new addr line2
	 */
	public void setAddrLine2(String addrLine2) {
		if(addrLine2!=null)
			this.addressLine2 = addrLine2.trim();
	}


	/**
	 * Gets the locality.
	 *
	 * @return the locality
	 */
	public String getLocality() {
		return locality;
	}


	/**
	 * Sets the locality.
	 *
	 * @param locality the new locality
	 */
	public void setLocality(String locality) {
		if(locality!=null)
			this.locality = locality.trim();
	}


	/**
	 * Gets the landmark.
	 *
	 * @return the landmark
	 */
	public String getLandmark() {
		return landmark;
	}


	/**
	 * Sets the landmark.
	 *
	 * @param landmark the new landmark
	 */
	public void setLandmark(String landmark) {
		if(landmark!=null)
		   this.landmark = landmark.trim();
	}


	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}


	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		if(state!=null)
		  this.state = state.trim();
	}


	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		if(city!=null)
		  this.city = city.trim();
	}


	/**
	 * Gets the pin.
	 *
	 * @return the pin
	 */
	public long getPin() {
		return pin;
	}


	/**
	 * Sets the pin.
	 *
	 * @param pin the new pin
	 */
	public void setPin(long pin) {
		this.pin = pin;
	}


	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}


	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		if(country!=null)
		  this.country = country.trim();
	}

	public String getCompleteAddress()
	{
		/**
		 * Date 28/12/2017
		 * By Jayshree
		 * To Check The null condition
		 * @param pin
		 */
		String completeAddress="";
		String addline1="";
		String addline2="";
		String landmark="";
		String Locality="";
		Logger logger = Logger.getLogger("NameOfYourLogger");
		if(getAddrLine1()!=null&&!getAddrLine1().equals("")){
//			addline1=getAddrLine1();
			completeAddress+=getAddrLine1();
			
		}
		if(getAddrLine2()!=null&&!getAddrLine2().equals("")){
			if(completeAddress.equals(""))
				completeAddress+=getAddrLine2();
			else
				completeAddress+=", "+getAddrLine2();
//			addline2=", "+getAddrLine2();
			
		}
		if(getLandmark()!=null&&!getLandmark().equals("")){
//			landmark=", "+getLandmark();
			if(completeAddress.equals(""))
				completeAddress+=getLandmark();
			else
				completeAddress+=", "+getLandmark();
			
		}
		if(getLocality()!=null&&!getLocality().equals("")&&!getLocality().contains("--SELECT--")){
//			Locality=", "+getLocality();
			if(completeAddress.equals(""))
				completeAddress+=getLocality();
			else
				completeAddress+=", "+getLocality();
		}
		
		/**
		 * @author Anil
		 * @since 20-01-2022
		 * As city is made non mandatory for thai client i.e. Innovative. so if it blank do not print comma
		 */
		String city="";
		if(getCity()!=null&&!getCity().equals("")&&!getCity().contains("--SELECT--")){
//			city=", "+getCity(); //Ashwini Patil to avoid printing commas if any field in address is missing
			if(completeAddress.equals(""))
				completeAddress+=getCity();
			else
				completeAddress+=", "+getCity();
			
		}
		
		if(getPin()!=0){
//			completeAddress=addline1+addline2+landmark+Locality+city+getState()+", "+getCountry()+", "+"  Pin:"+getPin();
			//Date: 17-02-2022 By:Ashwini Patil to avoid printing commas if any field in address is missing
//			completeAddress=addline1+addline2+landmark+Locality+city;
			if(getState()!=null&&!getState().equals("")&&!getState().contains("--SELECT--")) {
				if(completeAddress.equals(""))
					completeAddress+=getState();
				else
					completeAddress+=", "+getState();
				
			}
			if(getCountry()!=null&&!getCountry().equals("")&&!getCountry().contains("--SELECT--")) {
				if(completeAddress.equals(""))
					completeAddress+=getCountry();
				else
					completeAddress+=", "+getCountry();
				
			}
			String pn=getPin()+"";
			if(pn!=null&&!pn.equals("")&&!pn.equals("0"))
				completeAddress+=", "+"  Pin:"+getPin();
			
		}
		else{
//			completeAddress=addline1+addline2+landmark+Locality+city+getState()+", "+getCountry();
			//Date: 17-02-2022 By:Ashwini Patil to avoid printing commas if any field in address is missing
//			completeAddress=addline1+addline2+landmark+Locality+city;
			if(getState()!=null&&!getState().equals("")&&!getState().contains("--SELECT--")) {
				if(completeAddress.equals(""))
					completeAddress+=getState();
				else
					completeAddress+=", "+getState();
				
			}
			if(getCountry()!=null&&!getCountry().equals("")&&!getCountry().contains("--SELECT--")) {
				if(completeAddress.equals(""))
					completeAddress+=getCountry();
				else
					completeAddress+=", "+getCountry();
				
			}
				
		}
		return completeAddress;
	}
	/**************************************************************************************************************************/
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}






