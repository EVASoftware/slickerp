package com.slicktechnologies.shared.common.paymentmodelayer.companypayment;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Entity
public class CompanyPayment extends SuperModel {

	private static final long serialVersionUID = 5672059752473487978L;

	@Index
	protected String paymentComName;
	@Index
	protected String paymentBankName;
	@Index
	protected String paymentBranch;
	@Index
	protected String paymentPayableAt;
	@Index
	protected String paymentFavouring;
	@Index
	protected String paymentAccountNo;
	@Index
	protected String paymentIFSCcode;
	@Index
	protected String paymentMICRcode;
	@Index
	protected String paymentSWIFTcode;
	/*added by sheetal:30-11-2021 for IBAN No*/
	protected String paymentIBANNo;
	
	protected String paymentInstruction;
	protected Address addressInfo;
	@Index
	protected boolean paymentStatus;
	@Index
	protected boolean paymentPrint;
	@Index
	protected boolean paymentDefault;
	
	protected DocumentUpload QrCodeDocument;
	
	@Index
	protected int GooglePhonePayNo;
	
	protected ArrayList<ArticleType> articleTypeDetails;
	
	public CompanyPayment() {
		super();
		addressInfo=new Address();
		paymentComName="";
		paymentBankName="";
		paymentBranch="";
		paymentPayableAt="";
		paymentFavouring="";
		paymentAccountNo="";
		paymentIFSCcode="";
		paymentMICRcode="";
		paymentSWIFTcode="";
		paymentIBANNo="";
		paymentInstruction="";
		QrCodeDocument = new DocumentUpload();
		articleTypeDetails=new ArrayList<ArticleType>();
	}
	
	public String getPaymentComName() {
		return paymentComName;
	}


	public void setPaymentComName(String paymentComName) {
		this.paymentComName = paymentComName;
	}
	public Boolean isPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Boolean isPaymentPrint() {
		return paymentPrint;
	}

	public void setPaymentPrint(boolean paymentPrint) {
		this.paymentPrint = paymentPrint;
	}

	public Boolean isPaymentDefault() {
		return paymentDefault;
	}

	public void setPaymentDefault(boolean paymentDefault) {
		this.paymentDefault = paymentDefault;
	}

	public String getPaymentBankName() {
		return paymentBankName;
	}

	public void setPaymentBankName(String paymentBankName) {
		this.paymentBankName = paymentBankName;
	}

	public String getPaymentBranch() {
		return paymentBranch;
	}

	public void setPaymentBranch(String paymentBranch) {
		this.paymentBranch = paymentBranch;
	}

	public String getPaymentPayableAt() {
		return paymentPayableAt;
	}

	public void setPaymentPayableAt(String paymentPayableAt) {
		this.paymentPayableAt = paymentPayableAt;
	}

	public String getPaymentFavouring() {
		return paymentFavouring;
	}

	public void setPaymentFavouring(String paymentFavouring) {
		this.paymentFavouring = paymentFavouring;
	}

	public String getPaymentIFSCcode() {
		return paymentIFSCcode;
	}

	public void setPaymentIFSCcode(String paymentIFSCcode) {
		this.paymentIFSCcode = paymentIFSCcode;
	}

	public String getPaymentMICRcode() {
		return paymentMICRcode;
	}

	public void setPaymentMICRcode(String paymentMICRcode) {
		this.paymentMICRcode = paymentMICRcode;
	}

	public String getPaymentSWIFTcode() {
		return paymentSWIFTcode;
	}

	public void setPaymentSWIFTcode(String paymentSWIFTcode) {
		this.paymentSWIFTcode = paymentSWIFTcode;
	}

	public String getPaymentIBANNo() {
		return paymentIBANNo;
	}

	public void setPaymentIBANNo(String paymentIBANNo) {
		this.paymentIBANNo = paymentIBANNo;
	}
	
	
	public String getPaymentInstruction() {
		return paymentInstruction;
	}

	public void setPaymentInstruction(String paymentInstruction) {
		this.paymentInstruction = paymentInstruction;
	}

	public Address getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(Address addressInfo) {
		this.addressInfo = addressInfo;
	}

	public String getPaymentAccountNo() {
		return paymentAccountNo;
	}

	public void setPaymentAccountNo(String paymentAccountNo) {
		this.paymentAccountNo = paymentAccountNo;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * nidhi
	 * 12/4/2018
	 */
	@Override
	public String toString(){
		return this.count+" / "+this.paymentBankName;
	}

	public DocumentUpload getQrCodeDocument() {
		return QrCodeDocument;
	}

	public void setQrCodeDocument(DocumentUpload qrCodeDocument) {
		QrCodeDocument = qrCodeDocument;
	}

	public int getGooglePhonePayNo() {
		return GooglePhonePayNo;
	}

	public void setGooglePhonePayNo(int googlePhonePayNo) {
		GooglePhonePayNo = googlePhonePayNo;
	}
	
	public ArrayList<ArticleType> getArticleTypeDetails() {
		return articleTypeDetails;
	}

	public void setArticleTypeDetails(List<ArticleType> articleTypeDetails) {
		ArrayList<ArticleType> arrAricle=new ArrayList<ArticleType>();
		arrAricle.addAll(articleTypeDetails);
		this.articleTypeDetails = arrAricle;
	}

}
