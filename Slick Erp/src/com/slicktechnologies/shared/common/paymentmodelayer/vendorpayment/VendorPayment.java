package com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

@Entity
public class VendorPayment extends SuperModel {

	private static final long serialVersionUID = 5672059752473487978L;

	
	@Index
	protected String vendorPaymentBankName;
	@Index
	protected String vendorPaymentBranch;
	@Index
	protected String vendorPaymentPayableAt;
	@Index
	protected String vendorPaymentFavouring;
	@Index
	protected String vendorPaymentAccountNo;
	@Index
	protected String vendorPaymentIFSCcode;
	@Index
	protected String vendorPaymentMICRcode;
	@Index
	protected String vendorPaymentSWIFTcode;
	@Index
	protected String vendorPaymentInstruction;
	@Index
	protected Address addressInfo;
	@Index
	protected boolean vendorPaymentStatus;
	@Index
	protected boolean vendorPaymentPrint;
	@Index
	protected boolean vendorPaymentDefault;
	@Index
	protected PersonInfo personVendorInfo;
	
	
	public VendorPayment() {
		addressInfo=new Address();
		personVendorInfo=new PersonInfo();
	}
	
	
	public PersonInfo getPersonVendorInfo() {
		return personVendorInfo;
	}

	public void setPersonVendorInfo(PersonInfo personVendorInfo) {
		if (personVendorInfo != null)
			this.personVendorInfo = personVendorInfo;
	}
	
	
	public boolean isVendorPaymentStatus() {
		return vendorPaymentStatus;
	}




	public void setVendorPaymentStatus(boolean vendorPaymentStatus) {
		this.vendorPaymentStatus = vendorPaymentStatus;
	}




	public boolean isVendorPaymentPrint() {
		return vendorPaymentPrint;
	}




	public void setVendorPaymentPrint(boolean vendorPaymentPrint) {
		this.vendorPaymentPrint = vendorPaymentPrint;
	}




	public boolean isVendorPaymentDefault() {
		return vendorPaymentDefault;
	}




	public void setVendorPaymentDefault(boolean vendorPaymentDefault) {
		this.vendorPaymentDefault = vendorPaymentDefault;
	}


	public String getVendorPaymentBankName() {
		return vendorPaymentBankName;
	}




	public void setVendorPaymentBankName(String vendorPaymentBankName) {
		this.vendorPaymentBankName = vendorPaymentBankName;
	}




	public String getVendorPaymentBranch() {
		return vendorPaymentBranch;
	}




	public void setVendorPaymentBranch(String vendorPaymentBranch) {
		this.vendorPaymentBranch = vendorPaymentBranch;
	}




	public String getVendorPaymentPayableAt() {
		return vendorPaymentPayableAt;
	}




	public void setVendorPaymentPayableAt(String vendorPaymentPayableAt) {
		this.vendorPaymentPayableAt = vendorPaymentPayableAt;
	}




	public String getVendorPaymentFavouring() {
		return vendorPaymentFavouring;
	}




	public void setVendorPaymentFavouring(String vendorPaymentFavouring) {
		this.vendorPaymentFavouring = vendorPaymentFavouring;
	}




	public String getVendorPaymentIFSCcode() {
		return vendorPaymentIFSCcode;
	}




	public void setVendorPaymentIFSCcode(String vendorPaymentIFSCcode) {
		this.vendorPaymentIFSCcode = vendorPaymentIFSCcode;
	}




	public String getVendorPaymentMICRcode() {
		return vendorPaymentMICRcode;
	}




	public void setVendorPaymentMICRcode(String vendorPaymentMICRcode) {
		this.vendorPaymentMICRcode = vendorPaymentMICRcode;
	}




	public String getVendorPaymentSWIFTcode() {
		return vendorPaymentSWIFTcode;
	}




	public void setVendorPaymentSWIFTcode(String vendorPaymentSWIFTcode) {
		this.vendorPaymentSWIFTcode = vendorPaymentSWIFTcode;
	}




	public String getVendorPaymentInstruction() {
		return vendorPaymentInstruction;
	}




	public void setVendorPaymentInstruction(String vendorPaymentInstruction) {
		this.vendorPaymentInstruction = vendorPaymentInstruction;
	}





	public Address getAddressInfo() {
		return addressInfo;
	}




	public void setAddressInfo(Address addressInfo) {
		this.addressInfo = addressInfo;
	}




	public String getVendorPaymentAccountNo() {
		return vendorPaymentAccountNo;
	}




	public void setVendorPaymentAccountNo(String vendorPaymentAccountNo) {
		this.vendorPaymentAccountNo = vendorPaymentAccountNo;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
