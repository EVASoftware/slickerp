package com.slicktechnologies.shared.common.businessprocesslayer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.relationalLayer.VendorRelation;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * The Class Expense. Represents the expenditure.
 */
@Entity
public class Expense extends ConcreteBusinessProcess
{
	/***********************************************Entity Attributes****************************************************/
	
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4405594841510984785L;

	/** The invoice number. */
	@Index 
	protected String invoiceNumber;

	/** The description. */
	protected String description;

	/** The amount. */
	protected double amount; 

	/** the vendor is not mandatory, it will be used whenever expense will be related
	 *  to any {@link Vendor} */
	@Index
	protected String vendor;// To do make vendor connected with db;

	/** The currency. */
	protected  String currency;

	/** The payments can be done in multiple times and methods */
	protected List<Payment> payments;
	
	/** Date of Expense **/
	@Index
	protected Date expenseDate;
	
	/** Payment Method for this expense **/
	protected String paymentMethod;

	/** any document related to expense **/
	protected DocumentUpload expenseDocument;
	
	@Index
	protected String pettyCashName;
	@Index
	protected boolean expTypeAsPettyCash;
	
	protected String account;
	
	@Index
	protected int referenceNumber;
	
	@Index
	protected int serviceId;

//	protected ArrayList<ExpenseManagement> expenseList;
	
	/***********************************************Relational Attributes****************************************************/
	
	protected Key<VendorRelation> vendorKey;
	protected Key<Config>KeypaymentMethod;


	/**
	 * Instantiates a new expense.
	 */
	public Expense()
	{
//		expenseList = new ArrayList<ExpenseManagement>();
		payments=new ArrayList<Payment>();
		expenseDocument=new DocumentUpload();
		invoiceNumber="";
		description="";
		vendor="";
		category="";
		currency="";
		paymentMethod="";
		pettyCashName="";
		referenceNumber=0;
		serviceId=0;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "13", title = "Invoice Number", variableName = "tbInvoiceNumber")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 9, isFieldUpdater = false, isSortable = true, title = "Invoice Number")
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) 
	{
		if(invoiceNumber!=null)
			this.invoiceNumber = invoiceNumber.trim();
	}

	public String getDescription() {
		return description;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setDescription(String description) 
	{
		if(description!=null)
			this.description = description.trim();
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 10, isFieldUpdater = false, isSortable = true, title = "Amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Vendor")
	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		if(vendor!=null)
			this.vendor = vendor.trim();
	}

	public Key<VendorRelation> getVendorKey() {
		return vendorKey;
	}
	

//	public ArrayList<ExpenseManagement> getExpenseList() {
//		return expenseList;
//	}
//
//	public void setExpenseList(ArrayList<ExpenseManagement> expenseList) {
//		
//		ArrayList<ExpenseManagement> assessmentDetails = new ArrayList<ExpenseManagement>();
//		assessmentDetails.addAll(expenseList);
//		this.expenseList = assessmentDetails;
//	}

	public int getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(int referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public void setVendorKey(Key<VendorRelation> vendorKey) {
		this.vendorKey = vendorKey;
	}
	@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXCONFIG, flexFormNumber = "12", title = "Category", variableName = "olbConfigCategory",extra="EXPENSECATEGORY")
	//@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Category")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		if(category!=null)
			this.category = category.trim();
	}

	public Key<ConfigCategory> getCategoryKey() {
		return categoryKey;
	}

	public void setCategoryKey(Key<ConfigCategory> categoryKey) {
		this.categoryKey = categoryKey;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		if(currency!=null)
			this.currency = currency.trim();
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.DATEBOX, flexFormNumber = "01", title = " Date", variableName = "dateBoxExpenseDate")
	@TableAnnotation(ColType = FormTypes.DATETEXTCELL, colNo = 11, isFieldUpdater = false, isSortable = true, title = "Expense Date")
	public Date getExpenseDate() {
		return expenseDate;
	}

	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		if(paymentMethod!=null)
			this.paymentMethod = paymentMethod.trim();
	}

	public DocumentUpload getExpenseDocument() 
	{
		return expenseDocument;
	}

	public void setExpenseDocument(DocumentUpload expenseDocument) {
		this.expenseDocument = expenseDocument;
	}
	
	public String getPettyCashName() {
		return pettyCashName;
	}

	public void setPettyCashName(String pettyCashName) {
		if(pettyCashName!=null)
		this.pettyCashName = pettyCashName.trim();
	}

	public Boolean getExpTypeAsPettyCash() {
		return expTypeAsPettyCash;
	}

	public void setExpTypeAsPettyCash(boolean expTypeAsPettyCash) {
		this.expTypeAsPettyCash = expTypeAsPettyCash;
	}

	@Override
	//@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXCONFIG, flexFormNumber = "01", title = "status", variableName = "olbConfigstatus",extra="EXPENSESTATUS")
	public String getStatus() {
		// TODO Auto-generated method stub
		return super.getStatus();
	}

	@Override
	public void setStatus(String status) {
		// TODO Auto-generated method stub
		super.setStatus(status);
	}
	
	

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void onSave()
	{
		
			if(expenseDate!=null){
			   expenseDate= DateUtility.getDateWithTimeZone("IST", this.getExpenseDate());
			}
			categoryKey=MyUtility.getConfigCategoryFromCondition(getCategory());
			typeKey=MyUtility.getTypeKeyFromCondition(getType());
			KeypaymentMethod=MyUtility.getConfigKeyFromCondition(paymentMethod,ConfigTypes.PAYMENTMETHODS.getValue());
			MyQuerry querry=new MyQuerry();
			Filter vendorNameFilter=new Filter();
			vendorNameFilter.setQuerryString("vendorName");
			vendorNameFilter.setStringValue(this.vendor);
			querry.getFilters().add(vendorNameFilter);
			querry.setQuerryObject(new Vendor());
			vendorKey=(Key<VendorRelation>) MyUtility.getRelationalKeyFromCondition(querry);
		
	}
	
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{

		if(categoryKey!=null)
		{
			String categoryName=MyUtility.getConfigCatNameFromKey(categoryKey);
			if(categoryName.equals("")==false)
				setCategory(categoryName);
		}

		if(typeKey!=null)
		{
			String type=MyUtility.getTypeNameFromKey(typeKey);
			if(type.equals("")==false)
				setType(type);
		}

		if(vendorKey!=null)
		{
			String vendorName=MyUtility.getVendorNameFromKey(vendorKey);
			if(vendorName.equals("")==false)	
				setVendor(vendorName);
		}

		if(KeypaymentMethod!=null)
		{
			String paymentMethod=MyUtility.getConfigNameFromKey(KeypaymentMethod);
			if(paymentMethod.equals("")==false)
				setPaymentMethod(paymentMethod);
		}
	}
	
	
	public static List<String> getStatusList()
	{
		ArrayList<String> custstatuslist=new ArrayList<String>();
		custstatuslist.add(	CREATED);
		custstatuslist.add(REQUESTED);
		custstatuslist.add(REJECTED);
		custstatuslist.add(APPROVED);
		/**
		 * @author Anil
		 * @Since 05-01-2020
		 * Added cancelled status for searching cancelled expenses
		 */
		custstatuslist.add(CANCELLED);
		return custstatuslist;
	}

	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		deductPettyCashBalance();
		

		System.out.println("Querying Processname");
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGEXP).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag =0;
		
		if(processConfig!=null){
			System.out.println("Process Config Not Null");
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				System.out.println("----------------------------------------------------------");
				System.out.println("One===== "+processConfig.getProcessList().get(i).getProcessType());
				System.out.println("One===== "+processConfig.getProcessList().get(i).isStatus());
				System.out.println("----------------------------------------------------------");
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			accountingInterface();
		}
	}
	
	@GwtIncompatible
    protected void deductPettyCashBalance()
    {
		if(getPettyCashName()!=null)
		{
			PettyCash petty=ofy().load().type(PettyCash.class).filter("pettyCashName",getPettyCashName()).first().now();
			if(petty!=null)
			{
				double expAmt=this.getAmount();
//				int expAmount=(int) expAmt;
				double mainBalance=petty.getPettyCashBalance();
				
				// vijay has taken for petty cash tran list( ballance)
				
				double balance = petty.getPettyCashBalance();
				double deductAmount=mainBalance-expAmt;
				petty.setPettyCashBalance(deductAmount);
				ofy().save().entities(petty).now();
				
				/*********************** vijay code for petty cash transaction list *************/
				  SaveExpenseInTransactionList(balance,expAmt);
			}
		}
    }
	
	
	@GwtIncompatible
	private void SaveExpenseInTransactionList(double mainBalance, double expAmt) {
		// TODO Auto-generated method stub
		
		/**************************** vijay for transaction list *******************/
		System.out.println("hi vijay ====");
		System.out.println("main balance = "+mainBalance);
		System.out.println("expnse amt == "+expAmt);
		PettyCashTransaction pettycash = new PettyCashTransaction();
		pettycash.setOpeningStock(mainBalance);
		pettycash.setTransactionAmount(expAmt);
		pettycash.setClosingStock(mainBalance-expAmt );
		pettycash.setDocumentId(this.getCount());
		pettycash.setDocumentName("Expense Management");
		Date saveDate = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy h:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String datestring =  sdf.format(saveDate);
		Logger logger = Logger.getLogger("NameOfYourLogger");
		logger.log(Level.SEVERE," Date $$$$$$  === with IST"+datestring);
		String[] dateString=datestring.split(" ");
		String time=dateString[1];
		logger.log(Level.SEVERE," Date =="+dateString[0]);
		logger.log(Level.SEVERE," Date time =="+dateString[1]);
		pettycash.setTransactionTime(time);
		pettycash.setTransactionAmount(expAmt);
		try {
			pettycash.setTransactionDate(sdf.parse(datestring));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE," date parse exception  =="+e);
		}
		pettycash.setAppoverName(this.getApproverName());
		pettycash.setPersonResponsible(this.getEmployee());
		pettycash.setAction("-");
		pettycash.setDocumentTitle(this.getPettyCashName());
		pettycash.setCompanyId(this.getCompanyId());
		
		pettycash.setPettyCashName(this.getPettyCashName());
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(pettycash);
		System.out.println("petty cash transaction list saved successfully");
		/************************* vijay *******************************/
	}
	
	
	@GwtIncompatible
	protected void accountingInterface(){
		
		
		if(this.getCount()!=0){
			
					 
					 UpdateAccountingInterface.updateTally(new Date(),   //accountingInterfaceCreationDate
							 								this.getEmployee(),  //accountingInterfaceCreatedBy
							 								this.getStatus(),
							 								AppConstants.STATUS , //Status
							 								"",  //Remark
							 								"Accounts",    //String Module,
							 			 				
							 			 				
							 								"Expense Management",  //	String documentType,
							 								this.getCount(), //	int documentID,
							 								"Expense", 		 //documentTitle
							 								this.getExpenseDate(), //documentDate
							 								"GL-Expense",     		 //doucmentGL
							 								"",            		//referenceDocumentNumber1
							 								null,            	  //referenceDocumentDate1
							 								"Expense",       //referenceDocumentType
							 								"",            //referenceDocumentNumber2
							 								null,		 //referencedocumentDate2
							 								"",            //referencedocumentType2
							 								AppConstants.ACCOUNTTYPE,     //accountType
							 								0,       			//customerID
							 								"", 			 //customerName
							 								0, 				//customerCell
							 								0,      		 //vendorID
							 								"",    		//vendorName
							 								0,         		//vendorCell
							 								0, 			//employeeID
							 								this.getEmployee(),           //employeeName
							 								this.getBranch(),           //Branch
							 								"",             		 //personResponsible
							 								"",           			 //requestedBy
							 								this.getApproverName(),        //approvedBy
							 								this.getPaymentMethod(),        //paymentMethod
							 								null, 						//paymentDate
							 								"",           			   //chequeNumber
							 								null,        			 //chequeDate
							 								"",             			 //bankName
							 								null,                			//bankAccount
							 								0,              			 //transferReferenceNumber
							 								null,   					//transactionDate
							 								null,	//contract start date
							 								null,  // contract end date
							 								0,        					//productID
							 								"",          // productCode
							 								"",         //productName
							 								0,          //Quantity
							 								null,       //productDate
							 								0,        //duration
							 								0,         //services
							 								"",        //unitOfMeasurement
							 								0.0,         //productPrice
							 								0,             //VATpercent
							 								0,             //VATamount
							 								0,              //VATglAccount
							 								0,             //serviceTaxPercent
							 								0,              //serviceTaxAmount
							 								0,             //serviceTaxGLaccount
							 								"",              //cForm
							 								0,             //cFormPercent
							 								0,         //cFormAmount
							 								0,			//cFormGlAccountNO.
							 								0,          //totalAmount
							 								this.getAmount(),			//netPayable
							 								"",//Contract Category
							 								0,			//amountRecieved
							 								0.0,  // base Amt for tdscal in pay   by rohan 
							 								0,    //  tds percentage by rohan 
							 								0,     //  tds amount by rohan 
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								"",
							 								"",
							 								"",
							 								"",
							 								"",
							 								0,
							 								this.getCompanyId(),
							 								null,				//  billing from date (rohan)
							 								null,			//  billing to date (rohan)
							 								"", //Warehouse
															"",				//warehouseCode
															"",				//ProductRefId
															"",				//Direction
															"",				//sourceSystem
															"",				//Destination System
															null,null,null
								);
		}
	}

	
	
	
	
	
	

}
