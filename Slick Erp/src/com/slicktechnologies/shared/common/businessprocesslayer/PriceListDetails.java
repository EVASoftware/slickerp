package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class PriceListDetails.
 */
@Embed
public class PriceListDetails extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2886088891059022352L;

	

	
	
	/** The info. */
	
	ProductInfo info;
	
	@Index
	/** The personinfo. */
	PersonInfo personinfo;
	
	/** The fromdate. */
	protected Date fromdate;
	
	/** The todate. */
	protected Date todate;
	
	//protected String unitOfmeasuerment;
/********************************************Constructor*******************************/
	
	
	public PriceListDetails()
	{
		personinfo=new PersonInfo();
		info=new ProductInfo();
	}
	
	

	
	
/**************************************Getters and Setters***************************/	
	
	
	
	
	/**
	 * Gets the cell number.
	 *
	 * @return the cell number
	 */
	public Long getCellNumber() {
		return personinfo.getCellNumber();
	}




	public String getUnitofMeasure() {
		return info.getUnitofMeasure();
	}





	public void setUnitofMeasure(String unitofMeasure) {
		info.setUnitofMeasure(unitofMeasure);
	}



/*

	public String getUnitOfmeasuerment() {
		return unitOfmeasuerment;
	}





	public void setUnitOfmeasuerment(String unitOfmeasuerment) {
		this.unitOfmeasuerment = unitOfmeasuerment;
	}
*/




	/**
	 * Sets the cell number.
	 *
	 * @param cellNumber the new cell number
	 */
	public void setCellNumber(Long cellNumber) {
		personinfo.setCellNumber(cellNumber);
	}




	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return personinfo.getFullName();
	}




	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		personinfo.setFullName(fullName);
	}




	
	public int getVendorID() {
		return personinfo.getCount();
	}




	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#setCount(int)
	 */
	public void setCount(int id) {
		personinfo.setCount(id);
	}




	public String getEmail() {
		return personinfo.getEmail();
	}





	public void setEmail(String email) {
		personinfo.setEmail(email);
	}




	// added poc getter n setter by vijay

	public void setPocName(String pocname){
		personinfo.setPocName(pocname);
	}
	
	
	public String getPocName(){
		return personinfo.getPocName();
	}


	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public int getProdID() {
		return info.getProdID();
	}




	/**
	 * Sets the prod id.
	 *
	 * @param prodID the new prod id
	 */
	public void setProdID(int prodID) {
		info.setProdID(prodID);
	}




	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return info.getProductCode();
	}




	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		info.setProductCode(productCode);
	}




	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return info.getProductName();
	}




	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		info.setProductName(productName);
	}




	/**
	 * Gets the product category.
	 *
	 * @return the product category
	 */
	public String getProductCategory() {
		return info.getProductCategory();
	}




	/**
	 * Sets the product category.
	 *
	 * @param productCategory the new product category
	 */
	public void setProductCategory(String productCategory) {
		info.setProductCategory(productCategory);
	}




	/**
	 * Gets the product price.
	 *
	 * @return the product price
	 */
	public double getProductPrice() {
		return info.getProductPrice();
	}




	/**
	 * Sets the product price.
	 *
	 * @param productPrice the new product price
	 */
	public void setProductPrice(double productPrice) {
		info.setProductPrice(productPrice);
	}
	
	
	/**
	 * Gets the fromdate.
	 *
	 * @return the fromdate
	 */
	public Date getFromdate() {
		return fromdate;
	}
	
	/**
	 * Sets the fromdate.
	 *
	 * @param fromdate the new fromdate
	 */
	public void setFromdate(Date fromdate) {
		this.fromdate = fromdate;
	}
	
	/**
	 * Gets the todate.
	 *
	 * @return the todate
	 */
	public Date getTodate() {
		return todate;
	}
	
	/**
	 * Sets the todate.
	 *
	 * @param todate the new todate
	 */
	public void setTodate(Date todate) {
		this.todate = todate;
	}
	
	
	




	public PersonInfo getPersoninfo() {
		return personinfo;
	}





	public void setPersoninfo(PersonInfo personinfo) {
		this.personinfo = personinfo;
	}





	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
	}
