package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

/**
 * The Class Activity Represents an Activity corresponding to any {@link BusinessProcess}. 
 */
@Entity
public class Activity extends ConcreteBusinessProcess 
{
	/***********************************************Entity Attributes***********************************/
	private static final long serialVersionUID = -8929451655874530574L;

	/** The person who raised this activity {@link PersonInfo}*/
	@Index 
	protected PersonInfo person;
	
	/** count/Id of business process against which activity is initiated*/
	@Index 
	protected int businessProcessId;

	/** The activity description. */
	public String activityDescription;
    
    /** Actions to be taken against the activity */
    public ArrayList<Action> actions;
    
    /** Identifier for Business Process **/
   @Index 
   public int buisnessProcessType;
   
   /** priority for this activity **/
   public String activityPriority;
   
   public static int LEAD=1;
   public static int QUOTATION=1;
   public static int CONTRACT=1;
   public static int SERVICE=1;
   /*******************************************Status Constants**********************************/
   public static final String CREATED="Created";
   public static final String CLOSED="Closed";
   public static final String DISCARDED="Discarded";
   public static final String ACTIVE="Active";
   
   /***********************************************Relational Attributes**************************/  
  
	/**
	 * Instantiates a new action.
	 */
	public Activity() 
	{
		super();
		person=new PersonInfo();
		actions=new ArrayList<Action>();
		
		activityDescription="";
	}

	public PersonInfo getPerson() 
	{
		return person;
	}

	public void setPerson(PersonInfo person)
	{
		this.person = person;
	}

	public Integer getActivityAgainstId()
	{
		return businessProcessId;
	}

	public void setActivityAgainstType(int activityAgainstType) 
	{
		this.businessProcessId = activityAgainstType;
	}

	public String getActivityDescription()
	{
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) 
	{
		if(activityDescription!=null)
			this.activityDescription = activityDescription.trim();
	}

	public List<Action> getActivites()
	{
		return actions;
	}

	public void setActivites(List<Action> activites)
	{
		this.actions.addAll(activites) ;
	}

	public Integer getBuisnessProcesstype() 
	{
		return buisnessProcessType;
	}

	public void setBuisnessProcesstype(int buisnessProcesstype) 
	{
		this.buisnessProcessType = buisnessProcesstype;
	}
	
	
	public String getActivityPriority() {
		return activityPriority;
	}

	public void setActivityPriority(String activityPriority) {
		this.activityPriority = activityPriority;
	}
	
	/************************************Status List********************************************************/
	public static List<String> getStatusList()
	{
		ArrayList<String> custstatuslist=new ArrayList<String>();
		custstatuslist.add(CREATED);
		custstatuslist.add(ACTIVE);
		custstatuslist.add(CLOSED);
		custstatuslist.add(DISCARDED);
		return custstatuslist;
	}

	/**************************************************Relation Managment Part**********************************************/
	  @GwtIncompatible
	  @OnSave
	  public void OnSave()
	  {
		  super.OnSave();
		//if(id==null)
		{
			typeKey=MyUtility.getTypeKeyFromCondition(getType());
			groupKey=MyUtility.getConfigKeyFromCondition(group, ConfigTypes.COMPLAINGROUP.getValue());
			categoryKey=MyUtility.getConfigCategoryFromCondition(getCategory());
		}
	  }
	  @GwtIncompatible
	  @OnLoad
	  public void OnLoad()
	  {
		  super.OnLoad();
		  if(getTypeKey()!=null)
		  {
		  	String typeName=MyUtility.getTypeNameFromKey(getTypeKey());
		  	if(typeName!=null)
		  	{
		  		if(typeName.equals("")==false)
		  			setType(typeName);
		  	}
		  }
		  
		  if(getGroupKey()!=null)
		  {
			String groupName=MyUtility.getConfigNameFromKey(getGroupKey());
			if(groupName!=null)
			{
				if(groupName.equals("")==false)
					setGroup(groupName);
			}
		  }
			
		  if(getCategoryKey()!=null)
		  {
			String categoryName=MyUtility.getConfigCatNameFromKey(getCategoryKey());
			if(categoryName!=null)
			{
				if(categoryName.equals("")==false)
				setCategory(categoryName);
			}
		  }
	  }
	
}
