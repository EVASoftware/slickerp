package com.slicktechnologies.shared.common.businessprocesslayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ExpenseReturnHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7594616684159147573L;
	
Date expenseReturnDate;
	
	double expenseReturnAmt;
	
	protected String personResponsible;
	
	public ExpenseReturnHistory(){
		
		expenseReturnDate = null;
		personResponsible="";
		
	}

	public String getPersonResponsible() {
		return personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		this.personResponsible = personResponsible;
	}

	public Date getExpenseReturnDate() {
		return expenseReturnDate;
	}

	public void setExpenseReturnDate(Date expenseReturnDate) {
		this.expenseReturnDate = expenseReturnDate;
	}

	public double getExpenseReturnAmt() {
		return expenseReturnAmt;
	}

	public void setExpenseReturnAmt(double expenseReturnAmt) {
		this.expenseReturnAmt = expenseReturnAmt;
	}
	

}
