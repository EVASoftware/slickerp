package com.slicktechnologies.shared.common.businessprocesslayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class VendorPriceListDetails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3096759085735252901L;
	
	/******************************************Applicability Attributes************************************/
	
	
	@Index
	private int priceListId;
	@Index
	private String priceListTitle;
	@Index
	private int vendorId;
	@Index
	private String vendorName;
	@Index
	private int productId;
	@Index
	private String productName;
	@Index
	private String productCode;
	@Index
	private String productCategory;
	private double productPrice;
	
	/**************************************Constructor****************************************/
	
	public VendorPriceListDetails() {
		super();
		priceListTitle="";
		vendorName="";
		productName="";
		productCode="";
		productCategory="";
	}
	
	/********************************************Getters And Setters**************************************/
	
	
	public int getPriceListId() {
		return priceListId;
	}

	public void setPriceListId(int priceListId) {
		this.priceListId = priceListId;
	}

	public String getPriceListTitle() {
		return priceListTitle;
	}

	public void setPriceListTitle(String priceListTitle) {
		if(priceListTitle!=null){
			this.priceListTitle = priceListTitle.trim();
		}
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		if(vendorName!=null){
			this.vendorName = vendorName.trim();
		}
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		if(productName!=null){
			this.productName = productName.trim();
		}
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		if(productCode!=null){
			this.productCode = productCode.trim();
		}
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		if(productCategory!=null){
			this.productCategory = productCategory.trim();
		}
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
