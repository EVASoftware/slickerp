package com.slicktechnologies.shared.common.businessprocesslayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.PettyCash;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.PettyCashTransaction;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

@Entity
public class MultipleExpenseMngt extends ConcreteBusinessProcess{

	private static final long serialVersionUID = -6477329503507482271L;

	
	
	/** The description. */
	protected String description;

	/** any document related to expense **/
	protected DocumentUpload expenseDocument;
	
	@Index
	protected String pettyCashName;
	@Index
	protected boolean expTypeAsPettyCash;
	
	protected String account;
	
	@Index
	protected int referenceNumber;
	
	@Index
	protected int serviceId;
	
	protected double totalAmount;
	
	/**
	 * Date : 12-07-2017 By ANil
	 * Made list indexed on 12 July 2017 for NBHC
	 */
	@Index
	protected ArrayList<ExpenseManagement> expenseList;
	
	/** Date 07-08-2017 added by vijay 
	 * for return expense history
	 */
	
	ArrayList<ExpenseReturnHistory> returnExpenseHistory;
	
	/**
	 * @author Anil , Date : 26-08-2019
	 * adding empId and empCell
	 */
	@Index
	int empId;
	@Index
	long empCell;
	
	@Index
	String employeeRole;
	
	@Index
	protected int salesOrderId;
	@Index 
	protected PersonInfo personInfo;
	
	public MultipleExpenseMngt()
	{
		expenseList = new ArrayList<ExpenseManagement>();
		expenseDocument=new DocumentUpload();
		description="";
		pettyCashName="";
		referenceNumber=0;
		serviceId=0;
		totalAmount=0;
		
		returnExpenseHistory = new ArrayList<ExpenseReturnHistory>();
		
		personInfo = new PersonInfo();
	}
	
	
	
	public String getEmployeeRole() {
		return employeeRole;
	}



	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}



	public int getEmpId() {
		return empId;
	}



	public void setEmpId(int empId) {
		this.empId = empId;
	}



	public long getEmpCell() {
		return empCell;
	}



	public void setEmpCell(long empCell) {
		this.empCell = empCell;
	}



	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DocumentUpload getExpenseDocument() {
		return expenseDocument;
	}

	public void setExpenseDocument(DocumentUpload expenseDocument) {
		this.expenseDocument = expenseDocument;
	}

	public String getPettyCashName() {
		return pettyCashName;
	}

	public void setPettyCashName(String pettyCashName) {
		this.pettyCashName = pettyCashName;
	}

	public boolean isExpTypeAsPettyCash() {
		return expTypeAsPettyCash;
	}

	public void setExpTypeAsPettyCash(boolean expTypeAsPettyCash) {
		this.expTypeAsPettyCash = expTypeAsPettyCash;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(int referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public ArrayList<ExpenseManagement> getExpenseList() {
		return expenseList;
	}

	
	public void setExpenseList(ArrayList<ExpenseManagement> expenseList) {
		
		ArrayList<ExpenseManagement> assessmentDetails = new ArrayList<ExpenseManagement>();
		assessmentDetails.addAll(expenseList);
		this.expenseList = assessmentDetails;
	}
	
	public ArrayList<ExpenseReturnHistory> getReturnExpenseHistory() {
		return returnExpenseHistory;
	}

	public void setReturnExpenseHistory(
			ArrayList<ExpenseReturnHistory> returnExpenseHistory) {
		this.returnExpenseHistory = returnExpenseHistory;
	}
	
	@Override
	@GwtIncompatible
	public void reactOnApproval() {
		deductPettyCashBalance();
		
		updateServiceCost();
		
		System.out.println("Querying Processname");
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		System.out.println("Process Name Query Executed");
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			System.out.println("NotNull Process Name");
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.MULTIPLEXPMANAGEMENT).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
		}
		System.out.println("Process Config Executed");
		int flag =0;
		
		if(processConfig!=null){
			System.out.println("Process Config Not Null");
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				System.out.println("----------------------------------------------------------");
				System.out.println("One===== "+processConfig.getProcessList().get(i).getProcessType());
				System.out.println("One===== "+processConfig.getProcessList().get(i).isStatus());
				System.out.println("----------------------------------------------------------");
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					System.out.println("Flag One=====*****=====");
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			accountingInterface();

		}
	}
	
	
	@GwtIncompatible
	private void updateServiceCost() {
		
		try {
			if(this.getServiceId()!=0){
				Logger logger = Logger.getLogger("logger");

				Service serviceEntity = ofy().load().type(Service.class).filter("companyId", this.getCompanyId()).filter("count", this.getServiceId()).first().now();
				if(serviceEntity!=null){
					double totalExpensesCost = serviceEntity.getExpenseCost();
					totalExpensesCost += this.getTotalAmount();
					serviceEntity.setExpenseCost(totalExpensesCost);
					logger.log(Level.SEVERE, "totalExpensesCost "+totalExpensesCost);
					ofy().save().entity(serviceEntity);
				}
			}
		} catch (Exception e) {
		}
		
		
	}



	@GwtIncompatible
    protected void deductPettyCashBalance()
    {
		System.out.println("in sidereact on approved");
		if(getPettyCashName()!=null)
		{
			PettyCash petty=ofy().load().type(PettyCash.class).filter("pettyCashName",getPettyCashName()).first().now();
			if(petty!=null)
			{
				
				System.out.println("in sidereact on approved in side petty cash");
				
				double expAmt=this.getTotalAmount();
//				int expAmount=(int) expAmt;
				double mainBalance=petty.getPettyCashBalance();
				
				// vijay has taken for petty cash tran list( ballance)
				
				double balance = petty.getPettyCashBalance();
				double deductAmount=mainBalance-expAmt;
				petty.setPettyCashBalance(deductAmount);
				ofy().save().entities(petty).now();
				
				/*********************** vijay code for petty cash transaction list *************/
				  SaveExpenseInTransactionList(balance,expAmt);
			}
		}
    }
	
	
	@GwtIncompatible
	private void SaveExpenseInTransactionList(double mainBalance, double expAmt) {
		// TODO Auto-generated method stub
		
		/**************************** vijay for transaction list *******************/
		System.out.println("hi vijay ====");
		System.out.println("main balance = "+mainBalance);
		System.out.println("expnse amt == "+expAmt);
		PettyCashTransaction pettycash = new PettyCashTransaction();
		pettycash.setOpeningStock(mainBalance);
		pettycash.setTransactionAmount(expAmt);
		pettycash.setClosingStock(mainBalance-expAmt);
		pettycash.setDocumentId(this.getCount());
		pettycash.setDocumentName("Expense Management");
		Date saveDate = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy h:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		String datestring =  sdf.format(saveDate);
		Logger logger = Logger.getLogger("NameOfYourLogger");
		logger.log(Level.SEVERE," Date $$$$$$  === with IST"+datestring);
		String[] dateString=datestring.split(" ");
		String time=dateString[1];
		logger.log(Level.SEVERE," Date =="+dateString[0]);
		logger.log(Level.SEVERE," Date time =="+dateString[1]);
		pettycash.setTransactionTime(time);
		pettycash.setTransactionAmount(expAmt);
		try {
			pettycash.setTransactionDate(sdf.parse(datestring));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log(Level.SEVERE," date parse exception  =="+e);
		}
		pettycash.setAppoverName(this.getApproverName());
		pettycash.setPersonResponsible(this.getEmployee());
		pettycash.setAction("-");
		pettycash.setDocumentTitle(this.getPettyCashName());
		pettycash.setCompanyId(this.getCompanyId());
		pettycash.setPettyCashName(this.getPettyCashName());
		
		pettycash.setDocumentDate(getExpenseDate());
		/**Added by sheetal:24-12-2021**/
		pettycash.setexpenseCategory(getExpenseCategory());
		/** Sheetal : 07-04-2022 , storing description in petty cash transaction list**/
		pettycash.setDescription(this.getDescription());
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(pettycash);
		System.out.println("petty cash transaction list saved successfully");
		/************************* vijay *******************************/
	}
	private Date getExpenseDate() {
		Date docDate=null;
		if(expenseList!=null&&expenseList.size()!=0){
			for(ExpenseManagement obj:expenseList){
				Date tempDate=obj.getExpenseDate();
				if(docDate!=null){
					if(docDate.before(tempDate)){
						docDate=tempDate;
					}
				}else{
					docDate=tempDate;
				}
			}
		}else{
			docDate=new Date();
		}
		return docDate;
	}

    private String getExpenseCategory()  {
    		String category= "";
    		for(ExpenseManagement expense: this.getExpenseList()){
    			category = category + expense.getExpenseCategory()+"/";
        	}
    		return category;
    		
    	}

	@GwtIncompatible
	protected void accountingInterface(){
		for(ExpenseManagement expense : expenseList){
		
		String expenseCategory = "";
		String pettyCashName = "";
		if(expense.getExpenseCategory()!=null){
			expenseCategory = expense.getExpenseCategory();
		}
		if(getPettyCashName()!=null){
			pettyCashName = getPettyCashName();
		}
		if(this.getCount()!=0){
			
			/***date 28.7.2018 added by komal for new date format **/
	    	 Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());	 
			UpdateAccountingInterface.updateTally(currentDate,   //accountingInterfaceCreationDate
							 								this.getEmployee(),  //accountingInterfaceCreatedBy
							 								this.getStatus(),
							 								AppConstants.STATUS , //Status
							 								"",  //Remark
							 								"Accounts",    //String Module,
							 			 				
							 			 				
							 								"Expense Management",  //	String documentType,
							 								this.getCount(), //	int documentID,
							 								"Expense", 		 //documentTitle
							 								expense.getExpenseDate(), //documentDate
							 								"GL-Expense",     		 //doucmentGL
							 								"",            		//referenceDocumentNumber1
							 								null,            	  //referenceDocumentDate1
							 								"Expense",       //referenceDocumentType
							 								expense.getInvoiceNumber(),            //referenceDocumentNumber2
							 								null,		 //referencedocumentDate2
							 								pettyCashName,            //referencedocumentType2
							 								AppConstants.ACCOUNTTYPE,     //accountType
							 								0,       			//customerID
							 								"", 			 //customerName
							 								0, 				//customerCell
							 								0,      		 //vendorID
							 								"",    		//vendorName
							 								0,         		//vendorCell
							 								0, 			//employeeID
							 								this.getEmployee(),           //employeeName
							 								this.getBranch(),           //Branch
							 								"",             		 //personResponsible
							 								"",           			 //requestedBy
							 								this.getApproverName(),        //approvedBy
							 								expense.getPaymentMethod(),        //paymentMethod
							 								null, 						//paymentDate
							 								"",           			   //chequeNumber
							 								null,        			 //chequeDate
							 								"",             			 //bankName
							 								null,                			//bankAccount
							 								0,              			 //transferReferenceNumber
							 								null,   					//transactionDate
							 								null,	//contract start date
							 								null,  // contract end date
							 								0,        					//productID
							 								"",          // productCode
							 								"",         //productName
							 								0,          //Quantity
							 								null,       //productDate
							 								0,        //duration
							 								0,         //services
							 								"",        //unitOfMeasurement
							 								0.0,         //productPrice
							 								0,             //VATpercent
							 								0,             //VATamount
							 								0,              //VATglAccount
							 								0,             //serviceTaxPercent
							 								0,              //serviceTaxAmount
							 								0,             //serviceTaxGLaccount
							 								"",              //cForm
							 								0,             //cFormPercent
							 								0,         //cFormAmount
							 								0,			//cFormGlAccountNO.
							 								0,          //totalAmount
							 								expense.getAmount(),			//netPayable
							 								expenseCategory,//Contract Category 
							 								0,			//amountRecieved
							 								0.0,  // base Amt for tdscal in pay   by rohan 
							 								0,    //  tds percentage by rohan 
							 								0,     //  tds amount by rohan 
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								0,
							 								"",
							 								"",
							 								"",
							 								"",
							 								"",
							 								"",
							 								0,
							 								this.getCompanyId(),
							 								null,				//  billing from date (rohan)
							 								null,			//  billing to date (rohan)
							 								"", //Warehouse
															"",				//warehouseCode
															"",				//ProductRefId
															"",				//Direction
															"",				//sourceSystem
															"",				//Destination System
															null,
															null,
															null
								);
		}
	}
}



	public int getSalesOrderId() {
		return salesOrderId;
	}



	public void setSalesOrderId(int salesOrderId) {
		this.salesOrderId = salesOrderId;
	}



	public PersonInfo getPersonInfo() {
		return personInfo;
	}



	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}
	
	
	
	
}
