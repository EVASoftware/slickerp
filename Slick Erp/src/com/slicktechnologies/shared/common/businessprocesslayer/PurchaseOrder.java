package com.slicktechnologies.shared.common.businessprocesslayer;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.PurchaseOrderServiceImplementor;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.inventory.GRNDetails;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.OtherChargesMargin;
import com.slicktechnologies.shared.common.salesorder.VendorMargin;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;

@Entity
public class PurchaseOrder extends PurchaseDetails implements ApprovableProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = 881073765475956529L;

	@Index
	protected String POName;
	@Index
	protected Date PODate;
	@Index
	protected int RFQID;
	@Index
	protected String LOIGroup;
	@Index
	protected String LOItype;
//	@Index 31-10-2018 /\/
	protected ArrayList<ProductDetailsPO> productDetails;
	@Index
	protected ArrayList<VendorDetails> vendorDetails;
	@Index
	protected Date RFQDate;
	@Index
	protected Date LOIDate;
	@Index
	protected int LOIId;
	protected String poWarehouseName;
	protected double totalAmount;
	protected ArrayList<PaymentTerms> paymentTermsList;
	public static final String CREATED="Created",CANCELLED="Cancelled",REJECTED="Rejected",APPROVED="Approved",REQUESTED="Requested",CLOSED="Closed";
	protected String description;
	protected int creditDays;
	
	protected String deliverAdds;
	@Index
	protected List<Contact> contacts;
	
	/**Taxes Information related to the sales line items*/
	protected ArrayList<ProductOtherCharges> productTaxes;
	
	/**Other Charges excluding taxes on sales amount*/
	protected ArrayList<ProductOtherCharges> productCharges;
	
	protected double total;
	
	protected double gtotal;
	
	protected double netpayble;
	protected boolean deliverAdd;
	protected String cForm;
	protected double cstpercent;
	protected String paymentMethod;
	protected Date referenceDate;
	protected String cstName;
	
	
	/**
	 * Pur Engg assign pr to its team.
	 * Date :29-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	@Index
	protected String assignTo1;
	@Index
	protected String assignTo2;
	
	/**
	 * END
	 */
	
	/*** Date 18-08-2017 added by vijay for quick Purchase Order ******/
	@Index
	protected double paymentPaid;
	@Index
	protected double balancePayment;
	
	@Index
	Date quickPurchaseInvoiceDate;
	
	
	/**
	 * Date 20-7-2018 by jayshree 
	 */
	protected String termsOfDelivery;
	
	/**
	 * Date 09/06/2018 By vijay
	 * Des :- for before round off total final Amt and Round off Amount
	 */
	protected double totalFinalAmount;
	protected double roundOffAmount;
	/**
	 * ends here
	 */
	/** date 13.10.2018 added by komal for sasha **/
	@Index
	String authorisedBy;
	
	
	/**
	 * Date : 24-11-2018 By Vijay 
	 * Des :- for Other Charges
	 */
	protected ArrayList<OtherCharges> otherCharges;
	/**
	 * End
	 */
	 /** date 06.12.2018 added by komal **/
	protected String creditDaysComment;
	/**Date 10-10-2019 by Amol**/
	protected String pocName;
	protected long pocNumber;
	
	/**
	 * @author Anil
	 * @since 20-07-2020
	 * For PTSPL raised by Rahul Tiwari
	 * calculating vendor and other charges margin
	 */
	
	ArrayList<VendorMargin> vendorMargins;
	ArrayList<OtherChargesMargin> otherChargesMargins;
	
	double totalVendorMargin;
	double totalOtherChargesMargin;
	double totalOtherChargesSales;
	double totalOtherChargesPO;
	
	String deliveryAddressName;
	
/*****************************************Constructor*********************************************/
	public PurchaseOrder()
	{
		super();
		productDetails=new ArrayList<ProductDetailsPO>();
		vendorDetails=new ArrayList<VendorDetails>();
		paymentTermsList=new ArrayList<PaymentTerms>();
		Contact contact=new Contact();
		this.contacts=new Vector<Contact>();
		contacts.add(contact);
		contact=new Contact();
		contacts.add(contact);
		productCharges=new ArrayList<ProductOtherCharges>();
		productTaxes=new ArrayList<ProductOtherCharges>();
		paymentMethod="";
		cstName="";
		cForm="";
		description="";
		
		assignTo1="";
		assignTo2="";
		termsOfDelivery="";
		quickPurchaseInvoiceDate = null;
		authorisedBy = "";
		
		otherCharges=new ArrayList<OtherCharges>();
		creditDaysComment = "";
		deliveryAddressName="";
	}
	
	
	public String getTermsOfDelivery() {
	return termsOfDelivery;
}


public void setTermsOfDelivery(String termsOfDelivery) {
	this.termsOfDelivery = termsOfDelivery;
}


	public String getAssignTo1() {
		return assignTo1;
	}
	public void setAssignTo1(String assignTo1) {
		this.assignTo1 = assignTo1;
	}
	public String getAssignTo2() {
		return assignTo2;
	}
	public void setAssignTo2(String assignTo2) {
		this.assignTo2 = assignTo2;
	}
	
	
/**************************************Getters and setters*********************************/	
	
	public ArrayList<PaymentTerms> getPaymentTermsList() {
		return paymentTermsList;
	}

	public void setPaymentTermsList(List<PaymentTerms> list) {
		if(list!=null)
		{
		this.paymentTermsList = new ArrayList<PaymentTerms>();
		this.paymentTermsList.addAll(list);
		}
	}

	
	public double getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}


	public int getLOIId() {
		return LOIId;
	}


	public void setLOIId(int lOIId) {
		LOIId = lOIId;
	}


	public Date getRFQDate() {
		return RFQDate;
	}


	public void setRFQDate(Date rFQDate) {
		RFQDate = rFQDate;
	}


	public Date getLOIDate() {
		return LOIDate;
	}


	public void setLOIDate(Date lOIDate) {
		LOIDate = lOIDate;
	}


	public ArrayList<ProductDetailsPO> getProductDetails() {
		return productDetails;
	}


	public void setProductDetails(List<ProductDetailsPO> productDetails) {
		if(productDetails!=null)
		this.productDetails = new ArrayList<ProductDetailsPO>();
		this.productDetails.addAll(productDetails);
	}
	public ArrayList<VendorDetails> getVendorDetails() {
		return vendorDetails;
	}

	public void setVendorDetails(List<VendorDetails> vendorDetails) {
		if(vendorDetails!=null)
		this.vendorDetails = new ArrayList<VendorDetails>();
		this.vendorDetails.addAll(vendorDetails);
	}

	
	public String getPOName() {
		return POName;
	}

	public void setPOName(String pOName) {
		POName = pOName;
	}

	public Date getPODate() {
		return PODate;
	}

	public void setPODate(Date pODate) {
		PODate = pODate;
	}


	public int getRFQID() {
		return RFQID;
	}

	public void setRFQID(int rFQID) {
		RFQID = rFQID;
	}


	public String getLOIGroup() {
		return LOIGroup;
	}


	public void setLOIGroup(String lOIGroup) {
		LOIGroup = lOIGroup;
	}

	public String getLOItype() {
		return LOItype;
	}

	public void setLOItype(String lOItype) {
		LOItype = lOItype;
	}
	

	public String getDeliverAdds() {
		return deliverAdds;
	}

	public void setDeliverAdds(String deliverAdds) {
		this.deliverAdds = deliverAdds;
	}



	public String getPoWarehouseName() {
		return poWarehouseName;
	}

	public void setPoWarehouseName(String poWarehouseName) {
		this.poWarehouseName = poWarehouseName;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		if(paymentMethod!=null){
			this.paymentMethod = paymentMethod.trim();
		}
	}
	
	public String getCstName() {
		return cstName;
	}

	public void setCstName(String cstName) {
		if(cstName!=null){
			this.cstName = cstName.trim();
		}
	}

	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		if(referenceDate!=null){
			this.referenceDate = referenceDate;
		}
	}

	
	public double getPaymentPaid() {
		return paymentPaid;
	}


	public void setPaymentPaid(double paymentPaid) {
		this.paymentPaid = paymentPaid;
	}


	public double getBalancePayment() {
		return balancePayment;
	}


	public void setBalancePayment(double balancePayment) {
		this.balancePayment = balancePayment;
	}


	public Date getQuickPurchaseInvoiceDate() {
		return quickPurchaseInvoiceDate;
	}


	public void setQuickPurchaseInvoiceDate(Date quickPurchaseInvoiceDate) {
		this.quickPurchaseInvoiceDate = quickPurchaseInvoiceDate;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public static List<String> getStatusList()
	{
		ArrayList<String> purchaseorder=new ArrayList<String>();
		purchaseorder.add(APPROVED);
		purchaseorder.add(CREATED);
		purchaseorder.add(REQUESTED);
		purchaseorder.add(REJECTED);
		purchaseorder.add(CLOSED);
		purchaseorder.add(CANCELLED);//11-12-2023 ph-aircon want cancelled option in po status search

		
		return purchaseorder;
		
	}
	
	
	
	public void setAdress(Address address)
	{
		if(address!=null)
			this.contacts.get(0).setAddress(address);
	}

	public Address getAdress()
	{
		return contacts.get(0).getAddress();
	}

	public List<ProductOtherCharges> getProductCharges() {
		return productCharges;
	}

	public void setProductCharges(List<ProductOtherCharges> productCharges) {
		ArrayList<ProductOtherCharges> chargesArr=new ArrayList<ProductOtherCharges>();
		chargesArr.addAll(productCharges);
		this.productCharges = chargesArr;
	}
	
	public List<ProductOtherCharges> getProductTaxes() {
		return productTaxes;
	}

	public void setProductTaxes(List<ProductOtherCharges> productTaxes) {
		ArrayList<ProductOtherCharges> prodTaxesArr=new ArrayList<ProductOtherCharges>();
		prodTaxesArr.addAll(productTaxes);
		this.productTaxes = prodTaxesArr;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getGtotal() {
		return gtotal;
	}

	public void setGtotal(double gtotal) {
		this.gtotal = gtotal;
	}

	public double getNetpayble() {
		return netpayble;
	}

	public void setNetpayble(double netpayble) {
		this.netpayble = netpayble;
	}

	public String getcForm() {
		return cForm;
	}

	public void setcForm(String cForm) {
		if(cForm!=null){
			this.cForm = cForm.trim();
		}
	}

	public double getCstpercent() {
		return cstpercent;
	}

	public void setCstpercent(double cstpercent) {
		this.cstpercent = cstpercent;
	}

	public boolean isDeliverAdd() {
		return deliverAdd;
	}

	public void setDeliverAdd(boolean deliverAdd) {
		this.deliverAdd = deliverAdd;
	}
	
	public Integer getCreditDays() {
		return creditDays;
	}

	public void setCreditDays(int creditDays) {
		this.creditDays = creditDays;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}





	public long getPocNumber() {
		return pocNumber;
	}


	public void setPocNumber(long pocNumber) {
		this.pocNumber = pocNumber;
	}


	public String getPocName() {
		return pocName;
	}


	public void setPocName(String pocName) {
		this.pocName = pocName;
	}


	


	


	@OnSave
	@GwtIncompatible
	public void reactOnSaveData()
	{
		if(this.getPODate()!=null){
			setPODate(DateUtility.getDateWithTimeZone("IST", this.getPODate()));
		}
		/** date 14.10.2018 added by komal for sasha (auto generate po number**/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MonthwiseNumberGeneration", this.getCompanyId())){
		if(id == null){
			DecimalFormat df=new DecimalFormat("000");
			ServerAppUtility utility = new ServerAppUtility();
			SimpleDateFormat mmm = new SimpleDateFormat("MMM");
			mmm.setTimeZone(TimeZone.getTimeZone("IST"));
			String month = mmm.format(this.getPODate());
			String year = "";
			try {
				year = utility.getFinancialYearFormat(this.getPODate(), true);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String number = "PO/"+month+"/"+year;
			ProcessConfiguration config = new ProcessConfiguration();
			config= ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", companyId)
					.filter("processName", number).first().now();
			int cnt = 1;
				if (config !=null) {

					for (int i = 0; i < config.getProcessList().size(); i++) {
						try{
							cnt = Integer.parseInt(config.getProcessList().get(i).getProcessType());
							cnt = cnt +1;
							config.getProcessList().get(i).setProcessType(cnt+"");
						}catch(Exception e){
							cnt = 0;
						}
					}

				}
				else{
					config = new ProcessConfiguration();
					config.setCompanyId(this.companyId);
					config.setProcessName(number);
					config.setConfigStatus(true);
					ArrayList<ProcessTypeDetails> processList = new ArrayList<ProcessTypeDetails>();
					ProcessTypeDetails type = new ProcessTypeDetails();
					type.setProcessName(number);
					type.setProcessType(cnt+"");
					type.setStatus(true);
					processList.add(type);	
					config.setProcessList(processList);
				}
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(config);
				if(cnt <= 999){
					this.setRefOrderNO("PO/"+month.toUpperCase()+"/"+df.format(cnt)+"/"+year);
				}else{
					this.setRefOrderNO("PO/"+month.toUpperCase()+"/"+cnt+"/"+year);
				}
					
			}
	}
	}
	
	
		
		@Override
		@GwtIncompatible
		public void reactOnApproval(){
			Logger logger=Logger.getLogger("Starting Approval Logger ");
			
			//Ashwini Patil Date:30-08-2022 Only selected vendor required to be saved at the time of approval.
			ArrayList<VendorDetails> selectedvendorDetails=new  ArrayList<VendorDetails>();
			for(VendorDetails vd:this.getVendorDetails()) {
				if(vd.getStatus()==true) {
					selectedvendorDetails.add(vd);
					break;
				}			
			}
			this.setVendorDetails(selectedvendorDetails);
			
			
			/** date 20.9.2018 commented by komal **/
		//	PurchaseOrderServiceImplementor implementor=new PurchaseOrderServiceImplementor();
		//	implementor.approvalTransaction(this);
			
			ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
			ProcessConfiguration processConfig=null;
			if(processName!=null){
				processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGPO).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
			}
			int flag =0;
			
			if(processConfig!=null){
				for(int i = 0;i<processConfig.getProcessList().size();i++){
					if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
						flag = 1;
					}
				}
			}
			
			if(flag == 1&&processConfig!=null){
				System.out.println("ExecutingAccInterface");
				accountingInterface();
			}
			
			if(this.getCount()!=0){
				System.out.println("On APPROVAL !!!");
				HashSet<String>uniqueWarehouseName=new HashSet<String>();
				List<Integer> prodIdList = new ArrayList<Integer>();
				for(ProductDetailsPO obj:this.getProductDetails()){
					if(!obj.getWarehouseName().equals("")){
						uniqueWarehouseName.add(obj.getWarehouseName().trim());
						prodIdList.add(obj.getProductID());
					}
				}
				List<String> warehouseLis=new ArrayList<String>();
				warehouseLis.addAll(uniqueWarehouseName);
				
				if(warehouseLis.size()!=0){
					
					/** date 20.11.2018 added by komal for auto selection of grn details **/
					List<ProductInventoryView> viewList = ofy().load().type(ProductInventoryView.class).filter("companyId", this.getCompanyId())
													.filter("details.prodid IN", prodIdList).list();
					List<Storagebin> binList = ofy().load().type(Storagebin.class).filter("companyId", this.getCompanyId())
												.filter("warehouseName IN", warehouseLis).list();
					System.out.println("Wh Lis Size : "+warehouseLis.size());
					ArrayList<SuperModel> modelList=new ArrayList<SuperModel>();
					for(String warehouse:warehouseLis){
						ArrayList<GRNDetails> grnProdLis = getGrnProductListAsPerWarehouse(warehouse , viewList ,binList);
						GRN entity=getGrnDetailsAsPerWarehouse(warehouse,grnProdLis);
						SuperModel model=entity;
						modelList.add(model);
					}
					System.out.println("GRN LIST : "+modelList.size());
					
					GenricServiceImpl impl=new GenricServiceImpl();
					impl.save(modelList);
					
				}else{
					createGRN();
				}
				
				/**
				 * Date 20-12-2018 By Vijay
				 * Des :- NBHC Inventory management Automatic PR Qty
				 * PO qty will updated in PR for Automatic PR qty
				 */
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition",
						"EnableAutomaticPROrderQty", this.getCompanyId())){
					PurchaseRequisition purchaseReq = ofy().load().type(PurchaseRequisition.class)
							.filter("count", this.getPurchaseReqNo()).filter("companyId", this.getCompanyId())
							.first().now();
					if(purchaseReq!=null){
						for(ProductDetailsPO productpo : this.getProductDetails()){
							for(ProductDetails prodDetailsPr :purchaseReq.getPrproduct()){
								if(productpo.getProductID()==prodDetailsPr.getProductID()
									&& productpo.getWarehouseName().equals(prodDetailsPr.getWarehouseName())){
									prodDetailsPr.setPoQty(productpo.getProductQuantity());
								}
							}
						}
						ofy().save().entity(purchaseReq);
					}
				}
				/**
				 * ends here
				 */
			}
		}
		
		/**
		 * This method return GRN of particular warehouse
		 * Date : 13-10-2016 By Anil
		 * Release : 30 Sept 2016 
		 * Project : PURCHASE MODIFICATION(NBHC)
		 */
		@GwtIncompatible
		private GRN getGrnDetailsAsPerWarehouse(String warehouse,ArrayList<GRNDetails> grnProdLis) {
			GRN grnEntity=new GRN();
			
			grnEntity.setWarehouseName(warehouse);
			grnEntity.setCompanyId(this.getCompanyId());
			grnEntity.setPoNo(this.getCount());
			grnEntity.setPoTitle(this.getPOName());
			grnEntity.setPoCreationDate(this.getPODate());
			grnEntity.setTitle(this.getPOName()+" "+warehouse);
			
			if(this.getProject()!=null){
				grnEntity.setProJectName(this.getProject());
			}
			grnEntity.setCreationDate(this.getDeliveryDate());
			if(this.getEmployee()!=null){
				grnEntity.setEmployee(this.getEmployee());
			}
			if(this.getApproverName()!=null){
				grnEntity.setApproverName(this.getApproverName());
			}
			grnEntity.setStatus(GRN.CREATED);
			grnEntity.setInspectionRequired(AppConstants.NO);
			grnEntity.setBranch(this.getBranch());
			if(!this.getVendorDetails().isEmpty()){
				for(int i=0;i<this.getVendorDetails().size();i++){
					if(this.getVendorDetails().get(i).getStatus()==true){
						grnEntity.getVendorInfo().setFullName(this.getVendorDetails().get(i).getVendorName());
						grnEntity.getVendorInfo().setCount(this.getVendorDetails().get(i).getVendorId());
						grnEntity.getVendorInfo().setCellNumber(this.getVendorDetails().get(i).getVendorPhone());
					
						/**
						 * Date : 06-02-2017 By Anil
						 * 
						 */
						grnEntity.getVendorInfo().setPocName(this.getVendorDetails().get(i).getPocName());
						/**
						 * End
						 */
					}
				}
			}
			grnEntity.setInventoryProductItem(grnProdLis);
			
			/**
			 * Date 20-12-2018 by Vijay
			 * Des :- NBHC Inventory Management 
			 * for Automatic PR creation
			 */
			grnEntity.setPrNumber(this.getPurchaseReqNo());
			/**
			 * ends here
			 */
			
			/** Date 12-08-2020 by Vijay for sasha PO's Supplier's Ref./Order No in GRN then into Billingdocument the same ***/
			grnEntity.setRefNo3(this.getRefOrderNO());
			
			return grnEntity;
		}

		/**
		 * This method returns GRN Product List as per warehouse from PO product list
		 * Date: 13-10-2016 By Anil
		 * Release : 30 Sept 2016
		 * Project : PURCHASSE MODIFICATION(NBHC)
		 */
		@GwtIncompatible
		private ArrayList<GRNDetails> getGrnProductListAsPerWarehouse(String warehouse , List<ProductInventoryView> viewList , List<Storagebin> binList) {
			ArrayList<GRNDetails> newprod = new ArrayList<GRNDetails>();
			HashSet<Integer> unigueProdSet=new HashSet<Integer>();
			for (ProductDetailsPO temp : this.getProductDetails()) {
				if(temp.getWarehouseName().trim().equals(warehouse)){
					GRNDetails grn = new GRNDetails();
					String LocationBIN = getLocationAndBin(warehouse , viewList , temp.getProductID(),true,binList);
					if(LocationBIN.equals("")){
						LocationBIN = getLocationAndBin(warehouse, viewList, temp.getProductID() , false,binList);
					}
					String location = "" , bin = "";
					double quantity = 0;
					if(!LocationBIN.equals("")){
						/**
						 * @author Anil
						 * @since 07-01-2021
						 * earlier it was dash seprated
						 * Raised by Ashwini
						 */
						String array[] = LocationBIN.split("[$]");
//						String array[] = LocationBIN.split("-");
						if(array.length>0){
							location = array[0];
						}
						if(array.length>1){
							bin = array[1];
						}
						if(array.length>2){
							quantity = Double.parseDouble(array[2]);
						}
					}
					grn.setProductID(temp.getProductID());
					grn.setProductCode(temp.getProductCode());
					grn.setProductCategory(temp.getProductCategory());
					grn.setProductName(temp.getProductName());
					grn.setUnitOfmeasurement(temp.getUnitOfmeasurement());
					grn.setProductQuantity(temp.getProductQuantity());
					grn.setProdPrice(temp.getProdPrice());
					grn.setTotal(temp.getTotal());
					grn.setDiscount(temp.getDiscount());
					grn.setTax(temp.getTax());
					grn.setVat(temp.getVat());
					grn.setWarehouseLocation(temp.getWarehouseName());
					/** date 20.11.2018 added by komal**/
					grn.setStorageLoc(location);
					grn.setStorageBin(bin);
					grn.setAvailableStock(quantity);
					grn.setWarehouseAddress(temp.getWarehouseAddress());
					/** date 1.10.2018 added by komal**/
					grn.setPrduct(temp.getPrduct());
					grn.setPurchaseTax1(temp.getPurchaseTax1());
					grn.setPurchaseTax2(temp.getPurchaseTax2());
					newprod.add(grn);
					unigueProdSet.add(temp.getProductID());
				}
			}
			System.out.println("GRN PROD LIS SIZE : "+newprod.size());
			System.out.println("Unique Prod List Size : "+unigueProdSet.size());
			
			if(unigueProdSet.size()<newprod.size()){
				ArrayList<GRNDetails> grnList=new ArrayList<GRNDetails>();
				for(Integer object:unigueProdSet){
					GRNDetails prodObj=getUniqueGrnProduct(object,newprod);
					grnList.add(prodObj);
				}
				System.out.println("UNIQUE GRN PRO LIS SIZ : "+grnList.size());
				return grnList;
			}
			
			 
			return newprod;
		}

		/**
		 * This method return unique GRN product in case PO is made of multiple PR 
		 * and two or more PR is raise for same product.
		 * In that case PO contains same product twice or more than that.
		 * Date : 13-10-2016 By Anil 
		 * Release : 30 SEPT 2016 
		 * Project: PUCHASE MODIFICATION(NBHC)  
		 */
		@GwtIncompatible
		private GRNDetails getUniqueGrnProduct(int productId,ArrayList<GRNDetails> newprod) {
			double qty=0;
			GRNDetails obj = null;
			for (GRNDetails object : newprod) {
				if(productId==object.getProductID()){
					qty=qty+object.getProductQuantity();
					if(obj==null){
						obj=object;
					}
				}
			}
			
			GRNDetails grn = new GRNDetails();
			
			grn.setProductID(obj.getProductID());
			grn.setProductCode(obj.getProductCode());
			grn.setProductCategory(obj.getProductCategory());
			grn.setProductName(obj.getProductName());
			grn.setUnitOfmeasurement(obj.getUnitOfmeasurement());
			grn.setProductQuantity(qty);
			grn.setProdPrice(obj.getProdPrice());
			double total=qty*obj.getProdPrice();
			grn.setTotal(total);
			grn.setDiscount(obj.getDiscount());
			grn.setTax(obj.getTax());
			grn.setVat(obj.getVat());
			grn.setWarehouseLocation(obj.getWarehouseName());
			grn.setStorageLoc("");
			grn.setStorageBin("");
			
			return grn;
		}


		@GwtIncompatible
		protected void createGRN()
		{
			final GenricServiceImpl genimpl=new GenricServiceImpl();
			
			GRN grnEntity=new GRN();
			grnEntity.setPoNo(this.getCount());
			grnEntity.setPoTitle(this.getPOName());
			grnEntity.setPoCreationDate(this.getPODate());
			grnEntity.setTitle(this.getPOName());
			
			if(this.getProject()!=null){
				grnEntity.setProJectName(this.getProject());
			}
			grnEntity.setCreationDate(this.getDeliveryDate());
			if(this.getEmployee()!=null){
				grnEntity.setEmployee(this.getEmployee());
			}
			if(this.getApproverName()!=null){
				grnEntity.setApproverName(this.getApproverName());
			}
			grnEntity.setStatus(GRN.CREATED);
			grnEntity.setInspectionRequired(AppConstants.NO);
			grnEntity.setBranch(this.getBranch());
			if(!this.getVendorDetails().isEmpty()){
				for(int i=0;i<this.getVendorDetails().size();i++){
					if(this.getVendorDetails().get(i).getStatus()==true){
						grnEntity.getVendorInfo().setFullName(this.getVendorDetails().get(i).getVendorName());
						grnEntity.getVendorInfo().setCount(this.getVendorDetails().get(i).getVendorId());
						grnEntity.getVendorInfo().setCellNumber(this.getVendorDetails().get(i).getVendorPhone());
					
						/**
						 * Date : 06-02-2017 By Anil
						 * 
						 */
						grnEntity.getVendorInfo().setPocName(this.getVendorDetails().get(i).getPocName());
						/**
						 * End
						 */
					
					}
				}
			}
			
			if(this.getProductDetails().size()!=0){
				ArrayList<GRNDetails> newprod = new ArrayList<GRNDetails>();
				ArrayList<ProductDetailsPO> prodlist = this.getProductDetails();
				
				for (ProductDetailsPO temp : prodlist) {
					GRNDetails grn = new GRNDetails();
					grn.setProductID(temp.getProductID());
					grn.setProductCode(temp.getProductCode());
					grn.setProductCategory(temp.getProductCategory());
					grn.setProductName(temp.getProductName());
					grn.setUnitOfmeasurement(temp.getUnitOfmeasurement());
					grn.setProductQuantity(temp.getProductQuantity());
					grn.setProdPrice(temp.getProdPrice());
					grn.setTotal(temp.getTotal());
					grn.setDiscount(temp.getDiscount());
					grn.setTax(temp.getTax());
					grn.setVat(temp.getVat());
					grn.setWarehouseLocation("");
					grn.setStorageLoc("");
					grn.setStorageBin("");
					/** date 1.10.2018 added by komal**/
					grn.setPrduct(temp.getPrduct());
					grn.setPurchaseTax1(temp.getPurchaseTax1());
					grn.setPurchaseTax2(temp.getPurchaseTax2());
					newprod.add(grn);
				}
				grnEntity.setInventoryProductItem(newprod);
			}
			
			grnEntity.setCompanyId(this.getCompanyId());
			
			// vijay
			grnEntity.setTitle(this.getPOName());
			
			/**
			 * Date 20-12-2018 by Vijay
			 * Des :- NBHC Inventory Management 
			 * for Automatic PR creation
			 */
			grnEntity.setPrNumber(this.getPurchaseReqNo());
			/**
			 * ends here
			 */
			
			/** Date 12-08-2020 by Vijay for sasha PO's Supplier's Ref./Order No in GRN then into Billingdocument the same ***/
			grnEntity.setRefNo3(this.getRefOrderNO());
			
			genimpl.save(grnEntity);
		}
		
			
		
		
		
		
		@GwtIncompatible
		protected void accountingInterface(){
		if(this.getCount()!=0){
				
				System.out.println("Product List Size :: "+this.getProductDetails().size());
				
				/*************************************************************************************************************/
				for(int  i = 0;i<this.getProductDetails().size();i++){
					
					System.out.println("Vat Tax %"+this.getProductDetails().get(i).getVat());
					System.out.println("Prod ");
					
			/*************************************************Start************************************************/
					
					String unitofmeasurement = this.getProductDetails().get(i).getUnitOfmeasurement();
					double productprice = this.getProductDetails().get(i).getProdPrice();
					 int prodId=this.getProductDetails().get(i).getProductID();
					 String productCode = this.getProductDetails().get(i).getProductCode();
					 String productName = this.getProductDetails().get(i).getProductName();
					 double productQuantity = this.getProductDetails().get(i).getProductQuantity();
					 double totalAmount = this.getProductDetails().get(i).getProdPrice()*this.getProductDetails().get(i).getProductQuantity();
					 double calculatedamt = 0;
						double calculetedservice = 0;
					 
						double cformamtService= 0;
					 double serviceTax=0 ;
					 double cformPercent=0;
					 double cformAmount=0;
					 double vatPercent=0;
					 String cform = "";
					 
					 int vatglaccno=0;
					 int serglaccno=0;
					 int cformglaccno=0;
					 
				/***************************************************************************************/
					 
					 if(this.getcForm()!= null&&this.getcForm().trim().equals(AppConstants.YES)){
						 cform=AppConstants.YES;
						 cformPercent=this.getCstpercent();
						 cformAmount=this.getCstpercent()*totalAmount/100;
						 cformamtService=this.getCstpercent()*totalAmount/100;
						 if(this.getProductDetails().get(i).getTax()!=0){
							 serviceTax=this.getProductDetails().get(i).getTax();
							 calculetedservice=((cformamtService+totalAmount)*this.getProductDetails().get(i).getTax())/100;
						 }
						 
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getCstpercent()).filter("isCentralTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 cformglaccno=serglAcc.getGlAccountNo();
						 
						 
						 
					 }
					 
					 else if(this.getcForm()!= null&&this.getcForm().trim().equals(AppConstants.NO)){
						 cform=AppConstants.NO;
						 cformPercent=this.getProductDetails().get(i).getVat();
						 cformAmount=this.getProductDetails().get(i).getVat()*totalAmount/100;
						 cformamtService=this.getProductDetails().get(i).getVat()*totalAmount/100;
						 
						 if(this.getProductDetails().get(i).getTax()!=0){
							 serviceTax=this.getProductDetails().get(i).getTax();
							 calculetedservice=((cformamtService+totalAmount)*this.getProductDetails().get(i).getTax())/100;
						 }
						 
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getCstpercent()).filter("isCentralTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 serglaccno=serglAcc.getGlAccountNo();
					 }
					 
					 
					 else{//if the customer is of same state
						 
						 if(this.getProductDetails().get(i).getVat()!=0)
						 {
							 vatPercent=this.getProductDetails().get(i).getVat();
							 calculatedamt=this.getProductDetails().get(i).getVat()*totalAmount/100;
							 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getProductDetails().get(i).getVat()).filter("isVatTax",true).first().now();
							 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
							 if(vatglAcc!=null) {
								 vatglaccno=vatglAcc.getGlAccountNo();
							 }
						 }
						 
						 if(this.getProductDetails().get(i).getTax()!=0)
						 {
							 serviceTax=this.getProductDetails().get(i).getTax();
							 calculetedservice=(calculatedamt+totalAmount)*this.getProductDetails().get(i).getTax()/100;
							 
						 }
						 
						 
					 }
					 
					 
					 if(this.getProductDetails().get(i).getTax()!=0)
					 {
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getProductDetails().get(i).getTax()).filter("isServiceTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 if(serglAcc!=null) {
							 serglaccno=serglAcc.getGlAccountNo();
						 }
					 }
					 
				
					 
					 
					 /***************************************************************************************/
					 
//					 int referenceOrderNo = 0;
					 
//					 String referencedoctype = "";
					
					 
//					 if(this.getLOIId()!=0){
//						 referenceOrderNo = this.getLOIId();
//						 referencedoctype = AppConstants.LOIREFERENCETYPE;
//					 }
//					 
//					 if(this.getRFQID()!=0){
//						 
//						 referenceOrderNo = this.getRFQID();
//						 referencedoctype = AppConstants.RFQREFERENCETYPE;
//					 }
//					 
//					 if(this.getPurchaseReqNo()!=0){
//						 
//						 referenceOrderNo = this.getPurchaseReqNo();
//						 referencedoctype = AppConstants.PRREFERENCETYPE;
//					 }
//					 
					 
					 
					 /*******************************************************************/
					 
					
					 
					 if(this.getProductDetails().get(i).getVat()!=0&&this.getcForm()==null){
						 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getProductDetails().get(i).getVat()).filter("isVatTax",true).first().now();
//						 System.out.println("tax val"+vattaxDtls.getGlAccountName().trim());
						 GLAccount vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
						 vatglaccno=vatglAcc.getGlAccountNo();
					 }
					 
					 if(this.getProductDetails().get(i).getTax()!=0){
						 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getProductDetails().get(i).getTax()).filter("isServiceTax",true).first().now();
						 GLAccount serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now();
						 serglaccno=serglAcc.getGlAccountNo();
					 }

					 
					 double netPayable = this.getNetpayble();
					 
					 
					 String[] oc = new String[12];
					 double[] oca = new double[12];
					 
					 
					 
					 for(int k = 0; k < this.getProductCharges().size();k++){
						 oc[k] = getProductCharges().get(k).getChargeName();
						 oca[k] = getProductCharges().get(k).getChargePayable();
					 }
					 
					 /**********************************End************************************************/
					
					 String vName = null;
					 long vCell = 0;
					 int vId = 0;
					 Vendor vendor = null;
					 
					 for(int o  = 0; o<this.getVendorDetails().size();o++){
						 if(this.getVendorDetails().get(o).getStatus()==true){
							
						vName = this.getVendorDetails().get(o).getVendorName();
						vCell = this.getVendorDetails().get(o).getVendorPhone();
						vId= this.getVendorDetails().get(o).getVendorId();
						
						 vendor=ofy().load().type(Vendor.class).filter("companyId",getCompanyId()).filter("count", vId).first().now();
							 
						 }
						 }
						 
						 
					 
					 
					 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
														 this.getEmployee(),//accountingInterfaceCreatedBy
														 this.getStatus(),
														 AppConstants.STATUS ,//Status
														 "",//remark
														 "Purchase",//module
														 "Purchase Order",//documentType
															this.getCount(),//docID
															this.getPOName(),//DOCtile
															this.getCreationDate(), //docDate
															AppConstants.DOCUMENTGL,//docGL
															"",//ref doc no.1
															this.getCreationDate(),//ref doc date.1
															AppConstants.REFDOCPO,//ref doc type 1
															"",//ref doc no 2
															null,//ref doc date 2
															"",//ref doc type 2
															AppConstants.ACCOUNTTYPEAP,//accountType
															0,//custID
															"",//custName
															0,//custCell
															vId,//vendor ID
															vName,//vendor NAme
															vCell,//vendorCell
															0,//empId
															" ",//empNAme
															this.getBranch(),//branch
															this.getEmployee(),//personResponsible
															"",//requestedBY
															this.getApproverName(),//approvedBY
															this.getPaymentMethod(),//paymentmethod
															null,//paymentdate
															"",//cheqno.
															null,//cheqDate
															null,//bankName
															null,//bankAccount
															0,//transferReferenceNumber
															null,//transactionDate
															null,//contract start date
															null, // contract end date
															prodId,//prod ID
															productCode,//prod Code
															productName,//productNAme
															productQuantity,//prodQuantity
															null,//productDate
															0,//duration
															0,//services
															unitofmeasurement,//unit of measurement
															productprice,//productPrice
															vatPercent,//VATpercent
															calculatedamt,//VATamt
															vatglaccno,//VATglAccount
															serviceTax,//serviceTaxPercent
															calculetedservice,//serviceTaxAmount
															serglaccno,//serviceTaxGLaccount
															cform,//cform
															cformPercent,//cformpercent
															cformAmount,//cformamount
															cformglaccno,//cformGlaccount	
															totalAmount,//totalamouNT
															netPayable,//NET PAPAYABLE
															"",//Contract Category
															0,//amount Recieved
															0.0,// base Amt for tdscal in pay  by rohan  
															0, //  tds percentage by rohan 
															0,//  tds amount  by rohan 
															oc[0],
															oca[0],
															oc[1],
															oca[1],
															oc[2],
															oca[2],
															oc[3],
															oca[3],
															oc[4],
															oca[4],
															oc[5],
															oca[5],
															oc[6],
															oca[6],
															oc[7],
															oca[7],
															oc[8],
															oca[8],
															oc[9],
															oca[9],
															oc[10],
															oca[10],
															oc[11],
															oca[11],
															vendor.getPrimaryAddress().getAddrLine1(),
															vendor.getPrimaryAddress() .getLocality(),
															vendor.getPrimaryAddress().getLandmark(),
															vendor.getPrimaryAddress().getCountry(),
															vendor.getPrimaryAddress().getState(),
															vendor.getPrimaryAddress().getCity(),
															vendor.getPrimaryAddress().getPin(),
															this.getCompanyId(),
															null,				//  billing from date (rohan)
															null,			//  billing to date (rohan)
															"", //Warehouse
															"",				//warehouseCode
															"",				//ProductRefId
															"",				//Direction
															"",				//sourceSystem
															"",
															null,null,null
															);

			}
			
		}

		}
			
			
		/**
		 * Date :09-02-2017 By ANIL
		 * This method is used to send Email to PR Purchase Engineer on assigning of PO to PO Purchase Engineer and after approval of PO.
		 * this is  NBHC DEADSTOCK requirement active only after process configuration is true.
		 */
		@OnSave
		@GwtIncompatible
		private void sendEmailToPoPurchaseEngg() {
			
			if(this.getStatus().equals(PurchaseOrder.CREATED)||this.getStatus().equals(PurchaseOrder.APPROVED)){
				boolean sendEmailFlag=false;
				ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", this.getCompanyId()).filter("processName", "PurchaseOrder").filter("configStatus", true).first().now();
				if(processConfig!=null){
					for(int k=0;k<processConfig.getProcessList().size();k++){
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("POASSIGNANDAPPROVALMAILTOPRPURENGG")
								&&processConfig.getProcessList().get(k).isStatus()==true){
							sendEmailFlag=true;
						}
					}
				}
				
				if(sendEmailFlag){
					ArrayList<String> toEmailList=new ArrayList<String>();
					ArrayList<String> ccEmailList=new ArrayList<String>();
					String fromEmail="";
					PurchaseRequisition pr=ofy().load().type(PurchaseRequisition.class).filter("companyId", this.getCompanyId()).filter("count", this.getPurchaseReqNo()).first().now();
					if(pr!=null){
						Employee emp=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
								.filter("fullname", pr.getEmployee().trim()).first().now();
						if(emp!=null){
							if(emp.getEmail()!=null&&!emp.getEmail().equals("")){
								toEmailList.add(emp.getEmail());
							}
						}
						if(toEmailList.size()!=0){
							Company comp=ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
							if(comp!=null){
								if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
									fromEmail=comp.getEmail();
								}
							}
						}
						if(!fromEmail.equals("")){
							String mailSub="";
							String mailMsg="";
							if(this.getStatus().equals(PurchaseOrder.CREATED)){
								mailSub="PR Id -"+this.getPurchaseReqNo()+" is processed.";
								mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
												+"PR Id "+this.getPurchaseReqNo()+" is assigned to "+this.getAssignTo1()+" "+this.getAssignTo2()+"\n"+"\n"+"\n"
												+"Purchase Engineer : "+this.getEmployee()+"\n";
							}else if(this.getStatus().equals(PurchaseOrder.APPROVED)){
								mailSub="PR Id -"+this.getPurchaseReqNo()+" is Approved.";
								mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
												+"PR Id "+this.getPurchaseReqNo()+" is Approved"+"\n"+"\n"+"\n"
												+"Purchase Engineer : "+this.getEmployee()+"\n";
							}
							
							Email obj=new Email();
							obj.sendEmail(mailSub, mailMsg, toEmailList, ccEmailList, fromEmail);
						}
					}
				}
			}
			
		}
			

		
		public double getTotalFinalAmount() {
			return totalFinalAmount;
		}


		public void setTotalFinalAmount(double totalFinalAmount) {
			this.totalFinalAmount = totalFinalAmount;
		}


		public double getRoundOffAmount() {
			return roundOffAmount;
		}


		public void setRoundOffAmount(double roundOffAmount) {
			this.roundOffAmount = roundOffAmount;
		}


		public String getAuthorisedBy() {
			return authorisedBy;
		}


		public void setAuthorisedBy(String authorisedBy) {
			this.authorisedBy = authorisedBy;
		}
		@GwtIncompatible
		private String getLocationAndBin(String warehouse , List<ProductInventoryView> viewList , int productID , boolean flag , List<Storagebin> binList){
			String str = ""; 
			if(flag){
			for(ProductInventoryView view : viewList){
				if(view.getProdID() == productID){
					for(ProductInventoryViewDetails info : view.getDetails()){
						if(info.getWarehousename().equals(warehouse)){
							str = info.getStoragelocation()+"$"+info.getStoragebin()+"$"+info.getAvailableqty();
							break;
						}
					}
				}
			}
		}else{
			for(Storagebin bin : binList){
				if(bin.getWarehouseName().equals(warehouse)){
					str = bin.getStoragelocation()+"$"+bin.getBinName()+"$"+0;
					break;
				}
			}
		}
			return str;
			
		}


		public ArrayList<OtherCharges> getOtherCharges() {
			return otherCharges;
		}


		public void setOtherCharges(ArrayList<OtherCharges> otherCharges) {
			this.otherCharges = otherCharges;
		}
		
		public String getCreditDaysComment() {
			return creditDaysComment;
		}


		public void setCreditDaysComment(String creditDaysComment) {
			this.creditDaysComment = creditDaysComment;
		}

		/**
		 * @author Vijay Chougule Date 22-06-2020
		 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
		 */

		@OnSave
		@GwtIncompatible
		public void setTimeMidOfTheDayToDate()
		{
			DateUtility dateUtility = new DateUtility();
			if(this.getPODate()!=null){
				this.setPODate(dateUtility.setTimeMidOftheDayToDate(this.getPODate()));
			}
		}
		/**
		 * ends here	
		 */
		
		public ArrayList<VendorMargin> getVendorMargins() {
			return vendorMargins;
		}


		public void setVendorMargins(List<VendorMargin> vendorMargins) {
			ArrayList<VendorMargin> list=new ArrayList<VendorMargin>();
			if(vendorMargins!=null&&vendorMargins.size()!=0){
				list.addAll(vendorMargins);
			}
			this.vendorMargins = list;
		}


		public ArrayList<OtherChargesMargin> getOtherChargesMargins() {
			return otherChargesMargins;
		}


		public void setOtherChargesMargins(List<OtherChargesMargin> otherChargesMargins) {
			ArrayList<OtherChargesMargin> list=new ArrayList<OtherChargesMargin>();
			if(otherChargesMargins!=null&&otherChargesMargins.size()!=0){
				list.addAll(otherChargesMargins);
			}
			this.otherChargesMargins = list;
		}


		public double getTotalVendorMargin() {
			return totalVendorMargin;
		}


		public void setTotalVendorMargin(double totalVendorMargin) {
			this.totalVendorMargin = totalVendorMargin;
		}


		public double getTotalOtherChargesMargin() {
			return totalOtherChargesMargin;
		}


		public void setTotalOtherChargesMargin(double totalOtherChargesMargin) {
			this.totalOtherChargesMargin = totalOtherChargesMargin;
		}


		public double getTotalOtherChargesSales() {
			return totalOtherChargesSales;
		}


		public void setTotalOtherChargesSales(double totalOtherChargesSales) {
			this.totalOtherChargesSales = totalOtherChargesSales;
		}


		public double getTotalOtherChargesPO() {
			return totalOtherChargesPO;
		}


		public void setTotalOtherChargesPO(double totalOtherChargesPO) {
			this.totalOtherChargesPO = totalOtherChargesPO;
		}


		public String getDeliveryAddressName() {
			return deliveryAddressName;
		}


		public void setDeliveryAddressName(String deliveryAddressName) {
			this.deliveryAddressName = deliveryAddressName;
		}
		
		
		
}
