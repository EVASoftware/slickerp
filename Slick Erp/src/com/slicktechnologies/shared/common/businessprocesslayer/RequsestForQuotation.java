package com.slicktechnologies.shared.common.businessprocesslayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

// TODO: Auto-generated Javadoc
/**
 * Repersents a Request For Quotation to Multiple Vendors, Purpose of this
 * Entity is to send Email to Vendors.
 */
@Entity
public class RequsestForQuotation extends PurchaseDetails {

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 7460028191219498680L;

	/******************************************************** Constants ************************************************************/

	/** The Constant REQUESTED. */
	public static final String CREATED = "Created", CANCELLED = "Cancelled",
			REJECTED = "Rejected", APPROVED = "Approved",
			REQUESTED = "Requested", CLOSED = "Closed";

	/********************************************************* Applicability Attibutes *********************************************/

	/** The product details. */
	@Index
	ArrayList<ProductDetails> productDetails;

	/** The vendor details. */
	@Index
	ArrayList<VendorDetails> vendorDetails;

	/** The letter type. */
	protected int letterType;

	ArrayList<Key<SuperProduct>> productKey;
	ArrayList<Key<Vendor>> vendorKey;

	String description;
	
	/**
	 * Pur Engg assign pr to its team.
	 * Date :28-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	@Index
	protected String assignTo1;
	@Index
	protected String assignTo2;
	
	/**
	 * END
	 */
	
	
	/************************* Constructor *****************************************/
	/**
	 * Instantiates a new requsest for quotation.
	 */

	public RequsestForQuotation() {
		super();
		productKey = new ArrayList<Key<SuperProduct>>();
		vendorKey = new ArrayList<Key<Vendor>>();
		description="";
		assignTo1="";
		assignTo2="";
	}
/**
 * **********************getters and setters *************************
 * @return
 */
	public String getAssignTo1() {
		return assignTo1;
	}
	public void setAssignTo1(String assignTo1) {
		this.assignTo1 = assignTo1;
	}
	public String getAssignTo2() {
		return assignTo2;
	}
	public void setAssignTo2(String assignTo2) {
		this.assignTo2 = assignTo2;
	}
	
	/**************************************** Relational Management part *************************/

	/******* for Product ************/

	Key<SuperProduct> RfqprodKey;

	@GwtIncompatible
	@OnSave
	protected void makeProdKey() {
		if (productDetails != null) {
			this.productKey = new ArrayList<Key<SuperProduct>>();
			for (ProductDetails details : this.productDetails) {
				Key<SuperProduct> prodKey = ofy().load()
						.type(SuperProduct.class)
						.filter("count", details.getProductID())
						.filter("companyId", getCompanyId()).keys().first()
						.now();
				productKey.add(prodKey);

			}

		}
	}

	@GwtIncompatible
	@OnLoad
	public void upDateProduct() {
		if (productKey != null) {

			for (Key<SuperProduct> pKey : this.productKey) {
				if (pKey != null) {
					SuperProduct prod = ofy().load().key(pKey).now();
					for (ProductDetails details : productDetails) {
						System.out.println(prod.getCount());
						System.out.println(details.getProductID());
						if (prod.getCount() == details.getProductID()) {
							details.setProductName(prod.getProductName());
							details.setProductCode(prod.getProductCode());
							details.setProductCategory(prod.getProductCategory());
						}
					}

				} else
					System.out.println("key null");

			}
		}

	}

	/***************** For Vendors ****************/

	@GwtIncompatible
	@OnSave
	public void saveVendorKey() {
		if (vendorDetails != null) {
			this.vendorKey = new ArrayList<Key<Vendor>>();
			for (VendorDetails details : this.vendorDetails) {
				// Load Vendor Using Relation id
				Key<Vendor> venKey = ofy().load().type(Vendor.class)
						.filter("count", details.getVendorId())
						.filter("companyId", getCompanyId()).keys().first()
						.now();
				vendorKey.add(venKey);

			}
		}
	}

	@GwtIncompatible
	@OnLoad
	public void upDateVendor() {
		if (vendorKey != null) {

			for (Key<Vendor> venKey : this.vendorKey) {
				if (venKey != null) {
					Vendor ven = ofy().load().key(venKey).now();
					for (VendorDetails details : vendorDetails) {
						if (ven.getCount() == details.getVendorId()) {
							details.setVendorName(ven.getVendorName());
							details.setVendorPhone(ven.getCellNumber1());
							details.setVendorEmailId(ven.getEmail());
						}
					}

				}

			}
		}

	}

	/**
	 * *********************Getters and setters********************************.
	 *
	 * @return the quotation type
	 */

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */

	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<ProductDetails> getProductinfo() {
		return productDetails;
	}

	public void setProductinfo(List<ProductDetails> productinfo) {
		if (productinfo != null) {
			this.productDetails = new ArrayList<ProductDetails>();
			this.productDetails.addAll(productinfo);
		}
	}

	public ArrayList<VendorDetails> getVendorinfo() {
		return vendorDetails;
	}

	public void setVendorinfo(List<VendorDetails> vendorinfo) {
		if (vendorinfo != null) {
			this.vendorDetails = new ArrayList<VendorDetails>();
			this.vendorDetails.addAll(vendorinfo);
		}
	}

	public String getHeader() {
		return header;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	@Override
	public String getEmployee() {
		return this.employee.toString();
	}

	/**
	 * Gets the branch.
	 *
	 * @return the branch
	 */
	@Override
	public String getBranch() {
		return this.branch;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}

	/**
	 * ***********list of status************************.
	 *
	 * @return the status list
	 */

	public static List<String> getStatusList() {
		ArrayList<String> quostatuslist = new ArrayList<String>();
		quostatuslist.add(APPROVED);
		quostatuslist.add(CREATED);
		quostatuslist.add(REQUESTED);
		quostatuslist.add(REJECTED);
		quostatuslist.add(CLOSED);
		quostatuslist.add(CANCELLED);

		return quostatuslist;

	}

	/**
	 * React on approval.
	 */
	@Override
	public void reactOnApproval() {
		// TODO Auto-generated method stub

	}

	/**
	 * Checks if is duplicate.
	 *
	 * @param m
	 *            the m
	 * @return true, if is duplicate
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getPrDate()!=null){
			this.setPrDate(dateUtility.setTimeMidOftheDayToDate(this.getPrDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */

}
