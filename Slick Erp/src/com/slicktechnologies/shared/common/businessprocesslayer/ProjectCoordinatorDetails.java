package com.slicktechnologies.shared.common.businessprocesslayer;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;


@Embed
public class ProjectCoordinatorDetails implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1694748870806623151L;
	/************************************************Applicability Attributes*****************************************************/
	@Index
	protected int count;
	@Index
	protected String role;
	@Index
	protected String name;
	@Index
	protected String email;
	
	@Index
	protected Long cell;

	@Index
	protected String pocName;

	@Index
	protected double kdf;

	/************************************************Getters And Setters********************************************************/


	public void setRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCell() {
		return cell;
	}

	public void setCell(Long cell) {
		this.cell = cell;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public double getKdf() {
		return kdf;
	}

	public void setKdf(double kdf) {
		this.kdf = kdf;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	
	
}

