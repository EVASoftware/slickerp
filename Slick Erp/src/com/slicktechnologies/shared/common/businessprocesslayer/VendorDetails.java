package com.slicktechnologies.shared.common.businessprocesslayer;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


// TODO: Auto-generated Javadoc
/**
 * Entity repersenting Vendor Details for {@link RequsestForQuotation}.
 */
@Embed
@Index
public class VendorDetails extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1666501219678853005L;

	
	
	/** The vendor name. */
	protected String vendorName;
	
	/** The vendor id. */
	protected int vendorId;

	/** The vendor phone */
	protected long vendorPhone;
	
	/** The vendor email */
	
	protected String vendorEmailId;
	
	protected boolean status;
	
	
	protected String pocName;
	
	protected String refNo;
	
	
	/*************************************Constructor****************************************/
	
	public VendorDetails()
	{
		super();
		refNo="";
	}
	
	
	
	
	/***********************Getters and Setters************************************************/
	
	/**
	 * Gets the vendor id.
	 *
	 * @return the vendor id
	 */
	public int getVendorId() {
		return vendorId;
	}
	public boolean getStatus() {
		return status;
	}




	public void setStatus(boolean status) {
		this.status = status;
	}




	public long getVendorPhone() {
		return vendorPhone;
	}




	public void setVendorPhone(long vendorPhone) {
		this.vendorPhone = vendorPhone;
	}




	/**
	 * Sets the vendor id.
	 *
	 * @param vendorId the new vendor id
	 */
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	/** The vendor email id. */
	
	
	/**
	 * Gets the vendor name.
	 *
	 * @return the vendor name
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * Sets the vendor name.
	 *
	 * @param vendorName the new vendor name
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * Gets the vendor email id.
	 *
	 * @return the vendor email id
	 */
	public String getVendorEmailId() {
		return vendorEmailId;
	}
	
	





	public String getPocName() {
		return pocName;
	}




	public void setPocName(String pocName) {
		this.pocName = pocName;
	}




	/**
	 * Sets the vendor email id.
	 *
	 * @param vendorEmailId the new vendor email id
	 */
	public void setVendorEmailId(String vendorEmailId) {
		this.vendorEmailId = vendorEmailId;
	}
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}







	@Override
	public boolean equals(Object obj) {
		if(obj instanceof VendorDetails)
		{
			System.out.println("in equal method");
			VendorDetails vend=(VendorDetails) obj;
			return (vend.getVendorName().equals(this.vendorName));
		
			
		}
		else return false;
	
	}




	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.vendorId;
	}

	public String getRefNo() {
		return refNo;
	}




	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
}
