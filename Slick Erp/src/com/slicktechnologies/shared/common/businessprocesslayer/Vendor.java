package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchCompositeAnnatonation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;
import com.slicktechnologies.shared.common.helperlayer.TaxInfo;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.relationalLayer.VendorRelation;

/**
 * The Class Vendor. Represents Vendor which is a {@link Person} or a Company from which 
 * company purchases raw material.
 */
@Entity
@SearchCompositeAnnatonation(Datatype = { ColumnDatatype.PERSONINFO }, flexFormNumber = { "00" }, 
title = { "" }, variableName = { "personInfo" },extra={"Vendor"},colspan={1},rowspan={1})

public class Vendor extends ConcreteBusinessProcess
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7116769792095830413L;

	
	protected Person pointOfContact;
	@Index
	protected Contact contact;
	@Index
	protected String vendorName;
	protected TaxInfo taxInfo;
	@Index
	protected boolean vendorStatus;
	protected SocialInformation socialInfo;
	/** Instantiates a new vendor */
	
	protected DocumentUpload uptestReport;
	
	protected ArrayList<ArticleType> articleTypeDetails;
	
	/***********************************************Relation Attributes****************************************************/
	@Index
	protected Key<Vendor>vendorRelationKey;
	@Ignore
	protected boolean nameChange=false;

	/*
	 * Rahul Added this on 24 March 2017
	 * This will be used for mapping of vendor reference Id from other System
	 */
		@Index
		protected String vendorRefNo;
		
	/** date 12.10.2018 added by komal**/
	protected int creditDays;	
	
	/** date 12.10.2018 added by komal**/
	protected String creditDaysComment;	
	
	@Index
	protected Contact secondaryAddress;
	public Vendor()
	{
		//For primary address and contact.
		contact=new Contact();
		taxInfo=new TaxInfo();
	    pointOfContact=new Person();
	    socialInfo=new SocialInformation();
	    articleTypeDetails=new ArrayList<ArticleType>();
	    vendorRefNo="";
	    creditDaysComment = "";
	    secondaryAddress = new Contact();
	}
	@Override
	public String toString()
	{
		return vendorName.toString();
	}
	
	
	public Long getLandline()
	{
		 return this.contact.getLandline();
	}
	
	public void setLandline(Long landline)
	{
		 if(landline!=null)
			 contact.setLandline(landline);
		
	}
	
	
	
	public String getVendorName() {
		return vendorName.trim();
	}

	public void setVendorName(String vendorName) {
		if(vendorName!=null)
		{
		    if(vendorName.trim().equals(this.vendorName))
		    	nameChange=true;
			this.vendorName = vendorName.trim();
		}
	}

	public void setEmail(String email)
	{
		  this.contact.setEmail(email);
	}
	
	public String getEmail()
	{
		  return this.contact.getEmail();
	}
	
	public Long getLandLine(Long landlineNo)
	{
		  return this.contact.getLandline();
		  
	}
	
	public void setLandLine(Long landline)
	{
		 if(landline!=null) 
			 this.contact.setLandline(landline);
	}
	
	public void setWebsite(String website)
	{
		  this.contact.setWebsite(website);
	}
	
	public String getWebsite()
	{
		  return this.contact.getWebsite();
	}
	
	public  Long getCellNumber1()
	{
		  return contact.getCellNo1();
	}
	
	public  void setCellNumber1(Long cellNuber1)
	{
		 if(cellNuber1!=null)  
		      contact.setCellNo1(cellNuber1);
	}
	
	public  Long getCellNumber2()
	{
		  return contact.getCellNo2();
	}
	
	public  void setCellNumber2(Long cellNuber2)
	{
		 if(cellNuber2!=null)  
		 	contact.setCellNo2(cellNuber2);
	}
	
	public  Long getFaxNumber()
	{
		  return contact.getFaxNo();
	}
	
	public  void setFaxNumber(Long faxno)
	{
		 if(faxno!=null)    
		 contact.setFaxNo(faxno);
	}
	
	public void setPrimaryAddress(Address address)
	{
		  this.contact.setAddress(address);
	}
	
	public Address getPrimaryAddress()
	{
		  return contact.getAddress();
	}
	
//	public void setSecondaryAddress(Address address)
//	{
//		  this.contact.setAddress(address);
//	}
//	
//	public Address getSecondaryAddress()
//	{
//		  return contact.getAddress();
//	}
	
	public void setSecondaryAddress(Address address)
	{
		  this.secondaryAddress.setAddress(address);
	}
	
	public Address getSecondaryAddress()
	{
		  return secondaryAddress.getAddress();
	}
	
	
	public TaxInfo getTaxInfo()
	{
		return taxInfo;
	}
	
	public void setTaxInfo(TaxInfo taxInfo)
	{
		this.taxInfo = taxInfo;
	}

	public DocumentUpload getUptestReport() {
		return uptestReport;
	}

	public void setUptestReport(DocumentUpload uptestReport) {
		this.uptestReport = uptestReport;
	}

	public Person getPointOfContact() {
		return pointOfContact;
	}



	public void setPointOfContact(Person pointOfContact) {
		this.pointOfContact = pointOfContact;
	}
	
//	public String getFirstName() {
//		if(pointOfContact!=null)
//		    return pointOfContact.getFirstName();
//		return"";
//	}
//	
//	public String getMiddleName() {
//		if(pointOfContact!=null)
//		    return pointOfContact.getMiddleName();
//		return"";
//	}
//
//	public String getLastName() {
//		if(pointOfContact!=null)
//		return pointOfContact.getLastName();
//		return"";
//	}
	
//	public void setFirstName(String firstName)
//	{
//		if(firstName!=null)
//			pointOfContact.setFirstName(firstName.trim());
//	}
//	
//	public void setMiddleName(String firstName)
//	{
//		if(firstName!=null)
//			pointOfContact.setMiddleName(firstName.trim());
//	}
//	
//	public void setLastName(String lastName)
//	{
//		if(lastName!=null)
//			pointOfContact.setLastName(lastName.trim());
//	}
	
	
	/**** vijay **********/
	public void setfullName(String fullname){
		pointOfContact.setFullname(fullname.trim());
	}
	
	public String getfullName(){
		
		return pointOfContact.getFullname().trim();
	}
	
	
	public ArrayList<ArticleType> getArticleTypeDetails() {
		return articleTypeDetails;
	}

	public void setArticleTypeDetails(ArrayList<ArticleType> articleTypeDetails) {
		ArrayList<ArticleType> article=new ArrayList<ArticleType>();
		article.addAll(articleTypeDetails);
		this.articleTypeDetails = article;
	}

	@GwtIncompatible
	@OnSave
	public void onSave()
	{
		 
		 
		super.OnSave();
		typeKey=MyUtility.getTypeKeyFromCondition(getType());
		groupKey=MyUtility.getConfigKeyFromCondition(getGroup(), ConfigTypes.VENDORGROUP.getValue());
		categoryKey=MyUtility.getConfigCategoryFromCondition(getCategory());
		if(id==null)
		{
		VendorRelation vendorRelation=new VendorRelation();
        vendorRelation.setVendorName(this.vendorName);
        vendorRelationKey =(Key<Vendor>) MyUtility.createRelationalKey(vendorRelation);
		}
		// by vijay
//		pointOfContact.fillFullName();
		
		if(nameChange==true)
		{
			VendorRelation vendorRelation=new VendorRelation();
	        vendorRelation.setVendorName(this.vendorName);
	        MyUtility.updateRelationalEntity(vendorRelation,this.vendorRelationKey);
	        nameChange=false;
		}
	}
	
	@GwtIncompatible
	@OnLoad
	 public void OnLoad()
	 {
		
		 
		 super.OnLoad();
		 String typeName=MyUtility.getTypeNameFromKey(getTypeKey());
		 if(typeName!=null)
		 {
			 if(typeName.equals("")==false)
				 setType(typeName.trim());
		 }
			String groupName=MyUtility.getConfigNameFromKey(getGroupKey());
			if(groupName!=null)
			{
				if(groupName.equals("")==false)
					setGroup(groupName.trim());
			}
			String categoryName=MyUtility.getConfigCatNameFromKey(getCategoryKey());
			if(categoryName!=null)
			{
				if(categoryName.equals("")==false)
					setCategory(categoryName);
			}
			pointOfContact.fillAllNames();
	 }
	
	



	public boolean isVendorStatus() {
		return vendorStatus;
	}

	public void setVendorStatus(boolean vendorStatus) {
		this.vendorStatus = vendorStatus;
	}
	
	

	public SocialInformation getSocialInfo() {
		return socialInfo;
	}

	public void setSocialInfo(SocialInformation socialInfo) {
		this.socialInfo = socialInfo;
	}

	@Override
	//@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Creation Date")
	public String getCreationTime() 
	{
		// TODO Auto-generated method stub
		return getCreationTime();
	}
	
	public String getVendorRefNo() {
		return vendorRefNo;
	}

	public void setVendorRefNo(String vendorRefNo) {
		this.vendorRefNo = vendorRefNo;
	}

	public int getCreditDays() {
		return creditDays;
	}

	public void setCreditDays(int creditDays) {
		this.creditDays = creditDays;
	}

	public String getCreditDaysComment() {
		return creditDaysComment;
	}

	public void setCreditDaysComment(String creditDaysComment) {
		this.creditDaysComment = creditDaysComment;
	}

	
}
