package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.PurchaseOrderServiceImplementor;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
@Entity
public class ServicePo extends ConcreteBusinessProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6630340851078137382L;

	

	@Index
	PersonInfo vinfo;
	@Index 
	String servicePoTitle;
	@Index
	Date servicePoDate;
	@Index
	Date expDeliveryDate;
	@Index
	String refNum;
	@Index
	Date refDate;
	String project;
	String category;
	String type;
	
	@Index
	String assignTo1;
	@Index
	String assignTo2;
	String description;
	
	DocumentUpload upload;
	
	String paymentMethod;
	int creditDays;
	ArrayList<PaymentTerms> paymentTermsList;
	ArrayList<SalesLineItem>items;
	ArrayList<ProductOtherCharges> productTaxes;
	ArrayList<ProductOtherCharges> productCharges;
	
	double totalAmount1;
	double totalAmount2;
	double netPayable;
	
	public ServicePo() {
		
		vinfo=new PersonInfo();
		servicePoTitle="";
		refNum="";
		project="";
		category="";
		type="";
		assignTo1="";
		assignTo2="";
		description="";
		paymentMethod="";
		upload=new DocumentUpload();
		paymentTermsList=new ArrayList<PaymentTerms>();
		items=new ArrayList<SalesLineItem>();
		productTaxes=new ArrayList<ProductOtherCharges>();
		productCharges=new ArrayList<ProductOtherCharges>();
		
		
	}
	

	
	public static List<String> getStatusList()
	{
		ArrayList<String> purchaseorder=new ArrayList<String>();
		purchaseorder.add(APPROVED);
		purchaseorder.add(CREATED);
		purchaseorder.add(REQUESTED);
		purchaseorder.add(REJECTED);
		purchaseorder.add(CLOSED);
		return purchaseorder;
		
	}
	
	
	
	
	public PersonInfo getVinfo() {
		return vinfo;
	}
	public void setVinfo(PersonInfo vinfo) {
		this.vinfo = vinfo;
	}
	public String getServicePoTitle() {
		return servicePoTitle;
	}
	public void setServicePoTitle(String servicePoTitle) {
		this.servicePoTitle = servicePoTitle;
	}
	public Date getServicePoDate() {
		return servicePoDate;
	}
	public void setServicePoDate(Date servicePoDate) {
		this.servicePoDate = servicePoDate;
	}
	public Date getExpDeliveryDate() {
		return expDeliveryDate;
	}
	public void setExpDeliveryDate(Date expDeliveryDate) {
		this.expDeliveryDate = expDeliveryDate;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public Date getRefDate() {
		return refDate;
	}
	public void setRefDate(Date refDate) {
		this.refDate = refDate;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAssignTo1() {
		return assignTo1;
	}
	public void setAssignTo1(String assignTo1) {
		this.assignTo1 = assignTo1;
	}
	public String getAssignTo2() {
		return assignTo2;
	}
	public void setAssignTo2(String assignTo2) {
		this.assignTo2 = assignTo2;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public DocumentUpload getUpload() {
		return upload;
	}
	public void setUpload(DocumentUpload upload) {
		this.upload = upload;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public int getCreditDays() {
		return creditDays;
	}
	public void setCreditDays(int creditDays) {
		this.creditDays = creditDays;
	}
	public ArrayList<PaymentTerms> getPaymentTermsList() {
		return paymentTermsList;
	}
	public void setPaymentTermsList(List<PaymentTerms> paymentTermsList) {
		ArrayList<PaymentTerms> list=new ArrayList<PaymentTerms>();
		list.addAll(paymentTermsList);
		this.paymentTermsList = list;
	}
	public ArrayList<SalesLineItem> getItems() {
		return items;
	}
	public void setItems(List<SalesLineItem> items) {
		ArrayList<SalesLineItem> list=new ArrayList<SalesLineItem>();
		list.addAll(items);
		this.items = list;
	}
	public ArrayList<ProductOtherCharges> getProductTaxes() {
		return productTaxes;
	}
	public void setProductTaxes(List<ProductOtherCharges> productTaxes) {
		ArrayList<ProductOtherCharges> list=new ArrayList<ProductOtherCharges>();
		list.addAll(productTaxes);
		this.productTaxes = list;
	}
	public ArrayList<ProductOtherCharges> getProductCharges() {
		return productCharges;
	}
	public void setProductCharges(List<ProductOtherCharges> productCharges) {
		ArrayList<ProductOtherCharges> list=new ArrayList<ProductOtherCharges>();
		list.addAll(productCharges);
		this.productCharges = list;
	}
	public double getTotalAmount1() {
		return totalAmount1;
	}
	public void setTotalAmount1(double totalAmount1) {
		this.totalAmount1 = totalAmount1;
	}
	public double getTotalAmount2() {
		return totalAmount2;
	}
	public void setTotalAmount2(double totalAmount2) {
		this.totalAmount2 = totalAmount2;
	}
	public double getNetPayable() {
		return netPayable;
	}
	public void setNetPayable(double netPayable) {
		this.netPayable = netPayable;
	}
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		if(this.getStatus().equals(APPROVED)){
			PurchaseOrderServiceImplementor implementor=new PurchaseOrderServiceImplementor();
			implementor.creatingBillingOfServicePo(this);
		}
		
	}
	@Override
	public void reactOnRejected() {
		
	}
}
