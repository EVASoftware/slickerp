package com.slicktechnologies.shared.common.businessprocesslayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.CommunicationLogServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.android.analytics.AnalyticsMenuUpdate;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.interactionlayer.InteractionType;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.sms.SMSDetails;

/**
 * Represents the Lead.
 */
@Entity
public class Lead extends ConcreteBusinessProcess
{
	/***********************************************Entity Attributes*****************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3419150382608763812L;

	public static final String SUCCESSFULL = "Successfull";

	public static final String UNSUCCESSFULL = "Unsucessfull";

	/** Priority of Lead*/
	@Index 
	protected String priority;

	/**Description Of Lead */
	protected String description;

	/** Title of lead **/
	protected String title;

	/** customer contact information against which this lead is raised **/
	@Index
	protected PersonInfo personInfo;
	
	/** To identify which type of lead. Whether from sales or service */
	@Index
	protected String leadScreenType;
	
	protected ArrayList<SalesLineItem> leadProducts;
	protected String segment; //Added by Ashwini
	
	
	// vijay
		/**Taxes Information related to the lead product items*/
		public ArrayList<ProductOtherCharges> productTaxes;
		
		
	/**
	 * rohan added this for saving netpayabe and totatlAmount 
	 * 	
	 */
		
	 /** The total Amount to be paid */
	 protected double netpayable;
		
	 /** The total amount corresponding to the total price of the items for this sales  is -1 if not exist */
	  protected double totalAmount;
	  
	  
	  /** Date 29-08-2017 added by vijay for discount for total amt **/
	  protected double discountAmt;
	  
	  /** Date 29-08-2017 added by vijay for after discount total amt **/
	  protected double finalTotalAmt;
	  
	  /** Date 31-08-2017 added by vijay for Round Off amt **/
	  protected double roundOffAmt;
	
	  
	
	/**
	 * ends here 	
	 */
	/***********************************************Relational Attributes******************************/
	protected Key<Config> priorityKey;
	protected Key<CustomerRelation>keyCustomerRelation;

	/** date 23/10/2017 added by komal for project coordinator list **/
	protected ArrayList<ProjectCoordinatorDetails> projectCoordinators ;
	
	/** Date 10-10-2017 added by komal for  Follow-up date **/ 
	@Index
	protected Date followUpDate;
	
	/**
	 * Instantiates a new lead.
	 */
	/**
	 *   Created By : Nidhi
	 * Date : 20-11-2017
	 * for  send email introductory info to Customer
	 */
	/** Document uploaded corresponding to this Sales */
	protected Vector<DocumentUpload> doc;
	/**
	 * end
	 */
	@Index
	protected String productGroup;
	
	/**
	 * @author Anil , Date : 26-12-2019
	 * as per the priora requirement adding follow up time in String (HH:MM) 
	 */
	String followUpTime;
	String contractCount;
	
	@Index
	String paymentGatewayUniqueId;
		/**25-11-2020 by Amol added this field to send the sms to previous sales person***/
	String previousSalesPerson;
	
	/**
	 * @author Vijay Date 30-12-2020
	 * Des :- added to differentiate Sales lead and Service Lead.
	 * For sales its value Sales and for service its value Service
	 */
	@Index
	String orderType;
	
	@Index
	String numberRange;
	
	String salutation;
	
	public Lead()
	{
		super();
		personInfo=new PersonInfo();
		priority="";
		description="";
		title="";
		segment = "";
		leadScreenType="";
		leadProducts=new ArrayList<SalesLineItem>();
		productTaxes = new ArrayList<ProductOtherCharges>();
		/**
		 *  nidhi
		 *  1-12-2017
		 *  for email attachment
		 */
		doc=new Vector<DocumentUpload>();
		/**
		 * end
		 */
		paymentGatewayUniqueId = "";
		orderType = "";
		numberRange = "";
		salutation = "";
	}
	
	


	public String getFollowUpTime() {
		return followUpTime;
	}




	public void setFollowUpTime(String followUpTime) {
		this.followUpTime = followUpTime;
	}




	public Date getFollowUpDate() {
		return followUpDate;
	}


	public void setFollowUpDate(Date followUpDate) {
		this.followUpDate = followUpDate;
	}


	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description)
	{
		if(description!=null)
			this.description = description.trim();
	}


	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
//	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Lead Title")
	public String getTitle() {
		return title;
	}


	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title)
	{
		if(title!=null)
			this.title = title.trim();
	}

	public String getLeadScreenType() {
		return leadScreenType;
	}

	public void setLeadScreenType(String leadScreenType) {
		if(leadScreenType!=null){
			this.leadScreenType = leadScreenType.trim();
		}
	}


	@SearchAnnotation(Datatype = ColumnDatatype.PERSONINFO, flexFormNumber = "20", title = "", variableName = "personInfo",colspan=3,extra="Customer")
	public PersonInfo getPersonInfo() {
		return personInfo;
	}


	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}


	public Key<Config> getPriorityKey() {
		return priorityKey;
	}


	public void setPriorityKey(Key<Config> priorityKey) {
		this.priorityKey = priorityKey;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessprocesslayer.ConcreteBuisnessProcess#compareTo(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	public int compareTo(SuperModel o) {
		Lead entity =(Lead) o;
		return entity.creationDate.compareTo(creationDate);
	}

	
	/**
	 * Checks if is duplicate.
	 *
	 * @param model the model
	 * @return true, if is duplicate
	 */
	public boolean isDuplicate(List<SuperModel> model) 
	{
		return false;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessprocesslayer.ConcreteBuisnessProcess#isDuplicate(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m)
	{
		return false;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 10, isFieldUpdater = false, isSortable = true, title = "Lead Priority")
	@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXCONFIG, flexFormNumber = "12", title = "Lead Priority", variableName = "olbConfigLeadPriority",extra="LEADPRIORITY")
	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		if(priority!=null)
			this.priority = priority.trim();
	}

	@SearchAnnotation(Datatype = ColumnDatatype.INTEGERBOX, flexFormNumber = "30", title = "Lead Id", variableName = "tbLeadId")
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}


	@Override
	public void setCount(int long1) {
		// TODO Auto-generated method stub
		super.setCount(long1);
	}



	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 9, isFieldUpdater = false, isSortable = true, title = "Customer Cell")
	public Long getCustomerCellNumber() {
		return personInfo.getCellNumber();
	}

	public void setCustomerCellNumber(Long val1) {
		personInfo.setCellNumber(val1);

	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Customer Name")
	public String getCustomerFullName() {
		return personInfo.getFullName();
	}

	/**
	 * Updated By: Viraj
	 * Date: 08-01-2019
	 * Description: To add customer poc 
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Customer POC")
	public String getCustomerPOC() {
		return personInfo.getPocName();
	}
	/** Ends **/


	@Override
	public String getGroup() {
		// TODO Auto-generated method stub
		return super.getGroup();
	}


	@Override
	public void setGroup(String group)
	{
		if(group!=null)
		{
		    group=group.trim();
		    super.setGroup(group);
		}
	}
   
	/*
	 * Date:14-12-2018
	 * @author Ashwini
	 */
	public String getSegment() {
		return segment;
	}


	public void setSegment( String segment) {
		this.segment = segment;
	}
   
   /*
    * end by Ashwini
    */
	@Override
	public String getCategory() {
		// TODO Auto-generated method stub
		return super.getCategory();
	}
	

	@Override
	public void setCategory(String category) {
		// TODO Auto-generated method stub
		super.setCategory(category);
	}


	@Override
	@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXCONFIG, flexFormNumber = "02", title = "Lead Type", variableName = "olbcLeadType",extra="LEADTYPE")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo =5, isFieldUpdater = false, isSortable = true, title = "Lead Type")

	public String getType() 
	{
		return super.getType();
	}

	public void setType(String Type)
	{
		if(type!=null)
		{
			type=type.trim();
			super.setType(Type);
		}
	}
	
	public List<SalesLineItem> getLeadProducts() {
		return leadProducts;
	}

	public void setLeadProducts(List<SalesLineItem> leadProducts) {
		if(leadProducts.size()!=0){
			ArrayList<SalesLineItem> arrOfProducts=new ArrayList<SalesLineItem>();
			arrOfProducts.addAll(leadProducts);
			this.leadProducts = arrOfProducts;
		}
	}
    
	public String getSalutation()
	{
		return salutation;
	}
	
	public void setSalutation(String salutation)
	{
		 this.salutation=salutation;
	}
	

	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void OnSave()
	{
		Logger logger = Logger.getLogger("NameOfYourLogger");
		super.OnSave();
		
		priorityKey=MyUtility.getConfigKeyFromCondition(getPriority(),ConfigTypes.LEADPRIORITY.getValue());
		statusKey=MyUtility.getConfigKeyFromCondition(getStatus(),ConfigTypes.LEADSTATUS.getValue());

		// Create Querry for getting keys from Customer Relation
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setIntValue(personInfo.getCount());
		filter.setQuerryString("customerCount");
		querry.getFilters().add(filter);
		querry.setQuerryObject(new CustomerRelation());
		if(id==null)
		{
			keyCustomerRelation=(Key<CustomerRelation>) MyUtility.getRelationalKeyFromCondition(querry);
			
			typeKey=MyUtility.getTypeKeyFromCondition(getType());
			
			groupKey=MyUtility.getConfigKeyFromCondition(group, ConfigTypes.LEADGROUP.getValue());
			
			categoryKey=MyUtility.getConfigCategoryFromCondition(getCategory());	
		
			/** Date 14-09-2017 added by vijay for lead creation send email requirement Intech ****/
			SendEmail();
		} 
		
		/*
		 * Date:15-12-2018
		 * @author Ashwini
		 * Des:To get customer category in lead
		 */
//		Lead lead = new Lead();
//		Customer cust = null ;
//		cust = ofy().load().type(Customer.class).filter("companyId", getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
//		lead.setCustomerType(cust.getCategory());
//		System.out.println("customer Category::"+cust.getCategory());
//		
//		System.out.println("cust category in lead::"+lead.getCustomerType());
		
		/**Added by sheetal:30-12-2021
		   Des: To get salutation in lead**/
		Lead lead = new Lead();
		Customer cust = null ;
		if(this.getSalutation()==null || this.getSalutation().equals("")){
		cust = ofy().load().type(Customer.class).filter("companyId", getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
	    lead.setSalutation(cust.getSalutation());
	    logger.log(Level.SEVERE,"salutation"+lead.getSalutation());
		}
	}


	/** Date 14-09-2017 added by vijay for lead creation send email requirement Intech ****/

	@GwtIncompatible
	private void SendEmail() {
		
		 Logger logger = Logger.getLogger("NameOfYourLogger");

		ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","Lead").filter("status",true).filter("companyId", this.getCompanyId()).first().now();
		ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", this.getCompanyId()).filter("processName","Lead").first().now();				
		
		boolean prossconfigSMSflag = false;
		if(processName!=null){	
		if(processconfig!=null){
			if(processconfig.isConfigStatus()){
				for(int l=0;l<processconfig.getProcessList().size();l++){
					if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("Lead") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("LeadSaveEmail") && processconfig.getProcessList().get(l).isStatus()==true){
						prossconfigSMSflag=true;
					}
				}
		   }
		}
		}
		logger.log(Level.SEVERE,"Process config=="+processconfig);
		if(prossconfigSMSflag){
			
		Customer customer = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getPersonInfo().getCount()).first().now();
		
		if(customer!=null){
			logger.log(Level.SEVERE,"customer --"+customer.getEmail());
			ArrayList<String> toEmailList = new ArrayList<String>();
			toEmailList.add(customer.getEmail());
			String fromEmail="";
			Company comp=ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
			if(comp!=null){
				if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
					fromEmail=comp.getEmail();
				}
			String productName="";
			for(int i=0;i<this.getLeadProducts().size();i++){
				productName +=this.getLeadProducts().get(i).getProductName()+",";
			}
			
			String mailSub=""; //thank you for reaching out to us. you have enquired about product name
			String mailMsg="";
				mailSub="Enquiry";
				mailMsg="Dear Sir/Madam"+","+"\n"+"\n"+"\n"
								+"Thank you for reaching out to us. you have enquired about "+productName +" incase any query please contact us. \n"+"\n"+"\n"
								+"Company :-"+comp.getBusinessUnitName()+"\n"
								+"Contact No :-"+comp.getCellNumber1()+"\n";
			
			Email obj=new Email();
			obj.sendEmail(mailSub, mailMsg, toEmailList, new ArrayList<String>(), fromEmail);
			logger.log(Level.SEVERE,"EMAIL Sending --"+customer.getEmail());

			}
			
		}
		
	  }
	}
	
	
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{
		super.OnLoad();
		if(priorityKey!=null)
		{
			String priorityName=MyUtility.getConfigNameFromKey(priorityKey);
			if(priorityName.equals("")==false)
				setPriority(priorityName);
		}

		if(statusKey!=null)
		{
			String statusName=MyUtility.getConfigNameFromKey(statusKey);
			if(statusName.equals("")==false)
				setStatus(statusName);	
		}


		if(keyCustomerRelation!=null)
		{
			CustomerRelation relation=MyUtility.getCustomerRelationFromKey(keyCustomerRelation);
			if(relation!=null||personInfo!=null)
			{
				if(relation.getCellNumber()!=null)
					this.personInfo.setCellNumber(relation.getCellNumber());
				if(relation.getCustomerCount()!=-1)
					this.personInfo.setCount(relation.getCustomerCount());
				if(relation.getFullName().equals("")==false)
					this.personInfo.setFullName(relation.getFullName());
			}

		}
		
		if(getTypeKey()!=null)
		{
			String typeName=MyUtility.getTypeNameFromKey(getTypeKey());
			if(typeName.equals("")==false)
				setType(typeName);
		}
		
		if(getGroupKey()!=null)
		{
			String groupName=MyUtility.getConfigNameFromKey(getGroupKey());
			if(groupName.equals("")==false)
				setGroup(groupName);
		}
		
		if(getCategoryKey()!=null)
		{
			String categoryName=MyUtility.getConfigCatNameFromKey(getCategoryKey());
			if(categoryName.equals("")==false)
				setCategory(categoryName);
		}
		

	}


	public ArrayList<ProductOtherCharges> getProductTaxes() {
		return productTaxes;
	}


	public void setProductTaxes(ArrayList<ProductOtherCharges> productTaxes) {
		ArrayList<ProductOtherCharges> prodTaxesArr=new ArrayList<ProductOtherCharges>();
		prodTaxesArr.addAll(productTaxes);
		this.productTaxes = prodTaxesArr;
	}


	public double getNetpayable() {
		return netpayable;
	}


	public void setNetpayable(double netpayable) {
		this.netpayable = netpayable;
	}


	public double getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/** Date 31-08-2017 added by vijay for getter and setter  **/

	public double getDiscountAmt() {
		return discountAmt;
	}


	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}


	public double getFinalTotalAmt() {
		return finalTotalAmt;
	}


	public void setFinalTotalAmt(double finalTotalAmt) {
		this.finalTotalAmt = finalTotalAmt;
	}


	public double getRoundOffAmt() {
		return roundOffAmt;
	}


	public void setRoundOffAmt(double roundOffAmt) {
		this.roundOffAmt = roundOffAmt;
	}
    
	/** date 23/10/2017 added by komal for project coordinator list **/

	public ArrayList<ProjectCoordinatorDetails> getProjectCoordinators() {
		return projectCoordinators;
	}
	

	public String getProductGroup() {
		return productGroup;
	}


	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}


	public String getContractCount() {
		return contractCount;
	}




	public void setContractCount(String contractCount) {
		this.contractCount = contractCount;
	}




	public String getPaymentGatewayUniqueId() {
		return paymentGatewayUniqueId;
	}




	public void setPaymentGatewayUniqueId(String paymentGatewayUniqueId) {
		this.paymentGatewayUniqueId = paymentGatewayUniqueId;
	}




	public void setProjectCoordinators(ArrayList<ProjectCoordinatorDetails> projectCoordinators) 
	{
		ArrayList<ProjectCoordinatorDetails> projectCoordinatorsArr = new ArrayList<ProjectCoordinatorDetails>();
		projectCoordinatorsArr.addAll(projectCoordinators);
		this.projectCoordinators = projectCoordinatorsArr;
	}
	/**
	 *  Created By : Nidhi
	 *  Date : 20-11-2017
	 *  
	 */
	/**
	 * Gets the doc.
	 * @return the doc
	 */
	public DocumentUpload getDocument() 
	{
		if(doc.size()!=0)
		  return doc.get(0);
		return null;
	}

	/**
	 * Sets the doc.
	 * @param doc the new doc
	 */
	public void setDocument(DocumentUpload doc)
	{
		this.doc=new Vector<DocumentUpload>();
		if(doc!=null)
		   this.doc.add(doc);
		
	}
	/**
	 * end
	 */
	@OnSave
	@GwtIncompatible
	private void saveFollowUp(){
		Logger logger = Logger.getLogger("lead ");
		
		if(id != null){
		Lead lead = ofy().load().type(Lead.class).filter("companyId", this.getCompanyId()).filter("count", this.getCount()).first().now();
		if(lead != null && !(lead.getStatus().equals(Lead.UNSUCCESSFULL) || lead.getStatus().equals(AppConstants.UNSUCCESSFUL))){
		if(this.getStatus().equals(Lead.UNSUCCESSFULL) || this.getStatus().equals(AppConstants.UNSUCCESSFUL)){
			SimpleDateFormat hours = new SimpleDateFormat("HH:mm:ss");
			hours.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			Date date = null;
			try {
				date = sdf.parse(sdf.format(new Date()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "follwupdate :" + date );
			this.setFollowUpDate(date);
		   List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",this.getCount()).list();
		   ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
			if(interactionTypeList.size()!=0){
				communicationList.addAll(interactionTypeList);
			}
		   InteractionType interactionType= AnalyticsMenuUpdate.getCommunicationLog("Service", "Lead", this.getCount(), null, "Status updated to Unsuccessful.", sdf.format(new Date()), this.getPersonInfo(),null, null, this.getBranch(), this.getEmployee(),this,"",hours.format(new Date()));
		   communicationList.add(interactionType);
		   ArrayList<SuperModel> interactionlist = new ArrayList<SuperModel>();
			for(int i=0;i<communicationList.size();i++){
				
				if(communicationList.get(i).isCommunicationlogFlag()){
					communicationList.get(i).setCommunicationlogFlag(false);
					interactionlist.add(communicationList.get(i));
				}
			}
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(interactionlist);
		}
		}
		if(lead != null && !(lead.getStatus().equals(Lead.SUCCESSFULL) || lead.getStatus().equals(AppConstants.Successful) 
				|| lead.getStatus().equals(AppConstants.Sucessful))){
			if(this.getStatus().equals(Lead.SUCCESSFULL) || this.getStatus().equals(AppConstants.Successful)
					|| lead.getStatus().equals(AppConstants.Sucessful)){
				SimpleDateFormat hours = new SimpleDateFormat("HH:mm:ss");
				hours.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				Date date = null;
				try {
					date = sdf.parse(sdf.format(new Date()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.setFollowUpDate(date);
			   List<InteractionType> interactionTypeList=ofy().load().type(InteractionType.class).filter("interactionDocumentName","Lead").filter("interactionDocumentId",this.getCount()).list();
				ArrayList<InteractionType> communicationList=new ArrayList<InteractionType>();
				if(interactionTypeList.size()!=0){
					communicationList.addAll(interactionTypeList);
				}
			   InteractionType interactionType= AnalyticsMenuUpdate.getCommunicationLog("Service", "Lead", this.getCount(), null, "Status updated to Successful.", sdf.format(new Date()), this.getPersonInfo(),null, null, this.getBranch(), this.getEmployee(),this,"",hours.format(new Date()));
			   communicationList.add(interactionType);
			   ArrayList<SuperModel> interactionlist = new ArrayList<SuperModel>();
				for(int i=0;i<communicationList.size();i++){
					
					if(communicationList.get(i).isCommunicationlogFlag()){
						communicationList.get(i).setCommunicationlogFlag(false);
						interactionlist.add(communicationList.get(i));
					}
				}
				GenricServiceImpl impl = new GenricServiceImpl();
				impl.save(interactionlist);
			}
			}
		}
	}
	
	
	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getFollowUpDate()!=null){
			this.setFollowUpDate(dateUtility.setTimeMidOftheDayToDate(this.getFollowUpDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */
	@OnSave
	@GwtIncompatible
	public void sendSMSToSalesPerson(){
		try {
			
			 if(id==null){
				 Logger logger = Logger.getLogger("NameOfYourLogger");
				 smsNotificationToSalesPerson("Lead Assign",this.getEmployee());
				 
				 /**Added by sheetal : 15-03-2022
			        * Des : For Sending SMS automatically when new lead is created**/
				 SMSDetails smsdetails = new SMSDetails();
				 smsdetails.setModel(this);
				 SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", "New Lead").filter("status",true).first().now();
				 if(smsEntity!=null) {
				 smsdetails.setSmsTemplate(smsEntity);
				 smsdetails.setEntityName("Lead");
				 
				 //Ashwini Patil Date:10-05-2022 Sms were getting send to customer. It should go to salesperson
				 Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", this.getEmployee()).first().now();				
				 String mobileNo=Long.toString(emp.getCellNumber1());
//				 String mobileNo=Long.toString(this.getPersonInfo().getCellNumber());
				 smsdetails.setMobileNumber(mobileNo);
				 smsdetails.setCommunicationChannel(AppConstants.SMS);
				 smsdetails.setModuleName(AppConstants.SERVICEMODULE);
				 smsdetails.setDocumentName(AppConstants.LEAD);
				 SmsServiceImpl sendImpl = new SmsServiceImpl();
				 String smsmsg = sendImpl.validateAndSendSMS(smsdetails);
				 logger.log(Level.SEVERE," inside validateAndSendSMS"+smsmsg);
				 }
				 /**end**/
			 }else{
				 if(previousSalesPerson!=null&&!previousSalesPerson.equals("")){
					 if(!previousSalesPerson.equals(getEmployee())){
						 smsNotificationToSalesPerson("Lead Transfer",this.getEmployee());
						 smsNotificationToSalesPerson("Lead Transfer Notification",this.getPreviousSalesPerson());
					 }
				 }
			 }
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	
	@GwtIncompatible
	private void smsNotificationToSalesPerson(String smsEvent,String employeeName){
		 Logger logger = Logger.getLogger("NameOfYourLogger");
		 logger.log(Level.SEVERE," inside sms method");
//		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",this.getCompanyId()).filter("status",true).first().now();
//
//		if(smsconfig!=null){
//			
//			String accountauthkey = smsconfig.getAuthToken();
//			String accSenderId =smsconfig.getAccountSID(); 
//			String accRoute = smsconfig.getPassword();
//			System.out.println("in sms config");
//			 logger.log(Level.SEVERE,"in sms config");

			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", smsEvent).filter("status",true).first().now();
			if(smsEntity!=null){
				
				long salesPersonmobileNo=0;
				String actualMsg="";
				try {
					
				String templatemsgwithbraces = smsEntity.getMessage();
//				Company comp = ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
				logger.log(Level.SEVERE,"in sms template msg");
				
				String fullname = this.getPersonInfo().getFullName();
				System.out.println(" customerName ="+this.getPersonInfo().getCellNumber());
				String cellNo =String.valueOf(this.getPersonInfo().getCellNumber());
				
				String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
				String cellNumber = customerName.replace("{cellNumber}",cellNo);
				actualMsg = cellNumber.replace("{leadId}", this.getCount()+"");
			
				Employee employee=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId()).filter("fullname", employeeName).first().now(); 
					
				System.out.println(" msg =="+actualMsg);
				logger.log(Level.SEVERE," Message =="+actualMsg);
				salesPersonmobileNo = employee.getCellNumber1();
				logger.log(Level.SEVERE," SalesPerson Mob No =="+salesPersonmobileNo);
				} catch (Exception e) {
					// TODO: handle exception
				}
				
//				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				smsimpl.sendSmsToClient(actualMsg, salesPersonmobileNo, accSenderId, accountauthkey, accRoute,this.getCompanyId());
				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				SmsServiceImpl smsimpl = new SmsServiceImpl();
				smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.LEAD,smsEntity.getEvent(),this.getCompanyId(),actualMsg,salesPersonmobileNo,false);
				logger.log(Level.SEVERE,"after sendMessage method");
				
				
				
			}
//		}
		
	}



	public String getPreviousSalesPerson() {
		return previousSalesPerson;
	}

	public void setPreviousSalesPerson(String previousSalesPerson) {
		this.previousSalesPerson = previousSalesPerson;
	}




	public String getOrderType() {
		return orderType;
	}


	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return count+"";
	}
	


	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}
	
}
