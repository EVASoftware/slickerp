package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class LetterOfIntent extends PurchaseDetails implements ApprovableProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1857653448436474276L;

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Index
	protected int reqforQuotationID;
	@Index
	protected Date rfqDate;
	@Index
	protected Date LoiDate;
	protected Date expectedClosureDate;
	protected Date responseDate;
	@Index
	protected String refOrderno;
//	@Index
//	protected String employeePurEngg;
	@Index
	protected String loiType;
	@Index
	protected String loiGroup;
	@Index
	protected ArrayList<ProductDetails> productinfo;
	@Index
	protected ArrayList<VendorDetails> vendorinfo;
	@Index
	protected String status;
	protected String description;
	
	public static final String CREATED="Created",CANCELLED="Cancelled",REJECTED="Rejected",APPROVED="Approved",REQUESTED="Requested",CLOSED="Closed";
	
	
	/**
	 * Pur Engg assign pr to its team.
	 * Date :29-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	@Index
	protected String assignTo1;
	@Index
	protected String assignTo2;
	
	/**
	 * END
	 */
	
	
	
/*********************************Constructor************************************************/
	
	
	public LetterOfIntent()
	{
		super();
		description="";
		assignTo1="";
		assignTo2="";
	}
	
	
	
	
	/*****************************Getters and Setters***************************************/
	
	public String getAssignTo1() {
		return assignTo1;
	}
	public void setAssignTo1(String assignTo1) {
		this.assignTo1 = assignTo1;
	}
	public String getAssignTo2() {
		return assignTo2;
	}
	public void setAssignTo2(String assignTo2) {
		this.assignTo2 = assignTo2;
	}

	
	public Date getRfqDate() {
		return rfqDate;
	}

	public void setRfqDate(Date rfqDate) {
		this.rfqDate = rfqDate;
	}

	public Date getLoiDate() {
		return LoiDate;
	}

	public void setLoiDate(Date loiDate) {
		LoiDate = loiDate;
	}


	public Date getExpectedClosureDate() {
		return expectedClosureDate;
	}

	public void setExpectedClosureDate(Date expectedClosureDate) {
		this.expectedClosureDate = expectedClosureDate;
	}

	public Date getResponseDate() {
		return responseDate;
	}


	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getRefOrderNO() {
		return refOrderno;
	}


	public void setRefOrderNO(String refOrderno) {
		this.refOrderno = refOrderno;
	}

//	public String getEmployeePurEngg() {
//		return employeePurEngg;
//	}
//
//	public void setEmployeePurEngg(String employeePurEngg) {
//		this.employeePurEngg = employeePurEngg;
//	}




	public String getLoiType() {
		return loiType;
	}

	public void setLoiType(String loiType) {
		this.loiType = loiType;
	}
	public String getLoiGroup() {
		return loiGroup;
	}

	public void setLoiGroup(String loiGroup) {
		this.loiGroup = loiGroup;
	}

	public ArrayList<ProductDetails> getProductinfo() {
		return productinfo;
	}


	public void setProductinfo(List<ProductDetails> productinfo) {
		if(productinfo!=null)
		this.productinfo = new ArrayList<ProductDetails>();
		this.productinfo.addAll(productinfo);
	}
	public ArrayList<VendorDetails> getVendorinfo() {
		return vendorinfo;
	}

	public void setVendorinfo(List<VendorDetails> vendorinfo) {
		if(vendorinfo!=null)
		this.vendorinfo = new ArrayList<VendorDetails>();
		this.vendorinfo.addAll(vendorinfo);
	}

	
		public int getReqforQuotationID() {
		return reqforQuotationID;
	}

	public void setReqforQuotationID(int reqforQuotationID) {
		this.reqforQuotationID = reqforQuotationID;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description!=null){
			this.description = description.trim();
		}
	}




	@Override
	public String getStatus() {
		return status;
	}


	@Override
	public String getApproverName() {
		return this.approverName.toString();
	}


	@Override
	public String getBranch() {
		return this.branch;
	}


	@Override
	public void setStatus(String status) {
		this.status=status;
		
	}

	public static List<String> getStatusList()
	{
		ArrayList<String> letterofintent=new ArrayList<String>();
		letterofintent.add(APPROVED);
		letterofintent.add(CREATED);
		letterofintent.add(REQUESTED);
		letterofintent.add(REJECTED);
		letterofintent.add(CLOSED);
		
		
		return letterofintent;
		
	}




	@Override
	public void reactOnApproval() {
		// TODO Auto-generated method stub
		
	}
	

}
