package com.slicktechnologies.shared.common.businessprocesslayer;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
/**
 * The Class Action Represents any Action corresponding to the a {@link Activity}.
 */

@Embed
public class Action extends ConcreteBusinessProcess implements Serializable
{
	/***********************************************Entity Attributes****************************************************/
	/** The Constant serialVersionUID. */

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2225850108113492076L;

	/**  The description of the action. */
	protected String description;
	
    /** The action due date. */
	protected Date actionDueDate;
	
	/** What action Performed */
	protected String actionTaken;
	
	/**
	 * Instantiates a new Action
	 */
	public Action() 
	{
		super();
		description="";
		actionTaken="";
	}

   /**
    * Gets the description.
    *
    * @return the description
    */
   public String getDescription()
   {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description)
	{
		if(description!=null)
			this.description = description.trim();
	}

	/**
	 * Gets the activity due date.
	 *
	 * @return the activity due date
	 */
	public Date getActivityDueDate() 
	{
		return actionDueDate;
	}

	/**
	 * Sets the activity due date.
	 *
	 * @param activityDueDate the new activity due date
	 */
	public void setActivityDueDate(Date activityDueDate) 
	{
		this.actionDueDate = activityDueDate;
	}

	/**
	 * Gets the action taken.
	 *
	 * @return the action taken
	 */
	public String getActionTaken() 
	{
		return actionTaken;
	}

	/**
	 * Sets the action taken.
	 *
	 * @param actionTaken the new action taken
	 */
	public void setActionTaken(String actionTaken) 
	{
		if(actionTaken!=null)
			this.actionTaken = actionTaken.trim();
	}	
}
