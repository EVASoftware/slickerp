package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;

@Embed
public class ProductDetailsPO extends SuperModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6530021185055457478L;

	protected double total;
	protected double tax;
	protected double discount;
	protected double vat;
	private ServiceProduct serviceProduct;
	private ItemProduct itemProduct;
	@Index
	ProductDetails productdetails;
	
	
	String serviceTaxEdit;
	String vatTaxEdit;
	double discountAmt; 
	
	/*
	 *Added by Rahul Verma on 28 March 2017
	 *warehouseDeliveryAddress: this will be used for multiple delivery address. It will be of warehouse address.
	 */
	String productRefId;
	String warehouseDeliveryAddress;
	
	
	/**
	 * Date :10-07-2018 BY ANIL
	 */
	
	Tax salesTax1;
	Tax salesTax2;
	double salesPrice;
	
	/**
	 * @author Anil
	 * @since 20-07-2020
	 * Storing product wise other charges from sales order
	 * for PTSPL raised by rahul tiwari
	 */
	double otherCharges;


	/********************************* Constructor **************************************************/
	/**
	 * nidhi 21-08-2018 for map serial number details
	 */
		@EmbedMap
	  @Serialize
	  protected HashMap<Integer, ArrayList<ProductSerialNoMapping>>  proSerialNoDetails;
	public ProductDetailsPO() {
		super();
		productdetails = new ProductDetails();
		productRefId="";
		
		salesTax1=new Tax();
		salesTax2=new Tax();
	}

	/********************************* Getters and Setters **************************************************/

	/**
	 * Date : 10-07-2018 BY ANIL
	 */
	public Tax getSalesTax1() {
		return salesTax1;
	}

	public void setSalesTax1(Tax salesTax1) {
		this.salesTax1 = salesTax1;
	}

	public Tax getSalesTax2() {
		return salesTax2;
	}

	public void setSalesTax2(Tax salesTax2) {
		this.salesTax2 = salesTax2;
	}

	public double getSalesPrice() {
		return salesPrice;
	}

	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}
	
	
	public Tax getPurchaseTax1(){
		if(itemProduct!=null){
			if(itemProduct.getPurchaseTax1()!=null){
				return itemProduct.getPurchaseTax1();
			}else{
				return null;
			}
		}else{
			return null;
		}
		
		
	}
	
	public void setPurchaseTax1(Tax tax){
		itemProduct.setPurchaseTax1(tax);
	}
	
	public Tax getPurchaseTax2(){
		if(itemProduct!=null){
			if(itemProduct.getPurchaseTax2()!=null)
				return itemProduct.getPurchaseTax2();
			else{
				return null;
			}
		}else{
			return null;
		}
		
	}
	
	public void setPurchaseTax2(Tax tax){
		itemProduct.setPurchaseTax2(tax);
	}
	/**
	 * End
	 */
	
	
	
	public SuperProduct getPrduct() {

		if (serviceProduct == null)
			return itemProduct;

		else if (itemProduct == null)
			return serviceProduct;
		else {
			return null;
		}
	}

	/**
	 * Sets the prduct.
	 *
	 * @param product
	 *            the new prduct
	 */
	public void setPrduct(SuperProduct product) {
		if (product instanceof ServiceProduct)
			serviceProduct = (ServiceProduct) product;
		else if (product instanceof ItemProduct)
			itemProduct = (ItemProduct) product;
		System.out.println("Service Product IS " + serviceProduct);
		System.out.println("Item Product IS " + itemProduct);
	}
	
	
	
	public Tax getServiceTax() {
		if(serviceProduct!=null)
			return serviceProduct.getServiceTax();
		else
			return itemProduct.getServiceTax();
	}

	/**
	 * Sets the service tax.
	 *
	 * @param serviceTax the new service tax
	 */
	public void setServiceTax(Tax serviceTax) {
		if(serviceProduct!=null)
			serviceProduct.setServiceTax(serviceTax);
		else
			itemProduct.setServiceTax(serviceTax);
	}
	
	
	/**
	 * Gets the vat tax.
	 *
	 * @return the vat tax
	 */
	public Tax getVatTax() {
		if(itemProduct!=null)
			return itemProduct.getVatTax();
		else
			return serviceProduct.getVatTax();
	}
	
	/**
	 * Sets the vat tax.
	 *
	 * @param vatTaxEntity the new vat tax
	 */
	public void setVatTax(Tax vatTaxEntity) {
		if(itemProduct!=null)
			itemProduct.setVatTax(vatTaxEntity);
		else
			serviceProduct.setVatTax(vatTaxEntity);
	}
	

	public String getUnitOfmeasurement() {
		return productdetails.getUnitOfmeasurement();
	}

	public void setUnitOfmeasurement(String unitOfmeasurement) {
		productdetails.setUnitOfmeasurement(unitOfmeasurement);
	}

	public double getProdPrice() {
		return productdetails.getProdPrice();
	}

	public void setProdPrice(double prodPrice) {
		productdetails.setProdPrice(prodPrice);
	}

	public Date getProdDate() {
		return productdetails.getProdDate();
	}

	public void setProdDate(Date prodDate) {
		productdetails.setProdDate(prodDate);
	}

	public double getProductQuantity() {
		return productdetails.getProductQuantity();
	}

	public void setProductQuantity(double productQuantity) {
		productdetails.setProductQuantity(productQuantity);
	}

	public String getProductCode() {
		return productdetails.getProductCode();
	}

	public void setProductCode(String productCode) {
		productdetails.setProductCode(productCode);
	}

	public String getProductName() {
		return productdetails.getProductName();
	}

	public void setProductName(String productName) {
		productdetails.setProductName(productName);
	}

	public String getProductCategory() {
		return productdetails.getProductCategory();
	}

	public void setProductCategory(String productCategory) {
		productdetails.setProductCategory(productCategory);
	}

	public int getProductID() {
		return productdetails.getProductID();
	}

	public void setProductID(int productID) {
		productdetails.setProductID(productID);
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(double vat) {
		this.discount = vat;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Double getTax() {
		return tax;
	}
	
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public Double getVat() {
		return vat;
	}

	public void setVat(double vat) {
		this.vat = vat;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

	public double getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}

	public String getServiceTaxEdit() {
		return serviceTaxEdit;
	}

	public void setServiceTaxEdit(String serviceTaxEdit) {
		this.serviceTaxEdit = serviceTaxEdit;
	}

	public String getVatTaxEdit() {
		return vatTaxEdit;
	}

	public void setVatTaxEdit(String vatTaxEdit) {
		this.vatTaxEdit = vatTaxEdit;
	}

	public String getWarehouseName() {
		return productdetails.getWarehouseName();
	}


	public void setWarehouseName(String warehouseName) {
		this.productdetails.setWarehouseName(warehouseName);
	}
	
	
	public double getAvailableStock() {
		return productdetails.getAvailableStock();
	}
	
	
	public void setAvailableStock(double availableStock) {
		this.productdetails.setAvailableStock(availableStock);
	}
	
	public String getProductRefId() {
		return productRefId;
	}

	public void setProductRefId(String productRefId) {
		this.productRefId = productRefId;
	}

//	public String getWarehouseDeliveryAddress() {
//		return warehouseDeliveryAddress;
//	}
//
//	public void setWarehouseDeliveryAddress(String warehouseDeliveryAddress) {
//		this.warehouseDeliveryAddress = warehouseDeliveryAddress;
//	}
	
	
	/**
	 * Date 21 jun 2017 added by vijay for Quick Sales Order
	 * @return
	 */
	public double getItemProductAvailableQty(){
		return itemProduct.getAvailableQuantity();
	}
	
	public void setItemProductAvailableQty(double availableQty){
		this.itemProduct.setAvailableQuantity(availableQty);
	}
	
	public String getItemProductWarehouseName(){
		return itemProduct.getWarehouse();
	}
	
	public void setItemProductWarehouoseName(String WarehouseName){
		this.itemProduct.setWarehouse(WarehouseName);
	}
	
	public String getItemProductStrorageLocation(){
		return itemProduct.getStorageLocation();
	}
	
	public void setItemProductStorageLocation(String storageLocationName){
		this.itemProduct.setStorageLocation(storageLocationName);
	}
	
	public String getItemProductStorageBin(){
		return itemProduct.getStorageBin();
	}
	
	public void setItemStorageBin(String storageBin){
		this.itemProduct.setStorageBin(storageBin);
	}
	
	/**
	 * ends here
	 */
	
	
	public String getWarehouseDeliveryAddress() {
		return warehouseDeliveryAddress;
	}

	public void setWarehouseDeliveryAddress(String warehouseDeliveryAddress) {
		this.warehouseDeliveryAddress = warehouseDeliveryAddress;
	}
	
	public String getWarehouseAddress(){
		return productdetails.getWarehouseAddress();
	}
	
	public void setWarehouseAddress(String warehouseAddress){
		this.productdetails.setWarehouseAddress(warehouseAddress);
	}
	
	public HashMap<Integer, ArrayList<ProductSerialNoMapping>> getProSerialNoDetails() {
		return proSerialNoDetails;
	}
	public void setProSerialNoDetails(
			HashMap<Integer, ArrayList<ProductSerialNoMapping>> proSerialNoDetails) {
		this.proSerialNoDetails = proSerialNoDetails;
	}

	public double getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}
	
	
}
