package com.slicktechnologies.shared.common.businessprocesslayer;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;

@Embed
public class PRproducts extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4204154132007131763L;

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	PersonInfo personinfo;
	ProductInfo info;
	protected int Quantity;
	
	
	public PRproducts() {
		personinfo=new PersonInfo();
		info=new ProductInfo();
	}
	
	
	public Long getCellNumber() {
		return personinfo.getCellNumber();
	}







	/**
	 * Sets the cell number.
	 *
	 * @param cellNumber the new cell number
	 */
	public void setCellNumber(Long cellNumber) {
		personinfo.setCellNumber(cellNumber);
	}




	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return personinfo.getFullName();
	}




	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		personinfo.setFullName(fullName);
	}




	
	public int getVendorID() {
		return personinfo.getCount();
	}




	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#setCount(int)
	 */
	public void setCount(int id) {
		personinfo.setCount(id);
	}




	public String getEmail() {
		return personinfo.getEmail();
	}





	public void setEmail(String email) {
		personinfo.setEmail(email);
	}





	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	public int getProdID() {
		return info.getProdID();
	}




	/**
	 * Sets the prod id.
	 *
	 * @param prodID the new prod id
	 */
	public void setProdID(int prodID) {
		info.setProdID(prodID);
	}




	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return info.getProductCode();
	}




	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		info.setProductCode(productCode);
	}




	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return info.getProductName();
	}




	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		info.setProductName(productName);
	}




	/**
	 * Gets the product category.
	 *
	 * @return the product category
	 */
	public String getProductCategory() {
		return info.getProductCategory();
	}




	/**
	 * Sets the product category.
	 *
	 * @param productCategory the new product category
	 */
	public void setProductCategory(String productCategory) {
		info.setProductCategory(productCategory);
	}




	/**
	 * Gets the product price.
	 *
	 * @return the product price
	 */
	public double getProductPrice() {
		return info.getProductPrice();
	}




	/**
	 * Sets the product price.
	 *
	 * @param productPrice the new product price
	 */
	public void setProductPrice(double productPrice) {
		info.setProductPrice(productPrice);
	}

	public String getUnitofMeasure() {
		return info.getUnitofMeasure();
	}





	public void setUnitofMeasure(String unitofMeasure) {
		info.setUnitofMeasure(unitofMeasure);
	}


	public int getQuantity() {
		return Quantity;
	}


	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	
	

}
