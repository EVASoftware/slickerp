package com.slicktechnologies.shared.common.businessprocesslayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Entity
public class CompanyProfileConfiguration extends SuperModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7365411936968453852L;
	@Index
	protected String leadCategory;
	protected DocumentUpload compProfileConfig;
	@Index
	protected boolean status;
	
	
	public String getLeadCategory() {
		return leadCategory;
	}
	public void setLeadCategory(String leadCategory) {
		this.leadCategory = leadCategory;
	}
	public DocumentUpload getCompProfileConfig() {
		return compProfileConfig;
	}
	public void setCompProfileConfig(DocumentUpload compProfileConfig) {
		this.compProfileConfig = compProfileConfig;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
