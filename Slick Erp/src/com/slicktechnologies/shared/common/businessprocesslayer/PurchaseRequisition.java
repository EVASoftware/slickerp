package com.slicktechnologies.shared.common.businessprocesslayer;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

@Entity
public class PurchaseRequisition extends SuperModel implements ApprovableProcess
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3262080891104631433L;
	public static final String CREATED="Created",CANCELLED="Cancelled",REJECTED="Rejected",APPROVED="Approved",REQUESTED="Requested",CLOSE="Close",PROCESSED="Processed";

	
	@Index
	protected PurchaseDetails purchasedetails;
	@Index
	protected String prTitle;
	@Index
	protected String prCategory;
	@Index
	protected String prType;
	@Index
	protected String description;
	@Index
	protected ArrayList<ProductDetails> prproduct;
	@Index
	protected String prGroup;
	@Index
	protected Date expectedDeliveryDate;
	@Index
	protected Date RefOrderDate;
	protected DocumentUpload uptestReport;
	protected String header;
	protected String footer;
	protected ArrayList<VendorDetails> vendlist;
	
	/**
	 * This filed stores remark given at the time of approval.
	 * Date : 09-09-2016 By Anil
	 * Release : 31 Aug 2016
	 * Project : PURCHASE ERROR SOLVING (NBHC)
	 */
	protected String remark;
	
	/**
	 * END
	 */
	
	
	/**
	 * Pur Engg assign pr to its team.
	 * Date :26-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	@Index
	protected String assignTo1;
	@Index
	protected String assignTo2;
	
	/**
	 * END
	 */
	
	
	/**
	 * Record Select Flag is Added
	 */
	
	protected boolean recordSelect;
	
	
	/**
	 * Date 08-02-2017 By Anil
	 * Adding description 2 field because it can not store more than 500 character (approx)
	 */
	
	@Index
	protected String description2;
	/**
	 * End
	 */
	/** daet 2/2/2018 added by komal to set approve status in pr(excel download ) **/
	@Index
	protected String approvalStatus;
	
	/*** Date 17-12-2018 By Vijay
	 *  Des :- For Automatic PR creation flag
	 *  and Comment for automatic PR
	 */
	@Index
	protected boolean automaticPRFlag;
	/**
	 * ends here
	 */
	
	/**********************************************constructor*************************************/
	
	
	public PurchaseRequisition() {
		super();
		purchasedetails=new PurchaseDetails();
		prproduct=new ArrayList<ProductDetails>();
		remark="";
		
		assignTo1="";
		assignTo2="";
		recordSelect=false;
		
		prTitle="";
		prCategory="";
		prType="";
		prGroup="";
		description="";
		description2="";
		automaticPRFlag = false;
	}
	
	
	  
	
	
	/****************************************Getters and setters**************************************/
	
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	
	public boolean getRecordSelect() {
		return recordSelect;
	}
	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	
	

	public String getHeader() {
		return purchasedetails.getHeader();
	}
	public String getAssignTo1() {
		return assignTo1;
	}
	public void setAssignTo1(String assignTo1) {
		this.assignTo1 = assignTo1;
	}
	public String getAssignTo2() {
		return assignTo2;
	}
	public void setAssignTo2(String assignTo2) {
		this.assignTo2 = assignTo2;
	}





	public void setHeader(String header) {
		purchasedetails.setHeader(header);
	}


	public String getFooter() {
		return purchasedetails.getFooter();
	}

	public void setFooter(String footer) {
		purchasedetails.setFooter(footer);
	}
  
	
	public Date getResponseDate() {
		return purchasedetails.getResponseDate() ;
	}

	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}


	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public Date getRefOrderDate() {
		return RefOrderDate;
	}

	public void setRefOrderDate(Date refOrderDate) {
		RefOrderDate = refOrderDate;
	}

	public String getPrGroup() {
		return prGroup;
	}


	public void setPrGroup(String prGroup) {
		this.prGroup = prGroup;
	}


	public String getPrTitle() {
		return prTitle;
	}

	public void setPrTitle(String prTitle) {
		this.prTitle = prTitle;
	}

	public String getPrCategory() {
		return prCategory;
	}

	public void setPrCategory(String prCategory) {
		this.prCategory = prCategory;
	}

	public String getPrType() {
		return prType;
	}

	public void setPrType(String prType) {
		this.prType = prType;
	}


//	public String getExpDeliveryDate() {
//		return expDeliveryDate;
//	}
//
//	public void setExpDeliveryDate(String expDeliveryDate) {
//		this.expDeliveryDate = expDeliveryDate;
//	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void setResponseDate(Date responseDate) {
		purchasedetails.setResponseDate(responseDate);
	}

	public String getRefOrderNO() {
		return purchasedetails.getRefOrderNO();
	}


	public void setRefOrderNO(String refOrderNO) {
		purchasedetails.setRefOrderNO(refOrderNO);
	}


	public String getProject() {
		return purchasedetails.getProject();
	}

	public void setProject(String project) {
		purchasedetails.setProject(project);
	}


	public Integer getPurchaseReqNo() {
		return purchasedetails.getPurchaseReqNo();
	}


	public void setPurchaseReqNo(Integer purchaseReqNo) {
		purchasedetails.setPurchaseReqNo(purchaseReqNo);
	}

	public String getBranch() {
		return purchasedetails.getBranch();
	}


	public String getEmployee() {
		return purchasedetails.getEmployee();
	}


	public String getStatus() {
		return purchasedetails.getStatus();
	}


	public void setBranch(String branch) {
		purchasedetails.setBranch(branch);
	}

	public void setEmployee(String employee) {
		purchasedetails.setEmployee(employee);
	}


	public void setStatus(String status) {
		purchasedetails.setStatus(status);
	}

	public void setCreationDate(Date date) {
		purchasedetails.setCreationDate(date);
	}

	public Date getCreationDate() {
		return purchasedetails.getCreationDate();
	}

	public String getApproverName() {
		return purchasedetails.getApproverName();
	}


	public void setApproverName(String name) {
		purchasedetails.setApproverName(name);
	}


	public DocumentUpload getUptestReport() {
		return uptestReport;
	}





	public void setUptestReport(DocumentUpload uptestReport) {
		this.uptestReport = uptestReport;
	}



	public ArrayList<ProductDetails> getPrproduct() {
		return prproduct;
	}


	public void setPrproduct(List<ProductDetails> prproduct) {
		if(prproduct!=null)
		{
			ArrayList<ProductDetails> list=new ArrayList<ProductDetails>();
			list.addAll(prproduct);
			this.prproduct=list;
		}
	}


	public ArrayList<VendorDetails> getVendlist() {
		return vendlist;
	}


	public void setVendlist(ArrayList<VendorDetails> vendlist) {
		this.vendlist = vendlist;
	}


/*******************************************************************************************************/	

	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	
	public static List<String> getStatusList()
	{
		ArrayList<String> purchasereq=new ArrayList<String>();
		purchasereq.add(APPROVED);
		purchasereq.add(CREATED);
		purchasereq.add(REQUESTED);
		purchasereq.add(PROCESSED);
		purchasereq.add(REJECTED);
		purchasereq.add(CLOSE);
		purchasereq.add(CANCELLED);
		
		
		return purchasereq;
		
	}

	@Override
	public void setApprovalDate(Date date) {
		
	}

	@Override
	public String getRemark() {
		return this.remark.trim();
	}

	@Override
	public void setRemark(String remark) {
		if(remark!=null){
			this.remark=remark.trim();
		}
	}
		
	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	@Override
	public void reactOnApproval() {
		System.out.println("ON APPROVAL OF PR");
	}
	
	
	/**
	 * This method set the status of Pr to close and create the balance qty Pr(only For Multiple Pr).
	 * Date : 13-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	@GwtIncompatible
	private void executeCodeOnApproval(){
		if(this.getStatus().equals(PurchaseRequisition.APPROVED)){
			boolean prFlag=false;
			int prCount=this.getPrproduct().get(0).getPrCount();
			for(ProductDetails obj:this.getPrproduct()){
				if(obj.getPrCount()!=prCount){
					prFlag=true;
					break;
				}
			}
			System.out.println("CHN Flag "+prFlag);
			if(prFlag){
				HashSet<Integer> intSet=new HashSet<Integer>();
				ArrayList<Integer> intArrLis=new ArrayList<Integer>();
				System.out.println("PROD LIS SIZE : "+this.getPrproduct().size());
				for(ProductDetails obj:this.getPrproduct()){
					intSet.add(obj.getPrCount());
				}
				System.out.println("Unique Prod size : "+intSet.size());
				intArrLis.addAll(intSet);
				
				if(intArrLis.contains(this.getCount())){
					intArrLis.remove(this.getCount());
				}
				System.out.println("Unique Prod size AF : "+intArrLis.size());
				
				List<PurchaseRequisition> entityList=new ArrayList<PurchaseRequisition>();
				ArrayList<PurchaseRequisition> prList=new ArrayList<PurchaseRequisition>();
				
				for(Integer count:intArrLis){
					System.out.println("PR COUNT");
					PurchaseRequisition entity=ofy().load().type(PurchaseRequisition.class).filter("companyId",this.getCompanyId()).filter("count",count)
							.filter("purchasedetails.status", PurchaseRequisition.PROCESSED).first().now();
					
					if(entity!=null){
						ArrayList<ProductDetails> prodList=new ArrayList<ProductDetails>();
						
						//Getting product list of PR whose balance quantity in not zero
						for(ProductDetails obj:entity.getPrproduct()){
							if(obj.getBalanceQty()!=0){
								ProductDetails prod=getPrProductInfo(obj);
								prodList.add(prod);
							}
						}
						// if prod list is not zero then we create a new PR of balance Quantity
						if(prodList.size()!=0){
							PurchaseRequisition object=getPrData(entity,prodList);
							prList.add(object);
						}
						
						//Setting old Pr Status to close 
						entity.setStatus(PurchaseRequisition.CLOSE);
						entityList.add(entity);
					}
				}
				
				System.out.println("Entity List Size : "+entityList.size());
				System.out.println("New PR List Size : "+prList.size());
				
				if(entityList.size()!=0){
					System.out.println("SAVING OLD PR as CLOSED");
					ofy().save().entities(entityList);
				}
				
				if(prList.size()!=0){
					System.out.println("SAVING NEW PR...");
					ArrayList<SuperModel>modelLis=new ArrayList<SuperModel>();
					for(PurchaseRequisition obj:prList){
						SuperModel model=obj;
						modelLis.add(model);
					}
					GenricServiceImpl impl=new GenricServiceImpl();
					impl.save(modelLis);
				}
				
				
			}
		}
	}
	
	
	
	/**
	 * This method return new Pr of balance quantity with reference of old Pr.
	 * Date : 12-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : Purchase Modification(NBHC)
	 */

	@GwtIncompatible
	private PurchaseRequisition getPrData(PurchaseRequisition entity,
			ArrayList<ProductDetails> prodList) {
		SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
		
		PurchaseRequisition object=new PurchaseRequisition();
		object.setCompanyId(entity.getCompanyId());
		object.setRefOrderNO(entity.getCount()+"");
		object.setRefOrderDate(entity.getCreationDate());
		object.setEmployee(entity.getEmployee());
		object.setApproverName(entity.getApproverName());
		object.setExpectedDeliveryDate(entity.getExpectedDeliveryDate());
		object.setBranch(entity.getBranch());
		object.setPrproduct(prodList);
		object.setStatus(PurchaseRequisition.CREATED);
		object.setDescription("This PR is a balance quantity PR of PR ID- "+entity.getCount()+" Dated : "+fmt.format(entity.getCreationDate())+".");
		
		object.setAssignTo1(entity.getAssignTo1());
		object.setAssignTo2(entity.getAssignTo2());
		object.setPrTitle(entity.getPrTitle());
		object.setPrCategory(entity.getPrCategory());
		object.setPrType(entity.getPrType());
		object.setPrGroup(entity.getPrGroup());
		object.setProject(entity.getProject());
		if(entity.getResponseDate()!=null){
			object.setResponseDate(entity.getResponseDate());
		}
		object.setHeader(entity.getHeader());
		object.setFooter(entity.getFooter());
		return object;
	}





	/**
	 *This method returns the modified product details which is generated from existing product details for creating new Pr of balance qty.
	 *Date : 12-10-2016 By Anil
	 *Release : 30 Sept 2016 
	 *Project : Purchase Modification (NBHC)
	 */
	@GwtIncompatible
	private ProductDetails getPrProductInfo(ProductDetails obj) {
		ProductDetails entity=new ProductDetails();
		entity.setPrduct(obj.getPrduct());
		entity.setProductID(obj.getProductID());
		entity.setProductCode(obj.getProductCode());
		entity.setProductCategory(obj.getProductCategory());
		entity.setProductName(obj.getProductName());
		entity.setProdDate(obj.getProdDate());
		entity.setProdPrice(obj.getProdPrice());
		entity.setUnitOfmeasurement(obj.getUnitOfmeasurement());
		entity.setWarehouseName(obj.getWarehouseName());
		entity.setAvailableStock(obj.getAvailableStock());
		entity.setProductQuantity(obj.getBalanceQty());
		entity.setAcceptedQty(obj.getBalanceQty());
		entity.setBalanceQty(0);
		return entity;
	}





	@Override
	public void reactOnRejected() {
		
	}

	
	/**
	 * This Method Execute When you Save Pr
	 * Date : 12-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	@OnSave
	@GwtIncompatible
	private void onSaveCodeExecution(){
		setRecordSelect(false);
		setPrCountToPrductTable();
		changeExistingPrStatusForCombinedPr();
		executeCodeOnApproval();
		if(this.id!=null){
			sendEmailToPoPurchaseEngg();
		}
		updatePRStatusToClose();
	}
	
	
	/**
	 * This Method Send email to PO Purchase Engg after approval of PR that PR is approved and ready
	 * to make PO
	 * Date : 14-10-2016 By Anil 
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 * 
	 */
	@GwtIncompatible
	private void sendEmailToPoPurchaseEngg() {
		System.out.println("SEND EMAIL METHOD");
		if(this.getStatus().equals(PurchaseRequisition.APPROVED)){
			System.out.println("PR APPROVED");
			if((this.getAssignTo1()!=null&&!this.getAssignTo1().equals(""))
					||(this.getAssignTo2()!=null&&!this.getAssignTo2().equals(""))){
				System.out.println("ASSIGN TO 1 OR 2 NOT NULL ");
				boolean sendEmailFlag=false;
				ProcessConfiguration processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", this.getCompanyId()).filter("processName", "PurchaseRequisition").filter("configStatus", true).first().now();
				if(processConfig!=null){
					for(int k=0;k<processConfig.getProcessList().size();k++){
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PRAPPROVALMAILTOPOPURENGG")
								&&processConfig.getProcessList().get(k).isStatus()==true){
							sendEmailFlag=true;
						}
					}
				}
				
				if(sendEmailFlag){
					ArrayList<String> toEmailList=new ArrayList<String>();
					ArrayList<String> ccEmailList=new ArrayList<String>();
					String fromEmail=null;
					System.out.println("SEND EMAIL FLAG TRUE");
					if(!this.getAssignTo1().equals("")&&!this.getAssignTo2().equals("")){
						if(!this.getAssignTo1().equals(this.getAssignTo2())){
							Employee emp1=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
									.filter("fullname", this.getAssignTo1().trim()).first().now();
							if(emp1!=null){
								if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
									toEmailList.add(emp1.getEmail());
								}
							}
							
							Employee emp2=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
									.filter("fullname", this.getAssignTo2().trim()).first().now();
							if(emp2!=null){
								if(emp2.getEmail()!=null&&!emp2.getEmail().equals("")){
									toEmailList.add(emp2.getEmail());
								}
							}
									
						}else{
							Employee emp1=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
									.filter("fullname", this.getAssignTo1().trim()).first().now();
							if(emp1!=null){
								if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
									toEmailList.add(emp1.getEmail());
								}
							}
						}
						
					}else if(!this.getAssignTo1().equals("")&&this.getAssignTo2().equals("")){
						Employee emp1=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
								.filter("fullname", this.getAssignTo1().trim()).first().now();
						if(emp1!=null){
							if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
								toEmailList.add(emp1.getEmail());
							}
						}
						
					}else if(this.getAssignTo1().equals("")&&!this.getAssignTo2().equals("")){
						Employee emp2=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
								.filter("fullname", this.getAssignTo2().trim()).first().now();
						if(emp2!=null){
							if(emp2.getEmail()!=null&&!emp2.getEmail().equals("")){
								toEmailList.add(emp2.getEmail());
							}
						}
					}
					
					System.out.println("To Email List Size : "+toEmailList.size());
					if(toEmailList.size()!=0){
						Company comp=ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
						if(comp!=null){
							if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
								fromEmail=comp.getEmail();
							}
						}
					}
					
					if(fromEmail!=null){
						Employee emp1=ofy().load().type(Employee.class).filter("companyId", this.getCompanyId())
								.filter("fullname", this.getEmployee().trim()).first().now();
						if(emp1!=null){
							if(emp1.getEmail()!=null&&!emp1.getEmail().equals("")){
								ccEmailList.add(emp1.getEmail());
							}
						}
						
						String requester="";
						Approvals approval=ofy().load().type(Approvals.class).filter("companyId", this.getCompanyId())
								.filter("businessprocesstype", "Purchase Requisition").filter("businessprocessId", this.getCount()).first().now();
						if(approval!=null){
							requester=approval.getRequestedBy();
						}
						System.out.println("Form Email Not Null");
						String mailSub="Process PR Id -"+this.getCount()+" Branch -"+this.getBranch();
						String mailMsg= "";
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CronJob", "EnableSendGridEmailApi",this.getCompanyId())){
							StringBuilder builder = new StringBuilder();
							builder.append("<!DOCTYPE html>");
							builder.append("<html lang=\"en\">");
							builder.append("<style> p {  max-width: 40%;} h3.ex{margin-top: 10px;    margin-bottom: 10px;    margin-right: 900px;    margin-left: 50px; height:50px; width:400px; }</style><title></title></head>");
							builder.append("<body align=\"justify\"  min-width: 200px;  max-width: 300px; min-height=100px;>");

							builder.append("Dear Sir/Madam,");
							builder.append("<br>");
							builder.append("<br>");
							builder.append("PR Id "+this.getCount()+" Branch "+this.getBranch()+" is assigned to you please process."+"</br>"+"</br>"+"</br>"
									+"Requester : "+requester+"</br>");
							builder.append("<br>");
							builder.append("<br>");
							builder.append("Purchase Engineer : "+this.getEmployee());
							builder.append("<br>");
							builder.append("Requester : "+requester+"</br>");
							builder.append("<br>");
							builder.append("<br>");
							builder.append("</body>");
							builder.append("</html>");
							mailMsg = builder.toString();
						}
						else{
							mailMsg = "Dear Sir/Madam"+","+"\n"+"\n"+"\n"
									+"PR Id "+this.getCount()+" Branch "+this.getBranch()+" is assigned to you please process."+"\n"+"\n"+"\n"
									+"Purchase Engineer : "+this.getEmployee()+"\n"
									+"Requester : "+requester+"\n";
						}
						
						/**
						 * Date 06-12-2019 by vijay Handling exception added try catch for normal execution
						 */
						try {
						Email obj=new Email();
						obj.sendEmail(mailSub, mailMsg, toEmailList, ccEmailList, fromEmail);
						
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					
				}
			}
		}
	}





	/**
	 * This method sets PR count to the product table data.
	 * Date 11-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	@GwtIncompatible
	private void setPrCountToPrductTable(){
		System.out.println("inside PR save mthod");
		if(this.id==null){
			System.out.println("saving data for the first time");
			for(ProductDetails obj:this.getPrproduct()){
				if(obj.getPrCount()==0){
					obj.setPrCount(this.getCount());
				}
			}
		}
	}
	
	
	
	/**
	 * This method change the status of Pr which is combinedly used to create single PR
	 * Date : 12-10-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : Purchase Modification (NBHC) 
	 */
	@GwtIncompatible
	private void changeExistingPrStatusForCombinedPr(){
		System.out.println("MULTI PR CHNG STATUS");
		if(this.id==null){
			SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
			boolean prFlag=false;
			if(this.getPrproduct()!=null){
			int prCount=this.getPrproduct().get(0).getPrCount();
			for(ProductDetails obj:this.getPrproduct()){
				if(obj.getPrCount()!=prCount){
					prFlag=true;
					break;
				}
			}
			}
			System.out.println("CHN Flag "+prFlag);
			if(prFlag){
				HashSet<Integer> intSet=new HashSet<Integer>();
				ArrayList<Integer> intArrLis=new ArrayList<Integer>();
				System.out.println("PROD LIS SIZE : "+this.getPrproduct().size());
				for(ProductDetails obj:this.getPrproduct()){
					intSet.add(obj.getPrCount());
				}
				System.out.println("Unique Prod size : "+intSet.size());
				intArrLis.addAll(intSet);
				
				List<PurchaseRequisition> prList=new ArrayList<PurchaseRequisition>();
				for(Integer count:intArrLis){
					PurchaseRequisition entity=ofy().load().type(PurchaseRequisition.class)
							.filter("companyId",this.getCompanyId()).filter("count",count).first().now();
					
					if(entity!=null){
						System.out.println("PR NOT Null ");
						if(entity.getRefOrderNO().equals("")){
							entity.setRefOrderNO(this.getCount()+"");
						}
						if(entity.getRefOrderDate()==null){
							entity.setRefOrderDate(this.getCreationDate());
						}
						String msg="This PR is processed in PR ID - "+this.getCount()+" Dated : "+fmt.format(this.getCreationDate())+" By "+this.getEmployee()+".";
						if(!entity.getDescription().equals("")){
							String newMsg=entity.getDescription()+"\n"+msg;
							entity.setDescription(newMsg);
						}else{
							entity.setDescription(msg);
						}
						entity.setStatus(PROCESSED);
						prList.add(entity);
					}
				}
				
				System.out.println("PR LIST SIZ : "+prList.size());
				
				if(prList.size()!=0){
					System.out.println("SAVING....");
					ofy().save().entities(prList).now();
				}
			}
		}
	}





	public boolean isAutomaticPRFlag() {
		return automaticPRFlag;
	}

	public void setAutomaticPRFlag(boolean automaticPRFlag) {
		this.automaticPRFlag = automaticPRFlag;
	}

	/**
	 * @author Vijay Chougule
	 * Date 14-09-2019 
	 * Des :- NBHC Inventory Management Lock Seal
	 * PR will close when GRNBalance qty <=0 in PR. GRNbalanceQty updating from GRN
	 */
	@GwtIncompatible
	private void updatePRStatusToClose() {

		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseRequisition", "EnableClosePRBasedOnGRNQty", this.getCompanyId())){
			for(ProductDetails prDetails : this.getPrproduct()){
				if(prDetails.getGrnBalancedQty()<=0){
					this.setStatus(PurchaseRequisition.CLOSE);
				}
			}
		}
	}
	/**
	 * ends here
	 */
	

}
