package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

@Embed
public class PurchaseDetails extends ConcreteBusinessProcess{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5804303438536243096L;

	/** The header. */
	@Index
	protected String header;
	
	/** The footer. */
	protected String footer;
	
	/** The Constant REQUESTED. */
	public static final String CREATED="Created",CANCELLED="Cancelled",REJECTED="Rejected",APPROVED="Approved",REQUESTED="Requested";
	
	/*
	/** The quotation name. */
	@Index
	protected String quotationName;
	
	@Index
	protected Date CreatedDate;
	
	/** The response date. */
	@Index
	protected Date responseDate;
	
	/** The quotation type. */
	@Index
	protected String quotationType;
	
	/** The quotation category. */
	@Index
	protected String quotationCategory;
	
	/** The letter type. */
	protected int letterType;
	
//	ArrayList<Key<SuperProduct>>productKey;
	//ArrayList<Key<Vendor>>vendorKey;
	
	@Index
	protected Date expectedClosureDate;
	
	@Index
	protected String refOrderNO;
	
	@Index
	protected String project;

	@Index
	protected int purchaseReqNo;
	
	@Index
	protected String title;
	
	@Index
	protected Date deliveryDate;
	
	@Index
	protected Date prDate;
	
	
	public PurchaseDetails() {
		title="";
		header="";
		footer="";
		quotationName="";
		quotationType="";
		quotationCategory="";
		refOrderNO="";
		project="";
		
		
	}
	
	public Date getPrDate() {
		return prDate;
	}


	public void setPrDate(Date prDate) {
		if(prDate!=null)
			this.prDate = prDate;
	}


	protected DocumentUpload uptestReport;
	
	/****************************getters and setters************************************************/
	
	public String getHeader() {
		return header;
	}


	public void setHeader(String header) {
		this.header = header;
	}


	public String getFooter() {
		return footer;
	}


	public void setFooter(String footer) {
		this.footer = footer;
	}


	public String getQuotationName() {
		return quotationName;
	}


	public void setQuotationName(String quotationName) {
		this.quotationName = quotationName;
	}


	public Date getCreatedDate() {
		return CreatedDate;
	}


	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}


	public Date getResponseDate() {
		return responseDate;
	}


	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}


	public String getQuotationType() {
		return quotationType;
	}


	public void setQuotationType(String quotationType) {
		this.quotationType = quotationType;
	}


	public String getQuotationCategory() {
		return quotationCategory;
	}


	public void setQuotationCategory(String quotationCategory) {
		this.quotationCategory = quotationCategory;
	}


	public Date getExpectedClosureDate() {
		return expectedClosureDate;
	}


	public void setExpectedClosureDate(Date expectedClosureDate) {
		this.expectedClosureDate = expectedClosureDate;
	}


	public String getRefOrderNO() {
		return refOrderNO;
	}


	public void setRefOrderNO(String refOrderNO) {
		this.refOrderNO = refOrderNO;
	}


	
	
	public String getProject() {
		return project;
	}


	public void setProject(String project) {
		this.project = project;
	}


	public int getPurchaseReqNo() {
		return purchaseReqNo;
	}


	public void setPurchaseReqNo(int purchaseReqNo) {
		this.purchaseReqNo = purchaseReqNo;
	}

	

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	
	


	public Date getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public DocumentUpload getUptestReport() {
		return uptestReport;
	}


	public void setUptestReport(DocumentUpload uptestReport) {
		this.uptestReport = uptestReport;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	


}
