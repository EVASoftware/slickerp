package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

// TODO: Auto-generated Javadoc
/**
 * The Class RfqProductDetails.
 */
@Embed
public class ProductDetails extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -275323280205337769L;

	/** The product id. */
	protected int productID;

	/** The product code. */
	protected String productCode;
	
	/** The product name. */
	protected String productName;
	
	/** The product category. */
	protected String productCategory;
	
	/** The product quantity. */
	protected double productQuantity;
	
	/** The prod date. */
	protected Date expectedDeliveryDate;
	
	protected double prodPrice;
	
	protected String unitOfmeasurement;
	
	protected double serviceTax;
	
	protected double vat;
	
	private ItemProduct itemProduct;
	
	/**
	 * While adding product also capture warehouse and available Stock details.
	 * Date : 26-09-2016 By Anil
	 * Release : 30 Sept 2016 
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	private String warehouseName;
	private double availableStock;
	
	/**
	 * End
	 */
	
	
	
	/**
	 * when user merge multiple PR into single then he may reduce the requested qty which will be accepted qty.
	 * it balance qty is not zero then for perticular qty new pr will be raised.
	 * Date : 10-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	private double acceptedQty;
	private double balanceQty;
	
	/**
	 * End
	 */
	
	
	/**
	 * This PR count is used identify the product of multiple PR's when we combine and make single PR
	 * Date : 11-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	/**
	 * Date 26-04-2017
	 * added by vijay for NBHC Deadstock
	 * total product price into quantity
	 * and Warehouse Address
	 */
	private double total;
	private String warehouseAddress;
	/**
	 * Ends here
	 */
	
	
	private int prCount;
	/**
	 * ENd
	 */
	
	/**
	 * Date  20-12-2018 By vijay
	 * Des :- NBHC Inventory Management GRN Qty received 
	 * related this PR
	 */
	private double grnQtyReceived;
	/** Date  20-12-2018 By vijay
	 * Des :- NBHC Inventory Management Po Qty  
	 * related this PR
	 */
	private double poQty;
	/**
	 * ends here
	 */
	/**
	 * Date 28-01-2019 by Vijay
	 * Des :- NBHC Inventory Management Automatic PR Logic maintaining PR quantity in another variable because
	 * if Automatic PR Created then if we reduced the the quantity then again PR will created so this variable is
	 * used to mainting the PR Quantity
	 */
	private double prQuantity;
	
	 /** Date  13-09-2019 By vijay
	 * Des :- NBHC Inventory Management GRN balanced Qty to close the PR 
	 * for lock seal GRN in FIFO
	 */
	private double grnBalancedQty;
	
/************************************Constuctor*******************************************/
	public ProductDetails()
	{
		super();
		productQuantity=0;
		warehouseName="";
		availableStock=0;
		acceptedQty=0;
		balanceQty=0;
		prCount=0;
		total=0;
		warehouseAddress="";
		
	}
	
	
/*********************************Getters and Setters*****************************************/
	
	public int getPrCount() {
		return prCount;
	}
	public void setPrCount(int prCount) {
		this.prCount = prCount;
	}

	public double getAcceptedQty() {
		return acceptedQty;
	}

	public void setAcceptedQty(double acceptedQty) {
		this.acceptedQty = acceptedQty;
	}
	public double getBalanceQty() {
		return balanceQty;
	}
	public void setBalanceQty(double balanceQty) {
		this.balanceQty = balanceQty;
	}

	
	
	
	public SuperProduct getPrduct() {
		if (itemProduct != null){
			return itemProduct;
		}else {
			return null;
		}
	}
	public void setPrduct(SuperProduct product) {
		if (product instanceof ItemProduct){
			itemProduct = (ItemProduct) product;
		}
	}




	public String getWarehouseName() {
		return warehouseName;
	}


	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	
	
	public double getAvailableStock() {
		return availableStock;
	}
	
	
	public void setAvailableStock(double availableStock) {
		this.availableStock = availableStock;
	}


	
	
	public double getTotal() {
		return total;
	}


	public void setTotal(double total) {
		this.total = total;
	}
	
	public String getWarehouseAddress() {
		return warehouseAddress;
	}

	public void setWarehouseAddress(String warehouseAddress) {
		this.warehouseAddress = warehouseAddress;
	}
	
	
	
	
	
	
	
	
	
	/**
	 * Gets the prod date.
	 *
	 * @return the prod date
	 */
	public Date getProdDate() {
		return expectedDeliveryDate;
	}

	/**
	 * Sets the prod date.
	 *
	 * @param prodDate the new prod date
	 */
	public void setProdDate(Date prodDate) {
		this.expectedDeliveryDate = prodDate;
	}
	

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public int getProductID() {
		return productID;
	}
	
	/**
	 * Sets the product id.
	 *
	 * @param productID the new product id
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}
	
	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}
	
	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/**
	 * Gets the product category.
	 *
	 * @return the product category
	 */
	public String getProductCategory() {
		return productCategory;
	}
	
	/**
	 * Sets the product category.
	 *
	 * @param productCategory the new product category
	 */
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	/**
	 * Gets the product quantity.
	 *
	 * @return the product quantity
	 */
	public double getProductQuantity() {
		return productQuantity;
	}
	
	/**
	 * Sets the product quantity.
	 *
	 * @param productQuantity the new product quantity
	 */
	public void setProductQuantity(double productQuantity) {
		this.productQuantity = productQuantity;
	}
	
	public double getProdPrice() {
		return prodPrice;
	}
	
	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}

	public String getUnitOfmeasurement() {
		return unitOfmeasurement;
	}
	public void setUnitOfmeasurement(String unitOfmeasurement) {
		this.unitOfmeasurement = unitOfmeasurement;
	}

	public double getServiceTax() {
		return serviceTax;
	}


	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}


	public double getVat() {
		return vat;
	}


	public void setVat(double vat) {
		this.vat = vat;
	}

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}
	


	@Override
	public String toString()
	{
		return this.productName.toString();
	}
	
	public double getGrnQtyReceived() {
		return grnQtyReceived;
	}


	public void setGrnQtyReceived(double grnQtyReceived) {
		this.grnQtyReceived = grnQtyReceived;
	}


	public double getPoQty() {
		return poQty;
	}


	public void setPoQty(double poQty) {
		this.poQty = poQty;
	}


	public double getPrQuantity() {
		return prQuantity;
	}


	public void setPrQuantity(double prQuantity) {
		this.prQuantity = prQuantity;
	}


	public double getGrnBalancedQty() {
		return grnBalancedQty;
	}


	public void setGrnBalancedQty(double grnBalancedQty) {
		this.grnBalancedQty = grnBalancedQty;
	}
	
	
	
}
