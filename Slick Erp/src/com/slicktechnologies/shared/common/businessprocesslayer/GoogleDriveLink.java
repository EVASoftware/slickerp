package com.slicktechnologies.shared.common.businessprocesslayer;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class GoogleDriveLink extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3216746034685101610L;


	protected String documentType;
	protected Date date;
	protected String googleDriveLink;
	protected String remark;
	protected Date creationDate;
	
	public GoogleDriveLink() {
		documentType="";
		date=new Date();
		googleDriveLink="";
		remark="";
		creationDate=new Date();
	}
	
	
	

	public String getDocumentType() {
		return documentType;
	}




	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}




	public Date getDate() {
		return date;
	}




	public void setDate(Date date) {
		this.date = date;
	}




	public String getGoogleDriveLink() {
		return googleDriveLink;
	}




	public void setGoogleDriveLink(String googleDriveLink) {
		this.googleDriveLink = googleDriveLink;
	}




	public String getRemark() {
		return remark;
	}




	public void setRemark(String remark) {
		this.remark = remark;
	}




	public Date getCreationDate() {
		return creationDate;
	}




	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}




	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
