package com.slicktechnologies.shared.common.businessprocesslayer;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class ExpensePolicies extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4859078782367090168L;
	
	
	@Index
	String cityClass;
	@Index
	String employeeRole;
	
	@Index
	String expenseGrp;
	@Index
	String expenseCategory;
	double expenseLimit;
	@Index
	boolean atActual;
	@Index
	boolean perKm;
	double ratePerKm;
	
	@Index
	boolean status;

	
	public ExpensePolicies() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	

	public String getCityClass() {
		return cityClass;
	}



	public void setCityClass(String cityClass) {
		this.cityClass = cityClass;
	}



	public String getEmployeeRole() {
		return employeeRole;
	}



	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}



	public String getExpenseGrp() {
		return expenseGrp;
	}



	public void setExpenseGrp(String expenseGrp) {
		this.expenseGrp = expenseGrp;
	}


	public String getExpenseCategory() {
		return expenseCategory;
	}

	public void setExpenseCategory(String expenseCategory) {
		this.expenseCategory = expenseCategory;
	}



	public double getExpenseLimit() {
		return expenseLimit;
	}



	public void setExpenseLimit(double expenseLimit) {
		this.expenseLimit = expenseLimit;
	}



	public boolean isAtActual() {
		return atActual;
	}



	public void setAtActual(boolean atActual) {
		this.atActual = atActual;
	}



	public boolean isPerKm() {
		return perKm;
	}



	public void setPerKm(boolean perKm) {
		this.perKm = perKm;
	}


	

	public double getRatePerKm() {
		return ratePerKm;
	}



	public void setRatePerKm(double ratePerKm) {
		this.ratePerKm = ratePerKm;
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		if(isCityClassDuplicate(m)&&isEmployeeRoleDuplicate(m)&&isExpGrpDuplicate(m)&&isExpCategoryDuplicate(m)){
			return true;
		}
		return false;
	}
	
	
	private boolean isCityClassDuplicate(SuperModel m){
		ExpensePolicies entity = (ExpensePolicies) m;
		String name = entity.getCityClass().trim();
		String curname = this.cityClass.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();

		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}
	
	private boolean isEmployeeRoleDuplicate(SuperModel m){
		ExpensePolicies entity = (ExpensePolicies) m;
		String name = entity.getEmployeeRole().trim();
		String curname = this.employeeRole.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();

		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}
	
	private boolean isExpGrpDuplicate(SuperModel m){
		ExpensePolicies entity = (ExpensePolicies) m;
		String name = entity.getExpenseGrp().trim();
		String curname = this.expenseGrp.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();

		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}
	
	private boolean isExpCategoryDuplicate(SuperModel m){
		ExpensePolicies entity = (ExpensePolicies) m;
		String name = entity.getExpenseCategory().trim();
		String curname = this.expenseCategory.trim();
		curname = curname.replaceAll("\\s", "");
		curname = curname.toLowerCase();

		name = name.replaceAll("\\s", "");
		name = name.toLowerCase();

		// New Object is being added
		if (id == null) {
			if (name.equals(curname))
				return true;
			else
				return false;
		}
		// Old object is being updated
		else {
			if (id.equals(entity.id)) {
				return false;
			}
			if ((name.equals(curname) == true))
				return true;
			else
				return false;
		}
	}

}
