package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.ArrayList;
import java.util.List;







import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

import static  com.googlecode.objectify.ObjectifyService.ofy;

// TODO: Auto-generated Javadoc
/**
 * The Class PriceList.
 */
@Entity
public class PriceList extends SuperModel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8787372052070797474L;
	
	@Index
	protected String pricelistTitle;
	
	/** The prod category. */
	@Index
	protected String prodCategory;
	
	/** The prod code. */
	@Index
	protected String prodCode;
	
	/** The prod name. */
	@Index
	protected String prodName;
	
	protected double percDiscount;
	
	/** The prod price. */
	protected double prodPrice;
	
	/** The prod id. */
	@Index
	protected int prodID;
	
	protected String unitOfmeasurement;
	/** The price info. list of vendors */
	
	ArrayList<PriceListDetails> priceInfo;
	
	ArrayList<Key<Vendor>>vendorKey;
	
//  rohan added this for saving vendor product price ahistory	
	ArrayList<PriceListDetails> vendorPriceChangeHistory;
	
	@Index
	protected Double serviceTax;	
	@Index
	protected Double vat;
	
	
	/*******************************Constructor*************************************************/
	
	/**
	 * Instantiates a new price list.
	 */
	
	public PriceList()
	{
		priceInfo=new ArrayList<PriceListDetails>();
		
		vendorPriceChangeHistory = new ArrayList<PriceListDetails>();
		vendorKey=new ArrayList<Key<Vendor>>();
		prodCategory="";
		prodCode="";
		prodName="";
		pricelistTitle="";
		
		
	}
	
	
	
	/**************************************Relation Management part**************************/
	
	Key<SuperProduct>prodKey;
	
	@GwtIncompatible
	@OnSave
	protected void makeProdKey()
	{
		if(getId()==null)
		{
			
		prodKey=ofy().load().type(SuperProduct.class).filter("count",getProdID()).
				filter("companyId",getCompanyId()).
				keys().first().now();
		}
	}
	
	
	@GwtIncompatible
	@OnLoad
	protected void updateProductAttribute()
	{
		if(prodKey!=null)
		{
			
		 SuperProduct prod= ofy().load().key(prodKey).now();
		 if(prod!=null)
		 {
			 this.prodCategory=prod.getProductCategory();
			 this.prodCode=prod.getProductCode();
			 this.prodName=prod.getProductName();
			 
			 for(PriceListDetails details:this.priceInfo)
			 {
				 details.setProductCategory(prod.getProductCategory());
				 details.setProductCode(prod.getProductCode());
				 details.setProductName(prod.getProductName());
			 }
		 }
		 
		 
		}
	}
	
	@GwtIncompatible
	@OnSave
	public void saveVendorKey()
	{
		if(priceInfo!=null)
		{
			this.vendorKey=new ArrayList<Key<Vendor>>();
			for(PriceListDetails details:this.priceInfo)
			{
				//Load Vendor Using Relation id
				Key<Vendor>venKey=ofy().load().type(Vendor.class).filter("count",details.getVendorID()).
						filter("companyId",getCompanyId()).
						keys().first().now();
				vendorKey.add(venKey);
				

				
			}
		}
	}
	
	@GwtIncompatible
	@OnLoad
	public void upDateVendor()
	{
		if(vendorKey!=null)
		{
//			for(Key<Vendor>venKey:this.vendorKey)
//			{
//				if(venKey!=null)
//				{
//				  Vendor ven=ofy().load().key(venKey).now();
//				  for(PriceListDetails details:priceInfo)
//				  {
//					  if(ven.getCount()==details.getVendorID()){
//						  details.setFullName(ven.getVendorName());
//						  details.setCellNumber(ven.getCellNumber1());
//						  details.setEmail(ven.getEmail());
//					  }
//				  }
//				}
//			}
		}
	}

	
	
	
	/********************************************** on Save *****************************************/
	
	@OnSave
	@GwtIncompatible
	public void reactOnSave()
	{
		if(id==null)
		{
			saveVendorPriceList();
		}
		if(id!=null)
		{
			updateVendorPriceList();
		}
		
		//    rohan added this method for making vendor product price  
		
		createHistoryForVendorProductPrice();
		
//		saveTransactionList();
		
	}
	
	
	private void createHistoryForVendorProductPrice() {
		
	}


	@GwtIncompatible
	private void saveVendorPriceList()
	{
		ArrayList<SuperModel> saveArray=new ArrayList<SuperModel>();
		
		for(int i=0;i<this.getPriceInfo().size();i++)
		{
			VendorPriceListDetails vpld=new VendorPriceListDetails();
			vpld.setCompanyId(getCompanyId());
			vpld.setPriceListId(this.getCount());
			vpld.setPriceListTitle(this.getPricelistTitle());
			vpld.setVendorId(this.getPriceInfo().get(i).getVendorID());
			vpld.setVendorName(this.getPriceInfo().get(i).getFullName());
			vpld.setProductId(this.getProdID());
			vpld.setProductName(this.getProdName());
			vpld.setProductCode(this.getProdCode());
			vpld.setProductCategory(this.getProdCategory());
			vpld.setProductPrice(this.getPriceInfo().get(i).getProductPrice());
			
			saveArray.add(vpld);
		}
		
		if(saveArray.size()!=0)
		{
			GenricServiceImpl impl=new GenricServiceImpl();
			impl.save(saveArray);
		}
	}
	
	@GwtIncompatible
	private void updateVendorPriceList()
	{
		int prodInvDetailsLis=ofy().load().type(VendorPriceListDetails.class).filter("priceListId", this.getCount()).filter("companyId", getCompanyId()).count();
		if(prodInvDetailsLis==1)
		{
			VendorPriceListDetails prodInvDetails=ofy().load().type(VendorPriceListDetails.class).filter("priceListId", this.getCount()).filter("companyId", getCompanyId()).first().now();
			
			if(prodInvDetails!=null){
				ofy().delete().entity(prodInvDetails);
				
				saveVendorPriceList();
			}
		}
		
		if(prodInvDetailsLis>1)
		{
			List<VendorPriceListDetails> prodInvDetails=ofy().load().type(VendorPriceListDetails.class).filter("priceListId", this.getCount()).filter("companyId", getCompanyId()).list();
			System.out.println("Size Of Total Detials"+prodInvDetails.size());
			
			for(int i=0;i<prodInvDetails.size();i++)
			{
				ofy().delete().entity(prodInvDetails.get(i));
			}
			
			saveVendorPriceList();
		}
	}
	

	
	/************************************Getters and Setters********************************/
	
	/**
	 * Gets the prod id.
	 *
	 * @return the prod id
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Product ID")
	public int getProdID() {
		return prodID;
	}

	public String getUnitOfmeasurement() {
		return unitOfmeasurement;
	}


	public void setUnitOfmeasurement(String unitOfmeasurement) {
		this.unitOfmeasurement = unitOfmeasurement;
	}


	public String getPricelistTitle() {
		return pricelistTitle;
	}


	public void setPricelistTitle(String pricelistTitle) {
		if(pricelistTitle!=null)
		this.pricelistTitle = pricelistTitle.trim();
	}


	/**
	 * Sets the prod id.
	 *
	 * @param prodID the new prod id
	 */
	public void setProdID(int prodID) {
		this.prodID = prodID;
	}

	/**
	 * Gets the prod category.
	 *
	 * @return the prod category
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Product Category")
	public String getProdCategory() {
		return prodCategory;
	}

	/**
	 * Sets the prod category.
	 *
	 * @param prodCategory the new prod category
	 */
	public void setProdCategory(String prodCategory) {
		if(prodCategory!=null)
		this.prodCategory = prodCategory.trim();
	}

	/**
	 * Gets the prod code.
	 *
	 * @return the prod code
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Product Code")
	public String getProdCode() {
		return prodCode;
	}

	/**
	 * Sets the prod code.
	 *
	 * @param prodCode the new prod code
	 */
	public void setProdCode(String prodCode) {
		if(prodCode!=null)
		this.prodCode = prodCode.trim();
	}

	/**
	 * Gets the prod name.
	 *
	 * @return the prod name
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Product Name")
	public String getProdName() {
		return prodName;
	}

	/**
	 * Sets the prod name.
	 *
	 * @param prodName the new prod name
	 */
	public void setProdName(String prodName) {
		if(prodName!=null)
		this.prodName = prodName.trim();
	}

	/**
	 * Gets the prod price.
	 *
	 * @return the prod price
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Price")
	public double getProdPrice() {
		return prodPrice;
	}

	/**
	 * Sets the prod price.
	 *
	 * @param prodPrice the new prod price
	 */
	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}


	/**
	 * Gets the price info.
	 *
	 * @return the price info
	 */
	public List<PriceListDetails> getPriceInfo() {
		return priceInfo;
	}

	/**
	 * Sets the price info.
	 *
	 * @param priceInfo the new price info
	 */
	public void setPriceInfo(List<PriceListDetails> priceInfo) {
		ArrayList<PriceListDetails> arrPriceLis=new ArrayList<PriceListDetails>();
		arrPriceLis.addAll(priceInfo);
		this.priceInfo = arrPriceLis;
	}

	
	

	
	public Double getServiceTax() {
		return serviceTax;
	}


	public void setServiceTax(Double serviceTax) {
		this.serviceTax = serviceTax;
	}

	

	public Double getVat() {
		return vat;
	}


	public void setVat(Double vat) {
		this.vat = vat;
	}


	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		//return prodName.toString(); 
		return this.prodName.toString();
	}

	/**
	 * Gets the vendorname.
	 *
	 * @return the vendorname
	 */
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Vendor Name")
	public String getVendorname()
	{
		String name="";
		int i=0;
		for(PriceListDetails list:priceInfo)
		{
			 String li=list.getFullName();
			 name=list.getFullName();
		
		}
		return name;
	}



	public Key<SuperProduct> getProdKey() {
		return prodKey;
	}



	public void setProdKey(Key<SuperProduct> prodKey) {
		this.prodKey = prodKey;
	}


	public Double getPercDiscount() {
		return percDiscount;
	}


	public void setPercDiscount(double percDiscount) {
		this.percDiscount = percDiscount;
	}


	public ArrayList<PriceListDetails> getVendorPriceChangeHistory() {
		return vendorPriceChangeHistory;
	}


	public void setVendorPriceChangeHistory(ArrayList<PriceListDetails> vendorPriceChangeHistory) {
		
		ArrayList<PriceListDetails> historyPriceDetails = new ArrayList<PriceListDetails>();
		historyPriceDetails.addAll(vendorPriceChangeHistory);
		this.vendorPriceChangeHistory = historyPriceDetails;
	}
	
}
