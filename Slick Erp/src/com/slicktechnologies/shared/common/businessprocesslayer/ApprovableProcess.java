package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.Date;

public interface ApprovableProcess {

	String getStatus();

	String getEmployee();

	String getApproverName();

	int getCount();

	String getBranch();

	void setStatus(String status);

	void reactOnApproval();
	
	void reactOnRejected();
	
	public void setApprovalDate(Date date) ;
	
	/**
	 * Remark Added. Changes By Dipak 15 May 2015  
	 */
	String getRemark();
	public void setRemark(String remark);

}
