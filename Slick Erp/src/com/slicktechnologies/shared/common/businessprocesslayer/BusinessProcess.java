package com.slicktechnologies.shared.common.businessprocesslayer;

import java.util.Date;
/**
 * Represents all BuisnessProcess in System.
 */
public interface BusinessProcess 
{
	/**
	 * Gets the branch.
	 * @return the branch
	 */
	public String getBranch();
	
	/**
	 * Gets the employee.
	 * @return the employee
	 */
	public String getEmployee();
	
	/**
	 * Gets the status.
	 * @return the status
	 */
	public String getStatus();
	
	/**
	 * Gets the creation date.
	 * @return the creation date
	 */
	public Date getCreationDate();
	
	/**
	 * Gets the approver name who approved the business process.
	 * @return the creation date
	 */
	public String getApproverName();
	
	/**
	 * Gets the approval date of the business process.
	 * 	 * @return the creation date
	 */
	public Date getApprovalDate();
	
	/**
	 * Gets the Creation time.
	 * @return the time
	 */
	public String getCreationTime();
	
	/**
	 * Gets the Approval time.
	 * @return the time
	 */
	public String getApprovalTime();
	
	/**
	 * Sets the branch.
	 * @param branch the new branch
	 */
	public void setBranch(String branch);
	
	/**
	 * Sets the employee.
	 * @param employee the new employee
	 */
	public void setEmployee(String employee);
	
	/**
	 * Sets the status.
	 * @param status the new status
	 */
	public void setStatus(String status);
	
	/**
	 * Sets the creation date.
	 * @param date the new creation date
	 */
	public void setCreationDate(Date date);
	
	/**
	 * Sets the approver name of the business process.
	 * @param name the new approver name
	 */
	public void setApproverName(String name);
	
	/**
	 * Sets the approval date of the business process.
	 * @param date the new creation date
	 */
	public void setApprovalDate(Date date);
	
	
	/**
	 * Sets the Creation time.
	 * @param time the new time
	 */
	public void setCreationTime(String time);
	
	/**
	 * Sets the approval time.
	 * @param time the new time
	 */
	public void setApprovalTime(String time);
	
	
	/**
	 * Gets the group.
	 * @return the group
	 * 	 */
	public String getGroup();
	
	/**
	 * Sets the group for the business process
	 * @param group the new group
	 */
	public void setGroup(String group);
	
	/**
	 * Gets the category of the business process
	 * @return the category
	 */
	public String getCategory();
	
	/**
	 * Sets the category of the business process
	 * @param category the new category
	 */
	public void setCategory(String category);
	
	/**
	 * Gets the type corresponding to the business process
	 * @return the type
	 */
	public String getType();
	
	/**
	 * Sets the type of the business process
	 * @param type the new type
	 */
	public void setType(String type);
}
