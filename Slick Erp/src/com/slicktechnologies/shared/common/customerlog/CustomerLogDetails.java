package com.slicktechnologies.shared.common.customerlog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
@Embed
@Entity
public class CustomerLogDetails extends SuperModel {

	private static final long serialVersionUID = 7177301498857187166L; 	
	public static final String CREATED = "Created";//When user create call log and will be displayed in Backoffice view
//	public static final String REGISTERED = "Registered";
	public static final String ONHOLD = "On Hold";//
//	public static final String ALLOCATED = "Allocated";
	public static final String CANCELLED = "Cancelled";////When user cancel the call log
	public static final String MATERIALPENDNG = "Material Pending";//When backoffice make log status as Material Pendingthen it  will be displayed in Warehouse  view
	public static final String MATERIAISSUED = "Material Issued";//WareHouse person enter the material details and then status is automatically changed as "Material Issued" and then it will be diplayed to Account
	public static final String COMPLETED = "Completed";//Account people will add the payment and Mark Completed log .In backend invoice will be created.
	public static final String CLOSED = "Closed";
	
	@Index 
	protected PersonInfo cinfo;
	@Index
	protected Address address;
	@Index
	protected PaymentInfo paymentInfo;
	protected Long cellNumber2;
    @Index
    protected String complaintNumber;
    @Index
    protected String callType;
   
//    protected String priority;
    @Index
    protected String status;
//    @Index 
//    protected Long customerRelationshipId; 
    @Index
    protected String modelNo;
//    @Index
//    protected Date purchaseDate;
    @Index 
    protected String warrantyStatus; //in confihuration value should be "Warranty" and "Out Of Warranty"
//    @Index
//    protected String productGroup;
    @Index 
    protected String product;
    @Index
    protected String prodSerialNumber;
//    protected String symptom;
//    protected String ticketSource;
//    @Index
//    protected Date complaintDate;   
//    @Index
//    protected String complaintTime;
    @Index
    protected Date appointmentDate; 
  //  protected String appointmentTime;    
    @Index
    protected Date closingDate; 
    @Index 
    protected String technician;
    @Index
    protected String companyName;
    @Index
    protected Date createdOn;
    @Index
    protected String callbackStatus;
    @Index
    protected String paymentStatus;
    @Index
    protected boolean isCallback = false;
   
    protected String remark;
    protected Boolean recordSelect;
    @Index
    protected boolean issueFlag;
    @Index
    protected boolean returnFlag;
    @Index
    protected boolean defectiveFlag;
    @Index
    protected String toWarehouse;
    @Index 
    protected String toLocation;
    @Index
    protected String toBin;
    @Index
    protected String bookNo;
    @Index
    protected String tcrNo;
    @Index
    protected String paymentMethod;
    @Index
    protected String visitStatus;
    @Index
    protected boolean isReapeatCall;
    protected boolean schemeFlag;
    protected double schemeAmount;
 
    protected List<MaterialRequired> issueMaterial;
    protected List<MaterialRequired> defectiveMaterial;
    protected List<MaterialRequired> returnMaterial;
    protected List<MaterialRequired> serviceCharges;
    protected List<MaterialRequired> companyCost;
    protected List<MaterialRequired> customerCost;
    
    /** date 6.7.2018 added by komal for technician remark **/
    @Index
    protected String resolution;
    public CustomerLogDetails(){
    	cinfo = new PersonInfo();
    	address = new Address();
    	paymentInfo = new PaymentInfo();
    	issueMaterial = new ArrayList<MaterialRequired>();
    	defectiveMaterial = new ArrayList<MaterialRequired>();
    	returnMaterial = new ArrayList<MaterialRequired>();
    	serviceCharges = new ArrayList<MaterialRequired>();
    	companyCost  = new ArrayList<MaterialRequired>();
    	customerCost = new ArrayList<MaterialRequired>();
    	complaintNumber ="";
    	callType = "";
    	status = "";
    	warrantyStatus ="";
    	prodSerialNumber ="";
    	technician = "";
    	companyName = "";
    	bookNo ="";
    	tcrNo ="";
    	remark = "";
    	isReapeatCall=false;
    	issueFlag =false;
    	returnFlag =false;
    	defectiveFlag =false;
    	schemeFlag = false;
    	 /** date 6.7.2018 added by komal for technician remark **/
    	resolution = "";
    }
	public PersonInfo getCinfo() {
		return cinfo;
	}
	public void setCinfo(PersonInfo cinfo) {
		this.cinfo = cinfo;
	}
	public Address getAddress() {
		return address;
	}
    public void setAddress(Address address) {
		this.address = address;
	}
	public String getComplaintNumber() {
		return complaintNumber;
	}
	public void setComplaintNumber(String complaintNumber) {
		this.complaintNumber = complaintNumber;
	}	
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getWarrantyStatus() {
		return warrantyStatus;
	}
	public void setWarrantyStatus(String warrantyStatus) {
		this.warrantyStatus = warrantyStatus;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getCellNumber2() {
		return cellNumber2;
	}
	public void setCellNumber2(Long cellNumber2) {
		this.cellNumber2 = cellNumber2;
	}
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getTechnician() {
		return technician;
	}
	public void setTechnician(String technician) {
		this.technician = technician;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCallbackStatus() {
		return callbackStatus;
	}
	public void setCallbackStatus(String callbackStatus) {
		this.callbackStatus = callbackStatus;
	}
	public boolean isCallback() {
		return isCallback;
	}
	public void setCallback(boolean isCallback) {
		this.isCallback = isCallback;
	}
	
	public Boolean getRecordSelect() {
		return recordSelect;
	}
	public void setRecordSelect(Boolean recordSelect) {
		this.recordSelect = recordSelect;
	}
	public boolean isIssueFlag() {
		return issueFlag;
	}
	public void setIssueFlag(boolean issueFlag) {
		this.issueFlag = issueFlag;
	}
	public boolean isReturnFlag() {
		return returnFlag;
	}
	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}
	public boolean isDefectiveFlag() {
		return defectiveFlag;
	}
	public void setDefectiveFlag(boolean defectiveFlag) {
		this.defectiveFlag = defectiveFlag;
	}
	public List<MaterialRequired> getIssueMaterial() {
		return issueMaterial;
	}
	public void setIssueMaterial(List<MaterialRequired> issueMaterial) {
		this.issueMaterial = issueMaterial;
	}
	public List<MaterialRequired> getDefectiveMaterial() {
		return defectiveMaterial;
	}
	public void setDefectiveMaterial(List<MaterialRequired> defectiveMaterial) {
		this.defectiveMaterial = defectiveMaterial;
	}
	public List<MaterialRequired> getReturnMaterial() {
		return returnMaterial;
	}
	public void setReturnMaterial(List<MaterialRequired> returnMaterial) {
		this.returnMaterial = returnMaterial;
	}
	public String getProdSerialNumber() {
		if(this.prodSerialNumber!=null)
			return this.prodSerialNumber;
		else{
			return "";
		}
	}
	public void setProdSerialNumber(String prodSerialNumber) {
		this.prodSerialNumber = prodSerialNumber;
	}	
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}	
	public String getToWarehouse() {
		return toWarehouse;
	}
	public void setToWarehouse(String toWarehouse) {
		this.toWarehouse = toWarehouse;
	}
	public String getToLocation() {
		return toLocation;
	}
	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}
	public String getToBin() {
		return toBin;
	}
	public void setToBin(String toBin) {
		this.toBin = toBin;
	}
	public List<MaterialRequired> getServiceCharges() {
		return serviceCharges;
	}
	public void setServiceCharges(List<MaterialRequired> serviceCharges) {
		this.serviceCharges = serviceCharges;
	}
	public String getBookNo() {
		return bookNo;
	}
	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}
	public String getTcrNo() {
		return tcrNo;
	}
	public void setTcrNo(String tcrNo) {
		this.tcrNo = tcrNo;
	}
	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}
	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getModelNo() {
		if(modelNo!=null){
			return modelNo;
		}else{
			return "";
		}
		
	}
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getVisitStatus() {
		return visitStatus;
	}
	public void setVisitStatus(String visitStatus) {
		this.visitStatus = visitStatus;
	}
	
	public boolean isReapeatCall() {
		return isReapeatCall;
	}
	public void setReapeatCall(boolean isReapeatCall) {
		this.isReapeatCall = isReapeatCall;
	}
	
	public List<MaterialRequired> getCompanyCost() {
		return companyCost;
	}
	public void setCompanyCost(List<MaterialRequired> companyCost) {
		this.companyCost = companyCost;
	}
	public List<MaterialRequired> getCustomerCost() {
		return customerCost;
	}
	public void setCustomerCost(List<MaterialRequired> customerCost) {
		this.customerCost = customerCost;
	}
	
	public boolean isSchemeFlag() {
		return schemeFlag;
	}
	public void setSchemeFlag(boolean schemeFlag) {
		this.schemeFlag = schemeFlag;
	}
	public double getSchemeAmount() {
		return schemeAmount;
	}
	public void setSchemeAmount(double schemeAmount) {
		this.schemeAmount = schemeAmount;
	}
	
	public Date getClosingDate() {
		return closingDate;
	}
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}
	
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
