package com.slicktechnologies.shared.common.customerlog;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.SalesLineItem;
@Embed
public class MaterialRequired extends SalesLineItem{
	private static final long serialVersionUID = 269203280126950224L;
	@Index
	protected int productId;
	@Index
	protected String productCode;
	@Index
	protected String productName;
	@Index
	protected String productCategory;
	@Index
	protected double productAvailableQty;
	@Index
	protected double productRequestQty;
	@Index
	protected double productIssueQty;
	@Index
	protected double productReturnQty;
	@Index
	protected double productPrice;
	@Index
	protected String productUOM;
	@Index
	protected String materialProductWarehouse;
	@Index
	protected String materialProductStorageLocation;
	@Index
	protected String materialProductStorageBin;
    @Index
    protected double productPayableQuantity;
    @Index
    protected boolean flag;
	@Index
	protected boolean isMaterialCredit;
	//protected double discountAmt; 
	protected double taxAmount;
	public MaterialRequired() {
		productCode="";
		productCategory="";
		productName="";
		productUOM="";
		taxAmount = 0.0d;
		flag = false;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	public String getProductUOM() {
		return productUOM;
	}
	public void setProductUOM(String productUOM) {
		this.productUOM = productUOM;
	}

	public double getProductAvailableQty() {
		return productAvailableQty;
	}
	public void setProductAvailableQty(double productAvailableQty) {
		this.productAvailableQty = productAvailableQty;
	}
	public double getProductRequestQty() {
		return productRequestQty;
	}
	public void setProductRequestQty(double productRequestQty) {
		this.productRequestQty = productRequestQty;
	}
	public double getProductIssueQty() {
		return productIssueQty;
	}
	public void setProductIssueQty(double productIssueQty) {
		this.productIssueQty = productIssueQty;
	}
	public double getProductReturnQty() {
		return productReturnQty;
	}
	public void setProductReturnQty(double productReturnQty) {
		this.productReturnQty = productReturnQty;
	}
	public String getMaterialProductWarehouse() {
		return materialProductWarehouse;
	}
	public void setMaterialProductWarehouse(String materialProductWarehouse) {
		this.materialProductWarehouse = materialProductWarehouse;
	}
	public String getMaterialProductStorageLocation() {
		return materialProductStorageLocation;
	}
	public void setMaterialProductStorageLocation(
			String materialProductStorageLocation) {
		this.materialProductStorageLocation = materialProductStorageLocation;
	}
	public String getMaterialProductStorageBin() {
		return materialProductStorageBin;
	}
	public void setMaterialProductStorageBin(String materialProductStorageBin) {
		this.materialProductStorageBin = materialProductStorageBin;
	}
	public double getProductPayableQuantity() {
		return productPayableQuantity;
	}
	public void setProductPayableQuantity(double productPayableQuantity) {
		this.productPayableQuantity = productPayableQuantity;
	}
	
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
		
//	public double getDiscountAmt() {
//		return discountAmt;
//	}
//	public void setDiscountAmt(double discountAmt) {
//		this.discountAmt = discountAmt;
//	}

	public boolean isMaterialCredit() {
		return isMaterialCredit;
	}
	public void setMaterialCredit(boolean isMaterialCredit) {
		this.isMaterialCredit = isMaterialCredit;
	}
	
	public double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}
	@Override
	public String toString() {
		return "MaterialRequired [productId=" + productId + ", productCode="
				+ productCode + ", productName=" + productName
				+ ", productCategory=" + productCategory
				+ ", productAvailableQty=" + productAvailableQty
				+ ", productRequestQty=" + productRequestQty
				+ ", productIssueQty=" + productIssueQty
				+ ", productReturnQty=" + productReturnQty + ", productPrice="
				+ productPrice + ", productUOM=" + productUOM
				+ ", materialProductWarehouse=" + materialProductWarehouse
				+ ", materialProductStorageLocation="
				+ materialProductStorageLocation
				+ ", materialProductStorageBin=" + materialProductStorageBin
				+ ", productPayableQuantity=" + productPayableQuantity + "]";
	}

}
