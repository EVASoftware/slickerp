package com.slicktechnologies.shared.common.customerlog;
import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;


/**
 * The PersonInfo Represents the Person's identity and cell no. (will get replaced by contactInfo)
 */
@SuppressWarnings("serial")
@Embed
@Index
public class PaymentInfo implements Serializable
{	
	/** The bank name. */
	protected String bankName;
	
	/** The bank branch. */
	protected String bankBranch;
	
	/** The bank acc no. */
	protected String bankAccNo;
	
	/** The cheque no. */
	protected String chequeNo;
	
	/** The cheque date. */
	protected Date chequeDate;
		
	/** The amount transfer date. */
	protected Date amountTransferDate;
	
	/** The reference no. */
	protected String referenceNo;
	
	/** The payment received. */
	protected double paymentReceived;
	
	/** balance payment amount **/
	protected double balancePayment;
	/** The payment amount */
	protected double paymentAmt;
	
	protected double discountAmount;
	protected String chequeIssuedBy;
	
	protected double netPay;

	public PaymentInfo(){
		discountAmount = 0.0d;
		balancePayment = 0.0d;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getBankAccNo() {
		return bankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public Date getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public double getPaymentReceived() {
		return paymentReceived;
	}
	public void setPaymentReceived(double paymentReceived) {
		this.paymentReceived = paymentReceived;
	}
	public double getPaymentAmt() {
		return paymentAmt;
	}
	public void setPaymentAmt(double paymentAmt) {
		this.paymentAmt = paymentAmt;
	}
	public String getChequeIssuedBy() {
		return chequeIssuedBy;
	}
	public void setChequeIssuedBy(String chequeIssuedBy) {
		this.chequeIssuedBy = chequeIssuedBy;
	}
	public double getNetPay() {
		return netPay;
	}
	public void setNetPay(double netPay) {
		this.netPay = netPay;
	}
	public Date getAmountTransferDate() {
		return amountTransferDate;
	}
	public void setAmountTransferDate(Date amountTransferDate) {
		this.amountTransferDate = amountTransferDate;
	}
	public double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public double getBalancePayment() {
		return balancePayment;
	}
	public void setBalancePayment(double balancePayment) {
		this.balancePayment = balancePayment;
	}
	
}

