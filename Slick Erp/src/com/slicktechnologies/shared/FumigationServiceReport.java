package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class FumigationServiceReport extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2700690549850314351L;
	@Index
	int fumigationServiceId;
	@Index
	int prophylacticServiceId;
	@Index
	int deggasingServiceId;
	@Index
	Date fumigationServiceDate;
	@Index
	Date fumigationServiceCompletionDate;
	@Index
	Date prophylacticServiceDate;
	@Index
	Date prophylacticServiceCompletionDate;
	@Index
	Date deggasingServiceDate;
	@Index
	Date deggasingServiceCompletionDate;
	@Index
	String warehouseName;
	@Index
	String warehouseCode;
	@Index
	String compartment;
	@Index
	String shade;
	@Index
	String stackNumber;
	double stackQuantity;
	@Index
	String fumigationStatus;
	@Index
	Date fumigationStatusDate;
	@Index
	String successFailureBy;
	String fumigationRemark;
	@Index
	String prophylacticStatus;
	@Index
	String degassingStatus;
	@Index
	Date suspendedDate;
	String suspendedBy;
	String suspendedRemark;
	@Index
	int contractId;
	String fumigationCompletionRemark;
	String prophylacticCompletionRemark;
	String degassingCompletionRemark;
	String fumigationTechnicianName;
	String prophylacticTechnicianName;
	String degassingTechnicianName;
	int rescheduleCount;
	/** below for Fumigation Service status updation for eg. schedule/Completed **/
	String fumigationServiceStatus;
	/** below for original Fumigation marked as failure and then updated its 
	 * id to original fumigation service id
	 * Note :- this will applicable only for marked as failure fumigation service
	 */
	int originalfumigationId;
	
	public FumigationServiceReport(){
		warehouseName ="";
		warehouseCode="";
		compartment ="";
		shade = "";
		stackNumber ="";
		fumigationStatus ="";
		prophylacticStatus = "Scheduled";
		degassingStatus = "";
		suspendedBy ="";
		successFailureBy = "";
		fumigationRemark ="";
		suspendedRemark ="";
		fumigationCompletionRemark="";
		prophylacticCompletionRemark="";
		degassingCompletionRemark ="";		
		fumigationTechnicianName = "";
		prophylacticTechnicianName ="";
		degassingTechnicianName ="";
		fumigationServiceStatus="";
	}
	
	

	public int getFumigationServiceId() {
		return fumigationServiceId;
	}

	public void setFumigationServiceId(int fumigationServiceId) {
		this.fumigationServiceId = fumigationServiceId;
	}

	public int getProphylacticServiceId() {
		return prophylacticServiceId;
	}

	public void setProphylacticServiceId(int prophylacticServiceId) {
		this.prophylacticServiceId = prophylacticServiceId;
	}


	public Date getFumigationServiceDate() {
		return fumigationServiceDate;
	}

	public void setFumigationServiceDate(Date fumigationServiceDate) {
		this.fumigationServiceDate = fumigationServiceDate;
	}

	public Date getFumigationServiceCompletionDate() {
		return fumigationServiceCompletionDate;
	}

	public void setFumigationServiceCompletionDate(Date fumigationServiceCompletionDate) {
		this.fumigationServiceCompletionDate = fumigationServiceCompletionDate;
	}

	public Date getProphylacticServiceDate() {
		return prophylacticServiceDate;
	}

	public void setProphylacticServiceDate(Date prophylacticServiceDate) {
		this.prophylacticServiceDate = prophylacticServiceDate;
	}

	public Date getProphylacticServiceCompletionDate() {
		return prophylacticServiceCompletionDate;
	}

	public void setProphylacticServiceCompletionDate(Date prophylacticServiceCompletionDate) {
		this.prophylacticServiceCompletionDate = prophylacticServiceCompletionDate;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getCompartment() {
		return compartment;
	}

	public void setCompartment(String compartment) {
		this.compartment = compartment;
	}

	public String getShade() {
		return shade;
	}

	public void setShade(String shade) {
		this.shade = shade;
	}

	public String getStackNumber() {
		return stackNumber;
	}

	public void setStackNumber(String stackNumber) {
		this.stackNumber = stackNumber;
	}

	public double getStackQuantity() {
		return stackQuantity;
	}

	public void setStackQuantity(double stackQuantity) {
		this.stackQuantity = stackQuantity;
	}
	
	
	
	public String getFumigationStatus() {
		return fumigationStatus;
	}



	public void setFumigationStatus(String fumigationStatus) {
		this.fumigationStatus = fumigationStatus;
	}



	public Date getFumigationStatusDate() {
		return fumigationStatusDate;
	}



	public void setFumigationStatusDate(Date fumigationStatusDate) {
		this.fumigationStatusDate = fumigationStatusDate;
	}



	public String getProphylacticStatus() {
		return prophylacticStatus;
	}



	public void setProphylacticStatus(String prophylacticStatus) {
		this.prophylacticStatus = prophylacticStatus;
	}



	public String getDegassingStatus() {
		return degassingStatus;
	}



	public void setDegassingStatus(String degassingStatus) {
		this.degassingStatus = degassingStatus;
	}



	public String getSuspendedBy() {
		return suspendedBy;
	}



	public void setSuspendedBy(String suspendedBy) {
		this.suspendedBy = suspendedBy;
	}



	public Date getSuspendedDate() {
		return suspendedDate;
	}



	public void setSuspendedDate(Date suspendedDate) {
		this.suspendedDate = suspendedDate;
	}



	public String getSuccessFailureBy() {
		return successFailureBy;
	}



	public void setSuccessFailureBy(String successFailureBy) {
		this.successFailureBy = successFailureBy;
	}
 


	public String getFumigationRemark() {
		return fumigationRemark;
	}



	public void setFumigationRemark(String fumigationRemark) {
		this.fumigationRemark = fumigationRemark;
	}



	public String getSuspendedRemark() {
		return suspendedRemark;
	}



	public void setSuspendedRemark(String suspendedRemark) {
		this.suspendedRemark = suspendedRemark;
	}



	public int getContractId() {
		return contractId;
	}



	public void setContractId(int contractId) {
		this.contractId = contractId;
	}



	public String getFumigationCompletionRemark() {
		return fumigationCompletionRemark;
	}



	public void setFumigationCompletionRemark(String fumigationCompletionRemark) {
		this.fumigationCompletionRemark = fumigationCompletionRemark;
	}



	public String getProphylacticCompletionRemark() {
		return prophylacticCompletionRemark;
	}



	public void setProphylacticCompletionRemark(String prophylacticCompletionRemark) {
		this.prophylacticCompletionRemark = prophylacticCompletionRemark;
	}



	public String getDegassingCompletionRemark() {
		return degassingCompletionRemark;
	}



	public void setDegassingCompletionRemark(String degassingCompletionRemark) {
		this.degassingCompletionRemark = degassingCompletionRemark;
	}



	public int getDeggasingServiceId() {
		return deggasingServiceId;
	}



	public void setDeggasingServiceId(int deggasingServiceId) {
		this.deggasingServiceId = deggasingServiceId;
	}



	public Date getDeggasingServiceDate() {
		return deggasingServiceDate;
	}



	public void setDeggasingServiceDate(Date deggasingServiceDate) {
		this.deggasingServiceDate = deggasingServiceDate;
	}



	public Date getDeggasingServiceCompletionDate() {
		return deggasingServiceCompletionDate;
	}



	public void setDeggasingServiceCompletionDate(Date deggasingServiceCompletionDate) {
		this.deggasingServiceCompletionDate = deggasingServiceCompletionDate;
	}



	



	public String getProphylacticTechnicianName() {
		return prophylacticTechnicianName;
	}



	public void setProphylacticTechnicianName(String prophylacticTechnicianName) {
		this.prophylacticTechnicianName = prophylacticTechnicianName;
	}



	public String getDegassingTechnicianName() {
		return degassingTechnicianName;
	}



	public void setDegassingTechnicianName(String degassingTechnicianName) {
		this.degassingTechnicianName = degassingTechnicianName;
	}



	public String getFumigationTechnicianName() {
		return fumigationTechnicianName;
	}



	public void setFumigationTechnicianName(String fumigationTechnicianName) {
		this.fumigationTechnicianName = fumigationTechnicianName;
	}



	public int getRescheduleCount() {
		return rescheduleCount;
	}



	public void setRescheduleCount(int rescheduleCount) {
		this.rescheduleCount = rescheduleCount;
	}



	public String getFumigationServiceStatus() {
		return fumigationServiceStatus;
	}



	public void setFumigationServiceStatus(String fumigationServiceStatus) {
		this.fumigationServiceStatus = fumigationServiceStatus;
	}



	public int getOriginalfumigationId() {
		return originalfumigationId;
	}



	public void setOriginalfumigationId(int originalfumigationId) {
		this.originalfumigationId = originalfumigationId;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
