package com.slicktechnologies.shared;

import java.util.Date;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

// TODO: Auto-generated Javadoc
/**
 * The Class PettyCashDeposits.
 */
@Entity
@Index
public class PettyCashDeposits extends PettyCash {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3465441464021781738L;
	
	/** The deposit date. */
	protected Date depositDate;
	
	/** The deposit time. */
	protected String depositTime;
	
//	/** The deposit amount. */
//	protected int depositAmount;
	
	/**
	 * Date 4/03/2017
	 * added bu vijay
	 * for while paymnet document summited and
	 * if its deposited in peety cash then 
	 * its payment doc id stored here
	 */
	@Index
	protected int paymentdocId;
	/**
	 * ends here
	 */
	
	//08-08-2017 changed by vijay
	protected double depositAmount;
	
	
	protected String description; //Ashwini Patil Date:19-04-2024 for ultra
		

	/**
	 * Instantiates a new petty cash deposits.
	 */
	public PettyCashDeposits() {
		super();
		depositDate=new Date();
		depositTime="";
		description="";
		paymentdocId=-2;
	}


	/**
	 * Gets the deposit date.
	 *
	 * @return the deposit date
	 */
	public Date getDepositDate() {
		return depositDate;
	}

	/**
	 * Sets the deposit date.
	 *
	 * @param depositDate the new deposit date
	 */
	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	/**
	 * Gets the deposit time.
	 *
	 * @return the deposit time
	 */
	public String getDepositTime() {
		return depositTime;
	}

	/**
	 * Sets the deposit time.
	 *
	 * @param depositTime the new deposit time
	 */
	public void setDepositTime(String depositTime) {
		if(depositTime!=null)
		this.depositTime = depositTime.trim();
	}

//	/**
//	 * Gets the deposit amount.
//	 *
//	 * @return the deposit amount
//	 */
//	public Integer getDepositAmount() {
//		return depositAmount;
//	}
//
//	/**
//	 * Sets the deposit amount.
//	 *
//	 * @param depositAmount the new deposit amount
//	 */
//	public void setDepositAmount(int depositAmount) {
//		this.depositAmount = depositAmount;
//	}

	public int getPaymentdocId() {
		return paymentdocId;
	}

	public void setPaymentdocId(int paymentdocId) {
		this.paymentdocId = paymentdocId;
	}
	
	public Double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
