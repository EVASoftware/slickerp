package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class GeneralKeyValueBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7272619419323613952L;
	String name;
	double value;
	String groupName;
	boolean status;
	
	public GeneralKeyValueBean(){
		name = "";
		groupName = "";
		status = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
