package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class CommodityFumigationDetails extends SuperModel{
	
	/**
	 * nidhi
	 * 18-11-2017
	 * class for download service fumigation report
	 * and also for contract revanue report
	 * 
	 * 
	 */
	private static final long serialVersionUID = -5675386628428839770L;

	
	protected double dayWiseTotal ;
	protected String branchName;
	protected double monthWiseTotal;
	protected String prodCode;
	protected Date date;
	protected String proName;
	
	/**
	 * Date 02-09-2018 By Vijay
	 * Des :- NBHC CCPM Report need in daily waise summary details
	 */
	protected Date summaryDate;
	protected String status;
	
	/**
	 * ends here
	 */
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	public double getDayWiseTotal() {
		return dayWiseTotal;
	}


	public void setDayWiseTotal(double dayWiseTotal) {
		this.dayWiseTotal = dayWiseTotal;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public double getMonthWiseTotal() {
		return monthWiseTotal;
	}


	public void setMonthWiseTotal(double monthWiseTotal) {
		this.monthWiseTotal = monthWiseTotal;
	}


	public String getProdCode() {
		return prodCode;
	}


	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getProName() {
		return proName;
	}


	public void setProName(String proName) {
		this.proName = proName;
	}
	

	public Date getSummaryDate() {
		return summaryDate;
	}


	public void setSummaryDate(Date summaryDate) {
		this.summaryDate = summaryDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
}
