package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class TallyInterfaceBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7388183038127953693L;
	String documentName ;
	String invoiceType;
	Date invoiceDate;
	String supplierInvoiceNumber;
	Date supplierDate;
	String invoiceNumber;
	String gstn;
	String placeToSupply;
	double TotalInvoiceAmount;
	List<String> ledgerNameList;
	List<String> groupNameList;
	List<String> hsnSacList;
	List<Double> amountList;
	List<Double> gstList;
	List<String> narrationList;
	Date  paymentDate;
	String bankAccountNo;
	Date chequeDate;
	String chequeNumber;
	String bankName;
	String billNumber;
	Date  expenseDate;
	String voucherNumber;
	String companyBankName;
	String sAddressLine1;
	String sAddressLine2;
	String sLandMark;
	String sLocality;
	String sPinNumber;
	String sCity;
	String sState;
	String sCountry;
	String bAddressLine1;
	String bAddressLine2;
	String bLandMark;
	String bLocality;
	String bPinNumber;
	String bCity;
	String bState;
	String bCountry;
	
	/*
	 * Added by Ashwini
	 * Date:6/10/2018
	 */
	String branch; 
	Long companyId;
	
	/*
	 * end by Ashwini
	 */
	
	/**
	 * @author Anil , Date : 30-05-2019
	 * for stock item wise tally entry
	 * for sasha
	 */
	List<String> stockItemNameList;
	List<String> stockGroupNameList;
	List<String> stockQtyList;
	List<String> stockRateList;
	
	/**
	 * @author Anil , 04-07-2019
	 */
	List<String> godownList;
	
	protected String documentStatus;
	protected String referenceNumber;
	protected String einvoiceNo;
	protected String einvoiceStatus;
	protected String einvoiceAckNo;
	protected String einvoiceAckDate;
	protected String einvoiceCancellationDate;
	protected String einvoiceQRCodeString;
	
	protected String numberRange;
	
	public TallyInterfaceBean(){
		ledgerNameList = new ArrayList<String>();
		groupNameList = new ArrayList<String>();
		hsnSacList = new ArrayList<String>();
		amountList = new ArrayList<Double>();
		gstList = new ArrayList<Double>();
		narrationList = new ArrayList<String>();
		
		documentStatus ="";
		referenceNumber="";
		einvoiceNo="";
		einvoiceStatus="";
		einvoiceAckNo="";
		einvoiceAckDate="";
		einvoiceCancellationDate="";
		einvoiceQRCodeString="";
		numberRange="";
	}
	
	
	
	public List<String> getGodownList() {
		return godownList;
	}



	public void setGodownList(List<String> godownList) {
		this.godownList = godownList;
	}



	public List<String> getStockItemNameList() {
		return stockItemNameList;
	}



	public void setStockItemNameList(List<String> stockItemNameList) {
		this.stockItemNameList = stockItemNameList;
	}



	public List<String> getStockGroupNameList() {
		return stockGroupNameList;
	}



	public void setStockGroupNameList(List<String> stockGroupNameList) {
		this.stockGroupNameList = stockGroupNameList;
	}



	public List<String> getStockQtyList() {
		return stockQtyList;
	}



	public void setStockQtyList(List<String> stockQtyList) {
		this.stockQtyList = stockQtyList;
	}



	public List<String> getStockRateList() {
		return stockRateList;
	}



	public void setStockRateList(List<String> stockRateList) {
		this.stockRateList = stockRateList;
	}



	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getSupplierInvoiceNumber() {
		return supplierInvoiceNumber;
	}
	public void setSupplierInvoiceNumber(String supplierInvoiceNumber) {
		this.supplierInvoiceNumber = supplierInvoiceNumber;
	}
	public Date getSupplierDate() {
		return supplierDate;
	}
	public void setSupplierDate(Date supplierDate) {
		this.supplierDate = supplierDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getGstn() {
		return gstn;
	}
	public void setGstn(String gstn) {
		this.gstn = gstn;
	}
	public String getPlaceToSupply() {
		return placeToSupply;
	}
	public void setPlaceToSupply(String placeToSupply) {
		this.placeToSupply = placeToSupply;
	}
	public double getTotalInvoiceAmount() {
		return TotalInvoiceAmount;
	}
	public void setTotalInvoiceAmount(double totalInvoiceAmount) {
		TotalInvoiceAmount = totalInvoiceAmount;
	}
	public List<String> getLedgerNameList() {
		return ledgerNameList;
	}
	public void setLedgerNameList(List<String> ledgerNameList) {
		this.ledgerNameList = ledgerNameList;
	}
	public List<String> getGroupNameList() {
		return groupNameList;
	}
	public void setGroupNameList(List<String> groupNameList) {
		this.groupNameList = groupNameList;
	}
	public List<String> getHsnSacList() {
		return hsnSacList;
	}
	public void setHsnSacList(List<String> hsnSacList) {
		this.hsnSacList = hsnSacList;
	}
	public List<Double> getAmountList() {
		return amountList;
	}
	public void setAmountList(List<Double> amountList) {
		this.amountList = amountList;
	}
	public List<Double> getGstList() {
		return gstList;
	}
	public void setGstList(List<Double> gstList) {
		this.gstList = gstList;
	}
	public List<String> getNarrationList() {
		return narrationList;
	}
	public void setNarrationList(List<String> narrationList) {
		this.narrationList = narrationList;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getBankAccountNo() {
		return bankAccountNo;
	}
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}
	public Date getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	public Date getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(Date expenseDate) {
		this.expenseDate = expenseDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCompanyBankName() {
		return companyBankName;
	}
	public void setCompanyBankName(String companyBankName) {
		this.companyBankName = companyBankName;
	}
	public String getsAddressLine1() {
		return sAddressLine1;
	}
	public void setsAddressLine1(String sAddressLine1) {
		this.sAddressLine1 = sAddressLine1;
	}
	public String getsAddressLine2() {
		return sAddressLine2;
	}
	public void setsAddressLine2(String sAddressLine2) {
		this.sAddressLine2 = sAddressLine2;
	}
	public String getsLandMark() {
		return sLandMark;
	}
	public void setsLandMark(String sLandMark) {
		this.sLandMark = sLandMark;
	}
	public String getsLocality() {
		return sLocality;
	}
	public void setsLocality(String sLocality) {
		this.sLocality = sLocality;
	}
	public String getsPinNumber() {
		return sPinNumber;
	}
	public void setsPinNumber(String sPinNumber) {
		this.sPinNumber = sPinNumber;
	}
	public String getsCity() {
		return sCity;
	}
	public void setsCity(String sCity) {
		this.sCity = sCity;
	}
	public String getsState() {
		return sState;
	}
	public void setsState(String sState) {
		this.sState = sState;
	}
	public String getsCountry() {
		return sCountry;
	}
	public void setsCountry(String sCountry) {
		this.sCountry = sCountry;
	}
	public String getbAddressLine1() {
		return bAddressLine1;
	}
	public void setbAddressLine1(String bAddressLine1) {
		this.bAddressLine1 = bAddressLine1;
	}
	public String getbAddressLine2() {
		return bAddressLine2;
	}
	public void setbAddressLine2(String bAddressLine2) {
		this.bAddressLine2 = bAddressLine2;
	}
	public String getbLandMark() {
		return bLandMark;
	}
	public void setbLandMark(String bLandMark) {
		this.bLandMark = bLandMark;
	}
	public String getbLocality() {
		return bLocality;
	}
	public void setbLocality(String bLocality) {
		this.bLocality = bLocality;
	}
	public String getbPinNumber() {
		return bPinNumber;
	}
	public void setbPinNumber(String bPinNumber) {
		this.bPinNumber = bPinNumber;
	}
	public String getbCity() {
		return bCity;
	}
	public void setbCity(String bCity) {
		this.bCity = bCity;
	}
	public String getbState() {
		return bState;
	}
	public void setbState(String bState) {
		this.bState = bState;
	}
	public String getbCountry() {
		return bCountry;
	}
	public void setbCountry(String bCountry) {
		this.bCountry = bCountry;
	}
	
	
	/*
	 * Added by Ashwini
	 */
	

	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		if(branch!=null){
			this.branch = branch.trim();
		}
		
		}
	
	public Long getCompanyId() {
		 
		return companyId;
	}
	
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/*
	 * End by Ashwini
	 */
	

	public String getDocumentStatus() {
		return documentStatus;
	}



	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}



	public String getReferenceNumber() {
		return referenceNumber;
	}



	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}



	public String getEinvoiceNo() {
		return einvoiceNo;
	}



	public void setEinvoiceNo(String einvoiceNo) {
		this.einvoiceNo = einvoiceNo;
	}



	public String getEinvoiceStatus() {
		return einvoiceStatus;
	}



	public void setEinvoiceStatus(String einvoiceStatus) {
		this.einvoiceStatus = einvoiceStatus;
	}



	public String getEinvoiceAckNo() {
		return einvoiceAckNo;
	}



	public void setEinvoiceAckNo(String einvoiceAckNo) {
		this.einvoiceAckNo = einvoiceAckNo;
	}



	public String getEinvoiceAckDate() {
		return einvoiceAckDate;
	}



	public void setEinvoiceAckDate(String einvoiceAckDate) {
		this.einvoiceAckDate = einvoiceAckDate;
	}



	public String getEinvoiceCancellationDate() {
		return einvoiceCancellationDate;
	}



	public void setEinvoiceCancellationDate(String einvoiceCancellationDate) {
		this.einvoiceCancellationDate = einvoiceCancellationDate;
	}



	public String getEinvoiceQRCodeString() {
		return einvoiceQRCodeString;
	}



	public void setEinvoiceQRCodeString(String einvoiceQRCodeString) {
		this.einvoiceQRCodeString = einvoiceQRCodeString;
	}



	public String getNumberRange() {
		return numberRange;
	}



	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}

	
	
	
}
