package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;

@Embed
public class EmployeeAssetBean implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7874297969411460038L;
	/**
	 * 
	 */
	protected String assetName;
	protected double price;
	@Index
	protected Date fromDate;
	@Index
	protected Date toDate;
	protected double qty;
	protected boolean recordSelect;
	/** date 14.9.2018 added by komal**/
	protected String assetCategory;
	protected double perUnitPrice;
	
	/***End***/
	
	/**
	 * @author Anil , Date : 10-07-2019
	 * Adding asset id field
	 */
	@Index
	int assetId;
	
	String warehouseName;
	String storageLocName;
	String storageLocBin;
	
	double availableQty;
	double deductQty;
	
	@Index
	String status;
	
	@Index
	Date date;
	
	@Index
	String remark;
	
	String uom;
	String assetCode;
	
	boolean isStockUpdated;
	
	/**
	 * @author Anil
	 * @since 12-11-2020
	 * Adding serial number 
	 */
	int srNo;
	
	public EmployeeAssetBean(){
		assetName = "";
		assetCategory = "";
	}
	
	
	

	public boolean isStockUpdated() {
		return isStockUpdated;
	}




	public void setStockUpdated(boolean isStockUpdated) {
		this.isStockUpdated = isStockUpdated;
	}




	public String getAssetCode() {
		return assetCode;
	}



	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}



	public String getUom() {
		return uom;
	}



	public void setUom(String uom) {
		this.uom = uom;
	}



	public double getDeductQty() {
		return deductQty;
	}



	public void setDeductQty(double deductQty) {
		this.deductQty = deductQty;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public int getAssetId() {
		return assetId;
	}



	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}

	
	


	public String getWarehouseName() {
		return warehouseName;
	}



	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}



	public String getStorageLocName() {
		return storageLocName;
	}



	public void setStorageLocName(String storageLocName) {
		this.storageLocName = storageLocName;
	}



	public String getStorageLocBin() {
		return storageLocBin;
	}



	public void setStorageLocBin(String storageLocBin) {
		this.storageLocBin = storageLocBin;
	}



	public double getAvailableQty() {
		return availableQty;
	}



	public void setAvailableQty(double availableQty) {
		this.availableQty = availableQty;
	}



	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}
	
	public boolean isRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(boolean recordSelect) {
		this.recordSelect = recordSelect;
	}

	public String getAssetCategory() {
		return assetCategory;
	}

	public void setAssetCategory(String assetCategory) {
		this.assetCategory = assetCategory;
	}

	public double getPerUnitPrice() {
		return perUnitPrice;
	}

	public void setPerUnitPrice(double perUnitPrice) {
		this.perUnitPrice = perUnitPrice;
	}




	public Date getDate() {
		return date;
	}




	public void setDate(Date date) {
		this.date = date;
	}




	public String getRemark() {
		return remark;
	}




	public void setRemark(String remark) {
		this.remark = remark;
	}




	public int getSrNo() {
		return srNo;
	}




	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	
	

}
