package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

@Embed
	public class Device extends SuperModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7376171587002869947L;
	protected Integer deviceId;
	protected String deviceName;
	
	
	public Device() 
	{
		super();
	}
	

	public Device(Integer deviceId, String deviceName) {
		super();
		this.deviceId = deviceId;
		this.deviceName = deviceName;
		
	}

	public Integer getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}


	public String getDeviceName() {
		return deviceName;
	}


	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}



	@Override
	public int compareTo(SuperModel arg0) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
