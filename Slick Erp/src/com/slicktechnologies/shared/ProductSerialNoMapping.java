package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Serialize;

@Embed
public class ProductSerialNoMapping implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2145570465750355997L;
	
	boolean status;
	String proSerialNo="";
	boolean AvailableStatus = true;
	boolean newAddNo = false;
	public ProductSerialNoMapping(){
		status = false;
		proSerialNo = "";
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getProSerialNo() {
		if(proSerialNo!=null){
			return proSerialNo;
		}else{
			return "";
		}
		
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public boolean isAvailableStatus() {
		return AvailableStatus;
	}

	public void setAvailableStatus(boolean availableStatus) {
		AvailableStatus = availableStatus;
	}

	public boolean isNewAddNo() {
		return newAddNo;
	}

	public void setNewAddNo(boolean newAddNo) {
		this.newAddNo = newAddNo;
	}

	
}
