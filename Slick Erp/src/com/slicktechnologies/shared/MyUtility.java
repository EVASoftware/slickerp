package com.slicktechnologies.shared;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;
import com.slicktechnologies.shared.common.relationalLayer.VendorRelation;

/**
 * The Class MyUtility. Contains Code for maintaining Relations
 */
public class MyUtility 
{
  
  /**
   * Creates the relational key. The one Entity when created for first time creates a new entity in 
   * its Relational Kind.
   
   */
  @GwtIncompatible
  public static Key<?> createRelationalKey(SuperModel m)
  {
	    Key<?>relationalKey;
		GenricServiceImpl implemntor=new GenricServiceImpl();
		implemntor.save(m);
		relationalKey=Key.create(m);
		System.out.println("Created Key Is "+relationalKey);
		return relationalKey;
  }
	
	/**
	 * Update relational entity. Updates the Relational Entity , will always be called by One Kind
	 *
	 * @param cellNumber the cell number
	 * @param branchName the name
	 * @param id the id
	 * @param personInfoKey the person info key
	 */
	@GwtIncompatible
	public static void updateRelationalEntity(SuperModel model,Key<?>personInfoKey)
	  {
		 
		if(personInfoKey!=null)
		 {
		     
			 SuperModel relationalEntity= (SuperModel) ofy().load().key(personInfoKey).now();
			 if(relationalEntity!=null)
			 {
				 model.setId(relationalEntity.getId());
				 ofy().save().entity(model);
			 }
		     
		 }
	  }
  
  
  /**
   * Gets the config name from key.
   *
   * @param configKey the config key
   * @return the config name from key
   */
  @GwtIncompatible
  public static String getConfigNameFromKey(Key<Config>configKey)
  {
	if(configKey!=null)  
	{
		Config config=ofy().load().key(configKey).now();
		if(config!=null)
			return config.getName();
	}
	
	return "";
  }
  
  
  @GwtIncompatible
  public static String getTypeNameFromKey(Key<Type>typeKey)
  {
	if(typeKey!=null)  
	{
		Type type=ofy().load().key(typeKey).now();
		if(type!=null)
			return type.getTypeName();
	}
	
	return "";
  }
  
  
  @GwtIncompatible
  public static String getConfigCatNameFromKey(Key<ConfigCategory>typeKey)
  {
	if(typeKey!=null)  
	{
		ConfigCategory type=ofy().load().key(typeKey).now();
		if(type!=null)
			return type.getCategoryName();
	}
	
	return "";
  }
  @GwtIncompatible
  /**
   * Gets the config key from condition.
   *
   * @param name the name name of Config 
   * @param type the type type of Config
   * @return the config key from condition
   */
  public static Key<Config> getConfigKeyFromCondition(String name,int type)
  {
	  Long companyid=UserConfiguration.getCompanyId();
	  Key<Config>configKey = null;
	  if(companyid!=null)
	  {
		  configKey=ofy().load().type(Config.class).filter("companyId",companyid).
				  filter("type",type).filter("name",name).keys().first().now();
	  }
	  
	  else
	  {
		  configKey=ofy().load().type(Config.class).filter("type",type).filter("name",name).keys().first().now();
	  }
	 
	  return configKey;
	  
  }
  
  public static Key<Type> getTypeKeyFromCondition(String name)
  {
	  Key<Type> typeKey=ofy().load().type(Type.class).filter("typeName",name).keys().first().now();
	  return typeKey;
  }
  
  public static Key<ConfigCategory> getConfigCategoryFromCondition(String name)
  {
	  Long companyid=UserConfiguration.getCompanyId();
	  Key<ConfigCategory> configCatKey = null;
	  if(companyid!=null)
	  {
		  configCatKey=ofy().load().type(ConfigCategory.class).filter("companyId",companyid).
				  filter("categoryName",name).keys().first().now();
	  }
	  else
	  {
		  configCatKey=ofy().load().type(ConfigCategory.class).
				  filter("categoryName",name).keys().first().now(); 
	  }
	  return configCatKey;
  }
  @GwtIncompatible
  /**
   * Gets the relational key from condition.
   *
   * @param condition the condition
   * @param value value of property
   * @return the relational key from condition
   */
  public static Key<?> getRelationalKeyFromCondition(MyQuerry querry)
  {
	 Key<?>relationalKey=null; 
	 GenricServiceImpl implemtor=new GenricServiceImpl();
	 ArrayList<SuperModel>relationalEntity=implemtor.getSearchResult(querry);
	 System.out.println("Result"+relationalEntity.size());
	 if(relationalEntity!=null&&relationalEntity.size()!=0)
	 {
		 relationalKey=Key.create(relationalEntity.get(0));
	 }
	
	  return relationalKey;
  }
  @GwtIncompatible
  /**
   * Gets the relational name from key.
   *
   * @param personInfoKey the person info key
   * @return the relational name from key
   */
  
  public static String getEmployeeNameFromKey(Key<EmployeeRelation>employeeRelationKey)
  {
	  EmployeeRelation employeeRelation=null;
	  if(employeeRelationKey!=null)
	      employeeRelation=ofy().load().key(employeeRelationKey).now();
	  if(employeeRelation!=null)
	     return employeeRelation.getEmployeeName();
	  else
		  return"";
  }
  
  
  public static String getBranchNameFromKey(Key<BranchRelation>branchRelationKey)
  {
	  BranchRelation branchRelation=null;
	  if(branchRelationKey!=null)
		  branchRelation=ofy().load().key(branchRelationKey).now();
	  if(branchRelation!=null)
	     return branchRelation.getName();
	  else
		  return"";
  }
  
  public static CustomerRelation getCustomerRelationFromKey(Key<CustomerRelation>customerRelationKey)
  {
	  CustomerRelation customerRelation=null;
	  if(customerRelationKey!=null)
		  customerRelation=ofy().load().key(customerRelationKey).now();
	 return customerRelation;
	  
  }
  
  
  
  public static String getVendorNameFromKey(Key<VendorRelation>vendorRelationKey)
  {
	  VendorRelation vendorRelation=null;
	  if(vendorRelationKey!=null)
		  vendorRelation=ofy().load().key(vendorRelationKey).now();
	  if(vendorRelation!=null)
	     return vendorRelation.getVendorName();
	  else
		  return"";
  }
  
  
  
}
