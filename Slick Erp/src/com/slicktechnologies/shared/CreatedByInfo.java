package com.slicktechnologies.shared;

public class CreatedByInfo {
	
	public static String userId;
	public static String empName;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		CreatedByInfo.userId = userId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		CreatedByInfo.empName = empName;
	}
	
	

}
