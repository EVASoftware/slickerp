package com.slicktechnologies.shared;
import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class ApproversRemark  extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8282105253000484038L;


	private int approverLevel;
	private String approverName;
	private String approverRemark;
	
	
	public ApproversRemark() {
		approverLevel=0;
		approverName="";
		approverRemark="";
	}
	
	
	public int getApproverLevel() {
		return approverLevel;
	}


	public void setApproverLevel(int approverLevel) {
		this.approverLevel = approverLevel;
	}


	public String getApproverName() {
		return approverName;
	}


	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}


	public String getApproverRemark() {
		return approverRemark;
	}


	public void setApproverRemark(String approverRemark) {
		this.approverRemark = approverRemark;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		return false;
	}

}
