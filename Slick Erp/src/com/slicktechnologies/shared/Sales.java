package com.slicktechnologies.shared;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.SearchCompositeAnnatonation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.relationalLayer.CustomerRelation;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;

// TODO: Auto-generated Javadoc

/**
 * Represents Sales Process in AMC, Sales Process can be Quotation or Contract 
 * @author Sumit
 *
 */

public abstract class Sales extends ConcreteBusinessProcess
{

	/**************************************************Entity Attributes**********************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8772422723400963422L;

	/** The description of this Sales*/
	protected String description;
	
	protected String descriptiontwo;
	
	protected double vendorPrice;

	/** The referred by attribute in this sales */
	protected String referedBy;

	/** Document uploaded corresponding to this Sales */
	protected Vector<DocumentUpload> doc;

	/** The contact information of the person corresponding to this sale */
	@Index 
	protected PersonInfo cinfo;

	/** The total amount corresponding to the total price of the items for this sales  is -1 if not exist */
	protected double totalAmount;

	/** The contract count. Count/id of contract associated with this Sales  is -1 if not exist */
	@Index
	protected int contractCount;

	/** The quotationCount. Count/id of quotation associated with this Sales  is -1 if not exist */
	@Index
	protected int quotationCount;

	/** The leadCount. Count/id of lead associated with this Sales is -1 if not exist*/
	@Index
	protected int leadCount;

	/** The list of sale line items corresponding to this Sales*/
//	@Index  Date 05-05-2018 By vijay due to this large customer branches (eg.>900) contract not getting saved for orion so i have commented this not required this required
	protected ArrayList<SalesLineItem>items;

	/**Methods of Payment **/
	protected String paymentMethod;
	
	
	/** payment terms corresponding to the contract **/
	protected ArrayList<PaymentTerms> paymentTermsList;
	
	/**Taxes Information related to the sales line items*/
	public ArrayList<ProductOtherCharges> productTaxes;
	
	/**Other Charges excluding taxes on sales amount*/
	protected ArrayList<ProductOtherCharges> productCharges;
	
	protected String cformstatus;
	protected double cstpercent;
	protected String cstName;
	
	protected int creditPeriod;
	
	/***********************************************Relational Attribute*****************************************************/
	protected Key<CustomerRelation> keyCustomerRelation;
	protected Key<Config> paymentMethodKey;
	
	/**
	 * Date : 20-09-2017 By ANIL
	 */
	protected ArrayList<OtherCharges> otherCharges;
	/**
	 * End
	 */
	

	/**************************************************Default Ctor**********************************************/
	/**
	 * Instantiates a new sales.
	 */
	public Sales()
	{
		doc=new Vector<DocumentUpload>();
		cinfo=new PersonInfo();
		items=new ArrayList<SalesLineItem>();
		description="";
		referedBy="";
		paymentMethod="";
		quotationCount=-1;
		leadCount=-1;
		contractCount=-1;
		productTaxes=new ArrayList<ProductOtherCharges>();
		productCharges=new ArrayList<ProductOtherCharges>();
		paymentTermsList=new ArrayList<PaymentTerms>();
		cformstatus="";
		cstName="";
		otherCharges=new ArrayList<OtherCharges>();
	
	}

	/**************************************************Getter/Setter**********************************************/
	
	public ArrayList<OtherCharges> getOtherCharges() {
		return otherCharges;
	}

	public void setOtherCharges(List<OtherCharges> otherCharges) {
		ArrayList<OtherCharges> list=new ArrayList<OtherCharges>();
		list.addAll(otherCharges);
		this.otherCharges = list;
	}
	
	/**
	 * Gets the description.
	 * @return the description
	 */
	public String getDescription() 
	{
		return description;
	}

	/**
	 * Sets the description.
	 * @param description the new description
	 */
	public void setDescription(String description)
	{
		if(description!=null)
			this.description = description.trim();
	}

	/**
	 * Gets the refered by.
	 * @return the refered by
	 */
	public String getReferedBy() 
	{
		return referedBy;
	}

	/**
	 * Sets the refered by.
	 * @param referedBy the new refered by
	 */
	public void setReferedBy(String referedBy)
	{
		if(referedBy!=null)
			this.referedBy = referedBy.trim();
	}

	/**
	 * Gets the doc.
	 * @return the doc
	 */
	public DocumentUpload getDocument() 
	{
		if(doc.size()!=0)
		  return doc.get(0);
		return null;
	}

	/**
	 * Sets the doc.
	 * @param doc the new doc
	 */
	public void setDocument(DocumentUpload doc)
	{
		this.doc=new Vector<DocumentUpload>();
		if(doc!=null)
		   this.doc.add(doc);
		
	}

	/**
	 * Gets the {@link PersonInfo} object.
	 * @return the cinfo
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.PERSONINFO, flexFormNumber = "20", title = "", variableName = "personInfo",colspan=3,extra="Customer")
	public PersonInfo getCinfo() 
	{
		return cinfo;
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 9, isFieldUpdater = false, isSortable = true, title = "Customer Cell")
	public Long getCustomerCellNumber() {
		return cinfo.getCellNumber();
	}
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Customer Name")
	public String getCustomerFullName() {
		return cinfo.getFullName();
	}
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 7, isFieldUpdater = false, isSortable = true, title = "Customer Id")
	public int getCustomerId() {
		return cinfo.getCount();
	}
	
	//@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 9, isFieldUpdater = false, isSortable = true, title = "Customer Cell")
	public void setCustomerCellNumber(Long CustomerCellNumber) {
	cinfo.setCellNumber(CustomerCellNumber);
	}
	//@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Customer Name")
	public void setCustomerFullName(String CustomerFullName) {
	cinfo.setFullName(CustomerFullName);
	}
	//@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 7, isFieldUpdater = false, isSortable = true, title = "Customer Id")
	public void setCustomerId(int CustomerId) {
		cinfo.setCount(CustomerId);
	}
	
	
	

	/**
	 * Sets the {@link PersonInfo} object.
	 * @param cinfo the new cinfo
	 */
	public void setCinfo(PersonInfo cinfo) 
	{
		this.cinfo = cinfo;
	}

	/**
	 * Gets the total amount.
	 * @return the total amount
	 */
	public double getTotalAmount() 
	{
		return totalAmount;
	}

	/**
	 * Sets the total amount.
	 * @param totalAmount the new total amount
	 */
	public void setTotalAmount(Double totalAmount) 
	{
		if(totalAmount!=null)
		  this.totalAmount = totalAmount;
	}

	/**
	 * Gets the contract count.
	 * @return the contract count
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "32", title = "Contract Id", variableName = "tbContractId")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 10, isFieldUpdater = false, isSortable = true, title = "Contract Id")
	public int getContractCount() 
	{
		return contractCount;
	}

	/**
	 * Sets the contract count.
	 * @param contractCount the new contract count
	 */
	public void setContractCount(Integer contractCount) 
	{
		if(contractCount!=null)
			this.contractCount = contractCount;
	}

	/**
	 * Gets the quotation count.
	 * @return the quotation count
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "30", title = "Quotation Id", variableName = "tbQuotationId")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = true, title = "Quotation Id")
	public int getQuotationCount()
	{
		return quotationCount;
	}

	/**
	 * Sets the quotation count.
	 * @param quotationCount the new quotation count
	 */
	public void setQuotationCount(Integer quotationCount) 
	{
		if(quotationCount!=null)
		  this.quotationCount = quotationCount;
	}

	/**
	 * Gets the lead count.
	 * @return the lead count
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "31", title = "Lead Id", variableName = "tbLeadId")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "Lead Id")
	public int getLeadCount() 
	{
		return leadCount;
	}

	/**
	 * Sets the lead count.
	 * @param leadCount the new lead count
	 */
	public void setLeadCount(Integer leadCount) 
	{
		if(leadCount!=null)
			this.leadCount = leadCount;
	}

	/**
	 * Gets the items.
	 * @return the items
	 */
	public List<SalesLineItem> getItems() 
	{
		return items;
	}

	/**
	 * Sets the items.
	 * @param items the new items
	 */
	public void setItems(List<SalesLineItem> items) 
	{
		ArrayList<SalesLineItem> arrayitems=new ArrayList<SalesLineItem>();
		arrayitems.addAll(items);
		this.items = arrayitems;
	}

	public String getPaymentMethod() 
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		if(paymentMethod!=null)
			this.paymentMethod = paymentMethod.trim();
	}
	
	public List<ProductOtherCharges> getProductCharges() {
		return productCharges;
	}

	public void setProductCharges(List<ProductOtherCharges> productCharges) {
		ArrayList<ProductOtherCharges> chargesArr=new ArrayList<ProductOtherCharges>();
		chargesArr.addAll(productCharges);
		this.productCharges = chargesArr;
	}
	
	public List<ProductOtherCharges> getProductTaxes() {
		return productTaxes;
	}

	public void setProductTaxes(List<ProductOtherCharges> productTaxes) {
		ArrayList<ProductOtherCharges> prodTaxesArr=new ArrayList<ProductOtherCharges>();
		prodTaxesArr.addAll(productTaxes);
		this.productTaxes = prodTaxesArr;
	}
	
	public List<PaymentTerms> getPaymentTermsList() {
		return paymentTermsList;
	}

	public void setPaymentTermsList(List<PaymentTerms> paymentTermsList) {
		ArrayList<PaymentTerms> payTrmsArr=new ArrayList<PaymentTerms>(); 
		payTrmsArr.addAll(paymentTermsList);
		this.paymentTermsList = payTrmsArr;
	}
	
	public String getCformstatus() {
		return cformstatus;
	}

	public void setCformstatus(String cformstatus) {
		if(cformstatus!=null){
			this.cformstatus = cformstatus;
		}
	}

	public Double getCstpercent() {
		return cstpercent;
	}

	public void setCstpercent(double cstpercent) {
		this.cstpercent = cstpercent;
	}
	
	public String getCstName() {
		return cstName;
	}

	public void setCstName(String cstName) {
		if(cstName!=null){
			this.cstName = cstName.trim();
		}
	}
	
	public Integer getCreditPeriod() {
		return creditPeriod;
	}

	public void setCreditPeriod(int creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	
	public String getDescriptiontwo() {
		return descriptiontwo;
	}

	public void setDescriptiontwo(String descriptiontwo) {
		this.descriptiontwo = descriptiontwo;
	}

	public double getVendorPrice() {
		return vendorPrice;
	}

	public void setVendorPrice(double vendorPrice) {
		this.vendorPrice = vendorPrice;
	}

	/**************************************************Relation Managment Part**********************************************/
	 
	 @GwtIncompatible
	  public void OnSave()
	  {
		//if(id==null)
		 
		 super.OnSave();
		  if(id==null)
		  {
			  MyQuerry querry=new MyQuerry();
			  Filter filter=new Filter();
			  
			  filter.setIntValue(cinfo.getCount());
			  filter.setQuerryString("customerCount");
			  querry.getFilters().add(filter);
			  querry.setQuerryObject(new CustomerRelation()); 
			 keyCustomerRelation=(Key<CustomerRelation>) MyUtility.getRelationalKeyFromCondition(querry);
		     System.out.println("********"+keyCustomerRelation);
		  
		  }
			
			paymentMethodKey=MyUtility.getConfigKeyFromCondition(paymentMethod,ConfigTypes.PAYMENTMETHODS.getValue());
		
	 }
	/**************************************************************************************************************/
   @OnLoad
   @GwtIncompatible
   public void onLoad()
   {
	   if(keyCustomerRelation!=null)
	   {
		   CustomerRelation relation=MyUtility.getCustomerRelationFromKey(keyCustomerRelation);
		   
		   /************ vijay commented this for quick contract ***************/
//		   if(relation!=null&&cinfo!=null)
//		   {
//	    	this.cinfo.setCellNumber(relation.getCellNumber());
//	    	this.cinfo.setCount(relation.getCustomerCount());
//	    	this.cinfo.setFullName(relation.getFullName());
//		   }
	   }
	   

	   if(paymentMethodKey!=null)
	   {
		   String paymentMethodName=MyUtility.getConfigNameFromKey(paymentMethodKey);
		   if(paymentMethodName.equals("")==false)
			   setPaymentMethod(paymentMethodName);
	   }
	   

   }

	
}
