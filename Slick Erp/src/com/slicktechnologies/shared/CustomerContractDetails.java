package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Contact;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;

@Embed
public class CustomerContractDetails extends SuperModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 25133201770990805L;


	
	private boolean isCompany;
	private String companyName;
	private String fullName;
	private Date contractDate;
	private Date contractStartDate;
	private Date contractEndDate;
	protected List<Contact> contacts;
	
	private long cellNumber;
	private long landLine;
	private String email;
	private String refId;
	private int noOfService;
	private String contractGroup;
	private String contractType;
	private String contractCategory;
	private String salePerson;
	private double price;
	private Date cutOffDate;
	private String Branch;
	private String proName;
	private double outStandingamount;
	
	private String Remark;
	
	private int customerCount;
	private int contractCount;
	private int serviceCount;
	private int billingCount;
	private String serviceWeekDay;
	private String serviceTime;
	/**
	 * nidhi
	 *  2-11-2017
	 *  add field for new contract upload process
	 */
	private ArrayList<SalesLineItem> serProduct;
	protected ArrayList<PaymentTerms> paymentTermsList;
	private String paymentTerms;
	private String approverName;
	private int quotationCount;
	private int leadCount;
	private boolean isRateContract;
	/**
	 *  end
	 *  2-11-2017
	 *  
	 */
	/**
	 * nidhi
	 * 13-03-2018
	 * for new field
	 */
	private String custGST;
	private boolean serviceBilling;
	private boolean consilated;
	private String salesSource;
	private String typeOfFreq;
	private String typeOfTretment;
	private String salutation;
	/**
	 *  end
	 */
	
	/***18-03-2020 Deepak Salve added this code ***/
	private String numberRang;
	/***End***/
	
	
//	private ArrayList<payment>
	/**
	 * nidhi
	 * 20-08-2018
	 */
	private String proModelNo,proSerialNo;
	
	/**
	 * @author Vijay Chougule Date 10-09-2020
	 * Des :- Adding Service Duration for LifeLine
	 */
	private double serviceDuration;
	
	/**
	 * @author Priyanka Bhagwat Date 12-11-2020
	 * Des :- Adding Service Duration As per Rahul and Vaishali mam's requirement for star cool
	 */
	private String correspondenceName;
	private String description;
	/*
	 * Added by Sheetal 16-11-2021
	 * Des :- Adding Tax1 , Tax2 column as per rahul sir
	 */
	private String tax1;
	private String tax2;
	
	public CustomerContractDetails(){
		super();
		
		Contact contact=new Contact();
		this.contacts=new Vector<Contact>();
		contacts.add(contact);
		contact=new Contact();
		contacts.add(contact);
		outStandingamount = 0 ; 
		price = 0;
		contractType="";
		email="";
		Branch="";
		contractCategory="";
		paymentTerms="";
		isCompany = false;
		approverName="";
		refId="";
		salePerson="";
		quotationCount=0;
		isRateContract = false;
		custGST="";
		consilated = false;
		serviceBilling = false;
		salutation="";
		proModelNo="";
		proSerialNo="";
		this.serProduct = new ArrayList<SalesLineItem>();
	}

	public String getServiceTime() {
		return serviceTime;
	}
	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getServiceWeekDay() {
		return serviceWeekDay;
	}

	public void setServiceWeekDay(String serviceWeekDay) {
		this.serviceWeekDay = serviceWeekDay;
	}

	public double getOutStandingamount() {
		return outStandingamount;
	}
	public void setOutStandingamount(double outStandingamount) {
		this.outStandingamount = outStandingamount;
	}
	public String getBranch() {
		return Branch;
	}
	public void setBranch(String branch) {
		Branch = branch;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public boolean getIsCompany() {
		return isCompany;
	}
	public void isCompany(boolean isCompany) {
		this.isCompany = isCompany;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getContractDate() {
		return contractDate;
	}
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}
	public Date getContractStartDate() {
		return contractStartDate;
	}
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public Date getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public List<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	public long getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(long cellNumber) {
		this.cellNumber = cellNumber;
	}
	public long getLandLine() {
		return landLine;
	}
	public void setLandLine(long landLine) {
		this.landLine = landLine;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public int getNoOfService() {
		return noOfService;
	}
	public void setNoOfService(int noOfService) {
		this.noOfService = noOfService;
	}
	public String getContractGroup() {
		return contractGroup;
	}
	public void setContractGroup(String contractGroup) {
		this.contractGroup = contractGroup;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getContractCategory() {
		return contractCategory;
	}
	public void setContractCategory(String contractCategory) {
		this.contractCategory = contractCategory;
	}
	public String getSalePerson() {
		return salePerson;
	}
	public void setSalePerson(String salePerson) {
		this.salePerson = salePerson;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getCutOffDate() {
		return cutOffDate;
	}
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	
	public void setAdress(Address address)
	{
		if(address!=null)
			this.contacts.get(0).setAddress(address);
	}

	public Address getAdress()
	{
		return contacts.get(0).getAddress();
	}
	
	
	
	public String getRemark() {
		return Remark;
	}
	public void setRemark(String remark) {
		Remark = remark;
	}
	public Address getSecondaryAdress()
	{
		return contacts.get(1).getAddress();
	}
	
	public void setSecondaryAdress(Address address)
	{
	 contacts.get(1).setAddress(address);
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getCustomerCount() {
		return customerCount;
	}

	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}

	public int getContractCount() {
		return contractCount;
	}

	public void setContractCount(int contractCount) {
		this.contractCount = contractCount;
	}

	public int getServiceCount() {
		return serviceCount;
	}

	public void setServiceCount(int serviceCount) {
		this.serviceCount = serviceCount;
	}

	public ArrayList<SalesLineItem> getServiceProduct() {
		return serProduct;
	}

	public void setServiceProduct(ArrayList<SalesLineItem> serviceProduct) {
		this.serProduct = serviceProduct;
	}

	public ArrayList<PaymentTerms> getPaymentTermsList() {
		return paymentTermsList;
	}

	public void setPaymentTermsList(ArrayList<PaymentTerms> paymentTermsList) {
		this.paymentTermsList = paymentTermsList;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}


	public int getQuotationCount() {
		return quotationCount;
	}

	public void setQuotationCount(int quotationCount) {
		this.quotationCount = quotationCount;
	}

	public int getLeadCount() {
		return leadCount;
	}

	public void setLeadCount(int leadCount) {
		this.leadCount = leadCount;
	}

	public boolean isRateContract() {
		return isRateContract;
	}

	public void setRateContract(boolean isRateContract) {
		this.isRateContract = isRateContract;
	}

	public int getBillingCount() {
		return billingCount;
	}

	public void setBillingCount(int billingCount) {
		this.billingCount = billingCount;
	}

	public ArrayList<SalesLineItem> getSerProduct() {
		return serProduct;
	}

	public void setSerProduct(ArrayList<SalesLineItem> serProduct) {
		this.serProduct = serProduct;
	}

	public String getCustGST() {
		return custGST;
	}

	public void setCustGST(String custGST) {
		this.custGST = custGST;
	}

	public boolean isServiceBilling() {
		return serviceBilling;
	}

	public void setServiceBilling(boolean serviceBilling) {
		this.serviceBilling = serviceBilling;
	}

	public boolean isConsilated() {
		return consilated;
	}

	public void setConsilated(boolean consilated) {
		this.consilated = consilated;
	}

	public String getSalesSource() {
		return salesSource;
	}

	public void setSalesSource(String salesSource) {
		this.salesSource = salesSource;
	}

	public String getTypeOfFreq() {
		return typeOfFreq;
	}

	public void setTypeOfFreq(String typeOfFreq) {
		this.typeOfFreq = typeOfFreq;
	}

	public String getTypeOfTretment() {
		return typeOfTretment;
	}

	public void setTypeOfTretment(String typeOfTretment) {
		this.typeOfTretment = typeOfTretment;
	}

	public void setCompany(boolean isCompany) {
		this.isCompany = isCompany;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getProModelNo() {
		return proModelNo;
	}

	public void setProModelNo(String proModelNo) {
		this.proModelNo = proModelNo;
	}

	public String getProSerialNo() {
		return proSerialNo;
	}

	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public String getNumberRang() {
		return numberRang;
	}

	public void setNumberRang(String numberRang) {
		this.numberRang = numberRang;
	}

	public double getServiceDuration() {
		return serviceDuration;
	}

	public void setServiceDuration(double serviceDuration) {
		this.serviceDuration = serviceDuration;
	}

	public String getCorrespondenceName() {
		return correspondenceName;
	}

	public void setCorrespondenceName(String correspondenceName) {
		this.correspondenceName = correspondenceName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public String getTax1(){
		return tax1;
	}
	public void setTax1(String tax1) {
		this.tax1 = tax1;
	}
  
	public String getTax2(){
			return tax2;
	}
	public void setTax2(String tax2) {
			this.tax2 = tax2;
		
	}
		
	
	

	
}
