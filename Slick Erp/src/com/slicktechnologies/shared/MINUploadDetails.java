package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;

@Embed
public class MINUploadDetails implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9353820309316624L;
	long companyId;
	int serviceCount;
	String branch;
	int contractCount;
	Date serviceDate;
	Date serviceComplDate;
	String remark;
	double serviceComplTime;
//	int productCount;
	double unit;
	int empCount;
	String uom,serviType;
	ArrayList<EmployeeInfo> empDetails;
	ArrayList<ProductGroupList> minDetails;
	ArrayList<MaterialProduct> minProdDetails;
	
	boolean isCreateProject;
	boolean isCreateMin;
	String appEmpName ;
	
	
	int customerProCount;
	public int getCustomerProCount() {
		return customerProCount;
	}
	public void setCustomerProCount(int customerProCount) {
		this.customerProCount = customerProCount;
	}
	public int getMinProCount() {
		return minProCount;
	}
	public void setMinProCount(int minProCount) {
		this.minProCount = minProCount;
	}
	public int getMinTrCount() {
		return minTrCount;
	}
	public void setMinTrCount(int minTrCount) {
		this.minTrCount = minTrCount;
	}

	int minProCount;
	int minTrCount;
	public int getServiceCount() {
		return serviceCount;
	}
	public void setServiceCount(int serviceCount) {
		this.serviceCount = serviceCount;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getContractCount() {
		return contractCount;
	}
	public void setContractCount(int contractCount) {
		this.contractCount = contractCount;
	}
	public Date getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}
	public Date getServiceComplDate() {
		return serviceComplDate;
	}
	public void setServiceComplDate(Date serviceComplDate) {
		this.serviceComplDate = serviceComplDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public double getServiceComplTime() {
		return serviceComplTime;
	}
	public void setServiceComplTime(double serviceComplTime) {
		this.serviceComplTime = serviceComplTime;
	}
//	public int getProductCount() {
//		return productCount;
//	}
//	public void setProductCount(int productCount) {
//		this.productCount = productCount;
//	}
	public ArrayList<EmployeeInfo> getEmpDetails() {
		return empDetails;
	}
	public void setEmpDetails(ArrayList<EmployeeInfo> empDetails) {
		this.empDetails = empDetails;
	}
	public ArrayList<ProductGroupList> getMinDetails() {
		return minDetails;
	}
	public void setMinDetails(ArrayList<ProductGroupList> minDetails) {
		this.minDetails = minDetails;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getServiType() {
		return serviType;
	}
	public void setServiType(String serviType) {
		this.serviType = serviType;
	}
	public double getUnit() {
		return unit;
	}
	public void setUnit(double unit) {
		this.unit = unit;
	}
	public int getEmpCount() {
		return empCount;
	}
	public void setEmpCount(int empCount) {
		this.empCount = empCount;
	}
	public boolean isCreateProject() {
		return isCreateProject;
	}
	public void setCreateProject(boolean isCreateProject) {
		this.isCreateProject = isCreateProject;
	}
	public boolean isCreateMin() {
		return isCreateMin;
	}
	public void setCreateMin(boolean isCreateMin) {
		this.isCreateMin = isCreateMin;
	}
	public String getAppEmpName() {
		return appEmpName;
	}
	public void setAppEmpName(String appEmpName) {
		this.appEmpName = appEmpName;
	}
	public ArrayList<MaterialProduct> getMinProdDetails() {
		return minProdDetails;
	}
	public void setMinProdDetails(ArrayList<MaterialProduct> minProdDetails) {
		this.minProdDetails = minProdDetails;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	public MINUploadDetails(){
		super();
		uom="";
		serviType = "";
		remark= "";
		isCreateProject = true;
		isCreateMin = true;
		empDetails = new ArrayList<EmployeeInfo>();
		minDetails = new ArrayList<ProductGroupList>();
		minProdDetails = new ArrayList<MaterialProduct>();
	}
	
	public MINUploadDetails myClone(){
		MINUploadDetails mindetails = new MINUploadDetails();
		mindetails.unit = this.unit;
		mindetails.uom = this.uom;
		mindetails.serviType = this.serviType;
		mindetails.branch = this.branch;
		mindetails.remark = this.remark;
		mindetails.isCreateProject = this.isCreateProject;
		mindetails.isCreateMin = this.isCreateMin;
		ArrayList<EmployeeInfo> emp = new ArrayList<EmployeeInfo>();
		emp.addAll(this.empDetails);
		mindetails.setEmpDetails(emp);
		
		ArrayList<ProductGroupList> pro = new ArrayList<ProductGroupList>();
		pro.addAll(this.minDetails);
		mindetails.setMinDetails(pro);
		
		
		ArrayList<MaterialProduct> prod = new ArrayList<MaterialProduct>();
		prod.addAll(this.minProdDetails);
		mindetails.setMinProdDetails(prod);
		
		mindetails.companyId = this.companyId;
		mindetails.setServiceComplDate(this.serviceComplDate);
		mindetails.setAppEmpName(this.appEmpName);
		mindetails.contractCount = this.contractCount;
		mindetails.serviceCount = this.serviceCount;
		mindetails.minProCount = this.minProCount;
		mindetails.minTrCount = this.minTrCount;
		mindetails.remark = this.remark;
		mindetails.serviceComplTime = this.serviceComplTime;
		mindetails.serviceDate = this.serviceDate;
		mindetails.customerProCount = this.customerProCount;
		return mindetails;
	}
}
