package com.slicktechnologies.shared;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class TechnicianWarehouseBean {

	String employeeName;
	String branch;
	double salary;
	double workingHours;
	long cellNumber;
	String parentWarehouse;
	String parentStorageLocation;
	String parentStorageBin;
	String userName;
	String password;
	int employeeId;
	
	public TechnicianWarehouseBean(){
		employeeName = "";
		branch = "";
		parentWarehouse = "";
		parentStorageLocation = "";
		parentStorageBin = "";
		userName = "";
		password = "";
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(double workingHours) {
		this.workingHours = workingHours;
	}

	public long getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(long cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String getParentWarehouse() {
		return parentWarehouse;
	}

	public void setParentWarehouse(String parentWarehouse) {
		this.parentWarehouse = parentWarehouse;
	}

	public String getParentStorageLocation() {
		return parentStorageLocation;
	}

	public void setParentStorageLocation(String parentStorageLocation) {
		this.parentStorageLocation = parentStorageLocation;
	}

	public String getParentStorageBin() {
		return parentStorageBin;
	}

	public void setParentStorageBin(String parentStorageBin) {
		this.parentStorageBin = parentStorageBin;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
}
