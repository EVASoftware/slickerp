package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ScreenName implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6683118337588867891L;

	
	String screenName;
	boolean check;
	
	public ScreenName(){
		screenName="";
		check = false;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}
	
	
	
	
}
