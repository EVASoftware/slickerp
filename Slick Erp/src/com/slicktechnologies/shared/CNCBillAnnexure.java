package com.slicktechnologies.shared;

import java.util.ArrayList;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class CNCBillAnnexure extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4829523625222513362L;
	/**
	 * 
	 */
	

	
	@Index
	int cncId;
	@Index
	String month;
	double total;
	double taxableAmount;
	@Index
	int billingId;
	@Index
	ArrayList<CNCBillAnnexureBean> CNCBillAnnexureList;
	
	
	public CNCBillAnnexure() {
		month = "";
		CNCBillAnnexureList = new ArrayList<CNCBillAnnexureBean>();
	}



	public int getCncId() {
		return cncId;
	}



	public void setCncId(int cncId) {
		this.cncId = cncId;
	}



	public String getMonth() {
		return month;
	}



	public void setMonth(String month) {
		this.month = month;
	}



	public double getTotal() {
		return total;
	}



	public void setTotal(double total) {
		this.total = total;
	}



	public double getTaxableAmount() {
		return taxableAmount;
	}



	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}



	public ArrayList<CNCBillAnnexureBean> getCNCBillAnnexureList() {
		return CNCBillAnnexureList;
	}



	public void setCNCBillAnnexureList(
			ArrayList<CNCBillAnnexureBean> cNCBillAnnexureList) {
		CNCBillAnnexureList = cNCBillAnnexureList;
	}



	public int getBillingId() {
		return billingId;
	}



	public void setBillingId(int billingId) {
		this.billingId = billingId;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
