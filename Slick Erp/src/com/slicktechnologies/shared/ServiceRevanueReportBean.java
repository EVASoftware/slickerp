package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Embed
public class ServiceRevanueReportBean extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4596925022749216994L;

	protected int srno;
	
	@Index
	protected int contractCount;
	
	@Index
	protected int customerId;
	
	@Index
	protected String custName;
	 
	@Index
	protected Long custCellNumber;
	
	@Index
	protected String pocName;
	
	@Index
	protected String compBranch;
	
	@Index
	protected String contractstatus;
	
	@Index
	protected String contCategory;
	
	@Index
	protected String contType;
	
	@Index
	protected double contNetpayableAmt;
	
	@Index
	protected int invCont;
	
	@Index
	protected String invDetails;
	
	@Index
	protected String prodDetails;
	
	/*
	 * total of all invoice
	 */
	@Index
	protected double ActualAmt;
	
	@Index
	protected Date contStartDate;
   
	@Index
	protected Date contEndDate;
	
	@Index
	protected Date serviceCompletionDate;
	
	@Index
	protected String contProName;

	@Index
	protected boolean contractRate;
	
	@Index
	protected String invStatus;
	
	@Index
	protected String technicianName;
	
	@Index
	protected Date serviceDate;
	
	@Index
	protected int projectId;
	
	@Index
	protected String projectName;
	
	@Index
	protected double prodPrice;
	
	@Index
	protected int serviceId;
	
	@Index
	protected Date contractDate;
	
	@Index 
	protected String remark;
	
	void serviceRevanueReportBean(){
		serviceId=0;
		prodPrice=0.0;
		projectName="";
		technicianName="";
		invStatus="";
		contProName="";
		ActualAmt=0;
		prodDetails="";
		contType="";
		compBranch="";
		pocName="";
		custCellNumber=(long) 0;
		contNetpayableAmt=0.0;
		remark="";
		custName="";
	}
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public boolean isContractRate() {
		return contractRate;
	}

	public void setContractRate(boolean contractRate) {
		this.contractRate = contractRate;
	}

	public String getInvStatus() {
		return invStatus;
	}

	public void setInvStatus(String invStatus) {
		this.invStatus = invStatus;
	}

	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public double getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}

	public int getSrno() {
		return srno;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public int getContractCount() {
		return contractCount;
	}

	public void setContractCount(int contractCount) {
		this.contractCount = contractCount;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public Long getCustCellNumber() {
		return custCellNumber;
	}

	public void setCustCellNumber(Long custCellNumber) {
		this.custCellNumber = custCellNumber;
	}

	public String getPocName() {
		return pocName;
	}

	public void setPocName(String pocName) {
		this.pocName = pocName;
	}

	public String getCompBranch() {
		return compBranch;
	}

	public void setCompBranch(String compBranch) {
		this.compBranch = compBranch;
	}

	public String getContractstatus() {
		return contractstatus;
	}

	public void setContractstatus(String contractstatus) {
		this.contractstatus = contractstatus;
	}

	public String getContCategory() {
		return contCategory;
	}

	public void setContCategory(String contCategory) {
		this.contCategory = contCategory;
	}

	public String getContType() {
		return contType;
	}

	public void setContType(String contType) {
		this.contType = contType;
	}

	public double getContNetpayableAmt() {
		return contNetpayableAmt;
	}

	public void setContNetpayableAmt(double contNetpayableAmt) {
		this.contNetpayableAmt = contNetpayableAmt;
	}

	public int getInvCont() {
		return invCont;
	}

	public void setInvCont(int invCont) {
		this.invCont = invCont;
	}

	public String getInvDetails() {
		return invDetails;
	}

	public void setInvDetails(String invDetails) {
		this.invDetails = invDetails;
	}

	public String getProdDetails() {
		return prodDetails;
	}

	public void setProdDetails(String prodDetails) {
		this.prodDetails = prodDetails;
	}

	public double getActualAmt() {
		return ActualAmt;
	}

	public void setActualAmt(double actualAmt) {
		ActualAmt = actualAmt;
	}

	public Date getContStartDate() {
		return contStartDate;
	}

	public void setContStartDate(Date contStartDate) {
		this.contStartDate = contStartDate;
	}

	public Date getContEndDate() {
		return contEndDate;
	}

	public void setContEndDate(Date contEndDate) {
		this.contEndDate = contEndDate;
	}

	public String getContProName() {
		return contProName;
	}

	public void setContProName(String contProName) {
		this.contProName = contProName;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public Date getServiceCompletionDate() {
		return serviceCompletionDate;
	}

	public void setServiceCompletionDate(Date serviceCompletionDate) {
		this.serviceCompletionDate = serviceCompletionDate;
	}
	
	

}
