package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class TaxSummaryBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7719376270839235950L;
	String hsnNumber;
	double taxAmount;
	double cgstPer;
	double cgstAmount;
	double sgstPer;
	double sgstAmount;
	double igstAmount;
	double igstPer;
	double totalTaxAmount;
	
	public TaxSummaryBean(){
		hsnNumber = "";
	}

	public String getHsnNumber() {
		return hsnNumber;
	}

	public void setHsnNumber(String hsnNumber) {
		this.hsnNumber = hsnNumber;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getCgstPer() {
		return cgstPer;
	}

	public void setCgstPer(double cgstPer) {
		this.cgstPer = cgstPer;
	}

	public double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public double getSgstPer() {
		return sgstPer;
	}

	public void setSgstPer(double sgstPer) {
		this.sgstPer = sgstPer;
	}

	public double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public double getIgstPer() {
		return igstPer;
	}

	public void setIgstPer(double igstPer) {
		this.igstPer = igstPer;
	}

	public double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}
	
}
