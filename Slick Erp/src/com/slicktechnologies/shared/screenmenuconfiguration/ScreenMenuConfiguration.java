package com.slicktechnologies.shared.screenmenuconfiguration;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
@Embed
public class ScreenMenuConfiguration extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -931275051261514141L;
	@Index
	String moduleName;
	@Index
	String documentName;
	@Index
	String menuName;
	@Index
	boolean status;
	@Index
	ArrayList<MenuConfiguration> menuConfigurationList;
	
	
	
	public String getModuleName() {
		return moduleName;
	}



	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}



	public String getDocumentName() {
		return documentName;
	}



	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}



	public String getMenuName() {
		return menuName;
	}



	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}



	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	public ArrayList<MenuConfiguration> getMenuConfigurationList() {
		return menuConfigurationList;
	}



	public void setMenuConfigurationList(List<MenuConfiguration> menuConfigurationList) {
		ArrayList<MenuConfiguration> list=new ArrayList<MenuConfiguration>();
		list.addAll(menuConfigurationList);
		this.menuConfigurationList = list;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		ScreenMenuConfiguration obj = (ScreenMenuConfiguration) m;
		String moduleName = obj.getModuleName().trim();
		String documentName = obj.getDocumentName().trim();
		String menuName = obj.getMenuName().trim();

		String currentModuleName = this.getModuleName().trim();
		String currentDocumentName = this.getDocumentName().trim();
		String currentMenuName = this.getMenuName().trim();

		// check duplicate value while new Object is being added
		if (obj.id == null) {
			if (moduleName.equals(currentModuleName)&&documentName.equals(currentDocumentName)&&menuName.equals(currentMenuName))
				return true;
			else
				return false;
		}
		// while updating a Old object
		else {
			if (obj.id.equals(id))
				return false;
			else if (moduleName.equals(currentModuleName)&&documentName.equals(currentDocumentName)&&menuName.equals(currentMenuName))
				return true;
			else
				return false;
		}
	}

}
