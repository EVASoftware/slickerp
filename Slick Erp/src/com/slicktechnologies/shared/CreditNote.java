package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class CreditNote extends SuperModel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5819792763428257248L;
	
	@Index
	protected int tallyCreditNoteId;
	@Index
	protected Date creditNoteDate;
	@Index
	protected int invoiceId;
	protected double creditNoteAmt;
	protected double baseAmount;
	protected double cgstAmt;
	protected double sgstAmt;
	protected double igstAmt;
	protected double roundOffAmt;
	protected String remark;
	@Index
	protected int paymentId;
	@Index
	protected String branch;
	
	public CreditNote(){
		remark = "";
		branch = "";
	}
	

	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}


	public int getTallyCreditNoteId() {
		return tallyCreditNoteId;
	}


	public void setTallyCreditNoteId(int tallyCreditNoteId) {
		this.tallyCreditNoteId = tallyCreditNoteId;
	}


	public Date getCreditNoteDate() {
		return creditNoteDate;
	}


	public void setCreditNoteDate(Date creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}


	public int getInvoiceId() {
		return invoiceId;
	}


	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}


	public double getCreditNoteAmt() {
		return creditNoteAmt;
	}


	public void setCreditNoteAmt(double creditNoteAmt) {
		this.creditNoteAmt = creditNoteAmt;
	}


	public double getBaseAmount() {
		return baseAmount;
	}


	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}



	public double getSgstAmt() {
		return sgstAmt;
	}


	public void setSgstAmt(double sgstAmt) {
		this.sgstAmt = sgstAmt;
	}


	public double getIgstAmt() {
		return igstAmt;
	}


	public void setIgstAmt(double igstAmt) {
		this.igstAmt = igstAmt;
	}


	public double getRoundOffAmt() {
		return roundOffAmt;
	}


	public void setRoundOffAmt(double roundOffAmt) {
		this.roundOffAmt = roundOffAmt;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public int getPaymentId() {
		return paymentId;
	}


	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public double getCgstAmt() {
		return cgstAmt;
	}


	public void setCgstAmt(double cgstAmt) {
		this.cgstAmt = cgstAmt;
	}



	
	
	
}
