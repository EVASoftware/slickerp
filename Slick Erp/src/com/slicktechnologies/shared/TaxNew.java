package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.composite.DateComparator;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

@Entity
@Index
public class TaxNew extends SuperModel implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5184476967868928868L;
	protected String taxName;
	protected Double taxPercentage;
	protected Boolean inclusive;
	protected Double accountNumber;
	protected Boolean status;
	protected Date fromdate;
	protected Date toDate;
	
	public TaxNew()
	{
		super();
		taxName="";
	
	}


	public TaxNew(String taxName, Double taxPercentage, Boolean inclusive,Double accountNumber, Boolean status, Date fromdate,Date todate) 
	{
		super();
		this.taxName = taxName;
		this.taxPercentage = taxPercentage;
		this.inclusive = inclusive;
		this.accountNumber = accountNumber;
		this.status = status;
		this.fromdate = fromdate;
		this.toDate=todate;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Tax Name")
	public String getTaxName() {
		return taxName;
	}


	public void setTaxName(String taxName)
	{
		if(taxName!=null)
			this.taxName = taxName.trim();
	}

	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Tax Percentage")
	public Double getTaxPercentage() {
		return taxPercentage;
	}


	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}


	public Boolean isInclusive() {
		return inclusive;
	}


	public void setInclusive(Boolean inclusive) {
		this.inclusive = inclusive;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "Account Number")
	public Double getAccountNumber() {
		return accountNumber;
	}


	public void setAccountNumber(Double accountNumber) {
		this.accountNumber = accountNumber;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Tax Status")
	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) 
	{
			this.status = status;
	}


	public Date getDate() {
		return fromdate;
	}


	public void setDate(Date date) {
		this.fromdate = date;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	@Override
	public int compareTo(SuperModel arg0) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
	

}
