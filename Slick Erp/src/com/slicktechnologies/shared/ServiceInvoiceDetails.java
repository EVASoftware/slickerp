package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ServiceInvoiceDetails {

	String custName = "";
	Integer custId;
	Integer contractId;
	Date contStartDate;
	Integer serviceID,billId,invId;
	String serviceStatus = "";
	String billStatus = "";
	String invStatus = "";
	String sapInvId;
	Date billDate ;
	Date invDate;
	Date serDate;
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public Integer getCustId() {
		return custId;
	}
	public void setCustId(Integer custId) {
		this.custId = custId;
	}
	public Integer getContractId() {
		return contractId;
	}
	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}
	public Date getContStartDate() {
		return contStartDate;
	}
	public void setContStartDate(Date contStartDate) {
		this.contStartDate = contStartDate;
	}
	public Integer getServiceID() {
		return serviceID;
	}
	public void setServiceID(Integer serviceID) {
		this.serviceID = serviceID;
	}
	public Integer getBillId() {
		return billId;
	}
	public void setBillId(Integer billId) {
		this.billId = billId;
	}
	public Integer getInvId() {
		return invId;
	}
	public void setInvId(Integer invId) {
		this.invId = invId;
	}
	public String getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	public String getBillStatus() {
		return billStatus;
	}
	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}
	public String getSapInvId() {
		return sapInvId;
	}
	public void setSapInvId(String sapInvId) {
		this.sapInvId = sapInvId;
	}
	public Date getBillDate() {
		return billDate;
	}
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}
	public Date getInvDate() {
		return invDate;
	}
	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}
	public Date getSerDate() {
		return serDate;
	}
	public void setSerDate(Date serDate) {
		this.serDate = serDate;
	}
	public String getInvStatus() {
		return invStatus;
	}
	public void setInvStatus(String invStatus) {
		this.invStatus = invStatus;
	}
	
	
}
