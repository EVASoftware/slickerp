package com.slicktechnologies.shared;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.dev.util.collect.HashMap;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.ContractRenewalServiceImpl;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.LoginServiceImpl;
import com.slicktechnologies.server.QuotationServiceImplentor;
import com.slicktechnologies.server.api.GetUserRegistrationOtp;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ClientSideAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.sms.SMSDetails;
import com.slicktechnologies.shared.common.tallyaccounts.UpdateAccountingInterface;


/**
 * Represents Contract for approved quotation/s of AMC only.
 */

@Entity
public class Contract extends Quotation implements Serializable
{
	
	public static final String CONTRACTCANCEL = "Cancelled";
	public static final String CONTRACTEXPIRED = "Expired";
	/**
	 * nidhi
	 * for discountinued status
	 */
	public static final String CONTRACTDISCOUNTINED = "Discontinued";
	
	/************************************************** Attributes**********************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2219476895484431841L;

	/** advance paid by customer. Zero for non approved Contracts. Uneditable field */
	protected double advance;

	/** balance of this contract. Uneditable field*/
	@Index 
	protected double balance;

	/** payment corresponding to this contract. */
	@Serialize
	protected ArrayList<Payment> payments;
	
	
	/***********************Rahul(Used)**********************************************/
	@Index
	protected String contractstatus;
	

	/** ID of old contract, if this contract is renewed from old contract.Otherwise zero */
	@Index
	protected String refNo;
	
	protected Date refDate;
	
	@Index
	protected int refContractCount;
	
	@Index
	protected boolean renewFlag;
	@Index
	protected Date contractDate;
	
	
	//***********this is used for customer interest in contract renew or not ***** 
	@Index
	protected boolean customerInterestFlag;
	
	protected boolean contractStatusChanged=false;
	

	@Index
	protected String renewProcessed;
	
	protected boolean branchWiseBilling;
	
	@Index
	protected String numberRange;
	
	
	/************************* vijay for quick contract ***************/
	protected double paymentRecieved;
	
	protected double balancePayment;
	
	protected String newcompanyname;
	
	protected String newcustomerfullName;
	
	protected Long newcustomercellNumber;
	
	protected String newcustomerEmail;
	
	protected boolean customersaveflag;

	//  rohan added this code for customer branch
	protected String newCustomerBranch;
	
//	@Index
//	protected int newCustomerId;
	
	@Index
	protected Address newcustomerAddress;
	
	protected String payTerms;
	
	//*********vaishnavi************
	protected ArrayList<ClientSideAsset> asset;
	
	//vijay
		protected boolean contractRate;
		Date quickContractInvoiceDate;
	/******************************** end here **************************/
	
	/**
	 * Date :24-01-2017 by Anil
	 * Adding ref No. 2 for PCAMB and Pest Cure Incorporation	
	 */
	@Index	
	protected String refNo2;
	/**
	 * End
	 */
		
	/**
	 * Added by rahul verma
	 * Description: Added to store all values of wareHouse,StorageLocation and
	 * Stack. Date: 06-10-2016 Project : Required For : This is created for NBHC
	 *
	 */
	@Index
	protected ArrayList<StackDetails> stackDetailsList;

	/**
	 * End
	 */
	
	/**
	 * Added by rohan bhagde
	 * Date : 12-05-2017
	 * This is added for Inegrated pest control adding tecnician in contract 
	 */
	@Index
	protected String technicianName;
		/**
		 * ends here 
		 */
		
	
	/**
	 * rohan added this code for accepting payment informations also at contract level
	 */
	/** The bank name. */
	protected String bankName;
	
	/** The bank branch. */
	protected String bankBranch;
	
	/** The bank acc no. */
	protected String bankAccNo;
	
	/** The cheque no. */
	protected String chequeNo;
	
	/** The cheque date. */
	protected Date chequeDate;
	
	/** The amount transfer date. */
	protected Date amountTransferDate;
	
	/** The reference no. */
	protected String referenceNo;
	
	/** The chequeIssuedBy. */
	protected String chequeIssuedBy;
	
	/**
	 * ends here 
	 */
	
	/*
	 * nidhi
	 * 29-06-2017
	 * segment field declaration 
	 * As per NBHC CCPM requirement ,this field stores the customer category's value. to distinguish contract of particular segment.
	 * 
	 */
	@Index
	protected String segment;
	/*
	 * end
	 */
	
	/** Date 02-09-2017 added by vijay for new customer GST number **/
	protected String customerGSTNumber;
	
	/** Date 13-09-2017 2017 added by vijay for customer Address ******/
	@Index
	protected Address customerServiceAddress;
	
	/**
	 * Date : 06-10-2017 BY ANIL
	 * Following flag is used to generate bill service wise.
	 */
	@Index
	protected boolean serviceWiseBilling;
	
	/**
	 * End
	 */
	 /** Date 06/2/2018 added by komal for consolidate price checkbox **/
	  protected boolean consolidatePrice;
	  
   /**
	 * Date 23-01-2018 By vijay 
	 * conract period for PCAMB
	 */
	@Index
	protected String contractPeriod;
	/**
	 * ends here
	 */
	
	/**************************************************Default Constructor*****************************************************/

	/**
	 *  nidhi
	 *  29-01-2018
	 *  get poc name for contract
	 */
	@Index
	protected String pocName;
	
	/** date 04/12/2014 added by komal for hvac (to store customer name from sales order)
	 * 
	 */
	@Index
	protected String AMCNumber;
	
	/**
	 * Instantiates a new contract.
	 */
	/**
	 * Instantiates a new contract.
	 */
	/*
	 * nidhi
	 * 27-04-2018
	 * for contract created from old contract
	 */
	@Index
	protected boolean renewContractFlag;
	/**
	 * nidhi
	 * 8-06-2018
	 */
	@Index
	protected String nonRenewalRemark;
	/**
	 * Date 03-4-2019 by Vijay for NBHC CCPM cancellation Date
	 */
	@Index
	protected Date cancellationDate;
	/**
	 * Date 31-07-2019 by Vijay for NBHC CCPM Fumigation Services
	 */
	@Index
	protected String warehouseName;
	
	
	/**Date 29-8-2019 by Amol added a po number and po date;
	 * 
	 */
	protected String poNumber;
	protected Date PoDate;
	
	@Index
	protected String productGroup;
	
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@Index
	Date lastUpdatedDate;
	@Index
	protected String paymentMode;
	
	/**
	 * @author Anil
	 * @since 02-05-2020
	 * Adding TAT (turn around time) for premium
	 * @since 13-05-2020
	 * changed type from double to string
	 */
	String tat;
	
	/**
	 * @author Anil
	 * @since 29-06-2020
	 * Adding account manager drop down list
	 * Raised by Nitin Sir for pecopp
	 */
	@Index
	String accountManager;
	@Index
	boolean stationedTechnician;
	
	/**
	 * @author Vijay Date 23-06-2021
	 * Des :-  This is used for Payment Gateway processed from IP Address
	 */
	String ipAddress;
	
	Date acceptedDateTime;
	
//	/**Added by sheetal:28-03-2022
//	 * Des : This is used for the purpose of Quick contract cannot be submitted from contract**/
//	public boolean isRateContract;
	
	public boolean isQuickContract;
	
	protected String invoicePdfNameToPrint;
	
	
	/*
	Ashwini Patil 
	Date:18-10-2023 
	Rex is using zoho for creating quotation and they are creating contract in eva using contractinvoicepaymnet api.
	When they multiple times change quotation status to won, multiple contracts getting created for same quotation.
	Requirement is if contract exist for zoho quotation id then do not create new contract. Update the existing contract.
	*/
	@Index
	protected String  zohoQuotID;
	
	@Index
	protected String  servicetype; //Ashwini Patil Date:29-02-2024 For orion
	
	protected String updatedBy;
	
	protected String updationLog;
	
	public Contract() 
	{
		super();
		payments=new ArrayList<Payment>();
		contractStatusChanged=false;
		refContractCount=-1;
		renewFlag=false;
		customerInterestFlag=false;
		refNo=0+"";
		renewProcessed=AppConstants.NO;
		ticketNumber=-1;
		complaintFlag=false;
		isQuotation=false;
		
		branchWiseBilling = false;
		
		paymentRecieved=0;
		balancePayment=0;
		newcompanyname="";
		newcustomerfullName="";
		customersaveflag = false;
		asset= new ArrayList<ClientSideAsset>();
		quickContractInvoiceDate = null;
		contractRate =false;
//		numberRange="";
		
		refNo2="";
		technicianName="";
		
	//  rohan added this code for adding payment info in contract 
		
		bankName="";
		bankBranch="";
		bankAccNo="";
		chequeNo="";
		referenceNo="";
		chequeIssuedBy="";
		
		segment="";
		customerGSTNumber="";
		serviceWiseBilling=false;
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		consolidatePrice =false;
		
		contractPeriod ="";
		/**
		 * nidhi
		 * 29-01-2018
		 * for poc name for contract
		 */
		pocName = "";
		/**
		 *  end
		 */
		renewContractFlag = false;
		/**
		 * nidhi
		 * 8-06-2018
		 */
		nonRenewalRemark = "";
		warehouseName ="";
		stationedTechnician = false;
		ipAddress = "";
		acceptedDateTime = null;
//		isRateContract = false;
		
		isQuickContract = false;
		
		invoicePdfNameToPrint ="";
		servicetype="";
	}
	
	

	public String getZohoQuotID() {
		return zohoQuotID;
	}


	public void setZohoQuotID(String zohoQuotID) {
		this.zohoQuotID = zohoQuotID;
	}

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}



	/**************************************************Getter/Setter***********************************************************/
	
	public String getContractPeriod() {
		return contractPeriod;
	}
	

	public String getPaymentMode() {
		return paymentMode;
	}



	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}



	public void setContractPeriod(String contractPeriod) {
		this.contractPeriod = contractPeriod;
	}
	
	
	public boolean isServiceWiseBilling() {
		return serviceWiseBilling;
	}
	public void setServiceWiseBilling(boolean serviceWiseBilling) {
		this.serviceWiseBilling = serviceWiseBilling;
	}
	
	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}
	
	public String getTechnicianName() {
		return technicianName;
	}

	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}
	public String getRefNo2() {
		return refNo2;
	}

	public void setRefNo2(String refNo2) {
		this.refNo2 = refNo2;
	}
	
	public Date getQuickContractInvoiceDate() {
		return quickContractInvoiceDate;
	}

	public void setQuickContractInvoiceDate(Date quickContractInvoiceDate) {
		this.quickContractInvoiceDate = quickContractInvoiceDate;
	}

	public String getRenewProcessed() {
		return renewProcessed;
	}

	public boolean isContractRate() {
		return contractRate;
	}

	public void setContractRate(boolean contractRate) {
		this.contractRate = contractRate;
	}

	public boolean isCustomersaveflag() {
		return customersaveflag;
	}

	public void setCustomersaveflag(boolean customersaveflag) {
		this.customersaveflag = customersaveflag;
	}

	public double getBalancePayment() {
		return balancePayment;
	}

	public void setBalancePayment(double balancePayment) {
		this.balancePayment = balancePayment;
	}

	public double getPaymentRecieved() {
		return paymentRecieved;
	}

	public void setPaymentRecieved(double paymentRecieved) {
		this.paymentRecieved = paymentRecieved;
	}

	public String getNewcompanyname() {
		return newcompanyname;
	}

	public void setNewcompanyname(String newcompanyname) {
		this.newcompanyname = newcompanyname;
	}

	public String getNewcustomerfullName() {
		return newcustomerfullName;
	}

	public void setNewcustomerfullName(String newcustomerfullName) {
		this.newcustomerfullName = newcustomerfullName;
	}

	public Long getNewcustomercellNumber() {
		return newcustomercellNumber;
	}

	public void setNewcustomercellNumber(Long newcustomercellNumber) {
		this.newcustomercellNumber = newcustomercellNumber;
	}

	public String getNewcustomerEmail() {
		return newcustomerEmail;
	}

	public void setNewcustomerEmail(String newcustomerEmail) {
		this.newcustomerEmail = newcustomerEmail;
	}

	public Address getNewcustomerAddress() {
		return newcustomerAddress;
	}

	public void setNewcustomerAddress(Address newcustomerAddress) {
		this.newcustomerAddress = newcustomerAddress;
	}

	public void setRenewProcessed(String renewProcessed) {
		this.renewProcessed = renewProcessed;
	}
	
	public Date getRefDate() {
		return refDate;
	}

	public ArrayList<ClientSideAsset> getClientSideAsset() {
		return asset;
	}

	public void setClientSideAsset(List<ClientSideAsset> asset)
	{
		if(asset!=null){
			ArrayList<ClientSideAsset> list=new ArrayList<ClientSideAsset>();
			list.addAll(asset);
			this.asset = list;
			System.out.println("Size of Assets"+this.asset.size());
		}
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		if(refNo!=null){
			this.refNo = refNo;
		}
	}

	public void setRefDate(Date refDate) {
		if(refDate!=null){
			this.refDate = refDate;
		}
	}

	/**
	 * Gets the advance.
	 * @return the advance
	 */
	public double getadvance() 
	{
		return this.advance;
	}

	/**
	 * Sets the advance.
	 * @param advance the new advance
	 */
	public void setadvance(Double advance)
	{
		if(advance!=null)
		  this.advance=advance;

	}
	
	

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments)
	{
		if(payments!=null){
			this.payments=new ArrayList<Payment>();
			this.payments.addAll(payments);
			System.out.println("Size of Payment"+this.payments.size());
		}
	}

	/**
	 * Gets the balance.
	 * @return the balance
	 */
	//@SearchAnnotation(Datatype = ColumnDatatype.CHECKBOX, flexFormNumber = "12", title = "Balance", variableName = "lbBalance")
	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 11, isFieldUpdater = false, isSortable = true, title = "Balance")
	public double getbalance()
	{
		return balance;
	}

	/**
	 * Sets the balance.
	 * @param balance the new balance
	 */
	public void setbalance(Double balance)
	{
		if(balance!=null)
		  this.balance=balance;
	}
	
	public Integer getRefContractCount() {
		return refContractCount;
	}

	public void setRefContractCount(int refContractCount) {
		this.refContractCount = refContractCount;
	}

	public Boolean isRenewFlag() {
		return renewFlag;
	}

	public void setRenewFlag(boolean renewFlag) {
		this.renewFlag = renewFlag;
	}
	
	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		if(contractDate!=null){
			this.contractDate = contractDate;
		}
	}
	
	public boolean isCustomerInterestFlag() {
		return customerInterestFlag;
	}

	public void setCustomerInterestFlag(boolean customerInterestFlag) {
		this.customerInterestFlag = customerInterestFlag;
	}

	public String getProductGroup() {
		return productGroup;
	}



	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

     
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@OnSave
	@GwtIncompatible
	public void setLastUpdatedDate(){
//		setLastUpdatedDate(DateUtility.setTimeToMidOfDay( new Date()));
		setLastUpdatedDate(new Date());//Ashwini Patil Date:25-06-2024 removed setTimeToMidOfDay as it was saving 12:00 time and due to which it is becoming difficult to trace update related issues
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	/**************************************************Relation Management Part****************************************************/
	
	
	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CREATED);
		statuslist.add(APPROVED);
		statuslist.add(Contract.REJECTED);
		statuslist.add(Contract.REQUESTED);
		statuslist.add(CONTRACTCANCEL);
		statuslist.add(CONTRACTEXPIRED);
		/**
		 * nidhi
		 * 18-05-2018
		 * 
		 */
		statuslist.add(CONTRACTDISCOUNTINED);
		/**
		 * end
		 */
		return statuslist;
	}


	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		Logger logger=Logger.getLogger("Starting Approval Logger ");
		logger.log(Level.SEVERE,"Starting Approval Process");
//		ContractServiceImplementor implementor=new ContractServiceImplementor();
//		implementor.changeStatus(this);
//		logger.log(Level.SEVERE,"Impl Called For Services");
//		
//		if(this.getPaymentTermsList().size()>0)
//		{
//			logger.log(Level.SEVERE,"Billing Method Processing");
//			createBillingDocs();
//		}
//			
		
		/**
		 * if contract is created against complain then that contract may be billable or non billable 
		 * and service is not getting created at the time of approval.
		 *  
		 *  if contract is billable then only it creates billing document.
		 */
		if(getComplaintFlag()!=null){
			if(getComplaintFlag()==false&&getTicketNumber()==-1){
				logger.log(Level.SEVERE,"Complain Flage False and Not NULL Creating Services");
				ContractServiceImplementor implementor=new ContractServiceImplementor();
				implementor.changeStatus(this);
			}
		}
		else{
			logger.log(Level.SEVERE,"Complain Flage False NULL Creating Services");
			ContractServiceImplementor implementor=new ContractServiceImplementor();
			implementor.changeStatus(this);
		}
		
		
		
		logger.log(Level.SEVERE,"Impl Called For Services");
		
		if(this.getPaymentTermsList().size()>0)
		{
			
			logger.log(Level.SEVERE,"Billing Method Processing");
			if(getComplaintFlag()!=null&&getTicketNumber()!=-1){
				if(getComplaintFlag()==true){
					Complain complain=ofy().load().type(Complain.class).filter("companyId", this.getCompanyId()).filter("count", getTicketNumber()).first().now();
					if(complain!=null){
						complain.setExistingContractId(this.getCount());
						ofy().save().entities(complain);
					}
					if(complain.getBbillable()==true){
						logger.log(Level.SEVERE," Com Flag True Billable");
						/**Date 9-5-2020 by Amol added this for creating services if contract is billable**/
						ContractServiceImplementor implementor=new ContractServiceImplementor();
						implementor.makeServices(this);
						// added by vijay this flag for when contract is Rate contract then billing will not create from here
					
						/** 
						 * nidhi
						 * this line done for when net  this.getNetpayable() > 0 than not allow to create bill on 0 net amount
						 */
						
						if(this.isContractRate()==false && this.getNetpayable() > 0){
							createBillingDocs();
						}
							
					}
				}
			}
			else{
				logger.log(Level.SEVERE," Com Flag false  Billable");
				// added by vijay this flag for when contract is Rate contract then billing will not create from here
				/** 
				 * nidhi
				 * this line done for when net  this.getNetpayable() > 0 than not allow to create bill on 0 net amount
				 */
				
				if(this.isContractRate()==false && this.getNetpayable() > 0){
					createBillingDocs();
				}
				
			}
		}
		
		
		/**
		 * Date :- 10 Nov 2016
		 * by Vijay
		 * Description :-  this for UniversalpestControl to send SMS when we approve the contract must send sms to customer welcome sms
		 */
			System.out.println("before Call SMS method");
			CheckSMSConfiguration();
		
			/**
			 * ends here 
			 */
			
		
		if(this.getCount()!=0){
			final GenricServiceImpl genimpl=new GenricServiceImpl();
			TaxesAndCharges taxChargeEntity=new TaxesAndCharges();
			List<ContractCharges> listaxcharge=this.saveCharges();
			ArrayList<ContractCharges> arrtaxchrg=new ArrayList<ContractCharges>();
			arrtaxchrg.addAll(listaxcharge);
			taxChargeEntity.setContractId(this.getCount());
			taxChargeEntity.setIdentifyOrder(AppConstants.BILLINGSERVICEFLOW.trim());
			taxChargeEntity.setTaxesChargesList(arrtaxchrg);
			taxChargeEntity.setCompanyId(this.getCompanyId());
			
			genimpl.save(taxChargeEntity);
		}
		
		
		
//		/*  This line of code is used to change the status of Contract Renewal entity from inprocessed to renewed. 
//		 * 
//		 * */
//		
//		if(this.getRefContractCount()!=-1){
//			ContractRenewal conRenew=ofy().load().type(ContractRenewal.class).filter("companyId", this.getCompanyId()).filter("contractId", this.getRefContractCount()).first().now();
//			if(conRenew!=null){
//				System.out.println("CONTRACT RENEWAL STATUS !!!!!!!!!!!!!!!!!!!");
//				conRenew.setStatus("Renewed");
//				conRenew.setDescription("New Contract Id : "+this.getCount());
//				
//				ofy().save().entity(conRenew).now();
//			}
//		}
		
		
		logger.log(Level.SEVERE, "for renewal of contract"+this.getStatus());
		logger.log(Level.SEVERE, "for renewal of contract"+this.getRefContractCount());

		if(this.getStatus().equals(Contract.APPROVED)&&this.getRefContractCount()!=null && this.getRefContractCount()!=-1)
		{
			logger.log(Level.SEVERE, "inside renewal update logic"+this.getRefContractCount());

			Contract contractEntity=ofy().load().type(Contract.class).filter("companyId", getCompanyId()).filter("count", this.getRefContractCount()).first().now();
			if(contractEntity!=null&&contractEntity.isRenewFlag()==false){
				contractEntity.setRenewProcessed(AppConstants.YES);
				contractEntity.setRenewFlag(true);
				ofy().save().entity(contractEntity).now();
				
				ContractRenewalServiceImpl contractrenewal = new ContractRenewalServiceImpl();
				contractrenewal.contractRenewalEntry(contractEntity,AppConstants.RENEWED,""); 
			}
		}
		
		
		
		ProcessName processName=ofy().load().type(ProcessName.class).filter("companyId", this.getCompanyId()).filter("processName",AppConstants.PROCESSNAMEACCINTERFACE).filter("status", true).first().now();
		ProcessConfiguration processConfig=null;
		if(processName!=null){
			processConfig = ofy().load().type(ProcessConfiguration.class).filter("processName",AppConstants.PROCESSCONFIGCO).filter("companyId", getCompanyId()).filter("configStatus", true).first().now();
		}
		int flag =0;
		
		if(processConfig!=null){
			for(int i = 0;i<processConfig.getProcessList().size();i++){
				if(processConfig.getProcessList().get(i).getProcessType().trim().equalsIgnoreCase(AppConstants.PROCESSTYPEACCINTERFACE)&&processConfig.getProcessList().get(i).isStatus()==true){
					flag = 1;
				}
			}
		}
		
		if(flag == 1&&processConfig!=null){
			System.out.println("ExecutingAccInterface");
			accountingInterface();
		}
	}
	
	
	@GwtIncompatible
	@OnSave
	private void setQuotationId()
	{
		Quotation quot;
		System.out.println("Called Quotation Count"+(id==null &&quotationCount!=-1));
		if(id==null &&quotationCount!=-1)
		{
		  if(getCompanyId()==null)
			 quot= ofy().load().type(Quotation.class).filter("count",quotationCount).first().now();
		  else
			  quot= ofy().load().type(Quotation.class).filter("count",quotationCount).filter("companyId", getCompanyId()).first().now();
		
		   if(quot!=null)
		   {
			   quot.setContractCount(count);
			   ofy().save().entity(quot).now();
			   /**
			    * @author Vijay Date 22-01-2020 for copying address from Quotation to Contract
			    */
			   if(quot.getServiceAddress()!=null)
			   this.setCustomerServiceAddress(quot.getServiceAddress());
			   if(quot.getBillingAddress()!=null)
			   this.setNewcustomerAddress(quot.getBillingAddress());
			   System.out.println("quotation address storing in Contract");

		   }
		   
		   
		}
//		/**
//		 *  Date : 27-02-2021 Added By Priyanka.
//		 *  Des : Contract renewal improperly updating status issue raised by Ashwini.
//		 */
//		if(this.getStatus().equals(Contract.APPROVED)&&this.getRefContractCount()!=null && this.getRefContractCount()!=-1)
//		{
//			System.out.println("for renewal");
//			Contract contractEntity=ofy().load().type(Contract.class).filter("companyId", getCompanyId()).filter("count", this.getRefContractCount()).first().now();
//			if(contractEntity!=null&&contractEntity.isRenewFlag()==false){
//				contractEntity.setRenewProcessed(AppConstants.YES);
//				contractEntity.setRenewFlag(true);
//				ofy().save().entity(contractEntity).now();
//				
//				/**
//				 * @author Vijay Date 08-12-2020
//				 * Des :- when we make renewal from contract screen then its entry must be create in contract renewal screen 
//				 * so we can get reports in contract renewal screen for renewal
//				 */
//				ContractRenewalServiceImpl contractrenewal = new ContractRenewalServiceImpl();
//				contractrenewal.contractRenewalEntry(contractEntity,AppConstants.RENEWED); 
//				/**
//				 * ends here
//				 */
//				
//			}
//		}
		
		if(getId()==null&&this.getRefContractCount()!=null && this.getRefContractCount()!=-1){
			Contract contractEntity=ofy().load().type(Contract.class).filter("companyId", getCompanyId()).filter("count", this.getRefContractCount()).first().now();
			if(contractEntity!=null&&contractEntity.isRenewFlag()==false){
				/**
				 * @author Vijay Date 08-12-2020
				 * Des :- when we make renewal from contract screen then its entry must be create in contract renewal screen 
				 * so we can get reports in contract renewal screen for renewal
				 */
				ContractRenewalServiceImpl contractrenewal = new ContractRenewalServiceImpl();
				contractrenewal.contractRenewalEntry(contractEntity,AppConstants.PROCESSED,""); 
				/**
				 * ends here
				 */
				
			}
		}
		
	
	
}
	
	//********************rohan added this code for lead successful **************
	
	@GwtIncompatible
	@OnSave
	private void setLeadStatus()
	{
		Lead lead;
		System.out.println("Called Quotation Count"+(id==null &&leadCount!=-1));
		if(id==null &&leadCount!=-1)
		{
		  if(getCompanyId()==null)
			  lead= ofy().load().type(Lead.class).filter("count",leadCount).first().now();
		  else
			  lead= ofy().load().type(Lead.class).filter("count",leadCount).filter("companyId", getCompanyId()).first().now();
		
	
		   if(lead!=null)
		   {
			   /**Date 23-12-2019 by Amol set Contract Count in Lead**/
				if(lead.getCount()!=0){
					 lead.setContractCount(this.getCount()+"");
				}
			   lead.setStatus(AppConstants.Successful);
			   ofy().save().entity(lead).now();
		   }
		}
		
		
		
		
		
		
		
		
		
		/**
		 * Date 12-07-2019 by Vijay in above method same code is written so commented this.
		 */
//		if(getId()==null&&this.getRefContractCount()!=null)
//		{
//			Contract contractEntity=ofy().load().type(Contract.class).filter("companyId", getCompanyId()).filter("count", this.getRefContractCount()).first().now();
//			if(contractEntity!=null){
//				contractEntity.setRefContractCount(this.getCount());
//				contractEntity.setRenewFlag(true);
//				ofy().save().entity(contractEntity).now();
//			}
//		}
		
	}
	
	//***************************changes ends here *******************************
	@OnSave
	@GwtIncompatible
	private void updateComplaint(){
		if(id==null){
			if(getTicketNumber()!=-1&&getIsQuotation()==false){
				System.out.println("INSIDE CONTRACT ON SAVE....!!!!");
				Complain complain=ofy().load().type(Complain.class).filter("companyId", this.getCompanyId()).filter("count", this.getTicketNumber()).first().now();
				if(complain!=null){
					complain.setContrtactId(this.getCount());
				}
				ofy().save().entity(complain);
			}
		}
	}
	
	
	/******************** vijay ************************/
	@OnSave
	@GwtIncompatible
	private void savecustomer(){
		
		System.out.println("full Name === "+this.getNewcustomerfullName());
		System.out.println("compnay name ==="+this.getNewcompanyname());

		
		if(this.isCustomersaveflag()==false){
		if(!this.newcompanyname.equals("") || !this.newcustomerfullName.equals("")){
			
			System.out.println("customer iddddd === "+this.getCinfo().getCount());

			if(this.getCinfo().getCount()==0){
				
			System.out.println("inside new customer method");
			
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			
			Customer cust = new Customer();
			  
			System.out.println("compnay name ==="+this.getNewcompanyname());
			System.out.println("hi == "+ !this.getNewcompanyname().equals(""));
			
			
			
				if(!this.getNewcompanyname().equals("")){
					System.out.println("22222222222222");
					cust.setCompany(true);
					cust.setCompanyName(this.getNewcompanyname().toUpperCase());
				}else{
					System.out.println("111111111");
					cust.setCompany(false);
				}
				if(this.getNewcustomerfullName()!=null){
					cust.setFullname(this.getNewcustomerfullName().toUpperCase());
				}
				if(this.getNewcustomerEmail()!=null){
					cust.setEmail(this.getNewcustomerEmail());
				}
				if(this.getNewcustomercellNumber()!=null){
					cust.setCellNumber1(this.getNewcustomercellNumber());
				}
				
				//  rohan added branch in customer
				if(this.getNewCustomerBranch()!=null){
					cust.setBranch(this.getNewCustomerBranch());
				}
				
				cust.setNewCustomerFlag(true);

				if (this.getNewcustomerAddress()!=null) {
					cust.setAdress(this.getNewcustomerAddress());
					cust.setSecondaryAdress(this.getNewcustomerAddress());
				}
				cust.setCompanyId(this.getCompanyId());
			   
				
				// Date 02-09-2017 added by vijay for new customer GST
				if(this.getCustomerGSTNumber()!=null && !this.getCustomerGSTNumber().equals("")){
				ArticleType articalType=new ArticleType();
				articalType.setDocumentName("Invoice");
				articalType.setArticleTypeName("GSTIN");
				articalType.setArticleTypeValue(this.getCustomerGSTNumber());
				articalType.setArticlePrint("Yes");
				cust.getArticleTypeDetails().add(articalType);
				}
				
				genimpl.save(cust);	   

				System.out.println("after saving customer count ==="+cust.getCount());
				
				PersonInfo pinfo = new PersonInfo();
				
				pinfo.setCount(cust.getCount());
				
				if(cust.isCompany()){
					pinfo.setFullName(cust.getCompanyName());
					pinfo.setPocName(cust.getFullname());
				}else{
					pinfo.setFullName(cust.getFullname());
					pinfo.setPocName(cust.getFullname());
				}
				
				pinfo.setCellNumber(cust.getCellNumber1());
				pinfo.setEmail(cust.getEmail());
				
				this.setCinfo(pinfo);
			
			}else{
				
				
			 Customer customer = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getCinfo().getCount()).first().now();
				
				System.out.println("compnay name ==="+this.getNewcompanyname());
				System.out.println("hi == "+ !this.getNewcompanyname().equals(""));
				System.out.println("for editing customer ===");
				
				if(!this.getNewcompanyname().equals("")){
					System.out.println("22222222222222");
					customer.setCompany(true);
					customer.setCompanyName(this.getNewcompanyname().toUpperCase());
				}else{
					System.out.println("111111111");
					customer.setCompany(false);
				}
				if(this.getNewcustomerfullName()!=null){
					customer.setFullname(this.getNewcustomerfullName().toUpperCase());
				}
				if(this.getNewcustomerEmail()!=null){
					customer.setEmail(this.getNewcustomerEmail());
				}
				if(this.getNewcustomercellNumber()!=null){
					customer.setCellNumber1(this.getNewcustomercellNumber());
				}

				if (this.getNewcustomerAddress()!=null) {
					customer.setAdress(this.getNewcustomerAddress());
					customer.setSecondaryAdress(this.getNewcustomerAddress());
				}
				customer.setCompanyId(this.getCompanyId());
			   
			//  rohan added branch in customer
				if(this.getNewCustomerBranch()!=null){
					customer.setBranch(this.getNewCustomerBranch());
				}
				
				
				// Date 02-09-2017 added by vijay for new customer GST
				if(this.getCustomerGSTNumber()!=null && !this.getCustomerGSTNumber().equals("")){
				ArticleType articalType=new ArticleType();
				articalType.setDocumentName("Invoice");
				articalType.setArticleTypeName("GSTIN");
				articalType.setArticleTypeValue(this.getCustomerGSTNumber());
				articalType.setArticlePrint("Yes");
				customer.getArticleTypeDetails().add(articalType);
				}

				ofy().save().entity(customer);
				
				System.out.println("customer updated");
				
				PersonInfo pinfo = new PersonInfo();
				pinfo.setCount(customer.getCount());
			
				if(customer.isCompany()){
					pinfo.setFullName(customer.getCompanyName());
					pinfo.setPocName(customer.getFullname());
				}else{
					pinfo.setFullName(customer.getFullname());
					pinfo.setPocName(customer.getFullname());
				}
				
				pinfo.setCellNumber(customer.getCellNumber1());
				pinfo.setEmail(customer.getEmail());
				
				this.setCinfo(pinfo);
			}
		}
	}
	}
	/*********************************************/
	
	
	
	/** Date 13-09-2017 added by vijay for customer billing and service address saving *******/
	@OnSave
	@GwtIncompatible
	private void savecustomerAddress(){
		if(id==null && this.getCustomerServiceAddress()==null){
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.getCinfo().getCount()).first().now();
			 if(customer!=null){
				 this.setNewcustomerAddress(customer.getAdress());
				 this.setCustomerServiceAddress(customer.getSecondaryAdress());
				 this.getCinfo().setEmail(customer.getEmail());
			 }
		}
		 
	}
	
	public double getTotalBillAmt(List<SalesOrderProductLineItem> lisForTotalBill)
	{
		double saveTotalBillAmt=0;
		for(int i=0;i<lisForTotalBill.size();i++)
		{
			//*****old code   
			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getBaseBillingAmount();
			
			//***************new code 
//			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getTotalAmount();
		}
		
		System.out.println("Save Total Billing Amount"+saveTotalBillAmt);
		return saveTotalBillAmt;
	}
	
	public double getTotalFromTaxTable(ArrayList<ContractCharges> lisForTotalBill)
	{
		double totalCalcAmt=0;
		for(int i=0;i<lisForTotalBill.size();i++)
		{
			//*****old code   
			totalCalcAmt+=lisForTotalBill.get(i).getTaxChargeAssesVal()*lisForTotalBill.get(i).getTaxChargePercent()/100;
			
			System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getChargesBalanceAmt());
			System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getPayableAmt());
			System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getTaxChargeAbsVal());
			System.out.println("aaaaaaaaaaaaaaaaaaaa"+lisForTotalBill.get(i).getTaxChargeAssesVal());
			
			//***************new code 
//			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getTotalAmount();
		}
		
	
		System.out.println("Save Total Billing Amount"+totalCalcAmt);
		return totalCalcAmt;
	}
	
//	private double getTotalFromTaxTableForOtherCharge(ArrayList<ContractCharges> lisForTotalBill)
//	{
//		double saveTotalBillAmt=0;
//		for(int i=0;i<lisForTotalBill.size();i++)
//		{
//			//*****old code   
//			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getTaxChargeAssesVal();
//			
//			//***************new code 
////			saveTotalBillAmt=saveTotalBillAmt+lisForTotalBill.get(i).getTotalAmount();
//		}
//		
//		System.out.println("Save Total Billing Amount"+saveTotalBillAmt);
//		return saveTotalBillAmt;
//	}
	
	
	/**
	 * Methods returns the arraylist of sales order products.
	 * The Base Billing Amount is calculated as per percent corresponding to payment terms.
	 * @param payTermPercent
	 * @return
	 */
	@GwtIncompatible
	public List<SalesOrderProductLineItem> retrieveSalesProducts(double payTermPercent)
	{
		/**
		 * nidhi
		 * 21-09-2018
		 * for stop calculation for area into prod price
		 * for nbhc
		 */
		boolean areaCalFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "DisableAreaWiseBillingForAmc", this.getCompanyId());
		/**
		 * end
		 */
		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0,discAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<this.getItems().size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", this.getItems().get(i).getPrduct().getCount()).first().now();
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
			//SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProductSrNumber(this.getItems().get(i).getProductSrNo());
			salesOrder.setProdId(this.getItems().get(i).getPrduct().getCount());
			salesOrder.setProdCategory(this.getItems().get(i).getProductCategory());
			salesOrder.setProdCode(this.getItems().get(i).getProductCode());
			salesOrder.setProdName(this.getItems().get(i).getProductName());
			salesOrder.setQuantity(this.getItems().get(i).getQty());
			salesOrder.setOrderDuration(this.getItems().get(i).getDuration());
			salesOrder.setOrderServices(this.getItems().get(i).getNumberOfServices());
			totalTax=this.removeTaxAmt(this.getItems().get(i).getPrduct());
			prodPrice=this.getItems().get(i).getPrice()-totalTax;
			System.out.println("Prod Price"+prodPrice);
			salesOrder.setPrice(prodPrice);
			salesOrder.setVatTaxEdit(this.getItems().get(i).getVatTaxEdit());
			salesOrder.setServiceTaxEdit(this.getItems().get(i).getServiceTaxEdit());
			salesOrder.setVatTax(this.getItems().get(i).getVatTax());
			salesOrder.setServiceTax(this.getItems().get(i).getServiceTax());
			
			//  rohan added this code for area considered in calculation
			salesOrder.setArea(this.getItems().get(i).getArea());
		//  rohan added this code for setting HSN Code Date : 26-06-2017
			salesOrder.setHsnCode(this.getItems().get(i).getPrduct().getHsnNumber());
			
			salesOrder.setProdPercDiscount(this.getItems().get(i).getPercentageDiscount());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			if(this.getItems().get(i).getUnitOfMeasurement()!=null){
				salesOrder.setUnitOfMeasurement(this.getItems().get(i).getUnitOfMeasurement());
			}
			
			//*****************rohan commented this code***************** 
			
			
			salesOrder.setDiscountAmt(this.getItems().get(i).getDiscountAmt());
			
			
			
			
			
			
			
			
//			if(this.getItems().get(i).getPercentageDiscount()!=0){
//				percAmt=prodPrice-(prodPrice*this.getItems().get(i).getPercentageDiscount()/100);
//				percAmt=percAmt*this.getItems().get(i).getQty();
//			}
//			if(this.getItems().get(i).getPercentageDiscount()==0){
//				percAmt=prodPrice*this.getItems().get(i).getQty();
//			}
			
			
			/********************** vijay commented this code for area wise calculation **********************/
//			if((this.getItems().get(i).getPercentageDiscount()==null && this.getItems().get(i).getPercentageDiscount()==0) && (this.getItems().get(i).getDiscountAmt()==0) ){
//				
//				System.out.println("inside both 0 condition");
//				percAmt=prodPrice;
//			}
//			
//			else if((this.getItems().get(i).getPercentageDiscount()!=null)&& (this.getItems().get(i).getDiscountAmt()!=0)){
//				
//				System.out.println("inside both not null condition");
//				
//				percAmt=prodPrice-(prodPrice*this.getItems().get(i).getPercentageDiscount()/100);
//				percAmt=percAmt-(this.getItems().get(i).getDiscountAmt());
//				percAmt=percAmt*this.getItems().get(i).getQty();
//			}
//			else 
//			{
//				System.out.println("inside oneof the null condition");
//				
//					if(this.getItems().get(i).getPercentageDiscount()!=null && this.getItems().get(i).getPercentageDiscount()!=0){
//						System.out.println("inside getPercentageDiscount oneof the null condition");
//						percAmt=prodPrice-(prodPrice*this.getItems().get(i).getPercentageDiscount()/100);
//						
//					}
//					else 
//					{
//						System.out.println("inside getDiscountAmt oneof the null condition");
//						percAmt=prodPrice-(this.getItems().get(i).getDiscountAmt());
//					}
//					percAmt=percAmt*this.getItems().get(i).getQty();
//			}
			
			/***************** new code area calculation and discount on total *******************/
			//  rohan remove qty form price date : 10/11/2016
			
			if((this.getItems().get(i).getPercentageDiscount()==null && this.getItems().get(i).getPercentageDiscount()==0) && (this.getItems().get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				/**
				 * nidhi
				 * 21-09-2018
				 * areaCalFlag flag for stop area wise calculation
				 */
				if(!this.getItems().get(i).getArea().equalsIgnoreCase("NA") && !areaCalFlag){
					double squareArea = Double.parseDouble(this.getItems().get(i).getArea());
					percAmt=prodPrice*squareArea;
				}
				else{
						System.out.println("Old code");
						percAmt=prodPrice;
				}
				
			}
			
			else if((this.getItems().get(i).getPercentageDiscount()!=null)&& (this.getItems().get(i).getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				
				/**
				 * nidhi
				 * 21-09-2018
				 * areaCalFlag flag for stop area wise calculation
				 */
				if(!this.getItems().get(i).getArea().equalsIgnoreCase("NA") && !areaCalFlag){
					double squareArea = Double.parseDouble(this.getItems().get(i).getArea());
					percAmt=prodPrice*squareArea;
					percAmt= percAmt-(percAmt*this.getItems().get(i).getPercentageDiscount()/100);
					percAmt=percAmt-this.getItems().get(i).getDiscountAmt();
				}else{
					
					percAmt=prodPrice;
					percAmt=percAmt-(percAmt*this.getItems().get(i).getPercentageDiscount()/100);
					percAmt=percAmt-this.getItems().get(i).getDiscountAmt();
				}
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				/**
				 * nidhi
				 * 21-09-2018
				 * areaCalFlag flag for stop area wise calculation
				 */
				if(this.getItems().get(i).getPercentageDiscount()!=null && this.getItems().get(i).getPercentageDiscount()!=0){
					if(!this.getItems().get(i).getArea().equalsIgnoreCase("NA") && !areaCalFlag){
						Double squareArea = Double.parseDouble(this.getItems().get(i).getArea());
						percAmt = prodPrice*squareArea;
						percAmt=percAmt-(percAmt*this.getItems().get(i).getPercentageDiscount()/100);
					}else{
						percAmt=prodPrice;
						percAmt=percAmt-(percAmt*this.getItems().get(i).getPercentageDiscount()/100);
					}
					
				}
				else 
				{
					/**
					 * nidhi
					 * 21-09-2018
					 * areaCalFlag flag for stop area wise calculation
					 */
					if(!this.getItems().get(i).getArea().equals("NA") && !areaCalFlag){
						Double squareArea = Double.parseDouble(this.getItems().get(i).getArea());
						percAmt = prodPrice*squareArea;
						percAmt=percAmt-this.getItems().get(i).getDiscountAmt();
					}else{
						percAmt=prodPrice;
						percAmt=percAmt-this.getItems().get(i).getDiscountAmt();
					}
				}
				System.out.println("percAmt ====== "+percAmt);
			}

	/******************************************************************************************************/
			
			salesOrder.setTotalAmount(percAmt);
			if(payTermPercent!=0){
				baseBillingAmount=(percAmt*payTermPercent)/100;
			}
		
//			salesOrder.setBaseBillingAmount(Math.round(baseBillingAmount));
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			
			/**
			 * Date : 04-08-2017 By Anil
			 * Setting base bill amount to base payment amount
			 */
			/**Date : 14/3/2018 By Manisha
			 * Remove round off value as per vaishnavi's requirement.
			 */
//			salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
			salesOrder.setBasePaymentAmount(baseBillingAmount);
			/**
			 * End
			 */
			salesOrder.setIndexVal(i+1);
			/**
			 * nidhi
			 * 20-06-2018
			 *  for manage product sr number ; 
			 *  
			 */
			salesOrder.setProductSrNumber(this.getItems().get(i).getProductSrNo());

			/**
			 * Date 06/06/2018 By vijay
			 * for product warranty need to show in invoice
			 */
			if(this.getItems().get(i).getWarrantyPeriod()!=0)
			salesOrder.setWarrantyPeriod(this.getItems().get(i).getWarrantyPeriod());
			
			/**
			 * ends here
			 */
			
			salesOrder.setPrduct(superProdEntity);
			/**
			 * nidhi
			 * 8-08-2018
			 */
			salesOrder.setProModelNo(this.getItems().get(i).getProModelNo());
			salesOrder.setProSerialNo(this.getItems().get(i).getProSerialNo());
			
			/**
			 * Date 09-04-2019 by Vijay
			 * Des :- NBHC CCPM To know revenue loss so i have storing billing base amount in another filed.
			 *  so make in the invoice they will know the loss revenue.
			 */
			salesOrder.setBillingDocBaseAmount(salesOrder.getBaseBillingAmount());
			
			
			/////////////////////////////////////////////////////////////////////////////////
			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
	}
	public List<ContractCharges> listForBillingTaxes(double percPay){
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
		double assessValue=0;
		for(int i=0;i<this.productTaxes.size();i++){
			ContractCharges taxDetails=new ContractCharges();
			taxDetails.setTaxChargeName(getProductTaxes().get(i).getChargeName());
			taxDetails.setTaxChargePercent(getProductTaxes().get(i).getChargePercent());
			assessValue=getProductTaxes().get(i).getAssessableAmount()*percPay/100;
			taxDetails.setTaxChargeAssesVal(assessValue);
			
			taxDetails.setIdentifyTaxCharge(getProductTaxes().get(i).getIndexCheck());
			arrBillTax.add(taxDetails);
		}
		return arrBillTax;
	}
	
	private List<ContractCharges> saveCharges()
	{
		ArrayList<ContractCharges> arrCharge=new ArrayList<ContractCharges>();
		double calcBalAmt=0;
		for(int i=0;i<this.getProductCharges().size();i++)
		{
			ContractCharges chargeDetails=new ContractCharges();
			chargeDetails.setTaxChargeName(getProductCharges().get(i).getChargeName());
			chargeDetails.setTaxChargePercent(getProductCharges().get(i).getChargePercent());
			chargeDetails.setTaxChargeAbsVal(getProductCharges().get(i).getChargeAbsValue());
			System.out.println("Find Assess"+getProductCharges().get(i).getAssessableAmount());
			chargeDetails.setTaxChargeAssesVal(getProductCharges().get(i).getAssessableAmount());
			if(getProductCharges().get(i).getChargeAbsValue()!=0){
				calcBalAmt=getProductCharges().get(i).getChargeAbsValue();
			}
			if(getProductCharges().get(i).getChargePercent()!=0){
				calcBalAmt=getProductCharges().get(i).getChargePercent()*getProductCharges().get(i).getAssessableAmount()/100;
			}
			chargeDetails.setChargesBalanceAmt(calcBalAmt);
			arrCharge.add(chargeDetails);
		}
		return arrCharge;
		
	}
	
	@GwtIncompatible
	protected void createBillingDocs()
	{
		
		 /**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", this.getCompanyId());
		 
		 /**
		  * end
		  */
		 
		Logger billingLogger=Logger.getLogger("Billing Logger");
		billingLogger.log(Level.SEVERE,"BIlling Process Started for Creation");
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());
		
		// below count variable is added by vijay for billing period
	    int count=0;
	    
	    
	    /**
	     * Date : 05-04-2018 By ANIL
	     * billing list is initialized out side the loop
	     */
	    ArrayList<SuperModel> arrbilling=new ArrayList<SuperModel>();
	    
		for(int i=0;i<getPaymentTermsList().size();i++)
		{
			

			BillingDocument billingDocEntity=new BillingDocument();
			List<SalesOrderProductLineItem> salesProdLis=null;
			List<ContractCharges> billTaxLis=null;
			ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
			PaymentTerms paymentTerms=new PaymentTerms();
			Date conStartDate=null;
			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			System.out.println("Contract Start Date"+conStartDate);
		
			
	/***********************Invoice Date******************************/
			
			billingLogger.log(Level.SEVERE,"Contract Date As per IST"+DateUtility.getDateWithTimeZone("IST",this.getContractDate()));
			billingLogger.log(Level.SEVERE,"Contract Date As Per Saved"+this.getContractDate());
			
			Calendar calInvoiceDate = Calendar.getInstance();
			if(this.getContractDate()!=null){
				Date serConDate=DateUtility.getDateWithTimeZone("IST", this.getContractDate());
				calInvoiceDate.setTime(serConDate);
			}
			else{
				conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
				calInvoiceDate.setTime(conStartDate);
			}
			
			calInvoiceDate.add(Calendar.DATE, this.getPaymentTermsList().get(i).getPayTermDays());
			Date calculatedInvoiceDate=null;
			try {
				calInvoiceDate.set(Calendar.HOUR_OF_DAY,23);
				calInvoiceDate.set(Calendar.MINUTE,59);
				calInvoiceDate.set(Calendar.SECOND,59);
				calInvoiceDate.set(Calendar.MILLISECOND,999);
				calculatedInvoiceDate=dateFormat.parse(dateFormat.format(calInvoiceDate.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date"+calculatedInvoiceDate);
				if(currentDate.after(calculatedInvoiceDate))
				{
					calculatedInvoiceDate=currentDate;
				}
				
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***********************************************************************************/
			
			/*********************************Billing Date************************************/
			
			Date calBillingDate=null;
			Calendar calBilling=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calBilling.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate ));
			}
			
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing"+calculatedInvoiceDate);
			
//			calBilling.add(Calendar.DATE, -2);
			calBilling.add(Calendar.DATE, 0);
			try {
				calBilling.set(Calendar.HOUR_OF_DAY,23);
				calBilling.set(Calendar.MINUTE,59);
				calBilling.set(Calendar.SECOND,59);
				calBilling.set(Calendar.MILLISECOND,999);
				calBillingDate=dateFormat.parse(dateFormat.format(calBilling.getTime()));
				
				if(currentDate.after(calBillingDate)){
					calBillingDate=currentDate;
				}
				
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calBillingDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***************************************************************************************/
			
			/********************************Payment Date******************************************/
			
			Date calPaymentDate=null;
			int creditVal=0;
			if(this.getCreditPeriod()!=null){
				creditVal=this.getCreditPeriod();
			}
			
			Calendar calPayment=Calendar.getInstance();
			if(calculatedInvoiceDate!=null){
				calPayment.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate));
			}
			billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Payment"+calculatedInvoiceDate);
			
			calPayment.add(Calendar.DATE, creditVal);
			try {
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				calPayment.set(Calendar.HOUR_OF_DAY,23);
				calPayment.set(Calendar.MINUTE,59);
				calPayment.set(Calendar.SECOND,59);
				calPayment.set(Calendar.MILLISECOND,999);
				calPaymentDate=dateFormat.parse(dateFormat.format(calPayment.getTime()));
				billingLogger.log(Level.SEVERE,"Calculated Billing"+calPaymentDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/***********************************************************************************/
			
			System.out.println("Billing date to be saved==="+calBillingDate);
			
			/******************************************************************************/
			if(this.getCinfo()!=null)
				billingDocEntity.setPersonInfo(this.getCinfo());
			if(this.getCount()!=0)
				billingDocEntity.setContractCount(this.getCount());
			if(this.getStartDate()!=null)
				billingDocEntity.setContractStartDate(this.getStartDate());
			if(this.getEndDate()!=null)
				billingDocEntity.setContractEndDate(this.getEndDate());
			if(this.getNetpayable()!=0)
				billingDocEntity.setTotalSalesAmount(this.getNetpayable());
			salesProdLis=this.retrieveSalesProducts(this.getPaymentTermsList().get(i).getPayTermPercent());
			ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
			salesOrdArr.addAll(salesProdLis);
			System.out.println("Size here here"+salesOrdArr.size());
			if(salesOrdArr.size()!=0)
				billingDocEntity.setSalesOrderProducts(salesOrdArr);
			if(this.getCompanyId()!=null)
				billingDocEntity.setCompanyId(this.getCompanyId());
			/**
			 * Date : 09-08-2017 BY ANIL
			 * if payment date in payment term list is not null then we set bill,invoice and payment date as passed payment date. 
			 */
			if(this.getPaymentTermsList().get(i).getPaymentDate()!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
				billingLogger.log(Level.SEVERE,"Bill-Payment Date Not Null : "+billingDocEntity.getBillingDate());
			
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
				billingLogger.log(Level.SEVERE,"Invoice-Payment Date Not Null : "+billingDocEntity.getInvoiceDate());
				
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
				billingLogger.log(Level.SEVERE,"Payment-Payment Date Not Null : "+billingDocEntity.getPaymentDate());
			
			}else{
				if(calBillingDate!=null){
					billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
					billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
				}
				
				if(calculatedInvoiceDate!=null){
					billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
					billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
				}
				if(calPaymentDate!=null){
					billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
					billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
				}
			}
			
			
			/**
			if(calBillingDate!=null){
				billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
				billingLogger.log(Level.SEVERE,"Calculated Billing Date While SAving"+billingDocEntity.getBillingDate());
			}
			
			if(calculatedInvoiceDate!=null){
				billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
			}
			if(calPaymentDate!=null){
				billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", calPaymentDate));
				billingLogger.log(Level.SEVERE,"Calculated Payment Date While SAving"+billingDocEntity.getPaymentDate());
			}
			*/
			if(this.getPaymentMethod()!=null)
				billingDocEntity.setPaymentMethod(this.getPaymentMethod());
			if(this.getApproverName()!=null)
				billingDocEntity.setApproverName(this.getApproverName());
			if(this.getEmployee()!=null)
				billingDocEntity.setEmployee(this.getEmployee());
			if(this.getBranch()!=null)
				billingDocEntity.setBranch(this.getBranch());
			
			/*
			 *  nidhi
			 *  	30-06-2017
			 */
		
			if(this.getSegment()!=null){
				billingDocEntity.setSegment(this.getSegment());
			}
			/*
			 * End
			 */
			
			ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
			if(this.getProductTaxes().size()!=0){
				ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
				billTaxLis=this.listForBillingTaxes(this.getPaymentTermsList().get(i).getPayTermPercent());
				billTaxArr.addAll(billTaxLis);
				billingDocEntity.setBillingTaxes(billTaxArr);
				billTaxArrROHAN.addAll(billTaxLis);
			}

			if(this.getStartDate()!=null){
				billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST", this.getStartDate()));
			}
			if(this.getEndDate()!=null){
				billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST", this.getEndDate()));
			}
			
	//***********************changes made by rohan for saving gross value *************	
			double grossValue=this.getTotalAmount();
			
			
			billingDocEntity.setGrossValue(grossValue);
			
	//********************************changes ends here *******************************		
			
			
			double totBillAmt=this.getTotalBillAmt(salesProdLis);
			
			double taxAmt= this.getTotalFromTaxTable(billTaxArrROHAN);
			
			double totalBillingAmount = totBillAmt+taxAmt;
			billingDocEntity.setTotalBillingAmount(totalBillingAmount);
			
			billingDocEntity.setOrderCreationDate(this.getCreationDate());
			
			int payTrmsDays=this.getPaymentTermsList().get(i).getPayTermDays();
			double payTrmsPercent=this.getPaymentTermsList().get(i).getPayTermPercent();
			String payTrmsComment=this.getPaymentTermsList().get(i).getPayTermComment().trim();
			
			
			/***17-12-2019 Deepak Salve added this code for single if payment is 100% and Customer is only one :- Customer branch set on billing and invoice level***/
			HashSet<String> custBranch=new HashSet<String>();  
			  
			  for(int cnt=0;cnt<this.getItems().size();cnt++){
				  List<BranchWiseScheduling> branchlist = this.getItems().get(cnt).getCustomerBranchSchedulingInfo().get(this.getItems().get(cnt).getProductSrNo());
				  if(this.getItems().get(cnt).getCustomerBranchSchedulingInfo()!=null&&branchlist!=null&&branchlist.size()!=0){
				  for(BranchWiseScheduling branchName : branchlist){
					  if(branchName.isCheck() && !branchName.equals("Service Address")){
						  custBranch.add(branchName.getBranchName());
					  }
				  }
				 }
			  }
			  ArrayList<String> custBranchList=null;
			  if(custBranch!=null&&custBranch.size()!=0){
				  custBranchList=new ArrayList<String>(custBranch);
			  }
			String paytrmsBranch = this.getPaymentTermsList().get(i).getBranch();
			if(paytrmsBranch!=null){
				billingDocEntity.setCustomerBranch(paytrmsBranch);
				
			} 
			if(custBranchList!=null&&custBranchList.size()==1){
				billingDocEntity.setCustomerBranch(custBranchList.get(0));
			}
			/***End***/
			paymentTerms.setPayTermDays(payTrmsDays);
			paymentTerms.setPayTermPercent(payTrmsPercent);
			paymentTerms.setPayTermComment(payTrmsComment);
			billingPayTerms.add(paymentTerms);
			billingDocEntity.setArrPayTerms(billingPayTerms);
			billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
			billingDocEntity.setStatus(BillingDocument.CREATED);
			billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
			billingDocEntity.setOrderCformStatus("");
			billingDocEntity.setOrderCformPercent(-1);


			/*************************** vijay number range **************************/
			if(this.getNumberRange()!=null)
				billingDocEntity.setNumberRange(this.getNumberRange());
			
			

			/**
			 * Date : 13 Feb 2017
			 * by Vijay
			 * here i am setting billing period based on payment terms
			 * billing period from date is billing date and To date is next payment terms -1 date if payment terms size not !=1
			 * and Last payment terms To date is set by contract end Date
			 * (logic is same as per above billing date calculated just i am changing next payment terms days -1 day date is calculating
			 * for to set billing period To date)
			 * if payment terms size is 1 then From date is billing date and To date Contract end Date
			 */
			
			System.out.println("From Billing peroid Date ==="+calBillingDate);
			
			count++;
			
			if(getPaymentTermsList().size()==1){
				billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));
				billingDocEntity.setBillingPeroidToDate(this.getEndDate());
			}else{
				
				
//				billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calBillingDate));

				billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", calculatedInvoiceDate));
				
				if(count!=getPaymentTermsList().size()){
					
				Calendar calInvoiceDate2 = Calendar.getInstance();
				if(this.getContractDate()!=null){
					Date serConDate=DateUtility.getDateWithTimeZone("IST", this.getContractDate());
					calInvoiceDate2.setTime(serConDate);
				}
				else{
					conStartDate=DateUtility.getDateWithTimeZone("IST", new Date());
					calInvoiceDate2.setTime(conStartDate);
				}
				
				calInvoiceDate2.add(Calendar.DATE, this.getPaymentTermsList().get(i+1).getPayTermDays());
				Date calculatedInvoiceDate2=null;
				try {
					calInvoiceDate2.set(Calendar.HOUR_OF_DAY,23);
					calInvoiceDate2.set(Calendar.MINUTE,59);
					calInvoiceDate2.set(Calendar.SECOND,59);
					calInvoiceDate2.set(Calendar.MILLISECOND,999);
					calculatedInvoiceDate2=dateFormat.parse(dateFormat.format(calInvoiceDate2.getTime()));
					billingLogger.log(Level.SEVERE,"Calculated Invoice Date =="+calculatedInvoiceDate2);
					if(currentDate.after(calculatedInvoiceDate2))
					{
						calculatedInvoiceDate2=currentDate;
					}
					
					
				} catch (ParseException e) {
					e.printStackTrace();
				}
				billingDocEntity.setBillingPeroidToDate(calculatedInvoiceDate2);

				/***********************************************************************************/
				
				
				Date calBillingDate2=null;
				Calendar calBilling2=Calendar.getInstance();
				if(calculatedInvoiceDate2!=null){
					calBilling2.setTime(DateUtility.getDateWithTimeZone("IST",calculatedInvoiceDate2 ));
				}
				
				billingLogger.log(Level.SEVERE,"Calculated Invoice Date For Billing=="+calculatedInvoiceDate2);
				
//				calBilling2.add(Calendar.DATE, -3);
				calBilling2.add(Calendar.DATE, -1);

				try {
					calBilling2.set(Calendar.HOUR_OF_DAY,23);
					calBilling2.set(Calendar.MINUTE,59);
					calBilling2.set(Calendar.SECOND,59);
					calBilling2.set(Calendar.MILLISECOND,999);
					calBillingDate2=dateFormat.parse(dateFormat.format(calBilling2.getTime()));
					
					if(currentDate.after(calBillingDate2)){
						calBillingDate2=currentDate;
					}
					
					billingLogger.log(Level.SEVERE,"Calculated Billing=="+calBillingDate2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
					billingDocEntity.setBillingPeroidToDate(DateUtility.getDateWithTimeZone("IST", calBillingDate2));
					
					System.out.println("To billing peroid Date"+DateUtility.getDateWithTimeZone("IST", calBillingDate2));

				}else{
					
					billingDocEntity.setBillingPeroidToDate(this.getEndDate());
				}
				
			}	
			
			/**
			 * End here
			 */
			
			/**
			 * Date : 10-08-2017 By ANIL
			 * Recalculating billing period from and to date , if payment date on payment term is not null
			 */
			
			if(this.getPaymentTermsList().get(i).getPaymentDate()!=null){
				if(this.getPaymentTermsList().size()==1){
					billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
					billingDocEntity.setBillingPeroidToDate(this.getEndDate());
				}else if(i==this.getPaymentTermsList().size()-1){
//					billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
//					billingDocEntity.setBillingPeroidToDate(this.getEndDate());
					
					/**
					 * @author Vijay Date :- 05-04-2023
					 * Des :- monthly previous billing period CSAT
					 */
					if(this.getPayTerms()!=null && !this.getPayTerms().equals("") && this.getPayTerms().equals(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD)){
						Date billingperiodDate = DateUtility.getDateWithTimeZone("IST", this.getContractDate());
						Calendar cal=Calendar.getInstance();
						cal.setTime(billingperiodDate);
						cal.add(Calendar.DATE, this.getPaymentTermsList().get(i).getPayTermDays());
						
						Date previousMonthbillingperiod=null;
						
						try {
							previousMonthbillingperiod=dateFormat.parse(dateFormat.format(cal.getTime()));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						System.out.println("previousMonthbillingperiod"+previousMonthbillingperiod);
						billingDocEntity.setBillingPeroidFromDate(previousMonthbillingperiod);
						
						Date date=DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate());
						Calendar calendar = DateUtility.getCalendarForDate(date);
					    calendar.add(Calendar.DATE, -1);
					    calendar.getTime();
						billingDocEntity.setBillingPeroidToDate(calendar.getTime());
						System.out.println("Billing period to Date"+billingDocEntity.getBillingPeroidToDate());
					}
					else{
						billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
						billingDocEntity.setBillingPeroidToDate(this.getEndDate());
					}
					
				}else{
//					billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
//					
//					Date date=DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i+1).getPaymentDate());
//					Calendar calendar = DateUtility.getCalendarForDate(date);
//				    calendar.add(Calendar.DATE, -1);
//				    calendar.getTime();
//					billingDocEntity.setBillingPeroidToDate(calendar.getTime());
					
					/**
					 * @author Vijay Date :- 05-04-2023
					 * Des :- monthly previous billing period CSAT
					 */
					if(this.getPayTerms()!=null && !this.getPayTerms().equals("") && this.getPayTerms().equals(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD)){
						Date billingperiodDate = DateUtility.getDateWithTimeZone("IST", this.getContractDate());
						Calendar cal=Calendar.getInstance();
						cal.setTime(billingperiodDate);
						cal.add(Calendar.DATE, this.getPaymentTermsList().get(i).getPayTermDays());

						Date previousMonthbillingperiod=null;
						
						try {
							previousMonthbillingperiod=dateFormat.parse(dateFormat.format(cal.getTime()));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						billingDocEntity.setBillingPeroidFromDate(previousMonthbillingperiod);
						
						Date date=DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate());
						Calendar calendar = DateUtility.getCalendarForDate(date);
					    calendar.add(Calendar.DATE, -1);
					    calendar.getTime();
						billingDocEntity.setBillingPeroidToDate(calendar.getTime());
						System.out.println("Billing period to Date"+billingDocEntity.getBillingPeroidToDate());
					}
					else{
						billingDocEntity.setBillingPeroidFromDate(DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i).getPaymentDate()));
						Date date=DateUtility.getDateWithTimeZone("IST", this.getPaymentTermsList().get(i+1).getPaymentDate());
						Calendar calendar = DateUtility.getCalendarForDate(date);
					    calendar.add(Calendar.DATE, -1);
					    calendar.getTime();
						billingDocEntity.setBillingPeroidToDate(calendar.getTime());
					}
					
				}
			}
			
			/**
			 * End
			 */
			
			;
			
			/** Date 03-09-2017 added by vijay for total amount before taxes**/
			billingDocEntity.setTotalAmount(totBillAmt);
			/** Date 05-09-2017 added by vijay for Discount amount**/
			double discountamt = 0;
			if(this.getDiscountAmt()!=0){
				discountamt = this.getDiscountAmt()/this.getPaymentTermsList().size();
				billingDocEntity.setDiscountAmt(discountamt);
			}
			
			/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
			double totalfinalAmt=totBillAmt-discountamt;

			billingDocEntity.setFinalTotalAmt(totalfinalAmt);
			 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
			double totalamtincludingtax=totalfinalAmt+taxAmt;
			System.out.println("totalfinalAmt "+totalfinalAmt);
			System.out.println("taxAmt "+taxAmt);
			System.out.println("totalamtincludingtax "+totalamtincludingtax);

			billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT, this.getCompanyId())){
				String strtotalamtincludingtax = totalamtincludingtax+"";
				if(strtotalamtincludingtax.contains(".")){
					String [] includingTaxAmtString = strtotalamtincludingtax.split("\\.");
					String firstDecimal = includingTaxAmtString[0];
					String secondDecimal = includingTaxAmtString[1];
					String twoDecimal = null;
					for(int p=0;p<secondDecimal.length();p++){
						if(p==0){
							twoDecimal = secondDecimal.charAt(p)+"";
						}
						else{
							twoDecimal += secondDecimal.charAt(p)+"";
						}
						if(p==1){
							break;
						}
					}
					System.out.println("twoDecimal "+twoDecimal);
					if(twoDecimal!=null){
						firstDecimal = includingTaxAmtString[0] +"."+twoDecimal;
						billingDocEntity.setTotalAmtIncludingTax(Double.parseDouble(firstDecimal));
						/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
						billingDocEntity.setGrandTotalAmount(Double.parseDouble(firstDecimal));
						
						/*** Date 10-10-2017 added by vijay for total billing amount *********/
						billingDocEntity.setTotalBillingAmount(Double.parseDouble(firstDecimal));
					}
					else{
						billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
					}
				}
			}
			else{
				billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
				 /** Date 05-09-2017 added by vijay for roundoff amount **/
				double roundoff=0;
				if(this.getRoundOffAmt()!=0){
					roundoff=this.getRoundOffAmt()/this.getPaymentTermsList().size();
					billingDocEntity.setRoundOffAmt(roundoff);
				}
				
				/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
				billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
				
				/*** Date 10-10-2017 added by vijay for total billing amount *********/
				billingDocEntity.setTotalBillingAmount(totalamtincludingtax+roundoff);
			}
			
			
			
			/**
			 * Date :24-10-2017 BY ANIL
			 * Copying contract reference no to billing reference 
			 * as required by NBHC
			 */
			if(this.getRefNo()!=null){
				billingDocEntity.setRefNumber(this.getRefNo());
			}
			/**
			 * End
			 */
			/**
			 *  nidhi
			 *  Date : 4-12-2017
			 *  For copy configration details to bill
			 */
			if(confiFlag){
				billingDocEntity.setBillingCategory(this.getCategory());
				billingDocEntity.setBillingType(this.getType());
				billingDocEntity.setBillingGroup(this.getGroup());
			}
			/**
			 *  end
			 */
			/** Date 06/2/2018 added by komal for consolidate price checkbox **/
			billingDocEntity.setConsolidatePrice(this.consolidatePrice);
			/** 
			 * end komal
			 */
			/**
			 * nidhi
			 * Date : 29-01-2018
			 * save poc name from multiple contact list
			 */
			if(this.getPocName()!=null && !this.getPocName().trim().equals("")){
				billingDocEntity.getPersonInfo().setPocName(this.getPocName());
			}
			/** 
			 * end
			 */
			/**
			 *  nidhi
			 *  27-04-2018
			 *  for contract renewal flag
			 */
			billingDocEntity.setRenewContractFlag(this.isRenewContractFlag());
			if(this.getPaymentMode()!=null)
			billingDocEntity.setPaymentMode(this.getPaymentMode());
			
			/**
			 * @author Vijay 28-03-2022 for do not print service address flag updating to billing and invoice
			 */
			billingDocEntity.setDonotprintServiceAddress(this.isDonotprintServiceAddress());
			
			/**
			 * @author Vijay Date :- 25-01-2022
			 * Des :- to manage invoice pdf print on invoice
			 */
			if(this.getInvoicePdfNameToPrint()!=null)
				billingDocEntity.setInvoicePdfNameToPrint(this.getInvoicePdfNameToPrint());
			/**
			 * ends here
			 */
			
			arrbilling.add(billingDocEntity);
			/******************************************************************************/
			
			/**
			 * Date : 05-04-2018 By ANIL
			 * Save should be call after the loop to save data at once
			 */
//			GenricServiceImpl impl=new GenricServiceImpl();
//			if(arrbilling.size()!=0){
//				impl.save(arrbilling);
//			}
			
			/**
			 * End
			 */
			
		}
		
		/**
		 * Date : 05-04-2018 By ANIL
		 * above save is called after loop
		 */
		GenricServiceImpl impl=new GenricServiceImpl();
		if(arrbilling.size()!=0){
			impl.save(arrbilling);
		}
		
		/**
		 * End
		 */
		
		/**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		if(confiFlag){
			boolean updateFlag = false;
			updateFlag = ServerAppUtility.checkContractCategoryAvailableOrNot(this.getCategory(), this.getCompanyId(), this.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkContractTypeAvailableOrNot(this.getType(), this.getCategory(), this.getCompanyId(), this.getApproverName(), 8);
			updateFlag = ServerAppUtility.checkServiceTypeAvailableOrNot(this.getGroup(), this.getCompanyId(), this.getApproverName(), 46);
		}
		/**
		 * end
		 */
		billingLogger.log(Level.SEVERE,"Billing Process Completed");
	}
	
	
	public double removeTaxAmt(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			/**
			 * Date : 31-07-2017 By ANIL
			 * Checking GST printable name 
			 */
			if(entity.getServiceTax().getTaxPrintName()!=null && ! entity.getServiceTax().getTaxPrintName().equals("")
					   && entity.getVatTax().getTaxPrintName()!=null && ! entity.getVatTax().getTaxPrintName().equals(""))
			{
				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
			}
			else
			{
			double removeServiceTax=(entity.getPrice()/(1+service/100));
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
//			double taxPerc=service+vat;
//			retrServ=Math.round(entity.getPrice()/(1+taxPerc/100));
//			retrServ=entity.getPrice()-retrServ;
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}

	
	@GwtIncompatible
	protected void accountingInterface(){
		
		
		if(this.getCount()!=0&&getStatus().equals(Contract.APPROVED)){
			
			for(int  i = 0;i<this.getItems().size();i++){
				
				Customer cust = ofy().load().type(Customer.class).filter("companyId",getCompanyId()).filter("count",this.getCinfo().getCount()).first().now();
				
				
				int prodId=this.getItems().get(i).getPrduct().getCount();
				String productCode = this.getItems().get(i).getProductCode();
				String productName = this.getItems().get(i).getProductName();
				double productQuantity = this.getItems().get(i).getQty();
				
				double taxamount = removeTaxAmt(this.getItems().get(i).getPrduct());
				double productprice = (this.getItems().get(i).getPrice()-taxamount);
				double totalAmount = (this.getItems().get(i).getPrice()-taxamount)*this.getItems().get(i).getQty();

				 double serviceTax=0 ;
				 double vatPercent=0;
				 double calculatedamt = 0;
				 double calculetedservice = 0;
				 int vatglaccno=0;
				 int serglaccno=0;
				 
				 
				 
				 if(this.getItems().get(i).getVatTax().getPercentage()!=0)
				 {
					 vatPercent=this.getItems().get(i).getVatTax().getPercentage();
					 calculatedamt=this.getItems().get(i).getVatTax().getPercentage()*totalAmount/100;
				 }
				 
				 if(this.getItems().get(i).getServiceTax().getPercentage()!=0)
				 {
					 serviceTax=this.getItems().get(i).getServiceTax().getPercentage();
					 calculetedservice=(this.getItems().get(i).getServiceTax().getPercentage()*(totalAmount+calculatedamt))/100;
				 }
				 
				
				 
				 
				 if(this.getItems().get(i).getVatTax().getPercentage()!=0)
				 {
//					 TaxDetails vattaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getItems().get(i).getVatTax().getPercentage()).filter("isVatTax",true).first().now();
//					 GLAccount vatglAcc =null;
//					 if(vattaxDtls!=null){
//						 vatglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",vattaxDtls.getGlAccountName()).first().now();
//					 }
//					 if(vatglAcc!=null){
//						 vatglaccno=vatglAcc.getGlAccountNo();
//					 }
					 
					 vatglaccno=0;
				 }
				 
				 if(this.getItems().get(i).getServiceTax().getPercentage()!=0)
				 {
//					 TaxDetails sertaxDtls = ofy().load().type(TaxDetails.class).filter("companyId",getCompanyId()).filter("taxChargePercent",this.getItems().get(i).getServiceTax().getPercentage()).filter("isServiceTax",true).first().now();
//					 GLAccount serglAcc=null;
//					 if(sertaxDtls!=null){
//						 serglAcc = ofy().load().type(GLAccount.class).filter("companyId",getCompanyId()).filter("glAccountName",sertaxDtls.getGlAccountName().trim()).first().now(); 
//					 }
//					 if(serglAcc!=null){
//						 serglaccno=serglAcc.getGlAccountNo();
//					 }
					 serglaccno=0;
					
				 }
				 double netPayable = getNetpayable();
				 
				 
				 String[] oc = new String[12];
				 double[] oca = new double[12];
				 
				 
				 
				 for(int k = 0; k < this.getProductCharges().size();k++){
					 oc[k] = getProductCharges().get(k).getChargeName();
					 oca[k] = getProductCharges().get(k).getChargePayable();
				 }
				 
				 String numberRange="";
				 if(this.getNumberRange()!=null){
					 numberRange = this.getNumberRange();
				 }
				 
				 UpdateAccountingInterface.updateTally(new Date(),//accountingInterfaceCreationDate
						 							this.getEmployee(),//accountingInterfaceCreatedBy
						 							this.getStatus(),
						 							AppConstants.STATUS ,//Status
						 							"",//Remark
						 							"Services",//Module
						 							"Contract",//documentType
													this.getCount(),//documentID
													" ",//documentTitle
													this.getCreationDate(),//documentDate
													"Contract",//doucmentGL
													this.getRefNo(),//referenceDocumentNumber1
													this.getRefDate(),//referenceDocumentDate1
													AppConstants.REFERENCEDOCUMENTTYPE,//referenceDocumentType1
													"",//referenceDocumentNumber2
													null,//referenceDocumentDate2
													"",//referenceDocumentType2
													AppConstants.ACCOUNTTYPE,//accountType
													this.getCinfo().getCount(),//customerID
													this.getCinfo().getFullName(),//customerName
													this.getCinfo().getCellNumber(),//customerCell
													0,//VendorId
													"",//VendorName
													0,//VendorCell
													0,//employeeID
													"",//employeeName
													this.getBranch(),//Branch
													this.getEmployee(),//personResponsible
													"",//requestedBy
													this.getApproverName(),//approvedBy
													this.getPaymentMethod(),//paymentMethod
													null,//paymentDate
													"",//chequeNumber
													null,//chequeDate
													null,//BankName
													null,//bankAccount
													0,//transferReferenceNumber
													null,//transactionDate
													this.getStartDate(),// contract start date
													this.getEndDate(), // contract end date
													prodId,//productID
													productCode,//productCode
													productName,//productName
													productQuantity,//Quantity
													this.getItems().get(i).getStartDate(),//productDate
													this.getItems().get(i).getDuration(),//duration
													this.getItems().get(i).getNumberOfServices(),//services
													null,//unitofmeasurement
													productprice,//productPrice
													vatPercent,//VATpercent
													calculatedamt,// VATamount
													vatglaccno,//VATglAccount
													serviceTax,//serviceTaxPercent
													calculetedservice,//servicetaxamount
													serglaccno,//servicetaxglaccno
													null,//cform
													0,//cformpercent
													0,//cformAmount
													0,//cformglaccno
													totalAmount,//totalamount
													netPayable,//netpayable
													this.getCategory(),//This is added by RV for NBHC to distinguish between Tax Applicable and not Applicale in Contract
													0,			//amountRecieved
													0.0,         // base Amt in payment for tds calc by rohan 
													0,   //  tds percentage by rohan 
													0,   //  tds amount by rohan 
													oc[0],
													oca[0],
													oc[1],
													oca[1],
													oc[2],
													oca[2],
													oc[3],
													oca[3],
													oc[4],
													oca[4],
													oc[5],
													oca[5],
													oc[6],
													oca[6],
													oc[7],
													oca[7],
													oc[8],
													oca[8],
													oc[9],
													oca[9],
													oc[10],
													oca[10],
													oc[11],
													oca[11],
													cust.getSecondaryAdress().getAddrLine1(),
													cust.getSecondaryAdress().getLocality(),
													cust.getSecondaryAdress().getLandmark(),
													cust.getSecondaryAdress().getCountry(),
													cust.getSecondaryAdress().getState(),
													cust.getSecondaryAdress().getCity(),
													cust.getSecondaryAdress().getPin(),
													this.getCompanyId(),
													null,				//  billing from date (rohan)
													null,			//  billing to date (rohan)
													"", //Warehouse
													"",				//warehouseCode
													"",				//ProductRefId
													"",				//Direction
													"",				//sourceSystem
													"",				//Destination Syste
													null,
													null,numberRange
						 );
	}
	}
	}

	
	
	// *******************vijay added this code for sms **************************************
	
	@GwtIncompatible
	private void CheckSMSConfiguration() {

		 Logger logger = Logger.getLogger("NameOfYourLogger");
		 logger.log(Level.SEVERE," inside sms method");
		 System.out.println("inside SMS method");
//		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId",this.getCompanyId()).filter("status",true).first().now();
		SimpleDateFormat fmtcondate = new SimpleDateFormat("dd/MM/yyyy");
//
//		/**
//		 * Date 9-03-2017
//		 * added by vijay
//		 * for ankita pest control needed reference number of contract in place of contract id
//		 * so while sending sms below if condition for contract id value is reference number and else for contract id value
//		 */
//		
//		ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "ContractApprovedSMS").filter("processList.processType", "SMSWithReferenceNumber").filter("processList.status", true).filter("companyId", this.getCompanyId()).first().now();
//		/**
//		 * ends here
//		 */
//		
//		if(smsconfig!=null){
//			
//			String accountauthkey = smsconfig.getAuthToken();
//			String accSenderId =smsconfig.getAccountSID(); 
//			String accRoute = smsconfig.getPassword();
//			System.out.println("in sms config");
//			 logger.log(Level.SEVERE,"in sms config");
//
////			if(accountauthkey!=null && accSenderId!=null && accRoute!=null  )
			Company comp = ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", "Contract Approved" ).filter("status",true).first().now();
			if(smsEntity!=null){
				
				String templatemsgwithbraces = smsEntity.getMessage();
				logger.log(Level.SEVERE,"in sms template msg");
				
				String fullname = this.getCinfo().getFullName();
				System.out.println(" customerName ="+this.getCinfo().getCellNumber());
				String constartDate = fmtcondate.format(this.getStartDate());
				String conendDate = fmtcondate.format(this.getEndDate());
				/**
				 * Date 29 jun 2017 added by vijay for Eco friendly
				 */
				
				ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","SMS").filter("status",true).filter("companyId", this.getCompanyId()).first().now();
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", this.getCompanyId()).filter("processName","SMS").first().now();				
					
				boolean prossconfigBhashSMSAPIflag = false;
				if(processName!=null){
				if(processConfig!=null){
					if(processConfig.isConfigStatus()){
						for(int l=0;l<processConfig.getProcessList().size();l++){
							if(processConfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("SMS") && processConfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("BHASHSMSAPI") && processConfig.getProcessList().get(l).isStatus()==true){
								prossconfigBhashSMSAPIflag=true;
							}
						}
				   }
				}
				}
				logger.log(Level.SEVERE,"process config  BHASH SMS Flag =="+prossconfigBhashSMSAPIflag);
				String actualMsg="";
				if(prossconfigBhashSMSAPIflag){
					fullname = getCustomerName(fullname,this.getCompanyId());
					
					String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
					String companyName = customerName.replace("{companyName}",comp.getBusinessUnitName());
					String contractStartDate = companyName.replace("{contractstartDate}", constartDate);
					 actualMsg = contractStartDate.replace("{contractendDate}", conendDate);
				}
				/**
				 * ends here
				 * else for normal sms template
				 */
				else{
					
					String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
					String companyName = customerName.replace("{companyName}",comp.getBusinessUnitName());
					String contractId;
					if(processConfig!=null){
						contractId= companyName.replace("{referenceNum}", this.getRefNo()+"");
					}else{
						contractId = companyName.replace("{contractId}", this.getCount()+"");
					}
					String contractStartDate = contractId.replace("{contractstartDate}", constartDate);
					 actualMsg = contractStartDate.replace("{contractendDate}", conendDate);
				}
				
				/**
				 * @author Vijay Date :- 29-12-2022
				 * Des :- updated code to send PDF link and other variables
				 */
				if(actualMsg.contains("{DocumentDate}")){
					actualMsg = actualMsg.replace("{DocumentDate}", fmtcondate.format(this.getContractDate()));
				}
				if(actualMsg.contains("{PDFLink}")){
					CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
					logger.log(Level.SEVERE, "commonserviceimpl.getCompleteURL(comp)" + commonserviceimpl.getCompleteURL(comp));
					
					String pdflink = commonserviceimpl.getPDFURL("Contract", this, this.getCount(), this.getCompanyId(), comp, commonserviceimpl.getCompleteURL(comp),null);
					logger.log(Level.SEVERE, "Contract pdflink " + pdflink);
					String tinyurl = ServerAppUtility.getTinyUrl(pdflink,comp.getCompanyId());
					actualMsg = actualMsg.replace("{PDFLink}", tinyurl);
				}
				if(actualMsg.contains("{NetPayable}")){
					actualMsg = actualMsg.replace("{NetPayable}", this.getNetpayable()+"");
				}

				/**
				 * ends here
				 */
				
				System.out.println(" msg =="+actualMsg);
				logger.log(Level.SEVERE," Message =="+actualMsg);
				long customermobileNo = this.getCinfo().getCellNumber();
				logger.log(Level.SEVERE," Customer Mob No =="+customermobileNo);
				
				SmsServiceImpl smsimpl = new SmsServiceImpl();
//				smsimpl.sendSmsToClient(actualMsg, customermobileNo, accSenderId, accountauthkey, accRoute,this.getCompanyId());
//				logger.log(Level.SEVERE," Impl method called");

				/**
				 * @author Vijay Date :- 29-04-2022
				 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
				 * whats and sms based on communication channel configuration.
				 */
				smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,smsEntity.getEvent(),this.getCompanyId(),actualMsg,customermobileNo,false);
				logger.log(Level.SEVERE,"after sendMessage method");
			
			}
			
			
			
		
		
		//Ashwini Patil Date:24-1-2023
		GetUserRegistrationOtp api=new GetUserRegistrationOtp();
	//	Company comp = ofy().load().type(Company.class).filter("companyId", this.getCompanyId()).first().now();
		
		String compName=ServerAppUtility.getCompanyName(this.getBranch(), comp);
		
		String result=api.validateLicense(comp,AppConstants.LICENSETYPELIST.CUSTOMERPORTAL);
		String errorReport="";
		if(result.equalsIgnoreCase("success")) {
			logger.log(Level.SEVERE,"Customer portal is active");
			
			Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", this.getCompanyId()).filter("count", this.cinfo.getCount()).first().now();
			
			if(!customerEntity.isDisableCustomerPortal()) {
				logger.log(Level.SEVERE,"Customer Portal active for this customer");
				boolean smsCommunicationChannelConfigurationFlag=false;
				boolean whatsAppCommunicationChannelConfigurationFlag=false;
				boolean emailAppCommunicationChannelConfigurationFlag=false;
				SmsServiceImpl smsimpl=new SmsServiceImpl();
				SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE ).filter("status",true).first().now();
				smsCommunicationChannelConfigurationFlag = checkCommunicationChannelConfiguration(AppConstants.SERVICE,AppConstants.CONTRACT,AppConstants.SMS,AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE,companyId,null,null,false);
				whatsAppCommunicationChannelConfigurationFlag = checkCommunicationChannelConfiguration(AppConstants.SERVICE,AppConstants.CONTRACT,AppConstants.WHATSAPP,AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE,companyId,null,null,false);			
				emailAppCommunicationChannelConfigurationFlag = checkCommunicationChannelConfiguration(AppConstants.SERVICE,AppConstants.CONTRACT,AppConstants.EMAIL,AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE,companyId,null,null,false);			
				String Url ="";
				String custportalLink="";
				custportalLink=ServerAppUtility.getCustomerPortalLink(comp);
				logger.log(Level.SEVERE,"custportalLink="+custportalLink);
				if(!custportalLink.equalsIgnoreCase("failed")) {
				
				if(smsTemplate!=null) {
					String acManagerName="";
					if(this.getAccountManager()!=null)
						acManagerName=this.getAccountManager();
					String templatemsgwithbraces = smsTemplate.getMessage();
					String customerName = templatemsgwithbraces.replace("{CustomerName}", this.getCinfo().getFullName());
					String customerCell = customerName.replace("{CustomerMobile}", this.getCinfo().getCellNumber()+"");
					String conId=customerCell.replace("{ContractId}", this.getCount()+"");
					String conDate=conId.replace("{ContractDate}",fmtcondate.format( this.getContractDate()));
					String conStartdate=conDate.replace("{ContractStartDate}", fmtcondate.format(this.getStartDate()));
					String conEnddate="";
					if(this.getEndDate()!=null)
						conEnddate=conStartdate.replace("{ContractEndDate}",fmtcondate.format(this.getEndDate()));
					String acManager=conEnddate.replace("{AccountManager}",acManagerName);
					String portalLink=acManager.replace("{PortalLink}",custportalLink);
					
					String actualMessage = portalLink.replace("{CompanyName}", compName);
					logger.log(Level.SEVERE, AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE+" actaulMsg" + actualMessage);
					if(this.getCinfo().getCellNumber()>0) {
						if(smsCommunicationChannelConfigurationFlag) {
							String smsResponse = smsimpl.sendSmsToClient(actualMessage, this.getCinfo().getCellNumber()+"", companyId, AppConstants.SMS);
							logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);	
						}else {
							logger.log(Level.SEVERE,"sms template 'Customer Portal Link Communication' configuration not found for automatic");
							errorReport+="\n"+"Could not share customer portal link to customer via SMS as communication template 'Customer Portal Link Communication' not found for automatic";
							
						}
						if(whatsAppCommunicationChannelConfigurationFlag) {
							String response = smsimpl.sendMessageOnWhatsApp(companyId, this.getCinfo().getCellNumber()+"", actualMessage);
							logger.log(Level.SEVERE,"whats app response"+response);
						}else {
							logger.log(Level.SEVERE,"whatsapp template 'Customer Portal Link Communication' configuration not found for automatic");
							errorReport+="\n"+"Could not share customer portal link to customer via Whatsapp as communication template 'Customer Portal Link Communication' not found for automatic";
							
						}
					}
					 
				}else {
					logger.log(Level.SEVERE,"sms template 'Customer Portal Link Communication' not found");
					errorReport+="\n"+"Could not share customer portal link to customer via SMS/Whatsapp as communication template 'Customer Portal Link Communication' not found";					
				}
				 
				EmailTemplate emailTemplate = null;
					emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", this.getCompanyId())
								.filter("templateName",AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE)	
								.filter("templateStatus", true).first().now();
					
				if(emailTemplate!=null) {
					if(emailAppCommunicationChannelConfigurationFlag) {
						String mailSubject = emailTemplate.getSubject();
						String emailbody = emailTemplate.getEmailBody();
						String resultString = emailbody.replaceAll("[\n]", "<br>");
						String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
						emailbody=resultString1;
						
						ArrayList<String> toEmailList=new ArrayList<String>();
						if(customerEntity.getEmail()!=null&&!customerEntity.getEmail().equals(""))
							toEmailList.add(customerEntity.getEmail());
						else {
							logger.log(Level.SEVERE,"No email found in customer");
							errorReport+="\n"+"Could not share customer portal link to customer via email as No email id found in customer details";							
//							return;
						}
						String companyEmail=ServerAppUtility.getCompanyEmail(this.getBranch(), comp);
//						if(comp.getEmail()==null||comp.getEmail().equals("")) {
//							logger.log(Level.SEVERE,"No email found in company");
//							return;
//						}
							
						String acManagerName="";
						if(this.getAccountManager()!=null)
							acManagerName=this.getAccountManager();
						String customerName = emailbody.replace("{CustomerName}", this.getCinfo().getFullName());
						String customerCell = customerName.replace("{CustomerMobile}", this.getCinfo().getCellNumber()+"");
						String conId=customerCell.replace("{ContractId}", this.getCount()+"");
						String conDate=conId.replace("{ContractDate}",fmtcondate.format( this.getContractDate()));
						String conStartdate=conDate.replace("{ContractStartDate}", fmtcondate.format(this.getStartDate()));
						String conEnddate="";
						if(this.getEndDate()!=null)
							conEnddate=conStartdate.replace("{ContractEndDate}",fmtcondate.format(this.getEndDate()));						
						String acManager=conEnddate.replace("{AccountManager}",acManagerName);
						String portalLink=acManager.replace("{PortalLink}","<a href=\""+custportalLink+"\"><button> View Portal </button></a>");				
						String companyName = portalLink.replace("{CompanyName}", compName);	
						
						emailbody=companyName.replace("{CompanySignature}", ServerAppUtility.getCompanySignature(this.getBranch(),this.getCompanyId()));
						
						if(companyEmail!=null&&!companyEmail.equals("")&&toEmailList.size()>0) {
							if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Email", AppConstants.ENABLESENDGRIDEMAILAPI, comp.getCompanyId())){
								SendGridEmailServlet sdEmail=new SendGridEmailServlet();
								String msg=sdEmail.sendMailWithSendGrid(companyEmail, toEmailList, null, null, mailSubject, emailbody, "text/html",null,null,"application/pdf",null);								
							}else {
								errorReport+="\n"+"Could not share customer portal link to customer via email as 'Email'-'EnableSendGridEmailApi' process config inactive.";															
							}
						}else {
							errorReport+="\n"+"Could not share customer portal link to customer via email as no email id found in company or branch.";							
						}
					
					}else {
					logger.log(Level.SEVERE,"email template 'Customer Portal Link Communication' configuration not found for automatic");
					errorReport+="\n"+"Could not share customer portal link to customer via email as email template 'Customer Portal Link Communication' not found for automatic";
					}
				}else {
					logger.log(Level.SEVERE,"email template 'Customer Portal Link Communication' not found");
					errorReport+="\n"+"Could not share customer portal link to customer via email as email template 'Customer Portal Link Communication' not found";
				}
				}else {
					errorReport+="\n"+"Could not share customer portal link to customer as Company url missing in company information.";
					logger.log(Level.SEVERE,"Company url missing in company information.");
				}
				 
			}else {
				errorReport+="\n"+"Could not share customer portal link to customer as Customer Portal disabled for this customer";
				logger.log(Level.SEVERE,"Customer Portal disabled for this customer");
			}
			 
		}
//		else {
//			logger.log(Level.SEVERE,"result");
//			if(!result.contains("Customer portal is not enabled."))
//				errorReport+="\n Could not share customer portal link because "+result;
//		}
		logger.log(Level.SEVERE,"errorReport="+errorReport); 
		if(!errorReport.equals("")) {
			if(comp.getEmail()!=null&&!comp.getEmail().equals("")) {
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				ArrayList<String> toEmailList=new ArrayList<String>();
				toEmailList.add(comp.getEmail());
				logger.log(Level.SEVERE,"sending error report to "+comp.getEmail()); 
				String finalReport="Contract Id:"+this.getCount()+"\n"+"Customer id:"+this.getCinfo().getCount()+"\n"+"Customer Name"+this.getCinfo().getFullName()+"\n"+errorReport;
				sdEmail.sendMailWithSendGrid(comp.getEmail(), toEmailList, null, null, "Customer Portal Link Sharing Failed on Contract Approval", finalReport, "text/html",null,null,"application/pdf",null);													
			}
		}
		
		
		
			
		
	}
	
	
	//**************************************ends here  *****************************************
	
	
	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getPoDate()!=null){
			this.setPoDate(dateUtility.setTimeMidOftheDayToDate(this.getPoDate()));
		}
		if(this.getRefDate()!=null){
			this.setRefDate(dateUtility.setTimeMidOftheDayToDate(this.getRefDate()));
		}
		if(this.getReferenceDate()!=null){
			this.setReferenceDate(dateUtility.setTimeMidOftheDayToDate(this.getReferenceDate()));
		}
		if(this.getCancellationDate()!=null){
			this.setCancellationDate(dateUtility.setTimeMidOftheDayToDate(this.getCancellationDate()));
		}
		if(this.getChequeDate()!=null){
			this.setChequeDate(dateUtility.setTimeMidOftheDayToDate(this.getChequeDate()));
		}
		if(this.getAmountTransferDate()!=null){
			this.setAmountTransferDate(dateUtility.setTimeMidOftheDayToDate(this.getAmountTransferDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
		if(this.getFollowUpDate()!=null){
			this.setFollowUpDate(dateUtility.setTimeMidOftheDayToDate(this.getFollowUpDate()));
		}
	}
	/**
	 * ends here	
	 */
	
	public String getContractstatus() {
		return contractstatus;
	}

	public void setContractstatus(String contractstatus) {
		this.contractstatus = contractstatus;
	}

	public boolean isBranchWiseBilling() {
		return branchWiseBilling;
	}

	public void setBranchWiseBilling(boolean branchWiseBilling) {
		this.branchWiseBilling = branchWiseBilling;
	}

	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}

	public String getPayTerms() {
		return payTerms;
	}

	public void setPayTerms(String payTerms) {
		this.payTerms = payTerms;
	}
	
	public ArrayList<StackDetails> getStackDetailsList() {
		return stackDetailsList;
	}

	public void setStackDetailsList(ArrayList<StackDetails> stackDetailsList) {
		this.stackDetailsList = stackDetailsList;
	}

	public String getNewCustomerBranch() {
		return newCustomerBranch;
	}

	public void setNewCustomerBranch(String newCustomerBranch) {
		this.newCustomerBranch = newCustomerBranch;
	}
	
	
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBankAccNo() {
		return bankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public Date getAmountTransferDate() {
		return amountTransferDate;
	}

	public void setAmountTransferDate(Date amountTransferDate) {
		this.amountTransferDate = amountTransferDate;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getChequeIssuedBy() {
		return chequeIssuedBy;
	}

	public void setChequeIssuedBy(String chequeIssuedBy) {
		this.chequeIssuedBy = chequeIssuedBy;
	}
	
	
	/**
	 * Date 29 jun 2017 added by vijay for gtetting customer correspondence Name or full name
	 * @param customerName
	 * @param companyId
	 * @return
	 */
	@GwtIncompatible
	private String getCustomerName(String customerName, long companyId) {
		
		String custName;
		
		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
		
		if(customer!=null){
			
			if(!customer.getCustPrintableName().equals("")){
				custName = customer.getCustPrintableName();
			}else{
				custName = customer.getFullname();
			}
			
		}else{
			custName = customerName;
		}
		
		
		return custName;
	}
	
	/**
	 * ends here
	 */
	
	
	public String getCustomerGSTNumber() {
		return customerGSTNumber;
	}


	public void setCustomerGSTNumber(String customerGSTNumber) {
		this.customerGSTNumber = customerGSTNumber;
	}
	
	
	public Address getCustomerServiceAddress() {
		return customerServiceAddress;
	}

	public void setCustomerServiceAddress(Address customerServiceAddress) {
		this.customerServiceAddress = customerServiceAddress;
	}

	public boolean isConsolidatePrice() {
		return consolidatePrice;
	}

	public void setConsolidatePrice(boolean consolidatePrice) {
		this.consolidatePrice = consolidatePrice;
	}



	public String getPocName() {
		return pocName;
	}



	public void setPocName(String pocName) {
		this.pocName = pocName;
	}



	public String getAMCNumber() {
		return AMCNumber;
	}



	public void setAMCNumber(String aMCNumber) {
		AMCNumber = aMCNumber;
	}



	public boolean isRenewContractFlag() {
		return renewContractFlag;
	}



	public void setRenewContractFlag(boolean renewContractFlag) {
		this.renewContractFlag = renewContractFlag;
	}



	public String getNonRenewalRemark() {
		return nonRenewalRemark;
	}



	public void setNonRenewalRemark(String nonRenewalRemark) {
		this.nonRenewalRemark = nonRenewalRemark;
	}



	public Date getCancellationDate() {
		return cancellationDate;
	}



	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}



	



	public String getPoNumber() {
		return poNumber;
	}



	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}



	public Date getPoDate() {
		return PoDate;
	}



	public void setPoDate(Date poDate) {
		PoDate = poDate;
	}



	
	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}



	public String getTat() {
		return tat;
	}



	public void setTat(String tat) {
		this.tat = tat;
	}



	public String getAccountManager() {
		return accountManager;
	}



	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}



	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return count+"";
	}



	public boolean isStationedTechnician() {
		return stationedTechnician;
	}



	public void setStationedTechnician(boolean stationedTechnician) {
		this.stationedTechnician = stationedTechnician;
	}



	public String getIpAddress() {
		return ipAddress;
	}



	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	public Date getAcceptedDateTime() {
		return acceptedDateTime;
	}

	
	public void setAcceptedDateTime(Date acceptedDateTime) {
		this.acceptedDateTime = acceptedDateTime;
	}






	public boolean isQuickContract() {
		return isQuickContract;
	}


	public void setQuickContract(boolean isQuickContract) {
		this.isQuickContract = isQuickContract;
	}

	@GwtIncompatible
	public boolean checkCommunicationChannelConfiguration(String moduleName, String documentName, String communicationChannel,String templateName, long companyId, String actualMsg, String mobileNo, boolean messagefromPopupFlag) {
		
		EmailTemplateConfiguration emailTemplateconfigurationEntity = ofy().load().type(EmailTemplateConfiguration.class)
				.filter("moduleName", moduleName).filter("documentName", documentName)
				.filter("communicationChannel", communicationChannel).filter("templateName", templateName).first().now();
		if(emailTemplateconfigurationEntity!=null){
			if(emailTemplateconfigurationEntity.isStatus()){
				if(messagefromPopupFlag) {
					return true;
				}
				if(communicationChannel.equalsIgnoreCase("Email")) {
					return true;			
				}else {
					if(emailTemplateconfigurationEntity.isAutomaticMsg() && emailTemplateconfigurationEntity.isSendMsgAuto()) {
						return true;
					}
					else {
						return false;
					}	
				}
				
			}
			else{
				return false;
			}
		}else {
			return false;
		}
	}
	
   public String getInvoicePdfNameToPrint() {
		return invoicePdfNameToPrint;
	}






	public void setInvoicePdfNameToPrint(String invoicePdfNameToPrint) {
		this.invoicePdfNameToPrint = invoicePdfNameToPrint;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public String getUpdationLog() {
		return updationLog;
	}



	public void setUpdationLog(String updationLog) {
		this.updationLog = updationLog;
	}

	
}

