package com.slicktechnologies.shared;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.SearchCompositeAnnatonation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.PestTreatmentDetails;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.BusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;


/*
 * Ashwini Patil 
 * Date: 4-12-2024
 * Pest o touch pest trend GDS is showing duplicate rows as finding details are stored in list format in service entity
 * So we have created new entity for serviceFindings
 */


@Entity
@Cache
@Serialize
public class ServiceFinding extends ConcreteBusinessProcess 
{

	private static final long serialVersionUID = -3760105621082212976L;
	public static final String SERVICESTATUSSCHEDULE = "Scheduled";
	public static final String SERVICESTATUSCANCELLED = "Cancelled";
	public static final String SERVICESTATUSCOMPLETED = "Completed";
    public static final String SERVICESTATUSPENDING = "Pending";
	public static final String SERVICESTATUSRESCHEDULE = "Rescheduled";
	public static final String SERVICESTATUSCLOSED = "Closed";
	public static final String SERVICESTATUSREPORTED = "Reported";
	public static final String SERVICESTATUSTECHCOMPLETED="Completed by Technician";
	public static final String SERVICESUSPENDED ="Suspended";//Add By Jayshree
	
	public static final String SERVICETCOMPLETED="TCompleted";
	/**************************************************Entity Attributes**********************************************/
	
	public static final String SERVICESTATUSSTARTED = "Started";
	public static final String SERVICESTATUSPLANNED = "Planned";

	public static final String SERVICESTATUSOPEN = "Open";
	public static final String SERVICESTATUSREOPEN = "ReOpen";
	/** Date 11-11-2019 By Vijay NBHC CCPM for FumigationService ***/
	public static final String SERVICESTATUSSUCCESS = "Success";
	public static final String SERVICESTATUSFAILURE = "Failure";
	public static final String SERVICESTATUSSCHEDULERESCHEDULE ="Schedule/Reschedule";
	public static final String SERVICESTATUSSTARTEDREPORTEDTCOMPLETED ="Started/Reported/Tcompleted";
	
	
	
	
	/** The service date. */
	@Index protected Date serviceDate;

	
	/** The address where service will be provided */
	protected Address address;
	
	/** The product. */
	protected ServiceProduct product;
	
	@Index
	protected Integer serviceCount;
	
	@Index
	protected Integer contractCount;
	
	@Index
	protected String refNo;

	/** contact information of customer */
	@Index
	protected PersonInfo personInfo;
	
	/** extra information about service */
	protected String comment;
	
	@Index
	protected Date serviceCompletionDate;
	
	protected String serviceDay;
	protected String serviceDateDay;
	protected String serviceTime;
	@Index 
	protected String serviceBranch;
	
	int catchCount;
	String catchPestName; 
	String catchLocation;
	String catchLevel;

	@Index
	protected String team;
	
	protected String premises;		
	
	
	protected ArrayList<EmployeeInfo> technicians;
	
	 @Index
	 boolean completedByApp;
	
	@Index
	protected Date systemCompletionDate;

	 String completedDate_time;
	 String completedTime;
   
	@Index
	Date completedDate;
	
	
	
	
	/**************************************************Default Ctor**********************************************/
	public ServiceFinding() {
		super();
		catchPestName = "";
		catchLocation = "";
		address=new Address();
		personInfo=new PersonInfo();
		product=new ServiceProduct();
		serviceDay="";
		serviceTime="";
		serviceBranch="";
		serviceDateDay="";
		
		premises = "";
		remark = "";
		
		
		technicians=new ArrayList<EmployeeInfo>();
		
		completedByApp = false;
		
		completedDate_time = "";
		completedTime = "";         
	
	}

	public ServiceFinding(String status, String employee, String branch, Date creationDate)
	{
		super(status, employee, branch, creationDate);

	}

	/**************************************************Getter/Setter**********************************************/

	public List<EmployeeInfo> getTechnicians() {
		return technicians;
	}
	public void setTechnicians(List<EmployeeInfo> technicians) {
		ArrayList<EmployeeInfo> arrEmployee=new ArrayList<EmployeeInfo>();
		arrEmployee.addAll(technicians);
		this.technicians = arrEmployee;
	}
	
	public Date getServiceDate() {
		return serviceDate;
	}


	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}


	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public SuperProduct getProduct() {
		return product;
	}

	public void setProduct(ServiceProduct product) {
		this.product = product;
	}

	@Override
	public String getCreationTime() {
		
		return null;
	}

	@Override
	public String getApprovalTime() {
		
		return null;
	}

	@Override
	public void setCreationTime(String time)
	{
		if(time!=null)
			setCreationTime(time.trim());
		
	}

	@Override
	public void setApprovalTime(String time) 
	{
		if(time!=null)
			setApprovalTime(time.trim());	
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}


	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "12", title = "Contract Id", variableName = "tbContractId")
	public Integer getContractCount() {
		return contractCount;
	}

	public void setContractCount(Integer contractCount) {
		this.contractCount = contractCount;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.PERSONINFO, flexFormNumber = "20", title = "", variableName = "PersonInfo")
	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}


	public Date getServiceCompletionDate() {
		return serviceCompletionDate;
	}

	public void setServiceCompletionDate(Date serviceCompletionDate) {
		if(serviceCompletionDate!=null){
			this.serviceCompletionDate = serviceCompletionDate;
		}
	}
	


	public String getServiceDay() {
		return serviceDay;
	}

	public void setServiceDay(String serviceDay) {
		if(serviceDay!=null){
			this.serviceDay = serviceDay.trim();
		}
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		if(serviceTime!=null){
			this.serviceTime = serviceTime.trim();
		}
	}

	public String getServiceBranch() {
		return serviceBranch;
	}

	public void setServiceBranch(String serviceBranch) {
		if(serviceBranch!=null){
			this.serviceBranch = serviceBranch.trim();
		}
	}
	
	
	public String getServiceDateDay() {
		return serviceDateDay;
	}

	public void setServiceDateDay(String serviceDateDay) {
		if(serviceDateDay!=null){
			this.serviceDateDay = serviceDateDay;
		}
	}
	
	public Date getSystemCompletionDate() {
		return systemCompletionDate;
	}

	public void setSystemCompletionDate(Date systemCompletionDate) {
		this.systemCompletionDate = systemCompletionDate;
	}
	

	@OnSave
	@GwtIncompatible
	public void onSave()
	{}
	
	public Long getCellNumber() {
		// TODO Auto-generated method stub
		if(personInfo!=null)
			return personInfo.getCellNumber();
		else
			return null;
	}
	
	public String getName() {
		// TODO Auto-generated method stub
		if(personInfo!=null)
			return personInfo.getFullName();
		else
			return null;
	}

	public String getProductName() {
		if(this.product!=null)
			return this.product.getProductName();
		return null;
	}
	
	public static List<String> getStatusList()
	{
		ArrayList<String> servicestatuslist=new ArrayList<String>();
		servicestatuslist.add(SERVICESTATUSSCHEDULE);
		servicestatuslist.add(SERVICESTATUSCANCELLED);
		servicestatuslist.add(SERVICESTATUSCOMPLETED);
		servicestatuslist.add(SERVICESTATUSPENDING);
		servicestatuslist.add(SERVICESTATUSRESCHEDULE);
		servicestatuslist.add(SERVICESTATUSCLOSED);
		servicestatuslist.add(SERVICESTATUSTECHCOMPLETED);
		servicestatuslist.add(SERVICESTATUSREPORTED);
		servicestatuslist.add(SERVICESUSPENDED);//Add by jayshree
		servicestatuslist.add(SERVICESTATUSPLANNED);
		servicestatuslist.add(SERVICETCOMPLETED);
		servicestatuslist.add(SERVICESTATUSOPEN);
		servicestatuslist.add(SERVICESTATUSREOPEN);
		servicestatuslist.add(SERVICESTATUSSCHEDULERESCHEDULE);
		servicestatuslist.add(SERVICESTATUSSTARTEDREPORTEDTCOMPLETED);
		return servicestatuslist;
	}

	
	public ServiceFinding Myclone() {
		// TODO Auto-generated method stub
		ServiceFinding temp=new ServiceFinding();
		temp = this;
		temp.id=id;
		temp.companyId=companyId;
		temp.setDeleted(isDeleted());
		temp.setStatus(status);
		temp.setEmployee(employee);
		temp.branch=branch;
		temp.creationTime=creationTime;
		temp.approvalTime=approvalTime;
		temp.creationDate=new Date();
		temp.group=group;
		temp.category=category;
		temp.type=type;
		temp.serviceDate=this.serviceDate;
		temp.address=address;
		temp.product=product;
		temp.contractCount=contractCount;
		temp.personInfo=personInfo;
		temp.comment=comment;
		
		
		temp.serviceCompletionDate= serviceCompletionDate ;
		
		temp.serviceDay= serviceDay ;
		temp.serviceDateDay= serviceDateDay ;
		temp.serviceTime= serviceTime ;
		temp.serviceBranch = serviceBranch;
		
		temp.catchCount=catchCount;
		temp.catchLocation=catchLocation;
		temp.catchPestName=catchPestName;
		
		temp.team = team;
		
		temp.refNo = refNo;
		
		temp.remark = remark;
		
		temp.premises = premises;
		
		temp.technicians = technicians;
		
		
		temp.serviceDate = serviceDate;
		
		temp.product = product;
		temp.contractCount = contractCount;
		
		temp.personInfo= personInfo;
		
		temp.comment = comment;
		temp.createdBy = createdBy;
		
		temp.status=status;
		temp.employee=employee;
		temp.branch=branch;
		temp.creationTime=creationTime;
		temp.creationDate=creationDate;
		temp.approverName=approverName;
		temp.group=group;
		temp.category=category;
		temp.type=type;
		temp.remark=remark;
	

		temp.companyId=companyId;
		temp.setDeleted(isDeleted());
		temp.setStatus(status);
		temp.setEmployee(employee);
		temp.branch=branch;
		temp.creationTime=creationTime;
		temp.approvalTime=approvalTime;
		temp.creationDate=new Date();
		temp.group=group;
		temp.category=category;
		temp.type=type;
		temp.serviceDate=this.serviceDate;
		temp.address=address;
		temp.product=product;
		temp.contractCount=contractCount;
		temp.personInfo=personInfo;
		temp.comment=comment;
		
		return temp;
	}

	
	public String getCustomerName() {
		if(personInfo!=null)
			return personInfo.getFullName();
		else
			return "";
		
	}

	public int getCatchCount() {
		return catchCount;
	}

	public void setCatchCount(int catchCount) {
		this.catchCount = catchCount;
	}

	public String getCatchPestName() {
		return catchPestName;
	}

	public void setCatchPestName(String catchPestName) {
		this.catchPestName = catchPestName;
	}

	public String getCatchLocation() {
		return catchLocation;
	}

	public void setCatchLocation(String catchLocation) {
		this.catchLocation = catchLocation;
	}

	public String getLocality() {
		if(address!=null)
		return address.getLocality();
		else
			return "";
	}

	public void setLocality(String locality) {
		address.setLocality(locality);
	}

	public String getLandmark() {
		if(address!=null){
			if(address.getLandmark()!=null || address.getLandmark().equals("")==false)
				return address.getLandmark();
			else
				return "";
		}
		
		else
			return "";
	}

	public void setLandmark(String landmark) {
			address.setLandmark(landmark);
	}

	public String getAddrLine1() {
		if(address!=null)
		   return address.getAddrLine1();
		else
		   return "";	
	}

	public void setAddrLine1(String addrLine1) {
		address.setAddrLine1(addrLine1);
	}

	public String getAddrLine2() {
		if(address!=null){
			if(address.getAddrLine2()!=null || address.getAddrLine2().equals("")==false)
				return address.getAddrLine2();
			else
				return "";
		}
		else
			return "";
	}

	
	public String getState() {
		if(address!=null)
		return address.getState();
		else
		return "";
	}

	public void setState(String state) {
		address.setState(state);
	}

	public String getCity() {
		if(address!=null)
		return address.getCity();
		else
			return "";
	}

	public void setCity(String city) {
		address.setCity(city);
	}

	public long getPin() {
		if(address!=null)
		return address.getPin();
		else
			return 0;
	}

	public void setPin(long pin) {
		address.setPin(pin);
	}

	public String getCountry() {
		if(address!=null)
		return address.getCountry();
		else
			return "";
	}

	public void setCountry(String country) {
		address.setCountry(country);
	}

	public int getCustomerId() {
		if(this.personInfo!=null)
		{
			return personInfo.getCount();
		}
		return -1;
	}

	
	
	
	public String getEntireAddress()
	{
		String address;
		address=getAddrLine1()+" "+getAddrLine2()+" "+getLandmark()
				+" "+getLocality()+" "+getCity()+" "+getState()+" "+getCountry()+" "+getPin();
		return address;
	}

	

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * Date2-7-2018
	 * By Jayshree
	 * add new field
	 * @return
	 */
	
	
	public String getPremises() {
		return premises;
	}

	public void setPremises(String premises) {
		this.premises = premises;
	}

	@Override
	public String toString() {
//		return super.toString();
//		return " "+count+" "+serviceDate+" "+product.getProductName();
		return count+"/"+product.getProductName()+"/"+serviceDate;
	}
	
	public ServiceProductGroupList getServiceProdDt(ProductGroupList proDt){
		ServiceProductGroupList serPRoDt = new ServiceProductGroupList();
		serPRoDt.setProductGroupId(proDt.getProductGroupId());
		serPRoDt.setProduct_id(proDt.getProduct_id());
		serPRoDt.setPrice(proDt.getPrice());
		serPRoDt.setCount(proDt.getCount());
		serPRoDt.setQuantity(proDt.getQuantity());
		serPRoDt.setSerNo(proDt.getSerNo());
		serPRoDt.setUnit(proDt.getUnit());
		serPRoDt.setName(proDt.getName());
		serPRoDt.setProductGroupId(proDt.getProductGroupId());
		serPRoDt.setCode(proDt.getCode());
		serPRoDt.setCompanyId(proDt.getCompanyId());
		/**
		 * nidhi 27-12-2018
		 *  ||*||
		 */
		if(proDt.getPlannedQty()==0 && proDt.getQuantity()!=0){
			serPRoDt.setPlannedQty(proDt.getQuantity());
			serPRoDt.setPlannedUnit(proDt.getUnit());
		}else{
			serPRoDt.setPlannedQty(proDt.getPlannedQty());
			serPRoDt.setPlannedUnit(proDt.getPlannedUnit());
		}/**
		end
		*/
		serPRoDt.setTitle(proDt.getTitle());
		
	 return	serPRoDt;
	}
	
	
	public static List<String> getFumigationProductCodes(){
		ArrayList<String> pCodeList = new ArrayList<String>();
		pCodeList.add("STK-01");
		pCodeList.add("SLM-01");
		pCodeList.add("AFC-10");
		pCodeList.add("CON-02");
		pCodeList.add("AFC-02");
		pCodeList.add("BNF-01");
		pCodeList.add("CON-01");
		/***Date 22-10-2020 by Amol for degassing service and prophyalytic service raised by Vaishnavi pawar**/
		pCodeList.add("DSSG");
		pCodeList.add("PHM-01");
		return pCodeList;
	}

public boolean isCompletedByApp() {
	return completedByApp;
}

public void setCompletedByApp(boolean completedByApp) {
	this.completedByApp = completedByApp;
}


@GwtIncompatible
public void updateServiceProject(ServiceFinding service , ServiceProject project){
	Logger logger = Logger.getLogger("service logger");
	   logger.log(Level.SEVERE , "project");
			TechnicianWareHouseDetailsList technicianDetails = ofy().load().type(TechnicianWareHouseDetailsList.class)
					.filter("companyId", this.getCompanyId()).filter("wareHouseList.technicianName", this.getEmployee()).first().now();
			String warehouseName = "" ,storageLocation = "", storageBin = "",
					parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
			Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", this.getEmployee()).first().now();
			ArrayList<EmployeeInfo> techlist = new ArrayList<EmployeeInfo>();
			if(project.getTechnicians() != null){
				techlist.addAll(project.getTechnicians());
			}
			if(emp!=null){
				EmployeeInfo empInfoEntity = new EmployeeInfo();
		  		empInfoEntity.setEmpCount(emp.getCount());
		  		empInfoEntity.setFullName(emp.getFullName());
		      	empInfoEntity.setCellNumber(emp.getCellNumber1());
		      	empInfoEntity.setDesignation(emp.getDesignation());
		      	empInfoEntity.setDepartment(emp.getDepartMent());
		      	empInfoEntity.setEmployeeType(emp.getEmployeeType());
		      	empInfoEntity.setEmployeerole(emp.getRoleName());
		      	empInfoEntity.setBranch(emp.getBranchName());
		      	empInfoEntity.setCountry(emp.getCountry());
		      	if(techlist != null && techlist.size() > 0){
		      		techlist.set(0 ,empInfoEntity);
		      	}else{
		      		techlist.add(empInfoEntity);
		      	}
		      
		  		}
			if(project.getTechnicians() != null){
				project.setTechnicians(techlist);
			}
			
			
			if(technicianDetails != null){
				
				for(TechnicianWarehouseDetails details : technicianDetails.getWareHouseList()){
					if(details.getTechnicianName().equalsIgnoreCase(this.getEmployee())){
						warehouseName = details.getTechnicianWareHouse();
						storageLocation = details.getTechnicianStorageLocation();				
						storageBin = details.getTechnicianStorageBin();				
						parentWareHouse = details.getParentWareHouse();
						parentStorageLocation = details.getParentStorageLocation();						
						parentStorageBin = details.getPearentStorageBin();
						if(project != null){
							if(project.getProdDetailsList() != null){
								for(ProductGroupList group : project.getProdDetailsList()){
									group.setParentWarentwarehouse(parentWareHouse);
									group.setParentStorageLocation(parentStorageLocation);
									group.setParentStorageBin(parentStorageBin);
									group.setWarehouse(warehouseName);
									group.setStorageLocation(storageLocation);
									group.setStorageBin(storageBin);
							   }
							}
						ofy().save().entity(project);
						}
						break;
					}
				}
			
			
		}	
	}


public String getCompletedDate_time() {
	return completedDate_time;
}

public void setCompletedDate_time(String completedDate_time) {
	this.completedDate_time = completedDate_time;
}

public String getCompletedTime() {
	return completedTime;
}

public void setCompletedTime(String completedTime) {
	this.completedTime = completedTime;
}


@OnSave
@GwtIncompatible
public void setTimeMidOfTheDayToDate()
{
	Logger logger = Logger.getLogger("service logger");
	   
	DateUtility dateUtility = new DateUtility();
	if(this.getServiceDate()!=null){
		this.setServiceDate(dateUtility.setTimeMidOftheDayToDate(this.getServiceDate()));
	}


}


	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Integer getServiceCount() {
		return serviceCount;
	}

	public void setServiceCount(Integer serviceCount) {
		this.serviceCount = serviceCount;
	}

	public void setTechnicians(ArrayList<EmployeeInfo> technicians) {
		this.technicians = technicians;
	}

	public String getCatchLevel() {
		return catchLevel;
	}

	public void setCatchLevel(String catchLevel) {
		this.catchLevel = catchLevel;
	}


}
