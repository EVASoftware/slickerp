package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

// TODO: Auto-generated Javadoc
/**
 * The Class OtherTaxCharges.
 */

@Entity
public class OtherTaxCharges extends SuperModel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6314433162625019147L;
	
/** ***************************************Applicability Attributes******************************************. */	
	@Index
	protected String otherChargeName;
	
	protected double otherChargePercent;
	
	protected double otherChargeAbsValue;
	
	protected String glAccountName;
	
	protected String taxDescription;
	@Index
	protected boolean otherChargeStatus;
	
	/**
	 * Date : 16-09-2017 BY ANIL
	 */
	protected String sacCode;
	
	/**
	 * ***********************************Constructor******************************************************.
	 */
	
	public OtherTaxCharges() {
		super();
		otherChargeName="";
		glAccountName="";
		taxDescription="";
		sacCode="";
	}
	
	/**
	 * ****************************************************************************************************.
	 *
	 * @return the tax name
	 */

	
/************************************************Getters And Setters******************************************/	
	
	public String getSacCode() {
		return sacCode;
	}
	
	public void setSacCode(String sacCode) {
		this.sacCode = sacCode;
	}
	
	public String getOtherChargeName() {
		return otherChargeName;
	}

	public void setOtherChargeName(String otherChargeName) {
		if(otherChargeName!=null)
			this.otherChargeName = otherChargeName;
	}

	public Double getOtherChargePercent() {
		return otherChargePercent;
	}

	public void setOtherChargePercent(double otherChargePercent) {
		this.otherChargePercent = otherChargePercent;
	}

	public Double getOtherChargeAbsValue() {
		return otherChargeAbsValue;
	}

	public void setOtherChargeAbsValue(double otherChargeAbsValue) {
		this.otherChargeAbsValue = otherChargeAbsValue;
	}

	public String getGlAccountName() {
		return glAccountName;
	}

	public void setGlAccountName(String glAccountName) {
		if(glAccountName!=null)
			this.glAccountName = glAccountName.trim();
	}

	public String getTaxDescription() {
		return taxDescription;
	}

	public void setTaxDescription(String taxDescription) {
		if(taxDescription!=null)
			this.taxDescription = taxDescription.trim();
	}

	public Boolean getOtherChargeStatus() {
		return otherChargeStatus;
	}

	public void setOtherChargeStatus(boolean otherChargeStatus) {
		this.otherChargeStatus = otherChargeStatus;
	}

	/**
	 * ***************************************************************************************************.
	 *
	 * @param arg0 the arg0
	 * @return the int
	 */

	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof OtherTaxCharges)
		{
			OtherTaxCharges otherChargeEntity = (OtherTaxCharges) arg0;
			if(otherChargeEntity.getOtherChargeName()!=null)
			{
				return this.otherChargeName.compareTo(otherChargeEntity.otherChargeName);
			}
		}
		return 0;
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.SuperModel#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		OtherTaxCharges ocentity = (OtherTaxCharges) m;
		String name = ocentity.getOtherChargeName().trim();
		String curname=this.otherChargeName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(ocentity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return otherChargeName.toString();
	}
	
	


}
