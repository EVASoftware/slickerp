package com.slicktechnologies.shared;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.SearchCompositeAnnatonation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.PestTreatmentDetails;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.android.technicianapp.TechnicianAppOperation;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.BusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;
import com.slicktechnologies.shared.common.unitconversion.UnitConversion;
import com.slicktechnologies.shared.common.unitconversion.UnitConversionDetailList;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
/**
 * Represents one Service from all assigned services of contract(only for an Approved {@link Contract}).
 */
@Entity
@Cache
@Serialize
public class Service extends ConcreteBusinessProcess 
{

	public static final String SERVICESTATUSSCHEDULE = "Scheduled";
	public static final String SERVICESTATUSCANCELLED = "Cancelled";
	public static final String SERVICESTATUSCOMPLETED = "Completed";
    public static final String SERVICESTATUSPENDING = "Pending";
	public static final String SERVICESTATUSRESCHEDULE = "Rescheduled";
	public static final String SERVICESTATUSCLOSED = "Closed";
	public static final String SERVICESTATUSREPORTED = "Reported";
	public static final String SERVICESTATUSTECHCOMPLETED="Completed by Technician";
	public static final String SERVICESUSPENDED ="Suspended";//Add By Jayshree
	/**
	 * @author Komal
	 * @since 19.06.2019 
	 * new status TCompleted 
	 */
	public static final String SERVICETCOMPLETED="TCompleted";
	/**************************************************Entity Attributes**********************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 813609515557153702L;
	public static final String SERVICESTATUSSTARTED = "Started";
	public static final String SERVICESTATUSPLANNED = "Planned";

	public static final String SERVICESTATUSOPEN = "Open";
	public static final String SERVICESTATUSREOPEN = "ReOpen";
	/** Date 11-11-2019 By Vijay NBHC CCPM for FumigationService ***/
	public static final String SERVICESTATUSSUCCESS = "Success";
	public static final String SERVICESTATUSFAILURE = "Failure";
	
	/**
	 * @author Abhinav Bihade 
	 * @since 23/01/2020,
	 * NBHC- Add two additional status 'Schedule / Reschedule' and 'Started / Reported / Tcompleted 'on Service status dropdown list,
	 * and data be searched accordingly from service search screen and service list search screen
	 */
	public static final String SERVICESTATUSSCHEDULERESCHEDULE ="Schedule/Reschedule";
	public static final String SERVICESTATUSSTARTEDREPORTEDTCOMPLETED ="Started/Reported/Tcompleted";
	
	/** The service date. */
	@Index protected Date serviceDate;

	/** The reason for change. */
	protected String reasonForChange;

	/** The address where service will be provided */
	protected Address address;
	
	//Patch Patch
	/** The product. */
	protected ServiceProduct product;
	
	/** The contract number/count of corresponding contract*/
	@Index
	protected Integer contractCount;
	
	protected Date contractStartDate;
	
	protected Date contractEndDate;
	@Index
	protected int serviceSerialNo;
	@Index
	protected int serviceIndexNo;
	
	/** contact information of customer */
	@Index
	protected PersonInfo personInfo;
	
	/** extra information about service */
	protected String comment;
	
	protected ArrayList<Device> deviceList;
	@Index
	protected String serviceType;
	@Index
	protected boolean isAdHocService;
	protected DocumentUpload uptestReport;
	protected DocumentUpload upDeviceDocument;
	protected ArrayList<ModificationHistory>listHistory;
	@Index
	protected Date serviceCompletionDate;
	
	protected String serviceDay;
	protected String serviceDateDay;
	protected String serviceTime;
	@Index 
	protected String serviceBranch;
	protected int serviceCompleteDuration;
	protected String serviceCompleteRemark;
	
	protected ArrayList<CatchTraps> catchtrapList;
	
	@Index
	protected Integer ticketNumber;
	
	@Index
	protected String customerFeedback;
	
	
	@Index
	protected Double servicingTime;
	
	@Index
	protected String team;
	protected Boolean scheduled;
	protected String reason;
	protected Boolean specificServFlag;
	
	protected Boolean recordSelect;
	@Index
	protected String numberRange;
	
	//Added by RV for tracking details pop up.	
	/**
	 * Description : When service status is changed as "Started" , "Reported" ,"TCompleted",
	 * 				 "Completed" then date , time , status , lat , long, loacation is stored.
	 */
		ArrayList<TrackTableDetails> trackServiceTabledetails;
	
	//    rohan added this ref no for bpt 
	@Index
	protected String refNo;
	
	protected String remark;
	/**
	 * Date 2-7-2018
	 * by jayshree
	 * add new field
	 */
	
	protected String technicianremark;
	protected String premises;
	
	
	/*
	 * Rohan Added this 2 String fields for Saving from date and todate 
	 */
	
	protected String fromTime;
	protected String toTime;
	
//	Warehouse code / Shed Name / Multiple stack No
	@Index
	String wareHouse;
	@Index
	String storageLocation;
	@Index
	String storageBin;
	
	
	/**
	 * Description: Added to store all values of wareHouse,StorageLocation and Stack.
	 * Date: 09-09-2016
	 * Project :
	 * Required For : This is created for NBHC 
	 *	
	 */
	@Index
	protected ArrayList<StackDetails> stackDetailsList;
	/**
	 * End
	 */
	
	
	/**
	 * Date :24-01-2017 by Anil
	 * Adding ref No. 2 for PCAMB and Pest Cure Incorporation	
	 */
	@Index	
	protected String refNo2;
	/**
	 * End
	 */
	
	/**
	 * Date 03-04-2017
	 * added by vijay
	 * for Service week No
	 */
	@Index
	protected int serviceWeekNo;
	/**
	 * Ends here
	 */
	
	/**
	 * Rahul Added the below fields on 16 June 2017
	 * 
	 */
	protected double quantity;
	protected String uom;
	/**
	 * Rahul changes ends here 
	 */
	
	/******** Added by Sagar sore for Service value **********/
	protected double serviceValue=0.0;
	
	/**
	 * Date : 06-10-2017 BY ANIL
	 * this flag is used to generate bill after completion of service.
	 */
	@Index
	protected boolean serviceWiseBilling;
	int serviceSrNo;
	/**
	 * End
	 */
	
	/** Date 27-09-2017 added by vijay for technician scheduling Unique Number **/
	protected long schedulingNumber;
	
	/** Date 23 Oct 2017 added by vijay for technician scheduling project Id
	 */
	@Index
	protected int projectId;
	/** Date 24 oct 2017 added by vijay for technician Scheduling invoice Id and invoice Date ***/
	@Index
	protected int invoiceId;
	@Index
	protected Date invoiceDate;
	@Index
	protected boolean isRateContractService;
	
	@Index
	protected boolean assessmentServiceFlag;
	
	/** technician scheduling plan popup required for print in pdf for gifts**/
	protected String techPlanOtherInfo;
	/** ends here *******************/
	/**
	 * Rahul Added this
	 */
	@Index
	protected DocumentUpload customerSignature;
	protected String customerSignName;
	protected String customerSignNumber;
	protected String description;
	
	
	/***30-12-2019 Deepak Salve added this code for Customer Designation and Emp Id***/
	private String customerPersonDesignation;
	private String customerPersonEmpId;
	/***End***/
	
	/**
	 * Ends
	 */
	
	/**
	 * Date : 04-11-2017 BY ANIL
	 * added technician list to store the technician in the service through customer service list
	 * for NBHC
	 */
	protected ArrayList<EmployeeInfo> technicians;
	/**
	 * End
	 */
	/**
	 * Dev : Rahul Verma
	 * Date : 01 Dec 2017
	 * Description : These field will store Lat and Long at every status
	 * 				 Use seperator '$' for separating
	 */
	String startLatnLong="";
	String reportLatnLong="";
	String completeLatnLong="";
	/**
	 * Ends for Rahul Verma
	 */
	/** date 16.02.2018 added by komal for area wise billing **/
	double perUnitPrice;
	String costCenter;
	/** end komal
	/*******************************Relational Attribute***************************************************************/
	protected Key<PersonInfo> personInfoKey;

	String serviceWiseLatLong="";
	/*
	 * nidhi
	 * 27-04-2018
	 * for contract created from old contract
	 */
	@Index
	protected boolean renewContractFlag;
	
	/** date 9/5/2018 added by komal for call wise service flag**/
	@Index
	protected boolean isCallwiseService;
	 /**
	   * nidhi
	   * 8-08-2018
	   * for map model no , serial no
	   */
	  protected String proModelNo;
	  protected String proSerialNo;
	  /**
	   * end
	   */  /**
		 * please   update copyofobject method when ever new variable declared in this class
		 * must do this
		 * nidhi
		 *  19-09-2018
		 */
	protected ArrayList<ServiceProductGroupList> serviceProductList ;
	/**************************************************Default Ctor**********************************************/
	
	/**
	 *  ||*
	 * nidhi
	 *  11-10-2018
	 */
	@Index
	protected int billingCount = 0;
	protected String billingStatus = "";
	/***
	 * nidhi
	 * Please update 
	 *  Myclone  method when ever u declare any new object in this or super class
	 */
	 /**
	   * Rahul Verma added on 01 Oct 2018
	   */
	  protected String latnLong;
	  /**
	   * Ends
	   */
	  /**
	   * end
	   */
	 
	 /**
	  * @author Anil ,Date :15-03-2019
	  * Storing service images from pedio app
	  * raised by Nitin sir and amit  
	  */
	  protected DocumentUpload serviceImage1;
	  protected DocumentUpload serviceImage2;
	  protected DocumentUpload serviceImage3;
	  protected DocumentUpload serviceImage4;
	  
	    /**
		 * Date 03-4-2019 by Vijay for NBHC CCPM cancellation Date
		 */
		@Index
		protected Date cancellationDate;
	  
	  /** date 23.3.2019 added by komal to store contract group , type and amount in service **/
	 @Index
	 protected String contractType;
	 @Index
	 protected String contractGroup;
	 protected double contractAmount;
	 @Index 
	 protected Date oldServiceDate;
	  /** date 23.3.2019 added by komal to store service duration(Completed - started time) from app **/
	 protected int totalServiceDuration; 
	 @Index
	 protected boolean isServiceScheduled;
	 /** date 24.05.2019 added by komal to store hh , mm ,am/pm**/
	 protected String hours ,minutes , ampm; 
	 /** Date 25.5.2019 added by komal for unique service id **/
	 String serviceNumber ;
	 /** date 31.05.2019 added by komal to store mmn id in list **/
	 MaterialMovementNote mmn;
	 @Index
	 String srCopyNumber;
	 @Index
	 boolean completedByApp;
	 @Index
	 boolean docketUploadedByApp;
	/***
	 * @author Vijay Date 31-07-2019 Des :- NBHC CCPM Stack Details Maintaining
	 *         for Fumigation services
	 */
	@Index
	String shade;
	@Index
	String compartment;
	@Index
	String stackNo;
	double stackQty;
	@Index
	Date depositeDate;
	 double totalPlannedMaterialCost;
	 double totalPlannedOperatorCost;
	 double totalActualMaterialCost;
	 double totalActualOeratorCost;
	 boolean isMaterialAdded;
	@Index
	protected Date systemCompletionDate;


	 @Index
	 boolean isMINUploaded;
 	 String branchEmail;
	/**
	 * Date 08-11-2019 
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM WMS Service Flag and Fumigation Service id store in Deggassing Service 
	 * and Fumigation success or failure status
	 */
 	 @Index
	 boolean wmsServiceFlag;
	 @Index
	 int prophylacticServiceId;
	 @Index
	 int deggassingServiceId;
	 @Index
	 String fumigationStatus;
	 @Index
	 Date fumigationStatusDate;
	 /** date 20.11.2019 added by komal to store started , reported , Tcompleted and completed values individually
	  *  as instructed by Nitin sir
	  */
	 String startedDate_time;
	 String startedLatitude;
	 String startedLongitude;
	 String startedLocationName;
	 String startedTime;
	 String reportedDate_time;
	 String reportedLatitude;
	 String reportedLongitude;
	 String reportedLocationName;
	 String reportedTime;
	 String tCompletedDate_time;
	 String tCompletedLatitude;
	 String tCompletedLongitude;
	 String tCompletedLocationName;
	 String tCompletedTime;
	 String completedDate_time;
	 String completedLatitude;
	 String completedLongitude;
	 String completedLocationName;
	 String completedTime;
    /**
	 * Date 10-12-2019 
	 * @author Vijay Chougule
	 *  Des :- NBHC CCPM Fumigation Deggasing Service status
	 *  when degassing service completed then its status upating in fumigation service
	 *  SO in NBHC CIO app will allow to mark success or failure for fumigation service  
	 */
	 @Index
	 String degassingServiceStatus;
	 @Index
	 int fumigationServiceId;
	 String commodityName;
	 /**
	  * Date 20-12-2019 by Vijay
	  * Des :- NBHC WMS Service CIO app they will make suspend the service
	  */
	 String actionRemark;
	 String actionBy;
	 /** Date 07-01-2020 by vijay
	  * Des :- once CIO reschedule then service can be postpond T+5 in EVA ERP
	  * Validation.
	  */
	 Date cioRescheduleDate;
	 String clusterName;
	 
//	 /** @UAT
//	  * @author Vijay Chougule
//	  * Date :- 13-01-2020
//	  * Des :- For Pedio app sepearting checklist and remark
//	  */
//	 protected ArrayList<CheckList> checklist;
//	 protected String checklistRemark;
 /**
		 * @author Abhinav Bihade
		 * @since 25/02/2020
		 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
		 * 
		 */
		@Index
		Date lastUpdatedDate;
		
		/**
		 * @author Anil , Date : 09-03-2020
		 * If Sr copy is sent to through cron job then we set 
		 * Sr copy status to true
		 */
		@Index
		boolean srCopyStatus;
		
	/**
	 * @author Anil
	 * @since 03-06-2020
	 * Adding asset related field for premium tech 
	 */
	@Index
	int assetId;
	String componentName;
	String mfgNo;
	@Index
	Date mfgDate;
	@Index
	Date replacementDate;
	@Index
	String tierName;
	/**
	 * @author Anil
	 * @since 16-06-2020
	 * asset unit
	 */ 
	String assetUnit;
	/**
	 * @author Anil
	 * @since 19-06-2020
	 * Added summary report 2 for PTSPL
	 */
	DocumentUpload serviceSummaryReport2;
	
	/**
	 * @author Anil
	 * @since 24-06-2020
	 */
	protected ArrayList<CatchTraps> checkList;
	
	/**
	 * @author Anil
	 * @since 03-07-2020
	 * Addind pest treatement details
	 * for umas raised by Nitin sir
	 */
	ArrayList<PestTreatmentDetails> pestTreatmentList;
	
	/**
	 * @author Vijay Chougule Date :- 09-09-2020
	 * Des :- Added Service Duration for Life Line
	 */
	double serviceDuration;
	
	/**
	 * @author Vijay  Date :- 24-10-2020
	 * Des :- As per nitin Sir instruction started reported tcompletion and completion
	 * should be store in in Date. same fields stored in string but nitin sir needed this in Date 
	 */
	@Index
	Date startedDate;
	@Index
	Date reportedDate;
	@Index
	Date tcompletedDate;
	@Index
	Date completedDate;
	
	/**
	 * @author Anil @since 30-04-2021
	 * Added flag to determined whether service is schduled or not
	 */
	@Index
	boolean scheduledByClient;
	
	
	/**
	 * @author Anil @since 19-10-2021
	 * Adding fields to capture before and after service image
	 * earlier we were storing only 4, now as per the new requirement we neen to store 5 more
	 * Raised by Rahul Tiwari and Nitin Sir
	 */
	
	 protected DocumentUpload serviceImage5;
	 protected DocumentUpload serviceImage6;
	 protected DocumentUpload serviceImage7;
	 protected DocumentUpload serviceImage8;
	 protected DocumentUpload serviceImage9;
	 protected DocumentUpload serviceImage10;
	
	@Index
	String serviceLocation;
	@Index
	String serviceLocationArea;
	@Index
	boolean onsiteTechnician;
	 
	double kmTravelled;
	
	double materialCost;
	double expenseCost;
	double laborCost;

	String serviceCompletionOTP;
	
	String productFrequency;
	String certificateValidTillDate;
	
	

	/**************************************************Default Ctor**********************************************/
	public Service() {
		super();
		address=new Address();
		personInfo=new PersonInfo();
		product=new ServiceProduct();
		reasonForChange="";
		deviceList=new ArrayList<Device>();
		listHistory=new ArrayList<ModificationHistory>();
		serviceType="";
		serviceDay="";
		serviceTime="";
		serviceBranch="";
		serviceCompleteRemark="";
		serviceDateDay="";
		ticketNumber=-1;
		catchtrapList = new ArrayList<CatchTraps>();
		
		recordSelect=false;
		premises = "";
		remark = "";
		/**
		 * Add by jayshree at2-7-2018
		 */
		technicianremark="";
		fromTime ="";
		toTime = "";
		
		trackServiceTabledetails = new ArrayList<TrackTableDetails>();
		
		refNo2="";
		uom="";
		serviceWiseBilling=false;
		isRateContractService=false;
		assessmentServiceFlag=false;
		techPlanOtherInfo="";
		customerSignName="";
		customerSignNumber="";
		description="";
		
		technicians=new ArrayList<EmployeeInfo>();
		serviceWiseLatLong="";
		renewContractFlag =false;
		isCallwiseService = false;
		/**
		 * nidhi
		 * 8-08-2018
		 * 
		 */
		proSerialNo = "";
		proModelNo = "";
		latnLong="";
		
		serviceImage1=new DocumentUpload();
		serviceImage2=new DocumentUpload();
		serviceImage3=new DocumentUpload();
		serviceImage4=new DocumentUpload();
		 /** date 23.3.2019 added by komal to store contract group , type and amount in service **/
		contractType  = "";
		contractGroup = "";
		isServiceScheduled = false;
		hours = "--";
		minutes = "--";
		ampm = "--";
		serviceNumber = "";
		mmn = new MaterialMovementNote();
		srCopyNumber = "";
		completedByApp = false;
		docketUploadedByApp=false;
		compartment="";
		shade="";
		stackNo="";
		isMaterialAdded = false;
		isMINUploaded =false;
		wmsServiceFlag = false;
		fumigationStatus ="NA";
		branchEmail = "";
		startedDate_time = "";      
		startedLatitude = "";       
		startedLongitude = "";      
		startedLocationName = "";   
		startedTime = "";           
		reportedDate_time = "";     
		reportedLatitude = "";      
		reportedLongitude = "";     
		reportedLocationName = "";  
		reportedTime = "";          
		tCompletedDate_time = "";   
		tCompletedLatitude = "";    
		tCompletedLongitude = "";   
		tCompletedLocationName = "";
		tCompletedTime = "";        
		completedDate_time = "";    
		completedLatitude = "";     
		completedLongitude = "";    
		completedLocationName = ""; 
		completedTime = "";         
		degassingServiceStatus ="";
		commodityName="";
		
		actionRemark ="";
		actionBy = "";
		
//		checklist = new  ArrayList<CheckList>();
//		checklistRemark = "";
		clusterName="";
		
		srCopyStatus=false;
		
		checkList=new ArrayList<CatchTraps>();
		serviceLocation = "";
		serviceLocationArea="";
		serviceCompletionOTP="";
		productFrequency = "";
		certificateValidTillDate="";
	}

	public Service(String status, String employee, String branch, Date creationDate)
	{
		super(status, employee, branch, creationDate);

	}

	/**************************************************Getter/Setter**********************************************/
	
	/**
	 * @author Abhinav Bihade
	 * @since 25/02/2020
	 * As per Nitin Sir's, report will generate as on lastUpdatedDate 
	 * 
	 */
	@OnSave
	@GwtIncompatible
	public void setLastUpdatedDate(){
		setLastUpdatedDate(DateUtility.setTimeToMidOfDay( new Date()));
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	
	public DocumentUpload getServiceImage1() {
		return serviceImage1;
	}

	public void setServiceImage1(DocumentUpload serviceImage1) {
		this.serviceImage1 = serviceImage1;
	}

	public DocumentUpload getServiceImage2() {
		return serviceImage2;
	}

	public void setServiceImage2(DocumentUpload serviceImage2) {
		this.serviceImage2 = serviceImage2;
	}

	public DocumentUpload getServiceImage3() {
		return serviceImage3;
	}

	public void setServiceImage3(DocumentUpload serviceImage3) {
		this.serviceImage3 = serviceImage3;
	}
	
	public DocumentUpload getServiceImage4() {
		return serviceImage4;
	}

	public void setServiceImage4(DocumentUpload serviceImage4) {
		this.serviceImage4 = serviceImage4;
	}

	public String getLatnLong() {
		return latnLong;
	}

	

	public void setLatnLong(String latnLong) {
		this.latnLong = latnLong;
	}

	public List<EmployeeInfo> getTechnicians() {
		return technicians;
	}
	public void setTechnicians(List<EmployeeInfo> technicians) {
		ArrayList<EmployeeInfo> arrEmployee=new ArrayList<EmployeeInfo>();
		arrEmployee.addAll(technicians);
		this.technicians = arrEmployee;
	}
	
	
	public int getServiceSrNo() {
		return serviceSrNo;
	}

	public void setServiceSrNo(int serviceSrNo) {
		this.serviceSrNo = serviceSrNo;
	}


	public boolean isServiceWiseBilling() {
		return serviceWiseBilling;
	}

	public void setServiceWiseBilling(boolean serviceWiseBilling) {
		this.serviceWiseBilling = serviceWiseBilling;
	}
	
	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}
	
	public String getRefNo2() {
		return refNo2;
	}

	public void setRefNo2(String refNo2) {
		this.refNo2 = refNo2;
	}
	
	public Integer getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(Integer ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	public Date getServiceDate() {
		return serviceDate;
	}


	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getReasonForChange() {
		return reasonForChange;
	}

	public void setReasonForChange(String reasonForChange)
	{
		if(reasonForChange!=null)
			this.reasonForChange = reasonForChange.trim();
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public SuperProduct getProduct() {
		return product;
	}

	public void setProduct(ServiceProduct product) {
		this.product = product;
	}

	@Override
	public String getCreationTime() {
		
		return null;
	}

	@Override
	public String getApprovalTime() {
		
		return null;
	}

	@Override
	public void setCreationTime(String time)
	{
		if(time!=null)
			setCreationTime(time.trim());
		
	}

	
	
	public String getStartLatnLong() {
		return startLatnLong;
	}

	public void setStartLatnLong(String startLatnLong) {
		this.startLatnLong = startLatnLong;
	}

	public String getReportLatnLong() {
		return reportLatnLong;
	}

	public void setReportLatnLong(String reportLatnLong) {
		this.reportLatnLong = reportLatnLong;
	}

	public String getCompleteLatnLong() {
		return completeLatnLong;
	}

	public void setCompleteLatnLong(String completeLatnLong) {
		this.completeLatnLong = completeLatnLong;
	}

	@Override
	public void setApprovalTime(String time) 
	{
		if(time!=null)
			setApprovalTime(time.trim());	
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Key<PersonInfo> getPersonInfoKey() {
		return personInfoKey;
	}

	public void setPersonInfoKey(Key<PersonInfo> personInfoKey) {
		this.personInfoKey = personInfoKey;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "12", title = "Contract Id", variableName = "tbContractId")
	public Integer getContractCount() {
		return contractCount;
	}

	public void setContractCount(Integer contractCount) {
		this.contractCount = contractCount;
	}

	@SearchAnnotation(Datatype = ColumnDatatype.PERSONINFO, flexFormNumber = "20", title = "", variableName = "PersonInfo")
	public PersonInfo getPersonInfo() {
		return personInfo;
	}

	public void setPersonInfo(PersonInfo personInfo) {
		this.personInfo = personInfo;
	}

	
	
	public List<Device> getArray() {
		return deviceList;
	}

	public void setArray(List<Device> array) {
		  if(array!=null)
		  {
			  deviceList=new ArrayList<Device>();
			  deviceList.addAll(array);
		  }
		
	}

	public DocumentUpload getUptestReport() {
		return uptestReport;
	}

	public void setUptestReport(DocumentUpload uptestReport) {
		this.uptestReport = uptestReport;
	}

	public DocumentUpload getUpDeviceDocument() {
		return upDeviceDocument;
	}

	public void setUpDeviceDocument(DocumentUpload upDeviceDocument) {
		this.upDeviceDocument = upDeviceDocument;
	}

	public int getServiceSerialNo() {
		return serviceSerialNo;
	}

	public void setServiceSerialNo(int serviceSerialNo) {
		this.serviceSerialNo = serviceSerialNo;
	}
	
	public Date getServiceCompletionDate() {
		return serviceCompletionDate;
	}

	public void setServiceCompletionDate(Date serviceCompletionDate) {
		if(serviceCompletionDate!=null){
			this.serviceCompletionDate = serviceCompletionDate;
		}
	}
	
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		if(serviceType!=null){
			this.serviceType = serviceType.trim();
		}
	}

	public boolean isAdHocService() {
		return isAdHocService;
	}

	public void setAdHocService(boolean isAdHocService) {
		this.isAdHocService = isAdHocService;
	}

	public String getServiceDay() {
		return serviceDay;
	}

	public void setServiceDay(String serviceDay) {
		if(serviceDay!=null){
			this.serviceDay = serviceDay.trim();
		}
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		if(serviceTime!=null){
			this.serviceTime = serviceTime.trim();
		}
	}

	public String getServiceBranch() {
		return serviceBranch;
	}

	public void setServiceBranch(String serviceBranch) {
		if(serviceBranch!=null){
			this.serviceBranch = serviceBranch.trim();
		}
	}
	
	public int getServiceIndexNo() {
		return serviceIndexNo;
	}

	public void setServiceIndexNo(int serviceIndexNo) {
		this.serviceIndexNo = serviceIndexNo;
	}

	public int getServiceCompleteDuration() {
		return serviceCompleteDuration;
	}

	public void setServiceCompleteDuration(int serviceCompleteDuration) {
		this.serviceCompleteDuration = serviceCompleteDuration;
	}

	public String getServiceCompleteRemark() {
		return serviceCompleteRemark;
	}

	public void setServiceCompleteRemark(String serviceCompleteRemark) {
		if(serviceCompleteRemark!=null){
			this.serviceCompleteRemark = serviceCompleteRemark.trim();
		}
	}
	
	public String getServiceDateDay() {
		return serviceDateDay;
	}

	public void setServiceDateDay(String serviceDateDay) {
		if(serviceDateDay!=null){
			this.serviceDateDay = serviceDateDay;
		}
	}
	
	
	public double getServiceValue() {
		return serviceValue;
	}

	public void setServiceValue(double serviceValue) {
		this.serviceValue = serviceValue;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	public long getSchedulingNumber() {
		return schedulingNumber;
	}

	public void setSchedulingNumber(long schedulingNumber) {
		this.schedulingNumber = schedulingNumber;
	}
	
	public boolean isRateContractService() {
		return isRateContractService;
	}

	public void setRateContractService(boolean isRateContractService) {
		this.isRateContractService = isRateContractService;
	}
	
	public boolean isAssessmentServiceFlag() {
		return assessmentServiceFlag;
	}

	public void setAssessmentServiceFlag(boolean assessmentServiceFlag) {
		this.assessmentServiceFlag = assessmentServiceFlag;
	}

	
	public String getTechPlanOtherInfo() {
		return techPlanOtherInfo;
	}

	public void setTechPlanOtherInfo(String techPlanOtherInfo) {
		this.techPlanOtherInfo = techPlanOtherInfo;
	}
	
	
	public Date getSystemCompletionDate() {
		return systemCompletionDate;
	}

	public void setSystemCompletionDate(Date systemCompletionDate) {
		this.systemCompletionDate = systemCompletionDate;
	}
	
	public String getCustomerPersonDesignation() {
		return customerPersonDesignation;
	}

	public void setCustomerPersonDesignation(String customerPersonDesignation) {
		this.customerPersonDesignation = customerPersonDesignation;
	}

	public String getCustomerPersonEmpId() {
		return customerPersonEmpId;
	}

	public void setCustomerPersonEmpId(String customerPersonEmpId) {
		this.customerPersonEmpId = customerPersonEmpId;
	}

	/********************************** Relation Managment Part*****************************************************************/
	@OnSave
	@GwtIncompatible
	public void onSave()
	{
		Logger logger = Logger.getLogger("logger");
		//logger.log(Level.SEVERE, "Service entity onSave called");
		/**Date 18-10-2019 by Amol Added a SystemCompletionDate raised by Sonu Sir for orionpestcontrol
		 * To Track the systemDate of service completed"
		 */
		
		if(id!=null&&this.status.equals(SERVICESTATUSCOMPLETED)){
			Date date=new Date();
			this.setSystemCompletionDate(DateUtility.getDateWithTimeZone("IST", date));
		}
		
		if(id==null&&this.isAdHocService==true)
		{
			createAdHocProject();
			statusKey=MyUtility.getConfigKeyFromCondition(status, ConfigTypes.SERVICESTATUS.getValue());
		}
		ServiceProject projectEntity = null;
		if(id!=null)
		{
			projectEntity=ofy().load().type(ServiceProject.class).filter("companyId", this.getCompanyId()).filter("contractId", this.getContractCount()).filter("serviceId", this.getCount()).filter("serviceSrNo", this.getServiceSerialNo()).first().now();
			if(projectEntity!=null)
			{
				projectEntity.setserviceDate(this.getServiceDate());
				ofy().save().entity(projectEntity).now();
			}
		}
		
		/**
		 * 	nidhi
		 *   20-07-2017
		 *   this method declare for set quantity and uom in billing entity
		 */
		System.out.println(" Id : = " +  id  + " status  : - " + this.status +  " quantity : " + this.quantity + " Mesurement : " + this.uom  );
		if(id!=null && this.status.equals(SERVICESTATUSCOMPLETED)){
			if(this.quantity != 0 && this.uom != null ){
				BillingDocument billingDoc = null;
				if(this.isRateContractService()){
					billingDoc= ofy().load().type(BillingDocument.class).filter("companyId", this.getCompanyId()).filter("contractCount", this.getContractCount()).filter("rateContractServiceId", this.getCount()).first().now();
				}else if(this.getBillingCount()!=0){
					billingDoc= ofy().load().type(BillingDocument.class).filter("companyId", this.getCompanyId()).filter("contractCount", this.getContractCount()).filter("count", this.getBillingCount()).first().now();
				}
				
			
				if(billingDoc != null){
					billingDoc.setQuantity(this.quantity);
					billingDoc.setUom(this.uom);
					
					/**
					 *  ||*
					 *  nidhi 15-10-2018
					 */
					boolean validateFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess", this.getCompanyId());
					
					if(!isServiceWiseBilling() && !isRateContractService() && validateFlag){
						HashSet<Integer> serList = new HashSet<Integer>();
						serList.addAll(billingDoc.getServiceId());
						serList.add(this.getCount());
						billingDoc.getServiceId().addAll(serList);
					}
					/**
					 * end
					 */
					ofy().save().entity(billingDoc);
				}
			}else{
				boolean validateFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ServiceBillingDetailMapingProcess", this.getCompanyId());
				
				if(validateFlag){
					BillingDocument billingDoc = null;
					if(this.isRateContractService() ){
						billingDoc= ofy().load().type(BillingDocument.class).filter("companyId", this.getCompanyId()).filter("contractCount", this.getContractCount()).filter("rateContractServiceId", this.getCount()).first().now();
					}else if(this.getBillingCount()!=0 && !this.isServiceWiseBilling() ){
						billingDoc= ofy().load().type(BillingDocument.class).filter("companyId", this.getCompanyId()).filter("contractCount", this.getContractCount()).filter("count", this.getBillingCount()).first().now();
					}
					
					if(billingDoc != null){
						
						/**
						 *  ||*
						 *  nidhi 15-10-2018
						 */
					
						if(!isServiceWiseBilling() && !isRateContractService() ){
							HashSet<Integer> serList = new HashSet<Integer>();
							serList.addAll(billingDoc.getServiceId());
							serList.add(this.getCount());
							billingDoc.getServiceId().addAll(serList);
						}
						/**
						 * end
						 */
						ofy().save().entity(billingDoc);
					}
				}
			
			}
		}
		
		/**
		 *  end
		 */
		/** date 23.3.2019 added by komal to store contract group , type and amount **/
		if(id == null){
			Contract contract = null;
			if(this.getContractCount() != null){
				contract = ofy().load().type(Contract.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
			}
			if(contract != null){
				if(contract.getGroup() != null){
					this.setContractGroup(contract.getGroup());
				}
				if(contract.getType() != null){
					this.setContractType(contract.getType());
				}
				this.setContractAmount(contract.getNetpayable());
				if(this.getServiceDate() != null){
					this.setOldServiceDate(this.getServiceDate());
				}
			}
		}
		
		/** date 25/05.2019 added by komal to reset flag when service is rescheduled or completed **/
		if (id != null) {
			Service service = ofy().load().type(Service.class)
					.filter("companyId", this.getCompanyId())
					.filter("count", this.getCount()).first().now();
			boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MarkServiceScheduledStatusTrue", this.getCompanyId());
			boolean freematerialFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.SERVICEMODULE, AppConstants.PC_FREEMATERIAL, this.getCompanyId());

			if (this.isServiceScheduled() && !flag) {
				System.out.println("Inside Reschedule ====");
				
				
				if (this.status.equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE)
						|| this.status.equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)
						|| this.status.equalsIgnoreCase(Service.SERVICESTATUSTECHCOMPLETED)
						|| this.status.equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)
						|| this.status.equalsIgnoreCase(Service.SERVICESTATUSPLANNED) ) {
					if(this.status.equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE)
						 || this.status.equalsIgnoreCase(Service.SERVICESTATUSPLANNED)){
						int size = 0 , size1 =0;
						
						if(this.getListHistory() != null && this.getListHistory().size() >0){
							size = this.getListHistory().size();
						}
						if(service.getListHistory() != null && service.getListHistory().size() >0){
							size1 = service.getListHistory().size();
						}
						if(size == size1){	
						}else{
							this.setServiceScheduled(false);
							this.setServiceNumber("");
						}
					}else{
						this.setServiceScheduled(false);
						this.setServiceNumber("");
					}
				}
				
				if (service != null) {
					if (!this.getEmployee().equalsIgnoreCase(
							service.getEmployee())) {
						this.setServiceScheduled(false);
						this.setServiceNumber("");
					}
				}
				
				
			}
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "updateProjectOnTechnicianChange", this.getCompanyId())){
				projectEntity=ofy().load().type(ServiceProject.class).filter("companyId", this.getCompanyId()).filter("contractId", this.getContractCount()).filter("serviceId", this.getCount()).first().now();
				if (service != null && projectEntity!= null) {
				if (!this.getEmployee().equalsIgnoreCase(
						service.getEmployee())) {
					updateServiceProject(service , projectEntity);					
				}
			  }
			}
			
			/*** Date 30-09-2020 by Vijay if Free Material process config is true then ScheduleService will not false for change Technician **/
			if(freematerialFlag && (this.status.equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE) || this.status.equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) || this.status.equalsIgnoreCase(Service.SERVICESTATUSOPEN) || this.status.equalsIgnoreCase(Service.SERVICESTATUSPLANNED))){
				System.out.println("on save 222");
				
				if(this.getContractCount() != null){
					Contract contract = ofy().load().type(Contract.class).filter("companyId", this.getCompanyId()).filter("count", this.getContractCount()).first().now();
//					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//					Date myDefaultDate;
//					try {
//						myDefaultDate = format.parse("30/01/2021");
						
						if(contract!=null && contract.isStationedTechnician()) {
							if(contract.isStationedTechnician()) {
								this.setServiceScheduled(true);
							}
//							else {
//								this.setServiceScheduled(false);
//							}
						}
//						else {
//							this.setServiceScheduled(true);
//						}
//					} catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
					
					
				}
				
			}
		}
		/** date 06.09.2019 added by komal to store total planned material cost , total actual material cost ,
		 *  total planned operator cost , total actual operator cost in service as per nitin sir's instruction
		 */
		
			if(projectEntity != null){
				ofy().load().entity(projectEntity);
			}else{
				projectEntity = ofy().load()
					.type(ServiceProject.class)
					.filter("companyId", companyId)
					.filter("serviceId", count).first().now();
				if(projectEntity != null){
					ofy().load().entity(projectEntity);
				}
			}
			/*** Date 01-11-2019 by Vijay Process configuration Changed due to MIN Unable to Upload getting exception as concurrent process tas was cancelled ***/
			 boolean billofMaterialActive = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ApplyBillOfMaterial", this.getCompanyId());
			if (billofMaterialActive && id != null) {
				double totalMaterialCostPlanned = 0;
				double totalMaterialCostActual = 0;
				double totalOperatorCostPlanned = 0;
				double totalOperatorCostActual = 0;
				if (this.getTotalPlannedMaterialCost() == 0) {
					if (this.getServiceProductList() != null) {
						for (ServiceProductGroupList groupList : this
								.getServiceProductList()) {
							totalMaterialCostPlanned += (groupList.getPrice());
						}
					}
					this.setTotalPlannedMaterialCost(totalMaterialCostPlanned);
				}
				if (this.totalPlannedOperatorCost == 0) {
					Quotation quotation = ofy().load().type(Quotation.class)
							.filter("companyId", companyId)
							.filter("contractCount", this.getContractCount())
							.first().now();
					if (quotation != null && quotation.getManPowCostList() != null
							&& quotation.getManPowCostList().size() > 0) {

						for (ManPowerCostStructure manPower : quotation
								.getManPowCostList()) {
							if (this.getProduct().getProductName()
									.equalsIgnoreCase(manPower.getServiceName())) {
								totalOperatorCostPlanned += manPower.getTotalCost();
							}
						}
						this.setTotalPlannedOperatorCost(totalOperatorCostPlanned);
					} else {
						Map<String, Double> empHourlyRateMap = new HashMap<String, Double>();
						Set<String> employeeSet = new HashSet<String>();
						if (projectEntity != null) {
							
							for (EmployeeInfo info : projectEntity.getTechnicians()) {
								employeeSet.add(info.getFullName());
							}

							List<Employee> empList = new ArrayList<Employee>();
							if (employeeSet.size() > 0) {
								ArrayList<String> employeeNameList = new ArrayList<String>();
								employeeNameList.addAll(employeeSet);
								empList = ofy().load().type(Employee.class)
										.filter("companyId", companyId)
										.filter("fullname IN", employeeNameList)
										.list();
							}
							for (Employee emp : empList) {
								double empMonthlySalary = emp.getSalaryAmt();
								double empMonthlyWorkingHrs = emp
										.getMonthlyWorkingHrs();
								double empHourSalary = empMonthlySalary
										/ empMonthlyWorkingHrs;
								empHourlyRateMap.put(emp.getFullName(),
										empHourSalary);
							}
							for (EmployeeInfo info : projectEntity.getTechnicians()) {
								double salary = 0;
								if (empHourlyRateMap
										.containsKey(info.getFullName())) {
									salary = empHourlyRateMap.get(info
											.getFullName());
								}
								if (this.getServicingTime() != null) {
									totalOperatorCostPlanned += (salary * this
											.getServicingTime());
								} else {
									totalOperatorCostPlanned += 0;
								}

							}
						}else{
							Employee emp = ofy().load().type(Employee.class)
									.filter("companyId", companyId)
									.filter("fullname ", this.getEmployee())
									.first().now();
								if(emp != null){
									double empMonthlySalary = emp.getSalaryAmt();
									double empMonthlyWorkingHrs = emp
											.getMonthlyWorkingHrs();
									double empHourSalary = empMonthlySalary
											/ empMonthlyWorkingHrs;
									if (this.getServicingTime() != null) {
										totalOperatorCostPlanned += (empHourSalary * this
												.getServicingTime());
									} else {
										totalOperatorCostPlanned += 0;
									}
								}
						}

						this.setTotalPlannedOperatorCost(totalOperatorCostPlanned);

					}

				}
				if (this.getStatus().equalsIgnoreCase(
						Service.SERVICESTATUSCOMPLETED)) {

					Map<String, Double> empHourlyRateMap = new HashMap<String, Double>();
					Set<String> employeeSet = new HashSet<String>();
					Set<Integer> productIdSet = new HashSet<Integer>();
					Map<Integer, Double> productMap = new HashMap<Integer, Double>();
					

					if (projectEntity != null) {
						for (ProductGroupList list : projectEntity
								.getProdDetailsList()) {
							productIdSet.add(list.getProduct_id());
						}
						for (EmployeeInfo info : projectEntity.getTechnicians()) {
							employeeSet.add(info.getFullName());
						}
						if (productIdSet.size() > 0) {
							ArrayList<Integer> productIdList = new ArrayList<Integer>(
									productIdSet);
							List<SuperProduct> productList = ofy().load()
									.type(SuperProduct.class)
									.filter("companyId", companyId)
									.filter("count IN", productIdList).list();
							for (SuperProduct product : productList) {
								if (product.getPurchasePrice() != 0) {
									productMap.put(product.getCount(),
											product.getPurchasePrice());
								} else {
									productMap.put(product.getCount(),
											product.getPrice());
								}
							}
						}

						for (ProductGroupList group : projectEntity
								.getProdDetailsList()) {
							if (group.getReturnQuantity() != 0) {
								totalMaterialCostActual += (group
										.getReturnQuantity() * productMap.get(group
										.getProduct_id()));
								totalMaterialCostPlanned += (group
										.getPlannedQty() * productMap.get(group
										.getProduct_id()));
							}

						}

						if (this.getTotalPlannedMaterialCost() == 0) {
							this.setTotalPlannedMaterialCost(totalMaterialCostPlanned);
						}
						List<Employee> empList = new ArrayList<Employee>();
						if (employeeSet.size() > 0) {
							ArrayList<String> employeeNameList = new ArrayList<String>();
							employeeNameList.addAll(employeeSet);
							empList = ofy().load().type(Employee.class)
									.filter("companyId", companyId)
									.filter("fullname IN", employeeNameList).list();
						}
						for (Employee emp : empList) {
							double empMonthlySalary = emp.getSalaryAmt();
							double empMonthlyWorkingHrs = emp
									.getMonthlyWorkingHrs();
							double empHourSalary = empMonthlySalary
									/ empMonthlyWorkingHrs;
							empHourlyRateMap.put(emp.getFullName(), empHourSalary);
						}
						for (EmployeeInfo info : projectEntity.getTechnicians()) {
							double salary = 0;
							if (empHourlyRateMap.containsKey(info.getFullName())) {
								salary = empHourlyRateMap.get(info.getFullName());
							}

							totalOperatorCostActual += (salary * (this
									.getServiceCompleteDuration() / 60.0));
						}
					}

					this.setTotalActualOeratorCost(totalOperatorCostActual);
					this.setTotalActualMaterialCost(totalMaterialCostActual);

				}
			 }
		
			
			/**
			 * @author Vijay Date :- 08-04-2022
			 * Des :- As per nitin sir updating service with new logic
			 */
			
			if(id!=null&&this.status.equals(SERVICESTATUSCOMPLETED) && !this.isRateContractService()){
				try {
					
				Contract contractEntity = ofy().load().type(Contract.class).filter("count", this.getContractCount())
										.filter("companyId", this.getCompanyId()).first().now();
				if(contractEntity!=null){
					ServerAppUtility serverapputility = new ServerAppUtility();
					for(SalesLineItem item : contractEntity.getItems()){
						if(item.getProductCode().equals(this.getProduct().getProductCode()) && this.getServiceSrNo()==item.getProductSrNo() ){
								this.setServiceValue(serverapputility.getServiceValue(item, this.getServiceBranch(), this.getServiceSerialNo(), this.getCompanyId(),this.isRateContractService()));
						}
							
					}
				}
				
				} catch (Exception e) {
					e.printStackTrace();
				}	
				
			}
			
			/**
			 * @author Vijay
			 * Des :- Service cost calculation
			 */
			try {
				if(id!=null&&this.status.equals(SERVICESTATUSCOMPLETED)){
					if(this.getMaterialCost()==0) {
						double materialPrice = getMaterialPrice(this.getCount(),this.getCompanyId());
						this.setMaterialCost(materialPrice);
					}
				
					if(this.getLaborCost()==0) {
						double laborCost = getLaborCost(this.getCount(),this.getCompanyId());
						this.setLaborCost(laborCost);
					}
				
				}
			} catch (Exception e) {
			}
			
			/**
			 * ends here
			 */
			try {
				if(id!=null&&this.getCatchtrapList()!=null&&this.getCatchtrapList().size()>0) {
				//	logger.log(Level.SEVERE, "calling updateFindingsEntity from onsave");
					
					TechnicianAppOperation opr=new TechnicianAppOperation();
					opr.updateFindingsEntity(this, this.getCatchtrapList());
				}
			}catch(Exception e) {
					
			}
				

	}
	
	@GwtIncompatible
	private double getLaborCost(int serviceId, Long companyId) {
		double labourcost = 0;
		Logger logger = Logger.getLogger("logger");
		try {
			
		ServiceProject serviceproject = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("count", serviceId)
				.filter("status", ServiceProject.COMPLETED).first().now();
		ArrayList<Integer> employeeIdlist = new ArrayList<Integer>();
		
		if(serviceproject!=null && serviceproject.getTechnicians().size()>0){
			for(EmployeeInfo employeeinfo : serviceproject.getTechnicians()){
				employeeIdlist.add(employeeinfo.getEmpCount());
			}
		}
		else{
			Employee employee = ofy().load().type(Employee.class).filter("companyId", this.getCompanyId()).filter("fullname", this.getEmployee()).first().now();
			if(employee!=null){
				employeeIdlist.add(employee.getCount());
			}
		}
		
		List<CTC> ctclist = ofy().load().type(CTC.class).filter("companyId", companyId).filter("empid IN", employeeIdlist)
							.filter("status", CTC.ACTIVE).list();
		logger.log(Level.SEVERE, "ctclist size "+ctclist.size());

		ArrayList<Integer> noctcEmployeelist = new ArrayList<Integer>();
		noctcEmployeelist.addAll(employeeIdlist);
		
		logger.log(Level.SEVERE, "all noctcEmployeelist size "+noctcEmployeelist.size());
		if(ctclist.size()>0){
			for(CTC ctc : ctclist){
				for(int i=0;i<employeeIdlist.size();i++){
					if(ctc.getEmpid()==employeeIdlist.get(i)){
						noctcEmployeelist.remove(i);
					}
				}
			}
			
			List<EmployeeInfo> employeeinfolist = ofy().load().type(EmployeeInfo.class).filter("empCount IN", employeeIdlist)
													.filter("companyId", companyId).list();
			logger.log(Level.SEVERE, "employeeinfolist size "+employeeinfolist.size());

			for(CTC ctc : ctclist){
				logger.log(Level.SEVERE, "CTC Wise labour cost ");
				for(EmployeeInfo employee : employeeinfolist){
					if(ctc.getEmpid() == employee.getEmpCount()){
						logger.log(Level.SEVERE, "employee id "+employee.getEmpCount());

						if(employee.getLeaveCalendar()!=null && employee.getLeaveCalendar().getWorkingHours()>0){
							double monthlySalary = ctc.getCtcAmount()/12;
							int numberOfDaysInMonth = getnumberofDays(this.getCompletedDate());
							double daysalary = monthlySalary/numberOfDaysInMonth;
							double hoursalary = daysalary/employee.getLeaveCalendar().getWorkingHours();
							double minutesSalary = hoursalary/60;
							
							long difference_In_Minutes = 0;
							
							if(this.getStartedDate_time()!=null && !this.getStartedDate_time().equals("") && this.getCompletedDate_time()!=null && !this.getCompletedDate_time().equals("")){
								 difference_In_Minutes = getDurationOfDatesInMinutes(this.getStartedDate_time(),this.getCompletedDate_time());
							}
							else{
								difference_In_Minutes = this.getServiceCompleteDuration();
							}
							logger.log(Level.SEVERE, "difference_In_Minutes "+difference_In_Minutes);

							if(difference_In_Minutes>0){
								labourcost += minutesSalary*difference_In_Minutes;
							}
						}
						else{
							noctcEmployeelist.remove(employee.getEmpCount());
						}
					
					}
				}
			}
			logger.log(Level.SEVERE, "labourcost "+labourcost);

//			getEmployeeLaborCostOnBasisSalary();
//			completed - started for service duration
			
		}
		logger.log(Level.SEVERE,"noctcEmployeelist size "+noctcEmployeelist.size());
		if(noctcEmployeelist.size()>0){
			List<Employee> employeelist = ofy().load().type(Employee.class).filter("count IN", employeeIdlist)
					.filter("companyId", companyId).list();
			if(employeelist.size()>0){
				for(Employee employee : employeelist){
					logger.log(Level.SEVERE,"employee master wise calculation size ");
					logger.log(Level.SEVERE,"employee.getSalaryAmt() "+employee.getSalaryAmt());
					logger.log(Level.SEVERE,"employee.getMonthlyWorkingHrs() "+employee.getMonthlyWorkingHrs());
					 
					  
					if(employee.getSalaryAmt()>0 && employee.getMonthlyWorkingHrs()>0){
						double monthlySalary = employee.getSalaryAmt()/12;
						int numberOfDaysInMonth = getnumberofDays(this.getCompletedDate());
						double daysalary = monthlySalary/numberOfDaysInMonth;
						double hoursalary = daysalary/employee.getMonthlyWorkingHrs();
						double minutesSalary = hoursalary/60;
						long difference_In_Minutes = 0;
						if(this.getStartedDate_time()!=null && !this.getStartedDate_time().equals("") && this.getCompletedDate_time()!=null && !this.getCompletedDate_time().equals("")){
							 difference_In_Minutes = getDurationOfDatesInMinutes(this.getStartedDate_time(),this.getCompletedDate_time());
						}
						else{
							difference_In_Minutes = this.getServiceCompleteDuration();
						}
						logger.log(Level.SEVERE, "difference_In_Minutes "+difference_In_Minutes);
						if(difference_In_Minutes>0){
							labourcost += minutesSalary*difference_In_Minutes;
						}
						
					}
				}
			}

		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return labourcost;
	}
	
	@GwtIncompatible
	private long getDurationOfDatesInMinutes(String startedDate_time,
			String completedDate_time) {
		Logger logger = Logger.getLogger("logger");

		SimpleDateFormat sdf   = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		long difference_In_Minutes =0;
		
		try {
			Date startedDateTime = sdf.parse(startedDate_time);
			Date completedDateTime = sdf.parse(completedDate_time);
			
			long difference_In_Time  = completedDateTime.getTime() - startedDateTime.getTime();
			difference_In_Minutes  = (difference_In_Time   / (1000 * 60)) % 60;
//          long difference_In_Hours  = (difference_In_Time / (1000 * 60 * 60)) % 24;

		} catch (ParseException e) {
			e.printStackTrace();
			SimpleDateFormat sdf2   = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			try {
				Date startedDateTime = sdf2.parse(startedDate_time);
				Date completedDateTime = sdf2.parse(completedDate_time);
			long difference_In_Time  = completedDateTime.getTime() - startedDateTime.getTime();
			difference_In_Minutes  = (difference_In_Time   / (1000 * 60)) % 60;
			
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		logger.log(Level.SEVERE, "difference_In_Time == "+difference_In_Minutes);

		return difference_In_Minutes;
	}

	@GwtIncompatible
	private int getnumberofDays(Date completedDate) {
		Logger logger = Logger.getLogger("logger");
		int numDays=30;
		
		try {
		Calendar cal = Calendar.getInstance();
		cal.setTime(completedDate);
		int month = cal.get(Calendar.MONTH);
		
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(completedDate);
		int year = cal2.get(Calendar.YEAR);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		 numDays = calendar.getActualMaximum(Calendar.DATE);
		logger.log(Level.SEVERE, "numDays of month "+numDays);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return numDays;
	}


	/**
	 * @author Vijay Date :- 12-09-2022
	 * Des :- Currenty we not storing unit conversion logic in price and qty in the MIN and in the Project also
	 * so we will calculate the material price qty * purchase price will our material price
	 * 
	 */
	@GwtIncompatible
	private double getMaterialPrice(int serviceId, Long companyId) {
		double materialcost = 0;
		Logger logger = Logger.getLogger("logger");

		try {
			logger.log(Level.SEVERE, "Service completed from App "+this.isCompletedByApp());
			if(this.isCompletedByApp()) {
				
				ServiceProject serviceproject = ofy().load().type(ServiceProject.class).filter("companyId", companyId).filter("serviceId", serviceId)
										.first().now();
				logger.log(Level.SEVERE, "serviceproject =="+serviceproject);
				
				List<Integer> itemproductIDlist = new ArrayList<Integer>();
				if(serviceproject!=null){
				logger.log(Level.SEVERE, "serviceproject status"+serviceproject.getServiceStatus());
				
				for(ProductGroupList productDetails : serviceproject.getProdDetailsList()){
				itemproductIDlist.add(productDetails.getProduct_id());
				}
				}
				logger.log(Level.SEVERE, "itemproductIDlist"+itemproductIDlist);
				
				List<ItemProduct> itemproductlist = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
											.filter("count IN", itemproductIDlist).list();
				logger.log(Level.SEVERE, "itemproductlist =="+itemproductlist.size());
				
				if(itemproductlist.size()>0){
				for(ProductGroupList productDetails : serviceproject.getProdDetailsList()){
				for(ItemProduct itemproduct : itemproductlist){
					if(productDetails.getProduct_id()==itemproduct.getCount()){
						
						double consumedQty =0;
						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity", companyId)){
									logger.log(Level.SEVERE, "IF consume then");
									consumedQty = productDetails.getReturnQuantity();
						}else{
								if(productDetails.getReturnQuantity()!=0){
									logger.log(Level.SEVERE, "IF return then");
									consumedQty =productDetails.getQuantity()-productDetails.getReturnQuantity();
								}else{
									System.out.println("");
									logger.log(Level.SEVERE, "NO  return then");
									consumedQty =productDetails.getQuantity();
								}
						}
						logger.log(Level.SEVERE, "consumedQty"+consumedQty);
						logger.log(Level.SEVERE, "itemproduct.getPurchasePrice()"+itemproduct.getPurchasePrice());
				
						if(consumedQty>0 && itemproduct.getPurchasePrice()>0){
							materialcost += consumedQty*itemproduct.getPurchasePrice();
							logger.log(Level.SEVERE, "material cost "+materialcost);
						}
					}
				}
				}
				
				}
				logger.log(Level.SEVERE, "materialcost =="+materialcost);

			}
			else {
				logger.log(Level.SEVERE, "cheking MIN "+this.isCompletedByApp());

				MaterialIssueNote minEntity = ofy().load().type(MaterialIssueNote.class).filter("companyId", companyId).filter("serviceId", serviceId)
												.filter("status", MaterialIssueNote.APPROVED).first().now();
				if(minEntity!=null) {
					ArrayList<Integer> itemproductIDlist = new ArrayList<Integer>();
					for(MaterialProduct lineItem : minEntity.getSubProductTablemin()){
						itemproductIDlist.add(lineItem.getMaterialProductId());
					}

					logger.log(Level.SEVERE, "itemproductIDlist"+itemproductIDlist);
					List<ItemProduct> itemproductlist = ofy().load().type(ItemProduct.class).filter("companyId", companyId)
												.filter("count IN", itemproductIDlist).list();
					logger.log(Level.SEVERE, "itemproductlist =="+itemproductlist.size());
					
					if(itemproductlist.size()>0){
						
						for(MaterialProduct minproduct :minEntity.getSubProductTablemin()) {
							for(ItemProduct itemproduct : itemproductlist){
								if(minproduct.getMaterialProductId()== itemproduct.getCount()) {
									double consumedQty =minproduct.getMaterialProductRequiredQuantity();
									if(consumedQty>0 && itemproduct.getPurchasePrice()>0){
										materialcost += consumedQty*itemproduct.getPurchasePrice();
										logger.log(Level.SEVERE, "material cost "+materialcost);
									}
								}

							}

						}
					}
					
				}
				
				logger.log(Level.SEVERE, "materialcost =="+materialcost);

			}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	
		return materialcost;
	}

	@OnSave
	@GwtIncompatible
	public void setServiceId()
	{
		boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","ServiceStatusAsPlanned",this.getCompanyId());
		if(id==null){
			if(getTicketNumber()!=-1){
				System.out.println("INSIDE SERVICE ON SAVE....!!!!");
				Complain complain=ofy().load().type(Complain.class).filter("companyId", this.getCompanyId()).filter("count", this.getTicketNumber()).first().now();
				if(complain!=null){
					complain.setServiceId(this.getCount());
					complain.setServiceDate(this.getServiceDate());
					if(this.getServiceTime()!=null){
						complain.setServicetime(this.getServiceTime());
					}
				}
				ofy().save().entity(complain);
			}
			if(flag){
				this.setStatus(Service.SERVICESTATUSOPEN);
			}
			/**
			 * @author Anil @since 16-12-2021
			 * If service product is of assessment category then set service type as assessment and mark assessment flag active
			 * raised by Nitin for prominent
			 */
			if(product!=null&&product.getProductCategory()!=null){
				if(product.getProductCategory().equalsIgnoreCase("Assessment")){
					setServiceType("Assessment");
					setAssessmentServiceFlag(true);
				}
			}
			
			
		}
		if(id != null){
			if(flag){
				if(this.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
					this.setStatus(Service.SERVICESTATUSPLANNED);
				}
			
			}
		}
	}
	
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{
		
		if(statusKey!=null)
		{
			String statusName=MyUtility.getConfigNameFromKey(statusKey);
			if(statusName.equals("")==false)
				setStatus(statusName);
		}
		
		if(personInfoKey!=null)
		{
			personInfo=ofy().load().key(personInfoKey).now();
		}
	}
	/******************************************************************************************************************/

	@GwtIncompatible
	protected void createAdHocProject()
	{
		ServiceProject projectEntity=new ServiceProject();
		projectEntity.setCompanyId(this.getCompanyId());
		projectEntity.setAddr(this.getAddress());
		projectEntity.setContractStartDate(this.getContractStartDate());
		projectEntity.setContractEndDate(this.getContractEndDate());
		projectEntity.setContractId(this.getContractCount());
		projectEntity.setPersonInfo(this.getPersonInfo());
		projectEntity.setProjectName("Project Service"+this.getServiceSerialNo());
		projectEntity.setProjectStatus(ServiceProject.CREATED);
		projectEntity.setserviceDate(this.getServiceDate());
		projectEntity.setServiceSrNo(this.getServiceSerialNo());
		projectEntity.setServiceStatus(this.getStatus());
		projectEntity.setserviceEngineer(this.getEmployee());
		projectEntity.setserviceId(this.getCount());
		
		GenricServiceImpl impl=new GenricServiceImpl();
		impl.save(projectEntity);
	}
	
	public Long getCellNumber() {
		// TODO Auto-generated method stub
		if(personInfo!=null)
			return personInfo.getCellNumber();
		else
			return null;
	}
	
	public String getName() {
		// TODO Auto-generated method stub
		if(personInfo!=null)
			return personInfo.getFullName();
		else
			return null;
	}

	public String getProductName() {
		if(this.product!=null)
			return this.product.getProductName();
		return null;
	}
	
	public static List<String> getStatusList()
	{
		ArrayList<String> servicestatuslist=new ArrayList<String>();
		servicestatuslist.add(SERVICESTATUSSCHEDULE);
		servicestatuslist.add(SERVICESTATUSCANCELLED);
		servicestatuslist.add(SERVICESTATUSCOMPLETED);
		servicestatuslist.add(SERVICESTATUSPENDING);
		servicestatuslist.add(SERVICESTATUSRESCHEDULE);
		servicestatuslist.add(SERVICESTATUSCLOSED);
		servicestatuslist.add(SERVICESTATUSTECHCOMPLETED);
		servicestatuslist.add(SERVICESTATUSREPORTED);
		servicestatuslist.add(SERVICESUSPENDED);//Add by jayshree
		servicestatuslist.add(SERVICESTATUSPLANNED);
		servicestatuslist.add(SERVICETCOMPLETED);
		servicestatuslist.add(SERVICESTATUSOPEN);
		servicestatuslist.add(SERVICESTATUSREOPEN);
		servicestatuslist.add(SERVICESTATUSSCHEDULERESCHEDULE);
		servicestatuslist.add(SERVICESTATUSSTARTEDREPORTEDTCOMPLETED);
		return servicestatuslist;
	}

	
	public Service Myclone() {
		// TODO Auto-generated method stub
		Service temp=new Service();
		temp = this;
		temp.id=id;
		temp.companyId=companyId;
		temp.setDeleted(isDeleted());
		temp.setStatus(status);
		temp.setEmployee(employee);
		temp.branch=branch;
		temp.creationTime=creationTime;
		temp.approvalTime=approvalTime;
		temp.creationDate=new Date();
		temp.group=group;
		temp.category=category;
		temp.type=type;
		temp.serviceDate=this.serviceDate;
		temp.reasonForChange="Service Rescheduled";
		temp.address=address;
		temp.product=product;
		temp.contractCount=contractCount;
		temp.personInfo=personInfo;
		temp.comment=comment;
		temp.deviceList=deviceList;
		temp.uptestReport=temp.uptestReport;
		temp.upDeviceDocument=upDeviceDocument;
		
		
		temp.listHistory = listHistory;
		temp.serviceCompletionDate= serviceCompletionDate ;
		
		temp.serviceDay= serviceDay ;
		temp.serviceDateDay= serviceDateDay ;
		temp.serviceTime= serviceTime ;
		temp.serviceBranch = serviceBranch;
		temp.serviceCompleteDuration= serviceCompleteDuration;
		temp.serviceCompleteRemark = serviceCompleteRemark;
		
		temp.catchtrapList = catchtrapList;
		
		temp.ticketNumber= ticketNumber;
		
		temp.customerFeedback = customerFeedback;
		
		temp.servicingTime = servicingTime;
		
		temp.team = team;
		temp.scheduled = scheduled;
		temp.reason = reason;
		temp.specificServFlag = specificServFlag;
		
		temp.recordSelect = recordSelect;

		temp.numberRange = numberRange;
		
		temp.trackServiceTabledetails = trackServiceTabledetails;
		
		temp.refNo = refNo;
		
		temp.remark = remark;
		
		temp.technicianremark = technicianremark;
		temp.premises = premises;
		
		
		temp.fromTime = fromTime;
		temp.toTime = toTime;

		temp.wareHouse = wareHouse;

		temp.storageLocation = storageLocation;
		temp.storageBin= storageBin ;

		temp.stackDetailsList =stackDetailsList;
		temp.refNo2 = refNo2;
		temp.serviceWeekNo = serviceWeekNo;

		temp.quantity = quantity;
		temp.uom = uom;

		temp.serviceValue=serviceValue;
		
		temp.serviceWiseBilling = serviceWiseBilling;
		temp.serviceSrNo = serviceSrNo;
		temp.schedulingNumber = schedulingNumber;
		
		temp.projectId = projectId;
		temp.invoiceId = invoiceId;
		temp.invoiceDate = invoiceDate;
		temp.isRateContractService = isRateContractService;
		temp.assessmentServiceFlag = assessmentServiceFlag;
		
		temp.techPlanOtherInfo = techPlanOtherInfo;
		temp.customerSignature = customerSignature;
		temp.customerSignName = customerSignName;
		temp.customerSignNumber = customerSignNumber;
		temp.description  =description;
		
		temp.technicians = technicians;
		temp.startLatnLong=startLatnLong;
		temp.reportLatnLong=reportLatnLong;
		temp.completeLatnLong=completeLatnLong;
		temp.perUnitPrice = perUnitPrice;
		temp.costCenter  =costCenter;

//		temp.personInfoKey = personInfoKey;

		temp.serviceWiseLatLong=serviceWiseLatLong;
		
		temp.renewContractFlag = renewContractFlag;
		
		temp.isCallwiseService= isCallwiseService ;
		temp.proModelNo = proModelNo;
		temp.proSerialNo= proSerialNo ;
		temp.serviceProductList = serviceProductList  ;
		
		temp.serviceDate = serviceDate;

		temp.reasonForChange = reasonForChange;

		temp.product = product;
		temp.contractCount = contractCount;
		
		temp.contractStartDate = contractStartDate;
		
		temp.contractEndDate  =contractEndDate;
		
		temp.serviceSerialNo  =serviceSerialNo;
		temp.serviceIndexNo = serviceIndexNo;
		
		temp.personInfo= personInfo;
		
		temp.comment = comment;
		
		temp.deviceList = deviceList;
		temp.serviceType =serviceType;
		temp.isAdHocService = isAdHocService;
		temp.uptestReport = uptestReport;
		temp.createdBy = createdBy;
		
		temp.status=status;
		temp.employee=employee;
		temp.branch=branch;
		temp.creationTime=creationTime;
		temp.creationDate=creationDate;
		temp.approverName=approverName;
		temp.group=group;
		temp.category=category;
		temp.type=type;
		temp.remark=remark;
	

		temp.companyId=companyId;
		temp.setDeleted(isDeleted());
		temp.setStatus(status);
		temp.setEmployee(employee);
		temp.branch=branch;
		temp.creationTime=creationTime;
		temp.approvalTime=approvalTime;
		temp.creationDate=new Date();
		temp.group=group;
		temp.category=category;
		temp.type=type;
		temp.serviceDate=this.serviceDate;
		temp.reasonForChange="Service Rescheduled";
		temp.address=address;
		temp.product=product;
		temp.contractCount=contractCount;
		temp.personInfo=personInfo;
		temp.comment=comment;
		temp.deviceList=deviceList;
		temp.uptestReport=temp.uptestReport;
		temp.upDeviceDocument=upDeviceDocument;
		/**
		 * nidhi
		 * 11-10-2018
		 */
		temp.billingCount = billingCount;
			/**
			 * end
			 */
		temp.serviceNumber = serviceNumber;
		this.hours = hours;
		this.minutes = minutes;
		this.ampm = ampm;
		return temp;
	}

	
	public String getCustomerName() {
		if(personInfo!=null)
			return personInfo.getFullName();
		else
			return "";
		
	}

	public ArrayList<ModificationHistory> getListHistory() {
		return listHistory;
	}

	public void setListHistory(List<ModificationHistory> listHistory) {
		if(listHistory!=null)
		{
			this.listHistory =new ArrayList<ModificationHistory>();
			this.listHistory.addAll(listHistory);
		
		}
	}
	
	
	
	public ArrayList<CatchTraps> getCatchtrapList() {
		return catchtrapList;
	}

	public void setCatchtrapList(List<CatchTraps> list) {
		if(list!=null)
		{
			this.catchtrapList =new ArrayList<CatchTraps>();
			this.catchtrapList.addAll(list);
		
		}
	}
	
	/**
	 * Date :- 03-02-2018 by vijay
	 * address object if condition !null checked in address getter because old records download getting issue 
	 */

	public String getLocality() {
		if(address!=null)
		return address.getLocality();
		else
			return "";
	}

	public void setLocality(String locality) {
		address.setLocality(locality);
	}

	public String getLandmark() {
		if(address!=null){
			if(address.getLandmark()!=null || address.getLandmark().equals("")==false)
				return address.getLandmark();
			else
				return "";
		}
		
		else
			return "";
	}

	public void setLandmark(String landmark) {
			address.setLandmark(landmark);
	}

	public String getAddrLine1() {
		if(address!=null)
		   return address.getAddrLine1();
		else
		   return "";	
	}

	public void setAddrLine1(String addrLine1) {
		address.setAddrLine1(addrLine1);
	}

	public String getAddrLine2() {
		if(address!=null){
			if(address.getAddrLine2()!=null || address.getAddrLine2().equals("")==false)
				return address.getAddrLine2();
			else
				return "";
		}
		else
			return "";
	}

	
	public String getState() {
		if(address!=null)
		return address.getState();
		else
		return "";
	}

	public void setState(String state) {
		address.setState(state);
	}

	public String getCity() {
		if(address!=null)
		return address.getCity();
		else
			return "";
	}

	public void setCity(String city) {
		address.setCity(city);
	}

	public long getPin() {
		if(address!=null)
		return address.getPin();
		else
			return 0;
	}

	public void setPin(long pin) {
		address.setPin(pin);
	}

	public String getCountry() {
		if(address!=null)
		return address.getCountry();
		else
			return "";
	}

	public void setCountry(String country) {
		address.setCountry(country);
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	
	public int getCustomerId() {
		if(this.personInfo!=null)
		{
			return personInfo.getCount();
		}
		return -1;
	}

	
	/*@Override
	public int compareTo(SuperModel arg0) 
	{
		if(arg0 instanceof Service)
		{
			Service service=(Service) arg0;
			if(this.serviceDate!=null &&service.serviceDate!=null)
			{
				if(this.serviceDate.equals(service.serviceDate))
				{
					if(this.getCustomerName().equals(service.getCustomerName()))
							{
						      return getStatus().compareTo(service.getStatus());
							}
					else
						getCustomerName().compareTo(service.getCustomerName());
				}
				else
				{
					return serviceDate.compareTo(service.serviceDate);
				}
			}
			return 0;
		}
		
		return 0;
	}*/
	
	public String getEntireAddress()
	{
		String address;
		address=getAddrLine1()+" "+getAddrLine2()+" "+getLandmark()
				+" "+getLocality()+" "+getCity()+" "+getState()+" "+getCountry()+" "+getPin();
		return address;
	}

	public String getCustomerFeedback() {
		return customerFeedback;
	}

	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}

	public Double getServicingTime(){
		return servicingTime;
	}

	public void setServicingTime(Double servicingTime) {
		this.servicingTime = servicingTime;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public Boolean getScheduled() {
		return scheduled;
	}

	public void setScheduled(Boolean scheduled) {
		this.scheduled = scheduled;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getSpecificServFlag() {
		return specificServFlag;
	}

	public void setSpecificServFlag(Boolean specificServFlag) {
		this.specificServFlag = specificServFlag;
	}

	public Boolean getRecordSelect() {
		return recordSelect;
	}

	public void setRecordSelect(Boolean recordSelect) {
		this.recordSelect = recordSelect;
	}

	public String getNumberRange() {
		return numberRange;
	}

	public void setNumberRange(String numberRange) {
		this.numberRange = numberRange;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public ArrayList<TrackTableDetails> getTrackServiceTabledetails() {
		return trackServiceTabledetails;
	}

	public void setTrackServiceTabledetails(
			List<TrackTableDetails> trackServiceTabledetails) {
		ArrayList<TrackTableDetails> list=new ArrayList<TrackTableDetails>();
		list.addAll(trackServiceTabledetails);
		this.trackServiceTabledetails = list;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	/**
	 * Date2-7-2018
	 * By Jayshree
	 * add new field
	 * @return
	 */
	
	
	public String getTechnicianRemark() {
		return technicianremark;
	}

	public void setTechnicianRemark(String technicianremark) {
		this.technicianremark = technicianremark;
	}

	public String getPremises() {
		return premises;
	}

	public void setPremises(String premises) {
		this.premises = premises;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}

	public String getWareHouse() {
		return wareHouse;
	}

	public void setWareHouse(String wareHouse) {
		this.wareHouse = wareHouse;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}

	public void setTrackServiceTabledetails(
			ArrayList<TrackTableDetails> trackServiceTabledetails) {
		ArrayList<TrackTableDetails> list=new ArrayList<TrackTableDetails>();
		list.addAll(trackServiceTabledetails);
		this.trackServiceTabledetails = list;
	}
	
	
	
	public ArrayList<StackDetails> getStackDetailsList() {
		return stackDetailsList;
	}

	public void setStackDetailsList(ArrayList<StackDetails> stackDetailsList) {
		this.stackDetailsList = stackDetailsList;
	}

	
	
	public int getServiceWeekNo() {
		return serviceWeekNo;
	}

	public void setServiceWeekNo(int serviceWeekNo) {
		this.serviceWeekNo = serviceWeekNo;
	}
	
	
	
	
	public DocumentUpload getCustomerSignature() {
		return customerSignature;
	}

	public void setCustomerSignature(DocumentUpload customerSignature) {
		this.customerSignature = customerSignature;
	}

	public String getCustomerSignName() {
		return customerSignName;
	}

	public void setCustomerSignName(String customerSignName) {
		this.customerSignName = customerSignName;
	}

	public String getCustomerSignNumber() {
		return customerSignNumber;
	}

	public void setCustomerSignNumber(String customerSignNumber) {
		this.customerSignNumber = customerSignNumber;
	}
	
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

	public String getServiceWiseLatLong() {
		return serviceWiseLatLong;
	}

	public void setServiceWiseLatLong(String serviceWiseLatLong) {
		this.serviceWiseLatLong = serviceWiseLatLong;
	}

	public double getPerUnitPrice() {
		return perUnitPrice;
	}

	public void setPerUnitPrice(double perUnitPrice) {
		this.perUnitPrice = perUnitPrice;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	
	public boolean isRenewContractFlag() {
		return renewContractFlag;
	}
	

	public boolean isCallwiseService() {
		return isCallwiseService;
	}

	public void setCallwiseService(boolean isCallwiseService) {
		this.isCallwiseService = isCallwiseService;
	}

	public void setRenewContractFlag(boolean renewContractFlag) {
		this.renewContractFlag = renewContractFlag;
	}

	@Override
	public String toString() {
//		return super.toString();
//		return " "+count+" "+serviceDate+" "+product.getProductName();
		return count+"/"+product.getProductName()+"/"+serviceDate;
	}
	
	/**
	 * nidhi
	 * 8-08-2018
	 * 
	 * @return
	 */
	public String getProModelNo() {
		if(this.proModelNo != null){
			return this.proModelNo;
		}else{
			return "";
		}
	}


	public void setProModelNo(String proModelNo) {
		this.proModelNo = proModelNo;
	}


	public String getProSerialNo() {
		if(this.proSerialNo != null){
			return proSerialNo;
		}else{
			return "";
		}
	}


	public void setProSerialNo(String proSerialNo) {
		this.proSerialNo = proSerialNo;
	}

	public ArrayList<ServiceProductGroupList> getServiceProductList() {
		return serviceProductList;
	}

	public void setServiceProductList(
			ArrayList<ServiceProductGroupList> serviceProductList) {
		this.serviceProductList = serviceProductList;
	}
	
	public ServiceProductGroupList getServiceProdDt(ProductGroupList proDt){
		ServiceProductGroupList serPRoDt = new ServiceProductGroupList();
		serPRoDt.setProductGroupId(proDt.getProductGroupId());
		serPRoDt.setProduct_id(proDt.getProduct_id());
		serPRoDt.setPrice(proDt.getPrice());
		serPRoDt.setCount(proDt.getCount());
		serPRoDt.setQuantity(proDt.getQuantity());
		serPRoDt.setSerNo(proDt.getSerNo());
		serPRoDt.setUnit(proDt.getUnit());
		serPRoDt.setName(proDt.getName());
		serPRoDt.setProductGroupId(proDt.getProductGroupId());
		serPRoDt.setCode(proDt.getCode());
		serPRoDt.setCompanyId(proDt.getCompanyId());
		/**
		 * nidhi 27-12-2018
		 *  ||*||
		 */
		if(proDt.getPlannedQty()==0 && proDt.getQuantity()!=0){
			serPRoDt.setPlannedQty(proDt.getQuantity());
			serPRoDt.setPlannedUnit(proDt.getUnit());
		}else{
			serPRoDt.setPlannedQty(proDt.getPlannedQty());
			serPRoDt.setPlannedUnit(proDt.getPlannedUnit());
		}/**
		end
		*/
		serPRoDt.setTitle(proDt.getTitle());
		
	 return	serPRoDt;
	}
	/**
	 * ends here
	 */

	public int getBillingCount() {
		return billingCount;
	}

	public void setBillingCount(int billingCount) {
		this.billingCount = billingCount;
	}

	public String getBillingStatus() {
		return billingStatus;
	}

	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}

	public Date getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public String getContractGroup() {
		return contractGroup;
	}

	public void setContractGroup(String contractGroup) {
		this.contractGroup = contractGroup;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public Date getOldServiceDate() {
		return oldServiceDate;
	}

	public void setOldServiceDate(Date oldServiceDate) {
		this.oldServiceDate = oldServiceDate;
	}

	public int getTotalServiceDuration() {
		return totalServiceDuration;
	}

	public void setTotalServiceDuration(int totalServiceDuration) {
		this.totalServiceDuration = totalServiceDuration;
	}

	
public boolean isServiceScheduled() {
		return isServiceScheduled;
	}

	public void setServiceScheduled(boolean isServiceScheduled) {
		this.isServiceScheduled = isServiceScheduled;
	}

/** date 26.3.2019 added by komal for nbhc fumigation product code **/
	public static List<String> getFumigationProductCodes(){
		ArrayList<String> pCodeList = new ArrayList<String>();
		pCodeList.add("STK-01");
		pCodeList.add("SLM-01");
		pCodeList.add("AFC-10");
		pCodeList.add("CON-02");
		pCodeList.add("AFC-02");
		pCodeList.add("BNF-01");
		pCodeList.add("CON-01");
		/***Date 22-10-2020 by Amol for degassing service and prophyalytic service raised by Vaishnavi pawar**/
		pCodeList.add("DSSG");
		pCodeList.add("PHM-01");
		return pCodeList;
	}

public String getHours() {
	return hours;
}

public void setHours(String hours) {
	this.hours = hours;
}

public String getMinutes() {
	return minutes;
}

public void setMinutes(String minutes) {
	this.minutes = minutes;
}

public String getAmpm() {
	return ampm;
}

public void setAmpm(String ampm) {
	this.ampm = ampm;
}

public String getServiceNumber() {
	return serviceNumber;
}

public void setServiceNumber(String serviceNumber) {
	this.serviceNumber = serviceNumber;
}

public MaterialMovementNote getMmn() {
	return mmn;
}

public void setMmn(MaterialMovementNote mmn) {
	this.mmn = mmn;
}

public String getSrCopyNumber() {
	return srCopyNumber;
}

public void setSrCopyNumber(String srCopyNumber) {
	this.srCopyNumber = srCopyNumber;
}

public boolean isCompletedByApp() {
	return completedByApp;
}

public void setCompletedByApp(boolean completedByApp) {
	this.completedByApp = completedByApp;
}

public String getShade() {
	return shade;
}

public void setShade(String shade) {
	this.shade = shade;
}

public String getCompartment() {
	return compartment;
}

public void setCompartment(String compartment) {
	this.compartment = compartment;
}

public String getStackNo() {
	return stackNo;
}

public void setStackNo(String stackNo) {
	this.stackNo = stackNo;
}

public double getStackQty() {
	return stackQty;
}

public void setStackQty(double stackQty) {
	this.stackQty = stackQty;
}

public Date getDepositeDate() {
	return depositeDate;
}

public void setDepositeDate(Date depositeDate) {
	this.depositeDate = depositeDate;
}

public double getTotalPlannedMaterialCost() {
	return totalPlannedMaterialCost;
}

public void setTotalPlannedMaterialCost(double totalPlannedMaterialCost) {
	this.totalPlannedMaterialCost = totalPlannedMaterialCost;
}

public double getTotalPlannedOperatorCost() {
	return totalPlannedOperatorCost;
}

public void setTotalPlannedOperatorCost(double totalPlaneedOperatorCost) {
	this.totalPlannedOperatorCost = totalPlaneedOperatorCost;
}

public double getTotalActualMaterialCost() {
	return totalActualMaterialCost;
}

public void setTotalActualMaterialCost(double totalActualMaterialCost) {
	this.totalActualMaterialCost = totalActualMaterialCost;
}

public double getTotalActualOeratorCost() {
	return totalActualOeratorCost;
}

public void setTotalActualOeratorCost(double totalActualOeratorCost) {
	this.totalActualOeratorCost = totalActualOeratorCost;
}

public boolean isMaterialAdded() {
	return isMaterialAdded;
}

public void setMaterialAdded(boolean isMaterialAdded) {
	this.isMaterialAdded = isMaterialAdded;
}
		
@GwtIncompatible
public void updateServiceProject(Service service , ServiceProject project){
	Logger logger = Logger.getLogger("service logger");
	   logger.log(Level.SEVERE , "project");
			TechnicianWareHouseDetailsList technicianDetails = ofy().load().type(TechnicianWareHouseDetailsList.class)
					.filter("companyId", this.getCompanyId()).filter("wareHouseList.technicianName", this.getEmployee()).first().now();
			String warehouseName = "" ,storageLocation = "", storageBin = "",
					parentWareHouse = "" , parentStorageLocation = "" , parentStorageBin = "";
			Employee emp = ofy().load().type(Employee.class).filter("companyId", companyId).filter("fullname", this.getEmployee()).first().now();
			ArrayList<EmployeeInfo> techlist = new ArrayList<EmployeeInfo>();
			if(project.getTechnicians() != null){
				techlist.addAll(project.getTechnicians());
			}
			if(emp!=null){
				EmployeeInfo empInfoEntity = new EmployeeInfo();
		  		empInfoEntity.setEmpCount(emp.getCount());
		  		empInfoEntity.setFullName(emp.getFullName());
		      	empInfoEntity.setCellNumber(emp.getCellNumber1());
		      	empInfoEntity.setDesignation(emp.getDesignation());
		      	empInfoEntity.setDepartment(emp.getDepartMent());
		      	empInfoEntity.setEmployeeType(emp.getEmployeeType());
		      	empInfoEntity.setEmployeerole(emp.getRoleName());
		      	empInfoEntity.setBranch(emp.getBranchName());
		      	empInfoEntity.setCountry(emp.getCountry());
		      	if(techlist != null && techlist.size() > 0){
		      		techlist.set(0 ,empInfoEntity);
		      	}else{
		      		techlist.add(empInfoEntity);
		      	}
		      
		  		}
			if(project.getTechnicians() != null){
				project.setTechnicians(techlist);
			}
			
			
			if(technicianDetails != null){
				
				for(TechnicianWarehouseDetails details : technicianDetails.getWareHouseList()){
					if(details.getTechnicianName().equalsIgnoreCase(this.getEmployee())){
						warehouseName = details.getTechnicianWareHouse();
						storageLocation = details.getTechnicianStorageLocation();				
						storageBin = details.getTechnicianStorageBin();				
						parentWareHouse = details.getParentWareHouse();
						parentStorageLocation = details.getParentStorageLocation();						
						parentStorageBin = details.getPearentStorageBin();
						if(project != null){
							if(project.getProdDetailsList() != null){
								for(ProductGroupList group : project.getProdDetailsList()){
									group.setParentWarentwarehouse(parentWareHouse);
									group.setParentStorageLocation(parentStorageLocation);
									group.setParentStorageBin(parentStorageBin);
									group.setWarehouse(warehouseName);
									group.setStorageLocation(storageLocation);
									group.setStorageBin(storageBin);
							   }
							}
						ofy().save().entity(project);
						}
						break;
					}
				}
			
			
		}	
	}

public boolean isMINUploaded() {
	return isMINUploaded;
}

public void setMINUploaded(boolean isMINUploaded) {
	this.isMINUploaded = isMINUploaded;
}

public boolean isWmsServiceFlag() {
	return wmsServiceFlag;
}

public void setWmsServiceFlag(boolean wmsServiceFlag) {
	this.wmsServiceFlag = wmsServiceFlag;
}


public String getFumigationStatus() {
	return fumigationStatus;
}

public void setFumigationStatus(String fumigationStatus) {
	this.fumigationStatus = fumigationStatus;
}

public Date getFumigationStatusDate() {
	return fumigationStatusDate;
}

public void setFumigationStatusDate(Date fumigationStatusDate) {
	this.fumigationStatusDate = fumigationStatusDate;
}

public int getProphylacticServiceId() {
	return prophylacticServiceId;
}

public void setProphylacticServiceId(int prophylacticServiceId) {
	this.prophylacticServiceId = prophylacticServiceId;
}

public int getDeggassingServiceId() {
	return deggassingServiceId;
}

public void setDeggassingServiceId(int deggassingServiceId) {
	this.deggassingServiceId = deggassingServiceId;
}

public String getBranchEmail() {
	return branchEmail;
}

public void setBranchEmail(String branchEmail) {
	this.branchEmail = branchEmail;
}

public String getStartedDate_time() {
	return startedDate_time;
}

public void setStartedDate_time(String startedDate_time) {
	this.startedDate_time = startedDate_time;
}

public String getStartedLatitude() {
	return startedLatitude;
}

public void setStartedLatitude(String startedLatitude) {
	this.startedLatitude = startedLatitude;
}

public String getStartedLongitude() {
	return startedLongitude;
}

public void setStartedLongitude(String startedLongitude) {
	this.startedLongitude = startedLongitude;
}

public String getStartedLocationName() {
	return startedLocationName;
}

public void setStartedLocationName(String startedLocationName) {
	this.startedLocationName = startedLocationName;
}

public String getStartedTime() {
	return startedTime;
}

public void setStartedTime(String startedTime) {
	this.startedTime = startedTime;
}

public String getReportedDate_time() {
	return reportedDate_time;
}

public void setReportedDate_time(String reportedDate_time) {
	this.reportedDate_time = reportedDate_time;
}

public String getReportedLatitude() {
	return reportedLatitude;
}

public void setReportedLatitude(String reportedLatitude) {
	this.reportedLatitude = reportedLatitude;
}

public String getReportedLongitude() {
	return reportedLongitude;
}

public void setReportedLongitude(String reportedLongitude) {
	this.reportedLongitude = reportedLongitude;
}

public String getReportedLocationName() {
	return reportedLocationName;
}

public void setReportedLocationName(String reportedLocationName) {
	this.reportedLocationName = reportedLocationName;
}

public String getReportedTime() {
	return reportedTime;
}

public void setReportedTime(String reportedTime) {
	this.reportedTime = reportedTime;
}

public String gettCompletedDate_time() {
	return tCompletedDate_time;
}

public void settCompletedDate_time(String tCompletedDate_time) {
	this.tCompletedDate_time = tCompletedDate_time;
}

public String gettCompletedLatitude() {
	return tCompletedLatitude;
}

public void settCompletedLatitude(String tCompletedLatitude) {
	this.tCompletedLatitude = tCompletedLatitude;
}

public String gettCompletedLongitude() {
	return tCompletedLongitude;
}

public void settCompletedLongitude(String tCompletedLongitude) {
	this.tCompletedLongitude = tCompletedLongitude;
}

public String gettCompletedLocationName() {
	return tCompletedLocationName;
}

public void settCompletedLocationName(String tCompletedLocationName) {
	this.tCompletedLocationName = tCompletedLocationName;
}

public String gettCompletedTime() {
	return tCompletedTime;
}

public void settCompletedTime(String tCompletedTime) {
	this.tCompletedTime = tCompletedTime;
}

public String getCompletedDate_time() {
	return completedDate_time;
}

public void setCompletedDate_time(String completedDate_time) {
	this.completedDate_time = completedDate_time;
}

public String getCompletedLatitude() {
	return completedLatitude;
}

public void setCompletedLatitude(String completedLatitude) {
	this.completedLatitude = completedLatitude;
}

public String getCompletedLongitude() {
	return completedLongitude;
}

public void setCompletedLongitude(String completedLongitude) {
	this.completedLongitude = completedLongitude;
}

public String getCompletedLocationName() {
	return completedLocationName;
}

public void setCompletedLocationName(String completedLocationName) {
	this.completedLocationName = completedLocationName;
}

public String getCompletedTime() {
	return completedTime;
}

public void setCompletedTime(String completedTime) {
	this.completedTime = completedTime;
}

public int getFumigationServiceId() {
	return fumigationServiceId;
}

public void setFumigationServiceId(int fumigationServiceId) {
	this.fumigationServiceId = fumigationServiceId;
}

public String getDegassingServiceStatus() {
	return degassingServiceStatus;
}

public void setDegassingServiceStatus(String degassingServiceStatus) {
	this.degassingServiceStatus = degassingServiceStatus;
}

public String getCommodityName() {
	return commodityName;
}

public void setCommodityName(String commodityName) {
	this.commodityName = commodityName;
}


public String getActionBy() {
	return actionBy;
}

public void setActionBy(String actionBy) {
	this.actionBy = actionBy;
}

public String getActionRemark() {
	return actionRemark;
}

public void setActionRemark(String actionRemark) {
	this.actionRemark = actionRemark;
}

public Date getCioRescheduleDate() {
	return cioRescheduleDate;
}

public void setCioRescheduleDate(Date cioRescheduleDate) {
	this.cioRescheduleDate = cioRescheduleDate;
}

public String getClusterName() {
	return clusterName;
}

public void setClusterName(String clusterName) {
	this.clusterName = clusterName;
}

public boolean isSrCopyStatus() {
	return srCopyStatus;
}

public void setSrCopyStatus(boolean srCopyStatus) {
	this.srCopyStatus = srCopyStatus;
}

public int getAssetId() {
	return assetId;
}

public void setAssetId(int assetId) {
	this.assetId = assetId;
}

public String getComponentName() {
	return componentName;
}

public void setComponentName(String componentName) {
	this.componentName = componentName;
}

public String getMfgNo() {
	return mfgNo;
}

public void setMfgNo(String mfgNo) {
	this.mfgNo = mfgNo;
}

public Date getMfgDate() {
	return mfgDate;
}

public void setMfgDate(Date mfgDate) {
	this.mfgDate = mfgDate;
}

public Date getReplacementDate() {
	return replacementDate;
}

public void setReplacementDate(Date replacementDate) {
	this.replacementDate = replacementDate;
}

public String getTierName() {
	return tierName;
}

public void setTierName(String tierName) {
	this.tierName = tierName;
}

public String getAssetUnit() {
	return assetUnit;
}

public void setAssetUnit(String assetUnit) {
	this.assetUnit = assetUnit;
}

public DocumentUpload getServiceSummaryReport2() {
	return serviceSummaryReport2;
}

public void setServiceSummaryReport2(DocumentUpload serviceSummaryReport2) {
	this.serviceSummaryReport2 = serviceSummaryReport2;
}

public ArrayList<CatchTraps> getCheckList() {
	return checkList;
}

public void setCheckList(List<CatchTraps> checkList) {
	ArrayList<CatchTraps> list=new ArrayList<CatchTraps>();
	if(checkList.size()!=0){
		list.addAll(checkList);
	}
	this.checkList = list;
}

public ArrayList<PestTreatmentDetails> getPestTreatmentList() {
	return pestTreatmentList;
}

public void setPestTreatmentList(List<PestTreatmentDetails> pestTreatmentList) {
	ArrayList<PestTreatmentDetails> list=new ArrayList<PestTreatmentDetails>();
	if(pestTreatmentList.size()!=0){
		list.addAll(pestTreatmentList);
	}
	this.pestTreatmentList = list;
}



public double getServiceDuration() {
	return serviceDuration;
}

public void setServiceDuration(double serviceDuration) {
	this.serviceDuration = serviceDuration;
}


//public ArrayList<CheckList> getChecklist() {
//	return checklist;
//}
//
//public void setChecklist(ArrayList<CheckList> checklist) {
//	this.checklist = checklist;
//}
//
//public String getChecklistRemark() {
//	return checklistRemark;
//}
//
//public void setChecklistRemark(String checklistRemark) {
//	this.checklistRemark = checklistRemark;
//}




/**
 * @author Vijay Chougule Date 19-06-2020
 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
 */

@OnSave
@GwtIncompatible
public void setTimeMidOfTheDayToDate()
{
	Logger logger = Logger.getLogger("service logger");
	   
	DateUtility dateUtility = new DateUtility();
	if(this.getServiceDate()!=null){
		this.setServiceDate(dateUtility.setTimeMidOftheDayToDate(this.getServiceDate()));
	}
	if(this.id==null){
		if(this.getOldServiceDate()!=null){
			this.setOldServiceDate(dateUtility.setTimeMidOftheDayToDate(this.getOldServiceDate()));
		}
	}
	if(this.getMfgDate()!=null){
		this.setMfgDate(dateUtility.setTimeMidOftheDayToDate(this.getMfgDate()));
	}
	if(this.getReplacementDate()!=null){
		this.setReplacementDate(dateUtility.setTimeMidOftheDayToDate(this.getReplacementDate()));
	}
	if(this.getCancellationDate()!=null){
		this.setCancellationDate(dateUtility.setTimeMidOftheDayToDate(this.getCancellationDate()));
	}

}
/**
 * ends here	
 */

	public Date getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(Date startedDate) {
		this.startedDate = startedDate;
	}

	public Date getReportedDate() {
		return reportedDate;
	}

	public void setReportedDate(Date reportedDate) {
		this.reportedDate = reportedDate;
	}

	public Date getTcompletedDate() {
		return tcompletedDate;
	}

	public void setTcompletedDate(Date tcompletedDate) {
		this.tcompletedDate = tcompletedDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public boolean isScheduledByClient() {
		return scheduledByClient;
	}

	public void setScheduledByClient(boolean scheduledByClient) {
		this.scheduledByClient = scheduledByClient;
	}

	public String getServiceLocation() {
		return serviceLocation;
	}

	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}


	public boolean isOnsiteTechnician() {
		return onsiteTechnician;
	}

	public void setOnsiteTechnician(boolean onsiteTechnician) {
		this.onsiteTechnician = onsiteTechnician;
	}

	public String getServiceLocationArea() {
		return serviceLocationArea;
	}

	public void setServiceLocationArea(String serviceLocationArea) {
		this.serviceLocationArea = serviceLocationArea;
	}

	public DocumentUpload getServiceImage5() {
		return serviceImage5;
	}

	public void setServiceImage5(DocumentUpload serviceImage5) {
		this.serviceImage5 = serviceImage5;
	}

	public DocumentUpload getServiceImage6() {
		return serviceImage6;
	}

	public void setServiceImage6(DocumentUpload serviceImage6) {
		this.serviceImage6 = serviceImage6;
	}

	public DocumentUpload getServiceImage7() {
		return serviceImage7;
	}

	public void setServiceImage7(DocumentUpload serviceImage7) {
		this.serviceImage7 = serviceImage7;
	}

	public DocumentUpload getServiceImage8() {
		return serviceImage8;
	}

	public void setServiceImage8(DocumentUpload serviceImage8) {
		this.serviceImage8 = serviceImage8;
	}

	public DocumentUpload getServiceImage9() {
		return serviceImage9;
	}

	public void setServiceImage9(DocumentUpload serviceImage9) {
		this.serviceImage9 = serviceImage9;
	}

	public DocumentUpload getServiceImage10() {
		return serviceImage10;
	}

	public void setServiceImage10(DocumentUpload serviceImage10) {
		this.serviceImage10 = serviceImage10;
	}

	public double getKmTravelled() {
		return kmTravelled;
	}

	public void setKmTravelled(double kmTravelled) {
		this.kmTravelled = kmTravelled;
	}

	public double getMaterialCost() {
		return materialCost;
	}

	public void setMaterialCost(double materialCost) {
		this.materialCost = materialCost;
	}
	
	public double getExpenseCost() {
		return expenseCost;
	}

	public void setExpenseCost(double expenseCost) {
		this.expenseCost = expenseCost;
	}

	public double getLaborCost() {
		return laborCost;
	}

	public void setLaborCost(double laborCost) {
		this.laborCost = laborCost;
	}

	public String getServiceCompletionOTP() {
		return serviceCompletionOTP;
	}

	public void setServiceCompletionOTP(String serviceCompletionOTP) {
		this.serviceCompletionOTP = serviceCompletionOTP;
	}

	public String getProductFrequency() {
		return productFrequency;
	}

	public void setProductFrequency(String productFrequency) {
		this.productFrequency = productFrequency;
	}
	
	public String getCertificateValidTillDate() {
		return certificateValidTillDate;
	}

	public void setCertificateValidTillDate(String certificateValidTillDate) {
		this.certificateValidTillDate = certificateValidTillDate;
	}

	public boolean isDocketUploadedByApp() {
		return docketUploadedByApp;
	}

	public void setDocketUploadedByApp(boolean docketUploadedByApp) {
		this.docketUploadedByApp = docketUploadedByApp;
	}
	
}
