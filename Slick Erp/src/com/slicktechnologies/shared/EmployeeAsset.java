package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.inventory.InventoryTransactionLineItem;
import com.slicktechnologies.shared.common.inventory.UpdateStock;

@Entity
public class EmployeeAsset extends SuperModel implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7107785248620285130L;
	/**
	 * 
	 */
	@Index
	int empId;
	@Index
	String empName;
	@Index
	long empCellNo;
	ArrayList<EmployeeAssetBean> empAssetList;
	
	/**
	 * @author Anil , Date : 10-07-2019
	 */
	@Index
	String branch;
	
	String remark;
	
	ArrayList<EmployeeAssetBean> empAssetHisList;
	String transactionType;

	public EmployeeAsset(){
		empName ="";
		empAssetList = new ArrayList<EmployeeAssetBean>();
	}
	
	

	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public String getBranch() {
		return branch;
	}



	public void setBranch(String branch) {
		this.branch = branch;
	}



	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public long getEmpCellNo() {
		return empCellNo;
	}

	public void setEmpCellNo(long empCellNo) {
		this.empCellNo = empCellNo;
	}

	public ArrayList<EmployeeAssetBean> getEmpAssetList() {
		return empAssetList;
	}

	public void setEmpAssetList(List<EmployeeAssetBean> empAssetList) {
		ArrayList<EmployeeAssetBean> list = new ArrayList<EmployeeAssetBean>();
		if(empAssetList !=null){
			list.addAll(empAssetList);
			this.empAssetList = list;
		}
		
	}
	
	public ArrayList<EmployeeAssetBean> getEmpAssetHisList() {
		return empAssetHisList;
	}

	public void setEmpAssetHisList(List<EmployeeAssetBean> empAssetHisList) {
		ArrayList<EmployeeAssetBean> list = new ArrayList<EmployeeAssetBean>();
		if(empAssetHisList !=null){
			list.addAll(empAssetHisList);
			this.empAssetHisList = list;
		}
		
	}
	
	

	public String getTransactionType() {
		return transactionType;
	}



	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}



	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@OnSave
	@GwtIncompatible
	public void updateEmployeeAssetStock(){
		System.out.println("updateEmployeeAssetStock");
//		if(id!=null){
			System.out.println("updateEmployeeAssetStock "+transactionType );
			if(transactionType!=null&&!transactionType.equals("")){
				ArrayList<InventoryTransactionLineItem> itemList=getItemTransactionList();
				if(transactionType.equals("Issue")||transactionType.equals("Return")){
					System.out.println("Condition true.."+itemList.size());
					if(itemList.size()!=0){
						UpdateStock.setProductInventory(itemList);
					}
					setTransactionType("");
				}else if(transactionType.equals("Scrap")){
					System.out.println("Condition true.."+itemList.size());
					if(itemList.size()!=0){
						for(InventoryTransactionLineItem item:itemList){
							UpdateStock.setProductInventoryTransaction(item.getProdId(), item.getProductName(), item.getUpdateQty(), item.getProductUOM(), item.getProductPrice(), item.getWarehouseName(), item.getStorageLocation(), item.getStorageBin(), "Employee Asset Allocation", empId, empName+ " Scrap", new Date(), AppConstants.SUBTRACT, 0, item.getUpdateQty(), this.getCompanyId(), branch,"");
						}
					}
					setTransactionType("");
				}
			}
//		}
	}
	
	public ArrayList<InventoryTransactionLineItem> getItemTransactionList(){
		ArrayList<InventoryTransactionLineItem> itemList=new ArrayList<InventoryTransactionLineItem>();
		for(EmployeeAssetBean empAssBean:empAssetHisList){
			if(empAssBean.isStockUpdated()==false){
				InventoryTransactionLineItem item=new InventoryTransactionLineItem();
				item.setCompanyId(this.getCompanyId());
				item.setBranch(branch);
				item.setDocumentType("Employee Asset Allocation");
				item.setDocumentId(empId);
				item.setDocumentDate(new Date());
				item.setDocumnetTitle(empName);
				item.setRemark(remark);
				
				if(transactionType.equals("Issue")){
					item.setUpdateQty(empAssBean.getQty());
					item.setOperation(AppConstants.SUBTRACT);
					item.setDocumentSubType("Issue");
				}else if(transactionType.equals("Return")){
					item.setUpdateQty(empAssBean.getDeductQty());
					item.setOperation(AppConstants.ADD);
					item.setDocumentSubType("Return");
				}else if(transactionType.equals("Scrap")){
					item.setUpdateQty(empAssBean.getDeductQty());
					item.setOperation(AppConstants.SUBTRACT);
					item.setDocumentSubType("Scrap");
				}
				
				item.setWarehouseName(empAssBean.getWarehouseName());
				item.setStorageLocation(empAssBean.getStorageLocName());
				item.setStorageBin(empAssBean.getStorageLocBin());
				
				item.setProdId(empAssBean.getAssetId());
				item.setProductName(empAssBean.getAssetName());
				item.setProductCode(empAssBean.getAssetCode());
				item.setProductUOM(empAssBean.getUom());
				itemList.add(item);
				
				empAssBean.setStockUpdated(true);
			}
		}
		return itemList;
	}

}
