package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.annotation.Embed;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.ProductInventoryView;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

@Embed
public class MINUploadExcelDetails implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 32915440371561295L;

	public ArrayList<String> minList;
	public ArrayList<String> errorList;
	HashSet<Integer> serCount = new HashSet<Integer>();
	HashSet<Integer> emplcount;
	HashSet<String> branchList;
	HashSet<String> wareHouse;
	HashSet<Integer> prodCount;
	HashSet<Integer> contCount;
	HashSet<Integer> CustSerList;
	ArrayList<Service> serviceDetailList = new ArrayList<Service>();
	ArrayList<Employee> empInfoList = new ArrayList<Employee>();
	ArrayList<Branch> branchDtList = new ArrayList<Branch>();
	ArrayList<ProductInventoryView> prodInvDetails = new ArrayList<ProductInventoryView>();
	ArrayList<String> exceldatalist = new ArrayList<String>();
	ArrayList<ItemProduct> prodDetails = new ArrayList<ItemProduct>();
	ArrayList<ServiceProject> serProject = new ArrayList<ServiceProject>();
	ArrayList<MaterialIssueNote> minIssnote = new ArrayList<MaterialIssueNote>();

	Map<String, Double> productInDetails = new HashMap<String, Double>();
	Map<String, Double> productInErrorList = new HashMap<String, Double>();
	ArrayList<ProductInventoryViewDetails> productInvViewDt;
	Long companyId;
	String loginName;
	ArrayList<NumberGeneration> numGenList;
	ArrayList<MINUploadDetails> minDetails = new ArrayList<MINUploadDetails>();
	/**
	 * nidhi
	 * 26-12-2018
	 *  ||*|| for make bill of material for old service
	 */
	ArrayList<BillOfMaterial> prodBillOfMetrialList = new ArrayList<BillOfMaterial>();
	public MINUploadExcelDetails (){
		minList =  new ArrayList<String>();
		errorList = new ArrayList<String>();
		emplcount =  new HashSet<Integer>();
		wareHouse = new HashSet<String>();
		prodCount = new HashSet<Integer>();
		contCount = new HashSet<Integer>();

		CustSerList = new HashSet<Integer>();
//		minList.addAll(exceldataDetailslist);
		
//		logger.log(Level.SEVERE," saveMinUploadProcess List " + minList.size());
		serviceDetailList = new ArrayList<Service>();
		 empInfoList = new ArrayList<Employee>();
		
		prodInvDetails = new ArrayList<ProductInventoryView>();
		
		 prodDetails = new ArrayList<ItemProduct>();
		 serProject = new ArrayList<ServiceProject>();
		 minIssnote = new ArrayList<MaterialIssueNote>();
		 productInvViewDt = new ArrayList<ProductInventoryViewDetails>();
		 productInDetails = new HashMap<String,Double>();
		 productInErrorList = new HashMap<String,Double>();
		 CustSerList = new HashSet<Integer>();
		 numGenList = new ArrayList<NumberGeneration>();
	}
	
	public ArrayList<String> getMinList() {
		return minList;
	}
	public void setMinList(ArrayList<String> minList) {
		this.minList = minList;
		if(minList!=null){
			this.minList=new ArrayList<String>();
			this.minList.addAll(minList);
//			System.out.println("Size of errorList"+this.minList.size());
		}
	}
	public ArrayList<String> getErrorList() {
		return errorList;
	}
	public void setErrorList(ArrayList<String> errorList) {
		
//		this.errorList = errorList;
		if(errorList!=null){
			this.errorList=new ArrayList<String>();
			this.errorList.addAll(errorList);
//			System.out.println("Size of errorList"+this.errorList.size());
		}
	}
	public HashSet<Integer> getSerCount() {
		return serCount;
	}
	public void setSerCount(HashSet<Integer> serCount) {
//		this.serCount = serCount;
		if(serCount!=null){
			this.serCount=new HashSet<Integer>();
			this.serCount.addAll(serCount);
//			System.out.println("Size of errorList"+this.errorList.size());
		}
		
	}
	public HashSet<Integer> getEmplcount() {
		return emplcount;
	}
	public void setEmplcount(HashSet<Integer> emplcount) {
//		this.emplcount = emplcount;
		if(emplcount!=null){
			this.emplcount=new HashSet<Integer>();
			this.emplcount.addAll(emplcount);
//			System.out.println("Size of errorList"+this.errorList.size());
		}
	}
	public HashSet<String> getBranchList() {
		return branchList;
	}
	public void setBranchList(HashSet<String> branchList) {
		if(branchList!=null){
			this.branchList=new HashSet<String>();
			this.branchList.addAll(branchList);
		}
	}
	public HashSet<String> getWareHouse() {
		return wareHouse;
	}
	public void setWareHouse(HashSet<String> wareHouse) {
		if(wareHouse!=null){
			this.wareHouse=new HashSet<String>();
			this.wareHouse.addAll(wareHouse);
		}
	}
	public HashSet<Integer> getProdCount() {
		return prodCount;
	}
	public void setProdCount(HashSet<Integer> prodCount) {
		if(prodCount!=null){
			this.prodCount=new HashSet<Integer>();
			this.prodCount.addAll(prodCount);
		}
	}
	public HashSet<Integer> getContCount() {
		return contCount;
	}
	public void setContCount(HashSet<Integer> contCount) {
		if(contCount!=null){
			this.contCount=new HashSet<Integer>();
			this.contCount.addAll(contCount);
		}
	}
	public HashSet<Integer> getCustSerList() {
		return CustSerList;
	}
	public void setCustSerList(HashSet<Integer> custSerList) {
//		CustSerList = custSerList;
		if(CustSerList!=null){
			this.CustSerList=new HashSet<Integer>();
			this.CustSerList.addAll(CustSerList);
//			System.out.println("Size of serviceDetailList"+this.serviceDetailList.size());
		}
	}
	public ArrayList<Service> getServiceDetailList() {
		return serviceDetailList;
	}
	public void setServiceDetailList(ArrayList<Service> serviceDetailList) {
		if(serviceDetailList!=null){
			this.serviceDetailList=new ArrayList<Service>();
			this.serviceDetailList.addAll(serviceDetailList);
//			System.out.println("Size of serviceDetailList"+this.serviceDetailList.size());
		}
	}
	public ArrayList<Employee> getEmpInfoList() {
		return empInfoList;
	}
	public void setEmpInfoList(ArrayList<Employee> empInfoList) {
		if(empInfoList!=null){
			this.empInfoList=new ArrayList<Employee>();
			this.empInfoList.addAll(empInfoList);
//			System.out.println("Size of empInfoList"+this.serviceDetailList.size());
		}
	}
	public ArrayList<Branch> getBranchDtList() {
		return branchDtList;
	}
	public void setBranchDtList(ArrayList<Branch> branchDtList) {
//		this.branchDtList = branchDtList;
		if(branchDtList!=null){
			this.branchDtList=new ArrayList<Branch>();
			this.branchDtList.addAll(branchDtList);
//			System.out.println("Size of empInfoList"+this.serviceDetailList.size());
		}
	}
	public ArrayList<ProductInventoryView> getProdInvDetails() {
		return prodInvDetails;
	}
	public void setProdInvDetails(ArrayList<ProductInventoryView> prodInvDetails) {
//		this.prodInvDetails = prodInvDetails;
		if(prodInvDetails!=null){
			this.prodInvDetails=new ArrayList<ProductInventoryView>();
			this.prodInvDetails.addAll(prodInvDetails);
//			System.out.println("Size of prodInvDetails"+this.prodInvDetails.size());
		}
	}
	public ArrayList<ItemProduct> getProdDetails() {
		return prodDetails;
	}
	public void setProdDetails(ArrayList<ItemProduct> prodDetails) {
//		this.prodDetails = prodDetails;
		if(prodDetails!=null){
			this.prodDetails=new ArrayList<ItemProduct>();
			this.prodDetails.addAll(prodDetails);
//			System.out.println("Size of prodInvDetails"+this.prodInvDetails.size());
		}
	}
	public ArrayList<ServiceProject> getSerProject() {
		return serProject;
	}
	public void setSerProject(ArrayList<ServiceProject> serProject) {
		this.serProject = serProject;
	}
	public ArrayList<MaterialIssueNote> getMinIssnote() {
		return minIssnote;
	}
	public void setMinIssnote(ArrayList<MaterialIssueNote> minIssnote) {
		this.minIssnote = minIssnote;
	}
	public Map<String, Double> getProductInDetails() {
		return productInDetails;
	}
	public void setProductInDetails(Map<String, Double> productInDetails) {
		this.productInDetails = productInDetails;
	}
	public Map<String, Double> getProductInErrorList() {
		return productInErrorList;
	}
	public void setProductInErrorList(Map<String, Double> productInErrorList) {
		this.productInErrorList = productInErrorList;
	}
	public ArrayList<MINUploadDetails> getMinDetails() {
		return minDetails;
	}
	public void setMinDetails(ArrayList<MINUploadDetails> minDetails) {
//		this.minDetails = minDetails;
		if(minDetails!=null){
			this.minDetails=new ArrayList<MINUploadDetails>();
			this.minDetails.addAll(minDetails);
//			System.out.println("Size of Payment"+this.minDetails.size());
		}
	}
	public ArrayList<String> getExceldatalist() {
		return exceldatalist;
	}
	public void setExceldatalist(ArrayList<String> exceldatalist) {
		if(exceldatalist!=null){
			this.exceldatalist=new ArrayList<String>();
			this.exceldatalist.addAll(exceldatalist);
//			System.out.println("Size of Payment"+this.exceldatalist.size());
		}
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public ArrayList<ProductInventoryViewDetails> getProductInvViewDt() {
		return productInvViewDt;
	}

	public void setProductInvViewDt(ArrayList<ProductInventoryViewDetails> productInvViewDt) {
		if(productInvViewDt!=null){
			this.productInvViewDt=new ArrayList<ProductInventoryViewDetails>();
			this.productInvViewDt.addAll(productInvViewDt);
//			System.out.println("Size of Payment"+this.exceldatalist.size());
		}
	}

	public ArrayList<NumberGeneration> getNumGenList() {
		return numGenList;
	}

	public void setNumGenList(ArrayList<NumberGeneration> numGenList) {
		if(numGenList!=null){
			this.numGenList=new ArrayList<NumberGeneration>();
			this.numGenList.addAll(numGenList);
		}
	}

	/**
	 * nidhi ||*||
	 * @return
	 */
	public ArrayList<BillOfMaterial> getProdBillOfMetrialList() {
		return prodBillOfMetrialList;
	}

	public void setProdBillOfMetrialList(
			ArrayList<BillOfMaterial> prodBillOfMetrialList) {
		this.prodBillOfMetrialList = prodBillOfMetrialList;
	}
	
}
