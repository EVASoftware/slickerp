package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Embed
public class CNCArrearsDetails extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6908607976300411222L;
	
	Date fromDate;
	Date toDate;
	String version;
	
	public CNCArrearsDetails(){
		fromDate = null;
		toDate = null;
	}

	
	public Date getFromDate() {
		return fromDate;
	}



	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}



	public Date getToDate() {
		return toDate;
	}



	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}



	public String getVesion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
