package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ObservationImpactRecommendation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6004349548117323731L;

	
	String observationName;
	String impact;
	String action;
	String recommendation;
	String newentry;
	
	public ObservationImpactRecommendation(){
		observationName="";
		impact="";
		action="";
		recommendation="";
		newentry = "";
	}

	public String getObservationName() {
		return observationName;
	}

	public void setObservationName(String observationName) {
		this.observationName = observationName;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getNewentry() {
		return newentry;
	}

	public void setNewentry(String newentry) {
		this.newentry = newentry;
	}
	
	
	
	
	
	
}
