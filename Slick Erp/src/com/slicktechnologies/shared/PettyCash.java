package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class PettyCash.
 */
@Entity
public class PettyCash extends ConcreteBusinessProcess {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3463305582651264082L;
	
	/** The petty cash name. */
	@Index
	protected String pettyCashName;
	
	/** The petty cash status. */
	@Index
	protected boolean pettyCashStatus;
	
	ArrayList<ApproverAmount>amountApproved;
	
//	protected int pettyCashBalance;
	
	//Date 08-08-2017 changed double by vijay
	protected double pettyCashBalance;
	
		/**
	 * Instantiates a new petty cash.
	 */
	public PettyCash() {
		super();
		pettyCashName="";
		amountApproved = new ArrayList<ApproverAmount>();
	}

	public ArrayList<ApproverAmount> getAmountApproved() {
		return amountApproved;
	}

	public void setAmountApproved(ArrayList<ApproverAmount> amountApproved) {
		if(amountApproved!=null)
		this.amountApproved = amountApproved;
	}

	/**
	 * Instantiates a new petty cash.
	 *
	 * @param status the status
	 * @param employee the employee
	 * @param branch the branch
	 * @param creationDate the creation date
	 */
	public PettyCash(String status, String employee, String branch,
			Date creationDate) {
		super(status, employee, branch, creationDate);
	}

	
	
	/**
	 * Gets the petty cash name.
	 *
	 * @return the petty cash name
	 */
	public String getPettyCashName() {
		return pettyCashName;
	}

	/**
	 * Sets the petty cash name.
	 *
	 * @param pettyCashName the new petty cash name
	 */
	public void setPettyCashName(String pettyCashName) {
		if(pettyCashName!=null)
		this.pettyCashName = pettyCashName.trim();
	}

	/**
	 * Checks if is petty cash status.
	 *
	 * @return true, if is petty cash status
	 */
	public boolean isPettyCashStatus() {
		return pettyCashStatus;
	}

	/**
	 * Sets the petty cash status.
	 *
	 * @param pettyCashStatus the new petty cash status
	 */
	public void setPettyCashStatus(boolean pettyCashStatus) {
		this.pettyCashStatus = pettyCashStatus;
	}
	
	

//	public Integer getPettyCashBalance() {
//		return pettyCashBalance;
//	}
//
//	public void setPettyCashBalance(int pettyCashBalance) {
//		this.pettyCashBalance = pettyCashBalance;
//	}
	
	public Double getPettyCashBalance() {
		return pettyCashBalance;
	}

	public void setPettyCashBalance(double pettyCashBalance) {
		this.pettyCashBalance = pettyCashBalance;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess#compareTo(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel arg0) {
		if(arg0 instanceof PettyCash)
		{
			PettyCash petty=(PettyCash) arg0;
			if(petty.getPettyCashName()!=null)
			{
				return this.pettyCashName.compareTo(petty.pettyCashName);
			}
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess#isDuplicate(com.simplesoftwares.client.library.appstructure.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		PettyCash entity = (PettyCash) m;
		String name = entity.getPettyCashName().trim();
		String curname=this.pettyCashName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
		name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}


	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return pettyCashName.toString();
	}

	@Embed
	public static class ApproverAmount implements Serializable
	{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -3994033837137663111L;
		public String employeeName;
		public int amount;
		
		
		
		public Integer getAmount() {
			return amount;
		}
		public void setAmount(int amount) {
			this.amount = amount;
		}
		public String getEmployeeName() {
			return employeeName;
		}
		public void setEmployeeName(String employeeName) {
			if(employeeName!=null)
			this.employeeName = employeeName.trim();
		}
		
	}
	
	
	
	
	

}
