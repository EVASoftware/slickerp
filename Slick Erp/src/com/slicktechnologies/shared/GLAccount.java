package com.slicktechnologies.shared;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

@Entity
public class GLAccount extends SuperModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2146778391583932221L;
	
	final static String STATUS_QUERRY="Active";
	@Index
	protected int glAccountNo;
	@Index
	protected String glAccountName;
	protected String description;
	protected String glAccountGroup;
	@Index
	protected String status;
	/*************/
	
	public GLAccount() {
		super();
		glAccountName="";
		description="";
		glAccountGroup="";
		status="";
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "GLAccount No.")
	public Integer getGlAccountNo() {
		return glAccountNo;
	}


	public void setGlAccountNo(Integer glAccountNo) {
		this.glAccountNo = glAccountNo;
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "GLAccount Name")
	public String getGlAccountName() {
		return glAccountName;
	}


	public void setGlAccountName(String glAccountName) {
		if(glAccountName!=null)
		this.glAccountName = glAccountName.trim();
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		if(description!=null)
		this.description = description.trim();
	}

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "GLAccount Group")
	public String getGlAccountGroup() {
		return glAccountGroup;
	}


	public void setGlAccountGroup(String glAccountGroup) {
		if(glAccountGroup!=null)
		this.glAccountGroup = glAccountGroup.trim();
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		return duplicateName(m)||duplicateNo(m);
	}
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public boolean duplicateName(SuperModel m)
	{
		GLAccount entity = (GLAccount) m;
		String name = entity.getGlAccountName().trim();
		String curname=this.glAccountName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	public boolean duplicateNo(SuperModel m)
	{
		GLAccount entity = (GLAccount) m;
		String name = entity.getGlAccountNo()+"".trim();
		String curname=this.glAccountNo+"".trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	
	@Override
	public String toString() {
		return glAccountName.toString();
	}
	
	public static void initializeGLAccount(ObjectListBox<GLAccount> gla)
	{
		MyQuerry querry=new MyQuerry();
		Filter filter=new Filter();
		filter.setQuerryString("status");
		filter.setStringValue(STATUS_QUERRY);
		querry.getFilters().add(filter);
		querry.setQuerryObject(new GLAccount());
		gla.MakeLive(querry);
	}

	
	

}
