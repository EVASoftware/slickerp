package com.slicktechnologies.shared;

import com.googlecode.objectify.annotation.Entity;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;

@Entity
public class TaxDefination extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5964557558232402597L;

	
	protected String taxName;
	protected String Description;
	protected double percent;
	protected boolean fixed;
	protected double fixValue;
	protected String inclusive;
	protected String GlaccountName;
	protected String status;
	protected String parentTax;
	
	
	

	
	/*************************Getters and Setters*********************************************/
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 6, isFieldUpdater = false, isSortable = true, title = "Parent Tax")
	public String getParentTax() {
		return parentTax;
	}



	public void setParentTax(String parentTax) {
		this.parentTax = parentTax;
	}



	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 1, isFieldUpdater = false, isSortable = true, title = "Name")
	public String getTaxName() {
		return taxName;
	}



	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 7, isFieldUpdater = false, isSortable = true, title = "Description")
	public String getDescription() {
		return Description;
	}



	public void setDescription(String description) {
		Description = description;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 2, isFieldUpdater = false, isSortable = true, title = "% Tax")
	public double getPercent() {
		return percent;
	}



	public void setPercent(double percent) {
		this.percent = percent;
	}


	
	public Boolean getFixed() {
		return fixed;
	}



	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 3, isFieldUpdater = false, isSortable = true, title = "Fix Value")
	public double getFixValue() {
		return fixValue;
	}



	public void setFixValue(double fixValue) {
		this.fixValue = fixValue;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Inclusive/Exclusive")
	public String getInclusive() {
		return inclusive;
	}



	public void setInclusive(String inclusive) {
		this.inclusive = inclusive;
	}


	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "GLAccount")
	public String getGlaccountName() {
		return GlaccountName;
	}



	public void setGlaccountName(String glaccountName) {
		GlaccountName = glaccountName;
	}
	
	
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 8, isFieldUpdater = false, isSortable = true, title = "Status")
	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}





	@Override
	public boolean isDuplicate(SuperModel m) {
		TaxDefination entity = (TaxDefination) m;
		String name = entity.getTaxName().trim();
		String curname=this.taxName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
		
	


	}
	
	@Override
	public String toString() {
		return taxName.toString();
	}

	

}
