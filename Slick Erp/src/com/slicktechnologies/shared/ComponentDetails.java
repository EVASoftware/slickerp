package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.annotation.Serialize;
@Serialize
public class ComponentDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7260493504591357912L;
	
	/**
	 * @author Anil
	 * @since 04-06-2020
	 * Adding component and asset details for ups maintaince
	 * for premium tech raised by Rahul Tiwari
	 */
	int assetId;
	String componentName;
	String mfgNum;
	Date mfgDate;
	Date replacementDate;
	int durationForReplacement;
	/**
	 * @author Anil
	 * @since 08-06-2020
	 * for generating asset id
	 */
	int srNo;
	/**
	 * @author Anil
	 * @since 16-06-2020
	 * asset unit
	 */
	String assetUnit;
	
	/**
	 * @author Vijay Date 06-04-2020
	 *  for checkbox
	 */
	boolean check;
	
	public int getAssetId() {
		return assetId;
	}
	public void setAssetId(int assetId) {
		this.assetId = assetId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getMfgNum() {
		return mfgNum;
	}
	public void setMfgNum(String mfgNum) {
		this.mfgNum = mfgNum;
	}
	public Date getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(Date mfgDate) {
		this.mfgDate = mfgDate;
	}
	public Date getReplacementDate() {
		return replacementDate;
	}
	public void setReplacementDate(Date replacementDate) {
		this.replacementDate = replacementDate;
	}
	public int getDurationForReplacement() {
		return durationForReplacement;
	}
	public void setDurationForReplacement(int durationForReplacement) {
		this.durationForReplacement = durationForReplacement;
	}
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public String getAssetUnit() {
		return assetUnit;
	}
	public void setAssetUnit(String assetUnit) {
		this.assetUnit = assetUnit;
	}
	@Override
	public String toString() {
		return assetId + "";
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	
	
	

}
