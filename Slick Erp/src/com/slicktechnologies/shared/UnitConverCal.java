package com.slicktechnologies.shared;

import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;

public class UnitConverCal {
	
	protected String uom = "";
	protected double area = 00;
	protected ProductGroupDetails prodGroupDt;
	
	public UnitConverCal(){
		prodGroupDt = new ProductGroupDetails();
	}
	
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public ProductGroupDetails getProdGroupDt() {
		return prodGroupDt;
	}
	public void setProdGroupDt(ProductGroupDetails prodGroupDt) {
		this.prodGroupDt = prodGroupDt;
	}
	
	
	
	
}
