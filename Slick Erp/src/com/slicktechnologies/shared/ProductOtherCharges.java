package com.slicktechnologies.shared;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

@Embed
public class ProductOtherCharges implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1114193706743248590L;
	
	protected String chargeName;
	protected double chargePercent;
	protected double chargeAbsValue;
	protected double assessableAmount;
	protected double chargePayable;
	protected boolean flagVal;
	protected int indexCheck;
	
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		if(chargeName!=null){
			this.chargeName = chargeName.trim();
		}
	}
	
	public Double getChargePercent() {
		return chargePercent;
	}
	public void setChargePercent(double chargePercent) {
		this.chargePercent = chargePercent;
	}
	
	public Double getChargeAbsValue() {
		return chargeAbsValue;
	}
	public void setChargeAbsValue(double chargeAbsValue) {
		this.chargeAbsValue = chargeAbsValue;
	}
	public Double getAssessableAmount() {
		return assessableAmount;
	}
	public void setAssessableAmount(double assessableAmount) {
		this.assessableAmount = assessableAmount;
	}
	public double getChargePayable() {
		return chargePayable;
	}
	public void setChargePayable(double chargePayable) {
		this.chargePayable = chargePayable;
	}
	public Boolean getFlagVal() {
		return flagVal;
	}
	public void setFlagVal(Boolean flagVal) {
		this.flagVal = flagVal;
	}
	public int getIndexCheck() {
		return indexCheck;
	}
	public void setIndexCheck(int indexCheck) {
		this.indexCheck = indexCheck;
	}
	
	

	
	
	

}
