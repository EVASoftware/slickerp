package com.slicktechnologies.shared;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.labs.repackaged.com.google.common.annotations.GwtCompatible;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.ModificationHistory;
import static com.googlecode.objectify.ObjectifyService.ofy;


@Entity
public class StackMaster extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3746687949884803952L;

	@Index
	protected String warehouseName;
	@Index
	protected String warehouseCode;
	@Index
	protected String shade;
	@Index
	protected String compartment;
	@Index
	protected String stackNo;
	protected double quantity;
	protected String UOM;
	@Index
	protected boolean status;
	protected String commodityName;
	protected String clusterName;
	
	public StackMaster(){
		super();
		warehouseName="";
		warehouseCode="";
		shade="";
		compartment="";
		stackNo="";
		status = false;		
		commodityName="";
		clusterName="";
	}
	
	
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	
//	@OnSave
//	@GwtCompatible
//	private void suspendService(){
//		if(this.id!=null){
//			System.out.println("Onsave"+this.getQuantity());
//			if(this.getQuantity()==0){
////				reactonSuspendServices();
//			}
//		}
//	}
//
//	@GwtCompatible
//	private void reactonSuspendServices() {
//
//		Logger logger=Logger.getLogger("Services Creation On Approval");
//
//		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", this.getCompanyId())
//				.filter("refNo2", this.getWarehouseCode()).filter("shade", this.getShade()).list();
//		logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
////		if(servicelist.size()>0){
////			
////			for(Service serviceEntity : servicelist){
////				serviceEntity.setStatus(Service.SERVICESUSPENDED);
////				serviceEntity.setCancellationDate(new Date());
////				ModificationHistory history = new ModificationHistory();
////				history.setOldServiceDate(serviceEntity.getServiceDate());
////				serviceEntity.setServiceDate(serviceEntity.getServiceDate());
////				history.setResheduleBranch(serviceEntity.getBranch());
////				history.setReason("Stack Qty is Zero so Suspended this Automatic by System");
////				history.setSystemDate(new Date());
////				serviceEntity.getListHistory().add(history);
////			}
////						
////			ofy().save().entities(servicelist).now();
////			logger.log(Level.SEVERE, "services suspended sucessfully ");
////			
//////			ofy().delete().entity(this);
//////			logger.log(Level.SEVERE, "This stack Stock is zero so Stack Master Record Deleted Successfully");
////		}
//	}


	public String getWarehouseName() {
		return warehouseName;
	}


	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}


	public String getWarehouseCode() {
		return warehouseCode;
	}


	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}


	public String getShade() {
		return shade;
	}


	public void setShade(String shade) {
		this.shade = shade;
	}


	public String getCompartment() {
		return compartment;
	}


	public void setCompartment(String compartment) {
		this.compartment = compartment;
	}


	public String getStackNo() {
		return stackNo;
	}


	public void setStackNo(String stackNo) {
		this.stackNo = stackNo;
	}


	public double getQuantity() {
		return quantity;
	}


	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	public String getUOM() {
		return UOM;
	}


	public void setUOM(String uOM) {
		UOM = uOM;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public String getCommodityName() {
		return commodityName;
	}


	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}


	public String getClusterName() {
		return clusterName;
	}


	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	
	
	
}
