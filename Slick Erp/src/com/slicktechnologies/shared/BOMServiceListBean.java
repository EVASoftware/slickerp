package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;

@Embed
public class BOMServiceListBean extends SuperModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1764964022720932306L;
	List<Integer> serviceCompleted = new ArrayList<Integer>();
	List<Integer> servicePedding  = new ArrayList<Integer>();
	List<Service> serviceCompeted = new ArrayList<Service>();
	List<Service> servicePeddingLst = new ArrayList<Service>();
	List<Service> serviceProList = new ArrayList<Service>();
	List<MaterialIssueNote> minList = new ArrayList<MaterialIssueNote>();
	
	List<Integer> servicewithCompleted = new ArrayList<Integer>();
	List<Service> servicewithCompeted = new ArrayList<Service>();
	List<BillOfMaterial> bomList = new ArrayList<BillOfMaterial>();
	List<Integer> minSerList = new ArrayList<Integer>();
	List<MaterialMovementNote> mmnList = new ArrayList<MaterialMovementNote>();
	
	HashMap<Integer, String> mmnSerSet = new HashMap<Integer, String>();
	public BOMServiceListBean(){
		
	}
	public List<Integer> getServiceCompleted() {
		return serviceCompleted;
	}
	public void setServiceCompleted(List<Integer> serviceCompleted) {
		this.serviceCompleted = serviceCompleted;
	}
	public List<Integer> getServicePedding() {
		return servicePedding;
	}
	public void setServicePedding(List<Integer> servicePedding) {
		this.servicePedding = servicePedding;
	}
	public List<Service> getServiceCompeted() {
		return serviceCompeted;
	}
	public void setServiceCompeted(List<Service> serviceCompeted) {
		this.serviceCompeted = serviceCompeted;
	}
	public List<Service> getServicePeddingLst() {
		return servicePeddingLst;
	}
	public void setServicePeddingLst(List<Service> servicePeddingLst) {
		this.servicePeddingLst = servicePeddingLst;
	}
	public List<Service> getServiceProList() {
		return serviceProList;
	}
	public void setServiceProList(List<Service> serviceProList) {
		this.serviceProList = serviceProList;
	}
	public List<MaterialIssueNote> getMinList() {
		return minList;
	}
	public void setMinList(List<MaterialIssueNote> minList) {
		this.minList = minList;
	}
	public List<Integer> getServicewithCompleted() {
		return servicewithCompleted;
	}
	public void setServicewithCompleted(List<Integer> servicewithCompleted) {
		this.servicewithCompleted = servicewithCompleted;
	}
	public List<Service> getServicewithCompeted() {
		return servicewithCompeted;
	}
	public void setServicewithCompeted(List<Service> servicewithCompeted) {
		this.servicewithCompeted = servicewithCompeted;
	}
	public List<BillOfMaterial> getBomList() {
		return bomList;
	}
	public void setBomList(List<BillOfMaterial> bomList) {
		this.bomList = bomList;
	}
	public List<Integer> getMinSerList() {
		return minSerList;
	}
	public void setMinSerList(List<Integer> minSerList) {
		this.minSerList = minSerList;
	}
	public List<MaterialMovementNote> getMmnList() {
		return mmnList;
	}
	public void setMmnList(List<MaterialMovementNote> mmnList) {
		this.mmnList = mmnList;
	}
	public HashMap<Integer, String> getMmnSerSet() {
		return mmnSerSet;
	}
	public void setMmnSerSet(HashMap<Integer, Integer> String) {
		this.mmnSerSet = mmnSerSet;
	}
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
