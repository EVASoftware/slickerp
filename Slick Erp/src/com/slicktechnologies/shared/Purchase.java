package com.slicktechnologies.shared;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.googlecode.objectify.annotation.Serialize;
import com.simplesoftwares.client.library.helperconstants.ColumnDatatype;
import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.annatonations.SearchAnnotation;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.MyUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

/**
 * The Class PurchaseRegister Stores the Records of Purchases
 */
@Entity
public class Purchase extends ConcreteBusinessProcess
{
	/**************************************************Entity Attributes**********************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4103886278194971218L;

	/** it represents contact information of the vendor corresponding to this purchase */
	@Index 
	protected PersonInfo vendorInfo;
	
	/** The description about this purchase */
	protected String description;
	
	/** The list of purchased products. */
	@Serialize
	protected ArrayList<SalesLineItem> purchasedProducts;
	
	/** The purchase record creation date. */
	@Index 
	protected Date poDate;
	
	/** The po nr. */
	@Index 
	protected String poNr;
	
	protected Key<PersonInfo> personInfoKey;
	
	protected double poValue;
	
	
	/**************************************************Default Constructor**********************************************/
	/**
	 * Instantiates a new purchase register.
	 */
	public Purchase() 
	{	
		vendorInfo=new PersonInfo();
		purchasedProducts=new ArrayList<SalesLineItem>();
		poDate=new Date();
		description="";
		poNr="";
	}
	
	/**************************************************Getter/Setter**********************************************/
/**
 * Sets the vendor info.
 *
 * @param vendorInfo the new vendor info
 */
	public void setVendorInfo(PersonInfo vendorInfo) 
	{
		this.vendorInfo = vendorInfo;
	}

	/**
	 * Gets the description.
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	/**
	 * Sets the description.
	 * @param description the new description
	 */
	public void setDescription(String description) 
	{
		if(description!=null)
			this.description = description.trim();
	}
	/**
	 * Gets the purchased products.
	 * @return the purchased products
	 */
	public List<SalesLineItem> getPurchasedProducts() 
	{
		return purchasedProducts;
	}
	/**
	 * 

	 * @param list the new purchased products
	 */
	public void setPurchasedProducts(List<SalesLineItem> list)
	{
		if(list!=null)
		{
			this.purchasedProducts=new ArrayList<SalesLineItem>();
				purchasedProducts.addAll(list);
		}
	}
	/**
	 * Gets the po date.
	 * @return the po date
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.DATEBOX, flexFormNumber = "01", title = "PO Date", variableName = "dateBoxPODate")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 7, isFieldUpdater = false, isSortable = true, title = "PO Date")
	public Date getPoDate() 
	{
		return poDate;
	}
	/**
	 * Sets the po date.
	 * @param poDate the new po date
	 */
	public void setPoDate(Date poDate) 
	{
		this.poDate = poDate;
	}

	/**
	 * Gets the po nr.
	 * @return the po nr
	 */
	@SearchAnnotation(Datatype = ColumnDatatype.TEXTBOX, flexFormNumber = "12", title = "PO Nr", variableName = "tbPONr")
	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 5, isFieldUpdater = false, isSortable = true, title = "PO Nr")
	public String getPoNr() 
	{
		return poNr;
	}
	/**
	 * Sets the po nr.
	 * @param poNr the new po nr
	 */
	public void setPoNr(String poNr) 
	{
		if(poNr!=null)
			this.poNr = poNr.trim();
	}
	
	
	@TableAnnotation(ColType = FormTypes.NUMBERTEXTCELL, colNo = 6, isFieldUpdater = false, isSortable = true, title = "PO Value")
	public double getPoValue() {
		return poValue;
	}

	public void setPoValue(double poValue) {
		this.poValue = poValue;
	}

	@Override
	public String getStatus() {
		// TODO Auto-generated method stub
		return super.getStatus();
	}

	@Override
	public void setStatus(String status) {
		// TODO Auto-generated method stub
		super.setStatus(status);
	}

	
	@Override
	@SearchAnnotation(Datatype = ColumnDatatype.OBJECTLISTBOXEMPLOYEE, flexFormNumber = "11", title = "Purchase Engineer", variableName = "olbPurchaseEngg")
	public String getEmployee() {
		// TODO Auto-generated method stub
		return super.getEmployee();
	}

	@Override
	public void setEmployee(String employee) {
		// TODO Auto-generated method stub
		super.setEmployee(employee);
	}
	

	@TableAnnotation(ColType = FormTypes.TEXTCOLUM, colNo = 4, isFieldUpdater = false, isSortable = true, title = "Vendor")
	public String getVendorFullName() {
		return vendorInfo.getFullName();
	}

	
	@Override
	public String getGroup() {
		return super.getGroup();
	}

	@Override
	public String getCategory() {
		return super.getCategory();
	}

	

	@Override
	public String getType() {
		return super.getType();
	}

	/**************************************************Relation Management Part**********************************************/
	
	 @OnSave
	 @GwtIncompatible
	  public void OnSave()
	  {
		super.OnSave();
		if(poDate!=null)
		{
			this.poDate=GenricServiceImpl.parseDate(poDate);
					
		}

      }
	
   @OnLoad
   @GwtIncompatible
   public void onLoad()
   {
	   super.OnLoad();
	   if(personInfoKey!=null)
		   vendorInfo=ofy().load().key(personInfoKey).now();
  }

public PersonInfo getVendorInfo() {
	return vendorInfo;
}


}
