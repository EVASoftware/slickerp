package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
@Entity
public class FinancialCalender extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1447114824712877391L;

	
	/***************************** Applicability Attributes ***********************************/	

	@Index
	protected String calendarName;
	@Index
	protected Date startDate;
	@Index
	protected Date endDate;
	@Index
	protected boolean status;
	
	/**********************************************Constructor******************************************/

	public FinancialCalender(){
		super();
		
	}
	
	@Override
	public boolean isDuplicate(SuperModel m) {

		FinancialCalender entity = (FinancialCalender) m;
		String name = entity.getCalendarName().trim();
		String curname=this.calendarName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}

	@Override
	public String toString() {
		return calendarName;
	}
	
	/***************************** getters and setters *******************************/
	
	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
	
}
