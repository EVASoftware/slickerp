package com.slicktechnologies.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;

@Entity
public class EmployeeTrackingDetails extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2932904089608563397L;

	@Index
	int employeeId;
	@Index
	String employeeName;
	long mobileNo;
	@Index
	Date date;
	@Index
	Date timeFormat;
	@Index
	String time;
	long imeiNumber;
	String latitude;
	String longitude;
	String address;
	@Index
	int serviceId;
	@Index
	int contractId;
	@Index
	String branch;
	
	
	public EmployeeTrackingDetails() {
		super();
		employeeName = "";
		branch= "";
		latitude = "";
		longitude = "";
		address = "";
		time = "";
	}


	public int getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}


	public String getEmployeeName() {
		return employeeName;
	}


	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}


	public long getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}


	public long getImeiNumber() {
		return imeiNumber;
	}


	public void setImeiNumber(long imeiNumber) {
		this.imeiNumber = imeiNumber;
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public int getServiceId() {
		return serviceId;
	}


	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}


	public int getContractId() {
		return contractId;
	}


	public void setContractId(int contractId) {
		this.contractId = contractId;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public Date getTimeFormat() {
		return timeFormat;
	}


	public void setTimeFormat(Date timeFormat) {
		this.timeFormat = timeFormat;
	}


	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

}
