package com.slicktechnologies.shared;

import java.util.Vector;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

@Entity
public class TaxDetails extends SuperModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -604212581956655407L;
	
	/*************************************Applicability Attributes********************************************/
	
	@Index
	protected String taxChargeName;
	@Index
	protected boolean isCentralTax;
	@Index
	protected boolean isServiceTax;
	@Index
	protected boolean isVatTax;
	@Index
	protected double taxChargePercent;
	@Index
	protected  String glAccountName;
	protected String taxDescription;
	@Index
	protected boolean taxChargeStatus;
	
//  rohan added this code for GST implementation Date : 22-06-2017
	protected String taxPrintName;
	
	/*******************************************Constructor***************************************************/
	public TaxDetails() {
		super();
		taxChargeName="";
		glAccountName="";
		taxDescription="";
		taxPrintName="";
	}

/*******************************************************Getters And Setters**************************************/	

	public String getTaxChargeName() {
		return taxChargeName;
	}

	public void setTaxChargeName(String taxChargeName) {
		if(taxChargeName!=null){
			this.taxChargeName = taxChargeName;
		}
	}

	public Boolean getCentralTax() {
		return isCentralTax;
	}
	
	public void setCentralTax(boolean isCentralTax) {
		this.isCentralTax = isCentralTax;
	}
	
	public Boolean getServiceTax() {
		return isServiceTax;
	}

	public void setServiceTax(boolean isServiceTax) {
		this.isServiceTax = isServiceTax;
	}

	public Boolean getVatTax() {
		return isVatTax;
	}

	public void setVatTax(boolean isVatTax) {
		this.isVatTax = isVatTax;
	}

	public Double getTaxChargePercent() {
		return taxChargePercent;
	}
	public void setTaxChargePercent(double taxChargePercent) {
		this.taxChargePercent = taxChargePercent;
	}

	public String getGlAccountName() {
		return glAccountName;
	}
	
	public void setGlAccountName(String glAccountName) {
		if(glAccountName!=null){
			this.glAccountName = glAccountName.trim();
		}
	}

	public String getTaxDescription() {
		return taxDescription;
	}

	public void setTaxDescription(String taxDescription) {
		if(taxDescription!=null){
			this.taxDescription = taxDescription.trim();
		}
	}

	public Boolean getTaxChargeStatus() {
		return taxChargeStatus;
	}

	public void setTaxChargeStatus(boolean taxChargeStatus) {
		this.taxChargeStatus = taxChargeStatus;
	}
	
	public String getTaxPrintName() {
		return taxPrintName;
	}

	public void setTaxPrintName(String taxPrintName) {
		this.taxPrintName = taxPrintName;
	}

	/*************************************To String(Overridden)********************************************/
	
	@Override
	public String toString() {
		return taxChargeName;
	}
	
	/*************************************Is duplicate(Overridden)********************************************/

	@Override
	public boolean isDuplicate(SuperModel m) {
		return duplicateName(m);
	}

/******************************************Duplicate Validation for Tax Name***********************************/	
	public boolean duplicateName(SuperModel m)
	{
		TaxDetails entity = (TaxDetails) m;
		String name = entity.getTaxChargeName().trim();
		String curname=this.taxChargeName.trim();
		curname=curname.replaceAll("\\s","");
		curname=curname.toLowerCase();
				name=name.replaceAll("\\s","");
		name=name.toLowerCase();
		
		//New Object is being added
		if(id==null)
		{
			if(name.equals(curname))
				return true;
			else
				return false;
		}
		
		// Old object is being updated
		else
		{
			if(id.equals(entity.id))
			{
			  return false;	
			}
			if((name.equals(curname)==true))
			  return true;
			else
				return false;
				
		}
	}
	
	
	public static void initializeVatTax(ObjectListBox<TaxDetails> vatTax)
	{
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("taxChargeStatus");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		//rohan commented this code for GST implementation
//		filter=new Filter();
//		filter.setQuerryString("isVatTax");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new TaxDetails());
		vatTax.MakeLive(querry);
	}


	public static void initializeServiceTax(ObjectListBox<TaxDetails> serTax)
	{
		MyQuerry querry=new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter=null;
		
		filter=new Filter();
		filter.setQuerryString("taxChargeStatus");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		//rohan commented this code for GST implementation
//		filter=new Filter();
//		filter.setQuerryString("isServiceTax");
//		filter.setBooleanvalue(true);
//		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new TaxDetails());
		serTax.MakeLive(querry);
	}

	
	
	
	

}
