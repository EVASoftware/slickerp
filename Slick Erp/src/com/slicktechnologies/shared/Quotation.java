package com.slicktechnologies.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.config.configurations.ConfigTypes;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.LoginServiceImpl;
import com.slicktechnologies.server.QuotationServiceImplentor;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.ConcreteBusinessProcess;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.coststructure.ManPowerCostStructure;
import com.slicktechnologies.shared.common.coststructure.MaterialCostStructure;
import com.slicktechnologies.shared.common.coststructure.OtherCost;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;
import com.slicktechnologies.shared.common.sms.SMSDetails;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Represents the Quotation of AMC only.
 */
@Entity

public class Quotation extends Sales implements Serializable
{
	
	public final static String  QUOTATIONSUCESSFUL="Successful";
	public final static String  QUOTATIONUNSUCESSFUL="UnSuccessful";
	public final static String  QUOTATIONREVISED="Revised";
	
	/**************************************************Entity Attributes**********************************************/
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4383102276376664406L;

	/** The grand total of all sales line items */
	protected double grandTotal;

	/** The flat discount on grand total*/
	protected double flatDiscount;

	/** The per discount on grand total */
	protected double perDiscount;

	/** The total Amount to be paid */
	protected double netpayable;

	/** The valid until. Expiry date of the quotation*/
	protected Date validUntill;

	/**Priority Of Quotation*/
	@Index
	protected String priority;
	
	@Index
	protected Date quotationDate;
	
	
	@Index
	protected Integer ticketNumber;
	
	@Index
	protected Boolean complaintFlag;
	
	@Index
	protected Boolean isQuotation;
	
	// *******vaishnavi***************
		protected String payTerms;
	
	//***********rohan changes here ************************
	protected DocumentUpload customeQuotationUpload;
	
	protected boolean customequotation=false;
	
	protected String premisesDesc;
	
	protected String inspectedBy;
	protected String designation;
	
	protected Date inspectionDate;
	
	protected String referenceNumber;
	protected Date referenceDate;
/*********************************Attributes for Security Deposit PopUp*************************************/
	
	/** ID of current document */
	@Index
	protected int sdDocId;
	
	/** Date of current Document */
	protected Date sdDocDate;
	
	/** Status of current document */
	@Index
	protected String sdDocStatus;
	
	/** Field to identify if quotation includes the pay of deposit amount  */
	@Index
	protected String isDepositPaid;
	
	/** Represents the amount paid as deposit */
	@Index
	protected double sdDepositAmount;
	
	/** Represents the payment method of deposit amount */
	@Index
	protected String sdPaymentMethod;
	
	/** Represents the return of deposit amount in case of quotation not accepted */
	@Index
	protected Date sdReturnDate;
	
	/** Field to identify whether deposit has been returned or not */
	@Index
	protected boolean isDepositReceived;
	
	/** Deposit Bank Information */
	
	protected String sdBankAccNo;
	protected String sdBankName;
	protected String sdBankBranch;
	protected String sdPayableAt;
	protected String sdFavouring;
	protected int sdChequeNo;
	protected Date sdChequeDate;
	protected String sdReferenceNo;
	protected String sdIfscCode;
	protected String sdMicrCode;
	protected String sdSwiftCode;
	protected String sdRemark;
	protected Address sdAddress;
	protected String sdInstruction;
	
	
	/********************************Service Scheduling Field*********************************/
	/**
	 * Represents the services scheduled while saving contract.
	 */
	protected ArrayList<ServiceSchedule> serviceScheduleList;
	protected String scheduleServiceDay;
	
	
	/**************************************************Relational Part************************************************************/
    protected Key<Config>keyQuotationPriority;
    protected boolean statusChanged=false;
    @Index
	private Date startDate;
    @Index
	private Date endDate;
	
    protected String chemicaltouse;
    protected String freqService;
    protected String warrantyPeriod;
    
    /**
     * Date : 22-05-2017 BY ANIL
     * This fields stores the cost details for estimating/calculate profit quotations cost
     */
    
    ArrayList<MaterialCostStructure> matCostList;
    ArrayList<ManPowerCostStructure> manPowCostList;
    ArrayList<OtherCost>otherCostList;
    
    /**
     * END
     */
	
    /** Date 01-09-2017 added by vijay for discount for total amt **/
	  protected double discountAmt;
	  
	  /** Date 01-09-2017 added by vijay for after discount final total amt  before taxes**/
	  protected double finalTotalAmt;
	  
	  /** Date 01-09-2017 added by vijay for Round Off amt **/
	  protected double roundOffAmt;
	  
	  /** Date 04-09-2017 added by vijay for Total amt after all taxes and before roundoff **/
	  protected double grandTotalAmount;
	
	/** Date 05-09-2017 added by vijay for total amount after tax before other charges *****/
	  protected double inclutaxtotalAmount;

	  /** Date 11/10/2017 added by komal for followup date **/
	  @Index
	  private Date followUpDate;
	  
	  
     /**
	 * Date 11-08-2018 By Vijay
	 * Des :- revised quotation old quotation id in new quotation 
	 */
		protected int oldQuotationId;
		
		/**
		 * Updated By: Viraj
		 * Date: 04-04-2019
		 * Description: To show consolidated data
		 */
		protected boolean consolidatePrice;
		@Index
		protected String productGroup;
		
		protected Address serviceAddress;
		protected Address billingAddress;
		
	
	/**
	 * @author 27-06-2021 by Vijay
	 * Des :- payment Gateway unique ID	
	 */
	@Index
	protected String paymentGatewayUniqueId;
	
	@Index
	protected String quotationNumberRange;
	
	protected boolean donotprintServiceAddress;
	
	/**@Sheetal:30-03-2022,Adding payment mode tab on quotation**/
	protected String paymentModeName;
	
	protected boolean donotPrintTotal;
	
	protected ArrayList<TermsAndConditions> termsAndConditionlist;
	protected boolean printAllTermsConditions;
	
	protected boolean startOfPeriod;
	/**************************************************Default Ctor**********************************************/
	/**
	 * Instantiates a new quotation.
	 */
	
	/*
	Ashwini Patil 
	Date:27-06-2024
	Pecopp is using zoho for creating quotation and they want to cretae quotation in eva using anylaticsdatacreation api.
	
	*/
	@Index
	protected String  zohoQuotID;
	
	
	public Quotation()
	{
		super();
		priority="";
		this.status=CREATED;
		statusChanged=false;
		sdDocStatus="";
		isDepositPaid="";
		sdPaymentMethod="";
		sdBankAccNo="";
		sdBankName="";
		sdBankBranch="";
		sdPayableAt="";
		sdFavouring="";
		sdIfscCode="";
		sdMicrCode="";
		sdSwiftCode="";
		sdAddress=new Address();
		sdInstruction="";
		sdRemark="";
		sdReferenceNo="";
		serviceScheduleList=new ArrayList<ServiceSchedule>();
		scheduleServiceDay="";
		ticketNumber=-1;
		complaintFlag=false;
		isQuotation=true;
		
		/**
		 * Date : 22-05-2017 by Anil
		 */
		
		matCostList=new ArrayList<MaterialCostStructure>();
		manPowCostList=new ArrayList<ManPowerCostStructure>();
		otherCostList=new ArrayList<OtherCost>();
		
		/**
		 * End
		 */
		/**
		 * Updated By: Viraj
		 * Date: 04-04-2019
		 * Description: To show consolidated data
		 */
		consolidatePrice =false;
		
		serviceAddress = new Address();
		billingAddress = new Address();
		
		paymentGatewayUniqueId = "";
		quotationNumberRange="";
		donotprintServiceAddress = false;
		paymentModeName="";
		donotPrintTotal = false;
		termsAndConditionlist = new ArrayList<TermsAndConditions>();
		printAllTermsConditions = false;
	}

	/**************************************************Getter/Setter**********************************************/
	/**
	 * Updated By: Viraj
	 * Date: 04-04-2019
	 * Description: To show consolidated data
	 */
	public boolean isConsolidatePrice() {
		return consolidatePrice;
	}

	public void setConsolidatePrice(boolean consolidatePrice) {
		this.consolidatePrice = consolidatePrice;
	}
	/** Ends **/
	public Date getFollowUpDate() {
		return followUpDate;
	}

	public void setFollowUpDate(Date followUpDate) {
		if(followUpDate != null){
			this.followUpDate = followUpDate;
		}
	}
	
	public ArrayList<MaterialCostStructure> getMatCostList() {
		return matCostList;
	}

	public void setMatCostList(List<MaterialCostStructure> matCostList) {
		ArrayList<MaterialCostStructure> list=new ArrayList<MaterialCostStructure>();
		list.addAll(matCostList);
		this.matCostList = list;
	}

	public ArrayList<ManPowerCostStructure> getManPowCostList() {
		return manPowCostList;
	}

	public void setManPowCostList(List<ManPowerCostStructure> manPowCostList) {
		ArrayList<ManPowerCostStructure> list=new ArrayList<ManPowerCostStructure>();
		list.addAll(manPowCostList);
		this.manPowCostList = list;
	}

	public ArrayList<OtherCost> getOtherCostList() {
		return otherCostList;
	}

	public void setOtherCostList(List<OtherCost> otherCostList) {
		ArrayList<OtherCost> list=new ArrayList<OtherCost>();
		list.addAll(otherCostList);
		this.otherCostList = list;
	}
	
	
	/**
	 * Gets the net payable.
	 *
	 * @return the net payable
	 */
	


	/**
	 * Sets the net payable.
	 *
	 * @param netpayable the new net payable
	 */
	

	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessprocesslayer.ConcreteBuisnessProcess#compareTo(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	@Override
	public int compareTo(SuperModel o) {
		Quotation quot=(Quotation) o;
		return quot.creationDate.compareTo(creationDate);
	}



	/**
	 * Sets the grand total.
	 *
	 * @param grandTotal the new grand total
	 */
	public void setGrandTotal(Double grandTotal) {
		if(grandTotal!=null)
		   this.grandTotal = grandTotal;
	}

	/**
	 * Gets the grand total.
	 *
	 * @return the grand total
	 */
	public double getGrandTotal() {
		return grandTotal;
	}



	/**
	 * Gets the flat discount.
	 *
	 * @return the flat discount
	 */
	public double getFlatDiscount() {
		return flatDiscount;
	}


	/**
	 * Sets the flat discount.
	 *
	 * @param flatDiscount the new flat discount
	 */
	public void setFlatDiscount(Double flatDiscount) {
		if(flatDiscount!=null)
		   this.flatDiscount = flatDiscount;
	}


	/**
	 * Gets the per discount.
	 *
	 * @return the per discount
	 */
	public double getPerDiscount() {
		return perDiscount;
	}


	/**
	 * Sets the per discount.
	 *
	 * @param perDiscount the new per discount
	 */
	public void setPerDiscount(Double perDiscount) {
		if(perDiscount!=null)
			this.perDiscount = perDiscount;
	}


	/**
	 * Gets the netpayable.
	 *
	 * @return the netpayable
	 */
	public double getNetpayable() {
		return netpayable;
	}


	/**
	 * Sets the netpayable.
	 *
	 * @param netpayable the new netpayable
	 */
	public void setNetpayable(Double netpayable) {
		if(netpayable!=null)
			this.netpayable = netpayable;
	}


	/**
	 * Gets the validuntil.
	 *
	 * @return the validuntil
	 */
	public Date getValidUntill() {
		return validUntill;
	}


	/**
	 * Sets the validuntil.
	 *
	 * @param validuntil the new validuntil
	 */
	public void setValidUntill(Date validuntil) {
		this.validUntill = validuntil;
	}
	
	public Date getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(Date quotationDate) {
		if(quotationDate!=null){
			this.quotationDate = quotationDate;
		}
	}

	/*************************************Getters And Setters For Security Deposit*****************************/

	
	
	
	

	public Integer getTicketNumber() {
		return ticketNumber;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public void setTicketNumber(Integer ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public Boolean getComplaintFlag() {
		return complaintFlag;
	}

	public void setComplaintFlag(Boolean complaintFlag) {
		this.complaintFlag = complaintFlag;
	}

	public Boolean getIsQuotation() {
		return isQuotation;
	}

	public void setIsQuotation(Boolean isQuotation) {
		this.isQuotation = isQuotation;
	}

	public int getSdDocId() {
		return sdDocId;
	}

	public void setSdDocId(int sdDocId) {
		this.sdDocId = sdDocId;
	}


	public Date getSdDocDate() {
		return sdDocDate;
	}


	public void setSdDocDate(Date sdDocDate) {
		this.sdDocDate = sdDocDate;
	}


	public String getSdDocStatus() {
		return sdDocStatus;
	}


	public void setSdDocStatus(String sdDocStatus) {
		this.sdDocStatus = sdDocStatus;
	}


	public String getIsDepositPaid() {
		return isDepositPaid;
	}


	public void setIsDepositPaid(String isDepositPaid) {
		this.isDepositPaid = isDepositPaid;
	}


	public double getSdDepositAmount() {
		return sdDepositAmount;
	}


	public void setSdDepositAmount(double sdDepositAmount) {
		this.sdDepositAmount = sdDepositAmount;
	}


	public String getSdPaymentMethod() {
		return sdPaymentMethod;
	}


	public void setSdPaymentMethod(String sdPaymentMethod) {
		this.sdPaymentMethod = sdPaymentMethod;
	}


	public Date getSdReturnDate() {
		return sdReturnDate;
	}


	public void setSdReturnDate(Date sdReturnDate) {
		this.sdReturnDate = sdReturnDate;
	}


	public Boolean isDepositReceived() {
		return isDepositReceived;
	}


	public void setDepositReceived(boolean isDepositReceived) {
		this.isDepositReceived = isDepositReceived;
	}


	public String getSdBankAccNo() {
		return sdBankAccNo;
	}


	public void setSdBankAccNo(String sdBankAccNo) {
		this.sdBankAccNo = sdBankAccNo;
	}


	public String getSdBankName() {
		return sdBankName;
	}


	public void setSdBankName(String sdBankName) {
		this.sdBankName = sdBankName;
	}


	public String getSdBankBranch() {
		return sdBankBranch;
	}


	public void setSdBankBranch(String sdBankBranch) {
		this.sdBankBranch = sdBankBranch;
	}


	public String getSdPayableAt() {
		return sdPayableAt;
	}


	public void setSdPayableAt(String sdPayableAt) {
		this.sdPayableAt = sdPayableAt;
	}


	public String getSdFavouring() {
		return sdFavouring;
	}


	public void setSdFavouring(String sdFavouring) {
		this.sdFavouring = sdFavouring;
	}


	public int getSdChequeNo() {
		return sdChequeNo;
	}


	public void setSdChequeNo(int sdChequeNo) {
		this.sdChequeNo = sdChequeNo;
	}


	public Date getSdChequeDate() {
		return sdChequeDate;
	}


	public void setSdChequeDate(Date sdChequeDate) {
		this.sdChequeDate = sdChequeDate;
	}

	public String getSdReferenceNo() {
		return sdReferenceNo;
	}

	public void setSdReferenceNo(String sdReferenceNo) {
		if(sdReferenceNo!=null){
			this.sdReferenceNo = sdReferenceNo.trim();
		}
	}

	public DocumentUpload getCustomeQuotationUpload() {
		return customeQuotationUpload;
	}

	public void setCustomeQuotationUpload(DocumentUpload customeQuotationUpload) {
		this.customeQuotationUpload = customeQuotationUpload;
	}

	public boolean isCustomequotation() {
		return customequotation;
	}

	public void setCustomequotation(boolean customequotation) {
		this.customequotation = customequotation;
	}

	public String getSdIfscCode() {
		return sdIfscCode;
	}


	public void setSdIfscCode(String sdIfscCode) {
		this.sdIfscCode = sdIfscCode;
	}


	public String getSdMicrCode() {
		return sdMicrCode;
	}


	public void setSdMicrCode(String sdMicrCode) {
		this.sdMicrCode = sdMicrCode;
	}


	public String getSdSwiftCode() {
		return sdSwiftCode;
	}


	public void setSdSwiftCode(String sdSwiftCode) {
		this.sdSwiftCode = sdSwiftCode;
	}


	public Address getSdAddress() {
		return sdAddress;
	}


	public void setSdAddress(Address sdAddress) {
		this.sdAddress = sdAddress;
	}


	public String getSdInstruction() {
		return sdInstruction;
	}


	public void setSdInstruction(String sdInstruction) {
		this.sdInstruction = sdInstruction;
	}
	
	public String getSdRemark() {
		return sdRemark;
	}

	public void setSdRemark(String sdRemark) {
		this.sdRemark = sdRemark;
	}



	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.shared.businessprocesslayer.ConcreteBuisnessProcess#isDuplicate(com.simplesoftwares.shared.helperlayer.SuperModel)
	 */
	@Override
	public boolean isDuplicate(SuperModel m) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) 
	{
		if(priority!=null)
			this.priority = priority.trim();
	}
	
	public List<ServiceSchedule> getServiceScheduleList() {
		return serviceScheduleList;
	}

	public void setServiceScheduleList(List<ServiceSchedule> serviceScheduleList) {
		ArrayList<ServiceSchedule> arrOfServices=new ArrayList<ServiceSchedule>();
		arrOfServices.addAll(serviceScheduleList);
		this.serviceScheduleList = arrOfServices;
	}
	
	public String getScheduleServiceDay() {
		return scheduleServiceDay;
	}

	public void setScheduleServiceDay(String scheduleServiceDay) {
		if(scheduleServiceDay!=null){
			this.scheduleServiceDay = scheduleServiceDay.trim();
		}
	}

	@Override
	public String getStatus() {
		return super.getStatus();
	}
	
	

	
	/**************************************************Relation Managment Part**********************************************/
	@OnSave
	@GwtIncompatible
	public void manageQuotationRelation()
	{
			
			keyQuotationPriority=MyUtility.getConfigKeyFromCondition(priority,ConfigTypes.QUOTATIONPRIORITY.getValue());	
			
	}
	
	

	@OnSave
	@GwtIncompatible
	private void updateComplaint1(){
		if(id==null){
			if(getTicketNumber()!=-1&&getIsQuotation()==true){
				System.out.println("INSIDE Quotation ON SAVE....!!!!");
				Complain complain=ofy().load().type(Complain.class).filter("companyId", this.getCompanyId()).filter("count", this.getTicketNumber()).first().now();
				if(complain!=null){
					complain.setQuatationId(this.getCount());
				}
				ofy().save().entity(complain);
			}
			
			//Date 10-Aug-2018 added by vijay for lead status changing to Quotation Created
			changeLeadStatus(this);
			
			/**
			 * Date 11-08-2018 By Vijay
			 * Des :- revised old quotation changing status to Revised as per nitin sir
			 * because when we create revised quotation then on dashboard old and new quotation display which is wrong
			 * so old quotation changed status to revised
			 */
			if(this.getOldQuotationId()!=0){
				Quotation quotation = ofy().load().type(Quotation.class).filter("companyId", this.getCompanyId()).filter("count", this.getOldQuotationId()).first().now();
				if(quotation!=null){
					quotation.setStatus(QUOTATIONREVISED);
					ofy().save().entity(quotation);
					System.out.println("Quotaion updated as revised");
				}
			}
			/**
			 * ends here
			 */
			
			/**
			 * @author Vijay Chougule Date 19-06-2020
			 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
			 */
				if(this.getStartDate()!=null){
					DateUtility dateUtility = new DateUtility();
					this.setStartDate(dateUtility.setTimeMidOftheDayToDate(this.getStartDate()));
				}
				if(this.getEndDate()!=null){
					DateUtility dateUtility = new DateUtility();
					this.setEndDate(dateUtility.setTimeMidOftheDayToDate(this.getEndDate()));
				}
			/**
			 * ends here	
			 */
		}
	}
	
	
	/**
	 * Date 10-Aug-2018 below code updated 
	 * Des :- Lead status will changed as quotation Created when we create quotation against it
	 */
	
	/**
	 * Date 05-03-2018 By Vijay As per Nitin sir We updated lead status successful when contract Approved
	 * so below code commented
	 */
	
	/** Date 10 Oct 2017 added by vijay for lead status changing to successful requirement : Intech ***/
	@GwtIncompatible
	private void changeLeadStatus(Quotation q) {
		if(q.getLeadCount()!=0){
			
			
		Lead lead = ofy().load().type(Lead.class).filter("companyId", q.getCompanyId()).filter("count", q.getLeadCount()).first().now();
			if(lead!=null){
				List<Config> leadStatusList = ofy().load().type(Config.class).filter("type",6).filter("companyId", q.getCompanyId()).list();
				boolean flag = false;
				for(Config config :leadStatusList){
					if(config.getName().equals("Quotation Created")){
						flag =true;
						break;
					}
				}
				if(flag==false){
					Config config = new Config();
					config.setName("Quotation Created");
					config.setStatus(true);
					config.setType(6);
					config.setCompanyId(lead.getCompanyId());
					GenricServiceImpl genImpl = new GenricServiceImpl();
					genImpl.save(config);
				}
				lead.setStatus("Quotation Created");
				ofy().save().entity(lead);
			}
		}
	}
	@OnLoad
	@GwtIncompatible
	public void OnLoad()
	{	

	}
	/*********************************************************************************************************************/


    
    	
    		

	public static List<String> getStatusList()
	{
		ArrayList<String> statuslist=new ArrayList<String>();
		statuslist.add(CREATED);
		statuslist.add(QUOTATIONSUCESSFUL);
		statuslist.add(QUOTATIONUNSUCESSFUL);
		statuslist.add(APPROVED);
		statuslist.add(Contract.REJECTED);
		statuslist.add(Contract.REQUESTED);
		
		return statuslist;
	}
	
	public Date getStartDate()
	{
		return startDate;
	}
	
	public Date getEndDate()
	{
		return endDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	

	public boolean isSellContract()
	{
		
		for(SalesLineItem item:this.items)
		{
			if(item.getPrduct()!=null&&item.getPrduct() instanceof ServiceProduct)
				return false;
			
		}
		return true;
	}

	@GwtIncompatible
	@Override
	public void reactOnApproval() {
		QuotationServiceImplentor implementor=new QuotationServiceImplentor();
		implementor.changeStatus(this);
		
		
		/**
		 * @author Vijay Chougule Date - 29-12-2022 
		 * Des :- To send automate message with pdf 
		 */
		 sendMessage();
		 
	}	 

	
	@GwtIncompatible
	private void sendMessage() {
		 Logger logger = Logger.getLogger("NameOfYourLogger");
		 SMSDetails smsdetails = new SMSDetails();
		 smsdetails.setModel(this);
		 SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId", this.getCompanyId()).filter("event", AppConstants.QUOTATIONAPPROVAL).filter("status",true).first().now();
		 
		 if(smsEntity!=null) {
			 smsdetails.setSmsTemplate(smsEntity);
			 smsdetails.setEntityName("Quotation");

			 String mobileNo=Long.toString(this.getCinfo().getCellNumber());
			 smsdetails.setMobileNumber(mobileNo);
			 smsdetails.setCommunicationChannel(AppConstants.SMS);
			 smsdetails.setModuleName(AppConstants.SERVICEMODULE);
			 smsdetails.setDocumentName(AppConstants.QUOTATION);

			 SmsServiceImpl sendImpl = new SmsServiceImpl();
			 String smsmsg = sendImpl.validateAndSendSMS(smsdetails);
			 logger.log(Level.SEVERE," inside validateAndSendSMS"+smsmsg);
		 }
	}

	public String getPremisesDesc() {
		return premisesDesc;
	}

	public void setPremisesDesc(String premisesDesc) {
		this.premisesDesc = premisesDesc;
	}

	public String getInspectedBy() {
		return inspectedBy;
	}

	public void setInspectedBy(String inspectedBy) {
		this.inspectedBy = inspectedBy;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getChemicaltouse() {
		return chemicaltouse;
	}

	public void setChemicaltouse(String chemicaltouse) {
		this.chemicaltouse = chemicaltouse;
	}

	public String getFreqService() {
		return freqService;
	}

	public void setFreqService(String freqService) {
		this.freqService = freqService;
	}

	public String getWarrantyPeriod() {
		return warrantyPeriod;
	}

	public void setWarrantyPeriod(String warrantyPeriod) {
		this.warrantyPeriod = warrantyPeriod;
	}

	public String getPayTerms() {
		return payTerms;
	}

	public void setPayTerms(String payTerms) {
		this.payTerms = payTerms;
	}
	
	
	/** Date 01-09-2017 added by vijay for getter and setter  **/

	public double getDiscountAmt() {
		return discountAmt;
	}


	public void setDiscountAmt(double discountAmt) {
		this.discountAmt = discountAmt;
	}


	public double getFinalTotalAmt() {
		return finalTotalAmt;
	}


	public void setFinalTotalAmt(double finalTotalAmt) {
		this.finalTotalAmt = finalTotalAmt;
	}


	public double getRoundOffAmt() {
		return roundOffAmt;
	}


	public void setRoundOffAmt(double roundOffAmt) {
		this.roundOffAmt = roundOffAmt;
	}
	
	public double getGrandTotalAmount() {
		return grandTotalAmount;
	}

	public void setGrandTotalAmount(double grandTotalAmount) {
		this.grandTotalAmount = grandTotalAmount;
	}

	public double getInclutaxtotalAmount() {
		return inclutaxtotalAmount;
	}

	public void setInclutaxtotalAmount(double inclutaxtotalAmount) {
		this.inclutaxtotalAmount = inclutaxtotalAmount;
	}
	
	public int getOldQuotationId() {
		return oldQuotationId;
	}

	public void setOldQuotationId(int oldQuotationId) {
		this.oldQuotationId = oldQuotationId;
	}
	
	
	/**
	 * @author Vijay Chougule Date 21-07-2020
	 * Des :- As per Nitin Sir common method to set mid of the time (1pm) to the Date for Google studio report
	 */

	@OnSave
	@GwtIncompatible
	public void setTimeMidOfTheDayToDate()
	{
		DateUtility dateUtility = new DateUtility();
		if(this.getFollowUpDate()!=null){
			this.setFollowUpDate(dateUtility.setTimeMidOftheDayToDate(this.getFollowUpDate()));
		}
		if(this.getCreationDate()!=null){
			this.setCreationDate(dateUtility.setTimeMidOftheDayToDate(this.getCreationDate()));
		}
	}
	/**
	 * ends here	
	 */

	public Address getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(Address serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return count+"";
	}


	public String getPaymentGatewayUniqueId() {
		return paymentGatewayUniqueId;
	}

	public void setPaymentGatewayUniqueId(String paymentGatewayUniqueId) {
		this.paymentGatewayUniqueId = paymentGatewayUniqueId;
	}
	
	public void setQuotationNumberRange(String quotationNumberRange) {
		this.quotationNumberRange = quotationNumberRange;
	}
		public String getQuotationNumberRange() {
		return quotationNumberRange;
	}

	public boolean isDonotprintServiceAddress() {
		return donotprintServiceAddress;
	}

	public void setDonotprintServiceAddress(boolean donotprintServiceAddress) {
		this.donotprintServiceAddress = donotprintServiceAddress;
	}

	public String getPaymentModeName() {
		return paymentModeName;
	}

	public void setPaymentModeName(String paymentModeName) {
		this.paymentModeName = paymentModeName;
	}

	public boolean isDonotPrintTotal() {
		return donotPrintTotal;
	}

	public void setDonotPrintTotal(boolean donotPrintTotal) {
		this.donotPrintTotal = donotPrintTotal;
	}

	public ArrayList<TermsAndConditions> getTermsAndConditionlist() {
		return termsAndConditionlist;
	}


	public void setTermsAndConditionlist(ArrayList<TermsAndConditions> termsAndConditionlist) {
		this.termsAndConditionlist = termsAndConditionlist;
	}


	public boolean isPrintAllTermsConditions() {
		return printAllTermsConditions;
	}



	public void setPrintAllTermsConditions(boolean printAllTermsConditions) {
		this.printAllTermsConditions = printAllTermsConditions;
	}

	public boolean isStartOfPeriod() {
		return startOfPeriod;
	}

	public void setStartOfPeriod(boolean startOfPeriod) {
		this.startOfPeriod = startOfPeriod;
	}

	public String getZohoQuotID() {
		return zohoQuotID;
	}

	public void setZohoQuotID(String zohoQuotID) {
		this.zohoQuotID = zohoQuotID;
	}
	
	
	
}
