package com.slicktechnologies.shared;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;







import com.google.gwt.core.shared.GwtIncompatible;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.documenthistorydetails.DocumentHistory;
import com.slicktechnologies.shared.common.documenthistorydetails.SaveHistory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.relationalLayer.BranchRelation;
import com.slicktechnologies.shared.common.relationalLayer.EmployeeRelation;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class Approvals extends SuperModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2347072473756224563L;
	@Index
	protected String businessprocesstype;
	@Index
	protected int businessprocessId;
	@Index
	protected String requestedBy;
	@Index
	protected String approverName;
	@Index
	protected Date creationDate;
	@Index
	protected String branchname;
	@Index
	protected String status;
	@Index
	protected String remark;
	@Index
	protected Date approvalOrRejectDate;
	protected int approvalLevel;
	
	// vijay 30 sep release
	protected String personResponsible;
	
	/**************************************Status Constants************************************************/
	public final static String REQUESTED="Requested";
	public final static String APPROVED="Approved";
	public final static String REJECTED="Rejected";
	public final static String CANCELLED="Cancelled";

	public static final String PENDING = "Pending";
	
	/***********************************Relational Attributes**************************************************/
	
	protected Key<EmployeeRelation> requestedByKey;
	protected Key<EmployeeRelation> approvernameKey;
	protected Key<BranchRelation> branchKey;
	
	
	
	/**
	 * This list stores the remarks given by the approver
	 * it is mainly purpose is for multilevel approval where you want to the remark 
	 * given by previous level approver
	 * Date: 14-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	@Index
	protected ArrayList<ApproversRemark> remarkList;
	/**
	 * End
	 */
	
	/**
	 * This variable stores the name of the person, who created particular approvable document.
	 * This is used in approval mail as CC so he can have track of document which he has created.
	 * Date : 14-10-2016 by Anil
	 * Release : 30 SEPT 2016
	 * Project : PURCHASE MODIFICATION (NBHC)
	 */
	
	protected String documentCreatedBy;
	
	/**
	 * End
	 */
	
	/**
	 *  nidhi
	 *  7-10-2017
	 *  for display customer id and name in approval details
	 */
	protected String bpId;

	protected String bpName;
	/**
	 * end
	 */
	
	/**
	 * Date 13-01-2019 by Vijay for NBHC Inventory Management
	 * Des :- for approval check flag true then allow to approve the PR Document
	 */
	protected boolean documentValidation;
	/**
	 * ends here
	 */
	/**
	 * Date 30-08-2019 By Vijay
	 * Des :- Inventory Management PR Local Vendor ID
	 */
	protected int vendorId;
	protected double vendorPrice;
	/**
	 * ends here
	 */
	
	public Approvals()
	{
		businessprocesstype="";
		requestedBy="";
		approverName="";
		branchname="";
		status="";
		remark="";
		personResponsible = "";
		
		remarkList=new ArrayList<ApproversRemark>();
		documentCreatedBy="";
		vendorId=-1;
	}
	
	/******************************************Getters And Setters********************************************/

	public ArrayList<ApproversRemark> getRemarkList() {
		return remarkList;
	}

	public void setRemarkList(List<ApproversRemark> remarkLis) {
		ArrayList<ApproversRemark> list=new ArrayList<ApproversRemark>();
		list.addAll(remarkLis);
		if(this.remarkList.size()!=0){
			this.remarkList.addAll(remarkLis);
		}else{
			this.remarkList = list;
		}
	}
	
	public String getDocumentCreatedBy() {
		return documentCreatedBy;
	}

	public void setDocumentCreatedBy(String documentCreatedBy) {
		this.documentCreatedBy = documentCreatedBy;
	}
	
	public String getBusinessprocesstype() {
		return businessprocesstype;
	}

	public void setBusinessprocesstype(String businessprocesstype) {
		this.businessprocesstype = businessprocesstype;
	}

	public int getBusinessprocessId() {
		return businessprocessId;
	}

	public void setBusinessprocessId(int businessprocessId) {
		this.businessprocessId = businessprocessId;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Key<EmployeeRelation> getRequestedByKey() {
		return requestedByKey;
	}

	public void setRequestedByKey(Key<EmployeeRelation> requestedByKey) {
		this.requestedByKey = requestedByKey;
	}

	public Key<EmployeeRelation> getApprovernameKey() {
		return approvernameKey;
	}

	public void setApprovernameKey(Key<EmployeeRelation> approvernameKey) {
		this.approvernameKey = approvernameKey;
	}

	public Key<BranchRelation> getBranchKey() {
		return branchKey;
	}

	public void setBranchKey(Key<BranchRelation> branchKey) {
		this.branchKey = branchKey;
	}
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		if(remark!=null){
			this.remark = remark.trim();
		}
	}

	public Date getApprovalOrRejectDate() {
		return approvalOrRejectDate;
	}

	public void setApprovalOrRejectDate(Date approvalOrRejectDate) {
		this.approvalOrRejectDate = approvalOrRejectDate;
	}
	
	public int getApprovalLevel() {
		return approvalLevel;
	}

	public void setApprovalLevel(int approvalLevel) {
		this.approvalLevel = approvalLevel;
	}

	/****************************************Overridden Method*******************************************/

	@Override
	public int compareTo(SuperModel o) {
		
		return 0;
	}

	@Override
	public boolean isDuplicate(SuperModel m) {
		
		return false;
	}

	  @GwtIncompatible
	  @OnSave
	  public void OnSave()
	  {
		  
		  DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		  dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
		 // if(id==null)
			{
				
				/** 
				 *   Date 01-05-2017 By Vijay NBHC Deadstock
				 *   below if condition commented by vijay becs no need this
				 *   this storing date only. we needed Date with time for NBHC Deadstock automatic approval cron job so must store date with time 
				 *   so creation date with time new code added below 
				 */
//				if(id==null){
//					setCreationDate(DateUtility.getDateWithTimeZone("IST",new Date()));
//				}
				
				if(id==null){
					Date todayDate = new Date();
					try {
						Date today = dateFormat.parse(dateFormat.format(todayDate));
						setCreationDate(today);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
				/**
				 * ends here
				 */
				
				

			/**
			 * This code is commented on Date: 9/1/2017 by rohan and anil 
			 * because client(NBHC CCPM) facing MIN approval issue due to this.
			 * 
			 * Observation: if MIN has more than 4 product in it then this issue occurs.
			 * following code execute more than once until it gets branch and employee name as parameter in below 
			 * Query. initially  branch and employee name is coming blank .
			 */
					
				
				
				
//			  	MyQuerry querry=new MyQuerry();
//			  	Vector<Filter>filter=new Vector<Filter>();
//			  	Filter nameFilter=new Filter();
//			  	nameFilter.setStringValue(getBranchname());
//			  	nameFilter.setQuerryString("branchName");
//			  	filter.add(nameFilter);
//			  	querry.setFilters(filter);
//			  	querry.setQuerryObject(new BranchRelation());
//			  	branchKey=(Key<BranchRelation>) MyUtility.getRelationalKeyFromCondition(querry);
//			  	
//			  	querry=new MyQuerry();
//			  	filter=new Vector<Filter>();
//			  	nameFilter=new Filter();
//			  	nameFilter.setStringValue(getApproverName());
//			  	nameFilter.setQuerryString("employeeName");
//			  	filter.add(nameFilter);
//			  	querry.setFilters(filter);
//			  	
//				querry.setQuerryObject(new EmployeeRelation());
//			  	approvernameKey=(Key<EmployeeRelation>) MyUtility.getRelationalKeyFromCondition(querry);
//			  	
//			  	querry=new MyQuerry();
//			  	filter=new Vector<Filter>();
//			  	nameFilter=new Filter();
//			  	nameFilter.setStringValue(getRequestedBy());
//			  	nameFilter.setQuerryString("employeeName");
//			  	filter.add(nameFilter);
//			  	querry.setFilters(filter);
//			  	
//				querry.setQuerryObject(new EmployeeRelation());
//			  	requestedByKey=(Key<EmployeeRelation>) MyUtility.getRelationalKeyFromCondition(querry);
				
				/**
				 * End
				 */
			}  
			
			/**
			 * @author Anil , Date : 23-10-2019
			 * Commenting approval history code
			 * it takes lot of time to save approval request
			 * Raised BY NBHC
			 * 
			 */
			
			
//			/**
//			 *  Code Below is used for saving data to Document History.
//			 */
//			
//			Type typeEntity=ofy().load().type(Type.class).filter("companyId", getCompanyId()).filter("internalType", 13).filter("typeName", this.getBusinessprocesstype().trim()).first().now();
//			String moduleName="";
//			if(typeEntity!=null)
//			{
//				moduleName=typeEntity.getCategoryName();
//			}
//			
//			if(this.getStatus().trim().equals(Approvals.PENDING)){
//				SaveHistory.saveHistoryData(getCompanyId(), moduleName, this.getBusinessprocesstype().trim(), this.getBusinessprocessId(), DocumentHistory.APPROVALREQUESTED, getCreatedBy(),getUserId());
//			}
//			
//			if(this.getStatus().trim().equals(Approvals.APPROVED)){
//				SaveHistory.saveHistoryData(getCompanyId(), moduleName, this.getBusinessprocesstype().trim(), this.getBusinessprocessId(), DocumentHistory.APPROVED,getCreatedBy(),getUserId());
//			}
//			
//			if(this.getStatus().trim().equals(Approvals.CANCELLED)){
//				SaveHistory.saveHistoryData(getCompanyId(), moduleName, this.getBusinessprocesstype().trim(), this.getBusinessprocessId(), DocumentHistory.APPROVALCANCELLED, getCreatedBy(),getUserId());
//			}
//			
//			if(this.getStatus().trim().equals(Approvals.REJECTED)){
//				SaveHistory.saveHistoryData(getCompanyId(), moduleName, this.getBusinessprocesstype().trim(), this.getBusinessprocessId(), DocumentHistory.APPROVALREJECTED, getCreatedBy(),getUserId());
//			}
	  }
	  
	  @GwtIncompatible
	  @OnLoad
	  public void OnLoad()
	  {
		  //Get Entity From Key for Branch 
		  if(branchKey!=null)
		  {
		    String branchName=MyUtility.getBranchNameFromKey(branchKey);
		    if(branchName.equals("")==false)
		    	setBranchname(branchName);
	
		  }
		  
		  //Get Entity From Key for Employee
		  if(approvernameKey!=null)
		  {
			  String employeeName=MyUtility.getEmployeeNameFromKey(approvernameKey);
			  if(employeeName.equals("")==false)
				  setApproverName(employeeName);
		     
		  }
		  
		  if(requestedByKey!=null)
		  {
			  String employeeName=MyUtility.getEmployeeNameFromKey(requestedByKey);
			  if(employeeName.equals("")==false)
				  setRequestedBy(employeeName);
		     
		  }
	  }
	  
	  
	  
	  
	  /********************************Drop down List for DOc Type ********************************************/
			public static List<String> getDocTypeList()
			{
				ArrayList<String> docTypeList=new ArrayList<String>();
				docTypeList.add("Contract");
				docTypeList.add("Quotation");
				docTypeList.add("Expense Managment");
				docTypeList.add("Complaint");
				docTypeList.add("Leave Application");
				docTypeList.add("Advance");
				docTypeList.add("Letter Of Intent");
				docTypeList.add("Billing Details");
				docTypeList.add("Invoice Details");
				docTypeList.add("Request For Quotation");
				docTypeList.add("Sales Quotation");
				docTypeList.add("Purchase Order");
				docTypeList.add("Purchase Requisition");
				docTypeList.add("Sales Order");
				docTypeList.add("GRN");
				docTypeList.add("MRN");
				docTypeList.add("MIN");
				docTypeList.add("MMN");
				docTypeList.add("Delivery Note");
				docTypeList.add("Inspection");
				docTypeList.add("Work Order");
				docTypeList.add("Campaign Management ");
				docTypeList.add("Service PO");
				docTypeList.add("Customer");
				docTypeList.add("Employee");   // added by Ajinkya on Date : 28/04/2017 
				docTypeList.add("Vendor Invoice Details");
				docTypeList.add("CNC");
				return docTypeList;
				
			}
			/********************************Drop down List For approval status****************************/
			
			
			public static List<String> getStatusList()
			{
		  
				ArrayList<String> statusList=new ArrayList<String>();
				
				statusList.add("Pending");
				statusList.add("Requested");
				statusList.add("Approved");
				statusList.add("In Process");
				statusList.add("Cancelled");
				statusList.add("Rejected");
				
			
				return statusList;
			}

			public String getPersonResponsible() {
				return personResponsible;
			}

			public void setPersonResponsible(String personResponsible) {
				this.personResponsible = personResponsible;
			}

			public boolean getDocumentValidation() {
				return documentValidation;
			}

			public void setDocumentValidation(boolean documentValidation) {
				this.documentValidation = documentValidation;
			}
		  

			public String getBpId() {
				return bpId;
			}

			public void setBpId(String bpId) {
				this.bpId = bpId;
			}

			public String getBpName() {
				return bpName;
			}

			public void setBpName(String bpName) {
				this.bpName = bpName;
			}

			public int getVendorId() {
				return vendorId;
			}

			public void setVendorId(int vendorId) {
				this.vendorId = vendorId;
			}

			public double getVendorPrice() {
				return vendorPrice;
			}

			public void setVendorPrice(double vendorPrice) {
				this.vendorPrice = vendorPrice;
			}
			
	
}
