package com.simplesoftwares.rebind.annatonations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ibm.icu.text.Replaceable;
import com.simplesoftwares.rebind.SuperGenerator;
import com.simplesoftwares.rebind.TableGenerators;

// TODO: Auto-generated Javadoc
/**
 * Used to pass corresponding Entity name of replaceable type to {@link SuperGenerator}.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EntityNameAnnotation
{
	
	/**
	 * Qualified name of Replaceable type Entity.
	 *
	 * @return the string
	 */
	public String EntityName();
}
