package com.simplesoftwares.rebind.annatonations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.simplesoftwares.client.library.composite.CommonSearchComposite;

/**
 * Annatonation handels the cases whrere there is no one to one correspondance
 * between Search Widget and entity field(For example in case of {@link CommonSearchComposite} three
 * entity fields combines to give One PopUp Widget.
 * In case of {@link PersonInfo} composite it can exist on Popup without having any corresponding entity field
 * @author SUMIT
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SearchCompositeAnnatonation  {
	
	/**
	 * 
	 *Datatypes of compostie  widgets on search pop up
	 * @return 
	 */
	public String[] Datatype();
	
	/**
	 * Variable name of composite widgets on search pop up
	 * 
	 *
	 * @return the string
	 */
	public String[] variableName();
	
	/**
	 * Flex form numbers of widget.(row , col number of the widget of search popup)
	 *
	 * @return the int
	 */
	public String[] flexFormNumber();
	/**
	 * Label of Widgets on search pop up
	 * @return
	 */
	
	public String[] title();
	/**
	 * Extra attribute some time needed for Widget variable initialization.
	 * @return
	 */
	
	public String[] extra() default {"",""};
	/**
	 * colspan of widget
	 * @return
	 */
	
	public int[] colspan() default {1,1};
	/**
	 * rowspan of widget
	 * @return
	 */
	
	public int[] rowspan() default {1,1};
	
	public String[] EmbededQuerry()default {"",""};

}
