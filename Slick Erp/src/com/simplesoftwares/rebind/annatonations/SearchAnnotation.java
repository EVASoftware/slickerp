package com.simplesoftwares.rebind.annatonations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotations on entity field for generation of search pop ups.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SearchAnnotation 
{
	/**
	 * 
	 *Datatype of widget on search pop up
	 * @return 
	 */
	public String Datatype();
	
	/**
	 * Variable name of widget on search pop up
	 * 
	 *
	 * @return the string
	 */
	public String variableName();
	
	/**
	 * Flex form number.(row , col number of the widget of search popup)
	 *
	 * @return the int
	 */
	public String flexFormNumber();
	/**
	 * Label of Widget on search pop up
	 * @return
	 */
	
	public String title();
	/**
	 * Extra attribute some time needed for Widget variable initialization.
	 * @return
	 */
	
	public String extra() default "";
	/**
	 * colspan of widget
	 * @return
	 */
	
	public int colspan() default 0;
	/**
	 * rowspan of widget
	 * @return
	 */
	
	public int rowspan() default 0;
	
	public String EmbededQuerry() default "";
	

}
