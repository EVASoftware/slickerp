package com.simplesoftwares.rebind.annatonations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.TextColumn;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.rebind.SuperGenerator;
import com.simplesoftwares.rebind.TableGenerators;

// TODO: Auto-generated Javadoc
/**
 * This Annotation Will come on getter Methods  of Entity.
 * {@link SuperGenerator} will use this for generation of concrete implementation of {@link SuperTable}
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TableAnnotation
{
	
	/**
	 * Title of genrated table column.
	 *
	 * @return the column title
	 */
	public String title();
	
	/**
	 * Checks if column is sortable.
	 *
	 * @return true, if is sortable
	 */
	public boolean isSortable();
	
	/**
	 * Checks if {@link FieldUpdater} will be generated with this respect to this column.
	 *
	 * @return true, if is field updater
	 */
	public boolean isFieldUpdater();
	
	/**
	 * The order of column in table
	 *
	 * @return the int
	 */
	public int colNo();
	
	/**
	 * DataType of column 
	 *
	 * @return the string
	 */
	public String ColType();
	

}
