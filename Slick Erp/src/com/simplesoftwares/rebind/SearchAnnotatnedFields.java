package com.simplesoftwares.rebind;

import com.google.gwt.core.ext.typeinfo.JMethod;

public class SearchAnnotatnedFields 
{
	/**
	 *Datatype of widget on search pop up
	 * @return 
	 */
	 public String dataType;
	
	/**
	 * Variable name of widget on search pop up
	 * @return the string
	 */
	public String variableName;
	
	/**
	 * Flex form number.(row , col number of the widget of search popup)
	 * @return the int
	 */
	public String flexFormNumber;
	/**
	 * Label of Widget on search pop up
	 * @return
	 */
	
	public String title;
	/**
	 * Extra attribute some time needed for Widget variable initialization.
	 * @return
	 */
	
	public String extra;
	/**
	 * colspan of widget
	 * @return
	 */
	
	public int colspan;
	/**
	 * rowspan of widget
	 * @return
	 */
	/**
	 * rowSpan
	 */
	
	public int rowspan;
	/**
	 * embededQuerry if available
	 */
	
	
	
	public String embededQuerry;
	/**
	 * Jmethod object corresponding to Method annatonation , is null for class Annatonation
	 */
	public JMethod method;
	
	
	public JMethod getMethod() {
		return method;
	}
	public void setMethod(JMethod method) {
		this.method = method;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getVariableName() {
		return variableName;
	}
	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}
	public String getFlexFormNumber() {
		return flexFormNumber;
	}
	public void setFlexFormNumber(String flexFormNumber) {
		this.flexFormNumber = flexFormNumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public int getColspan() {
		return colspan;
	}
	public void setColspan(int colspan) {
		this.colspan = colspan;
	}
	public int getRowspan() {
		return rowspan;
	}
	public void setRowspan(int rowspan) {
		this.rowspan = rowspan;
	}
	public String getEmbededQuerry() {
		return embededQuerry;
	}
	public void setEmbededQuerry(String embededQuerry) {
		this.embededQuerry = embededQuerry;
	}
	

}
