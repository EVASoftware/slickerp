package com.simplesoftwares.rebind;

import com.simplesoftwares.client.library.helperconstants.FormTypes;
import com.simplesoftwares.rebind.columnwriters.ColumnGenerator;
import com.simplesoftwares.rebind.columnwriters.DateColumnGenerator;
import com.simplesoftwares.rebind.columnwriters.EditTextCellGenerator;
import com.simplesoftwares.rebind.columnwriters.NumberGenerator;
import com.simplesoftwares.rebind.columnwriters.TextColumnGenerator;

/**
 * 
 * @author intel
 * It generates the Object depending on given condition
 */
public class ColumnGeneratorFactory 
{
      public static ColumnGenerator getColumnGenerator(String columnname)
      {
    	  ColumnGenerator generator=null;
		  if(columnname.equals(FormTypes.TEXTCOLUM))
			  generator=new TextColumnGenerator();
		  if(columnname.equals(FormTypes.EDITTEXTCELL))
			  generator=new EditTextCellGenerator();
		  if(columnname.equals(FormTypes.NUMBERTEXTCELL))
			  generator=new NumberGenerator();
		  if(columnname.equals(FormTypes.DATETEXTCELL))
			  generator=new DateColumnGenerator();
		  return generator;
      }
}
