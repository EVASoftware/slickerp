package com.simplesoftwares.rebind.columnwriters;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.rebind.SourceWriter;
import com.simplesoftwares.rebind.TableGenerators;
import com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields;

// TODO: Auto-generated Javadoc
/**
 * Generates the Date Column.
 */
public class DateColumnGenerator implements ColumnGenerator
{
	
	/** cots ("")*/
	private static final String cots="\"";
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.rebind.ColumnGenerator#createColumn(com.google.gwt.user.rebind.SourceWriter, com.google.gwt.core.ext.typeinfo.JClassType, com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields)
	 */
	/**
	 * Untested
	 */
	@Override
	public void createColumn(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno)
	{
		//Get simple Source name of entity
		String entityName=entity.getSimpleSourceName();
		//Get field name corresponding to which column will be generated.
		String fieldName =tableAnno.method.getName();
		
		//Start writing method body.
		writer.println("protected void addColumn"+tableAnno.method.getName()+"()");
		writer.println("{");
		writer.println("DateTimeFormat fmt = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);");
		writer.println("DatePickerCell date= new DatePickerCell(fmt);");
		writer.println(fieldName+"Column=new Column<"+entityName+",Date>(date)");
		writer.println("{");
		writer.println("@Override");
		writer.println("public Date getValue("+entityName+" "+"object)");
		writer.println("{");
		
		String mName=fieldName;
		
		//create setter
		mName=mName.replace("get","set");
		writer.println("if(object."+fieldName+"()!=null){");
		writer.println("return object."+fieldName+"();"+"}");
		writer.println("else{");
		
		writer.println("object."+mName+"(new Date());");
		writer.println("return new Date();");
		writer.println("}");
		writer.println("}");
		writer.println("};");
		writer.println("table.addColumn("+fieldName+"Column,"+cots+tableAnno.tableAnnotation.title()+cots+");");
		if(tableAnno.tableAnnotation.isSortable()==true)
			writer.println(tableAnno.method.getName()+"Column"+".setSortable(true);");
		writer.println("}");
		
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.rebind.ColumnGenerator#createFieldUpdater(com.google.gwt.user.rebind.SourceWriter, com.google.gwt.core.ext.typeinfo.JClassType, com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields)
	 */
	/**
	 * Un Tested
	 */
	@Override
	public void createFieldUpdater(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno)
	{
		String fieldName =tableAnno.method.getName();
        String entityName=entity.getSimpleSourceName();
        
				
				writer.println("protected void addFieldUpdater"+fieldName+"()");
				writer.println("{");
				writer.println(fieldName+"Column.setFieldUpdater(new FieldUpdater<"+entityName+", Date>()");
				writer.println("{");
				writer.println("@Override");
				writer.println("public void update(int index,"+entityName+" "+"object,Date value)");
				writer.println("{");

				String mName=fieldName;
				mName=fieldName.replace("get","set");
				
				writer.println("object."+mName+"(value);");
				writer.println("table.redrawRow(index);");
				writer.println("}");
				writer.println("});");
				writer.println("}");
		
		
	}

}
