package com.simplesoftwares.rebind.columnwriters;

import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.SourceWriter;
import com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields;

/**
 * Generates Number Column.
 */
public class NumberGenerator implements ColumnGenerator
{
	/** The cots. */
	String cots="\"";
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.rebind.ColumnGenerator#createColumn(com.google.gwt.user.rebind.SourceWriter, com.google.gwt.core.ext.typeinfo.JClassType, com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields)
	 */
	@Override
	public void createColumn(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno) 
	{
		String entityName=entity.getSimpleSourceName();
		String fieldName =tableAnno.method.getName();
		writer.println("protected void addColumn"+fieldName+"()");
		writer.println("{");
		writer.println("NumberCell editCell=new NumberCell();");
		writer.println(fieldName+"Column=new Column<"+entityName+",Number>(editCell)");
		writer.println("{");
		writer.println("@Override");
		writer.println("public Number getValue("+entityName+" "+"object)");
		writer.println("{");
			
		writer.println("if(object."+fieldName+"()==0){");
		writer.println("return 0;}");
		writer.println("else{");
		writer.println("return object."+fieldName+"();}");
		writer.println("}");
		writer.println("};");
		writer.println("table.addColumn("+fieldName+"Column,"+cots+tableAnno.tableAnnotation.title()+cots+");");
		if(tableAnno.tableAnnotation.isSortable()==true)
			writer.println(tableAnno.method.getName()+"Column"+".setSortable(true);");
		writer.println("}");
		
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.rebind.ColumnGenerator#createFieldUpdater(com.google.gwt.user.rebind.SourceWriter, com.google.gwt.core.ext.typeinfo.JClassType, com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields)
	 */
	@Override
	public void createFieldUpdater(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno)
	{
		String fieldName =tableAnno.method.getName();
		
        String entityName=entity.getSimpleSourceName();
        String datatype=tableAnno.method.getReturnType().getSimpleSourceName();
				
				writer.println("protected void addFieldUpdater"+fieldName+"()");
				writer.println("{");
				writer.println(fieldName+"Column.setFieldUpdater(new FieldUpdater<"+entityName+", Number>()");
				writer.println("{");
				writer.println("@Override");
				writer.println("public void update(int index,"+entityName+" "+"object,Number value)");
				writer.println("{");

				String mName=fieldName;
				mName=mName.replace("get","set");
				
				
				if(datatype.equals("int")||datatype.equals("Integer"))
				{
					writer.println("Integer val1=(Integer) value;");
				}
				
				if(datatype.equals("double")||datatype.equals("Double"))
				{
					writer.println("Double val1=(Double) value;");
				}
				
				if(datatype.equals("Long")||datatype.equals("long"))
				{
					writer.println("Long val1=(Long) value;");
				}
				
				if(datatype.equals("Float")||datatype.equals("float"))
				{
					writer.println("Float val1=(Float) value");
				}
				
				
				writer.println("object."+mName+"(val1);");
				writer.println("table.redrawRow(index);");
				writer.println("}");
				writer.println("});");
				writer.println("}");
	}

}
