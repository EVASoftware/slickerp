package com.simplesoftwares.rebind.columnwriters;


import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.SourceWriter;
import com.simplesoftwares.rebind.TableGenerators;
import com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields;


/**
 * Genrates the Column specifec code.
 */
public interface  ColumnGenerator {
	
	
	/**
	 * Creates the column.Since 
	 *
	 * @param writer  global writer for current class Generation
	 * @param entity  corresponding to which table is generated
	 * @param tableAnno column level annotation
	 */
	public abstract void createColumn(SourceWriter writer,JClassType entity, AnnotatnedFields tableAnno);
	public abstract void createFieldUpdater(SourceWriter writer,JClassType entity, AnnotatnedFields tableAnno);
	
	}
