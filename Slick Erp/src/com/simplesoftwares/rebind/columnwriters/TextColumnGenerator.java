package com.simplesoftwares.rebind.columnwriters;

import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JMethod;
import com.google.gwt.core.ext.typeinfo.JType;
import com.google.gwt.user.rebind.SourceWriter;
import com.simplesoftwares.rebind.TableGenerators;
import com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields;

/**
 * 
 * 
 */

public class TextColumnGenerator implements ColumnGenerator
{
	/** The cots. */
	String cots="\"";

	/* (non-Javadoc)
	 *  @param writer  global writer for current class Generation
	 * @param entity   corresponding to which table is generated
	 * @param tableAnno column level annotation
	 * 
	 * 
	 * The actual method creating columns of table.
	 */
	@Override
	public void createColumn(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno)
	{
		
		String entityName=entity.getSimpleSourceName();
		
		String fieldName =tableAnno.method.getName();
		writer.println("protected void addColumn"+tableAnno.method.getName()+"()");
		writer.println("{");
		writer.println(tableAnno.method.getName()+"Column" +"=" +"new"+" "+"TextColumn"+"<"+entityName+">"+"()");
		writer.println("{");
		writer.println("@Override");
		writer.println("public String getValue("+entityName+" "+"object)");
		writer.println("{");
		
		//String getMethod=gettingCorrespondingmethod(tableAnno,entity);
		
		String mName=fieldName;
		String newName="";
		for(int i=0;i<mName.length();i++)
		{
			if(i==0)
			{
				newName=newName+mName.charAt(0);
				newName=newName.toUpperCase();
			}
			else
				newName=newName+mName.charAt(i);
			
		}
		
	
		JType type=tableAnno.method.getReturnType();
		String name=type.getParameterizedQualifiedSourceName();
		if(name.contains("Bool"))
		{
			writer.println("if( object."+fieldName+"()==true)");
			writer.println("return "+cots+"Active"+cots+";");
			writer.println("else ");
				writer.println("return "+cots+"In Active"+cots+";");
		}
		
		else if(name.contains("int"))
		{
			writer.println("if( object."+fieldName+"()==-1)");
			writer.println("return "+cots+"N.A"+cots+";");
			writer.println("else return object."+fieldName+"()+"+cots+cots+";");
			
		}
		else
		  writer.println("return object."+fieldName+"()+"+cots+cots+";");
		writer.println("}");
		writer.println("};");
		
		writer.println("table.addColumn(" + tableAnno.method.getName()+"Column" +","+cots+tableAnno.tableAnnotation.title()+cots+");");
		
		
		if(tableAnno.tableAnnotation.isSortable()==true)
			writer.println(tableAnno.method.getName()+"Column"+".setSortable(true);");
		writer.println("}");
		
	}
	
	/**
	 * Gets the getting correspondingmethod.
	 *
	 * @param tableAnno the table anno
	 * @param entity the entity
	 * @return the getting corresponding method
	 */
	public String gettingCorrespondingmethod(AnnotatnedFields tableAnno,JClassType entity)
	{
		JMethod[] allmethods=entity.getMethods();
		String methodName="";
	    for(int j=0;j<allmethods.length;j++)
		{
			if((allmethods[j].getName()).contains("get"+tableAnno.method.getName()))
					methodName=allmethods[j].getName();
		}
		return methodName;
	}

	/**
	 * The actual method creating field Updater.
	 */
	@Override
	public void createFieldUpdater(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno)
	{
		
    }
	
	
	
}
