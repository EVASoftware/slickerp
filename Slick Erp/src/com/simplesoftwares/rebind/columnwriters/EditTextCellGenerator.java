package com.simplesoftwares.rebind.columnwriters;

import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.user.rebind.SourceWriter;
import com.simplesoftwares.rebind.TableGenerators;
import com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields;

// TODO: Auto-generated Javadoc
/**
 * Genrates EditTextColumn
 */
public class EditTextCellGenerator implements ColumnGenerator
{
	
	/** The cots. */
	String cots="\"";
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.rebind.ColumnGenerator#createColumn(com.google.gwt.user.rebind.SourceWriter, com.google.gwt.core.ext.typeinfo.JClassType, com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields)
	 */
	@Override
	public void createColumn(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno) 
	{
		String entityName=entity.getSimpleSourceName();
		String fieldName =tableAnno.method.getName();
		writer.println("protected void addColumn"+tableAnno.method.getName()+"()");
		writer.println("{");
		writer.println("EditTextCell editCell=new EditTextCell();");
		writer.println(fieldName+"Column=new Column<"+entityName+",String>(editCell)");
		writer.println("{");
		writer.println("@Override");
		writer.println("public String getValue("+entityName+" "+"object)");
		writer.println("{");
		
		writer.println("return object."+fieldName+"()+"+cots+cots+";");
		writer.println("}");
		writer.println("};");
		writer.println("table.addColumn("+fieldName+"Column,"+cots+tableAnno.tableAnnotation.title()+cots+");");
		if(tableAnno.tableAnnotation.isSortable()==true)
			writer.println(tableAnno.method.getName()+"Column"+".setSortable(true);");
		writer.println("}");
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.rebind.ColumnGenerator#createFieldUpdater(com.google.gwt.user.rebind.SourceWriter, com.google.gwt.core.ext.typeinfo.JClassType, com.simplesoftwares.rebind.TableGenerators.AnnotatnedFields)
	 */
	@Override
	public void createFieldUpdater(SourceWriter writer, JClassType entity,AnnotatnedFields tableAnno) 
	{
	
		String fieldName =tableAnno.method.getName();
        String entityName=entity.getSimpleSourceName();
        
				
				writer.println("protected void addFieldUpdater"+fieldName+"()");
				writer.println("{");
				writer.println(fieldName+"Column.setFieldUpdater(new FieldUpdater<"+entityName+", String>()");
				writer.println("{");
				writer.println("@Override");
				writer.println("public void update(int index,"+entityName+" "+"object,String value)");
				writer.println("{");

				String mName=fieldName;
				mName.replace("get", "set");
				writer.println("object."+mName+"(value);");
				writer.println("table.redrawRow(index);");
				writer.println("}");
				writer.println("});");
				writer.println("}");
	}

}
