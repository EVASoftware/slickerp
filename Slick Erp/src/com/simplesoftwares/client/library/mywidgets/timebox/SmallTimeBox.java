package com.simplesoftwares.client.library.mywidgets.timebox;

import java.util.Date;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;
public class SmallTimeBox extends TimeBox{
	private static final String STYLE_TIMEPICKER = "timePicker";
	private static final String STYLE_TIMEPICKER_ENTRY = "timePickerEntrySmall";
	private static final String STYLE_TIMEPICKER_READONLY = "timePickerReadOnlySmall";
	private static final ImageResource IMG_TIMEPICKER_AM = ResourcesBundle.INSTANCE.timePickerAMSmall();
	private static final ImageResource IMG_TIMEPICKER_PM = ResourcesBundle.INSTANCE.timePickerPMSmall();
	
	public SmallTimeBox(Date time) {
		this(time, false);
	}

	public SmallTimeBox(Date time, boolean useAMPM) {
		this(time, TIME_PRECISION.MINUTE, useAMPM);
	}
	
	public SmallTimeBox(Date time, TIME_PRECISION precision, boolean useAMPM) {
		super(time, precision, useAMPM);
	}

	protected String getStyleTimePickerEntry() {
		return STYLE_TIMEPICKER_ENTRY;
	}

	protected String getStyleTimePicker() {
		return STYLE_TIMEPICKER;
	}

	protected String getStyleTimePickerReadOnly() {
		return STYLE_TIMEPICKER_READONLY;
	}

	protected Image getStyleTimePickerAM() {
		return new Image(IMG_TIMEPICKER_AM);
	}

	protected Image getStyleTimePickerPM() {
		return new Image(IMG_TIMEPICKER_PM);
	}
}
