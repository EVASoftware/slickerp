package com.simplesoftwares.client.library.mywidgets;

import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.ValueBox;

public class NumbersOnly implements KeyPressHandler {
    @Override
    public void onKeyPress(KeyPressEvent event) 
    {
    	if(!(Character.isDigit(event.getCharCode()) ||event.getCharCode()=='.'))
        	{
    		ValueBox v=(ValueBox) event.getSource();
    		System.out.println("Char Code "+event.getCharCode());
    		v.cancelKey();
    		
    		}
    	
    	
    }
    
    
}