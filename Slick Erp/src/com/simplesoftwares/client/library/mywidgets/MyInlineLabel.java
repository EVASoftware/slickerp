package com.simplesoftwares.client.library.mywidgets;

import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.appskeleton.AbstractScreen;

public class MyInlineLabel extends InlineLabel {
	
	public AbstractScreen redirectScreen;
	public MyInlineLabel(String title,AbstractScreen screen)
	{
		super(title);
		redirectScreen=screen;
	}
	

}
