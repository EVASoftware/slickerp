package com.simplesoftwares.client.library.mywidgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.TextBox;


public class EmailTextBox extends TextBox implements ChangeHandler{
	
	public EmailTextBox() {
		super();
		this.addChangeHandler(this);
		
	}
	
	
	public boolean validate()
	{
		if(!this.getText().matches("^([a-zA-Z0-9_.\\-+])+@(([a-zA-Z0-9\\-])+\\.)+[a-zA-Z0-9]{2,4}$"))
		{
		   this.getElement().getStyle().setBackgroundColor("PINK");
		   return false;
		   
		}
		
		else
			return true;
	}


	@Override
	public void onChange(ChangeEvent event) {
		this.getElement().getStyle().setBackgroundColor("WHITE");
		
	}
}
