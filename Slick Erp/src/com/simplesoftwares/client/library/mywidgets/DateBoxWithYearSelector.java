package com.simplesoftwares.client.library.mywidgets;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;
import com.slicktechnologies.client.utils.Console;

public class DateBoxWithYearSelector extends DateBox implements ValueChangeHandler<Date>
{
	public DateBoxWithYearSelector()
	{
		super(new DatePickerWithYearSelector(),null,new DateBox.DefaultFormat());

		this.addValueChangeHandler(this);

	}
	
	/**
	 * @author Vijay Chougule
	 * Des :- to set time mid of the day to Date For GDS Report
	 */
	@Override
	public Date getValue() {
		Date date  = super.getValue();
		if(date==null){
			return super.getValue();
		}
		date.setHours(13);   // setting hours to 0
		date.setMinutes(00); // setting minutes to 0
		date.setSeconds(00); // setting seconds to 0
		return date;
	}

	/**
	 * @author Vijay Date :- 22-12-2020
	 * Des :- Date validation 
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		Console.log("on value change in datebox");
		DateTimeFormat dateformat1 = DateTimeFormat.getFormat("dd-MM-yyyy");
		DateTimeFormat dateformat2 = DateTimeFormat.getFormat("yyyy-MMM");
		DateTimeFormat dateformat3 = DateTimeFormat.getFormat("MMM-yyyy");
		DateTimeFormat dateformat4 = DateTimeFormat.getFormat("dd/MM/yyyy");

		boolean ddmmyyyyformatFlag = true;
		boolean mmmyyyyformatFlag = true;
		boolean yyyymmm = true;
		boolean ddmmyyformatFlag = true;
		
		
		DateBox db=(DateBox) this;
		Console.log("date value"+db.getValue());
		String strTextDate = db.getTextBox().getValue();
		Date value=db.getValue();
		GWTCAlert alert = new GWTCAlert();
		
		try {
			Date date =dateformat1.parse(strTextDate);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Console.log("ddmmyyyyformatFlag Exception ");
			ddmmyyyyformatFlag = false;
		}
		try {
			Date date2 =dateformat2.parse(strTextDate);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Console.log("mmmyyyyformatFlag Exception");
			mmmyyyyformatFlag = false;
		}
		
		try {
			Date date2 =dateformat3.parse(strTextDate);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Console.log("yyyymmm Exception");
			yyyymmm = false;
		}
		
		try {
			Date date2 =dateformat4.parse(strTextDate);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Console.log("dd/mm/yyyy Exception");
			ddmmyyformatFlag = false;
		}
		
		if(ddmmyyyyformatFlag ==false && mmmyyyyformatFlag == false && yyyymmm == false && ddmmyyformatFlag==false) {
			alert.alert("Invalid Date Format!");
			this.setValue(null);
		}
	
		if(ddmmyyyyformatFlag){
			
			if(value==null && strTextDate!=null && strTextDate.length()>0) {					
				alert.alert("Invalid Date Format!");
				this.setValue(null);
			}
			if(value!=null) {
				
				String strDate = "";
				try {
				DateTimeFormat dateformat = DateTimeFormat.getFormat("dd-MM-yyyy");
				strDate = dateformat.format(value);
				dateformat.parseStrict(strDate);
				} catch (Exception e) {
					alert.alert("Invalid Date Format!");
					this.setValue(null);

				}
				
				String strDateFormat = "dd-MM-yyyy";

				if(strDate.length()>10 || strDate.length()<10 || validateDate(value,strTextDate,db,strDateFormat)){
					alert.alert("Invalid Date Format! ");
					this.setValue(null);
				}
			}
		}
		else if (mmmyyyyformatFlag){
			DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MMM");
			String strDate = "";
			try {
			strDate = format.format(value);
			format.parseStrict(strDate);
			} catch (Exception e) {
				alert.alert("Invalid Date Format!");
				this.setValue(null);

			}
			if(strDate.length()>8 || strDate.length()<8 || validateMonthDate(value,strTextDate,db)){
				alert.alert("Invalid Date Format!");
				this.setValue(null);

			}
		}
		else if(yyyymmm){
			
			DateTimeFormat format=DateTimeFormat.getFormat("MMM-yyyy");
			String strDate = "";
			try {
			strDate = format.format(value);
			format.parseStrict(strDate);
			} catch (Exception e) {
				alert.alert("Invalid Date Format!");
				this.setValue(null);
			}
			if(strDate.length()>8 || strDate.length()<8 || validateMonthDate(value,strTextDate,db,true)){
				alert.alert("Invalid Date Format!");
				this.setValue(null);

			}
		}
		else if(ddmmyyformatFlag) {
			
			if(value==null && strTextDate!=null && strTextDate.length()>0) {					
				alert.alert("Invalid Date Format!");
				this.setValue(null);
			}
			if(value!=null) {
				
				String strDate = "";
				try {
				DateTimeFormat dateformat = DateTimeFormat.getFormat("dd/MM/yyyy");
				strDate = dateformat.format(value);
				dateformat.parseStrict(strDate);
				} catch (Exception e) {
					alert.alert("Invalid Date Format!");
					this.setValue(null);

				}
				String strDateFormat = "dd/MM/yyyy";
				if(strDate.length()>10 || strDate.length()<10 || validateDate(value,strTextDate,db,strDateFormat)){
					alert.alert("Invalid Date Format! ");
					this.setValue(null);
				}
			}
		
		}
		
		
		
	}

	private boolean validateDate(Date value, String strTextDate, DateBox db, String strDateFormat) {
		
		if(strTextDate.length()>10 || strTextDate.length()<10 ){
			Console.log("strDateValueDD == "+strTextDate.length());
			return true;
		}
		
		String strsplitformat = "";
		String strDate = "";
		if(strDateFormat.equalsIgnoreCase("dd-MM-yyyy")) {
			strsplitformat = "-";
			DateTimeFormat dateformat = DateTimeFormat.getFormat("dd-MM-yyyy");
			 strDate = dateformat.format(value);
		}
		else {
			strsplitformat = "/";
			DateTimeFormat dateformat = DateTimeFormat.getFormat("dd/MM/yyyy");
			 strDate = dateformat.format(value);
		}
		Console.log("strsplitformat "+strsplitformat);
		try {
		Console.log("strDate"+strDate);
		
		String[] straarayDate = strDate.split("\\"+strsplitformat);
		String strDateValueDD = straarayDate[0];
		
		String strDateMMValue = straarayDate[1];


		String[] strArrayTextDate = strTextDate.split("\\"+strsplitformat);
		String strTextDateValueDD = strArrayTextDate[0];
		
		String strTextMMValue = strArrayTextDate[1];
		
		Console.log("strDateValueDD"+strDateValueDD);
		Console.log("strTextDateValueDD "+strTextDateValueDD);

		if(!strDateValueDD.equals(strTextDateValueDD)){
			Console.log("strDateValueDD == $$"+strTextDate.length());
			return true;
		}
		
		Console.log("strDateMMValue == "+strDateMMValue);
		Console.log("strTextMMValue == "+strTextMMValue);

		if(!strDateMMValue.equals(strTextMMValue)){
			Console.log("strTextMMValue == "+strTextMMValue);
			return true;
		}
		
		String strarraydate[] = strDate.split("\\"+strsplitformat);
		Console.log("1 = "+strarraydate[0].trim().matches("[0-9]+"));
		Console.log("2 = "+strarraydate[1].trim().matches("[0-9]+"));
		Console.log("3 = "+strarraydate[2].trim().matches("[0-9]+"));

		
		if(!strarraydate[0].trim().matches("[0-9]+") || 
				!strarraydate[1].trim().matches("[0-9]+") ||
				!strarraydate[2].trim().matches("[0-9]+")){
			Console.log("strDateValueDD == $$ 111 = "+strTextDate.length());

			return true;
		}
		if(Integer.parseInt(strarraydate[1].trim())<=0 || Integer.parseInt(strarraydate[1].trim())>12 ) {
			Console.log("Integer.parseInt(strarraydate[1].trim()) "+Integer.parseInt(strarraydate[1].trim()));
			return true;
		}
		
		} catch (Exception e) {
			Console.log("Exception = "+e.getMessage());
			return true;
		}
		
		
		return false;
	}
	
	private boolean validateMonthDate(Date value, String strTextDate, DateBox db) {

		DateTimeFormat dateformat = DateTimeFormat.getFormat("yyyy-MMM");
		String strDate = dateformat.format(value);
		Console.log("strDate"+strDate);
		
		String strarraydate[] = strDate.split("\\-");
		Console.log("1 = "+strarraydate[0].trim().matches("[0-9]+"));
		Console.log("2 = "+strarraydate[1].trim());
		if(strTextDate.length()>8 || strTextDate.length()<8 || !strarraydate[0].trim().matches("[0-9]+") 
								  || !checkMonth(strarraydate[1].trim())){
			Console.log("strDateValueDD == $$ 111 = "+strTextDate.length());
			return true;
		}
		
		return false;
	}
	
	private boolean validateMonthDate(Date value, String strTextDate, DateBox db,boolean flag) {

		DateTimeFormat dateformat = DateTimeFormat.getFormat("MMM-yyyy");
		String strDate = dateformat.format(value);
		Console.log("strDate"+strDate);
		
		String strarraydate[] = strDate.split("\\-");
		Console.log("1 = "+strarraydate[1].trim().matches("[0-9]+"));
		Console.log("2 = "+strarraydate[0].trim());
		if(strTextDate.length()>8 || strTextDate.length()<8 || !strarraydate[1].trim().matches("[0-9]+") 
								  || !checkMonth(strarraydate[0].trim())){
			Console.log("strDateValueDD == $$ 111 = "+strTextDate.length());
			return true;
		}
		
		return false;
	}
	
	private boolean checkMonth(String DateMonthName) {
		
		if(DateMonthName.toUpperCase().equals("JAN") ||
				DateMonthName.toUpperCase().equals("FEB") ||
				DateMonthName.toUpperCase().equals("MAR") ||
				DateMonthName.toUpperCase().equals("APR") ||
				DateMonthName.toUpperCase().equals("MAY") ||
				DateMonthName.toUpperCase().equals("JUN") ||
				DateMonthName.toUpperCase().equals("JUL") ||
				DateMonthName.toUpperCase().equals("AUG") ||
				DateMonthName.toUpperCase().equals("SEP") ||
				DateMonthName.toUpperCase().equals("OCT") ||
				DateMonthName.toUpperCase().equals("NOV") ||
				DateMonthName.toUpperCase().equals("DEC")){
			Console.log("DateMonthName"+DateMonthName);
			return true;
		}
		return false;
	}
}
