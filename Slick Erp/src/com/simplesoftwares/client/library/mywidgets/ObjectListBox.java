package com.simplesoftwares.client.library.mywidgets;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.util.HSSFColor.GOLD;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.client.views.popups.GeneralPopup;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Department;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApproval;
import com.slicktechnologies.shared.common.multilevelapproval.MultilevelApprovalDetails;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;

public class ObjectListBox<T> extends ListBox implements HasValue<String>,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9017614910013890296L;

	protected List<T> items;
	
	private final GenricServiceAsync genricservice = GWT.create(GenricService.class);
	
	/**
	 * @author Anil @since 15-04-2021
	 */
	MyQuerry myQuerry=null;
	GeneralPopup genPopup = null;
	public String categoryName=null;
	public String employeeRole=null;
	
	public PersonInfo cinfo=null;
	public String customerBranchName="";
	public int customerBranchId=0;
	
	public ObjectListBox()
	{
		super();
		items=new ArrayList<T>();
		
		/**
		 * @author Anil @since 14-04-2021
		 * Adding change handler to objectlistbox
		 */
		myQuerry=null;
		if(LoginPresenter.myUserEntity!=null&&LoginPresenter.myUserEntity.getRoleName()!=null
				&&(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") 
				|| LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
			addChangeHandler(this);
		}
		
	}

	public ObjectListBox(List<T> items) {
		super();
		this.items = items;
		
		/**
		 * @author Anil @since 14-04-2021
		 * Adding change handler to objectlistbox
		 */
		myQuerry=null;
		if(LoginPresenter.myUserEntity!=null&&LoginPresenter.myUserEntity.getRoleName()!=null
				&&(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") 
				|| LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
			addChangeHandler(this);
		}
		
		setListItems(items);
	}
	
	/**
	 * @author Anil @since 14-04-2021
	 * Adding change handler to objectlistbox
	 */
	@SuppressWarnings("deprecation")
	private void addChangeHandler(final ObjectListBox<T> objectListBox) {
		
		addChangeListener(new ChangeListener() {
			@Override
			public void onChange(Widget sender) {
				if(myQuerry!=null && (myQuerry.getQuerryObject() instanceof Config==true 
						|| myQuerry.getQuerryObject() instanceof ConfigCategory==true
						|| myQuerry.getQuerryObject() instanceof Type==true
						|| myQuerry.getQuerryObject() instanceof City==true
						|| myQuerry.getQuerryObject() instanceof Locality==true
						|| myQuerry.getQuerryObject() instanceof Branch==true
						|| myQuerry.getQuerryObject() instanceof Employee==true
						|| myQuerry.getQuerryObject() instanceof CustomerBranchServiceLocation==true
						|| myQuerry.getQuerryObject() instanceof CustomerBranchDetails==true
						|| myQuerry.getQuerryObject() instanceof Department==true)){
					
					int selectedIndex=objectListBox.getSelectedIndex();
					int requiredIndex=1;
					
					if(selectedIndex==requiredIndex){
//						try{
							Console.log("Last item selected.. "+objectListBox.getValue(objectListBox.getSelectedIndex()));
							if(objectListBox.getValue(objectListBox.getSelectedIndex()).equals("New")){
								objectListBox.setSelectedIndex(0);
								int internalType = retrieveInternalType(myQuerry);
								Console.log("internalType "+internalType+" "+myQuerry.toString());
								
								if(myQuerry.getQuerryObject() instanceof Config==true){
									Console.log("Config");
									genPopup=new GeneralPopup(AppConstants.CONFIG, internalType);
								}else if(myQuerry.getQuerryObject() instanceof ConfigCategory==true){
									Console.log("ConfigCategory");
									genPopup=new GeneralPopup(AppConstants.CONFIGCATEGORY, internalType);
								}else if(myQuerry.getQuerryObject() instanceof Type==true){
									Console.log("Type");
									
									GWTCAlert alert=new GWTCAlert();
									List<Type>typeList=(List<Type>) items;
									List<Type>updatedList=new ArrayList<Type>();
									for(int i=2;i<getItemCount();i++){
										for(Type obj:typeList){
											if(obj.getTypeName().equals(getItemText(i))){
												updatedList.add(obj);
											}
										}
									}
									
									HashSet<String> categoryHs=new HashSet<String>();
									
									if(categoryName!=null&&!categoryName.equals("")){
									}else if(updatedList!=null&&updatedList.size()!=0){
										Console.log("typeList :: "+typeList.size());
										Console.log("updatedList :: "+updatedList.size());
										for(Type type:updatedList){
											categoryHs.add(type.getCategoryName());
										}
										Console.log("categoryHs :: "+categoryHs.size());
										if(categoryHs.size()==0){
											alert.alert("Please select category first!");
											return;
										}else if(categoryHs.size()>1){
											alert.alert("Please select category first!");
											return;
										}
										if(categoryHs.size()==1){
											for(String name:categoryHs){
												categoryName=name;
											}
										}
									}else{
										alert.alert("Please select category first!");
										return;
									}
									Console.log("Type :: "+categoryName);
									genPopup=new GeneralPopup(AppConstants.TYPE, internalType,categoryName);
								}else if(myQuerry.getQuerryObject() instanceof City==true){
									Console.log("City");
									genPopup=new GeneralPopup(AppConstants.CITY, internalType);
								}else if(myQuerry.getQuerryObject() instanceof Locality==true){
									Console.log("Locality");
									genPopup=new GeneralPopup(AppConstants.LOCALITY, internalType);
								}else if(myQuerry.getQuerryObject() instanceof Branch==true){
									Console.log("Branch");
									genPopup=new GeneralPopup(AppConstants.BRANCH, internalType);
								}else if(myQuerry.getQuerryObject() instanceof Employee==true){
									Console.log("Employee");
									genPopup=new GeneralPopup(AppConstants.EMPLOYEE, internalType);
								}else if(myQuerry.getQuerryObject() instanceof CustomerBranchServiceLocation==true){
									Console.log("CustomerBranchServiceLocation");
									genPopup=new GeneralPopup("CustomerBranchServiceLocation", internalType);
									genPopup.cinfo=cinfo;
									genPopup.customerBranchId=customerBranchId;
									genPopup.customerBranchName=customerBranchName;
								}
								else if(myQuerry.getQuerryObject() instanceof CustomerBranchDetails==true){
									Console.log("CustomerBranchDetails");
									genPopup=new GeneralPopup("CustomerBranchDetails", internalType);
									System.out.println("cinfo =="+cinfo.getCount());
									genPopup.pinfo=cinfo;
								}
								else if(myQuerry.getQuerryObject() instanceof Department==true){
									Console.log("Department");
									genPopup=new GeneralPopup("Department", internalType);
								}
								
								genPopup.showPopUp();
								/**
								 * @author Anil @since 20-07-2021
								 * Issue raised by Rutuza New functionaluty not working on compalain screen
								 */
								genPopup.getPopup().getElement().getStyle().setZIndex(1000001);
								
								genPopup.setModuleName(LoginPresenter.currentModule);
								genPopup.setDocumentName(LoginPresenter.currentDocumentName);
								genPopup.setEmployeeRole(employeeRole);
								
								genPopup.getLblOk().addClickListener(new ClickListener() {
									@Override
									public void onClick(Widget sender) {
										if(genPopup.validate()){
											genPopup.validateData(objectListBox);
											genPopup.hidePopUp();
										}
									}
								});
								
								
								if(myQuerry.getQuerryObject() instanceof CustomerBranchServiceLocation==true){
									genPopup.getLblCancel().addClickListener(new ClickListener() {
										
										@Override
										public void onClick(Widget sender) {
											// TODO Auto-generated method stub
											objectListBox.setSelectedIndex(0);
											genPopup.hidePopUp();
										}
									});
								}
							}
//						}catch(Exception e){
//							Console.log("outer Exception-1 "+e.getLocalizedMessage());
//						}
					}
				}
			}
		});
		
		
	}
	
	public void setListItems(List<T>vec)
	{
		removeAllItems();
		
		addItem("--SELECT--");
		
		/**
		 * @author Anil @since 14-04-2021
		 * Hard coded last element
		 */
		if(LoginPresenter.myUserEntity!=null&&LoginPresenter.myUserEntity.getRoleName()!=null
				&&(LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("ADMIN") 
				|| LoginPresenter.myUserEntity.getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
			if(myQuerry!=null && (myQuerry.getQuerryObject() instanceof Config==true 
					|| myQuerry.getQuerryObject() instanceof ConfigCategory==true
					|| myQuerry.getQuerryObject() instanceof Type==true
					|| myQuerry.getQuerryObject() instanceof City==true
					|| myQuerry.getQuerryObject() instanceof Locality==true
					|| myQuerry.getQuerryObject() instanceof Branch==true
					|| myQuerry.getQuerryObject() instanceof Employee==true
					|| myQuerry.getQuerryObject() instanceof CustomerBranchServiceLocation==true
					|| myQuerry.getQuerryObject() instanceof Department==true )){
				
				addItem("New");
			}
		}
		
		items=vec;
		/**
		 * @author Anil
		 * @since 17-06-2020
		 */
		if(items!=null&&items.size()!=0&&items.get(0) instanceof Contract==false){
			Comparator<T> dropDownComparator = new Comparator<T>() {
				public int compare(T c1, T c2) {
				String configName1=c1.toString();
				String configName2=c2.toString();
				
				return configName1.compareTo(configName2);
				}
			};
			Collections.sort(items,dropDownComparator);
		}
		
		for(int i=0;i<items.size();i++)
		{
			
			if(i==0){
				if(items.get(i) instanceof CustomerBranchDetails){
					addItem("New");
				}
			}
			

			/**
			 * Date : 07-11-2017 BY ANIL
			 * updated service list box display item style
			 * for customer support/complain
			 */
			if(items.get(i) instanceof SalesLineItem){/** date 24/4/2018 added by komal for ashjoi **/
				SalesLineItem object=(SalesLineItem) items.get(i);
				String productDetails = object.getPrduct().getProductName();
				if(object.getPremisesDetails()!=null){
					productDetails+= "/"+object.getPremisesDetails();
				}
				addItem(productDetails);
			}else if(items.get(i) instanceof Service){
				Service object=(Service) items.get(i);
				String serviceDetails=object.getProductName()+"/"+AppUtility.parseDate1(object.getServiceDate())+"/"+object.getCount();
				addItem(serviceDetails);
			}
			else if(items.get(i) instanceof Contract){ //Ashwini Patil Date:14-12-2022 
				addItem(items.get(i).toString());
			}
			/*** Date 20-03-2019 by Vijay for NBHC CCPM contract renewal quotation id is mandatory with process config*****/
			else if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "EnableQuotationMandatoryForContractRenwal") && 
					items.get(i) instanceof Quotation){
				Quotation quotation = (Quotation) items.get(i);
				String quotaionIdandDate = quotation.getCount()+""+"/"+AppUtility.parseDate1(quotation.getQuotationDate());
				addItem(quotaionIdandDate);
			}
			else if(items.get(i) instanceof ContactPersonIdentification){
			
				ContactPersonIdentification contactperson = (ContactPersonIdentification) items.get(i);
				StringBuilder contactpersoninfo = new StringBuilder("");
				
				if(contactperson.getRole()!=null && !contactperson.getRole().equals("")){
					contactpersoninfo.append(contactperson.getRole()+" - ");
				}
				contactpersoninfo.append(contactperson.getName());
				
				addItem(contactpersoninfo.toString());

			}
			else{
				addItem(items.get(i).toString());
			}
		}
	}
	
	//   rohan added this for branch level restriction 
	//  this will restrict inserting --Select-- in branch drop down
	
	public void setListItemsWitoutSelect(List<T>vec)
	{
		removeAllItems();
		
		
		items=vec;
		
		Comparator<T> dropDownComparator = new Comparator<T>() {
			public int compare(T c1, T c2) {
			String configName1=c1.toString();
			String configName2=c2.toString();
			
			return configName1.compareTo(configName2);
			}
		};
		Collections.sort(items,dropDownComparator);
		
		
		for(int i=0;i<items.size();i++)
		{
			addItem(items.get(i).toString());
		}
		
	}
	
	public void removeAllItems()
	{
		items=new ArrayList<T>();
		int count=this.getItemCount()-1;
		
		for(int i=count;i>=0;i--)
		{
			removeItem(i);
		}
	}
	
	
	public T getSelectedItem()
	{
//		if(super.getSelectedIndex()==0)
//			return null;
//		else
//		  return items.get(getSelectedIndex()-1);
		
		if(super.getSelectedIndex()==0){
			return null;
		}else{
			try{
				int diff=1;
				if(items!=null&&items.size()!=0){
					if(items.get(0) instanceof Config 
							|| items.get(0) instanceof ConfigCategory 
							|| items.get(0) instanceof Type
							|| items.get(0) instanceof Locality
							|| items.get(0) instanceof City
							|| items.get(0) instanceof Branch
							|| items.get(0) instanceof Employee
							|| items.get(0) instanceof CustomerBranchServiceLocation
							|| items.get(0) instanceof CustomerBranchDetails
							|| items.get(0) instanceof Department){
						if(getItemText(1).equals("New")){
							diff=2;
						}
					}
				}
				return items.get(super.getSelectedIndex()-diff);
			}catch(Exception e){
				return null;
			}
		}
	}
	
	public void MakeLive(final MyQuerry query) {
		/**
		 * @author Anil @since 15-04-2021
		 */
		myQuerry=query;
		Console.log("MAKE LIVE : ");
		Timer timer = new Timer() {
			@Override
			public void run() {
				boolean classCheck = checkingClass(query);

				if (classCheck == true) {
					String className = retrieveClassName(query);
					int internalType = retrieveInternalType(query);

					setDropDown(className, internalType,null);
				} else {
					genricservice.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
						}

						@SuppressWarnings("unchecked")
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
							items = new ArrayList<T>();
							items.addAll((Collection<? extends T>) result);

							setListItems(items);
						}
					});
				}
			}
		};
		timer.schedule(1000);
	}
	
	
	public void makeApproverLive(final MyQuerry query, final String typeScreen) {
		Timer timer = new Timer() {
			@Override
			public void run() {
				setDropDownForApproval(typeScreen);
			}
		};
		timer.schedule(1000);
	}

	
	
	
	
	
	

	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
		return this.addValueChangeHandler(handler);
	}

	@Override
	public String getValue() 
	{
//		if(super.getSelectedIndex()==0)
//			return null;
//		else
//		  return items.get(super.getSelectedIndex()-1).toString();
		
		if(super.getSelectedIndex()==0){
			return null;
		}else{
			try{
				int diff=1;
				if(items!=null&&items.size()!=0){
					if(items.get(0) instanceof Config 
							|| items.get(0) instanceof ConfigCategory 
							|| items.get(0) instanceof Type
							|| items.get(0) instanceof Locality
							|| items.get(0) instanceof City
							|| items.get(0) instanceof Branch
							|| items.get(0) instanceof Employee
							|| items.get(0) instanceof CustomerBranchServiceLocation
							|| items.get(0) instanceof CustomerBranchDetails
							|| items.get(0) instanceof Department){
						if(getItemText(1).equals("New")){
							diff=2;
						}
					}
				}
				return items.get(super.getSelectedIndex()-diff).toString();
			}catch(Exception e){
				return null;
			}
		}
	}

	@Override
	public void setValue(String value) {
		int count = getItemCount();
		String item=value.toString();
		for(int i=0;i<count;i++)
		{
			if(item.trim().equals(getItemText(i).trim()))
			{
				setSelectedIndex(i);
				break;
			}
		}
	}

	@Override
	public void setValue(String value, boolean fireEvents) {
		
	}

	public List<T> getItems() {
		if(items!=null)
			return items;
		return null;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}
	
	
	
	/*************************************************************************************************/
	
	public boolean checkingClass(MyQuerry querry)
	{
		String sr[]=querry.getQuerryObject().getClass().getName().split("\\.");
		String className=sr[sr.length-1];
		boolean val=false;
		
		if(className.equals("Config"))
		{
			val = true;
		}
		else if(className.equals("ConfigCategory"))
		{
			val = true;
		}
		else if(className.equals("Type"))
		{
			val = true;
		}
		else if(className.equals("Employee"))
		{
			val = true;
		}
		else if(className.equals("Branch"))
		{
			val = true;
		}
		else if(className.equals("Country"))
		{
			val = true;
		}
		else if(className.equals("State"))
		{
			val = true;
		}
		else if(className.equals("City"))
		{
			val = true;
		}
		else if(className.equals("Locality"))
		{
			val = true;
		}
		else if(className.equals("ProcessName"))
		{
			val = true;
		}
		else if(className.equals("HrProject"))
		{
			val = true;
		}
		
		return val;
	}
	
	public String retrieveClassName(MyQuerry querry)
	{
		String sr[]=querry.getQuerryObject().getClass().getName().split("\\.");
		return sr[sr.length-1];
	}
	
	public int retrieveInternalType(MyQuerry querry)
	{
		Integer internalType=0;
		if(querry.getFilters()!=null)
		{
			for(int i=0;i<querry.getFilters().size();i++)
			{
				Object value=querry.getFilters().get(i).geFilterValue();
				if(value instanceof Integer){
					internalType=Integer.parseInt(value.toString());
				}
			}
		}
		else
		{
			internalType=0;
		}
		return internalType;
	}
	
	@SuppressWarnings("unchecked")
	public void setDropDown(String cName,int iType,String value)
	{
		items=new ArrayList<T>();
		if(cName.equals("Config"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Config> listConfig=LoginPresenter.globalConfig;
			
			Comparator<Config> configArrayComparator = new Comparator<Config>() {
				public int compare(Config c1, Config c2) {
				String configName1=c1.getName();
				String configName2=c2.getName();
				
				return configName1.compareTo(configName2);
				}
			};
			Collections.sort(listConfig,configArrayComparator);
			for(int i=0;i<listConfig.size();i++)
			{
				if(listConfig.get(i).getType()==iType)
				{
					//   rohan added this code for active Config loads only 
					if(listConfig.get(i).isStatus()){
					Config config=new Config();
					config.setCompanyId(listConfig.get(i).getCompanyId());
					config.setCount(listConfig.get(i).getCount());
					config.setDescription(listConfig.get(i).getDescription());
					config.setId(listConfig.get(i).getId());
					config.setName(listConfig.get(i).getName());
					config.setStatus(listConfig.get(i).isStatus());
					config.setType(listConfig.get(i).getType());
					// Date 26-02-2021 added by vijay for GST applicable or not for Number Range
					config.setGstApplicable(listConfig.get(i).isGstApplicable());
					lisModel.add(config);
				}
				}
			}
			
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("ConfigCategory"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<ConfigCategory> listCategory=LoginPresenter.globalCategory;
			Comparator<ConfigCategory> categoryArrayComparator = new Comparator<ConfigCategory>() {
				public int compare(ConfigCategory c1, ConfigCategory c2) {
				Integer catName1=c1.getInternalType();
				Integer catName2=c2.getInternalType();
				
				return catName1.compareTo(catName2);
				}
			};
			Collections.sort(listCategory,categoryArrayComparator);
			
			Comparator<ConfigCategory> categoryArayComparator = new Comparator<ConfigCategory>() {
				public int compare(ConfigCategory c1, ConfigCategory c2) {
				String catName11=c1.getCategoryName();
				String catName21=c2.getCategoryName();
				
				return catName11.compareTo(catName21);
				}
			};
			Collections.sort(listCategory,categoryArayComparator);
			
			
			
			for(int i=0;i<listCategory.size();i++)
			{
				if(listCategory.get(i).getInternalType()==iType)
				{
					//   rohan added this code for active Category loads only 
					if(listCategory.get(i).getStatus()){
					ConfigCategory category=new ConfigCategory();
					category.setCategoryCode(listCategory.get(i).getCategoryCode());
					category.setCategoryName(listCategory.get(i).getCategoryName());
					category.setCompanyId(listCategory.get(i).getCompanyId());
					category.setCount(listCategory.get(i).getCount());
					category.setDescription(listCategory.get(i).getDescription());
					category.setId(listCategory.get(i).getId());
					category.setInternalType(listCategory.get(i).getInternalType());
					category.setStatus(listCategory.get(i).getStatus());
					lisModel.add(category);
				}
				}
			}
			/**
			 * @author Vijay Date :- 27-06-2022
			 * For paymnet terms added hardcoded value as per Nitin Sir
			 */
			if(iType==25){
				ConfigCategory category=new ConfigCategory();
				category.setCategoryCode("001");
				category.setCategoryName(AppConstants.MONTHLYPARTIALFIRSTMONTH);
				category.setDescription("30");
				category.setInternalType(25);
				category.setStatus(true);
				lisModel.add(category);
				
				ConfigCategory category2=new ConfigCategory();
				category2.setCategoryCode("002");
				category2.setCategoryName(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD);
				category2.setDescription("30");
				category2.setInternalType(25);
				category2.setStatus(true);
				lisModel.add(category2);

			}
			
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("Type"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Type> listType=LoginPresenter.globalType;
			
			Comparator<Type> typeArrayComparator = new Comparator<Type>() {
				public int compare(Type c1, Type c2) {
				String typeName1=c1.getTypeName();
				String typeName2=c2.getTypeName();
				
				return typeName1.compareTo(typeName2);
				}
			};
			Collections.sort(listType,typeArrayComparator);
			
			for(int i=0;i<listType.size();i++)
			{
				if(listType.get(i).getInternalType()==iType)
				{
					
				//   rohan added this code for active types loads only 
				  if(listType.get(i).getStatus()){
					Type type=new Type();
					type.setCatCode(listType.get(i).getCatCode());
					type.setCategoryName(listType.get(i).getCategoryName());
					type.setCompanyId(listType.get(i).getCompanyId());
					type.setCount(listType.get(i).getCount());
					type.setDescription(listType.get(i).getDescription());
					type.setId(listType.get(i).getId());
					type.setInternalType(listType.get(i).getInternalType());
					type.setStatus(listType.get(i).getStatus());
					type.setTypeCode(listType.get(i).getTypeCode());
					type.setTypeName(listType.get(i).getTypeName());
					
					lisModel.add(type);
				}
				}
			}
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("Employee"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Employee> listEmployee=LoginPresenter.globalEmployee;
			/**
			 * rohan added this code for inactive not load 
			 */
			ArrayList<Employee> aciveEmployeeList = new ArrayList<Employee>();
			/**
			 * ends here   
			 */
			Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
				public int compare(Employee c1, Employee c2) {
				String employeeName1=c1.getFullname();
				String employeeName2=c2.getFullname();
				
				return employeeName1.compareTo(employeeName2);
				}
			};
			Collections.sort(listEmployee,employeeArrayComparator);
			
			/**
			 * rohan added this code for inactive not load 
			 */
			for (int i = 0; i < listEmployee.size(); i++) {
				if(listEmployee.get(i).isStatus())
				{
					aciveEmployeeList.add(listEmployee.get(i));
				}
			}
			/**
			 * ends here  
			 */
			
			
			lisModel.addAll(aciveEmployeeList);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("Branch"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Branch> listBranch=LoginPresenter.globalBranch;
			/**
			 * rohan added this code for inactive not load 
			 */
			ArrayList<Branch> aciveBranchList = new ArrayList<Branch>();
			/**
			 * ends here   
			 */
			Comparator<Branch> branchArrayComparator = new Comparator<Branch>() {
				public int compare(Branch c1, Branch c2) {
				String branchName1=c1.getBusinessUnitName();
				String branchName2=c2.getBusinessUnitName();
				
				return branchName1.compareTo(branchName2);
				}
			};
			Collections.sort(listBranch,branchArrayComparator);
			
			/**
			 * rohan added this code for inactive not load 
			 */
			
			for(int j=0 ; j<LoginPresenter.globalBranch.size();j++)
			{
				/**
				 * @author Anil @since 22-10-2021
				 * Commented this unwanted code, which loads same name branch multiple time
				 * raised by Poonam for Indogulf
				 */
//				System.out.println("status:"+LoginPresenter.globalBranch.get(j).getstatus());
//				for (int i = 0; i < listBranch.size(); i++) 
//				{
//					if(LoginPresenter.globalBranch.get(j).getBusinessUnitName().equalsIgnoreCase(listBranch.get(i).getBusinessUnitName())){
						if(LoginPresenter.globalBranch.get(j).getstatus())
						{
							aciveBranchList.add(LoginPresenter.globalBranch.get(j));
						}
//					}
//			   }
			}
			/**
			 * ends here  
			 */
			System.out.println("branchList "+listBranch.size());
			System.out.println("active branch load "+aciveBranchList.size());
			lisModel.addAll(aciveBranchList);
			items.addAll((Collection<? extends T>) lisModel);
			
				setListItems(items);
			
		}
		
		if(cName.equals("Country"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Country> listCountry=LoginPresenter.globalCountry;
			Comparator<Country> countryArrayComparator = new Comparator<Country>() {
				public int compare(Country c1, Country c2) {
				String countryName1=c1.getCountryName();
				String countryName2=c2.getCountryName();
				
				return countryName1.compareTo(countryName2);
				}
			};
			Collections.sort(listCountry,countryArrayComparator);
			
			
			lisModel.addAll(listCountry);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("State"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<State> listState=LoginPresenter.globalState;
			
			/**
			 * rohan added this code for inactive not load 
			 */
			ArrayList<State> aciveStateList = new ArrayList<State>();
			/**
			 * ends here   
			 */
			
			Comparator<State> stateArrayComparator = new Comparator<State>() {
				public int compare(State c1, State c2) {
				String stateName1=c1.getStateName();
				String stateName2=c2.getStateName();
				
				return stateName1.compareTo(stateName2);
				}
			};
			Collections.sort(listState,stateArrayComparator);
			

			/**
			 * rohan added this code for inactive not load 
			 */
			for (int i = 0; i < listState.size(); i++) {
				if(listState.get(i).isStatus())
				{
					aciveStateList.add(listState.get(i));
				}
			}
			/**
			 * ends here  
			 */
			
			
			lisModel.addAll(aciveStateList);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("City"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<City> listCity=LoginPresenter.globalCity;
			
			/**
			 * rohan added this code for inactive not load 
			 */
			ArrayList<City> aciveCityList = new ArrayList<City>();
			/**
			 * ends here   
			 */
			
			Comparator<City> cityArrayComparator = new Comparator<City>() {
				public int compare(City c1, City c2) {
				String cityName1=c1.getCityName();
				String cityName2=c2.getCityName();
				
				return cityName1.compareTo(cityName2);
				}
			};
			Collections.sort(listCity,cityArrayComparator);
			
			/**
			 * rohan added this code for inactive not load 
			 */
			for (int i = 0; i < listCity.size(); i++) {
				if(listCity.get(i).isStatus())
				{
					aciveCityList.add(listCity.get(i));
				}
			}
			/**
			 * ends here  
			 */
			
			lisModel.addAll(aciveCityList);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("Locality"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Locality> listLocality=LoginPresenter.globalLocality;
			
			/**
			 * rohan added this code for inactive not load 
			 */
			ArrayList<Locality> aciveLocalityList = new ArrayList<Locality>();
			/**
			 * ends here   
			 */
			
			Comparator<Locality> localityArrayComparator = new Comparator<Locality>() {
				public int compare(Locality c1, Locality c2) {
				String localityName1=c1.getLocality();
				String localityName2=c2.getLocality();
				
				return localityName1.compareTo(localityName2);
				}
			};
			Collections.sort(listLocality,localityArrayComparator);
			
			/**
			 * rohan added this code for inactive not load 
			 */
			for (int i = 0; i < listLocality.size(); i++) {
				if(listLocality.get(i).isStatus())
				{
					aciveLocalityList.add(listLocality.get(i));
				}
			}
			/**
			 * ends here  
			 */
			
			lisModel.addAll(aciveLocalityList);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("ProcessName"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<ProcessName> listProcessName=LoginPresenter.globalProcessName;
			
			Comparator<ProcessName> processNameArrayComparator = new Comparator<ProcessName>() {
				public int compare(ProcessName c1, ProcessName c2) {
				String processName1=c1.getProcessName();
				String processName2=c2.getProcessName();
				
				return processName1.compareTo(processName2);
				}
			};
			Collections.sort(listProcessName,processNameArrayComparator);
			
//			lisModel.addAll(listProcessName);
			/**
			 * Date : 30-10-2017 By ANIL
			 * loading only active process name
			 */
			for(ProcessName obj:listProcessName){
				if(obj.isStatus()==true){
					lisModel.add(obj);
				}
				
			}
			/**
			 * End
			 */
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		
		/**
		 * @author Vijay Chougule
		 * Date :- 19-July-2020
		 * Des :- For HRProject Dropdown HRProject loading globally
		 */
		
		if(cName.equals("HrProject"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			final ArrayList<HrProject> listHRProject=LoginPresenter.globalHRProject;
			
			Comparator<HrProject> countryArrayComparator = new Comparator<HrProject>() {
				public int compare(HrProject c1, HrProject c2) {
				String projectName1=c1.getProjectName();
				String projectName2=c2.getProjectName();
				
				return projectName1.compareTo(projectName2);
				}
			};
			//Ashwini Patil Date:8-09-2022 added below condition so that if projects are not loaded at login time then they will get loaded at screen initialization
			Console.log("Global HRProjectlist size="+LoginPresenter.globalHRProject.size());
			if(listHRProject.size()>0) {
				
				Collections.sort(listHRProject,countryArrayComparator);
				
				
				lisModel.addAll(listHRProject);
				items.addAll((Collection<? extends T>) lisModel);
				setListItems(items);
			}
			else {
				Console.log("loading hrprojects");
				MyQuerry query=new MyQuerry();
				Filter temp1=new Filter();
				temp1.setQuerryString("status");
				temp1.setBooleanvalue(true);
				
				query.getFilters().add(temp1);
				query.setQuerryObject(new HrProject());
				final GenricServiceAsync genricservice = GWT.create(GenricService.class);
				
				genricservice.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>() {
					@Override
					public void onFailure(Throwable caught) {
						caught.printStackTrace();
					}

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						items = new ArrayList<T>();
						items.addAll((Collection<? extends T>) result);

						setListItems(items);
						listHRProject.addAll((Collection<? extends HrProject>) items);
						LoginPresenter.globalHRProject=listHRProject;
						
						Console.log("Global HRProjectlist size after loading="+LoginPresenter.globalHRProject.size());
		
					}
				});
					
			}
			
			
		}
		
		if(cName.equals("CustomerBranchServiceLocation")){
			Console.log("CustomerBranchServiceLocation : "+value);
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<CustomerBranchServiceLocation> serviceLocation=LoginPresenter.globalServiceLocationList;
			Comparator<CustomerBranchServiceLocation> countryArrayComparator = new Comparator<CustomerBranchServiceLocation>() {
				public int compare(CustomerBranchServiceLocation c1, CustomerBranchServiceLocation c2) {
					String projectName1=c1.getServiceLocation();
					String projectName2=c2.getServiceLocation();
					return projectName1.compareTo(projectName2);
				}
			};
			Collections.sort(serviceLocation,countryArrayComparator);
			
			
			lisModel.addAll(serviceLocation);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("CustomerBranchDetails")){
			Console.log("CustomerBranchDetails : "+value);
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<CustomerBranchDetails> customerBranch=LoginPresenter.globalCustomerBranchList;
			Comparator<CustomerBranchDetails> countryArrayComparator = new Comparator<CustomerBranchDetails>() {
				public int compare(CustomerBranchDetails c1, CustomerBranchDetails c2) {
					String branchName1=c1.getBusinessUnitName();
					String branchName2=c2.getBusinessUnitName();
					return branchName2.compareTo(branchName1);
				}
			};
			Collections.sort(customerBranch,countryArrayComparator);
			
			
			lisModel.addAll(customerBranch);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(cName.equals("Department")){
			Console.log("Department : "+value);
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Department> department=LoginPresenter.globalDepartmentList;
			Comparator<Department> countryArrayComparator = new Comparator<Department>() {
				public int compare(Department c1, Department c2) {
					String branchName1=c1.getDeptName();
					String branchName2=c2.getDeptName();
					return branchName2.compareTo(branchName1);
				}
			};
			Collections.sort(department,countryArrayComparator);
			
			lisModel.addAll(department);
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		
		if(value!=null){
			Console.log("Set drop down value : "+value);
			try{
				String strArray[]=null;
				if(value.contains("$")){
					strArray=value.split("\\$");
					
					if(strArray[0].equalsIgnoreCase("Remove_Add_New")){
						if(getItemText(getItemCount()-1).equals("New")){
							removeItem(getItemCount()-1);
						}
					}
					
					for(int i=0;i<getItemCount();i++){
						if(getItemText(i).equals(strArray[1])){
							setSelectedIndex(i);
							break;
						}
					}
					
					Console.log("$ split "+strArray[1]);
					
					return;
				}
			}catch(Exception e){
				
			}
			
			if(value.equalsIgnoreCase("Remove_Add_New")){
				if(getItemText(getItemCount()-1).equals("New")){
					removeItem(getItemCount()-1);
				}
			}
			
			for(int i=0;i<getItemCount();i++){
				if(getItemText(i).equals(value)){
					setSelectedIndex(i);
					break;
				}
			}
			
			if(!value.equalsIgnoreCase("Remove_Add_New")){
				DomEvent.fireNativeEvent(Document.get().createChangeEvent(), this);
				Console.log("Change event fired"+value);
			}
		}
		/**
		 * ends here
		 */
		
		
	}
	
	public void fireChangeEvevnt(){
		DomEvent.fireNativeEvent(Document.get().createChangeEvent(), this);
		Console.log("Change event fired");
	}
	
	//  used for multilevel amount loading 
	public void setApproverDropDownAsPerDocumentAmount(MultilevelApproval multipleApp,Double amount)
	{

		
		System.out.println("inside method setApproverDropDownAsPerDocumentAmount ");
		
		items=new ArrayList<T>();
		
		/**
		 * Date : 15-02-2017 By Anil
		 */
		boolean amountLevelFlag=false;
		ArrayList<MultilevelApprovalDetails> amountLevelApproverList=new ArrayList<MultilevelApprovalDetails>();
		/**
		 * End
		 */
		
		
		boolean multilevelStatus=multipleApp.isStatus();
		ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
		ArrayList<Employee> listEmployee=new ArrayList<Employee>();
		
		
		////////////////////////////////////////////////
		
		
//		for(int g=0;g<multipleApp.getApprovalLevelDetails().size();g++){
//			if(multilevelStatus){
//				if(multipleApp.getApprovalLevelDetails().get(g).getLevel().equals("1")){
//					
//					if(multipleApp.getApprovalLevelDetails().get(g).getAmountUpto()!=null){
//						if(multipleApp.getApprovalLevelDetails().get(g).getAmountUpto()>=amount){
//							listEmployee.add(getRoleEmployees(multipleApp.getApprovalLevelDetails().get(g).getEmployeeName()));
//						}
//					}else{
//						listEmployee.add(getRoleEmployees(multipleApp.getApprovalLevelDetails().get(g).getEmployeeName()));
//					}
//					
//				}
//			}else{
//				if(multipleApp.getApprovalLevelDetails().get(g).getAmountUpto()!=null){
//					if(multipleApp.getApprovalLevelDetails().get(g).getAmountUpto()>=amount){
//						listEmployee.add(getRoleEmployees(multipleApp.getApprovalLevelDetails().get(g).getEmployeeName()));
//					}
//				}else{
//					listEmployee.add(getRoleEmployees(multipleApp.getApprovalLevelDetails().get(g).getEmployeeName()));
//				}
//			}
//		}
		
		///////////////////////////////////////////////////////
		
		/**
		 * Checking if Multilevel approval is true or not
		 */
		if(multilevelStatus){
			
			/**
			 * Date : 27-02-2017 by ANIL
			 * here we are updating approver drop down as per branch and amount
			 */
			
			
			
			Console.log("3-MultiLevel Status True");
			for(MultilevelApprovalDetails obj:multipleApp.getApprovalLevelDetails()){
				if(obj.getLevel().equals("1")&&obj.getAmountUpto()!=null){
					amountLevelFlag=true;
					amountLevelApproverList.add(obj);
				}
			}
			if(!amountLevelFlag){
				Console.log("4-Amount Level Status False");
				for(MultilevelApprovalDetails obj:multipleApp.getApprovalLevelDetails()){
					if(obj.getLevel().equals("1")){
						if(getRoleEmployees(obj.getEmployeeName())!=null){
							listEmployee.add(getRoleEmployees(obj.getEmployeeName()));
						}
					}
				}
			}
		}else{
			Console.log("5-MultiLevel Status False");
			for(MultilevelApprovalDetails obj:multipleApp.getApprovalLevelDetails()){
				if(obj.getAmountUpto()!=null){
					amountLevelFlag=true;
					amountLevelApproverList.add(obj);
				}
			}
			if(!amountLevelFlag){
				Console.log("6-Amount Level Status False");
				for(MultilevelApprovalDetails obj:multipleApp.getApprovalLevelDetails()){
					if(getRoleEmployees(obj.getEmployeeName())!=null){
						listEmployee.add(getRoleEmployees(obj.getEmployeeName()));
					}
				}
			}
		}
		
		if(amountLevelFlag){
			Console.log("6-Amount Level Status True "+amountLevelApproverList.size()+"  "+amount);
			
			/**
			 * Date : 28-02-2017 By Anil
			 * Removing other branches approvers from approvers list 
			 */
			if(LoginPresenter.branchRestrictionFlag==true
					&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("ADMIN")){
				for(int i=0;i<amountLevelApproverList.size();i++){
					boolean empFoundFlag=false;
					for(Employee emp:LoginPresenter.globalEmployee){
						if(emp.getFullname().equals(amountLevelApproverList.get(i).getEmployeeName())){
							empFoundFlag=true;
						}
					}
					if(!empFoundFlag){
						amountLevelApproverList.remove(i);
						i--;
					}
				}
			}
			Console.log("6 A-Branch Restricted Approvers list  "+amountLevelApproverList.size()+"  "+amount);
			/**
			 * End
			 */
			
			Comparator<MultilevelApprovalDetails> approvalComparator = new Comparator<MultilevelApprovalDetails>() {
				public int compare(MultilevelApprovalDetails c1, MultilevelApprovalDetails c2) {
					Double amount1=c1.getAmountUpto();
					Double amount2=c2.getAmountUpto();
					return amount1.compareTo(amount2);
				}
			};
			Collections.sort(amountLevelApproverList,approvalComparator);
			
			Double maxAmount = null;
			for(int i=0;i<amountLevelApproverList.size();i++){
				Console.log("7 B-  "+amountLevelApproverList.get(i).getEmployeeName()+" "+amountLevelApproverList.get(i).getAmountUpto()+"  DOC AMT: "+amount);
				if(LoginPresenter.branchRestrictionFlag==true
						&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("ADMIN")){
					if(maxAmount!=null){
						if(amountLevelApproverList.get(i).getAmountUpto()>=amount&&amountLevelApproverList.get(i).getAmountUpto()<=maxAmount){
							maxAmount=amountLevelApproverList.get(i).getAmountUpto();
							if(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName())!=null){
								listEmployee.add(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName()));
							}
						}
					}else{
						if(amountLevelApproverList.get(i).getAmountUpto()>=amount){
							maxAmount=amountLevelApproverList.get(i).getAmountUpto();
							if(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName())!=null){
								listEmployee.add(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName()));
							}
						}
					}
				}else{
					if(amountLevelApproverList.get(i).getAmountUpto()>=amount){
						if(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName())!=null){
							listEmployee.add(getRoleEmployees(amountLevelApproverList.get(i).getEmployeeName()));
						}
					}
				}
			}
		}
		/**
		 * 
		 */
		
		Console.log("7-Approver List Size "+listEmployee.size());
							
		Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
			public int compare(Employee c1, Employee c2) {
				String employeeName1=c1.getFullname();
				String employeeName2=c2.getFullname();
							
				return employeeName1.compareTo(employeeName2);
			}
		};
		Collections.sort(listEmployee,employeeArrayComparator);
		
		removeDuplicateApproverForDocumentLevelApproval(listEmployee);
							
		lisModel.addAll(listEmployee);
		items.addAll((Collection<? extends T>) lisModel);
		setListItems(items);
		
		
		if(listEmployee.size()==0){
			GWTCAlert alert=new GWTCAlert();
			alert.alert("No employee found for approval.");
		}else{
			if(amountLevelFlag){
				String amountLevelApproversName=listEmployee.get(0).getFullname();
				Console.log("8-Approver Name "+amountLevelApproversName);
				setValue(amountLevelApproversName);
			}
		}
		
	}
	

	@SuppressWarnings("unchecked")
	public void setDropDownForApproval(String documentName)
	{

		items=new ArrayList<T>();
		
		ArrayList<ProcessName> processNme=LoginPresenter.globalProcessName;
		boolean flagApproval=false;
		for(int b=0;b<processNme.size();b++)
		{
			if(processNme.get(b).getProcessName().trim().equals(AppConstants.GLOBALRETRIEVALMULTILEVELAPPROVAL)&&processNme.get(b).isStatus()==true)
			{
				flagApproval=true;
			}
		}
		
		if(flagApproval==true){
			ArrayList<MultilevelApproval> approvalList=LoginPresenter.globalMultilevelApproval;
			int ctr=0;
			System.out.println("in object list box Approval list size()===== "+approvalList.size());
			Console.log("approval list ob box list size === "+approvalList.size());
			for(int y=0;y<approvalList.size();y++)
			{
				if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(documentName.trim())&&approvalList.get(y).isStatus()==true&&approvalList.get(y).isDocStatus()==true)
				{
					System.out.println("in ctr 1");
					Console.log("in ctr 1");
					ctr=1;
					setMultilevelApprovalDropDown(approvalList.get(y).getApprovalLevelDetails());
					
				}
				if(approvalList.get(y).getDocumentType().trim().equalsIgnoreCase(documentName.trim())&&approvalList.get(y).isStatus()==false&&approvalList.get(y).isDocStatus()==true)
				{
					ctr=2;
					setApprovalDropDown(approvalList.get(y).getApprovalLevelDetails());
				}
				
			}
			if(ctr==0){
				ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
				ArrayList<Employee> listEmployee=LoginPresenter.globalEmployee;
				
				Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
					public int compare(Employee c1, Employee c2) {
					String employeeName1=c1.getFullname();
					String employeeName2=c2.getFullname();
					
					return employeeName1.compareTo(employeeName2);
					}
				};
				Collections.sort(listEmployee,employeeArrayComparator);
				
				lisModel.addAll(listEmployee);
				items.addAll((Collection<? extends T>) lisModel);
				setListItems(items);
				
			}
		}else{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Employee> listEmployee=LoginPresenter.globalEmployee;
			
			Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
				public int compare(Employee c1, Employee c2) {
				String employeeName1=c1.getFullname();
				String employeeName2=c2.getFullname();
				
				return employeeName1.compareTo(employeeName2);
				}
			};
			Collections.sort(listEmployee,employeeArrayComparator);
			
			/**
			 * Date 14-06-2018 By vijay
			 * Des :- Inactive employee also showing in approver Name drop down so i have checked here
			 * only active employee load in approver drop down
			 */
			for(Employee employee :listEmployee){
				if(employee.isStatus()){
					lisModel.add(employee);
				}
			}
//			lisModel.addAll(listEmployee);
			
			/**
			 * ends here
			 */
			
			items.addAll((Collection<? extends T>) lisModel);
			setListItems(items);
		}
		

	}
	
	public void setApprovalDropDown(List<MultilevelApprovalDetails> levelApproval)
	{
		ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
		ArrayList<Employee> listEmployee=new ArrayList<Employee>();
		for(int g=0;g<levelApproval.size();g++){
			if(getRoleEmployees(levelApproval.get(g).getEmployeeName())!=null){
				listEmployee.add(getRoleEmployees(levelApproval.get(g).getEmployeeName()));
			}
		}
							
		Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
			public int compare(Employee c1, Employee c2) {
				String employeeName1=c1.getFullname();
				String employeeName2=c2.getFullname();
							
				return employeeName1.compareTo(employeeName2);
			}
		};
		Collections.sort(listEmployee,employeeArrayComparator);
				
		removeDuplicateApproverForDocumentLevelApproval(listEmployee);
		
		lisModel.addAll(listEmployee);
		items.addAll((Collection<? extends T>) lisModel);
		setListItems(items);
		
		if(listEmployee.size()==0){
			GWTCAlert alert=new GWTCAlert();
			alert.alert("No employee found for approval.");
		}
	}
	
	public void setMultilevelApprovalDropDown(List<MultilevelApprovalDetails> levelApproval)
	{
		ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
		ArrayList<Employee> listEmployee=new ArrayList<Employee>();
		for(int g=0;g<levelApproval.size();g++){
			if(levelApproval.get(g).getLevel().equals("1")){
				if(getRoleEmployees(levelApproval.get(g).getEmployeeName())!=null){
					listEmployee.add(getRoleEmployees(levelApproval.get(g).getEmployeeName()));
				}
			}
		}
							
		Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
			public int compare(Employee c1, Employee c2) {
				String employeeName1=c1.getFullname();
				String employeeName2=c2.getFullname();
							
				return employeeName1.compareTo(employeeName2);
			}
		};
		Collections.sort(listEmployee,employeeArrayComparator);
				
		removeDuplicateApproverForDocumentLevelApproval(listEmployee);
		
		lisModel.addAll(listEmployee);
		items.addAll((Collection<? extends T>) lisModel);
		setListItems(items);
		
		if(listEmployee.size()==0){
			GWTCAlert alert=new GWTCAlert();
			alert.alert("No employee found for approval.");
		}
		
//		final DropDownServiceAsync async=GWT.create(DropDownService.class);
//		Company c=new Company();
//		ArrayList<MultilevelApprovalDetails> multiDetails=new ArrayList<MultilevelApprovalDetails>();
//		multiDetails.addAll(levelApproval);
//			async.getApprovalDropDown(c.getCompanyId(),multiDetails,1,new AsyncCallback<ArrayList<String>>() {
//
//					@Override
//					public void onFailure(Throwable caught) {
//						
//					}
//					@SuppressWarnings("unchecked")
//					@Override
//					public void onSuccess(ArrayList<String> result) {
//						
//						System.out.println("object list box  result size() === "+result.size());
//						
//						Console.log("object list box size()"+result.size());
//						
//						if(result.size()>0)
//						{
//							ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
//							ArrayList<Employee> listEmployee=new ArrayList<Employee>();
//							for(int g=0;g<result.size();g++)
//							{
//								listEmployee.add(getRoleEmployees(result.get(g)));
//							}
//							
//							Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
//								public int compare(Employee c1, Employee c2) {
//								String employeeName1=c1.getFullname();
//								String employeeName2=c2.getFullname();
//								
//								return employeeName1.compareTo(employeeName2);
//								}
//							};
//							Collections.sort(listEmployee,employeeArrayComparator);
//							
//							lisModel.addAll(listEmployee);
//							items.addAll((Collection<? extends T>) lisModel);
//							setListItems(items);
//						}
//						else{
//							
//							Console.log("in else condition alert");
//							GWTCAlert alert=new GWTCAlert();
//							alert.alert("No employee found for approval.");
//						}
//					}
//				});
				 
	}
	
	
	public Employee getRoleEmployees(String empName)
	{
		Employee emp=null;
		for(int r=0;r<LoginPresenter.globalEmployee.size();r++)
		{
			if(LoginPresenter.globalEmployee.get(r).getFullname().trim().equals(empName))
			{
				emp=LoginPresenter.globalEmployee.get(r);
			}
		}
		
		return emp;
	}


	/**
	 * This method returns unique list of approver.
	 * 
	 * Date 09-09-2016 By Anil
	 * Release :31-Aug-2016
	 * Project: NBHC (Document Level Approver)
	 * I/P: This method take list of approver as i/p
	 * O/p: Unique Approver list.
	 * 
	 */
	
	private void removeDuplicateApproverForDocumentLevelApproval(ArrayList<Employee> listEmployee) {
		
		HashSet<Employee> empList=new HashSet<Employee>(listEmployee);
		listEmployee.clear();
		listEmployee.addAll(empList);
	}
	/**
	 * END
	 */

	
	
	/**
	 * Developed by : Rohan Bhagde
	 * Used for : MAkin customer branch drop down live 
	 * date : 20-09-2016
	 */
	public void MakeLiveCustomerScreen(final MyQuerry qu) {
		myQuerry=qu;
		 Timer timer=new Timer() 
    	 {
			@Override
			public void run() 
			{
		boolean classCheck=checkingClass(qu);
		
		if(classCheck==true){
			String className=retrieveClassName(qu);
			int internalType=retrieveInternalType(qu);
			
			setDropDownForCustomerScreen(className,internalType,null);
		}
		else{
				genricservice.getSearchResult(qu,new AsyncCallback<ArrayList<SuperModel>>()
						{
								@Override
								public void onFailure(Throwable caught) {
									caught.printStackTrace();
								}

								@SuppressWarnings("unchecked")
								@Override
								public void onSuccess(ArrayList<SuperModel> result)
								{
									items=new ArrayList<T>();
									items.addAll((Collection<? extends T>) result);
									
								    setListItems(items);
								}
					});
				}
			}
			
    	 };
    	 timer.schedule(1000);
	}
	
	
	
	public void setDropDownForCustomerScreen(String className,int internalType,String value) {
		
		items=new ArrayList<T>();
		if(className.equals("Branch"))
		{
			ArrayList<SuperModel> lisModel=new ArrayList<SuperModel>();
			ArrayList<Branch> listBranch=LoginPresenter.customerScreenBranchList;
			
			//  rohan added this for not loading inactive branches in drop down

			/**
			 * rohan added this code for inactive not load 
			 */
			ArrayList<Branch> aciveBranchList = new ArrayList<Branch>();
			/**
			 * ends here   
			 */
			
			Comparator<Branch> branchArrayComparator = new Comparator<Branch>() {
				public int compare(Branch c1, Branch c2) {
				String branchName1=c1.getBusinessUnitName();
				String branchName2=c2.getBusinessUnitName();
				
				return branchName1.compareTo(branchName2);
				}
			};
			Collections.sort(listBranch,branchArrayComparator);
			
			

			/**
			 * rohan added this code for inactive not load 
			 */
			for (int i = 0; i < listBranch.size(); i++) {
				if(listBranch.get(i).getstatus())
				{
					aciveBranchList.add(listBranch.get(i));
				}
			}
			/**
			 * ends here  
			 */
			
			lisModel.addAll(aciveBranchList);
			items.addAll((Collection<? extends T>) lisModel);
			
			setListItems(items);

			if(value!=null){
				Console.log("setDropDownForCustomerScreen : "+value);
				try{
					String strArray[]=null;
					if(value.contains("$")){
						strArray=value.split("$");
						
						if(strArray[0].equalsIgnoreCase("Remove_Add_New")){
							if(getItemText(getItemCount()-1).equals("New")){
								removeItem(getItemCount()-1);
							}
						}
						
						for(int i=0;i<getItemCount();i++){
							if(getItemText(i).equals(strArray[1])){
								setSelectedIndex(i);
								break;
							}
						}
						
						Console.log("$ split "+strArray[1]);
						
						return;
					}
				}catch(Exception e){
					
				}
				
				if(value.equalsIgnoreCase("Remove_Add_New")){
					if(getItemText(getItemCount()-1).equals("New")){
						removeItem(getItemCount()-1);
					}
				}
				
				for(int i=0;i<getItemCount();i++){
					if(getItemText(i).equals(value)){
						setSelectedIndex(i);
						break;
					}
				}
				
				if(!value.equalsIgnoreCase("Remove_Add_New")){
					DomEvent.fireNativeEvent(Document.get().createChangeEvent(), this);
					Console.log("Change event fired"+value);
				}
			}
			
		}
	}
	
	
	/**
	 * Date :08-02-2017 By Anil
	 * This Method is Used to Load Employee List Box as per their Role form global employee list 
	 * if global employee list size is zero then it Load it from dataStore
	 * 
	 */
	
	public void makeEmployeeLive(final String moduleName,final String documentName,final String employeeRole)
	{
//		Timer timer = new Timer() {
//			@Override
//			public void run() {
				MyQuerry querry=new MyQuerry();
				Vector<Filter> temp=new Vector<Filter>();
				Filter filter=null;
				
//				filter=new Filter();
//				filter.setQuerryString("roleName");
//				filter.setStringValue(employeeRole);
//				temp.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("status");
				filter.setBooleanvalue(true);
				temp.add(filter);
				
				filter=new Filter();
				filter.setQuerryString("companyId");
				filter.setLongValue(UserConfiguration.getCompanyId());
				temp.add(filter);
				
				querry.setFilters(temp);
				querry.setQuerryObject(new Employee());
				
				myQuerry=querry;
				this.employeeRole=employeeRole;
				if (LoginPresenter.globalEmployee.size()!=0) {
					
					checkRoleDefinition(moduleName,documentName,employeeRole);
//					setEmployeeToDropDown(employeeRole);
				} else {
					genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
						}

						@SuppressWarnings("unchecked")
						@Override
						public void onSuccess(ArrayList<SuperModel> result) {
//							items = new ArrayList<T>();
//							items.addAll((Collection<? extends T>) result);
//							setListItems(items);
							
							if(result.size()!=0){
								for(SuperModel model:result){
									Employee entity=(Employee) model;
									LoginPresenter.globalEmployee.add(entity);
								}
								checkRoleDefinition(moduleName,documentName,employeeRole);
							}
						}
					});
				}
//			}
//		};
//		timer.schedule(1000);
	}
	/**
	 *Date : 14-02-2017 By Anil
	 *This method checks role define for particular type of drop down in global role list
	 *and load data according to it
	 * 
	 **/
	private void checkRoleDefinition(final String moduleName, final String documentName,final String roleType) {
		 boolean loadAllSalesPersonFlag= false;

		 Console.log("moduleName " + moduleName+" documentName "+documentName+" Role type -- " + roleType);
		 Console.log("Role type -- " + roleType);
		 Console.log("LoginPresenter.globalRoleDefinition.size() -- " + LoginPresenter.globalRoleDefinition.size());

		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableLoadAllEmployeeWithBranchLevelRestriction")){
			if(roleType.equals("Sales Person")){ 
				loadAllSalesPersonFlag = true;
			}
			else{
				loadAllSalesPersonFlag = false;
			}
		}

		
		final ArrayList<String> roleTypeList=new ArrayList<String>();
		if(LoginPresenter.globalRoleDefinition.size()!=0){
			for(RoleDefinition role:LoginPresenter.globalRoleDefinition){
				if(role.getModuleName().trim().equals(moduleName.trim())
						&&role.getDocumentName().trim().equals(documentName.trim())
						&&role.getRoleType().trim().equalsIgnoreCase(roleType)
						&&role.isStatus()==true){
					roleTypeList.addAll(role.getRoleList());
					
					/**
					 * Date 29-05-2018 by vijay for NBHC load all sales person employee dropdown 
					 */
					if(roleType.equals("Sales Person")){ 
						loadAllSalesPersonFlag = true;
					}else if(roleType.equals("Requested By")){ // ** Materail Moment n
						loadAllSalesPersonFlag = true;
					}
//					else if(roleType.equals("Operator")){
//						loadAllSalesPersonFlag = true;
//					}
					/**
					 * ends here
					 */
					
					break;
				}
			}
			Console.log("loadAllSalesPersonFlag =="+loadAllSalesPersonFlag);
			setEmployeeToDropDown(roleTypeList,loadAllSalesPersonFlag,null);
		}
		else{
			MyQuerry querry=new MyQuerry();
			Vector<Filter> temp=new Vector<Filter>();
			Filter filter=null;
			
			filter=new Filter();
			filter.setQuerryString("companyId");
			filter.setLongValue(UserConfiguration.getCompanyId());
			temp.add(filter);
			
			querry.setFilters(temp);
			querry.setQuerryObject(new RoleDefinition());
			genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
				@Override
				public void onFailure(Throwable caught) {
					caught.printStackTrace();
				}

				@SuppressWarnings("unchecked")
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					 boolean loadAllSalesPersonFlag= false;
					if(result.size()!=0){
						for(SuperModel model:result){
							RoleDefinition entity=(RoleDefinition) model;
							LoginPresenter.globalRoleDefinition.add(entity);
						}
						if(LoginPresenter.globalRoleDefinition.size()!=0){
							for(RoleDefinition role:LoginPresenter.globalRoleDefinition){
								if(role.getModuleName().trim().equals(moduleName.trim())
										&&role.getDocumentName().trim().equals(documentName.trim())
										&&role.getRoleType().trim().equalsIgnoreCase(roleType)
										&&role.isStatus()==true){
									roleTypeList.addAll(role.getRoleList());
									
									/**
									 * Date 29-05-2018 by vijay for NBHC load all sales person employee dropdown 
									 */
									if(roleType.equals("Sales Person")){ 
										loadAllSalesPersonFlag = true;
									}else if(roleType.equals("Requested By")){ // ** Materail Moment n
										loadAllSalesPersonFlag = true;
									}
//									else if(roleType.equals("Operator")){
//										loadAllSalesPersonFlag = true;
//									}
									/**
									 * ends here
									 */
									
									break;
								}
							}
						}
					}
					setEmployeeToDropDown(roleTypeList,loadAllSalesPersonFlag,null);
				}
			});
		}
	}

	/**
	 * Loading data in ObjectListBox from global employee list
	 * Date : 08-02-2017 By Anil
	 * @param loadAllSalesPersonFlag 
	 */
	
	public void setEmployeeToDropDown(ArrayList<String>roleTypeList, boolean loadAllSalesPersonFlag,String value){
		items = new ArrayList<T>();
		ArrayList<SuperModel> lisModel = new ArrayList<SuperModel>();
		
		/**
		 * Date 11-05-2018 
		 * Developer :Vijay
		 * Des :- This is for loading all sales person role employee for branch level restriction for Sales Person Drop Down with Process Config
		 * Requirements : NBHC
		 */
		boolean branchResTrict  = false;
		Console.log("Setemployee dropdown loadAllSalesPersonFlag "+loadAllSalesPersonFlag);
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("MaterialMovementNote", "LoadAllEmployees")){
			branchResTrict = true;
		}else if(LoginPresenter.myUserEntity.getRole().getRoleName().equals("ADMIN")){
			branchResTrict = true;
		}
		/** Date 03-01-2018 updated by vijay Bug:- sales person load withoout branch restriction for nbhc ***/
		else if(loadAllSalesPersonFlag){
			branchResTrict = true;
		}
		
		Console.log("Brancch restrict flag --- " + branchResTrict +" load all sales person flag --   " + loadAllSalesPersonFlag);
		boolean loadallemployeeInSalesPersonFlag = false;
		
		ArrayList<Employee> listEmployee;
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "EnableLoadAllEmployeeWithBranchLevelRestriction")){
			Console.log("in if Brancch restrict flag --- " + branchResTrict +" load all sales person flag --   " + loadAllSalesPersonFlag);
			Console.log("allEmployeeList list dt -- " + LoginPresenter.allEmployeeList.size());
			if(loadAllSalesPersonFlag  && branchResTrict){
				loadallemployeeInSalesPersonFlag = true;
				if(LoginPresenter.allEmployeeList.size()==0){
					Company company = new Company();
					LoginPresenter.loadAllEmployeeForBranchLevelRestriction(AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+company.getCompanyId());
				}
				listEmployee = LoginPresenter.allEmployeeList;
				Console.log("inside Loading all employee for sales person ");
			}else{
				listEmployee = LoginPresenter.globalEmployee;
			}
		}else{
			Console.log("else LoginPresenter.globalEmployee "+LoginPresenter.globalEmployee);
			listEmployee = LoginPresenter.globalEmployee;

		}
//		ArrayList<Employee> listEmployee = LoginPresenter.globalEmployee;

		/**
		 * ends here
		 */
		
		ArrayList<Employee> aciveEmployeeList = new ArrayList<Employee>();
		Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
			public int compare(Employee c1, Employee c2) {
				String employeeName1 = c1.getFullname();
				String employeeName2 = c2.getFullname();
				return employeeName1.compareTo(employeeName2);
			}
		};
		Collections.sort(listEmployee, employeeArrayComparator);
		Console.log("listEmployee"+listEmployee.size());
		Console.log("Role type list -- "+roleTypeList.size());
		Console.log("For Sales Person drop loading all employees"+loadallemployeeInSalesPersonFlag);

		if(roleTypeList.size()==0 || loadallemployeeInSalesPersonFlag){
			for (int i = 0; i < listEmployee.size(); i++) {
				if (listEmployee.get(i).isStatus()) {
					aciveEmployeeList.add(listEmployee.get(i));
				}
			}
		}else{
			/**
			 * @author Vijay Date:- 15-07-2023
			 * Des :- added if condition to load all employees in sales person drop down even if branch level restriction is active
			 */
			if(loadallemployeeInSalesPersonFlag){
				Console.log("For Sales Person drop loading all employees");
				for (int i = 0; i < listEmployee.size(); i++) {
					if (listEmployee.get(i).isStatus()) {
						aciveEmployeeList.add(listEmployee.get(i));
					}
				}
			}
			/**
			 * ends here
			 */
			else{
				Console.log("Role type list -- "+roleTypeList.size() +" listEmployee size -- " + listEmployee.size());
				for(String roleType:roleTypeList){
					Console.log("Role type list -- "+roleType);
					for (int i = 0; i < listEmployee.size(); i++) {
						if (listEmployee.get(i).isStatus()&&listEmployee.get(i).getRoleName().trim().equals(roleType.trim())) {
							aciveEmployeeList.add(listEmployee.get(i));
						}
					}
					Console.log("emp size list -- "+ aciveEmployeeList.size());
				}
			}
			
			
		}

		Console.log("active list -- " + aciveEmployeeList.size());
		lisModel.addAll(aciveEmployeeList);
		items.addAll((Collection<? extends T>) lisModel);
		setListItems(items);
		
		if(value!=null){
			Console.log("Set drop down value : "+value);
			try{
				String strArray[]=null;
				if(value.contains("$")){
					strArray=value.split("$");
					
					if(strArray[0].equalsIgnoreCase("Remove_Add_New")){
						if(getItemText(getItemCount()-1).equals("New")){
							removeItem(getItemCount()-1);
						}
					}
					
					for(int i=0;i<getItemCount();i++){
						if(getItemText(i).equals(strArray[1])){
							setSelectedIndex(i);
							break;
						}
					}
					
					Console.log("$ split "+strArray[1]);
					
					return;
				}
			}catch(Exception e){
				
			}
			
			if(value.equalsIgnoreCase("Remove_Add_New")){
				if(getItemText(getItemCount()-1).equals("New")){
					removeItem(getItemCount()-1);
				}
			}
			
			for(int i=0;i<getItemCount();i++){
				if(getItemText(i).equals(value)){
					setSelectedIndex(i);
					break;
				}
			}
			
			if(!value.equalsIgnoreCase("Remove_Add_New")){
				DomEvent.fireNativeEvent(Document.get().createChangeEvent(), this);
				Console.log("Change event fired"+value);
			}
		}
	}
	
	/** date 24/10/2017 added by komal for architect , pmc , contractor dropdown **/
	public void MakeContactPersonLive(final MyQuerry query){
		System.out.println("inside query ");
		Timer timer = new Timer(){
						@Override
						public void run() {
							genricservice.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>()
									{
											@Override
											public void onFailure(Throwable caught) {
												caught.printStackTrace();
											}

											@SuppressWarnings("unchecked")
											@Override
											public void onSuccess(ArrayList<SuperModel> result)
											{
												System.out.println("size : " + result.size());
												addToList(result);
											}
									});
						}
			
		};
		timer.schedule(1000);
	}
	
	private void addToList(ArrayList<SuperModel> result){
		ArrayList<SuperModel> lisModel = new ArrayList<SuperModel>();
		ArrayList<ContactPersonIdentification> listContactPerson = new ArrayList<ContactPersonIdentification>();
		
		for(SuperModel model:result){
			ContactPersonIdentification entity=(ContactPersonIdentification) model;
			listContactPerson.add(entity);
		}
		lisModel.addAll(listContactPerson);
		items.addAll((Collection<? extends T>) lisModel);
		setListItems(items);
	}
	/**
	 * End
	 */
	
	public void MakeLiveUniqueItems(final MyQuerry query)
	{
		 Timer timer=new Timer() 
    	 {
			@Override
			public void run() 
			{
		boolean classCheck=checkingClass(query);
		
		if(classCheck==true){
			String className=retrieveClassName(query);
			int internalType=retrieveInternalType(query);
			
			setDropDown(className,internalType,null);
		}
		else{
				genricservice.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>()
						{
								@Override
								public void onFailure(Throwable caught) {
									caught.printStackTrace();
								}

								@SuppressWarnings("unchecked")
								@Override
								public void onSuccess(ArrayList<SuperModel> result)
								{
									items=new ArrayList<T>();
								//	items.addAll((Collection<? extends T>) result);
									HashSet set1 = new HashSet();
									for(SuperModel s : result){
										if(s instanceof AccountingInterface){
											AccountingInterface accInterface = (AccountingInterface) s;							
											set1.add(accInterface.getDocumentUniqueId());
										}else if(s instanceof CNCVersion){/** date 21.1.2019 added by komal for unique cncversion **/
											CNCVersion cncVersion = (CNCVersion) s;
											set1.add(cncVersion.getVersion()+"");
										}
										System.out.println("hash set " + s);
									}
								//	set1.addAll((Collection<? extends T>) result);
									ArrayList list1 = new ArrayList(set1);
									items.addAll(list1);
									
								    setListItems(items);
								}
					});
				}
			}
    	 };
    	 timer.schedule(1000);
	
	}

	
	/**
	 * Date 19-01-2019 By Vijay
	 * Des :- For Zonal Head Login in search pop who employee are reporting to him they all are show in Sales person
	 * Drop Down including all branches
	 * Requirement :- NBHC CCPM
	 */
	public void makeEmployeeLiveAsperReportingTo(String LoggedInUserName) {
		
		items = new ArrayList<T>();
		ArrayList<SuperModel> lisModel = new ArrayList<SuperModel>();
		ArrayList<Employee> reportingToSalesPerson = new ArrayList<Employee>();
		
		System.out.println("allEmployeeList =="+LoginPresenter.allEmployeeList.size());
		
		for(Employee employee : LoginPresenter.allEmployeeList){
			if(employee.getReportsTo().equalsIgnoreCase(LoggedInUserName)){
				reportingToSalesPerson.add(employee);
			}
			/**** Date 28-03-2019 by Vijay Zonal user also show in sales person ****/
			if(employee.getFullname().equalsIgnoreCase(LoggedInUserName)){
				reportingToSalesPerson.add(employee);
			}
		}
		
		ArrayList<Employee> listEmployee  = reportingToSalesPerson;
		
		ArrayList<Employee> aciveEmployeeList = new ArrayList<Employee>();
		Comparator<Employee> employeeArrayComparator = new Comparator<Employee>() {
			public int compare(Employee c1, Employee c2) {
				String employeeName1 = c1.getFullname();
				String employeeName2 = c2.getFullname();
				return employeeName1.compareTo(employeeName2);
			}
		};
		Collections.sort(listEmployee, employeeArrayComparator);

		for (int i = 0; i < listEmployee.size(); i++) {
			if (listEmployee.get(i).isStatus()) {
				aciveEmployeeList.add(listEmployee.get(i));
			}
		}
		Console.log("emp size list -- "+ aciveEmployeeList.size());

		Console.log("active list -- " + aciveEmployeeList.size());
		lisModel.addAll(aciveEmployeeList);
		items.addAll((Collection<? extends T>) lisModel);
		setListItems(items);

	}
	/**
	 * ends here
	 */
	
	
	/**
	 * Date 18-06-2019 By Vijay
	 * For NBHC IM allow to load all cluster in Transfer To DropDown with process Config. even if branch level restriction
	 * and Should not show expiry warehouses
	 */

	public void MakeLiveWarehouse() {
		MyQuerry querry = new MyQuerry();
		Vector<Filter> filtervec=new Vector<Filter>();
		Filter filter = null;
		
		filter = new Filter();
		filter.setQuerryString("status");
		filter.setBooleanvalue(true);
		filtervec.add(filter);
		
		filter = new Filter();
		filter.setQuerryString("companyId");
		filter.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(filter);
		
		querry.setFilters(filtervec);
		querry.setQuerryObject(new WareHouse());
		
		genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
		{
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ArrayList<SuperModel> result)
			{
				
				ArrayList<SuperModel> modellist = new ArrayList<SuperModel>();
				for(SuperModel model : result){
					WareHouse warehouse = (WareHouse) model;
					if(!LoginPresenter.myUserEntity.getRole().getRoleName().trim().equalsIgnoreCase("ADMIN") 
							&& warehouse.getWarehouseExpiryDate() !=null && warehouse.getWarehouseExpiryDate().after(new Date())){
						for(BranchDetails branchName : warehouse.getBranchdetails()){
								for(Branch branch:LoginPresenter.globalBranch){
									if(branchName.getBranchName().trim().equalsIgnoreCase(branch.getBusinessUnitName().trim())){
										modellist.add(warehouse);
									}
								}
						}
						if(warehouse.getParentWarehouse().trim().equalsIgnoreCase("NBHC HO")){
							modellist.add(warehouse);
						}
					}
					else{
						if(warehouse.getWarehouseExpiryDate() !=null && warehouse.getWarehouseExpiryDate().after(new Date()) 
								|| warehouse.getParentWarehouse().trim().equalsIgnoreCase("NBHC HO")){
							modellist.add(warehouse);
						}
					}
				}
				items=new ArrayList<T>();
				items.addAll((Collection<? extends T>) modellist);
				setListItems(items);
			}
});
	}
	
	
	/***
	 * Date 22-05-2019 by Vijay Des :- for customer support service location
	 * branch with hardcode service address
	 * 
	 * @param serviceaddress
	 * @param personInfo
	 */
	public void MakeLiveCustomerBranchrScreen(final MyQuerry querry, final String serviceaddress,
			PersonInfo personInfo) {

		if (serviceaddress != null && serviceaddress.equalsIgnoreCase("Service Address")) {
			System.out.println("Inside Service Address");
			MyQuerry querry2 = new MyQuerry();
			Vector<Filter> filtervec = new Vector<Filter>();
			Filter filter;

			filter = new Filter();
			filter.setQuerryString("count");
			filter.setIntValue(personInfo.getCount());
			filtervec.add(filter);
			querry2.setFilters(filtervec);
			querry2.setQuerryObject(new Customer());
			genricservice.getSearchResult(querry2, new AsyncCallback<ArrayList<SuperModel>>() {

				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					for (SuperModel model : result) {
						Customer customer = (Customer) model;
						String serviceAddressCity = customer.getSecondaryAdress().getCity();

						reactOnCustomerBranch(querry, serviceAddressCity);
					}
				}

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub

				}
			});
		} else {
			reactOnCustomerBranch(querry, null);
		}

	}

	protected void reactOnCustomerBranch(MyQuerry querry, final String serviceAddressCity) {

		items = new ArrayList<T>();
		genricservice.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				if (serviceAddressCity != null) {
					CustomerBranchDetails branch = new CustomerBranchDetails();
					branch.setBusinessUnitName(serviceAddressCity);
					result.add(branch);
				}

				items.addAll((Collection<? extends T>) result);
				setListItems(items);
			}

			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}
	/**
	 * ends here
	 */
	
	public void MakeLiveWithDesendingSorting(final MyQuerry query)
	{
		 Timer timer=new Timer() 
    	 {
			@Override
			public void run() 
			{
		boolean classCheck=checkingClass(query);
		
		if(classCheck==true){
			String className=retrieveClassName(query);
			int internalType=retrieveInternalType(query);
			
			setDropDown(className,internalType,null);
		}
		else{
				genricservice.getSearchResult(query,new AsyncCallback<ArrayList<SuperModel>>()
						{
								@Override
								public void onFailure(Throwable caught) {
									caught.printStackTrace();
								}

								@SuppressWarnings("unchecked")
								@Override
								public void onSuccess(ArrayList<SuperModel> result)
								{
									
									Comparator<SuperModel> supermodelcomparator = new Comparator<SuperModel>() {
										public int compare(SuperModel c1, SuperModel c2) {
										
											Integer count1 = c1.getCount();
											Integer count2 = c2.getCount();
											
											return count2.compareTo(count1);
										}
									};
									Collections.sort(result,supermodelcomparator);
									
									items=new ArrayList<T>();
									items.addAll((Collection<? extends T>) result);
									
								    setListItems(items);
								}
					});
				}
			}
    	 };
    	 timer.schedule(1000);
    	}

	public void makeliveContactPerson(MyQuerry querry, final String personName,final String personEmail) {

		genricservice.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>()
				{
						@Override
						public void onFailure(Throwable caught) {
							caught.printStackTrace();
						}

						@SuppressWarnings("unchecked")
						@Override
						public void onSuccess(ArrayList<SuperModel> result)
						{
							
							Comparator<SuperModel> supermodelcomparator = new Comparator<SuperModel>() {
								public int compare(SuperModel c1, SuperModel c2) {
								
									Integer count1 = c1.getCount();
									Integer count2 = c2.getCount();
									
									return count2.compareTo(count1);
								}
							};
							Collections.sort(result,supermodelcomparator);
							System.out.println("conatct person result size"+result.size());
							if(!personName.trim().equals("") && !personEmail.trim().equals("")){
								System.out.println("inside customer email id adding");
								ContactPersonIdentification persondetails = new ContactPersonIdentification();
								persondetails.setName(personName);
								persondetails.setEmail(personEmail);
								result.add(persondetails);
							}
							System.out.println("conatct person result size == "+result.size());
 							items=new ArrayList<T>();
							items.addAll((Collection<? extends T>) result);
							System.out.println("items ="+items.size());
						    setListItems(items);
						}
			});
		
		
		
		
	}

	public void makeliveContactPerson(List<ContactPersonIdentification> list,String personName, String personEmailOrCellNumber, boolean cellNumberFlag) {
		

		Comparator<SuperModel> supermodelcomparator = new Comparator<SuperModel>() {
			public int compare(SuperModel c1, SuperModel c2) {
			
				Integer count1 = c1.getCount();
				Integer count2 = c2.getCount();
				
				return count2.compareTo(count1);
			}
		};
		Collections.sort(list,supermodelcomparator);
		System.out.println("lissssttt "+list.size());
		
		

		if(cellNumberFlag){
			if(personName!=null &&personEmailOrCellNumber !=null && !personName.trim().equals("") && !personEmailOrCellNumber.trim().equals("")){
				ContactPersonIdentification persondetails = new ContactPersonIdentification();
				persondetails.setName(personName);
				persondetails.setCell(Long.parseLong(personEmailOrCellNumber));
				list.add(persondetails);
			}
		}
		else{
			if(personName!=null &&personEmailOrCellNumber !=null && !personName.trim().equals("") && !personEmailOrCellNumber.trim().equals("")){
				ContactPersonIdentification persondetails = new ContactPersonIdentification();
				persondetails.setName(personName);
				persondetails.setEmail(personEmailOrCellNumber);
				list.add(persondetails);
			}
		}
		System.out.println("lissssttt == "+list.size());

		items=new ArrayList<T>();
		items.addAll((Collection<? extends T>) list);
		Console.log("Contact Person list == "+list.size());
	    setListItems(items);
	}

	public void makeliveEmailTemplate(List<EmailTemplate> emailtemplatelist) {

		Comparator<EmailTemplate> supermodelcomparator = new Comparator<EmailTemplate>() {
			public int compare(EmailTemplate c1, EmailTemplate c2) {
			
				return c2.getTemplateName().compareTo(c1.getTemplateName());
			}
		};
		Collections.sort(emailtemplatelist,supermodelcomparator);
		
		items=new ArrayList<T>();
		items.addAll((Collection<? extends T>) emailtemplatelist);
		
	    setListItems(items);
	}

	
	public void makeliveSMSTemplate(List<SmsTemplate> smstemplatelist) {

		Comparator<SmsTemplate> supermodelcomparator = new Comparator<SmsTemplate>() {
			public int compare(SmsTemplate c1, SmsTemplate c2) {
			
				return c2.getEvent().compareTo(c1.getEvent());
			}
		};
		Collections.sort(smstemplatelist,supermodelcomparator);
		
		items=new ArrayList<T>();
		items.addAll((Collection<? extends T>) smstemplatelist);
		
	    setListItems(items);
	}

}




	
	

