package com.simplesoftwares.client.library.mywidgets;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.text.client.LongParser;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.ValueBox;
import com.google.gwt.user.client.ui.Widget;

public class MyLongBox extends ValueBox<Long> {
	  public MyLongBox() {
	    super(Document.get().createTextInputElement(), 
	          new AbstractRenderer<Long>() {
	            public String render(Long l) {
	              return l == null ? "" : l.toString();
	            }
	          },
	          LongParser.instance());
	  }
	  
	  
	 
	}
