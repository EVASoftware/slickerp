package com.simplesoftwares.client.library.appskeleton;

/**
 * ScreenState abstarct the states on which screen could exist.
 * To Do : Ideally screen state should contain the logic common to each state. Komal pradnya and sumit can do that modification
 * 
 * @author Kamala
 *
 */
public enum ScreeenState 
{
	NEW,VIEW,EDIT;
}
