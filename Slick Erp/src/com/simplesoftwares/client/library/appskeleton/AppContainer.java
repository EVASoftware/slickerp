
package com.simplesoftwares.client.library.appskeleton;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class AppLayoutView.Provides outer structure of application.Is a subclass of Dock Layout panel
 * To Do : write now we use percentage as the base unit, this makes the UI perfect on most screens and also
 * on mobile devices.This approach has its disadvantage also as it will not handle browser resize properly.
 * The absolute units such as px may be needed.
 * @author Ajay
 * @version 1
 */
public class AppContainer extends DockLayoutPanel
{
	
	/**
	 * Instantiates a new Dock Lay out Panel
	 * Calls super class constructor.
	 *
	 * @param unit the unit of the doc lay out 
	 */
	private AppContainer(Unit unit) 
	{
		super(unit);
	}
	
	/**
	 * Instantiates a new app layout view.
	 * This method is responsible for setting the skeleton of application
	 *
	 * @param skeleton the skeleton object provides the three flow panels to {@link DockLayoutPanel} the provided 
	 * {@link FlowPanel} are NavigationBar SideBar and AppHeaderBar
	 * @param enableMenuBar 
	 */
	public AppContainer(AppSkeleton skeleton, boolean enableMenuBar)
	{
		this(Unit.PCT);
		this.addNorth(skeleton.getNavigationPanel(),skeleton.getNavigationPanelpct());
		/**
		 * @author Anil
		 * @since 11-09-2020
		 */
		this.addSouth(skeleton.getFooterBarPanel(),skeleton.getFooterBarPer());
		
		if(!enableMenuBar){
			this.addWest(skeleton.getSideBarPanel(),skeleton.getSidebarPanelpct());
		}

		this.addNorth(skeleton.getAppHeaderBarPanel(),skeleton.getAppHeaderBarPanelpct());
		
		
	}
}
