/*
 * 
 */

package com.simplesoftwares.client.library.appskeleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.simplesoftwares.client.library.ProcessLevelMenuBarPopup;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.ViewContainer;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.DsReport;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;


/**
 * The Class AppMemory.Manages initialization of application.Its attributes contains reference of 
 * <b>current screen</b>
 * all the menu items,current screen state.
 * To Do: Singleton design pattern is for name only a good design challenge for komal,pradnya,sumit.No practical
 * benifits though.
 * @author Ajay, Pradnya
 * @version 1.0
 * 
 */
public abstract class AppMemory
{
	/** Vector of menu items contains reference of all menu items corresponding to all screens.
	 *  Get initialized by Concrete Implementation.
	 * */
	public  Vector<MenuItems>items;
	public  Vector<NavigationBarItem>navItems;
	
	/** The Reference to outer holder of app */
	public AppContainer apl;
	
	/** Reference to current screen of application which user is working on particular instance */
	public  AbstractScreen currentScreen;
	
	/** Reference to current screen state of application which user is working on particular instance */
	public  ScreeenState currentState;
	
	/** Instance of class {@link AppSkeleton} This objects encapsulates refrences of Process Name ,AppLevelMenu
	 * etc.*/
	public AppSkeleton skeleton;
	
	/** The memory. Singleton object, only one instance will be created throughout an user session.
	 * 
	 * */
	protected static AppMemory memory;
	
	/**
	 * @author Anil
	 * @since 16-09-2020
	 */
	public boolean enableMenuBar=false;
	
	/**
	 * @author Anil
	 * @since 24-09-2020
	 * Process level menu bar app drawer panel
	 */
	public PopupPanel appdrawerPanel;
	
	/**
	 * @author Anil
	 * @since 25-09-2020
	 */
	ProcessLevelMenuBarPopup processMenuPopup;
	
	
	public static ArrayList<DsReport> globalDsReportList=new ArrayList<DsReport>();
	
	/**
	 * Instantiates a new app memory.
	 */
	protected AppMemory() 
	{
		items=new Vector<MenuItems>();
	}
	
	/**
	 * Removes the panel from {@link AppSkeleton} and stick new panel
	 *
	 * @param mv the mv
	 */
	public void stickPnel(ViewContainer mv)
	{
		/**
		 * @author Anil
		 * @since 11-09-2020
		 */
		Console.log("Total Widget in App container "+this.apl.getWidgetCount());
		
		if(enableMenuBar){
			if(this.apl.getWidgetCount()==4)
				apl.remove(3);
		}else{
			if(this.apl.getWidgetCount()==5)
				apl.remove(4);
		}
		mv.content.setSize("100%", "auto");
		apl.add(mv.content);
	}
	
	/**
	 * Change current screen,and sets the process name
	 *
	 * @param processname the processname to be applied
	 * @param screen Screen which will become current screen
	 */
	public  void setCurrentScreenandChangeProcessName(String processname,AbstractScreen screen)
	   {
		 this.currentScreen=screen;
		 skeleton.getProcessName().setText(processname); 
		 
		 /**
		  * @author Anil
		  * @since 21-12-2020
		  * 
		  */
		 if(AppMemory.getAppMemory().enableMenuBar){
			 String separateDocument=processname;
			 try{
				 /**
				  * @author Anil @since 04-01-2022
				  * Screen menu was not appearing on purchase module
				  * raised by Nitin Sir
				  */
				 if(separateDocument.contains("/")){
					 String[] docArray=separateDocument.split("/");
					 LoginPresenter.currentModule=docArray[0].trim();
					 LoginPresenter.currentDocumentName=docArray[1].trim();
				 }
			 }catch(Exception e){
				 LoginPresenter.currentDocumentName=separateDocument;
			 }
		 }
		 
		 Console.log("mdl - "+LoginPresenter.currentModule+" | doc - "+LoginPresenter.currentDocumentName);
		 
	   }
	
	/**
	 * Method creates the {@link AppContainer} and stick that on {@link RootPanel}
	 */
	private void setAppContainer()
	{
		RootLayoutPanel rp = RootLayoutPanel.get();
		rp.clear();
		rp.add(this.apl);	
    }
	
	/**
	 * Sets the list.
	 *
	 * @param item the item
	 * @param lb the lb
	 */
	public static void setList(String item,ListBox lb)
	{
		int count = lb.getItemCount();
		for(int i=0;i<count;i++)
		{
			if(item.trim().equals(lb.getItemText(i).trim()))
			{
				lb.setSelectedIndex(i);
				break;
			}
		}
	}

	/**
	 * Gets the app memory.
	 *
	 * @return the app memory
	 */
	public static AppMemory getAppMemory() 
	{
		return memory;
		
	}
	
	
	/**
	 * Creates the Application outer structure , also initalizes the {@link AppContainer} variable apl.
	 * In this  navigationbarPanel,SideBarPanel and headerbar panel are created.
	 */
	private void createSkeleton()
	{
		//Implementation provided by Concrete class the method returns vector of menu items for 
		//navigational panel
		Vector<NavigationBarItem> navimenus=createNewNavigationBar();
//		Vector<MenuItems> navimenus=createNavigationBar();
		//Implementat
		Vector<SideBarItems> sidemenus=createSideBar();
		String[] appmenus=getApplevelMenus();
		/**
		 * @author Anil
		 * @since 11-09-2020
		 */
//		skeleton=new AppSkeleton(5, 17, 15);
//		skeleton=new AppSkeleton(5,17,15,5);
		skeleton=new AppSkeleton(5,17,10,5,enableMenuBar);
		
		if(enableMenuBar){
			skeleton.createMenuBarPanel(navimenus);
		}else{
			skeleton.createNewNavigationPanel(navimenus);
		}
//		skeleton.createSideBarPanel(sidemenus);
		skeleton.createAppHeaderBarPanel(appmenus);
		
		skeleton.createFooterPanel();
		
		// Application Skeleton Initializes
		apl=new AppContainer(skeleton,enableMenuBar);
		
	}
	
	/**
	 * Utility method which creates and stick application skeleton
	 */
	public void initialingapp()
	{
		/**
		 * @author Anil, Date : 15-01-2020
		 * Activating session flag after successful login
		 */
//		Slick_Erp.isSessionActive=true;
//		checkSessionTimeOutProcess();
		
		checkHomeScreenConfiguration();
		
//		createSkeleton();
//		setAppContainer();
	}

	
	/**
	 * Creates the navigation bar.
	 *
	 * @return the vector
	 */
	protected abstract Vector<MenuItems> createNavigationBar();
	
	protected abstract Vector<NavigationBarItem> createNewNavigationBar();
	
	/**
	 * Creates the side bar.
	 *
	 * @return the vector
	 */
	protected abstract Vector<SideBarItems> createSideBar();
	
	/**
	 * Gets the applevel menus.
	 *
	 * @return the applevel menus
	 */
	protected abstract String[] getApplevelMenus();
	
    /**
     * Go to home.
     */
    public abstract void goToHome() ;

	
    public void checkSessionTimeOutProcess(){
    	GenricServiceAsync service=GWT.create(GenricService.class);
    	final Company company=new Company();
    	MyQuerry querry=new MyQuerry();
    	Filter filter=null;
    	Vector<Filter> filterList=new Vector<Filter>();
    	
    	List<String> processTypeList=new ArrayList<String>();
    	processTypeList.add("EnableMenuBar");
    	processTypeList.add("EnableSessionTimeOut");
    	
    	filter=new Filter();
    	filter.setQuerryString("companyId");
    	filter.setLongValue(company.getCompanyId());
    	filterList.add(filter);
    	
    	filter=new Filter();
    	filter.setQuerryString("processName");
    	filter.setStringValue("Company");
    	filterList.add(filter);
    	
//    	filter=new Filter();
//    	filter.setQuerryString("processList.processType");
//    	filter.setStringValue("EnableSessionTimeOut");
//    	filterList.add(filter);
//    	
//    	filter=new Filter();
//    	filter.setQuerryString("processList.status");
//    	filter.setBooleanvalue(true);
//    	filterList.add(filter);
    	
    	/**
    	 * @author Anil
    	 * @since 14-01-2021
    	 * due to in filter data is not getting searched if only one process type was added
    	 */
//    	filter=new Filter();
//    	filter.setQuerryString("processList.processType IN");
//    	filter.setList(processTypeList);
//    	filterList.add(filter);
    	
    	filter=new Filter();
    	filter.setQuerryString("configStatus");
    	filter.setBooleanvalue(true);
    	filterList.add(filter);
    	
    	querry.getFilters().addAll(filterList);
    	querry.setQuerryObject(new ProcessConfiguration());
    	
    	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				if(result!=null&&result.size()!=0){
					
					for(SuperModel model:result){
						ProcessConfiguration obj=(ProcessConfiguration) model;
						for(ProcessTypeDetails process:obj.getProcessList()){
							if(process.getProcessType().equalsIgnoreCase("EnableMenuBar")&&process.isStatus()==true){
								enableMenuBar=true;
							}
							if(process.getProcessType().equalsIgnoreCase("EnableSessionTimeOut")&&process.isStatus()==true){
								Slick_Erp.isSessionActive=true;
							}
							
						}
					}
//					Slick_Erp.isSessionActive=true;
				}
				
				/**
				 * @author Vijay Upload help UI Issue so called here after enableMenuBar value set
				 * @since 15-06-2021
				 */
				String screenMenuConfg=AppConstants.SCREENMENUCONFIGURATION+"-"+company.getCompanyId();
				LoginPresenter.loadSystemConfigurationMenu(screenMenuConfg);
				
				
				/**
				 * @author Anil
				 * @since 16-09-2020
				 */
				createSkeleton();
				setAppContainer();
				for(int i=0;i<skeleton.menuLabels.length;i++){
					skeleton.menuLabels[i].setVisible(false);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
			}
		});
    	
    	
    }
    
    public void checkHomeScreenConfiguration(){
    	GenricServiceAsync service=GWT.create(GenricService.class);
    	Company company=new Company();
    	MyQuerry querry=new MyQuerry();
    	
    	Filter filter=null;
    	Vector<Filter> filterList=new Vector<Filter>();
    	
    	filter=new Filter();
    	filter.setQuerryString("companyId");
    	filter.setLongValue(company.getCompanyId());
    	filterList.add(filter);
    	
    	filter=new Filter();
    	filter.setQuerryString("status");
    	filter.setBooleanvalue(true);
    	filterList.add(filter);
    	
    	querry.getFilters().addAll(filterList);
    	querry.setQuerryObject(new DsReport());
    	
    	service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				
				if(result!=null&&result.size()!=0){
					Console.log("Home Screen DS : "+result.size());
					for(SuperModel model:result){
						DsReport ds=(DsReport) model;
						globalDsReportList.add(ds);
					}
				}
				checkSessionTimeOutProcess();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				checkSessionTimeOutProcess();
			}
		});
    	
    	
    }
	
	
}
