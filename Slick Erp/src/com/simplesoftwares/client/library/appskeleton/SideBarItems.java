package com.simplesoftwares.client.library.appskeleton;

import java.util.Vector;

/**
 * Repersents a single  Sidebar Items.
 * @author Kamala
 *
 */
public class SideBarItems 
{
	/**
	 * Title of a single side bar item
	 */
	String title;
	/**
	 * Menuitem vector coresponding to that sidebar item
	 */
	protected Vector<MenuItems> sidebarmenu;

	
	/**
	 * 
	 * @param title title of the sidebar item
	 * @param menus Vector of Menu items
	 */

	public SideBarItems(String title, Vector<MenuItems> menus)
	{
		super();
		this.title = title;
		this.sidebarmenu = menus;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Vector<MenuItems> getMenus() {
		return sidebarmenu;
	}

	public void setMenus(Vector<MenuItems> menus) {
		this.sidebarmenu = menus;
	}
	
}


