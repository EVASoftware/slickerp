package com.simplesoftwares.client.library.appskeleton;

import java.util.ArrayList;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.*;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.ViewContainer;


// TODO: Auto-generated Javadoc
/**
 * The Class ProcessLevelBar.This class creates the view of process level bar.
 * To Do: Ideally this class should be Non Static Inner Class of the UiScreen.
 */
public class ProcessLevelBar extends ViewContainer
{
	
	/** The app header bar pct. */
	private int perCentSize;
	
	
	
	/** The menu nameslbls. */
	public InlineLabel[] btnLabels;
	
	/** The registration. */
	public HandlerRegistration[] registration;
	
	/**
	 * @author Anil
	 * @since 25-09-2020
	 * If EnableMenuBar process is active then we will add buttons on vertical panel instead of 
	 * direct adding to container
	 */
	VerticalPanel vPanel=new VerticalPanel();
	
	
	/**
	 * @author Anil
	 * @since 03-12-2020
	 * Separating action and navigation buttons when 
	 * process enable menu bar is active
	 */
	String[]actionMenuNames;
	String[]navigationMenuNames;
	public VerticalPanel actionMenuPanel=new VerticalPanel();
	public VerticalPanel navigationMenuPanel=new VerticalPanel();
	
	/**
	 * @author Anil
	 * @since 05-01-2021
	 * For hr screen/ the screen which are exempt from simplification for those process level bar was not visible
	 * raised by Rahul Tiwari
	 */
	FormStyle defaultFormStyle;

	/**
	 * Instantiates a new process level bar.
	 * @param appHeaderBarPct the app header bar pct
	 * @param menuNames the menu names 
	 */
	public ProcessLevelBar(int appHeaderBarPct,String[]menuNames)
	{
		this.actionMenuNames=menuNames;
		this.perCentSize=appHeaderBarPct;
		content.getElement().setId("processbar");
		
		btnLabels=new InlineLabel[menuNames.length];
		registration= new HandlerRegistration[menuNames.length];
		for(int i=0;i<menuNames.length;i++)
		{
			btnLabels[i]=new InlineLabel(menuNames[i]);
		}
		
		this.createGui();
	}
	
	/**
	 * 
	 * @param appHeaderBarPct
	 * @param actionMenuNames
	 * @param navigationMenuNames
	 * @author Anil
	 * @param default1 
	 * @since 03-12-2020
	 * separating action and navigation menus
	 * 
	 */
	public ProcessLevelBar(int appHeaderBarPct,String[]actionMenuNames,String[]navigationMenuNames,FormStyle default1){
		this.perCentSize=appHeaderBarPct;
		content.getElement().setId("processbar");
		defaultFormStyle=default1;
		/**
		 * @author Anil
		 * @since 21-12-2020
		 * removing common element
		 */
//		this.actionMenuNames=actionMenuNames;
//		this.navigationMenuNames=navigationMenuNames;
		
		if(navigationMenuNames!=null){
			ArrayList<String> navigationList=new ArrayList<String>();
			ArrayList<String> actionList=new ArrayList<String>();
			
			for(String navigation:navigationMenuNames){
				navigationList.add(navigation);
			}
			for(String action:actionMenuNames){
				actionList.add(action);
			}
			
			for(String navigation:navigationList){
				for(int i=0;i<actionList.size();i++){
					if(navigation.equals(actionList.get(i))){
						actionList.remove(i);
						i--;
					}
				}
			}
			
//			navigationMenuNames=(String[]) navigationList.toArray();
//			actionMenuNames=(String[]) actionList.toArray();
			actionMenuNames=new String[actionList.size()];
			int index=0;
			for(String action:actionList){
				actionMenuNames[index]=action;
				index++;
			}
			
		}
		
		this.actionMenuNames=actionMenuNames;
		this.navigationMenuNames=navigationMenuNames;
		
		int length=0;
		if(actionMenuNames!=null){
			length=length+actionMenuNames.length;
		}
		if(navigationMenuNames!=null){
			length=length+navigationMenuNames.length;
		}
		
		
		
		if(length>0){
			btnLabels=new InlineLabel[length];
			registration= new HandlerRegistration[length];
			
			int index=0;
			if(actionMenuNames!=null){
				for(int i=0;i<actionMenuNames.length;i++){
					btnLabels[index]=new InlineLabel(actionMenuNames[i]);
					index++;
				}
			}
			
			if(navigationMenuNames!=null){
				for(int i=0;i<navigationMenuNames.length;i++){
					btnLabels[index]=new InlineLabel(navigationMenuNames[i]);
					index++;
				}
			}
		}
		this.createGui();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.simplesoftwares.client.library.appstructure.ViewContainer#createGui()
	 */
	@Override
	public void createGui() {

		for (int i = 0; i < btnLabels.length; i++) {
//			System.out.println("LBL : "+btnLabels[i].getText());
			if (AppMemory.getAppMemory().enableMenuBar&&defaultFormStyle!=null&&FormStyle.DEFAULT!=defaultFormStyle) {

				if (actionMenuNames != null) {
						
					for (String menu : actionMenuNames) {
//						System.out.println("MENU : "+menu);
						if (btnLabels[i].getText().equals(menu)) {
							actionMenuPanel.add(btnLabels[i]);
							break;
						}
					}
				}

				if (navigationMenuNames!=null) {
					for (String menu : navigationMenuNames) {
						if (btnLabels[i].getText().equals(menu)) {
							navigationMenuPanel.add(btnLabels[i]);
							break;
						}
					}
				}

//				vPanel.add(btnLabels[i]);
			} else {
				content.add(btnLabels[i]);
			}
			// content.add(btnLabels[i]);
			btnLabels[i].getElement().setClassName("processbaritems");
		}
		if (AppMemory.getAppMemory().enableMenuBar&&defaultFormStyle!=null&&FormStyle.DEFAULT!=defaultFormStyle) {
			vPanel.add(actionMenuPanel);
			vPanel.add(navigationMenuPanel);
			content.add(vPanel);
		}
		/**
		 * @author Anil
		 * @since 15-09-2020 reducing process bar width
		 */
		// content.setWidth("100%");
		content.setWidth("98%");
	}
	
/**
 * *****************************Togel the Save Update Button **************************************************************.
 *
 * @return the app header bar pct
 */
	
	
	/***************************************************/
	public int getAppHeaderBarPct() {
		return perCentSize;
	}

	/**
	 * Sets the app header bar pct.
	 *
	 * @param appHeaderBarPct the new app header bar pct
	 */
	public void setAppHeaderBarPct(int appHeaderBarPct) {
		this.perCentSize = appHeaderBarPct;
	}
	
	/**
	 * Sets the process level bar invisible or visible
	 *
	 * @param state true means process level bar should be visible,false means it should be invisible
	 */
	public  void setVisibleFalse(boolean state)
	
	{
		for(int k=0;k<btnLabels.length;k++)
		{
			btnLabels[k].setVisible(state);
		}
		
	}

	public void applyStyle() {
	}
	
	
	public boolean isProcessMenuActive(){
		for (int i = 0; i < btnLabels.length; i++) {
			if(btnLabels[i].isVisible()){
				return true;
			}
		}
		return false;
	}
	
	public void setUpdatedCSS(){
		for (int i = 0; i < btnLabels.length; i++) {
			btnLabels[i].getElement().setClassName("appdrawermenu");
		}
	}
	
	public int getNumberOfActiveButtons(){
		int counter=0;
		for (int i = 0; i < btnLabels.length; i++) {
			if(btnLabels[i].isVisible()){
				counter++;
			}
		}
		return counter;
	}
	
	public void hideActionMenuPanel(){
		if(actionMenuPanel!=null){
			actionMenuPanel.setVisible(false);
		}
	}
	public void showActionMenuPanel(){
		if(actionMenuPanel!=null){
			actionMenuPanel.setVisible(true);
		}
	}
	
	public void hideNavigationMenuPanel(){
		if(navigationMenuPanel!=null){
			navigationMenuPanel.setVisible(false);
		}
	}
	public void showNavigationMenuPanel(){
		if(navigationMenuPanel!=null){
			navigationMenuPanel.setVisible(true);
		}
	}
	
	public boolean isActionMenuActive(){
		if(actionMenuNames!=null){
			for(String menu:actionMenuNames){
				for(InlineLabel label:btnLabels){
					if(label.getText().equals(menu)){
						if(label.isVisible()){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean isNavigationMenuActive(){
		if(navigationMenuNames!=null){
			for(String menu:navigationMenuNames){
				for(InlineLabel label:btnLabels){
					if(label.getText().equals(menu)){
						if(label.isVisible()){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public int getNumberOfActiveActionButtons(){
		int counter=0;
		if(actionMenuNames!=null){
			for(String menu:actionMenuNames){
				for(InlineLabel label:btnLabels){
					if(label.getText().equals(menu)){
						if(label.isVisible()){
							counter++;
						}
					}
				}
			}
		}
		return counter;
	}
	
	public int getNumberOfActiveNavigationButtons(){
		int counter=0;
		if(navigationMenuNames!=null){
			for(String menu:navigationMenuNames){
				for(InlineLabel label:btnLabels){
					if(label.getText().equals(menu)){
						if(label.isVisible()){
							counter++;
						}
					}
				}
			}
		}
		return counter;
	}

	public String[] getActionMenuNames() {
		return actionMenuNames;
	}

	public void setActionMenuNames(String[] actionMenuNames) {
		this.actionMenuNames = actionMenuNames;
	}

	public String[] getNavigationMenuNames() {
		return navigationMenuNames;
	}

	public void setNavigationMenuNames(String[] navigationMenuNames) {
		this.navigationMenuNames = navigationMenuNames;
	}
	
	
	
}