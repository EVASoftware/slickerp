
package com.simplesoftwares.client.library.appskeleton;

import java.util.Vector;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.simplesoftwares.client.library.ProcessLevelMenuBarPopup;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.libservice.LoginService;
import com.simplesoftwares.client.library.libservice.LoginServiceAsync;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.LogoutService;
import com.slicktechnologies.client.services.LogoutServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.userprofile.UserLoginInfoPopupForm;
import com.slicktechnologies.client.userprofile.changepassword.ChangePasswordPopupForm;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.approval.ApprovalPresenter;
import com.slicktechnologies.client.views.customerDetails.CustomerDetailsPresenter;
import com.slicktechnologies.client.views.freshdeskhome.FreshDeskHomeScreen;
import com.slicktechnologies.client.views.humanresource.statuteryreport.StatuteryReportPresenter;
import com.slicktechnologies.client.views.summaryscreen.SummaryForm;
import com.slicktechnologies.client.views.summaryscreen.SummaryPresenter;
import com.slicktechnologies.client.views.implementation.ImplementationPresenter;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.User;


/**
 * The Class AppSkeleton.Creates the whole navigational skeleton of the app Sidebar,Appheaderbar,AppLevel menu and NavigationalBar
 * </br>
 * To Do extensive use of flow panel which means to many nested divs in generated HTML. can be simplified again not any practical
 * benifets though.
 */
public class AppSkeleton 
{
	
	/** The navigation panel.
	 * To Do :On an urgent basis sumit should find an athestically pleasing way to display user name and pic
	 *  same as that in MyErp.
	 */
	private FlowPanel navigationPanel;
	
	/** The side bar panel. */
	private FlowPanel sideBarPanel;
	
	/** The app header bar panel.
	*/
	private FlowPanel appHeaderBarPanel;
	
	/** The app level menu panel.
	 * To DO: On an urgent basis sumit should integrate icons
	 * */
	private FlowPanel appLevelMenuPanel;
	
	/** The process name. */
	private  InlineLabel processName;
	
	/** 
	 * Client company name should be initalized from database.
	 *  */
	@SuppressWarnings("unused")
	private  InlineLabel companyName;
	/**
	 * Our company Logo will be initalized from database.
	 */
	@SuppressWarnings("unused")
	private Image ourCompanyLogo;
	
	/**
	 * The client company logo of company to be displayed on AppHeaderBar.Will be initalized from Database
	 */
	@SuppressWarnings("unused")
	private  Image clientCompanyLogo;
	
	/** The navigation panel pct. */
	private int navigationPanelpct;
	
	/** The sidebar panel pct. */
	private int sidebarPanelpct;
	
	/** The app header bar panel pct. */
	private int appHeaderBarPanelpct; 
	
	/** Since each screen will provide its own logic when clicked on AppLevel menus.The Reference to current handler
	 * needs to be saved */
	public HandlerRegistration[] registration;
	
	/** The menu labels.
	 * Needed to set app level menu from outside
	 * To Do : Bad design this should not be an attribute,pradnya and sumit should change this.
	 *  */
	public  InlineLabel[]menuLabels;
	
	
	/** The event handler object.Drives the screen transition. 
	 * 
	 * */
	private SkeletonEventHandling eventHandler;
	
	/** The logout label 
	 * To Do : Same as navigational panel
	 * */
	private static Label  logout;
	
	private static Label homenavigation;
	
	private static Label  loggedUser;

	public Image dispLogo;
	
	LogoutServiceAsync logoutService=GWT.create(LogoutService.class);


	/**
	 * this panel holds popup which comes when we click on the name of user currently logged in
	 * and provide option to change password
	 * Date :19-09-2016 By Anil
	 * Release 30 Sept 2016
	 */
	PopupPanel panel=new PopupPanel(true);
	PopupPanel panel1=new PopupPanel(true);
	UserLoginInfoPopupForm userInfoPopup=new UserLoginInfoPopupForm();
	ChangePasswordPopupForm changePassPopup=new ChangePasswordPopupForm();
	LoginServiceAsync loginservice=GWT.create(LoginService.class);
	GenricServiceAsync async=GWT.create(GenricService.class);
	
	/**
	 * End
	 */
	
	/**
	 * @author Anil
	 * @since 11-09-2020
	 */
	private int footerBarPer; 
	private FlowPanel footerBarPanel;
	/**
	 * @author Anil
	 * @since 23-09-2020
	 */
	public HorizontalPanel horAppPanel;
	public Image appdrawer;
	public boolean isMenuVisble=true;
	
	/**
	 * @author Anil
	 * @since 03-12-2020
	 * Adding two more image icon
	 * 1-Navigation
	 * 2-Upload
	 * @since 18-12-2020
	 * added help and report button
	 */
	
	public Image navigationBtn;
	public Image uploadBtn;
	public Image helpBtn;
	public Image reportBtn;
	
	
	/**
	 * @author Anil
	 * @since 24-09-2020
	 * Process level menu bar app drawer panel
	 */
//	public PopupPanel appdrawerPanel;
	
	/**
	 * @author Anil
	 * @since 25-09-2020
	 */
//	public ProcessLevelMenuBarPopup processMenuPopup;
	
    /**
     * Instantiates a new AppSkeleton.
     */
    private AppSkeleton()
    {
    	eventHandler=new SkeletonEventHandling();
    	navigationPanel=new FlowPanel();
    	sideBarPanel=new FlowPanel();
    	appLevelMenuPanel=new FlowPanel();
    	appHeaderBarPanel = new FlowPanel();	
    	
    	footerBarPanel=new FlowPanel();
    	
    	horAppPanel=new HorizontalPanel();
    	horAppPanel.setWidth("98%");
    }
    
    /**
     * Instantiates a new app skeleton.
     *
     * @param navper the navper percentage width of navigational bar panel
     * @param sidebarper the sidebarper  percentage width of sidebar bar panel
     * @param appheaderbarper the appheaderbarper percentage width of app header  bar panel
     */
    public AppSkeleton(int navper,int sidebarper,int appheaderbarper)
    {
    	this();
    	this.navigationPanelpct=navper;
    	this.sidebarPanelpct=sidebarper;
    	this.appHeaderBarPanelpct=appheaderbarper;
    }
    
    /**
     * @author Anil
     * @param enableMenuBar 
     * @since 11-09-2020
     * Adding footer bar in the app layout
     */
    public AppSkeleton(int navper,int sidebarper,int appheaderbarper,int footerBarPer, boolean enableMenuBar)
    {
    	this();
    	this.navigationPanelpct=navper;
    	if(enableMenuBar){
    		this.sidebarPanelpct=0;
    	}else{
        	this.sidebarPanelpct=sidebarper;
    	}

    	
    	this.appHeaderBarPanelpct=appheaderbarper;
    	this.footerBarPer=footerBarPer;
    	
    	horAppPanel=new HorizontalPanel();
    	horAppPanel.setWidth("98%");
    	
//    	appdrawerPanel = new PopupPanel(true);
//		processMenuPopup = new ProcessLevelMenuBarPopup();
//		appdrawerPanel.add(processMenuPopup);
//		appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
//		appdrawerPanel.setWidth("180px");
//		appdrawerPanel.getElement().setId("login-form");
    }
	
///////////////////////////////////////////NAVIGATION PANEL CREATION///////////////////////////////////
	/**
	 * Creates the navigation panel.
	 * @param navigationNames menuitems vector refrence
	 */
	public void createNavigationPanel(Vector<MenuItems> navigationNames) {

		navigationPanel.getElement().setId("navigationbar");
		Label lbl;
		for (int i = 0; i < navigationNames.size(); i++) {
			if (navigationNames.get(i) != null) {
				lbl = (Label) navigationNames.get(i).getLabel();
				lbl.getElement().setClassName("navigationitems");
				navigationPanel.add(lbl);
				lbl.addClickHandler(eventHandler);
			}
		}

		logout = new Label("Logout");
		logout.getElement().setId("logout");
		navigationPanel.add(logout);
		logout.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Window.Location.reload();

			}
		});

	}



	public void createNewNavigationPanel(final Vector<NavigationBarItem> navigationNames) {

		navigationPanel.getElement().setId("navigationbar");
		Label lbl;

		Vector<NavigationBarItem> navigationBarVec = new Vector<NavigationBarItem>();

		homenavigation = new Label("Home");
		homenavigation.getElement().setClassName("navigationitems");

		NavigationBarItem homeNav = new NavigationBarItem();
		homeNav.setLabel(homenavigation);
		navigationBarVec.add(homeNav);
		navigationBarVec.addAll(navigationNames);

		NavigationBarItem temp = navigationBarVec.get(0);
		if (temp != null)
			temp.changeSideBarPanel();

		for (int i = 0; i < navigationBarVec.size(); i++) {
			if (navigationBarVec.get(i) != null) {
				lbl = (Label) navigationBarVec.get(i).getLabel();
				lbl.getElement().setClassName("navigationitems");
				navigationPanel.add(lbl);
				lbl.addClickHandler(eventHandler);
			}
		}

		logout = new Label("Logout");
		logout.getElement().setId("logout");
		navigationPanel.add(logout);
		logout.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				userLogout();
			}
		});

		homenavigation.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				AppMemory mem = AppMemory.getAppMemory();
				mem.goToHome();
			}
		});

	}
	
	public void userLogout()
	{
		long companyId=UserConfiguration.getCompanyId();
		System.out.println("CID"+companyId);
		//***************old code *******************8
//		String userName=UserConfiguration.getUserconfig().getUser().getUserName();
//		System.out.println("UName"+userName);
		
		//*****************Anil changes here for customer logout ****************8
		String userName=null;
		if(UserConfiguration.getUserconfig().getUser()!=null){
			userName=UserConfiguration.getUserconfig().getUser().getUserName();
		}else{
			userName=UserConfiguration.getUserconfig().getCustUser().getUserName();
		}
		
		//  rohan added session count to logout specific sessionsonly
		// Date : 14/3/2017
		
		Console.log("session id "+LoginPresenter.sessionCountValue);
		
		logoutService.doLogoutonBrowserClose(companyId,userName,"Logged out successfully",LoginPresenter.sessionCountValue,new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
			@Override
			public void onSuccess(User result) {
				Window.Location.reload();
			}
		});
	}

////////////////////////////////////////////////SIDEBAR PANEL CREATION///////////////////////////////////
	/**
	 * Creates the side bar panel with {@link MenuItems}.The actual widget type
	 * of {@link MenuItems} can be any {@link UiHandler} which implements
	 * {@link HasClickHandlers}.This is because all Side bar items should be
	 * clickable
	 * @param sideBarNames Sidebaritems vector which will go on sidebar.
	 */
	public void createSideBarPanel(Vector<SideBarItems> sideBarNames) {
		sideBarPanel.getElement().setId("sidebar");
		VerticalPanel vp;
		int count = sideBarPanel.getWidgetCount();
		Console.log("SIDEBAR ITEM NOS : " + count);
		for (int i = count - 1; i >= 0; i--) {
			System.out.println("I VAL " + i);
			if (sideBarPanel.getWidget(i) != null)
				sideBarPanel.remove(i);
		}

		for (int i = 0; i < sideBarNames.size(); i++) {
			ScrollPanel scroll = new ScrollPanel();
			scroll.setHeight("120px");
			scroll.setWidth("200px");
			vp = new VerticalPanel();
			Vector<MenuItems> menus = sideBarNames.get(i).getMenus();
			System.out.println("Appskeleton");
			HasClickHandlers clickable;
			for (int j = 0; j < menus.size(); j++) {
				if (menus.get(j) != null) {
					clickable = (HasClickHandlers) menus.get(j).getLabel();
					((UIObject) clickable).getElement().setClassName("leftbaritems");
					clickable.addClickHandler(eventHandler);
					vp.add(menus.get(j).getLabel());
				}
			}
			DisclosurePanel discPanel = new DisclosurePanel(sideBarNames.get(i).getTitle());
			// scroll.add(vp);
			discPanel.add(vp);

			sideBarPanel.add(discPanel);
			sideBarPanel.add(new HTML("<hr/>"));
			discPanel.setAnimationEnabled(true);

			sideBarPanel.setWidth("100%");
		}

		// dispLogo=new Image();
		// System.out.println("display");
		// dispLogo.setUrl("/images/logo15.jpg");
		// dispLogo.setAltText("Eva Software Solutions");
		// dispLogo.getElement().setClassName("displaylogo");
		// dispLogo.setSize("100%", "10%");
		// //dispLogo.getElement().getStyle().setPadding(100, Unit.PX);
		// dispLogo.getElement().getStyle().setPaddingTop(483, Unit.PX);
		// dispLogo.getElement().getStyle().setPosition(Position.ABSOLUTE);
		// sideBarPanel.add(dispLogo);

	}

////////////////////////////////////////APPHEADER BAR CREATION////////////////////////////////////////////////
	/**
	 * Creates the AppHeaderBar panel.
	 * @param menuNames the menu names String array containing app level menus
	 */
	public void createAppHeaderBarPanel(String[] menuNames) {
		createAppLevelMenuPanel(menuNames);
		appHeaderBarPanel.getElement().setId("appheaderbar");
		processName = new InlineLabel("Home");

		processName.addClickHandler(eventHandler);
		processName.getElement().setId("processname");
		appHeaderBarPanel.add(processName);

		companyName = new InlineLabel(Slick_Erp.businessUnitName);
		companyName.getElement().setId("headercompany");
		appHeaderBarPanel.add(companyName);

		loggedUser = new Label(LoginPresenter.loggedInUser);
		loggedUser.getElement().setId("loggeduser");
		
		/**
		 * @author Anil
		 * @since 29-09-2020
		 * 
		 */
		if(!AppMemory.getAppMemory().enableMenuBar){
			appHeaderBarPanel.add(loggedUser);
		}


		loggedUser.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				panel = new PopupPanel(true);
				panel.add(userInfoPopup);
				panel.show();
				panel.setPopupPosition(Window.getClientWidth() - 200,Window.getClientHeight()- (Window.getClientHeight() - 50));
			}
		});

		/**
		 * @author Anil
		 * @since 23-09-2020
		 */
		horAppPanel.add(appLevelMenuPanel);
		
		helpBtn=new Image();
		helpBtn.setUrl("/images/menubar/056-information.png");
		helpBtn.setAltText("Help");
//		reportBtn.getElement().setClassName("displaylogo");
		helpBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		helpBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		helpBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		helpBtn.getElement().getStyle().setMarginLeft(-173, Unit.PX);
		helpBtn.setSize("2%", "34%");
		helpBtn.setVisible(false);
		horAppPanel.add(helpBtn);
		
		reportBtn=new Image();
		reportBtn.setUrl("/images/menubar/044-file.png");
		reportBtn.setAltText("Reports");
//		reportBtn.getElement().setClassName("displaylogo");
		reportBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		reportBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		reportBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		reportBtn.getElement().getStyle().setMarginLeft(-133, Unit.PX);
		reportBtn.setSize("2%", "34%");
		reportBtn.setVisible(false);
		horAppPanel.add(reportBtn);
		
		uploadBtn=new Image();
		uploadBtn.setUrl("/images/menubar/145-upload.png");
		uploadBtn.setAltText("Upload");
//		uploadBtn.getElement().setClassName("displaylogo");
		uploadBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		uploadBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		uploadBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		uploadBtn.getElement().getStyle().setMarginLeft(-93, Unit.PX);
		uploadBtn.setSize("2%", "34%");
		uploadBtn.setVisible(false);
		horAppPanel.add(uploadBtn);
		
		navigationBtn=new Image();
		navigationBtn.setUrl("/images/menubar/040-compass.png");
		navigationBtn.setAltText("Navigation");
//		navigationBtn.getElement().setClassName("displaylogo");
		navigationBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		navigationBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		navigationBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		navigationBtn.getElement().getStyle().setMarginLeft(-53, Unit.PX);
		navigationBtn.setSize("2%", "34%");
		navigationBtn.setVisible(false);
		horAppPanel.add(navigationBtn);
		
		
		appdrawer=new Image();
		appdrawer.setUrl("/images/menubar/165-menu.png");
		appdrawer.setAltText("Process Menu");
		appdrawer.getElement().setClassName("displaylogo");
		appdrawer.getElement().getStyle().setPosition(Position.ABSOLUTE);
		appdrawer.getElement().getStyle().setBackgroundColor("whitesmoke");
		appdrawer.getElement().getStyle().setMarginTop(7, Unit.PX);
		appdrawer.getElement().getStyle().setMarginLeft(-13, Unit.PX);
		appdrawer.setSize("2%", "34%");
		appdrawer.setVisible(false);
//		horAppPanel.setWidth("98%");
		horAppPanel.add(appdrawer);
		
		
		appHeaderBarPanel.add(horAppPanel);
		appHeaderBarPanel.setWidth("100%");

		userInfoPopup.changePasswordLbl.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				panel.hide();
				panel1 = new PopupPanel(true);
				changePassPopup.clear();
				panel1.add(changePassPopup);
				panel1.show();
				// panel1.center();
				panel1.setPopupPosition(Window.getClientWidth() - 340,Window.getClientHeight() - (Window.getClientHeight() - 50));

			}
		});

		userInfoPopup.logoutLbl.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				panel.hide();
				userLogout();
			}
		});

		changePassPopup.btnCancel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				panel1.hide();
			}
		});

		changePassPopup.btnOk.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (changePassPopup.valid()) {
					if (LoginPresenter.loggedInCustomer == false) {
						final User user = LoginPresenter.myUserEntity;
						user.setPassword(changePassPopup.ptbNewPassword.getValue());
						async.save(user, new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onSuccess(ReturnFromServer result) {
								panel1.hide();
								LoginPresenter.myUserEntity = user;
								Window.alert("Password Changed Successfully!");
							}

							@Override
							public void onFailure(Throwable caught) {

							}
						});

					} else {
						final CustomerUser user = LoginPresenter.myCustUserEntity;
						user.setPassword(changePassPopup.ptbNewPassword.getValue());
						async.save(user, new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onSuccess(ReturnFromServer result) {
								panel1.hide();
								LoginPresenter.myCustUserEntity = user;
								Window.alert("Password Changed Successfully!");
							}

							@Override
							public void onFailure(Throwable caught) {

							}
						});
					}
				}
			}
		});

		changePassPopup.ptbOldPassword.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				if (!changePassPopup.ptbOldPassword.getValue().equals("")) {
					if (LoginPresenter.loggedInCustomer == false) {
						loginservice.isOldPasswordValid(LoginPresenter.myUserEntity,changePassPopup.ptbOldPassword.getValue(),new AsyncCallback<Boolean>() {
							@Override
							public void onSuccess(Boolean result) {
								if (!result) {
									Window.alert("Old Password is Wrong!");
									changePassPopup.ptbOldPassword.setValue("");
								}
							}

							@Override
							public void onFailure(Throwable caught) {

							}
						});
					} else {
						if (!LoginPresenter.myCustUserEntity.getPassword().equals(changePassPopup.ptbOldPassword.getValue())) {
							Window.alert("Old Password is Wrong!");
							changePassPopup.ptbOldPassword.setValue("");
						}
					}
				}
			}
		});

	}

///////////////////////////////////////////////APPLEVELMENU CREATION////////////////////////////////////////
	/**
	 * Creates the AppLevelMenu panel. To Do: The AppLevelMenus now are
	 * {@link InlineLabel}.Change them to Icons
	 * @param menuNames the menu names String array containing AppLevelMenus
	 */
	public void createAppLevelMenuPanel(String[] menuNames) {
		menuLabels = new InlineLabel[menuNames.length];
		registration = new HandlerRegistration[menuNames.length];
		for (int i = 0; i < menuNames.length; i++) {

			menuLabels[i] = new InlineLabel(menuNames[i]);
			appLevelMenuPanel.add(menuLabels[i]);

			/**
			 * @author Abhinav Bihade
			 * @since 18/12/2019 In Weekly Meeting decision has come out by
			 *        Nitin Sir, Anil Sir,Sonu Porel and other member also was
			 *        in Meeting they decided to Remove 'Discard' Button from
			 *        Framework if(i<3) i have change 3 to 2
			 */

			if (i < 2) {
				menuLabels[i].getElement().setClassName("leftfloatbuttons");
			} else {
				menuLabels[i].getElement().setClassName("rightfloatbuttons");
			}

			/**
			 * @author Anil
			 * @since 15-09-2020
			 * reduced the top 
			 */
//			menuLabels[i].getElement().getStyle().setMarginTop(25, Unit.PX);
			menuLabels[i].getElement().getStyle().setMarginTop(7, Unit.PX);

			if (i == 0) {
				menuLabels[i].getElement().getStyle().setMarginLeft(20, Unit.PX);
			}
			if (i == 2) {
				menuLabels[i].getElement().getStyle().setMarginRight(20, Unit.PX);
			}
		}

		appLevelMenuPanel.setWidth("100%");
	}
	
	/**
	 * *****************************GETTER SETTERS******************.
	 *
	 * @return the NavigationalPanel
	 */

	public FlowPanel getNavigationPanel() {
		return navigationPanel;
	}

	/**
	 * Sets the NavigationalPanel.
	 *
	 * @param navigationPanel the new navigation panel
	 */
	public void setNavigationPanel(FlowPanel navigationPanel) {
		this.navigationPanel = navigationPanel;
	}

	/**
	 * Gets the SideBarPanel.
	 *
	 * @return the SideBarPanel
	 */
	public FlowPanel getSideBarPanel() {
		return sideBarPanel;
	}

	/**
	 * Sets the SideBarPanel.
	 *
	 * @param SideBarPanel the new SideBarPanel
	 */
	public void setSideBarPanel(FlowPanel sideBarPanel) {
		this.sideBarPanel = sideBarPanel;
	}

	/**
	 * Gets the AppHeaderBarPanel.
	 *
	 * @return the AppHeaderBarPanel
	 */
	public FlowPanel getAppHeaderBarPanel() {
		return appHeaderBarPanel;
	}

	/**
	 * Sets the AppHeaderBarPanel.
	 *
	 * @param appHeaderBarPanel the new AppHeaderBarPanel
	 */
	public void setAppHeaderBarPanel(FlowPanel appHeaderBarPanel) {
		this.appHeaderBarPanel = appHeaderBarPanel;
	}

	/**
	 * Gets the AppLevelMenuPanel.
	 *
	 * @return the AppLevelMenuPanel
	 */
	public FlowPanel getAppLevelMenuPanel() {
		return appLevelMenuPanel;
	}

	/**
	 * Sets the AppLevelMenuPanel.
	 *
	 * @param AppLevelMenuPanel the new AppLevelMenuPanel
	 */
	public void setAppLevelMenuPanel(FlowPanel appLevelMenuPanel) {
		this.appLevelMenuPanel = appLevelMenuPanel;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the process name
	 */
	public InlineLabel getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName the new process name
	 */
	public void setProcessName(InlineLabel processName) {
		this.processName = processName;
	}

	
	/**
	 * Gets the navigation panelpct.
	 *
	 * @return the navigation panelpct
	 */
	public int getNavigationPanelpct() {
		return navigationPanelpct;
	}

	/**
	 * Sets the navigation panelpct.
	 *
	 * @param navigationPanelpct the new navigation panelpct
	 */
	public void setNavigationPanelpct(int navigationPanelpct) {
		this.navigationPanelpct = navigationPanelpct;
	}

	/**
	 * Gets the sidebar panelpct.
	 *
	 * @return the sidebar panelpct
	 */
	public int getSidebarPanelpct() {
		return sidebarPanelpct;
	}

	/**
	 * Sets the sidebar panelpct.
	 *
	 * @param sidebarPanelpct the new sidebar panelpct
	 */
	public void setSidebarPanelpct(int sidebarPanelpct) {
		this.sidebarPanelpct = sidebarPanelpct;
	}

	/**
	 * Gets the app header bar panelpct.
	 *
	 * @return the app header bar panelpct
	 */
	public int getAppHeaderBarPanelpct() {
		return appHeaderBarPanelpct;
	}

	/**
	 * Sets the app header bar panelpct.
	 *
	 * @param appHeaderBarPanelpct the new app header bar panelpct
	 */
	public void setAppHeaderBarPanelpct(int appHeaderBarPanelpct) {
		this.appHeaderBarPanelpct = appHeaderBarPanelpct;
	}

	/**
	 * Gets the registration.
	 *
	 * @return the registration
	 */
	public HandlerRegistration[] getRegistration() {
		return registration;
	}

	/**
	 * Sets the registration.
	 *
	 * @param registration the new registration
	 */
	public void setRegistration(HandlerRegistration[] registration) {
		this.registration = registration;
	}

	
	/**
	 * Gets the menu labels.
	 *
	 * @return the menu labels
	 */
	public InlineLabel[] getMenuLabels() {
		return menuLabels;
	}

	/**
	 * Sets the menu labels.
	 *
	 * @param menuLabels the new menu labels
	 */
	public void setMenuLabels(InlineLabel[] menuLabels) {
		this.menuLabels = menuLabels;
	}
	
	/**
	 * Gets the event handler.
	 *
	 * @return the event handler
	 */
	public SkeletonEventHandling getEventHandler() {
		return eventHandler;
	}

	/**
	 * Sets the event handler.
	 *
	 * @param eventHandler the new event handler
	 */
	public void setEventHandler(SkeletonEventHandling eventHandler) {
		this.eventHandler = eventHandler;
	}
	
	public void setImageInSideBarPanel(Image image,String idName,int widthPx,int heightPx,final String link)
	{
		
		image.setPixelSize(widthPx,heightPx);
		sideBarPanel.add(image);
		
		image.getElement().setId(idName);
		image.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Window.open(link,"_blank","");
				
			}
		});
		
	}
	
	
	
	public void createFooterPanel() {
		footerBarPanel.getElement().setId("navigationbar");
		
		Console.log("Footer Panel 1");
		
		dispLogo=new Image();
		System.out.println("display");
		dispLogo.setUrl("/images/SmallLogo.png");
		dispLogo.setAltText("Eva Software Solutions");
		dispLogo.getElement().setClassName("displaylogo");
		dispLogo.getElement().getStyle().setPosition(Position.ABSOLUTE);
		dispLogo.setSize("4%", "90%");
//		dispLogo.getElement().getStyle().setWidth(18, Unit.PCT);
//		dispLogo.getElement().getStyle().setHeight(80, Unit.PCT);
		
		HorizontalPanel hpanel = new HorizontalPanel();
		hpanel.getElement().getStyle().setWidth(20, Unit.PCT);
		
		hpanel.getElement().getStyle().setPaddingTop(4, Unit.PX);
		hpanel.getElement().getStyle().setPaddingBottom(4, Unit.PX);
		hpanel.getElement().getStyle().setPaddingLeft(4, Unit.PX);
		hpanel.getElement().getStyle().setPaddingRight(4, Unit.PX);
		
		hpanel.add(dispLogo);
		footerBarPanel.add(hpanel);
		
		/**@Sheetal:21-02-2022
		 * Des : Adding ERP license expiry date in footer panel,requirement by nitin sir**/
		Label lbl2 = new Label("ERP License Expiry :");
		lbl2.getElement().setClassName("navigationitems");
		lbl2.getElement().getStyle().setPaddingLeft(5, Unit.PCT);
		footerBarPanel.add(lbl2);
		
		Label lbl3 = new Label(LoginPresenter.endDate);
		lbl3.getElement().setClassName("navigationitems");
		footerBarPanel.add(lbl3);
		Console.log("Footer Panel 2");
		/**end**/
		Label lbl = new Label("Copyright "+"\u00a9"+" All copyright reserved to EVA Software Solutions" );
		lbl.getElement().setClassName("navigationitems");
//		lbl.getElement().getStyle().setWidth(60, Unit.PCT);
		lbl.getElement().getStyle().setPaddingLeft(20, Unit.PCT);
		lbl.getElement().getStyle().setTextAlign(com.google.gwt.dom.client.Style.TextAlign.CENTER);
		footerBarPanel.add(lbl);
		
		Console.log("Footer Panel 3");
		
		Label lbl1 = new Label(AppConstants.VERSION);
		lbl1.getElement().setClassName("navigationitems");
//		lbl1.getElement().getStyle().setWidth(20, Unit.PCT);
		lbl1.getElement().getStyle().setPaddingLeft(25, Unit.PCT);
		lbl1.getElement().getStyle().setTextAlign(com.google.gwt.dom.client.Style.TextAlign.RIGHT);
		footerBarPanel.add(lbl1);
		
		Console.log("Footer Panel 4");
		
		
		
		
		
	}
	
	public void createMenuBarPanel(Vector<NavigationBarItem> navigationBarVec) {
		navigationPanel.getElement().setId("navigationbar");
		
		String url="";
		double width=100;
		if(LoginPresenter.company!=null&&LoginPresenter.company.getLogo()!=null
				&&LoginPresenter.company.getLogo().getUrl()!=null&&!LoginPresenter.company.getLogo().getUrl().equals("")){
			url=LoginPresenter.company.getLogo().getUrl();
			width=97;
		}
		
		HorizontalPanel hrPanel=new HorizontalPanel();
		hrPanel.getElement().getStyle().setWidth(width, Unit.PCT);
		hrPanel.getElement().getStyle().setHeight(100, Unit.PCT);
		
		MenuBar menu = new MenuBar();
		menu.setAnimationEnabled(true);
//		menu.setAutoOpen(true);
		
		for(final NavigationBarItem model:navigationBarVec){
			Label lbl=(Label) model.getLabel();
			String menuName=lbl.getText();
			
			if(menuName.equals("Approval")||menuName.equals("Settings")||menuName.equals("Support")){
				continue;
			}
			
			MenuBar subMenu = new MenuBar(true);
			int subMenuItemCounter=0;
			for(SideBarItems document:model.getSideBarItems()){
				if(document!=null){
					String subMenuName=document.getTitle();
					MenuBar menuItem = new MenuBar(true);
					int menuItemCounter=0;
					for(final MenuItems items:document.getMenus()){
						if(items!=null){
							menuItemCounter++;
							final Label lbl1=(Label) items.getLabel();
							MenuItem menuItems = new MenuItem(lbl1.getText(), new Command() {
								public void execute() {
									Label lbl=(Label) model.getLabel();
									String separateModule=lbl.getText();
									separateModule=separateModule.replaceAll("\\<.*?\\>", "");
									LoginPresenter.currentModule=separateModule;
									
									String separateDocument=items.screenTitle;
									try{
										String[] docArray=separateDocument.split("/");
										LoginPresenter.currentDocumentName=docArray[1];
									}catch(Exception e){
										
									}
									
									Console.log("MDL :: "+LoginPresenter.currentModule+" | DOC :: "+LoginPresenter.currentDocumentName);
									
									AppMemory.getAppMemory().currentState=ScreeenState.NEW;
									AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(items.screenTitle, items.screen);
									items.screen.changeScreen();
								}
							});
							menuItem.addItem(menuItems);
						}
					}
					if(menuItemCounter!=0){
						subMenuItemCounter++;
						subMenu.addItem(subMenuName, menuItem);
					}
				}
			}
			if(subMenuItemCounter!=0){
				menu.addItem(menuName, subMenu);
			}
		}
		hrPanel.add(menu);
		
		
		
		MenuBar rightMenu = new MenuBar();
		rightMenu.setAnimationEnabled(true);
//		rightMenu.setAutoOpen(true);
		rightMenu.addStyleName("rightalignmenus");
		rightMenu.setHeight("30px");
		
		/**
		 * @author Anil @since 20-08-2021
		 */
		final String gdsRep = "<img src='/images/menubar/001-monitor.png' height='20px' width='25px' />";
		SafeHtml gdsRepImgPath = new SafeHtml() {
		    @Override
		    public String asString() {
		        return gdsRep;
		     }
		 };
		MenuItem gdsRepMenu = new MenuItem(gdsRepImgPath, new Command() {
			public void execute() {
				if(AppMemory.getAppMemory().globalDsReportList!=null&&AppMemory.getAppMemory().globalDsReportList.size()!=0){
					FreshDeskHomeScreen form=new FreshDeskHomeScreen();
				}else{
					GWTCAlert alert =new GWTCAlert();
					alert.alert("Please configure GDS Report");
				}
			}
		});
		rightMenu.addItem(gdsRepMenu);
		
		/**
		 * @author Vijay Date :- 07-06-2021 for implementation screen added menu
		 */
		final String implementation = "<img src='/images/menubar/implementation.png' height='20px' width='25px' />";
		SafeHtml implementationImgPath = new SafeHtml() {
		    @Override
		    public String asString() {
		        return implementation;
		     }
		 };
		MenuItem implementationMenu = new MenuItem(implementationImgPath, new Command() {
			public void execute() {
				ImplementationPresenter.initalize();
			}
		});
		rightMenu.addItem(implementationMenu);
		
		
		
		for(final NavigationBarItem model:navigationBarVec){
			Label lbl=(Label) model.getLabel();
			String menuName=lbl.getText();
			
			/**
			 * @author Anil @since 02-02-2021
			 * Removing support module as we have implemented fresh desk widget to get clients complaint
			 * As per the Nitin sir's instruction removing this option
			 */
			if(menuName.equals("Settings")){
//			if(menuName.equals("Settings")||menuName.equals("Support")){
				MenuBar subMenu = new MenuBar(true);
				int subMenuItemCounter=0;
				for(SideBarItems document:model.getSideBarItems()){
					if(document!=null){
						String subMenuName=document.getTitle();
						MenuBar menuItem = new MenuBar(true);
						int menuItemCounter=0;
						for(final MenuItems items:document.getMenus()){
							if(items!=null){
								menuItemCounter++;
								Label lbl1=(Label) items.getLabel();
								MenuItem menuItems = new MenuItem(lbl1.getText(), new Command() {
									public void execute() {
										Label lbl=(Label) model.getLabel();
										String separateModule=lbl.getText();
										separateModule=separateModule.replaceAll("\\<.*?\\>", "");
										LoginPresenter.currentModule=separateModule;
										
										String separateDocument=items.screenTitle;
										try{
											String[] docArray=separateDocument.split("/");
											LoginPresenter.currentDocumentName=docArray[1];
										}catch(Exception e){
											
										}
										
										AppMemory.getAppMemory().currentState=ScreeenState.NEW;
										AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(items.screenTitle, items.screen);
										items.screen.changeScreen();
										
										
									}
								});
//								menuItem.addItem(menuItems);
								subMenu.addItem(menuItems);
							}
						}
						if(menuItemCounter!=0){
							subMenuItemCounter++;
//							subMenu.addItem(subMenuName, menuItem);
						}
					}
				}
				if(subMenuItemCounter!=0){
					if(menuName.equals("Settings")){
						final String image1 = "<img src='/images/menubar/073-settings.png' height='20px' width='25px' />";
						SafeHtml imgPath = new SafeHtml() {
						    @Override
						    public String asString() {
						        return image1;
						     }
						 };
						 rightMenu.addItem(imgPath, subMenu);
					}else if(menuName.equals("Support")){
						final String image1 = "<img src='/images/menubar/056-information.png' height='20px' width='25px' />";
						SafeHtml imgPath = new SafeHtml() {
						    @Override
						    public String asString() {
						        return image1;
						     }
						 };
						 rightMenu.addItem(imgPath, subMenu);
					}else {
						rightMenu.addItem(menuName, subMenu);
					}
				}
			}
		}
		
		final String image1 = "<img src='/images/menubar/141-home.png' height='20px' width='25px' />";
		SafeHtml homeImgPath = new SafeHtml() {
		    @Override
		    public String asString() {
		        return image1;
		     }
		 };
		MenuItem homeMenu = new MenuItem(homeImgPath, new Command() {
//		MenuItem homeMenu = new MenuItem("Home", new Command() {
			public void execute() {
//				CustomerDetailsPresenter.initalize();
				
				/**
				 * @author Anil @since 20-08-2021
				 * Summary Screen will be visible to all user
				 */
//				if(AppMemory.getAppMemory().globalDsReportList!=null&&AppMemory.getAppMemory().globalDsReportList.size()!=0&&!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//					FreshDeskHomeScreen form=new FreshDeskHomeScreen();
//				}else if(!UserConfiguration.getRole().getRoleName().equalsIgnoreCase("Admin")){
//					SummaryPresenter.initalize();
//				}else if (LoginPresenter.myCustUserEntity!=null&&LoginPresenter.myCustUserEntity.getCompanyType().equalsIgnoreCase("Facility Management")) {
//					StatuteryReportPresenter.initalize();
//				}else{
//					CustomerDetailsPresenter.initalize();
//				}
				SummaryForm.className="technicianSchedule1";
				SummaryPresenter.initalize();
			}
		});
		rightMenu.addItem(homeMenu);
		
		final String image2 = "<img src='/images/menubar/082-alarm-1.png' height='20px' width='25px' />";
		SafeHtml appImgPath = new SafeHtml() {
		    @Override
		    public String asString() {
		        return image2;
		     }
		 };
		 
		MenuItem approvalMenu = new MenuItem(appImgPath, new Command() {
//		MenuItem approvalMenu = new MenuItem("Approval", new Command() {
			public void execute() {
				ApprovalPresenter.initalize();
			}
		});
		rightMenu.addItem(approvalMenu);
		
		final String image3 = "<img src='/images/menubar/059-log-out.png' height='20px' width='25px' />";
		SafeHtml logoutImgPath = new SafeHtml() {
		    @Override
		    public String asString() {
		        return image3;
		     }
		 };
		
		MenuItem logoutMenu = new MenuItem(logoutImgPath, new Command() {
//		MenuItem logoutMenu = new MenuItem("Logout", new Command() {
			public void execute() {
				userLogout();
			}
		});
//		rightMenu.addItem(logoutMenu);
		
		
		final String image4 = "<img src='/images/menubar/153-user.png' height='20px' width='25px' />";
		SafeHtml userImgPath = new SafeHtml() {
		    @Override
		    public String asString() {
		        return image4;
		     }
		 };
		
		MenuItem userMenu = new MenuItem(userImgPath, new Command() {
//		MenuItem userMenu = new MenuItem("User", new Command() {
			public void execute() {
//				userLogout();
				
				panel = new PopupPanel(true);
				panel.add(userInfoPopup);
				panel.show();
				panel.setPopupPosition(Window.getClientWidth() - 200,Window.getClientHeight()- (Window.getClientHeight() - 30));
			
			}
		});
		rightMenu.addItem(userMenu);
		
		
		hrPanel.add(rightMenu);
		
		Console.log("CLIENT LOGO 1 "+url);
		if(url!=null&&!url.equals("")){
			Console.log("CLIENT LOGO 2 "+GWT.getModuleBaseURL());
			String moduleUrl=GWT.getModuleBaseURL();
			moduleUrl=moduleUrl.replaceAll("/slick_erp/", "");
			Console.log("CLIENT LOGO 3 "+moduleUrl);
			Image clientLogo=new Image();
			clientLogo.setUrl(moduleUrl+url);
//			clientLogo.setUrl("/images/SmallLogo.png");
			clientLogo.setAltText("Eva Software Solutions");
			clientLogo.getElement().setClassName("displaylogo");
			clientLogo.getElement().getStyle().setPosition(Position.ABSOLUTE);
			clientLogo.setSize("3%", "80%");
			
			
			HorizontalPanel hpanel = new HorizontalPanel();
			hpanel.getElement().getStyle().setWidth(3, Unit.PCT);
			hpanel.getElement().getStyle().setPaddingTop(4, Unit.PX);
			hpanel.getElement().getStyle().setPaddingBottom(4, Unit.PX);
			hpanel.getElement().getStyle().setPaddingLeft(4, Unit.PX);
			hpanel.getElement().getStyle().setPaddingRight(4, Unit.PX);
			hpanel.add(clientLogo);
			hpanel.addStyleName("rightalignmenus");
			
			hrPanel.add(hpanel);
		}
		
		navigationPanel.add(hrPanel);
	}

	public int getFooterBarPer() {
		return footerBarPer;
	}

	public void setFooterBarPer(int footerBarPer) {
		this.footerBarPer = footerBarPer;
	}

	public FlowPanel getFooterBarPanel() {
		return footerBarPanel;
	}

	public void setFooterBarPanel(FlowPanel footerBarPanel) {
		this.footerBarPanel = footerBarPanel;
	}
	
	public boolean isMenuVisible(InlineLabel[] inlineLabels){
		if(inlineLabels==null||inlineLabels.length==0){
			return false;
		}else{
			for (int i = 0; i < inlineLabels.length; i++) {
				if(inlineLabels[i].isVisible()==true);{
					Console.log(" :: MENU ::  "+inlineLabels[i].getText());
					return true;
				}
			}
		}
		return false;
	}
	
}
