package com.simplesoftwares.client.library.appskeleton;

import java.util.List;
import java.util.Vector;

import com.google.gwt.user.client.ui.Widget;


/**
 * The Class MenuItems.Repersents a clickable MenuItem.Object of this class sticks on SideBarPanel and NavigationBarPanel
 */
public class NavigationBarItem extends MenuItems
{
	
	/** contains reference of widget to be pasted as MenuItem
	  */
	Widget label;
	Vector<SideBarItems>sideBarItems;
	
	
	
	
	
	
	
	public NavigationBarItem() {
		super();
		sideBarItems=new Vector<SideBarItems>();
		
	}

	/**
	 * Instantiates a new menu items.
	 *
	 * @param label widget to be pasted as menu item
	 * @param screen the redirection screen
	 * @param screenTitle the screen title of the screen
	 */
	public NavigationBarItem(Widget label, Vector<SideBarItems> items) 
	{
		super();
		this.label = label;
		this.sideBarItems=items;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public Widget getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(Widget label) {
		this.label = label;
	}

	
		
	/**
	 * This method facilitate transition to different screens.It also sets the process name/screen name 
	 * and sets the app in New Mode.
	 */
	public void changeSideBarPanel()
	{
		AppMemory.getAppMemory().skeleton.createSideBarPanel(sideBarItems);
		
	}

	public Vector<SideBarItems> getSideBarItems() {
		return sideBarItems;
	}

	public void setSideBarItems(Vector<SideBarItems> sideBarItems) {
		this.sideBarItems = sideBarItems;
	}
	
	
	
}
