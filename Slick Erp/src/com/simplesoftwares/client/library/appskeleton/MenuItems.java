package com.simplesoftwares.client.library.appskeleton;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.client.ui.Widget;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.businessunitlayer.Company;


/**
 * The Class MenuItems.Repersents a clickable MenuItem.Object of this class sticks on SideBarPanel and NavigationBarPanel
 */
public class MenuItems
{
	
	/** contains reference of widget to be pasted as MenuItem
	  */
	Widget label;
	
	/** Refrence of the screen where the transition will take place when user will  click on MenuItem*/
	AbstractScreen screen;
	
	/** The screen title. Title at screen*/
	String screenTitle;
	
	Logger logger = Logger.getLogger("Menu Items Logger");
	
	
	/**
	 * Instantiates a new menu items.
	 *
	 * @param label widget to be pasted as menu item
	 * @param screen the redirection screen
	 * @param screenTitle the screen title of the screen
	 */
	public MenuItems(Widget label, AbstractScreen screen,String screenTitle) 
	{
		super();
		this.label = label;
		this.screen = screen;
		this.screenTitle=screenTitle;
	}
	
	public MenuItems() {
		
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public Widget getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(Widget label) {
		this.label = label;
	}

	/**
	 * Gets the screen title.
	 *
	 * @return the screen title
	 */
	public String getScreenTitle() {
		return screenTitle;
	}

	/**
	 * Sets the screen title.
	 *
	 * @param screenTitle the new screen title
	 */
	public void setScreenTitle(String screenTitle) {
		this.screenTitle = screenTitle;
	}

	/**
	 * Gets the screen.
	 *
	 * @return the screen
	 */
	public AbstractScreen getScreen() {
		return screen;
	}

	/**
	 * Sets the screen.
	 *
	 * @param screen the new screen
	 */
	public void setScreen(AbstractScreen screen) {
		this.screen = screen;
	}
		
	/**
	 * This method facilitate transition to different screens.It also sets the process name/screen name 
	 * and sets the app in New Mode.
	 */
	public void screenTransition()
	{
		System.out.println("Menu Items");
		
		/**
		 *  Changed By Dipak For Drop Down Handling
		 */
		validateDropDownValues();
		
		/********************************End*****************************/
		
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		AppMemory.getAppMemory().setCurrentScreenandChangeProcessName(screenTitle, screen);
		 screen.changeScreen();
	}
	
	
	
	public void validateDropDownValues()
	{
		int configSize=LoginPresenter.globalConfig.size();
		int categorySize=LoginPresenter.globalCategory.size();
		int typeSize=LoginPresenter.globalType.size();
		int branchSize=LoginPresenter.globalBranch.size();
		int empSize=LoginPresenter.globalEmployee.size();
		int countrySize=LoginPresenter.globalCountry.size();
		int citySize=LoginPresenter.globalCity.size();
		int stateSize=LoginPresenter.globalState.size();
		int localitySize=LoginPresenter.globalLocality.size();
		int processConfig=LoginPresenter.globalProcessConfig.size();
		int processNameSize=LoginPresenter.globalProcessName.size();
		Company c=new Company();
		
		logger.log(Level.SEVERE,"Company ID"+c.getCompanyId());
		logger.log(Level.SEVERE,"Config Size"+configSize);
		logger.log(Level.SEVERE,"Category Size"+categorySize);
		logger.log(Level.SEVERE,"Type Size"+typeSize);
		logger.log(Level.SEVERE,"Employee Size"+branchSize);
		logger.log(Level.SEVERE,"Branch Size"+empSize);
		logger.log(Level.SEVERE,"Country Size"+countrySize);
		logger.log(Level.SEVERE,"City Size"+citySize);
		logger.log(Level.SEVERE,"State Size"+stateSize);
		logger.log(Level.SEVERE,"Locality Size"+localitySize);
		logger.log(Level.SEVERE,"Process Config Size"+processConfig);
		logger.log(Level.SEVERE,"Process Name Size"+processNameSize);
		
		
		
		if(configSize==0){
			String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveConfig(configType);
		}
		
		if(categorySize==0){
			String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveCategory(categoryType);
		}
		
		if(typeSize==0){
			String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveType(typeType);
		}
		
		if(empSize==0){
			String empType=AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveEmployee(empType);
		}
		
		if(branchSize==0){
			String branchType=AppConstants.GLOBALRETRIEVALBRANCH+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveBranch(branchType);
		}
		
		if(countrySize==0){
			String countryType=AppConstants.GLOBALRETRIEVALCOUNTRY+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveCountry(countryType);
		}
		
		if(citySize==0){
			String cityType=AppConstants.GLOBALRETRIEVALCITY+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveCity(cityType);
		}
		
		if(stateSize==0){
			String stateType=AppConstants.GLOBALRETRIEVALSTATE+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveState(stateType);
		}
		
		/**
		 * Date 17-04-2018 By vijay
		 * if DontLoadAllLocalityAndCity process configuration is active then we are not loading all global data for City And Locality
		 * because if we locality has heavy data then our system not respondig properly so this cases we will load locality on basis of City in address composite
		 */
		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			if(localitySize==0){
				String localityType=AppConstants.GLOBALRETRIEVALLOCALITY+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveLocality(localityType);
			}
		}
		/**
		 * ends here
		 */
		
		if(processConfig==0){
			String processConfigType=AppConstants.GLOBALRETRIEVALPROCESSCONFIG+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveProcessConfig(processConfigType);
		}
		
		if(processNameSize==0){
			String processNameType=AppConstants.GLOBALRETRIEVALPROCESSNAME+"-"+c.getCompanyId();
			LoginPresenter.globalMakeLiveProcessName(processNameType);
		}
	}
	
}
