package com.simplesoftwares.client.library.appskeleton;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.libservice.LoginService;
import com.simplesoftwares.client.library.libservice.LoginServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.settings.employee.EmployeeBranch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.personlayer.Employee;

/**
 * Handels all the events corresponding to view transition
 * To Do: Empact of GWT Event bus and history managemnt is not understood.
 * 
 * @author Kamala
 *
 */
public class SkeletonEventHandling implements ClickHandler
{
	
	 Logger logger = Logger.getLogger("Skeleton Handling");
	 public static String clickedModule="";
	 
	@Override
	public void onClick(ClickEvent event) 
	{
		/*
		 * Get the text of currently clicked label.Iterate the items array to find the clicked menuitem.
		 * Once found call its transition method to initiate the screen transition.
		 * 
		 */
		if(event.getSource() instanceof Label)
		{
			HasClickHandlers myLabel=  (HasClickHandlers) event.getSource();
			AppMemory mem=AppMemory.getAppMemory();
			for(int i=0;i<mem.items.size();i++)
			{
				Label temp=(Label)mem.items.get(i).getLabel();
				if(temp==myLabel)
				{
					if(mem.items.get(i) instanceof NavigationBarItem)
					{
						/**
						 *  Changed By Dipak For Module Click Drop Down Handler
						 */
						String separateModule= mem.items.get(i).getLabel().toString();
						separateModule=separateModule.replaceAll("\\<.*?\\>", "");
						LoginPresenter.currentModule=separateModule;
						validateDropDownValues();
						
						/**Changes For Getting Module Name By Dipak On 18 May */
						
						 String text =mem.items.get(i).getLabel().toString();
					     text = text.replaceAll("\\<.*?\\>", "");
					     Console.log("MODULE NAME : "+text);
						clickedModule=text;
						/*************************End**************************/
						NavigationBarItem item=(NavigationBarItem) mem.items.get(i);
						item.changeSideBarPanel();
						Console.log("SIDEBAR CREATION COMPLETED");
						return;
					}
					
					mem.items.get(i).screenTransition(); //changes the screen
					break;
				}
			}
		}
	}
	
	
	public void validateDropDownValues()
	{
		final int configSize=LoginPresenter.globalConfig.size();
		final int categorySize=LoginPresenter.globalCategory.size();
		final int typeSize=LoginPresenter.globalType.size();
		final int branchSize=LoginPresenter.globalBranch.size();
		final int empSize=LoginPresenter.globalEmployee.size();
		final int countrySize=LoginPresenter.globalCountry.size();
		final int citySize=LoginPresenter.globalCity.size();
		final int stateSize=LoginPresenter.globalState.size();
		final int localitySize=LoginPresenter.globalLocality.size();
		final int processConfig=LoginPresenter.globalProcessConfig.size();
		final int processNameSize=LoginPresenter.globalProcessName.size();
		final Company c=new Company();
		
		logger.log(Level.SEVERE,"Company ID"+c.getCompanyId());
		logger.log(Level.SEVERE,"Config Size"+configSize);
		logger.log(Level.SEVERE,"Category Size"+categorySize);
		logger.log(Level.SEVERE,"Type Size"+typeSize);
		logger.log(Level.SEVERE,"Employee Size"+branchSize);
		logger.log(Level.SEVERE,"Branch Size"+empSize);
		logger.log(Level.SEVERE,"Country Size"+countrySize);
		logger.log(Level.SEVERE,"City Size"+citySize);
		logger.log(Level.SEVERE,"State Size"+stateSize);
		logger.log(Level.SEVERE,"Locality Size"+localitySize);
		logger.log(Level.SEVERE,"Process Config Size"+processConfig);
		logger.log(Level.SEVERE,"Process Name Size"+processNameSize);
		
		
		/**
		 * Date 05-06-2018
		 * Developer - Vijay
		 * Description :- if condition added for load global data according to branch level restriction and else block for old code without 
		 * branch level restriction 
		 */
		
		if(LoginPresenter.branchRestrictionFlag){
			  GenricServiceAsync async=GWT.create(GenricService.class);

				MyQuerry querry = new MyQuerry();
				Vector<Filter> filterVec = new Vector<Filter>();
				Filter filter = null;
				
				filter = new Filter();
				filter.setStringValue(LoginPresenter.loggedInUser);
				filter.setQuerryString("fullname");
				filterVec.add(filter);
				
				querry.setFilters(filterVec);
				querry.setQuerryObject(new Employee());
				async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
					
					@Override
					public void onSuccess(ArrayList<SuperModel> result) {
						// TODO Auto-generated method stub
						for(SuperModel model : result){
							Employee employee = (Employee)model;
							
							List<EmployeeBranch> empBranchList = employee.getEmpBranchList();
							String empBranch = employee.getBranchName();
							HashSet<String> hset=new HashSet<String>();
							for(EmployeeBranch eb:empBranchList){
								hset.add(eb.getBranchName());
							}
							hset.add(empBranch);
							List<String> branchList=new ArrayList<String>(hset);
							
							if(empSize==0){
								LoginPresenter.globalMakeLiveEmployee(branchList, c.getCompanyId());
							}
							if(branchSize==0){
								LoginPresenter.globalMakeLiveWithBranchRestriction(empBranchList,empBranch);

							}

							if(configSize==0){
								String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveConfig(configType);
							}
							
							if(categorySize==0){
								String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveCategory(categoryType);
							}
							
							if(typeSize==0){
								String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveType(typeType);
							}
							
							if(countrySize==0){
								String countryType=AppConstants.GLOBALRETRIEVALCOUNTRY+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveCountry(countryType);
							}
							
							if(citySize==0){
								String cityType=AppConstants.GLOBALRETRIEVALCITY+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveCity(cityType);
							}
							
							if(stateSize==0){
								String stateType=AppConstants.GLOBALRETRIEVALSTATE+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveState(stateType);
							}
							
							/**
							 * Date 17-04-2018 By vijay
							 * if DontLoadAllLocalityAndCity process configuration is active then we are not loading all global data for City And Locality
							 * because if we locality has heavy data then our system not respondig properly so this cases we will load locality on basis of City in address composite
							 */
							if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
								if(localitySize==0){
									String localityType=AppConstants.GLOBALRETRIEVALLOCALITY+"-"+c.getCompanyId();
									LoginPresenter.globalMakeLiveLocality(localityType);
								}
							}
							
							/**
							 * ends here
							 */
							
							if(processConfig==0){
								String processConfigType=AppConstants.GLOBALRETRIEVALPROCESSCONFIG+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveProcessConfig(processConfigType);
							}
							
							if(processNameSize==0){
								String processNameType=AppConstants.GLOBALRETRIEVALPROCESSNAME+"-"+c.getCompanyId();
								LoginPresenter.globalMakeLiveProcessName(processNameType);
							}
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				});
			
		}else{
			
			
			if(configSize==0){
				String configType=AppConstants.GLOBALRETRIEVALCONFIG+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveConfig(configType);
			}
			
			if(categorySize==0){
				String categoryType=AppConstants.GLOBALRETRIEVALCATEGORY+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveCategory(categoryType);
			}
			
			if(typeSize==0){
				String typeType=AppConstants.GLOBALRETRIEVALTYPE+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveType(typeType);
			}
			
			if(empSize==0){
				String empType=AppConstants.GLOBALRETRIEVALEMPLOYEE+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveEmployee(empType);
			}
			
			if(branchSize==0){
				String branchType=AppConstants.GLOBALRETRIEVALBRANCH+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveBranch(branchType);
			}
			
			if(countrySize==0){
				String countryType=AppConstants.GLOBALRETRIEVALCOUNTRY+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveCountry(countryType);
			}
			
			if(citySize==0){
				String cityType=AppConstants.GLOBALRETRIEVALCITY+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveCity(cityType);
			}
			
			if(stateSize==0){
				String stateType=AppConstants.GLOBALRETRIEVALSTATE+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveState(stateType);
			}
			
			/**
			 * Date 17-04-2018 By vijay
			 * if DontLoadAllLocalityAndCity process configuration is active then we are not loading all global data for City And Locality
			 * because if we locality has heavy data then our system not respondig properly so this cases we will load locality on basis of City in address composite
			 */
			if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
				if(localitySize==0){
					String localityType=AppConstants.GLOBALRETRIEVALLOCALITY+"-"+c.getCompanyId();
					LoginPresenter.globalMakeLiveLocality(localityType);
				}
			}
			
			/**
			 * ends here
			 */
			
			if(processConfig==0){
				String processConfigType=AppConstants.GLOBALRETRIEVALPROCESSCONFIG+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveProcessConfig(processConfigType);
			}
			
			if(processNameSize==0){
				String processNameType=AppConstants.GLOBALRETRIEVALPROCESSNAME+"-"+c.getCompanyId();
				LoginPresenter.globalMakeLiveProcessName(processNameType);
			}
		}
		
		
	}
	

	
	
}
