
package com.simplesoftwares.client.library.appskeleton;


/**
 * A class which abstracts the Screens in project.Concrete Screen which is project specific provides its implementation.
 * Concrete screen is responsible for the code which manages the view transition.
 * When history management will be integrated
 * the concrete implementation will be responsible for Gwt event bus and all that.
 * To Do Gwt history management and event bus.
 * @author Ajay
 * @version 1
 */
public interface AbstractScreen {
	
	/**
	 * This method is responsible for actual transition.Implemented by concrete screen
	
	 */
	
	public void changeScreen();
	}
