package com.simplesoftwares.client.library;

public enum FieldType 
{
	ListBox,TextBox,TextArea,DateBox,SuggestBox,Hyperlink,PasswordTextBox,DatePicker,
	CheckBox,RadioButton,Button, FileUpload,
	IntegerBox,DoubleBox,Empty,Grouping,LongBox,TAXCOMPOSITE,
	MyLongBox,SocialInformation,TaxInformation,CustomerInformation,
	CityListBox, CityStateBox, CountryListBox, 
	StateListBox,ObjectListBox,
	SEARCHDATECOMPOSITE, PERSONINFOCOMPOSITE, COMMONSEARCHCOMPOSITE,
	OneToManyComposite, PersonInfoComposite, FormPanel, Adress, MYUPLOAD, PAYMENTCOMPOSITE, TABLE,GREYGROUPING,
	/** date 10.4.2018 added by komal **/
	HALFGROUPING;	
;	
}
