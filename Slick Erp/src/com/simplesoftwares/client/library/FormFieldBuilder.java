package com.simplesoftwares.client.library;

import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class is used to instantiate the {@link FormField} class as per the fields built over this class.
 * @author Kamala
 *
 */
public class FormFieldBuilder
{
	/**Label of the widget field*/
	public String label;
	
	/**Rowspan for the widget field*/
	public int rowspan;
	
	/**Rowspan for the widget field*/
	public int colspan;
	
	/**True if the widget field is mandatory*/
	public boolean isMandatory;
	
	/**Type of the widget field*/
	public FieldType widgetType;
	
	/**Widget*/
	public Widget widget;
	
	/**Mandatory message for mandatory fields*/
	public String mandatoryMsg;
	
	public InlineLabel mandatoryMsgWidget;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 */
	public boolean isVisible=true;
	
	/**
	 * @author Anil
	 * @since 12-10-2020
	 * For Menu bar screen we need to separate key fields from Tab view panel
	 */
	public boolean isKeyField=false;
	
	public FormFieldBuilder()
	{
		super();
	}
	
	/**Parameterized constructor with mandatory arguments*/
	public FormFieldBuilder(String label,Widget widg)
	{
		super();
		this.label = label;
		this.widget=widg;
		this.isMandatory=false;
	}
	
	/**
	 * Sets the label of the UI field and returns object of this class
	 * @param label new label
	 * @return this class's object
	 */
	public FormFieldBuilder setlabel(String label)
	{
		this.label=label;
		return this;
	}
	
	/**
	 * Sets the rowspan for the UI field and returns object of this class
	 * @param rowspan
	 * @return this class's object
	 */
	public FormFieldBuilder setRowSpan(int rowspan)
	{
		this.rowspan=rowspan;
		return this;
	}
	 
	public FormFieldBuilder setMandatoryMsgInlinelable(InlineLabel mandatorymsgLabel)
	{
		this.mandatoryMsgWidget=mandatorymsgLabel;
		return this;
	}
	/**
	 * Sets the colspan for the UI field and returns object of this class
	 * @param colspan
	 * @return this class's object
	 */
	public FormFieldBuilder setColSpan(int colspan)
	{
		this.colspan=colspan;
		return this;
	}
	 
	/**
	 * Sets whether the UI field is mandatory or not 
	 * @param isMandatory
	 * @return this class's object
	 */
	public FormFieldBuilder setMandatory(boolean isMandatory)
	{
		this.isMandatory=isMandatory;
		return this;
	}
	 
	
	/**
	 * Sets the widget type of the UI field 
	 * @param widgetType
	 * @return this class's object
	 */
	public FormFieldBuilder widgetType(FieldType widgetType)
	{
		this.widgetType=widgetType;
		return this;
	}
	  /**
	   * Sets the widget
	   * @param widget
	   * @return this class's object
	   */
	public FormFieldBuilder widget(Widget widget)
	{
		this.widget=widget;
		return this;
	}
	
	/**
	 * Sets the mandatory msg on mandatory UI field
	 * @param msg
	 * @return this class's object
	 */
	public FormFieldBuilder setMandatoryMsg(String msg)
	{
		this.mandatoryMsg=msg;
		return this;
	}
	/**
	 * Instantiates the {@link Formfield} constructor
	 * @return
	 */
     public FormField build() {
         return new FormField(this);
     }
     
     /**
      * @author Anil
      * @since 28-05-2020
      */
     public FormFieldBuilder setVisible(boolean isVisible){
 		this.isVisible=isVisible;
 		if(isVisible==false){
 			label="";
 		}
 		widget.setVisible(isVisible);
 		return this;
 	 }

	public boolean isKeyField() {
		return isKeyField;
	}

	public FormFieldBuilder setKeyField(boolean isKeyField) {
		this.isKeyField = isKeyField;
		return this;
	}
     
     
}
