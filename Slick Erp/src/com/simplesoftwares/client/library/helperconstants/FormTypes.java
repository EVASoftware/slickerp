package com.simplesoftwares.client.library.helperconstants;

public class FormTypes {
	/** Repersents TextColumn */
	public static final String TEXTCOLUM = "TextColumn";
	
	/** Repersents Column */
	public static final String COLUMN="Column";
	/** Repersents EditTextCell */
	public static final String EDITTEXTCELL="EditTextCell";
	/** Repersents NUBERTEXTCELL */
	public static final String NUMBERTEXTCELL="NumberTextCell";
	/** Repersents DATE TEXT CELL */
    public static final String DATETEXTCELL="DateTextCell";

}
