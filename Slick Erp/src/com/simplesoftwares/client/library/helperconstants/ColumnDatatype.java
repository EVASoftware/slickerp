package com.simplesoftwares.client.library.helperconstants;

public class ColumnDatatype {
	

	/** The Constant TEXTBOX. */
	public static final String TEXTBOX="TextBox";
	
	/** The Constant INTEGERBOX. */
	public static final String INTEGERBOX="IntegerBox";
	
	/** The Constant DOUBLEBOX. */
	public static final String DOUBLEBOX="DoubleBox";
	
	/** The Constant LISTBOX. */
	public static final String LISTBOX="ListBox";

	public static final String DATEBOX = "DateBox";

	public static final String SUGGESTBOX = "SuggestBox";

	public static final String PERSONINFO = "PersonInfo";

	public static final String COMMONSEARCHCOMPOSITE="CommonSearchComposite";
	
	public static final String DATECOMPARATOR="DateComparator";

	public static final String OBJECTLISTBOXCONFIG = "ObjectListBox<Config>";
	public static final String OBJECTLISTBOXBRANCH = "ObjectListBox<Branch>";
	public static final String OBJECTLISTBOXEMPLOYEE = "ObjectListBox<Employee>";
	public static final String OBJECTLISTBOXTYPE="ObjectListBox<Type>";
	public static final String OBJECTLISTBOXCONFIGCATEGORY="ObjectListBox<ConfigCategory>";

	public static final String LONGBOX ="LongBox";
	public static final String CHECKBOX="CheckBox";

}
