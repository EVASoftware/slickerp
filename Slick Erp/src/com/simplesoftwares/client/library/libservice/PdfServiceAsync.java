package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;


public interface PdfServiceAsync {
	public void createPdf(Quotation quot,AsyncCallback<Void> callback);
	public void setServiceList(ArrayList<Service>aray,AsyncCallback<Void> callback);
	public void createPdf(Invoice inv,AsyncCallback<Void> callback);
	public void createPdf(SalesQuotation squot,AsyncCallback<Void> callback);
	public void createPdf(PurchaseOrder purchaseorder,AsyncCallback<Void> callback);
	public void createPdf(CompanyPayment companypayment,AsyncCallback<Void> callback);
	public void createPdf(DeliveryNote dnote,AsyncCallback<Void> callback);
	public void createPdf(VendorPayment vendorpayment,AsyncCallback<Void> callback);
	public void createPdf(Service customerservice,AsyncCallback<Void> callback);
	public void createPdf(Inspection inspection,AsyncCallback<Void> callback);
	public void createPdf(WorkOrder workOrder,AsyncCallback<Void> callback);
	public void createPdf(CustomerPayment custPayment,AsyncCallback<Void> callback);
	public void createPdf(MaterialConsumptionReport report,AsyncCallback<Void> callback);
}
