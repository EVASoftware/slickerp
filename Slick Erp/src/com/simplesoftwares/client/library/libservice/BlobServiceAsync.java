package com.simplesoftwares.client.library.libservice;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public interface BlobServiceAsync {
	
	void getBlobStoreUploadUrl(AsyncCallback<String> callback);

	void getDocument(String id, AsyncCallback<DocumentUpload> callback);
	
	/** Date Date 18-07-2019 by Vijay for NBHC CCPM Contract Upload ***/
	void getBlobStoreNewUploadUrl(String transactionName,AsyncCallback<String> callback);

}
