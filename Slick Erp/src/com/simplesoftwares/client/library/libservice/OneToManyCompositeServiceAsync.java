package com.simplesoftwares.client.library.libservice;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;

public interface OneToManyCompositeServiceAsync<T,C> {
	public  void LoadOneToManyHashMap(MyQuerry one,MyQuerry many,AsyncCallback<HashMap<T,List<C>>> callback);
	
	
	

}
