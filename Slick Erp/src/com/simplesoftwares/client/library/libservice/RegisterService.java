package com.simplesoftwares.client.library.libservice;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../registerservice")
public interface RegisterService extends RemoteService{
	
	public void registerClass();

}
