package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;


@RemoteServiceRelativePath("../personinfoservice")
public interface PersonInfoService extends RemoteService {

	public ArrayList<PersonInfo> LoadVinfo(MyQuerry querry,String loggedinUser);
	public ArrayList<PersonInfo> LoadPinfo(Long companyId,int countVal,String entityType);
	
	/**
	 * Date 02-11-2018 by Vijay
	 * Des:- To load large customer Data with process config 
	 */
	public ArrayList<PersonInfo> LoadCustomerPinfo(MyQuerry querry, int customerCount);

}
