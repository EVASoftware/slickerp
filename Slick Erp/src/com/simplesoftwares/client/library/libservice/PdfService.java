package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.paymentmodelayer.vendorpayment.VendorPayment;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

@RemoteServiceRelativePath("printservice")
public interface PdfService extends RemoteService  {
	public void createPdf(Quotation quot);
	public void setServiceList(ArrayList<Service>aray) throws IllegalArgumentException;
	public void createPdf(Invoice inv);
	public void createPdf(SalesQuotation squot);
	public void createPdf(PurchaseOrder purchaseorder);
	public void createPdf(CompanyPayment companypayment);
	public void createPdf(DeliveryNote dnote);
	public void createPdf(VendorPayment vendorpayment);
	public void createPdf(Service customerservice);
	public void createPdf(Inspection inspection);
	public void createPdf(WorkOrder workOrder);
	public void createPdf(CustomerPayment custPayment);
	public void createPdf(MaterialConsumptionReport report);
}
