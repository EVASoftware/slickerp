package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.impl.Keys;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;



@RemoteServiceRelativePath("../employeeinfoservice")
public interface EmployeeInfoService extends RemoteService{
	
	public ArrayList<EmployeeInfo> LoadVinfo(MyQuerry querry);

	ArrayList<Key<EmployeeInfo>> LoadEmployeeInfoRef(MyQuerry quer);
	
	

}
