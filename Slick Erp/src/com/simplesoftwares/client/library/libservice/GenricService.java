package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.Contract;

@RemoteServiceRelativePath("../genricservice")
public interface GenricService extends RemoteService {
	
	public ReturnFromServer save(SuperModel m);
	public ReturnFromServer saveDemoConfigData(SuperModel m);
	public ArrayList<ReturnFromServer> save(ArrayList<SuperModel> m);
	public void update(SuperModel m);
	public void delete(SuperModel m);
	public void print (Long id);
	public void download(ArrayList<SuperModel>m);
	public void email(Long id);
 
	ArrayList<SuperModel> getSearchResult(MyQuerry quer);
	int getCount(SuperModel m);
	void putInMemCache(SuperModel model);
	
	/**
	 * Date : 15-06-2017 BY ANIL
	 * adding new search method for contract
	 */
	ArrayList<Contract> getContractSearchResult(MyQuerry quer);
	/**
	 * End
	 */
	
	public String nbhcContractServiceCycleDelProcess(Long companyId);
	
	/**
	 * Date : 22-03-2018 BY ANIL
	 * This method is used to save data at the time of link creation when process name and number generation is not defined 
	 */
	public ReturnFromServer saveForNewLink(SuperModel m);
	/**
	 * End
	 */
	
	/*
	 *  * nidhi
	 * 17-05-2018
	 * 
	 */
	public String contractServiceCycleCanceltionProcess(Long companyId);
	
	/**
	 * @author Nidhi 6-11-2018 
	 */
	public String productPriceUtilty(Long companyId);
}
