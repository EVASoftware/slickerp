package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Type;

@RemoteServiceRelativePath("../categoryservice")
public interface CategoryTypeService extends RemoteService
{
	public HashMap<ConfigCategory,ArrayList<Type>> LoadOneToManyHashMap(MyQuerry one,MyQuerry many);
	ArrayList<SuperModel> getSearchResult(MyQuerry quer);
}
