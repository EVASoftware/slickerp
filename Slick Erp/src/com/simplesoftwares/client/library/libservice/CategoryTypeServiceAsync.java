package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Type;

public interface CategoryTypeServiceAsync
{
	public  void LoadOneToManyHashMap(MyQuerry one,MyQuerry many,AsyncCallback<HashMap<ConfigCategory,ArrayList<Type>>> callback);
	void getSearchResult(MyQuerry quer,AsyncCallback<ArrayList<SuperModel>>callback);
}
