package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;

public interface PersonInfoServiceAsync {
	 void LoadVinfo(MyQuerry querry,String loggedinUser,AsyncCallback<ArrayList<PersonInfo>>callback);
	 void LoadPinfo(Long companyId,int countVal,String entityType,AsyncCallback<ArrayList<PersonInfo>>callback);

	/**
	 * Date 02-11-2018 by Vijay
	 * Des:- To load large customer Data with process config for customer composite
	 */
	 void LoadCustomerPinfo(MyQuerry querry, int customerCount,AsyncCallback<ArrayList<PersonInfo>>callback);
}
