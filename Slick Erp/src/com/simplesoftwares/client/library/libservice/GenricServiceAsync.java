package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.Contract;

public interface GenricServiceAsync {
	
	void save(SuperModel m,AsyncCallback<ReturnFromServer>callback);
	void saveDemoConfigData(SuperModel m,AsyncCallback<ReturnFromServer>callback);
	void save(ArrayList<SuperModel> m,AsyncCallback<ArrayList<ReturnFromServer>>callback);
	
	void update(SuperModel m,AsyncCallback<Void>callback);
	void delete(SuperModel m,AsyncCallback<Void>callback);
	public void print (Long id,AsyncCallback<Void>callback);
	public void download(ArrayList<SuperModel>m,AsyncCallback<Void>callback);
	public void email(Long id,AsyncCallback<Void>callback);
	
	
	void getSearchResult(MyQuerry quer,AsyncCallback<ArrayList<SuperModel>>callback);
	void  getCount(SuperModel m,AsyncCallback<Integer>callback);
	
	public void putInMemCache(SuperModel model,AsyncCallback<Void>callback);

	/**
	 * Date : 15-06-2017 BY ANIL
	 * adding new search method for contract
	 */
	void getContractSearchResult(MyQuerry quer,AsyncCallback<ArrayList<Contract>>callback);
	/**
	 * End
	 */
	
	void nbhcContractServiceCycleDelProcess(Long companyId,AsyncCallback<String>callback);
	
	/**
	 * Date : 22-03-2018 BY ANIL
	 * This method is used to save data at the time of link creation when process name and number generation is not defined 
	 */
	void saveForNewLink(SuperModel m,AsyncCallback<ReturnFromServer>callback);
	/**
	 * End
	 */
	void contractServiceCycleCanceltionProcess(Long companyId,AsyncCallback<String> callback);
	
	/**
	 * @author Nidhi 6-11-2018 
	 */
	void productPriceUtilty(Long companyId,AsyncCallback<String> callback);
}
