package com.simplesoftwares.client.library.libservice;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.LoginProxy;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;

@RemoteServiceRelativePath("../loginservice")
public interface LoginService extends RemoteService{
	
	public User giveLogin(LoginProxy loginproxy);
	public CustomerUser giveCustLogin(LoginProxy loginproxy);
	public Employee getBranchLevelRestriction(User user);
	public String processForgotPasswordRequest(String companyName,String uname,String email);
	public Boolean isOldPasswordValid(User user,String oldPassword);
	public String sendPasswordResetEmailToCompanyAndBranch(String companyName,String uname);//Ashwini Patil Date:27-04-2023
}
