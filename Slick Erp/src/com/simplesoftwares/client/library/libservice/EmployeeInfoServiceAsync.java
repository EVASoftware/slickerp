package com.simplesoftwares.client.library.libservice;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;


public interface EmployeeInfoServiceAsync {

	void LoadVinfo(MyQuerry querry,AsyncCallback<ArrayList<EmployeeInfo>>callback);
	void LoadEmployeeInfoRef(MyQuerry quer,AsyncCallback<ArrayList<Key<EmployeeInfo>>>callback);
}
