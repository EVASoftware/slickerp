package com.simplesoftwares.client.library.libservice;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.slicktechnologies.shared.common.helperlayer.CustomerUser;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.LoginProxy;
import com.slicktechnologies.shared.common.helperlayer.User;
import com.slicktechnologies.shared.common.personlayer.Employee;

public interface LoginServiceAsync {
	
	public void giveLogin(LoginProxy loginproxy,AsyncCallback<User>callback);
	public void giveCustLogin(LoginProxy loginproxy,AsyncCallback<CustomerUser>callback);
	public void getBranchLevelRestriction(User user,AsyncCallback<Employee> asyncCallback);
	public void processForgotPasswordRequest(String companyName,String uname,String email,AsyncCallback<String>callback);
	public void isOldPasswordValid(User user,String oldPassword,AsyncCallback<Boolean>callback);	
	public void sendPasswordResetEmailToCompanyAndBranch(String companyName,String uname,AsyncCallback<String>callback);
	
}
