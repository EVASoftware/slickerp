package com.simplesoftwares.client.library.libservice;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;


@RemoteServiceRelativePath("../blobservice")
public interface BlobService extends RemoteService{
	
	String getBlobStoreUploadUrl();

	DocumentUpload getDocument(String id);

	/** Date Date 18-07-2019 by Vijay for NBHC CCPM Contract Upload ***/
	String getBlobStoreNewUploadUrl(String transactionName);


}
