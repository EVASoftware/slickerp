package com.simplesoftwares.client.library.libservice;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RegisterServiceAsync {

	public void registerClass(AsyncCallback<Void> calback);
}
