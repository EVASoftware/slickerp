/*
 * 
 */
package com.simplesoftwares.client.library.libservice;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;

// TODO: Auto-generated Javadoc
/**
 * The Interface OneToManyCompositeService.
 *
 * @param <T> the generic type
 * @param <C> the generic type
 */
@RemoteServiceRelativePath("../onetomanyservice")
public interface OneToManyCompositeService<T extends SuperModel, C extends SuperModel> extends RemoteService {
	
	/**
	 * Load one to many hash map.Imlpementation of this method is dependent on data model of One kind
	 * and many Kind
	 *
	 * @param one the one {@link MyQuerry} of one kind 
	 * @param many the many {@link MyQuerry} of Many kind 
	 * @return Hashmap containng One kind as Key and corresponding arraylist of many as value.
	 */
	public HashMap<T,List< C>> LoadOneToManyHashMap(MyQuerry one,MyQuerry many);

}
