package com.simplesoftwares.client.library.composite;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.slicktechnologies.shared.common.helperlayer.TaxInfo;


// TODO: Auto-generated Javadoc
/**
 * The Class TaxInformationComposite.A simple composite corresponding to {@link TaxInformation} entity
 */
public class TaxInformationComposite extends Composite implements HasValue<TaxInfo>,Editor<TaxInfo>
,CompositeInterface
{
    
    /** The taxfield3. */
    private TextBox vatNo,taxfield1,taxfield2,taxfield3;
    
    /** The panel. */
    private FlowPanel panel=new FlowPanel();
    
    /** The form. */
    private FlexForm form;
	
	/** The ftax3. */
	private FormField fvatno,ftax1,ftax2,ftax3;
	
	/** The builder. */
	private FormFieldBuilder builder;
    
    /** The ff. */
    private FormField[][]ff;
	
	/**
	 * Instantiates a new tax information composite.
	 */
	public TaxInformationComposite() {
		createGui();
		applyStyle();
		initWidget(panel);
		
	}
	
	/**
	 * Initializewidgets.
	 */
	private void initializewidgets()
	{
		vatNo=new TextBox();
		taxfield1=new TextBox();
		taxfield2=new TextBox();
		taxfield3=new TextBox();
	}
	
	/**
	 * Creates the gui.
	 */
	private void createGui()
	{
		
		panel=new FlowPanel();
		initializewidgets();
		 builder=new FormFieldBuilder("VAT No",vatNo);
		 fvatno=builder.build();
		builder=new FormFieldBuilder("Tax Field 1",taxfield1);
		ftax1=builder.build();
		 builder=new FormFieldBuilder("Tax Field 2",taxfield2);
		 ftax2=builder.build();
		 builder=new FormFieldBuilder("Tax Field 3",taxfield3);
		 ftax3=builder.build();
		
		
		ff=new FormField[][]{{fvatno,ftax1,ftax2,ftax3}};
		form=new FlexForm(ff,FormStyle.ROWFORM);
		panel.add(form);
		
		
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<TaxInfo> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#getValue()
	 */
	/**
	 * @return -{@link TaxInformation} object
	 */
	@Override
	public TaxInfo getValue() {
		String vat=this.vatNo.getValue();
		String tax1=this.taxfield1.getValue();
		String tax2=this.taxfield2.getValue();
		String tax3=this.taxfield3.getValue();
		if(vat==null)
			vat="";
		if(tax1==null)
			tax1="";
		if(tax2==null)
			tax2="";
		if(tax3==null)
			tax3="";
	
		TaxInfo entity=new TaxInfo();
			entity.setTaxInfo1(tax1);
			entity.setTaxInfo2(tax2);
			entity.setTaxInfo3(tax3);
			entity.setVat(vat);
			return entity;
	
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
	 */
	/**
	 * Sets the composite with {@link TaxInfo} object.
	 */
	@Override
	public void setValue(TaxInfo entity) {
		if(entity!=null)
		{
		this.taxfield1.setText(entity.getTaxInfo1());
		this.taxfield2.setText(entity.getTaxInfo2());
		this.taxfield3.setText(entity.getTaxInfo3());
		this.vatNo.setText(entity.getVat());
		}
		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object, boolean)
	 */
	@Override
	public void setValue(TaxInfo value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	@Override
	public void clear()
	{
		int row=ff.length;
		for(int k=0;k<row;k++)
		{
			int colCount =ff[k].length ;
			for(int j=0;j<colCount;j++)
			{
				Widget widg= ff[k][j].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setText("");
				
				}
				
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean status)
	{
		int row=ff.length;
		for(int k=0;k<row;k++)
		{
			int colCount =ff[k].length ;
			for(int j=0;j<colCount;j++)
			{
				Widget widg= ff[k][j].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setEnabled(status);
				
				}
				
			}
		}
	}
	
	/**
	 * Sets the form width.
	 */
	public void applyStyle()
	{
		form.setWidth("98%");
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
	 */
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}	
					

}
