package com.simplesoftwares.client.library.composite;

import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;


/**
 * The Class CommonSearchComposite.Utility class.Eliminates the need of creating filter 
 * for repetitive fromDate,toDate.This class places and undesirable burden on entiti's as variable names 
 * for count and creationDate needs to be same.
 * TO DO:Komal and Pradnya should use reflection on server side and give a massive functionality enhancement to MyFilterValue
 * and MyQuerry.See Slim3 for reference.
 */
public class CommonSearchComposite extends Composite implements HasValue<Vector<Filter>>,CompositeInterface,
ValueChangeHandler<Date>
{
	
	/** The from date. */
	protected DateBox fromDate;
	
	/** The to date. */
	protected DateBox toDate;
	
	
	
	/** The content. */
	public FlowPanel content;
	
	/** The form. */
	public FlexForm form;
	
	/** The fields. */
	public FormField[][]fields;
	
	/** The field to date. */
	private FormField fieldFromDate,fieldToDate;
	
	/** The builder. */
	private FormFieldBuilder builder;
	
	/**
	 * Instantiates a new common search composite.
	 */
	public CommonSearchComposite()
	{
		createGui();
		initWidget(content);
	
	}
	
	/**
	 * Initialize widgets. Also apply Handlers on manditory fields.
	 */
	private void initializewidgets()
	{
		fromDate=new DateBoxWithYearSelector();
		toDate=new DateBoxWithYearSelector();
		toDate.addValueChangeHandler(this);
		
	}

	/**
	 * Creates the gui using FlexForm
	 */
	private void createGui()
	{
		content= new FlowPanel();
		initializewidgets();
		
		
		 builder=new FormFieldBuilder("From Date",fromDate);
		 fieldFromDate=builder.setMandatory(true).build();
		 builder=new FormFieldBuilder("To Date",toDate);
		 fieldToDate=builder.setMandatory(true).build();
		
		fields=new FormField[][]{{fieldFromDate,fieldToDate}};
		
		
		
		 form=new FlexForm(fields,FormStyle.ROWFORM);
			
			//applyStyle();
			content.add(form);
		
	}



	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Vector<Filter>> handler) 
	{
	
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#getValue()
	 * 
	 */
	/**
	 * Returns the composite value which is {@link Filter} type object.
	 */
	@Override
	public Vector<Filter> getValue() 
	{
		Vector<Filter>filtervec=new Vector<Filter>();
		Filter filtervalue=new Filter();
	    
	
		if(this.fromDate.getValue()!=null)
		{
			filtervalue.setDateValue(this.fromDate.getValue());
			filtervalue.setQuerryString("creationDate>");
			filtervec.add(filtervalue);
			
		}
		
		if(this.toDate.getValue()!=null)
		{
			filtervalue.setDateValue(this.fromDate.getValue());
			filtervalue.setQuerryString("creationDate<");
			filtervec.add(filtervalue);
			
		}
		if(filtervec.size()==0)
			return null;
		else
			return filtervec;

	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
	 * Set Value is not implemented as it does not makes any sense in present use case.
	 */

	@Override
	public void setValue(Vector<Filter> entity)
	{
		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object, boolean)
	 */
	@Override
	public void setValue(Vector<Filter>value , boolean fireEvents)
	{
	
		
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean status) {
		
		
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	@Override
	public void clear() {
		fromDate.getTextBox().setText("");
		toDate.getTextBox().setText("");
		
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
	 */
	@Override
	public boolean validate() {
		int res=fromDate.getValue().compareTo(toDate.getValue());
		if(res<0)
			return false;
		else
			return true;
		
	}



	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		DateBox b=(DateBox) event.getSource();
		b.getElement().getStyle().clearBackgroundColor();
		
	}

}
