package com.simplesoftwares.client.library.composite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.CategoryTypeService;
import com.simplesoftwares.client.library.libservice.CategoryTypeServiceAsync;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.config.categoryconfig.CategoryTypes;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;

public class CategoryType implements ChangeHandler
{
	/** The one combo box. */
	private ObjectListBox<ConfigCategory> oneComboBox;
	
	/** The many combo box. */
	private ObjectListBox<Type> manyComboBox;
	
	/** The one query. Decides entities in one combobox */
	private MyQuerry oneQuerry;
	
	/** The many query. Decides entities in many combobox*/
	private MyQuerry manyQuerry;
	
	 private  HashMap<ConfigCategory,ArrayList<Type>>oneToManyHashMap;

	private final CategoryTypeServiceAsync service=GWT.create(CategoryTypeService.class);
	
	
	public CategoryType()
	{
		
	}

	public CategoryType(ObjectListBox<ConfigCategory> onecombo,ObjectListBox<Type> manycombo)
	{
		this.oneComboBox=onecombo;
		this.manyComboBox=manycombo;
		oneComboBox.addChangeHandler(this);
	}
	public void initializeHashMaps()
	{
		
		   
		service.LoadOneToManyHashMap(oneQuerry, manyQuerry, new AsyncCallback<HashMap<ConfigCategory,ArrayList<Type>>>() 
		{

			@Override
			public void onFailure(Throwable caught)
			{
				System.out.println("FAILED  !!! "+caught);
			}

			@Override
			public void onSuccess(HashMap<ConfigCategory, ArrayList<Type>> result) 
			{
				oneToManyHashMap=result;
				System.out.println("Hashmap--------------"+oneToManyHashMap.size());
				setCategoryListBox();
			}
		});
		//oneComboBox.addChangeHandler(this);
    }
	
	 private void setCategoryListBox()
	    {
	    	Iterator<Entry<ConfigCategory, ArrayList<Type>>>  it = oneToManyHashMap.entrySet().iterator();
	    	Vector<ConfigCategory>catvector=new Vector<ConfigCategory>();
	        while (it.hasNext()) {
	            Entry<ConfigCategory, ArrayList<Type>> pairs = it.next();
	            catvector.add(pairs.getKey());
	            System.out.println("Vector:::::::::::"+catvector.size());
	            }
	        this.oneComboBox.setListItems(catvector);
	    }
	 
	 protected void reactOnChange()
		{
			if(oneComboBox.getSelectedIndex()==0)
			{
				// Set many combobox to zeroth index.
				manyComboBox.setSelectedIndex(0);
				
			}
			
			else{
			
			ConfigCategory prodCat=oneComboBox.getSelectedItem();
		    List<Type>vec=this.oneToManyHashMap.get(prodCat);
		    for(Type temp:vec)
		    	System.out.println("type name #################  "+temp.getTypeName());
		    System.out.println("Vec"+vec);
		    Vector<Type> vecprod=new Vector<Type>();
		    vecprod.addAll(vec);
		   // System.out.println("Product:::::::"+vecprod);
		    manyComboBox.setListItems(vecprod);
			}
		}
		
		/* (non-Javadoc)
		 * @see com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt.event.dom.client.ChangeEvent)
		 */
		
		@Override
		public void onChange(ChangeEvent event) 
		{
			//System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
			reactOnChange();
		 }

		public ObjectListBox<ConfigCategory> getOneComboBox() {
			return oneComboBox;
		}

		public void setOneComboBox(ObjectListBox<ConfigCategory> oneComboBox) {
			this.oneComboBox = oneComboBox;
		}

		public ObjectListBox<Type> getManyComboBox() {
			return manyComboBox;
		}

		public void setManyComboBox(ObjectListBox<Type> manyComboBox) {
			this.manyComboBox = manyComboBox;
		}

		public MyQuerry getOneQuerry() {
			return oneQuerry;
		}

		public void setOneQuerry(MyQuerry oneQuerry) {
			this.oneQuerry = oneQuerry;
		}

		public MyQuerry getManyQuerry() {
			return manyQuerry;
		}

		public void setManyQuerry(MyQuerry manyQuerry) {
			this.manyQuerry = manyQuerry;
		}

		public HashMap<ConfigCategory, ArrayList<Type>> getOneToManyHashMap() {
			return oneToManyHashMap;
		}

		public void setOneToManyHashMap(HashMap<ConfigCategory, ArrayList<Type>> oneToManyHashMap) {
			this.oneToManyHashMap = oneToManyHashMap;
		}

		public CategoryTypeServiceAsync getService() {
			return service;
		}
		
		
}
