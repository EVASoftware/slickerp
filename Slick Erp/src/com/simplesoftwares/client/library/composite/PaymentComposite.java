package com.simplesoftwares.client.library.composite;

import java.util.List;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.mywidgets.NumbersOnly;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.contract.PaymentTable;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.paymentlayer.Payment;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class PaymentComposite extends Composite implements HasValue<Payment>,ValueChangeHandler<Double>,CompositeInterface
{

	public DoubleBox totalAmt;
	public DoubleBox netPayable;
	public DoubleBox percentageDiscount;
	public DoubleBox flatdiscount;

	public DoubleBox advance,balance;
	public TextBox chequenumber,bankname,bankbranch,cardNo;
	public DoubleBox cheqamt,cashamt,credidamt;
	public DateBox chequeDate;
	
	public FlexForm form ;
	
	  private FormField[][]ff;
	  private FlowPanel panel;
	  
	  
	  
	  public PaymentComposite()
	  {
		  super();
			createForm();
			initWidget(panel);
			applyStyle();
			advance.setEnabled(false);
			balance.setEnabled(false);
			totalAmt.setEnabled(false);
			netPayable.setEnabled(false);
	  }
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Payment> handler) 
	{
		
		return null;
	}

	@Override
	public Payment getValue()
	{
		Payment entity = new Payment();
		entity.setCardAmount(credidamt.getValue());
	    entity.setChqAmount(cheqamt.getValue());
		entity.setchqDate(chequeDate.getValue());
		entity.setbankName(bankname.getValue());
		entity.setbranch(bankbranch.getValue());
		entity.setchequeNo(chequenumber.getValue());
		entity.setCashAmount(cashamt.getValue());

		return entity;
	}

	@Override
	public void setValue(Payment value)
	{
		credidamt.setValue(value.getCardAmount());
		
		cheqamt.setValue(value.getChqAmount());
		cashamt.setValue(value.getCashAmount());
		
		chequenumber.setValue(value.getChequeNo());
		chequeDate.setValue(value.getChqDate());
		bankname.setValue(value.getbankName());
		bankbranch.setValue(value.getbranch());
		totalAmt.setValue(value.getTotalAmount());
		
	}

	@Override
	public void setValue(Payment value, boolean fireEvents) 
	{
		
		
	}
	
	private void initializewidgets()
	{
		
		credidamt=new DoubleBox();
		cheqamt=new DoubleBox();
		cashamt=new DoubleBox();
		chequenumber=new TextBox();
		chequeDate=new DateBox();
		bankname=new TextBox();
		bankbranch=new TextBox();
		totalAmt=new DoubleBox();
		percentageDiscount=new DoubleBox();
		flatdiscount=new DoubleBox();
		netPayable=new DoubleBox();
		advance=new DoubleBox();
		balance=new DoubleBox();
		
		percentageDiscount.addValueChangeHandler(this);
		flatdiscount.addValueChangeHandler(this);
		totalAmt.addValueChangeHandler(this);
		cashamt.addValueChangeHandler(this);
		credidamt.addValueChangeHandler(this);
		cheqamt.addValueChangeHandler(this);
		percentageDiscount.addKeyPressHandler(new NumbersOnly());
		flatdiscount.addKeyPressHandler(new NumbersOnly());
	}
	
	private void createForm()
	{
		initializewidgets();
		FormFieldBuilder builder;
	     FormField CreditAmt,CashAmt,ChequeAmt,ChequeNo,ChequeDate,BankName,BranchName,TotalAmt,
	     PercentageDiscount,FlatDiscount,NetPayable,Balance;
	     
	     builder= new FormFieldBuilder("Card Amount",credidamt);
	     CreditAmt=builder.setMandatory(false).build();
		
	     builder=new FormFieldBuilder("Cash Amount",cashamt);
	     CashAmt=builder.build();
	     
	     builder=new FormFieldBuilder("Cheque Amount",cheqamt);
	     ChequeAmt=builder.build();
	     
	     builder=new FormFieldBuilder("Cheque Number",chequenumber);
	     ChequeNo=builder.build();
	     
	     builder=new FormFieldBuilder("Cheque Date",chequeDate);
	     ChequeDate=builder.build();
	     
	     builder=new FormFieldBuilder("Bank Name",bankname);
	     BankName=builder.build();
	     
	     builder=new FormFieldBuilder("Branch Name",bankbranch);
	     BranchName=builder.build();
	     
	     builder=new FormFieldBuilder("Total Amount",totalAmt);
	     TotalAmt=builder.build();
	     
	     builder=new FormFieldBuilder("Percentage Discount",percentageDiscount);
	     PercentageDiscount=builder.build();
	     
	     builder=new FormFieldBuilder("Flat Discount",flatdiscount);
	     FlatDiscount=builder.build();
	     
	     builder=new FormFieldBuilder("Net Payable",netPayable);
	     NetPayable=builder.build();
	     
	     builder=new FormFieldBuilder("Advance",advance);
	     builder.build();
	     
	     builder=new FormFieldBuilder("Balance",balance);
	     Balance=builder.build();
	
	
	ff=new FormField[][]
	{
		{CreditAmt,ChequeNo,TotalAmt},
		{CashAmt,ChequeDate,PercentageDiscount},
		{ChequeAmt,BankName,FlatDiscount},
		{BranchName,Balance,NetPayable}
    };
	
	form = new FlexForm(ff,FormStyle.ROWFORM);
	panel=new FlowPanel();
	form.setWidth("100%");
	panel.add(form);
		    
   }
	
	public void applyStyle()
	{
		 panel.setWidth("98%");
	}
	
	
	@Override
	public void onValueChange(ValueChangeEvent<Double> event)
	{
		setAmount();
	}
	
	
	public void setAmount()
	{
		double cashAmt,chequeAmt,cardAmt;
		cashAmt=chequeAmt=cardAmt=0;
		
		if(this.cashamt.getValue()!=null)
			cashAmt=this.cashamt.getValue();
		
		if(this.credidamt.getValue()!=null)
			cardAmt=credidamt.getValue();
		
		if(this.cheqamt.getValue()!=null)
			chequeAmt=this.cheqamt.getValue();
		
		double paidAmt=cashAmt+chequeAmt+cardAmt;
         double flatDiscount=getFlatDiscount();
		
		double percentDiscount=getPercentageDiscount();
		if(totalAmt.getValue()!=null)
		{
		    int val=(int) (this.totalAmt.getValue()-(flatDiscount+percentDiscount));
		    double netVal=val;
			netPayable.setValue(netVal);
		}
		if(netPayable.getValue()!=null)
		{
			balance.setValue(netPayable.getValue()-paidAmt);
		}
		/*System.out.println("Per:::"+percentDiscount);
		
		advance.setValue(paidAmt);
	
		
		System.out.println("NetPayble::"+netPayable.getValue());
		System.out.println("Advance:::"+advance.getValue()+"********************");
		System.out.println("PPPPPPPPPPPPPPP"+paidAmt);*/
	}
	
	
	
	private double getFlatDiscount()
	{
		if(this.percentageDiscount.getValue()==null)
			return 0;
		else
		{
			double valperdisc=(this.percentageDiscount.getValue()/100)*totalAmt.getValue();
			return valperdisc;
		}
	}
	
	public double getPercentageDiscount()
	{
		if(this.flatdiscount.getValue()==null)
			return 0;
		else
			return flatdiscount.getValue();
	}
	
	
	public boolean validate()
	{

		boolean result=true;
		int row=this.ff.length;
		for(int k=0;k<row;k++)
		{
			int column=ff[k].length;
			for(int i=0;i<column;i++)
			{
				if(ff[k][i].isMandatory())
				{
					Widget widg=ff[k][i].getWidget();
					if(widg instanceof TextBox)
					{
						TextBox tb=(TextBox) widg;
						String value=tb.getText();
						if(value.equals(""))
						{
							result=false;
							applyBackgroundColor(widg,"PINK");
							
						}
					}
					
					if(widg instanceof DoubleBox)
					{
						DoubleBox tb=(DoubleBox) widg;
						String value=tb.getText();
						if(value.equals(""))
						{
							result=false;
							applyBackgroundColor(widg,"PINK");
						}
					}
				}
			}
		}
					
		return result;
	}
	
	public void applyBackgroundColor(Widget widg,String color)
	 {
		 widg.getElement().getStyle().setBackgroundColor(color);
	 }
	
	
	 public void clearBackgroundColor(Widget widg)
	 {
		 widg.getElement().getStyle().clearBackgroundColor();
	 }
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	/**
	 * Clears the composite.
	 */
	public void clear()
	{
		int row=this.ff.length;
		for(int k=0;k<row;k++)
		{
			int colCount =ff[k].length ;
			for(int j=0;j<colCount;j++)
			{
				Widget widg= ff[k][j].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setText("");
					clearBackgroundColor(widg);
				}
				
				if(widg instanceof DoubleBox)
				{
					DoubleBox mb=(DoubleBox) widg;
					mb.setText("");
					clearBackgroundColor(widg);
				}
				
			}
		}
	}
	
	public void check(FlexForm form)
	{
		for(int i=0;i<form.getRowCount();i++)
		{
			int  colCount=form.getCellCount(i);
			for(int j=0;j<colCount;j++)
			{
				
				if(form.getWidget(i, j )instanceof InlineLabel)
					form.getWidget(i, j).setVisible(false);
				
			}
		}
	}

	public void setTotalAmt(DoubleBox totalAmt) {
		this.totalAmt = totalAmt;
	}
	public DoubleBox getNetPayable() {
		return netPayable;
	}
	public void setNetPayable(DoubleBox netPayable) {
		this.netPayable = netPayable;
	}
	public DoubleBox getFlatdiscount() {
		return flatdiscount;
	}
	public void setFlatdiscount(DoubleBox flatdiscount) {
		this.flatdiscount = flatdiscount;
	}
	public DoubleBox getAdvance() {
		return advance;
	}
	public void setAdvance(DoubleBox advance) {
		this.advance = advance;
	}
	public DoubleBox getBalance() {
		return balance;
	}
	public void setBalance(DoubleBox balance) {
		this.balance = balance;
	}
	public TextBox getChequenumber() {
		return chequenumber;
	}
	public void setChequenumber(TextBox chequenumber) {
		this.chequenumber = chequenumber;
	}
	public TextBox getBankname() {
		return bankname;
	}
	public void setBankname(TextBox bankname) {
		this.bankname = bankname;
	}
	public TextBox getBankbranch() {
		return bankbranch;
	}
	public void setBankbranch(TextBox bankbranch) {
		this.bankbranch = bankbranch;
	}
	public TextBox getCardNo() {
		return cardNo;
	}
	public void setCardNo(TextBox cardNo) {
		this.cardNo = cardNo;
	}
	public DoubleBox getCheqamt() {
		return cheqamt;
	}
	public void setCheqamt(DoubleBox cheqamt) {
		this.cheqamt = cheqamt;
	}
	public DoubleBox getCashamt() {
		return cashamt;
	}
	public void setCashamt(DoubleBox cashamt) {
		this.cashamt = cashamt;
	}
	public DoubleBox getCredidamt() {
		return credidamt;
	}
	public void setCredidamt(DoubleBox credidamt) {
		this.credidamt = credidamt;
	}
	public DateBox getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(DateBox chequeDate) {
		this.chequeDate = chequeDate;
	}
	public FlexForm getForm() {
		return form;
	}
	public void setForm(FlexForm form) {
		this.form = form;
	}
	public FormField[][] getFf() {
		return ff;
	}
	public void setFf(FormField[][] ff) {
		this.ff = ff;
	}
	public FlowPanel getPanel() {
		return panel;
	}
	public void setPanel(FlowPanel panel) {
		this.panel = panel;
	}
	
	
	
	public void setPercentageDiscount(DoubleBox percentageDiscount) {
		this.percentageDiscount = percentageDiscount;
	}
	
	public DoubleBox getPerDiscount()
	{
		return this.percentageDiscount;
	}
	public DoubleBox getTotalAmt() {
		// TODO Auto-generated method stub
		return this.totalAmt;
	}
	
	
	
	@Override
	public void setEnable(boolean status) {
		{
			
			form.setEnabled(status);
			this.totalAmt.setEnabled(false);
			this.netPayable.setEnabled(false);
			advance.setEnabled(false);
			balance.setEnabled(false);
			
		}
		
		
	}


}
