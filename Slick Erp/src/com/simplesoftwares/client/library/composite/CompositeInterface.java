package com.simplesoftwares.client.library.composite;

// TODO: Auto-generated Javadoc
/**
 * The Interface CompositeInterface provides composite the common nitin flow widget  functionality clear,setEnable
 * validate
 */
public interface CompositeInterface {
	
	/**
	 * Sets the enable on widgets.
	 *
	 * @param status- if true enables the widgets , if false disables them
	 */
	public void setEnable(boolean status);
	
	/**
	 * Clear.clears the widget from any content
	 */
	public void clear();
	
	/**
	 * Validates.
	 *
	 * @return true, if passes validation else returns false
	 */
	public boolean validate();

}
