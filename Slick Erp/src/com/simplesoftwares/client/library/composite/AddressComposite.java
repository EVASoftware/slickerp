
package com.simplesoftwares.client.library.composite;

import java.util.ArrayList;
import java.util.Vector;

import org.apache.poi.hssf.record.DBCellRecord;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.popups.FetchLocalityPopUp;
import com.slicktechnologies.client.views.popups.LocalitySerachPopup;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;

/**
 * The Class AddressComposite. Repersents an Adress entity.
 * To Do : On an urgent basis please sets the value to Return Gae type adress.
 */
public  class AddressComposite extends Composite implements HasValue<Address>,CompositeInterface,
	Editor<AddressComposite>,ChangeHandler ,ValueChangeHandler<Long>, ClickHandler
{
	/** The adressline1. */
	public TextBox  adressline1;
	
	/** The adressline2. */
	public TextBox  adressline2;
	
	/** The locality. */
	public ObjectListBox<Locality>  locality;
	
	/** The land mark. */
	public TextBox  landMark;
	
	/** The country. */
	public ObjectListBox<Country>  country;
	
	/** The state. */
	public ObjectListBox<State>  state;
	
	/** The city. */
	public ObjectListBox<City>  city;
	
	/** The pin. */
	public MyLongBox pin;
	
	/** The content. */
	public FlowPanel content;
	
	/** The form. */
	public FlexForm form;
	
	/** The fields. */
	public FormField[][]fields;
	
	/** The builder. */
	public FormFieldBuilder builder;
	
	/** The fpin. */
	public FormField faddr1,faddr2,flocality,flandmark,fcity,fstate,fcountry,fpin,fnoteForAddress,fbtnlocalityandSearch;
	
	boolean nonManadatory;
	
	public Label noteForAddress=new Label("Please do not use special characters like ( comma ,     ) while entering address");
	
	
	/** Date 16-04-2018 By Vijay
	 *  load locality on basis of city 
	 */
	Button btnFetchLocality;
	FetchLocalityPopUp fetchlocalityPopup = new FetchLocalityPopUp();
	final GWTCAlert alert = new GWTCAlert();
	GenricServiceAsync genAsync = GWT.create(GenricService.class);
	Button btnSearch;
	HorizontalPanel horizontalpanel;
	LocalitySerachPopup localitySearchPopup = new LocalitySerachPopup();
	/**
	 * ends here
	 */
	
	/**
	 * Instantiates a new address composite.
	 */
	TextBox dbLatitude;
	TextBox dbLongitude;
	
	public AddressComposite()
	{
		
		nonManadatory=false;
		createGui();
		initWidget(content);
		country.addChangeHandler(this);
		state.addChangeHandler(this);
		city.addChangeHandler(this);
		
		/**
		 * Date : 02-08-2017 By ANIL
		 */
		locality.addChangeHandler(this);
		/**
		 * End
		 */
		
		/**
		 * date 28 feb 2017 
		 * by vijay
		 * for validation on pin code. pin number not greater than 6 numbers
		 * and for addressline1 addresslin2 and landmark do not use "," validation
		 */
		pin.addValueChangeHandler(this);
		adressline1.addChangeHandler(this);
		adressline2.addChangeHandler(this);
		landMark.addChangeHandler(this);
		
		/**
		 * ends here
		 */
		
	}
	
	
	//******************rohan added this for removing mandetory of address in fumigation ******
 	
		public AddressComposite(boolean mode)
		{
			
			nonManadatory=mode;
			createGui();
			initWidget(content);
			country.addChangeHandler(this);
			state.addChangeHandler(this);
			city.addChangeHandler(this);
			/**
			 * Date : 02-08-2017 By ANIL
			 */
			locality.addChangeHandler(this);
			/**
			 * End
			 */
			
			/**
			 * date 28 feb 2017 
			 * by vijay
			 * for validation on pin code. pin number not greater than 6 numbers
			 * and for addressline1 addresslin2 and landmark do not use "," validation
			 */
			pin.addValueChangeHandler(this);
			adressline1.addChangeHandler(this);
			adressline2.addChangeHandler(this);
			landMark.addChangeHandler(this);
			
			/**
			 * ends here
			 */
			
		}
	
	/**
	 * Initializes the variables necessary for creation of widgets.Change handlers are applied on mandatory widgets.
	 */
	private void initializewidgets()
	{
		adressline1=new TextBox();
		//adressline1.addChangeHandler(this);
		adressline2=new TextBox();
		locality=new ObjectListBox<Locality>();
		Locality.MakeObjectListBoxLive(locality);
		landMark=new TextBox();
		state=new ObjectListBox<State>();
		State.makeOjbectListBoxLive(state);
		country=new ObjectListBox<Country>();
		city=new ObjectListBox<City>();
		City.makeOjbectListBoxLive(city);
		Country.makeOjbectListBoxLive(country);
		pin=new MyLongBox();
		
		/**
		 *  Date 16-04-2018 By vijay for load locality on basis of city 
		 *  Client :- Orion
		*/
		btnFetchLocality = new Button("Fetch Locality");
		btnFetchLocality.addClickHandler(this);
		fetchlocalityPopup.getLblCancel().addClickHandler(this);
		fetchlocalityPopup.getLblOk().addClickHandler(this);
		btnSearch = new Button("Search");
		btnSearch.addClickHandler(this);
		
		horizontalpanel = new HorizontalPanel();
		horizontalpanel.add(btnFetchLocality);
		horizontalpanel.add(btnSearch);
		
		btnSearch.getElement().setId("addresscompositeButton");
		btnFetchLocality.getElement().setId("addresscompositeButton");
		
		localitySearchPopup.getLblCancel().addClickHandler(this);
		localitySearchPopup.getLblOk().addClickHandler(this);

		locality.addClickHandler(this);
		city.addClickHandler(this);
		
		/** 
		 * ends here
		 */
		dbLatitude = new TextBox();
		dbLongitude = new TextBox();
			
	}
	
	/**
	 * Creates  the Gui through FlexForm Method
	 */
	public void createGui()
	{
		initializewidgets();
		content= new FlowPanel();
		
		if(nonManadatory==true){
			
			builder=new FormFieldBuilder("Address Line 1",adressline1);
			faddr1=builder.setMandatory(true).setMandatoryMsg("Address Line 1 is mandatory!").build();
			builder=new FormFieldBuilder("Address Line 2",adressline2);
			faddr2=builder.build();
			builder=new FormFieldBuilder("Locality",locality);
			flocality=builder.build();
			builder=new FormFieldBuilder("Landmark",landMark);
			flandmark=builder.build();
			
			
			
			
			//Date: 11-01-2022 By: Ashwini
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "PC_CITY_NOT_MANDATORY")){
				builder=  new FormFieldBuilder("City",city);
				fcity=builder.setMandatory(false).build();	
				Console.log("in if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(Customer,PC_CITY_NOT_MANDATORY))");
			}
			else{
				builder=  new FormFieldBuilder("* City",city);
				fcity=builder.setMandatory(true).setMandatoryMsg("City is mandatory!").build();
				Console.log("in else of if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(Customer,PC_CITY_NOT_MANDATORY))");
			}
				
			builder=new FormFieldBuilder("State",state);
			fstate=builder.setMandatory(true).setMandatoryMsg("State is mandatory!").build();
			builder=new FormFieldBuilder("Country",country);
			fcountry=builder.setMandatory(true).setMandatoryMsg("Country is mandatory!").build();
			builder=	new FormFieldBuilder("Pin",pin);
			fpin=builder.setMandatory(true).setMandatoryMsg("Pin is mandatory!").build();
			
			builder=	new FormFieldBuilder("",noteForAddress);
			fnoteForAddress=builder.setMandatory(false).setColSpan(3).build();
			
			/**
			 * Date 16-04-2018 By vijay
			 */
			builder = new FormFieldBuilder("",horizontalpanel); //horizontalpanel btnSearch
			fbtnlocalityandSearch = builder.setMandatory(false).build();
			/**
			 * ends here
			 */
		}
		else
		{
			builder=new FormFieldBuilder("* Address Line 1",adressline1);
			faddr1=builder.setMandatory(true).setMandatoryMsg("Address Line 1 is mandatory!").build();
			builder=new FormFieldBuilder("  Address Line 2",adressline2);
			faddr2=builder.build();
			builder=new FormFieldBuilder("Locality",locality);
			flocality=builder.build();
			builder=new FormFieldBuilder("  Landmark",landMark);
			flandmark=builder.build();
			
			
			//Date: 11-01-2022 By: Ashwini
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "PC_CITY_NOT_MANDATORY")){
				builder=  new FormFieldBuilder("City",city);
				fcity=builder.setMandatory(false).build();	
				Console.log("in if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(Customer,PC_CITY_NOT_MANDATORY))");
			}
			else{
				builder=  new FormFieldBuilder("* City",city);
				fcity=builder.setMandatory(true).setMandatoryMsg("City is mandatory!").build();
				Console.log("in else of if(AppUtility.checkForProcessConfigurartionIsActiveOrNot(Customer,PC_CITY_NOT_MANDATORY))");
			}
			builder=new FormFieldBuilder("* State",state);
			fstate=builder.setMandatory(true).setMandatoryMsg("State is mandatory!").build();
			builder=new FormFieldBuilder("* Country",country);
			fcountry=builder.setMandatory(true).setMandatoryMsg("Country is mandatory!").build();
		
			/**
			 * rohan make this pin as non mandatory required by most of clients
			 * date  : 16/12/2016
			 * Nitin sir told me to that 
			 */
			
			builder=	new FormFieldBuilder("Pin",pin);
			fpin=builder.setMandatory(false).setMandatoryMsg("Pin is mandatory!").build();
			
			builder=	new FormFieldBuilder("",noteForAddress);
			fnoteForAddress=builder.setMandatory(false).setColSpan(3).build();
			
			/**
			 * Date 16-04-2018 By vijay
			 */
			builder = new FormFieldBuilder("",horizontalpanel); //horizontalpanel btnSearch
			fbtnlocalityandSearch = builder.setMandatory(false).build();
			/**
			 * ends here
			 */
		}
		/** date 05-06-2019 added by komal to shaow latitude and longitude **/
		builder = new FormFieldBuilder("Latitude",dbLatitude); 
		FormField fdbLatitude = builder.setMandatory(false).build();
		
		builder = new FormFieldBuilder("Longitude",dbLongitude); 
		FormField fdbLongitude = builder.setMandatory(false).build();
		/**
		 *  Date 16-04-2018 By vijay for load locality on basis of city buttons visible only if process config enable
		 *  and old code added in else block
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
		
			fields=new FormField[][]{
					{fnoteForAddress},
					{faddr1,faddr2,flandmark},
					{flocality,fcity,fstate},
					{fcountry,fpin,fbtnlocalityandSearch},
					{fdbLatitude , fdbLongitude}
			};
		}else{
			fields=new FormField[][]{
//					{fnoteForAddress},
//					{faddr1,faddr2,flandmark},
//					{fcountry,fstate,fcity},
//					{flocality,fpin}
					{fnoteForAddress},
					{faddr1,faddr2,flandmark},
					{flocality,fcity,fstate},
					{fcountry,fpin},
					{fdbLatitude , fdbLongitude}
			};
		}
		
		form=new FlexForm(fields,FormStyle.ROWFORM);
		
		applyStyle();
		content.add(form);
	}
	
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Address> handler) {
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#getValue()
	 */
	/**
	 *Returns the composite value which is {@link Address} type object.
	 */
	@Override
	public Address getValue() 
	{
		Address entity= new Address();
			entity.setAddrLine1(this.adressline1.getText().trim());
			entity.setAddrLine2(this.adressline2.getText().trim());
			if(locality.getValue()!=null){
				entity.setLocality(locality.getValue());
			}
			entity.setLandmark(this.landMark.getText().trim());
			if(state.getValue()!=null){
			  entity.setState(state.getValue());
			}
			if(city.getValue()!=null){
			   entity.setCity(city.getValue());
			}
			if(country.getValue()!=null){
				entity.setCountry(country.getValue());
			}
			if(pin.getValue()!=null){
			  entity.setPin(pin.getValue());
			}
			if(dbLatitude.getValue() != null){
				entity.setLatitude(dbLatitude.getValue());
			}
			if(dbLongitude.getValue() != null){
				entity.setLongitude(dbLongitude.getValue());
			}
		
		return entity;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
	 */
	
	/**
	 * Sets the composite with the address entity.
	 */
	@Override
	public void setValue(Address value) 
	{
		adressline1.setText(value.getAddrLine1().trim());
		if(value.getAddrLine2()!=null)
			adressline2.setText(value.getAddrLine2().trim());
		if(value.getLocality()!=null)
			locality.setValue(value.getLocality());
		if(value.getLandmark()!=null)
			landMark.setText(value.getLandmark().trim());
		if(value.getCity()!=null)
			city.setValue(value.getCity());
		if(value.getState()!=null)
			state.setValue(value.getState());
		if(value.getCountry()!=null){
			//   rohan added this code for matching country and 
			//  new code
			
			for (int i = 0; i < country.getItemCount(); i++) {
				if(value.getCountry().trim().equalsIgnoreCase(country.getValue(i).trim())){
					country.setValue(country.getValue(i).trim());
				}
			}
			// old code
//			country.setValue(value.getCountry());
			
		}
		if(value.getPin()!=0)
		   pin.setValue(value.getPin());
		if(value.getLatitude() != null && !value.getLatitude().equals("")){
			dbLatitude.setValue(value.getLatitude());
		}
		if(value.getLongitude() != null && !value.getLongitude().equals("")){
			dbLongitude.setValue(value.getLongitude());
		}
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object, boolean)
	 */
	@Override
	public void setValue(Address value, boolean fireEvents) 
	{
			
	}
	
	/**
	 * Sets the form width.
	 */
	public void applyStyle()
	{
		form.setWidth("98%");
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
	 */
	/**
	 * Validates the manditory fields.
	 */
	@Override
	public boolean validate()
	{
		boolean result=true;
		
		/**
		 * Date : 03-08-2017 By ANIL
		 * added comma validation on save 
		 * Commented on 04-08-2017 By ANil
		 **/
//		boolean addflag1=false;
//		boolean addflag2=false;
//		boolean addflag3=false;
//		if(adressline1.getValue()!=null){
//			if(adressline1.getValue().contains(",")){
//				addflag1=true;
//			}
//		}
//		if(adressline2.getValue()!=null){
//			if(adressline2.getValue().contains(",")){
//				addflag2=true;
//			}
//		}
//		if(landMark.getValue()!=null){
//			if(landMark.getValue().contains(",")){
//				addflag3=true;
//			}
//		}
//		
//		String msg="";
//		if(addflag1&&addflag2&&addflag3){
//			msg="Do not use comma in address line 1 ,address line 2 and landmark.";
//		}else if(!addflag1&&addflag2&&addflag3){
//			msg="Do not use comma in address line 2 and landmark.";
//		}else if(addflag1&&!addflag2&&addflag3){
//			msg="Do not use comma in address line 1 and landmark.";
//		}else if(addflag1&&!addflag2&&!addflag3){
//			msg="Do not use comma in address line 1";
//		}else if(!addflag1&&addflag2&&!addflag3){
//			msg="Do not use comma in address line 2";
//		}else if(!addflag1&&!addflag2&&addflag3){
//			msg="Do not use comma in landmark.";
//		}else if(addflag1&&addflag2&&!addflag3){
//			msg="Do not use comma in address line 1 and address line 2.";
//		}
//		
//		if(!msg.equals("")){
//			GWTCAlert alert = new GWTCAlert();
//			alert.alert(msg);
//			return false;
//		}
		/**
		 * ENd
		 */
		
		if(nonManadatory==true)
			return true;
		int row=this.fields.length;
		
		for(int k=0;k<row;k++)
		{
			int column=fields[k].length;
			for(int i=0;i<column;i++)
			{
				if(fields[k][i].isMandatory())
				{
					Widget widget=fields[k][i].getWidget();
					if(widget instanceof TextBox)
					{
						TextBox tb=(TextBox) widget;
						String value=tb.getText();
						if(value.equals(""))
						{
							result=false;
							applyBorderColor(widget,"#dd4b39");
							
							InlineLabel l=fields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					if(widget instanceof MyLongBox)
					{
						MyLongBox tb=(MyLongBox) widget;
						String value=tb.getText();
						if(value.equals(""))
						{
							result=false;
							applyBorderColor(widget,"#dd4b39");
							InlineLabel l=fields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					if(widget instanceof ListBox)
					{
						ListBox tb=(ListBox) widget;
						if(tb.getSelectedIndex()==0)
						{
							result=false;
							applyBorderColor(widget,"#dd4b39");
							InlineLabel l=fields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
						
					}
				}
			}
		}
					
		return result;
}
	
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	/**
	 * Clears the composite.
	 */
	@Override
	public void clear()
	{
		int row=this.fields.length;
		for(int k=0;k<row;k++)
		{
			int colCount =fields[k].length ;
			for(int j=0;j<colCount;j++)
			{
				Widget widg= fields[k][j].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setText("");

					resetBorderColor(widg);
					if(fields[k][j].getMandatoryMsgWidget()!=null)
						fields[k][j].getMandatoryMsgWidget().setVisible(false);
				

				}
				
				if(widg instanceof MyLongBox)
				{
					MyLongBox mb=(MyLongBox) widg;
					mb.setText("");

					resetBorderColor(widg);
					if(fields[k][j].getMandatoryMsgWidget()!=null)
					{
						fields[k][j].getMandatoryMsgWidget().setVisible(false);
					    
					}

					resetBorderColor(widg);

				}
				
				if(widg instanceof ListBox)
				{
					ListBox lb=(ListBox) widg;
					lb.setSelectedIndex(0);
					resetBorderColor(widg);
					if(fields[k][j].getMandatoryMsgWidget()!=null)
						fields[k][j].getMandatoryMsgWidget().setVisible(false);


				}
				
			}
			
			
		}
	}
				
 /**
  * Applies background color.Called when validation get failed
  *
  * @param widg the widg
  * @param color the color
  */
 public void applyBorderColor(Widget widg,String color)
 {
	 widg.getElement().getStyle().setBorderColor(color);
	 
 }
 
	

@Override
public void setEnable(boolean status)
{
	int row=this.fields.length;
	for(int k=0;k<row;k++)
	{
		int colCount =fields[k].length ;
		for(int j=0;j<colCount;j++)
		{
			Widget widg= fields[k][j].getWidget();
			if(widg instanceof TextBox)
			{
				TextBox vb=(TextBox) widg;
				vb.setEnabled(status);
			}
			
			if(widg instanceof MyLongBox)
			{
				MyLongBox mb=(MyLongBox) widg;
				mb.setEnabled(status);
			}
			
			if(widg instanceof ListBox)
			{
				ListBox lb=(ListBox) widg;
				lb.setEnabled(status);
			}
			/**
			 * Dare 19-04-2018 by vijay for loading locality button 
			 */
			if(widg instanceof HorizontalPanel){
				HorizontalPanel lb=(HorizontalPanel) widg;
				lb.setVisible(status);
			}
			/**
			 * ends here
			 */
		}
	}
}
///////////////////////////////////////To Be expanded in near future////////////////////////////////////////////////////
/**
 * Method sets the hard coded values in city ,country,state list boxes.
 * To Do On an urgent basis integrate dynamic behavior such as in any common web application.
 */

public void setList(String value,ListBox lb)
{
	for(int i=0;i<lb.getItemCount();i++)
	{
		//System.out.println("INSIDE ADRESS LIST BOX "+value.trim().equals(lb.getItemText(lb.getSelectedIndex())));
		if(value.trim().equals(lb.getItemText(i)))
		{
			lb.setSelectedIndex(i);
		}
	}
}

private void resetBorderColor(Widget widg)
{
	 widg.getElement().getStyle().clearBorderColor();
}

public TextBox getAdressline1() {
	return adressline1;
}

public void setAdressline1(TextBox adressline1) {
	this.adressline1 = adressline1;
}

public TextBox getAdressline2() {
	return adressline2;
}

public void setAdressline2(TextBox adressline2) {
	this.adressline2 = adressline2;
}

public ObjectListBox<Locality> getLocality() {
	return locality;
}

public void setLocality(ObjectListBox<Locality> locality) {
	this.locality = locality;
}

public TextBox getLandMark() {
	return landMark;
}

public void setLandMark(TextBox landMark) {
	this.landMark = landMark;
}

public ObjectListBox<Country> getCountry() {
	return country;
}

public void setCountry(ObjectListBox<Country> country) {
	this.country = country;
}

public ObjectListBox<State> getState() {
	return state;
}

public void setState(ObjectListBox<State> state) {
	this.state = state;
}

public ObjectListBox<City> getCity() {
	return city;
}

public void setCity(ObjectListBox<City> city) {
	this.city = city;
}

public MyLongBox getPin() {
	return pin;
}

public void setPin(MyLongBox pin) {
	this.pin = pin;
}

public FlowPanel getContent() {
	return content;
}

public void setContent(FlowPanel content) {
	this.content = content;
}

public FlexForm getForm() {
	return form;
}

public void setForm(FlexForm form) {
	this.form = form;
}

public FormField[][] getFields() {
	return fields;
}

public void setFields(FormField[][] fields) {
	this.fields = fields;
}

public FormFieldBuilder getBuilder() {
	return builder;
}

public void setBuilder(FormFieldBuilder builder) {
	this.builder = builder;
}

public FormField getFaddr1() {
	return faddr1;
}

public void setFaddr1(FormField faddr1) {
	this.faddr1 = faddr1;
}

public FormField getFaddr2() {
	return faddr2;
}

public void setFaddr2(FormField faddr2) {
	this.faddr2 = faddr2;
}

public FormField getFlocality() {
	return flocality;
}

public void setFlocality(FormField flocality) {
	this.flocality = flocality;
}

public FormField getFlandmark() {
	return flandmark;
}

public void setFlandmark(FormField flandmark) {
	this.flandmark = flandmark;
}

public FormField getFcity() {
	return fcity;
}

public void setFcity(FormField fcity) {
	this.fcity = fcity;
}

public FormField getFstate() {
	return fstate;
}

public void setFstate(FormField fstate) {
	this.fstate = fstate;
}

public FormField getFcountry() {
	return fcountry;
}

public void setFcountry(FormField fcountry) {
	this.fcountry = fcountry;
}

public FormField getFpin() {
	return fpin;
}

public void setFpin(FormField fpin) {
	this.fpin = fpin;
}

public boolean isNonManadatory() {
	return nonManadatory;
}

public void setNonManadatory(boolean nonManadatory) {
	this.nonManadatory = nonManadatory;
	if(nonManadatory==true)
	{
		faddr1.getHeaderLabel().setText("Address Line 1 ");
		flocality.getHeaderLabel().setText("Locality");
		fcity.getHeaderLabel().setText("City");
		fpin.getHeaderLabel().setText("Pin");
		fcountry.getHeaderLabel().setText("Country");
		fstate.getHeaderLabel().setText("State");
	}
}


private void reactOnValidChangeCountry()
{
	if(this.country.getValue()!=null)
	{
		String country=this.country.getValue();
		ArrayList<State>lisCity=new ArrayList<State>();
//		GenricServiceAsync async=GWT.create(GenricService.class);
//		MyQuerry querry=new MyQuerry();
//		querry.setQuerryObject(new State());
//		
//		Filter countryFilter=new Filter();
//		countryFilter.setQuerryString("country");
//		countryFilter.setStringValue(country);
//		
//		Filter statusFilter=new Filter();
//		statusFilter.setQuerryString("status");
//		statusFilter.setBooleanvalue(true);
//		
//		querry.getFilters().add(statusFilter);
//		querry.getFilters().add(countryFilter);
//		
//		
//		
//		
//		final GWTCGlassPanel pane=new GWTCGlassPanel();
//		pane.show();
//		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				// TODO Auto-generated method stub
//				pane.hide();
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				removeAllItemsFromListBox(state);
//				removeAllItemsFromListBox(city);
//				removeAllItemsFromListBox(locality);
//				if(result!=null)
//				{
//					ArrayList<State>lisCity=new ArrayList<State>();
//					for(SuperModel temp:result)
//					{
//						State loc=(State) temp;
//						lisCity.add(loc);
//					}
//					
//					Comparator<State> stateNameComparator = new Comparator<State>() {
//						public int compare(State c1, State c2) {
//						String stateName1=c1.getStateName();
//						String stateName2=c2.getStateName();
//						
//						return stateName1.compareTo(stateName2);
//						}
//					};
//					Collections.sort(lisCity,stateNameComparator);
//					
//					
//					state.setListItems(lisCity);
//					
//					
//					pane.hide();
//					
//				}
//				
//			}
//		});
		
		
		
		
		//*******************rohan added this code because of drop down issue in country state city and locality
		
		for(int i=0;i<LoginPresenter.globalState.size();i++)
		{
			if((country.trim().equalsIgnoreCase(LoginPresenter.globalState.get(i).getCountry().trim()) && (LoginPresenter.globalState.get(i).getStatus()==true )))
			{
				State state = new State();
				state.setStateName(LoginPresenter.globalState.get(i).getStateName());
				lisCity.add(state);
			}
		}
		
		state.setListItems(lisCity);
	}
}



private void reactOnValidChangeState()
{
	if(this.state.getValue()!=null)
	{
		String country=this.state.getValue();
		ArrayList<City>lisCity=new ArrayList<City>();
//		GenricServiceAsync async=GWT.create(GenricService.class);
//		MyQuerry querry=new MyQuerry();
//		querry.setQuerryObject(new City());
//		
//		Filter countryFilter=new Filter();
//		countryFilter.setQuerryString("stateName");
//		countryFilter.setStringValue(country);
//		
//		Filter statusFilter=new Filter();
//		statusFilter.setQuerryString("status");
//		statusFilter.setBooleanvalue(true);
//		
//		querry.getFilters().add(statusFilter);
//		querry.getFilters().add(countryFilter);
//		
//		
//		
//		
//		final GWTCGlassPanel pane=new GWTCGlassPanel();
//		pane.show();
//		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//        
//			@Override
//			public void onFailure(Throwable caught) {
//				pane.hide();
//				
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				removeAllItemsFromListBox(locality);
//				removeAllItemsFromListBox(city);
//				if(result!=null)
//				{
//					ArrayList<City>lisCity=new ArrayList<City>();
//					for(SuperModel temp:result)
//					{
//						City loc=(City) temp;
//						lisCity.add(loc);
//						
//					}
//					
//					
//					Comparator<City> cityArrayComparator = new Comparator<City>() {
//						public int compare(City c1, City c2) {
//						String cityName1=c1.getCityName();
//						String cityName2=c2.getCityName();
//						
//						return cityName1.compareTo(cityName2);
//						}
//					};
//					Collections.sort(lisCity,cityArrayComparator);
//					
//					city.setListItems(lisCity);
//					
//				}
//				pane.hide();
//				
//				
//			}
//		});
		
		
		
		//*******************rohan added this code because of drop down issue in country state city and locality
		
				for(int i=0;i<LoginPresenter.globalCity.size();i++)
				{
					if((country.trim().equalsIgnoreCase(LoginPresenter.globalCity.get(i).getStateName().trim())))
					{
						City city = new City();
						city.setCityName(LoginPresenter.globalCity.get(i).getCityName());
						lisCity.add(city);
					}
				}
				
				city.setListItems(lisCity);
		
	}
}




private void reactOnValidChangeCity()
{
	if(this.city.getValue()!=null)
	{
		String country=this.city.getValue();
		ArrayList<Locality>lisLocality=new ArrayList<Locality>();
//		GenricServiceAsync async=GWT.create(GenricService.class);
//		MyQuerry querry=new MyQuerry();
//		querry.setQuerryObject(new Locality());
//		
//		Filter countryFilter=new Filter();
//		countryFilter.setQuerryString("cityName");
//		countryFilter.setStringValue(country);
//		
//		Filter statusFilter=new Filter();
//		statusFilter.setQuerryString("status");
//		statusFilter.setBooleanvalue(true);
//		
//		querry.getFilters().add(statusFilter);
//		querry.getFilters().add(countryFilter);
//		
//		
//		final GWTCGlassPanel pane=new GWTCGlassPanel();
//		pane.show();
//		
//		
//		async.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//			pane.hide();
//				
//			}
//
//			@Override
//			public void onSuccess(ArrayList<SuperModel> result) {
//				removeAllItemsFromListBox(locality);
//				if(result!=null)
//				{
//					ArrayList<Locality>lisLocality=new ArrayList<Locality>();
//					for(SuperModel temp:result)
//					{
//						Locality loc=(Locality) temp;
//						lisLocality.add(loc);
//						
//					}
//					
//
//					Comparator<Locality> localityArrayComparator = new Comparator<Locality>() {
//						public int compare(Locality c1, Locality c2) {
//						String localityName1=c1.getLocality();
//						String localityName2=c2.getLocality();
//						
//						return localityName1.compareTo(localityName2);
//						}
//					};
//					Collections.sort(lisLocality,localityArrayComparator);
//					
//					locality.setListItems(lisLocality);
//					
//				}
//				
//				pane.hide();
//			}
//			
//		});
		
		
		
		//*******************rohan added this code because of drop down issue in country state city and locality
		
		for(int i=0;i<LoginPresenter.globalLocality.size();i++)
		{
			if((country.trim().equalsIgnoreCase(LoginPresenter.globalLocality.get(i).getCityName().trim())))
			{
				Locality locality = new Locality();
				locality.setLocality(LoginPresenter.globalLocality.get(i).getLocality());
				lisLocality.add(locality);
			}
		}
		
		locality.setListItems(lisLocality);
		
	}
}


/**
 * Removes the all items from list box.
 *
 * @param lb the lb
 */
private void removeAllItemsFromListBox(ListBox lb)
{
	for(int i=lb.getItemCount()-1;i>=1;i--){
		lb.removeItem(i);
	}
}






@Override
public void onChange(ChangeEvent event) {
	if(event.getSource()==country)
	{
		if(country.getValue()==null)
		{
			/**
			 * Old Code
			 */
//			removeAllItemsFromListBox(city);
//			removeAllItemsFromListBox(state);
//			removeAllItemsFromListBox(locality);
			
			reinitializeAdressDropDown("Country");
		}
		else
		{
			/**
			 * Old Code
			 */
//			reactOnValidChangeCountry();
//			System.out.println("Size Of State "+state.getItemCount());
			reactOnAddressChange("Country");
		}
	}
	
	if(event.getSource()==state)
	{
		if(state.getValue()==null)
		{
			/**
			 * Old Code
			 */
//			removeAllItemsFromListBox(city);
//			removeAllItemsFromListBox(locality);
			
			reinitializeAdressDropDown("State");
		}
		else
		{
			/**
			 * Old Code
			 */
//			reactOnValidChangeState();
			reactOnAddressChange("State");
		}
	}
	
	if(event.getSource()==city)
	{
		System.out.println("inside city"+city.getValue());
		if(city.getValue()==null)
		{
			/**
			 * Old Code
			 */
//			removeAllItemsFromListBox(locality);
			System.out.println("if block");
			locality.setSelectedIndex(0);
			reinitializeAdressDropDown("City");
		}
		else
		{
			/**
			 * Old Code
			 */
//			reactOnValidChangeCity();
			System.out.println("else block");
			reactOnAddressChange("City");
		}
	}
	
	if(event.getSource()==locality)
	{
		if(locality.getValue()==null)
		{
			reinitializeAdressDropDown("Locality");
		}
		else
		{
			reactOnAddressChange("Locality");
		}
	}
	
	
	/**
	 * Date 28 Feb 2017
	 * added by vijay
	 * for validation while entering address do not use comma in address
	 */
	if(event.getSource()==adressline1){
		System.out.println("onchange of address line 1");
		if(adressline1.getValue()!=null){
			if(adressline1.getValue().contains(",")){
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Do not use comma in address");
				
				/**
				 * Date:03-08-2017 BY ANIL
				 * as per client requirement if address contains comma, we will give only warning(Rohan-Pestomatic)
				 */
//				adressline1.setValue("");
			}
		}
	}
	if(event.getSource()==adressline2){
		System.out.println("onchange of address line 2");
		if(adressline2.getValue()!=null){
			if(adressline2.getValue().contains(",")){
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Do not use comma in address");
				/**
				 * Date:03-08-2017 BY ANIL
				 * as per client requirement if address contains comma, we will give only warning(Rohan-Pestomatic)
				 */
//				adressline2.setValue("");
			}
		}
	}
	if(event.getSource()==landMark){
		System.out.println("onchange of landmark");
		if(landMark.getValue()!=null){
			if(landMark.getValue().contains(",")){
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Do not use comma in address");
				/**
				 * Date:03-08-2017 BY ANIL
				 * as per client requirement if address contains comma, we will give only warning(Rohan-Pestomatic)
				 */
//				landMark.setValue("");
			}
		}
	}
	
	/**
	 * end here
	 */
}


/**
 * Date 28 feb 2017
 * added by vijay 
 * below method for validation for pin code
 * pin code can not be greater than 6 numbers.
 */

	@Override
	public void onValueChange(ValueChangeEvent<Long> event) {
		// TODO Auto-generated method stub
		if (event.getSource() == pin) {
			int pinlength = pin.getValue().toString().length();
			System.out.println("Pin lenth ==" + pinlength);
			if (pinlength > 6) {
				GWTCAlert alert = new GWTCAlert();
				alert.alert("Pin code can not be greater than 6 numbers !");
				pin.setValue(null);
			}
		}
	}

	/**
	 * End here
	 */


	/**
	 * Date : 02-08-107 By ANIL
	 * This method is used to update respective address drop down
	 */
	
	private void reactOnAddressChange(String addressField){
		ArrayList<Locality>localityList=new ArrayList<Locality>();
		ArrayList<City>cityList=new ArrayList<City>();
		ArrayList<State>stateList=new ArrayList<State>();
		ArrayList<Country>countryList=new ArrayList<Country>();
		
		ArrayList<String>strCity=new ArrayList<String>();
		ArrayList<String>strState=new ArrayList<String>();
		ArrayList<String>strCountry=new ArrayList<String>();
		
		Long strpin=0l;
		
		switch(addressField){
		
		case "Locality":
			
			String locality=this.locality.getValue();
			for(Locality obj:LoginPresenter.globalLocality){
				if((locality.trim().equalsIgnoreCase(obj.getLocality().trim()))){
					strCity.add(obj.getCityName());
					
					/*26-09-2017
					 * sagar sore
					*/
					strpin=obj.getPinCode();
					//end
				}
			}
			for(String city:strCity){
				for(City obj:LoginPresenter.globalCity){
					if((city.trim().equalsIgnoreCase(obj.getCityName().trim()))){
						strState.add(obj.getStateName());
						cityList.add(obj);
					}
				}
			}
			for(String state:strState){
				for(State obj:LoginPresenter.globalState){
					if((state.trim().equalsIgnoreCase(obj.getStateName().trim()))){
						strCountry.add(obj.getCountry());
						stateList.add(obj);
					}
				}
			}
			for(String country:strCountry){
				for(Country obj:LoginPresenter.globalCountry){
					if((country.trim().equalsIgnoreCase(obj.getCountryName().trim()))){
						countryList.add(obj);
					}
				}
			}
			city.setListItems(cityList);
			state.setListItems(stateList);
			country.setListItems(countryList);
			
			/**
			 * 26-09-2017
			 * sagar sore
			 */	
				pin.setValue(strpin);
			//end	
				
			if(strCity.size()==1){
				city.setValue(strCity.get(0));
				if(strState.size()==1){
					state.setValue(strState.get(0));
					if(strCountry.size()==1){
						country.setValue(strCountry.get(0));
					}
				}
			}
			
			break;
			
		case "City":
			
			String city=this.city.getValue();
			/**
			 * @author Anil @since 23-04-2021
			 * Commented if condition because of this code locality list was not getting updated
			 */
//			if(this.locality.getSelectedIndex()==0){
				for(Locality obj:LoginPresenter.globalLocality){
					if(city.trim().equalsIgnoreCase(obj.getCityName().trim())){
						localityList.add(obj);
					}
				}
				this.locality.setListItems(localityList);
//			}
			for(City obj:LoginPresenter.globalCity){
				if((city.trim().equalsIgnoreCase(obj.getCityName().trim()))){
					strState.add(obj.getStateName());
					cityList.add(obj);
				}
			}
			for(String state:strState){
				for(State obj:LoginPresenter.globalState){
					if((state.trim().equalsIgnoreCase(obj.getStateName().trim()))){
						strCountry.add(obj.getCountry());
						stateList.add(obj);
					}
				}
			}
			for(String country:strCountry){
				for(Country obj:LoginPresenter.globalCountry){
					if((country.trim().equalsIgnoreCase(obj.getCountryName().trim()))){
						countryList.add(obj);
					}
				}
			}
			state.setListItems(stateList);
			country.setListItems(countryList);
			
			if(strState.size()==1){
				state.setValue(strState.get(0));
				if(strCountry.size()==1){
					country.setValue(strCountry.get(0));
				}
			}
			/**
			 * @author Anil @since 23-04-2021
			 */
			if(localityList.size()==1){
				this.locality.setValue(localityList.get(0).getLocality());
				strpin=localityList.get(0).getPinCode();
			}
			break;
		case "State":
			String state=this.state.getValue();
			for(State obj:LoginPresenter.globalState){
				if((state.trim().equalsIgnoreCase(obj.getStateName().trim()))){
					strCountry.add(obj.getCountry());
					stateList.add(obj);
				}
			}
			for(String country:strCountry){
				for(Country obj:LoginPresenter.globalCountry){
					if((country.trim().equalsIgnoreCase(obj.getCountryName().trim()))){
						countryList.add(obj);
					}
				}
			}
			country.setListItems(countryList);
			
			if(strCountry.size()==1){
				country.setValue(strCountry.get(0));
			}
			break;
		
		}
		pin.setValue(strpin);
	}
	
	/**
	 * Date :02-08-2017 BY ANIL
	 * Reinitialize address drop down list
	 */
	public void reinitializeAdressDropDown(String addressField){
		
		ArrayList<Locality> locList=new ArrayList<Locality>();
		ArrayList<City> cityList=new ArrayList<City>();
		ArrayList<State> stateList=new ArrayList<State>();
		ArrayList<Country> countryList=new ArrayList<Country>();
		
		ArrayList<String>strCity=new ArrayList<String>();
		ArrayList<String>strState=new ArrayList<String>();
		ArrayList<String>strCountry=new ArrayList<String>();
		
		switch(addressField){
		
		case "Locality":
			locality.setListItems(LoginPresenter.globalLocality);
			city.setListItems(LoginPresenter.globalCity);
			state.setListItems(LoginPresenter.globalState);
			country.setListItems(LoginPresenter.globalCountry);
			/**
			 * 26-09-2017
			 * sagar sore
			 */	
				pin.setValue(0l);
			//end	
			break;
		case "City":
			/**
			 * @author Vijay Date :- 24-11-2022
			 * Des :-Bug city state not loading properly so added or condition to load all
			 */
			if(locality.getItems().size()==0 || locality.getValue()==null || (city.getItems().size()==0 || city.getValue()==null)){
				locality.setListItems(LoginPresenter.globalLocality);
				city.setListItems(LoginPresenter.globalCity);
				state.setListItems(LoginPresenter.globalState);
				country.setListItems(LoginPresenter.globalCountry);
				pin.setValue(0l);
				return;
			}
			String locNm=locality.getValue();
			
			for(Locality obj:locality.getItems()){
				if(locNm!=null&&locNm.trim().equalsIgnoreCase(obj.getLocality().trim())){
					strCity.add(obj.getCityName());
					locList.add(obj);
				}
			}
			for(String city:strCity){
				for(City obj:LoginPresenter.globalCity){
					if((city.trim().equalsIgnoreCase(obj.getCityName().trim()))){
						strState.add(obj.getStateName());
						cityList.add(obj);
					}
				}
			}
			for(String state:strState){
				for(State obj:LoginPresenter.globalState){
					if((state.trim().equalsIgnoreCase(obj.getStateName().trim()))){
						strCountry.add(obj.getCountry());
						stateList.add(obj);
					}
				}
			}
			for(String country:strCountry){
				for(Country obj:LoginPresenter.globalCountry){
					if((country.trim().equalsIgnoreCase(obj.getCountryName().trim()))){
						countryList.add(obj);
					}
				}
			}
			locality.setListItems(locList);
			city.setListItems(cityList);
			state.setListItems(stateList);
			country.setListItems(countryList);
			pin.setValue(0l);
			break;
		case "State":
			if(city.getItems().size()==0 || city.getValue()==null || (state.getItems().size()==0 || state.getValue()==null)){
				locality.setListItems(LoginPresenter.globalLocality);
				city.setListItems(LoginPresenter.globalCity);
				state.setListItems(LoginPresenter.globalState);
				country.setListItems(LoginPresenter.globalCountry);
				pin.setValue(0l);
				return;
			}
			String cityNm=city.getValue();
			for(City obj:city.getItems()){
				if(cityNm.trim().equalsIgnoreCase(cityNm.trim())){
					strState.add(obj.getStateName());
				}
			}
			for(String state:strState){
				for(State obj:LoginPresenter.globalState){
					if((state.trim().equalsIgnoreCase(obj.getStateName().trim()))){
						strCountry.add(obj.getCountry());
						stateList.add(obj);
					}
				}
			}
			for(String country:strCountry){
				for(Country obj:LoginPresenter.globalCountry){
					if((country.trim().equalsIgnoreCase(obj.getCountryName().trim()))){
						countryList.add(obj);
					}
				}
			}
			state.setListItems(stateList);
			country.setListItems(countryList);
			
			break;
		case "Country":
			if(city.getItems().size()==0 || city.getValue()==null || (country.getItems().size()==0 || country.getValue()==null)){
				locality.setListItems(LoginPresenter.globalLocality);
				city.setListItems(LoginPresenter.globalCity);
				state.setListItems(LoginPresenter.globalState);
				country.setListItems(LoginPresenter.globalCountry);
				pin.setValue(0l);
				return;
			}
			break;
		}
		
	}


	/**
	 * Date 16-04-2018 by Vijay for loading locality on basis of city popup click handler
	 */
		
		@Override
		public void onClick(ClickEvent event) {
			/**
			 * Date 17-04-2018 By vijay 
			 * des :- if below process config is active then we load locality on basis of city for Billing address
			 * this not reflect to Service address composite so here i am refreshing city and locality from loaded data
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
				if(event.getSource() == locality){
					if(LoginPresenter.globalLocality.size()>locality.getItems().size()){
							locality.setListItems(LoginPresenter.globalLocality);
					}
				}
			}	
			if(event.getSource() == btnFetchLocality){
				fetchlocalityPopup.getCity().getCityName().setValue("");
				fetchlocalityPopup.showPopUp();
			}
			if(event.getSource()==btnSearch){
				if(LoginPresenter.globalLocality.size()!= localitySearchPopup.locality.localityNameInfo.size()){
					localitySearchPopup.locality.initializeLocality();
				}
				localitySearchPopup.getLblOk().setText("Select");
				localitySearchPopup.getLocality().getLocalityName().setValue("");
				localitySearchPopup.showPopUp();
			}
			if(event.getSource() instanceof InlineLabel){
				InlineLabel lblText = (InlineLabel) event.getSource();
				
				if(lblText.getText().equals("OK")){
					if(!fetchlocalityPopup.getCity().getCityName().getValue().equals("")){
						boolean cityExistFlag = true;
						for(int i=0;i<LoginPresenter.globalLocality.size();i++){
							if(LoginPresenter.globalLocality.get(i).getCityName().equals(fetchlocalityPopup.getCity().getCityName().getValue())){
								cityExistFlag=false;
								break;
							}
						}
						if(LoginPresenter.globalLocality.size()==0){
							cityExistFlag=true;
						}
						if(cityExistFlag){
							loadLocality(fetchlocalityPopup.getCity().getCityName().getValue());
						}else{
							GWTCAlert alert = new GWTCAlert();
							alert.alert("City Already Loaded!");
							fetchlocalityPopup.hidePopUp();
						}
					}else{
						GWTCAlert alert = new GWTCAlert();
						alert.alert("Please enter city Name");
					}
				}	
				if(lblText.getText().equals("Cancel")){
					fetchlocalityPopup.hidePopUp();
					localitySearchPopup.hidePopUp();
				}
				
				if(lblText.getText().equals("Select")){
					this.locality.setValue(localitySearchPopup.locality.getLocalityName().getValue());
					reactOnAddressChange("Locality");
					localitySearchPopup.hidePopUp();
				}
			}
			
			
			
			/**
			 * ends here
			 */

			
		}


		private void loadLocality(final String cityName) {
			
			MyQuerry querry = new MyQuerry();
			Vector<Filter> filterVec = new Vector<Filter>();
			
			Filter filter = null;
			
			filter = new Filter();
			filter.setStringValue(cityName.trim());
			filter.setQuerryString("cityName");
			filterVec.add(filter);
			
			querry.setFilters(filterVec);
			querry.setQuerryObject(new Locality());
			
			genAsync.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
				
				@Override
				public void onSuccess(ArrayList<SuperModel> result) {
					// TODO Auto-generated method stub
					
					if(result.size()==0){
						alert.alert("No Locality Found with this city Name!");
					}
					for(int i=0;i<result.size();i++){
						SuperModel model = result.get(i);
						Locality locality = (Locality) model;
						LoginPresenter.globalLocality.add(locality);
					}
					if(result.size()!=0){
						String strlocality = locality.getValue();
						locality.setListItems(LoginPresenter.globalLocality);
						if(strlocality!=null && !strlocality.equals("--SELECT--"))
						locality.setValue(strlocality);
					}
					
					fetchlocalityPopup.hidePopUp();
				}
				
				
				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					alert.alert("Unexpected error occurred please try again !");
					fetchlocalityPopup.hidePopUp();
				}
			});
		}
		
	
}
