package com.simplesoftwares.client.library.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.Widget;
import com.itextpdf.text.log.SysoCounter;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.simplesoftwares.client.library.libservice.PersonInfoService;
import com.simplesoftwares.client.library.libservice.PersonInfoServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.reusabledata.ConditionDialogBox;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contactpersonidentification.contactpersondetails.ContactPersonForm;
import com.slicktechnologies.client.views.customer.CustomerPresenter;
import com.slicktechnologies.client.views.generalpopup.GeneralViewDocumentPopup;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.personlayer.Person;
import com.slicktechnologies.shared.common.role.AuthorizationHelper;
import com.slicktechnologies.shared.common.role.ScreenAuthorization;
import com.slicktechnologies.shared.common.role.UserRole;


// TODO: Auto-generated Javadoc
/**
 * The Class UploadComposite.Instance Comes with three {@link SuggestBox} id,count, cellNumber
 * corresponding to any {@link Person} object.
 * IF id is selected corresponding Name and cell phone number is get auto filled.Simmilar functionality 
 * for othe two suggest Box.
 * Underlying return type of composite is {@link PersonInfo}
 */
public class PersonInfoComposite extends  Composite implements HasValue<PersonInfo>,
SelectionHandler<Suggestion>,Editor<PersonInfo>,CompositeInterface, ClickHandler
{
    private SuggestBox id;
    private SuggestBox name;
    private SuggestBox phone;
    private TextBox tbpocName=new TextBox();
    private FormField customerId,customerName,customerCell,pocName;
    private boolean isValidate;
    public static ArrayList<PersonInfo> globalCustomerArray=new ArrayList<PersonInfo>();
    public static ArrayList<PersonInfo> globalVendorArray=new ArrayList<PersonInfo>();
    
    
    
    /**
     * Gets the querry.
     *
     * @return the querry
     */
    public MyQuerry getQuerry() {
		return querry;
	}

	/**
	 * Sets the querry.
	 *
	 * @param querry the new querry
	 */
	public void setQuerry(MyQuerry querry) {
		this.querry = querry;
	}

	
	private PersonInfo centity;
    
    /** The id to person info Map */
    private   HashMap<Integer,PersonInfo>idToPersonInfo = new HashMap<Integer,PersonInfo>();
	
	/** The cell to person info Map */
	private   HashMap<Long,PersonInfo>cellToPersonInfo = new HashMap<Long,PersonInfo>();
	
	/** The name to person info Map */
	private   HashMap<String,PersonInfo>nameToPersonInfo = new HashMap<String,PersonInfo>();
	
	/** The poc name */
	private   HashMap<String,PersonInfo>pocNameToPersonInfo = new HashMap<String,PersonInfo>();
	
	/** The service. */
	final PersonInfoServiceAsync service=GWT.create(PersonInfoService.class);
	
	/** The querry. */
	MyQuerry querry;
	
    /** The panel. */
    private FlowPanel panel;
    
    /** The ff. */
    private FormField[][]formfield;
    private boolean mandatory;
	
    
    /**
     * Date : 12-06-2017 BY ANIL
     * added customer approval flag
     * if customer approval process is active and custAppFlag is true then we load only active customer in composite
     * else load all customer in composite
     * 
     */
    boolean custAppFlag;
    
    /**
     * End
     */
    
    /**
     * Date : 12-06-2017 BY ANIL
     */
    ArrayList<PersonInfo> customerArray=new ArrayList<PersonInfo>();
    /**
     * End
     */
    
    /**
	 * Date 02-11-2018 by Vijay
	 * Des:- To load large customer Data with process config 
	 * by Using Recursive. 
	 */
    int customerCount = 0;
    /**
     * ends here
     */
    
    /**
     * @author Anil @since 10-08-2021
     * Adding horizontal panel for showing four shortcut button i.e New, Edit, Customer Location, Customer Contact
     * as per the user authorization
     */
    public HorizontalPanel hrButtonPanel;
    public Button btnNew;
    public Button btnEdit;
    public Button btnContact;
    public Button btnLocation;
    public Button btnClr;
    FormField buttonPanel;
    
    public GeneralViewDocumentPopup genPopup=null;
    Customer customerObj;
    CustomerBranchDetails custBranchObj=null;
    public ContactPersonIdentification contactObj=null;
    
    GWTCAlert alert =new GWTCAlert();
    GenricServiceAsync async=GWT.create(GenricService.class);
    
    ConditionDialogBox conditionalPopup = new ConditionDialogBox("Please select customer type.", AppConstants.YES,AppConstants.NO);
    PopupPanel popUpPanel=new PopupPanel(true);
    /**
	 * Instantiates a new person info composite.
	 * @param querry the querry
	 */
	public PersonInfoComposite(MyQuerry querry) 
	{
		super();
		Console.log("Constructor Called");
		mandatory=true;
		
		
		this.querry=querry;
		/**
		 * Date :12-06-2017 by ANIL
		 */
		custAppFlag=false;
		/**
		 * End
		 */
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		String classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value "+classNameValue);
		
		createForm(classNameValue,true);
		
		if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(classNameValue.trim()))
		{
			initalizeCustomerCompositeData();
		}
		if(AppConstants.PERSONCOMPOSITEVENDOR.equals(classNameValue.trim()))
		{
			initalizeVendorCompositeData();
		}
		
//		initalizePersonInfoHashMaps();
		initWidget(panel);
		isValidate=true;
		
		applyStyle();
	}
	
	public PersonInfoComposite(MyQuerry querry,boolean mode) 
	{
		super();
		Console.log("Booelan Mode Constructor Called");
//		createForm();
		this.querry=querry;
//		initalizePersonInfoHashMaps();
		/**
		 * Date :12-06-2017 by ANIL
		 */
		custAppFlag=false;
		/**
		 * End
		 */
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		String classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value "+classNameValue);
		createForm(classNameValue,mode);
		
		if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(classNameValue.trim()))
		{
			initalizeCustomerCompositeData();
		}
		if(AppConstants.PERSONCOMPOSITEVENDOR.equals(classNameValue.trim()))
		{
			initalizeVendorCompositeData();
		}
		
		initWidget(panel);
		mandatory=false;
		
		isValidate=mode;
		
		
		applyStyle();
	}
	
	/**
	 * 
	 * @param querry
	 * @param mode
	 * @param buttonFlag
	 * @author Anil @since 13-10-2021
	 * Added button flag for showing quick buttons on customer composite 
	 */
	public PersonInfoComposite(MyQuerry querry,boolean mode,boolean buttonFlag) 
	{
		super();
		Console.log("Booelan Mode Constructor Called");
//		createForm();
		this.querry=querry;
//		initalizePersonInfoHashMaps();
		/**
		 * Date :12-06-2017 by ANIL
		 */
		custAppFlag=false;
		/**
		 * End
		 */
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		String classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value "+classNameValue);
		createForm(classNameValue,buttonFlag);
		
		if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(classNameValue.trim()))
		{
			initalizeCustomerCompositeData();
		}
		if(AppConstants.PERSONCOMPOSITEVENDOR.equals(classNameValue.trim()))
		{
			initalizeVendorCompositeData();
		}
		
		initWidget(panel);
		mandatory=false;
		
		isValidate=mode;
		
		
		applyStyle();
	}
	
	
	/**
	 * Date : 12-06-2017 By ANIL
	 * This method is used to initialize customer composite which load all 
	 * customer whether customer approval process is active or not
	 */
	public PersonInfoComposite(MyQuerry querry,boolean mode,boolean custAppFlag, boolean mandatoryFlag) 
	{
		super();
		this.custAppFlag=custAppFlag;
		Console.log("Booelan Mode Constructor Called");
		mandatory = mandatoryFlag;
		
		this.querry=querry;
		
		String[] classType=querry.getQuerryObject().getClass().toString().trim().split("\\s+");
		String strcname=classType[1];
		String classNameArray[]=strcname.split("\\.");
		String classNameValue=classNameArray[classNameArray.length-1];
		System.out.println("Person Composite Querry Class"+classNameValue);
		Console.log("Class Name Value "+classNameValue);
		
		createForm(classNameValue,mode);
		
		if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(classNameValue.trim()))
		{
			initalizeCustomerCompositeData();
		}
		if(AppConstants.PERSONCOMPOSITEVENDOR.equals(classNameValue.trim()))
		{
			initalizeVendorCompositeData();
		}
		
		initWidget(panel);
		isValidate=mode;
//		mandatory=false;
		
		applyStyle();
	}
	
	public PersonInfoComposite() {
		
	}

	/**
	 * Initializewidgets.
	 */
	private void initializewidgets()
	{
		id=new SuggestBox();
		id.addSelectionHandler(this);
		
		name=new SuggestBox();
		name.addSelectionHandler(this);
		
		phone=new SuggestBox();
		phone.addSelectionHandler(this);	
		
		id.getValueBox().addKeyPressHandler(new NumbersOnly());
		phone.getValueBox().addKeyPressHandler(new NumbersOnly());
		
		tbpocName.setEnabled(false);
		
		intializeButtonPanel();
		
	}

	/**
	 * Creates the UI
	 * @param buttonPanelFlag 
	 * @param compositeName 
	 */
	public void createForm(String compositeName, boolean buttonPanelFlag)
	{
		initializewidgets();
		FormFieldBuilder builder;
	    
	    System.out.println("Mandatory is "+this.mandatory);
	    if(mandatory==true)
	    {
	      builder= new FormFieldBuilder("* Id",id);
	      customerId=builder.setMandatory(true).setMandatoryMsg("These fields are mandatory!").build();
	      builder=new FormFieldBuilder("* Name",name);
		    customerName=builder.setMandatory(true).build();
		    builder=new FormFieldBuilder("* Cell Number",phone);
		    customerCell=builder.setMandatory(true).build();
		    builder=new FormFieldBuilder("POC Name",tbpocName);
		    pocName=builder.setMandatory(false).build();
		    
		    builder=new FormFieldBuilder("",hrButtonPanel);
		    buttonPanel=builder.setMandatory(false).build();
		    
	    }
	    else
	    {
	    	builder= new FormFieldBuilder("Id",id);
	    	customerId=builder.setMandatory(false).setMandatoryMsg("These fields are mandatory!").build();
		    builder=new FormFieldBuilder("Name ",name);
			customerName=builder.setMandatory(false).build();
			builder=new FormFieldBuilder("Cell Number",phone);
			customerCell=builder.setMandatory(false).build();
			builder=new FormFieldBuilder("POC Name",tbpocName);
			pocName=builder.setMandatory(false).build();	
			
			builder=new FormFieldBuilder("",hrButtonPanel);
		    buttonPanel=builder.setMandatory(false).build();
	    	
	    }
	   
	    /**
		 * @author Anil @since 10-08-2021
		 * Moved name first then cell no and then id 
		 * requirement raised by Nitin Sir for simplification of composite
		 */
		
		if(compositeName.equals(AppConstants.PERSONCOMPOSITECUSTOMER)){
			if(!buttonPanelFlag){
				formfield=new FormField[][]{
					{customerName,customerCell,customerId,pocName }
				};
			}else{
				formfield=new FormField[][]{
					{ buttonPanel,customerName,customerCell,customerId,pocName }
				};
			}
		}else{
			formfield=new FormField[][]{
				{ customerId,customerName,customerCell,pocName }
			};
		}
		
		
		FlexForm form = new FlexForm(formfield,FormStyle.ROWFORM);
		panel=new FlowPanel();
		form.setWidth("98%");
		panel.add(form);
		
		
		Console.log("Customer FormField Process");
	  }
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(ValueChangeHandler<PersonInfo> handler) 
	{
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#getValue()
	 */
	@Override
	public PersonInfo getValue() 
	{
	    PersonInfo entity = null;
	    int id=-1;
	    if((this.id.getText().trim().equals(""))==false)
	       id=Integer.parseInt(this.getId().getText().trim());
	    
	    entity=idToPersonInfo.get(id);
	    return entity;
	}
	
	
	public int getIdValue()
	{
		if(this.id!=null)
		{
			String count=id.getText().trim();
			try{
			Integer intCount=Integer.parseInt(count);
			return intCount;
			}
			catch(Exception e)
			{
				return -1;
			}
			
			
		}
		return -1;
	}
	
	
	public Long getCellValue()
	{
		if(this.phone!=null)
		{
			String number=phone.getText().trim();
			try{
			Long longPhoneCount=Long.parseLong(number);
			return longPhoneCount;
			}
			catch(Exception e)
			{
				return -1L;
			}
			
			
		}
		return -1L;
	}
	
	
	public String getFullNameValue()
	{
		if(this.name!=null)
		{
			String name=this.name.getText().trim();
			return name;
		}
		return "";
			
	}
	
	public Vector<Filter> getQuerryFilters()
	{
		
		  Filter temp;
		  Vector<Filter>filtervec=new Vector<Filter>();
		  if(getIdValue()!=-1)
		  {
		  temp=new Filter();
		  temp.setIntValue(getIdValue());
		  temp.setQuerryString("personInfo.count");
		  filtervec.add(temp);
		  }
		  
		  if(!(getFullNameValue().equals("")))
		  {
		  temp=new Filter();
		  temp.setStringValue(getFullNameValue());
		  temp.setQuerryString("personInfo.fullName");
		  filtervec.add(temp);
		  }
		  if(getCellValue()!=-1l)
		  {
		  temp=new Filter();
		  temp.setLongValue(getCellValue());
		  temp.setQuerryString("personInfo.cellNumber");
		  filtervec.add(temp);
		  }
		  
		  return filtervec;
	}
	
	
	
	
	
	

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(PersonInfo value) 
	{
		id.setText(value.getCount()+"");
		name.setValue(value.getFullName());
		phone.setText(value.getCellNumber()+"");
		tbpocName.setText(value.getPocName());
		
		centity=value;
	}

	//Understand composites and use this power
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object, boolean)
	 */
	@Override
	public void setValue(PersonInfo value, boolean fireEvents) {
		
		
	}

	//Understand composites and use this power
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.SelectionHandler#onSelection(com.google.gwt.event.
	 * logical.shared.SelectionEvent)
	 */
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) 
	{	
		SuggestBox sb=(SuggestBox) event.getSource();
		String selectedItem=sb.getText().trim();
		resetBorderColor(id);
		resetBorderColor(name);
		resetBorderColor(phone);
		
		Console.log("On Selection");
		
		if(sb==this.id)
		{
			int count=Integer.parseInt(selectedItem);
			centity=idToPersonInfo.get(count);
			
			if(centity==null)
				sb.setText("");
			else
			{	
				name.setText(centity.getFullName());
				phone.setText(centity.getCellNumber()+"");
				tbpocName.setText(centity.getPocName());
			}
		}
		
		if(sb==this.phone)
		{
			Long  phone=Long.parseLong(selectedItem);
			centity=cellToPersonInfo.get(phone);
			
			if(centity==null)
				sb.setText("");
			
			else
			{
				name.setText(centity.getFullName());
				id.setText(centity.getCount()+"");
				tbpocName.setText(centity.getPocName());
			}
		}
		
		if(sb==this.name)
		{
			String[]xyz=selectedItem.split("-");
			
			if(xyz.length==0)
			{
				sb.setText("");
				return;	
			}
			
			else
			{
				centity=nameToPersonInfo.get(selectedItem);
				
				if(centity==null)
					sb.setText("");
				
				else
				{
					phone.setText(centity.getCellNumber()+"");
					id.setText(centity.getCount()+"");
					this.name.setText(centity.getFullName());
					tbpocName.setText(centity.getPocName());
				}
			}
		}
	}
	
	
	/**
	 * Sets the color border.
	 *
	 * @param widget the widget
	 * @param color the color
	 */
	public void setColorBorder(Widget widget,String color)
	{
		widget.getElement().getStyle().setBackgroundColor(color);
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	public void clear()
	{
		int row=this.formfield.length;
		for(int k=0;k<row;k++)
		{
			int column=formfield[k].length;
			for(int i=0;i<column;i++)
			{
				Widget widg=formfield[k][i].getWidget();
				if(widg instanceof SuggestBox)
				{
					SuggestBox sb=(SuggestBox) widg;
					resetBorderColor(sb);
					sb.setText("");
				}
				
				if(widg instanceof TextBox)
				{
					TextBox sb=(TextBox) widg;
					resetBorderColor(sb);
					sb.setText("");
				}
				
				if(formfield[k][i].getMandatoryMsgWidget()!=null)
					formfield[k][i].getMandatoryMsgWidget().setVisible(false);
					
					
				
				
				}
			}
		}					
	
	 
 	/**
 	 * Change background color.
 	 *
 	 * @param widg the widg
 	 * @param color the color
 	 */
 	private void changeBorderColor(Widget widg,String color)
	 {
		 widg.getElement().getStyle().setBorderColor(color);
	 }
	 
	 /**
 	 * Clear background color.
 	 *
 	 * @param widg the widg
 	 */
 	private void resetBorderColor(Widget widg)
	 {
		 widg.getElement().getStyle().clearBorderColor();
		
		 
	 }
		
	 
	 /**
 	 * Sets the enabled.
 	 *
 	 * @param status the new enabled
 	 */
 	public void setEnabled(boolean status)
 	{
		 this.id.setEnabled(status);
		 this.name.setEnabled(status);
		 this.phone.setEnabled(status);
		 this.tbpocName.setEnabled(false);
 	}
	 
	 /**
 	 * Initalize person info hash maps.
 	 */
 	public void initalizePersonInfoHashMaps()
	{	 
 		Console.log("2-Initializing Hash Map Before RPC : globlal array zero");
 		System.out.println("person info composite logged in user "+LoginPresenter.loggedInUser);
 		
 		PersonInfoServiceAsync  personservice=GWT.create(PersonInfoService.class);
		personservice.LoadVinfo(querry,LoginPresenter.loggedInUser, new AsyncCallback<ArrayList<PersonInfo>>() {
			@Override
			public void onFailure(Throwable caught) {
				Console.log("3-In Failure For Composite");
				caught.printStackTrace();
				
			}

			@Override
			public void onSuccess(ArrayList<PersonInfo> result) {
				Console.log("4-On Success Initializing Hash Map "+"Size Of Impl Retrieval"+result.size());
				
				for(PersonInfo pinfo:result)
				{
					/**
					 * Date 10 April 2017
					 * added by vijay
					 * if condition is for if  person Info  types is Vendor and else for customer type
					 * old code added in else block
					 */
				
					if(pinfo.isVendor()){
						globalVendorArray.add(pinfo);
					}else{
						globalCustomerArray.add(pinfo);
					}
					/**
					 * ends here
					 */
				}
				/**
				 * Date : 12-06-2017 BY ANIL
				 */
				if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")&&!custAppFlag){
					Console.log("5-Customer Approval process active other than cust");
					for(PersonInfo pinfo:result)
					{
						if(pinfo.getStatus().equals("Active")){
							customerArray.add(pinfo);
							
							int id=pinfo.getCount();
							String name=pinfo.getFullName();
							Long cellno=pinfo.getCellNumber();
							String pocNameVal=pinfo.getPocName();
							
							idToPersonInfo.put(id, pinfo);
							nameToPersonInfo.put(name, pinfo);
							cellToPersonInfo.put(cellno, pinfo);
							pocNameToPersonInfo.put(pocNameVal, pinfo);
						}
					}
					
					Console.log("CUST APP Hash Map For Id After Loop"+idToPersonInfo.size());
					Console.log("CUST APP Hash Map For Name After Loop"+nameToPersonInfo.size());
					Console.log("CUST APP Hash Map For Cell After Loop"+cellToPersonInfo.size());
					Console.log("CUST APP Hash Map For Poc After Loop"+pocNameToPersonInfo.size());
					
					
					initalizeCellOracle();
					initalizeIdOracle();
					initalizeNameOracle();
					
					return;
				}
				
				/**
				 * End
				 */
				
				
				
				Console.log("Global Customer Array"+globalCustomerArray.size());
				Console.log("Global Customer Approval Array"+customerArray.size());
				Console.log("Global Veendor Array"+globalVendorArray.size());
				
				for(int i=0;i<result.size();i++)
				{
					int id=result.get(i).getCount();
					String name=result.get(i).getFullName();
					Long cellno=result.get(i).getCellNumber();
					String pocNameVal=result.get(i).getPocName();
					
					idToPersonInfo.put(id, result.get(i));
					nameToPersonInfo.put(name, result.get(i));
					cellToPersonInfo.put(cellno, result.get(i));
					pocNameToPersonInfo.put(pocNameVal, result.get(i));
				}
				
				Console.log("Exited For Loop For Filling Hash Maps & Initializing Oracle");
				Console.log("Hash Map For Id After Loop"+idToPersonInfo.size());
				Console.log("Hash Map For Name After Loop"+nameToPersonInfo.size());
				Console.log("Hash Map For Cell After Loop"+cellToPersonInfo.size());
				Console.log("Hash Map For Poc After Loop"+pocNameToPersonInfo.size());
				
				
				initalizeCellOracle();
				initalizeIdOracle();
				initalizeNameOracle();
			}
		});
	 }
 	
 	
 	
 	public void initalizeCustomerCompositeData()
 	{
 		Console.log("1-Initializing Cust Comp");
 		if(globalCustomerArray.size()==0 || LoginPresenter.loadLargeCustomerDataFlag)
 		{
// 			initalizePersonInfoHashMaps();
 			
 			/**
 			 * Date 02-11-2018 By Vijay
 			 * Des :- To load customer large data using recursive with process config 
 			 */
 			if(LoginPresenter.loadLargeCustomerDataFlag){
 				Console.log("here we go");
 				/**
 				 * @author Anil , Date : 17-09-2019
 				 * system slow issue faced by ankita
 				 * if customer is created from any other branch then last customer count is n
 				 * not getting updated which caused infinite recursive call of method
 				 * hence system get slow
 				 */
 				
 				if(globalCustomerArray.size()!=0&&LoginPresenter.customerCount<globalCustomerArray.get(globalCustomerArray.size()-1).getCount()){
 					Console.log("Setting updated customer count if updated from other instance");
 					LoginPresenter.customerCount=globalCustomerArray.get(globalCustomerArray.size()-1).getCount();
 				}
 				
 				Console.log("LoginPresenter.customerCount="+LoginPresenter.customerCount);
 				//Console.log("globalCustomerArraycount="+globalCustomerArray.get(globalCustomerArray.size()-1).getCount());
 				if(globalCustomerArray.size() == 0 || LoginPresenter.customerCount != globalCustomerArray.get(globalCustomerArray.size()-1).getCount()){
					initalizePersonInfoHashMapsPartly();
				}
 			}else{
 				initalizePersonInfoHashMaps();
 			}
 		}
 		if(globalCustomerArray.size()!=0)
 		{
 			retrieveCustomerData();
 		}
 	}
 	
 	public void initalizeVendorCompositeData()
 	{
 		if(globalVendorArray.size()==0)
 		{
 			initalizePersonInfoHashMaps();
 		}
 		if(globalVendorArray.size()!=0)
 		{
 			retrieveVendorData();
 		}
 	}
 	
 	public void retrieveCustomerData()
 	{	 
 		Console.log("22-Initializing Hash Map Before RPC-global array not zero");
 		
 		/**
 		 * @author Vijay Date 23-11-2020
 		 * Des :- if below process config is active then below code should not execute becuase below code to load new customer and below confiruation
 		 * is ussed to load customer with branch level restriction
 		 */
		 
		 //Ashwini Patil Date:26-10-2023 commented this call as on navigating on any screen this method was getting called. which was affecting google billing
// 		if(!AppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", AppConstants.PC_LOADCUSTOMERWITHBRANCHLEVELRESTRICTION)|| 
// 				(LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Admin") ||
// 						LoginPresenter.myUserEntity.getRole().getRoleName().equalsIgnoreCase("Zonal Coordinator"))){
// 			
// 		PersonInfoServiceAsync  personservice=GWT.create(PersonInfoService.class);
// 		Company c=new Company();
// 		System.out.println("Company Id"+c.getCompanyId());
// 		Comparator<PersonInfo> customerArrayComparator = new Comparator<PersonInfo>() {
//			public int compare(PersonInfo c1, PersonInfo c2) {
//			Integer custCount1=c1.getCount();
//			Integer custCount2=c2.getCount();
//			
//			return custCount1.compareTo(custCount2);
//			}
//		};
//		Collections.sort(globalCustomerArray,customerArrayComparator);
//		Console.log("33-After Comparator");
//		int latestCustomerId=globalCustomerArray.get(globalCustomerArray.size()-1).getCount();
// 		Console.log("44-Latest Customer ID"+latestCustomerId);
		
		
//		personservice.LoadPinfo(c.getCompanyId(), latestCustomerId, AppConstants.PERSONCOMPOSITECUSTOMER, new AsyncCallback<ArrayList<PersonInfo>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				Console.log("55-In Failure For Composite In PInfo");
//				caught.printStackTrace();
//			}
//
//			@Override
//			public void onSuccess(ArrayList<PersonInfo> result) {
//				Console.log("66- On Success Initializing Hash Map for PInfo "+" Size Of Impl Retrieval "+result.size());
//				
//				if(result.size()>0)
//				{
//					for(int i=0;i<result.size();i++)
//					{
//						globalCustomerArray.add(result.get(i));
//					}
//					
//					/**
//					 * Date : 12-06-2017 BY ANIL
//					 */
//					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")&&!custAppFlag){
//						Console.log("11-Customer Approval process active other than cust");
//						for(PersonInfo pinfo:globalCustomerArray)
//						{
//							if(pinfo.getStatus().equals("Active")){
//								customerArray.add(pinfo);
//							}
//						}
//						
//						for(PersonInfo pinfo:result)
//						{
//							if(pinfo.getStatus().equals("Active")){
//								customerArray.add(pinfo);
//							}
//						}
//					}
//					/**
//					 * End
//					 */
//					creatingHashMaps(AppConstants.PERSONCOMPOSITECUSTOMER);
//				}
//				if(result.size()==0)
//				{
//					/**
//					 * Date : 12-06-2017 BY ANIL
//					 */
//					if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")&&!custAppFlag){
//						Console.log("111-Customer Approval process active other than cust");
//						for(PersonInfo pinfo:globalCustomerArray)
//						{
//							if(pinfo.getStatus().equals("Active")){
//								customerArray.add(pinfo);
//							}
//						}
//					}
//					creatingHashMaps(AppConstants.PERSONCOMPOSITECUSTOMER);
//				}
//			}
//		});
//		
// 		}
// 		else {
			creatingHashMaps(AppConstants.PERSONCOMPOSITECUSTOMER);
// 		}
		
	 }
 	
 	public void retrieveVendorData()
 	{	 
 		Console.log("Initializing Hash Map Before RPC");
 		
 		PersonInfoServiceAsync  personservice=GWT.create(PersonInfoService.class);
 		Company c=new Company();
 		System.out.println("Company Id"+c.getCompanyId());
 		Comparator<PersonInfo> vendorArrayComparator = new Comparator<PersonInfo>() {
			public int compare(PersonInfo c1, PersonInfo c2) {
			Integer vendorCount1=c1.getCount();
			Integer vendorCount2=c2.getCount();
			
			return vendorCount1.compareTo(vendorCount2);
			}
		};
		Collections.sort(globalVendorArray,vendorArrayComparator);
		Console.log("After Comparator");
		int latestVendorId=globalVendorArray.get(globalVendorArray.size()-1).getCount();
 		Console.log("Latest Vendor ID"+latestVendorId);
		
				 //Ashwini Patil Date:26-10-2023 commented this call as on navigating on any screen this method was getting called. which was affecting google billing
//		personservice.LoadPinfo(c.getCompanyId(), latestVendorId, AppConstants.PERSONCOMPOSITEVENDOR, new AsyncCallback<ArrayList<PersonInfo>>() {
//			@Override
//			public void onFailure(Throwable caught) {
//				Console.log("In Failure For Composite In PInfo");
//				caught.printStackTrace();
//			}
//
//			@Override
//			public void onSuccess(ArrayList<PersonInfo> result) {
//				Console.log("On Success Initializing Hash Map for PInfo");
//				Console.log("Size Of Impl Retrieval"+result.size());
//				
//				if(result.size()>0)
//				{
//					for(int i=0;i<result.size();i++)
//					{
//						globalVendorArray.add(result.get(i));
//					}
//					creatingHashMaps(AppConstants.PERSONCOMPOSITEVENDOR);
//				}
//				if(result.size()==0)
//				{
//					creatingHashMaps(AppConstants.PERSONCOMPOSITEVENDOR);
//				}
//			}
//		});
 		creatingHashMaps(AppConstants.PERSONCOMPOSITEVENDOR);
 	}
 	
 	public void creatingHashMaps(String type)
 	{
 		
 		if(AppConstants.PERSONCOMPOSITECUSTOMER.equals(type.trim()))
 		{
 			/**
			 * Date : 12-06-2017 BY ANIL
			 */
			if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "CustomerApprovalProcess")&&!custAppFlag){
				for(int i=0;i<customerArray.size();i++)
	 			{
	 				int id=customerArray.get(i).getCount();
	 				String name=customerArray.get(i).getFullName();
	 				Long cellno=customerArray.get(i).getCellNumber();
	 				String pocNameVal=customerArray.get(i).getPocName();
	 				
	 				idToPersonInfo.put(id, customerArray.get(i));
	 				nameToPersonInfo.put(name, customerArray.get(i));
	 				cellToPersonInfo.put(cellno, customerArray.get(i));
	 				pocNameToPersonInfo.put(pocNameVal, customerArray.get(i));
	 			}
			}else{
			
	 			for(int i=0;i<globalCustomerArray.size();i++)
	 			{
	 				int id=globalCustomerArray.get(i).getCount();
	 				String name=globalCustomerArray.get(i).getFullName();
	 				Long cellno=globalCustomerArray.get(i).getCellNumber();
	 				String pocNameVal=globalCustomerArray.get(i).getPocName();
	 				
	 				idToPersonInfo.put(id, globalCustomerArray.get(i));
	 				nameToPersonInfo.put(name, globalCustomerArray.get(i));
	 				cellToPersonInfo.put(cellno, globalCustomerArray.get(i));
	 				pocNameToPersonInfo.put(pocNameVal, globalCustomerArray.get(i));
	 			}
			}
 			
 			
 			/**
			 * End
			 */
 		}
 		
 		if(AppConstants.PERSONCOMPOSITEVENDOR.equals(type.trim()))
 		{
 			for(int i=0;i<globalVendorArray.size();i++)
 			{
 				int id=globalVendorArray.get(i).getCount();
 				String name=globalVendorArray.get(i).getFullName();
 				Long cellno=globalVendorArray.get(i).getCellNumber();
 				String pocNameVal=globalVendorArray.get(i).getPocName();
 				
 				idToPersonInfo.put(id, globalVendorArray.get(i));
 				nameToPersonInfo.put(name, globalVendorArray.get(i));
 				cellToPersonInfo.put(cellno, globalVendorArray.get(i));
 				pocNameToPersonInfo.put(pocNameVal, globalVendorArray.get(i));
 			}
 		}
 		
 		
		
		Console.log("Exited For Loop For Filling Hash Maps & Initializing Oracle");
		Console.log("Hash Map For Id After Loop"+idToPersonInfo.size());
		Console.log("Hash Map For Name After Loop"+nameToPersonInfo.size());
		Console.log("Hash Map For Cell After Loop"+cellToPersonInfo.size());
		Console.log("Hash Map For Poc After Loop"+pocNameToPersonInfo.size());
		
		
		initalizeCellOracle();
		initalizeIdOracle();
		initalizeNameOracle();
 	}
 	
 	
	 
	 /**
 	 * Apply style.
 	 */
 	public void applyStyle()
 	{
		 panel.setWidth("98%");
 	}
	 
	 /**
 	 * Initalize name oracle.
 	 */
 	private void initalizeNameOracle()
 	{
 		Console.log("In Name Oracle Entered");
		 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.name.getSuggestOracle();
		 Console.log("Size For Name Oracle Using KeySet"+this.nameToPersonInfo.keySet().size());
		 for(String name:nameToPersonInfo.keySet())
		   orcl.add(name);		 
		 
	 }
	 
	 /**
 	 * Initalize cell oracle.
 	 */
 	private void initalizeCellOracle()
 	{
 		Console.log("In Cell Oracle Entered");
		 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.phone.getSuggestOracle();
		 Console.log("Size For Phone Oracle Using KeySet"+this.cellToPersonInfo.keySet().size());
		 for(Long cell:this.cellToPersonInfo.keySet())
		 {
			 
			 orcl.add(cell+"");
		 }
 	}
	 
	 /**
 	 * Initalize id oracle.
 	 */
 	private void initalizeIdOracle()
 	{
 		Console.log("In Id Oracle Entered");
		 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.id.getSuggestOracle();
		 Console.log("Size For Id Oracle Using KeySet"+this.idToPersonInfo.keySet().size());
		 for(int id:this.idToPersonInfo.keySet())
		 {
			 orcl.add(id+"");
		 }
 	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public SuggestBox getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(SuggestBox id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public SuggestBox getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(SuggestBox name) {
		this.name = name;
	}

	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 */
	public SuggestBox getPhone() {
		return phone;
	}

	/**
	 * Sets the phone.
	 *
	 * @param phone the new phone
	 */
	public void setPhone(SuggestBox phone) {
		this.phone = phone;
	}

	/**
	 * Gets the centity.
	 *
	 * @return the centity
	 */
	public PersonInfo getCentity() {
		return centity;
	}

	/**
	 * Sets the centity.
	 *
	 * @param centity the new centity
	 */
	public void setCentity(PersonInfo centity) {
		this.centity = centity;
	}

	/**
	 * Gets the id to person info.
	 *
	 * @return the id to person info
	 */
	public HashMap<Integer, PersonInfo> getIdToPersonInfo() {
		return idToPersonInfo;
	}

	/**
	 * Sets the id to person info.
	 *
	 * @param idToPersonInfo the id to person info
	 */
	public void setIdToPersonInfo(HashMap<Integer, PersonInfo> idToPersonInfo) {
		this.idToPersonInfo = idToPersonInfo;
	}

	/**
	 * Gets the cell to person info.
	 *
	 * @return the cell to person info
	 */
	public HashMap<Long, PersonInfo> getCellToPersonInfo() {
		return cellToPersonInfo;
	}

	/**
	 * Sets the cell to person info.
	 *
	 * @param cellToPersonInfo the cell to person info
	 */
	public void setCellToPersonInfo(HashMap<Long, PersonInfo> cellToPersonInfo) {
		this.cellToPersonInfo = cellToPersonInfo;
	}

	/**
	 * Gets the name to person info.
	 *
	 * @return the name to person info
	 */
	public HashMap<String, PersonInfo> getNameToPersonInfo() {
		return nameToPersonInfo;
	}

	/**
	 * Sets the name to person info.
	 *
	 * @param nameToPersonInfo the name to person info
	 */
	public void setNameToPersonInfo(HashMap<String, PersonInfo> nameToPersonInfo) {
		this.nameToPersonInfo = nameToPersonInfo;
	}
	
	public HashMap<String, PersonInfo> getPocNameToPersonInfo() {
		return pocNameToPersonInfo;
	}

	public void setPocNameToPersonInfo(
			HashMap<String, PersonInfo> pocNameToPersonInfo) {
		this.pocNameToPersonInfo = pocNameToPersonInfo;
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public FlowPanel getPanel() {
		return panel;
	}

	/**
	 * Sets the panel.
	 *
	 * @param panel the new panel
	 */
	public void setPanel(FlowPanel panel) {
		this.panel = panel;
	}

	/**
	 * Gets the ff.
	 *
	 * @return the ff
	 */
	public FormField[][] getFf() {
		return formfield;
	}

	/**
	 * Sets the ff.
	 *
	 * @param ff the new ff
	 */
	public void setFf(FormField[][] ff) {
		this.formfield = ff;
	}

	/**
	 * Gets the service.
	 *
	 * @return the service
	 */
	public PersonInfoServiceAsync getService() {
		return service;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean status) 
	{
		this.id.setEnabled(status);
		this.name.setEnabled(status);
		this.phone.setEnabled(status);
		
		/**
		 * @author Anil @since 08-02-2022
		 * Buttons on customer composite should be editable on all state i.e. New,Edit and View
		 */
//		this.btnNew.setEnabled(status);
//		this.btnEdit.setEnabled(status);
//		this.btnLocation.setEnabled(status);
//		this.btnContact.setEnabled(status);
		this.btnNew.setEnabled(true);
		this.btnEdit.setEnabled(true);
		this.btnLocation.setEnabled(true);
		this.btnContact.setEnabled(true);
		
		this.btnClr.setEnabled(status);
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
	 */
	@Override
	public boolean validate() 
	{
		if(isValidate==false)
			return true;
		int count=0;
		boolean result=true;
		//this try-catch is checking if id contains any number format value or not.
		try
		{
			count=Integer.parseInt(this.id.getText().trim());
		}
		catch(Exception e)
		{
			changeBorderColor(this.name,"#dd4b39");
			changeBorderColor(this.phone,"#dd4b39");
			changeBorderColor(this.id,"#dd4b39");
			
			InlineLabel lbl=formfield[0][0].getMandatoryMsgWidget();
			
			if(this.id.getText().trim().equals(""))
				lbl.setVisible(true);
			
			else
			{	
				lbl.setText("Id not in Proper Format!");
				lbl.setVisible(true);
			}
			
			return false;
		}
		
		//it will be null if id value is in number format but not in present in db.
		PersonInfo info=idToPersonInfo.get(count);
		
		if(info==null)
		{
			result=false;
			changeBorderColor(this.name,"#dd4b39");
			changeBorderColor(this.phone,"#dd4b39");
			changeBorderColor(this.id,"#dd4b39");
			
			InlineLabel lbl=formfield[0][0].getMandatoryMsgWidget();
			lbl.setText("Given Id does not match in Database!");
			lbl.setVisible(true);
			
			return result;
		}
		
		//we are here, it means first user put right value in id field.
		
		//now check for if he/she changed either any value after the correct suggestion.
		
		String name=info.getFullName();
		
		String cell=info.getCellNumber()+"";
		
		String ids=info.getCount()+"";
		
		InlineLabel lbl=formfield[0][0].getMandatoryMsgWidget();
		
		if(this.name.getText().trim().equals(name)==false)
		{
			result=false;
			changeBorderColor(this.name,"#dd4b39");
			lbl.setText("Given name does not match in Database!");
			lbl.setVisible(true);
		}
		
		if(this.phone.getText().trim().equals(cell)==false)
		{
			result=false;
			changeBorderColor(this.phone,"#dd4b39");
			
			lbl.setText("Given cell number does not match in Database!");
			lbl.setVisible(true);
		}
		
		if(this.id.getText().trim().equals(ids)==false)
		{
			result=false;
			changeBorderColor(this.id,"#dd4b39");
			
			lbl.setText("Given Id does not match in Database!");
			lbl.setVisible(true);
		}
		
		return result;
	}
	
	public void makeMandatoryMessageFalse()
	{
		
		if(customerId!=null&&customerId.getMandatoryMsgWidget()!=null)
		{
			customerId.getMandatoryMsgWidget().setVisible(false);
		}
		
		if(customerName!=null&&customerName.getMandatoryMsgWidget()!=null)
		{
			customerName.getMandatoryMsgWidget().setVisible(false);
		}
		
		if(customerCell!=null&&customerCell.getMandatoryMsgWidget()!=null)
		{
			customerCell.getMandatoryMsgWidget().setVisible(false);
		}
	}

	public FormField getCustomerId() {
		return customerId;
	}

	public void setCustomerId(FormField customerId) {
		this.customerId = customerId;
	}

	public FormField getCustomerName() {
		return customerName;
	}

	public void setCustomerName(FormField customerName) {
		this.customerName = customerName;
	}

	public FormField getCustomerCell() {
		return customerCell;
	}

	public void setCustomerCell(FormField customerCell) {
		this.customerCell = customerCell;
	}
	
	public FormField getPocName() {
		return pocName;
	}

	public void setPocName(FormField pocName) {
		this.pocName = pocName;
	}

	public TextBox getTbpocName() {
		return tbpocName;
	}

	public void setTbpocName(TextBox tbpocName) {
		this.tbpocName = tbpocName;
	}

	public FormField[][] getFormfield() {
		return formfield;
	}

	public void setFormfield(FormField[][] formfield) {
		this.formfield = formfield;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean isValidate() {
		return isValidate;
	}

	public void setValidate(boolean isValidate) {
		this.isValidate = isValidate;
	}
	
	private static class NumbersOnly implements KeyPressHandler {
	    @Override
	    public void onKeyPress(KeyPressEvent event) 
	    {
	    	if(!(Character.isDigit(event.getCharCode()) ||event.getCharCode()=='.'))
	        {
	    		TextBox v=(TextBox) event.getSource();
	    		v.cancelKey();
	        }
	    }
	    
	    
	}
	

	public void updatingEditedCustomer(Customer customerObject){
		System.out.println("INside Customer Editing ............"+customerObject.getFullname());
		boolean flag =false;
		
		System.out.println("ID HASHMAP SIZE ::: "+idToPersonInfo.size());
		
		for(int i=0;i<globalCustomerArray.size();i++)
		{
			if(customerObject.getCount()==globalCustomerArray.get(i).getCount()){
				System.out.println("Customer Exist in global customer array.....");
				if(customerObject.isCompany()){
					System.out.println("Company.........");
					if(!customerObject.getCompanyName().equals(globalCustomerArray.get(i).getFullName()) || customerObject.getCellNumber1()!=globalCustomerArray.get(i).getCellNumber()){
						System.out.println("Company name Changed.........");
						flag=true;
						globalCustomerArray.get(i).setFullName(customerObject.getCompanyName());
						globalCustomerArray.get(i).setPocName(customerObject.getFullname());
						/** Date 16-01-2018 by Vijay added cell no in global list *****/
						globalCustomerArray.get(i).setCellNumber(customerObject.getCellNumber1());
						break;
					}
				}else{
					if(!customerObject.getFullname().equals(globalCustomerArray.get(i).getFullName()) || customerObject.getCellNumber1()!=globalCustomerArray.get(i).getCellNumber()){
						System.out.println("Customer name Changed.........");
						flag=true;
						globalCustomerArray.get(i).setFullName(customerObject.getFullname());
						globalCustomerArray.get(i).setPocName(customerObject.getFullname());
						/** Date 16-01-2018 by Vijay added cell no in global list *****/
						globalCustomerArray.get(i).setCellNumber(customerObject.getCellNumber1());
						System.out.println("customerObject.getCellNumber1()"+customerObject.getCellNumber1());
						break;
					}
				}
			}
		}
		if(flag==true){
			System.out.println("Updating Customer...............");
//			initializewidgets();
//			creatingHashMaps(AppConstants.PERSONCOMPOSITECUSTOMER);
			
//			CustomerPresenter.initalize();
			
		}
	}
	
	public void setPersonInfoHashMap(PersonInfo person){
		try {
			customerArray.add(person);
			idToPersonInfo.put( person.getCount(),person);
			nameToPersonInfo.put(person.getFullName(), person);
			cellToPersonInfo.put(person.getCellNumber(), person);
			pocNameToPersonInfo.put(person.getPocName(), person);
			initalizeCellOracle();
			initalizeIdOracle();
			initalizeNameOracle();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	
	/**
	 * Date 02-11-2018 by Vijay
	 * Des:- To load large customer Data with process config 
	 * by Using Recursive. 
	 */
	
	private void initalizePersonInfoHashMapsPartly() {
		
		/**
		 * @author Anil ,Date : 28-02-2018
		 * Setting latest customer count 
		 * for loading large no. of customer through process configuration
		 */
		
//		if(globalCustomerArray.size()!=0){
//			customerCount=(int) LoginPresenter.customerCount;
//		}
		/**
		 * End
		 */
 		Console.log("2-Initializing Hash Map Before RPC : globlal array zero =="+customerArray.size() );
 		Console.log("BF CUSTOMER LATEST COUNT : "+customerCount);
 		Console.log("BF STATIC CUSTOMER LATEST COUNT : "+LoginPresenter.customerCount);
 		
 		PersonInfoServiceAsync  personInfoservice=GWT.create(PersonInfoService.class);
 		personInfoservice.LoadCustomerPinfo(querry, customerCount, new AsyncCallback<ArrayList<PersonInfo>>() {
			
			@Override
			public void onSuccess(ArrayList<PersonInfo> result) {


				Console.log("4-On Success Initializing Hash Map "+"Size Of Impl Retrieval"+result.size());
				
				for(PersonInfo pinfo:result)
				{
					/**
					 * Date 10 April 2017
					 * added by vijay
					 * if condition is for if  person Info  types is Vendor and else for customer type
					 * old code added in else block
					 */
				
					if(pinfo.isVendor()){
						globalVendorArray.add(pinfo);
					}else{
						globalCustomerArray.add(pinfo);
					}
					/**
					 * ends here
					 */
				}
				Console.log("Global Customer Array ==== "+globalCustomerArray.size());
				
				for(int i=0;i<result.size();i++)
				{
					int id=result.get(i).getCount();
					String name=result.get(i).getFullName();
					Long cellno=result.get(i).getCellNumber();
					String pocNameVal=result.get(i).getPocName();
					
					idToPersonInfo.put(id, result.get(i));
					nameToPersonInfo.put(name, result.get(i));
					cellToPersonInfo.put(cellno, result.get(i));
					pocNameToPersonInfo.put(pocNameVal, result.get(i));
				}
				
				initalizeCellOracle();
				initalizeIdOracle();
				initalizeNameOracle();
				
				/**
				 * @author Anil ,Date : 28-02-2019
				 * Last customer count was set to the class variable customerCount but condition is checked with static variable customerCount
				 * thus condition is never true which create deadlock ,and size of globalCustomerArray keep on increasing as person composite is initialized from any screen
				 */
				
//				if(globalCustomerArray.size()!=0){
//					customerCount = globalCustomerArray.get(globalCustomerArray.size()-1).getCount();
//				}
				if(globalCustomerArray.size()!=0){
//					LoginPresenter.customerCount = globalCustomerArray.get(globalCustomerArray.size()-1).getCount();
					customerCount = globalCustomerArray.get(globalCustomerArray.size()-1).getCount();
				}
				/**
				 * End
				 */
				
				Console.log("AF CUSTOMER LATEST COUNT : "+customerCount);
		 		Console.log("AF STATIC CUSTOMER LATEST COUNT : "+LoginPresenter.customerCount);
		 		
				/**
				 * @author Ashwini
				 * @since 17-01-2022
				 * One more check if(result.size()==10) is added where 10 refers to the limit value passed 
				 * in the query to ofy in LoadCustomerPinfo method of PersonInfoCompositeImplementor class.
				 * this has resolved issue with the loading screen.
				 */
		 		if(result.size()==5000){
		 			if(LoginPresenter.customerCount != customerCount){
//					if(LoginPresenter.customerCount != globalCustomerArray.get(globalCustomerArray.size()-1).getCount()){
						initalizePersonInfoHashMapsPartly();
					}
		 		}
				
				Console.log("Hi Vijay global aareylist =="+globalCustomerArray.size());
				if(globalCustomerArray.size() != 0 && LoginPresenter.customerCount == customerCount){
//				if(globalCustomerArray.size() != 0 && LoginPresenter.customerCount == globalCustomerArray.get(globalCustomerArray.size()-1).getCount()){
					LoginPresenter.loadingPanel.hide();
					LoginPresenter.gwtGlassPanel.hide();
 				}
				/**
				 * @author Ashwini
				 * @since 14-1-2022
				 * Below code will generate console indicate value for customer in numger generation is not matching with actual number of records.
				 */
				else{
					Console.log("in else of if(globalCustomerArray.size() != 0 && LoginPresenter.customerCount == customerCount)");
					LoginPresenter.loadingPanel.hide();
					LoginPresenter.gwtGlassPanel.hide();
					//Window.alert("There is conflict in customer number configuration. Please check");				
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Console.log("3-In Failure For Composite");
				caught.printStackTrace();	
			}
		});
	 
	}
	

	private void intializeButtonPanel() {
//		genPopup=new GeneralViewDocumentPopup();
//		genPopup.getBtnClose().addClickHandler(this);
		
		conditionalPopup.getBtnOne().setText("Residential");
		conditionalPopup.getBtnTwo().setText("Commercial");
		
		conditionalPopup.getBtnOne().getElement().getStyle().setWidth(90, Unit.PX);
		conditionalPopup.getBtnTwo().getElement().getStyle().setWidth(90, Unit.PX);
		
		conditionalPopup.getBtnOne().getElement().getStyle().setLeft(230, Unit.PX);
		conditionalPopup.getBtnTwo().getElement().getStyle().setLeft(130, Unit.PX);
		
		conditionalPopup.getBtnOne().addClickHandler(this);
		conditionalPopup.getBtnTwo().addClickHandler(this);
		
		hrButtonPanel=new HorizontalPanel();
		
		btnNew=new Button("<image src='/images/menubar/043-plus.png' width='25px' height='18px'/>");
		btnEdit=new Button("<image src='/images/menubar/162-edit.png' width='25px' height='18px'/>");
		btnLocation=new Button("<image src='/images/menubar/097-pin.png' width='25px' height='18px'/>");
		btnContact=new Button("<image src='/images/menubar/019-add-user.png' width='25px' height='18px'/>");
		
		btnClr = new Button("<image src='/images/menubar/137-eraser.png' width='25px' height='18px'/>");
		
		btnNew.getElement().addClassName("buttonIcon");
		btnEdit.getElement().addClassName("buttonIcon");
		btnLocation.getElement().addClassName("buttonIcon");
		btnContact.getElement().addClassName("buttonIcon");
		
		btnClr.getElement().addClassName("buttonIcon");
		
		btnNew.addClickHandler(this);
		btnEdit.addClickHandler(this);
		btnLocation.addClickHandler(this);
		btnContact.addClickHandler(this);
		
		btnClr.addClickHandler(this);
		
		boolean naf,laf,caf;
		
		naf=isAuthorized("Service", Screen.CUSTOMER);
		if(!naf){
			naf=isAuthorized("Sales", Screen.SALESCUSTOMER);
		}
		
		if(naf){
			hrButtonPanel.add(btnNew);
			hrButtonPanel.add(btnEdit);
		}
		btnNew.setEnabled(naf);
		btnEdit.setEnabled(naf);
		
		laf=isAuthorized("Service", Screen.CUSTOMERBRANCH);
		if(!laf){
			laf=isAuthorized("Sales", Screen.CUSTOMERBRANCH);
		}
		
		if(laf){
			hrButtonPanel.add(btnLocation);
		}
		btnLocation.setEnabled(laf);
		
		caf=isAuthorized("Service", Screen.CONTACTPERSONDETAILS);
		if(!caf){
			caf=isAuthorized("Sales", Screen.CONTACTPERSONDETAILS);
		}
		
		if(caf){
			hrButtonPanel.add(btnContact);
		}
		btnContact.setEnabled(caf);
		
		hrButtonPanel.add(btnClr);
		
		if(!naf&&!laf&&!caf){
			hrButtonPanel.setVisible(false);
		}
		hrButtonPanel.setSpacing(2);
		
	}
	
	public boolean isAuthorized(String moduleName,Screen screen){
		UserRole role = UserConfiguration.getRole();
		if (role == null)
			return false;
		
		for (ScreenAuthorization hr : role.getAuthorization()) {
			String screenName = AuthorizationHelper.checkScreenName(screen);
			if(AppConstants.ADMINUSER.equals(role.getRoleName().trim())){
				return true;
			}else{
				if(hr.getScreens().trim().equalsIgnoreCase(screenName.trim())&&hr.getModule().equalsIgnoreCase(moduleName.trim())) {
					return hr.isCreate();
				}
			}
		}
		return false;
	}

	@Override
	public void onClick(ClickEvent event) {
		Console.log("Person info clicked ...");
		if(event.getSource()==btnNew){
			Console.log("new button clicked ...");
			popUpPanel=new PopupPanel(true);
			popUpPanel.add(conditionalPopup);
			popUpPanel.center();
			popUpPanel.show();
		}
		if(event.getSource()==btnEdit){
			Console.log("edit button clicked ...");
			if(id.getValue()==null||id.getValue().equals("")){
				centity=null;
			}
			if(centity!=null){
				loadCustomer(centity);
			}else{
				alert.alert("Please select customer");
			}
			
		}
		if(event.getSource()==btnClr){
			Console.log("btnClr button clicked ...");
			id.setText("");
			name.setText("");
			phone.setText("");
			tbpocName.setText("");
			centity=null;
			
		}
		if(event.getSource()==btnContact){
			Console.log("contact button clicked ...");
			if(centity==null){
				alert.alert("Please select customer");
				return;
			}
			genPopup=new GeneralViewDocumentPopup();
			genPopup.getBtnClose().addClickHandler(this);
			genPopup.setCompositeScreen("Contact Person Details", null, centity,false);
		}
		if(event.getSource()==btnLocation){
			Console.log("location button clicked ...");
			if(centity==null){
				alert.alert("Please select customer");
				return;
			}
			genPopup=new GeneralViewDocumentPopup();
			genPopup.getBtnClose().addClickHandler(this);
			genPopup.setCompositeScreen("Customer Branch", null, centity,false);
		}
		
		if(event.getSource()==conditionalPopup.getBtnOne()){
			popUpPanel.hide();
			Console.log("button one clicked ...");
			genPopup=new GeneralViewDocumentPopup();
			genPopup.getBtnClose().addClickHandler(this);
			genPopup.setCompositeScreen("Customer", null, centity,false);
		}
		
		if(event.getSource()==conditionalPopup.getBtnTwo()){
			popUpPanel.hide();
			Console.log("button two clicked ...");
			genPopup=new GeneralViewDocumentPopup();
			genPopup.getBtnClose().addClickHandler(this);
			genPopup.setCompositeScreen("Customer", null, centity,true);
		}
		try{
			if(event.getSource()==genPopup.getBtnClose()){
				Console.log("close button clicked ...");
				reactOnBtnClose();
			}
		}catch(Exception e){
			
		}
		
		
	}

	private void reactOnBtnClose() {
		// TODO Auto-generated method stub
		if(genPopup!=null){
			if(genPopup.getPresenter()!=null){
				ScreeenState scrState =genPopup.screenState;
				Console.log("In Comp ScreeenState : "+scrState);
				if(genPopup.getPresenter().getModel() instanceof Customer){
					customerObj=(Customer) genPopup.getPresenter().getModel();
					
					if(customerObj!=null&&customerObj.getCount()!=0){
						PersonInfo info=new PersonInfo();
						info.setCount(customerObj.getCount());
						if(customerObj.isCompany().equals(true)){
							info.setFullName(customerObj.getCompanyName());
						}else{
							info.setFullName(customerObj.getFullname());
						}
						info.setCellNumber(customerObj.getContact().get(0).getCellNo1());
						info.setEmail(customerObj.getContact().get(0).getEmail());
				    	String pocNameVal1=customerObj.getFullname();
				    	info.setPocName(pocNameVal1);
				    	info.setStatus(customerObj.getStatus());
				    	
				    	globalCustomerArray.add(info);
				    	
				    	int id=info.getCount();
						String name=info.getFullName();
						Long cellno=info.getCellNumber();
						String pocNameVal=info.getPocName();
						
						idToPersonInfo.put(id,info);
						nameToPersonInfo.put(name,info);
						cellToPersonInfo.put(cellno,info);
						pocNameToPersonInfo.put(pocNameVal,info);
						
						initalizeCellOracle();
						initalizeIdOracle();
						initalizeNameOracle();
						
						/**
						 * @author ANil
						 * @since 08-02-2022
						 * Buttons on customer composite should be editable in all state but in view state we will not 
						 * update data to composite
						 * Raised by Nitin Sir 
						 */
						if(scrState.equals(ScreeenState.NEW)||scrState.equals(ScreeenState.EDIT)){
							setValue(info);
							centity=info;
						}
					}
					
				}else if(genPopup.getPresenter().getModel() instanceof CustomerBranchDetails){
					custBranchObj=(CustomerBranchDetails) genPopup.getPresenter().getModel();
				}else if(genPopup.getPresenter().getModel() instanceof ContactPersonIdentification){
					contactObj=(ContactPersonIdentification) genPopup.getPresenter().getModel();
//					DomEvent.fireNativeEvent(Document.get().createChangeEvent(), this);
//					DomEvent.fireNativeEvent(Document.get().c, this);
//					SelectionEvent.fire(SelectionHandler<Suggestion>,this.getValue());
//					SelectionEvent.fire(this, SelectionEvent.getType());
//					super.addHandler(handler, SelectionEvent.getType());
					Console.log("Contact Person Details EVENT...");
				}
				
				AppMemory.getAppMemory().currentState=scrState;
			}
		}
	}

	private void loadCustomer(PersonInfo info) {
		genPopup=new GeneralViewDocumentPopup();
		genPopup.getBtnClose().addClickHandler(this);
		Vector<Filter> filtervec = new Vector<Filter>();
		Filter temp = null;
		
		temp=new Filter();
		temp.setQuerryString("companyId");
		temp.setLongValue(UserConfiguration.getCompanyId());
		filtervec.add(temp);
		
		temp = new Filter();
		temp.setIntValue(info.getCount());
		temp.setQuerryString("count");
		filtervec.add(temp);
		
		MyQuerry querry = new MyQuerry();
		querry.setFilters(filtervec);
		querry.setQuerryObject(new Customer());
		
		async.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				// TODO Auto-generated method stub
				if(result!=null&&result.size()!=0){
					Customer obj=(Customer) result.get(0);
					genPopup.setCompositeScreen("Customer", obj, centity,false);
				}
			}
		});
		
		
	}
		
}