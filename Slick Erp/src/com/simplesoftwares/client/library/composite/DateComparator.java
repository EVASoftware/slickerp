package com.simplesoftwares.client.library.composite;


import java.util.Date;
import java.util.Vector;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;


/**
 * Composite gives Date Comparator Functionality on Date Field of Entity.
 * Querry String can be set or by default is creationDate;
 */
public class DateComparator extends Composite implements CompositeInterface,
ValueChangeHandler<Date>,HasValue<Vector<Filter>>
{
	
	/** The from date. */
	protected DateBox fromDate;
	
	/** The to date. */
	protected DateBox toDate;
	
	
	
	/** The content. */
	public FlowPanel content;
	
	/** The form. */
	public FlexForm form;
	
	/** The fields. */
	public FormField[][]fields;
	
	/** The field to date. */
	private FormField fieldid,fieldFromDate,fieldToDate;
	
	/** The builder. */
	private FormFieldBuilder builder;
	
	private String querryString;
	private SuperModel querryKind;
	
	/**
	 * Instantiates a new common search composite.
	 */
	public  DateComparator(SuperModel querryKind)
	{
		createGui();
		querryString="creationDate";
		this.querryKind=querryKind;
		initWidget(content);
		
	
	}
	
	public DateComparator(String querryString,SuperModel querryKind)
	{
		this(querryKind);
		this.querryString=querryString;
		
	}
	
	/**
	 * Initialize widgets. Also apply Handlers on manditory fields.
	 */
	private void initializewidgets()
	{
		fromDate=new DateBoxWithYearSelector();
		toDate=new DateBoxWithYearSelector();
		toDate.addValueChangeHandler(this);
		
	}

	/**
	 * Creates the gui using FlexForm
	 */
	private void createGui()
	{
		content= new FlowPanel();
		initializewidgets();
		
		
		 builder=new FormFieldBuilder("From Date",fromDate);
		 fieldFromDate=builder.setMandatory(true).build();
		 builder=new FormFieldBuilder("To Date",toDate);
		 fieldToDate=builder.setMandatory(true).build();
		
		fields=new FormField[][]{{fieldFromDate,fieldToDate}};
		
		
		
		 form=new FlexForm(fields,FormStyle.ROWFORM);
			
			//applyStyle();
			content.add(form);
		
	}



	


	public DateBox getFromDate() {
		return fromDate;
	}

	public void setFromDate(DateBox fromDate) {
		this.fromDate = fromDate;
	}

	public DateBox getToDate() {
		return toDate;
	}

	public void setToDate(DateBox toDate) {
		this.toDate = toDate;
	}
	
	

	public Date getfromDateValue() {
		return fromDate.getValue();
	}
	
	public Date gettoDateValue() {
		return toDate.getValue();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean status) {
		fromDate.setEnabled(status);
		toDate.setEnabled(status);
		
		
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	@Override
	public void clear() {
		fromDate.getTextBox().setText("");
		toDate.getTextBox().setText("");
		
	}



	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
	 */
	@Override
	public boolean validate() {
		int res=fromDate.getValue().compareTo(toDate.getValue());
		if(res<0)
			return false;
		else
			return true;
		
	}



	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Date> event) {
		DateBox b=(DateBox) event.getSource();
		b.getElement().getStyle().clearBackgroundColor();
		
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Vector<Filter>> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector<Filter> getValue() 
	{
		Vector<Filter>vecFilter=new Vector<Filter>();
		Filter filter=null;
		if(fromDate.getValue()!=null)
			{
			
			/**********************************New Code********************************/
			
			if(toDate.getValue()==null){
				filter=new Filter();
				filter.setQuerryString(querryString+" >=");
				filter.setDateValue(fromDate.getValue());
				vecFilter.add(filter);
				
				filter=new Filter();
				filter.setQuerryString(querryString+" <=");
				filter.setDateValue(fromDate.getValue());
				vecFilter.add(filter);
			}
			else{
				filter=new Filter();
				filter.setQuerryString(querryString+" >=");
				filter.setDateValue(fromDate.getValue());
				vecFilter.add(filter);
			}
			
			/*************************Previous Code***********************************/
			
//				filter=new Filter();
//				if(toDate.getValue()==null)
//					filter.setQuerryString(querryString);
//				else
//					filter.setQuerryString(querryString+" >=");
//				filter.setDateValue(fromDate.getValue());
//				vecFilter.add(filter);
			}
		if(toDate.getValue()!=null)
		{
			filter=new Filter();
			filter.setQuerryString(querryString+" <=");
			filter.setDateValue(toDate.getValue());
			vecFilter.add(filter);
		}
		
		
		return vecFilter;
	}

	@Override
	public void setValue(Vector<Filter> value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setValue(Vector<Filter> value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}

}

