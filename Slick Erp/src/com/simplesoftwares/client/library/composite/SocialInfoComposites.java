/*
 * 
 */
package com.simplesoftwares.client.library.composite;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.slicktechnologies.shared.common.helperlayer.SocialInformation;

// TODO: Auto-generated Javadoc
/**
 * The Class SocialInfoComposites.A simple composite corresponding to {@link SocialInformation} entity
 */
public class SocialInfoComposites extends Composite implements HasValue<SocialInformation>,CompositeInterface 
,Editor<SocialInformation>{

	/** The panel. */
	private FlowPanel panel;
	
	/** The twitter. */
	private TextBox google,facebook,twitter;
	
	/** The form. */
	private FlexForm form;
	
	/** The ff. */
	private FormField [][]ff;
	
	/** The ftwitter. */
	private FormField fgoogle,ffacebook,ftwitter;
	
	/** The builder. */
	private FormFieldBuilder builder;
	
	/**
	 * Instantiates a new social info composites.
	 */
	public SocialInfoComposites()
	{
		createGui();
		initWidget(panel);
	}
	
	/**
	 * Initializes the widgets
	 */
	private void initializewidgets()
	{
		google=new TextBox();
		facebook=new TextBox();
		twitter=new TextBox();
	}
	/**
	 * Creates the gui.
	 */
	private void createGui()
	{
		panel = new FlowPanel();
		initializewidgets();
		
		builder=new FormFieldBuilder("Google Plus ID",google);
		fgoogle=builder.build();
		
		builder=new FormFieldBuilder("FaceBook ID",facebook);
		ffacebook=builder.build();
		
		builder=new FormFieldBuilder("Twitter ID",twitter);
		ftwitter=builder.build();
		
		ff=new FormField[][]{{fgoogle,ffacebook,ftwitter}};
		form=new FlexForm(ff,FormStyle.ROWFORM);
		panel.add(form);
		applyStyle();
		
		}
	
	
	
	

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.Widget#fireEvent(com.google.gwt.event.shared.GwtEvent)
	 */
	@Override
	public void fireEvent(GwtEvent<?> event) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#getValue()
	 */
	/**
	 * Returns the {@link SocialInformation} object.
	 */
	@Override
	public SocialInformation getValue() {
		 SocialInformation entity=new SocialInformation();
		 if(google.getValue()==null)
			 google.setValue("");
		 if(facebook.getValue()==null)
			 facebook.setValue("");
		 if(twitter.getValue()==null)
			 twitter.setValue("");
		 entity.setGooglePlusId(google.getValue());
		 entity.setFaceBookId(facebook.getValue());
		 entity.setTwitterId(twitter.getValue());
		return entity;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<SocialInformation> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
	 */
	/**
	 * Sets the composite with {@link SocialInformation} object.
	 */
	@Override
	public void setValue(SocialInformation value) {
		this.facebook.setValue(value.getFaceBookId());
		this.twitter.setValue(value.getTwitterId());
		this.google.setValue(value.getGooglePlusId());
		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object, boolean)
	 */
	@Override
	public void setValue(SocialInformation value, boolean fireEvents) {
		// TODO Auto-generated method stub
		
	}

	
	/**
	 * Sets the form width.
	 */
	public void applyStyle()
	{
		form.setWidth("98%");
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	@Override
	public void clear()
	{
		int row=ff.length;
		for(int k=0;k<row;k++)
		{
			int colCount =ff[k].length ;
			for(int j=0;j<colCount;j++)
			{
				Widget widg= ff[k][j].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setText("");
				
				}
				
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean status)
	{
		int row=ff.length;
		for(int k=0;k<row;k++)
		{
			int colCount =ff[k].length ;
			for(int j=0;j<colCount;j++)
			{
				Widget widg= ff[k][j].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setEnabled(status);
				
				}
				
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
	 */
	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * Gets the panel.
	 *
	 * @return the panel
	 */
	public FlowPanel getPanel() {
		return panel;
	}

	/**
	 * Sets the panel.
	 *
	 * @param panel the new panel
	 */
	public void setPanel(FlowPanel panel) {
		this.panel = panel;
	}

	/**
	 * Gets the google.
	 *
	 * @return the google
	 */
	public TextBox getGoogle() {
		return google;
	}

	/**
	 * Sets the google.
	 *
	 * @param google the new google
	 */
	public void setGoogle(TextBox google) {
		this.google = google;
	}

	/**
	 * Gets the facebook.
	 *
	 * @return the facebook
	 */
	public TextBox getFacebook() {
		return facebook;
	}

	/**
	 * Sets the facebook.
	 *
	 * @param facebook the new facebook
	 */
	public void setFacebook(TextBox facebook) {
		this.facebook = facebook;
	}

	/**
	 * Gets the twitter.
	 *
	 * @return the twitter
	 */
	public TextBox getTwitter() {
		return twitter;
	}

	/**
	 * Sets the twitter.
	 *
	 * @param twitter the new twitter
	 */
	public void setTwitter(TextBox twitter) {
		this.twitter = twitter;
	}

	/**
	 * Gets the form.
	 *
	 * @return the form
	 */
	public FlexForm getForm() {
		return form;
	}

	/**
	 * Sets the form.
	 *
	 * @param form the new form
	 */
	public void setForm(FlexForm form) {
		this.form = form;
	}

	/**
	 * Gets the ff.
	 *
	 * @return the ff
	 */
	public FormField[][] getFf() {
		return ff;
	}

	/**
	 * Sets the ff.
	 *
	 * @param ff the new ff
	 */
	public void setFf(FormField[][] ff) {
		this.ff = ff;
	}
	
	
	
	
	
	

}
