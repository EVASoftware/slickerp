package com.simplesoftwares.client.library.composite;

import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.search.Filter;

public interface HasFilter 
{
  Vector<Filter>getFilter();
  void setFilter(Vector<Filter>filter);
}
