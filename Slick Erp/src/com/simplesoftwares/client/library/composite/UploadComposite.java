package com.simplesoftwares.client.library.composite;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.FormElement;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.simplesoftwares.client.library.libservice.BlobService;
import com.simplesoftwares.client.library.libservice.BlobServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

/**
 * The Class UploadComposite.Instance Comes with displayed file link and a
 * delete button. State chart of component is such : Start : File Upload Widget
 * is visible link is invisible Delete is invisible Doc uploaded : File Upload
 * Widget is visible Link is visible Delete is visible Delete Pressed :
 * Transition to Start
 * 
 * Enable False : File Upload in disabled;Delete is non disabled;Link is
 * Enabled(if present) Enable True : File Upload in Enabled;Delete is non
 * Enabled;Link is Enabled
 * 
 * To DO : Any how will have to explain Komal,so explain complete the
 * documentation and explain her sequence diagram.Recent Changes are not
 * tested.Tell komal to test.
 * 
 */
public class UploadComposite extends Composite implements
		HasValue<DocumentUpload>, ClickHandler, SubmitCompleteHandler,
		ChangeHandler, CompositeInterface, Editor<DocumentUpload> {

	/** The singleuploader. File Upload Widget */
	protected FileUpload singleuploader;

	/**
	 * hyperlink used to download the uploaded document
	 * */
	protected Hyperlink hyper;

	/**
	 * FormPanel object to provide upload functionality
	 * */
	protected FormPanel panel;

	/**
	 * Horizontal Panel holds the UI
	 * */
	protected HorizontalPanel hp;

	/**
	 * . cancel button ,this will clear the fileupload widget and hide the
	 * hyperlink this button will also set the Entity document upload part to
	 * null;
	 * */
	protected Button cancel;

	/**
	 * flags which restrict the file type to be uploaded.
	 * */
	protected boolean image, pdf, word;

	/**
	 * The blobservice. This service provides server side functionality to the
	 * component
	 * */
	protected BlobServiceAsync blobservice = GWT.create(BlobService.class);

	/** The saved file name. */
	// private String savedfileName;

	/**
	 * The document upload entity representing the Document which is uploaded or
	 * will be uploaded
	 * */
	DocumentUpload uploadDocument;

	/**
	 * Instantiates a new my upload composite.
	 */
	
	/** The Newsingleuploader. File Upload Widget */
	public FileUpload singleNewUploader;


	/**
	 * On click of cancel the hyperlink and cancel should be hidden.When ever
	 * user selects a file the file should be automatically uploaded. To do
	 * these things we apply handlers on cancel and file upload widget.Also when
	 * submit completes the hyperlink should be visible with the url of
	 * download,We achieve this by applying submitComplete handler on panel
	 */
	public UploadComposite(){
		createGui();
		image = false;
		initWidget(panel);
		addHandlers();
	}
	/***
	 * Date 18-07-2019 by Vijay
	 * Des :- when we upload document then we are reading directly using this constructor
	 * and also using non static blob key for reading excel
	 * @param flag
	 */
	public UploadComposite(String transactionName, boolean flag){
		hp = new HorizontalPanel();
		singleNewUploader = new FileUpload();
		singleNewUploader.setTitle(transactionName);
		cancel = new Button("Delete");
		hyper = new Hyperlink();
		panel = new FormPanel();
		 FormElement.as(panel.getElement()).setAcceptCharset("UTF-8");
		singleNewUploader.setName("upload");
		// Form panel settings
		panel.setEncoding(FormPanel.ENCODING_MULTIPART);
		panel.setMethod(FormPanel.METHOD_POST);
		
		hp.add(singleNewUploader);
		hp.add(cancel);
		hp.add(hyper);
		hyper.setVisible(false);
		panel.setWidget(hp);
		cancel.setVisible(false);
		
		image = false;
		initWidget(panel);
		addHandlers();
	}

	/**
	 * Initializes the cancel button ,hyperlink ,panel. Also sets encoding on
	 * form panel
	 */
	public void initalizevariables() {
		hp = new HorizontalPanel();
		singleuploader = new FileUpload();
		cancel = new Button("Delete");
		hyper = new Hyperlink();
		panel = new FormPanel();
		singleuploader.setName("upload");
		// Form panel settings
		panel.setEncoding(FormPanel.ENCODING_MULTIPART);
		panel.setMethod(FormPanel.METHOD_POST);
	}

	/**
	 * Creates the gui.
	 */
	public void createGui() {
		initalizevariables();
		hp.add(singleuploader);
		hp.add(cancel);
		hp.add(hyper);
		hyper.setVisible(false);
		panel.setWidget(hp);
		cancel.setVisible(false);

	}

	/**
	 * Adds the {@link SubmitCompleteHandler} handler on panel Adds the
	 * {@link ClickHandler} on the cancel button Adds the {@link ChangeHandler}
	 * on <code>singleuploader</code>
	 */
	@SuppressWarnings("deprecation")
	public void addHandlers() {
		panel.addSubmitCompleteHandler(this);
		cancel.addClickHandler(this);
		if(singleuploader!=null){
		singleuploader.addChangeHandler(this);
		}
		this.hyper.addClickHandler(this);
		
		/** Date 18-07-2019 by Vijay ****/
		if(singleNewUploader!=null){
			singleNewUploader.addChangeHandler(this);
		}
		

	}

	/**
	 * The method makes the widget to transit in START state
	 */
	private void reactOnDelete() {
		// this.savedfileName=null;
		this.hyper.setVisible(false);
		this.hyper.setHTML("");
		cancel.setVisible(false);
		
		/**
		 * @author Anil,Date : 08-02-2019
		 * for tracking deleted record as well in employee,instead of setting null we initialize upload document
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "UploadCompositeTracking")){
			uploadDocument=new DocumentUpload();
			uploadDocument.setStatus(true);
		}else{
			uploadDocument = null;
		}
	}

	/**
	 * Downloads the file from corresponding link clicks.
	 */

	private void download() {
		System.out.println("Called-----"+uploadDocument.getUrl());
		String urls = uploadDocument.getUrl() + "&" + "filename="+ hyper.getHTML();
		Console.log("URLS :  "+urls);
		Window.open(urls,"","menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=no,toolbar=true, width="
						+ Window.getClientWidth()+ ",height="+ Window.getClientHeight());
		System.out.println("Called-----");

	}

	/**
	 * Actual file upload process code starts from this method. Method checks
	 * for valid files. If file is valid then it sets the url To Do : Understand
	 * the fake path thing
	 */
	private void reactOnFileChange() {
		String s = this.singleuploader.getFilename();
		if (validFile(s))
			setUrlAndSubmit();
		System.out.println("Res--" + validFile(s));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event
	 * .dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {

		if (event.getSource() instanceof Button)
			reactOnDelete();
		if (event.getSource() instanceof Hyperlink)
			download();

	}

	/**
	 * Valid file.
	 * 
	 * @param s
	 *            file path to be checked valid
	 *
	 * @return true, if uploaded file is not empty. Or if the file type does not
	 *         matches the valid file
	 */

	@SuppressWarnings("null")
	public boolean validFile(String s) {

		if (s == null || s.trim().equals(""))
			return false;
		else {

			// Check for Image
			if (image) {
				s = s.toLowerCase();
				if (s.contains("jpeg") || s.contains("exif")
						|| s.contains("tiff") || s.contains("png")
						|| s.contains("bmp"))
					return true;
				else
					return false;

			}

			else
				return true;
		}
	}

	/**
	 * Sets the url and submit. the methods uses blobservice to get the new url
	 * for upload and sets that to form panel.
	 * 
	 * @param s
	 */
	private void setUrlAndSubmit() {
		System.out.println("UPLOAD COMP ON URL SUBMIT");
		blobservice.getBlobStoreUploadUrl(new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(String result) {
				panel.setAction(result);
				panel.submit();
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasValue#getValue()
	 */
	@Override
	public DocumentUpload getValue() {

		return uploadDocument;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(DocumentUpload value) {
		uploadDocument = value;
		if (value != null&&value.getName()!=null&&!value.getName().equals("")) {
			System.out.println("UPLOAD COMP SET VALUE METHOD :: ");
			String savedfileName = uploadDocument.getName();
			System.out.println("FILE NAME : "+savedfileName);
			this.hyper.setVisible(true);
			this.hyper.setHTML(savedfileName);
			cancel.setVisible(true);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#
	 * addValueChangeHandler
	 * (com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<DocumentUpload> handler) {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object,
	 * boolean)
	 */
	@Override
	public void setValue(DocumentUpload value, boolean fireEvents) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.simplesoftwares.client.library.composite.CompositeInterface#clear()
	 */
	@Override
	public void clear() {
		System.out.println("UPLOAD COMP CLEAR METHOD :: ");
		panel.reset();
		this.hyper.setVisible(false);
		this.hyper.setHTML("");
		cancel.setVisible(false);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.simplesoftwares.client.library.composite.CompositeInterface#validate
	 * ()
	 */
	@Override
	public boolean validate() {

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.simplesoftwares.client.library.composite.CompositeInterface#setEnable
	 * (boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		singleuploader.setEnabled(state);
		if (hyper.getText().trim().equals(""))
			cancel.setVisible(false);
		else
			cancel.setVisible(state);

	}

	// The method is called when submit is completed.WE set the hyperlink of
	// uploaded url
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler#
	 * onSubmitComplete
	 * (com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent)
	 */
	/**
	 * Transit of app in Document Uploaded Mode To DO: If an error comes during
	 * upload process,system will behave unexpectedly ,please fix At this point
	 * uploadDocument object is created.
	 * 
	 */
	@Override
	public void onSubmitComplete(SubmitCompleteEvent event) {
		System.out.println("ON SUBMIT UPLOAD COMP");
//		String[] splitaray = singleuploader.getFilename().split("\\\\");
		/***
		 * Date 18-07-2019 by Vijay
		 * For normal upload and new overloaded method upload added if condition
		 */
		String[] splitaray = null;
		if(singleuploader!=null){
			splitaray = singleuploader.getFilename().split("\\\\");
		}
		if(singleNewUploader!=null){
			splitaray = singleNewUploader.getFilename().split("\\\\");
		}
		/**
		 * ends here
		 */
		String savedfileName = splitaray[splitaray.length - 1];
		// User has de selected the file do nothing
		hyper.setHTML(savedfileName);
		cancel.setVisible(true);
		hyper.setVisible(true);
		uploadDocument = new DocumentUpload();
		uploadDocument.setName(savedfileName);
		uploadDocument.setUrl(event.getResults());
		Console.log("Event.getResult at onsubmit :- "+event.getResults());
		
		/**
		 * Date: 17-11-2018 By ANIL
		 * Setting status to true for capturing employee upload history
		 */
		uploadDocument.setStatus(true);
		/**
		 * Date : 19-11-2018 By ANIL
		 */
		if(UserConfiguration.getUserconfig()!=null){
			uploadDocument.setUploadedBy(UserConfiguration.getUserconfig().getUser().getEmployeeName());
		}
		panel.reset();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.dom.client.ChangeHandler#onChange(com.google.gwt
	 * .event.dom.client.ChangeEvent)
	 */
	@Override
	public void onChange(ChangeEvent event) {
		System.out.println("UPLOAD COMP ON CHANGE");
		System.out.println("Ajay------");
//		reactOnFileChange();
		if(event.getSource()==singleuploader){
			reactOnFileChange();
		}
		/** Date 18-07-2019 by Vijay for NBHC CCPM Contract Upload and also usefull for Other uploading with seperated Static blobkey***/
		if(event.getSource()==singleNewUploader){
			reactOnNewFileChange();
		}
	}


	public Button getCancel() {
		return cancel;
	}

	public void setCancel(Button cancel) {
		this.cancel = cancel;
	}

	
	/** Date 18-07-2019 by Vijay 
	 * Des :- NBHC CCPM Contract Upload 
	 *  Also useful for other operation file reading with sepearte blob key for common
	 */
	/**
	 * Sets the url and submit. the methods uses blobservice to get the new url
	 * for upload and sets that to form panel.
	 * @param string 
	 */
	
	private void reactOnNewFileChange() {
		String s = this.singleNewUploader.getFilename();
		if (validFile(s)){
			setNewUrlAndSubmit(singleNewUploader.getTitle());
		}
	}
	
	private void setNewUrlAndSubmit(final String transactionName) {//Deepaks final veriable
		blobservice.getBlobStoreNewUploadUrl(transactionName,new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(String result) {
				
//				if(transactionName.equals("PurchaseRequisition")&&!result.equals("Inprocess")){
//				panel.setAction(result);
//				panel.submit();
//				}
				if(transactionName.equals("Contract")&&!result.equals("Inprocess")){
					panel.setAction(result);	
				    panel.submit();
				}else{
					panel.setAction(result);
					panel.submit();
				}
			}
		});

	}
	
	
}
