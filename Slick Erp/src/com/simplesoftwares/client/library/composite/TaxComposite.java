package com.simplesoftwares.client.library.composite;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.ListBox;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.mywidgets.NumbersOnly;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.productlayer.Tax;

// TODO: Auto-generated Javadoc
/**
 * The Class TaxComposite.
 */
public class TaxComposite extends Composite implements ValueChangeHandler<Boolean>,HasValue<Tax>,CompositeInterface
,Editor<Tax>
{
	
	/** The sellbl. */
	public InlineLabel taxlbl,sellbl;
	
	/** The taxch. */
	public CheckBox taxch;
	
	/** The percentagelbl. */
	public InlineLabel percentagelbl;
	
	/** The percentage box. */
	public ObjectListBox<TaxDetails> percentageBox;
	
	/** The percentage */
	public DoubleBox dbPercent;
	
	/** The tax inlcusive. */
	public InlineLabel taxInlcusive;
	
	/** The tax inclusivech. */
	public CheckBox taxInclusivech;
	
	/** The table. */
	public Grid table;
	
	
	/**
	 * Date : 01-07-2017 By ROHAN
	 */
	public String taxPrintableName;
	/**
	 * END
	 */
	
	/**
	 * Instantiates a new tax composite.
	 */
	public TaxComposite()
	{
		sellbl=new InlineLabel("Select");
		taxch=new CheckBox("");
		taxInclusivech=new CheckBox();
		taxInlcusive=new InlineLabel("Inclusive");
		percentageBox=new ObjectListBox<TaxDetails>();
//		percentageBox.addItem("SELECT");
		percentagelbl=new InlineLabel("%");
		taxInclusivech=new CheckBox();
		dbPercent=new DoubleBox();
		dbPercent.setEnabled(false);
		table=new Grid(1, 7);
		
		
		table.setWidget(0, 0, sellbl);
		table.setWidget(0, 1,taxch);
		table.setWidget(0, 2, percentagelbl);
		table.setWidget(0, 3, percentageBox);
		table.setWidget(0, 4, dbPercent);
		table.setWidget(0, 5, taxInlcusive);
		table.setWidget(0, 6, taxInclusivech);
		
		//Why this was needed?
		dbPercent.addStyleName("percentage");
		percentageBox.addStyleName("taxconfig");
		initWidget(table);
		
		//taxch.addValueChangeHandler(this);
		taxch.setValue(false);
		percentageBox.setEnabled(false);
		
		
		taxPrintableName=new String();
	}

	
	
	



	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<Boolean> event) {
		
		
		if(event.getValue()==true)
			percentageBox.setEnabled(true);
			
		else
		{
			percentageBox.setEnabled(false);
			percentageBox.setSelectedIndex(0);
		}
		
	}
		

/* (non-Javadoc)
 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
 */
@Override
public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Tax> handler) {
	// TODO Auto-generated method stub
	return null;
}

/* (non-Javadoc)
 * @see com.google.gwt.user.client.ui.HasValue#getValue()
 */
@Override
public Tax getValue() 
{
	Tax t=new Tax();
	
	if(dbPercent.getValue()==null)
		t.setPercentage(0);
	else
		t.setPercentage(dbPercent.getValue());
	
//	if(percentageBox.getSelectedIndex()==0)
//		t.setPercentage(0);
//	else
//		t.setPercentage(Double.parseDouble(percentageBox.getItemText(percentageBox.getSelectedIndex())));
	
	t.setTaxConfigName(percentageBox.getValue());
	
	t.setInclusive(taxInclusivech.getValue());
	
	
	t.setTaxPrintName(taxPrintableName);
	
	return t;
}

/* (non-Javadoc)
 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object)
 */
@Override
public void setValue(Tax value) 
{
	if(value!=null)
	{
		if(value.getPercentage()!=0)
		{
			this.taxch.setValue(true);
			percentageBox.setEnabled(true);
			this.dbPercent.setValue(value.getPercentage());
			percentageBox.setValue(value.getTaxConfigName());
			taxInclusivech.setValue(value.isInclusive());
			
			/**
			 * Date : 01-07-2017 BY ROHAN
			 */
			if(value.getTaxPrintName()!=null){
				taxPrintableName=value.getTaxPrintName();
			}
			
		}
		else
		{
			this.taxch.setValue(false);
			percentageBox.setSelectedIndex(0);
			percentageBox.setEnabled(false);
		}
	}
	
	
	
}


/* (non-Javadoc)
 * @see com.google.gwt.user.client.ui.HasValue#setValue(java.lang.Object, boolean)
 */
@Override
public void setValue(Tax value, boolean fireEvents) {
	// TODO Auto-generated method stub
	
}


/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.composite.CompositeInterface#setEnable(boolean)
 */
@Override
public void setEnable(boolean state)
{
	taxch.setEnabled(state);
	
	if(taxch.getValue()==false)
		percentageBox.setEnabled(false);
	else
		percentageBox.setEnabled(state);
	
	taxInclusivech.setEnabled(state);
	dbPercent.setEnabled(false);
}

/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.composite.CompositeInterface#clear()
 */
@Override
public void clear()
{
	taxch.setValue(false);
	taxInclusivech.setValue(false);
	percentageBox.setSelectedIndex(0);
	percentageBox.setEnabled(false);
	dbPercent.setValue(null);
}







/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.composite.CompositeInterface#validate()
 */
@Override
public boolean validate() {
	
	//  rohan added this code for validate 
	// Date : 27/2/2017
	
	if(taxch.getValue() && percentageBox.getSelectedIndex()==0)
	{
		System.out.println("inside validate  rohan");
		GWTCAlert alert =new GWTCAlert();
		alert.alert("Please select tax details..!");
		return false;
	}
	
	
	return true;
}




}
