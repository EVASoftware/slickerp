package com.simplesoftwares.client.library.composite;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FormFieldBuilder;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.EmployeeInfoService;
import com.simplesoftwares.client.library.libservice.EmployeeInfoServiceAsync;


import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;

public class EmployeeInfoComposite extends  Composite implements HasValue<EmployeeInfo>,
					SelectionHandler<Suggestion>,Editor<EmployeeInfo>,CompositeInterface
{
	  private SuggestBox id;
	  private SuggestBox name;
	  private SuggestBox phone;
	  TextBox designation;
	  TextBox department;
	  TextBox employeeType;
	  TextBox employeeRole;
	  TextBox branch;
	  FormField employeeId,employeeName,
	  employeeCell,employeeDesignation,
	  employeeDepartment,femployeeRole,femployeeType,fbranch;
	  FlexForm form;
	  private boolean isValidate;
	  
	  /**
	     * Gets the querry.
	     *
	     * @return the querry
	     */
	    public MyQuerry getQuerry() 
	    {
			return querry;
		}

		/**
		 * Sets the querry.
		 *
		 * @param querry the new querry
		 */
		public void setQuerry(MyQuerry querry) 
		{
			this.querry = querry;
		}
	
		private EmployeeInfo empentity;
	    
	    /** The id to person info Map */
	    private   HashMap<Integer,EmployeeInfo>idToEmployeeInfo = new HashMap<Integer,EmployeeInfo>();
		
		/** The cell to person info Map */
		private   HashMap<Long,EmployeeInfo>cellToEmployeeInfo = new HashMap<Long,EmployeeInfo>();
		
		/** The name to person info Map */
		private   HashMap<String,EmployeeInfo>nameToEmployeeInfo = new HashMap<String,EmployeeInfo>();
				
		/** The designation to person info Map */
		private   HashMap<String,EmployeeInfo>desinationToEmployeeInfo = new HashMap<String,EmployeeInfo>();
	
		/** The department to person info Map */
		private   HashMap<String,EmployeeInfo>departmentToEmployeeInfo = new HashMap<String,EmployeeInfo>();
		
		private   HashMap<String,EmployeeInfo>employeeRoleToEmployeeInfo = new HashMap<String,EmployeeInfo>();
	
		private   HashMap<String,EmployeeInfo>employeeTypeToEmployeeInfo = new HashMap<String,EmployeeInfo>();
		
		private   HashMap<String,EmployeeInfo>branchToEmployeeInfo = new HashMap<String,EmployeeInfo>();
		/** The service. */
		final EmployeeInfoServiceAsync service=GWT.create(EmployeeInfoService.class);
		
		/** The querry. */
		MyQuerry querry;
	
		/** The panel. */
	    private FlowPanel panel;
	    
	    /** The ff. */
	    private FormField[][]ff;
	    private boolean mandatory;
		
	    public EmployeeInfoComposite(MyQuerry querry) 
		{
			super();
			mandatory=true;
			createForm();
			this.querry=querry;
			initalizeEmployeeInfoHashMaps();
			initWidget(panel);
			isValidate=true;	
			applyStyle();
		}
		
	    public EmployeeInfoComposite(MyQuerry querry,boolean mode) 
		{
			super();
			createForm();
			this.querry=querry;
			initalizeEmployeeInfoHashMaps();
			initWidget(panel);
			mandatory=false;
			isValidate=mode;
			applyStyle();
		}

	    
	    private void initializewidgets()
		{
			id=new SuggestBox();
			id.addSelectionHandler(this);
			name=new SuggestBox();
			name.addSelectionHandler(this);
			phone=new SuggestBox();
			phone.addSelectionHandler(this);
			designation=new TextBox();
			
			designation.setEnabled(false);
			department=new TextBox();
			
			department.setEnabled(false);
			employeeRole=new TextBox();
			employeeRole.setEnabled(false);
			employeeType=new TextBox();
			employeeType.setEnabled(false);
			branch=new TextBox();
			branch.setEnabled(false);
		}
		
	    public void createForm()
		{
	    	initializewidgets();
			FormFieldBuilder builder;
			    
			builder= new FormFieldBuilder("* ID",id);
			employeeId=builder.setMandatory(true).setColSpan(0).
					setMandatoryMsg("ID  is mandatory!").build();
			builder=new FormFieldBuilder("* Name",name);
			employeeName=builder.setMandatory(true).setColSpan(0).
			    		 setMandatoryMsg("Name is mandatory!").build();
			builder=new FormFieldBuilder("* Cell",phone);
			employeeCell=builder.setMandatory(true).setColSpan(0).
			    		 setMandatoryMsg("Cell Phone is mandatory!").build();
			builder=new FormFieldBuilder("Designation",designation);
			
			employeeDesignation=builder.build();
			builder=new FormFieldBuilder("Department",department);
			employeeDepartment=builder.build();
			builder=new FormFieldBuilder("Employee Role",employeeRole);
			femployeeRole=builder.build();
			builder=new FormFieldBuilder("Employee Type",employeeType);
			femployeeType=builder.build();
			builder=new FormFieldBuilder("Branch",branch);
			fbranch=builder.build();
			     
			ff=new FormField[][]
			{
				{employeeId,employeeName,employeeCell,fbranch},
				{employeeDepartment,femployeeType,employeeDesignation,femployeeRole}
		    };
			
			form = new FlexForm(ff,FormStyle.ROWFORM);
			panel=new FlowPanel();
			form.setWidth("98%");
			panel.add(form);
		  }
	
	    @Override
	    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<EmployeeInfo> handler) 
	    {
	    	return this.addValueChangeHandler(handler);
	    }

	    @Override
	    public void setEnable(boolean status) 
	    {
	    	this.id.setEnabled(status);
	    	this.name.setEnabled(status);
	    	this.phone.setEnabled(status);
	    	this.designation.setEnabled(false);
	    	this.department.setEnabled(false);
	    	this.employeeRole.setEnabled(false);
	    	this.employeeType.setEnabled(false);
			this.branch.setEnabled(false);
	    }

	    @Override
	    public void clear() 
	    {
	    	form.clear();					
	    }
	
	
		/**
	 	 * Change background color.
	 	 *
	 	 * @param widg the widg
	 	 * @param color the color
	 	 */
	    private void changeBorderColor(Widget widg,String color)
	    {
	    	widg.getElement().getStyle().setBorderColor(color);
	    }
	 
		 /**
	 	 * Clear background color.
	 	 *
	 	 * @param widg the widg
	 	 */
	    private void resetBorderColor(Widget widg)
	    {
	    	widg.getElement().getStyle().clearBorderColor(); 
	    }
		
	    @Override
	    public boolean validate() 
	    {
//	    	boolean res=form.validate();
//			
//	    	if(res==false)
//				return false;
//	    	
//	    	int count=0;
//			boolean result=true;
	    	
	    	
	    	if(isValidate==false)
				return true;
	    	
	    	int count=0;
			boolean result=true;
			
	    	boolean res=form.validate();
			
	    	if(res==false)
				return false;
	    	
	    	
			try
			{
				count=Integer.parseInt(this.id.getText().trim());
			}
			
			catch(Exception e)
			{
				changeBorderColor(this.name,"#dd4b39");
				changeBorderColor(this.phone,"#dd4b39");
				changeBorderColor(this.id,"#dd4b39");
				
				if(this.employeeId!=null)
				{
					employeeId.setMandatoryMsg("Employee Id Should be Numeric !");
					employeeId.getMandatoryMsgWidget().setVisible(true);
				}
				result=false;
				return result;
			}
			
			EmployeeInfo info=idToEmployeeInfo.get(count);
			
			if(info==null)
			{
				result=false;
				changeBorderColor(this.name,"#dd4b39");
				changeBorderColor(this.phone,"#dd4b39");
				changeBorderColor(this.id,"#dd4b39");
				
				if(this.employeeId!=null)
				{
					employeeId.setMandatoryMsg("Employee Id do not exists !");
					employeeId.getMandatoryMsgWidget().setVisible(true);
					return result;
				}
			}
			
			String name= this.name.getText();
	        info=nameToEmployeeInfo.get(name);
	        
	        if(info==null)
	        {
	        	if(employeeName!=null)
	        	{
	        	employeeName.setMandatoryMsg("Employee Name do not exists  !");
				employeeId.getMandatoryMsgWidget().setVisible(true);
	        	}
				return false;
	        }
	        Long cell;
	        try
	        {
	        	cell=Long.parseLong(this.phone.getText().trim());
	    	}
	        
	        catch(Exception e)
	    	{
	    			changeBorderColor(this.name,"#dd4b39");
	    			changeBorderColor(this.phone,"#dd4b39");
	    			changeBorderColor(this.id,"#dd4b39");
	    			if(this.employeeCell!=null)
	    			{
	    				employeeId.setMandatoryMsg("Employee Cell Should be Numeric !");
	    				employeeId.getMandatoryMsgWidget().setVisible(true);
	    			}
	    			result=false;
	    			return result;
	    	}
	        info=cellToEmployeeInfo.get(cell);
	        if(info==null)
	        {
	    			result=false;
	    			changeBorderColor(this.name,"#dd4b39");
	    			changeBorderColor(this.phone,"#dd4b39");
	    			changeBorderColor(this.id,"#dd4b39");
	    			
	    			if(this.employeeCell!=null)
	    			{
	    				employeeId.setMandatoryMsg("Employee Cell do not exist !");
	    				employeeId.getMandatoryMsgWidget().setVisible(true);
	    				return result;
	    			}
	    	}
	        
	        return result;
	    }

	    @Override
	    public void onSelection(SelectionEvent<Suggestion> event) 
	    {
	    	SuggestBox sb=(SuggestBox) event.getSource();
	    	String selitem=sb.getText().trim();
	    	setColorBorder(sb,"WHITE");
		
			if(sb==this.id)
			{
				int count=Integer.parseInt(selitem);
				empentity=idToEmployeeInfo.get(count);
				if(empentity==null)
				{
					setColorBorder(sb,"#dd4b39");
					sb.setText("");
				}
				else
				{
					name.setText(empentity.getFullName());
					phone.setText(empentity.getCellNumber()+"");
					designation.setText(empentity.getDesignation());
					department.setText(empentity.getDepartment());
					employeeRole.setText(empentity.getEmployeerole());
					employeeType.setText(empentity.getEmployeeType());
					branch.setText(empentity.getBranch());
					this.clearAllBorderColor();	
				}
			}
		
			if(sb==this.phone)
			{
				Long  phone=Long.parseLong(selitem);
				empentity=cellToEmployeeInfo.get(phone);
				if(empentity==null)
				{
					setColorBorder(sb,"#dd4b39");
					sb.setText("");
				}
				else
				{
					name.setText(empentity.getFullName());
					id.setText(empentity.getCount()+"");
					designation.setText(empentity.getDesignation());
					department.setText(empentity.getDepartment());
					employeeRole.setText(empentity.getEmployeerole());
					employeeType.setText(empentity.getEmployeeType());
					branch.setText(empentity.getBranch());
					this.clearAllBorderColor();
				}
			}
		
			if(sb==this.name)
			{
				String[]xyz=selitem.split("-");
				
				if(xyz.length==0)
				{
					setColorBorder(sb,"#dd4b39");
					sb.setText("");
					return;		
				}
				
				else
				{
					empentity=nameToEmployeeInfo.get(selitem);
					if(empentity==null)
					{
						setColorBorder(sb,"#dd4b39");
						sb.setText("");
					}
					else
					{
						phone.setText(empentity.getCellNumber()+"");
						id.setText(empentity.getCount()+"");
						this.name.setText(empentity.getFullName());
						designation.setText(empentity.getDesignation());
						department.setText(empentity.getDepartment());
						employeeRole.setText(empentity.getEmployeerole());
						employeeType.setText(empentity.getEmployeeType());
						branch.setText(empentity.getBranch());
						this.clearAllBorderColor();			
					}
				}
			}
	    }
	
	    public void setColorBorder(Widget widget,String color)
	    {
	    	widget.getElement().getStyle().setBackgroundColor(color);
	    }

		 /**
	 	 * Sets the enabled.
	 	 *
	 	 * @param status the new enabled
	 	 */
  	
 	
	 	 /**
	 	 * Initalize person info hash maps.
	 	 */
	    public void initalizeEmployeeInfoHashMaps()
	    { 
	    	EmployeeInfoServiceAsync  personservice=GWT.create(EmployeeInfoService.class);
		
	    	personservice.LoadVinfo(querry, new AsyncCallback<ArrayList<EmployeeInfo>>() {

			@Override
			public void onFailure(Throwable caught) 
			{
				
			}

			@Override
			public void onSuccess(ArrayList<EmployeeInfo> result) 
			{
				for(int i=0;i<result.size();i++)
				{
					int id=result.get(i).getCount();
				
					String name=result.get(i).getFullName();
					Long cellno=result.get(i).getCellNumber();
					String department=result.get(i).getDepartment();
					String designation=result.get(i).getDesignation();
					String employeeRole=result.get(i).getEmployeerole();
					String employeeType=result.get(i).getEmployeeType();
					String branch=result.get(i).getBranch();
					
					idToEmployeeInfo.put(id, result.get(i));
					nameToEmployeeInfo.put(name, result.get(i));
					cellToEmployeeInfo.put(cellno, result.get(i));
					departmentToEmployeeInfo.put(department, result.get(i));
					desinationToEmployeeInfo.put(designation, result.get(i));
					employeeRoleToEmployeeInfo.put(employeeRole, result.get(i));
					employeeRoleToEmployeeInfo.put(employeeType, result.get(i));
					branchToEmployeeInfo.put(branch, result.get(i));		
				}
				initalizeCellOracle();
				initalizeIdOracle();
				initalizeNameOracle();
				
				//initalizeDesignationOracle();
				//initalizeDepartmentOracle();
			}
		});
	 }

	    /**
	     *	Apply style.
	     */
	    public void applyStyle()
	    {
	    	panel.setWidth("98%");
	    }
	 

	 	 /**
	 	 * Initalize name oracle.
	 	 */
	    private void initalizeNameOracle()
	    {
			 MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.name.getSuggestOracle();
			 System.out.println("size of name hashmap  --------------- ::"+nameToEmployeeInfo.size());
		 
			 for(String name:nameToEmployeeInfo.keySet())
			 {
				 System.out.println("Name IS ");
				 orcl.add(name);
			 }
	    }
	 	 /**
	 	 * Initalize cell oracle.
	 	 */
	    private void initalizeCellOracle()
	    {
	    	MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.phone.getSuggestOracle();
	    	for(Long cell:this.cellToEmployeeInfo.keySet())
	    		orcl.add(cell+"");
	    }
	 
		 /**
	 	 * Initalize id oracle.
	 	 */
	    private void initalizeIdOracle()
	    {
	    	MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.id.getSuggestOracle();
	    	for(int id:this.idToEmployeeInfo.keySet())
	    		orcl.add(id+""); 
	    }
	    
	    /*	private void initalizeDesignationOracle()
 		{
 			MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) ((SuggestBox) this.designation).getSuggestOracle();
		 	System.out.println("size of name hashmap  --------------- ::"+desinationToEmployeeInfo.size());
		 
			 for(String designation:desinationToEmployeeInfo.keySet())
			 {
				 System.out.println("Designation IS ");
				 orcl.add(designation); 
			 }
 		}
 		
	 	private void initalizeDepartmentOracle()
	 	{
	 		MultiWordSuggestOracle orcl = (MultiWordSuggestOracle) this.designation.getSuggestOracle();
			 System.out.println("size of name hashmap  --------------- ::"+departmentToEmployeeInfo.size());
			 
			 for(String department:departmentToEmployeeInfo.keySet())
			 {
				 System.out.println("Department IS ");
				 orcl.add(department);	 
			 }
	 	}*/

	    @Override
	    public EmployeeInfo getValue() 
	    {
	    	EmployeeInfo entity = null;
		    int id=-1;
		    if((this.id.getText().trim().equals(""))==false)
		       id=Integer.parseInt(this.getId().getText().trim());
		    
		    entity=idToEmployeeInfo.get(id);
		    
		    return entity;
	    }

		@Override
		public void setValue(EmployeeInfo value) 
		{
			id.setText(value.getEmpCount()+"");
			name.setValue(value.getFullName());
			phone.setText(value.getCellNumber()+"");
			department.setText(value.getDepartment());
			designation.setText(value.getDesignation());
			employeeRole.setText(value.getEmployeerole());
			employeeType.setText(value.getEmployeeType());
			branch.setText(value.getBranch());
			
			EmployeeInfo info=idToEmployeeInfo.get(value.getEmpCount());
			
			
			
		}

		@Override
		public void setValue(EmployeeInfo value, boolean fireEvents) 
		{
			
		}
	
	
		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public SuggestBox getId() {
			return id;
		}
	
		/**
		 * Sets the id.
		 *
		 * @param id the new id
		 */
		public void setId(SuggestBox id) {
			this.id = id;
		}
	
		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public SuggestBox getName() {
			return name;
		}
	
		/**
		 * Sets the name.
		 *
		 * @param name the new name
		 */
		public void setName(SuggestBox name) {
			this.name = name;
		}
	
		
		
		/**
		 * Gets the phone.
		 *
		 * @return the phone
		 */
		public SuggestBox getPhone() {
			return phone;
		}
	
		/**
		 * Sets the phone.
		 *
		 * @param phone the new phone
		 */
		public void setPhone(SuggestBox phone) {
			this.phone = phone;
		}
	
		/**
		 * Gets the centity.
		 *
		 * @return the centity
		 */
		public EmployeeInfo getEmpentity() {
			return empentity;
		}
	
		/**
		 * Sets the centity.
		 *
		 * @param centity the new centity
		 */
		public void setEmpentity(EmployeeInfo empentity) {
			this.empentity = empentity;
		}
	
		/**
		 * Gets the id to person info.
		 *
		 * @return the id to person info
		 */
		public HashMap<Integer, EmployeeInfo> getIdToEmployeeInfo() {
			return idToEmployeeInfo;
		}
	
		/**
		 * Sets the id to person info.
		 *
		 * @param idToPersonInfo the id to person info
		 */
		public void setIdToEmployeeInfo(HashMap<Integer, EmployeeInfo> idToEmployeeInfo) {
			this.idToEmployeeInfo = idToEmployeeInfo;
		}
	
		/**
		 * Gets the cell to person info.
		 *
		 * @return the cell to person info
		 */
		public HashMap<Long, EmployeeInfo> getCellToEmployeeInfo() {
			return cellToEmployeeInfo;
		}
	
		/**
		 * Sets the cell to person info.
		 *
		 * @param cellToPersonInfo the cell to person info
		 */
		public void setCellToEmployeeInfo(HashMap<Long, EmployeeInfo> cellToEmployeeInfo) {
			this.cellToEmployeeInfo = cellToEmployeeInfo;
		}
	
		/**
		 * Gets the name to person info.
		 *
		 * @return the name to person info
		 */
		public HashMap<String, EmployeeInfo> getNameToEmployeeInfo() {
			return nameToEmployeeInfo;
		}
	
		/**
		 * Sets the name to person info.
		 *
		 * @param nameToPersonInfo the name to person info
		 */
		public void setNameToEmployeeInfo(HashMap<String, EmployeeInfo> nameToEmployeeInfo) 
		{
			this.nameToEmployeeInfo = nameToEmployeeInfo;
		}
	
		/*****Department********/
		public HashMap<String, EmployeeInfo> getDepartmentToEmployeeInfo() {
			return departmentToEmployeeInfo;
		}
	
		public void setDepartmentToEmployeeInfo(HashMap<String, EmployeeInfo> departmentToEmployeeInfo) {
			this.departmentToEmployeeInfo = departmentToEmployeeInfo;
		}
	
		/********Designation******/
		
		public HashMap<String, EmployeeInfo> getDesignationToEmployeeInfo() {
			return desinationToEmployeeInfo;
		}
	
		public void setDesignationToEmployeeInfo(HashMap<String, EmployeeInfo> desinationToEmployeeInfo) {
			this.desinationToEmployeeInfo = desinationToEmployeeInfo;
		}
	
		/***********************Employee Role**************/
		
		
		public HashMap<String, EmployeeInfo> getEmployeeRoleToEmployeeInfo() {
			return employeeRoleToEmployeeInfo;
		}
	
		public void setEmployeeRoleToEmployeeInfo(
				HashMap<String, EmployeeInfo> employeeRoleToEmployeeInfo) {
			this.employeeRoleToEmployeeInfo = employeeRoleToEmployeeInfo;
		}
		
		
		/***************Employee Type********************/
	
		public HashMap<String, EmployeeInfo> getEmployeeTypeToEmployeeInfo() {
			return employeeTypeToEmployeeInfo;
		}
	
		public void setEmployeeTypeToEmployeeInfo(
				HashMap<String, EmployeeInfo> employeeTypeToEmployeeInfo) {
			this.employeeTypeToEmployeeInfo = employeeTypeToEmployeeInfo;
		}
	
		/**************Branch************************/
		public HashMap<String, EmployeeInfo> getBranchToEmployeeInfo() {
			return branchToEmployeeInfo;
		}
	
		public void setBranchToEmployeeInfo(
				HashMap<String, EmployeeInfo> branchToEmployeeInfo) {
			this.branchToEmployeeInfo = branchToEmployeeInfo;
		}
	
		/**
		 * Gets the panel.
		 *
		 * @return the panel
		 */
		public FlowPanel getPanel() {
			return panel;
		}
	
		/**
		 * Sets the panel.
		 *
		 * @param panel the new panel
		 */
		public void setPanel(FlowPanel panel) {
			this.panel = panel;
		}
	
		/**
		 * Gets the ff.
		 *
		 * @return the ff
		 */
		public FormField[][] getFf() {
			return ff;
		}
	
		/**
		 * Sets the ff.
		 *
		 * @param ff the new ff
		 */
		public void setFf(FormField[][] ff) {
			this.ff = ff;
		}
	
		/**
		 * Gets the service.
		 *
		 * @return the service
		 */
		public EmployeeInfoServiceAsync getService() {
			return service;
		}
	
		public String getEmployeeName()
		{
			return this.name.getText().trim();
		}
		
		public void setEmployeeName(String name)
		{
			this.name.setText(name);
		}
		public String getBranch()
		{
			return this.branch.getText().trim();
		}
		
		public void setBranch(String branch)
		{
			this.branch.setText(branch);
		}
		
		public String getDepartment()
		{
			return this.department.getText().trim();
		}
		
		public void setDepartment(String department)
		{
			this.department.setText(department);
		}
		public String getEmployeeType()
		{
			return this.employeeType.getText().trim();
		}
		
		public void setEmployeeType(String type)
		{
			this.employeeType.setText(type);
		}
		public String getEmpDesignation()
		{
			return this.designation.getText().trim();
		}
		public void setEmpDesignation(String designation)
		{
			this.designation.setText(designation);
		}
		
		public String getEmpRole()
		{
			return this.employeeRole.getText().trim();
		}
		public void setEmpRole(String role)
		{
			this.employeeRole.setText(role);
		}
		public Long getCellNumber()
		{
			try{
				
				if((phone.getText().trim().equals(""))!=true)
				{
				Long cell=Long.parseLong(phone.getText().trim());
				return cell;
				
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			return null;
		}
		
		public void setCellNumber(Long phone)
		{
			this.phone.setValue(phone+"");
		}
		
		public int getEmployeeId()
		{
			try{
				if((id.getText().trim().equals(""))!=true)
				{
				int cell=Integer.parseInt(id.getText().trim());
				return cell;
				
				}
				
			}
			catch(Exception e)
			{
				
			}
			
			return -1;
		}
		
		public void setEmployeeId(Integer empid)
		{
			id.setValue(empid+"");
		}
		
		private void clearAllBorderColor()
		{
			for(int i=0;i<ff.length;i++)
			{
				int col=ff[i].length;
				for(int j=0;j<col;j++)
				{
					
					if(ff[i][j].getMandatoryMsgWidget()!=null)
					    ff[i][j].getMandatoryMsgWidget().setVisible(false);
					ff[i][j].getWidget().getElement().getStyle().clearBorderColor();
				}
			}
		}

		
		
	}