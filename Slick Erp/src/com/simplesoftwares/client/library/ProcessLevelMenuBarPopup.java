package com.simplesoftwares.client.library;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.slicktechnologies.client.utils.Console;

public class ProcessLevelMenuBarPopup extends AbsolutePanel implements ClickHandler {
	
	
	VerticalPanel verticalPanel=new VerticalPanel();
	
	public ProcessLevelMenuBarPopup() {
		verticalPanel=new VerticalPanel();
		add(verticalPanel,0,0);
		setWidth("180Px");
		setHeight("100%");
		this.getElement().setId("login-form");
	}

	public VerticalPanel getVerticalPanel() {
		return verticalPanel;
	}

	public void setVerticalPanel(VerticalPanel verticalPanel) {
		this.verticalPanel = verticalPanel;
	}

	@Override
	public void onClick(ClickEvent event) {
		Console.log("PROCESS LEVEL BAR CLICKED");
	}
	
	
	
	
	
}
