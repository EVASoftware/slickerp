package com.simplesoftwares.client.library;
import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.PersonInfoComposite;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;

public class FlexForm extends FlexTable implements FocusHandler
{
	private FormField[][] formfields;
	private FormStyle formStyle;
	
	public FlexForm() 
	{
		super();
	}

	public FlexForm(FormField[][] formfields, FormStyle formStyle) 
	{
		super();
		this.formfields = formfields;
		this.formStyle = formStyle;
		if(formStyle==FormStyle.COLUMNFORM)
			this.createColumnStyleForm();
		else if(formStyle==FormStyle.ROWFORM||formStyle==FormStyle.DEFAULT||formStyle==FormStyle.TABLEFORMLAYOUT)
			 createRowForm();
		else
			createlayout();
	}

	/****************************************************************/
	public void createRowForm()
	{
		Widget widget;
		FlexCellFormatter formater=getFlexCellFormatter();
		int i=0;
		int k=0;
		int columnlength;
		
		int visibleCount=0;
		
		for( i=0;i<this.formfields.length;i++)

		{
			columnlength=this.formfields[i].length;
			visibleCount=0;
			for(int j=0;j<columnlength;j++)

			{
				widget=formfields[i][j].getWidget();
				
				if(formfields[i][j].isVisible()==false){
					visibleCount++;
					continue;
				}
				 
				if(formfields[i][j]!=null)
				{
				   InlineLabel lbl=new InlineLabel(formfields[i][j].getLabel());
				   formfields[i][j].setHeaderLabel(lbl);
				   setWidget(k,j-visibleCount,lbl);
				   widget=formfields[i][j].getWidget();
				

				   /**
					 * Date : 06-02-2017 By Anil
					 * This code set the width of list box labeled with title warehouse
					 */
					if(widget instanceof ListBox){
						String title=lbl.getText();
						if(title.contains("Warehouse")){
							widget.getElement().getStyle().setWidth(300,Unit.PX);
						}
					}
					
					/**
					 * End
					 */
				
				   if(widget!=null)
					   setWidget(k+1,j-visibleCount,widget);
				   boolean mandatory=formfields[i][j].isMandatory();
				   if(mandatory==true)
				   {
					   InlineLabel msglabel=formfields[i][j].getMandatoryMsgWidget();
					   if(msglabel!=null)
					   {
						   setWidget(k+2, j-visibleCount, msglabel);
						   msglabel.setVisible(false);
					   }
				   }
						   
				   if(formfields[i][j].getWidgetType()==FieldType.Grouping)
				   {
					   RowFormatter format=this.getRowFormatter();
					   format.addStyleName(k,"grouping");
				   }
				   
				   /**
				    * Date : 23-05-2017 BY ANIL
				    */
				   if(formfields[i][j].getWidgetType()==FieldType.GREYGROUPING)
				   {
					   RowFormatter format=this.getRowFormatter();
					   format.addStyleName(k,"greygrouping");
				   }
				   /**
				    * End
				    */
				 //komal
				   if(formfields[i][j].getWidgetType()== FieldType.HALFGROUPING){
					   
					 //  CellFormatter format=this.getCellFormatter();
					   RowFormatter format=this.getRowFormatter();
					   format.addStyleName(k,"blankGrouping");
				   }
				}
			 }
			 k=k+3;
		}
		
		k=0;
		for( i=0;i<this.formfields.length;i++)
		{
			columnlength=this.formfields[i].length;
			for(int j=0;j<columnlength;j++)
			{
				if(formfields[i][j]!=null)
				{
					int rowspan=formfields[i][j].getRowspan();
					int colspan=formfields[i][j].getColspan();
				 /**
				  *  nidhi
				  *  21-03-2018 
				  *  for ui changes for new chrome updation	
				  */
//					formater.setRowSpan(k+1, j, rowspan);
					formater.setColSpan(k+1, j,colspan);
				    formater.setColSpan(k, j,colspan);
				    formater.setColSpan(k+2 , j , colspan);//komal
				}
			}
			k=k+3;
		}
		
		applyFocusHandler();
	}
	
	/**************************************************************************/
	private void createColumnStyleForm()
	{
		FlexCellFormatter formater=getFlexCellFormatter();
		int k=0;
		int colCount = formfields[0].length;
		int rowCount = formfields.length;
		for(int i=0;i<colCount;i++)
		{
			
			for(int j=0;j<rowCount;j++)
			{
				
				
				if(i<formfields[j].length&&formfields[j][i]!=null)
				{
					
				InlineLabel lbl=new InlineLabel(formfields[j][i].getLabel());
				setWidget(j, k, lbl);
				Widget widg=formfields[j][i].getWidget();
				setWidget(j, k+1, widg);
				
				
				}
			}
			k=k+2;
			
			
		}
		k=0;
		for(int i=0;i<colCount;i++)
		{
			
			for(int j=0;j<rowCount;j++)
			{
				
			
				if(i<formfields[j].length&&formfields[j][i]!=null)
				{
					
				  int rowSpan= formfields[j][i].getRowspan();
				  int colSpan =formfields[j][i].getColspan();
				  
				 // setting rowspan to label
				  formater.setRowSpan(j, k, rowSpan);
				  
				  
				// setting rowspan  colspan to widgets
				  formater.setRowSpan(j, k+1, rowSpan);
				  formater.setColSpan(j, k+1, colSpan);
				 
				  
				 
				}
			}
			k=k+2;
			
		}
		
		
	
		
	}
	
	
	public void createlayout()
	{
		Widget widgets;
		FlexCellFormatter formater=getFlexCellFormatter();
		int i=0;
		
		int columnlength;
		for( i=0;i<this.formfields.length;i++)
		{
			
			columnlength=this.formfields[i].length;
			for(int j=0;j<columnlength;j++)
			{
				if(formfields[i][j]!=null)
				{
				
				InlineLabel lbl=new InlineLabel(formfields[i][j].getLabel());
				setWidget(i,j,lbl);
				widgets=formfields[i][j].getWidget();
				setWidget(i,j,widgets);
				
				
				widgets.setHeight("90%");
				widgets.setWidth("90%");
				}
				
			
		}
			
		
	}
		
	
		for( i=0;i<this.formfields.length;i++)
		{
			 columnlength=this.formfields[i].length;
			for(int j=0;j<columnlength;j++)
			{
				if(formfields[i][j]!=null)
				{
				int rowspan=formfields[i][j].getRowspan();
				int colspan=formfields[i][j].getColspan();
				formater.setRowSpan(i, j, rowspan);
				formater.setColSpan(i, j,colspan);
				}
			}
			
		}
	}
	
	public void clear()
	{
		Console.log("INSIDE Clearing...");
		int noRow = getRowCount();
		for(int k=0;k<noRow;k++)
		{
			int colCount = this.getCellCount(k);
			for(int j=0;j<colCount;j++)
			{
				
					
				Widget widg= this.getWidget(k,j);
				if(widg!=null)
				{
					widg.getElement().getStyle().clearBackgroundColor();
				}
				
				if(widg instanceof TextBox)
				{
					TextBox vb=(TextBox) widg;
					vb.setText("");
					resetBorderColor(widg);
				}
				
				if(widg instanceof IntegerBox)
				{
					IntegerBox ib=(IntegerBox) widg;
					ib.setText("");
					resetBorderColor(widg);
				}
				
				if(widg instanceof CheckBox)
				{
					CheckBox vb=(CheckBox) widg;
					vb.setValue(true);
					resetBorderColor(widg);
				}
				if(widg instanceof TextArea)
				{
					TextArea vb=(TextArea) widg;
					vb.setText("");
					resetBorderColor(widg);
				}
				
				if(widg instanceof LongBox)
				{
					LongBox vb=(LongBox) widg;
					vb.setText("");
					resetBorderColor(widg);
				}
				if(widg instanceof MyLongBox)
				{
					MyLongBox vb=(MyLongBox) widg;
					vb.setText("");
					resetBorderColor(widg);
				}
				if(widg instanceof DateBox)
				{
					DateBox vb=(DateBox) widg;
					vb.getTextBox().setText("");
					/**
					 * @author Anil , Date : 16-01-2020
					 * Datbox same date selection issue
					 */
					vb.setValue(null);
					resetBorderColor(vb.getTextBox());
				}
				
				if(widg instanceof ListBox)
				{
					ListBox vb=(ListBox) widg;
					vb.setSelectedIndex(0);
					resetBorderColor(widg);
				}
				if(widg instanceof ObjectListBox)
				{
					ListBox vb=(ListBox) widg;
					vb.setSelectedIndex(0);
					resetBorderColor(widg);
				}
				if(widg instanceof SuggestBox)
				{
					SuggestBox vb=(SuggestBox) widg;
					vb.setText("");
					resetBorderColor(widg);
				}
				
				if(widg instanceof MyLongBox)
				{
					MyLongBox mb=(MyLongBox) widg;
					mb.setText("");
					resetBorderColor(widg);
				}
				
				if(widg instanceof DoubleBox)
				{
					DoubleBox mb=(DoubleBox) widg;
					mb.setText("");
					mb.setValue(null);
					resetBorderColor(widg);
				}
				
				if(widg instanceof IntegerBox)
				{
					IntegerBox mb=(IntegerBox) widg;
					mb.setText("");
					resetBorderColor(widg);
				}
				
				if(widg instanceof CompositeInterface)
				{
					CompositeInterface a= (CompositeInterface) widg;
					a.clear();
					resetBorderColor((Widget) a);
				}
				
				/**
				 * @author Anil,Date : 08-02-2019
				 * At the time of data view ,we clear all data from previous and then map current selected data
				 * it was not done for upload composite,that why document mismatch issue was happening for SASHA
				 */
				if(widg instanceof UploadComposite)
				{
					UploadComposite mb=(UploadComposite) widg;
					mb.setValue(null);
					mb.clear();
					resetBorderColor(widg);
				}
				
			}
		}
		
		int rowCount=this.formfields.length;
		for(int x=0;x<rowCount;x++)
		{
			int temp=formfields[x].length;
			for(int j=0;j<temp;j++)
			{
				FormField tempFormField=formfields[x][j];
				
				
				if(tempFormField!=null)
				{
					if(tempFormField.getMandatoryMsgWidget()!=null)
					{
						
						tempFormField.getMandatoryMsgWidget().setVisible(false);
						if(tempFormField!=null&&tempFormField.getWidget()!=null)
						   resetBorderColor(tempFormField.getWidget());
					}
					
					/***
					 * @author Anil
					 * @Since 04-01-2021
					 */
					
					if(tempFormField.getWidget() instanceof UploadComposite)
					{
						UploadComposite mb=(UploadComposite) tempFormField.getWidget();
						mb.setValue(null);
						mb.clear();
						resetBorderColor(tempFormField.getWidget());
					}
					
					/**
					 * @author Anil
					 * @since 29-01-2021
					 * clearing widget issue on simplified screens
					 */
					if(tempFormField.getWidget() instanceof DoubleBox)
					{
						DoubleBox mb=(DoubleBox) tempFormField.getWidget();
						mb.setText("");
						mb.setValue(null);
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof TextBox)
					{
						TextBox vb=(TextBox) tempFormField.getWidget();
						vb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof IntegerBox)
					{
						IntegerBox ib=(IntegerBox) tempFormField.getWidget();
						ib.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof CheckBox)
					{
						CheckBox vb=(CheckBox) tempFormField.getWidget();
						vb.setValue(false);
						resetBorderColor(tempFormField.getWidget());
					}
					if(tempFormField.getWidget() instanceof TextArea)
					{
						TextArea vb=(TextArea) tempFormField.getWidget();
						vb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof LongBox)
					{
						LongBox vb=(LongBox) tempFormField.getWidget();
						vb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					if(tempFormField.getWidget() instanceof MyLongBox)
					{
						MyLongBox vb=(MyLongBox) tempFormField.getWidget();
						vb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					if(tempFormField.getWidget() instanceof DateBox)
					{
						DateBox vb=(DateBox) tempFormField.getWidget();
						vb.getTextBox().setText("");
						/**
						 * @author Anil , Date : 16-01-2020
						 * Datbox same date selection issue
						 */
						vb.setValue(null);
						resetBorderColor(vb.getTextBox());
					}
					
					if(tempFormField.getWidget() instanceof ListBox)
					{
						ListBox vb=(ListBox) tempFormField.getWidget();
						vb.setSelectedIndex(0);
						resetBorderColor(tempFormField.getWidget());
					}
					if(tempFormField.getWidget() instanceof ObjectListBox)
					{
						ListBox vb=(ListBox) tempFormField.getWidget();
						vb.setSelectedIndex(0);
						resetBorderColor(tempFormField.getWidget());
					}
					if(tempFormField.getWidget() instanceof SuggestBox)
					{
						SuggestBox vb=(SuggestBox) tempFormField.getWidget();
						vb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof MyLongBox)
					{
						MyLongBox mb=(MyLongBox) tempFormField.getWidget();
						mb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof IntegerBox)
					{
						IntegerBox mb=(IntegerBox) tempFormField.getWidget();
						mb.setText("");
						resetBorderColor(tempFormField.getWidget());
					}
					
					if(tempFormField.getWidget() instanceof CompositeInterface)
					{
						CompositeInterface a= (CompositeInterface) tempFormField.getWidget();
						a.clear();
						resetBorderColor((Widget) a);
					}
					
				}
			}
		
		}
		
	}
	
	public boolean validate()
	{
		
		
		boolean result=true;
		int row=this.formfields.length;
		
		for(int k=0;k<row;k++)
		{
			int column=formfields[k].length;
			
			for(int i=0;i<column;i++)
			{
				Widget widg=formfields[k][i].getWidget();
				
				if(formfields[k][i].getLabel()!=null&&formfields[k][i].getLabel().contains("Email"))
				{
					if(widg instanceof TextBox){
						
					
					TextBox tb=(TextBox) widg;
					String value=tb.getText();
					
					boolean result1=validateEmail(value.trim());

					if(result1==false)
				    {
				    	if(tb.getText().trim().equals("")==false)
				    	{
				    		result=false;
							changeBorderColor(widg,"#dd4b39");
							
							
							
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
							{
								l.setVisible(true);
								formfields[k][i].getMandatoryMsgWidget().setText("Invalid Email Format!");
				    	}}
				     }	
					
					}
				}
				
				if(widg instanceof CompositeInterface)
				{
					
					CompositeInterface a= (CompositeInterface) widg;
					boolean resultadress=a.validate();
					if(resultadress==false)
						result=resultadress;	
				}
				
				if(formfields[k][i].isMandatory())
				{
					if(widg instanceof TextBox)
					{
						TextBox tb=(TextBox) widg;
						String value=tb.getText();
						
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
						
					}
					
					
					if(widg instanceof TextArea)
					{
						TextArea tb=(TextArea) widg;
						String value=tb.getText();
						
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
						
						
						
					}
					
					else if(widg instanceof ListBox)
					{
						ListBox lb=(ListBox) widg;
					
						int index=lb.getSelectedIndex();
						if(index==0)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					else if(widg instanceof ObjectListBox)
					{
						ListBox lb=(ListBox) widg;
						
						int index=lb.getSelectedIndex();
						if(index==0)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					else if(widg instanceof DateBox || widg instanceof DateBoxWithYearSelector)
					{
						DateBox db=(DateBox) widg;
						Date value=db.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					if(widg instanceof DoubleBox)
					{
						DoubleBox db=(DoubleBox) widg;
						String value=db.getText().trim();
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
							
						}
					}
					
					if(widg instanceof SuggestBox)
					{
						SuggestBox sb=(SuggestBox) widg;
						String value=sb.getValue();
						
						if(value.equals(""))
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					if(widg instanceof LongBox)
					{
						LongBox sb=(LongBox) widg;
						Long value=sb.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
				
					if(widg instanceof MyLongBox)
					{
						MyLongBox sb=(MyLongBox) widg;
						
						Long value=sb.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
					
					
					if(widg instanceof IntegerBox)
					{
						IntegerBox sb=(IntegerBox) widg;
						
						Integer value=sb.getValue();
						if(value==null)
						{
							result=false;
							changeBorderColor(widg,"#dd4b39");
							InlineLabel l=formfields[k][i].getMandatoryMsgWidget();
							if(l!=null)
								l.setVisible(true);
						}
					}
				}
			}
		}
		return result;
	}
	
	public void applyFocusHandler()
	{
		int row=this.formfields.length;
		
		for(int k=0;k<row;k++)
		{
			int column=formfields[k].length;
			
			for(int i=0;i<column;i++)
			{
				//apply FocusHandler on Email whether it is mandatory or not (format validation) 
				Widget widg1=formfields[k][i].getWidget();
				if(widg1 instanceof TextBox)
				{
					if(formfields[k][i].getLabel().contains("Email"))
						((TextBox) widg1).addFocusHandler(this);
				}
				
				//apply FocusHandler on mandatory fields only.
				if(formfields[k][i].isMandatory())
				{
					Widget widget=formfields[k][i].getWidget();
					
					if(widget instanceof TextBox)
					{
						TextBox tb=(TextBox) widget;
						tb.addFocusHandler(this);
					}
					
					if(widget instanceof ListBox)
					{
						ListBox lb=(ListBox) widget;
						lb.addFocusHandler(this);
					}
					

					if(widget instanceof DoubleBox)
					{
						DoubleBox db=(DoubleBox) widget;
						db.addFocusHandler(this);
					}
					//since FocusHandler can't be apply on SuggestBox
					//we put it on it's ValueBox
					if(widget instanceof SuggestBox)
					{
						SuggestBox sb=(SuggestBox) widget;
						sb.getValueBox().addFocusHandler(this);
					}
				
					if(widget instanceof LongBox)
					{
						LongBox lb=(LongBox) widget;
						lb.addFocusHandler(this);
					}
					if(widget instanceof DoubleBox)
					{
						DoubleBox db=(DoubleBox) widget;
						db.addFocusHandler(this);
					}
					
					if(widget instanceof MyLongBox)
					{
						MyLongBox mb=(MyLongBox) widget;
						mb.addFocusHandler(this);
					}
					
					if(widget instanceof DateBox)
					{
						DateBox date=(DateBox) widget;
						date.getTextBox().addFocusHandler(this);
						
					}
					
					if(widget instanceof IntegerBox)
					{
						IntegerBox date=(IntegerBox) widget;
						date.addFocusHandler(this);
						
					}
					
					if(widget instanceof TextArea)
					{
						TextArea textArea=(TextArea) widget;
						textArea.addFocusHandler(this);
					}
				}
			}
		}
	}
	
	public void setEnabled(boolean status)
	{
		int row=this.formfields.length;
		for(int k=0;k<row;k++)
		{
			int column=formfields[k].length;
			for(int i=0;i<column;i++)
			{
				Widget widg=formfields[k][i].getWidget();
				if(widg instanceof TextBox)
				{
					TextBox tb=(TextBox) widg;
					tb.setEnabled(status);
				}
				
				else if(widg instanceof ListBox)
				{
					ListBox lb=(ListBox) widg;
					lb.setEnabled(status);
				}
				
				else if(widg instanceof SuggestBox)
				{
					SuggestBox sb=(SuggestBox) widg;
					sb.setEnabled(status);
				}
			
				else if(widg instanceof LongBox)
				{
					LongBox lb=(LongBox) widg;
					lb.setEnabled(status);
				}
				
				else if(widg instanceof MyLongBox)
				{
					MyLongBox mb=(MyLongBox) widg;
					mb.setEnabled(status);
				}
				
				else if(widg instanceof IntegerBox)
				{
					IntegerBox sb=(IntegerBox) widg;
					sb.setEnabled(status);
				}
				
				else if(widg instanceof DoubleBox)
				{
					DoubleBox sb=(DoubleBox) widg;
					sb.setEnabled(status);
				}
				
				else if(widg instanceof TextArea)
				{
					TextArea sb=(TextArea) widg;
					sb.setEnabled(status);
				}
				
				
				
				else if(widg instanceof CheckBox)
				{
					CheckBox c=(CheckBox) widg;
					c.setEnabled(status);
				}
				
			
				else if(widg instanceof DateBox)
				{
					DateBox comp=(DateBox) widg;    
					comp.setEnabled(status);
				}
				
				
				
				
				else if(widg instanceof Button)
				{
					Button comp=(Button) widg;
					comp.setEnabled(status);
				}
				
				else if(widg instanceof CompositeInterface)
				{
					CompositeInterface comp=(CompositeInterface) widg;
					comp.setEnable(status);
				}
				
				
				
			}
		}
	}
	
	
	
	
/*************************************************************/
	public static enum FormStyle{
		ROWFORM,COLUMNFORM,LAYOUT,DEFAULT,TABLEFORMLAYOUT;
	}


     private void resetBorderColor(Widget widg)
     {
    	 widg.getElement().getStyle().clearBorderColor();
    	
     }
     
     private void changeBorderColor(Widget widg,String color)
     {
    	 widg.getElement().getStyle().setBorderColor(color);
     }

	
	public static boolean validateEmail(String email)
	{
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		return email.matches(EMAIL_PATTERN);     
	}
	
	@Override
	public void onFocus(FocusEvent event) 
	{
		int row=this.formfields.length;
		
		for(int k=0;k<row;k++)
		{
			int column=formfields[k].length;
		
			for(int i=0;i<column;i++)
			{
				Widget widget=formfields[k][i].getWidget();
				
				if(event.getSource().equals(widget) && formfields[k][i].isMandatory()==true)
				{

					if(formfields[k][i].getMandatoryMsgWidget()!=null)
						formfields[k][i].getMandatoryMsgWidget().setVisible(false);
					resetBorderColor(widget);
					return;
				}
				
				//SuggestBox does't implement FocusHandler
				else if(widget instanceof SuggestBox)
				{
					SuggestBox sb=(SuggestBox) widget;
					if(event.getSource().equals(sb.getValueBox()))
					{
						resetBorderColor(sb);
						if(formfields[k][i].getMandatoryMsgWidget()!=null)
							formfields[k][i].getMandatoryMsgWidget().setVisible(false);
						return;
					}
				}
				
				else if(widget instanceof DateBox)
				{
					DateBox sb=(DateBox) widget;
					if(event.getSource().equals(sb.getTextBox()))
					{
						resetBorderColor(sb);
						if(formfields[k][i].getMandatoryMsgWidget()!=null)
							formfields[k][i].getMandatoryMsgWidget().setVisible(false);
						return;
					}
				}
			}
		}
	} 
	
	/**
	 * @author Anil , Date : 02-04-2020
	 * This method returns false if listbox is not loaded
	 */
	public boolean validateSearchPopupListFields() {
		
		boolean result = true;
		int row = this.formfields.length;

		for (int k = 0; k < row; k++) {
			int column = formfields[k].length;

			for (int i = 0; i < column; i++) {
				Widget widg = formfields[k][i].getWidget();

				if (widg instanceof ListBox) {
					ListBox lb = (ListBox) widg;
					if(lb.getItemCount()==0){
						return false;
					}
//					int index = lb.getSelectedIndex();
//					if (index == 0) {
//						result = false;
//						changeBorderColor(widg, "#dd4b39");
//						InlineLabel l = formfields[k][i].getMandatoryMsgWidget();
//						if (l != null)
//							l.setVisible(true);
//					}
				}

				else if (widg instanceof ObjectListBox) {
					ListBox lb = (ListBox) widg;
					if(lb.getItemCount()==0){
						return false;
					}
//					int index = lb.getSelectedIndex();
//					if (index == 0) {
//						result = false;
//						changeBorderColor(widg, "#dd4b39");
//						InlineLabel l = formfields[k][i].getMandatoryMsgWidget();
//						if (l != null)
//							l.setVisible(true);
//					}
				}

			}
		}
		return result;
	}
	
//	private boolean validateDate(Date value, String strTextDate, DateBox db) {
//		
//		if(strTextDate.length()>10 || strTextDate.length()<10 ){
//			Console.log("strDateValueDD == "+strTextDate.length());
//			return true;
//		}
//		
//		try {
//		DateTimeFormat dateformat = DateTimeFormat.getFormat("dd-MM-yyyy");
//		String strDate = dateformat.format(value);
//		Console.log("strDate"+strDate);
//		
//		String[] straarayDate = strDate.split("\\-");
//		String strDateValueDD = straarayDate[0];
//		
//		String strDateMMValue = straarayDate[1];
//
//
//		String[] strArrayTextDate = strTextDate.split("\\-");
//		String strTextDateValueDD = strArrayTextDate[0];
//		
//		String strTextMMValue = strArrayTextDate[1];
//		
//		Console.log("strDateValueDD"+strDateValueDD);
//		Console.log("strTextDateValueDD "+strTextDateValueDD);
//
//		if(!strDateValueDD.equals(strTextDateValueDD)){
//			Console.log("strDateValueDD == $$"+strTextDate.length());
//			return true;
//		}
//		
//		Console.log("strDateMMValue == "+strDateMMValue);
//		Console.log("strTextMMValue == "+strTextMMValue);
//
//		if(!strDateMMValue.equals(strTextMMValue)){
//			Console.log("strTextMMValue == "+strTextMMValue);
//			return true;
//		}
//		
//		String strarraydate[] = strDate.split("\\-");
//		Console.log("1 = "+strarraydate[0].trim().matches("[0-9]+"));
//		Console.log("2 = "+strarraydate[1].trim().matches("[0-9]+"));
//		Console.log("3 = "+strarraydate[2].trim().matches("[0-9]+"));
//
//		
//		if(!strarraydate[0].trim().matches("[0-9]+") || 
//				!strarraydate[1].trim().matches("[0-9]+") ||
//				!strarraydate[2].trim().matches("[0-9]+")){
//			Console.log("strDateValueDD == $$ 111 = "+strTextDate.length());
//
//			return true;
//		}
//		if(Integer.parseInt(strarraydate[1].trim())<=0 || Integer.parseInt(strarraydate[1].trim())>12 ) {
//			Console.log("Integer.parseInt(strarraydate[1].trim()) "+Integer.parseInt(strarraydate[1].trim()));
//			return true;
//		}
//		
//		} catch (Exception e) {
//			Console.log("Exception = "+e.getMessage());
//			return true;
//		}
//		
//		
//		return false;
//	}
//	
//	private boolean validateMonthDate(Date value, String strTextDate, DateBox db) {
//
//		DateTimeFormat dateformat = DateTimeFormat.getFormat("yyyy-MMM");
//		String strDate = dateformat.format(value);
//		Console.log("strDate"+strDate);
//		
//		String strarraydate[] = strDate.split("\\-");
//		Console.log("1 = "+strarraydate[0].trim().matches("[0-9]+"));
//		Console.log("2 = "+strarraydate[1].trim());
//		if(strTextDate.length()>8 || strTextDate.length()<8 || !strarraydate[0].trim().matches("[0-9]+") 
//								  || !checkMonth(strarraydate[1].trim())){
//			Console.log("strDateValueDD == $$ 111 = "+strTextDate.length());
//			return true;
//		}
//		
//		return false;
//	}
//
//	private boolean checkMonth(String DateMonthName) {
//		
//		if(DateMonthName.toUpperCase().equals("JAN") ||
//				DateMonthName.toUpperCase().equals("FEB") ||
//				DateMonthName.toUpperCase().equals("MAR") ||
//				DateMonthName.toUpperCase().equals("APR") ||
//				DateMonthName.toUpperCase().equals("MAY") ||
//				DateMonthName.toUpperCase().equals("JUN") ||
//				DateMonthName.toUpperCase().equals("JUL") ||
//				DateMonthName.toUpperCase().equals("AUG") ||
//				DateMonthName.toUpperCase().equals("SEP") ||
//				DateMonthName.toUpperCase().equals("OCT") ||
//				DateMonthName.toUpperCase().equals("NOV") ||
//				DateMonthName.toUpperCase().equals("DEC")){
//			Console.log("DateMonthName"+DateMonthName);
//			return true;
//		}
//		return false;
//	}
	
	public boolean isDataPresent(){
		Console.log("INSIDE isDataPresent...");
		int noRow = getRowCount();
		for (int k = 0; k < noRow; k++) {
			int colCount = this.getCellCount(k);
			for (int j = 0; j < colCount; j++) {

				Widget widg = this.getWidget(k, j);
				Console.log("Console.log 1");
				if (widg instanceof TextBox) {
					TextBox vb = (TextBox) widg;
					if(!vb.getValue().equals("")){
						return true;
					}
				}
				Console.log("Console.log 2");
				if (widg instanceof IntegerBox) {
					IntegerBox ib = (IntegerBox) widg;
					if(ib.getValue()!=null){
						return true;
					}
				}
				Console.log("Console.log 3");
				if (widg instanceof LongBox) {
					LongBox vb = (LongBox) widg;
					if(vb.getValue()!=null){
						return true;
					}
				}
				Console.log("Console.log 4");
				if (widg instanceof MyLongBox) {
					MyLongBox vb = (MyLongBox) widg;
					if(vb.getValue()!=null){
						return true;
					}
				}
				Console.log("Console.log 5");
				if (widg instanceof DateBox) {
					DateBox vb = (DateBox) widg;
					if(vb.getValue()!=null){
						return true;
					}
				}
				Console.log("Console.log 6");
				if (widg instanceof ListBox) {
					ListBox vb = (ListBox) widg;
					if(vb.getSelectedIndex()!=0){
						return true;
					}
				}
				Console.log("Console.log 7");
				if (widg instanceof ObjectListBox) {
					ListBox vb = (ListBox) widg;
					if(vb.getSelectedIndex()!=0){
						return true;
					}
				}
				Console.log("Console.log 8");
				if (widg instanceof SuggestBox) {
					SuggestBox vb = (SuggestBox) widg;
					if(vb.getValue()!=null&&!vb.getValue().equals("")){
						return true;
					}
				}
				Console.log("Console.log 9");
				if (widg instanceof DoubleBox) {
					DoubleBox mb = (DoubleBox) widg;
					if(mb.getValue()!=null){
						return true;
					}
				}
				Console.log("Console.log 10");
				if (widg instanceof CompositeInterface) {
					CompositeInterface a = (CompositeInterface) widg;
					if(a instanceof PersonInfoComposite){
						if(((PersonInfoComposite) a).getValue()!=null){
							return true;
						}
					}
					if(a instanceof ProductInfoComposite){
						if(((ProductInfoComposite) a).getValue()!=null){
							return true;
						}
					}
				}
			}
		}
		
		Console.log("Console.log 11");

		int rowCount = this.formfields.length;
		for (int x = 0; x < rowCount; x++) {
			int temp = formfields[x].length;
			for (int j = 0; j < temp; j++) {
				FormField tempFormField = formfields[x][j];

				if (tempFormField != null) {

					if (tempFormField.getWidget() instanceof DoubleBox) {
						DoubleBox mb = (DoubleBox) tempFormField.getWidget();
						if(mb.getValue()!=null){
							return true;
						}
					}
					Console.log("Console.log 12");

					if (tempFormField.getWidget() instanceof TextBox) {
						TextBox vb = (TextBox) tempFormField.getWidget();
						if(vb.getValue()!=null&&!vb.getValue().equals("")){
							return true;
						}
					}
					
					Console.log("Console.log 13");

					if (tempFormField.getWidget() instanceof IntegerBox) {
						IntegerBox ib = (IntegerBox) tempFormField.getWidget();
						if(ib.getValue()!=null){
							return true;
						}
					}
					Console.log("Console.log 14");
					if (tempFormField.getWidget() instanceof LongBox) {
						LongBox vb = (LongBox) tempFormField.getWidget();
						if(vb.getValue()!=null){
							return true;
						}
					}
					Console.log("Console.log 15");
					if (tempFormField.getWidget() instanceof MyLongBox) {
						MyLongBox vb = (MyLongBox) tempFormField.getWidget();
						if(vb.getValue()!=null){
							return true;
						}
					}
					Console.log("Console.log 16");
					if (tempFormField.getWidget() instanceof DateBox) {
						DateBox vb = (DateBox) tempFormField.getWidget();
						if(vb.getValue()!=null){
							return true;
						}
					}
					Console.log("Console.log 17");
					if (tempFormField.getWidget() instanceof ListBox) {
						ListBox vb = (ListBox) tempFormField.getWidget();
						if(vb.getSelectedIndex()!=0){
							return true;
						}
					}
					Console.log("Console.log 18");
					if (tempFormField.getWidget() instanceof ObjectListBox) {
						ListBox vb = (ListBox) tempFormField.getWidget();
						if(vb.getSelectedIndex()!=0){
							return true;
						}
					}
					Console.log("Console.log 19");
					if (tempFormField.getWidget() instanceof SuggestBox) {
						SuggestBox vb = (SuggestBox) tempFormField.getWidget();
						if(vb.getValue()!=null&&!vb.getValue().equals("")){
							return true;
						}
					}
					Console.log("Console.log 20");
					if (tempFormField.getWidget() instanceof CompositeInterface) {
						CompositeInterface a = (CompositeInterface) tempFormField.getWidget();
						
						if(a instanceof PersonInfoComposite){
							if(((PersonInfoComposite) a).getValue()!=null){
								return true;
							}
						}
						if(a instanceof ProductInfoComposite){
							if(((ProductInfoComposite) a).getValue()!=null){
								return true;
							}
						}
					}
					Console.log("Console.log 21");
				}
			}

		}
		return false;
	}

}
