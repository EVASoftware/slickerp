package com.simplesoftwares.client.library.appstructure;


import com.google.gwt.user.client.Timer;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsForm;
import com.slicktechnologies.client.views.customerlog.CustomerLogDetailsPopup;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerlog.CustomerLogDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.Locality;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

/**
 * This class applies selection model on Super Table.
 * Since it also implements {@link SelectionChangeEvent.Handler} so we need not apply
 * Handler from outside.
 * On click of table row any form get filled , The functionality comes from this class.
 * @author Kamala
 * @param <T>
 */

public class TableSelection<T> extends NoSelectionModel<T> implements  SelectionChangeEvent.Handler
{
  protected UiScreen<T> mv;
	
	/**
	 * Zero args constructor
	 */
   public TableSelection()
	{
		super();
	}
   /**
    * 
    * @param mv Ui screen which will get filled on row click.
    * @param keyProvider Key Provider object
    */
	
	public TableSelection(UiScreen<T> mv,ProvidesKey<T>keyProvider)
	{
		super(keyProvider);
		this.mv=mv;
		addSelectionChangeHandler(this);
	}
	
	/**
	 * Crucial method provides functionality of view filling on row click.
	 * To Do: Need to exactly decide weather click is on pop up table or in normal table.
	 * @author Kamala
	 * 
	 */
	@Override
	public void onSelectionChange(SelectionChangeEvent event) 
	{	
		// Get the clicked object
		T obj=TableSelection.this.getLastSelectedObject();
		//Type cast in super model(if the obj is not type of supermodel functionality will break)
		
        SuperModel model=(SuperModel) obj;
        /** date 6.7.2018 added by komal for log details popup **/
    	if(obj instanceof CustomerLogDetails){
    		final CustomerLogDetailsForm form = (CustomerLogDetailsForm) mv;
    		final CustomerLogDetailsPopup popup = new CustomerLogDetailsPopup();
    		final CustomerLogDetails log=(CustomerLogDetails) obj;
    		Timer timer = new Timer() {
    			@Override
    			public void run() {
    				popup.showPopUp();
    	    		popup.setEnable(false);
    	    		form.setPopupValueForView(log  , popup);
    	    		if(mv.getSearchpopupscreen()!=null)
    	    		{
    	    			System.out.println("MMMMMMMMMMMMMMMMMMMMMMMM"+mv.getSearchpopupscreen());
    	    			mv.getSearchpopupscreen().hidePopUp();	 
    	    			
    	    		}
    			}
    		};
    		timer.schedule(2000);
    		
    		
    		return;
    	}
    	/**
    	 * end komal
    	 */
        if(mv==null)
        	return;
        mv.getPresenter().setModel(model);
        //Clears the screen
		mv.clear();
		
		/**
		 * Date 17-04-2018 By vijay
		 * for orion pest with process config
		 * Des :- here we are adding locality and city to global list from an entity. first
		 * i am checking locality and city exist in global or not, if doesnt exit then added into global list
		 * for loading address composite address drop down data
		 */
		if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("Locality", "DontLoadAllLocalityAndCity")){
			updateCityAndLocalityToGlobalList(model);
		}
		
		/**
		 * ends here
		 */
		
		//updates the view 
		mv.updateView(obj);
		mv.setToViewState();
		
		//If row is click is in pop up we need to hide it
		if(mv.getSearchpopupscreen()!=null)
		{
			System.out.println("MMMMMMMMMMMMMMMMMMMMMMMM"+mv.getSearchpopupscreen());
			mv.getSearchpopupscreen().hidePopUp();	 
			
		}
	}
	
	/**
	 * Date 17-04-2018 By vijay
	 * for orion pest with process config
	 * Des :- here we are adding locality and city to global list from an entity. first
	 * i am checking locality and city exist in global or not, if doesnt exit then added into global list
	 * for loading address composite address drop down data
	 */
	
	private void updateCityAndLocalityToGlobalList(SuperModel model) {
		System.out.println("hi vijay update global list from entity");
		if(model instanceof Customer){
			Customer customer = (Customer) model;
			System.out.println("Cuty size --"+LoginPresenter.globalCity.size());
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(customer.getAdress().getLocality());
				locality.setCityName(customer.getAdress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
			}
			boolean flag = true;
			for(int j=0;j<LoginPresenter.globalLocality.size();j++){
				if(customer.getAdress().getLocality()!=null && !customer.getAdress().getLocality().equals("") ){
					if(LoginPresenter.globalLocality.get(j).getLocality().equals(customer.getAdress().getLocality())){
						System.out.println(" hi vijiii  33");
						flag = false;
						break;
					}
				}
				
			}	
			
			if(flag){
				Locality locality = new Locality();
				locality.setLocality(customer.getAdress().getLocality());
				locality.setCityName(customer.getAdress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				System.out.println("3333333333");

			}else{
				flag = true;
			}
			
			for(int j=0;j<LoginPresenter.globalLocality.size();j++){
				
				if(customer.getSecondaryAdress().getLocality()!=null && !customer.getSecondaryAdress().getLocality().equals("") ){
					if(!customer.getSecondaryAdress().getLocality().equals(customer.getAdress().getLocality())){
						if(LoginPresenter.globalLocality.get(j).getLocality().equals(customer.getSecondaryAdress().getLocality())){
							System.out.println(" hi vijiii  44");
							flag = false;
							break;
						}
					}else{
						flag = false;
						break;	
					}
					
				}
			}

			if(flag){
				Locality locality = new Locality();
				locality.setLocality(customer.getSecondaryAdress().getLocality());
				locality.setCityName(customer.getSecondaryAdress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				System.out.println("44444444444");

			}
			
			System.out.println("customer done here");
		}	
		else if(model instanceof Vendor){
			Vendor vendor = (Vendor) model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(vendor.getPrimaryAddress().getLocality());
				locality.setCityName(vendor.getPrimaryAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
			}
			boolean flag = true;
			
			for(int i=0;i<LoginPresenter.globalLocality.size();i++){
				if(vendor.getPrimaryAddress().getLocality()!=null && !vendor.getPrimaryAddress().getLocality().equals("") ){
					if(vendor.getPrimaryAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
						flag = false;
						break;
					}
				}
			}	
			
			if(flag){
				Locality locality = new Locality();
				locality.setLocality(vendor.getPrimaryAddress().getLocality());
				locality.setCityName(vendor.getPrimaryAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
			}else{
				flag = true;
			}
			
			for(int i=0;i<LoginPresenter.globalLocality.size();i++){
				if(vendor.getSecondaryAddress().getLocality()!=null && !vendor.getSecondaryAddress().getLocality().equals("") ){
					if(!vendor.getSecondaryAddress().getLocality().equals(vendor.getPrimaryAddress().getLocality())){
						if(vendor.getSecondaryAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}else{
						flag = false;
						break;	
					}
					
				}
			}
			
			if(flag){
				Locality locality = new Locality();
				locality.setLocality(vendor.getSecondaryAddress().getLocality());
				locality.setCityName(vendor.getSecondaryAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
			}
			

		}
		else if(model instanceof Branch){
			Branch branch = (Branch) model;
			
			if(LoginPresenter.globalLocality.size()==0){
				
				Locality locality = new Locality();
				locality.setLocality(branch.getAddress().getLocality());
				locality.setCityName(branch.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;
				
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(branch.getAddress().getLocality()!=null && !branch.getAddress().getLocality().equals("") ){
						if(branch.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(branch.getAddress().getLocality());
					locality.setCityName(branch.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
			}

		
		}
		else if(model instanceof Employee){
			Employee employee = (Employee) model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(employee.getAddress().getLocality());
				locality.setCityName(employee.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				
				boolean flag = true;
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(employee.getAddress().getLocality()!=null && !employee.getAddress().getLocality().equals("") ){
						if(employee.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
			
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(employee.getAddress().getLocality());
					locality.setCityName(employee.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
			}
		
		}
		else if(model instanceof WareHouse){
			WareHouse warehouse = (WareHouse)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(warehouse.getAddress().getLocality());
				locality.setCityName(warehouse.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;
				
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(warehouse.getAddress().getLocality()!=null && !warehouse.getAddress().getLocality().equals("") ){
						if(warehouse.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(warehouse.getAddress().getLocality());
					locality.setCityName(warehouse.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}else{
					flag = true;
				}
			}
		}
		else if(model instanceof Company){
			Company company = (Company)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				
				Locality locality = new Locality();
				locality.setLocality(company.getAddress().getLocality());
				locality.setCityName(company.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;
				
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(company.getAddress().getLocality()!=null && !company.getAddress().getLocality().equals("") ){
						if(company.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(company.getAddress().getLocality());
					locality.setCityName(company.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}else{
					flag = true;
				}
			}

			
		}
		else if(model instanceof CustomerBranchDetails){
			CustomerBranchDetails customerBranch = (CustomerBranchDetails)model;
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(customerBranch.getAddress().getLocality());
				locality.setCityName(customerBranch.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				
				boolean flag = true;
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(customerBranch.getAddress().getLocality()!=null && !customerBranch.getAddress().getLocality().equals("") ){
						if(customerBranch.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(customerBranch.getAddress().getLocality());
					locality.setCityName(customerBranch.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
				
			}
			
		}
		else if(model instanceof Service){
			Service service = (Service)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(service.getAddress().getLocality());
				locality.setCityName(service.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
				
			}else{
				boolean flag = true;

				
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(service.getAddress().getLocality()!=null && !service.getAddress().getLocality().equals("") ){
						if(service.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(service.getAddress().getLocality());
					locality.setCityName(service.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
				
			}

			
		}
		else if(model instanceof ServiceProject){
			ServiceProject serviceProject = (ServiceProject)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(serviceProject.getAddr().getLocality());
				locality.setCityName(serviceProject.getAddr().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;

				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(serviceProject.getAddr().getLocality()!=null && !serviceProject.getAddr().getLocality().equals("") ){
						if(serviceProject.getAddr().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(serviceProject.getAddr().getLocality());
					locality.setCityName(serviceProject.getAddr().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
				
			}

			
		}
		else if(model instanceof Fumigation){
			Fumigation fumigation = (Fumigation)model;
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(fumigation.getFromaddress().getLocality());
				locality.setCityName(fumigation.getFromaddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}
			boolean flag = true;
			
			for(int i=0;i<LoginPresenter.globalLocality.size();i++){
				if(fumigation.getFromaddress().getLocality()!=null && !fumigation.getFromaddress().getLocality().equals("") ){
					if(fumigation.getFromaddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
						flag = false;
						break;
					}
				}
			}
			
			if(flag){
				Locality locality = new Locality();
				locality.setLocality(fumigation.getFromaddress().getLocality());
				locality.setCityName(fumigation.getFromaddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
			}else{
				flag = true;
			}
			
			for(int i=0;i<LoginPresenter.globalLocality.size();i++){
				
				if(fumigation.getTomaddress().getLocality()!=null && !fumigation.getTomaddress().getLocality().equals("") ){
					if(!fumigation.getTomaddress().getLocality().equals(fumigation.getFromaddress().getLocality())){
						if(fumigation.getTomaddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}else{
						flag = false;
						break;
					}
				}
			}
			if(flag){
				Locality locality = new Locality();
				locality.setLocality(fumigation.getTomaddress().getLocality());
				locality.setCityName(fumigation.getTomaddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
			}
			
		
		}
		else if(model instanceof SalesOrder){
			SalesOrder salesOrder = (SalesOrder)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(salesOrder.getShippingAddress().getLocality());
				locality.setCityName(salesOrder.getShippingAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;
				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(salesOrder.getShippingAddress().getLocality()!=null && !salesOrder.getShippingAddress().getLocality().equals("") ){
						if(salesOrder.getShippingAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(salesOrder.getShippingAddress().getLocality());
					locality.setCityName(salesOrder.getShippingAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
			}

		
		}
		else if(model instanceof DeliveryNote){
			DeliveryNote deliveryNote = (DeliveryNote)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				
				Locality locality = new Locality();
				locality.setLocality(deliveryNote.getShippingAddress().getLocality());
				locality.setCityName(deliveryNote.getShippingAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;

				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(deliveryNote.getShippingAddress().getLocality()!=null && !deliveryNote.getShippingAddress().getLocality().equals("") ){
						if(deliveryNote.getShippingAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(deliveryNote.getShippingAddress().getLocality());
					locality.setCityName(deliveryNote.getShippingAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}else{
					flag = true;
				}
			}

			
		}
		else if(model instanceof PurchaseOrder){
			PurchaseOrder purchaseOrder = (PurchaseOrder)model;
			
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(purchaseOrder.getAdress().getLocality());
				locality.setCityName(purchaseOrder.getAdress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;

				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(purchaseOrder.getAdress().getLocality()!=null && !purchaseOrder.getAdress().getLocality().equals("") ){
						if(purchaseOrder.getAdress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(purchaseOrder.getAdress().getLocality());
					locality.setCityName(purchaseOrder.getAdress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
			}
			
		}
		else if(model instanceof StorageLocation){
			StorageLocation storageLocation = (StorageLocation)model;
			if(LoginPresenter.globalLocality.size()==0){
				Locality locality = new Locality();
				locality.setLocality(storageLocation.getAddress().getLocality());
				locality.setCityName(storageLocation.getAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);
				
			}else{
				boolean flag = true;

				for(int i=0;i<LoginPresenter.globalLocality.size();i++){
					if(storageLocation.getAddress().getLocality()!=null && !storageLocation.getAddress().getLocality().equals("") ){
						if(storageLocation.getAddress().getLocality().equals(LoginPresenter.globalLocality.get(i).getLocality())){
							flag = false;
							break;
						}
					}
				}	
				if(flag){
					Locality locality = new Locality();
					locality.setLocality(storageLocation.getAddress().getLocality());
					locality.setCityName(storageLocation.getAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}else{
					flag = true;
				}
			}

			
		}
		else if(model instanceof SalesOrder){
//			Customer customer = (Customer) model;
			SalesOrder salesOrder = (SalesOrder) model; 
			if(LoginPresenter.globalLocality.size()==0){
				if(salesOrder.getNewcustomerAddress().getLocality()!=null){
					
					Locality locality = new Locality();
					locality.setLocality(salesOrder.getNewcustomerAddress().getLocality());
					locality.setCityName(salesOrder.getNewcustomerAddress().getCity());
					locality.setStatus(true);
					LoginPresenter.globalLocality.add(locality);
				}
				
				
			}
			boolean flag = true;
			for(int j=0;j<LoginPresenter.globalLocality.size();j++){
				if(salesOrder.getNewcustomerAddress().getLocality()!=null && !salesOrder.getNewcustomerAddress().getLocality().equals("") ){
					if(LoginPresenter.globalLocality.get(j).getLocality().equals(salesOrder.getNewcustomerAddress().getLocality())){
						flag = false;
						break;
					}
				}
				
			}	
			
			if(flag){
				Locality locality = new Locality();
				locality.setLocality(salesOrder.getNewcustomerAddress().getLocality());
				locality.setCityName(salesOrder.getNewcustomerAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);

			}else{
				flag = true;
			}
			
			for(int j=0;j<LoginPresenter.globalLocality.size();j++){
				
				if(salesOrder.getShippingAddress().getLocality()!=null && !salesOrder.getShippingAddress().getLocality().equals("") ){
					if(!salesOrder.getShippingAddress().getLocality().equals(salesOrder.getNewcustomerAddress().getLocality())){
						if(LoginPresenter.globalLocality.get(j).getLocality().equals(salesOrder.getShippingAddress().getLocality())){
							flag = false;
							break;
						}
					}else{
						flag = false;
						break;	
					}
					
				}
			}

			if(flag){
				Locality locality = new Locality();
				locality.setLocality(salesOrder.getShippingAddress().getLocality());
				locality.setCityName(salesOrder.getShippingAddress().getCity());
				locality.setStatus(true);
				LoginPresenter.globalLocality.add(locality);

			}
			
			System.out.println("customer done here");
		}
		
	}
	
	/**
	 * ends here
	 */
   
   
}
