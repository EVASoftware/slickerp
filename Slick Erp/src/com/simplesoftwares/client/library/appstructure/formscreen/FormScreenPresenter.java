package com.simplesoftwares.client.library.appstructure.formscreen;


import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.EntityPresenter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.CommonService;
import com.slicktechnologies.client.services.CommonServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.cnc.CNCForm;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.fumigationAustralia.FumigationAustraliaForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationALPForm;
import com.slicktechnologies.client.views.fumigationdetails.FumigationForm;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.client.views.purchase.ProductInfoComposite;
import com.slicktechnologies.client.views.purchase.purchaseorder.PurchaseOrderForm;
import com.slicktechnologies.client.views.settings.employee.EmployeeForm;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.CNCVersion;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.ProductInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProjectOvertime;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.inventory.BillOfMaterial;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.shared.Service;

/**
 * Presenter for Form Screens.
 * @author Kamala
 *
 * @param <T>
 */
public abstract class FormScreenPresenter<T extends SuperModel> extends EntityPresenter<T> {
	
    /**
     * 
     * Reffers to   {@link FormScreen} type screen.This variable actually points to ConcreteScreen.In this design pattern
     * concrete screen will be refferd by three variable </br>
     * 1. ConcreteScreen Type - Concrete Presenter can acess ConcreteScreen level methods.
     * 2. FormScreen Type - {@link FormScreenPresenter} can acess FormScreen level methods.
     * 3. UiScreen Type - {@link EntityPresenter} can acess UiScreen level methods.
     */
	protected FormScreen<T> formview;
	/**
	 * 
	 * @param view Screen for this presenter
	 * @param model Model for this presenter
	 */
	public FormScreenPresenter(UiScreen<T> view, T model) {
		super(view, model);
		formview=(FormScreen<T>) view;
	}
	
	/**
	 * If validation is true validate then saves the model.
	 * To Do:Prone to concurrency failure.
	 * To Do : Pradnya Idaea can be used
	 */

	@Override
	public void reactOnSave() {
		if(validateAndUpdateModel())
		{
			/**
			 * Date : 31-08-2017 By ANIL
			 * If we are saving contract following method will be called
			 */
			if(model instanceof Contract){
				saveContract();
				return;
			}
			/**
			 * Date 13-09-2018 By Vijay
			 * If we are saving Quotation following method will be called
			 */
			if(model instanceof Quotation){
				saveQuotation();
				return;
			}
			
			if(model instanceof HrProject){
				HrProject hrproject = (HrProject) model;
				Console.log("HR Project Ot list size "+hrproject.getOtList().size());
				if(hrproject.getOtList().size()>150 || hrproject.isSeperateEntityFlag()) {
					saveHRProject();
					return;
				}
				
			}
			
			if(model instanceof ProjectAllocation){
				ProjectAllocation projectAllocation = (ProjectAllocation) model;
				Console.log("Employee Project Allocation list size "+projectAllocation.getEmployeeProjectAllocationList().size());
				if(projectAllocation.getEmployeeProjectAllocationList().size()>AppConstants.employeeProjectAllocationListNumber || projectAllocation.isSeperateEntityFlag()) {
					saveProjectAllocation();
					return;
				}
				
			}
			
			if(model instanceof Service){
				Console.log("in service  on save status="+((Service) model).getStatus());
				Service service = (Service) model;
				if(service.getUptestReport()!=null){
					Console.log("setting status to tcompleted");
					((Service) model).setStatus(Service.SERVICETCOMPLETED);	
					DateTimeFormat format= DateTimeFormat.getFormat("dd/MM/yyyy HH:mm:ss");
					((Service) model).settCompletedDate_time(format.format(new Date()));
					
					
				}
				else if(service.getStatus().equals(Service.SERVICETCOMPLETED)){
					Console.log("setting status to scheduled");
					((Service) model).setStatus(Service.SERVICESTATUSSCHEDULE);	
					((Service) model).settCompletedDate_time("");
				}else if(service.getStatus().equals(Service.SERVICESTATUSOPEN)||service.getStatus().equals(Service.SERVICESTATUSSCHEDULE)||service.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)||service.getStatus().equals(Service.SERVICESTATUSPLANNED)){
					((Service) model).settCompletedDate_time("");
				}
			}
			
			// Async querry is going to start. We do not want user to do any thing untill it completes.
			view.showWaitSymbol();
			service.save(model,new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					//Async run sucessfully , hide the wait symbol
					view.hideWaitSymbol();
					// set the count returned from server to this model
					model.setCount(result.count);
				   //set the id from server to this model
					model.setId(result.id);
					view.setToViewState();
					//Set count on view
					view.setCount(result.count);
					
					/**
					 * nidhi
					 *  *:*:*
					 * 11-09-2018
					 * forupdate bill of material global list
					 */
					if(model instanceof BillOfMaterial){
						BillOfMaterial billM = new BillOfMaterial();
						view.getPresenter().getModel().setCount(result.count);
						billM = (BillOfMaterial)model;
						boolean get = false;
						if (ContractPresenter.globalProdBillOfMaterial==null ){
							ContractPresenter.globalProdBillOfMaterial = new ArrayList<BillOfMaterial>();
						}
						for(int i=0 ; i<ContractPresenter.globalProdBillOfMaterial.size();i++){
							if(ContractPresenter.globalProdBillOfMaterial.get(i).getCount() == billM.getCount()){
								ContractPresenter.globalProdBillOfMaterial.remove(i);
								ContractPresenter.globalProdBillOfMaterial.add(billM);
								get = true;
								return ;
							}
						}
						if(!get){
							ContractPresenter.globalProdBillOfMaterial.add(billM);
						}
					}
					/**
					 * end
					 */
					
					
					if(model instanceof Fumigation){
						Timer timer=new Timer() {
							@Override
							public void run() {
								final Fumigation entity=(Fumigation) model;
								if(entity.getCertificateNo() == null){
								System.out.println("After Saving Sales Order...!! "+model.getCount());
								System.out.println("SALE ORDER ID "+entity.getCount());
								Console.log("After Saving Sales Order...!! "+model.getCount());
								Console.log("SALE ORDER ID "+entity.getCount());
	
								Vector<Filter> filtervec = new Vector<Filter>();
								Filter filter = null;
								MyQuerry querry = null;	
								filter = new Filter();
								filter.setQuerryString("companyId");
								filter.setLongValue(UserConfiguration.getCompanyId());
								filtervec.add(filter);
								
								filter = new Filter();
								filter.setQuerryString("count");
								filter.setIntValue(model.getCount());
								filtervec.add(filter);
								
								querry = new MyQuerry();
								querry.setQuerryObject(new Fumigation());
								querry.setFilters(filtervec);
								service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
									
									@Override
									public void onSuccess(ArrayList<SuperModel> result) {
										// TODO Auto-generated method stu
										Console.log("Size "+ result.size());
										if(result.size()!=0){
											Console.log("Size "+ result.size());
											
											Fumigation fumigation= (Fumigation) result.get(0);
											Console.log( "certificate :"+ fumigation.getCertificateNo());
											((Fumigation) model).setCertificateNo(fumigation.getCertificateNo());
											if(view instanceof FumigationForm){
												FumigationForm form=(FumigationForm) view;
												form.getTbCertificateNo().setValue(fumigation.getCertificateNo());
											}
											if(view instanceof FumigationALPForm){
												FumigationALPForm form=(FumigationALPForm) view;
												form.getTbCertificateNo().setValue(fumigation.getCertificateNo());
											}
											if(view instanceof FumigationAustraliaForm){
												FumigationAustraliaForm form=(FumigationAustraliaForm) view;
												form.getTbCertificateNo().setValue(fumigation.getCertificateNo());
											}
											//view.setCertificateNo(fumigation.getCertificateNo());
										}
										view.setToViewState();
										
									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}
								});
																
							//	view.hideWaitSymbol();
							//	view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
								}
							}
						};
						timer.schedule(5000);
					
					}else if(model instanceof Service){
						/*
						 * Name:Apeksha Gunjal
						 * Date: 29/06/2018 @18:51
						 * Note: Send Notification to Technician on EVA Pedio after assigning respective technician
						 */
						final Logger logger = Logger.getLogger("Name of logger");
						try{
							GeneralServiceAsync generalServiceImpl = GWT.create(GeneralService.class);
							Service service = (Service) model;
							logger.log(Level.SEVERE, "FromScreenPresenter service.getEmployee(): "+service.getEmployee());
							if(!service.getEmployee().isEmpty()){
								generalServiceImpl.sendPushNotificationToTechnician(service, service.getCompanyId(),new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										logger.log(Level.SEVERE, "FromScreenPresenter sendNotification:Fail");
									}

									@Override
									public void onSuccess(String result) {
										// TODO Auto-generated method stub
										logger.log(Level.SEVERE, "FromScreenPresenter sendNotification:Successful");
										
									}
								});
							}
						}catch(Exception e){
							logger.log(Level.SEVERE, "FromScreenPresenter Error in sending push notification to technician: "+e.getMessage());
						}
					}else  if (model instanceof CNC) {
						 CNC cnc = (CNC) model;
						 CNCForm cncForm  = (CNCForm) view;
						 if(cncForm.isAmended){
							 CNCVersion cncVer = new CNCVersion();
							 cnc.setIslatestVersion(false);
							 cncVer.setCnc(cnc);
							 cncVer.setVersionCreationDate(new Date());
							 cncVer.setVersionStatus(cnc.getStatus());
							 cncVer.setVersion(cnc.getVersionNumber()+"");
							 cncVer.setCompanyId(cnc.getCompanyId());
							 cncForm.isAmended = false;
							 service.save(cncVer, new AsyncCallback<ReturnFromServer>() {
								
								@Override
								public void onSuccess(ReturnFromServer result) {
									// TODO Auto-generated method stub
									view.updateView(model);
								}
								
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-gesnerated method stub
									
								}
							});
						 }else{
							 view.updateView(model);
						 }
						
					 }else if(view instanceof PurchaseOrderForm){
						 if(AppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder", "MonthwiseNumberGeneration")){
							Timer timer=new Timer() {
								@Override
								public void run() {
									final PurchaseOrder entity=(PurchaseOrder) model;
									if(((PurchaseOrderForm) view).getIbRefOrderNo().getValue() == null || ((PurchaseOrderForm) view).getIbRefOrderNo().getValue().equals("")){
									System.out.println("After Saving Sales Order...!! "+model.getCount());
									System.out.println("SALE ORDER ID "+entity.getCount());
									Console.log("After Saving Sales Order...!! "+model.getCount());
									Console.log("SALE ORDER ID "+entity.getCount());
		
									Vector<Filter> filtervec = new Vector<Filter>();
									Filter filter = null;
									MyQuerry querry = null;	
									filter = new Filter();
									filter.setQuerryString("companyId");
									filter.setLongValue(UserConfiguration.getCompanyId());
									filtervec.add(filter);
									
									filter = new Filter();
									filter.setQuerryString("count");
									filter.setIntValue(model.getCount());
									filtervec.add(filter);
									
									querry = new MyQuerry();
									querry.setQuerryObject(new PurchaseOrder());
									querry.setFilters(filtervec);
									service.getSearchResult(querry, new AsyncCallback<ArrayList<SuperModel>>() {
										
										@Override
										public void onSuccess(ArrayList<SuperModel> result) {
											// TODO Auto-generated method stu
											Console.log("Size "+ result.size());
											if(result.size()!=0){
												Console.log("Size "+ result.size());
												
												PurchaseOrder purchaseOrder= (PurchaseOrder) result.get(0);
												Console.log( "certificate :"+ purchaseOrder.getRefOrderNO());
												((PurchaseOrder) model).setRefOrderNO(purchaseOrder.getRefOrderNO());
												if(view instanceof PurchaseOrderForm){
													PurchaseOrderForm form=(PurchaseOrderForm) view;
													form.getIbRefOrderNo().setValue(purchaseOrder.getRefOrderNO());
												}
											}
											view.setToViewState();
											
										}
										
										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											
										}
									});
																	
								//	view.hideWaitSymbol();
								//	view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
									}
								}
							};
							timer.schedule(3000);
						
						 }
					 }
					/**
					 * @author Anil , Date : 14-08-2019
					 * customer loading issue if large customer data process configuration is active
					 * For ankita raised by rahul tiwari
					 */
					 else if(model instanceof Customer){
						 LoginPresenter.customerCount=model.getCount();
					 }
					
					/**Date 5-3-2020 by Amol to do not show the item product 
					 * when it is InActive to Solve logOut Issue**/
					
					 else if(model instanceof ItemProduct){
						 Console.log("inside item product Inactive");
						 ItemProduct itemProd=(ItemProduct) model;
						 if(ProductInfoComposite.globalItemProdArray!=null){
							 for(ProductInfo prodInfo:ProductInfoComposite.globalItemProdArray){
								 if(prodInfo.getProdID()==itemProd.getCount()){
									 prodInfo.setStatus(itemProd.isStatus());
								 }
							 }
						 }
					 }
					
					/**
					 * @author Anil @since 31-03-2021
					 * 
					 */
//					 else if(model instanceof Employee){
//						 if(view instanceof EmployeeForm){
//							 Employee employee=(Employee) model;
//							 EmployeeForm employeeForm=(EmployeeForm) view;
//							 employeeForm.manageEmployeeDropDown(employee);
//						 }
//					 }
					
					
					view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();
					
				}
			});
			
		}
		
		
	}

	

	

	

/**
    * Stub for design side issues.
    */
	@Override
	public void reactOnDownload() {
		
		
	}

	 @Override
		protected void reactOnHistory() {
		   	HistoryPopup popup = new HistoryPopup();
			LoginPresenter.panel=new PopupPanel(true);
			popup.getTable().getDataprovider().setList(LoginPresenter.globalHistoryList);
			LoginPresenter.panel.add(popup);
			LoginPresenter.panel.center();
			LoginPresenter.panel.show();		
		}
	 
	 /**
	  * Date 13 April 2017 added by vijay
	  * here i am override because wherever this method we we can override this method
	  */
	@Override
	public void reactOnCommunicationLog() {
		// TODO Auto-generated method stub
	}
	
	public void saveContract(){
		Console.log("1-INSIDE CONTRACT SAVE");
		final ContractServiceAsync conSer=GWT.create(ContractService.class);
		final Contract contract=(Contract) model;
		/**
		 * Date : 20-03-2018 By ANIL
		 * Changed services from 1000 to 700 
		 * Issue faced by Express pesticides (Rohan Bhagde)
		 */
		/**nidhi
		 * 28-11-2018 for update range for bom list save
		 */
		int numberOfSerLimit = 700;
		if(LoginPresenter.billofMaterialActive){
			numberOfSerLimit = 350;
		}
		if(contract.getServiceScheduleList().size()>numberOfSerLimit){
			/**
			 * end
			 */
			for(ServiceSchedule sch:contract.getServiceScheduleList()){
				sch.setDocumentType("Contract");
				sch.setDocumentId(contract.getCount());
				sch.setCompanyId(UserConfiguration.getCompanyId());
			}
			final ArrayList<ServiceSchedule> schSerList=new ArrayList<ServiceSchedule>(contract.getServiceScheduleList());
			Console.log("1(A)-SCHEDULE LIST "+schSerList.size());
			
			view.showWaitSymbol();
			Console.log("COM_ID "+contract.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+contract.getCount());
			
			/////////////////////////
			Timer t = new Timer() {
			      @Override
			      public void run() {
			conSer.getScheduleServiceSearchResult(contract.getCompanyId(), "Contract", contract.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
				}
				@Override
				public void onSuccess(ArrayList<ServiceSchedule> result) {
					Console.log("2-ALREADY SAVED RECORDS "+result.size());
					if(result!=null&&result.size()!=0){
						
						
						
						conSer.updateScheduleService("Delete", result, new AsyncCallback<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								view.hideWaitSymbol();
							}
							@Override
							public void onSuccess(Void result) {
								Console.log("3-Deleting.... ");
								
								
								///////////////
								Timer t = new Timer() {
								      @Override
								      public void run() {
								
								contract.getServiceScheduleList().clear();
								service.save(model,new AsyncCallback<ReturnFromServer>() {
									@Override
									public void onSuccess(ReturnFromServer result) {
										Console.log("3C-Contract SAVED.... ");
										// set the count returned from server to this model
										model.setCount(result.count);
									   //set the id from server to this model
										model.setId(result.id);
										//Set count on view
										view.setCount(result.count);
										
										Console.log("3(A)-SCHEDULE LIST "+schSerList.size());
										for(ServiceSchedule sch:schSerList){
											sch.setDocumentType("Contract");
											sch.setDocumentId(result.count);
											sch.setCompanyId(UserConfiguration.getCompanyId());
										}
										
										Console.log("3(B)-SCHEDULE FIRST "+schSerList.get(0).getDocumentId()+" LAST "+schSerList.get(schSerList.size()-1).getDocumentId());
										conSer.updateScheduleService("Save", schSerList, new AsyncCallback<Void>() {
											@Override
											public void onFailure(Throwable caught) {
												view.hideWaitSymbol();
											}
											@Override
											public void onSuccess(Void result) {
												Console.log("4-Saving sch list .... ");
												//Async run sucessfully , hide the wait symbol
												
												Timer t = new Timer() {
											      @Override
											      public void run() {
											    	  Console.log("5-SHow popup.... ");
													view.hideWaitSymbol();
													view.setToViewState();
													view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
											      }
											    };
											    t.schedule(40000);
											}
										});
										
									}
									
									@Override
									public void onFailure(Throwable caught) {
										view.hideWaitSymbol();
										view.showDialogMessage("Data Save Unsuccessful! Try again.");
										caught.printStackTrace();
									}
								});
								      }
							    };
							    t.schedule(40000);
								
								/////////////////
								
							}
						});
						
						
						
					}else{
						
						contract.getServiceScheduleList().clear();
						/**
						 * Date : 25-04-2018 By ANIL
						 * earlier by passing model ,it saves the contract 
						 * but now there is schedule list is also in reference of model object we have clear it from contract object 
						 */
						service.save(contract,new AsyncCallback<ReturnFromServer>() {
//						service.save(model,new AsyncCallback<ReturnFromServer>() {
							@Override
							public void onSuccess(ReturnFromServer result) {
								// set the count returned from server to this model
								model.setCount(result.count);
							   //set the id from server to this model
								model.setId(result.id);
								
								//Set count on view
								view.setCount(result.count);
								Console.log("5(A)-SCHEDULE LIST "+schSerList.size());
								for(ServiceSchedule sch:schSerList){
									sch.setDocumentType("Contract");
									sch.setDocumentId(result.count);
									sch.setCompanyId(UserConfiguration.getCompanyId());
								}
								Console.log("5(B)-SCHEDULE FIRST "+schSerList.get(0).getDocumentId()+" LAST "+schSerList.get(schSerList.size()-1).getDocumentId());
								conSer.updateScheduleService("Save", schSerList, new AsyncCallback<Void>() {
									@Override
									public void onFailure(Throwable caught) {
										view.hideWaitSymbol();
									}
									@Override
									public void onSuccess(Void result) {
										Console.log("5-Saving.... ");
//										view.hideWaitSymbol();
										//Async run sucessfully , hide the wait symbol
										Timer t = new Timer() {
										      @Override
										      public void run() {
										    	  Console.log("5-SHow popup.... ");
												view.hideWaitSymbol();
												view.setToViewState();
												view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
										      }
										    };
										    t.schedule(40000);
										    }
								});
							}
							
							@Override
							public void onFailure(Throwable caught) {
								view.hideWaitSymbol();
								view.showDialogMessage("Data Save Unsuccessful! Try again.");
								caught.printStackTrace();
							}
						});
						
					}
				}
			});
			
			      }
		    };
		    t.schedule(20000);
			/////////////////////////
			
		}else{
			view.showWaitSymbol();
			service.save(model,new AsyncCallback<ReturnFromServer>() {
				@Override
				public void onSuccess(ReturnFromServer result) {
					//Async run sucessfully , hide the wait symbol
					view.hideWaitSymbol();
					// set the count returned from server to this model
					model.setCount(result.count);
				   //set the id from server to this model
					model.setId(result.id);
					view.setToViewState();
					//Set count on view
					view.setCount(result.count);
					view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();
				}
			});
		}
	}
	
	/**
	 * Date 13-09-2018 By Vijay
	 * Des:- if services are greater than 700 then quotation unable to save then we have written this method for services
	 * store in another entity
	 */
	   private void saveQuotation() {
			// TODO Auto-generated method stub

			Console.log("1-INSIDE Quotation SAVE");
			final ContractServiceAsync conSer=GWT.create(ContractService.class);
			final Quotation quotation=(Quotation) model;
			/**
			 * Date : 20-03-2018 By ANIL
			 * Changed services from 1000 to 700 
			 * Issue faced by Express pesticides (Rohan Bhagde)
			 */
			/**nidhi |*|
			 * 8-01-2019 for update range for bom list save
			 */
			int numberOfSerLimit = 700;
			if(LoginPresenter.billofMaterialActive){
				numberOfSerLimit = 350;
			}
			if(quotation.getServiceScheduleList().size()>700){
				for(ServiceSchedule sch:quotation.getServiceScheduleList()){
					sch.setDocumentType("Quotation");
					sch.setDocumentId(quotation.getCount());
					sch.setCompanyId(UserConfiguration.getCompanyId());
				}
				final ArrayList<ServiceSchedule> schSerList=new ArrayList<ServiceSchedule>(quotation.getServiceScheduleList());
				Console.log("1(A)-SCHEDULE LIST "+schSerList.size());
				
				view.showWaitSymbol();
				Console.log("COM_ID "+quotation.getCompanyId()+" DOC_TYPE "+"Contract"+" DOC_ID "+quotation.getCount());
				
				/////////////////////////
				Timer t = new Timer() {
				      @Override
				      public void run() {
				conSer.getScheduleServiceSearchResult(quotation.getCompanyId(), "Quotation", quotation.getCount(), new AsyncCallback<ArrayList<ServiceSchedule>>() {
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
					}
					@Override
					public void onSuccess(ArrayList<ServiceSchedule> result) {
						Console.log("2-ALREADY SAVED RECORDS "+result.size());
						if(result!=null&&result.size()!=0){
							
							
							
							conSer.updateScheduleService("Delete", result, new AsyncCallback<Void>() {
								@Override
								public void onFailure(Throwable caught) {
									view.hideWaitSymbol();
								}
								@Override
								public void onSuccess(Void result) {
									Console.log("3-Deleting.... ");
									
									
									///////////////
									Timer t = new Timer() {
									      @Override
									      public void run() {
									
						    	  quotation.getServiceScheduleList().clear();
									service.save(model,new AsyncCallback<ReturnFromServer>() {
										@Override
										public void onSuccess(ReturnFromServer result) {
											Console.log("3C-Contract SAVED.... ");
											// set the count returned from server to this model
											model.setCount(result.count);
										   //set the id from server to this model
											model.setId(result.id);
											//Set count on view
											view.setCount(result.count);
											
											Console.log("3(A)-SCHEDULE LIST "+schSerList.size());
											for(ServiceSchedule sch:schSerList){
												sch.setDocumentType("Quotation");
												sch.setDocumentId(result.count);
												sch.setCompanyId(UserConfiguration.getCompanyId());
											}
											
											Console.log("3(B)-SCHEDULE FIRST "+schSerList.get(0).getDocumentId()+" LAST "+schSerList.get(schSerList.size()-1).getDocumentId());
											conSer.updateScheduleService("Save", schSerList, new AsyncCallback<Void>() {
												@Override
												public void onFailure(Throwable caught) {
													view.hideWaitSymbol();
												}
												@Override
												public void onSuccess(Void result) {
													Console.log("4-Saving sch list .... ");
													//Async run sucessfully , hide the wait symbol
													
													Timer t = new Timer() {
												      @Override
												      public void run() {
												    	  Console.log("5-SHow popup.... ");
														view.hideWaitSymbol();
														view.setToViewState();
														view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
												      }
												    };
												    t.schedule(40000);
												}
											});
											
										}
										
										@Override
										public void onFailure(Throwable caught) {
											view.hideWaitSymbol();
											view.showDialogMessage("Data Save Unsuccessful! Try again.");
											caught.printStackTrace();
										}
									});
									      }
								    };
								    t.schedule(40000);
									
									/////////////////
									
								}
							});
							
							
							
						}else{
							
							quotation.getServiceScheduleList().clear();
							/**
							 * Date : 25-04-2018 By ANIL
							 * earlier by passing model ,it saves the contract 
							 * but now there is schedule list is also in reference of model object we have clear it from contract object 
							 */
							service.save(quotation,new AsyncCallback<ReturnFromServer>() {
//							service.save(model,new AsyncCallback<ReturnFromServer>() {
								@Override
								public void onSuccess(ReturnFromServer result) {
									// set the count returned from server to this model
									model.setCount(result.count);
								   //set the id from server to this model
									model.setId(result.id);
									
									//Set count on view
									view.setCount(result.count);
									Console.log("5(A)-SCHEDULE LIST "+schSerList.size());
									for(ServiceSchedule sch:schSerList){
										sch.setDocumentType("Quotation");
										sch.setDocumentId(result.count);
										sch.setCompanyId(UserConfiguration.getCompanyId());
									}
									Console.log("5(B)-SCHEDULE FIRST "+schSerList.get(0).getDocumentId()+" LAST "+schSerList.get(schSerList.size()-1).getDocumentId());
									conSer.updateScheduleService("Save", schSerList, new AsyncCallback<Void>() {
										@Override
										public void onFailure(Throwable caught) {
											view.hideWaitSymbol();
										}
										@Override
										public void onSuccess(Void result) {
											Console.log("5-Saving.... ");
//											view.hideWaitSymbol();
											//Async run sucessfully , hide the wait symbol
											Timer t = new Timer() {
											      @Override
											      public void run() {
											    	  Console.log("5-SHow popup.... ");
													view.hideWaitSymbol();
													view.setToViewState();
													view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
											      }
											    };
											    t.schedule(40000);
											    }
									});
								}
								
								@Override
								public void onFailure(Throwable caught) {
									view.hideWaitSymbol();
									view.showDialogMessage("Data Save Unsuccessful! Try again.");
									caught.printStackTrace();
								}
							});
							
						}
					}
				});
				
				      }
			    };
			    t.schedule(20000);
				/////////////////////////
				
			}else{
				view.showWaitSymbol();
				service.save(model,new AsyncCallback<ReturnFromServer>() {
					@Override
					public void onSuccess(ReturnFromServer result) {
						//Async run sucessfully , hide the wait symbol
						view.hideWaitSymbol();
						// set the count returned from server to this model
						model.setCount(result.count);
					   //set the id from server to this model
						model.setId(result.id);
						view.setToViewState();
						//Set count on view
						view.setCount(result.count);
						view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
						view.showDialogMessage("Data Save Unsuccessful! Try again.");
						caught.printStackTrace();
					}
				});
			}
		
		}
	   /**
	    * ends here
	    */
	   
	   
	   

	   private void saveHRProject() {
		
			Console.log("INSIDE HRPROJECT SAVE");
			view.showWaitSymbol();
			
			final CommonServiceAsync commonserviceasync = GWT.create(CommonService.class);
			
			final HrProject hrproject = (HrProject) model;
			
			final ArrayList<HrProjectOvertime> otlist = new ArrayList<HrProjectOvertime>();
			for(Overtime overtime : hrproject.getOtList()){
				HrProjectOvertime ot = new HrProjectOvertime();
//				ot.setProjectId(hrproject.getCount());
				ot = ot.MycloneHrProjectOT(overtime);
				otlist.add(ot);
			}
			
			Console.log("otlist size "+otlist.size());
			
			
			hrproject.getOtList().clear();
			service.save(hrproject, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					// TODO Auto-generated method stub
					Console.log("3C-HRProject SAVED.... ");

					// set the count returned from server to this model
					model.setCount(result.count);
				   //set the id from server to this model
					model.setId(result.id);
					//Set count on view
					view.setCount(result.count);
					
					Console.log("project id "+result.count);
					for(HrProjectOvertime overtime : otlist){
						overtime.setProjectId(result.count);
					}
					
					commonserviceasync.updateHrProjectOvertime(otlist, "Save", new AsyncCallback<String>() {
						
						@Override
						public void onSuccess(String result) {
							Console.log("4-Saving project ot list .... ");
							
							Timer t = new Timer() {
						      @Override
						      public void run() {
						    	  Console.log("5-SHow popup.... ");
								view.hideWaitSymbol();
								view.setToViewState();
								view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
						     
								if(hrproject.getDeletedOtIdlist()!=null && hrproject.getDeletedOtIdlist().size()>0){
									
									commonserviceasync.updateHrProjectOvertimelist(hrproject.getDeletedOtIdlist(), hrproject.getCompanyId(), "Delete", hrproject.getCount(), new AsyncCallback<String>() {

										@Override
										public void onFailure(Throwable caught) {
											// TODO Auto-generated method stub
											
										}

										@Override
										public void onSuccess(String result) {
											// TODO Auto-generated method stub
											Console.log("HrProjectOvertime entry deleted");
										}
									});
									
								}
						      
						      }
						    };
						    t.schedule(10000);
							
						}
						
						@Override
						public void onFailure(Throwable caught) {
							view.hideWaitSymbol();
						}
					});
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();
				}
			});
			
			
			
		}
	   
	   private void saveProjectAllocation() {
			
			Console.log("INSIDE ProjectAllocation SAVE");
			view.showWaitSymbol();
			
			final CommonServiceAsync commonserviceasync = GWT.create(CommonService.class);
			
			final ProjectAllocation projectAllocationEntity = (ProjectAllocation) model;
			
			final ArrayList<EmployeeProjectAllocation> employeeProjectAllocationlist = new ArrayList<EmployeeProjectAllocation>();
			employeeProjectAllocationlist.addAll(projectAllocationEntity.getEmployeeProjectAllocationList());
			
			Console.log("employeeProjectAllocationlist "+employeeProjectAllocationlist.size());
			
			projectAllocationEntity.getEmployeeProjectAllocationList().clear();
			service.save(projectAllocationEntity, new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					// TODO Auto-generated method stub
					Console.log("3C-projectAllocationEntity SAVED.... ");
					// set the count returned from server to this model
					model.setCount(result.count);
				   //set the id from server to this model
					model.setId(result.id);
					//Set count on view
					view.setCount(result.count);
					
					Console.log("project id "+result.count);
					for(EmployeeProjectAllocation empProjectAllocation : employeeProjectAllocationlist){
//						overtime.setProjectId(result.count);
						empProjectAllocation.setProjectAllocationId(result.count);
					}
					
				commonserviceasync.updateEmployeeProjectAllocationList(employeeProjectAllocationlist, "Save", new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						Console.log("4-Saving project Allocation list .... ");

						Timer timer = new Timer() {
							
							@Override
							public void run() {

								Console.log("5-SHow popup.... ");
								view.hideWaitSymbol();
								view.setToViewState();
								view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
							  
								projectAllocationEntity.setEmployeeProjectAllocationList(employeeProjectAllocationlist);
								commonserviceasync.genrateHRProjectOnProcejectAllocationSave(projectAllocationEntity, new AsyncCallback<String>() {
									
									@Override
									public void onSuccess(String arg0) {
										// TODO Auto-generated method stub
										Console.log("HRProject generation done");

										if(projectAllocationEntity.getDeletedEmpoyeeProjectAllocationlist()!=null && projectAllocationEntity.getDeletedEmpoyeeProjectAllocationlist().size()>0){
											   commonserviceasync.updateEmployeeProjectAllocationList(projectAllocationEntity.getDeletedEmpoyeeProjectAllocationlist(), "Delete", new AsyncCallback<String>() {

												@Override
												public void onFailure(Throwable caught) {
													// TODO Auto-generated method stub
													
												}

												@Override
												public void onSuccess(String result) {
													Console.log("EmployeeProjectAllocation employee entry deleted");
													
													
												}
											});
										   }
										
									}
									
									@Override
									public void onFailure(Throwable arg0) {
										// TODO Auto-generated method stub
										
									}
								});
								
								
							}
						};
						timer.schedule(10000);

					}
					
					@Override
					public void onFailure(Throwable caught) {
						view.hideWaitSymbol();
						
					}
				});
					
				}
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();
				}
			});
			
			
			
		
		}
}
