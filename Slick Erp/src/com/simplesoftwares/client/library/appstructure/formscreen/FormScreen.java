package com.simplesoftwares.client.library.appstructure.formscreen;


import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TabBar.Tab;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FieldType;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.simplesoftwares.client.library.FormField;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utils.Console;


/**
 * The Class FormScreen.
 * Repersents a FormScreen
 * @author Komal
 * @param <T> the generic type
 */
public abstract class FormScreen<T> extends  UiScreen<T>{
	

	/** The fields. 
	 * holds the formfield objects.This array is responsible for UI LAYOUT*/
	protected FormField[][]fields;
	
	/** The form. for creating the flexttable*/
	protected FlexForm form ;
	
	/** The formstyle.	decides the formStyle(rowform or coulmnform) */
	public FormStyle formstyle;
	
	/** The default form style. */
	protected static FormStyle DEFAULT_FORM_STYLE=FormStyle.ROWFORM;
	
	/**
	 * @author Anil
	 * @since 01-10-2020
	 * TabLayout
	 */
	LinkedHashMap<String,FormField[][]> groupedFormFieldsMap=null;
	TabLayoutPanel tabLayoutPanel;
	public TabPanel tabPanel;
	
	/**
	 * @author Anil
	 * @since 12-10-2020
	 * For Menu bar screen we need to separate key fields from Tab view panel
	 */
	protected FlexForm keyFieldForm ;
	
	protected GeneralServiceAsync genService=GWT.create(GeneralService.class);
	
	 /**
 	 * Instantiates a new form screen.
 	 * Call to parameterized constructor with default_form_style(Rowform).
 	 */
 	protected FormScreen()
	 {
		 this(DEFAULT_FORM_STYLE);
	}
	 
 	/**
	 * @author Vijay Date 25-05-2021
	 * Des :- added below constructor to show screen on Popup screen
	 * 
	 */
 	protected FormScreen(boolean popupFlag)
	{
		 this(DEFAULT_FORM_STYLE,popupFlag);
	}
 	
	 /**
 	 * Instantiates a new form screen with passed formstyle.
 	 *	Instantiates the flexform with set fields & passed formstyle 
 	 *& instantiates the processlevel bar if the prosesslevel menus are not null 
 	 * @param formstyle the formstyle
 	 */
	public FormScreen(FormStyle formstyle) {
		super(formstyle);
		this.formstyle = formstyle;
		
		form = new FlexForm(fields, formstyle);
		
		if(AppMemory.getAppMemory().enableMenuBar&&FormStyle.DEFAULT!=formstyle&&FormStyle.TABLEFORMLAYOUT!=formstyle){
			try {
				groupedFormFieldsMap=getGroupingWiseFormFields();
				
				if(groupedFormFieldsMap!=null&&groupedFormFieldsMap.size()>1){
					tabPanel=new TabPanel();
					tabPanel.setWidth("95%");
					
					for (Map.Entry<String, FormField[][]> entry : groupedFormFieldsMap.entrySet()) {
						FormField[][]fields=entry.getValue();
						FlexForm form1=new FlexForm(fields, formstyle);
						form1.getElement().setId("form");
						
						/**
						 * @author Anil
						 * @since 12-10-2020
						 */
						if(isKeyField(entry.getValue())){
							keyFieldForm=form1;
							keyFieldForm.getElement().getStyle().setMarginTop(5, Unit.PX);
							keyFieldForm.getElement().getStyle().setMarginBottom(0,Unit.PX);
						}else{
							tabPanel.add(form1, entry.getKey());
						}
					}
					try{
						tabPanel.selectTab(0);
					}catch(Exception e){
						
					}
					tabPanel.getElement().setId("formcontent");
					
					
					tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
						@Override
						public void onSelection(SelectionEvent<Integer> event) {
							/**
							 * @author Anil
							 * @since 31-12-2020
							 * setting tab to default property
							 */
							try{
								String tabName=tabPanel.getTabBar().getTabHTML(tabPanel.getTabBar().getSelectedTab());
								if(tabName.contains("<div class=")){
									try{
										tabName=tabName.split(">")[1];
										tabName=tabName.split("<")[0];
									}catch(Exception e){
										
									}
								}
								Console.log("TAB "+tabName);
								
								String style= "<div class='gwt-Label' style='white-space: nowrap;'>"+tabName+"</div>";
								Console.log("TAB NAME : "+style);
								tabPanel.getTabBar().setTabHTML(tabPanel.getTabBar().getSelectedTab(), style);
								
								refreshTableData();
								
							}catch(Exception e){
								
							}
							
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		isAction=false;
		// create the process level bar and form add them in content
		if (processlevelBarNames != null) {
//			processLevelBar = new ProcessLevelBar(5, processlevelBarNames);
			processLevelBar = new ProcessLevelBar(5, processlevelBarNames,navigationProcessMenuBar,formstyle);
			processLevelBar.setVisibleFalse(false);
			
			if(FormStyle.DEFAULT!=formstyle){
				isAction=true;
			}
		}

		if(FormStyle.DEFAULT==formstyle){
			isAction=false;
			isHelp=false;
			isReports=false;
			isNavigation=false;
			isUpload=false;
			navigationProcessMenuBar=null;
		}
		
//		if (AppMemory.getAppMemory().skeleton.appdrawerPanel != null && AppMemory.getAppMemory().skeleton.appdrawerPanel.isShowing()) {
//			AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//		}
		
		if (appdrawerPanel != null && appdrawerPanel.isShowing()) {
			appdrawerPanel.hide();
		}

//		isScreenMenuVisible(false);
	}
	

	 /**
	 * Instantiates a new form screen with passed formstyle.
	 *	Instantiates the flexform with set fields & passed formstyle 
	 *& instantiates the processlevel bar if the prosesslevel menus are not null 
	 * @param formstyle the formstyle
	 */
	public FormScreen(FormStyle formstyle, boolean popupFlag) {
		super(formstyle,popupFlag);
		this.formstyle = formstyle;
		
		form = new FlexForm(fields, formstyle);
		
		if(AppMemory.getAppMemory().enableMenuBar&&FormStyle.DEFAULT!=formstyle&&FormStyle.TABLEFORMLAYOUT!=formstyle){
			try {
				groupedFormFieldsMap=getGroupingWiseFormFields();
				
				if(groupedFormFieldsMap!=null&&groupedFormFieldsMap.size()>1){
//					Console.log("FORM SCREEN CONSOLE LOG 1");
//					System.out.println("FORM SCREEN CONSOLE LOG 1");
//					tabLayoutPanel=new TabLayoutPanel(2.5, Unit.EM);
					tabPanel=new TabPanel();
					tabPanel.setWidth("95%");
					tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
						@Override
						public void onSelection(SelectionEvent<Integer> event) {
							/**
							 * @author Anil
							 * @since 31-12-2020
							 * setting tab to default property
							 */
							try{
								
								
								String tabName=tabPanel.getTabBar().getTabHTML(tabPanel.getTabBar().getSelectedTab());
								if(tabName.contains("<div class=")){
									try{
										tabName=tabName.split(">")[1];
										tabName=tabName.split("<")[0];
									}catch(Exception e){
										
									}
								}
								Console.log("TAB "+tabName);
								
								String style= "<div class='gwt-Label' style='white-space: nowrap;'>"+tabName+"</div>";
//								String tabName1=tabPanel.getTabBar().getTabHTML(tabPanel.getTabBar().getSelectedTab());
//								if(tabName1.contains("background: red;")){
//									tabName1=tabName1.replace("background: red;", "");
//									style=tabName1;
//								}
								Console.log("TAB NAME : "+style);
								tabPanel.getTabBar().setTabHTML(tabPanel.getTabBar().getSelectedTab(), style);
							}catch(Exception e){
								
							}
							refreshTableData();
						}
					});
					
//					tabPanel.add
					
					
					for (Map.Entry<String, FormField[][]> entry : groupedFormFieldsMap.entrySet()) {
						FormField[][]fields=entry.getValue();
						FlexForm form1=new FlexForm(fields, formstyle);
						form1.getElement().setId("form");
						
						/**
						 * @author Anil
						 * @since 12-10-2020
						 */
						if(isKeyField(entry.getValue())){
							keyFieldForm=form1;
							keyFieldForm.getElement().getStyle().setMarginTop(5, Unit.PX);
							keyFieldForm.getElement().getStyle().setMarginBottom(0,Unit.PX);
						}else{
							tabPanel.add(form1, entry.getKey());
						}
					}
					try{
						tabPanel.selectTab(0);
					}catch(Exception e){
						
					}
					tabPanel.getElement().setId("formcontent");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		Console.log("Done with form tab layout");
		isAction=false;
		// create the process level bar and form add them in content
		if (processlevelBarNames != null) {
			Console.log("processlevelBarNames not null");
//			processLevelBar = new ProcessLevelBar(5, processlevelBarNames);
			processLevelBar = new ProcessLevelBar(5, processlevelBarNames,navigationProcessMenuBar,formstyle);
			processLevelBar.setVisibleFalse(false);
			
			if(FormStyle.DEFAULT!=formstyle){
				isAction=true;
			}
		}

		if(FormStyle.DEFAULT==formstyle){
			isAction=false;
			isHelp=false;
			isReports=false;
			isNavigation=false;
			isUpload=false;
			navigationProcessMenuBar=null;
		}
		
//		if (AppMemory.getAppMemory().skeleton.appdrawerPanel != null && AppMemory.getAppMemory().skeleton.appdrawerPanel.isShowing()) {
//			AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//		}
		
		if (appdrawerPanel != null && appdrawerPanel.isShowing()) {
			appdrawerPanel.hide();
		}

//		isScreenMenuVisible(false);
	}

	/**
 	 * Instantiates a new form screen with passed processlevel menus,
 	 * fields & formstyle
 	 *
 	 * @param processlevel the processlevel menu names
 	 * @param fields the fields  The array of form field
 	 * @param formstyle the formstyle The default style of form
 	 */
 	public FormScreen( String[] processlevel,
			FormField[][] fields, FormStyle formstyle) 
	 {
		this(formstyle);
		this.processlevelBarNames = processlevel;
		this.fields = fields;
		this.formstyle = formstyle;
	}

///////////////////////////////////////////GUI CREATION/////////////////////////////////////////////
	 @SuppressWarnings("deprecation")
/*
	  * LOGICAL EXCEPTION TO B THROWN IF "fields & formstyle" ARE NOT INITIALIZED
	  */
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.ViewContainer#createGui()
 */
 	/**
 	 * Adds the processlevel bar on content if it is not null & adds the form on content
 	 */
	@Override
	protected  void createGui() 
	{
		applyStyle();
		if(processlevelBarNames!=null)
		{
			/**
			 * @author Anil
			 * @since 23-09-2020
			 * removing process level menu bar and adding those menu on popup panel
			 */
			
			if(!AppMemory.getAppMemory().enableMenuBar||FormStyle.DEFAULT==formstyle){
				content.add(processLevelBar.content);	
			}else{
//				processMenuPopup.getVerticalPanel().clear();
				processLevelBar.content.getElement().setId("login-form");
				processLevelBar.setUpdatedCSS();
				
				if(processMenuPopup!=null){
					processMenuPopup.getVerticalPanel().add(processLevelBar.content);
				}
				
				addClickEventOnActionAndNavigationMenus();
			}
		}
		
		
		if(AppMemory.getAppMemory().enableMenuBar&&FormStyle.DEFAULT!=formstyle&&FormStyle.TABLEFORMLAYOUT!=formstyle&&groupedFormFieldsMap!=null&&groupedFormFieldsMap.size()>1){
			Console.log("FORM SCREEN CONSOLE LOG 2");
			System.out.println("FORM SCREEN CONSOLE LOG 2");
			
			if(keyFieldForm!=null){
				content.add(keyFieldForm);
			}
			content.add(tabPanel);
		}else{
			content.add(form);
		}
		
	}

/////////////////////////////////////INHERITED METHODS(OVERRIDEN)/////////////////////////////////////////
	
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.UiScreen#clear()
 */
@Override
	public void clear() {
		form.clear();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.UiScreen#validate()
	 */
	@Override
	public boolean validate() {
		/**
		 * @author Anil
		 * @since 30-12-2020
		 */
		highlightTab();
		
		return form.validate();
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.UiScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		form.setEnabled(state);	
	}
	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.UiScreen#applyStyle()
	 */
	
	public void applyStyle()
    {
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
	}

///////////////////////////////////////////////////////////GETTER SETTERS///////////////////////////////////
	
	/**
	 * Gets the bar.
	 *
	 * @return the bar
	 */
	public ProcessLevelBar getBar() {
		return processLevelBar;
	}


	/**
	 * Sets the bar.
	 *
	 * @param bar the new bar
	 */
	public void setBar(ProcessLevelBar bar) {
		this.processLevelBar = bar;
	}


	/**
	 * Gets the processlevel.
	 *
	 * @return the processlevel
	 */
	public String[] getProcesslevel() {
		return processlevelBarNames;
	}


	/**
	 * Sets the processlevel.
	 *
	 * @param processlevel the new processlevel
	 */
	public void setProcesslevel(String[] processlevel) {
		this.processlevelBarNames = processlevel;
	}


	/**
	 * Gets the fields.
	 *
	 * @return the fields
	 */
	public FormField[][] getFields() {
		return fields;
	}


	/**
	 * Sets the fields.
	 *
	 * @param fields the new fields
	 */
	public void setFields(FormField[][] fields) {
		this.fields = fields;
	}


	/**
	 * Gets the form.
	 *
	 * @return the form
	 */
	public FlexForm getForm() {
		return form;
	}


	/**
	 * Sets the form.
	 *
	 * @param form the new form
	 */
	public void setForm(FlexForm form) {
		this.form = form;
	}
    
 
	/**
	 * Gets the formstyle.
	 *
	 * @return the formstyle
	 */
	public FormStyle getFormstyle() {
		return formstyle;
	}

	/**
	 * Sets the formstyle.
	 *
	 * @param formstyle the new formstyle
	 */
	public void setFormstyle(FormStyle formstyle) {
		this.formstyle = formstyle;
	}
	
	public LinkedHashMap<String,FormField[][]> getGroupingWiseFormFields(){
//		Console.log("getGroupingWiseFormFields "+fields.length);
//		System.out.println("getGroupingWiseFormFields "+fields.length);
		
		LinkedHashMap<String,FormField[][]> hmap=new LinkedHashMap<String,FormField[][]>();
		int row=0;
		int col=0;
		String title="";
		
		FormField[][] fields1=new FormField[fields.length][];
		
		for (int i = 0; i < fields.length; i++){
			
			if (fields[i][0].getWidgetType() == FieldType.Grouping) {
				InlineLabel lbl = new InlineLabel(fields[i][0].getLabel());
				title=lbl.getText();
				row=0;
//				Console.log("KEY TITLE "+title);
//				System.out.println("KEY TITLE  "+title);
				fields1=new FormField[fields.length][];
				
				/**
				 * @author Anil
				 * @since 03-12-2020
				 * for skipping group heading which comes under tab
				 */
				if (fields[i][0].isKeyField==false) {
//					Console.log("GROUPING TITLE "+title);
					continue;
				}
			}
			fields1[row]=new FormField[fields[i].length];
			col=0;
			
			
			for (int j = 0; j < fields[i].length; j++) {
				fields1[row][col]=fields[i][j];
//				Console.log("(ROW,COL) :::  ("+i+","+j+") "+"(row,col) :::  ("+row+","+col+")"+" "+fields1[row][col].getLabel());
//				System.out.println("(ROW,COL) :::  ("+i+","+j+") "+"(row,col) :::  ("+row+","+col+")"+" "+fields1[row][col].getLabel());
				hmap.put(title, fields1);
				col++;
			}
			row++;
		}
		
		/**
		 * 
		 */
		
		LinkedHashMap<String,FormField[][]> updatedMap=new LinkedHashMap<String,FormField[][]>();
		for (Map.Entry<String, FormField[][]> entry : hmap.entrySet()) {
//			System.out.println(" ");
//			Console.log(" ");
//			System.out.println("Key = " + entry.getKey());
//			Console.log("Key = " + entry.getKey());
			
			FormField[][] formF=new FormField[entry.getValue().length][];
			try{
				int rowSize=0; 
				for(int i=0;i<entry.getValue().length;i++){
					
					if(entry.getValue()[i]==null){
						
						formF=new FormField[rowSize][];
						int r=0;
						int c=0;
						for(int j=0;j<rowSize;j++){
							c=0;
							formF[r]=new FormField[entry.getValue()[j].length];
							for (int k = 0; k < entry.getValue()[j].length; k++) {
								formF[r][c]=entry.getValue()[j][k];
								c++;
							}
							r++;
						}
						
						updatedMap.put(entry.getKey(), formF);
						
//						System.out.println("updatedMap = " + formF.length);
//						Console.log("updatedMap = " + formF.length);
						break;
					}
					rowSize++;
				}
				
			}catch(NullPointerException e){
				System.out.println("CATCH " + entry.getKey());
			}
		}
		return updatedMap;
		
		/**
		 * 
		 */
		
//		return hmap;
	}
	
	private boolean isKeyField(FormField[][] value) {
		for(int i=0;i<value.length;i++){
			
			if (value[i][0].getWidgetType() == FieldType.Grouping) {
				InlineLabel lbl = new InlineLabel(value[i][0].getLabel());
				String title=lbl.getText();
				if(value[i][0].isKeyField==true){
//					Console.log("KEY FIELD TITLE "+title);
//					System.out.println("KEY FIELD TITLE "+title);
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void refreshTableData(){
		
	}

	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateModel(T model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(T model) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("deprecation")
	public void addClickEventOnActionAndNavigationMenus(){
		if(appdrawerPanel==null){
			return;
		}
		Console.log("addClickEventOnActionAndNavigationMenus");
		AppMemory.getAppMemory().skeleton.appdrawer.removeClickListener(new ClickListener() {
			
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.appdrawer.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
				
				genService.ping(new AsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
//						AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
						
						processMenuPopup.getVerticalPanel().clear();
						processLevelBar.content.getElement().setId("login-form");
						processLevelBar.setUpdatedCSS();
						
						if(processLevelBar.isActionMenuActive()){
							
							processLevelBar.hideNavigationMenuPanel();
							processLevelBar.showActionMenuPanel();
							
//							AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(processLevelBar.content);
							
							processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//							Console.log("Window H :: "+Window.getClientHeight()+" WW "+Window.getClientWidth());
							
//							Console.log("getNumberOfActiveActionButtons 1 :: "+processLevelBar.getNumberOfActiveActionButtons());
//							int height=250;
//							if(processLevelBar.getNumberOfActiveActionButtons()!=0){
//								height=(int) (24.5*processLevelBar.getNumberOfActiveActionButtons());
//							}
//							Console.log("height 1 :: "+height);
//							
//							for(String actionBtn:processLevelBar.getActionMenuNames()){
//								double fixedLength=26.0;
//								int factorRate=9;
//								int contentLength=actionBtn.length();
//								if(contentLength>26){
//									double extraLine=contentLength/fixedLength;
//									extraLine=Math.ceil(extraLine);
//									if(extraLine==4){
//										factorRate=11;
//									}else if(extraLine==3){
//										factorRate=10;
//									}else if(extraLine==2){
//										factorRate=9;
//									}
//									height=(int) (height+(extraLine*factorRate));
//								}
//							}
							
//							if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//								AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//							}else{
//								AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//							}
//							AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
//							AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
//							Console.log("ACTION MENU HEIGHT 1 :: "+height);
							
//							Console.log("ACTION MENU HEIGHT  :: "+height+" / "+processLevelBar.content.getOffsetHeight()+" / "+processLevelBar.content.getElement().getClientHeight()+" / "+processLevelBar.content.getElement().getOffsetHeight()+" / "+processLevelBar.content.getParent().getOffsetHeight()+" / "+processLevelBar.content.getParent().getElement().getClientHeight());
//							Console.log("HEIGHT  :: "+processMenuPopup.getOffsetHeight()+" / "+processMenuPopup.getElement().getOffsetHeight()+" / "+processMenuPopup.getElement().getClientHeight()+" / "+processMenuPopup.asWidget().getOffsetHeight()+" / "+processMenuPopup.asWidget().getParent().getOffsetHeight());
//							Console.log("HEIGHT11  :: "+processMenuPopup.getVerticalPanel().getOffsetHeight()+" / "+processMenuPopup.getVerticalPanel().getElement().getOffsetHeight()+" / "+processMenuPopup.getVerticalPanel().getElement().getClientHeight());
							
							
//							if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//								Console.log("ACTION MENU HEIGHT 1 :: "+processMenuPopup.getVerticalPanel().getElement().getClientHeight());
//								processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//							}else{
////								processMenuPopup.setHeight(height+"px");
//							}
							appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
							appdrawerPanel.show();
							
							Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
							    public void execute()
							    {
							        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
							        Console.log("Scheduler :: "+offsetHeight);
							        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
							    }
							});
							
						}else{
//							AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
							appdrawerPanel.hide();
						}
						
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
							showDialogMessage("Server unreachable, Please check your internet connection!");
							if(glassPanel!=null){
								hideWaitSymbol();
							}
				        }
					}
				});
				
				
				
				
			}
		});
		
		
		AppMemory.getAppMemory().skeleton.navigationBtn.removeClickListener(new ClickListener() {
			
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.navigationBtn.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
				
//				genService.ping(new AsyncCallback<Void>() {
//					@Override
//					public void onSuccess(Void result) {
//						// TODO Auto-generated method stub
//					}
//					
//					@Override
//					public void onFailure(Throwable caught) {
//						// TODO Auto-generated method stub
//						if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
//							showDialogMessage("Server unreachable, Please check your internet connection!");
//							if(glassPanel!=null){
//								hideWaitSymbol();
//							}
//				        }
//					}
//				});
				
//				AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
				processMenuPopup.getVerticalPanel().clear();
				processLevelBar.content.getElement().setId("login-form");
				processLevelBar.setUpdatedCSS();
				
				if(processLevelBar.isNavigationMenuActive()){
					processLevelBar.hideActionMenuPanel();
					processLevelBar.showNavigationMenuPanel();
					
//					int height=250;
//					if(processLevelBar.getNumberOfActiveNavigationButtons()!=0){
//						height=(int) (24.5*processLevelBar.getNumberOfActiveNavigationButtons());
//					}
//					
//					/**
//					 * @author Anil
//					 * @since 04-01-2021
//					 */
//					for(String actionBtn:processLevelBar.getNavigationMenuNames()){
//						double fixedLength=26.0;
//						int factorRate=9;
//						int contentLength=actionBtn.length();
//						if(contentLength>26){
//							double extraLine=contentLength/fixedLength;
//							extraLine=Math.ceil(extraLine);
//							
//							if(extraLine==4){
//								factorRate=11;
//							}else if(extraLine==3){
//								factorRate=10;
//							}else if(extraLine==2){
//								factorRate=9;
//							}
//							height=(int) (height+(extraLine*factorRate));
//						}
//					}
					
//					AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//					if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//					}
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 256,Window.getClientHeight() - (Window.getClientHeight() - 90));
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
					
					processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//					if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						processMenuPopup.setHeight(height+"px");
//					}
					
					
					appdrawerPanel.setPopupPosition(Window.getClientWidth() - 256,Window.getClientHeight() - (Window.getClientHeight() - 90));
					appdrawerPanel.show();
					
					Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
					    public void execute()
					    {
					        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
					        Console.log("Scheduler :: "+offsetHeight);
					        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
					    }
					});
					
				}else{
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
					appdrawerPanel.hide();
				}
			}
		});
	}
	
	private void highlightTab() {
		try{
			for (Map.Entry<String, FormField[][]> entry : groupedFormFieldsMap.entrySet()) {
				FormField[][]fields=entry.getValue();
				if(!isKeyField(entry.getValue())){
					if(!validateTab(fields)){
						if(tabPanel!=null&&tabPanel.getTabBar().getTabCount()!=0){
							for(int i=0;i<tabPanel.getTabBar().getTabCount();i++){
								String tabName=tabPanel.getTabBar().getTabHTML(i);
								Console.log("tabName "+tabName);
								if(tabName.contains("<div class=")){
									try{
										tabName=tabName.split(">")[1];
										tabName=tabName.split("<")[0];
									}catch(Exception e){
										
									}
								}
								Console.log("TAB "+tabName);
								if(tabName.equals(entry.getKey())){
									String style= "<div class='gwt-Label' style='white-space: nowrap;background: red;' >"+tabName+"</div>";
									tabPanel.getTabBar().setTabHTML(i, style);
								}
							}
						}
					}
					else{
						
						/**
						 * @author Vijay Date :- 05-08-2022
						 * Des :- bug when grouping  tab is red then it will remain red when mandatory data also updated
						 * to resolve this issue updated code below
						 */
						if(tabPanel!=null&&tabPanel.getTabBar().getTabCount()!=0){
							for(int i=0;i<tabPanel.getTabBar().getTabCount();i++){
								String tabName=tabPanel.getTabBar().getTabHTML(i);

								if(tabName.contains("background: red;") || tabName.contains("background:red;")){
									if(tabName.contains("<div class=")){
										try{
											tabName=tabName.split(">")[1];
											tabName=tabName.split("<")[0];
										}catch(Exception e){
											
										}
									}
									if(tabName.equals(entry.getKey())){
										String style= "<div class='gwt-Label' style='white-space: nowrap;background: white;' >"+tabName+"</div>";
										tabPanel.getTabBar().setTabHTML(i, style);
										String style2= "<div class='gwt-Label' style='white-space: nowrap;' >"+tabName+"</div>";
										tabPanel.getTabBar().setTabHTML(i, style2);
									}
									
								}

							}
						}
						/**
						 * ends here
						 */
					}
				}
			}
		}catch(Exception e){
			
		}
	}
	
	public boolean validateTab(FormField[][] formfields){
		boolean result=true;
		int row=formfields.length;
		
		for(int k=0;k<row;k++)
		{
			int column=formfields[k].length;
			for(int i=0;i<column;i++)
			{
				Widget widg=formfields[k][i].getWidget();
				
				if(formfields[k][i].getLabel()!=null&&formfields[k][i].getLabel().contains("Email"))
				{
					TextBox tb=(TextBox) widg;
					String value=tb.getText();
					boolean result1=form.validateEmail(value.trim());
					if(result1==false)
				    {
				    	if(tb.getText().trim().equals("")==false)
				    	{
				    		result=false;
				    		return false;
				    	}
			    	}
			     }	
				
				if(widg instanceof CompositeInterface)
				{
					
					CompositeInterface a= (CompositeInterface) widg;
					boolean resultadress=a.validate();
					if(resultadress==false)
						result=resultadress;	
				}
				
				
				if(formfields[k][i].isMandatory())
				{
					if(widg instanceof TextBox)
					{
						TextBox tb=(TextBox) widg;
						String value=tb.getText();
						
						if(value.equals(""))
						{
							return false;
							
						}
						
					}
					
					
					if(widg instanceof TextArea)
					{
						TextArea tb=(TextArea) widg;
						String value=tb.getText();
						
						if(value.equals(""))
						{
							return false;
							
						}
						
						
						
					}
					
					else if(widg instanceof ListBox)
					{
						ListBox lb=(ListBox) widg;
					
						int index=lb.getSelectedIndex();
						if(index==0)
						{
							return false;
							
						}
					}
					
					else if(widg instanceof ObjectListBox)
					{
						ListBox lb=(ListBox) widg;
						
						int index=lb.getSelectedIndex();
						if(index==0)
						{
							return false;
							
						}
					}
					
					else if(widg instanceof DateBox)
					{
						DateBox db=(DateBox) widg;
						Date value=db.getValue();
						if(value==null)
						{
							return false;
							
						}
					}
					
					if(widg instanceof DoubleBox)
					{
						DoubleBox db=(DoubleBox) widg;
						String value=db.getText().trim();
						if(value.equals(""))
						{
							return false;
							
						}
					}
					
					if(widg instanceof SuggestBox)
					{
						SuggestBox sb=(SuggestBox) widg;
						String value=sb.getValue();
						
						if(value.equals(""))
						{
							return false;
						}
					}
					
					if(widg instanceof LongBox)
					{
						LongBox sb=(LongBox) widg;
						Long value=sb.getValue();
						if(value==null)
						{
							return false;
						}
					}
				
					if(widg instanceof MyLongBox)
					{
						MyLongBox sb=(MyLongBox) widg;
						
						Long value=sb.getValue();
						if(value==null)
						{
							return false;
						}
					}
					
					
					if(widg instanceof IntegerBox)
					{
						IntegerBox sb=(IntegerBox) widg;
						Integer value=sb.getValue();
						if(value==null)
						{
							return false;
						}
					}
				}
			}
		}
		return result;
	}

}
