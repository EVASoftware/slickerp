package com.simplesoftwares.client.library.appstructure.tablescreen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.HasTable;
import com.simplesoftwares.client.library.ProcessLevelMenuBarPopup;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.UiScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;


/**
 * Creates a screen with table.Screen follows the data binding method as it can fill itself 
 * without presenter. That's good kamla but only saying data binding is enough as we are documenting code
 * not explaining data binding.
 * @author Kamala
 *
 * @param <T>
 */
public abstract class TableScreen<T> extends UiScreen<T> implements HasTable
{
	/**
	 * {@link SuperTable}table object for this view;
	 */
	protected SuperTable<T>superTable;
	/**
	 * Service used to provide data from server 
	 */
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	
	protected GeneralServiceAsync genService=GWT.create(GeneralService.class);
	
	
	/**
	 * Instantiates a new TableScreen
	 * @param superTable
	 */
	public TableScreen(SuperTable<T> superTable) {
		super();
		this.superTable = superTable;
		//Always be run after create Screen.
		

		isAction=false;
		if (processlevelBarNames != null) {
//			processLevelBar = new ProcessLevelBar(5, processlevelBarNames);
			processLevelBar = new ProcessLevelBar(5, processlevelBarNames,navigationProcessMenuBar,null);
			processLevelBar.setVisibleFalse(false);
			
			isAction=true;
		}

//		if (AppMemory.getAppMemory().skeleton.appdrawerPanel != null && AppMemory.getAppMemory().skeleton.appdrawerPanel.isShowing()) {
//			AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//		}
		if (appdrawerPanel != null &&appdrawerPanel.isShowing()) {
			appdrawerPanel.hide();
		}
		
	}
	
	public TableScreen() {
		super();
	}
	
	

/////////////////////////////////////////GUI CREATION///////////////////////////////////////////////
	
	@SuppressWarnings("deprecation")
	/**
	 * adds the table on content and process level bar if it is not null
	 */
	@Override
	protected void createGui() {
		
		
		if(processLevelBar!=null){
			if(!AppMemory.getAppMemory().enableMenuBar){
				content.add(processLevelBar.content);
			}else{
				addClickEventOnActionAndNavigationMenus();
			}
		
		}
		
		
		content.add(superTable.content);
		
		applyStyle();
		
	}
//////////////////////////////////////////CSS STYLING///////////////////////////////////////////////
	/**
	 * applies the style on screen
	 */
	
	public void applyStyle() {
		//content.getElement().setId("serviceview");
	}

///////////////////////////////////ABSTRACT METHODS(OVERRIDEN)///////////////////////////////////////////////

	/**
	 * Retrives data from server depending upon the querry.
	 */
	@Override
	public void retriveTable(MyQuerry querry) {
		superTable.connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
//				System.out.println("in side retrive method");
				
//				if(result.get(0) instanceof Service)
//				{
//					System.out.println("in side condition of method");
//					result = sortingTbaleBySrNoAndContractNo(result);
//				}
				
				/**
				 * rohan added this code for showing msg on Click of Approval if Approval list size is 0 then show MSG
				 * Date : 24/2/2017
				 * Release: 3 march 
				 */
				Console.log("Rohan Inside constructor result size"+result.size());
				if(result.size()==0)
				{
					showDialogMessage("No request is pending for approval..!");
				}
				
				/** this if condition added by vijay for apporoval data is load by request date sorting 
				 * 
				 */
				
				else if(result.size()!=0){
				if(result.get(0) instanceof Approvals){
					System.out.println(" hi vijay you are here now for approvals  == ");
					
					ArrayList<Approvals> approvallist = new ArrayList<Approvals>();
					
					for(SuperModel model:result){
						
						Approvals approval = (Approvals) model;
						approvallist.add(approval);
					}
					
					Collections.sort(approvallist, new Comparator<Approvals>() {

						@Override
						public int compare(Approvals o1, Approvals o2) {
							
							Date date1 = o1.getCreationDate();
							Date date2 = o2.getCreationDate();
							
							return date2.compareTo(date1);
						}
					});
					
					result.clear();
					result.addAll(approvallist);
					System.out.println(" done here vijay");
					
				}
				/**
				 * @author Anil
				 * @since 20-05-2020
				 */
				if(result.get(0) instanceof Service){
					ArrayList<Service> serviceList = new ArrayList<Service>();
					for(SuperModel model:result){
						Service service = (Service) model;
						service.setRecordSelect(false);
						serviceList.add(service);
					}
					result.clear();
					result.addAll(serviceList);
				}
				
				/**
				 * @author Anil @since 30-03-2021
				 * Sorting bills in descending orders
				 */
				if(result.get(0) instanceof BillingDocument){
					
					Collections.sort(result, new Comparator<SuperModel>() {
						@Override
						public int compare(SuperModel o1, SuperModel o2) {
							Integer date1 = o1.getCount();
							Integer date2 = o2.getCount();
							return date2.compareTo(date1);
						}
					});
				}
				
				}
				/***************** end here *******************************/
				
				
				for(SuperModel model:result)
				{
					
					superTable.getListDataProvider().getList().add((T) model);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		});
		
	}
	
	
	private ArrayList<SuperModel> sortingTbaleBySrNoAndContractNo(ArrayList<SuperModel> result) {
		System.out.println("Rohan inside method ");
		
		ArrayList<Service> serlist = new ArrayList<Service>(); 
		ArrayList<SuperModel> supModel = new ArrayList<SuperModel>(); 
		
		for(SuperModel model : result)
		{
			Service ser = (Service) model;
			serlist.add(ser);
		}
		System.out.println("Service list size "+serlist.size());
			Comparator<Service> comp1 =new Comparator<Service>() {
				
				@Override
				public int compare(Service o1, Service o2) {
					Integer srNo1 = o1.getServiceSerialNo();
					Integer srNo2 = o2.getServiceSerialNo();
					return srNo1.compareTo(srNo2);
				}
			};
			Collections.sort(serlist , comp1);
		
		
			
		Comparator<Service> comp2 =  new Comparator<Service>() {
			
			@Override
			public int compare(Service p1, Service p2) {
				
				Integer con1 = p1.getContractCount();
				Integer con2 = p2.getContractCount(); 
				return con1.compareTo(con2);
			}
		};
		Collections.sort(serlist ,comp2);
		
		
		
		for(Service service : serlist)
		{
			SuperModel ser = (SuperModel) service;
			supModel.add(ser);
		}
		
		return supModel;
		
	}
	
	
	/**
	 * clears the table 
	 */
	@Override
	public void clear() {
		//No implementation needed
		
//		AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
//		AppMemory.getAppMemory().skeleton.appdrawerPanel.clear();
	}
	/**
	 * validates the table.The table validation is will always depend heavily on requiremnt.So we provided a
	 * default true
	 */

	
	@Override
	public boolean validate() {
		
		
		return true;
	}
	/**
	 * enable otr disable the screen.
	 * @param state if true enables the screen if false disable
	 */

	@Override
	public void setEnable(boolean state) {
		superTable.setEnable(state);		
	}



	public SuperTable<T> getSuperTable() {
		return superTable;
	}



	public void setSuperTable(SuperTable<T> superTable) {
		this.superTable = superTable;
	}
	
	
	@Override
	public void createScreen() {
		
		
	}

	@Override
	public void updateModel(T model) {
		
		
	}

	@Override
	public void updateView(T model) {
		
		
	}
	
	@SuppressWarnings("deprecation")
	public void addClickEventOnActionAndNavigationMenus(){
		AppMemory.getAppMemory().skeleton.appdrawer.removeClickListener(new ClickListener() {
			
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.appdrawer.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
				
				genService.ping(new AsyncCallback<Void>() {
					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						
//						AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
						processMenuPopup.getVerticalPanel().clear();
						processLevelBar.content.getElement().setId("login-form");
						processLevelBar.setUpdatedCSS();
						
						if(processLevelBar.isActionMenuActive()){
							processLevelBar.hideNavigationMenuPanel();
							processLevelBar.showActionMenuPanel();
							
//							int height=250;
//							if(processLevelBar.getNumberOfActiveActionButtons()!=0){
////								height=23*processLevelBar.getNumberOfActiveActionButtons();
//								height=(int) (24.5*processLevelBar.getNumberOfActiveActionButtons());
//							}
//							/**
//							 * @author Anil
//							 * @since 04-01-2021
//							 */
//							for(String actionBtn:processLevelBar.getActionMenuNames()){
//								double fixedLength=26.0;
//								int factorRate=9;
//								int contentLength=actionBtn.length();
////								Console.log("contentLength :: "+contentLength+" Height : : "+height);
//								if(contentLength>26){
//									double extraLine=contentLength/fixedLength;
////									Console.log("extraLine :: "+extraLine);
//									extraLine=Math.ceil(extraLine);
//									
//									if(extraLine==4){
//										factorRate=11;
//									}else if(extraLine==3){
//										factorRate=10;
//									}else if(extraLine==2){
//										factorRate=9;
//									}
//									
//									height=(int) (height+(extraLine*factorRate));
////									Console.log("extraLine :: "+extraLine+" height :: "+height);
//								}
//							}
							
//							Console.log(" height :: "+height);
//							AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//							if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//								AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//							}else{
//								AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//							}
//							AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
//							AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
							
							
							processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//							Console.log(" height :: "+height);
//							if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//								Console.log(" height1 :: "+processMenuPopup.getVerticalPanel().getElement().getClientHeight());
//								processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//							}else{
//								processMenuPopup.setHeight(height+"px");
//							}
							
							appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
							appdrawerPanel.show();
							
							Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
							    public void execute()
							    {
							        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
							        Console.log("Scheduler :: "+offsetHeight);
							        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
							    }
							});
							
						}else{
//							AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
							appdrawerPanel.hide();
						}
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
							showDialogMessage("Server unreachable, Please check your internet connection!");
							if(glassPanel!=null){
								hideWaitSymbol();
							}
				        }
					}
				});
				
				
				
			}
		});
		
		
		AppMemory.getAppMemory().skeleton.navigationBtn.removeClickListener(new ClickListener() {
			
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.navigationBtn.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
//				AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
				processMenuPopup.getVerticalPanel().clear();
				processLevelBar.content.getElement().setId("login-form");
				processLevelBar.setUpdatedCSS();
				if(processLevelBar.isNavigationMenuActive()){
					processLevelBar.hideActionMenuPanel();
					processLevelBar.showNavigationMenuPanel();
					
//					int height=250;
//					if(processLevelBar.getNumberOfActiveNavigationButtons()!=0){
////						height=23*processLevelBar.getNumberOfActiveNavigationButtons();
//						height=(int) (24.5*processLevelBar.getNumberOfActiveNavigationButtons());
//					}
//					
//					/**
//					 * @author Anil
//					 * @since 04-01-2021
//					 */
//					for(String actionBtn:processLevelBar.getNavigationMenuNames()){
//						double fixedLength=26.0;
//						int factorRate=9;
//						int contentLength=actionBtn.length();
//						Console.log("contentLength :: "+contentLength+" Height : : "+height);
//						if(contentLength>26){
//							double extraLine=contentLength/fixedLength;
//							Console.log("extraLine :: "+extraLine);
//							extraLine=Math.ceil(extraLine);
//							
//							if(extraLine==4){
//								factorRate=11;
//							}else if(extraLine==3){
//								factorRate=10;
//							}else if(extraLine==2){
//								factorRate=9;
//							}
//							
//							height=(int) (height+(extraLine*factorRate));
//							Console.log("extraLine :: "+extraLine+" height :: "+height);
//						}
//					}
					
//					AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//					if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//					}
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 256,Window.getClientHeight() - (Window.getClientHeight() - 90));
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
					
					processMenuPopup.getVerticalPanel().add(processLevelBar.content);
//					if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						processMenuPopup.setHeight(height+"px");
//					}
					
					appdrawerPanel.setPopupPosition(Window.getClientWidth() - 256,Window.getClientHeight() - (Window.getClientHeight() - 90));
					appdrawerPanel.show();
					
					Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
					    public void execute()
					    {
					        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
					        Console.log("Scheduler :: "+offsetHeight);
					        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
					    }
					});
					
				}else{
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
					appdrawerPanel.hide();
				}
			}
		});
	}
	

}
