package com.simplesoftwares.client.library.appstructure;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.HasTable;
import com.simplesoftwares.client.library.appstructure.formtablescreen.FormTableScreen;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.composite.UploadComposite;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;
import com.slicktechnologies.client.utils.Console;

/**
 * This class creates a pop view for search functionality.Objects of this class are responsible for displaying 
 * the view when search is clicked.
 * To Do: Naturally master entity of this class is MyQuerry.Change as per.
 * @author Kamla
 * @param <T>
 */
public abstract class SearchPopUpScreen<T> extends FormTableScreen<T> implements HasTable{
	
	/**
	 * popup panel on which search ui goes.
	 */
	protected PopupPanel popup;
	/**
	 * download on click of this label  the table data is downloaded in xl format.
	 * To Do : change type in widget so that we can use icons in future
	 */
	protected InlineLabel dwnload;
	/**
	 *  on click of this label  the search table fills.
	 *  To Do : change type in widget so that we can use icons in future
	 * 
	 */
	protected InlineLabel golbl;
	/**
	 * Horizontal panle to hold go and download
	 */
	public HorizontalPanel horizontal;
	/**
	 * Default height of table
	 */
	private String DEFAULT_HEIGHT="350px";
	/**
	 * This attribute decides weather the result table is on pop up or on form.
	 */
	public  boolean TABLE_ON_POPUP=true;
	
	/**
	 * Used to set table without object creation.Mostly a hack.
	 */
	public static Object staticSuperTable;
	
	FormStyle frmStyl;
	
	InlineLabel lblCount;//Ashwini Patil Date:4-03-2023
	
   /**
    * Instatiate a new search pop up
    * To Do : Komal change the constructors as in date box implementation
    * @param table the table on which result will be plased.It can be on form on pop up.
    */
	public SearchPopUpScreen(SuperTable<T> table)
	{
		super(table,FormStyle.DEFAULT);
		horizontal=new HorizontalPanel();
		popup=new PopupPanel(true);
		golbl=new InlineLabel("Go");
		dwnload=new InlineLabel("Download");
		lblCount=new InlineLabel("Rows:");
	}
	
	//An elegent hack
	

	public SearchPopUpScreen()
	{
		this((SuperTable<T>) staticSuperTable);
		
	}
	
	public SearchPopUpScreen(boolean mode)
	{
		this((SuperTable<T>) staticSuperTable,mode);
		
	}
	
	
	/**
	 * Instantiate with table and mode
	 * @param table the table on which result will stick
	 * @param mode mode which decides table will stick on pop up or not.
	 */
	
	public SearchPopUpScreen(SuperTable<T> table,boolean mode)
	{
		this(table);
		TABLE_ON_POPUP=mode;
	}
	
	public SearchPopUpScreen(FormStyle formStyle) {
		super((SuperTable<T>) staticSuperTable,formStyle);
		frmStyl=formStyle;
		Console.log("Conside simplified search screen constructor : "+frmStyl);
		horizontal=new HorizontalPanel();
		popup=new PopupPanel(true);
		golbl=new InlineLabel("Go");
		dwnload=new InlineLabel("Download");
		lblCount=new InlineLabel("Rows:");
	}
/////////////////////////////////////////////////ABSTRACT METHODS///////////////////////////////////////////////////
	

	/**
	 * The method is implemented by concrete Which will crete a {@link MyQuerry} object corresponding to SearchPopUp
	 * @return {@link MyQuerry} object
	 */
	public abstract MyQuerry getQuerry();
	
	
///////////////////////////////////////////rohan added this abstract method for validate search pop-up //////////	
	
	public abstract boolean validate();
	
///////////////////////////////////////////////////CSS STYLING//////////////////////////////////////////////////////
	
	/**
	 * 
	 * @param w applies the style on widget
	 */
	protected void setStyling(Widget w)
	{
		w.getElement().setClassName("tablecaption");
		
	}

	/**
	 * Sets the styling
	 */
	
	@Override
	public void applyStyle() {
	
		content.getElement().setId("formcontent");
		form.getElement().setId("form");
		horizontal.getElement().addClassName("centering");
		dwnload.getElement().setId("addbutton");
		golbl.getElement().setId("addbutton");
		
		lblCount.getElement().setId("addlabel");
		
		popup.getElement().setId("searchpopup");
		/**
		 *  nidhi
		 *  5-08-2017
		 *  for make screen responsive class 
		 *  simpleERP.css
		 */
		popup.getElement().addClassName("searchviewpopup");
		/**
		 * end
		 */
		if(supertable!=null&&supertable.getTable()!=null) //condition added on 20-12-2023 as screen was getting freezed sometimes while saving Quick employee from implementation screen
		supertable.getTable().setHeight("400px");
		scrollpanel.setHeight("500px");
		popup.center();
		
		
		
		
	}
	
///////////////////////////////////////////GUI CREATION//////////////////////////////////////////////////
	//Idealy we should have passed the table and form should have been created as pop up is Form Table Screen
	//This shows limitation of design as no of ui elements to be added is fixed.
	
	/**
	 * Sticks Form or FormTable content on popup.
	 */
	
	@Override
    public void createGui()
	{
		/**
		 * @author Ashwini Patil 
		 * @since 19-04-2022 
		 * In case of ENABLEMENUBAR=false contract and Quick contract screen froms were not getting loaded due to exception.
		 * added condition tabPanel!=null
		 */
		
		if(frmStyl!=null&&(frmStyl!=formstyle.DEFAULT||frmStyl!=FormStyle.TABLEFORMLAYOUT)&&tabPanel!=null){ 
			content.add(tabPanel);
			tabPanel.getElement().getStyle().setMarginTop(10, Unit.PX);
			horizontal.getElement().getStyle().setMarginTop(15, Unit.PX);
			supertable.content.getElement().getStyle().setMarginTop(10, Unit.PX);
		}else{
			content.add(form);
		}
		horizontal.add(golbl);
		horizontal.add(dwnload);
		horizontal.add(lblCount);
		content.add(horizontal);
		if(TABLE_ON_POPUP)
			addTable();
		
		applyStyle();
		
		
		popup.add(content);
		popup.hide();
		
	}

///////////////////////////////////////POPUP RELATED METHODS/////////////////////////////////////////////// 
	/**
	 * shows the search popup.
	 * 
	 */
	public void showPopUp()
	{
		/**
		 * @author Anil
		 * @since 21-02-2022
		 * Nitin sir raised this requirement, search data is not going to delete similarly search filter should not get cleared
		 */
		clear(); //Ashwini Patil Date:23-09-2022 Description: As per Pecopp requirement and Nitin sir's instruction clearing all the filters.
		popup.show();
		popup.setSize("100%", "100%");
		/*popup.setSize("1150px","600px");*/
		popup.center();
		popup.setAnimationEnabled(true);
		
		/**
		 * Date :11-12-2017 BY ANIL
		 * for maintaining search data in search table.
		 * Merged on 26-03-2018
		 */
		if(supertable.getValue()!=null){
			supertable.getListDataProvider().refresh();
			lblCount.setText("Rows: "+supertable.getListDataProvider().getList().size());
		}
		/**
		 * End
		 */
		
	}
	
	/**
	 * Hides the Popn Up
	 */
	public void hidePopUp()
	{
		popup.hide();
	}
	
/////////////////////////////////////////APPLYING HANDLER ON "GO" & "DOWNLOAD" LABELS///////////////////////
	
	/**
	 * Applies handlers on go and download widgets
	 * @param handler handler object which will handel click events on these widgets
	 */
	public void applyHandler(ClickHandler handler)
	{
		golbl.addClickHandler(handler);
		dwnload.addClickHandler(handler);
	}
	
	@Override
	public void clear()
	{
		super.clear();
		/**
		 * Date : 11-12-2017 BY ANIL
		 * commented this code
		 * search data should not be cleared until the new search query is executed 
		 * Merged on 26-03-2018
		 */
//		if(supertable!=null&&TABLE_ON_POPUP==true)
//			supertable.clear();
		/**
		 * End
		 */
	}
	
	



	///////////////////////////////////////////////////////GETTER SETTERS///////////////////////////////////////////
	public PopupPanel getPopup() {
		return popup;
	}

	public void setPopup(PopupPanel popup) {
		this.popup = popup;
	}

	public InlineLabel getDwnload() {
		return dwnload;
	}

	public void setDwnload(InlineLabel dwnload) {
		this.dwnload = dwnload;
	}

	public InlineLabel getGolbl() {
		return golbl;
	}

	public void setGolbl(InlineLabel golbl) {
		this.golbl = golbl;
	}

	public HorizontalPanel getHorizontal() {
		return horizontal;
	}

	public void setHorizontal(HorizontalPanel horizontal) {
		this.horizontal = horizontal;
	}

	public Label getLblCount() {
		return lblCount;
	}

	public void setLblCount(InlineLabel lblCount) {
		this.lblCount = lblCount;
	}


	@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void updateModel(T model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(T view) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Date  : 11-12-2017 BY ANIL
	 * This method is used to update the search data 
	 */
	@SuppressWarnings("unchecked")
	public void updateSearchTableData(SuperModel model){
		System.out.println("INSide Update Search Data");
		if(supertable.getValue()!=null){
			for(int i=0;i<supertable.getValue().size();i++){
				SuperModel entity=(SuperModel) supertable.getValue().get(i);
				if(entity.getCount()==model.getCount()){
					supertable.getValue().set(i, (T) model);
				}
				
			}
		}
	}
	
  
}
