package com.simplesoftwares.client.library.appstructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.InlineLabel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.services.SessionService;
import com.slicktechnologies.client.services.SessionServiceAsync;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.Screen;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.service.ServiceForm;
import com.slicktechnologies.client.views.service.ServicePresenter;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;


/**
 * Entity presenter class handles all the crude on data in an entity independent way maintaining the flow.
 * This is an mvc style controller at.The concrete implementations can choose to implement it as mvc or mvp.
 * @author Kamala
 *
 * @param <T>
 */
public abstract class EntityPresenter<T extends SuperModel> implements ClickHandler {
	
	/**
	 * Refrence of view
	 * @author Kamala
	 */
	protected UiScreen<T> view;
	/**
	 * Refrence of model
	 */
	protected T model;
	/**
	 * Refernce of Appmemory
	 */
	protected AppMemory mem;
	/**
	 * The service corresponding to current presenter.Presenter acess the save ,delete ,search methods
	 * through this service
	 */
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	
	/**
	 * rohan added this on Date : 07/04/2017
	 * User for making RPC to check wether session is active or not
	 */
	private final static SessionServiceAsync sessionService=GWT.create(SessionService.class);
	
	/**
	 * @author Anil @since 17-02-2021
	 */
	protected GeneralServiceAsync genService=GWT.create(GeneralService.class);

	/**
	 * Initializes the presenter
	 * @param view
	 * @param model
	 */
	
	public EntityPresenter(UiScreen<T> view,T model)
	{
		this.view=view;
		this.model=model;
		//Set Presenter
		view.setPresenter(this);
		mem=AppMemory.getAppMemory();
		setEventHandeling();
		if(view.getSearchpopupscreen()!=null)
			view.getSearchpopupscreen().applyHandler(this);
		
		InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
		for(int k=0;k<menus.length;k++)
		{
			String text=menus[k].getText();
			if(text.contains("Rows:")){
				menus[k].setText("Rows: 0");	
				menus[k].getElement().setId("addlabel2");
			}
		}
	}

////////////////////////////////////////////ABSTRACT METHODS//////////////////////////////////////
	/**
	 * This method is responsible for saving the entity.
	 * To Do : make this non abstract as per pradnya
	 */
	public abstract void reactOnSave();
	/**
	 * Crucial method only concrete classes will provide the implementation.This is the 
	 * only point in this flow where buisness logic will be managed.
	 * 
	 */
	
	public abstract void reactToProcessBarEvents(ClickEvent e);
	/**
	 * Method which DownLoads any Table with header
	 */
	
	public  void reactOnDownload()
	{
		
	}
	
	/**
	 * Abstract method which will be called when user click print in app level menu
	 */
	
	public abstract void reactOnPrint();
	/**
	 * Abstract method which will be called when user click email in app level menu
	 */
	
	public abstract void reactOnEmail();
	/**
	 * Abstract method which reinitalizes the mmodel.
	 */
	
	//****************this method is use for navigation **********

	protected abstract void reactOnHistory();
	
	//***************ends here ********************
	
	protected  abstract void makeNewModel();	
	
	/**
	 * Date 13 April 2017 added by vijay for communication log
	 * This method  will called when user click on Communication Log in app level menu
	 */
	
	public void reactOnCommunicationLog(){
		
	}
	
	/**
	 * ends here
	 */
	
	/**
	 * 
	 */
	
	public void reactOnSMS(){
		
	}
	
//////////////////////////////VALIDATE AND UPDATE THE MODEL/////////////////////////////////////////	
	/**
	 * This method first validates the view.If validation is true then it fills the corresponding data
	 * in model.
	 */
	protected boolean validateAndUpdateModel()
	{
		if(view.validate()==false)
				return false;
		
		view.updateModel(model);
		System.out.println("ID OF MODEL"+model.getId());
		    return true;
	}
	
//////////////////////////////SETTING HANDLERS ON APPHEADERMENUS & PROCESSLEVEL MENUS///////////////////////////////
	
	/**
	 * The method sets the click handler.On the app level menus.
	 * 
	 * 
	 */
	
	protected void setEventHandeling() {
		if(!view.isPopUpAppMenubar()){
			for (int k = 0; k < mem.skeleton.menuLabels.length; k++) {
				if (mem.skeleton.registration[k] != null)
					mem.skeleton.registration[k].removeHandler();
			}
	
			for (int k = 0; k < mem.skeleton.menuLabels.length; k++)
				mem.skeleton.registration[k] = mem.skeleton.menuLabels[k].addClickHandler(this);
		}

		/**
		 * @author Anil @since 25-02-2021
		 * removing click handler on process level bar buttons
		 */
		if (view.getProcessLevelBar() != null) {
			for (int k = 0; k < view.getProcessLevelBar().btnLabels.length; k++) {
				if(view.getProcessLevelBar().registration[k]!=null){
					view.getProcessLevelBar().registration[k].removeHandler();
				}
			}
		}
		
		// Applying event handler to Process Level Bar
		if (view.getProcessLevelBar() != null) {
			InlineLabel[] lbl = view.getProcessLevelBar().btnLabels;
			for (int k = 0; k < lbl.length; k++) {
				lbl[k].addClickHandler(this);
			}
		}
	}

///////////////////////////////////////REGULAR METHODS///////////////////////////////////////
	
	/**
	 * Fascilates transition to home screen.
	 */
	public void reactOnDiscard()	//DISCARD
	{
		mem.goToHome();
	}
	
	/**
	 * Fascilates transition to edit state.
	 */

	public void reactOnEdit()	//EDIT
	{
		view.setToEditState();
	}
	
	/**
	 * Shows the search pop up screen
	 */
	
	public void reactOnSearch() 	//SEARCH
	{
		if(view.getSearchpopupscreen()!=null)
		   view.getSearchpopupscreen().showPopUp();
		
	}
	
	/**
	 * Delets the entity
	 * To Do : Pradnya approch needed to be utilized
	 */
	
	public void reactOnDelete()		//DELETE
	{
		view.showWaitSymbol();
		//make model deleted
		model.setDeleted(true);
		service.delete(model, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Failed");
				
			}

			@Override
			public void onSuccess(Void result) {
				view.hideWaitSymbol();
				view.clear();
				makeNewModel();
				view.setToNewState();
				
				System.out.println("Sucess!!");
				
			}
		});
		
	}
	
	/**
	 * Retrives the results from server using querry object formed as per search criteria.
	 */
	
	protected void reactOnGo()		//GO
	{
		if(!view.getSearchpopupscreen().getForm().validateSearchPopupListFields()){
			view.showDialogMessage("Please wait ! search filters are still loading.");
			return;
		}
		if(view.getSearchpopupscreen().validate())
		{
		view.showWaitSymbol();
		MyQuerry querry=view.getSearchpopupscreen().getQuerry();
		view.getSearchpopupscreen().getSupertable().connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				System.out.println("result == "+result.size());
				if(result.size()==0){
					view.showDialogMessage("No Result To Display !");
				}
				else{
					/**
					 * @author Vijay Date :- 14-10-2022
					 * Des :- Customer Service list when no result to display then pop up should not hide
					 */
					
					Screen screenName=(Screen) AppMemory.getAppMemory().currentScreen;
					Console.log("screenName"+screenName);
					if(screenName.toString().trim().equals("SERVICE") || screenName.toString().trim().equals("INHOUSESERVICE") ||
							screenName.toString().trim().equals("CONTACTPERSONLIST") || screenName.toString().trim().equals("SERVICESECURITYDEPOSITLIST") ||
							screenName.toString().trim().equals("INTERACTIONLIST") || screenName.toString().trim().equals("PURCHASEREQUISITIONLIST") ||
							screenName.toString().trim().equals("VENDORPRODUCTLIST") || screenName.toString().trim().equals("PRODUCTINVENTORYTRANSACTION") ||
							screenName.toString().trim().equals("PRODUCTINVENTORYVIEWLIST") || screenName.toString().trim().equals("GRNLIST") || 
							screenName.toString().trim().equals("DEFECTIVEMATERIALLIST") || screenName.toString().trim().equals("INVENTORYlIST") ||
							screenName.toString().trim().equals("PRODUCTGROUPLIST") || screenName.toString().trim().equals("COMPANYASSETLIST") ||
							screenName.toString().trim().equals("PETTYCASHTRANSACTIONLIST") || screenName.toString().trim().equals("BILLINGLIST") || 
							screenName.toString().trim().equals("INVOICELIST") || screenName.toString().trim().equals("VENDORINVOICELIST") ||
							screenName.toString().trim().equals("VENDORPAYMENTLIST") || screenName.toString().trim().equals("INTERFACETALLYLIST") ||
							screenName.toString().trim().equals("PAYMENTLIST")||screenName.toString().trim().equals("EMPLOYEELEAVEHISTORY")||screenName.toString().trim().equals("COMPANYSCONTRIBUTION"))//Ashwini Patil Date:18-12-2023 added EMPLOYEELEAVEHISTORY and COMPANYSCONTRIBUTION 
					{
						SearchPopUpScreen<T>popup=view.getSearchpopupscreen();
						if(popup!=null)
						popup.hidePopUp();
					}
				}
				
				/**Date 23/2/2018
				 * By Jayshree
				 * To Sort The search result by default in decendimg Order
				 * 
				 */
				if(result.size()>0){
					if(result.get(0) instanceof BillingDocument){
						Comparator<SuperModel> comp=new Comparator<SuperModel>() {
							@Override
							public int compare(SuperModel e1, SuperModel e2) {
//								Integer coutn1=o1.getCount();
//								Integer count2=o2.getCount();
//								return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() < e2.getCount()) {
									return -1;
								} else {
									return 1;
								}
							}
						};
						Collections.sort(result,comp);

					}
					else if(result.get(0) instanceof Service){
						
						/**
						 * Date 17-4-2018
						 * By Jayshree
						 * to add the default checkbox value false 
						 */
						for(SuperModel model:result){
							Service entity=(Service) model;
							entity.setRecordSelect(false);
						}
						//end 

						Comparator<SuperModel> comp=new Comparator<SuperModel>() {
							@Override
							public int compare(SuperModel e1, SuperModel e2) {
//								Integer coutn1=o1.getCount();
//								Integer count2=o2.getCount();
//								return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() < e2.getCount()) {
									return -1;
								} else {
									return 1;
								}
							}
						};
						Collections.sort(result,comp);

					}
					else{
						Comparator<SuperModel> comp=new Comparator<SuperModel>() {
							@Override
							public int compare(SuperModel e1, SuperModel e2) {
//								Integer coutn1=o1.getCount();
//								Integer count2=o2.getCount();
//								return ;
								if (e1.getCount() == e2.getCount()) {
									return 0;
								}
								if (e1.getCount() > e2.getCount()) {
									return -1;
								} else {
									return 1;
								}
							}
						};
						Collections.sort(result,comp);

					}
				}
				
				//End By jayshree
				
				for(SuperModel model:result)
				{
					view.getSearchpopupscreen().getSupertable().getListDataProvider().getList().add((T) model);
				    //Set the row count of table as per the recieved data length
					
				}
				view.getSearchpopupscreen().getSupertable().getTable().setRowCount(result.size());
				view.getSearchpopupscreen().getLblCount().setText("Rows: "+result.size());//Ashwini Patil Date:4-04-2023
				InlineLabel[] menus=AppMemory.getAppMemory().skeleton.getMenuLabels();
				for(int k=0;k<menus.length;k++)
				{
					String text=menus[k].getText();
					if(text.contains("Rows:")){
						menus[k].setText("Rows:"+result.size());	
						menus[k].getElement().setId("addlabel2");
					}
				}
			
				view.hideWaitSymbol();
	}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				view.showDialogMessage("Failed To Retrive Search Result!");
				view.hideWaitSymbol();
			}
		});
		}
	}

/////////////////////////////////////////////////CLICK EVENT//////////////////////////////////////
	/**
	 * rohan commented this method and added to success of update session method so that 1st we will 
	 * check session is active / inactive and then allow user to do the operation they want
	 * 
	 *  Date : 08/04/2017
	 */
	
	@Override
	public void onClick(ClickEvent event) {

		if (event.getSource() instanceof InlineLabel) {
			InlineLabel lbl = (InlineLabel) event.getSource();

			if (lbl.getText().equals("Save")) {
				/**
				 * Date : 24 -03-2018 BY ANIL Uncomment this code while
				 * deploying for the first for new link creation
				 */

				// if(model instanceof Company){
				// reactOnSave();
				// }else{
				// reactOnSaveAtNewLinkCreation();
				// }

				/**
				 * End
				 */
				updateSessionLastAccessedTime("Save");
			} else if (lbl.getText().equals("Edit")) {
				reactOnEdit();
			} else if (lbl.getText().equals("Print")) {
				// reactOnPrint();
				updateSessionLastAccessedTime("Print");
			} else if (lbl.getText().equals("Search")) {
				reactOnSearch();
			} else if (lbl.getText().equals("Go")) {
				// reactOnGo();
				updateSessionLastAccessedTime("Go");
			} else if (lbl.getText().equals(AppConstants.NAVIGATION)) {
				// reactOnHistory();
				updateSessionLastAccessedTime(AppConstants.NAVIGATION);
			}else if (lbl.getText().equals("Download")) {
				// reactOnDownload();
				updateSessionLastAccessedTime("Download");
			} else if (lbl.getText().equals("Discard")) {
//				reactOnDiscard();
				updateSessionLastAccessedTime("Discard");
			} else if (lbl.getText().equals(AppConstants.COMMUNICATIONLOG)) {
//				reactOnCommunicationLog();
				updateSessionLastAccessedTime(AppConstants.COMMUNICATIONLOG);
			} else if (lbl.getText().equals("Email")) {
//				reactOnEmail();
				updateSessionLastAccessedTime("Email");
			}
			else if(lbl.getText().equals(AppConstants.MESSAGE)){
				updateSessionLastAccessedTime(AppConstants.MESSAGE);
			}
			else {	
				
				try{
					reactToProcessBarEvents(event);
				}catch(StatusCodeException e){
					if (e.getStatusCode() == 0) {
						view.showDialogMessage("Server unreachable, Please check your internet connection!");
						if(view.glassPanel!=null){
							view.hideWaitSymbol();
						}
			        }
				}
				
				
				/**
				 * @author Anil
				 * @since 28-09-2020 Hiding appdrawer panel
				 */
//				if (AppMemory.getAppMemory().skeleton.appdrawerPanel != null) {
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//				}
				
				if (view.appdrawerPanel != null) {
					view.appdrawerPanel.hide();
				}
			}
		}
	}
	
//////////////////////////////////////Rohan added Session code here //////////////////////////////////////	
	/**
	 * Rohan added session code here for termination of session on particular click only   	
	 * Date : 07-04-2017
	 * @param string Opration (can be Save, )
	 */
	
	private void updateSessionLastAccessedTime(final String operation){
		Console.log("in side slick erp update session count="+LoginPresenter.sessionCountValue+"- userName"+LoginPresenter.proxyString+"- LoggedinUser"+LoginPresenter.loggedInUser+"Number of Users--"+LoginPresenter.NumberOfUser);	
		view.showWaitSymbol();
		Company comp=new Company();
		sessionService.ping(LoginPresenter.proxyString,LoginPresenter.sessionCountValue,true,comp.getCompanyId(),LoginPresenter.loggedInUser,operation,LoginPresenter.NumberOfUser,new AsyncCallback<Integer>() {
			public void onSuccess(Integer result) {
				view.hideWaitSymbol();
				Console.log("Result =="+result);
				if(result== -1){
					LoginPresenter.displaySessionTimedOut("Session time out due to simultaneous login and exceed no of user ..!");
				}
				else if(result== -10){
					LoginPresenter.displaySessionTimedOut("Session time out on server side..!");
				}
				else if(result== -20){
					
					LoginPresenter.displaySessionTimedOut("Your session has been terminated as <User Name= "+LoginPresenter.proxyString+"> has logged in  to other terminal or session..!");
				}
				else if(result== -2){
					LoginPresenter.displaySessionTimedOut("You Have been forcefully terminated by Admin..!");
				}
				else{
					if(operation.equalsIgnoreCase("Save")){
						Console.log("ON Save");
						reactOnSave();
					}
					else if(operation.equalsIgnoreCase("Print")){
						 reactOnPrint();
					}
					else if(operation.equalsIgnoreCase("Go")){
						reactOnGo();
					}
					else if(operation.equalsIgnoreCase(AppConstants.NAVIGATION)){
						reactOnHistory();
					}
					else if(operation.equalsIgnoreCase("Download")){
						reactOnDownload();
					}
					/**
					 * @author Anil @since 24-02-2021
					 * Adding discard and communication to log button under user login status validation
					 */
					else if(operation.equalsIgnoreCase("Discard")){
						reactOnDiscard();
					}
					else if(operation.equalsIgnoreCase(AppConstants.COMMUNICATIONLOG)){
						reactOnCommunicationLog();
					}else if(operation.equalsIgnoreCase("Email")){
						reactOnEmail();
					}
					else if(operation.equalsIgnoreCase(AppConstants.MESSAGE)){
						reactOnSMS();
					}
				}
			}
			 
			public void onFailure(Throwable caught) {
				view.hideWaitSymbol();
				if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
					view.showDialogMessage("Server unreachable, Please check your internet connection!");
		        }
			}
		});
	}
	
/*
 * ends here  	
 */
//////////////////////////////////////////////GETTER SETTERS////////////////////////////////////////////////
	
	public SuperModel getModel()
	{
		return model;
	}
	
	public ViewContainer getView() {
		return view;
	}

	public void setView(UiScreen<T> view) {
		this.view = view;
	}

	public GenricServiceAsync getService() {
		return service;
	}

	public void setService(GenricServiceAsync service) {
		this.service = service;
	}

	public  void setModel(T model)
	{
		this.model=model;
	}
	
	
	/**
	 * Date : 02-04-2018 By ANIL
	 * This method is called when we are creating new link
	 */
	public void reactOnSaveAtNewLinkCreation(){

		if(validateAndUpdateModel()){
			// Async querry is going to start. We do not want user to do any thing untill it completes.
			view.showWaitSymbol();
			service.saveForNewLink(model,new AsyncCallback<ReturnFromServer>() {
				
				@Override
				public void onSuccess(ReturnFromServer result) {
					//Async run sucessfully , hide the wait symbol
					view.hideWaitSymbol();
					// set the count returned from server to this model
					model.setCount(result.count);
				   //set the id from server to this model
					model.setId(result.id);
					view.setToViewState();
					//Set count on view
					view.setCount(result.count);
					view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE ,GWTCAlert.OPTION_ANIMATION);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					view.hideWaitSymbol();
					view.showDialogMessage("Data Save Unsuccessful! Try again.");
					caught.printStackTrace();
					
				}
			});
			
		}
	}
	
	/**
	 * @author Anil @since 24-02-2021
	 * since GWT doesn't support java.net and java.lang thats why commenting this method of checking Internet connection
	 */
//	public boolean isInternetConnected(){
//		try {
//			URL url = new URL("http://www.google.com");
//			URLConnection connection = url.openConnection();
//			connection.connect();
//			System.out.println("Internet is connected");
//			return true;
//		} catch (MalformedURLException e) {
//			System.out.println("Internet is not connected");
//			return false;
//		} catch (IOException e) {
//			System.out.println("Internet is not connected");
//			return false;
//		}
//		
//		Process process = null;
//		try {
//			process = java.lang.Runtime.getRuntime().exec("ping www.google.com");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//		
//		int x = 0;
//		try {
//			x = process.waitFor();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//		if (x == 0) {
//			System.out.println("Connection Successful, " + "Output was " + x);
//			return true;
//		} else {
//			System.out.println("Internet Not Connected, " + "Output was " + x);
//			return false;
//		}
//		
//		
//	}
	
}
