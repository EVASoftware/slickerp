package com.simplesoftwares.client.library.appstructure.popupscreen;

import com.google.gwt.user.client.ui.FlowPanel;
import com.slicktechnologies.client.utils.Console;
/**
 * Date 6-08-2017 by Vijay Chougule
 * @author Vijay Chougule
 *
 */
public abstract class PopupForm {

	
	/**
	 * Flow panel content.
	 * @author Vijay
	 */
	public FlowPanel content;
	
	
	/**
	 * creates the screen. All concrete class will give its implementation.
	 * @author Vijay
	 */
	public abstract void createScreen();
	

	/**
	 * Here I am creating standard GUI if this is does not fulfill your requirement then 
	 *  concrete class will give its implementation.
	 */
	
	protected abstract void createGui();
	
	/** Date 21-12-2017 by vijay
	 * for concrete class will give implementation for this to handle edit and view state
	 * @param state
	 */
	
	public abstract void setEnable(boolean state);
	
	
	/** defult Constructor **/
	PopupForm(){
		content = new FlowPanel();
		createScreen();
	}

	/** Parameter Constructor for complain new popup create screen called manually in popup constructor**/
	PopupForm(boolean flag){
		content = new FlowPanel();
	}
	
	PopupForm(String screenName,int internalType){
		content = new FlowPanel();
		createScreen(screenName, internalType);
	}
	
	PopupForm(String screenName,int internalType,String categoryName){
		content = new FlowPanel();
		createScreen(screenName, internalType,categoryName);
	}
	
	public abstract void createScreen(String screenName,int internalType);
	
	public abstract void createScreen(String screenName,int internalType,String categoryName );
	
}
