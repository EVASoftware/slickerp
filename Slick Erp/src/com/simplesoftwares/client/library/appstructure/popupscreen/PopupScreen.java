package com.simplesoftwares.client.library.appstructure.popupscreen;

import java.util.Date;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.FlexForm;
import com.simplesoftwares.client.library.FormField;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.composite.CompositeInterface;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;
import com.simplesoftwares.client.library.mywidgets.ObjectListBox;

/**
 * Date 6-08-2017 this class is reusable for any creating popups
 * created by vijay chougule
 * @author Vijay Chougule
 *
 */
public abstract class PopupScreen extends PopupForm implements ClickHandler{
	/**
	 * popup panel 
	 */
	protected PopupPanel popup;
	
	/** The fields. 
	 * holds the formfield objects.This array is responsible for UI LAYOUT*/
	protected FormField[][]fields;
	
	/** The form. for creating the flexttable*/
	protected FlexForm form ;
	
	/** The formstyle.	decides the formStyle(rowform or coulmnform) */
	protected FormStyle formstyle;
	
	/** The default form style. */
	protected static FormStyle DEFAULT_FORM_STYLE=FormStyle.ROWFORM;
	
	/**
	 * Horizontal panel to hold OK and Cancel
	 */
	public HorizontalPanel horizontal;

	protected InlineLabel lblOk;
	protected InlineLabel lblCancel;
	
	GWTCGlassPanel glassPanel;
	
	 /**
 	 * Instantiates a new form screen.
 	 * Call to parameterized constructor with default_form_style(Rowform).
 	 */
 	protected PopupScreen()
	 {
 		
		 this(DEFAULT_FORM_STYLE);
		 
	 }

 	/**
 	 * below constructor can pass ROW form style and Column form Style 
 	 * by default Row style Form applying
 	 **/
 	
	public PopupScreen(FormStyle FORM_STYLE){
		
		super();
		this.formstyle = FORM_STYLE; 
		horizontal=new HorizontalPanel();
		popup = new PopupPanel(true);
		lblOk=new InlineLabel("OK");
		lblCancel=new InlineLabel("Cancel");
		
		form=new FlexForm(fields,formstyle);
		
		applyHandler();
		
	}
	
	/**
	 * Date 13-08-2019 by Vijay for using below constructor flexform() will not create this will called manually in popup
	 * @param FORM_STYLE
	 * @param flag
	 */
	public PopupScreen(FormStyle FORM_STYLE,boolean flag){
		
		super();
		this.formstyle = FORM_STYLE; 
		horizontal=new HorizontalPanel();
		popup = new PopupPanel(true);
		lblOk=new InlineLabel("OK");
		lblCancel=new InlineLabel("Cancel");
		
		applyHandler();
		
	}
	
	public PopupScreen(FormStyle FORM_STYLE,boolean flag, boolean pupupFlag){
		
		super();
		this.formstyle = FORM_STYLE; 
		horizontal=new HorizontalPanel();
//		popup = new PopupPanel(true);
		popup = new PopupPanel(pupupFlag);
		lblOk=new InlineLabel("OK");
		lblCancel=new InlineLabel("Cancel");
		
		form=new FlexForm(fields,formstyle);
		
		applyHandler();
		
	}


	/**
	 * @author Vijay  Date 18-03-2021 for button Name different to use this call constructor directly form popup
	 * @param FORM_STYLE
	 * @param firstBtnName
	 * @param secondbtnName
	 */
	public PopupScreen(FormStyle FORM_STYLE, String firstBtnName, String secondbtnName){
		
		super();
		this.formstyle = FORM_STYLE; 
		horizontal=new HorizontalPanel();
		popup = new PopupPanel(true);
		lblOk=new InlineLabel(firstBtnName);
		lblCancel=new InlineLabel(secondbtnName);
		
		form=new FlexForm(fields,formstyle);
		
		applyHandler();
		
	}
	
		public PopupScreen(String screenName,int internalType) {
		// TODO Auto-generated constructor stub
		super(screenName, internalType);
		this.formstyle = DEFAULT_FORM_STYLE; 
		horizontal=new HorizontalPanel();
		popup = new PopupPanel(true);
		lblOk=new InlineLabel("OK");
		lblCancel=new InlineLabel("Cancel");
		form=new FlexForm(fields,formstyle);
		applyHandler();
	}
	
	public PopupScreen(String screenName,int internalType,String categoryName) {
		// TODO Auto-generated constructor stub
		super(screenName, internalType,categoryName);
		this.formstyle = DEFAULT_FORM_STYLE; 
		horizontal=new HorizontalPanel();
		popup = new PopupPanel(true);
		lblOk=new InlineLabel("OK");
		lblCancel=new InlineLabel("Cancel");
		form=new FlexForm(fields,formstyle);
		applyHandler();
	}

	@Override
    public void createGui()
	{
		content.add(form);
		
		horizontal.add(lblOk);
		horizontal.add(lblCancel);
		content.add(horizontal);
		
		applyStyle();
		popup.add(content);
		popup.hide();
		
	}
	
	
	/**
	 * Sets the styling
	 */
	
	public void applyStyle() {
	
		content.getElement().setId("popupformcontent");
		form.getElement().setId("form");  //form   popupform
		horizontal.getElement().addClassName("popupcentering");
		lblOk.getElement().setId("addbutton");
		lblCancel.getElement().setId("addbutton");
		
		popup.center();

		
	}
	
	
	
	/**
	 * shows the popup.
	 */
	public void showPopUp()
	{
		form.clear();
		popup.show();
		popup.center();
		popup.setAnimationEnabled(true);
		
	}
	
	/**
	 * Hides the Pop Up
	 */
	public void hidePopUp()
	{
		popup.hide();
	}
	
	
	/**
	 * Applies handlers on OK and Cancel widgets
	 * @param handler handler object which will handle click events on these widgets
	 */
	public void applyHandler()
	{
		lblOk.addClickHandler(this);
		lblCancel.addClickHandler(this);
	}
	
	
	/**
	 * Method shows a pop up with a message
	 * @param Message
	 */
	public void showDialogMessage(String Message)
	{
		final GWTCAlert alert = new GWTCAlert(); 
		/** Date 21-05-2019 by Vijay for SmartGWT it must work so added Z-Index ***/
		 alert.setZIndex(20000000);//Ashwini Patil changed zindex from 1000000 to 20000000 as error message "Email id does not exist" was going behind email popup
	     alert.alert(Message);
	}
	
	
	@Override
	public void setEnable(boolean state) {
		// TODO Auto-generated method stub
		form.setEnabled(state);
	}
	
	/************************  Getter and Setter **************************************/
	
	public PopupPanel getPopup() {
		return popup;
	}

	public void setPopup(PopupPanel popup) {
		this.popup = popup;
	}

	public InlineLabel getLblOk() {
		return lblOk;
	}

	public void setLblOk(InlineLabel lblOk) {
		this.lblOk = lblOk;
	}

	public InlineLabel getLblCancel() {
		return lblCancel;
	}

	public void setLblCancel(InlineLabel lblCancel) {
		this.lblCancel = lblCancel;
	}

	public HorizontalPanel getHorizontal() {
		return horizontal;
	}

	public void setHorizontal(HorizontalPanel horizontal) {
		this.horizontal = horizontal;
	}
	
	/** date 01/01/2018 added by komal for validation in popup **/
	public boolean validate(){
	boolean result=true;
	int row=this.fields.length;
	
	for(int k=0;k<row;k++)
	{
		int column=fields[k].length;
		
		for(int i=0;i<column;i++)
		{
			Widget widg=fields[k][i].getWidget();
			
			if(fields[k][i].getLabel()!=null&&fields[k][i].getLabel().contains("Email"))
			{
				if(widg instanceof TextBox){
					TextBox tb=(TextBox) widg;
					String value=tb.getText();
					
					boolean result1=validateEmail(value.trim());

					if(result1==false)
				    {
				    	if(tb.getText().trim().equals("")==false)
				    	{
				    		result=false;
							changeBorderColor(widg,"#dd4b39");
							
							
							
							InlineLabel l=fields[k][i].getMandatoryMsgWidget();
							if(l!=null)
							{
								l.setVisible(true);
								fields[k][i].getMandatoryMsgWidget().setText("Invalid Email Format!");
				    	}}
				     }
				}
					
			}
			
			if(widg instanceof CompositeInterface)
			{
				
				CompositeInterface a= (CompositeInterface) widg;
				boolean resultadress=a.validate();
				if(resultadress==false)
					result=resultadress;	
			}
			
			if(fields[k][i].isMandatory())
			{
				if(widg instanceof TextBox)
				{
					TextBox tb=(TextBox) widg;
					String value=tb.getText();
					
					if(value.equals(""))
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
						
					}
					
					
					
				}
				
				
				if(widg instanceof TextArea)
				{
					TextArea tb=(TextArea) widg;
					String value=tb.getText();
					
					if(value.equals(""))
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
						
					}
					
					
					
				}
				
				else if(widg instanceof ListBox)
				{
					ListBox lb=(ListBox) widg;
				
					int index=lb.getSelectedIndex();
					if(index==0)
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
						
					}
				}
				
				else if(widg instanceof ObjectListBox)
				{
					ListBox lb=(ListBox) widg;
					
					int index=lb.getSelectedIndex();
					if(index==0)
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
						
					}
				}
				
				else if(widg instanceof DateBox)
				{
					DateBox db=(DateBox) widg;
					Date value=db.getValue();
					if(value==null)
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
						
					}
				}
				
				if(widg instanceof DoubleBox)
				{
					DoubleBox db=(DoubleBox) widg;
					String value=db.getText().trim();
					if(value.equals(""))
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
						
					}
				}
				
				if(widg instanceof SuggestBox)
				{
					SuggestBox sb=(SuggestBox) widg;
					String value=sb.getValue();
					
					if(value.equals(""))
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
					}
				}
				
				if(widg instanceof LongBox)
				{
					LongBox sb=(LongBox) widg;
					Long value=sb.getValue();
					if(value==null)
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
					}
				}
			
				if(widg instanceof MyLongBox)
				{
					MyLongBox sb=(MyLongBox) widg;
					
					Long value=sb.getValue();
					if(value==null)
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
					}
				}
				
				
				if(widg instanceof IntegerBox)
				{
					IntegerBox sb=(IntegerBox) widg;
					
					Integer value=sb.getValue();
					if(value==null)
					{
						result=false;
						changeBorderColor(widg,"#dd4b39");
						InlineLabel l=fields[k][i].getMandatoryMsgWidget();
						if(l!=null)
							l.setVisible(true);
					}
				}
			}
		}
	}
	return result;

	}	
	 protected void changeBorderColor(Widget widg,String color)
     {
    	 widg.getElement().getStyle().setBorderColor(color);
     }
	 public static boolean validateEmail(String email)
		{
			String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			return email.matches(EMAIL_PATTERN);     
		}
	 
	 
	 
	 public void showWaitSymbol() {
			glassPanel = new GWTCGlassPanel();
			glassPanel.show();
			
			
		}
		public void hideWaitSymbol() {
		   glassPanel.hide();
			
		}
		
		/**
		 * Date 18-05-2019 by Vijay
		 * Des :- TabPanel with form fields mapping
		 */

		public FlexForm getFormFileds(FormField[][] formfileds){
			this.formstyle = DEFAULT_FORM_STYLE;
			FlexForm form2 ;
			form2 = new FlexForm(formfileds, formstyle);
			return form2;
		}

		@Override
		public void onClick(ClickEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void createScreen(String screenName, int internalType) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void createScreen(String screenName,int internalType,String categoryName ) {
			
		}
	}


	