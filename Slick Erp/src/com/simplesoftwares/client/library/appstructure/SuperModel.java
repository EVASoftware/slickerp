package com.simplesoftwares.client.library.appstructure;

import java.io.Serializable;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.simplesoftwares.rebind.annatonations.TableAnnotation;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;

/**
 * This class is the superclass of all the entites which get saved in db;.
 * @author Kamala
 */

public abstract class SuperModel implements  Serializable,Comparable<SuperModel>
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7031230647665058934L;
	
	
	/**
	 * id part of the kind's key.
	 */
	@Id
	protected Long id;
	/**
	 * Repersent the count of entity in kind.
	 * On Ui side we use this as id
	 */
	
	
	@Index 
	protected int count;
	/**
	 * To seprate data of one company from another company.
	 */
	@Index protected Long companyId;
	
	/** The deleted. */
	@Index boolean deleted;
	
	@Index
	protected String createdBy;
	
	@Index
	protected String userId;
	

	
	/**
	 * Instantiates a new super model.
	 */
	public SuperModel() 
	{
		super();
		if(UserConfiguration.getCompanyId()!=null){
		     companyId=UserConfiguration.getCompanyId();
		     createdBy=UserConfiguration.getUserconfig().getUser().getEmployeeName();
		     userId=UserConfiguration.getUserconfig().getUser().getUserName();
		}
		
	}

	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	@TableAnnotation(ColType = "TextColumn", colNo = 0, isFieldUpdater = false, isSortable = true, title = "Id")
	public int getCount() {
		return count;
	}

	/**
	 * Sets the count.
	 *
	 * @param long1 the new count
	 */
	public void setCount(int long1) {
		this.count = long1;
	}

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public Long getCompanyId() {
		return companyId;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public  Long getId()
	{
		return this.id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public  void setId(Long id)
	{
		this.id=id;
	}
	
	/**
	 * Checks if is duplicate.
	 *
	 * @param m passed supermodel
	 * @return weather passed model is conceptuly duplicate or not
	 */
	public abstract boolean isDuplicate(SuperModel m);

	/**
	 * Checks if is deleted.
	 *
	 * @return true, if is deleted
	 */
	public boolean isDeleted() 
	{
		return deleted;
	}

	/**
	 * Sets the deleted.
	 *
	 * @param deleted the new deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		if(createdBy!=null){
			this.createdBy = createdBy.trim();
		}
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		if(userId!=null){
			this.userId = userId.trim();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SuperModel arg0) 
	{
		return 0;
	}


	
	
	

}


