package com.simplesoftwares.client.library.appstructure;

import java.util.HashMap;
import java.util.Map;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.code.p.gwtchismes.client.GWTCGlassPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.ProcessLevelMenuBarPopup;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appskeleton.ProcessLevelBar;
import com.simplesoftwares.client.library.appskeleton.ScreeenState;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.services.GeneralService;
import com.slicktechnologies.client.services.GeneralServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.screenmenuconfiguration.UploadPopup;
import com.slicktechnologies.shared.screenmenuconfiguration.MenuConfiguration;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

/**
 * Ui Screen is top most class of any kind of screen.Abstracts logic common to all screens.
 * Conceptually this class is very important from flow point of view , may be it shows some properties 
 * of Master design pattern
 * @author Kamala
 *
 * @param <T>
 */
public abstract class UiScreen<T> extends ViewContainer {
	
	/**
	 * ProcessLevelBar objects.
	 */
	public ProcessLevelBar processLevelBar;
	/**
	 * An object of SearchPopUpScreen if it is set to null,It means the class does not wants a search popup.
	 */
	protected SearchPopUpScreen<T>searchpopupscreen;
	
	protected String[] processlevelBarNames;
	
	/**
	 * Glass Panel covers the whole screen and doesn't allow user to do interaction on screen.
	 */
	protected GWTCGlassPanel glassPanel;
	
	/**
	 *Presenter refrence 
	 */
	protected EntityPresenter presenter;
	
	
	/**
	 * Date 24 july 2017 added by vijay 
	 * this method only call from GERENRAL POPUP
	 * by default every screen flag is false 
	 */
	
	public boolean popUpAppMenubar= false;
	
	/**
	 * @author Anil
	 * @since 03-12-2020
	 * Navigation menu array
	 */
	protected String[] navigationProcessMenuBar;
	
	/**
	 * @author Anil
	 * @since 18-12-2020
	 */
	protected boolean isAction;
	protected boolean isNavigation;
	protected boolean isHelp;
	protected boolean isReports;
	protected boolean isUpload;
	
	HashMap<String,String> helpMap=new HashMap<String,String>();
	HashMap<String,String> reportsMap=new HashMap<String,String>();
	protected HashMap<String,String> uploadMap=new HashMap<String,String>();
	
	/**
	 * @author Anil
	 * @since 21-12-2020
	 *
	 */
	public InlineLabel[] helpBtnLabels;
	public InlineLabel[] reportBtnLabels;
	public InlineLabel[] uploadBtnLabels;
	
	/**
	 * @author Anil
	 * @since 22-12-2020
	 */
	protected UploadPopup uploadPopup=new UploadPopup();
	protected PopupPanel uploadPanel=new PopupPanel();
	
	/**
	 * @author Anil @since 08-04-2021
	 * 
	 */
	public ProcessLevelMenuBarPopup processMenuPopup;
	public PopupPanel appdrawerPanel;
	public int perUnitHeight=23;
//	public double
	
	protected GeneralServiceAsync genService=GWT.create(GeneralService.class);
	
	boolean isPopup;
	/**
	 * Creates a new Ui Screen.
	 */
	
	
	/**
	 * @author Ashwini Patil, Date : 22-12-2021
	 * Adding Summary popup flag
	 * Set processlevelBarNames for summary popup
	 */
	public boolean summaryPopupFlag=false;
	
	//Ashwini Patil Date:20-09-2022 If this is set to true then navigation buttons will not be displayed on popup
	public boolean hideNavigationButtons=false;
	


	
	public UiScreen(FormStyle formstyle) {
		super();
		System.out.println("before screen menu");
		/**
		 * @author Anil
		 * @since 18-12-2020
		 * Initializing Screen level menus for simplified screens
		 */
		if(FormStyle.DEFAULT!=formstyle){
//			double 
			appdrawerPanel = new PopupPanel(true);
			processMenuPopup = new ProcessLevelMenuBarPopup();
			appdrawerPanel.add(processMenuPopup);
			appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
			appdrawerPanel.setWidth("180px");
			appdrawerPanel.getElement().setId("login-form");
			Console.log("UI Constructor");
			System.out.println("UI Constructor");
			initializeScreen();
			intializeScreenMenus();
			addClickEventOnScreenMenus();
			
			isScreenMenuVisible(false);
		}
		else {
			/**
			 * @author Vijay Date :- 05-08-2022
			 * Des :- Bug :- screen menu configuration does not applicable but its showing previous screen menus 
			 * so below code updated
			 */
			boolean flag = false;
			Console.log("currentModule"+LoginPresenter.currentModule);
			Console.log("currentDocumentName"+LoginPresenter.currentDocumentName);

			if(AppMemory.getAppMemory().enableMenuBar){
				Console.log("intializeScreenMenus 1 ");
				
				if(LoginPresenter.globalScrMenuConf!=null&&LoginPresenter.globalScrMenuConf.size()!=0){
					
					Console.log("intializeScreenMenus 2 ");
					
					for(ScreenMenuConfiguration menu:LoginPresenter.globalScrMenuConf){
						
						if(menu.getModuleName().equals(LoginPresenter.currentModule)
								&&menu.getDocumentName().equals(LoginPresenter.currentDocumentName)){
							flag = true;
						}
					}	
				}
			}
			
			/**
			 * @author Vijay Date -24-08-2023
			 * Des :- Contract Renewal screen is not implification screen so new SImplification UI menus are not applicable here
			 * so added below code do not display previous screen UI menus on renewal screen 
			 */
			if(LoginPresenter.currentModule.equals("Service") && LoginPresenter.currentDocumentName.equals("Contract Renewal")){
				flag = false;
			}
			/**
			 * ends here
			 */
			
			if(!flag) {
				System.out.println("For default value false");
				Console.log("For default value false");
				AppMemory.getAppMemory().skeleton.appdrawer.setVisible(false);
				AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(false);
				AppMemory.getAppMemory().skeleton.reportBtn.setVisible(false);
				AppMemory.getAppMemory().skeleton.helpBtn.setVisible(false);
				AppMemory.getAppMemory().skeleton.uploadBtn.setVisible(false);
			}
			/**
			 * ends here
			 */
				
		}

		
		createScreen();
	Console.log("this.popUpAppMenubar =="+this.popUpAppMenubar);
		if(!this.popUpAppMenubar){
			toggleAppHeaderBarMenu();
		}
	}
	
	/**
	 * @author Vijay Date:- 25-05-2021
	 * Des :- To manage App level Menus added below constructor
	 * so when any screen showing on popup screen then using below constructor 
	 * app level menu bar should not print on current screen. it will show only on Popup screen 
	 * 
	 */
	public UiScreen(FormStyle formstyle,boolean popupFlag) {
		super();
		
		/**
		 * @author Anil
		 * @since 18-12-2020
		 * Initializing Screen level menus for simplified screens
		 */
		if(FormStyle.DEFAULT!=formstyle){
			
			if(!popupFlag){
				appdrawerPanel = new PopupPanel(true);
				processMenuPopup = new ProcessLevelMenuBarPopup();
				appdrawerPanel.add(processMenuPopup);
				appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
				appdrawerPanel.setWidth("180px");
				appdrawerPanel.getElement().setId("login-form");
				
				initializeScreen();
				intializeScreenMenus();
				addClickEventOnScreenMenus();
				isScreenMenuVisible(false);
			}
		}
		this.isPopup=popupFlag;
		
		createScreen();
		/**
		 * @author Vijay Date 25-05-2021
		 * Des :- added below condition to show(manage) screen on Popup screen
		 * 
		 */
		if(!popupFlag){
			toggleAppHeaderBarMenu();
		}
		this.popUpAppMenubar=popupFlag;
	}
	
	public UiScreen() {
		super();
		
		appdrawerPanel = new PopupPanel(true);
		processMenuPopup = new ProcessLevelMenuBarPopup();
		appdrawerPanel.add(processMenuPopup);
		appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
		appdrawerPanel.setWidth("180px");
		appdrawerPanel.getElement().setId("login-form");
		
		/**
		 * @author Anil
		 * @since 18-12-2020
		 * Initializing Screen level menus for simplified screens
		 */
		initializeScreen();
		intializeScreenMenus();
		addClickEventOnScreenMenus();
		
		isScreenMenuVisible(false);
		
		createScreen();
		toggleAppHeaderBarMenu();
	}

//////////////////////////////////////////////ABSTRACT METHODS/////////////////////////////////////////
	/**
	 * clears the screen
	 * @author Kamala
	 */
	public abstract void clear();
	/**
	 * validates the ui screen
	 * @return true if validation sucessfull .False if not
	 */
	
	public abstract boolean validate();
	/**
	 * Sets screen in enabled or disabled mode.
	 * @param state if true makes the screen enable
	 */
	
	public abstract void setEnable(boolean state);
	/**
	 * toogles the App header bar menu.
	 * To Do : Togelling logic depends upon screen,but some togelling also depends upon the screen state whoose logic we
	 * can write here and can make this method concrete.
	 */
	
	public abstract void toggleAppHeaderBarMenu();
	/**
	 * creates the screen. All concrete class will give its implementation.
	 * To Do: Introduce generators.
	 * @author Kamala
	 */
	
	public abstract void createScreen();
	/**
	 *  updates the model from view.
	 *  @param model the model object which needs to be updated from view.
	 */
	
	public abstract void updateModel(T model);
	/**
	 * updates the view from model.
	 * @param model
	 */
	
	public abstract void updateView(T model);

/////////////////////////////////////SETTING SCREEN STATES//////////////////////////////////////////////
	
	/**
	 * Sets the current state in new mode.Also hides the process level bar if it exists. 
	 */
	public void setToNewState() 
	{
		clear();
		AppMemory.getAppMemory().currentState=ScreeenState.NEW;
		toggleAppHeaderBarMenu();
		if(processLevelBar!=null)
		    processLevelBar.setVisibleFalse(false);
		
		
		isScreenMenuVisible(false);
		
//		if(AppMemory.getAppMemory().skeleton.appdrawerPanel!=null){
//			AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//		}
		
		if(appdrawerPanel!=null){
			appdrawerPanel.hide();
		}
		
		setEnable(true);	
	}
	
	/**
	 * Sets the current state in view mode.Also shows the process level bar if it exists.
	 */
	
	public void setToViewState() 
	{
		AppMemory.getAppMemory().currentState=ScreeenState.VIEW;
		toggleAppHeaderBarMenu();
		
//		if (processlevelBarNames != null) {
////			processLevelBar = new ProcessLevelBar(5, processlevelBarNames);
//			processLevelBar = new ProcessLevelBar(5, processlevelBarNames,navigationProcessMenuBar,formstyle);
//			processLevelBar.setVisibleFalse(false);
//		}
		if(processLevelBar!=null){
		    processLevelBar.setVisibleFalse(true);
		}
		
		isScreenMenuVisible(true);
		
//		if(AppMemory.getAppMemory().skeleton.appdrawerPanel!=null){
//			AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//		}
		
		if(appdrawerPanel!=null){
			appdrawerPanel.hide();
		}
		
		setEnable(false);
		
	}
	/**
	 * Sets the app in edit state.Also makes process level bar visible if exists.
	 */
	
	public void setToEditState() 
	{
		AppMemory.getAppMemory().currentState=ScreeenState.EDIT;
		toggleAppHeaderBarMenu();
		if(processLevelBar!=null)
		    processLevelBar.setVisibleFalse(true);
		
		isScreenMenuVisible(false);
		
//		if(AppMemory.getAppMemory().skeleton.appdrawerPanel!=null){
//			AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
//		}
		
		if(appdrawerPanel!=null){
			appdrawerPanel.hide();
		}
		
		setEnable(true);
	}

/////////////////////////////////INTERVAL METHODS/////////////////////////////////////////////
	
	
	public void showWaitSymbol() {
		glassPanel = new GWTCGlassPanel();
		glassPanel.show();
		
		
	}
	public void hideWaitSymbol() {
	   glassPanel.hide();
		
	}
	
//////////////////////////////////GETTER SETTERS///////////////////////////////////////////////
	
	public ProcessLevelBar getProcessLevelBar() {
		return processLevelBar;
	}
	public void setProcessLevelBar(ProcessLevelBar processLevelBar) {
		this.processLevelBar = processLevelBar;
	}
	public SearchPopUpScreen<T> getSearchpopupscreen() {
		return searchpopupscreen;
	}
	public void setSearchpopupscreen(SearchPopUpScreen<T> searchpopupscreen) {
		this.searchpopupscreen = searchpopupscreen;
	}
	
	

	/**
	 * Sets the counts whic returns from server on view.The method is a stub as many screens may not need this.
	 * 
	 * @param count count which returns from server
	 */
	public void setCount(int count) {
		
			
	}
	
	/**
	 * Method shows a pop up with a message
	 * @param Message
	 */
	public void showDialogMessage(String Message)
	{
		final GWTCAlert alert = new GWTCAlert(); 
	     alert.alert(Message);
	     alert.getElement().addClassName("showMessageBox"); // Updated by:Viraj for displaying alert in front of popup
	}
	
	public void showDialogMessage(String Message,int roundedBlue,int animation)
	{
		final GWTCAlert alert = new GWTCAlert(roundedBlue|animation); 
	     alert.alert(Message);
	     alert.getElement().addClassName("showMessageBox"); // Updated by:Viraj for displaying alert in front of popup
	}

	public String[] getProcesslevelBarNames() {
		return processlevelBarNames;
	}

	public void setProcesslevelBarNames(String[] processlevelBarNames) {
		this.processlevelBarNames = processlevelBarNames;
	}

	public GWTCGlassPanel getGlassPanel() {
		return glassPanel;
	}

	public void setGlassPanel(GWTCGlassPanel glassPanel) {
		this.glassPanel = glassPanel;
	}

	public EntityPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(EntityPresenter presenter) {
		this.presenter = presenter;
	}

	
	/**
	 * Date 24 july 2017 added by vijay 
	 * this  only call from GERENRAL POPUP
	 */
	
	public boolean isPopUpAppMenubar() {
		return popUpAppMenubar;
	}

	public void setPopUpAppMenubar(boolean popUpAppMenubar) {
		this.popUpAppMenubar = popUpAppMenubar;
	}

	
	/**
	 * Date 23 Dec 2021 added by Ashwini 
	 * Summary Screen GERENRAL POPUP uses this flag. This flag indicates popup screen is created through summary screen.
	 */
	public boolean isSummaryPopupFlag() {
		return summaryPopupFlag;
	}

	public void setSummaryPopupFlag(boolean summaryPopupFlag) {
		this.summaryPopupFlag = summaryPopupFlag;
	}
	
	public boolean isHideNavigationButtons() {
		return hideNavigationButtons;
	}

	public void setHideNavigationButtons(boolean hideNavigationButtons) {
		this.hideNavigationButtons = hideNavigationButtons;
	}

	/**
	 * @author Anil
	 * @since 18-12-2020
	 * This method checks whether screen menu label is active or not
	 * 
	 * @param menuName
	 * @return
	 */
	public void intializeScreenMenus(){
		Console.log("intializeScreenMenus ");
		isAction=false;
		isHelp=false;
		isReports=false;
		isNavigation=false;
		isUpload=false;
		navigationProcessMenuBar=null;
		
		AppMemory.getAppMemory().skeleton.appdrawer.setVisible(false);
		AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(false);
		AppMemory.getAppMemory().skeleton.reportBtn.setVisible(false);
		AppMemory.getAppMemory().skeleton.helpBtn.setVisible(false);
		AppMemory.getAppMemory().skeleton.uploadBtn.setVisible(false);
		
		if(AppMemory.getAppMemory().enableMenuBar){
			Console.log("intializeScreenMenus 1 ");
			
			if(LoginPresenter.globalScrMenuConf!=null&&LoginPresenter.globalScrMenuConf.size()!=0){
				
				Console.log("intializeScreenMenus 2 ");
				
				for(ScreenMenuConfiguration menu:LoginPresenter.globalScrMenuConf){
					
					if(menu.getModuleName().equals(LoginPresenter.currentModule)
							&&menu.getDocumentName().equals(LoginPresenter.currentDocumentName)){
						
						if(menu.getMenuConfigurationList()!=null&&menu.getMenuConfigurationList().size()!=0){
							if(menu.getMenuName().equals("Help")){
								isHelp=true;
								for(MenuConfiguration confg:menu.getMenuConfigurationList()){
									if(confg.isStatus()){
										helpMap.put(confg.getName(), confg.getLink());
									}
								}
							}else if(menu.getMenuName().equals("Reports")){
								isReports=true;
								for(MenuConfiguration confg:menu.getMenuConfigurationList()){
									if(confg.isStatus()){
										reportsMap.put(confg.getName(), confg.getLink());
									}
								}
							}else if(menu.getMenuName().equals("Navigation")){
								isNavigation=true;
								
								navigationProcessMenuBar=new String[menu.getMenuConfigurationList().size()];
								int index=0;
								for(MenuConfiguration confg:menu.getMenuConfigurationList()){
									navigationProcessMenuBar[index]=confg.getName();
									index++;
								}
								
							}else if(menu.getMenuName().equals("Upload")){
								isUpload=true;
								for(MenuConfiguration confg:menu.getMenuConfigurationList()){
									if(confg.isStatus()){
										uploadMap.put(confg.getName(), confg.getLink());
									}
								}
							}
						}
						
					}
				}
			}
			if(processLevelBar!=null){
				if(processLevelBar.isProcessMenuActive()){
					isAction=true;
				}
			}
		}
		Console.log("MODULE : "+LoginPresenter.currentModule+" DOCUMENT : "+LoginPresenter.currentDocumentName);
		Console.log("isAction : "+isAction+" isNavigation : "+isNavigation+" isReports : "+isReports+" isHelp : "+isHelp+" isUpload : "+isUpload);
	}
	
	public void isScreenMenuVisible(boolean isStatus){
		if(isPopup){
			return;
		}
		Console.log("isScreenMenuVisible "+isStatus+" :: "+"isAction : "+isAction+" isNavigation : "+isNavigation+" isReports : "+isReports+" isHelp : "+isHelp+" isUpload : "+isUpload);
		int menuCounter=0;
		if(AppMemory.getAppMemory().enableMenuBar){
			Console.log("isScreenMenuVisible 1 ");
			int leftMargin=27;
			if(isStatus){
				AppMemory.getAppMemory().skeleton.appdrawer.setVisible(isAction);
				if(isAction){
					if(processLevelBar.isActionMenuActive()==false){
						Console.log("isScreenMenuVisible 2 ");
						AppMemory.getAppMemory().skeleton.appdrawer.setVisible(false);
					}else{
						leftMargin=leftMargin-40;
						AppMemory.getAppMemory().skeleton.appdrawer.getElement().getStyle().setMarginLeft(leftMargin, Unit.PX);
						menuCounter++;
					}
				}
				
				AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(isNavigation);
				if(isNavigation){
					if(processLevelBar.isNavigationMenuActive()==false){
						AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(false);
					}else{
						leftMargin=leftMargin-40;
						AppMemory.getAppMemory().skeleton.navigationBtn.getElement().getStyle().setMarginLeft(leftMargin, Unit.PX);
						menuCounter++;
					}
				}
				
				AppMemory.getAppMemory().skeleton.reportBtn.setVisible(isReports);
				AppMemory.getAppMemory().skeleton.helpBtn.setVisible(isHelp);
				AppMemory.getAppMemory().skeleton.uploadBtn.setVisible(isUpload);
			}else{
				AppMemory.getAppMemory().skeleton.appdrawer.setVisible(false);
				AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(false);
				AppMemory.getAppMemory().skeleton.reportBtn.setVisible(isReports);
				AppMemory.getAppMemory().skeleton.helpBtn.setVisible(isHelp);
				AppMemory.getAppMemory().skeleton.uploadBtn.setVisible(isUpload);
			}
			
			if(isUpload){
				leftMargin=leftMargin-40;
				AppMemory.getAppMemory().skeleton.uploadBtn.getElement().getStyle().setMarginLeft(leftMargin, Unit.PX);
				menuCounter++;
			}
			if(isReports){
				leftMargin=leftMargin-40;
				AppMemory.getAppMemory().skeleton.reportBtn.getElement().getStyle().setMarginLeft(leftMargin, Unit.PX);
				menuCounter++;
			}
			
			if(isHelp){
				leftMargin=leftMargin-40;
				AppMemory.getAppMemory().skeleton.helpBtn.getElement().getStyle().setMarginLeft(leftMargin, Unit.PX);
				menuCounter++;
			}
		}else{
			AppMemory.getAppMemory().skeleton.appdrawer.setVisible(false);
			AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(false);
			AppMemory.getAppMemory().skeleton.reportBtn.setVisible(false);
			AppMemory.getAppMemory().skeleton.helpBtn.setVisible(false);
			AppMemory.getAppMemory().skeleton.uploadBtn.setVisible(false);
		}
		
		Console.log("menuCounter "+menuCounter);
		if(menuCounter==0){
			AppMemory.getAppMemory().skeleton.getAppLevelMenuPanel().setWidth("100%");
		}else if(menuCounter==1){
			AppMemory.getAppMemory().skeleton.getAppLevelMenuPanel().setWidth("99%");
		}else if(menuCounter==2){
			AppMemory.getAppMemory().skeleton.getAppLevelMenuPanel().setWidth("95%");
		}else if(menuCounter==3){
			AppMemory.getAppMemory().skeleton.getAppLevelMenuPanel().setWidth("93%");
		}else if(menuCounter==4){
			AppMemory.getAppMemory().skeleton.getAppLevelMenuPanel().setWidth("90%");
		}else if(menuCounter==5){
			AppMemory.getAppMemory().skeleton.getAppLevelMenuPanel().setWidth("86%");
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void addClickEventOnScreenMenus(){
		if(appdrawerPanel==null){
			return;
		}
		int index=0;
		if(helpMap!=null&&helpMap.size()!=0){
			helpBtnLabels=new InlineLabel[helpMap.size()];
			for (Map.Entry<String,String> entry : helpMap.entrySet()){ 
	            System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue());
	            InlineLabel label=new InlineLabel(entry.getKey());
	            helpBtnLabels[index]=label;
	            helpBtnLabels[index].getElement().setClassName("appdrawermenu");
	            index++;
			}
		}
		
		if(reportsMap!=null&&reportsMap.size()!=0){
			index=0;
			reportBtnLabels=new InlineLabel[reportsMap.size()];
			for (Map.Entry<String,String> entry : reportsMap.entrySet()){ 
	            System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue());
	            InlineLabel label=new InlineLabel(entry.getKey());
	            reportBtnLabels[index]=label;
	            reportBtnLabels[index].getElement().setClassName("appdrawermenu");
	            index++;
			}
		}
		
		if(uploadMap!=null&&uploadMap.size()!=0){
			index=0;
			uploadBtnLabels=new InlineLabel[uploadMap.size()];
			for (Map.Entry<String,String> entry : uploadMap.entrySet()){ 
	            System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue());
	            InlineLabel label=new InlineLabel(entry.getKey());
	            uploadBtnLabels[index]=label;
	            uploadBtnLabels[index].getElement().setClassName("appdrawermenu");
	            index++;
			}
		}
		
		
		
		
		
		AppMemory.getAppMemory().skeleton.helpBtn.removeClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.helpBtn.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
				
				if(helpBtnLabels!=null&&helpBtnLabels.length!=0){
//					AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
					processMenuPopup.getVerticalPanel().clear();
					double height=250;
					height=24.5*helpBtnLabels.length;
					for(final InlineLabel helpBtn:helpBtnLabels){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(helpBtn);
						processMenuPopup.getVerticalPanel().add(helpBtn);
						
						helpBtn.addClickListener(new ClickListener() {
							@Override
							public void onClick(Widget sender) {
								final String url = helpMap.get(helpBtn.getText());
								Console.log("URL :: "+url);
					 			Window.open(url, "test", "enabled");
//								Window.open(helpMap.get(helpBtn.getText()), "test", "enabled");
//								AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
								appdrawerPanel.hide();
							}
						});
						
//						double fixedLength=26.0;
//						int factorRate=9;
//						int contentLength=helpBtn.getText().length();
//						if(contentLength>26){
//							double extraLine=contentLength/fixedLength;
//							extraLine=Math.ceil(extraLine);
//							if(extraLine==4){
//								factorRate=11;
//							}else if(extraLine==3){
//								factorRate=10;
//							}else if(extraLine==2){
//								factorRate=9;
//							}
//							height=(int) (height+(extraLine*factorRate));
//						}
					}
					
//					if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//					}
//					Console.log("HELP HEIGHT :: "+height);
//					AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
					
//					Console.log("HELP HEIGHT :: "+height);
//					if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						Console.log("HELP HEIGHT :: "+processMenuPopup.getVerticalPanel().getElement().getClientHeight());
//						processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						processMenuPopup.setHeight(height+"px");
//					}
					
					appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
					appdrawerPanel.show();
					
					Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
					    public void execute()
					    {
					        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
					        Console.log("Scheduler :: "+offsetHeight);
					        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
					    }
					});
					
				}else{
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
					appdrawerPanel.hide();
				}
			}
		});
		
		
		AppMemory.getAppMemory().skeleton.reportBtn.removeClickListener(new ClickListener() {
			
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.reportBtn.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
				
				if(reportBtnLabels!=null&&reportBtnLabels.length!=0){
//					AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
					processMenuPopup.getVerticalPanel().clear();
//					double height=250;
//					height=24.5*reportBtnLabels.length;
					
					for(final InlineLabel reportBtn:reportBtnLabels){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(reportBtn);
						processMenuPopup.getVerticalPanel().add(reportBtn);
						
						reportBtn.addClickListener(new ClickListener() {
							@Override
							public void onClick(Widget sender) {
								final String url = reportsMap.get(reportBtn.getText());
								Console.log("URL :: "+url);
					 			Window.open(url, "test", "enabled");
//								Window.open(reportsMap.get(reportBtn.getText()), "test", "enabled");
//								AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
								appdrawerPanel.hide();
							}
						});
						
//						double fixedLength=26.0;
//						int factorRate=9;
//						int contentLength=reportBtn.getText().length();
//						if(contentLength>26){
//							double extraLine=contentLength/fixedLength;
//							extraLine=Math.ceil(extraLine);
//							
//							if(extraLine==4){
//								factorRate=11;
//							}else if(extraLine==3){
//								factorRate=10;
//							}else if(extraLine==2){
//								factorRate=9;
//							}
//							height=(int) (height+(extraLine*factorRate));
//						}
					}
					
//					if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//					}
//					Console.log("REPORT HEIGHT :: "+height);
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
					
//					Console.log("REPORT HEIGHT :: "+height);
//					if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						Console.log("REPORT HEIGHT :: "+processMenuPopup.getVerticalPanel().getElement().getClientHeight());
//						processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						processMenuPopup.setHeight(height+"px");
//					}
					
					appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
					appdrawerPanel.show();
					
					Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
					    public void execute()
					    {
					        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
					        Console.log("Scheduler :: "+offsetHeight);
					        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
					    }
					});
				}else{
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
					appdrawerPanel.hide();
				}
			}
		});
		
		
		AppMemory.getAppMemory().skeleton.uploadBtn.removeClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
			}
		});
		
		AppMemory.getAppMemory().skeleton.uploadBtn.addClickListener(new ClickListener() {
			@Override
			public void onClick(Widget sender) {
				
				if(uploadBtnLabels!=null&&uploadBtnLabels.length!=0){
//					AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().clear();
					processMenuPopup.getVerticalPanel().clear();
//					double height=250;
//					height=24.5*uploadBtnLabels.length;
					for(final InlineLabel uploadBtn:uploadBtnLabels){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().add(uploadBtn);
						processMenuPopup.getVerticalPanel().add(uploadBtn);
						
						uploadBtn.addClickListener(new ClickListener() {
							@Override
							public void onClick(Widget sender) {
								
								genService.ping(new AsyncCallback<Void>() {
									@Override
									public void onSuccess(Void result) {
										// TODO Auto-generated method stub
										
										final String url = uploadMap.get(uploadBtn.getText());
										Console.log("URL :: "+url);
//							 			Window.open(url, "test", "enabled");
										uploadPopup.clear();
										uploadPopup.getHeadingLabel().setText(uploadBtn.getText());
										uploadPopup.setUploadDoc(uploadBtn.getText());
										uploadPopup.setUrl(url);
										
										try{
											uploadPanel.add(uploadPopup);
										}catch(Exception e){
											
										}
										
										uploadPanel.center();
										uploadPanel.show();
//										AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
										appdrawerPanel.hide();
									}
									
									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub
										if (caught instanceof StatusCodeException && ((StatusCodeException) caught).getStatusCode() == 0) {
											showDialogMessage("Server unreachable, Please check your internet connection!");
											if(glassPanel!=null){
												hideWaitSymbol();
											}
								        }
									}
								});
								
								
							}
						});
						
//						double fixedLength=26.0;
//						int factorRate=9;
//						int contentLength=uploadBtn.getText().length();
//						if(contentLength>26){
//							double extraLine=contentLength/fixedLength;
//							extraLine=Math.ceil(extraLine);
//							if(extraLine==4){
//								factorRate=11;
//							}else if(extraLine==3){
//								factorRate=10;
//							}else if(extraLine==2){
//								factorRate=9;
//							}
//							height=(int) (height+(extraLine*factorRate));
//						}
					}
					
//					if(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(AppMemory.getAppMemory().skeleton.processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						AppMemory.getAppMemory().skeleton.processMenuPopup.setHeight(height+"px");
//					}
//					Console.log("UPLOAD HEIGHT :: "+height);
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.show();
					
//					if(processMenuPopup.getVerticalPanel().getElement().getClientHeight()!=0){
//						processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
//					}else{
//						processMenuPopup.setHeight(height+"px");
//					}
//					Console.log("UPLOAD HEIGHT :: "+height);
					
					appdrawerPanel.setPopupPosition(Window.getClientWidth() - 226,Window.getClientHeight() - (Window.getClientHeight() - 90));
					appdrawerPanel.show();
					
					Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){
					    public void execute()
					    {
					        int offsetHeight = processMenuPopup.getVerticalPanel().getElement().getClientHeight();
					        Console.log("Scheduler :: "+offsetHeight);
					        processMenuPopup.setHeight(processMenuPopup.getVerticalPanel().getElement().getClientHeight()+"px");
					    }
					});
					
				}else{
//					AppMemory.getAppMemory().skeleton.appdrawerPanel.hide();
					appdrawerPanel.hide();
				}
			}
		});
		
		
		uploadPopup.getBtnOk().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
//				uploadPanel.hide();
				
				
				if(uploadPopup.validate()){
					uploadPopup.uploadDocuments(uploadPanel);
				}
			}
		});
		
		uploadPopup.getBtnCancel().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				uploadPanel.hide();
			}
		});
	}

	public String[] getNavigationProcessMenuBar() {
		return navigationProcessMenuBar;
	}

	public void setNavigationProcessMenuBar(String[] navigationProcessMenuBar) {
		this.navigationProcessMenuBar = navigationProcessMenuBar;
	}

	public boolean isAction() {
		return isAction;
	}

	public void setAction(boolean isAction) {
		this.isAction = isAction;
	}
	
	public void initializeScreen(){
		helpMap=new HashMap<String,String>();
		reportsMap=new HashMap<String,String>();
		uploadMap=new HashMap<String,String>();
		
		
		AppMemory.getAppMemory().skeleton.horAppPanel.remove(AppMemory.getAppMemory().skeleton.helpBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.remove(AppMemory.getAppMemory().skeleton.reportBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.remove(AppMemory.getAppMemory().skeleton.uploadBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.remove(AppMemory.getAppMemory().skeleton.navigationBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.remove(AppMemory.getAppMemory().skeleton.appdrawer);
		
		
		AppMemory.getAppMemory().skeleton.helpBtn=new Image();
		AppMemory.getAppMemory().skeleton.helpBtn.setUrl("/images/menubar/056-information.png");
		AppMemory.getAppMemory().skeleton.helpBtn.setAltText("Help");
		AppMemory.getAppMemory().skeleton.helpBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		AppMemory.getAppMemory().skeleton.helpBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		AppMemory.getAppMemory().skeleton.helpBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		AppMemory.getAppMemory().skeleton.helpBtn.getElement().getStyle().setMarginLeft(-173, Unit.PX);
		AppMemory.getAppMemory().skeleton.helpBtn.setSize("2%", "34%");
		AppMemory.getAppMemory().skeleton.helpBtn.setVisible(false);
		
		AppMemory.getAppMemory().skeleton.reportBtn=new Image();
		AppMemory.getAppMemory().skeleton.reportBtn.setUrl("/images/menubar/044-file.png");
		AppMemory.getAppMemory().skeleton.reportBtn.setAltText("Reports");
		AppMemory.getAppMemory().skeleton.reportBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		AppMemory.getAppMemory().skeleton.reportBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		AppMemory.getAppMemory().skeleton.reportBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		AppMemory.getAppMemory().skeleton.reportBtn.getElement().getStyle().setMarginLeft(-133, Unit.PX);
		AppMemory.getAppMemory().skeleton.reportBtn.setSize("2%", "34%");
		AppMemory.getAppMemory().skeleton.reportBtn.setVisible(false);
		
		AppMemory.getAppMemory().skeleton.uploadBtn=new Image();
		AppMemory.getAppMemory().skeleton.uploadBtn.setUrl("/images/menubar/145-upload.png");
		AppMemory.getAppMemory().skeleton.uploadBtn.setAltText("Upload");
		AppMemory.getAppMemory().skeleton.uploadBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		AppMemory.getAppMemory().skeleton.uploadBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		AppMemory.getAppMemory().skeleton.uploadBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		AppMemory.getAppMemory().skeleton.uploadBtn.getElement().getStyle().setMarginLeft(-93, Unit.PX);
		AppMemory.getAppMemory().skeleton.uploadBtn.setSize("2%", "34%");
		AppMemory.getAppMemory().skeleton.uploadBtn.setVisible(false);
		
		AppMemory.getAppMemory().skeleton.navigationBtn=new Image();
		AppMemory.getAppMemory().skeleton.navigationBtn.setUrl("/images/menubar/040-compass.png");
		AppMemory.getAppMemory().skeleton.navigationBtn.setAltText("Navigation");
		AppMemory.getAppMemory().skeleton.navigationBtn.getElement().getStyle().setPosition(Position.ABSOLUTE);
		AppMemory.getAppMemory().skeleton.navigationBtn.getElement().getStyle().setBackgroundColor("whitesmoke");
		AppMemory.getAppMemory().skeleton.navigationBtn.getElement().getStyle().setMarginTop(7, Unit.PX);
		AppMemory.getAppMemory().skeleton.navigationBtn.getElement().getStyle().setMarginLeft(-53, Unit.PX);
		AppMemory.getAppMemory().skeleton.navigationBtn.setSize("2%", "34%");
		AppMemory.getAppMemory().skeleton.navigationBtn.setVisible(false);
		
		
		AppMemory.getAppMemory().skeleton.appdrawer=new Image();
		AppMemory.getAppMemory().skeleton.appdrawer.setUrl("/images/menubar/165-menu.png");
		AppMemory.getAppMemory().skeleton.appdrawer.setAltText("Process Menu");
		AppMemory.getAppMemory().skeleton.appdrawer.getElement().setClassName("displaylogo");
		AppMemory.getAppMemory().skeleton.appdrawer.getElement().getStyle().setPosition(Position.ABSOLUTE);
		AppMemory.getAppMemory().skeleton.appdrawer.getElement().getStyle().setBackgroundColor("whitesmoke");
		AppMemory.getAppMemory().skeleton.appdrawer.getElement().getStyle().setMarginTop(7, Unit.PX);
		AppMemory.getAppMemory().skeleton.appdrawer.getElement().getStyle().setMarginLeft(-13, Unit.PX);
		AppMemory.getAppMemory().skeleton.appdrawer.setSize("2%", "34%");
		AppMemory.getAppMemory().skeleton.appdrawer.setVisible(false);
		
		
		AppMemory.getAppMemory().skeleton.horAppPanel.add(AppMemory.getAppMemory().skeleton.helpBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.add(AppMemory.getAppMemory().skeleton.reportBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.add(AppMemory.getAppMemory().skeleton.uploadBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.add(AppMemory.getAppMemory().skeleton.navigationBtn);
		AppMemory.getAppMemory().skeleton.horAppPanel.add(AppMemory.getAppMemory().skeleton.appdrawer);
		
		Console.log("initializeScreen ");
	}
	
}
