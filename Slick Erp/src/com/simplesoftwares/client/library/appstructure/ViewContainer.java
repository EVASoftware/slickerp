package com.simplesoftwares.client.library.appstructure;

import com.google.gwt.user.client.ui.FlowPanel;
/**
 * Base of all screens.
 * @author Kamala
 *
 */
public abstract class ViewContainer {
	/**
	 * Flow panel content.
	 * @author Kamala
	 */
	public FlowPanel content;
	
	
	protected ViewContainer()
	{
	   content=new FlowPanel();
	   content.setSize("100%", "auto");
	}
	
	protected abstract void createGui();

	/**
	 * Contains code to apply css rules on widget(setting class or id on widget)
	 * class or id.Since all of the ui is being created dynamically it means that
	 * quality of ui will always suffer.
	
	 */
	
	 public abstract void applyStyle();
	
}
