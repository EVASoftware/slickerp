 package com.simplesoftwares.client.library.appstructure.formtablescreen;

import java.util.ArrayList;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.simplesoftwares.client.library.HasTable;
import com.simplesoftwares.client.library.FlexForm.FormStyle;
import com.simplesoftwares.client.library.appskeleton.AppMemory;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.simplesoftwares.client.library.appstructure.SuperTable;
import com.simplesoftwares.client.library.appstructure.formscreen.FormScreen;
import com.simplesoftwares.client.library.appstructure.search.Filter;
import com.simplesoftwares.client.library.appstructure.search.MyQuerry;
import com.simplesoftwares.client.library.libservice.GenricService;
import com.simplesoftwares.client.library.libservice.GenricServiceAsync;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.common.serviceLiaisonLayer.LiaisonStep;

// TODO: Auto-generated Javadoc
/**
 * The Class FormTableScreen.
 * Repersents a Screen with Form and Table.
 * Uses Data Binding which is probletic remove data binding.
 * @param <T> the generic type
 */
public abstract class FormTableScreen<T> extends FormScreen<T> implements HasTable {
	
	/** The supertable. */
	protected SuperTable<T>supertable;
	
	/** The mode. */
	protected int MODE;
	
	/** The Constant UPPER_MODE. for table above  the form */
	public final static int UPPER_MODE=1;
	
	/** The Constant LOWER_MODE. for table lower the form*/
	public final static int LOWER_MODE=0;
	
	/** The caption mode. decides weather caption styling should be on table or not*/
	protected boolean CAPTION_MODE=false;
	
	/** The default height. default height of table on this screen*/
	private String DEFAULT_HEIGHT="150px";
	
	/** The scrollpanel. */
	protected ScrollPanel scrollpanel;
	
	/** The captionpanel. */
	public CaptionPanel captionpanel;//Ashwini Patil changed scope to public Date: 23-09-2022

	/** The service. service for retrival of data from server */
	protected GenricServiceAsync service=GWT.create(GenricService.class);
	
	/**
	 * Instantiates a new form table screen.
	 *
	 * @param table object of {@link SuperTable} which will stick on this screeen
	 */
	public FormTableScreen(SuperTable<T>table)
	{
//		super();
		super(FormStyle.TABLEFORMLAYOUT);
		this.supertable=table;
		scrollpanel= new ScrollPanel();
	
	
	}
	
	/**
	 * @author Anil
	 * @since 10-02-2022
	 * @param table
	 * @param formStyle
	 * This constructor is used for search screen
	 */
	public FormTableScreen(SuperTable<T>table,FormStyle formStyle)
	{
//		super();
		super(formStyle);
		Console.log("Conside simplified search screen constructor FormTableScreen : "+formStyle);
		this.supertable=table;
		scrollpanel= new ScrollPanel();
	
	
	}
	
	public FormTableScreen(SuperTable<T>table,boolean popupFlag)
	{
		super(FormStyle.TABLEFORMLAYOUT,popupFlag);
		this.supertable=table;
		scrollpanel= new ScrollPanel();
	
	
	}
	
	public FormTableScreen()
	{
//		super();
		super(FormStyle.TABLEFORMLAYOUT);
	}
	
	/**
	 * Instantiates a new form table screen.
	 *
	 * @param table  object of {@link SuperTable} which will stick on this screeen
	 * @param mode the mode decides the passing of table with respect to form
	 * @param captionmode the caption mode decides the caption mode 
	 */
	public FormTableScreen(SuperTable<T>table,int mode,boolean captionmode)
	{
		this(table);
		supertable.setView(this);
		supertable.applySelectionModle();
	    this.CAPTION_MODE=captionmode;
	    this.MODE=mode;
	   
	   
	}
	/**
	 * @author Vijay Date 25-05-2021
	 * Des :- added below constructor to show screen on Popup screen
	 * 
	 */
	public FormTableScreen(SuperTable<T>table,int mode,boolean captionmode,boolean popupFlag)
	{
		this(table,popupFlag);
		supertable.setView(this);
		supertable.applySelectionModle();
	    this.CAPTION_MODE=captionmode;
	    this.MODE=mode;
	   
	}
	
	

////////////////////////////////////////FINDING THE TABLE CAPTION///////////////////////////////////
	/**
 * Find table caption based on the processName of screen.
 *
 * @return the string returns caption string to be set on caption panel.
 */
public String findTableCaption()
	{
		String name= AppMemory.getAppMemory().skeleton.getProcessName().getText();
		AppMemory.getAppMemory().skeleton.getProcessName().getText();

		String[]splitarray=name.split("/");
		name=splitarray[splitarray.length-1];
		return name;
	}

public String setTableCaption(String captionName)
{
	String name= captionName;
	String[]splitarray=name.split("/");
	name=splitarray[splitarray.length-1];
	return name;
}
//////////////////////////////////////////ADDING TABLE ON CONTENT///////////////////////////////////	
	/**
    *   Adds the table on screen.
   */
protected void addTable()
	{
		if(this.supertable==null)
			return;
		if(CAPTION_MODE)
		{
			String name=findTableCaption();
			captionpanel= new CaptionPanel(name);
			scrollpanel.add(supertable.content);
			captionpanel.add(scrollpanel);
			content.add(captionpanel);
			
			
		}
		
		else
		{
			scrollpanel.add(supertable.content);
			content.add(supertable.content);
		}
		
		if(formstyle==FormStyle.TABLEFORMLAYOUT){
			addClickEventOnActionAndNavigationMenus();
		}
	    
	}

///////////////////////////////////////ADDING FORM OF FORMSCREEN ON CONTENT/////////////////////////////////// 
	/**
 * Adds the form.
 */
protected void addForm()
	{
		if(this.processLevelBar!=null&&formstyle!=FormStyle.TABLEFORMLAYOUT)
			content.add(processLevelBar.content);
		content.add(form);
		
	}

//////////////////////////////////////////GUI CREATION/////////////////////////////////////////////////
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.appstructure.formscreen.FormScreen#createGui()
 * creates the gui depending on the table mode. 
 */
@Override
    public void createGui()
	{
		
		
		if(MODE==UPPER_MODE)
		{
			addTable();
		    addForm();
		}
		else
		{
			addForm();
			addTable();
		}
		applyStyle();
		System.out.println("create Gui");
	}
	
///////////////////////////////////////////////////CSS STYLING///////////////////////////////////////////

	
	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formscreen.FormScreen#applyStyle()
	 */
  @Override
	public void applyStyle()
	{
		super.applyStyle();
	  
		supertable.getTable().setHeight(DEFAULT_HEIGHT);
		if(captionpanel!=null)
		{
		   captionpanel.getElement().setClassName("tablecaption");
		   
		  
		}
		scrollpanel.setHeight("150px");
		
	}

////////////////////////////////////////////INHERITED METHODS(OVERRIDEN)///////////////////////////////
	
	/* (non-Javadoc)
 * @see com.simplesoftwares.client.library.HasTable#retriveTable(com.simplesoftwares.client.library.appstructure.search.MyQuerry)
 *
 *
 */
	
@Override
	public void toggleAppHeaderBarMenu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateModel(T model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateView(T model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void retriveTable(MyQuerry querry) {
		getSupertable().connectToLocal();
		service.getSearchResult(querry,new AsyncCallback<ArrayList<SuperModel>>() {
			
			@Override
			public void onSuccess(ArrayList<SuperModel> result) {
				
				for(SuperModel model:result)
				{
					getSupertable().getListDataProvider().getList().add((T) model);
					
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
			caught.printStackTrace();
				
			}
		});
		
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formscreen.FormScreen#clear()
	 */
	@Override
	public void clear() {
	   form.clear();
	   
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formscreen.FormScreen#validate()
	 */
	@Override
	public boolean validate() {
		boolean formval=form.validate();
		boolean tableval=supertable.validate();
		return formval&&tableval;
	}

	/* (non-Javadoc)
	 * @see com.simplesoftwares.client.library.appstructure.formscreen.FormScreen#setEnable(boolean)
	 */
	@Override
	public void setEnable(boolean state) {
		form.setEnabled(state);
		supertable.setEnable(state);
		
	}
	
///////////////////////////////////////////////GETTER SETTERS///////////////////////////////////////////
	/**
 * Gets the supertable.
 *
 * @return the supertable
 */
public SuperTable<T> getSupertable() {
		return supertable;
	}

	/**
	 * Sets the supertable.
	 *
	 * @param supertable the new supertable
	 */
	public void setSupertable(SuperTable<T> supertable) {
		this.supertable = supertable;
	}

	/**
	 * Gets the mode.
	 *
	 * @return the mode
	 */
	public int getMODE() {
		return MODE;
	}

	/**
	 * Sets the mode.
	 *
	 * @param mODE the new mode
	 */
	public void setMODE(int mODE) {
		MODE = mODE;
	}

	/**
	 * Checks if is caption mode.
	 *
	 * @return true, if is caption mode
	 */
	public boolean isCAPTION_MODE() {
		return CAPTION_MODE;
	}

	/**
	 * Sets the caption mode.
	 *
	 * @param cAPTION_MODE the new caption mode
	 */
	public void setCAPTION_MODE(boolean cAPTION_MODE) {
		CAPTION_MODE = cAPTION_MODE;
	}

	/**
	 * Gets the default height.
	 *
	 * @return the default height
	 */
	public String getDEFAULT_HEIGHT() {
		return DEFAULT_HEIGHT;
	}

	/**
	 * Sets the default height.
	 *
	 * @param dEFAULT_HEIGHT the new default height
	 */
	public void setDEFAULT_HEIGHT(String dEFAULT_HEIGHT) {
		DEFAULT_HEIGHT = dEFAULT_HEIGHT;
	}

	/**
	 * Gets the scrollpanel.
	 *
	 * @return the scrollpanel
	 */
	public ScrollPanel getScrollpanel() {
		return scrollpanel;
	}

	/**
	 * Sets the scrollpanel.
	 *
	 * @param scrollpanel the new scrollpanel
	 */
	public void setScrollpanel(ScrollPanel scrollpanel) {
		this.scrollpanel = scrollpanel;
	}

	/**
	 * Gets the captionpanel.
	 *
	 * @return the captionpanel
	 */
	public CaptionPanel getCaptionpanel() {
		return captionpanel;
	}

	/**
	 * Sets the captionpanel.
	 *
	 * @param captionpanel the new captionpanel
	 */
	public void setCaptionpanel(CaptionPanel captionpanel) {
		this.captionpanel = captionpanel;
	}

	/**
	 * Gets the upper mode.
	 *
	 * @return the upper mode
	 */
	public int getUPPER_MODE() {
		return UPPER_MODE;
	}

	/**
	 * Gets the lower mode.
	 *
	 * @return the lower mode
	 */
	public int getLOWER_MODE() {
		return LOWER_MODE;
	}

	
	
	public boolean isDataPresent(){
		return form.isDataPresent();
	}
	

}
