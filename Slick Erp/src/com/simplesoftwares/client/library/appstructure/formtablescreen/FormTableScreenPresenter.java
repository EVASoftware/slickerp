package com.simplesoftwares.client.library.appstructure.formtablescreen;

import java.util.Collections;
import java.util.List;

import com.google.code.p.gwtchismes.client.GWTCAlert;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.EntityPresenter;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.views.history.HistoryPopup;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.helperlayer.User;

/**
 * Presenter for FormTable screens.
 * @author Kamala
 *
 * @param <T>
 */
public abstract class FormTableScreenPresenter<T extends SuperModel>extends EntityPresenter<T>
{
	
	/**
     * Reffers to {@link FormTableScreen} type screen
     */
	protected FormTableScreen<T>tableScreenView;
	
	/**
	 * Instantiates a new FormtTableScreen with passed view and model
	 * @param view Screen for this presenter
	 * @param model Model for this presenter
	 */
	public FormTableScreenPresenter(FormTableScreen<T> view, T model) {
		super(view, model);
		tableScreenView=view;
		
	}

	/**
	 * If updateTable() is false returns without saving.If not
	 * Sets the screen in new state & makes a new model
	 */
	@Override
	public void reactOnSave() {
		
		if(!updateTable())
		{
			//view.clear();
			
			return;
			
		}
		
		view.showWaitSymbol();
		
		service.save(model,new AsyncCallback<ReturnFromServer>() {
			
			@Override
			public void onSuccess(ReturnFromServer result) {
				view.hideWaitSymbol();
				model.setCount(result.count);
				model.setId(result.id);
				
				if(model instanceof ConfigCategory){
					ConfigCategory conCat=(ConfigCategory) model;
					conCat.setCategoryCode("CC"+model.getCount());
					
				}
				
				if(model instanceof Type){
					Type conCat=(Type) model;
					conCat.setTypeCode("TT"+conCat.getCount());
				}
				
				view.setToNewState();
				makeNewModel();
				view.showDialogMessage("Data Successfully Saved !",GWTCAlert.OPTION_ROUNDED_BLUE , GWTCAlert.OPTION_ANIMATION);
				;
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				view.hideWaitSymbol();
				view.showDialogMessage("Data Save Unsuccessful! Try again.");
				
			}
		});
		
	}
	
	
	/*
	 * Complicated flow but braino should understand AND KOMAL IS A BRAINO
	 */
	
	/**
	 *  Validates the Table.Checks whether the called model is duplicate & if duplicate returns false,
	 *  if not adds it in the dataprovider in list(only when object is not already added) & returns true.
	 *  
	 * @return true if added in table false model to be added wad duplicate
	 */
	protected boolean updateTable()
	{
		// Updates the model if validation is true
	    if(!validateAndUpdateModel())
		    return false;
		//Data provider list of table
		List<T> data=null;
		//if tableScreenView's table is not null get the dataprovider list from its table 
		if(tableScreenView.getSupertable()!=null)
			data=this.tableScreenView.getSupertable().getDataprovider().getList();
		
		//for the called model check whether its duplicate in entire list, if duplicate return false
	   for(int i=0;i<data.size();i++)
	   {
		   SuperModel temp=data.get(i);
		   /**
			 * Updated by :Viraj
			 * Date: 27-11-2018
			 * Description: to check whether userId,EmployeeName and EmailId in user screen in duplicate
			 * @return: True or False 
			 */
		   if(temp instanceof User){
			   boolean validate = validUser((User) temp);
			   if(validate == false) {
				   return false;
			   }
		   }
		   /** Ends **/
		   else if(temp.isDuplicate(model))
		   {
			  
			  view.showDialogMessage("Can't Add! Duplicate Data");
			  return false;
			  
		   }
		  
	    }
		
	    // If model does not exist in table then ony add otherwise not
		    if(model.getId()==null)
		    {
			     data.add((T) model);
			     
			    
		    }
		  //refresh the table
		 tableScreenView.getSupertable().getDataprovider().refresh();
		 Collections.sort(data);
		 
		
		return true;
	}
	
	/**
	 * Updated by :Viraj
	 * Date: 27-11-2018
	 * Description: to check whether userId,EmployeeName and EmailId in user screen in duplicate
	 * @return: True or False 
	 */
	private boolean validUser(User temp) {
		
		if(temp.isDuplicateUser(model)) {
			view.showDialogMessage("Can't Add duplicate User Name");
			return false;
		}else {
			if(temp.isDuplicateEmployee(model)) {
				view.showDialogMessage("Can't Add duplicate Employee Name");
				return false;
			}
			else {
//				if(temp.isDuplicateEmail(model)) {
//					view.showDialogMessage("Can't Add duplicate Email Id");
//					return false;
//				}else {
					return true;
//				}
			}
		}
		
			
	}
/** Ends **/

	@Override
	public void reactOnDownload() {
		
	}
	

	
	@Override
	protected void reactOnHistory() {
		HistoryPopup popup = new HistoryPopup();
		LoginPresenter.panel=new PopupPanel(true);
		popup.getTable().getDataprovider().setList(LoginPresenter.globalHistoryList);
		LoginPresenter.panel.add(popup);
		LoginPresenter.panel.center();
		LoginPresenter.panel.show();
		
	}
}
