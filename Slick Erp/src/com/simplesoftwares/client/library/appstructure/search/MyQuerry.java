package com.simplesoftwares.client.library.appstructure.search;

import java.io.Serializable;
import java.util.Vector;

import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.userconfigurations.UserConfiguration;

/**
 * Represents a Query object which will be sent from client side to search kind on server side.
 * To Do : Order and Limits are important
 * @author Kamala
 *
 */
public class MyQuerry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -957633658995321375L;
	/**
	 * Vector of MyFilterValues encapsulating multiple filter values;
	 */
	private Vector<Filter>filters;
	/**
	 *To Do: Why this object is needed ?
	 */
	SuperModel querryObject;
	public MyQuerry() {
		super();
		filters=new Vector<Filter>();
		Filter companyid=new Filter();
		if(UserConfiguration.getCompanyId()!=null)
		{
			companyid.setLongValue(UserConfiguration.getCompanyId());
			companyid.setQuerryString("companyId");
			this.filters.add(companyid);
		}
		
	}
	public MyQuerry(Vector<Filter>filters, SuperModel querryObject) {
		super();
		this.filters=filters;
		this.querryObject = querryObject;
		
		Filter companyid=new Filter();
		if(UserConfiguration.getCompanyId()!=null)
		{
			companyid.setLongValue(UserConfiguration.getCompanyId());
			companyid.setQuerryString("companyId");
			this.filters.add(companyid);
		}
	}
	
	public Vector<Filter> getFilters() {
		return filters;
	}
	public void setFilters(Vector<Filter> filters) {
		
		 this.filters=filters;
		 if(this.filters!=null)
		 {
			 Filter companyid=new Filter();
				if(UserConfiguration.getCompanyId()!=null)
				{
					companyid.setLongValue(UserConfiguration.getCompanyId());
					companyid.setQuerryString("companyId");
					this.filters.add(companyid);
				}
		 }
		 }
	public SuperModel getQuerryObject() {
		return querryObject;
	}
	public void setQuerryObject(SuperModel querryObject) {
		this.querryObject = querryObject;
	}

	
	
}
