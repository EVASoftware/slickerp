package com.simplesoftwares.client.library.appstructure.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.Key;
import com.google.gwt.user.client.rpc.IsSerializable;


/**
 * Allows us to send filters on server in data type independent way
 * To DO : Attributes should contain all the permissable GAE data types.
 * @author Kamala
 *
 */
public class Filter implements IsSerializable {
	/**
	 * For integer filter value
	 */
	Integer intValue;
	/**
	 * For string filter value
	 */
	String  stringValue;
	/**
	 * For long filter values.
	 */
	Long    longValue;
	/**
	 * For Date filter values
	 */
	Date    dateValue;
	/**
	 * for boolean filte value
	 */
	Boolean booleanvalue;
	/**
	 * For raw key filter.
	 */
	Key key;
	/**
	 * Kind property name
	 */
	String querryString;
	/**
	 * For objectify type key
	 */
	com.googlecode.objectify.Key<?>objectifykey;
	
	List<String> list;

	
	
	public Filter()
	{
	}


	public Integer getIntValue() {
		return intValue;
	}


	public void setIntValue(Integer intValue) {
		this.intValue = intValue;
	}


	public String getStringValue() {
		return stringValue;
	}


	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}


	public Long getLongValue() {
		return longValue;
	}


	public void setLongValue(Long longValue) {
		this.longValue = longValue;
	}
	
	
	
	
   public Boolean isBooleanvalue() {
		return booleanvalue;
	}


	public void setBooleanvalue(Boolean booleanvalue) {
		this.booleanvalue = booleanvalue;
	}
	/**
	 * returns the filter's value.
	 * @return
	 */


public Object geFilterValue()
   {
	   if(intValue!=null)
		   return intValue;
	   else if(longValue!=null)
		   return longValue;
	   else if(stringValue!=null)
		   return stringValue;
	   else if(dateValue!=null)
		   return dateValue;
	   else if(key!=null)
		   return key;
	   else if(objectifykey!=null)
		   return objectifykey;
	   else if(booleanvalue!=null)
		   return booleanvalue;
	   else if(list!=null)
		   return list;
		   
	   
	return null;
   }


public Date getDateValue() {
	return dateValue;
}


public void setDateValue(Date dateValue) {
	this.dateValue = dateValue;
}




public String getQuerryString() {
	return querryString;
}


public void setQuerryString(String querryString) {
	this.querryString = querryString;
}


public Key getKey() {
	return key;
}


public void setKey(Key key) {
	this.key = key;
}


public com.googlecode.objectify.Key<?> getObjectifykey() {
	return objectifykey;
}


public void setObjectifykey(com.googlecode.objectify.Key<?> objectifykey) {
	this.objectifykey = objectifykey;
}

public List<String> getList() {
	return list;
}

public void setList(List<String> list) {
	this.list = list;
}



}
