package com.simplesoftwares.client.library;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;





import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.simplesoftwares.client.library.mywidgets.DateBoxWithYearSelector;
import com.simplesoftwares.client.library.mywidgets.MyLongBox;


// TODO: Auto-generated Javadoc
/*
 * Design goal make this class widget argument independent
 */

/**
 * The Class FormField. Creates the ui objects.
 */
public class FormField
{
	/** Label of the widget field. */
	private String label;
	
	/** Rowspan for the widget field. */
	private int rowspan;
	
	/** Rowspan for the widget field. */
	private int colspan;
	
	/** True if the widget field is mandatory. */
	private boolean isMandatory;
	
	/** Type of the widget field. */
	private FieldType widgetType;
	
	/** Widget. */
	private Widget widget;
	
	/** Mandatory message for mandatory fields. */
	private String mandatoryMsg;
	

	private InlineLabel mandatoryMsgWidget;
	private InlineLabel headerLabel;
	
	/**
	 * @author Anil
	 * @since 28-05-2020
	 */
	private boolean isVisible;
	
	/**
	 * @author Anil
	 * @since 12-10-2020
	 * For Menu bar screen we need to separate key fields from Tab view panel
	 */
	public boolean isKeyField=false;
	
	/**
	 * Instantiates a new form field.
	 */
	public FormField() {
		super();
		
	}


	
/**
 * Instantiates a new form field.
 *
 * @param label the label of UI field
 * @param rowspan the rowspan of UI field
 * @param colspan the colspan of UI field
 * @param isMandatory the is mandatory of UI field
 * @param widgetType the widget type of UI field
 * @param widget the widget
 * @param mandatorymsg the mandatorymsg of mandatory UI field
 */
public FormField(String label, int rowspan, int colspan, boolean isMandatory,
		FieldType widgetType, Widget widget,String mandatorymsg) {
	super();
	this.label = label;
	this.rowspan = rowspan;
	this.colspan = colspan;
	this.isMandatory = isMandatory;
	this.widgetType = widgetType;
	this.widget = widget;
	this.mandatoryMsg=mandatorymsg;
	this.mandatoryMsgWidget=new InlineLabel(mandatorymsg);
}



	/**
	 * Instantiates a new form field using the Builder pattern {@link FormFieldBuilder}.
	 *
	 * @param builder the builder
	 */
	public FormField(FormFieldBuilder builder)
	{
		this.label=builder.label;
		this.rowspan=builder.rowspan;
		this.colspan=builder.colspan;
		this.isMandatory=builder.isMandatory;
		this.widgetType=builder.widgetType;
		this.widget=builder.widget;
		this.mandatoryMsg=builder.mandatoryMsg;
		if(mandatoryMsg!=null)
		{
			this.mandatoryMsgWidget=new InlineLabel(mandatoryMsg);
			this.mandatoryMsgWidget.setStyleName("mandatoryLableStyle");
		}
		/**
		 * @author Anil
		 * @since 28-05-2020
		 */
		this.isVisible=builder.isVisible;
		this.isKeyField=builder.isKeyField;
		applyStyle();
	}
	
	


/**
 * Applies style on UI fields.
 */
private  void applyStyle()
	{
		
		  if(widget instanceof TextBox)
			   widget.addStyleName("commonheight");
			 
		  if(widget instanceof DateBoxWithYearSelector)
			 {
			  DateBoxWithYearSelector db = (DateBoxWithYearSelector) widget;
			  db.addStyleName("commonheight");
//			  DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MM-dd");
			  DateTimeFormat format=DateTimeFormat.getFormat("dd-MM-yyyy");
			  DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
			  db.setFormat(defformat);
			 }
		  
		  if(widget instanceof DateBox)
			 {
			  DateBox db = (DateBox) widget;
			  db.addStyleName("commonheight");
//			  DateTimeFormat format=DateTimeFormat.getFormat("yyyy-MM-dd");
			  DateTimeFormat format=DateTimeFormat.getFormat("dd-MM-yyyy");
			  DateBox.DefaultFormat defformat=new DateBox.DefaultFormat(format);
			  db.setFormat(defformat);
			 }
			 
			 if(widget instanceof PasswordTextBox)
				 widget.addStyleName("commonheight");
			 
			 if(widget instanceof SuggestBox)
				 widget.addStyleName("commonheight");
			
			 if(widget instanceof LongBox)
			{
				 LongBox lgbox=(LongBox) widget;
				 lgbox.addKeyPressHandler(new NumbersOnly());
				 widget.setStyleName("commonheightBox");
			}
       
			 if(widget instanceof MyLongBox)
			 {
				 MyLongBox mylongbox=(MyLongBox) widget;
				 mylongbox.addKeyPressHandler(new NumbersOnly());
				 mylongbox.setStyleName("commonheightBox");
				 widget=mylongbox;
			 }
			
			 if(widget instanceof TextArea)
			{
				 TextArea ta = (TextArea) widget;
				 widget=ta;
				 widget.setWidth("99%");
			}
		
			 if(widget instanceof IntegerBox)
			 {
				 IntegerBox intbox=(IntegerBox) widget;
				 intbox.setStyleName("commonheightBox");
				 widget=intbox;
				 intbox.addKeyPressHandler(new NumbersOnly());
			 }
		if(widget instanceof FileUpload)
		{
			FileUpload upload = (FileUpload) widget;
			upload.setName("upload");
			FormPanel panel = new FormPanel();
			panel.add(upload);
			panel.setEncoding(FormPanel.ENCODING_MULTIPART);
			panel.setMethod(FormPanel.METHOD_POST);
			
		}
		 if(widget instanceof DoubleBox)
		 {
			DoubleBox doublebox=(DoubleBox) widget;
			doublebox.setStyleName("commonheightBox");
			doublebox.addKeyPressHandler(new NumbersOnly());
			
		 }
		 
		 if(widgetType==FieldType.Empty)
			widget=null;
			
		 if(widgetType==FieldType.Grouping)
			widget=null;
	
		if(widget instanceof ListBox)
		{
			ListBox lb=(ListBox) widget;
			lb.setStyleName("commonheightListbox");
		}
		
		
	}
	
/**
 * Gets the label.
 *
 * @return the label
 */
public String getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Gets the rowspan.
	 *
	 * @return the rowspan
	 */
	public int getRowspan() {
		return rowspan;
	}

	
	

	/**
	 * Gets the colspan.
	 *
	 * @return the colspan
	 */
	public int getColspan() {
		return colspan;
	}

	/**
	 * Sets the colspan.
	 *
	 * @param colspan the new colspan
	 */
	public void setColspan(int colspan) {
		this.colspan = colspan;
	}

	/**
	 * Checks if is mandatory.
	 *
	 * @return true, if is mandatory
	 */
	public boolean isMandatory() {
		return isMandatory;
	}

	/**
	 * Sets the mandatory.
	 *
	 * @param isMandatory the new mandatory
	 */
	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	/**
	 * Gets the widget.
	 *
	 * @return the widget
	 */
	public Widget getWidget() {
		return widget;
	}

	/**
	 * Sets the widget.
	 *
	 * @param widget the new widget
	 */
	public void setWidget(Widget widget) {
		this.widget = widget;
	}

	/**
	 * Gets the widget type.
	 *
	 * @return the widget type
	 */
	public FieldType getWidgetType() {
		return widgetType;
	}

	/**
	 * Sets the widget type.
	 *
	 * @param widgetType the new widget type
	 */
	public void setWidgetType(FieldType widgetType) {
		this.widgetType = widgetType;
	}
	

	
	public InlineLabel getMandatoryMsgWidget() {
		return mandatoryMsgWidget;
	}



	public void setMandatoryMsgWidget(InlineLabel mandatoryMsgWidget) {
		this.mandatoryMsgWidget = mandatoryMsgWidget;
	}
	
	/**
	 * Gets the mandatory msg.
	 *
	 * @return the mandatory msg
	 */
	public String getMandatoryMsg() {
		return mandatoryMsg;
	}



	/**
	 * Sets the mandatory msg.
	 *
	 * @param mandatoryMsg the new mandatory msg
	 */
	public void setMandatoryMsg(String mandatoryMsg) {
		this.mandatoryMsg = mandatoryMsg;
	}

	/**
	 * Sets the rowspan.
	 *
	 * @param rowspan the new rowspan
	 */
	public void setRowspan(int rowspan) {
		this.rowspan = rowspan;
	}

	/**
	 * The Class NumbersOnly. Inner class that allows only numeric values in UI fields
	 * eg. Longbox, Integerbox
	 */
	class NumbersOnly implements KeyPressHandler {
        
        /* (non-Javadoc)
         * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
         */
        @Override
        public void onKeyPress(KeyPressEvent event) {
        	
            if(!(Character.isDigit(event.getCharCode())||event.getCharCode()=='.'))
            	
            	{
            	Widget widg=(Widget) event.getSource();
            	if(widg instanceof MyLongBox )
            		((MyLongBox)event.getSource()).cancelKey();
            	if(widg instanceof LongBox )
                    	((LongBox)event.getSource()).cancelKey();
            	if(widg instanceof IntegerBox )
                    	((IntegerBox)event.getSource()).cancelKey();
            	if(widg instanceof DoubleBox )
                	((DoubleBox)event.getSource()).cancelKey();
            	}
        }
        
        
    }

	/**
	 * Adds the in list box from string array.
	 *
	 * @param arraylist the arraylist
	 */
	public void addInListBoxFromStringArray(ArrayList<String>arraylist)
	{
		ListBox lb=(ListBox) widget; 
		switch(widgetType)
		{
		case CityListBox:
			lb.addItem("---Select City---");
			lb.addItem("Mumbai");
			lb.addItem("Pune");
			lb.addItem("Thane");
			
			break;
		case CountryListBox:
			lb.addItem("---Select Country---");
			lb.addItem("India");
			break;
		case StateListBox:
			lb.addItem("---Select State---");
			lb.addItem("Maharashtra");
			break;
		default:
			break;
		}
		
		TreeSet<String>hashset=new TreeSet<String>();
		for(String entity:arraylist)
		   hashset.add(entity);
		
		Iterator<String> iter = hashset.iterator();
		
		  while(iter.hasNext())
			  lb.addItem(iter.next());
	}



	public InlineLabel getHeaderLabel() {
		return headerLabel;
	}



	public void setHeaderLabel(InlineLabel headerLabel) {
		this.headerLabel = headerLabel;
	}



	public boolean isVisible() {
		return isVisible;
	}



	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
	
}
